unit EvDBMaintenanceMod;

interface

uses isBaseClasses, SysUtils, DateUtils, evDBMaintQueue, EvContext, isThreadManager,
     EvCommonInterfaces, EvMainboard, isBasicUtils, isLogFile;

implementation

type
  TevDBMaintenance = class(TisInterfacedObject, IevDBMaintenance)
  private
    FQueue: IevDBMaintQueue;
    FStartTime: TDateTime;
    FElapsedSeconds: Cardinal;
    procedure Start;
    procedure RunPatchingInThread(const Params: TTaskParamList);
  protected
    procedure DoOnConstruction; override;
    procedure InitThreadsPerServer;
    function  ThreadsPerServer: IisListOfValues;
    procedure UpdateThreadsPerServer(const AHost: String; const AMaxThreads: Integer);
    function  GetOnQueueChanged: TOnQueueChangedEvent;
    procedure SetOnQueueChanged(const AValue: TOnQueueChangedEvent);
    procedure ClearQueue;
    function  GetStatus: TevQueueStatus;
    function  GetElapsedSeconds: Cardinal;
    function  GetExecutingItems: IisParamsCollection;
    function  GetErroredItems: IisParamsCollection;
    procedure RunBackup(const ADatabases: array of TevBackupDBParam; const AFolder: String; const ACompress: Boolean);
    procedure RunRestore(const ADatabases: array of TevRestoreDBParam);
    procedure RunImport(const ADatabases: array of TevImportDBParam);
    procedure RunPatching(const AAutoSweep: Boolean; const AAppVersion: String; AAllowDowngrade: Boolean);
    procedure RunPatchingAsync(const AAutoSweep: Boolean; const AAppVersion: String; AAllowDowngrade: Boolean; const ACallBack: PevAsyncCallBack);
    procedure RunSweep(const ADatabases: array of TevDBParam);
    procedure RunCustomScript(const ADatabases: array of TevBackupDBParam; const AScript: String; const ASweep: Boolean);
    procedure RunTidyUpDBFolders;
  end;


function GetDBMaintenance: IevDBMaintenance;
begin
  Result := TevDBMaintenance.Create;
end;


{ TevDBMaintenance }

procedure TevDBMaintenance.DoOnConstruction;
begin
  inherited;
  FQueue := CreateDBPatchQueue;
end;

function TevDBMaintenance.GetElapsedSeconds: Cardinal;
begin
  if FQueue.Active then
    FElapsedSeconds := SecondsBetween(Now, FStartTime);
  Result := FElapsedSeconds;
end;

procedure TevDBMaintenance.RunBackup(const ADatabases: array of TevBackupDBParam; const AFolder: String; const ACompress: Boolean);
var
  i: Integer;
begin
  ClearQueue;
  for i := Low(ADatabases) to High(ADatabases) do
    FQueue.AddDBBackupItem(ADatabases[i].Name, ADatabases[i].SizeMB, AFolder, ACompress, ADatabases[i].Scramble);

  Start;
end;

procedure TevDBMaintenance.RunImport(const ADatabases: array of TevImportDBParam);
var
  i: Integer;
begin
  ClearQueue;
  for i := Low(ADatabases) to High(ADatabases) do
    FQueue.AddDBImportItem(ADatabases[i].SourceDB, ADatabases[i].SourcePassword, ADatabases[i].DBSizeMB);

  Start;
end;

procedure TevDBMaintenance.RunSweep(const ADatabases: array of TevDBParam);
var
  i: Integer;
begin
  ClearQueue;
  for i := Low(ADatabases) to High(ADatabases) do
    FQueue.AddDBSweepItem(ADatabases[i].Name, ADatabases[i].SizeMB);

  Start;
end;

procedure TevDBMaintenance.RunPatchingInThread(const Params: TTaskParamList);
var
  CallBack: TMethod;
  Err: String;
begin
  CallBack.Data := Pointer(Integer(Params[3]));
  CallBack.Code := Pointer(Integer(Params[4]));
  Err := '';
  try
    RunPatching(Params[0], Params[1], Params[2]);
  except
    on E: Exception do
    begin
      if Assigned(CallBack.Code) then
        Err := E.Message
      else
        raise;
    end;
  end;

  if Assigned(CallBack.Code) then
    PevAsyncCallBack(Callback)(Err);
end;

procedure TevDBMaintenance.RunPatching(const AAutoSweep: Boolean; const AAppVersion: String; AAllowDowngrade: Boolean);
begin
  ctx_DBAccess.SyncUDFAndApp;
  ClearQueue;
  FQueue.PopulateWithDBPatchItems(AAutoSweep, AAppVersion, AAllowDowngrade);
  Start;
end;

procedure TevDBMaintenance.Start;
begin
  if not FQueue.Active then
    FStartTime := Now;

  FQueue.Active := True;
end;

procedure TevDBMaintenance.RunTidyUpDBFolders;
begin
  ClearQueue;
  FQueue.PopulateWithTidyUpDBFoldersItems;
  Start;
end;

procedure TevDBMaintenance.RunPatchingAsync(const AAutoSweep: Boolean; const AAppVersion: String; AAllowDowngrade: Boolean; const ACallBack: PevAsyncCallBack);
begin
  if Assigned(ACallBack) then
    Context.RunParallelTask(RunPatchingInThread, Self, MakeTaskParams([AAutoSweep, AAppVersion, AAllowDowngrade, TMethod(ACallBack).Data, TMethod(ACallBack).Code]))
  else
    Context.RunParallelTask(RunPatchingInThread, Self, MakeTaskParams([AAutoSweep, AAppVersion, AAllowDowngrade, 0, 0]));
end;

function TevDBMaintenance.GetOnQueueChanged: TOnQueueChangedEvent;
begin
  Result := FQueue.OnQueueChanged;
end;

procedure TevDBMaintenance.SetOnQueueChanged(const AValue: TOnQueueChangedEvent);
begin
  FQueue.OnQueueChanged := AValue;
end;

function TevDBMaintenance.GetStatus: TevQueueStatus;
begin
  Result := FQueue.GetStatus;
end;

function TevDBMaintenance.GetExecutingItems: IisParamsCollection;
var
  L: IisList;
  i: Integer;
  LV: IisListOfValues;
begin
  L := FQueue.GetItems([qisExecuting]);

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    LV := Result.AddParams(IntToStr(i));
    LV.AddValue('DBName', (L[i] as IevDBItem).GetDBName);
    LV.AddValue('DBHost', (L[i] as IevDBItem).GetDBHost);
    LV.AddValue('Progress', (L[i] as IevDBItem).GetProgress);
    LV.AddValue('Status', (L[i] as IevDBItem).GetStatus);
  end;
end;

function TevDBMaintenance.GetErroredItems: IisParamsCollection;
var
  L: IisList;
  i: Integer;
  LV: IisListOfValues;
begin
  L := FQueue.GetItems([qisFinishedWithErrors]);

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    LV := Result.AddParams(IntToStr(i));
    LV.AddValue('DBName', (L[i] as IevDBItem).GetDBName);
    LV.AddValue('DBHost', (L[i] as IevDBItem).GetDBHost);
    LV.AddValue('Status', (L[i] as IevDBItem).GetStatus);
    LV.AddValue('Details', (L[i] as IevDBItem).GetDetails);    
  end;
end;

procedure TevDBMaintenance.RunRestore(const ADatabases: array of TevRestoreDBParam);
var
  i: Integer;
begin
  ClearQueue;
  for i := Low(ADatabases) to High(ADatabases) do
    FQueue.AddDBRestoreItem(ADatabases[i].Name, ADatabases[i].SizeMB, ADatabases[i].FileName);

  Start;
end;

procedure TevDBMaintenance.RunCustomScript(
  const ADatabases: array of TevBackupDBParam; const AScript: String;
  const ASweep: Boolean);
var
  i: Integer;
begin
  ClearQueue;
  for i := Low(ADatabases) to High(ADatabases) do
    FQueue.AddDBCustomScriptItem(ADatabases[i].Name, ADatabases[i].SizeMB, AScript, ASweep);

  Start;

end;

function TevDBMaintenance.ThreadsPerServer: IisListOfValues;
begin
  Result := FQueue.ThreadsPerServer;
end;

procedure TevDBMaintenance.InitThreadsPerServer;
var
  DBServersInfo: IisParamsCollection;
  i, n: Integer;
begin
  DBServersInfo := ctx_DBAccess.GetDBServersInfo;
  for i := 0 to DBServersInfo.Count - 1 do
  begin
    n := DBServersInfo[i].TryGetValue('CPU.Cores', 0);
    if n = 0 then
      n := 8
    else if n > 24 then
    begin
      n :=  24;
      mb_LogFile.AddEvent(etError, 'DBMaintenance', 'UDFs reported CPU.Cores=' + IntToStr(n), 'TevDBMaintenance.InitThreadsPerServer');
    end;

    FQueue.UpdateThreadsPerServer(DBServersInfo.ParamName(i), n);
  end;
end;

procedure TevDBMaintenance.UpdateThreadsPerServer(const AHost: String; const AMaxThreads: Integer);
begin
  FQueue.UpdateThreadsPerServer(AHost, AMaxThreads);
end;

procedure TevDBMaintenance.ClearQueue;
begin
  FQueue.Clear;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetDBMaintenance, IevDBMaintenance, 'Database Maintenance Module');

end.
