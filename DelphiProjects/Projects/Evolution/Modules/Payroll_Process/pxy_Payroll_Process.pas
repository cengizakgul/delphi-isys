// Module "Payroll Processing"
unit pxy_Payroll_Process;

interface

implementation

uses EvMainboard, EvCommonInterfaces, isBaseClasses, EvTypes, EvStreamUtils, evRemoteMethods,
     EvTransportDatagrams, evProxy, EvTransportInterfaces, 
  ISKbmMemDataSet;

type
  TevPayrollProcessingProxy = class(TevProxyModule, IevPayrollProcessing)
  protected
    procedure TaxCalculator(aPR, aPR_BATCH, aPR_CHECK, aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI, aPR_CHECK_LOCALS: IisStream;
      DisableYTDs, DisableShortfalls, NetToGross: Boolean);
    function GrossToNet(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction: Boolean; JustPreProcess: Boolean;
      FetchPayrollData: Boolean; SecondCheck: Boolean): Boolean; // Result indicates if there were any shortfalls
    function VoidCheck(CheckToVoid: Integer; PrBatchNbr: Integer): Integer;
    procedure RedistributeDBDT(const PrCheckLineNbr: Integer; Redistribution: IisParamsCollection);
    procedure UnvoidCheck(CheckNbr: Integer);
    function GetPayrollBatchDefaults(const PrNbr: Integer): IisListOfValues;
    procedure RefreshSchduledEDsCheck(const CheckNbr: Integer);
    function ProcessBilling(prNumber: Integer; ForPayroll: Boolean;
                             InvoiceNumber: Integer; ReturnDS: Boolean): Variant;
    procedure RefreshSchduledEDsBatch(const BatchNbr: Integer; const RegularChecksOnly: Boolean);
    function  DoQuarterEndCleanup(CoNbr: Integer; QTREndDate: TDateTime; MinTax: Double;
                                  CleanupAction: Integer; const ReportFilePath: string;
                                  const EECustomNbr: string; ForceCleanup: Boolean;
                                  PrintReports: Boolean; CurrentQuarterOnly: Boolean;
                                  const LockType: TResourceLockType; ExtraOptions: Variant; out Warnings: String): Boolean;
    function  DoPAMonthEndCleanup(CoNbr: Integer; MonthEndDate: TDateTime; MinTax: Double;
                                  const AutoProcess: Boolean; const EECustomNbr: string;
                                  const PrintReports: Boolean; const LockType: TResourceLockType;
                                  out Warnings: String): Boolean;
    function ProcessPayroll(PayrollNumber: Integer; JustPreProcess: Boolean;
      out Log: string;
      // Log contains a detailed log of the start times of the steps taken
      // during executing the ProcessPayroll functionality. The format is the
      // same as that of the Text property of a TStringList.
      const CheckReturnQueue: Boolean;
      const LockType: TResourceLockType = rlTryOnce;
      const PrBatchNbr: Integer = 0): String;
    function PrintPayroll(PrNbr: Integer; PrintChecks: Boolean;UseSbMiskCheckForm:boolean=false;DefaultMiskCheckForm:String=''): IisStream;
    function ReprintPayroll(PrNbr: Integer; PrintReports, PrintInvoices, CurrentTOA: Boolean;
      PayrollChecks, AgencyChecks, TaxChecks, BillingChecks, CheckForm, MiscCheckForm: String; CheckDate: TDateTime;
      SkipCheckStubCheck: Boolean; DontPrintBankInfo: Boolean; HideBackground: Boolean; DontUseVMRSettings: Boolean; ForcePrintVoucher: Boolean;
      PrReprintHistoryNBR: Integer; Reason: String; out AWarnings, AErrors: String): IisStream;
    procedure ReCreateVmrHistory(const CoNbr: Integer; const PeriodBeginDate, PeriodEndDate: TDateTime);
    procedure ReRunPayrolReportsForVmr(const PrNbr: Integer);
    procedure DoERTaxAdjPayroll(aDate: TDateTime);
    procedure DoQtrEndTaxLiabAdj(CoNbr: Integer; aDate: TDateTime; MinLiability: Double);
    procedure CreateTaxAdjCheck(aDate: TDateTime);
    procedure CleanupTaxableTips(PeriodBegin: TDateTime; PeriodEnd: TDateTime);
    function CreatePR(Co_Nbr: Integer; CheckDate: TDateTime): Integer;
    procedure CreatePRBatchDetails(PrBatchNumber: Integer; TwoChecksPerEE, CalcCheckLines: Boolean; CheckType, Check945Type: String;
      NumberOfChecks, SalaryOrHourly: Integer; EENumbers: Variant;
      const TcFileIn, TcFileOut: IisStream;
      const TcLookupOption: TCImportLookupOption; const TcDBDTOption: TCImportDBDTOption;
      const TcFourDigitYear: Boolean; const TcFileFormat: TCFileFormatOption;
      const TcAutoImportJobCodes: Boolean; const TcUseEmployeePayRates: boolean; const UpdateBalance: Boolean;
      const IncludeTimeOffRequests: Boolean);
    function  LoadYTD(Cl_Nbr: Integer; Co_Nbr: Integer; Year: Integer; EeNbr: Integer): Variant;
    function  GetEEYTD(const AYear: Integer; const AEeNbr: Integer): IisListOfValues;
    function ProcessBillingForServices(ClNbr: Integer; CoNbr: Integer; const List: string; const EffDate: TDateTime; const Notes: string; var bManualACH : Boolean): Real;
    function  IsQuarterEndCleanupNecessary(CoNbr: Integer; QTREndDate: TDateTime): Boolean;
    procedure ProcessPrenoteACH(ACHOptions: IevACHOptions; APrenoteCLAccounts: Boolean; ACHDataStream: IEvDualStream;
      out AchFile, AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
    procedure ProcessManualACH(ACHOptions: IevACHOptions; bPreprocess: Boolean; BatchBANK_REGISTER_TYPE: String; ACHDataStream: IEvDualStream;
      out AchFile, AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
    procedure CopyPayroll(const APayrollNbr: Integer);
    procedure VoidPayroll(const APayrollNbr: Integer;const VoidTaxChecks, VoidAgencyChecks, VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling : Boolean);
    procedure DeletePayroll(const APayrollNbr: Integer);
    function DeleteBatch(const ABatchNbr: Integer): boolean;
    procedure SetPayrollLock(const APrNbr: Integer; const AUnlocked: Boolean);
  end;


function GetPayrollProcessingProxy: IevPayrollProcessing;
begin
  Result := TevPayrollProcessingProxy.Create;
end;

{ TevPayrollProcessingProxy }

procedure TevPayrollProcessingProxy.CleanupTaxableTips(PeriodBegin,
  PeriodEnd: TDateTime);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_CLEANUPTAXABLETIPS);
  D.Params.AddValue('PeriodBegin', PeriodBegin);
  D.Params.AddValue('PeriodEnd', PeriodEnd);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.CopyPayroll(const APayrollNbr: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_COPYPAYROLL);
  D.Params.AddValue('APayrollNbr', APayrollNbr);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.VoidPayroll(const APayrollNbr: Integer; const VoidTaxChecks, VoidAgencyChecks, VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling : Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_VOIDPAYROLL);
  D.Params.AddValue('APayrollNbr', APayrollNbr);
  D.Params.AddValue('VoidTaxChecks', VoidTaxChecks);
  D.Params.AddValue('VoidAgencyChecks', VoidAgencyChecks);
  D.Params.AddValue('VoidBillingChecks', VoidBillingChecks);
  D.Params.AddValue('PrintReports', PrintReports);
  D.Params.AddValue('DeleteInvoice', DeleteInvoice);
  D.Params.AddValue('BlockBilling', BlockBilling);
  
  D := SendRequest(D);
end;

function TevPayrollProcessingProxy.CreatePR(Co_Nbr: Integer;
  CheckDate: TDateTime): Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_CREATEPR);
  D.Params.AddValue('Co_Nbr', Co_Nbr);
  D.Params.AddValue('CheckDate', CheckDate);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

procedure TevPayrollProcessingProxy.CreatePRBatchDetails(
  PrBatchNumber: Integer; TwoChecksPerEE, CalcCheckLines: Boolean;
  CheckType, Check945Type: String; NumberOfChecks, SalaryOrHourly: Integer;
  EENumbers: Variant; const TcFileIn, TcFileOut: IisStream;
  const TcLookupOption: TCImportLookupOption;
  const TcDBDTOption: TCImportDBDTOption; const TcFourDigitYear: Boolean;
  const TcFileFormat: TCFileFormatOption; const TcAutoImportJobCodes,
  TcUseEmployeePayRates: boolean; const UpdateBalance: Boolean;
  const IncludeTimeOffRequests: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_CREATEPRBATCHDETAILS);
  D.Params.AddValue('PrBatchNumber', PrBatchNumber);
  D.Params.AddValue('TwoChecksPerEE', TwoChecksPerEE);
  D.Params.AddValue('CalcCheckLines', CalcCheckLines);
  D.Params.AddValue('CheckType', CheckType);
  D.Params.AddValue('Check945Type', Check945Type);
  D.Params.AddValue('NumberOfChecks', NumberOfChecks);
  D.Params.AddValue('SalaryOrHourly', SalaryOrHourly);
  D.Params.AddValue('EENumbers', EENumbers);
  D.Params.AddValue('TcFileIn', TcFileIn);
  D.Params.AddValue('TcFileOut', TcFileOut);
  D.Params.AddValue('TcLookupOption', Ord(TcLookupOption));
  D.Params.AddValue('TcDBDTOption', Ord(TcDBDTOption));
  D.Params.AddValue('TcFourDigitYear', TcFourDigitYear);
  D.Params.AddValue('TcFileFormat', Ord(TcFileFormat));
  D.Params.AddValue('TcAutoImportJobCodes', TcAutoImportJobCodes);
  D.Params.AddValue('TcUseEmployeePayRates', TcUseEmployeePayRates);
  D.Params.AddValue('UpdateBalance', UpdateBalance);
  D.PArams.AddValue('IncludeTimeOffRequests', IncludeTimeOffRequests);

  D := SendRequest(D);
  
end;

procedure TevPayrollProcessingProxy.CreateTaxAdjCheck(aDate: TDateTime);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_CREATETAXADJCHECK);
  D.Params.AddValue('aDate', aDate);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.DoERTaxAdjPayroll(aDate: TDateTime);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_DOERTAXADJPAYROLL);
  D.Params.AddValue('aDate', aDate);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.DoQtrEndTaxLiabAdj(CoNbr: Integer;
  aDate: TDateTime; MinLiability: Double);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_DOQTRENDTAXLIABADJ);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('aDate', aDate);
  D.Params.AddValue('MinLiability', MinLiability);
  D := SendRequest(D);
end;

function TevPayrollProcessingProxy.DoQuarterEndCleanup(CoNbr: Integer;
  QTREndDate: TDateTime; MinTax: Double; CleanupAction: Integer;
  const ReportFilePath, EECustomNbr: string; ForceCleanup, PrintReports,
  CurrentQuarterOnly: Boolean; const LockType: TResourceLockType;
  ExtraOptions: Variant; out Warnings: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_DOQUARTERENDCLEANUP);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('QTREndDate', QTREndDate);
  D.Params.AddValue('MinTax', MinTax);
  D.Params.AddValue('CleanupAction', CleanupAction);
  D.Params.AddValue('ReportFilePath', ReportFilePath);
  D.Params.AddValue('EECustomNbr', EECustomNbr);
  D.Params.AddValue('ForceCleanup', ForceCleanup);
  D.Params.AddValue('PrintReports', PrintReports);
  D.Params.AddValue('CurrentQuarterOnly', CurrentQuarterOnly);
  D.Params.AddValue('LockType', Ord(LockType));
  D.Params.AddValue('ExtraOptions', ExtraOptions);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT] <> 0;
  Warnings := D.Params.Value['Warnings'];
end;

function TevPayrollProcessingProxy.IsQuarterEndCleanupNecessary(
  CoNbr: Integer; QTREndDate: TDateTime): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_ISQUARTERENDCLEANUPNECESSARY);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('QTREndDate', QTREndDate);
  D := SendRequest(D);
  Result := D.Params.Value['Result'] <> 0;  
end;

function TevPayrollProcessingProxy.GetEEYTD(const AYear: Integer; const AEeNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_GETEEYTD);
  D.Params.AddValue('AYear', AYear);
  D.Params.AddValue('AEeNbr', AEeNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevPayrollProcessingProxy.LoadYTD(Cl_Nbr, Co_Nbr,
  Year, EeNbr: Integer): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_LOADYTD);
  D.Params.AddValue('Cl_Nbr', Cl_Nbr);
  D.Params.AddValue('Co_Nbr', Co_Nbr);
  D.Params.AddValue('Year', Year);
  D.Params.AddValue('EeNbr', EeNbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevPayrollProcessingProxy.PrintPayroll(PrNbr: Integer;
  PrintChecks, UseSbMiskCheckForm: boolean;DefaultMiskCheckForm:String): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_PRINTPAYROLL);
  D.Params.AddValue('PrNbr', PrNbr);
  D.Params.AddValue('PrintChecks', PrintChecks);
  D.Params.AddValue('UseSbMiskCheckForm', UseSbMiskCheckForm);
  D.Params.AddValue('DefaultMiskCheckForm', DefaultMiskCheckForm);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
end;

function TevPayrollProcessingProxy.ProcessBilling(prNumber: Integer;
  ForPayroll: Boolean; InvoiceNumber: Integer; ReturnDS: Boolean): Variant;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_PROCESSBILLING);
  D.Params.AddValue('prNumber', prNumber);
  D.Params.AddValue('ForPayroll', ForPayroll);
  D.Params.AddValue('InvoiceNumber', InvoiceNumber);
  D.Params.AddValue('ReturnDS', ReturnDS);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevPayrollProcessingProxy.ProcessBillingForServices(ClNbr,
  CoNbr: Integer; const List: string; const EffDate: TDateTime;
  const Notes: string; var bManualACH : Boolean): Real;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_PROCESSBILLINGFORSERVICES);
  D.Params.AddValue('ClNbr', ClNbr);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('List', List);
  D.Params.AddValue('EffDate', EffDate);
  D.Params.AddValue('Notes', Notes);
  D.Params.AddValue('bManualACH', bManualACH);
  D := SendRequest(D);
  bManualACH := D.Params.Value['bManualACH'];
  Result := D.Params.Value['Result'];
end;

procedure TevPayrollProcessingProxy.ProcessManualACH(
  ACHOptions: IevACHOptions; bPreprocess: Boolean;
  BatchBANK_REGISTER_TYPE: String;
  ACHDataStream: IEvDualStream; out AchFile, AchRegularReport,
  AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_PROCESSMANUALACH);
  D.Params.AddValue('bPreprocess', bPreprocess);
  D.Params.AddValue('ACHFolder', ACHFolder);
  D.Params.AddValue('BatchBANK_REGISTER_TYPE', BatchBANK_REGISTER_TYPE);
  D.Params.AddValue('ACHOptions', ACHOptions);
  D.Params.AddValue('ACHDataStream', ACHDataStream);
  D := SendRequest(D);
  AchFile := IInterface(D.Params.Value['AchFile']) as IevDualStream;
  AchRegularReport := IInterface(D.Params.Value['AchRegularReport']) as IevDualStream;
  AchDetailedReport := IInterface(D.Params.Value['AchDetailedReport']) as IevDualStream;
  AchSave := IInterface(D.Params.Value['AchSave']) as IisListOfValues;
  AchFileName := D.Params.Value['AchFileName'];
  AExceptions := D.Params.Value['AExceptions'];
end;

function TevPayrollProcessingProxy.ProcessPayroll(PayrollNumber: Integer;
  JustPreProcess: Boolean; out Log: string;
  const CheckReturnQueue: Boolean;
  const LockType: TResourceLockType;
  const PrBatchNbr: Integer): String;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_PROCESSPAYROLL);
  D.Params.AddValue('PayrollNumber', PayrollNumber);
  D.Params.AddValue('JustPreProcess', JustPreProcess);
  D.Params.AddValue('CheckReturnQueue', CheckReturnQueue);
  D.Params.AddValue('LockType', Ord(LockType));
  D.Params.AddValue('PrBatchNbr', PrBatchNbr);
  D := SendRequest(D);
  Log := D.Params.Value['Log'];
  Result := D.Params.Value['Result'];
end;

procedure TevPayrollProcessingProxy.ProcessPrenoteACH(ACHOptions: IevACHOptions;
  APrenoteCLAccounts: Boolean; ACHDataStream: IEvDualStream; out AchFile,
  AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues;
  out AchFileName: String; out AExceptions: String; ACHFolder: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_PROCESSPRENOTEACH);
  D.Params.AddValue('ACHOptions', ACHOptions);
  D.Params.AddValue('ACHFolder', ACHFolder);
  D.Params.AddValue('APrenoteCLAccounts', APrenoteCLAccounts);
  D.Params.AddValue('ACHDataStream', ACHDataStream);
  D := SendRequest(D);
  AchFile := IInterface(D.Params.Value['AchFile']) as IevDualStream;
  AchRegularReport := IInterface(D.Params.Value['AchRegularReport']) as IevDualStream;
  AchDetailedReport := IInterface(D.Params.Value['AchDetailedReport']) as IevDualStream;
  AchSave := IInterface(D.Params.Value['AchSave']) as IisListOfValues;
  AchFileName := D.Params.Value['AchFileName'];
  AExceptions := D.Params.Value['AExceptions'];
end;

procedure TevPayrollProcessingProxy.ReCreateVmrHistory(
  const CoNbr: Integer; const PeriodBeginDate, PeriodEndDate: TDateTime);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_RECREATEVMRHISTORY);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('PeriodBeginDate', PeriodBeginDate);
  D.Params.AddValue('PeriodEndDate', PeriodEndDate);
  D := SendRequest(D);
end;

function TevPayrollProcessingProxy.ReprintPayroll(PrNbr: Integer;
  PrintReports, PrintInvoices, CurrentTOA: Boolean; PayrollChecks,
  AgencyChecks, TaxChecks, BillingChecks, CheckForm, MiscCheckForm: String;
  CheckDate: TDateTime; SkipCheckStubCheck: Boolean; DontPrintBankInfo: Boolean; HideBackground: Boolean;
  DontUseVMRSettings: Boolean; ForcePrintVoucher: Boolean; PrReprintHistoryNBR: Integer; Reason: String; out AWarnings, AErrors: String): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_REPRINTPAYROLL);
  D.Params.AddValue('PrNbr', PrNbr);
  D.Params.AddValue('PrintReports', PrintReports);
  D.Params.AddValue('PrintInvoices', PrintInvoices);
  D.Params.AddValue('CurrentTOA', CurrentTOA);
  D.Params.AddValue('PayrollChecks', PayrollChecks);
  D.Params.AddValue('AgencyChecks', AgencyChecks);
  D.Params.AddValue('TaxChecks', TaxChecks);
  D.Params.AddValue('BillingChecks', BillingChecks);
  D.Params.AddValue('CheckForm', CheckForm);
  D.Params.AddValue('MiscCheckForm', MiscCheckForm);
  D.Params.AddValue('CheckDate', CheckDate);
  D.Params.AddValue('SkipCheckStubCheck', SkipCheckStubCheck);
  D.Params.AddValue('DontPrintBankInfo', DontPrintBankInfo);
  D.Params.AddValue('HideBackground', HideBackground);
  D.Params.AddValue('DontUseVMRSettings', DontUseVMRSettings);
  D.Params.AddValue('ForcePrintVoucher', ForcePrintVoucher);
  D.Params.AddValue('PrReprintHistoryNBR', PrReprintHistoryNBR);
  D.Params.AddValue('Reason', Reason);
  D.Params.AddValue('AWarnings', AWarnings);
  D.Params.AddValue('AErrors', AErrors);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisStream;
  AWarnings := D.Params.Value['AWarnings'];
  AErrors := D.Params.Value['AErrors'];
end;

procedure TevPayrollProcessingProxy.ReRunPayrolReportsForVmr(
  const PrNbr: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_RERUNPAYROLREPORTSFORVMR);
  D.Params.AddValue('PrNbr', PrNbr);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.DeletePayroll(
  const APayrollNbr: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_DELETEPAYROLL);
  D.Params.AddValue('APayrollNbr', APayrollNbr);
  D := SendRequest(D);
end;

function TevPayrollProcessingProxy.DeleteBatch(
  const ABatchNbr: Integer): boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_DELETEBATCH);
  D.Params.AddValue('ABatchNbr', ABatchNbr);
  D := SendRequest(D);
  Result := D.Params.Value['Result'];
end;

function TevPayrollProcessingProxy.DoPAMonthEndCleanup(CoNbr: Integer;
  MonthEndDate: TDateTime; MinTax: Double; const AutoProcess: Boolean;
  const EECustomNbr: string; const PrintReports: Boolean;
  const LockType: TResourceLockType; out Warnings: String): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_DOMONTHENDCLEANUP);
  D.Params.AddValue('CoNbr', CoNbr);
  D.Params.AddValue('MonthEndDate', MonthEndDate);
  D.Params.AddValue('MinTax', MinTax);
  D.Params.AddValue('AutoProcess', AutoProcess);
  D.Params.AddValue('EECustomNbr', EECustomNbr);
  D.Params.AddValue('PrintReports', PrintReports);
  D.Params.AddValue('LockType', Ord(LockType));
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT] <> 0;
  Warnings := D.Params.Value['Warnings'];
end;

procedure TevPayrollProcessingProxy.SetPayrollLock(const APrNbr: Integer; const AUnlocked: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_SETPAYROLLLOCK);
  D.Params.AddValue('APrNbr', APrNbr);
  D.Params.AddValue('AUnlocked', AUnlocked);
  D := SendRequest(D);
end;

function TevPayrollProcessingProxy.GrossToNet(var Warnings: String;
  CheckNumber: Integer; ApplyUpdatesInTransaction, JustPreProcess,
  FetchPayrollData, SecondCheck: Boolean): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_GROSSTONET);
  D.Params.AddValue('CheckNumber', CheckNumber);
  D.Params.AddValue('ApplyUpdatesInTransaction', ApplyUpdatesInTransaction);
  D.Params.AddValue('JustPreProcess', JustPreProcess);
  D.Params.AddValue('FetchPayrollData', FetchPayrollData);
  D.Params.AddValue('SecondCheck', SecondCheck);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
  Warnings := D.Params.Value['Warnings'];
end;

function TevPayrollProcessingProxy.GetPayrollBatchDefaults(
  const PrNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_GETPAYROLLBATCHDEFAULTS);
  D.Params.AddValue('PrNbr', PrNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

procedure TevPayrollProcessingProxy.RefreshSchduledEDsBatch(
  const BatchNbr: Integer; const RegularChecksOnly: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_REFRESHSCHEDULEDEDSBATCH);
  D.Params.AddValue('BatchNbr', BatchNbr);
  D.Params.AddValue('RegularChecksOnly', RegularChecksOnly);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.RefreshSchduledEDsCheck(
  const CheckNbr: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_REFRESHSCHEDULEDEDSCHECK);
  D.Params.AddValue('CheckNbr', CheckNbr);
  D := SendRequest(D);
end;

function TevPayrollProcessingProxy.VoidCheck(CheckToVoid,
  PrBatchNbr: Integer): Integer;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_VOIDCHECK);
  D.Params.AddValue('CheckToVoid', CheckToVoid);
  D.Params.AddValue('PrBatchNbr', PrBatchNbr);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevPayrollProcessingProxy.TaxCalculator(aPR, aPR_BATCH, aPR_CHECK,
  aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI,
  aPR_CHECK_LOCALS: IisStream; DisableYTDs, DisableShortfalls,
  NetToGross: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_TAXCALCULATOR);
  D.Params.AddValue('PR', aPR);
  D.Params.AddValue('PR_BATCH', aPR_BATCH);
  D.Params.AddValue('PR_CHECK', aPR_CHECK);
  D.Params.AddValue('PR_CHECK_LINES', aPR_CHECK_LINES);
  D.Params.AddValue('PR_CHECK_LINE_LOCALS', aPR_CHECK_LINE_LOCALS);
  D.Params.AddValue('PR_CHECK_STATES', aPR_CHECK_STATES);
  D.Params.AddValue('PR_CHECK_SUI', aPR_CHECK_SUI);
  D.Params.AddValue('PR_CHECK_LOCALS', aPR_CHECK_LOCALS);
  D.Params.AddValue('DisableYTDs', DisableYTDs);
  D.Params.AddValue('DisableShortfalls', DisableShortfalls);
  D.Params.AddValue('NetToGross', NetToGross);
  D := SendRequest(D);

  aPR.Clear;
  aPR_BATCH.Clear;
  aPR_CHECK.Clear;
  aPR_CHECK_LINES.Clear;
  aPR_CHECK_LINE_LOCALS.Clear;
  aPR_CHECK_STATES.Clear;
  aPR_CHECK_SUI.Clear;
  aPR_CHECK_LOCALS.Clear;

  aPR_CHECK.CopyFrom(IInterface(D.Params.Value['PR_CHECK']) as IisStream);
  aPR_CHECK_LINES.CopyFrom(IInterface(D.Params.Value['PR_CHECK_LINES']) as IisStream);
  aPR_CHECK_LINE_LOCALS.CopyFrom(IInterface(D.Params.Value['PR_CHECK_LINE_LOCALS']) as IisStream);
  aPR_CHECK_STATES.CopyFrom(IInterface(D.Params.Value['PR_CHECK_STATES']) as IisStream);
  aPR_CHECK_SUI.CopyFrom(IInterface(D.Params.Value['PR_CHECK_SUI']) as IisStream);
  aPR_CHECK_LOCALS.CopyFrom(IInterface(D.Params.Value['PR_CHECK_LOCALS']) as IisStream);

end;

procedure TevPayrollProcessingProxy.UnvoidCheck(CheckNbr: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_UNVOIDCHECK);
  D.Params.AddValue('CheckNbr', CheckNbr);
  D := SendRequest(D);
end;

procedure TevPayrollProcessingProxy.RedistributeDBDT(
  const PrCheckLineNbr: Integer; Redistribution: IisParamsCollection);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_PAYROLL_PROCESS_REDISTRIBUTEDBDT);
  D.Params.AddValue('PrCheckLineNbr', PrCheckLineNbr);
  D.Params.AddValue('Redistribution', Redistribution);
  D := SendRequest(D);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPayrollProcessingProxy, IevPayrollProcessing, 'Payroll Processing Module');

end.
