// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_MiscCheck;

interface

uses
  SDataStructure, SysUtils, DB,  SFieldCodeValues, EvUtils,
  SMiscCheckProcedures;

function CreatePRMiscCheck(aPR_NBR: integer; AccountFieldName: string; Amount: Currency; MiscCheckType: string = ''): string;

implementation

function CreatePRMiscCheck(aPR_NBR: integer; AccountFieldName: string; Amount: Currency; MiscCheckType: string = ''): string;
var
  SerialNumber: integer;
var
  BankAccount: string;
begin
  NextCompanyAvailableCheckNumber(AccountFieldName, SerialNumber, BankAccount, MiscCheckType);
  CreateMiscCheck(aPR_NBR, MiscCheckType, Amount, SerialNumber, BankAccount, '');
  result := IntToStr(SerialNumber);
end;

end.
