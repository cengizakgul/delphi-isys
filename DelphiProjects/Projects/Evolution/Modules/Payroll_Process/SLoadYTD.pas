// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SLoadYTD;

interface

uses
   EvUtils, EvConsts, SysUtils, DB, SDataStructure, Variants, EvTypes,
  EvDataAccessComponents, EvContext, EvClientDataSet;

function LoadYTD(Cl_Nbr, Co_Nbr, Year, EeNbr: Integer): Variant;

implementation

function LoadYTD(Cl_Nbr, Co_Nbr, Year, EeNbr: Integer): Variant;
var
  BeginDate, EndDate: TDateTime;
  EndRN: Integer;
  CO_YTD_EDS, CO_YTD_TAXES: TEvClientDataSet;
begin
  ctx_DataAccess.OpenClient(CL_Nbr);

  CO_YTD_EDS := TevClientDataSet.Create(nil);
  CO_YTD_TAXES := TevClientDataSet.Create(nil);
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    BeginDate := EncodeDate(Year, 1, 1);
    EndDate := EncodeDate(Year, 12, 31);
    EndRN := 1000000;

    with TExecDSWrapper.Create('EarningsDeductionsYTDData') do
    begin
      SetMacro('SumField', 'SUM(l.hours_or_pieces) hours_or_pieces, SUM(l.Amount) Amount');
      SetParam('EeNbr', EeNbr);
      SetParam('StartDate', BeginDate);
      SetParam('StartRN', 1);
      SetParam('EndDate', EndDate);
      SetParam('EndRN', EndRN);
      SetParam('CoNbr', Co_Nbr);
      SetParam('Status', PAYROLL_STATUS_DELETED);
      SetMacro('Having', 'SUM(l.hours_or_pieces) <> 0 or SUM(l.amount) <> 0');
      SetMacro('Filter', '');
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.CUSTOM_VIEW.Open;
      CO_YTD_EDS.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
    end;

    with CO_YTD_TAXES.FieldDefs do
    begin
      Clear;
      with AddFieldDef do
      begin
        Name := 'Ee_nbr';
        DataType := ftInteger;
      end;
      with AddFieldDef do
      begin
        Name := 'Description';
        DataType := ftString;
        Size := 40;
      end;
      with AddFieldDef do
      begin
        Name := 'Amount';
        DataType := ftFloat;
      end;
    end;

    CO_YTD_TAXES.CreateDataSet;

    begin
      with TExecDSWrapper.Create('FederalTaxesYTDDataYTD') do
      begin
        SetParam('EeNbr', EeNbr);
        SetParam('StartDate', BeginDate);
        SetParam('StartRN', 1);
        SetParam('EndDate', EndDate);
        SetParam('EndRN', EndRN);
        SetParam('CoNbr', Co_Nbr);
        SetParam('Status', PAYROLL_STATUS_DELETED);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;

      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('federal_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'Federal';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['federal_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('ee_oasdi_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'OASDI';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['ee_oasdi_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('ee_medicare_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'Medicare';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['ee_medicare_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('ee_eic_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'EIC';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['ee_eic_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('back_up_withholding').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'Backup';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['back_up_withholding'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('er_oasdi_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'ER OASDI';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['er_oasdi_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('er_medicare_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'ER Medicare';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['er_medicare_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('er_fui_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'ER FUI';
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['er_fui_tax'];
          CO_YTD_TAXES.Post;
        end;

        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

      with TExecDSWrapper.Create('LocalTaxesYTDDataYTD') do
      begin
        SetParam('EeNbr', EeNbr);
        SetParam('StartDate', BeginDate);
        SetParam('StartRN', 1);
        SetParam('EndDate', EndDate);
        SetParam('EndRN', EndRN);
        SetParam('CoNbr', Co_Nbr);
        SetParam('Status', PAYROLL_STATUS_DELETED);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;

      DM_System_Local.SY_LOCALS.Activate;
      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        if (ctx_DataAccess.CUSTOM_VIEW.FieldByName('local_tax').AsFloat <> 0) and
          DM_System_Local.SY_LOCALS.Locate('sy_locals_nbr', ctx_DataAccess.CUSTOM_VIEW.FieldByName('SY_LOCALS_NBR').AsInteger, []) then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := DM_SYSTEM_Local.SY_LOCALS['NAME'];
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['local_tax'];
          CO_YTD_TAXES.Post;
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

      with TExecDSWrapper.Create('SDIAndStatesTaxesYTDDataYTD') do
      begin
        SetParam('EeNbr', EeNbr);
        SetParam('StartDate', BeginDate);
        SetParam('StartRN', 1);
        SetParam('EndDate', EndDate);
        SetParam('EndRN', EndRN);
        SetParam('CoNbr', Co_Nbr);
        SetParam('Status', PAYROLL_STATUS_DELETED);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;

      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('state_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'State ' + ctx_DataAccess.CUSTOM_VIEW.FieldByName('state').AsString;
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['state_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('ee_sdi_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'SDI ' + ctx_DataAccess.CUSTOM_VIEW.FieldByName('state').AsString;
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['ee_sdi_tax'];
          CO_YTD_TAXES.Post;
        end;

        if ctx_DataAccess.CUSTOM_VIEW.FieldByName('er_sdi_tax').AsFloat <> 0 then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := 'ER SDI ' + ctx_DataAccess.CUSTOM_VIEW.FieldByName('state').AsString;
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['er_sdi_tax'];
          CO_YTD_TAXES.Post;
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;

      with TExecDSWrapper.Create('SUITaxesYTDData') do
      begin
        SetParam('EeNbr', EeNbr);
        SetParam('StartDate', BeginDate);
        SetParam('StartRN', 1);
        SetParam('EndDate', EndDate);
        SetParam('EndRN', EndRN);
        SetParam('CoNbr', Co_Nbr);
        SetParam('Status', PAYROLL_STATUS_DELETED);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;

      DM_System_State.SY_SUI.Activate;
      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        if (ctx_DataAccess.CUSTOM_VIEW.FieldByName('sui_tax').AsFloat <> 0) and
          DM_System_State.SY_SUI.Locate('sy_sui_nbr', ctx_DataAccess.CUSTOM_VIEW.FieldByName('sy_sui_nbr').AsInteger, []) then
        begin
          CO_YTD_TAXES.Insert;
          CO_YTD_TAXES['ee_nbr'] := ctx_DataAccess.CUSTOM_VIEW['ee_nbr'];
          CO_YTD_TAXES['Description'] := DM_SYSTEM_State.SY_SUI['SUI_TAX_NAME'];
          CO_YTD_TAXES['Amount'] := ctx_DataAccess.CUSTOM_VIEW['sui_tax'];
          CO_YTD_TAXES.Post;
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
    end;
    Result := VarArrayOf([TevDataSetHolder.Create(CO_YTD_EDS) as IevDataSetHolder,
                          TevDataSetHolder.Create(CO_YTD_TAXES) as IevDataSetHolder]);
  finally
    ctx_DataAccess.RollbackNestedTransaction;
    // Do not FREE CO_YTD_EDS and CO_YTD_TAXES here!!!
  end;
end;

end.
