// Copyright � 2000-2012 iSystems LLC. All rights reserved.
unit SPAMonthEndCleanup;

interface

uses
  controls, classes,  db, dialogs, windows, EvTypes, Variants,
  EvBasicUtils, EvContext, EvExceptions, Math, EvClientDataSet;


function DoPAMonthEndCleanup(CONbr: Integer; MonthEndDate: TDateTime; MinTax: Real; AutoProcess: Boolean; EECustomNbr: String; PrintReports : Boolean; LockType: TResourceLockType; out Warnings: String): Boolean;

implementation

uses
  SysUtils, EvUtils, EvConsts, SDataStructure, SQuarterEndProcessing, SPD_ProcessException, DateUtils,
  ISKbmMemDataSet, EvStreamUtils, EvCommonInterfaces, evDataset, isBaseClasses, isBAsicUtils, SQECLocalTaxes,
  EvDataAccessComponents, EvSQLite;

function DoPAMonthEndCleanup(CONbr: Integer; MonthEndDate: TDateTime; MinTax: Real; AutoProcess: Boolean;
  EECustomNbr: String; PrintReports : Boolean; LockType: TResourceLockType; out Warnings: String): Boolean;
var
  MinimumTax, Net: Real;
  TaxWagesList, GrossWagesList, TaxList, EEList: TStringList;
  MyCount: Integer;
  aCount: String;
  MyTransactionManager: TTransactionManager;
  CheckDate: TDateTime;

  procedure StoreAmountInList(aList: TStringList; Name: string; Value: Real);
  begin
    if aList.IndexOfName(Name) = -1 then
      aList.Add(Name + '=' + FloatToStr(Value))
    else
      aList.Values[Name] := FloatToStr(StrToFloat(aList.Values[Name]) + Value);
  end;

  function SubtractWithLimit(First, Second: Real): Real;
  begin
    Result := First - Second;
    if Abs(Result) < MinimumTax then
      Result := 0;
  end;

  function CoDateStamp: string;
  begin
    Result := Trim(DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER']) + ' ' + Trim(DM_COMPANY.CO['NAME']) + ' - Check Date: ' + DateTimeToStr(CheckDate) + ' Process Date: ' + DateTimeToStr(Date) + ' ';
  end;

  function EndOfDay(const Value: TDateTime): TDateTime;
  begin
    Result := Value + 1 - 1 / 1440;
  end;

var
  Log: string;
  QecExistsForOtherCompany: Boolean;
  CheckReturnQueue: Boolean;
  PaStored: IevDataSet;
  Q: IevQuery;
begin
  Result := True; // Process payroll

  CheckDate := MonthEndDate;

  ctx_PayrollCalculation.ProcessFeedback(True);
  try
    MinimumTax := MinTax;
    MyCount := 0;

    DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(CONbr));

    DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(CONbr) + ' and PAYROLL_TYPE=''' + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + ''' and STATUS <> ''' + PAYROLL_STATUS_PROCESSED + ''' and STATUS <> ''' + PAYROLL_STATUS_VOIDED + '''');
    if DM_PAYROLL.PR.RecordCount > 0 then
    begin
      Warnings := CoDateStamp + 'Previous Quarter End Cleanup is not finished';
      Result := False;
      Exit;
    end;

    Q := TevQuery.Create('CheckIfQecExists');
    Q.Param['CO_NBR'] := CONbr;
    QecExistsForOtherCompany := Q.Result.RecordCount > 0;

    if QecExistsForOtherCompany then
    begin
      Warnings := CoDateStamp + 'Previous Quarter End Cleanup is not finished for consolidated company';
      Result := False;
      Exit;
    end;

    LockResource(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), 'A payroll for this client is already being processed by ', LockType);
    LockResource(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + IntToStr(CONbr), 'A payroll for this company is already being processed by ', LockType);
    try
  // Start getting data

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading history tables ...');
      DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + IntToStr(CONbr));
      DM_PAYROLL.PR.DataRequired('PR_NBR=0');
      DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=0');
      DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=0');
      DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_STATES_NBR=0');
      DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_CHECK_SUI_NBR=0');
      DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_CHECK_LOCALS_NBR=0');
      DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR=0');

      MyTransactionManager := TTransactionManager.Create(nil);

      TaxWagesList := TStringList.Create;
      GrossWagesList := TStringList.Create;
      TaxList := TStringList.Create;
      EEList := TStringList.Create;
      try
        if EECustomNbr = '' then
          aCount := IntToStr(DM_EMPLOYEE.EE.RecordCount)
        else
        begin
          EECustomNbr := EECustomNbr + ',';
          while Pos(',', EECustomNbr) <> 0 do
          begin
            EEList.Add(Copy(EECustomNbr, 1, Pos(',', EECustomNbr) - 1));
            System.Delete(EECustomNbr, 1, Pos(',', EECustomNbr));
          end;
          aCount := IntToStr(EEList.Count);
        end;

        with DM_EMPLOYEE, DM_PAYROLL do
        begin
          MyTransactionManager.Initialize([PR, PR_BATCH, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, DM_CLIENT.CL_BLOB, PR_CHECK_LINES]);
          EE.First;

          ctx_PayrollCalculation.ProcessFeedback(True, 'Calculating PA local taxes ...');
          ctx_DataAccess.SQLite := TSQLite.Create(':memory:');
          CalculateLocalTaxesLite(ctx_DataAccess.SQLite, CoNbr, 0, MonthEndDate, True, False);

          ctx_PayrollCalculation.ProcessFeedback(True, '...');

          while not EE.EOF do
          if (EEList.Count > 0) and (EEList.IndexOf(Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ''))) = -1) then
            EE.Next
          else
          try
            Inc(MyCount);
            ctx_PayrollCalculation.ProcessFeedback(True, 'Calculating EE #' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, '')) + '  (' +
              IntToStr(MyCount) + ' of ' + aCount + ')...');
            Net := 0;

            ctx_DataAccess.SQLite.Execute(' update QEC_LIABILITIES set'+
            '  LOCAL_TAX=round(LOCAL_TAX,2),'+
            '  LOCAL_GROSS_WAGES=round(LOCAL_GROSS_WAGES,2),'+
            '  LOCAL_TAXABLE_WAGES=round(LOCAL_TAXABLE_WAGES,2)');

            // remove PA local taxes from the list
            TaxWagesList.Clear;
            TaxList.Clear;

            PaStored := ctx_DataAccess.SQLite.Select('select * from NEW_QEC_LOCATIONS where EE_NBR='+EE.FieldByName('EE_NBR').AsString, []);
            PaStored.First;
            while not PaStored.Eof do
            begin

              CreateTaxAdjCheck(CheckDate);

              DM_PAYROLL.PR_CHECK_LOCALS.Insert;
              DM_PAYROLL.PR_CHECK_LOCALS.PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
              DM_PAYROLL.PR_CHECK_LOCALS.PR_NBR.Value := DM_PAYROLL.PR_CHECK['PR_NBR'];
              DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.Value := PaStored['EE_LOCALS_NBR'];
              DM_PAYROLL.PR_CHECK_LOCALS.EXCLUDE_LOCAL.Value := 'N';
              DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_GROSS_WAGES.Value := PaStored.FieldByName('LOCAL_GROSS_WAGES').AsFloat;
              DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_TAXABLE_WAGE.Value := PaStored.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat;
              DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_TAX.Value := PaStored.FieldByName('LOCAL_TAX').AsFloat;
              if (PaStored.FieldByName('CO_LOCATIONS_NBR').AsString <> '') and (PaStored.FieldByName('CO_LOCATIONS_NBR').AsInteger <> 0) then
                DM_PAYROLL.PR_CHECK_LOCALS['CO_LOCATIONS_NBR'] := PaStored['CO_LOCATIONS_NBR'];
              if (PaStored.FieldByName('NONRES_EE_LOCALS_NBR').AsString <> '') and (PaStored.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger <> 0) then
                DM_PAYROLL.PR_CHECK_LOCALS.NONRES_EE_LOCALS_NBR.Value := PaStored['NONRES_EE_LOCALS_NBR'];
              DM_PAYROLL.PR_CHECK_LOCALS.REPORTABLE.AsString := PaStored['REPORTABLE'];
              DM_PAYROLL.PR_CHECK_LOCALS.Post;

              PaStored.Next;
            end;

            if Net <> 0 then
            begin
              CreateTaxAdjCheck(CheckDate);
              PR_CHECK.Edit;
              PR_CHECK.FieldByName('NET_WAGES').Value := Net;
              PR_CHECK.Post;
            end;

            EE.Next;
          except
            on E: Exception do
            begin
              E.Message := E.Message + ' on EE #' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ''));
              raise;
            end;
          end;

        end;
        if PrintReports and (DM_PAYROLL.PR.RecordCount <> 0) then
        begin
          DM_PAYROLL.PR.Edit;
          DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_CHECKS;
          DM_PAYROLL.PR.Post;
          ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
        end;
        MyTransactionManager.ApplyUpdates;
        Sleep(1000);
      finally
        MyTransactionManager.Free;
        TaxWagesList.Free;
        GrossWagesList.Free;
        TaxList.Free;
        EEList.Free;
      end;
    finally
      try
        UnlockResource(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + IntToStr(CONbr), LockType);
      finally
        UnlockResource(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), LockType);
      end;  
    end;
  finally
    ctx_PayrollCalculation.ProcessFeedback(False);
  end;

  if (AutoProcess and (DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger <> 0))
  or (AutoProcess and Result and (DM_PAYROLL.PR.RecordCount <> 0)) then
  begin
    CheckReturnQueue := True;
    Warnings := ctx_PayrollProcessing.ProcessPayroll(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger, False, Log, CheckReturnQueue, LockType);
  end;

  if not AutoProcess and (DM_PAYROLL.PR.RecordCount <> 0) then
  begin
    DM_PAYROLL.PR.Edit;
    DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING;
    DM_PAYROLL.PR.Post;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
  end;

  DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(CONbr));
end;

end.

