// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQuarterEndCleanup;

interface

uses
  controls, classes,  db, dialogs, windows, EvTypes,  Variants,
  EvBasicUtils, EvContext, EvExceptions, EvClientDataSet, EvPayroll;


function DoQuarterEndCleanup(CONbr: Integer; QTREndDate: TDateTime; MinTax: Real; CleanupAction: TCleanupPayrollAction; ReportFilePath, EECustomNbr: String; ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean; LockType: TResourceLockType; ExtraOptions: Variant; out Warnings: String): Boolean;

implementation

uses
  SysUtils, EvUtils, EvConsts, SDataStructure, SQuarterEndProcessing, SPD_ProcessException, DateUtils,
  ISKbmMemDataSet, EvStreamUtils, EvCommonInterfaces, evDataset, isBaseClasses, isBAsicUtils,
  SQECLocalTaxes, Types, EvDataAccessComponents, evSQLite;

function DoQuarterEndCleanup(CONbr: Integer; QTREndDate: TDateTime; MinTax: Real; CleanupAction: TCleanupPayrollAction; ReportFilePath, EECustomNbr: String; ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean; LockType: TResourceLockType; ExtraOptions: Variant; out Warnings: String): Boolean;
var
  ALL_CHECKS, SORTED_PR, SORTED_PR_CHECK, ALL_CHECKS_FOR_FUI, SORTED_PR_CHECK_STATES,
  SORTED_PR_CHECK_LOCALS, SORTED_PR_CHECK_LINES, SORTED_PR_CHECK_LINES_FOR_FUI, FEDERAL_YTD, STATE_YTD, LOCAL_YTD, LOCAL_YTQ: TevClientDataSet;
  CLPersonTaxes: TevClientDataSet;
  TaxWages, TaxTips, Tax, GrossWages, MinimumTax, Net: Real;
  LogFile: Text;
  TaxWagesList, GrossWagesList, TaxList: TStringList;
  EEList: IisListOfValues;
  I, MyCount, TempNbr, OldNumber, SUIEmployeeCount: Integer;
  aName, aCount: String;
  MyTransactionManager: TTransactionManager;
  CheckDate, TmpCheckDate: TDateTime;
  PaExcludedTaxes: IevDataset;
  LocalLiab: IisParamsCollection;
  Evo: IevHistData;
  Person: IevPerson;
  EmployeeList: IevDataset;
  NJ_Newark_Payroll_Tax_Wages_YTD: Variant;
  
  procedure StoreAmountInList(aList: TStringList; Name: string; Value: Real);
  begin
    if aList.IndexOfName(Name) = -1 then
      aList.Add(Name + '=' + FloatToStr(Value))
    else
      aList.Values[Name] := FloatToStr(StrToFloat(aList.Values[Name]) + Value);
  end;

  function SubtractWithLimit(First, Second: Real): Real;
  begin
    Result := First - Second;
    if Abs(Result) < MinimumTax then
      Result := 0;
  end;

  procedure CalculateEEOASDITaxes(QTREndDate: TDateTime; EENbr: Integer; var TaxWages, TaxTips, GrossWages, Tax: Real);
  var
    OasdiWageLimit: Real;

    TaxWagesBefore: Double;
    TaxTipsBefore: Double;

    TaxPaidCurrentQuarter: Double;
    TaxCalcQuarter: Double;
    CalcTaxWagesCurrentQuarter: Double;
    CalcTaxTipsCurrentQuarter: Double;
    GrossWagesCurrentQuarter: Double;
    TaxWagesCurrentQuarter: Double;
    TaxTipsCurrentQuarter: Double;

    i: Integer;
  begin

    OasdiWageLimit := Evo.OasdiWageLimit(QTREndDate);

    TaxWagesBefore := 0;
    TaxTipsBefore  := 0;

    TaxPaidCurrentQuarter    := 0;
    CalcTaxWagesCurrentQuarter := 0;
    CalcTaxTipsCurrentQuarter  := 0;
    TaxWagesCurrentQuarter := 0;
    TaxTipsCurrentQuarter  := 0;
    GrossWagesCurrentQuarter := 0;

    for i := 0 to Person.CheckCount - 1 do
    if DateBetween(Person.Checks[i].CheckDate, GetBeginYear(QTREndDate), QTREndDate) then
    begin
      if (Person.Checks[i].EeNbr <> EENbr) and Evo.CoCombineOasdi(Person.CoNbr(EENbr), Person.CoNbr(Person.Checks[i].EeNbr), QTREndDate) then
      begin
        TaxWagesBefore := TaxWagesBefore + Person.Checks[i].EeOasdiTaxableWages;
        TaxTipsBefore  := TaxTipsBefore  + Person.Checks[i].EeOasdiTips;
      end
      else if Person.Checks[i].EeNbr = EENbr then
      begin
        CalcTaxWagesCurrentQuarter := CalcTaxWagesCurrentQuarter + Person.Checks[i].CalcEeOasdiWages;
        CalcTaxTipsCurrentQuarter  := CalcTaxTipsCurrentQuarter  + Person.Checks[i].CalcEeOasdiTips;
        TaxPaidCurrentQuarter      := TaxPaidCurrentQuarter      + Person.Checks[i].EeOasdiTax;
        GrossWagesCurrentQuarter   := GrossWagesCurrentQuarter   + Person.Checks[i].EeOasdiGrossWages;
        TaxWagesCurrentQuarter     := TaxWagesCurrentQuarter     + Person.Checks[i].EeOasdiTaxableWages;
        TaxTipsCurrentQuarter      := TaxTipsCurrentQuarter      + Person.Checks[i].EeOasdiTips;
      end;
    end;
    GrossWagesCurrentQuarter := CalcTaxWagesCurrentQuarter;

    if TaxWagesBefore + TaxTipsBefore > OasdiWageLimit then
    begin
      CalcTaxWagesCurrentQuarter := 0;
      CalcTaxTipsCurrentQuarter  := 0;
    end
    else
    if TaxWagesBefore + TaxTipsBefore + CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter > OasdiWageLimit then
    begin
      for i := Person.CheckCount - 1 downto 0 do
      if DateBetween(Person.Checks[i].CheckDate, GetBeginYear(QTREndDate), QTREndDate) and (Person.Checks[i].EeNbr = EENbr) then
      begin
        CalcTaxTipsCurrentQuarter := CalcTaxTipsCurrentQuarter - Person.Checks[i].CalcEeOasdiTips;
        if TaxWagesBefore + TaxTipsBefore + CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter <= OasdiWageLimit then
        begin
          CalcTaxTipsCurrentQuarter := OasdiWageLimit - TaxWagesBefore - TaxTipsBefore - CalcTaxWagesCurrentQuarter;
          break;
        end;
        CalcTaxWagesCurrentQuarter := CalcTaxWagesCurrentQuarter - Person.Checks[i].CalcEeOasdiWages;
        if TaxWagesBefore + TaxTipsBefore + CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter <= OasdiWageLimit then
        begin
          CalcTaxWagesCurrentQuarter := OasdiWageLimit - TaxWagesBefore - TaxTipsBefore - CalcTaxTipsCurrentQuarter;
          break;
        end;
      end;
    end;

    TaxCalcQuarter := RoundTwo((CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter) * Evo.EeOasdiRate(QTREndDate));

    GrossWages    := SubtractWithLimit(GrossWagesCurrentQuarter, GrossWagesCurrentQuarter);
    TaxWages      := SubtractWithLimit(CalcTaxWagesCurrentQuarter, TaxWagesCurrentQuarter);
    TaxTips       := SubtractWithLimit(CalcTaxTipsCurrentQuarter, TaxTipsCurrentQuarter);
    Tax           := SubtractWithLimit(TaxCalcQuarter, TaxPaidCurrentQuarter);

  end;

  procedure CalculateEROASDITaxes(QTREndDate: TDateTime; EENbr: Integer; var TaxWages, TaxTips, GrossWages, Tax: Real);
  var
    OasdiWageLimit: Real;

    TaxWagesBefore: Double;
    TaxTipsBefore: Double;

    TaxPaidCurrentQuarter: Double;
    TaxCalcQuarter: Double;
    CalcTaxWagesCurrentQuarter: Double;
    CalcTaxTipsCurrentQuarter: Double;
    GrossWagesCurrentQuarter: Double;
    TaxWagesCurrentQuarter: Double;
    TaxTipsCurrentQuarter: Double;

    i: Integer;
  begin

    OasdiWageLimit := Evo.OasdiWageLimit(QTREndDate);

    TaxWagesBefore := 0;
    TaxTipsBefore  := 0;

    TaxPaidCurrentQuarter    := 0;
    CalcTaxWagesCurrentQuarter := 0;
    CalcTaxTipsCurrentQuarter  := 0;
    TaxWagesCurrentQuarter := 0;
    TaxTipsCurrentQuarter  := 0;
    GrossWagesCurrentQuarter := 0;

    for i := 0 to Person.CheckCount - 1 do
    if DateBetween(Person.Checks[i].CheckDate, GetBeginYear(QTREndDate), QTREndDate) then
    begin
      if (Person.Checks[i].EeNbr <> EENbr) and Evo.CoCombineOasdi(Person.CoNbr(EENbr), Person.CoNbr(Person.Checks[i].EeNbr), QTREndDate) then
      begin
        TaxWagesBefore := TaxWagesBefore + Person.Checks[i].ErOasdiTaxableWages;
        TaxTipsBefore  := TaxTipsBefore  + Person.Checks[i].ErOasdiTips;
      end
      else if Person.Checks[i].EeNbr = EENbr then
      begin
        CalcTaxWagesCurrentQuarter := CalcTaxWagesCurrentQuarter + Person.Checks[i].CalcErOasdiWages;
        CalcTaxTipsCurrentQuarter  := CalcTaxTipsCurrentQuarter  + Person.Checks[i].CalcErOasdiTips;
        TaxPaidCurrentQuarter      := TaxPaidCurrentQuarter      + Person.Checks[i].ErOasdiTax;
        GrossWagesCurrentQuarter   := GrossWagesCurrentQuarter   + Person.Checks[i].ErOasdiGrossWages;
        TaxWagesCurrentQuarter     := TaxWagesCurrentQuarter     + Person.Checks[i].ErOasdiTaxableWages;
        TaxTipsCurrentQuarter      := TaxTipsCurrentQuarter      + Person.Checks[i].ErOasdiTips;
      end;
    end;
    GrossWagesCurrentQuarter := CalcTaxWagesCurrentQuarter;

    if TaxWagesBefore + TaxTipsBefore > OasdiWageLimit then
    begin
      CalcTaxWagesCurrentQuarter := 0;
      CalcTaxTipsCurrentQuarter  := 0;
    end
    else
    if TaxWagesBefore + TaxTipsBefore + CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter > OasdiWageLimit then
    begin
      for i := Person.CheckCount - 1 downto 0 do
      if DateBetween(Person.Checks[i].CheckDate, GetBeginYear(QTREndDate), QTREndDate) and (Person.Checks[i].EeNbr = EENbr) then
      begin
        CalcTaxTipsCurrentQuarter := CalcTaxTipsCurrentQuarter - Person.Checks[i].CalcErOasdiTips;
        if TaxWagesBefore + TaxTipsBefore + CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter <= OasdiWageLimit then
        begin
          CalcTaxTipsCurrentQuarter := OasdiWageLimit - TaxWagesBefore - TaxTipsBefore - CalcTaxWagesCurrentQuarter;
          break;
        end;
        CalcTaxWagesCurrentQuarter := CalcTaxWagesCurrentQuarter - Person.Checks[i].CalcErOasdiWages;
        if TaxWagesBefore + TaxTipsBefore + CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter <= OasdiWageLimit then
        begin
          CalcTaxWagesCurrentQuarter := OasdiWageLimit - TaxWagesBefore - TaxTipsBefore - CalcTaxTipsCurrentQuarter;
          break;
        end;
      end;
    end;

    TaxCalcQuarter := RoundTwo((CalcTaxWagesCurrentQuarter + CalcTaxTipsCurrentQuarter) * Evo.ErOasdiRate(QTREndDate));

    GrossWages    := SubtractWithLimit(GrossWagesCurrentQuarter, GrossWagesCurrentQuarter);
    TaxWages      := SubtractWithLimit(CalcTaxWagesCurrentQuarter, TaxWagesCurrentQuarter);
    TaxTips       := SubtractWithLimit(CalcTaxTipsCurrentQuarter, TaxTipsCurrentQuarter);
    Tax           := SubtractWithLimit(TaxCalcQuarter, TaxPaidCurrentQuarter);

  end;

  procedure CalculateERSDITaxes(QTREndDate: TDateTime; EENbr: Integer; TaxWagesList, GrossWagesList, TaxList: TStringList; ALL_CHECKS, PR, PR_CHECK,
    PR_CHECK_STATES, PR_CHECK_LINES, PR_CHECK_REAL, PR_CHECK_STATES_REAL: TevClientDataSet);
  var
    EDCodeType, aState: String;
    CheckDate, FollowingCheckDate: TDateTime;
    YTDTax, YTDTaxWages, YTDGrossWages, OldYTDTax, OldYTDTaxWages, TaxWages, GrossWages, Tax, SDITaxLimit: Real;
    SYStateNumber, EEStateNumber, TaxFreq, COStateNumber, I: Integer;
    Q: IevQuery;
  begin
    TaxWagesList.Clear;
    GrossWagesList.Clear;
    TaxList.Clear;
//    with ALL_CHECKS. do
    begin
      ALL_CHECKS.First;
      PR_CHECK.First;
      PR_CHECK_STATES.First;
      while not ALL_CHECKS.Eof do
      begin
        if ALL_CHECKS.FieldByName('EE_NBR').AsInteger = EENbr then
        begin
          CheckDate := ALL_CHECKS.FieldByName('CHECK_DATE').AsDateTime;

          PR_CHECK.FindKey([ALL_CHECKS['CHECK_DATE'], ALL_CHECKS['RUN_NUMBER'], ALL_CHECKS['EE_NBR'], ALL_CHECKS['PAYMENT_SERIAL_NUMBER']]);
          while (PR_CHECK['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK.Eof do
            PR_CHECK.Next;
          PR_CHECK_STATES.FindKey([ALL_CHECKS['CHECK_DATE'], ALL_CHECKS['RUN_NUMBER'], ALL_CHECKS['EE_NBR'], ALL_CHECKS['PAYMENT_SERIAL_NUMBER']]);
          while (PR_CHECK_STATES['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_STATES.Eof do
            PR_CHECK_STATES.Next;
          while (PR_CHECK_STATES['PR_CHECK_NBR'] = ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_STATES.Eof do
          begin
            EEStateNumber := PR_CHECK_STATES['EE_STATES_NBR'];
            try
              COStateNumber := Evo.GetEeCoStatesNbr(EEStateNumber, CheckDate);
            except
              raise EInconsistentData.CreateHelp('EE State #' + IntToStr(EEStateNumber) + ' does not exist as of ' + DateToStr(CheckDate), IDH_InconsistentData);
            end;

            aState := Evo.CoStateState(COStateNumber, CheckDate);
            if (aState = 'NY') or (aState = 'HI') then {SDIs for NY and HI are done as a deduction}
            begin
              PR_CHECK_STATES.Next;
              Continue;
            end;

            SYStateNumber := Evo.GetSyStateNbr(aState, CheckDate);

            if TaxWagesList.IndexOfName(aState) = -1 then
            begin
              ctx_PayrollCalculation.GetLimitedTaxAndDedYTD(ConvertNull(PR_CHECK_REAL['PR_NBR'], 0), 0, EENbr, EEStateNumber, ltERSDI, GetBeginQuarter(QTREndDate), QTREndDate, PR, PR_CHECK_REAL, PR_CHECK_STATES_REAL, nil,
                nil, OldYTDTaxWages, OldYTDTax, False, True);
              StoreAmountInList(TaxWagesList, aState, OldYTDTaxWages);
              StoreAmountInList(GrossWagesList, aState, 0);
              StoreAmountInList(TaxList, aState, OldYTDTax);
              ctx_PayrollCalculation.GetLimitedTaxAndDedYTD(0, 0, EENbr, EEStateNumber, ltERSDI, GetBeginYear(QTREndDate), GetBeginQuarter(QTREndDate) - 1, PR, PR_CHECK_REAL, PR_CHECK_STATES_REAL, nil,
                nil, OldYTDTaxWages, OldYTDTax);
              StoreAmountInList(TaxWagesList, aState, OldYTDTaxWages);
              StoreAmountInList(GrossWagesList, aState, 0);
              StoreAmountInList(TaxList, aState, OldYTDTax);
            end;

            if (Evo.SyStatesErSdiRate(SYStateNumber, CheckDate) = 0)
            or Evo.IsCoStateErSdiExempt(COStateNumber, CheckDate)
            or (Evo.GetEeStatesErSdiExemptExclude(EEStateNumber, CheckDate) = 'E') then
            begin
              PR_CHECK_STATES.Next;
              Continue;
            end;

            if aState = 'NM' then
            begin
              if Evo.GetEeStatesErSdiExemptExclude(EEStateNumber, CheckDate) = 'X' then
              begin
                PR_CHECK_STATES.Next;
                Continue;
              end;
              FollowingCheckDate := ctx_PayrollCalculation.GetNextCheckDate(ALL_CHECKS['CHECK_DATE'], DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString[1]);
              if GetQuarterNumber(ALL_CHECKS['CHECK_DATE']) <> GetQuarterNumber(FollowingCheckDate) then
              begin
                Tax := Evo.SyStatesErSdiRate(SYStateNumber, CheckDate);
                if (Evo.EeStateEeSdiExemptExclude(EEStateNumber, CheckDate) <> GROUP_BOX_INCLUDE)
                or Evo.IsCoStateEeSdiExempt(COStateNumber, CheckDate) then
                  Tax := Tax + Evo.SyStatesEeSdiRate(SYStateNumber, CheckDate);
                StoreAmountInList(TaxList, aState, Tax);
              end;
              PR_CHECK_STATES.Next;
              Continue;
            end;

            TaxFreq := GetTaxFreq(PR_CHECK['TAX_FREQUENCY']);
            TaxWages := 0;
            SDITaxLimit := Evo.SyStatesWeeklySdiTaxCap(SYStateNumber, CheckDate) * 52 / TaxFreq;

            PR_CHECK_LINES.First;
            PR_CHECK_LINES.FindKey([ALL_CHECKS['CHECK_DATE'], ALL_CHECKS['RUN_NUMBER'], ALL_CHECKS['EE_NBR'], ALL_CHECKS['PAYMENT_SERIAL_NUMBER']]);
            while (PR_CHECK_LINES['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_LINES.Eof do
              PR_CHECK_LINES.Next;
            while (PR_CHECK_LINES['PR_CHECK_NBR'] = ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_LINES.Eof do
            begin
              if (Evo.GetEeStatesNbr(EENbr, Evo.GetEeStateSdiApplyCoStateNbr(PR_CHECK_LINES['EE_STATES_NBR'], CheckDate),CheckDate) = EEStateNumber) or
              (PR_CHECK_LINES.FieldByName('EE_STATES_NBR').IsNull  and (Evo.GetEeStatesNbr(EENbr, Evo.GetEeStateSdiApplyCoStateNbr(Evo.EeHomeEeStateNbr(PR_CHECK['EE_NBR'], CheckDate), CheckDate),CheckDate) = EEStateNumber)) then
              begin
                EDCodeType := Evo.ClEdCodeType(PR_CHECK_LINES['CL_E_DS_NBR'], CheckDate);
                if TypeIsTaxable(EDCodeType)
                and not Evo.IsEdCodeStateExemptEmployerSdi(SYStateNumber, EDCodeType, CheckDate)
                and not Evo.IsEdCodeClErSdiExempt(PR_CHECK_LINES['CL_E_DS_NBR'], aState, CheckDate) then
                begin
                  TaxWages := TaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);
                end;
              end;
              PR_CHECK_LINES.Next;
            end;
            repeat
              PR_CHECK_LINES.Prior;
            until (PR_CHECK_LINES['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) or PR_CHECK_LINES.Bof;
            GrossWages := TaxWages;
            if GrossWagesList.IndexOfName(aState) = -1 then
              YTDGrossWages := 0
            else
              YTDGrossWages := StrToFloat(GrossWagesList.Values[aState]);
            if TaxWagesList.IndexOfName(aState) = -1 then
              YTDTaxWages := 0
            else
              YTDTaxWages := StrToFloat(TaxWagesList.Values[aState]);
            if YTDGrossWages > YTDTaxWages then
            begin
              if TaxWages > YTDTaxWages - YTDGrossWages then
                TaxWages := 0
              else
                TaxWages := TaxWages + YTDGrossWages - YTDTaxWages;
            end;

            if Evo.GetEeStatesErSdiExemptExclude(EEStateNumber, CheckDate) = 'X' then
            begin
              StoreAmountInList(TaxWagesList, aState, TaxWages);
              StoreAmountInList(GrossWagesList, aState, GrossWages);
              PR_CHECK_STATES.Next;
              Continue;
            end;

            if TaxWages * Evo.SyStatesErSdiRate(SYStateNumber, CheckDate) > SDITaxLimit then
            begin
              Tax := SDITaxLimit;
              TaxWages := SDITaxLimit / Evo.SyStatesErSdiRate(SYStateNumber, CheckDate);
            end
            else
              Tax := TaxWages * Evo.SyStatesErSdiRate(SYStateNumber, CheckDate);

            if (TaxWages + YTDTaxWages > Evo.SyStatesErSdiMaximumWage(SYStateNumber, CheckDate)) and
              (Evo.SyStatesErSdiMaximumWage(SYStateNumber, CheckDate) <> 0) then
            begin
              TaxWages := Evo.SyStatesErSdiMaximumWage(SYStateNumber, CheckDate) - YTDTaxWages;
              Tax := TaxWages * Evo.SyStatesErSdiRate(SYStateNumber, CheckDate);
            end;
            StoreAmountInList(TaxWagesList, aState, TaxWages);
            StoreAmountInList(GrossWagesList, aState, GrossWages);
            StoreAmountInList(TaxList, aState, Tax);
            PR_CHECK_STATES.Next;
          end;
        end;
        ALL_CHECKS.Next;
      end;
    end;

    CheckDate := Now;
    for I := 0 to TaxWagesList.Count - 1 do
    begin
      aState := TaxWagesList.Names[I];
      COStateNumber := Evo.GetCoStatesNbr(DM_COMPANY.CO.CO_NBR.AsInteger, aState, CheckDate);
      EEStateNumber := Evo.GetEeStatesNbr(EENbr, COStateNumber, CheckDate);
      ctx_PayrollCalculation.GetLimitedTaxAndDedYTD(ConvertNull(PR_CHECK_REAL['PR_NBR'], 0), 0, EENbr, EEStateNumber, ltERSDI, GetBeginYear(QTREndDate), QTREndDate, PR, PR_CHECK_REAL, PR_CHECK_STATES_REAL, nil,
        nil, OldYTDTaxWages, OldYTDTax);
      YTDTaxWages := StrToFloat(TaxWagesList.Values[aState]);
      TaxWagesList.Values[aState] := FloatToStr(SubtractWithLimit(YTDTaxWages, OldYTDTaxWages));
      if TaxList.IndexOfName(aState) <> -1 then
      begin
        YTDTax := StrToFloat(TaxList.Values[aState]);
        TaxList.Values[aState] := FloatToStr(SubtractWithLimit(YTDTax, OldYTDTax));
      end;

      if GrossWagesList.IndexOfName(aState) <> -1 then
      begin
        GrossWages := StrToFloat(GrossWagesList.Values[aState]);
        Q := TevQuery.Create('StateTaxes');
        Q.Macro['Columns'] := 'sum(S.ER_SDI_GROSS_WAGES) GROSS_WAGES';
        Q.Param['EENbr']      := EENbr;
        Q.Param['EEStateNbr'] := EEStateNumber;
        Q.Param['PrNbr']      := 0;
        Q.Param['StartDate']  := GetBeginQuarter(QTREndDate);
        Q.Param['EndDate']    := QTREndDate;
        GrossWagesList.Values[aState] := FloatToStr(SubtractWithLimit(GrossWages, ConvertNull(Q.Result['GROSS_WAGES'], 0)));
      end;
    end;
  end;

  procedure CalculateLocalTaxes(QTREndDate: TDateTime; EENbr: Integer; TaxWagesList, TaxList: TStringList; ALL_CHECKS, LOCAL_YTD, LOCAL_YTQ, PR, PR_CHECK,
    PR_CHECK_LOCALS, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS: TevClientDataSet);
  var
    EDCodeType, aLocal, CalcMethod, s: string;
    CheckDate: TDateTime;
    YTDTaxWages, YTDTax, TaxWages, TaxWagesPa, TaxWagesAdjusted, Tax, TaxRate, MiscAmount: Real;
    SYLocalNumber, EELocalNumber, EEStateNumber, COLocalNumber, I, OldMonth: Integer;
    SYExempt: Boolean;
    MonthlyWagesList: TStringList;
    YTDEeLocalTaxableWages: Real;
    YTQEeLocalTaxableWages: Real;
    ExemptExclude: Variant;
    OverrideLocalTaxType: Variant;
    Index, Nbr: Integer;
    Q: IevQuery;

  procedure StoreWageInMonthlyList;
  begin
    if MonthOf(CheckDate) <> OldMonth then
      MonthlyWagesList.Clear;
    OldMonth := MonthOf(CheckDate);
    StoreAmountInList(MonthlyWagesList, IntToStr(SYLocalNumber), TaxWages);
  end;

  begin

    TaxWagesList.Clear;
    TaxList.Clear;
    MonthlyWagesList := TStringList.Create;
    OldMonth := 0;
    try
      ALL_CHECKS.First;
      PR_CHECK_LOCALS.First;
      while not ALL_CHECKS.EOF do
      begin
        if ALL_CHECKS.FieldByName('EE_NBR').AsInteger = EENbr then
        begin
          CheckDate := ALL_CHECKS.FieldByName('CHECK_DATE').AsDateTime;
          PR_CHECK_LOCALS.FindKey([ALL_CHECKS['CHECK_DATE'], ALL_CHECKS['RUN_NUMBER'], ALL_CHECKS['EE_NBR'], ALL_CHECKS['PAYMENT_SERIAL_NUMBER']]);
          while (PR_CHECK_LOCALS['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_LOCALS.Eof do
            PR_CHECK_LOCALS.Next;
          while (PR_CHECK_LOCALS['PR_CHECK_NBR'] = ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_LOCALS.EOF do
          begin
            EELocalNumber := PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger;
            COLocalNumber := Evo.GetEeLocalCoLocalTaxNbr(EELocalNumber, CheckDate);
            SYLocalNumber := Evo.CoSyLocalsNbr(COLocalNumber, CheckDate);

            Tax := 0;
            TaxWages := 0;
            TaxWagesPa := 0;

            CalcMethod := Evo.SyLocalCalcMethod(SYLocalNumber, CheckDate);

            if TaxWagesList.IndexOfName(IntToStr(SYLocalNumber)) = -1 then
            begin
              StoreAmountInList(TaxWagesList, IntToStr(SYLocalNumber), ConvertNull(LOCAL_YTQ.Lookup('EE_NBR;SY_LOCALS_NBR',
                VarArrayOf([EENbr, SYLocalNumber]), 'local_taxable_wage'), 0));
              if CalcMethod[1] in [CALC_METHOD_GROSS, CALC_METHOD_GROSS_ADJ] then
                StoreAmountInList(TaxList, IntToStr(SYLocalNumber), ConvertNull(LOCAL_YTQ.Lookup('EE_NBR;SY_LOCALS_NBR',
                  VarArrayOf([EENbr, SYLocalNumber]), 'local_tax'), 0));
            end;

            if Evo.IsCoLocalExempt(COLocalNumber, CheckDate)
            or Evo.IsEeLocalExempt(EELocalNumber, CheckDate) then
            begin
              PR_CHECK_LOCALS.Next;
              Continue;
            end;

            PR_CHECK_LINES.First;
            PR_CHECK_LINES.FindKey([ALL_CHECKS['CHECK_DATE'], ALL_CHECKS['RUN_NUMBER'], ALL_CHECKS['EE_NBR'], ALL_CHECKS['PAYMENT_SERIAL_NUMBER']]);
            while (PR_CHECK_LINES['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_LINES.Eof do
              PR_CHECK_LINES.Next;
            while (PR_CHECK_LINES['PR_CHECK_NBR'] = ALL_CHECKS['PR_CHECK_NBR']) and not PR_CHECK_LINES.Eof do
            begin
              EDCodeType := Evo.ClEdCodeType(PR_CHECK_LINES['CL_E_DS_NBR'], CheckDate);
              if TypeIsTaxable(EDCodeType) and not (TypeIsSpecTaxedDed(EDCodeType) and not Evo.EeLocalIncludeInPretax(EELocalNumber, CheckDate)) then
              begin

                if Evo.SyLocalFollowStateEdExempt(SYLocalNumber, CheckDate) then
                  SYExempt := Evo.IsEdCodeStateExempt(Evo.SyLocalState(SYLocalNumber), EDCodeType, CheckDate)
                else if Evo.IsClEdLocalInclude(PR_CHECK_LINES['CL_E_DS_NBR'], SYLocalNumber, CheckDate) then
                  SYExempt := False
                else
                  SYExempt := Evo.IsEdCodeSyLocalExempt(EDCodeType, SYLocalNumber, CheckDate);

                s := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsString+'_'+IntToStr(EELocalNumber);

                if not SYExempt then
                if not Evo.IsClEdLocalExempt(PR_CHECK_LINES['CL_E_DS_NBR'], SYLocalNumber, CheckDate) then
                begin
                  if (Evo.EeLocalDeduct(EELocalNumber, CheckDate) = DEDUCT_NEVER)
                  and not PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber]), [])
                  and not      PaExcludedTaxes.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber]), []) then
                  begin
                    Nbr := 0;
                    if not PR_CHECK_LINES.FieldByName('CO_JOBS_NBR').IsNull then
                      if Evo.JobHasLocalTax(PR_CHECK_LINES['CO_JOBS_NBR'], COLocalNumber, CheckDate) then
                        Nbr := PR_CHECK_LINES['CO_JOBS_NBR'];

                    if (Nbr = 0) and not PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').IsNull then
                      if Evo.TeamHasLocalTax(PR_CHECK_LINES['CO_TEAM_NBR'], COLocalNumber, CheckDate) then
                        Nbr := PR_CHECK_LINES['CO_TEAM_NBR'];

                    if (Nbr = 0) and not PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').IsNull then
                      if Evo.DepartmentHasLocalTax(PR_CHECK_LINES['CO_DEPARTMENT_NBR'], COLocalNumber, CheckDate) then
                        Nbr := PR_CHECK_LINES['CO_DEPARTMENT_NBR'];

                    if (Nbr = 0) and not PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').IsNull then
                      if Evo.BranchHasLocalTax(PR_CHECK_LINES['CO_BRANCH_NBR'], COLocalNumber, CheckDate) then
                        Nbr := PR_CHECK_LINES['CO_BRANCH_NBR'];

                    if (Nbr = 0) and not PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').IsNull then
                    begin
                      if not Evo.DivisionHasLocalTax(PR_CHECK_LINES['CO_DIVISION_NBR'], COLocalNumber, CheckDate) then
                      begin
                        PR_CHECK_LINES.Next;
                        Continue;
                      end;
                    end
                    else
                    if (Nbr = 0) and not PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber]), [])
                    and not PaExcludedTaxes.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber]), []) then
                    begin
                      PR_CHECK_LINES.Next;
                      Continue;
                    end;
                  end
                  else
                  if (Evo.EeLocalDeduct(EELocalNumber, CheckDate) = DEDUCT_NO_OVERRIDES)
                  and PR_CHECK_LINE_LOCALS.Locate('PR_CHECK_LINES_NBR', PR_CHECK_LINES['PR_CHECK_LINES_NBR'], []) then
                  begin
                    PR_CHECK_LINES.Next;
                    Continue;
                  end;

                  if PaExcludedTaxes.Locate('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber]), []) then
                    ExemptExclude := PaExcludedTaxes['EXEMPT_EXCLUDE']
                  else
                    ExemptExclude := PR_CHECK_LINE_LOCALS.Lookup('PR_CHECK_LINES_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK_LINES['PR_CHECK_LINES_NBR'], EELocalNumber]), 'EXEMPT_EXCLUDE');

                  if ExemptExclude = GROUP_BOX_PABLOCK then
                    TaxWagesPa := TaxWagesPa + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);

                  if ExemptExclude <> GROUP_BOX_EXEMPT then
                    TaxWages := TaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES['AMOUNT']);

                end;
              end;
              PR_CHECK_LINES.Next;
            end;
            repeat
              PR_CHECK_LINES.Prior;
            until (PR_CHECK_LINES['PR_CHECK_NBR'] <> ALL_CHECKS['PR_CHECK_NBR']) or PR_CHECK_LINES.BOF;

//            if TaxWages < 0 then
//              TaxWages := 0;

            if TaxWagesPa < 0 then
              TaxWagesPa := 0;

            if Evo.EeLocalPercentageOfTaxWages(EELocalNumber, CheckDate) <> 0 then
              TaxWages := TaxWages * Evo.EeLocalPercentageOfTaxWages(EELocalNumber, CheckDate) * 0.01;

            if Evo.EeLocalExemptExclude(EELocalNumber, CheckDate) = 'X' then
            begin
              StoreAmountInList(TaxWagesList, IntToStr(SYLocalNumber), TaxWages);
              StoreWageInMonthlyList;
              PR_CHECK_LOCALS.Next;
              Continue;
            end;

            YTDTaxWages := 0;

            if (Evo.SyLocalType(SYLocalNumber, CheckDate) = LOCAL_TYPE_OCCUPATIONAL)
            and (Evo.SyLocalState(SYLocalNumber, CheckDate) = 'PA') then
            begin
              // PA OPT LST logic
              StoreAmountInList(TaxWagesList, IntToStr(SYLocalNumber), TaxWages);
              StoreWageInMonthlyList;
            end
            else
            begin
              if Evo.SyLocalMonthlyMaximum(SYLocalNumber, CheckDate) then
              begin
                if MonthlyWagesList.IndexOfName(IntToStr(SYLocalNumber)) = -1 then
                  YTDTaxWages := 0
                else
                  YTDTaxWages := StrToFloat(MonthlyWagesList.Values[IntToStr(SYLocalNumber)]);
              end
              else
                YTDTaxWages := StrToFloat(TaxWagesList.Values[IntToStr(SYLocalNumber)]);
              if (TaxWages + YTDTaxWages > Evo.SyLocalWageMaximum(SYLocalNumber, CheckDate))
              and (Evo.SyLocalWageMaximum(SYLocalNumber, CheckDate) <> 0) then
                TaxWages := Evo.SyLocalWageMaximum(SYLocalNumber, CheckDate) - YTDTaxWages;
            end;

            if TaxWagesPa <> 0 then
            begin
              TaxWagesAdjusted := TaxWages - TaxWagesPa;
            end
            else
              TaxWagesAdjusted := TaxWages;

            if TaxWagesAdjusted < 0 then
              TaxWagesAdjusted := 0;

            if Evo.SyLocalWageMinimum(SYLocalNumber, CheckDate) > 0 then
            begin
              if TaxWages + YTDTaxWages > Evo.SyLocalWageMinimum(SYLocalNumber, CheckDate) then
              begin
                if YTDTaxWages < Evo.SyLocalWageMinimum(SYLocalNumber, CheckDate) then
                  TaxWagesAdjusted := TaxWages + YTDTaxWages - Evo.SyLocalWageMinimum(SYLocalNumber, CheckDate);
              end
              else
                TaxWagesAdjusted := 0;
            end;

            if CalcMethod[1] in [CALC_METHOD_GROSS, CALC_METHOD_GROSS_ADJ] then
            begin

              OverrideLocalTaxType := Evo.EeLocalOverrideLocalTaxType(EELocalNumber, CheckDate);

              if OverrideLocalTaxType = OVERRIDE_VALUE_TYPE_REGULAR_PERCENT then
                TaxRate := Evo.EeLocalOverrideLocalTaxValue(EELocalNumber, CheckDate)
              else
              begin
                if not VarIsNull(Evo.CoLocalTaxRate(COLocalNumber, CheckDate)) then
                  TaxRate := Evo.CoLocalTaxRate(COLocalNumber, CheckDate)
                else
                  TaxRate := Evo.SyLocalRate(SYLocalNumber, CheckDate);
              end;

              if CalcMethod = CALC_METHOD_GROSS then
              begin
                Tax := TaxRate * TaxWagesAdjusted;
              end;

              if Evo.SyLocalHasTaxMinimum(SYLocalNumber, CheckDate) then
              begin
                if TaxList.IndexOfName(IntToStr(SYLocalNumber)) <> -1 then
                  YTDTax := StrToFloat(TaxList.Values[IntToStr(SYLocalNumber)])
                else
                  YTDTax := 0;
                if (Tax + YTDTax) > Evo.SyLocalTaxMinimum(SYLocalNumber, CheckDate) then
                  Tax := Evo.SyLocalTaxMinimum(SYLocalNumber, CheckDate) - YTDTax;
              end;

              if CalcMethod = CALC_METHOD_GROSS_ADJ then
              begin
                MiscAmount := Evo.EeLocalMiscellaneousAmount(EELocalNumber, CheckDate);
                if MiscAmount = 0 then
                  MiscAmount := Evo.SyLocalMiscAmount(SYLocalNumber, CheckDate);

                EEStateNumber := Evo.GetEeStatesNbr(EENbr, Evo.SyLocalState(SYLocalNumber, CheckDate), CheckDate);

                Tax := TaxRate * (TaxWagesAdjusted - MiscAmount * Evo.EeStateNumberWithholdingAllow(EEStateNumber, CheckDate));
              end;
              if Evo.EeLocalOverrideLocalTaxType(EELocalNumber, CheckDate) = OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT then
                Tax := Tax + TaxWagesAdjusted * Evo.EeLocalOverrideLocalTaxValue(EELocalNumber, CheckDate);

              StoreAmountInList(TaxWagesList, IntToStr(SYLocalNumber), TaxWages);
              StoreWageInMonthlyList;
              StoreAmountInList(TaxList, IntToStr(SYLocalNumber), Tax);

            end
            else
            if (CalcMethod[1] <> CALC_METHOD_FIXED) or (Evo.SyLocalMinimumWage(SYLocalNumber, CheckDate) <> 0) then
            begin
              StoreAmountInList(TaxWagesList, IntToStr(SYLocalNumber), TaxWages);
              StoreWageInMonthlyList;
            end;

            PR_CHECK_LOCALS.Next;
          end;
        end;
        ALL_CHECKS.Next;
      end;
    finally
      MonthlyWagesList.Free;
    end;

    for I := 0 to TaxWagesList.Count - 1 do
    begin
      aLocal := TaxWagesList.Names[I];
      SYLocalNumber := StrToInt(aLocal);
      if (Evo.SyLocalType(SYLocalNumber, QTREndDate) = LOCAL_TYPE_OCCUPATIONAL) and (Evo.SyLocalState(SYLocalNumber, QTREndDate) = 'PA') then
      begin
        YTDTaxWages := StrToFloat(TaxWagesList.Values[aLocal]);
        YTDEeLocalTaxableWages := YTDTaxWages;
        YTQEeLocalTaxableWages := 0;
      end
      else
      begin
        YTDTaxWages := StrToFloat(TaxWagesList.Values[aLocal]);
        YTDEeLocalTaxableWages := ConvertNull(LOCAL_YTD.Lookup('EE_NBR;SY_LOCALS_NBR', VarArrayOf([EENbr, SYLocalNumber]), 'LOCAL_TAXABLE_WAGE'), 0);
        YTQEeLocalTaxableWages := ConvertNull(LOCAL_YTQ.Lookup('EE_NBR;SY_LOCALS_NBR', VarArrayOf([EENbr, SYLocalNumber]), 'LOCAL_TAXABLE_WAGE'), 0);
      end;
      TaxWagesList.Values[aLocal] := FloatToStr(SubtractWithLimit(YTDTaxWages, YTDEeLocalTaxableWages + YTQEeLocalTaxableWages));
      if TaxList.IndexOfName(aLocal) <> -1 then
      begin

        if SyLocalNumber = NJ_NEWARK_PAYROLL_TAX then
        begin
          if VarIsNull(NJ_Newark_Payroll_Tax_Wages_YTD) then
          begin
            Q := TevQuery.Create('select sum(cl.LOCAL_TAXABLE_WAGE) LOCAL_TAXABLE_WAGE ' +
              'from PR p '+
              'join PR_CHECK c on p.PR_NBR = c.PR_NBR '+
              'join PR_CHECK_LOCALS cl on c.PR_CHECK_NBR = cl.PR_CHECK_NBR '+
              'join EE_LOCALS el on cl.EE_LOCALS_NBR = el.EE_LOCALS_NBR '+
              'join CO_LOCAL_TAX col on el.CO_LOCAL_TAX_NBR = col.CO_LOCAL_TAX_NBR ' +
              'where col.CO_NBR = :CoNbr and col.SY_LOCALS_NBR=' + IntToStr(NJ_NEWARK_PAYROLL_TAX) + ' and '+
              '{AsOfNow<p>} and ' +
              '{AsOfNow<c>} and ' +
              '{AsOfNow<cl>} and ' +
              '{AsOfNow<el>} and ' +
              '{AsOfNow<col>} ' +
              'and c.CHECK_STATUS<>''V'' ' +
              'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
              'and (p.run_number>=:StartRN AND p.check_date=:StartDate OR p.check_date>:StartDate) AND ' +
              '(p.run_number<=:EndRN AND p.check_date=:EndDate OR p.check_date<:EndDate) and ' +
              'p.{PayrollProcessedClause} ');
            Q.Param['StartDate'] := GetBeginQuarter(QTREndDate);
            Q.Param['EndDate'] := QTREndDate;
            Q.Param['StartRN'] := 1;
            Q.Param['EndRN'] := 999;
            Q.Param['CoNbr'] := CoNbr;
            Q.Param['Status'] := PAYROLL_STATUS_DELETED;
            NJ_Newark_Payroll_Tax_Wages_YTD := ConvertNull(Q.Result['LOCAL_TAXABLE_WAGE'], 0);
          end;
        end;

        if (SyLocalNumber <> NJ_NEWARK_PAYROLL_TAX)
        and (Evo.SyLocalQuarterlyMinimumThreshold(SYLocalNumber, QTREndDate) <> 0)
        and (YTDTaxWages < Evo.SyLocalQuarterlyMinimumThreshold(SYLocalNumber, QTREndDate))
        and (YTDTaxWages > 0) then
          YTDTax := 0
        else
        if (SyLocalNumber = NJ_NEWARK_PAYROLL_TAX)
        and (Evo.SyLocalQuarterlyMinimumThreshold(SYLocalNumber, QTREndDate) <> 0)
        and (NJ_Newark_Payroll_Tax_Wages_YTD < Evo.SyLocalQuarterlyMinimumThreshold(SYLocalNumber, QTREndDate)) then
          YTDTax := 0
        else
          YTDTax := StrToFloat(TaxList.Values[aLocal]);

        TaxList.Values[aLocal] := FloatToStr(SubtractWithLimit(YTDTax, ConvertNull(LOCAL_YTD.Lookup('EE_NBR;SY_LOCALS_NBR',
          VarArrayOf([EENbr, SYLocalNumber]), 'local_tax'), 0) + ConvertNull(LOCAL_YTQ.Lookup('EE_NBR;SY_LOCALS_NBR',
          VarArrayOf([EENbr, SYLocalNumber]), 'local_tax'), 0)));

      end;
    end;
  end;

  function CoDateStamp: string;
  begin
    Result := Trim(DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER']) + ' ' + Trim(DM_COMPANY.CO['NAME']) + ' - Check Date: ' + DateTimeToStr(CheckDate) + ' Process Date: ' + DateTimeToStr(Date) + ' ';
  end;

  function CalcFederal: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[0] = 'N'));
  end;

  function CalcLocals: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[10] = 'N'));
  end;

  function CalcSUI: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[9] = 'N'));
  end;

  function CalcStateTaxes: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[6] = 'N'));
  end;

  function CalcFuiTaxes: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[5] = 'N'));
  end;

  function CalcEeMedicare: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[3] = 'N'));
  end;

  function CalcErMedicare: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[4] = 'N'));
  end;

  function CalcERSDI: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[8] = 'N'));
  end;

  function CalcEESDI: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[7] = 'N'));
  end;

  function CalcEEOASDI: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[1] = 'N'));
  end;

  function CalcEROASDI: Boolean;
  begin
    Result := VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[2] = 'N'));
  end;

  function OnlyMovePaTaxAmounts: Boolean;
  begin
    Result := not VarIsNull(ExtraOptions) and (VarArrayHighBound(ExtraOptions, 1) > 12) and (ExtraOptions[13] = 'Y');
  end;
  
  procedure CreateCheck;
  begin
    SQuarterEndProcessing.CreateTaxAdjCheck(CheckDate);
  end;

var
  Log: string;
  QecExistsForOtherCompany: Boolean;
  CheckReturnQueue: Boolean;
  Q, QC, QB: IevQuery;
  BlobData: IisStream;
  ClBlobNbr: Integer;
  Fixes, CheckLocals: IisParamsCollection;
  AllOk: Boolean;
  CheckLineNbr, EeLocalsNbr, EmployeeNbr, j: Integer;
  CheckLocal, Fix: IisListOfValues;
  oldUpdateAsOfDate: TDateTime;
  ExemptExclude: String;
  Res: IisListOfValues;
  CheckLineLocals: IisParamsCollection;
  LineLocal: IisListOfValues;
  DbdtNbr: Integer;
  LocationLevel: String;
  Rep: IisStringList;
  SS, SL: IevQuery;
  PaSyStatesNbr: Integer;
  CoLocalTax: IisListOfValues;
  flag: Boolean;
  PaStored, DS, DS1: IevDataSet;
  PaEeNbr: Integer;
  PaLocalTax: Real;

  CalcTaxWages, ActualTaxWages, CalcTax, ActualTax, CalcGrossWages,
  ActualGrossWages, CalcTaxTips: Real;
  State: String;
  SuiList: IisListOfValues;

  procedure MovePaLocalTaxAmounts;
  var
    CoLocals: String;
    DS, LW, WW: IevDataset;
    CheckDate: TDateTime;
    EEList: TStringList;
    MyTransactionManager: TTransactionManager;
    Fix: Double;
    EeNbr, EeLocalsNbr, CoLocationsNbr, NonresEeLocalsNbr: Integer;
  begin

    EEList := TStringList.Create;

    if EECustomNbr <> '' then
    begin
      EECustomNbr := EECustomNbr + ',';
      while Pos(',', EECustomNbr) <> 0 do
      begin
        EEList.Add(Copy(EECustomNbr, 1, Pos(',', EECustomNbr) - 1));
        Delete(EECustomNbr, 1, Pos(',', EECustomNbr));
      end;
      aCount := IntToStr(EEList.Count);
    end;

    if VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[11] = 'Y')) or (QTREndDate <= Date) then
      CheckDate := QTREndDate
    else
      CheckDate := Date;
    DM_EMPLOYEE.EE.DataRequired( 'CO_NBR=' + IntToStr(CONbr));
    DM_PAYROLL.PR.DataRequired('PR_NBR=0');
    DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=0');
    DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=0');
    DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_CHECK_LOCALS_NBR=0');
    MyTransactionManager := TTransactionManager.Create(nil);
    MyTransactionManager.Initialize([DM_CLIENT.PR, DM_CLIENT.PR_BATCH, DM_CLIENT.PR_CHECK, DM_CLIENT.PR_CHECK_LOCALS]);
    try
      Q := TevQuery.Create('select CO_LOCAL_TAX_NBR, SY_LOCALS_NBR from CO_LOCAL_TAX where {AsOfNow<CO_LOCAL_TAX>}');
      Q.Result.First;
      CoLocals := '(';
      while not Q.Result.Eof do
      begin
        QC := TevQuery.Create('select 1 from SY_LOCALS where {AsOfNow<SY_LOCALS>} and SY_STATES_NBR=41 and '+
          'LOCAL_TYPE in (''N'',''R'') and SY_LOCALS_NBR='+Q.Result.FieldByName('SY_LOCALS_NBR').AsString);
        if QC.Result.RecordCount > 0 then
        begin
          if CoLocals = '(' then
            CoLocals := CoLocals + Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString
          else
            CoLocals := CoLocals + ',' + Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsString;
        end;
        Q.Result.Next;
      end;
      CoLocals := CoLocals + ')';

      Q := TevQuery.Create(
      '  select c.EE_NBR, l.*, t.SY_LOCALS_NBR'+
      '  from PR p'+
      '  join PR_CHECK c on c.PR_NBR=p.PR_NBR'+
      '  join PR_CHECK_LOCALS l on l.PR_CHECK_NBR=c.PR_CHECK_NBR'+
      '  join EE_LOCALS e on e.EE_LOCALS_NBR=l.EE_LOCALS_NBR'+
      '  join CO_LOCAL_TAX t on t.CO_LOCAL_TAX_NBR=e.CO_LOCAL_TAX_NBR'+
      '  where {AsOfNow<p>} and p.CHECK_DATE >= :BEGIN_DATE and p.CHECK_DATE <= :END_DATE'+
      '  and {AsOfNow<c>} and {AsOfNow<l>} and {AsOfNow<e>} and {AsOfNow<t>}'+
      '  and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
      '  and t.CO_LOCAL_TAX_NBR in '+ CoLocals+' and p.STATUS in ('''+PAYROLL_STATUS_PROCESSED+''') and c.CHECK_STATUS<>''V'' and p.CO_NBR='+IntToStr(CoNbr));
      Q.Params.AddValue('BEGIN_DATE', GetBeginQuarter(QTREndDate));
      Q.Params.AddValue('END_DATE', QTREndDate);

      ctx_DataAccess.SQLite.Insert('PR_CHECK_LOCALS_ACT', Q.Result.vclDataset);

      ctx_DataAccess.SQLite.Execute(
      '  create table ACTUAL as ' +
      '  select EE_NBR, EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, REPORTABLE,'+
      '  SUM(LOCAL_TAXABLE_WAGE) LOCAL_TAXABLE_WAGE, SUM(LOCAL_TAX) LOCAL_TAX'+
      '  from PR_CHECK_LOCALS_ACT l'+
      '  where REPORTABLE=''Y'''+
      '  group by EE_NBR, EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, REPORTABLE'+
      '  order by EE_NBR, EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, REPORTABLE');

      ctx_DataAccess.SQLite.Execute(
      '  create table TAXB as'+
      '  select EE_NBR, EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, sum(LOCAL_TAX) LOCAL_TAX'+
      '  from ACTUAL'+
      '  group by 1,2,3,4'+
      '  order by 1,2,3,4');

      ctx_DataAccess.SQLite.Execute(
      '  create table TAXA as'+
      '  select (select min(ee_nbr) from ee_locals l where l.ee_locals_nbr=x.ee_locals_nbr) EE_NBR,'+
      '  EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, sum(local_tax) LOCAL_TAX'+
      '  from new_qec_locations11 x group by 1,2,3,4 order by 1,2,3,4');

      ctx_DataAccess.SQLite.Execute('delete from taxb where abs(local_tax) < 0.009');

      ctx_DataAccess.SQLite.Execute('create table FIXES as'+
      '  select x.*,'+
      '  (select local_tax'+
      '  from taxa a'+
      '  where a.ee_nbr=x.ee_nbr and a.ee_locals_nbr=x.ee_locals_nbr'+
      '  and a.co_locations_nbr=x.co_locations_nbr'+
      '  and a.nonres_ee_locals_nbr=x.nonres_ee_locals_nbr) TAX'+
      '  from taxb x'+
      '  union all'+
      '  select x.EE_NBR, x.EE_LOCALS_NBR, x.CO_LOCATIONS_NBR, x.NONRES_EE_LOCALS_NBR,'+
      '  null LOCAL_TAX, LOCAL_TAX TAX'+
      '  from taxa x'+
      '  where'+
      '  (select local_tax'+
      '  from taxb a'+
      '  where a.ee_nbr=x.ee_nbr and a.ee_locals_nbr=x.ee_locals_nbr'+
      '  and a.co_locations_nbr=x.co_locations_nbr'+
      '  and a.nonres_ee_locals_nbr=x.nonres_ee_locals_nbr) is null'+
      '  order by 1,2,3,4');

      ctx_DataAccess.SQLite.Execute('update fixes set local_tax = 0 where local_tax is null');
      ctx_DataAccess.SQLite.Execute('update fixes set tax = 0 where tax is null');
      ctx_DataAccess.SQLite.Execute('create table FIXED (EE_NBR integer, EE_LOCALS_NBR integer, CO_LOCATIONS_NBR integer, NONRES_EE_LOCALS_NBR integer, LOCAL_TAX real)');

      WW := ctx_DataAccess.SQLite.Select('select * from FIXED',[]);
      DS := ctx_DataAccess.SQLite.Select('select x.*, local_tax-tax FIX from fixes x where x.local_tax > 0.009 order by ee_nbr, tax-local_tax, ee_locals_nbr',[]);

      DS.First;

      while not DS.Eof do
      begin
        EeNbr := DS.FieldByName('EE_NBR').AsInteger;
        EeLocalsNbr := DS.FieldByName('EE_LOCALS_NBR').AsInteger;
        CoLocationsNbr := DS.FieldByName('CO_LOCATIONS_NBR').AsInteger;
        NonresEeLocalsNbr := DS.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger;
        Fix := DS.FieldByName('FIX').AsFloat;
        LW := ctx_DataAccess.SQLite.Select('select * from fixes where local_tax = 0 and ee_nbr = :ee_nbr order by :tax-tax',[EeNbr, Fix]);
        WW.Insert;
        WW.FieldByName('EE_NBR').AsInteger := EeNbr;
        WW.FieldByName('EE_LOCALS_NBR').AsInteger := EeLocalsNbr;
        WW.FieldByName('CO_LOCATIONS_NBR').AsInteger := CoLocationsNbr;
        WW.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger := NonresEeLocalsNbr;
        WW.FieldByName('LOCAL_TAX').AsFloat := -Fix;
        WW.Post;

        if LW.FieldByName('ee_nbr').AsFloat = EeNbr then
        begin
          WW.Insert;
          WW.FieldByName('EE_NBR').AsInteger := EeNbr;
          WW.FieldByName('EE_LOCALS_NBR').AsInteger := LW.FieldByName('EE_LOCALS_NBR').AsInteger;
          WW.FieldByName('CO_LOCATIONS_NBR').AsInteger := LW.FieldByName('CO_LOCATIONS_NBR').AsInteger;
          WW.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger := LW.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger;
          WW.FieldByName('LOCAL_TAX').AsFloat := Fix;
          WW.Post;

          ctx_DataAccess.SQLite.Execute('update fixes set tax = tax - '+FloatToStr(Fix)+
            ' where ee_locals_nbr='+LW.FieldByName('EE_LOCALS_NBR').AsString+' and co_locations_nbr='+LW.FieldByName('CO_LOCATIONS_NBR').AsString+
            ' and nonres_ee_locals_nbr='+LW.FieldByName('NONRES_EE_LOCALS_NBR').AsString+' and tax='+LW.FieldByName('TAX').AsString);

        end;
        DS.Next;
      end;
      
      WW.First;
      ctx_DataAccess.SQLite.Insert('FIXED', WW.vclDataSet); 

      ctx_DataAccess.SQLite.Execute('delete from fixed where abs((select sum(x.local_tax) from fixed x where x.ee_nbr=fixed.ee_nbr)) > 0.009 or abs(local_tax) < 0.009');

//      ctx_DataAccess.SQLite.Execute('insert into NEW_QEC_LOCATIONS11 select PR_CHECK_NBR, EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, -LOCAL_TAXABLE_WAGE,  -LOCAL_TAX LOCAL_TAX'+
//      '  from PR_CHECK_LOCALS_ACT where REPORTABLE=''Y'' and (abs(LOCAL_TAX) > 0.009 or abs(LOCAL_TAXABLE_WAGE) > 0.009)');

      DS := ctx_DataAccess.SQLite.Select('select distinct EE_NBR from FIXED',[]);
      DS.First;
      while not DS.Eof do
      begin
        DM_CLIENT.EE.Locate('EE_NBR', DS.FieldByName('EE_NBR').Value, []);
        if (EEList.Count = 0) or (EEList.IndexOf(Trim(ConvertNull(DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ''))) <> -1) then
        begin
          SQuarterEndProcessing.CreateTaxAdjCheck(CheckDate);
          if DM_CLIENT.PR.STATUS.AsString = 'C' then
          begin
             DM_CLIENT.PR.Edit;
             DM_CLIENT.PR.STATUS.AsString := 'W';
             DM_CLIENT.PR.Post;
          end;

          WW := ctx_DataAccess.SQLite.Select('select EE_LOCALS_NBR, CO_LOCATIONS_NBR, NONRES_EE_LOCALS_NBR, sum(LOCAL_TAX) LOCAL_TAX from FIXED where EE_NBR='+
            DS.FieldByName('EE_NBR').AsString+' group by 1,2,3 order by 1,2,3',[]);
          WW.First;
          while not WW.Eof do
          begin
            DM_PAYROLL.PR_CHECK_LOCALS.Insert;
            DM_PAYROLL.PR_CHECK_LOCALS.PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
            DM_PAYROLL.PR_CHECK_LOCALS.PR_NBR.Value := DM_PAYROLL.PR_CHECK['PR_NBR'];
            DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.Value := WW['EE_LOCALS_NBR'];
            DM_PAYROLL.PR_CHECK_LOCALS.EXCLUDE_LOCAL.Value := 'N';
            DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_GROSS_WAGES.Value := 0;
            DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_TAXABLE_WAGE.Value := 0;
            DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_TAX.Value := WW.FieldByName('LOCAL_TAX').AsFloat;
            if (WW.FieldByName('CO_LOCATIONS_NBR').AsString <> '') and (WW.FieldByName('CO_LOCATIONS_NBR').AsInteger <> 0) then
              DM_PAYROLL.PR_CHECK_LOCALS.CO_LOCATIONS_NBR.Value := WW['CO_LOCATIONS_NBR'];
            if (WW.FieldByName('NONRES_EE_LOCALS_NBR').AsString <> '') and (WW.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger <> 0) then
              DM_PAYROLL.PR_CHECK_LOCALS.NONRES_EE_LOCALS_NBR.Value := WW['NONRES_EE_LOCALS_NBR'];
            DM_PAYROLL.PR_CHECK_LOCALS.REPORTABLE.AsString := 'Y';
            DM_PAYROLL.PR_CHECK_LOCALS.Post;
            WW.Next;
          end;
        end;
        DS.Next;
      end;

      MyTransactionManager.ApplyUpdates;
    finally
      MyTransactionManager.Free;
    end;
    EEList.Free;
  end;

var
  DebugList: IisParamsCollection;
  L, LN: IisListOfValues;
  k: Integer;

begin
  Evo := TevHistData.Create;

  if not ForceCleanup and not ctx_DataAccess.QecRecommended(CONbr, QTREndDate) then
  begin
    Result := True; // do not process payroll
    Exit;
  end;

  Result := True; // Process payroll
  SUIEmployeeCount := -1;
  if VarIsNull(ExtraOptions) or (not VarIsNull(ExtraOptions) and (ExtraOptions[11] = 'Y')) or (QTREndDate <= Date) then
    CheckDate := QTREndDate
  else
    CheckDate := Date;


  PaExcludedTaxes := TevDataset.Create;
  PaExcludedTaxes.vclDataSet.FieldDefs.Add('EE_LOCALS_NBR', ftInteger);
  PaExcludedTaxes.vclDataSet.FieldDefs.Add('PR_CHECK_LINES_NBR', ftInteger);
  PaExcludedTaxes.vclDataSet.FieldDefs.Add('EXEMPT_EXCLUDE', ftString, 1);
  PaExcludedTaxes.vclDataSet.Open;
  PaExcludedTaxes.IndexFieldNames := 'PR_CHECK_LINES_NBR;EE_LOCALS_NBR';

  ctx_PayrollCalculation.ProcessFeedback(True);
  try
    MinimumTax := MinTax;
    MyCount := 0;

    DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(CONbr));
    if ReportFilePath = '' then
    else if ReportFilePath[Length(ReportFilePath)] = '\' then
      AssignFile(LogFile, ReportFilePath + Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString) + '_Cleanup.log')
    else
      AssignFile(LogFile, ReportFilePath + '\' + Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString) + '_Cleanup.log');
    if ReportFilePath <> '' then
      Rewrite(LogFile);

    DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(CONbr) + ' and PAYROLL_TYPE=''' + PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT + ''' and STATUS <> ''' + PAYROLL_STATUS_PROCESSED + ''' and STATUS <> ''' + PAYROLL_STATUS_VOIDED + '''');
    if DM_PAYROLL.PR.RecordCount > 0 then
    begin
      if ReportFilePath <> '' then
        Writeln(LogFile, CoDateStamp + 'Previous Quarter End Cleanup is not finished');
      Warnings := CoDateStamp + 'Previous Quarter End Cleanup is not finished';
      Result := False;
      Exit;
    end;

    Q := TevQuery.Create('CheckIfQecExists');
    Q.Param['CO_NBR'] := CONbr;
    QecExistsForOtherCompany := Q.Result.RecordCount > 0;

    if QecExistsForOtherCompany then
    begin
      if ReportFilePath <> '' then
        Writeln(LogFile, CoDateStamp + 'Previous Quarter End Cleanup is not finished for consolidated company');
//      raise EevException.Create(CoDateStamp + 'Previous Quarter End Cleanup is not finished for consolidated company');
      Warnings := CoDateStamp + 'Previous Quarter End Cleanup is not finished for consolidated company';
      Result := true;
      Exit;
    end;

    LockResource(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), 'A payroll for this client is already being processed by ', LockType);
    LockResource(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + IntToStr(CONbr), 'A payroll for this company is already being processed by ', LockType);
    try

  // Start getting data

      if OnlyMovePaTaxAmounts then
      begin
        ctx_PayrollCalculation.ProcessFeedback(True, 'Moving PA taxes ...');

        ctx_DataAccess.SQLite := TSQLite.Create(':memory:');

        CalculateLocalTaxesLite(ctx_DataAccess.SQLite, CoNbr, 0, QTREndDate, False, True);
        MovePaLocalTaxAmounts;
        ctx_PayrollCalculation.ProcessFeedback(True, '...');
        Exit;        
      end;

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading tables ...');

      DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + IntToStr(CONbr));
      DM_PAYROLL.PR.DataRequired('PR_NBR=0');
      DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=0');
      DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=0');
      DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_STATES_NBR=0');
      DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_CHECK_SUI_NBR=0');
      DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_CHECK_LOCALS_NBR=0');
      DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR=0');

      SORTED_PR := TevClientDataSet.Create(nil);
      SORTED_PR.Name := 'PR';

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading payrolls');

      Q := TevQuery.Create('SelectSortedPayrollByCheckDateRunNumber');
      Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['CoNbr'] := CoNbr;
      SORTED_PR.Data := Q.Result.vclDataset.Data;
      SORTED_PR.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER';

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading checks');

      SORTED_PR_CHECK := TevClientDataSet.Create(nil);
      SORTED_PR_CHECK.Name := 'PR_CHECK';
      Q := TevQuery.Create('SelectSortedChecksByCheckDateRunNumber');
      Q.Macro['Columns'] := 'C.*, P.CHECK_DATE, P.RUN_NUMBER, P.PAYROLL_TYPE, P.FILLER PR_FILLER';
      Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['CoNbr'] := CoNbr;
      SORTED_PR_CHECK.Data := Q.Result.vclDataset.Data;
      SORTED_PR_CHECK.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER';

      SORTED_PR_CHECK_STATES := TevClientDataSet.Create(nil);
      SORTED_PR_CHECK_STATES.Name := 'PR_CHECK_STATES';
      Q := TevQuery.Create('SelectSortedTableByCheckDateRunNumber');
      Q.Macro['TableName'] := 'PR_CHECK_STATES';
      Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['CoNbr'] := CoNbr;
      SORTED_PR_CHECK_STATES.Data := Q.Result.vclDataset.Data;
      SORTED_PR_CHECK_STATES.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER;PR_CHECK_STATES_NBR';

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading locals');

      SORTED_PR_CHECK_LOCALS := TevClientDataSet.Create(nil);
      SORTED_PR_CHECK_LOCALS.Name := 'PR_CHECK_LOCALS';
      Q := TevQuery.Create('SelectSortedTableByCheckDateRunNumber');
      Q.Macro['TableName'] := 'PR_CHECK_LOCALS';
      Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['CoNbr'] := CoNbr;
      SORTED_PR_CHECK_LOCALS.Data := Q.Result.vclDataset.Data;
      SORTED_PR_CHECK_LOCALS.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER;PR_CHECK_LOCALS_NBR';

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading check lines');

      SORTED_PR_CHECK_LINES := TevClientDataSet.Create(nil);
      SORTED_PR_CHECK_LINES.Name := 'PR_CHECK_LINES';
      SORTED_PR_CHECK_LINES_FOR_FUI := TevClientDataSet.Create(nil);
      SORTED_PR_CHECK_LINES_FOR_FUI.Name := 'PR_CHECK_LINES';

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading tables ... check line locals');

      DM_PAYROLL.PR_CHECK_LINE_LOCALS.DataRequired('ALL'); // This will have to be changed to something nicer when it gets PR_NBR attached

      ctx_PayrollCalculation.ProcessFeedback(True, 'Loading YTD data');

      FEDERAL_YTD := TevClientDataSet.Create(nil);
      FEDERAL_YTD.Name := 'FEDERAL_YTD';
      Q := TevQuery.Create('FederalTaxesYTDData');
      Q.Param['StartDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['StartRN'] := 1;
      Q.Param['EndRN'] := 999;
      Q.Param['CoNbr'] := CoNbr;
      Q.Param['Status'] := PAYROLL_STATUS_DELETED;
      FEDERAL_YTD.Data := Q.Result.vclDataset.Data;


      STATE_YTD := TevClientDataSet.Create(nil);
      STATE_YTD.Name := 'STATE_YTD';
      Q := TevQuery.Create('SDIAndStatesTaxesYTDData');
      Q.Param['StartDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['StartRN'] := 1;
      Q.Param['EndRN'] := 999;
      Q.Param['CoNbr'] := CoNbr;
      Q.Param['Status'] := PAYROLL_STATUS_DELETED;
      STATE_YTD.Data := Q.Result.vclDataset.Data;

      LOCAL_YTD := TevClientDataSet.Create(nil);
      LOCAL_YTD.Name := 'LOCAL_YTD';
      Q := TevQuery.Create('LocalTaxesYTDData');
      Q.Param['StartDate'] := GetBeginQuarter(QTREndDate);
      Q.Param['EndDate'] := QTREndDate;
      Q.Param['StartRN'] := 1;
      Q.Param['EndRN'] := 999;
      Q.Param['CoNbr'] := CoNbr;
      Q.Param['Status'] := PAYROLL_STATUS_DELETED;
      LOCAL_YTD.Data := Q.Result.vclDataset.Data;

      NJ_Newark_Payroll_Tax_Wages_YTD := Null;

      LOCAL_YTQ := TevClientDataSet.Create(nil);
      LOCAL_YTQ.Name := 'LOCAL_YTQ';
      Q := TevQuery.Create('LocalTaxesYTDData');
      Q.Param['StartDate'] := GetBeginYear(QTREndDate);
      Q.Param['EndDate'] := GetBeginQuarter(QTREndDate) - 1;
      Q.Param['StartRN'] := 1;
      Q.Param['EndRN'] := 999;
      Q.Param['CoNbr'] := CoNbr;
      Q.Param['Status'] := PAYROLL_STATUS_DELETED;
      LOCAL_YTQ.Data := Q.Result.vclDataset.Data;

      // End getting data

      ALL_CHECKS := TevClientDataSet.Create(nil);
      ALL_CHECKS_FOR_FUI := TevClientDataSet.Create(nil);
      TaxWagesList := TStringList.Create;
      GrossWagesList := TStringList.Create;
      TaxList := TStringList.Create;
      CLPersonTaxes := TevClientDataSet.Create(nil);

      EEList := TIsListOfValues.Create;

      MyTransactionManager := TTransactionManager.Create(nil);

      try

        if EECustomNbr = '' then
          aCount := IntToStr(DM_EMPLOYEE.EE.RecordCount)
        else
        begin
          EECustomNbr := EECustomNbr + ',';
          while Pos(',', EECustomNbr) <> 0 do
          begin
            EEList.AddValue(Copy(EECustomNbr, 1, Pos(',', EECustomNbr) - 1), 0);
            Delete(EECustomNbr, 1, Pos(',', EECustomNbr));
          end;
          aCount := IntToStr(EEList.Count);
        end;

        CLPersonTaxes.FieldDefs.Add('CL_PERSON_NBR', ftInteger);
        CLPersonTaxes.FieldDefs.Add('EE_OASDI_WAGES', ftCurrency);
        CLPersonTaxes.FieldDefs.Add('EE_OASDI_TAX', ftCurrency);
        CLPersonTaxes.FieldDefs.Add('ER_OASDI_WAGES', ftCurrency);
        CLPersonTaxes.FieldDefs.Add('ER_OASDI_TAX', ftCurrency);
        CLPersonTaxes.FieldDefs.Add('FUI_WAGES', ftCurrency);
        CLPersonTaxes.FieldDefs.Add('FUI_TAX', ftCurrency);
        CLPersonTaxes.CreateDataSet;
        CLPersonTaxes.IndexFieldNames := 'CL_PERSON_NBR';

        if EEList.Count = 0 then
        begin
          Q := TevQuery.Create('select distinct CUSTOM_EMPLOYEE_NUMBER from EE where {AsOfNow<EE>} '+
            ' and exists(select 1 from PR_CHECK join PR on PR.PR_NBR=PR_CHECK.PR_NBR and {AsOfNow<PR>} where {AsOfNow<PR_CHECK>} and PR_CHECK.EE_NBR=EE.EE_NBR and PR.CHECK_DATE between :BeginDate and :EndDate)');

          Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
          Q.Param['EndDate'] := QTREndDate;
          Q.Param['CoNbr'] := CoNbr;

          EmployeeList := Q.Result;

          EmployeeList.First;
          while not EmployeeList.Eof do
          begin
            EEList.AddValue(Trim(EmployeeList.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString), 0);
            EmployeeList.Next;
          end;

        end;

        with DM_EMPLOYEE, DM_PAYROLL do
        begin
          MyTransactionManager.Initialize([PR, PR_BATCH, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, DM_CLIENT.CL_BLOB, PR_CHECK_LINES]);

          DM_EMPLOYEE.EE.First;

          if CalcLocals then
          begin
            ctx_PayrollCalculation.ProcessFeedback(True, 'Checking Act 32 ...');
            ctx_DataAccess.SQLite := TSQLite.Create(':memory:');
            CalculateLocalTaxesLite(ctx_DataAccess.SQLite, CoNbr, 0, QTREndDate, False, False);
            ctx_PayrollCalculation.ProcessFeedback(True, '...');
          end;

          while not EE.Eof do
          if (EEList.Count > 0) and not EEList.ValueExists(Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ''))) then
            EE.Next
          else
          try
            EmployeeNbr := EE['EE_NBR'];
            Inc(MyCount);

            ctx_PayrollCalculation.ProcessFeedback(True, 'Calculating EE #' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, '')) + '  (' +
              IntToStr(MyCount) + ' of ' + aCount + ')...');

            Net := 0;

            Person := Evo.GetPerson(EmployeeNbr, QTREndDate);
            Person.RedistributeNegativeCheckLines(QTREndDate);

            
            Q := TevQuery.Create(
                'select P.PR_NBR, P.CHECK_DATE, P.RUN_NUMBER, P.PROCESS_DATE, P.PAYROLL_TYPE, C.PR_CHECK_NBR, C.EE_NBR, C.PAYMENT_SERIAL_NUMBER ' +
                'from PR_CHECK C ' +
                'join PR P on C.PR_NBR=P.PR_NBR ' +
                'join EE E on E.CL_PERSON_NBR=:ClPersonNbr and C.EE_NBR=E.EE_NBR '+
                'where {AsOfNow<C>} and ' +
                '{AsOfNow<P>} and ' +
                '{AsOfNow<E>} and ' +                
                'P.CHECK_DATE between :BeginDate and :EndDate and ' +
                'P.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''') and P.CO_NBR=:CoNbr and '+
                'c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') and '+
                'P.PAYROLL_TYPE<>''V'' and C.CHECK_STATUS<>''V'' and C.CHECK_TYPE<>''V'' and ' +
                'P.PAYROLL_TYPE<>''A''');

            Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
            Q.Param['EndDate'] := QTREndDate;
            Q.Param['ClPersonNbr'] := Person.ClPersonNbr;
            Q.Param['CoNbr'] := CoNbr;
            ALL_CHECKS.Data := Q.Result.vclDataset.Data;

            if CurrentQuarterOnly then
              ALL_CHECKS_FOR_FUI.Data := ALL_CHECKS.Data
            else
            begin
              Q := TevQuery.Create(
                  'select P.PR_NBR, P.CHECK_DATE, P.RUN_NUMBER, P.PROCESS_DATE, P.PAYROLL_TYPE, C.PR_CHECK_NBR, C.EE_NBR, C.PAYMENT_SERIAL_NUMBER ' +
                  'from PR_CHECK C ' +
                  'join PR P on C.PR_NBR=P.PR_NBR ' +
                  'join EE E on E.CL_PERSON_NBR=:ClPersonNbr and C.EE_NBR=E.EE_NBR '+
                  'where {AsOfNow<C>} and ' +
                  '{AsOfNow<P>} and ' +
                  '{AsOfNow<E>} and ' +
                  'P.CHECK_DATE between :BeginDate and :EndDate and ' +
                  'P.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''') and P.CO_NBR=:CoNbr and '+
                  'c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') and '+
                  'P.PAYROLL_TYPE<>''V'' and C.CHECK_STATUS<>''V'' and C.CHECK_TYPE<>''V'' and ' +
                  'P.PAYROLL_TYPE<>''A''');

              Q.Param['BeginDate'] := GetBeginYear(QTREndDate);
              Q.Param['EndDate'] := QTREndDate;
              Q.Param['ClPersonNbr'] := Person.ClPersonNbr;
              Q.Param['CoNbr'] := CoNbr;
              ALL_CHECKS_FOR_FUI.Data := Q.Result.vclDataset.Data;
            end;
            ALL_CHECKS.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER';
            ALL_CHECKS_FOR_FUI.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER';

            Q := TevQuery.Create(
              'SELECT P.CHECK_DATE, P.RUN_NUMBER, '+
              ' C.EE_NBR, '+
              ' C.PAYMENT_SERIAL_NUMBER,'+
              ' S.PR_CHECK_LINES_NBR, ' +
              ' S.PR_CHECK_NBR, ' +
              ' S.CO_DIVISION_NBR, ' +
              ' S.CO_BRANCH_NBR, ' +
              ' S.CO_DEPARTMENT_NBR, ' +
              ' S.CO_TEAM_NBR, ' +
              ' S.CO_JOBS_NBR, ' +
              ' S.CL_E_DS_NBR, ' +
              ' S.EE_STATES_NBR, ' +
              ' S.EE_SUI_STATES_NBR, ' +
              ' S.AMOUNT, ' +
              ' S.PR_NBR ' +
              'from PR_CHECK_LINES S ' +
              'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
              'join PR P on C.PR_NBR=P.PR_NBR ' +
              'join EE E on E.CL_PERSON_NBR=:ClPersonNbr and C.EE_NBR=E.EE_NBR '+
              'where {AsOfNow<s>} and ' +
              ' {AsOfNow<C>} and ' +
              ' {AsOfNow<P>} and ' +
              ' {AsOfNow<E>} and ' +
              ' P.PR_NBR=S.PR_NBR and ' +
              'P.CHECK_DATE between :BeginDate and :EndDate and ' +
              'P.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''') and P.CO_NBR=:CoNbr and '+
              'P.PAYROLL_TYPE<>''V'' and C.CHECK_STATUS<>''V'' and C.CHECK_TYPE<>''V'''
            );
            Q.Param['BeginDate'] := GetBeginQuarter(QTREndDate);
            Q.Param['EndDate'] := QTREndDate;
            Q.Param['ClPersonNbr'] := Person.ClPersonNbr;
            Q.Param['CoNbr'] := CoNbr;
            SORTED_PR_CHECK_LINES.Data := Q.Result.vclDataset.Data;

            if CurrentQuarterOnly then
              SORTED_PR_CHECK_LINES_FOR_FUI.Data := SORTED_PR_CHECK_LINES.Data
            else
            begin
              Q := TevQuery.Create('select P.CHECK_DATE, B.PERIOD_BEGIN_DATE, P.RUN_NUMBER, P.PAYROLL_TYPE, C.EE_NBR, C.PAYMENT_SERIAL_NUMBER, ' +
                  ' S.PR_CHECK_LINES_NBR, ' +
                  ' S.PR_CHECK_NBR, ' +
                  ' S.CO_DIVISION_NBR, ' +
                  ' S.CO_BRANCH_NBR, ' +
                  ' S.CO_DEPARTMENT_NBR, ' +
                  ' S.CO_TEAM_NBR, ' +
                  ' S.CO_JOBS_NBR, ' +
                  ' S.CL_E_DS_NBR, ' +
                  ' S.EE_STATES_NBR, ' +
                  ' S.EE_SUI_STATES_NBR, ' +
                  ' S.AMOUNT, ' +
                  ' S.PR_NBR ' +
                  'from PR_CHECK_LINES S ' +
                  'join PR_CHECK C on S.PR_CHECK_NBR=C.PR_CHECK_NBR ' +
                  'join PR P on C.PR_NBR=P.PR_NBR ' +
                  'join PR_BATCH B on B.PR_BATCH_NBR=C.PR_BATCH_NBR ' +
                  'join EE E on E.CL_PERSON_NBR=:ClPersonNbr and C.EE_NBR=E.EE_NBR '+
                  'where {AsOfNow<S>} and ' +
                  'P.PR_NBR=S.PR_NBR and ' +
                  '{AsOfNow<C>} and ' +
                  '{AsOfNow<P>} and ' +
                  '{AsOfNow<B>} and ' +
                  '{AsOfNow<E>} and ' +                  
                  'P.CHECK_DATE between :BeginDate and :EndDate and ' +
                  'P.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''') and P.CO_NBR=:CoNbr and '+
                  'P.PAYROLL_TYPE<>''V'' and C.CHECK_STATUS<>''V'' and C.CHECK_TYPE<>''V''');

              Q.Param['BeginDate'] := GetBeginYear(QTREndDate);
              Q.Param['EndDate'] := QTREndDate;
              Q.Param['ClPersonNbr'] := Person.ClPersonNbr;
              Q.Param['CoNbr'] := CoNbr;
              SORTED_PR_CHECK_LINES_FOR_FUI.Data := Q.Result.vclDataset.Data;
            end;
            SORTED_PR_CHECK_LINES.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER;AMOUNT';
            SORTED_PR_CHECK_LINES_FOR_FUI.IndexFieldNames := 'CHECK_DATE;RUN_NUMBER;EE_NBR;PAYMENT_SERIAL_NUMBER;AMOUNT';

            if CalcFederal then
            begin
              CalcTaxWages := Person.CalcFederalTaxableWages(EmployeeNbr);
              ActualTaxWages := Person.FederalTaxableWages(EmployeeNbr);
              if Abs(CalcTaxWages - ActualTaxWages) > 0.009 then
              begin
                CreateCheck;
                Person.QecCheck[EmployeeNbr].FederalTaxableWages := RoundTwo(CalcTaxWages - ActualTaxWages);
                Person.QecCheck[EmployeeNbr].FederalGrossWages   := RoundTwo(CalcTaxWages - ActualTaxWages);

                PR_CHECK.Edit;
                PR_CHECK['FEDERAL_TAXABLE_WAGES'] := RoundTwo(CalcTaxWages - ActualTaxWages);
                PR_CHECK['FEDERAL_GROSS_WAGES'] := RoundTwo(CalcTaxWages - ActualTaxWages);
                PR_CHECK.Post;

                if ReportFilePath <> '' then
                  Writeln(LogFile, CoDateStamp + 'Federal: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' + FloatToStrF(TaxWages,
                    ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(0, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(0, ffCurrency, 10, 2));
              end;
            end;

            if not Evo.IgnoreFICA(EmployeeNbr, QTREndDate) then
            begin
              if CalcEEOASDI then
              begin

                Person.RestoreOriginalCheckLines;
                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  CalculateEEOASDITaxes(QTREndDate, EmployeeNbr, TaxWages, TaxTips, GrossWages, Tax);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter
                  Person.RedistributeNegativeCheckLines(TmpCheckDate);
                  CalculateEEOASDITaxes(TmpCheckDate, EmployeeNbr, CalcTaxWages, CalcTaxTips, CalcGrossWages, CalcTax);
                  Person.RestoreOriginalCheckLines;
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  CalculateEEOASDITaxes(QTREndDate, EmployeeNbr, TaxWages, TaxTips, GrossWages, Tax);
                  TaxWages := TaxWages - CalcTaxWages;
                  TaxTips := TaxTips - CalcTaxTips;
                  GrossWages := GrossWages - CalcGrossWages;
                  Tax := Tax - CalcTax;
                end;

                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(TaxTips) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin
                  if RoundTwo(Tax) <> 0 then
                    Result := False;
                  Net := Net - RoundTwo(Tax);
                  CreateCheck;

                  Person.QecCheck[EmployeeNbr].EeOasdiTaxableWages := RoundTwo(TaxWages);
                  Person.QecCheck[EmployeeNbr].EeOasdiTips := RoundTwo(TaxTips);
                  Person.QecCheck[EmployeeNbr].EeOasdiGrossWages := RoundTwo(GrossWages);
                  Person.QecCheck[EmployeeNbr].EeOasdiTax := RoundTwo(Tax);

                  PR_CHECK.Edit;
                  PR_CHECK['EE_OASDI_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                  PR_CHECK['EE_OASDI_TAXABLE_TIPS'] := RoundTwo(TaxTips);
                  PR_CHECK['EE_OASDI_GROSS_WAGES'] := RoundTwo(GrossWages);
                  PR_CHECK['EE_OASDI_TAX'] := RoundTwo(Tax);
                  PR_CHECK.Post;

                  if ReportFilePath <> '' then
                    Writeln(LogFile, CoDateStamp + 'EE OASDI: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' + FloatToStrF(TaxWages,
                      ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(TaxTips, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax, ffCurrency, 10, 2));
                end;
              end;

              if CalcEROASDI then
              begin

                Person.RestoreOriginalCheckLines;
                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  CalculateEROASDITaxes(QTREndDate, EmployeeNbr, TaxWages, TaxTips, GrossWages, Tax);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter
                  Person.RedistributeNegativeCheckLines(TmpCheckDate);
                  CalculateEROASDITaxes(TmpCheckDate, EmployeeNbr, CalcTaxWages, CalcTaxTips, CalcGrossWages, CalcTax);
                  Person.RestoreOriginalCheckLines;
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  CalculateEROASDITaxes(QTREndDate, EmployeeNbr, TaxWages, TaxTips, GrossWages, Tax);
                  TaxWages := TaxWages - CalcTaxWages;
                  TaxTips := TaxTips - CalcTaxTips;
                  GrossWages := GrossWages - CalcGrossWages;
                  Tax := Tax - CalcTax;
                end;

                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(TaxTips) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin
                  CreateCheck;

                  Person.QecCheck[EmployeeNbr].ErOasdiTaxableWages := RoundTwo(TaxWages);
                  Person.QecCheck[EmployeeNbr].ErOasdiTips := RoundTwo(TaxTips);
                  Person.QecCheck[EmployeeNbr].ErOasdiGrossWages := RoundTwo(GrossWages);
                  Person.QecCheck[EmployeeNbr].ErOasdiTax := RoundTwo(Tax);

                  PR_CHECK.Edit;
                  PR_CHECK['ER_OASDI_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                  PR_CHECK['ER_OASDI_TAXABLE_TIPS'] := RoundTwo(TaxTips);
                  PR_CHECK['ER_OASDI_GROSS_WAGES'] := RoundTwo(GrossWages);
                  PR_CHECK['ER_OASDI_TAX'] := RoundTwo(Tax);
                  PR_CHECK.Post;

                  if ReportFilePath <> '' then
                    Writeln(LogFile, CoDateStamp + 'ER OASDI: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' + FloatToStrF(TaxWages,
                      ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(TaxTips, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax, ffCurrency, 10, 2));
                end;
              end;

              if CalcEeMedicare then
              begin
                Person.RestoreOriginalCheckLines;

                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  ActualGrossWages := Person.EeMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  CalcGrossWages   := Person.CalcEeMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  CalcTaxWages     := Person.CalcEeMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  ActualTaxWages   := Person.EeMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  CalcTax          := Person.CalcEeMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  ActualTax        := Person.EeMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter
                  Person.RedistributeNegativeCheckLines(TmpCheckDate);

                  ActualGrossWages := Person.EeMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcGrossWages   := Person.CalcEeMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcTaxWages     := Person.CalcEeMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTaxWages   := Person.EeMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcTax          := Person.CalcEeMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTax        := Person.EeMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);

                  ActualGrossWages := Person.EeMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - ActualGrossWages;
                  CalcGrossWages   := Person.CalcEeMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - CalcGrossWages;
                  CalcTaxWages     := Person.CalcEeMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - CalcTaxWages;
                  ActualTaxWages   := Person.EeMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - ActualTaxWages;
                  CalcTax          := Person.CalcEeMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - CalcTax;
                  ActualTax        := Person.EeMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - ActualTax;

                end;

                Tax        := RoundTwo(SubtractWithLimit(CalcTax, ActualTax));
                TaxWages   := SubtractWithLimit(CalcTaxWages, ActualTaxWages);
                GrossWages := SubtractWithLimit(CalcGrossWages, ActualGrossWages);

                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin
                  if Tax <> 0 then
                    Result := False;
                  Net := Net - RoundTwo(Tax);
                  CreateCheck;
                  PR_CHECK.Edit;
                  PR_CHECK['EE_MEDICARE_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                  PR_CHECK['EE_MEDICARE_GROSS_WAGES'] := RoundTwo(GrossWages);
                  PR_CHECK['EE_MEDICARE_TAX'] := RoundTwo(Tax);
                  PR_CHECK.Post;

                  if ReportFilePath <> '' then
                    Writeln(LogFile, CoDateStamp + 'EE Medicare: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' +
                      FloatToStrF(TaxWages, ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(0, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax,
                      ffCurrency, 10, 2));
                end;
              end;

              if CalcErMedicare then
              begin
                Person.RestoreOriginalCheckLines;

                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  ActualGrossWages := Person.ErMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  CalcGrossWages   := Person.CalcErMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  CalcTaxWages     := Person.CalcErMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  ActualTaxWages   := Person.ErMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  CalcTax          := Person.CalcErMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                  ActualTax        := Person.ErMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter
                  Person.RedistributeNegativeCheckLines(TmpCheckDate);

                  ActualGrossWages := Person.ErMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcGrossWages   := Person.CalcErMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcTaxWages     := Person.CalcErMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTaxWages   := Person.ErMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcTax          := Person.CalcErMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTax        := Person.ErMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), TmpCheckDate);

                  ActualGrossWages := Person.ErMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - ActualGrossWages;
                  CalcGrossWages   := Person.CalcErMedicareGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - CalcGrossWages;
                  CalcTaxWages     := Person.CalcErMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - CalcTaxWages;
                  ActualTaxWages   := Person.ErMedicareTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - ActualTaxWages;
                  CalcTax          := Person.CalcErMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - CalcTax;
                  ActualTax        := Person.ErMedicareTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate) - ActualTax;

                end;

                Tax        := RoundTwo(SubtractWithLimit(CalcTax, ActualTax));
                TaxWages   := SubtractWithLimit(CalcTaxWages, ActualTaxWages);
                GrossWages := SubtractWithLimit(CalcGrossWages, ActualGrossWages);
                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin
                  CreateCheck;
                  PR_CHECK.Edit;
                  PR_CHECK['ER_MEDICARE_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                  PR_CHECK['ER_MEDICARE_GROSS_WAGES'] := RoundTwo(GrossWages);
                  PR_CHECK['ER_MEDICARE_TAX'] := RoundTwo(Tax);
                  PR_CHECK.Post;

                  if ReportFilePath <> '' then
                    Writeln(LogFile, CoDateStamp + 'ER Medicare: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' +
                      FloatToStrF(TaxWages, ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(0, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax,
                      ffCurrency, 10, 2));
                end;
              end;

            end;

            if CalcFuiTaxes then
            begin

              CalcTaxWages := Person.CalcFuiTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
              ActualTaxWages := Person.FuiTaxableWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
              TaxWages   := SubtractWithLimit(CalcTaxWages, ActualTaxWages);
              CalcTax := Person.CalcFuiTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
              ActualTax := Person.FuiTax(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);
              Tax        := RoundTwo(SubtractWithLimit(CalcTax, ActualTax));
              GrossWages := SubtractWithLimit(Person.CalcFuiGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate), Person.FuiGrossWages(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate));

              if (RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
              begin
                CreateCheck;
                PR_CHECK.Edit;
                PR_CHECK['ER_FUI_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                PR_CHECK['ER_FUI_GROSS_WAGES'] := RoundTwo(GrossWages);
                PR_CHECK['ER_FUI_TAX'] := RoundTwo(Tax);
                PR_CHECK.Post;

                if ReportFilePath <> '' then
                  Writeln(LogFile, CoDateStamp + 'FUI: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' +
                    FloatToStrF(TaxWages, ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(0, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax,
                    ffCurrency, 10, 2));
              end;

            end;

            if CalcStateTaxes then
            begin
              for i := 0 to Person.StatesCount(EmployeeNbr) - 1 do
              begin
                State := Person.State[EmployeeNbr, i];

                Person.RestoreOriginalCheckLines;

                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  CalcTaxWages := Person.CalcStateTaxableWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  ActualTaxWages := Person.StateTaxableWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  CalcGrossWages := Person.CalcStateGrossWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  ActualGrossWages := Person.StateGrossWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter
                  
                  Person.RestoreOriginalCheckLines;
                  Person.RedistributeNegativeCheckLines(TmpCheckDate);

                  CalcTaxWages := Person.CalcStateTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTaxWages := Person.StateTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcGrossWages := Person.CalcStateGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualGrossWages := Person.StateGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);

                  Person.RestoreOriginalCheckLines;
                  Person.RedistributeNegativeCheckLines(QTREndDate);

                  CalcTaxWages := Person.CalcStateTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - CalcTaxWages;
                  ActualTaxWages := Person.StateTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - ActualTaxWages;
                  CalcGrossWages := Person.CalcStateGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - CalcGrossWages;
                  ActualGrossWages := Person.StateGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - ActualGrossWages;

                end;

                if (Abs(CalcTaxWages - ActualTaxWages) > 0.009) or (Abs(CalcGrossWages - ActualGrossWages) > 0.009) then
                begin
                  CreateCheck;
                  if not PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([PR_CHECK['PR_CHECK_NBR'],
                    Evo.GetEeStatesNbr(EmployeeNbr, State, CheckDate)]), []) then
                  begin
                    PR_CHECK_STATES.Insert;
                    PR_CHECK_STATES['EE_STATES_NBR'] := Evo.GetEeStatesNbr(EmployeeNbr, State, CheckDate);
                    PR_CHECK_STATES['TAX_AT_SUPPLEMENTAL_RATE'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_STATE'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_SDI'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_SUI'] := 'N';
                    PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
                    PR_CHECK_STATES['EXCLUDE_ADDITIONAL_STATE'] := 'N';
                    PR_CHECK_STATES.Post;
                  end;
                  PR_CHECK_STATES.Edit;
                  PR_CHECK_STATES['STATE_TAXABLE_WAGES'] := RoundTwo(CalcTaxWages - ActualTaxWages);
                  PR_CHECK_STATES['STATE_GROSS_WAGES'] := RoundTwo(CalcGrossWages - ActualGrossWages);
                  PR_CHECK_STATES.Post;
                end;
              end;
            end;

            if CalcEESDI then
            begin
              for i := 0 to Person.StatesCount(EmployeeNbr) - 1 do
              begin
                Person.RestoreOriginalCheckLines;
                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  Person.RedistributeNegativeCheckLines(QTREndDate);
                  State := Person.State[EmployeeNbr, i];
                  CalcTaxWages := Person.CalcEeSdiTaxableWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  ActualTaxWages := Person.EeSdiTaxableWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  CalcTax := Person.CalcEeSdiTax(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  ActualTax := Person.EeSdiTax(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  CalcGrossWages := Person.CalcEeSdiGrossWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                  ActualGrossWages := Person.EeSdiGrossWages(EmployeeNbr, State, GetBeginQuarter(QTREndDate), QTREndDate);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter
                  Person.RestoreOriginalCheckLines;
                  Person.RedistributeNegativeCheckLines(TmpCheckDate);

                  CalcTaxWages := Person.CalcEeSdiTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTaxWages := Person.EeSdiTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcTax := Person.CalcEeSdiTax(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTax := Person.EeSdiTax(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  CalcGrossWages := Person.CalcEeSdiGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualGrossWages := Person.EeSdiGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), TmpCheckDate);

                  Person.RestoreOriginalCheckLines;
                  Person.RedistributeNegativeCheckLines(QTREndDate);

                  CalcTaxWages := Person.CalcEeSdiTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - CalcTaxWages;
                  ActualTaxWages := Person.EeSdiTaxableWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - ActualTaxWages;
                  CalcTax := Person.CalcEeSdiTax(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - CalcTax;
                  ActualTax := Person.EeSdiTax(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - ActualTax;
                  CalcGrossWages := Person.CalcEeSdiGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - CalcGrossWages;
                  ActualGrossWages := Person.EeSdiGrossWages(EmployeeNbr, State, GetBeginYear(QTREndDate), QTREndDate) - ActualGrossWages;

                end;
                if (Abs(CalcTaxWages - ActualTaxWages) > 0.009) or (Abs(CalcGrossWages - ActualGrossWages) > 0.009) or
                (not (Abs(CalcTax - ActualTax) < MinimumTax) and (Abs(CalcTax - ActualTax) > 0.005)) then
                begin
                  if Abs(CalcTax - ActualTax) > 0.009 then
                    Result := False;

                  CreateCheck;
                  if not PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([PR_CHECK['PR_CHECK_NBR'],
                    Evo.GetEeStatesNbr(EmployeeNbr, State, CheckDate)]), []) then
                  begin
                    PR_CHECK_STATES.Insert;
                    PR_CHECK_STATES['EE_STATES_NBR'] := Evo.GetEeStatesNbr(EmployeeNbr, State, CheckDate);
                    PR_CHECK_STATES['TAX_AT_SUPPLEMENTAL_RATE'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_STATE'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_SDI'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_SUI'] := 'N';
                    PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
                    PR_CHECK_STATES['EXCLUDE_ADDITIONAL_STATE'] := 'N';
                    PR_CHECK_STATES.Post;
                  end;
                  PR_CHECK_STATES.Edit;
                  PR_CHECK_STATES['EE_SDI_TAXABLE_WAGES'] := RoundTwo(CalcTaxWages - ActualTaxWages);
                  PR_CHECK_STATES['EE_SDI_GROSS_WAGES'] := RoundTwo(CalcGrossWages - ActualGrossWages);
                  PR_CHECK_STATES['EE_SDI_TAX'] := RoundTwo(CalcTax - ActualTax);
                  PR_CHECK_STATES.Post;
                end;

              end;

            end;

            if CalcERSDI then
            begin
              CalculateERSDITaxes(QTREndDate, EmployeeNbr, TaxWagesList, GrossWagesList, TaxList, ALL_CHECKS, SORTED_PR, SORTED_PR_CHECK,
                SORTED_PR_CHECK_STATES, SORTED_PR_CHECK_LINES, PR_CHECK, PR_CHECK_STATES);
              for I := 0 to TaxWagesList.Count - 1 do
              begin
                aName := TaxWagesList.Names[I];
                TaxWages := StrToFloat(TaxWagesList.Values[aName]);
                if GrossWagesList.IndexOfName(aName) = -1 then
                  GrossWages := 0
                else
                  GrossWages := StrToFloat(GrossWagesList.Values[aName]);
                if TaxList.IndexOfName(aName) = -1 then
                  Tax := 0
                else
                  Tax := StrToFloat(TaxList.Values[aName]);
                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin
                  CreateCheck;
                  if not PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([PR_CHECK['PR_CHECK_NBR'], Evo.GetEeStatesNbr(EmployeeNbr, aName, CheckDate)]), []) then
                  begin
                    PR_CHECK_STATES.Insert;
                    PR_CHECK_STATES['EE_STATES_NBR'] := Evo.GetEeStatesNbr(EmployeeNbr, aName, CheckDate);
                    PR_CHECK_STATES['TAX_AT_SUPPLEMENTAL_RATE'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_STATE'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_SDI'] := 'N';
                    PR_CHECK_STATES['EXCLUDE_SUI'] := 'N';
                    PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
                    PR_CHECK_STATES['EXCLUDE_ADDITIONAL_STATE'] := 'N';
                    PR_CHECK_STATES.Post;
                  end;
                  PR_CHECK_STATES.Edit;
                  PR_CHECK_STATES['ER_SDI_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                  PR_CHECK_STATES['ER_SDI_GROSS_WAGES'] := RoundTwo(GrossWages);
                  PR_CHECK_STATES['ER_SDI_TAX'] := RoundTwo(Tax);
                  PR_CHECK_STATES.Post;

                  if ReportFilePath <> '' then
                    Writeln(LogFile, CoDateStamp + aName + ' ER SDI: #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' +
                      FloatToStrF(TaxWages, ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(0, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax,
                      ffCurrency, 10, 2));
                end;
              end;
            end;

            if CalcSUI then
            begin
              SuiList := Person.GetSuiList(EmployeeNbr, GetBeginYear(QTREndDate), QTREndDate);

              for i := 0 to SuiList.Count - 1 do
              begin
                if GetMonth(GetBeginQuarter(QTREndDate)) = 1 then
                begin
                  CalcTaxWages   := Person.CalcSuiTaxableWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate, DebugList);
                  ActualTaxWages := Person.SuiTaxableWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);

                  CalcGrossWages := Person.CalcSuiGrossWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);
                  ActualGrossWages := Person.SuiGrossWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);

                  CalcTax := Person.CalcSuiTax(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);
                  ActualTax := Person.SuiTax(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);

                  TaxWages := CalcTaxWages - ActualTaxWages;
                  GrossWages := CalcGrossWages - ActualGrossWages;
                  Tax := RoundTwo(CalcTax - ActualTax);
                end
                else
                begin
                  TmpCheckDate := IncDay(GetBeginQuarter(QTREndDate), -1); // End of previous quarter

                  CalcTaxWages   := Person.CalcSuiTaxableWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), TmpCheckDate, DebugList);
                  ActualTaxWages := Person.SuiTaxableWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), TmpCheckDate);

                  CalcGrossWages := Person.CalcSuiGrossWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualGrossWages := Person.SuiGrossWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), TmpCheckDate);

                  CalcTax := Person.CalcSuiTax(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), TmpCheckDate);
                  ActualTax := Person.SuiTax(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), TmpCheckDate);

                  TaxWages := CalcTaxWages - ActualTaxWages;
                  GrossWages := CalcGrossWages - ActualGrossWages;
                  Tax := RoundTwo(CalcTax - ActualTax);

                  CalcTaxWages   := Person.CalcSuiTaxableWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate, DebugList);
                  ActualTaxWages := Person.SuiTaxableWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);

                  CalcGrossWages := Person.CalcSuiGrossWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);
                  ActualGrossWages := Person.SuiGrossWages(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);

                  CalcTax := Person.CalcSuiTax(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);
                  ActualTax := Person.SuiTax(EmployeeNbr, StrToInt(SuiList.Values[i].Name), GetBeginYear(QTREndDate), QTREndDate);

                  TaxWages := CalcTaxWages - ActualTaxWages - TaxWages;
                  GrossWages := CalcGrossWages - ActualGrossWages - GrossWages;
                  Tax := RoundTwo(CalcTax - ActualTax) - Tax;

                end;

                

                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin

                  if (Abs(Tax) < MinimumTax)  and ((RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0)) then
                    Tax := 0;

                  if (RoundTwo(TaxWages) <> 0) or (RoundTwo(GrossWages) <> 0) or ((RoundTwo(Tax) <> 0) and  (not (Abs(Tax) < MinimumTax))) then
                  begin

                    if RoundTwo(Tax) <> 0 then
                    if not Evo.IsErSui(StrToInt(SuiList.Values[i].Name), Now) then
                    begin
                      Result := False;
                      Net := Net - RoundTwo(Tax);
                    end;

                    Person.QecCheck[EmployeeNbr].AddSui(StrToInt(SuiList.Values[i].Name), RoundTwo(GrossWages), RoundTwo(TaxWages), RoundTwo(Tax));

                    CreateCheck;
                    PR_CHECK_SUI.Insert;
                    PR_CHECK_SUI['CO_SUI_NBR'] := Evo.GetCoSuiNbr(CONbr, StrToInt(SuiList.Values[i].Name), Now);
                    PR_CHECK_SUI['SUI_TAXABLE_WAGES'] := RoundTwo(TaxWages);
                    PR_CHECK_SUI['SUI_GROSS_WAGES'] := RoundTwo(GrossWages);
                    PR_CHECK_SUI['SUI_TAX'] := RoundTwo(Tax);
                    PR_CHECK_SUI.Post;

                    if ReportFilePath <> '' then
                    begin
                      Writeln(LogFile, CoDateStamp + Evo.SuiTaxName(StrToInt(SuiList.Values[i].Name), Now) + ': #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' +
                        FloatToStrF(TaxWages, ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(TaxTips, ffCurrency, 10, 2) + '; Tax ' +
                        FloatToStrF(Tax, ffCurrency, 10, 2));
                    end;

                  end;

                end;
              end;
            end;

            if CalcLocals then
            begin
              CalculateLocalTaxes(QTREndDate, EmployeeNbr, TaxWagesList, TaxList, ALL_CHECKS, LOCAL_YTD, LOCAL_YTQ, SORTED_PR, SORTED_PR_CHECK,
                SORTED_PR_CHECK_LOCALS, SORTED_PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS);

              ctx_DataAccess.SQLite.Execute(' update QEC_LIABILITIES set'+
              '  LOCAL_TAX=round(LOCAL_TAX,2),'+
              '  LOCAL_GROSS_WAGES=round(LOCAL_GROSS_WAGES,2),'+
              '  LOCAL_TAXABLE_WAGES=round(LOCAL_TAXABLE_WAGES,2)');

              // remove PA local taxes from the list
              for i := TaxWagesList.Count - 1 downto 0 do
              begin
                PaStored := ctx_DataAccess.SQLite.Select(
                  ' select 1 '+
                  ' from SY_LOCALS x '+
                  ' join SY_STATES s on s.CHECK_DATE=x.CHECK_DATE and s.SY_STATES_NBR=x.SY_STATES_NBR '+
                  ' where s.STATE=''PA'' and x.LOCAL_TYPE in (''R'',''N'',''S'') '+
                  ' and x.SY_LOCALS_NBR='+TaxWagesList.Names[I], []);
                if PaStored.RecordCount > 0 then
                  TaxWagesList.Delete(I);
              end;

              for i := TaxList.Count - 1 downto 0 do
              begin
                PaStored := ctx_DataAccess.SQLite.Select(
                  ' select 1 '+
                  ' from SY_LOCALS x '+
                  ' join SY_STATES s on s.CHECK_DATE=x.CHECK_DATE and s.SY_STATES_NBR=x.SY_STATES_NBR '+
                  ' where s.STATE=''PA'' and x.LOCAL_TYPE in (''R'',''N'',''S'') '+
                  ' and x.SY_LOCALS_NBR='+TaxList.Names[I], []);
                if PaStored.RecordCount > 0 then
                  TaxList.Delete(I);
              end;

              PaStored := ctx_DataAccess.SQLite.Select('select * from NEW_QEC_LOCATIONS where EE_NBR='+EE.FieldByName('EE_NBR').AsString, []);

              PaStored.First;
              while not PaStored.Eof do
              begin
                if Abs(PaStored.FieldByName('LOCAL_TAX').AsFloat) < MinimumTax then
                begin
                  PaStored.Edit;
                  PaStored.FieldByName('LOCAL_TAX').AsFloat := 0;
                  PaStored.Post;
                end;

                if Abs(PaStored.FieldByName('LOCAL_TAX').AsFloat) > 0.005 then
                  Result := False;

                if (Abs(PaStored.FieldByName('LOCAL_TAX').AsFloat) > 0.005)
                or (Abs(PaStored.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat) > 0.005)
                or (Abs(PaStored.FieldByName('LOCAL_GROSS_WAGES').AsFloat) > 0.005) then
                begin
                  CreateCheck;
                  DM_PAYROLL.PR_CHECK_LOCALS.Insert;
                  DM_PAYROLL.PR_CHECK_LOCALS.PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
                  DM_PAYROLL.PR_CHECK_LOCALS.PR_NBR.Value := DM_PAYROLL.PR_CHECK['PR_NBR'];
                  DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.Value := PaStored['EE_LOCALS_NBR'];
                  DM_PAYROLL.PR_CHECK_LOCALS.EXCLUDE_LOCAL.Value := 'N';
                  DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_GROSS_WAGES.Value := PaStored.FieldByName('LOCAL_GROSS_WAGES').AsFloat;
                  DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_TAXABLE_WAGE.Value := PaStored.FieldByName('LOCAL_TAXABLE_WAGES').AsFloat;
                  DM_PAYROLL.PR_CHECK_LOCALS.LOCAL_TAX.Value := PaStored.FieldByName('LOCAL_TAX').AsFloat;
                  if (PaStored.FieldByName('CO_LOCATIONS_NBR').AsString <> '') and (PaStored.FieldByName('CO_LOCATIONS_NBR').AsInteger <> 0) then
                    DM_PAYROLL.PR_CHECK_LOCALS.CO_LOCATIONS_NBR.Value := PaStored['CO_LOCATIONS_NBR'];
                  if (PaStored.FieldByName('NONRES_EE_LOCALS_NBR').AsString <> '') and (PaStored.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger <> 0) then
                    DM_PAYROLL.PR_CHECK_LOCALS.NONRES_EE_LOCALS_NBR.Value := PaStored['NONRES_EE_LOCALS_NBR'];
                  DM_PAYROLL.PR_CHECK_LOCALS.REPORTABLE.AsString := PaStored['REPORTABLE'];
                  DM_PAYROLL.PR_CHECK_LOCALS.Post;
                end;
                PaStored.Next;
              end;

              for I := 0 to TaxWagesList.Count - 1 do
              begin
                aName := TaxWagesList.Names[I];
                TaxWages := StrToFloat(TaxWagesList.Values[aName]);
                if TaxList.IndexOfName(aName) = -1 then
                  Tax := 0
                else
                  Tax := StrToFloat(TaxList.Values[aName]);
                TempNbr := 0;
                if (TaxWages <> 0) or (Tax <> 0) then
                begin
                  TempNbr := Evo.CoLocalTaxNbr(CONbr, StrToInt(aName), Now);
                  if (Tax <> 0) and (not Evo.CoLocalSelfAdjustTax(TempNbr, Now)) then
                    Tax := 0;
                end;

                if Abs(Tax) < MinimumTax then
                  Tax := 0;

                if (RoundTwo(TaxWages) <> 0) or (RoundTwo(Tax) <> 0) then
                begin
                  if Tax <> 0 then
                  if Evo.IsEmployeeLocalTax(StrToInt(aName), Now) then
                  begin
                    Result := False;
                    Net := Net - RoundTwo(Tax);
                  end;

                  CreateCheck;

                  PR_CHECK_LOCALS.Insert;
                  PR_CHECK_LOCALS['EE_LOCALS_NBR'] := Evo.GetEeLocalsNbr(EmployeeNbr, TempNbr, Now);
                  PR_CHECK_LOCALS['EXCLUDE_LOCAL'] := 'N';
                  PR_CHECK_LOCALS['LOCAL_TAXABLE_WAGE'] := RoundTwo(TaxWages);
                  PR_CHECK_LOCALS['LOCAL_GROSS_WAGES'] := RoundTwo(TaxWages);
                  PR_CHECK_LOCALS['LOCAL_TAX'] := RoundTwo(Tax);
                  PR_CHECK_LOCALS.Post;

                  if ReportFilePath <> '' then
                    Writeln(LogFile, CoDateStamp + Evo.SyLocalName(StrToInt(aName), Now) + ': #' + Trim(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString) + '  Tax Wages ' +
                      FloatToStrF(TaxWages, ffCurrency, 10, 2) + '; Tax Tips ' + FloatToStrF(0, ffCurrency, 10, 2) + '; Tax ' + FloatToStrF(Tax,
                      ffCurrency, 10, 2));
                end;
              end;

            end;

            if RoundTwo(Net) <> 0 then
            begin
              CreateCheck;
              PR_CHECK.Edit;
              PR_CHECK['NET_WAGES'] := Net;
              PR_CHECK.Post;
            end;

            EE.Next;
          except
            on E: Exception do
            begin
              E.Message := E.Message + ' on EE #' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ''));
              raise;
            end;
          end;
        end;
        if PrintReports and (DM_PAYROLL.PR.RecordCount <> 0) then
        begin
          DM_PAYROLL.PR.Edit;
          DM_PAYROLL.PR['EXCLUDE_R_C_B_0R_N'] := GROUP_BOX_CHECKS;
          DM_PAYROLL.PR.Post;
          ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
        end;
        MyTransactionManager.ApplyUpdates;
      finally
        MyTransactionManager.Free;
        TaxWagesList.Free;
        GrossWagesList.Free;
        TaxList.Free;
        ALL_CHECKS.Free;
        SORTED_PR_CHECK_LINES.Free;
        SORTED_PR_CHECK_LINES_FOR_FUI.Free;
        SORTED_PR.Free;
        SORTED_PR_CHECK.Free;
        ALL_CHECKS_FOR_FUI.Free;
        SORTED_PR_CHECK_STATES.Free;
        SORTED_PR_CHECK_LOCALS.Free;
        FEDERAL_YTD.Free;
        STATE_YTD.Free;
        LOCAL_YTD.Free;
        LOCAL_YTQ.Free;
        CLPersonTaxes.Free;
      end;
    finally
      try
        UnlockResource(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + IntToStr(CONbr), LockType);
      finally
        UnlockResource(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), LockType);
      end;  
    end;
  finally
    ctx_PayrollCalculation.ProcessFeedback(False);
    if ReportFilePath <> '' then
      Close(LogFile);
  end;

  if ((CleanupAction = cpaForceProcess) and (DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger <> 0)) or ((CleanupAction = cpaProcess) and Result and (DM_PAYROLL.PR.RecordCount <> 0)) then
  begin
    CheckReturnQueue := True;
    if not VarIsNull(ExtraOptions) and (VarArrayHighBound(ExtraOptions, 1) > 11) and (ExtraOptions[12] = 'N') then
      CheckReturnQueue := False;
    Warnings := ctx_PayrollProcessing.ProcessPayroll(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger, False, Log, CheckReturnQueue, LockType);
  end;

  if ((CleanupAction = cpaNone) or ((CleanupAction in [cpaQueue, cpaProcess]) and not Result)) and (DM_PAYROLL.PR.RecordCount <> 0) then
  begin
    DM_PAYROLL.PR.Edit;
    DM_PAYROLL.PR['STATUS'] := PAYROLL_STATUS_PENDING;
    DM_PAYROLL.PR.Post;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
  end;

  DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(CONbr));
end;

end.


