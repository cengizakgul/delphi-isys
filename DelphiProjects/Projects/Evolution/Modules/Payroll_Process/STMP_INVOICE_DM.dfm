object INVOICE_DM: TINVOICE_DM
  OldCreateOrder = True
  Left = 361
  Top = 306
  Height = 479
  Width = 741
  object CO_INVOICE_MASTER: TevClientDataSet
    OnCalcFields = CO_INVOICE_MASTERCalcFields
    Left = 62
    Top = 54
    object CO_INVOICE_MASTERCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object CO_INVOICE_MASTERCL_BILLING_NBR: TIntegerField
      FieldName = 'CL_BILLING_NBR'
    end
    object CO_INVOICE_MASTERCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CO_INVOICE_MASTERCO_BILLING_HISTORY_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_NBR'
    end
    object CO_INVOICE_MASTERNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object CO_INVOICE_MASTERADDRESS1: TStringField
      FieldName = 'ADDRESS1'
      Size = 30
    end
    object CO_INVOICE_MASTERADDRESS2: TStringField
      FieldName = 'ADDRESS2'
      Size = 30
    end
    object CO_INVOICE_MASTERCITY: TStringField
      FieldName = 'CITY'
    end
    object CO_INVOICE_MASTERSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object CO_INVOICE_MASTERZIP_CODE: TStringField
      FieldName = 'ZIP_CODE'
      Size = 10
    end
    object CO_INVOICE_MASTERINVOICE_NUMBER: TIntegerField
      FieldName = 'INVOICE_NUMBER'
    end
    object CO_INVOICE_MASTERINVOICE_DATE: TDateField
      FieldName = 'INVOICE_DATE'
    end
    object CO_INVOICE_MASTERCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object CO_INVOICE_MASTERTERMS: TStringField
      FieldName = 'TERMS'
      Size = 30
    end
    object CO_INVOICE_MASTERCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object CO_INVOICE_MASTERCO_NAME: TStringField
      FieldName = 'CO_NAME'
      Size = 40
    end
    object CO_INVOICE_MASTERPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object CO_INVOICE_MASTERAddressPrintLine1: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'AddressPrintLine1'
      Size = 50
    end
    object CO_INVOICE_MASTERAddressPrintLine2: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'AddressPrintLine2'
      Size = 50
    end
    object CO_INVOICE_MASTERAddressPrintLine3: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'AddressPrintLine3'
      Size = 50
    end
    object CO_INVOICE_MASTERINVOICE_AMOUNT: TCurrencyField
      FieldName = 'INVOICE_AMOUNT'
    end
    object CO_INVOICE_MASTERINVOICE_DISCOUNT: TCurrencyField
      FieldName = 'INVOICE_DISCOUNT'
    end
    object CO_INVOICE_MASTERINVOICE_TAX: TCurrencyField
      FieldName = 'INVOICE_TAX'
    end
    object CO_INVOICE_MASTERINVOICE_TOTAL: TCurrencyField
      FieldName = 'INVOICE_TOTAL'
    end
    object CO_INVOICE_MASTERCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object CO_INVOICE_MASTERCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object CO_INVOICE_MASTERCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object CO_INVOICE_MASTERCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object CO_INVOICE_MASTERNOTES: TMemoField
      FieldName = 'NOTES'
      BlobType = ftMemo
    end
    object CO_INVOICE_MASTERFILLER: TStringField
      FieldName = 'FILLER'
      Size = 512
    end
  end
  object CO_INVOICE_DETAIL: TevClientDataSet
    BeforePost = CO_INVOICE_DETAILBeforePost
    Left = 64
    Top = 104
    object CO_INVOICE_DETAILCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object CO_INVOICE_DETAILCO_BILLING_HISTORY_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_NBR'
    end
    object CO_INVOICE_DETAILCO_BILLING_HISTORY_DETAIL_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_DETAIL_NBR'
    end
    object CO_INVOICE_DETAIL_SOURCENAME: TStringField
      FieldName = 'NAME'
      Size = 60
    end
    object CO_INVOICE_DETAIL_SOURCEQUANTITY: TFloatField
      FieldName = 'QUANTITY'
    end
    object CO_INVOICE_DETAIL_SOURCECOST: TCurrencyField
      FieldName = 'AMOUNT'
    end
    object CO_INVOICE_DETAIL_SOURCEDISCOUNT: TCurrencyField
      FieldName = 'DISCOUNT'
    end
    object CO_INVOICE_DETAIL_SOURCETAX: TCurrencyField
      FieldName = 'TAX'
    end
    object CO_INVOICE_DETAIL_SOURCETOTAL: TCurrencyField
      FieldName = 'TOTAL'
    end
    object CO_INVOICE_DETAILCO_SERVICES_NBR: TIntegerField
      FieldName = 'CO_SERVICES_NBR'
    end
    object CO_INVOICE_DETAILSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
    end
  end
  object cdsDBDT: TevClientDataSet
    Left = 210
    Top = 57
  end
  object cdsTaxDistr: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    IndexName = 'DBDT'
    AggregatesActive = True
    IndexDefs = <
      item
        Name = 'DBDT'
        Fields = 'CO_DIVISION_NBR;CO_BRANCH_NBR;CO_DEPARTMENT_NBR;CO_TEAM_NBR'
        GroupingLevel = 4
      end>
    Left = 237
    Top = 111
    object cdsTaxDistrTAX: TFloatField
      FieldName = 'TAX'
      currency = True
    end
    object cdsTaxDistrCO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object cdsTaxDistrCO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object cdsTaxDistrCO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object cdsTaxDistrCO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object cdsTaxDistrPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdsTaxDistrfname: TStringField
      FieldName = 'fname'
      Size = 256
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 256
    Top = 168
  end
end
