// Copyright � 2000-2012 iSystems LLC. All rights reserved.
unit SQECLocalTaxes;

interface

uses
  controls, classes,  db, dialogs, windows, EvTypes, Variants,
  EvBasicUtils, EvContext, EvExceptions, SysUtils, EvUtils, EvConsts, SDataStructure, SQuarterEndProcessing, SPD_ProcessException, DateUtils,
  ISKbmMemDataSet, EvStreamUtils, EvCommonInterfaces, evDataset, isBaseClasses, isBAsicUtils, Math;

procedure CalculateLocalTaxesInt(const SQLite: ISQLite; const CoNbr,PrNbr: Integer; const QEndDate: TDateTime; const BeginDate: TDateTime);
procedure CalculateLocalTaxesLite(const SQLite: ISQLite; const CoNbr, PrNbr: Integer; const QEndDate: TDateTime; const Monthly: Boolean; const IgnoreSAF: Boolean);
procedure LoadData(const SQLite: ISQLite; const CoNbr, PrNbr: Integer; const QEndDate: TDateTime; const BeginDate: TDateTime);
procedure FixBlobs(const SQLite: ISQLite);
procedure FixBlobsDL(const SQLite: ISQLite);
procedure CreateIndexes(const SQLite: ISQLite);
procedure CalculateAdjustments(const SQLite: ISQLite; const CoNbr,PrNbr: Integer; const QEndDate: TDateTime; const Monthly: Boolean);

implementation

procedure CalculateLocalTaxesLite(const SQLite: ISQLite; const CoNbr, PrNbr: Integer; const QEndDate: TDateTime; const Monthly: Boolean; const IgnoreSAF: Boolean);
begin

  SQLite.StartTransaction;
  try
    LoadData(SQLite, CoNbr, PrNbr, QEndDate, GetBeginQuarter(QEndDate));
  finally
    SQLite.Commit;
  end;

  if IgnoreSAF then
    SQLite.Execute('update CO_LOCAL_TAX set SELF_ADJUST_TAX=''Y''');

  CreateIndexes(SQLite);

  SQLite.Execute(
  '  create table B as'+
  '  select PR_NBR, PR_CHECK_NBR, EE_LOCALS_NBR, SUM(LOCAL_TAX) TAX'+
  '  from BLOB'+
  '  group by PR_NBR, PR_CHECK_NBR, EE_LOCALS_NBR'+
  '  order by PR_NBR, PR_CHECK_NBR, EE_LOCALS_NBR');

  SQLite.Execute(
  '  create table C as'+
  '  select p.PR_NBR PR_NBR, l.PR_CHECK_NBR PR_CHECK_NBR, l.EE_LOCALS_NBR EE_LOCALS_NBR, SUM(l.LOCAL_TAX) TAX'+
  '  from PR_CHECK_LOCALS l'+
  '  join PR_CHECK p on p.PR_CHECK_NBR=l.PR_CHECK_NBR'+
  '  where l.EE_LOCALS_NBR in (select EE_LOCALS_NBR from EE_LOCALS)'+
  '  group by 1,2,3'+
  '  order by 1,2,3');


  SQLite.StartTransaction;
  try
    CalculateLocalTaxesInt(SQLite, CoNbr, PrNbr, QEndDate, GetBeginQuarter(QEndDate));
  finally
    SQLite.Commit;
  end;

  SQLite.StartTransaction;
  try
    FixBlobsDL(SQLite);
  finally
    SQLite.Commit;
  end;

  SQLite.StartTransaction;
  try
    FixBlobs(SQLite);
  finally
    SQLite.Commit;
  end;

  SQLite.StartTransaction;
  try
    FixBlobs(SQLite);
  finally
    SQLite.Commit;
  end;

  SQLite.StartTransaction;
  try
    CalculateAdjustments(SQLite, CoNbr, PrNbr, QEndDate, Monthly);
  finally
    SQLite.Commit;
  end;

end;

procedure CalculateLocalTaxesInt(const SQLite: ISQLite; const CoNbr,PrNbr: Integer; const QEndDate: TDateTime; const BeginDate: TDateTime);
var
  PaStored, DS, DS1: IevDataset;
  QTDWages: Double;
  EE_NBR: Integer;
  CO_LOCAL_TAX_NBR: Integer;
  Total, Wage: Double;
begin
  ctx_StartWait('Calculating PA local taxes ...');

  SQLite.Execute('create table PR_CHECK_LINES_1 as'+
    ' select z.E_D_CODE_TYPE E_D_CODE_TYPE,  ifnull(l.AMOUNT,0.0) TAXABLE_WAGE,'+
    ' l.PR_CHECK_NBR, l.PR_CHECK_LINES_NBR, l.CL_E_DS_NBR, ifnull(l.AMOUNT,0.0) AMOUNT,'+
    ' l.CO_JOBS_NBR, l.CO_TEAM_NBR, l.CO_DEPARTMENT_NBR, l.CO_BRANCH_NBR, l.CO_DIVISION_NBR,'+
    ' l.DBDT_ATTACHED, l.HOURS_OR_PIECES, l.WORK_ADDRESS_OVR'+
    ' from PR_CHECK_LINES l'+
    ' join PR_CHECK k on k.PR_CHECK_NBR=l.PR_CHECK_NBR'+
    ' join PR p on p.PR_NBR=k.PR_NBR'+
    ' join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
    ' join CL_E_DS z on z.CHECK_DATE=d.REALDATE and z.CL_E_DS_NBR=l.CL_E_DS_NBR');  

  SQLite.Execute('drop table PR_CHECK_LINES');
  SQLite.Execute('create table PR_CHECK_LINES as select * from PR_CHECK_LINES_1');
  SQLite.Execute('drop table PR_CHECK_LINES_1');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS PR_CHECK_LINES_IDX ON PR_CHECK_LINES(PR_CHECK_NBR)');

  SQLite.Execute('alter table PR_CHECK_LINES add column PR_NBR INT');
  SQLite.Execute('update PR_CHECK_LINES set PR_NBR=(select c.PR_NBR from PR_CHECK c where c.PR_CHECK_NBR=PR_CHECK_LINES.PR_CHECK_NBR)');

  SQLite.Execute('alter table PR_CHECK_LINES add column EE_NBR INT');
  SQLite.Execute('update PR_CHECK_LINES set EE_NBR=(select c.EE_NBR from PR_CHECK c where c.PR_CHECK_NBR=PR_CHECK_LINES.PR_CHECK_NBR)');

  SQLite.Execute('alter table PR_CHECK_LINES add column CHECK_DATE INT');
  SQLite.Execute('update PR_CHECK_LINES set CHECK_DATE=(select c.CHECK_DATE from PR c where c.PR_NBR=PR_CHECK_LINES.PR_NBR)');

  // list of local taxes
  SQLite.Execute( 'create table PREPARED_CHECK_LINES as'+
    '    select'+
    '      case when v.INCLUDE_IN_PRETAX=''N'' and f.E_D_CODE_TYPE is not null then ''N'''+
    '      when s.PRINT_RETURN_IF_ZERO=''Y'' and ifnull(n.EXEMPT_STATE,'''')=''E'' then ''N'''+
    '      when ifnull(y.EXEMPT,'''')=''E'' and s.PRINT_RETURN_IF_ZERO=''N'' then ''Exempt on system level'''+
    '      else ''Y'''+
    '      end DD,'+
    '      case when v.INCLUDE_IN_PRETAX=''N'' and f.E_D_CODE_TYPE is not null then ''Special code not included in pretax'''+
    '      when s.PRINT_RETURN_IF_ZERO=''Y'' and ifnull(n.EXEMPT_STATE,'''')=''E'' then ''Follow State E/D exemptions'''+
    '      when ifnull(y.EXEMPT,'''')=''E'' and s.PRINT_RETURN_IF_ZERO=''N'' then ''Exempt on system level'''+
    '      when ifnull(i.EXEMPT_EXCLUDE,'''')=''E'' then ''Exempt on client level'''+
    '      when ifnull(q.EXEMPT_EXCLUDE,'''')=''E'' then ''Exempt on check line level'''+
    '      when ifnull(q.EXEMPT_EXCLUDE,'''')=''X'' then ''Blocked in payroll'''+
    '      when ifnull(v.EXEMPT_EXCLUDE,'''')=''E'' then ''Exempt on EE level''' +
    '      when v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null'+
    '             and ifnull(l.CO_JOBS_NBR,-1) <> ifnull(j.CO_JOBS_NBR,-2)'+
    '             and ifnull(l.CO_TEAM_NBR,-1) <> ifnull(a.CO_TEAM_NBR,-2)'+
    '             and ifnull(l.CO_DEPARTMENT_NBR,-1) <> ifnull(b.CO_DEPARTMENT_NBR,-2)'+
    '             and ifnull(l.CO_BRANCH_NBR,-1) <> ifnull(g.CO_BRANCH_NBR,-2)'+
    '             and ifnull(l.CO_DIVISION_NBR,-1) <> ifnull(w.CO_DIVISION_NBR,-2) then ''Deduct=Never, no check line overrides, no local on DBDT'''+
    '      when v.DEDUCT=''D'' and q.EXEMPT_EXCLUDE is not null then ''No overrides'''+
    '      else '''''+
    '      end COMMENT,'+
    '      s.PRINT_RETURN_IF_ZERO,'+
    '      n.EXEMPT_STATE,'+
    '      case '+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''Y'' and ifnull(l.WORK_ADDRESS_OVR,'''') = ''Y'' then 0'+    
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''Y'' and q.EXEMPT_EXCLUDE is null and j.CO_JOBS_NBR is not null then 1'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''Y'' and q.EXEMPT_EXCLUDE is null and a.CO_TEAM_NBR is not null then 2'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''Y'' and q.EXEMPT_EXCLUDE is null and b.CO_DEPARTMENT_NBR is not null then 3'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''Y'' and q.EXEMPT_EXCLUDE is null and g.CO_BRANCH_NBR is not null then 4'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''Y'' and q.EXEMPT_EXCLUDE is null and w.CO_DIVISION_NBR is not null then 5'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and j.CO_JOBS_NBR is not null then 6'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and a.CO_TEAM_NBR is not null then 7'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and b.CO_DEPARTMENT_NBR is not null then 8'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and g.CO_BRANCH_NBR is not null then 9'+
    '      when ifnull(v.WORK_ADDRESS_OVR,'''') = ''N'' and v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and w.CO_DIVISION_NBR is not null then 10'+
    '      else 11 end NONRES_PRIORITY,'+
    '      p.LAST_NAME,'+
    '      case when s.LOCAL_TYPE=''C'' and h.STATE=''PA'' then ''Y'' else ''N'' end PAOPT,'+
    '      case when s.PAY_WITH_STATE=''Y'' then ''Y'' else ''N'' end MONTHLYMAX,'+
    '      case '+
    '      when v.INCLUDE_IN_PRETAX=''N'' and f.E_D_CODE_TYPE is not null then ''Y'''+
    '      when s.PRINT_RETURN_IF_ZERO=''Y'' and ifnull(n.EXEMPT_STATE,'''')=''E'' then ''Y'''+
    '      when ifnull(y.EXEMPT,'''')=''E'' and s.PRINT_RETURN_IF_ZERO=''N'' then ''Y'''+
    '      when l.E_D_CODE_TYPE not like ''D%'' and ifnull(i.EXEMPT_EXCLUDE,'''')=''E'' then ''Y'''+
    '      when l.E_D_CODE_TYPE not like ''D%'' and ifnull(q.EXEMPT_EXCLUDE,'''')=''E'' then ''Y'''+
    '      when l.E_D_CODE_TYPE not like ''D%'' and ifnull(v.EXEMPT_EXCLUDE,'''')=''E'' then ''Y'''+
    '      when l.E_D_CODE_TYPE not like ''D%'' and v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null'+
    '             and ifnull(l.CO_JOBS_NBR,-1) <> ifnull(j.CO_JOBS_NBR,-2)'+
    '             and ifnull(l.CO_TEAM_NBR,-1) <> ifnull(a.CO_TEAM_NBR,-2)'+
    '             and ifnull(l.CO_DEPARTMENT_NBR,-1) <> ifnull(b.CO_DEPARTMENT_NBR,-2)'+
    '             and ifnull(l.CO_BRANCH_NBR,-1) <> ifnull(g.CO_BRANCH_NBR,-2)'+
    '             and ifnull(l.CO_DIVISION_NBR,-1) <> ifnull(w.CO_DIVISION_NBR,-2) then ''Y'''+
    '      when l.E_D_CODE_TYPE not like ''D%'' and v.DEDUCT=''D'' and q.EXEMPT_EXCLUDE is not null then ''Y'''+
    '      else ''N'''+
    '      end DELETED,'+
    '      e.EE_NBR,'+
    '      r.PR_NBR,'+
    '      c.PR_CHECK_NBR,'+
    '      l.PR_CHECK_LINES_NBR,'+
    '      v.CO_LOCAL_TAX_NBR,'+
    '      v.EE_LOCALS_NBR,'+
    '      h.STATE,'+
    '      case when h.STATE=''PA'' and (s.LOCAL_TYPE=''S'' or s.LOCAL_TYPE=''R'') then ''Y'''+
    '      when v.WORK_ADDRESS_OVR=''Y'' then ''N'''+
    '      when h.STATE=''PA'' and s.LOCAL_TYPE=''N'' then ''N'''+
    '      else '''' end ISRES,'+
    '      case'+
    '      when v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and j.CO_JOBS_NBR is not null then l.DBDT_ATTACHED'+
    '      when v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and a.CO_TEAM_NBR is not null then l.DBDT_ATTACHED'+
    '      when v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and b.CO_DEPARTMENT_NBR is not null then l.DBDT_ATTACHED'+
    '      when v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and g.CO_BRANCH_NBR is not null then l.DBDT_ATTACHED'+
    '      when v.DEDUCT=''N'' and q.EXEMPT_EXCLUDE is null and w.CO_DIVISION_NBR is not null then l.DBDT_ATTACHED'+
    '      else ''N'' end DBDT_ATTACHED,'+

    '      ifnull(q.EXEMPT_EXCLUDE,'''') EXEMPT_EXCLUDE,'+
    '      case '+
    '      when l.WORK_ADDRESS_OVR=''Y'' then ''E'' '+
    '      when x5.CO_LOCATIONS_NBR is not null then ''5'''+
    '      when x4.CO_LOCATIONS_NBR is not null then ''4'''+
    '      when x3.CO_LOCATIONS_NBR is not null then ''3'''+
    '      when x2.CO_LOCATIONS_NBR is not null then ''2'''+
    '      when x1.CO_LOCATIONS_NBR is not null then ''1'''+
    '      else ''C'' end LOCATION_LEVEL,'+
    
    '      s.NAME,'+
    '      cast('+
    '      case'+
    '      when v.OVERRIDE_LOCAL_TAX_TYPE=''P'' then v.OVERRIDE_LOCAL_TAX_VALUE'+
    '      when t.TAX_RATE is null then s.TAX_RATE'+
    '      else t.TAX_RATE'+
    '      end as REAL) TAX_RATE,'+
    '      cast(case when v.PERCENTAGE_OF_TAX_WAGES is null then cast(l.TAXABLE_WAGE as REAL) else cast(l.TAXABLE_WAGE as REAL) * cast(v.PERCENTAGE_OF_TAX_WAGES as REAL)/100 end as REAL) TAXABLE_WAGE ,'+
    '      l.E_D_CODE_TYPE,'+
    '      v.DEDUCT, l.CO_JOBS_NBR, l.CO_TEAM_NBR, l.CO_DEPARTMENT_NBR, l.CO_BRANCH_NBR, l.CO_DIVISION_NBR'+
    '    from EE e'+
    '    join CL_PERSON p on p.CL_PERSON_NBR=e.CL_PERSON_NBR'+
    '    join PR_CHECK c on c.EE_NBR=e.EE_NBR'+
    '    join PR r on r.PR_NBR=c.PR_NBR '+ 
    '    join PR_CHECK_LINES l on l.PR_CHECK_NBR=c.PR_CHECK_NBR'+
    '    join CHECK_DATES d on d.CHECK_DATE=r.CHECK_DATE'+
    '    join CO k on k.CO_NBR=e.CO_NBR and k.CHECK_DATE=d.REALDATE'+
    '    join EE_LOCALS v on v.CHECK_DATE=d.REALDATE and v.EE_NBR=e.EE_NBR'+
    '    join CO_LOCAL_TAX t on t.CHECK_DATE=d.REALDATE and t.CO_LOCAL_TAX_NBR=v.CO_LOCAL_TAX_NBR'+
    '    join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=t.SY_LOCALS_NBR'+
    '    join SY_STATES h on h.CHECK_DATE=d.REALDATE and h.SY_STATES_NBR=s.SY_STATES_NBR'+
    '    join TAXABLE_TYPES u on u.E_D_CODE_TYPE=l.E_D_CODE_TYPE'+
    '    left join SPECIAL_TYPES f on f.E_D_CODE_TYPE=l.E_D_CODE_TYPE'+
    '    left join SY_STATE_EXEMPTIONS n on n.CHECK_DATE=d.REALDATE and n.SY_STATES_NBR=s.SY_STATES_NBR and n.E_D_CODE_TYPE=l.E_D_CODE_TYPE'+
    '    left join SY_LOCAL_EXEMPTIONS y on y.CHECK_DATE=d.REALDATE and y.SY_LOCALS_NBR=t.SY_LOCALS_NBR and y.E_D_CODE_TYPE=l.E_D_CODE_TYPE'+
    '    left join CL_E_D_LOCAL_EXMPT_EXCLD i on i.CHECK_DATE=d.REALDATE and i.CL_E_DS_NBR=l.CL_E_DS_NBR and i.SY_LOCALS_NBR=s.SY_LOCALS_NBR'+
    '    left join PR_CHECK_LINE_LOCALS q on q.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR and q.EE_LOCALS_NBR=v.EE_LOCALS_NBR and ifnull(q.EXEMPT_EXCLUDE,'''') <> ''C'''+
    '    left join CO_JOBS_LOCALS j on j.CHECK_DATE=d.REALDATE and j.CO_JOBS_NBR=l.CO_JOBS_NBR  and j.CO_LOCAL_TAX_NBR=t.CO_LOCAL_TAX_NBR'+
    '    left join CO_TEAM_LOCALS a on a.CHECK_DATE=d.REALDATE and a.CO_TEAM_NBR=l.CO_TEAM_NBR and a.CO_LOCAL_TAX_NBR=t.CO_LOCAL_TAX_NBR'+
    '    left join CO_DEPARTMENT_LOCALS b on b.CHECK_DATE=d.REALDATE and b.CO_DEPARTMENT_NBR=l.CO_DEPARTMENT_NBR and b.CO_LOCAL_TAX_NBR=t.CO_LOCAL_TAX_NBR'+
    '    left join CO_BRANCH_LOCALS g on g.CHECK_DATE=d.REALDATE and g.CO_BRANCH_NBR=l.CO_BRANCH_NBR and g.CO_LOCAL_TAX_NBR=t.CO_LOCAL_TAX_NBR'+
    '    left join CO_JOBS x5 on x5.CO_JOBS_NBR=l.CO_JOBS_NBR'+
    '    left join CO_TEAM x4 on x4.CO_TEAM_NBR=l.CO_TEAM_NBR'+
    '    left join CO_DEPARTMENT x3 on x3.CO_DEPARTMENT_NBR=l.CO_DEPARTMENT_NBR'+
    '    left join CO_BRANCH x2 on x2.CO_BRANCH_NBR=l.CO_BRANCH_NBR'+
    '    left join CO_DIVISION x1 on x1.CO_DIVISION_NBR=l.CO_DIVISION_NBR'+
    '    left join CO_DIVISION_LOCALS w on w.CHECK_DATE=d.REALDATE and w.CO_DIVISION_NBR=l.CO_DIVISION_NBR and w.CO_LOCAL_TAX_NBR=t.CO_LOCAL_TAX_NBR'+
    '    where r.PAYROLL_TYPE<>'''+PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT+''''
    );

  SQLite.Execute('create table DEDUCTIONS as'+
    '  select l.*'+
    '  from PREPARED_CHECK_LINES l'+
    '  where E_D_CODE_TYPE like ''D%'' and DELETED=''N'' ');

  SQLite.Execute('CREATE INDEX IF NOT EXISTS DEDUCTIONS_IDX ON DEDUCTIONS(PR_CHECK_NBR)');

  SQLite.Execute(
    '  delete from PREPARED_CHECK_LINES'+
    '  where E_D_CODE_TYPE like ''D%''');

  SQLite.Execute('delete from BLOB'+
    '  where (select max(s.LOCAL_TYPE) from SY_LOCALS s where s.SY_LOCALS_NBR='+
    '    (select max(c.SY_LOCALS_NBR) from CO_LOCAL_TAX c'+
    '    where c.CO_LOCAL_TAX_NBR=BLOB.CO_LOCAL_TAX_NBR)) not in (''R'',''N'',''S'')');

  SQLite.Execute('delete'+
    '  from BLOB'+
    '  where ifnull(BLOB.NONRES_EE_LOCALS_NBR,0)=0'+
    '  and exists(select 1 from BLOB b'+
    '       where b.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR'+
    '       and b.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
    '       and ifnull(b.NONRES_EE_LOCALS_NBR,0)<>0)');

  SQLite.Execute('insert into BLOB'+
    '  select'+
    '  c.PR_NBR PR_NBR,'+
    '  l.PR_CHECK_NBR PR_CHECK_NBR,'+
    '  (select MIN(q.PR_CHECK_LINES_NBR)'+
    '  from PREPARED_CHECK_LINES q'+
    '  where q.PR_CHECK_NBR=l.PR_CHECK_NBR and q.EE_LOCALS_NBR=l.EE_LOCALS_NBR) PR_CHECK_LINES_NBR,'+
    '  l.EE_LOCALS_NBR EE_LOCALS_NBR,'+
    '  e.CO_LOCAL_TAX_NBR CO_LOCAL_TAX_NBR,'+
    '  ''Y'' CALCULATE,'+
    '  case when s.LOCAL_TYPE=''R'' then ''Y'' else ''N'' end ISRES,'+
    '  ''N'' DBDT_ATTACHED,'+
    '  ''N'' BLOCKED,'+
    '  ''N'' ISLOSER,'+
    '  ''N'' EXCLUDED,'+
    '  (select MIN(q.EE_LOCALS_NBR)'+
    '  from PREPARED_CHECK_LINES q'+
    '  where q.PR_CHECK_NBR=l.PR_CHECK_NBR and q.ISRES=''N'') NONRES_EE_LOCALS_NBR,'+
    '  0 DIVISION_NBR,'+
    '  0 BRANCH_NBR,'+
    '  0 DEPARTMENT_NBR,'+
    '  0 TEAM_NBR,'+
    '  0 JOBS_NBR,'+
    '  ''C'' LOCATION_LEVEL,'+
    '  l.LOCAL_GROSS_WAGES AMOUNT,'+
    '  0.0 RATE,'+
    '  l.LOCAL_GROSS_WAGES LOCAL_GROSS_WAGES,'+
    '  l.LOCAL_TAXABLE_WAGE LOCAL_TAXABLE_WAGES,'+
    '  l.LOCAL_TAX LOCAL_TAX'+
    '  from PR_CHECK_LOCALS l'+
    '  join PR_CHECK c on c.PR_CHECK_NBR=l.PR_CHECK_NBR'+
    '  join PR p on p.PR_NBR=c.PR_NBR'+
    '  join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
    '  join EE_LOCALS e on e.EE_LOCALS_NBR=l.EE_LOCALS_NBR and e.CHECK_DATE=d.REALDATE'+
    '  join CO_LOCAL_TAX x on x.CO_LOCAL_TAX_NBR=e.CO_LOCAL_TAX_NBR and x.CHECK_DATE=d.REALDATE'+
    '  join SY_LOCALS s on s.SY_LOCALS_NBR=x.SY_LOCALS_NBR and s.CHECK_DATE=d.REALDATE'+
    '  where s.LOCAL_TYPE in (''R'',''N'',''S'')'+
    '  and not exists(select EE_LOCALS_NBR from BLOB b'+
    '  where b.EE_LOCALS_NBR=l.EE_LOCALS_NBR and b.PR_CHECK_NBR=l.PR_CHECK_NBR)');

  SQLite.Execute('create table CHECK_WAGES as'+
    ' select x.PR_CHECK_NBR, x.CO_LOCAL_TAX_NBR,'+
    ' ifnull((select SUM(v.TAXABLE_WAGE) from PREPARED_CHECK_LINES v where v.PR_CHECK_NBR=x.PR_CHECK_NBR and v.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR and v.DD=''Y''),0.0) AMOUNT,'+
    ' ifnull((select SUM(v.TAXABLE_WAGE) from PREPARED_CHECK_LINES v where v.PR_CHECK_NBR=x.PR_CHECK_NBR and v.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR and v.DD=''Y''),0.0) -'+
    ' (select SUM(d.TAXABLE_WAGE) from DEDUCTIONS d where d.PR_CHECK_NBR=x.PR_CHECK_NBR and d.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR) TAXABLE_WAGE'+
    ' from (select distinct PR_CHECK_NBR, CO_LOCAL_TAX_NBR from DEDUCTIONS where DELETED=''N'') x');

  DS := SQLite.Select('select * from CHECK_WAGES where abs(AMOUNT) > 0.005',[]);
  DS.First;
  while not DS.Eof do
  begin
    if Abs(DS['TAXABLE_WAGE']) > 0.0005 then
    begin
      Total := DS['TAXABLE_WAGE'];
      DS1 := SQLite.Select(
        'select * from PREPARED_CHECK_LINES where DD=''Y'' and PR_CHECK_NBR=:PR_CHECK_NBR and CO_LOCAL_TAX_NBR=:CO_LOCAL_TAX_NBR order by TAXABLE_WAGE',
        [DS['PR_CHECK_NBR'], DS['CO_LOCAL_TAX_NBR']]);
      DS1.First;
      while not DS1.Eof do
      begin
        Wage := RoundTwo(DS1['TAXABLE_WAGE']/DS['AMOUNT']*DS['TAXABLE_WAGE']);
        Total := Total - Wage;
        SQLite.Execute(' update PREPARED_CHECK_LINES set TAXABLE_WAGE='+FloatToStr(Wage)+
                                      ' where PR_CHECK_LINES_NBR='+DS1.FieldByName('PR_CHECK_LINES_NBR').AsString+
                                      ' and CO_LOCAL_TAX_NBR='+DS.FieldByName('CO_LOCAL_TAX_NBR').AsString);
        DS1.Next;
      end;
      if Abs(Total) > 0.005 then
        SQLite.Execute(' update PREPARED_CHECK_LINES set TAXABLE_WAGE=TAXABLE_WAGE+'+FloatToStr(RoundTwo(Total))+
                                      ' where PR_CHECK_LINES_NBR='+DS1.FieldByName('PR_CHECK_LINES_NBR').AsString+
                                      ' and CO_LOCAL_TAX_NBR='+DS.FieldByName('CO_LOCAL_TAX_NBR').AsString);
    end
    else
      SQLite.Execute('update PREPARED_CHECK_LINES set TAXABLE_WAGE=0.0 where PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+
                                    ' and CO_LOCAL_TAX_NBR='+DS.FieldByName('CO_LOCAL_TAX_NBR').AsString);
    DS.Next;
  end;

  SQLite.Execute(' create table PA_NONRES_LOCALS as '+
    '    select *'+
    '    from PREPARED_CHECK_LINES l'+
    '    where l.DELETED=''N'' and l.ISRES=''N''');

  SQLite.Execute(' create table PA_NONRES_LOCALS_1 as'+
    '    select distinct l.PR_CHECK_LINES_NBR'+
    '    from PREPARED_CHECK_LINES l'+
    '    where l.PR_CHECK_LINES_NBR not in (select PR_CHECK_LINES_NBR from PA_NONRES_LOCALS)');

  SQLite.Execute(' insert into PA_NONRES_LOCALS'+
    '    select l.*'+
    '    from PA_NONRES_LOCALS_1 x'+
    '    join PREPARED_CHECK_LINES l on x.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR and l.ISRES=''N'''+
    '    where l.EE_LOCALS_NBR ='+
    '    (select y.EE_LOCALS_NBR from PREPARED_CHECK_LINES y'+
    '    where y.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR order by NONRES_PRIORITY limit 1)');

  SQLite.Execute(' create table PA_RES_LOCALS as '+
    '    select *'+
    '    from PREPARED_CHECK_LINES l'+
    '    where l.DELETED=''N'' and l.ISRES=''Y''');

  SQLite.Execute(' create table NON_PA_LOCALS as '+
    '    select *'+
    '    from PREPARED_CHECK_LINES l'+
    '    where l.DELETED=''N'' and l.ISRES=''''');

  SQLite.Execute(' create table NONRES_RATES as'+
    '    select l.PR_CHECK_LINES_NBR, n.CO_LOCAL_TAX_NBR, ifnull(n.TAX_RATE,0) NONRES_TAX_RATE'+
    '    from (select distinct PR_CHECK_LINES_NBR'+
    '    from PREPARED_CHECK_LINES) l'+
    '    left join PA_NONRES_LOCALS n on n.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR'+
    '    and n.CO_LOCAL_TAX_NBR = (select r.CO_LOCAL_TAX_NBR from PA_NONRES_LOCALS r'+
    '    where r.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR order by NONRES_PRIORITY limit 1)');

  SQLite.Execute('create table PA_WINNERS as'+
    '    select PR_CHECK_LINES_NBR, NONRES_CO_LOCAL_TAX_NBR,'+
    '    case when NONRES_TAX_RATE > RES_TAX_RATE then ''N'''+
    '    else ''Y'' end WINNER,'+
    '    RES_TAX_RATE, NONRES_TAX_RATE'+
    '    from'+
    '    (select l.PR_CHECK_LINES_NBR, n.CO_LOCAL_TAX_NBR NONRES_CO_LOCAL_TAX_NBR, SUM(ifnull(r.TAX_RATE,0)) RES_TAX_RATE, MAX(n.NONRES_TAX_RATE) NONRES_TAX_RATE'+
    '    from (select distinct PR_CHECK_LINES_NBR'+
    '    from PREPARED_CHECK_LINES) l'+
    '    left join PA_RES_LOCALS r on r.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR'+
    '    left join NONRES_RATES n on n.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR'+
    '    group by l.PR_CHECK_LINES_NBR'+
    '    order by l.PR_CHECK_LINES_NBR)');

  SQLite.Execute('drop table NONRES_RATES');    
  SQLite.Execute('drop table PA_NONRES_LOCALS');
  SQLite.Execute('drop table PA_RES_LOCALS');

  SQLite.Execute('create index PREPARED_CHECK_LINES_I on PREPARED_CHECK_LINES (PR_CHECK_LINES_NBR)');
  SQLite.Execute('create index PA_WINNERS_I on PA_WINNERS(PR_CHECK_LINES_NBR)');
  SQLite.Execute('create index PR_I on PR(PR_NBR)');

  SQLite.Execute('create table PA_LOCALS as'+
    '    select * from PREPARED_CHECK_LINES'+
    '    where DELETED=''N'' and ISRES<>''''');

  SQLite.Execute('create table QEC_PA_LOCAL_TAXES as'+
    '    select r.CHECK_DATE, (select min(CHECK_DATE) from CHECK_DATES) BEGIN_DATE, p.MONTHLYMAX, r.RUN_NUMBER,'+
    '    p.EE_NBR, p.PR_NBR, p.PR_CHECK_NBR, p.PR_CHECK_LINES_NBR,'+
    '    p.CO_LOCAL_TAX_NBR, w.NONRES_CO_LOCAL_TAX_NBR,p.EE_LOCALS_NBR,p.DBDT_ATTACHED,'+
    '    p.NAME, z.AMOUNT AMOUNT, p.TAXABLE_WAGE, p.TAX_RATE, p.ISRES,p.EXEMPT_EXCLUDE,'+
    '    case when p.ISRES<>w.WINNER then ''Y'' else ''N'' end ISLOSER,'+
    '    case when p.EXEMPT_EXCLUDE=''X'' then ''Y'' else ''N'' end BLOCKED,'+
    '    case when (p.ISRES<>w.WINNER) and (p.EXEMPT_EXCLUDE<>''I'') then 0.0 else p.TAXABLE_WAGE * p.TAX_RATE end TAX,'+
    '    case when (p.ISRES<>w.WINNER) and (p.EXEMPT_EXCLUDE<>''I'') then 0.0 else round(p.TAXABLE_WAGE * p.TAX_RATE,2) end ROUNDEDTAX,'+
    '    p.LOCATION_LEVEL, p.CO_DIVISION_NBR, p.CO_BRANCH_NBR, p.CO_DEPARTMENT_NBR, p.CO_TEAM_NBR, p.CO_JOBS_NBR'+
    '    from PA_LOCALS p'+
    '    join PA_WINNERS w on w.PR_CHECK_LINES_NBR=p.PR_CHECK_LINES_NBR'+
    '    join PR_CHECK_LINES z on z.PR_CHECK_LINES_NBR=p.PR_CHECK_LINES_NBR'+
    '    join PR r on r.PR_NBR=p.PR_NBR '+
    '    where not (p.ISRES=''N'' and p.CO_LOCAL_TAX_NBR<>w.NONRES_CO_LOCAL_TAX_NBR)');

  SQLite.Execute('alter table QEC_PA_LOCAL_TAXES add column SY_LOCALS_NBR INT');

  SQLite.Execute('drop table PA_WINNERS');

  // update BEGIN_DATE field with the first day of month when monthly maximum is used
  PaStored := SQLite.Select('select PR_CHECK_LINES_NBR, CO_LOCAL_TAX_NBR from QEC_PA_LOCAL_TAXES where MONTHLYMAX=''Y''', []);
  PaStored.First;
  while not PaStored.Eof do
  begin
    SQLite.Execute('update QEC_PA_LOCAL_TAXES set BEGIN_DATE='+
    IntToStr(Trunc(PaStored.FieldByName('CHECK_DATE').AsDateTime + 1 - DayOfTheMonth(PaStored.FieldByName('CHECK_DATE').AsDateTime)))+
    ' where PR_CHECK_LINES_NBR='+
      PaStored.FieldByName('PR_CHECK_LINES_NBR').AsString+
      ' and CO_LOCAL_TAX_NBR='+PaStored.FieldByName('CO_LOCAL_TAX_NBR').AsString);
    PaStored.Next;
  end;

  PaStored := SQLite.Select('select *, cast(0.0 as REAL) QTD_WAGES from QEC_PA_LOCAL_TAXES order by EE_NBR, CO_LOCAL_TAX_NBR, CHECK_DATE, RUN_NUMBER, PR_CHECK_LINES_NBR', []);
  PaStored.First;

  SQLite.Execute('drop table QEC_PA_LOCAL_TAXES');

  SQLite.Insert('QEC_PA_LOCAL_TAXES', PaStored.vclDataset, 0, '', True);

  SQLite.Execute('create index QEC_PA_LOCAL_TAXES_I on QEC_PA_LOCAL_TAXES(CO_LOCAL_TAX_NBR,CHECK_DATE)');
  SQLite.Execute('create index QEC_PA_LOCAL_TAXES_I1 on QEC_PA_LOCAL_TAXES(NBR)');

  // calculating quarter to date wages for each check line

  SQLite.Execute('update QEC_PA_LOCAL_TAXES'+
    '  set LOCATION_LEVEL='+
    '  (select n.LOCATION_LEVEL'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  ),'+
    '  CO_DIVISION_NBR='+
    '  (select n.CO_DIVISION_NBR'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  ),'+
    '  CO_BRANCH_NBR='+
    '  (select n.CO_BRANCH_NBR'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  ),'+
    '  CO_DEPARTMENT_NBR='+
    '  (select n.CO_DEPARTMENT_NBR'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  ),'+
    '  CO_TEAM_NBR='+
    '  (select n.CO_TEAM_NBR'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  ),'+
    '  CO_JOBS_NBR='+
    '  (select n.CO_JOBS_NBR'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  )'+
    '  where ISRES=''Y'' '+
    '  and exists (select n.LOCATION_LEVEL'+
    '  from QEC_PA_LOCAL_TAXES x'+
    '  join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.ISRES=''N'''+
    '  where x.ISRES=''Y'' and x.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR'+
    '  and x.EE_LOCALS_NBR=QEC_PA_LOCAL_TAXES.EE_LOCALS_NBR'+
    '  )');

  PaStored := SQLite.Select('select * from QEC_PA_LOCAL_TAXES order by NBR', []);
  PaStored.First;
  if PaStored.RecordCount > 0 then
  begin
    QTDWages := 0;
    EE_NBR := 0;
    CO_LOCAL_TAX_NBR := 0;
    while not PaStored.Eof do
    begin
      if (EE_NBR <> PaStored['EE_NBR']) or (CO_LOCAL_TAX_NBR <> PaStored['CO_LOCAL_TAX_NBR']) then
        QTDWages := 0;
      EE_NBR := PaStored['EE_NBR'];
      CO_LOCAL_TAX_NBR := PaStored['CO_LOCAL_TAX_NBR'];
      SQLite.Execute('update QEC_PA_LOCAL_TAXES set QTD_WAGES='+FloatToStr(QTDWages)+
        ' where NBR='+PaStored.FieldByName('NBR').AsString);
      if PaStored.FieldByName('CHECK_DATE').AsInteger >= PaStored.FieldByName('BEGIN_DATE').AsInteger then
        QTDWages := QTDWages + PaStored.FieldByName('AMOUNT').AsFloat;
      PaStored.Next;
    end;
  end;

  SQLite.Execute('create table QEC_PA_LOCAL_TAXES_1 as'+
    '    select s.SY_LOCALS_NBR, s.WAGE_MINIMUM, s.WAGE_MAXIMUM, s.TAX_MAXIMUM, s.PAY_WITH_STATE MTD,'+
    '    l.QTD_WAGES,'+
    '    l.*'+
    '    from QEC_PA_LOCAL_TAXES l'+
    '    join CHECK_DATES d on d.CHECK_DATE=l.CHECK_DATE'+
    '    join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR'+
    '    join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR');

  // subtruct wage above maximum based on quarter to date wages
  SQLite.Execute('update QEC_PA_LOCAL_TAXES_1'+
    '    set TAXABLE_WAGE ='+
    '    case when QTD_WAGES > WAGE_MAXIMUM then'+
    '      case when (QTD_WAGES - WAGE_MAXIMUM) > AMOUNT then 0.0 else AMOUNT - (QTD_WAGES - WAGE_MAXIMUM) end'+
    '    else AMOUNT end'+
    '    where WAGE_MAXIMUM is not null');

  // subtruct wage below minimum based on quarter to date wages
  SQLite.Execute('update QEC_PA_LOCAL_TAXES_1'+
    '    set TAXABLE_WAGE ='+
    '      case when (QTD_WAGES - AMOUNT) < WAGE_MINIMUM then'+
    '         case when TAXABLE_WAGE - WAGE_MINIMUM - (QTD_WAGES - AMOUNT) > 0.0 then TAXABLE_WAGE - WAGE_MINIMUM - (QTD_WAGES - AMOUNT) else 0.0 end'+
    '      else AMOUNT end'+
    '    where WAGE_MINIMUM is not null');

  // totals by PA check locals

  SQLite.Execute('create index QEC_PA_LOCAL_TAXES_1_I on QEC_PA_LOCAL_TAXES_1(PR_CHECK_NBR, CO_LOCAL_TAX_NBR)');

  SQLite.Execute('create table PA_CHECK_LOCALS as'+
    '    select PR_CHECK_NBR, CO_LOCAL_TAX_NBR,'+
    '    case when (ISLOSER=''Y'') and (EXEMPT_EXCLUDE<>''I'') then 0.0 else TAX_RATE end TAX_RATE, SUM(TAXABLE_WAGE) TAXABLE_WAGE,'+
    '    SUM(0.00) TAX,'+
    '    SUM(ROUNDEDTAX) ROUNDEDTAX'+
    '    from QEC_PA_LOCAL_TAXES_1'+
    '    group by 1,2,3'+
    '    order by 1,2,3');

  // calculate local taxes
  SQLite.Execute('update PA_CHECK_LOCALS set TAX=round(TAX_RATE*TAXABLE_WAGE,2)');

  PaStored := SQLite.Select('select PR_CHECK_NBR, CO_LOCAL_TAX_NBR, TAX-ROUNDEDTAX PENNY from PA_CHECK_LOCALS where TAX-ROUNDEDTAX <> 0', []);
  PaStored.First;
  while not PaStored.Eof do
  begin
    SQLite.Execute('update QEC_PA_LOCAL_TAXES_1 set ROUNDEDTAX=ROUNDEDTAX+'+
    Format('%0.2f', [PaStored.FieldByName('PENNY').AsFloat])+
    '  where PR_CHECK_NBR='+PaStored.FieldByName('PR_CHECK_NBR').AsString+' and '+
    '  CO_LOCAL_TAX_NBR='+PaStored.FieldByName('CO_LOCAL_TAX_NBR').AsString+
    '  and PR_CHECK_LINES_NBR='+
      VarToStr(SQLite.Value('select PR_CHECK_LINES_NBR  from QEC_PA_LOCAL_TAXES_1'+
      '  where PR_CHECK_NBR=:PR_CHECK_NBR and CO_LOCAL_TAX_NBR=:CO_LOCAL_TAX_NBR'+
      '  order by ROUNDEDTAX desc limit 1', [PaStored['PR_CHECK_NBR'], PaStored['CO_LOCAL_TAX_NBR']])));

    PaStored.Next;

  end;

  SQLite.Execute('drop table PA_CHECK_LOCALS');

  SQLite.Execute('drop table QEC_PA_LOCAL_TAXES');

  SQLite.Execute('create table QEC_PA_LOCAL_TAXES as '+
    '  select l.*, ifnull(e.EE_LOCALS_NBR,0) NONRES_EE_LOCALS_NBR  '+
    '  from QEC_PA_LOCAL_TAXES_1 l '+
    '  left join EE_LOCALS e on e.CHECK_DATE=l.CHECK_DATE and e.EE_NBR=l.EE_NBR and e.CO_LOCAL_TAX_NBR=l.NONRES_CO_LOCAL_TAX_NBR '
    );

  SQLite.Execute('alter table QEC_PA_LOCAL_TAXES add column LOCAL_TYPE CHAR(1)');
  SQLite.Execute('update QEC_PA_LOCAL_TAXES set LOCAL_TYPE=(select max(LOCAL_TYPE) from SY_LOCALS s where s.SY_LOCALS_NBR=QEC_PA_LOCAL_TAXES.SY_LOCALS_NBR)');

  SQLite.Execute('alter table QEC_PA_LOCAL_TAXES ADD COLUMN COMBINE_FOR_TAX_PAYMENTS CHAR(1)');

  SQLite.Execute(
    '  update QEC_PA_LOCAL_TAXES set '+
    '  COMBINE_FOR_TAX_PAYMENTS= '+
    '  (select s.COMBINE_FOR_TAX_PAYMENTS'+
    '  from CHECK_DATES d'+
    '  join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=QEC_PA_LOCAL_TAXES.CO_LOCAL_TAX_NBR'+
    '  join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
    '  where d.CHECK_DATE=QEC_PA_LOCAL_TAXES.CHECK_DATE)');

  SQLite.Execute(
    '  update QEC_PA_LOCAL_TAXES set'+
    '    NONRES_CO_LOCAL_TAX_NBR=0,'+
    '    NONRES_EE_LOCALS_NBR=0'+
    '  where COMBINE_FOR_TAX_PAYMENTS = ''N''');

(*
  SQLite.Execute(
  '  update QEC_PA_LOCAL_TAXES set '+
  '    NONRES_CO_LOCAL_TAX_NBR=0,'+
  '    NONRES_EE_LOCALS_NBR=0,'+
  '    TAXABLE_WAGE=0.0'+
  '  where COMBINE_FOR_TAX_PAYMENTS=''Y'' and exists'+
  '  (select 1 from QEC_PA_LOCAL_TAXES z where z.COMBINE_FOR_TAX_PAYMENTS=''N'''+
  '  and z.PR_CHECK_LINES_NBR=QEC_PA_LOCAL_TAXES.PR_CHECK_LINES_NBR)');
*)

  SQLite.Execute('update BLOB'+
  '  set NONRES_EE_LOCALS_NBR='+
  '  (select q.NONRES_EE_LOCALS_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR),'+
  '  LOCATION_LEVEL='+
  '  (select q.LOCATION_LEVEL from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR),'+
  '  DIVISION_NBR='+
  '  (select q.CO_DIVISION_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR),'+
  '  BRANCH_NBR='+
  '  (select q.CO_BRANCH_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR),'+
  '  DEPARTMENT_NBR='+
  '  (select q.CO_DEPARTMENT_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR),'+
  '  TEAM_NBR='+
  '  (select q.CO_TEAM_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR)'+
  '  where ifnull(NONRES_EE_LOCALS_NBR,0)=0'+
  '  and exists(select 1 from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=BLOB.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=BLOB.EE_LOCALS_NBR)');

  SQLite.Execute('update BLOB set'+
    '    NONRES_EE_LOCALS_NBR=0'+
    '    where'+
    '    (select s.COMBINE_FOR_TAX_PAYMENTS'+
    '    from CHECK_DATES d'+
    '    join PR p on p.PR_NBR=BLOB.PR_NBR'+
    '    join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=BLOB.CO_LOCAL_TAX_NBR'+
    '    join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
    '    where d.CHECK_DATE=p.CHECK_DATE) = ''N''');

  SQLite.Execute('drop table QEC_PA_LOCAL_TAXES_1');

  SQLite.Execute('create index QEC_PA_LOCAL_TAXES_I on QEC_PA_LOCAL_TAXES(PR_CHECK_NBR, CO_LOCAL_TAX_NBR)');

  // reconstruct totals by PA check locals after fixing rounding
  SQLite.Execute('create table PA_CHECK_LOCALS as'+
    '  select PR_CHECK_NBR, CO_LOCAL_TAX_NBR,'+
    '  case when ISLOSER=''Y'' then 0.0 else TAX_RATE end TAX_RATE, SUM(TAXABLE_WAGE) TAXABLE_WAGE,'+
    '  SUM(0.00) TAX,'+
    '  SUM(ROUNDEDTAX) ROUNDEDTAX'+
    '  from QEC_PA_LOCAL_TAXES'+
    '  group by 1,2,3'+
    '  order by 1,2,3');

  SQLite.Execute('create view OLD_CALC as'+
    '  select l.EE_LOCALS_NBR, e.CUSTOM_EMPLOYEE_NUMBER EE, b.PR_CHECK_NBR, b.PR_CHECK_LINES_NBR, s.NAME TAX,'+
    '  ns.NAME NONRES, b.AMOUNT, round(b.LOCAL_TAXABLE_WAGES,2) LOCAL_TAXABLE_WAGES,'+
    '  round(b.LOCAL_TAX,2) LOCAL_TAX,'+
    '  b.DIVISION_NBR, b.BRANCH_NBR, b.DEPARTMENT_NBR, b.TEAM_NBR, b.JOBS_NBR, b.LOCATION_LEVEL'+
    '  from BLOB b'+
    '  join PR p on b.PR_NBR=p.PR_NBR'+
    '  join PR_CHECK z on z.PR_CHECK_NBR=b.PR_CHECK_NBR'+
    '  join EE e on e.EE_NBR=z.EE_NBR'+
    '  join EE_LOCALS l on l.CHECK_DATE=p.CHECK_DATE and l.EE_LOCALS_NBR=b.EE_LOCALS_NBR'+
    '  join CO_LOCAL_TAX c on c.CHECK_DATE=p.CHECK_DATE and c.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR'+
    '  join SY_LOCALS s on s.CHECK_DATE=p.CHECK_DATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
    '  left join EE_LOCALS nl on nl.CHECK_DATE=p.CHECK_DATE and nl.EE_LOCALS_NBR=b.NONRES_EE_LOCALS_NBR'+
    '  left join CO_LOCAL_TAX nc on nc.CHECK_DATE=p.CHECK_DATE and nc.CO_LOCAL_TAX_NBR=nl.CO_LOCAL_TAX_NBR'+
    '  left join SY_LOCALS ns on ns.CHECK_DATE=p.CHECK_DATE and ns.SY_LOCALS_NBR=nc.SY_LOCALS_NBR');

  SQLite.Execute('create view NEW_CALC as'+
    '  select l.EE_LOCALS_NBR, e.CUSTOM_EMPLOYEE_NUMBER EE, b.PR_CHECK_NBR, b.PR_CHECK_LINES_NBR, s.NAME TAX, ns.NAME NONRES, b.AMOUNT, b.TAXABLE_WAGE LOCAL_TAXABLE_WAGES,'+
    '  b.ROUNDEDTAX LOCAL_TAX,'+
    '  b.CO_DIVISION_NBR, b.CO_BRANCH_NBR, b.CO_DEPARTMENT_NBR, b.CO_TEAM_NBR, b.LOCATION_LEVEL'+
    '  from QEC_PA_LOCAL_TAXES b'+
    '  join PR p on b.PR_NBR=p.PR_NBR'+
    '  join PR_CHECK z on z.PR_CHECK_NBR=b.PR_CHECK_NBR'+
    '  join EE e on e.EE_NBR=z.EE_NBR'+
    '  join EE_LOCALS l on l.CHECK_DATE=p.CHECK_DATE and l.EE_LOCALS_NBR=b.EE_LOCALS_NBR'+
    '  join CO_LOCAL_TAX c on c.CHECK_DATE=p.CHECK_DATE and c.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR'+
    '  join SY_LOCALS s on s.CHECK_DATE=p.CHECK_DATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
    '  left join EE_LOCALS nl on nl.CHECK_DATE=p.CHECK_DATE and nl.EE_LOCALS_NBR=b.NONRES_EE_LOCALS_NBR'+
    '  left join CO_LOCAL_TAX nc on nc.CHECK_DATE=p.CHECK_DATE and nc.CO_LOCAL_TAX_NBR=nl.CO_LOCAL_TAX_NBR'+
    '  left join SY_LOCALS ns on ns.CHECK_DATE=p.CHECK_DATE and ns.SY_LOCALS_NBR=nc.SY_LOCALS_NBR');

  SQLite.Execute('create view NEW_CALC_NP as'+
    '  select l.EE_LOCALS_NBR, e.CUSTOM_EMPLOYEE_NUMBER EE, b.PR_CHECK_NBR, b.PR_CHECK_LINES_NBR, s.NAME TAX, ns.NAME NONRES, b.AMOUNT, b.TAXABLE_WAGE LOCAL_TAXABLE_WAGES,'+
    '  b.ROUNDEDTAX LOCAL_TAX,b.NONRES_EE_LOCALS_NBR,b.PR_NBR,b.ISLOSER,'+
    '  b.CO_DIVISION_NBR DIVISION_NBR, b.CO_BRANCH_NBR BRANCH_NBR, b.CO_DEPARTMENT_NBR DEPARTMENT_NBR, b.CO_TEAM_NBR TEAM_NBR, b.CO_JOBS_NBR JOBS_NBR, b.LOCATION_LEVEL'+
    '  from QEC_PA_LOCAL_TAXES b'+
    '  join PR p on b.PR_NBR=p.PR_NBR'+
    '  join PR_CHECK z on z.PR_CHECK_NBR=b.PR_CHECK_NBR'+
    '  join EE e on e.EE_NBR=z.EE_NBR'+
    '  join EE_LOCALS l on l.CHECK_DATE=p.CHECK_DATE and l.EE_LOCALS_NBR=b.EE_LOCALS_NBR'+
    '  join CO_LOCAL_TAX c on c.CHECK_DATE=p.CHECK_DATE and c.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR'+
    '  join SY_LOCALS s on s.CHECK_DATE=p.CHECK_DATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
    '  left join EE_LOCALS nl on nl.CHECK_DATE=p.CHECK_DATE and nl.EE_LOCALS_NBR=b.NONRES_EE_LOCALS_NBR'+
    '  left join CO_LOCAL_TAX nc on nc.CHECK_DATE=p.CHECK_DATE and nc.CO_LOCAL_TAX_NBR=nl.CO_LOCAL_TAX_NBR'+
    '  left join SY_LOCALS ns on ns.CHECK_DATE=p.CHECK_DATE and ns.SY_LOCALS_NBR=nc.SY_LOCALS_NBR where s.SY_LOCALS_NBR not in (921,922)');

  SQLite.Execute('create view NEW_CALC_PH as'+
    '  select l.EE_LOCALS_NBR, e.CUSTOM_EMPLOYEE_NUMBER EE, b.PR_CHECK_NBR, b.PR_CHECK_LINES_NBR, s.NAME TAX, ns.NAME NONRES, b.AMOUNT, b.TAXABLE_WAGE LOCAL_TAXABLE_WAGES,'+
    '  b.ROUNDEDTAX LOCAL_TAX,b.NONRES_EE_LOCALS_NBR,b.PR_NBR,b.ISLOSER,'+
    '  b.CO_DIVISION_NBR DIVISION_NBR, b.CO_BRANCH_NBR BRANCH_NBR, b.CO_DEPARTMENT_NBR DEPARTMENT_NBR, b.CO_TEAM_NBR TEAM_NBR, b.CO_JOBS_NBR JOBS_NBR, b.LOCATION_LEVEL'+
    '  from QEC_PA_LOCAL_TAXES b'+
    '  join PR p on b.PR_NBR=p.PR_NBR'+
    '  join PR_CHECK z on z.PR_CHECK_NBR=b.PR_CHECK_NBR'+
    '  join EE e on e.EE_NBR=z.EE_NBR'+
    '  join EE_LOCALS l on l.CHECK_DATE=p.CHECK_DATE and l.EE_LOCALS_NBR=b.EE_LOCALS_NBR'+
    '  join CO_LOCAL_TAX c on c.CHECK_DATE=p.CHECK_DATE and c.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR'+
    '  join SY_LOCALS s on s.CHECK_DATE=p.CHECK_DATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
    '  left join EE_LOCALS nl on nl.CHECK_DATE=p.CHECK_DATE and nl.EE_LOCALS_NBR=b.NONRES_EE_LOCALS_NBR'+
    '  left join CO_LOCAL_TAX nc on nc.CHECK_DATE=p.CHECK_DATE and nc.CO_LOCAL_TAX_NBR=nl.CO_LOCAL_TAX_NBR'+
    '  left join SY_LOCALS ns on ns.CHECK_DATE=p.CHECK_DATE and ns.SY_LOCALS_NBR=nc.SY_LOCALS_NBR where s.SY_LOCALS_NBR in (921,922)');

  ctx_EndWait;
end;

procedure LoadData(const SQLite: ISQLite; const CoNbr, PrNbr: Integer; const QEndDate: TDateTime;
  const BeginDate: TDateTime);

  function EndOfDay(const Value: TDateTime): TDateTime;
  begin
    Result := DateOf(Value) + 1 - 1 / 1440;
  end;

  procedure LoadTables(const CheckDate: TDateTime);
  var
    Q: IevQuery;
    D: IevDataset;
    Where: String;
  begin

    Q := TevQuery.Create(
      '  select '+
      '  CO_NBR,'+
      '  NAME,' +
      '  CUSTOM_COMPANY_NUMBER,'+
      '  FEDERAL_TAX_EXEMPT_STATUS,'+
      '  FED_TAX_EXEMPT_EE_OASDI,'+
      '  FED_TAX_EXEMPT_EE_MEDICARE,'+
      '  FUI_TAX_EXEMPT,'+
      '  FED_TAX_EXEMPT_ER_OASDI,'+
      '  FED_TAX_EXEMPT_ER_MEDICARE,'+
      '  FUI_RATE_OVERRIDE,'+
      '  CO_LOCATIONS_NBR'+
      '  from CO where CO_NBR=:CoNbr and {AsOfDate<CO>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    Q.Params.AddValue('CoNbr', CoNbr);
    SQLite.Insert('CO', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CL_E_DS_NBR,'+
      '  SY_LOCALS_NBR,'+
      '  EXEMPT_EXCLUDE'+
      '  from CL_E_D_LOCAL_EXMPT_EXCLD where {AsOfDate<CL_E_D_LOCAL_EXMPT_EXCLD>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CL_E_D_LOCAL_EXMPT_EXCLD', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CL_E_DS_NBR,'+
      '  SY_STATES_NBR,'+
      '  EMPLOYEE_EXEMPT_EXCLUDE_STATE,'+
      '  EMPLOYEE_EXEMPT_EXCLUDE_SDI,' +
      '  EMPLOYEE_EXEMPT_EXCLUDE_SUI' +
      '  from CL_E_D_STATE_EXMPT_EXCLD where {AsOfDate<CL_E_D_STATE_EXMPT_EXCLD>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CL_E_D_STATE_EXMPT_EXCLD', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CL_NBR,'+
      '  CUSTOM_CLIENT_NUMBER,'+
      '  RECIPROCATE_SUI,' +
      '  NAME' +
      '  from CL where {AsOfDate<CL>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CL', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  SY_STATES_NBR,'+
      '  STATE,'+
      '  NAME' +
      '  from SY_STATES where {AsOfDate<SY_STATES>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('SY_STATES', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  OASDI_WAGE_LIMIT,'+
      '  EE_OASDI_RATE,'+
      '  ER_OASDI_RATE,'+
      '  MEDICARE_WAGE_LIMIT,'+
      '  MEDICARE_RATE,'+
      '  FUI_WAGE_LIMIT,'+
      '  FUI_RATE_REAL,'+
      '  FUI_RATE_CREDIT'+
      '  from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('SY_FED_TAX_TABLE', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_LOCAL_TAX_NBR,'+
      '  SY_LOCALS_NBR,'+
      '  LOCAL_ACTIVE,'+
      '  EXEMPT,'+
      '  DEDUCT,'+
      '  SELF_ADJUST_TAX,'+
      '  TAX_RATE'+
      '  from CO_LOCAL_TAX where CO_NBR=:CoNbr and {AsOfDate<CO_LOCAL_TAX>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    Q.Params.AddValue('CoNbr', CoNbr);
    SQLite.Insert('CO_LOCAL_TAX', Q.Result.vclDataset, CheckDate);

    SQLite.Execute('create index if not exists CO_LOCAL_TAX_SY on CO_LOCAL_TAX (SY_LOCALS_NBR)');

    Q := TevQuery.Create(
      '  select '+
      '  SY_FED_EXEMPTIONS_NBR,'+
      '  E_D_CODE_TYPE,'+
      '  EXEMPT_FEDERAL,'+
      '  EXEMPT_FUI,'+
      '  EXEMPT_EMPLOYEE_OASDI,'+
      '  EXEMPT_EMPLOYER_OASDI,'+
      '  EXEMPT_EMPLOYEE_MEDICARE,'+
      '  EXEMPT_EMPLOYER_MEDICARE,'+
      '  EXEMPT_EMPLOYEE_EIC '+
      'from SY_FED_EXEMPTIONS where {AsOfDate<SY_FED_EXEMPTIONS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('SY_FED_EXEMPTIONS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_SUI_NBR,'+
      '  CO_NBR,'+
      '  SY_SUI_NBR,'+
      '  TAX_RETURN_CODE,'+
      '  CO_STATES_NBR,'+
      '  SUI_REIMBURSER,'+
      '  FINAL_TAX_RETURN,'+
      '  FILLER,'+
      '  RATE '+
      '  from CO_SUI where CO_NBR=:CoNbr and {AsOfDate<CO_SUI>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    Q.Params.AddValue('CoNbr', CoNbr);
    SQLite.Insert('CO_SUI', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CL_E_DS_NBR,'+
      '  E_D_CODE_TYPE,'+
      '  CUSTOM_E_D_CODE_NUMBER,'+
      '  DESCRIPTION,'+
      '  OVERRIDE_EE_RATE_NUMBER,'+
      '  OVERRIDE_FED_TAX_TYPE,'+
      '  EE_EXEMPT_EXCLUDE_OASDI,'+
      '  EE_EXEMPT_EXCLUDE_MEDICARE,'+
      '  EE_EXEMPT_EXCLUDE_FEDERAL,'+
      '  EE_EXEMPT_EXCLUDE_EIC,'+
      '  ER_EXEMPT_EXCLUDE_OASDI,'+
      '  ER_EXEMPT_EXCLUDE_MEDICARE,'+
      '  ER_EXEMPT_EXCLUDE_FUI,'+
      '  FILLER,'+
      '  SY_STATE_NBR,'+
      '  GL_OFFSET_TAG,'+
      '  OVERRIDE_FED_TAX_VALUE,'+
      '  APPLY_BEFORE_TAXES,'+
      '  OVERSTATE_HOURS_FOR_OVERTIME,'+
      '  SD_PRIORITY_NUMBER,'+
      '  SD_DEDUCTIONS_TO_ZERO '+
      '  from CL_E_DS where {AsOfDate<CL_E_DS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CL_E_DS', Q.Result.vclDataset, CheckDate);

    SQLite.Execute('create table if not exists TAXABLE_TYPES (E_D_CODE_TYPE CHAR(2))');
    SQLite.Execute('create table if not exists SPECIAL_TYPES (E_D_CODE_TYPE CHAR(2))');

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      if TypeIsTaxable(Q.Result.FieldByName('E_D_CODE_TYPE').AsString) then
        SQLite.Execute('insert into TAXABLE_TYPES select '''+Q.Result.FieldByName('E_D_CODE_TYPE').AsString+
          ''' where not exists (select 1 from TAXABLE_TYPES where E_D_CODE_TYPE='''+Q.Result.FieldByName('E_D_CODE_TYPE').AsString+''')');

      if TypeIsSpecTaxedDed(Q.Result.FieldByName('E_D_CODE_TYPE').AsString) then
        SQLite.Execute('insert into SPECIAL_TYPES select '''+Q.Result.FieldByName('E_D_CODE_TYPE').AsString+
          ''' where not exists (select 1 from SPECIAL_TYPES where E_D_CODE_TYPE='''+Q.Result.FieldByName('E_D_CODE_TYPE').AsString+''')');

      Q.Result.Next;
    end;

    D := SQLite.Select('select distinct SY_LOCALS_NBR from CO_LOCAL_TAX',[]);

    if D.RecordCount > 0 then
    begin
      Where := D.FieldByName('SY_LOCALS_NBR').AsString;
      D.First;
      while not D.Eof do
      begin
        D.Next;
        if D.Eof or (Length(Where) > 1000) then
        begin
          Q := TevQuery.Create(
            '  select '+
            '  SY_LOCAL_EXEMPTIONS_NBR,'+
            '  SY_LOCALS_NBR,'+
            '  E_D_CODE_TYPE,'+
            '  EXEMPT '+
            '  from SY_LOCAL_EXEMPTIONS'+
            '  where SY_LOCALS_NBR in ('+Where+') and {AsOfDate<SY_LOCAL_EXEMPTIONS>}');
          Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
          SQLite.Insert('SY_LOCAL_EXEMPTIONS', Q.Result.vclDataset, CheckDate);

          Q := TevQuery.Create(
            '  select '+
            '  SY_LOCALS_NBR,'+
            '  SY_STATES_NBR,'+
            '  LOCAL_TYPE,'+
            '  CALCULATION_METHOD,'+
            '  TAX_FREQUENCY,'+
            '  PRINT_RETURN_IF_ZERO,'+
            '  PAY_WITH_STATE,'+
            '  WAGE_MINIMUM,'+
            '  WAGE_MAXIMUM,'+
            '  TAX_RATE,'+
            '  TAX_MAXIMUM,'+
            '  MISCELLANEOUS_AMOUNT,'+
            '  LOCAL_MINIMUM_WAGE,'+
            '  TAX_TYPE,'+
            '  COMBINE_FOR_TAX_PAYMENTS,'+
            '  NAME,'+
            '  SY_LOCAL_TAX_PMT_AGENCY_NBR'+
            '  from SY_LOCALS where LOCAL_ACTIVE=''Y'' and {AsOfDate<SY_LOCALS>}'+
            '  and SY_LOCALS_NBR in ('+Where+')');
          Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
          SQLite.Insert('SY_LOCALS', Q.Result.vclDataset, CheckDate);

          if not D.Eof then
            Where := D.FieldByName('SY_LOCALS_NBR').AsString;
        end
        else
        if not D.Eof then
          Where := Where + ',' + D.FieldByName('SY_LOCALS_NBR').AsString;
      end;

    end;

    Q := TevQuery.Create(
      '  select '+
      '  SY_STATE_EXEMPTIONS_NBR,'+
      '  SY_STATES_NBR,'+
      '  E_D_CODE_TYPE,'+
      '  EXEMPT_STATE,'+
      '  EXEMPT_EMPLOYEE_SDI,'+
      '  EXEMPT_EMPLOYER_SDI,'+
      '  EXEMPT_EMPLOYEE_SUI,'+
      '  EXEMPT_EMPLOYER_SUI '+
      '  from SY_STATE_EXEMPTIONS where {AsOfDate<SY_STATE_EXEMPTIONS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('SY_STATE_EXEMPTIONS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  SY_SUI_NBR,'+
      '  SY_STATES_NBR,'+
      '  MAXIMUM_WAGE,'+
      '  EE_ER_OR_EE_OTHER_OR_ER_OTHER,'+
      '  USE_MISC_TAX_RETURN_CODE,'+
      '  SUI_ACTIVE,'+
      '  GLOBAL_RATE,'+
      '  FUTURE_DEFAULT_RATE_BEGIN_DATE,'+
      '  FUTURE_MAX_WAGE_BEGIN_DATE,'+
      '  FUTURE_DEFAULT_RATE,'+
      '  FUTURE_MAXIMUM_WAGE,'+
      '  FILLER,'+
      '  SUI_TAX_NAME '+
      '  from SY_SUI where {AsOfDate<SY_SUI>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('SY_SUI', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  EE_RATES_NBR,'+
      '  RATE_NUMBER,'+
      '  EE_NBR,'+
      '  CO_DIVISION_NBR,'+
      '  CO_BRANCH_NBR,'+
      '  CO_DEPARTMENT_NBR,'+
      '  CO_TEAM_NBR,'+
      '  CO_JOBS_NBR'+
      '  from EE_RATES '+
      '  where (CO_DIVISION_NBR is not null or'+
      '  CO_BRANCH_NBR is not null or'+
      '  CO_DEPARTMENT_NBR is not null or'+
      '  CO_TEAM_NBR is not null or'+
      '  CO_JOBS_NBR is not null) and {AsOfDate<EE_RATES>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('EE_RATES', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  EE_LOCALS_NBR,'+
      '  EE_NBR,'+
      '  CO_LOCAL_TAX_NBR,'+
      '  EXEMPT_EXCLUDE,'+
      '  WORK_ADDRESS_OVR,'+
      '  LOCAL_ENABLED,'+
      '  OVERRIDE_LOCAL_TAX_VALUE,'+
      '  OVERRIDE_LOCAL_TAX_TYPE,'+
      '  INCLUDE_IN_PRETAX,'+
      '  DEDUCT,'+
      '  PERCENTAGE_OF_TAX_WAGES,'+
      '  CO_LOCATIONS_NBR'+
      '  from EE_LOCALS where LOCAL_ENABLED = ''Y'' and {AsOfDate<EE_LOCALS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('EE_LOCALS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_JOBS_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_JOBS_LOCALS where {AsOfDate<CO_JOBS_LOCALS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CO_JOBS_LOCALS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_TEAM_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_TEAM_LOCALS where {AsOfDate<CO_TEAM_LOCALS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CO_TEAM_LOCALS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_DEPARTMENT_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_DEPARTMENT_LOCALS where {AsOfDate<CO_DEPARTMENT_LOCALS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CO_DEPARTMENT_LOCALS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_BRANCH_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_BRANCH_LOCALS where {AsOfDate<CO_BRANCH_LOCALS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CO_BRANCH_LOCALS', Q.Result.vclDataset, CheckDate);

    Q := TevQuery.Create(
      '  select '+
      '  CO_DIVISION_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_DIVISION_LOCALS where {AsOfDate<CO_DIVISION_LOCALS>}');
    Q.Params.AddValue('StatusDate', EndOfDay(CheckDate));
    SQLite.Insert('CO_DIVISION_LOCALS', Q.Result.vclDataset, CheckDate);

  end;

  procedure CreateTables;
  var
    Q: IevQuery;
    D: IevDataset;
  begin
    Q := TevQuery.Create(
      '  select '+
      '  CO_NBR,'+
      '  NAME,' +
      '  CUSTOM_COMPANY_NUMBER,'+
      '  FEDERAL_TAX_EXEMPT_STATUS,'+
      '  FED_TAX_EXEMPT_EE_OASDI,'+
      '  FED_TAX_EXEMPT_EE_MEDICARE,'+
      '  FUI_TAX_EXEMPT,'+
      '  FED_TAX_EXEMPT_ER_OASDI,'+
      '  FED_TAX_EXEMPT_ER_MEDICARE,'+
      '  FUI_RATE_OVERRIDE,'+
      '  CO_LOCATIONS_NBR'+
      '  from CO where 1=2');
    SQLite.Insert('CO', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CL_E_DS_NBR,'+
      '  SY_LOCALS_NBR,'+
      '  EXEMPT_EXCLUDE'+
      '  from CL_E_D_LOCAL_EXMPT_EXCLD where 1=2');
    SQLite.Insert('CL_E_D_LOCAL_EXMPT_EXCLD', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CL_E_DS_NBR,'+
      '  SY_STATES_NBR,'+
      '  EMPLOYEE_EXEMPT_EXCLUDE_STATE,'+
      '  EMPLOYEE_EXEMPT_EXCLUDE_SDI,' +
      '  EMPLOYEE_EXEMPT_EXCLUDE_SUI' +
      '  from CL_E_D_STATE_EXMPT_EXCLD where 1=2');
    SQLite.Insert('CL_E_D_STATE_EXMPT_EXCLD', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CL_NBR,'+
      '  CUSTOM_CLIENT_NUMBER,'+
      '  RECIPROCATE_SUI,' +
      '  NAME' +
      '  from CL where 1=2');
    SQLite.Insert('CL', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  SY_STATES_NBR,'+
      '  STATE,'+
      '  NAME' +
      '  from SY_STATES where 1=2');
    SQLite.Insert('SY_STATES', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  OASDI_WAGE_LIMIT,'+
      '  EE_OASDI_RATE,'+
      '  ER_OASDI_RATE,'+
      '  MEDICARE_WAGE_LIMIT,'+
      '  MEDICARE_RATE,'+
      '  FUI_WAGE_LIMIT,'+
      '  FUI_RATE_REAL,'+
      '  FUI_RATE_CREDIT'+
      '  from SY_FED_TAX_TABLE where 1=2');
    SQLite.Insert('SY_FED_TAX_TABLE', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_LOCAL_TAX_NBR,'+
      '  SY_LOCALS_NBR,'+
      '  LOCAL_ACTIVE,'+
      '  EXEMPT,'+
      '  DEDUCT,'+
      '  SELF_ADJUST_TAX,'+
      '  TAX_RATE'+
      '  from CO_LOCAL_TAX where 1=2');
    SQLite.Insert('CO_LOCAL_TAX', Q.Result.vclDataset, 1);
    SQLite.Execute('create index if not exists CO_LOCAL_TAX_SY on CO_LOCAL_TAX (SY_LOCALS_NBR)');

    Q := TevQuery.Create(
      '  select '+
      '  SY_FED_EXEMPTIONS_NBR,'+
      '  E_D_CODE_TYPE,'+
      '  EXEMPT_FEDERAL,'+
      '  EXEMPT_FUI,'+
      '  EXEMPT_EMPLOYEE_OASDI,'+
      '  EXEMPT_EMPLOYER_OASDI,'+
      '  EXEMPT_EMPLOYEE_MEDICARE,'+
      '  EXEMPT_EMPLOYER_MEDICARE,'+
      '  EXEMPT_EMPLOYEE_EIC '+
      'from SY_FED_EXEMPTIONS where 1=2');
    SQLite.Insert('SY_FED_EXEMPTIONS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_SUI_NBR,'+
      '  CO_NBR,'+
      '  SY_SUI_NBR,'+
      '  TAX_RETURN_CODE,'+
      '  CO_STATES_NBR,'+
      '  SUI_REIMBURSER,'+
      '  FINAL_TAX_RETURN,'+
      '  FILLER,'+
      '  RATE '+
      '  from CO_SUI where 1=2');
    SQLite.Insert('CO_SUI', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CL_E_DS_NBR,'+
      '  E_D_CODE_TYPE,'+
      '  CUSTOM_E_D_CODE_NUMBER,'+
      '  DESCRIPTION,'+
      '  OVERRIDE_EE_RATE_NUMBER,'+
      '  OVERRIDE_FED_TAX_TYPE,'+
      '  EE_EXEMPT_EXCLUDE_OASDI,'+
      '  EE_EXEMPT_EXCLUDE_MEDICARE,'+
      '  EE_EXEMPT_EXCLUDE_FEDERAL,'+
      '  EE_EXEMPT_EXCLUDE_EIC,'+
      '  ER_EXEMPT_EXCLUDE_OASDI,'+
      '  ER_EXEMPT_EXCLUDE_MEDICARE,'+
      '  ER_EXEMPT_EXCLUDE_FUI,'+
      '  FILLER,'+
      '  SY_STATE_NBR,'+
      '  GL_OFFSET_TAG,'+
      '  OVERRIDE_FED_TAX_VALUE,'+
      '  APPLY_BEFORE_TAXES,'+
      '  OVERSTATE_HOURS_FOR_OVERTIME,'+
      '  SD_PRIORITY_NUMBER,'+
      '  SD_DEDUCTIONS_TO_ZERO '+
      '  from CL_E_DS where 1=2');
    SQLite.Insert('CL_E_DS', Q.Result.vclDataset, 1);

    SQLite.Execute('create table if not exists TAXABLE_TYPES (E_D_CODE_TYPE CHAR(2))');
    SQLite.Execute('create table if not exists SPECIAL_TYPES (E_D_CODE_TYPE CHAR(2))');

    D := SQLite.Select('select distinct SY_LOCALS_NBR from CO_LOCAL_TAX',[]);

    Q := TevQuery.Create(
    '  select '+
    '  SY_LOCAL_EXEMPTIONS_NBR,'+
    '  SY_LOCALS_NBR,'+
    '  E_D_CODE_TYPE,'+
    '  EXEMPT '+
    '  from SY_LOCAL_EXEMPTIONS'+
    '  where 1=2');
    SQLite.Insert('SY_LOCAL_EXEMPTIONS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
    '  select '+
    '  SY_LOCALS_NBR,'+
    '  SY_STATES_NBR,'+
    '  LOCAL_TYPE,'+
    '  CALCULATION_METHOD,'+
    '  TAX_FREQUENCY,'+
    '  PRINT_RETURN_IF_ZERO,'+
    '  PAY_WITH_STATE,'+
    '  WAGE_MINIMUM,'+
    '  WAGE_MAXIMUM,'+
    '  TAX_RATE,'+
    '  TAX_MAXIMUM,'+
    '  MISCELLANEOUS_AMOUNT,'+
    '  LOCAL_MINIMUM_WAGE,'+
    '  TAX_TYPE,'+
    '  COMBINE_FOR_TAX_PAYMENTS,'+
    '  NAME,'+
    '  SY_LOCAL_TAX_PMT_AGENCY_NBR'+
    '  from SY_LOCALS'+
    '  where 1=2');
    SQLite.Insert('SY_LOCALS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  SY_STATE_EXEMPTIONS_NBR,'+
      '  SY_STATES_NBR,'+
      '  E_D_CODE_TYPE,'+
      '  EXEMPT_STATE,'+
      '  EXEMPT_EMPLOYEE_SDI,'+
      '  EXEMPT_EMPLOYER_SDI,'+
      '  EXEMPT_EMPLOYEE_SUI,'+
      '  EXEMPT_EMPLOYER_SUI '+
      '  from SY_STATE_EXEMPTIONS where 1=2');
    SQLite.Insert('SY_STATE_EXEMPTIONS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  SY_SUI_NBR,'+
      '  SY_STATES_NBR,'+
      '  MAXIMUM_WAGE,'+
      '  EE_ER_OR_EE_OTHER_OR_ER_OTHER,'+
      '  USE_MISC_TAX_RETURN_CODE,'+
      '  SUI_ACTIVE,'+
      '  GLOBAL_RATE,'+
      '  FUTURE_DEFAULT_RATE_BEGIN_DATE,'+
      '  FUTURE_MAX_WAGE_BEGIN_DATE,'+
      '  FUTURE_DEFAULT_RATE,'+
      '  FUTURE_MAXIMUM_WAGE,'+
      '  FILLER,'+
      '  SUI_TAX_NAME '+
      '  from SY_SUI where 1=2');
    SQLite.Insert('SY_SUI', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  EE_LOCALS_NBR,'+
      '  EE_NBR,'+
      '  CO_LOCAL_TAX_NBR,'+
      '  EXEMPT_EXCLUDE,'+
      '  WORK_ADDRESS_OVR,'+
      '  LOCAL_ENABLED,'+
      '  OVERRIDE_LOCAL_TAX_VALUE,'+
      '  OVERRIDE_LOCAL_TAX_TYPE,'+
      '  INCLUDE_IN_PRETAX,'+
      '  DEDUCT,'+
      '  PERCENTAGE_OF_TAX_WAGES,'+
      '  CO_LOCATIONS_NBR'+
      '  from EE_LOCALS where 1=2');
    SQLite.Insert('EE_LOCALS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_JOBS_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_JOBS_LOCALS where 1=2');
    SQLite.Insert('CO_JOBS_LOCALS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_TEAM_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_TEAM_LOCALS where 1=2');
    SQLite.Insert('CO_TEAM_LOCALS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_DEPARTMENT_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_DEPARTMENT_LOCALS where 1=2');
    SQLite.Insert('CO_DEPARTMENT_LOCALS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_BRANCH_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_BRANCH_LOCALS where 1=2');
    SQLite.Insert('CO_BRANCH_LOCALS', Q.Result.vclDataset, 1);

    Q := TevQuery.Create(
      '  select '+
      '  CO_DIVISION_NBR,'+
      '  CO_LOCAL_TAX_NBR'+
      '  from CO_DIVISION_LOCALS where 1=2');
    SQLite.Insert('CO_DIVISION_LOCALS', Q.Result.vclDataset, 1);


  end;

var
  Q: IevQuery;
  QE: TDateTime;
  Nbr: Integer;
  DS: IevDataset;
begin
  Q := TevQuery.Create(
    ' select CO_NBR, PR_NBR, CHECK_DATE, RUN_NUMBER, SCHEDULED, PAYROLL_TYPE, STATUS ' +
    ' from PR '+
    ' where {AsOfNow<PR>} ' +
    ' and CO_NBR=:CoNbr ' +
    ' and CHECK_DATE >=:BeginDate ' +
    ' and STATUS in ('''+PAYROLL_STATUS_PROCESSED+''') ' +
    ' and CHECK_DATE <=:EndDate '+
    ' order by CHECK_DATE, RUN_NUMBER');

  Q.Params.AddValue('BeginDate', BeginDate);
  Q.Params.AddValue('EndDate', QEndDate);
  Q.Params.AddValue('CoNbr', CoNbr);

  Q.Result.First;
  SQLite.Insert('PR', Q.Result.vclDataset, 0, '', True);

  if PrNbr <> 0 then
  begin
    DS := SQLite.Select('select NBR from PR where PR_NBR=:PR_NBR',[PrNbr]);
    Nbr := DS['NBR'];
    SQLite.Execute('delete from PR where NBR>='+IntToStr(Nbr));
  end;

  SQLite.Execute('delete from PR where PAYROLL_TYPE=''V'' or STATUS=''V''');

  Q := TevQuery.Create(
    ' select CO_JOBS_NBR, DESCRIPTION,'+
    ' CO_LOCATIONS_NBR'+
    ' from CO_JOBS '+
    ' where {AsOfNow<CO_JOBS>} and CO_NBR='+IntToStr(CoNbr));
  Q.Result.First;                                                                 
  SQLite.Insert('CO_JOBS', Q.Result.vclDataset, 0, '', True);

  QE := DateOf(QEndDate);
  Q := TevQuery.Create('SELECT DISTINCT CHECK_DATE, CHECK_DATE REALDATE FROM PR WHERE {AsOfNow<PR>} AND CO_NBR=:CoNbr AND CHECK_DATE BETWEEN :BeginDate and :EndDate AND STATUS IN (''P'') ORDER BY 1');
  Q.Params.AddValue('BeginDate', BeginDate);
  Q.Params.AddValue('EndDate', QEndDate);
  Q.Params.AddValue('CoNbr', CoNbr);
  Q.Result.First;

  Q.Result.First;
  if not Q.Result.Locate('CHECK_DATE', QE, []) then
  begin
    Q.Result.Insert;
    Q.Result['CHECK_DATE'] := QE;
    Q.Result['REALDATE'] := QE;
    Q.Result.Post;
  end;
  Q.Result.First;

  Q.Result.IndexFieldNames := 'CHECK_DATE';
  Q.Result.First;
  SQLite.Insert('CHECK_DATES', Q.Result.vclDataset);
  Q.Result.IndexFieldNames := 'CHECK_DATE';
  Q.Result.First;

  CreateTables;
  while not Q.Result.Eof do
  begin
    LoadTables(DateOf(Q.Result.FieldByName('CHECK_DATE').AsDateTime));
    Q.Result.Next;
  end;

  SQLite.Execute('create table DL as select distinct * from CO_DIVISION_LOCALS');
  SQLite.Execute('delete from CO_DIVISION_LOCALS');
  SQLite.Execute('insert into CO_DIVISION_LOCALS select * from DL');
  SQLite.Execute('drop table DL');

  SQLite.Execute('create table DL as select distinct * from CO_BRANCH_LOCALS');
  SQLite.Execute('delete from CO_BRANCH_LOCALS');
  SQLite.Execute('insert into CO_BRANCH_LOCALS select * from DL');
  SQLite.Execute('drop table DL');

  SQLite.Execute('create table DL as select distinct * from CO_DEPARTMENT_LOCALS');
  SQLite.Execute('delete from CO_DEPARTMENT_LOCALS');
  SQLite.Execute('insert into CO_DEPARTMENT_LOCALS select * from DL');
  SQLite.Execute('drop table DL');

  SQLite.Execute('create table DL as select distinct * from CO_TEAM_LOCALS');
  SQLite.Execute('delete from CO_TEAM_LOCALS');
  SQLite.Execute('insert into CO_TEAM_LOCALS select * from DL');
  SQLite.Execute('drop table DL');

  Q := TevQuery.Create(
    '  select '+
    '  CO_DIVISION_NBR,'+
    '  CUSTOM_DIVISION_NUMBER,'+
    '  CO_LOCATIONS_NBR'+
    '  from CO_DIVISION where CO_NBR=:CoNbr and {AsOfNow<CO_DIVISION>}');
  Q.Params.AddValue('CoNbr', CoNbr);
  SQLite.Insert('CO_DIVISION', Q.Result.vclDataset, 0);

  Q := TevQuery.Create(
    '  select '+
    '  CO_BRANCH_NBR,'+
    '  CO_DIVISION_NBR,'+
    '  CUSTOM_BRANCH_NUMBER,'+
    '  CO_LOCATIONS_NBR'+
    '  from CO_BRANCH where {AsOfNow<CO_BRANCH>}');
  SQLite.Insert('CO_BRANCH', Q.Result.vclDataset, 0);

  Q := TevQuery.Create(
    '  select '+
    '  CO_DEPARTMENT_NBR,'+
    '  CO_BRANCH_NBR,'+
    '  CUSTOM_DEPARTMENT_NUMBER,'+
    '  CO_LOCATIONS_NBR'+
    '  from CO_DEPARTMENT where {AsOfNow<CO_DEPARTMENT>}');
  SQLite.Insert('CO_DEPARTMENT', Q.Result.vclDataset, 0);

  Q := TevQuery.Create(
    '  select '+
    '  CO_TEAM_NBR,'+
    '  CO_DEPARTMENT_NBR,'+
    '  CUSTOM_TEAM_NUMBER,'+
    '  CO_LOCATIONS_NBR'+
    '  from CO_TEAM where {AsOfNow<CO_TEAM>}');
  SQLite.Insert('CO_TEAM', Q.Result.vclDataset, 0);

  SQLite.Execute('delete from CO_BRANCH where CO_DIVISION_NBR not in (select CO_DIVISION_NBR from CO_DIVISION)');
  SQLite.Execute('delete from CO_DEPARTMENT where CO_BRANCH_NBR not in (select CO_BRANCH_NBR from CO_BRANCH)');
  SQLite.Execute('delete from CO_TEAM where CO_DEPARTMENT_NBR not in (select CO_DEPARTMENT_NBR from CO_DEPARTMENT)');

  Q := TevQuery.Create(
    ' select c.PR_NBR, c.PR_CHECK_NBR, c.EE_NBR, c.CHECK_TYPE ' +
    ' from PR p'+
    ' join PR_CHECK c on {AsOfNow<c>} and c.PR_NBR=p.PR_NBR ' +
    ' where {AsOfNow<p>} ' +
    ' and p.CO_NBR=:CoNbr ' +
    ' and p.CHECK_DATE >=:BeginDate ' +
    ' and p.STATUS in ('''+PAYROLL_STATUS_PROCESSED+''') ' +
    ' and p.PR_NBR <> '+IntToStr(PrNbr)+''+
    ' and c.CHECK_STATUS<>''V'''+
    ' and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
    ' and p.CHECK_DATE <=:EndDate ');

  Q.Params.AddValue('BeginDate', BeginDate);
  Q.Params.AddValue('EndDate', QEndDate);
  Q.Params.AddValue('CoNbr', CoNbr);

  Q.Result.First;
  SQLite.Insert('PR_CHECK', Q.Result.vclDataset);

  Q := TevQuery.Create(
    ' select l.PR_CHECK_NBR, l.EE_LOCALS_NBR, l.LOCAL_GROSS_WAGES, '+
    ' l.LOCAL_TAXABLE_WAGE, l.LOCAL_TAX, l.NONRES_EE_LOCALS_NBR, l.CO_LOCATIONS_NBR, l.REPORTABLE ' +
    ' from PR p'+
    ' join PR_CHECK c on {AsOfNow<c>} and c.PR_NBR=p.PR_NBR ' +
    ' join PR_CHECK_LOCALS l on {AsOfNow<l>} and l.PR_CHECK_NBR=c.PR_CHECK_NBR ' +
    ' where {AsOfNow<p>} ' +
    ' and c.CHECK_STATUS<>''V'''+
    ' and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +
    ' and p.CO_NBR=:CoNbr ' +
    ' and p.CHECK_DATE >=:BeginDate ' +
    ' and p.STATUS in ('''+PAYROLL_STATUS_PROCESSED+''') ' +
    ' and p.PR_NBR <> '+IntToStr(PrNbr)+''+
    ' and p.CHECK_DATE <=:EndDate ');

  Q.Params.AddValue('BeginDate', BeginDate);
  Q.Params.AddValue('EndDate', QEndDate);
  Q.Params.AddValue('CoNbr', CoNbr);

  Q.Result.First;
  SQLite.Insert('PR_CHECK_LOCALS', Q.Result.vclDataset);

  Q := TevQuery.Create(
    ' select l.PR_CHECK_NBR, l.PR_CHECK_LINES_NBR, l.RATE_NUMBER, l.HOURS_OR_PIECES,'+
    ' l.CL_E_DS_NBR, l.AMOUNT, l.CO_JOBS_NBR, l.CO_TEAM_NBR, l.CO_DEPARTMENT_NBR, l.CO_BRANCH_NBR, l.CO_DIVISION_NBR, l.WORK_ADDRESS_OVR ' +
    ' from PR p'+
    ' join PR_CHECK c on {AsOfNow<c>} and c.PR_NBR=p.PR_NBR ' +
    ' join PR_CHECK_LINES l on {AsOfNow<l>} and l.PR_CHECK_NBR=c.PR_CHECK_NBR ' +
    ' where {AsOfNow<p>} ' +
    ' and p.CO_NBR=:CoNbr ' +
    ' and p.CHECK_DATE >=:BeginDate ' +
    ' and p.STATUS in ('''+PAYROLL_STATUS_PROCESSED+''') ' +
    ' and p.PR_NBR <> '+IntToStr(PrNbr)+''+
    ' and p.CHECK_DATE <=:EndDate ');

  Q.Params.AddValue('BeginDate', BeginDate);
  Q.Params.AddValue('EndDate', QEndDate);
  Q.Params.AddValue('CoNbr', CoNbr);

  Q.Result.First;
  SQLite.Insert('PR_CHECK_LINES', Q.Result.vclDataset);

  Q.Result.First;
  SQLite.Execute('ALTER TABLE PR_CHECK_LINES ADD COLUMN DBDT_ATTACHED CHAR(1)');
  while not Q.Result.Eof do
  begin
    if not Q.Result.FieldByName('CO_TEAM_NBR').IsNull then
    begin
      if Q.Result['WORK_ADDRESS_OVR'] = 'N' then
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''Y'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString)
      else
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''N'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString);
    end
    else
    if not Q.Result.FieldByName('CO_DEPARTMENT_NBR').IsNull then
    begin
      if Q.Result['WORK_ADDRESS_OVR'] = 'N' then
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''Y'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString)
      else
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''N'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString);
    end
    else
    if not Q.Result.FieldByName('CO_BRANCH_NBR').IsNull then
    begin
      if Q.Result['WORK_ADDRESS_OVR'] = 'N' then
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''Y'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString)
      else
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''N'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString);
    end
    else
    if not Q.Result.FieldByName('CO_DIVISION_NBR').IsNull then
    begin
      if Q.Result['WORK_ADDRESS_OVR'] = 'N' then
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''Y'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString)
      else
        SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''N'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString);
    end
    else
    begin
      SQLite.Execute('update PR_CHECK_LINES set DBDT_ATTACHED=''N'' where PR_CHECK_LINES_NBR='+Q.Result.FieldByName('PR_CHECK_LINES_NBR').AsString);
    end;

    Q.Result.Next;
  end;

  Q := TevQuery.Create(
    ' select k.PR_CHECK_LINES_NBR, k.EE_LOCALS_NBR, k.EXEMPT_EXCLUDE ' +
    ' from PR p'+
    ' join PR_CHECK c on {AsOfNow<c>} and c.PR_NBR=p.PR_NBR ' +
    ' join PR_CHECK_LINES l on {AsOfNow<l>} and l.PR_CHECK_NBR=c.PR_CHECK_NBR ' +
    ' join PR_CHECK_LINE_LOCALS k on {AsOfNow<k>} and k.PR_CHECK_LINES_NBR=l.PR_CHECK_LINES_NBR ' +
    ' where {AsOfNow<p>} ' +
    ' and p.CO_NBR=:CoNbr ' +
    ' and p.CHECK_DATE >=:BeginDate ' +
    ' and p.STATUS in ('''+PAYROLL_STATUS_PROCESSED+''') ' +
    ' and p.PR_NBR <> '+IntToStr(PrNbr)+''+
    ' and p.CHECK_DATE <=:EndDate ');

  Q.Params.AddValue('BeginDate', BeginDate);
  Q.Params.AddValue('EndDate', QEndDate);
  Q.Params.AddValue('CoNbr', CoNbr);

  Q.Result.First;
  SQLite.Insert('PR_CHECK_LINE_LOCALS', Q.Result.vclDataset);

  Q := TevQuery.Create(
    ' select EE_NBR, CO_NBR, CUSTOM_EMPLOYEE_NUMBER, CL_PERSON_NBR, CUSTOM_EMPLOYEE_NUMBER, '+
    ' CO_JOBS_NBR, CO_TEAM_NBR, CO_DEPARTMENT_NBR, CO_BRANCH_NBR, CO_DIVISION_NBR' +
    ' from EE '+
    ' where {AsOfNow<EE>} ' +
    ' and CO_NBR=:CoNbr');

  Q.Params.AddValue('CoNbr', CoNbr);

  Q.Result.First;
  SQLite.Insert('EE', Q.Result.vclDataset);

  Q := TevQuery.Create(
    ' select p.CL_PERSON_NBR, p.FIRST_NAME, p.LAST_NAME ' +
    ' from CL_PERSON p'+
    ' where {AsOfNow<p>} ');

  Q.Result.First;
  SQLite.Insert('CL_PERSON', Q.Result.vclDataset);

  SQLite.Execute('CREATE TABLE IF NOT EXISTS LOCATIONS (PR_NBR INTEGER,PR_CHECK_NBR INTEGER,PR_CHECK_LINES_NBR INTEGER,'+
  '  EE_LOCALS_NBR INTEGER, CO_LOCAL_TAX_NBR INTEGER,CALCULATE CHAR(1),ISRES CHAR(1),DBDT_ATTACHED CHAR(1),'+
  '  BLOCKED CHAR(1), ISLOSER CHAR(1),EXCLUDED CHAR(1),NONRES_EE_LOCALS_NBR INTEGER,DIVISION_NBR INTEGER,'+
  '  BRANCH_NBR INTEGER,DEPARTMENT_NBR INTEGER,TEAM_NBR INTEGER,JOBS_NBR INTEGER,LOCATION_LEVEL CHAR(2),AMOUNT REAL,RATE REAL,'+
  '  LOCAL_GROSS_WAGES REAL,LOCAL_TAXABLE_WAGES REAL,LOCAL_TAX REAL)');


  SQLite.Execute('CREATE TABLE IF NOT EXISTS BLOB as SELECT * from LOCATIONS where 1=2');

  SQLite.Execute('delete from BLOB where EXCLUDED=''Y''');

  SQLite.Execute('update PR_CHECK_LOCALS set LOCAL_GROSS_WAGES=0 where LOCAL_GROSS_WAGES is null');
  SQLite.Execute('update PR_CHECK_LOCALS set LOCAL_TAXABLE_WAGE=0 where LOCAL_TAXABLE_WAGE is null');
  SQLite.Execute('update PR_CHECK_LOCALS set LOCAL_TAX=0 where LOCAL_TAX is null');

  SQLite.Execute('update BLOB set LOCAL_TAX=round(LOCAL_TAX,2), LOCAL_TAXABLE_WAGES=round(LOCAL_TAXABLE_WAGES,2),'+
  '  LOCAL_GROSS_WAGES=round(LOCAL_GROSS_WAGES,2)');

  SQLite.Execute('insert into LOCATIONS select * from BLOB');
  SQLite.Execute('delete from LOCATIONS where PR_CHECK_NBR not in (select PR_CHECK_NBR from PR_CHECK)');

  SQLite.Execute('delete from PR_CHECK where PR_NBR not in (select PR_NBR from PR)');
  SQLite.Execute('delete from PR_CHECK_LINES where PR_CHECK_NBR not in (select PR_CHECK_NBR from PR_CHECK)');
  SQLite.Execute('delete from PR_CHECK_LOCALS where PR_CHECK_NBR not in (select PR_CHECK_NBR from PR_CHECK)');
  SQLite.Execute('delete from PR_CHECK_LINE_LOCALS where PR_CHECK_LINES_NBR not in (select PR_CHECK_LINES_NBR from PR_CHECK_LINES)');
  SQLite.Execute('delete from LOCATIONS where PR_CHECK_NBR not in (select PR_CHECK_NBR from PR_CHECK)');

  SQLite.Execute('update BLOB set LOCAL_TAX=0.0 where LOCAL_TAX is null');
  SQLite.Execute('update LOCATIONS set LOCAL_TAX=0.0 where LOCAL_TAX is null');

end;

procedure FixBlobsDL(const SQLite: ISQLite);
var
  DS, DS1: IevDataset;
begin

  SQLite.Execute('create index QEC_PA_LOCAL_TAXES_DL on QEC_PA_LOCAL_TAXES(PR_CHECK_NBR, CO_LOCAL_TAX_NBR, NONRES_EE_LOCALS_NBR)');
  SQLite.Execute('create index BLOB_DL on BLOB(PR_CHECK_NBR, CO_LOCAL_TAX_NBR, NONRES_EE_LOCALS_NBR)');

  SQLite.Execute('delete from BLOB where PR_CHECK_LINES_NBR not in (select PR_CHECK_LINES_NBR from PR_CHECK_LINES)');

  DS := SQLite.Select(
  '  select * from BLOB x'+
  '  join PR p on p.PR_NBR=x.PR_NBR'+
  '  join PR_CHECK_LINES l on l.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR'+
  '  where l.E_D_CODE_TYPE like ''D%''', []);

  DS.First;
  while not DS.Eof do
  begin
    DS1 := SQLite.Select(
    '  select x.* '+
    '  from BLOB x'+
    '  join PR_CHECK_LINES l on l.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR'+
    '  where x.PR_CHECK_NBR=:PR_CHECK_NBR and x.CO_LOCAL_TAX_NBR=:CO_LOCAL_TAX_NBR and x.NONRES_EE_LOCALS_NBR=:NONRES_EE_LOCALS_NBR'+
    '  and x.PR_CHECK_LINES_NBR<>:PR_CHECK_LINES_NBR and l.E_D_CODE_TYPE not like ''D%'' order by x.AMOUNT desc',
    [DS['PR_CHECK_NBR'],DS['CO_LOCAL_TAX_NBR'],DS['NONRES_EE_LOCALS_NBR'], DS['PR_CHECK_LINES_NBR']]);

    if not DS1.Eof then
    begin
      SQLite.Execute('delete from BLOB where '+
      '  PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+' and CO_LOCAL_TAX_NBR='+
      DS.FieldByName('CO_LOCAL_TAX_NBR').AsString+' and NONRES_EE_LOCALS_NBR='+
      DS.FieldByName('NONRES_EE_LOCALS_NBR').AsString+' and PR_CHECK_LINES_NBR='+
      DS.FieldByName('PR_CHECK_LINES_NBR').AsString);

      SQLite.Execute('update BLOB '+
      ' set LOCAL_TAX=LOCAL_TAX+'+DS.FieldByName('LOCAL_TAX').AsString+
      '     , LOCAL_TAXABLE_WAGES=LOCAL_TAXABLE_WAGES-'+DS.FieldByName('LOCAL_TAXABLE_WAGES').AsString+
      ' where '+
      '  PR_CHECK_NBR='+DS1.FieldByName('PR_CHECK_NBR').AsString+' and CO_LOCAL_TAX_NBR='+
      DS1.FieldByName('CO_LOCAL_TAX_NBR').AsString+' and NONRES_EE_LOCALS_NBR='+
      DS1.FieldByName('NONRES_EE_LOCALS_NBR').AsString+' and PR_CHECK_LINES_NBR='+
      DS1.FieldByName('PR_CHECK_LINES_NBR').AsString);

    end;

    DS.Next;
  end;

  DS := SQLite.Select(
  '  select PR_CHECK_NBR, CO_LOCAL_TAX_NBR, NONRES_EE_LOCALS_NBR, SUM(ROUNDEDTAX) LOCAL_TAX, COUNT(1) CNT'+
  '  from QEC_PA_LOCAL_TAXES group by 1,2,3 order by 1,2,3', []);

  DS.First;
  while not DS.Eof do
  begin
    DS1 := SQLite.Select(
    '  select SUM(LOCAL_TAX) LOCAL_TAX, COUNT(1) CNT'+
    '  from BLOB '+
    '  where PR_CHECK_NBR=:PR_CHECK_NBR and CO_LOCAL_TAX_NBR=:CO_LOCAL_TAX_NBR and NONRES_EE_LOCALS_NBR=:NONRES_EE_LOCALS_NBR',
    [DS['PR_CHECK_NBR'],DS['CO_LOCAL_TAX_NBR'],DS['NONRES_EE_LOCALS_NBR']]);

    DS1.First;
    if DoubleCompare(StrToFloatDef(DS1.FieldByName('LOCAL_TAX').AsString,0), coEqual, StrToFloatDef(DS.FieldByName('LOCAL_TAX').AsString,0))
    then
    begin
      SQLite.Execute('delete from BLOB where '+
      '  PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+' and CO_LOCAL_TAX_NBR='+
      DS.FieldByName('CO_LOCAL_TAX_NBR').AsString+' and NONRES_EE_LOCALS_NBR='+
      DS.FieldByName('NONRES_EE_LOCALS_NBR').AsString);

      SQLite.Execute('insert into BLOB '+
      'select '+
      ' PR_NBR,PR_CHECK_NBR,PR_CHECK_LINES_NBR,EE_LOCALS_NBR,CO_LOCAL_TAX_NBR,'+
      ' case when ISLOSER=''Y'' then ''N'' else ''Y'' end CALCULATE,ISRES,DBDT_ATTACHED,'+
      ' BLOCKED,ISLOSER,''N'' EXCLUDED,NONRES_EE_LOCALS_NBR,'+
      ' CO_DIVISION_NBR,CO_BRANCH_NBR,CO_DEPARTMENT_NBR,CO_TEAM_NBR,CO_JOBS_NBR,LOCATION_LEVEL,AMOUNT,TAX_RATE,'+
      ' AMOUNT,TAXABLE_WAGE,ROUNDEDTAX '+
      'from QEC_PA_LOCAL_TAXES where '+
      '  PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+' and CO_LOCAL_TAX_NBR='+
      DS.FieldByName('CO_LOCAL_TAX_NBR').AsString+' and NONRES_EE_LOCALS_NBR='+
      DS.FieldByName('NONRES_EE_LOCALS_NBR').AsString);
    end;
    DS.Next;
  end;

end;

procedure FixBlobs(const SQLite: ISQLite);
var
  DS, DS1: IevDataset;
begin

  SQLite.Execute('drop table if exists BLOB_TOTALS');

  SQLite.Execute('create table BLOB_TOTALS as'+
    '  select PR_CHECK_NBR, EE_LOCALS_NBR, SUM(LOCAL_TAX) LOCAL_TAX'+
    '  from BLOB'+
    '  group by 1,2'+
    '  order by 1,2');

  // fixing incorrect blob records...
  DS := SQLite.Select(
  ' select t.PR_CHECK_NBR, t.EE_LOCALS_NBR'+
  ' from BLOB_TOTALS t'+
  ' join PR_CHECK_LOCALS p on p.PR_CHECK_NBR=t.PR_CHECK_NBR and p.EE_LOCALS_NBR=t.EE_LOCALS_NBR'+
  ' where t.LOCAL_TAX=0 and p.LOCAL_TAX<>0',[]);

  DS.First;
  while not DS.Eof do
  begin
    SQLite.Execute('update BLOB set'+
    ' LOCAL_TAX=0, ' +
    ' LOCAL_GROSS_WAGES=0.0,'+
    ' LOCAL_TAXABLE_WAGES=0.0'+
    ' where PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+
    ' and EE_LOCALS_NBR='+DS.FieldByName('EE_LOCALS_NBR').AsString);
    DS.Next;
  end;

  DS := SQLite.Select(
  ' select t.PR_CHECK_NBR, t.EE_LOCALS_NBR, p.LOCAL_GROSS_WAGES, p.LOCAL_TAXABLE_WAGE, p.LOCAL_TAX'+
  ' from BLOB_TOTALS t'+
  ' join PR_CHECK_LOCALS p on p.PR_CHECK_NBR=t.PR_CHECK_NBR and p.EE_LOCALS_NBR=t.EE_LOCALS_NBR'+
  ' where t.LOCAL_TAX=0 and p.LOCAL_TAX<>0', []);

  DS.First;
  while not DS.Eof do
  begin
    DS1 := SQLite.Select('select PR_CHECK_LINES_NBR from BLOB '+
    ' where PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+
    ' and EE_LOCALS_NBR='+DS.FieldByName('EE_LOCALS_NBR').AsString, []);

    SQLite.Execute('update BLOB set'+
    ' LOCAL_TAX='+DS.FieldByName('LOCAL_TAX').AsString+', ' +
    ' LOCAL_GROSS_WAGES='+DS.FieldByName('LOCAL_GROSS_WAGES').AsString+','+
    ' LOCAL_TAXABLE_WAGES='+DS.FieldByName('LOCAL_TAXABLE_WAGE').AsString+
    ' where PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+
    ' and EE_LOCALS_NBR='+DS.FieldByName('EE_LOCALS_NBR').AsString+
    ' and PR_CHECK_LINES_NBR='+DS1.FieldByName('PR_CHECK_LINES_NBR').AsString);
    DS.Next;
  end;

  SQLite.Execute('delete from BLOB where not exists'+
    '  (select 1 from PR_CHECK_LOCALS c where c.PR_CHECK_NBR=PR_CHECK_NBR and c.EE_LOCALS_NBR=EE_LOCALS_NBR)');

  SQLite.Execute('drop table if exists PAYROLL_LOCATIONS');
  SQLite.Execute('drop table if exists payrolls');
  SQLite.Execute('drop table if exists qec');
  SQLite.Execute('drop table if exists WAGE_FIX');
  SQLite.Execute('drop table if exists BLOB_VS_CHECK');
  SQLite.Execute('drop table if exists LINE_FIX');

  SQLite.Execute('create table PAYROLL_LOCATIONS as select * from BLOB');

  SQLite.Execute('create table payrolls as'+
  '    select PR_CHECK_NBR, EE_LOCALS_NBR, SUM(LOCAL_GROSS_WAGES) GW, SUM(LOCAL_TAXABLE_WAGES) TW'+
  '    from BLOB'+
  '    group by 1,2'+
  '    order by 1,2');

  SQLite.Execute('create table qec as'+
    '  select PR_CHECK_NBR, EE_LOCALS_NBR, SUM(LOCAL_GROSS_WAGES) GW, SUM(LOCAL_TAXABLE_WAGE) TW'+
    '  from PR_CHECK_LOCALS'+
    '  group by 1,2'+
    '  order by 1,2');

  SQLite.Execute('create table BLOB_VS_CHECK as'+
  '    select p.*, ifnull(q.GW,0.00) GW1, ifnull(q.TW,0.00) TW1'+
  '    from payrolls p'+
  '    left join qec q on p.PR_CHECK_NBR=q.PR_CHECK_NBR and p.EE_LOCALS_NBR=q.EE_LOCALS_NBR'+
  '    union'+
  '    select q.PR_CHECK_NBR, q.EE_LOCALS_NBR, ifnull(p.GW,0.00) GW, ifnull(p.TW,0.00) TW, q.GW GW1, q.TW TW1'+
  '    from qec q'+
  '    left join payrolls p on p.PR_CHECK_NBR=q.PR_CHECK_NBR and p.EE_LOCALS_NBR=q.EE_LOCALS_NBR');

  SQLite.Execute('update BLOB_VS_CHECK set GW = ifnull(GW, 0.0), GW1 = ifnull(GW1, 0.0), TW = ifnull(TW, 0.0), TW1 = ifnull(TW1, 0.0)');

  SQLite.Execute('create table WAGE_FIX as'+
  '    select PR_CHECK_NBR, EE_LOCALS_NBR, GW1 - GW FIX, TW1 - TW TFIX'+
  '    from BLOB_VS_CHECK'+
  '    where abs(GW - GW1) > 0.005 or abs(TW - TW1) > 0.005');

  SQLite.Execute('create table LINE_FIX as'+
  '    select f.*,'+
  '    (select PR_CHECK_LINES_NBR'+
  '     from BLOB l'+
  '     where l.PR_CHECK_NBR=f.PR_CHECK_NBR and l.EE_LOCALS_NBR=f.EE_LOCALS_NBR'+
  '     order by LOCAL_TAX, LOCAL_GROSS_WAGES desc'+
  '    limit 1'+
  '    ) PR_CHECK_LINES_NBR'+
  '    from WAGE_FIX f');

  DS := SQLite.Select('select * from LINE_FIX', []);
  DS.First;
  while not DS.Eof do
  begin
    SQLite.Execute('update BLOB set LOCAL_GROSS_WAGES=LOCAL_GROSS_WAGES+'+
      DS.FieldByName('FIX').AsString +
      ', LOCAL_TAXABLE_WAGES=LOCAL_TAXABLE_WAGES+'+DS.FieldByName('TFIX').AsString +
      ' where PR_CHECK_LINES_NBR='+DS.FieldByName('PR_CHECK_LINES_NBR').AsString +
//      ' where PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString +
      ' and EE_LOCALS_NBR='+DS.FieldByName('EE_LOCALS_NBR').AsString+' limit 1');
    DS.Next;
  end;

  SQLite.Execute('drop table LINE_FIX');
  SQLite.Execute('drop table payrolls');
  SQLite.Execute('drop table qec');

end;

procedure CalculateAdjustments(const SQLite: ISQLite; const CoNbr,PrNbr: Integer; const QEndDate: TDateTime; const Monthly: Boolean);
var
  DS, PaStored: IevDataset;
begin

  SQLite.Execute('drop table LOCATIONS');
  SQLite.Execute('create table LOCATIONS as select * from BLOB where 1=2');

  // reverse amounts for the payroll calculated locations
//  SQLite.Execute('update LOCATIONS set LOCAL_TAX=-ifnull(LOCAL_TAX, 0), LOCAL_TAXABLE_WAGES=-ifnull(LOCAL_TAXABLE_WAGES,0), LOCAL_GROSS_WAGES=-ifnull(LOCAL_GROSS_WAGES,0)');

  // adding QEC calculated locations
  SQLite.Execute('insert into LOCATIONS'+
  '  (PR_NBR, PR_CHECK_NBR, PR_CHECK_LINES_NBR, EE_LOCALS_NBR, CO_LOCAL_TAX_NBR, ISRES, CALCULATE,ISLOSER,'+
  '  EXCLUDED, NONRES_EE_LOCALS_NBR, DIVISION_NBR, BRANCH_NBR, DEPARTMENT_NBR, TEAM_NBR, JOBS_NBR, LOCATION_LEVEL,'+
  '  AMOUNT, RATE, LOCAL_GROSS_WAGES, LOCAL_TAXABLE_WAGES, LOCAL_TAX)'+
  '  select x.PR_NBR, x.PR_CHECK_NBR, x.PR_CHECK_LINES_NBR, x.EE_LOCALS_NBR, x.CO_LOCAL_TAX_NBR, x.ISRES,'+
  '  case when ISLOSER=''Y'' then ''N'' else ''Y'' end CALCULATE, ISLOSER,'+
  '  ''N'', x.NONRES_EE_LOCALS_NBR, x.CO_DIVISION_NBR, x.CO_BRANCH_NBR, x.CO_DEPARTMENT_NBR, x.CO_TEAM_NBR, x.CO_JOBS_NBR, x.LOCATION_LEVEL,'+
  '  x.AMOUNT, x.TAX_RATE, x.AMOUNT, x.TAXABLE_WAGE, x.ROUNDEDTAX'+
  '  from QEC_PA_LOCAL_TAXES x');

  if Monthly then
  begin
    SQLite.Execute(
    '  delete from LOCATIONS'+
    '  where (select p.CHECK_DATE from PR p where p.PR_NBR=LOCATIONS.PR_NBR) < ' +
    IntToStr(Floor(GetBeginMonth(QEndDate))) +
    '  or (select p.CHECK_DATE from PR p where p.PR_NBR=LOCATIONS.PR_NBR) > ' +
    IntToStr(Floor(GetEndMonth(QEndDate))));

    SQLite.Execute(
    '  delete from PR'+
    '  where CHECK_DATE < ' + IntToStr(Floor(GetBeginMonth(QEndDate))) +
    '  or CHECK_DATE > ' + IntToStr(Floor(GetEndMonth(QEndDate))));

  end;

  SQLite.Execute('update LOCATIONS'+
  '  set NONRES_EE_LOCALS_NBR='+
  '  (select q.NONRES_EE_LOCALS_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR),'+
  '  LOCATION_LEVEL='+
  '  (select q.LOCATION_LEVEL from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR),'+
  '  DIVISION_NBR='+
  '  (select q.CO_DIVISION_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR),'+
  '  BRANCH_NBR='+
  '  (select q.CO_BRANCH_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR),'+
  '  DEPARTMENT_NBR='+
  '  (select q.CO_DEPARTMENT_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR),'+
  '  TEAM_NBR='+
  '  (select q.CO_TEAM_NBR from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR)'+
  '  where ifnull(NONRES_EE_LOCALS_NBR,0)=0'+
  '  and exists(select 1 from QEC_PA_LOCAL_TAXES q'+
  '  where q.PR_CHECK_LINES_NBR=LOCATIONS.PR_CHECK_LINES_NBR'+
  '  and q.EE_LOCALS_NBR=LOCATIONS.EE_LOCALS_NBR)');

  SQLite.Execute('create table QEC_LOCATIONS as select * from LOCATIONS where 1=2');
  SQLite.Execute('update LOCATIONS set LOCAL_TAX=round(LOCAL_TAX,2),'+
  ' LOCAL_TAXABLE_WAGES=round(LOCAL_TAXABLE_WAGES,2), LOCAL_GROSS_WAGES=round(LOCAL_GROSS_WAGES,2)');

  // calculating totals
  SQLite.Execute('create index LOCATIONS_I on LOCATIONS(PR_NBR, PR_CHECK_NBR, PR_CHECK_LINES_NBR, EE_LOCALS_NBR,CO_LOCAL_TAX_NBR)');

  SQLite.Execute('delete from LOCATIONS'+
  ' where not exists('+
  ' select 1 from PR p'+
  ' join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
  ' join CO_LOCAL_TAX c on c.CO_LOCAL_TAX_NBR=LOCATIONS.CO_LOCAL_TAX_NBR and c.CHECK_DATE=d.REALDATE'+
  ' join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
  ' join SY_STATES t on t.CHECK_DATE=d.REALDATE and t.SY_STATES_NBR=s.SY_STATES_NBR'+
  ' where p.PR_NBR=LOCATIONS.PR_NBR'+
  ' and t.STATE=''PA'' and s.LOCAL_TYPE in (''R'',''N'',''S''))');

  SQLite.Execute('insert into QEC_LOCATIONS '+
  ' (EE_LOCALS_NBR,CO_LOCAL_TAX_NBR,CALCULATE,ISRES,DBDT_ATTACHED,'+
  '  BLOCKED,ISLOSER,EXCLUDED,NONRES_EE_LOCALS_NBR,DIVISION_NBR,'+
  '  BRANCH_NBR,DEPARTMENT_NBR,TEAM_NBR,JOBS_NBR,LOCATION_LEVEL,AMOUNT,RATE,'+
  '  LOCAL_GROSS_WAGES,LOCAL_TAXABLE_WAGES,LOCAL_TAX,PR_CHECK_LINES_NBR, PR_NBR, PR_CHECK_NBR) ' +
  '    select EE_LOCALS_NBR,CO_LOCAL_TAX_NBR, ''Y'' CALCULATE, ISRES, ''N'' DBDT_ATTACHED, '+
  '    ''N'' BLOCKED, ISLOSER, ''N'' EXECLUDED, NONRES_EE_LOCALS_NBR,'+
  '    DIVISION_NBR, BRANCH_NBR, DEPARTMENT_NBR, TEAM_NBR, JOBS_NBR, LOCATION_LEVEL, 0.0 AMOUNT, MAX(RATE) RATE,'+
  '    round(SUM(LOCAL_GROSS_WAGES),2) LOCAL_GROSS_WAGES, round(SUM(LOCAL_TAXABLE_WAGES),2) LOCAL_TAXABLE_WAGES, round(SUM(LOCAL_TAX),2) LOCAL_TAX,'+
  '    MIN(PR_CHECK_LINES_NBR), MIN(PR_NBR), MIN(PR_CHECK_NBR)'+
  '    from LOCATIONS'+
  '    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15'+
  '    having SUM(LOCAL_TAX)<>0 or SUM(LOCAL_TAXABLE_WAGES)<>0 or SUM(LOCAL_GROSS_WAGES)<>0'+
  '    order by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15');

  SQLite.Execute('update QEC_LOCATIONS set DIVISION_NBR=NULL where DIVISION_NBR=0');
  SQLite.Execute('update QEC_LOCATIONS set BRANCH_NBR=NULL where BRANCH_NBR=0');
  SQLite.Execute('update QEC_LOCATIONS set DEPARTMENT_NBR=NULL where DEPARTMENT_NBR=0');
  SQLite.Execute('update QEC_LOCATIONS set TEAM_NBR=NULL where TEAM_NBR=0');

  // fixing the problem when there is a record in PR_CHECK_LOCALS but not in the BLOB field
  SQLite.Execute('create table CHECK_LOCALS as'+
  ' select EE_LOCALS_NBR, SUM(LOCAL_GROSS_WAGES) LOCAL_GROSS_WAGES,'+
  ' SUM(LOCAL_TAXABLE_WAGE) LOCAL_TAXABLE_WAGES, SUM(LOCAL_TAX) LOCAL_TAX'+
  ' from PR_CHECK_LOCALS where EE_LOCALS_NBR in (select EE_LOCALS_NBR from QEC_LOCATIONS)'+
  ' group by 1'+
  ' order by 1');

  SQLite.Execute('create table CHECK_LOCALS1 as'+
  ' select EE_LOCALS_NBR,'+
  ' SUM(LOCAL_GROSS_WAGES) LOCAL_GROSS_WAGES,'+
  ' SUM(LOCAL_TAXABLE_WAGES) LOCAL_TAXABLE_WAGES,'+
  ' SUM(LOCAL_TAX) LOCAL_TAX'+
  ' from PAYROLL_LOCATIONS'+
  ' group by 1'+
  ' order by 1');

  SQLite.Execute('create table CHECK_LOCALS_FIX as'+
  ' select c.EE_LOCALS_NBR,'+
  ' ifnull(c1.LOCAL_GROSS_WAGES, 0) - c.LOCAL_GROSS_WAGES LOCAL_GROSS_WAGES,'+
  ' ifnull(c1.LOCAL_TAXABLE_WAGES, 0) - c.LOCAL_TAXABLE_WAGES LOCAL_TAXABLE_WAGES,'+
  ' ifnull(c1.LOCAL_TAX, 0) - c.LOCAL_TAX LOCAL_TAX'+
  ' from CHECK_LOCALS c'+
  ' left join CHECK_LOCALS1 c1 on c1.EE_LOCALS_NBR=c.EE_LOCALS_NBR '+
  ' where c1.LOCAL_TAX is null');

  PaStored := SQLite.Select('select * from CHECK_LOCALS_FIX', []);

//  SQLite.Execute('drop table CHECK_LOCALS1');
//  SQLite.Execute('drop table CHECK_LOCALS_FIX');

  PaStored.First;
  while not PaStored.Eof do
  begin
    DS := SQLite.Select(
           ' select PR_CHECK_LINES_NBR, EE_LOCALS_NBR'+
           ' from QEC_LOCATIONS'+
           ' where EE_LOCALS_NBR='+ PaStored.FieldByName('EE_LOCALS_NBR').AsString +
           ' order by abs(LOCAL_TAX) desc'+
           ' limit 1', []);

    if DS.RecordCount > 0 then
    begin
      SQLite.Execute(
      ' update QEC_LOCATIONS set LOCAL_TAX=LOCAL_TAX+'+FloatToStr(StrToFloatDef(PaStored.FieldByName('LOCAL_TAX').AsString,0))+', '+
      ' LOCAL_TAXABLE_WAGES=LOCAL_TAXABLE_WAGES+'+FloatToStr(StrToFloatDef(PaStored.FieldByName('LOCAL_TAXABLE_WAGES').AsString,0))+', '+
      ' LOCAL_GROSS_WAGES=LOCAL_GROSS_WAGES+'+FloatToStr(StrToFloatDef(PaStored.FieldByName('LOCAL_GROSS_WAGES').AsString,0))+
      ' where PR_CHECK_LINES_NBR='+DS.FieldByName('PR_CHECK_LINES_NBR').AsString+' and '+
      ' EE_LOCALS_NBR='+DS.FieldByName('EE_LOCALS_NBR').AsString+' limit 1');
    end
    else
    begin
//      SQLite.InsertRecord('QEC_LOCATIONS', PaStored.vclDataset);
    end;

    PaStored.Next;
  end;

  SQLite.Execute('create table QEC_PR_CHECK_LOCALS as '+
  ' select c.EE_NBR EE_NBR, l.EE_LOCALS_NBR EE_LOCALS_NBR, x.CO_LOCAL_TAX_NBR CO_LOCAL_TAX_NBR, SUM(l.LOCAL_TAX) LOCAL_TAX, '+
  ' SUM(l.LOCAL_TAXABLE_WAGES) LOCAL_TAXABLE_WAGES, SUM(l.LOCAL_GROSS_WAGES) LOCAL_GROSS_WAGES '+
  ' from QEC_LOCATIONS l join PR_CHECK c on c.PR_CHECK_NBR=l.PR_CHECK_NBR join PR p on p.PR_NBR=c.PR_NBR '+
  ' join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE join EE_LOCALS x on x.CHECK_DATE=d.REALDATE and x.EE_LOCALS_NBR=l.EE_LOCALS_NBR '+
  ' group by 1, 2, 3 '+
  ' having abs(SUM(l.LOCAL_TAX)) > 0.005 or abs(SUM(l.LOCAL_TAXABLE_WAGES)) > 0.005 or abs(SUM(l.LOCAL_GROSS_WAGES)) > 0.005'+
  ' order by 1 , 2 ,3');

  DS := SQLite.Select(
  '  select PR_CHECK_NBR, EE_LOCALS_NBR, NONRES_EE_LOCALS_NBR,'+
  '  SUM(LOCAL_TAX), SUM(LOCAL_TAXABLE_WAGES), SUM(LOCAL_GROSS_WAGES)'+
  '  from QEC_LOCATIONS'+
  '  group by 1,2,3'+
  '  having SUM(LOCAL_TAX)=0 and SUM(LOCAL_TAXABLE_WAGES)=0 and SUM(LOCAL_GROSS_WAGES)=0'+
  '  order by 1,2,3', []);

  DS.First;
  while not DS.Eof do
  begin
    SQLite.Execute('delete from QEC_LOCATIONS where '+
    '  PR_CHECK_NBR='+DS.FieldByName('PR_CHECK_NBR').AsString+
    '  and EE_LOCALS_NBR='+DS.FieldByName('EE_LOCALS_NBR').AsString+
    '  and NONRES_EE_LOCALS_NBR='+DS.FieldByName('NONRES_EE_LOCALS_NBR').AsString);
    DS.Next;
  end;

   SQLite.Execute(' create table NOT_TO_ADJUST as'+
  '  select l.PR_CHECK_NBR, l.EE_LOCALS_NBR'+
  '  from QEC_LOCATIONS l'+
  '  join PR p on p.PR_NBR=l.PR_NBR'+
  '  join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
  '  join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR'+
  '  where c.SELF_ADJUST_TAX = ''N'''+
  '  and p.PAYROLL_TYPE<>'''+PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT+'''' +
  '  union '+
  '  select l.PR_CHECK_NBR, l.EE_LOCALS_NBR'+
  '  from PR_CHECK_LOCALS l'+
  '  join PR_CHECK k on k.PR_CHECK_NBR=l.PR_CHECK_NBR'+
  '  join PR p on p.PR_NBR=k.PR_NBR'+
  '  join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
  '  join EE_LOCALS e on e.CHECK_DATE=d.REALDATE and e.EE_LOCALS_NBR=l.EE_LOCALS_NBR'+
  '  join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=e.CO_LOCAL_TAX_NBR'+
  '  where c.SELF_ADJUST_TAX = ''N'''+
  '  and p.PAYROLL_TYPE<>'''+PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT+''''
  );

  SQLite.Execute(' update QEC_LOCATIONS set LOCAL_TAX=0'+
  '  where exists(select 1 from NOT_TO_ADJUST x'+
  '  where x.PR_CHECK_NBR=QEC_LOCATIONS.PR_CHECK_NBR'+
  '  and x.EE_LOCALS_NBR=QEC_LOCATIONS.EE_LOCALS_NBR)');

  SQLite.Execute(' create table QEC_LIABILITIES as'+
  '  select'+
  '  l.CO_LOCAL_TAX_NBR CO_LOCAL_TAX_NBR,'+
  '  (select MAX(CO_LOCAL_TAX_NBR) from EE_LOCALS e where e.EE_LOCALS_NBR=l.NONRES_EE_LOCALS_NBR) NONRES_CO_LOCAL_TAX_NBR,'+
  '  (select MAX(SY_LOCALS_NBR) from CO_LOCAL_TAX k where k.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR) SY_LOCAL_TAX_NBR,'+
  '  COMBINE_FOR_TAX_PAYMENTS,'+
  '  SUM(l.LOCAL_TAX) LOCAL_TAX,'+
  '  SUM(l.LOCAL_GROSS_WAGES) LOCAL_GROSS_WAGES,'+
  '  SUM(l.LOCAL_TAXABLE_WAGES) LOCAL_TAXABLE_WAGES'+
  '  from QEC_LOCATIONS l'+
  '  join SY_LOCALS s on s.SY_LOCALS_NBR='+
  '  (select MAX(SY_LOCALS_NBR) from CO_LOCAL_TAX k where k.CO_LOCAL_TAX_NBR=l.CO_LOCAL_TAX_NBR)'+
  '  and s.CHECK_DATE=(select MAX(REALDATE) from CHECK_DATES)'+
  '  group by 1,2,3,4'+
  '  order by 1,2,3,4');

  SQLite.Execute(' update QEC_LIABILITIES set'+
  '  LOCAL_TAX=round(LOCAL_TAX,2),'+
  '  LOCAL_GROSS_WAGES=round(LOCAL_GROSS_WAGES,2),'+
  '  LOCAL_TAXABLE_WAGES=round(LOCAL_TAXABLE_WAGES,2)');

  SQLite.Execute('create table NEW_QEC_LOCATIONS1 as '+
  '  select u.EE_NBR EE_NBR, b.PR_CHECK_NBR PR_CHECK_NBR,'+
  '  b.EE_LOCALS_NBR EE_LOCALS_NBR,'+
  '  s.SY_LOCAL_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR,'+
  '  b.LOCAL_GROSS_WAGES LOCAL_GROSS_WAGES,'+
  '  case when b.ISRES=''N'' then b.EE_LOCALS_NBR else b.NONRES_EE_LOCALS_NBR end NONRES_EE_LOCALS_NBR,'+
  '  b.ISLOSER ISLOSER,'+
  '  b.LOCAL_TAXABLE_WAGES LOCAL_TAXABLE_WAGES,'+
  '  b.LOCAL_TAX LOCAL_TAX,'+
  '  case'+
  '  when b.LOCATION_LEVEL = ''E'' then case when b.ISRES=''N'' then o.CO_LOCATIONS_NBR else n.CO_LOCATIONS_NBR end '+
  '  when ifnull(j.CO_LOCATIONS_NBR,0) <> 0 then j.CO_LOCATIONS_NBR'+
  '  when ifnull(t.CO_LOCATIONS_NBR,0) <> 0 then t.CO_LOCATIONS_NBR'+
  '  when ifnull(x.CO_LOCATIONS_NBR,0) <> 0 then x.CO_LOCATIONS_NBR'+
  '  when ifnull(v.CO_LOCATIONS_NBR,0) <> 0 then v.CO_LOCATIONS_NBR'+
  '  when ifnull(z.CO_LOCATIONS_NBR,0) <> 0 then z.CO_LOCATIONS_NBR'+
  '  else k.CO_LOCATIONS_NBR'+
  '  end CO_LOCATIONS_NBR,'+
  '  c1.SY_LOCALS_NBR'+
  '  from QEC_LOCATIONS b'+
  '  join PR p on p.PR_NBR=b.PR_NBR'+
  '  join PR_CHECK u on u.PR_CHECK_NBR=b.PR_CHECK_NBR'+
  '  join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
  '  join EE_LOCALS o on o.CHECK_DATE=d.REALDATE and o.EE_LOCALS_NBR=b.EE_LOCALS_NBR'+
  '  join CO_LOCAL_TAX c1 on c1.CHECK_DATE=d.REALDATE and c1.CO_LOCAL_TAX_NBR=o.CO_LOCAL_TAX_NBR'+
  '  left join EE_LOCALS n on n.CHECK_DATE=d.REALDATE and n.EE_LOCALS_NBR=b.NONRES_EE_LOCALS_NBR'+
  '  left join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=n.CO_LOCAL_TAX_NBR'+
  '  left join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
  '  left join CO k on k.CHECK_DATE=d.REALDATE and k.CO_NBR=p.CO_NBR'+
  '  left join EE e on e.EE_NBR=n.EE_NBR'+
  '  left join CO_DIVISION z on z.CO_DIVISION_NBR=b.DIVISION_NBR'+
  '  left join CO_BRANCH v on v.CO_BRANCH_NBR=b.BRANCH_NBR'+
  '  left join CO_DEPARTMENT x on x.CO_DEPARTMENT_NBR=b.DEPARTMENT_NBR'+
  '  left join CO_TEAM t on t.CO_TEAM_NBR=b.TEAM_NBR'+
  '  left join CO_JOBS j on j.CO_JOBS_NBR=b.JOBS_NBR'
  );

  SQLite.Execute('update NEW_QEC_LOCATIONS1 set CO_LOCATIONS_NBR=0, NONRES_EE_LOCALS_NBR=0, ISLOSER=''N'' where SY_LOCALS_NBR in (921,922)');

  SQLite.Execute('create table NEW_QEC_LOCATIONS11 as '+
  '  select'+
  '  b.PR_CHECK_NBR PR_CHECK_NBR,'+
  '  b.EE_LOCALS_NBR EE_LOCALS_NBR,'+
  '  case'+
  '  when b.LOCATION_LEVEL = ''E'' then case when b.ISRES=''N'' then o.CO_LOCATIONS_NBR else n.CO_LOCATIONS_NBR end'+
  '  when ifnull(j.CO_LOCATIONS_NBR,0) <> 0 then j.CO_LOCATIONS_NBR'+
  '  when ifnull(t.CO_LOCATIONS_NBR,0) <> 0 then t.CO_LOCATIONS_NBR'+
  '  when ifnull(x.CO_LOCATIONS_NBR,0) <> 0 then x.CO_LOCATIONS_NBR'+
  '  when ifnull(v.CO_LOCATIONS_NBR,0) <> 0 then v.CO_LOCATIONS_NBR'+
  '  when ifnull(z.CO_LOCATIONS_NBR,0) <> 0 then z.CO_LOCATIONS_NBR'+
  '  else k.CO_LOCATIONS_NBR'+
  '  end CO_LOCATIONS_NBR,'+
  '  case when b.ISRES=''N'' then b.EE_LOCALS_NBR else b.NONRES_EE_LOCALS_NBR end NONRES_EE_LOCALS_NBR,'+
  '  b.TAXABLE_WAGE LOCAL_TAXABLE_WAGES,'+
  '  b.TAX LOCAL_TAX'+
  '  from QEC_PA_LOCAL_TAXES b'+
  '  join PR p on p.PR_NBR=b.PR_NBR'+
  '  join PR_CHECK u on u.PR_CHECK_NBR=b.PR_CHECK_NBR'+
  '  join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
  '  join EE_LOCALS o on o.CHECK_DATE=d.REALDATE and o.EE_LOCALS_NBR=b.EE_LOCALS_NBR'+
  '  left join EE_LOCALS n on n.CHECK_DATE=d.REALDATE and n.EE_LOCALS_NBR=b.NONRES_EE_LOCALS_NBR'+
  '  left join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=n.CO_LOCAL_TAX_NBR'+
  '  left join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
  '  left join CO k on k.CHECK_DATE=d.REALDATE and k.CO_NBR=p.CO_NBR'+
  '  left join EE e on e.EE_NBR=n.EE_NBR'+
  '  left join CO_DIVISION z on z.CO_DIVISION_NBR=b.CO_DIVISION_NBR'+
  '  left join CO_BRANCH v on v.CO_BRANCH_NBR=b.CO_BRANCH_NBR'+
  '  left join CO_DEPARTMENT x on x.CO_DEPARTMENT_NBR=b.CO_DEPARTMENT_NBR'+
  '  left join CO_TEAM t on t.CO_TEAM_NBR=b.CO_TEAM_NBR'+
  '  left join CO_JOBS j on j.CO_JOBS_NBR=b.CO_JOBS_NBR'+
  '  where b.ISLOSER=''N'' and (abs(b.TAX) > 0.009 or abs(b.TAXABLE_WAGE) > 0.009)'
  );


  SQLite.Execute('create table NEW_QEC_LOCATIONS2 as'+
  '  select'+
  '  EE_NBR,'+
  '  EE_LOCALS_NBR,'+
  '  CO_LOCATIONS_NBR,'+
  '  case when ISLOSER = ''Y'' then ''N'' else ''Y'' end REPORTABLE,'+
  '  NONRES_EE_LOCALS_NBR,'+
  '  sum(LOCAL_GROSS_WAGES) LOCAL_GROSS_WAGES,'+
  '  sum(LOCAL_TAXABLE_WAGES) LOCAL_TAXABLE_WAGES,'+
  '  sum(LOCAL_TAX) LOCAL_TAX'+
  '  from NEW_QEC_LOCATIONS1'+
  '  group by 1,2,3,4,5'+
  '  order by 1,2,3,4,5');

  SQLite.Execute('insert into NEW_QEC_LOCATIONS2'+
  '  select c.EE_NBR, x.EE_LOCALS_NBR,'+
  '  case when s.SY_LOCALS_NBR in (921,922) then 0 else x.CO_LOCATIONS_NBR end,'+
  '  case when s.SY_LOCALS_NBR in (921,922) then ''Y'' else x.REPORTABLE end,'+
  '  case when s.SY_LOCALS_NBR in (921,922) then 0 else x.NONRES_EE_LOCALS_NBR end,'+
  '  -x.LOCAL_GROSS_WAGES,'+
  '  -x.LOCAL_TAXABLE_WAGE LOCAL_TAXABLE_WAGES,'+
  '  case when exists(select 1 from NOT_TO_ADJUST a where a.PR_CHECK_NBR=x.PR_CHECK_NBR and a.EE_LOCALS_NBR=x.EE_LOCALS_NBR) then 0 else '+
  '  -x.LOCAL_TAX end LOCAL_TAX'+
  '  from PR_CHECK_LOCALS x'+
  '  join PR_CHECK c on c.PR_CHECK_NBR=x.PR_CHECK_NBR'+
  '  join PR p on p.PR_NBR=c.PR_NBR'+
  '  join CHECK_DATES d on d.CHECK_DATE=p.CHECK_DATE'+
  '  join EE_LOCALS n on n.CHECK_DATE=d.REALDATE and n.EE_LOCALS_NBR=x.EE_LOCALS_NBR'+
  '  join CO_LOCAL_TAX c on c.CHECK_DATE=d.REALDATE and c.CO_LOCAL_TAX_NBR=n.CO_LOCAL_TAX_NBR'+
  '  join SY_LOCALS s on s.CHECK_DATE=d.REALDATE and s.SY_LOCALS_NBR=c.SY_LOCALS_NBR and s.LOCAL_TYPE in (''R'',''N'',''S'')'+
  '  join SY_STATES i on i.CHECK_DATE=d.REALDATE and i.SY_STATES_NBR=s.SY_STATES_NBR and i.STATE=''PA'' '
  );

  SQLite.Execute('update NEW_QEC_LOCATIONS2 set NONRES_EE_LOCALS_NBR=0 where NONRES_EE_LOCALS_NBR is null');

  SQLite.Execute('create table NEW_QEC_LOCATIONS as'+
  '  select EE_NBR, EE_LOCALS_NBR, CO_LOCATIONS_NBR,'+
  '  REPORTABLE, NONRES_EE_LOCALS_NBR,'+
  '  sum(LOCAL_GROSS_WAGES) LOCAL_GROSS_WAGES,'+
  '  sum(LOCAL_TAXABLE_WAGES) LOCAL_TAXABLE_WAGES,'+
  '  sum(LOCAL_TAX) LOCAL_TAX'+
  '  from NEW_QEC_LOCATIONS2'+
  '  group by 1,2,3,4,5'+
  '  order by 1,2,3,4,5');

  SQLite.Execute('delete from NEW_QEC_LOCATIONS where abs(LOCAL_TAX)<0.005 and abs(LOCAL_TAXABLE_WAGES)<0.005 and abs(LOCAL_GROSS_WAGES)<0.005');

end;

procedure CreateIndexes(const SQLite: ISQLite);
begin
  SQLite.Execute('CREATE INDEX IF NOT EXISTS EE_LOCALS_IDX ON EE_LOCALS(CHECK_DATE, EE_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS SY_STATE_EXEMPTIONS_IDX ON SY_STATE_EXEMPTIONS(CHECK_DATE, SY_STATES_NBR, E_D_CODE_TYPE)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS SY_LOCAL_EXEMPTIONS_IDX ON SY_LOCAL_EXEMPTIONS(CHECK_DATE, SY_LOCALS_NBR, E_D_CODE_TYPE)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_LOCAL_TAX_IDX ON CO_LOCAL_TAX(CHECK_DATE, CO_LOCAL_TAX_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_SUI_IDX ON CO_SUI(CHECK_DATE, CO_SUI_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CL_IDX ON CL(CHECK_DATE)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CHECK_DATES_IDX ON CHECK_DATES(CHECK_DATE)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS TAXABLE_TYPES_IDX ON TAXABLE_TYPES(E_D_CODE_TYPE)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS PR_CHECK_LINES_IDX ON PR_CHECK_LINES(PR_CHECK_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CL_PERSON_IDX ON CL_PERSON(CL_PERSON_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS SY_LOCALS_IDX ON SY_LOCALS(CHECK_DATE, SY_LOCALS_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS EE_IDX ON EE(EE_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS PR_CHECK_IDX ON PR_CHECK(PR_CHECK_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CL_E_DS_IDX ON CL_E_DS(CHECK_DATE, CL_E_DS_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_IDX ON CO(CHECK_DATE, CO_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS PR_CHECK_LINE_LOCALS_IDX ON PR_CHECK_LINE_LOCALS(PR_CHECK_LINES_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS SY_STATES_IDX ON SY_STATES(CHECK_DATE, SY_STATES_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_JOBS_LOCALS_IDX ON CO_JOBS_LOCALS(CHECK_DATE, CO_JOBS_NBR, CO_LOCAL_TAX_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_TEAM_LOCALS_IDX ON CO_TEAM_LOCALS(CHECK_DATE, CO_TEAM_NBR, CO_LOCAL_TAX_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_DEPARTMENT_LOCALS_IDX ON CO_DEPARTMENT_LOCALS(CHECK_DATE, CO_DEPARTMENT_NBR, CO_LOCAL_TAX_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_BRANCH_LOCALS_IDX ON CO_BRANCH_LOCALS(CHECK_DATE, CO_BRANCH_NBR, CO_LOCAL_TAX_NBR)');
  SQLite.Execute('CREATE INDEX IF NOT EXISTS CO_DIVISION_LOCALS_IDX ON CO_DIVISION_LOCALS(CHECK_DATE, CO_DIVISION_NBR, CO_LOCAL_TAX_NBR)');
end;

end.



