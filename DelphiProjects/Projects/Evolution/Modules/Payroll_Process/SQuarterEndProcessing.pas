// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQuarterEndProcessing;

interface

uses
  SysUtils, Variants, EvBasicUtils, EvContext, EvExceptions, EvClientDataSet;

procedure DoERTaxAdjPayroll(aDate: TDateTime);
procedure DoQtrEndTaxLiabAdj(CoNbr: Integer; aDate: TDateTime; MinLiability: Double);
procedure CreateTaxAdjCheck(aDate: TDateTime);

implementation

uses
  EvUtils, EvConsts, EvTypes, SDataStructure,  EvDataAccessComponents;

procedure CreateTaxAdjCheck(aDate: TDateTime);
begin
  with DM_PAYROLL do
  begin
    if PR.RecordCount = 0 then
    begin
      PR.Insert;
      PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
      PR.FieldByName('CHECK_DATE').Value := aDate;
      PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      PR.FieldByName('SCHEDULED').Value := 'N';
      PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT;
      PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
      PR.FieldByName('EXCLUDE_ACH').Value := 'N';
      PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
      PR.FieldByName('EXCLUDE_AGENCY').Value := 'Y';
      PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
      PR.FieldByName('EXCLUDE_BILLING').Value := 'Y';
      PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_BOTH2;
      PR.Post;
    end;

    if not PR_BATCH.Locate('FREQUENCY', DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value, []) then
    begin
      PR_BATCH.Insert;
      PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := aDate;
      PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      PR_BATCH.FieldByName('PERIOD_END_DATE').Value := aDate;
      PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_IGNORE;
      PR_BATCH.FieldByName('FREQUENCY').Value := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value;
      PR_BATCH.FieldByName('PAY_SALARY').Value := 'N';
      PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N';
      PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'N';
      PR_BATCH.Post;
    end;

    if not PR_CHECK.Locate('EE_NBR', DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, []) then
    begin
      PR_CHECK.Insert;
      PR_CHECK.FieldByName('EE_NBR').Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value;
      PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_MANUAL;
      PR_CHECK.FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;
      ctx_PayrollCalculation.AssignCheckDefaults(0, 0);
      ctx_DataAccess.TemplateNumber := 0;
      PR_CHECK.Post;
    end;
  end;
end;

procedure DoERTaxAdjPayroll(aDate: TDateTime);
var
  BeginQuarter, EndQuarter: TDateTime;
  OldTax, NewTax, FUIRate: Double;
  CUSTOM_VIEW_2, HISTORY_VIEW_2: TevClientDataSet;
  MyTransactionManager: TTransactionManager;
  State: string;
  SystemSUINbr: Integer;
  Log: string;
begin
  BeginQuarter := GetBeginQuarter(aDate);
  EndQuarter := GetEndQuarter(aDate);
  with DM_PAYROLL, DM_COMPANY, DM_EMPLOYEE do
  begin
    CUSTOM_VIEW_2 := TevClientDataSet.Create(nil);
    HISTORY_VIEW_2 := TevClientDataSet.Create(nil);
    MyTransactionManager := TTransactionManager.Create(nil);
    try
      MyTransactionManager.Initialize([PR, PR_BATCH, PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI]);
      PR.DataRequired('PR_NBR=0');
      PR_BATCH.DataRequired('PR_BATCH_NBR=0');
      PR_CHECK.DataRequired('PR_CHECK_NBR=0');
      PR_CHECK_STATES.DataRequired('PR_CHECK_STATES_NBR=0');
      PR_CHECK_SUI.DataRequired('PR_CHECK_SUI_NBR=0');

      DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
      DM_SYSTEM_STATE.SY_STATES.Activate;
      DM_SYSTEM_STATE.SY_SUI.Activate;

      EE.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      CO_STATES.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      CO_SUI.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);

// Look for federal ER discrepancies
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ListOtherWithShortfalls') do
      begin
        SetMacro('Columns', 'P.CHECK_DATE, C.EE_NBR, (C.ER_OASDI_TAXABLE_WAGES+C.ER_OASDI_TAXABLE_TIPS) '
          +
            'ER_OASDI_TAXABLE_WAGES, C.ER_OASDI_TAX, C.ER_MEDICARE_TAXABLE_WAGES, C.ER_MEDICARE_TAX, C.ER_FUI_TAXABLE_WAGES, C.ER_FUI_TAX');
        SetParam('StartDate', BeginQuarter);
        SetParam('EndDate', EndQuarter);
        SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        SetMacro('Group', 'C.EE_NBR, P.CHECK_DATE');
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      ctx_DataAccess.HISTORY_VIEW.Close;
      ctx_DataAccess.HISTORY_VIEW.DataRequest('AUDIT=CO_PROV,NO=' + CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.HISTORY_VIEW.Open;
      HISTORY_VIEW_2.Data := ctx_DataAccess.HISTORY_VIEW.Data;
      HISTORY_VIEW_2.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';

      ctx_DataAccess.HISTORY_VIEW.Close;
      ctx_DataAccess.HISTORY_VIEW.DataRequest('AUDIT=SY_FED_TAX_TABLE_PROV,NO=' +
        DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('SY_FED_TAX_TABLE_NBR').AsString);
      ctx_DataAccess.HISTORY_VIEW.Open;
      ctx_DataAccess.HISTORY_VIEW.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';

      EE.First;
      CUSTOM_VIEW_2.Filtered := True;
      while not EE.EOF do
      begin
        CUSTOM_VIEW_2.Filter := 'EE_NBR=' + EE.FieldByName('EE_NBR').AsString;
// ER OASDI
        OldTax := 0;
        NewTax := 0;
        CUSTOM_VIEW_2.First;
        ctx_DataAccess.HISTORY_VIEW.First;
        while not CUSTOM_VIEW_2.EOF do
        begin
          while (ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
          and (not ctx_DataAccess.HISTORY_VIEW.EOF) do
            ctx_DataAccess.HISTORY_VIEW.Next;
          if ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
            ctx_DataAccess.HISTORY_VIEW.Prior;
          NewTax := NewTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_OASDI_TAXABLE_WAGES').Value, 0) *
            ConvertNull(ctx_DataAccess.HISTORY_VIEW.FieldByName('ER_OASDI_RATE').Value, 0);
          OldTax := OldTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_OASDI_TAX').Value, 0);
          CUSTOM_VIEW_2.Next;
        end;
        if Abs(NewTax - OldTax) > TaxShortfallPickupLimit then
        begin
          CreateTaxAdjCheck(aDate);
          PR_CHECK.Edit;
          PR_CHECK.FieldByName('ER_OASDI_TAX').Value := NewTax - OldTax;
          PR_CHECK.Post;
        end;
// ER Medicare
        OldTax := 0;
        NewTax := 0;
        CUSTOM_VIEW_2.First;
        ctx_DataAccess.HISTORY_VIEW.First;
        while not CUSTOM_VIEW_2.EOF do
        begin
          while (ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
          and (not ctx_DataAccess.HISTORY_VIEW.EOF) do
            ctx_DataAccess.HISTORY_VIEW.Next;
          if ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
            ctx_DataAccess.HISTORY_VIEW.Prior;
          NewTax := NewTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_MEDICARE_TAXABLE_WAGES').Value, 0) *
            ConvertNull(ctx_DataAccess.HISTORY_VIEW.FieldByName('MEDICARE_RATE').Value, 0);
          OldTax := OldTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_MEDICARE_TAX').Value, 0);
          CUSTOM_VIEW_2.Next;
        end;
        if Abs(NewTax - OldTax) > TaxShortfallPickupLimit then
        begin
          CreateTaxAdjCheck(aDate);
          PR_CHECK.Edit;
          PR_CHECK.FieldByName('ER_MEDICARE_TAX').Value := NewTax - OldTax;
          PR_CHECK.Post;
        end;
// ER FUI
        OldTax := 0;
        NewTax := 0;
        CUSTOM_VIEW_2.First;
        ctx_DataAccess.HISTORY_VIEW.First;
        HISTORY_VIEW_2.First;
        while not CUSTOM_VIEW_2.EOF do
        begin
          while (ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
          and (not ctx_DataAccess.HISTORY_VIEW.EOF) do
            ctx_DataAccess.HISTORY_VIEW.Next;
          if ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
            ctx_DataAccess.HISTORY_VIEW.Prior;

          while (HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
          and (not HISTORY_VIEW_2.EOF) do
            HISTORY_VIEW_2.Next;
          if HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
            HISTORY_VIEW_2.Prior;

          if not HISTORY_VIEW_2.FieldByName('FUI_RATE_OVERRIDE').IsNull then
            FUIRate := ctx_DataAccess.HISTORY_VIEW.FieldByName('FUI_RATE_REAL').Value - HISTORY_VIEW_2.FieldByName('FUI_RATE_OVERRIDE').Value
              {This is actually FUI credit override}
          else
            FUIRate := ctx_DataAccess.HISTORY_VIEW.FieldByName('FUI_RATE_REAL').Value - ctx_DataAccess.HISTORY_VIEW.FieldByName('FUI_RATE_CREDIT').Value;

          NewTax := NewTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_FUI_TAXABLE_WAGES').Value, 0) * FUIRate;
          OldTax := OldTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_FUI_TAX').Value, 0);
          CUSTOM_VIEW_2.Next;
        end;
        if Abs(NewTax - OldTax) > TaxShortfallPickupLimit then
        begin
          CreateTaxAdjCheck(aDate);
          PR_CHECK.Edit;
          PR_CHECK.FieldByName('ER_FUI_TAX').Value := NewTax - OldTax;
          PR_CHECK.Post;
        end;

        EE.Next;
      end;
      CUSTOM_VIEW_2.Filtered := False;
      CUSTOM_VIEW_2.Filter := '';

// Look for ER SDI discrepancies
      EE_STATES.DataRequired('ALL');

      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('SDIDiscrepancies') do
      begin
        SetParam('StartDate', BeginQuarter);
        SetParam('EndDate', EndQuarter);
        SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
        ctx_DataAccess.SY_CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelect') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'SY_STATES');
        ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.SY_CUSTOM_VIEW.Open;
      HISTORY_VIEW_2.Close;
      HISTORY_VIEW_2.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;

      EE.First;
      CUSTOM_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';
      EE_STATES.Filtered := True;
      while not EE.EOF do
      begin
        EE_STATES.Filter := 'EE_NBR=' + EE.FieldByName('EE_NBR').AsString;
        EE_STATES.First;
        while not EE_STATES.EOF do
        begin
          CUSTOM_VIEW_2.Filter := 'EE_STATES_NBR=' + EE_STATES.FieldByName('EE_STATES_NBR').AsString;
          State := CO_STATES.Lookup('CO_STATES_NBR', EE_STATES.FieldByName('CO_STATES_NBR').Value, 'STATE');
          HISTORY_VIEW_2.Filter := 'STATE=''' + State + '''';
          OldTax := 0;
          NewTax := 0;
          CUSTOM_VIEW_2.First;
          HISTORY_VIEW_2.First;
          while not CUSTOM_VIEW_2.EOF do
          begin
            while (HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
            and (not HISTORY_VIEW_2.EOF) do
              HISTORY_VIEW_2.Next;
            if HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
              HISTORY_VIEW_2.Prior;
              
            NewTax := NewTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_SDI_TAXABLE_WAGES').Value, 0) *
              ConvertNull(HISTORY_VIEW_2.FieldByName('ER_SDI_RATE').Value, 0);
            OldTax := OldTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_SDI_TAX').Value, 0);
            CUSTOM_VIEW_2.Next;
          end;
          if Abs(NewTax - OldTax) > TaxShortfallPickupLimit then
          begin
            CreateTaxAdjCheck(aDate);
            PR_CHECK_STATES.Insert;
            PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value := EE_STATES.FieldByName('EE_STATES_NBR').Value;
            PR_CHECK_STATES.FieldByName('ER_SDI_TAX').Value := NewTax - OldTax;
            PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').Value := 'N';
            PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE;
            PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').Value := 'N';
            PR_CHECK_STATES.Post;
          end;

          EE_STATES.Next;
        end;

        EE.Next;
      end;
      EE_STATES.Filtered := False;
      EE_STATES.Filter := '';
      CUSTOM_VIEW_2.Filtered := False;
      CUSTOM_VIEW_2.Filter := '';
      HISTORY_VIEW_2.Filtered := False;
      HISTORY_VIEW_2.Filter := '';

// Look for ER SUI discrepancies
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('SUIDiscrepancies') do
      begin
        SetParam('StartDate', BeginQuarter);
        SetParam('EndDate', EndQuarter);
        SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelect') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'CO_SUI');
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      HISTORY_VIEW_2.Close;
      HISTORY_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      EE.First;
      CUSTOM_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';
      while not EE.EOF do
      begin
        CO_SUI.First;
        while not CO_SUI.EOF do
        begin
          CUSTOM_VIEW_2.Filter := 'EE_NBR=' + EE.FieldByName('EE_NBR').AsString + ' and CO_SUI_NBR=' +
            CO_SUI.FieldByName('CO_SUI_NBR').AsString;
          SystemSUINbr := CO_SUI.FieldByName('CO_SUI_NBR').Value;
          if (CUSTOM_VIEW_2.RecordCount > 0) and Is_ER_SUI(VarToStr(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', SystemSUINbr,
            'EE_ER_OR_EE_OTHER_OR_ER_OTHER'))) then
          begin
            HISTORY_VIEW_2.Filter := 'CO_SUI_NBR=' + CO_SUI.FieldByName('CO_SUI_NBR').AsString;
            OldTax := 0;
            NewTax := 0;
            CUSTOM_VIEW_2.First;
            HISTORY_VIEW_2.First;
            while not CUSTOM_VIEW_2.EOF do
            begin
              while (HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
              and (not HISTORY_VIEW_2.EOF) do
                HISTORY_VIEW_2.Next;
              if HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
                HISTORY_VIEW_2.Prior;

              NewTax := NewTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('SUI_TAXABLE_WAGES').Value, 0) *
                ConvertNull(HISTORY_VIEW_2.FieldByName('RATE').Value, 0);
              OldTax := OldTax + ConvertNull(CUSTOM_VIEW_2.FieldByName('SUI_TAX').Value, 0);
              CUSTOM_VIEW_2.Next;
            end;
            if Abs(NewTax - OldTax) > TaxShortfallPickupLimit then
            begin
              CreateTaxAdjCheck(aDate);
              PR_CHECK_SUI.Insert;
              PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value := CO_SUI.FieldByName('CO_SUI_NBR').Value;
              PR_CHECK_SUI.FieldByName('SUI_TAX').Value := NewTax - OldTax;
              PR_CHECK_SUI.Post;
            end;
          end;

          CO_SUI.Next;
        end;

        EE.Next;
      end;
      CUSTOM_VIEW_2.Filtered := False;
      CUSTOM_VIEW_2.Filter := '';
      HISTORY_VIEW_2.Filtered := False;
      HISTORY_VIEW_2.Filter := '';

      MyTransactionManager.ApplyUpdates;
    finally
      MyTransactionManager.Free;
      HISTORY_VIEW_2.Free;
      CUSTOM_VIEW_2.Free;
    end;

    if PR.RecordCount <> 0 then
    begin
      ctx_PayrollProcessing.ProcessPayroll(PR.FieldByName('PR_NBR').AsInteger, False, Log, True);
    end;

    PR.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
  end;
end;

procedure DoQtrEndTaxLiabAdj(CoNbr: Integer; aDate: TDateTime; MinLiability: Double);
var
  BeginQuarter, EndQuarter: TDateTime;
  OldOASDI, NewOASDI, OldMedicare, NewMedicare, OldFUI, NewFUI, OldSDI, NewSDI, OldSUI, NewSUI, FUIRate: Double;
  CUSTOM_VIEW_2, HISTORY_VIEW_2: TevClientDataSet;
  MyTransactionManager: TTransactionManager;
  State: string;
  SystemSUINbr: Integer;
begin
  BeginQuarter := GetBeginQuarter(aDate);
  EndQuarter := GetEndQuarter(aDate);
  with DM_PAYROLL, DM_COMPANY do
  begin
    CO_FED_TAX_LIABILITIES.DataRequired('CO_NBR=' + IntToStr(CoNbr));
    CO_STATE_TAX_LIABILITIES.DataRequired('CO_NBR=' + IntToStr(CoNbr));
    CO_SUI_LIABILITIES.DataRequired('CO_NBR=' + IntToStr(CoNbr));

    if CO_FED_TAX_LIABILITIES.Locate('DUE_DATE;ADJUSTMENT_TYPE', VarArrayOf([EndQuarter, TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END]), [])
      or
      CO_STATE_TAX_LIABILITIES.Locate('DUE_DATE;ADJUSTMENT_TYPE', VarArrayOf([EndQuarter, TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END]), [])
        or
      CO_SUI_LIABILITIES.Locate('DUE_DATE;ADJUSTMENT_TYPE', VarArrayOf([EndQuarter, TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END]), []) then
    begin
      raise EDuplicatedLiabilityAdjustment.CreateHelp('Tax liability adjustment for this quarter has been already done! Operation aborted.', IDH_CanNotPerformOperation);
    end;

    CUSTOM_VIEW_2 := TevClientDataSet.Create(nil);
    HISTORY_VIEW_2 := TevClientDataSet.Create(nil);
    MyTransactionManager := TTransactionManager.Create(nil);
    try
      MyTransactionManager.Initialize([CO_FED_TAX_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_SUI_LIABILITIES]);

      DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.Activate;
      DM_SYSTEM_STATE.SY_STATES.Activate;
      DM_SYSTEM_STATE.SY_SUI.Activate;

      CO_STATES.DataRequired('CO_NBR=' + IntToStr(CoNbr));
      CO_SUI.DataRequired('CO_NBR=' + IntToStr(CoNbr));

// Look for federal ER discrepancies
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ERFedDiscrepancies') do
      begin
        SetParam('StartDate', BeginQuarter);
        SetParam('EndDate', EndQuarter);
        SetParam('CoNbr', CoNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      ctx_DataAccess.HISTORY_VIEW.Close;
      ctx_DataAccess.HISTORY_VIEW.DataRequest('AUDIT=CO_PROV,NO=' + IntToStr(CoNbr));
      ctx_DataAccess.HISTORY_VIEW.Open;
      HISTORY_VIEW_2.Data := ctx_DataAccess.HISTORY_VIEW.Data;
      HISTORY_VIEW_2.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';

      ctx_DataAccess.HISTORY_VIEW.Close;
      ctx_DataAccess.HISTORY_VIEW.DataRequest('AUDIT=SY_FED_TAX_TABLE_PROV,NO=' +
        DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.FieldByName('SY_FED_TAX_TABLE_NBR').AsString);
      ctx_DataAccess.HISTORY_VIEW.Open;
      ctx_DataAccess.HISTORY_VIEW.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';

      OldOASDI := 0;
      NewOASDI := 0;
      OldMedicare := 0;
      NewMedicare := 0;
      OldFUI := 0;
      NewFUI := 0;

      CUSTOM_VIEW_2.First;
      ctx_DataAccess.HISTORY_VIEW.First;
      while not CUSTOM_VIEW_2.EOF do
      begin
        while (ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
        and (not ctx_DataAccess.HISTORY_VIEW.EOF) do
          ctx_DataAccess.HISTORY_VIEW.Next;
        if ctx_DataAccess.HISTORY_VIEW.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
          ctx_DataAccess.HISTORY_VIEW.Prior;

        while (HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
        and (not HISTORY_VIEW_2.EOF) do
          HISTORY_VIEW_2.Next;
        if HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
          HISTORY_VIEW_2.Prior;

        NewOASDI := NewOASDI + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_OASDI_TAXABLE_WAGES').Value, 0) *
          ConvertNull(ctx_DataAccess.HISTORY_VIEW.FieldByName('ER_OASDI_RATE').Value, 0);
        OldOASDI := OldOASDI + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_OASDI_TAX').Value, 0);

        NewMedicare := NewMedicare + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_MEDICARE_TAXABLE_WAGES').Value, 0) *
          ConvertNull(ctx_DataAccess.HISTORY_VIEW.FieldByName('MEDICARE_RATE').Value, 0);
        OldMedicare := OldMedicare + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_MEDICARE_TAX').Value, 0);

        if not HISTORY_VIEW_2.FieldByName('FUI_RATE_OVERRIDE').IsNull then
          FUIRate := ctx_DataAccess.HISTORY_VIEW.FieldByName('FUI_RATE_REAL').Value - HISTORY_VIEW_2.FieldByName('FUI_RATE_OVERRIDE').Value
            {This is actually FUI credit override}
        else
          FUIRate := ctx_DataAccess.HISTORY_VIEW.FieldByName('FUI_RATE_REAL').Value - ctx_DataAccess.HISTORY_VIEW.FieldByName('FUI_RATE_CREDIT').Value;
        NewFUI := NewFUI + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_FUI_TAXABLE_WAGES').Value, 0) * FUIRate;
        OldFUI := OldFUI + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_FUI_TAX').Value, 0);

        CUSTOM_VIEW_2.Next;
      end;

      with CO_FED_TAX_LIABILITIES do
      begin
        if Abs(NewOASDI - OldOASDI) >= MinLiability then
        begin
          Insert;
          FieldByName('CO_NBR').Value := CoNbr;
          FieldByName('DUE_DATE').Value := EndQuarter;
          FieldByName('CHECK_DATE').Value := aDate;
          FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
          FieldByName('TAX_TYPE').Value := TAX_LIABILITY_TYPE_ER_OASDI;
          FieldByName('AMOUNT').Value := NewOASDI - OldOASDI;
          FieldByName('ADJUSTMENT_TYPE').Value := TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END;
          Post;
        end;

        if Abs(NewMedicare - OldMedicare) >= MinLiability then
        begin
          Insert;
          FieldByName('CO_NBR').Value := CoNbr;
          FieldByName('DUE_DATE').Value := EndQuarter;
          FieldByName('CHECK_DATE').Value := aDate;
          FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
          FieldByName('TAX_TYPE').Value := TAX_LIABILITY_TYPE_ER_MEDICARE;
          FieldByName('AMOUNT').Value := NewMedicare - OldMedicare;
          FieldByName('ADJUSTMENT_TYPE').Value := TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END;
          Post;
        end;

        if Abs(NewFUI - OldFUI) >= MinLiability then
        begin
          Insert;
          FieldByName('CO_NBR').Value := CoNbr;
          FieldByName('DUE_DATE').Value := EndQuarter;
          FieldByName('CHECK_DATE').Value := aDate;
          FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
          FieldByName('TAX_TYPE').Value := TAX_LIABILITY_TYPE_ER_FUI;
          FieldByName('AMOUNT').Value := NewFUI - OldFUI;
          FieldByName('ADJUSTMENT_TYPE').Value := TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END;
          Post;
        end;
      end;

// Look for ER SDI discrepancies
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ERSDIDiscrepancies') do
      begin
        SetParam('StartDate', BeginQuarter);
        SetParam('EndDate', EndQuarter);
        SetParam('CoNbr', CoNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
        ctx_DataAccess.SY_CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelect') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'SY_STATES');
        ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.SY_CUSTOM_VIEW.Open;
      HISTORY_VIEW_2.Close;
      HISTORY_VIEW_2.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;

      CUSTOM_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';
      CO_STATES.First;
      while not CO_STATES.EOF do
      begin
        CUSTOM_VIEW_2.Filter := 'CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString;

        if CUSTOM_VIEW_2.RecordCount <> 0 then
        begin
          State := CO_STATES.FieldByName('STATE').AsString;
          HISTORY_VIEW_2.Filter := 'STATE=''' + State + '''';

          OldSDI := 0;
          NewSDI := 0;
          CUSTOM_VIEW_2.First;
          HISTORY_VIEW_2.First;
          while not CUSTOM_VIEW_2.EOF do
          begin
            while (HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
            and (not HISTORY_VIEW_2.EOF) do
              HISTORY_VIEW_2.Next;
            if HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
              HISTORY_VIEW_2.Prior;

            NewSDI := NewSDI + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_SDI_TAXABLE_WAGES').Value, 0) *
              ConvertNull(HISTORY_VIEW_2.FieldByName('ER_SDI_RATE').Value, 0);
            OldSDI := OldSDI + ConvertNull(CUSTOM_VIEW_2.FieldByName('ER_SDI_TAX').Value, 0);
            CUSTOM_VIEW_2.Next;
          end;

          if Abs(NewSDI - OldSDI) >= MinLiability then
            with CO_STATE_TAX_LIABILITIES do
            begin
              Insert;
              FieldByName('CO_NBR').Value := CoNbr;
              FieldByName('CO_STATES_NBR').Value := CO_STATES.FieldByName('CO_STATES_NBR').Value;
              FieldByName('DUE_DATE').Value := EndQuarter;
              FieldByName('CHECK_DATE').Value := aDate;
              FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
              FieldByName('TAX_TYPE').Value := TAX_LIABILITY_TYPE_ER_SDI;
              FieldByName('AMOUNT').Value := NewSDI - OldSDI;
              FieldByName('ADJUSTMENT_TYPE').Value := TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END;
              Post;
            end;
        end;

        CO_STATES.Next;
      end;
      CUSTOM_VIEW_2.Filtered := False;
      CUSTOM_VIEW_2.Filter := '';
      HISTORY_VIEW_2.Filtered := False;
      HISTORY_VIEW_2.Filter := '';

// Look for ER SUI discrepancies
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('ERSUIDiscrepancies') do
      begin
        SetParam('StartDate', BeginQuarter);
        SetParam('EndDate', EndQuarter);
        SetParam('CoNbr', CoNbr);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelect') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'CO_SUI');
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      HISTORY_VIEW_2.Close;
      HISTORY_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

      CUSTOM_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.Filtered := True;
      HISTORY_VIEW_2.IndexFieldNames := 'EFFECTIVE_DATE;CREATION_DATE';
      CO_SUI.First;
      while not CO_SUI.EOF do
      begin
        CUSTOM_VIEW_2.Filter := 'CO_SUI_NBR=' + CO_SUI.FieldByName('CO_SUI_NBR').AsString;
        SystemSUINbr := CO_SUI.FieldByName('CO_SUI_NBR').Value;
        if (CUSTOM_VIEW_2.RecordCount > 0) and Is_ER_SUI(VarToStr(DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', SystemSUINbr,
          'EE_ER_OR_EE_OTHER_OR_ER_OTHER'))) then
        begin
          HISTORY_VIEW_2.Filter := 'CO_SUI_NBR=' + CO_SUI.FieldByName('CO_SUI_NBR').AsString;
          OldSUI := 0;
          NewSUI := 0;
          CUSTOM_VIEW_2.First;
          HISTORY_VIEW_2.First;
          while not CUSTOM_VIEW_2.EOF do
          begin
            while (HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value <= CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value)
            and (not HISTORY_VIEW_2.EOF) do
              HISTORY_VIEW_2.Next;
            if HISTORY_VIEW_2.FieldByName('EFFECTIVE_DATE').Value > CUSTOM_VIEW_2.FieldByName('CHECK_DATE').Value then
              HISTORY_VIEW_2.Prior;

            NewSUI := NewSUI + ConvertNull(CUSTOM_VIEW_2.FieldByName('SUI_TAXABLE_WAGES').Value, 0) *
              ConvertNull(HISTORY_VIEW_2.FieldByName('RATE').Value, 0);
            OldSUI := OldSUI + ConvertNull(CUSTOM_VIEW_2.FieldByName('SUI_TAX').Value, 0);
            CUSTOM_VIEW_2.Next;
          end;

          if Abs(NewSUI - OldSUI) >= MinLiability then
            with CO_SUI_LIABILITIES do
            begin
              Insert;
              FieldByName('CO_NBR').Value := CoNbr;
              FieldByName('CO_SUI_NBR').Value := CO_SUI.FieldByName('CO_SUI_NBR').Value;
              FieldByName('DUE_DATE').Value := EndQuarter;
              FieldByName('CHECK_DATE').Value := aDate;
              FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
              FieldByName('AMOUNT').Value := NewSUI - OldSUI;
              FieldByName('ADJUSTMENT_TYPE').Value := TAX_LIABILITY_ADJUSTMENT_TYPE_QUARTER_END;
              Post;
            end;
        end;

        CO_SUI.Next;
      end;
      CUSTOM_VIEW_2.Filtered := False;
      CUSTOM_VIEW_2.Filter := '';
      HISTORY_VIEW_2.Filtered := False;
      HISTORY_VIEW_2.Filter := '';

      MyTransactionManager.ApplyUpdates;
    finally
      MyTransactionManager.Free;
      HISTORY_VIEW_2.Free;
      CUSTOM_VIEW_2.Free;
    end;
  end;
end;

end.
