// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STMP_INVOICE_DM;

interface

uses
   Db, Classes, Forms, EvUtils, EvConsts, SPayrollReport,
  SDataStructure, SysUtils, SPD_ProcessException, EvTypes, Windows, Variants, EvBasicUtils,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, evAchBase, EvContext,
  SDDClasses, SDataDictbureau, ISDataAccessComponents,
  EvDataAccessComponents, StrUtils, EvExceptions,EvMainboard,isBaseClasses, EvUIComponents,
  EvClientDataSet, EvCommonInterfaces, EvDataset, isDataSet;

type
  TINVOICE_DM = class(TDataModule)
    CO_INVOICE_MASTER: TevClientDataSet;
    CO_INVOICE_MASTERCL_NBR: TIntegerField;
    CO_INVOICE_MASTERCO_NBR: TIntegerField;
    CO_INVOICE_MASTERNAME: TStringField;
    CO_INVOICE_MASTERADDRESS1: TStringField;
    CO_INVOICE_MASTERADDRESS2: TStringField;
    CO_INVOICE_MASTERCITY: TStringField;
    CO_INVOICE_MASTERSTATE: TStringField;
    CO_INVOICE_MASTERZIP_CODE: TStringField;
    CO_INVOICE_MASTERINVOICE_NUMBER: TIntegerField;
    CO_INVOICE_MASTERINVOICE_DATE: TDateField;
    CO_INVOICE_MASTERCHECK_DATE: TDateField;
    CO_INVOICE_MASTERTERMS: TStringField;
    CO_INVOICE_MASTERCUSTOM_COMPANY_NUMBER: TStringField;
    CO_INVOICE_MASTERCO_NAME: TStringField;
    CO_INVOICE_DETAIL: TevClientDataSet;
    CO_INVOICE_DETAIL_SOURCENAME: TStringField;
    CO_INVOICE_DETAIL_SOURCEQUANTITY: TFloatField;
    CO_INVOICE_DETAIL_SOURCECOST: TCurrencyField;
    CO_INVOICE_DETAIL_SOURCETAX: TCurrencyField;
    CO_INVOICE_DETAIL_SOURCEDISCOUNT: TCurrencyField;
    CO_INVOICE_DETAIL_SOURCETOTAL: TCurrencyField;
    CO_INVOICE_MASTERCL_BILLING_NBR: TIntegerField;
    CO_INVOICE_DETAILCO_SERVICES_NBR: TIntegerField;
    CO_INVOICE_MASTERCO_BILLING_HISTORY_NBR: TIntegerField;
    CO_INVOICE_DETAILCO_BILLING_HISTORY_NBR: TIntegerField;
    CO_INVOICE_DETAILCO_BILLING_HISTORY_DETAIL_NBR: TIntegerField;
    CO_INVOICE_DETAILCL_NBR: TIntegerField;
    CO_INVOICE_MASTERPR_NBR: TIntegerField;
    CO_INVOICE_MASTERAddressPrintLine1: TStringField;
    CO_INVOICE_MASTERAddressPrintLine2: TStringField;
    CO_INVOICE_MASTERAddressPrintLine3: TStringField;
    CO_INVOICE_MASTERINVOICE_AMOUNT: TCurrencyField;
    CO_INVOICE_MASTERINVOICE_DISCOUNT: TCurrencyField;
    CO_INVOICE_MASTERINVOICE_TAX: TCurrencyField;
    CO_INVOICE_MASTERINVOICE_TOTAL: TCurrencyField;
    CO_INVOICE_DETAILSB_SERVICES_NBR: TIntegerField;
    CO_INVOICE_MASTERCO_DIVISION_NBR: TIntegerField;
    CO_INVOICE_MASTERCO_BRANCH_NBR: TIntegerField;
    CO_INVOICE_MASTERCO_DEPARTMENT_NBR: TIntegerField;
    CO_INVOICE_MASTERCO_TEAM_NBR: TIntegerField;
    cdsDBDT: TevClientDataSet;
    CO_INVOICE_MASTERNOTES: TMemoField;
    CO_INVOICE_MASTERFILLER: TStringField;
    cdsTaxDistr: TevClientDataSet;
    cdsTaxDistrTAX: TFloatField;
    cdsTaxDistrCO_DIVISION_NBR: TIntegerField;
    cdsTaxDistrCO_BRANCH_NBR: TIntegerField;
    cdsTaxDistrCO_DEPARTMENT_NBR: TIntegerField;
    cdsTaxDistrCO_TEAM_NBR: TIntegerField;
    cdsTaxDistrPR_CHECK_NBR: TIntegerField;
    cdsTaxDistrfname: TStringField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure CO_INVOICE_MASTERCalcFields(DataSet: TDataSet);
    procedure CO_INVOICE_DETAILBeforePost(DataSet: TDataSet);
  private
    sLevel: string;
    DBDT_Tables:IisStringList;
    PayrollNbr:integer;
    sLink: string;
    procedure DistrToDBDT(Amount: real);
    procedure AddToDBDT(DBDTNbr: Integer; Amount: real);
    procedure Count_EE;
    procedure Count_EE_Prev_Month(const d: TDateTime);
    procedure Count_All_Termed_EE;
    procedure Count_SelfServe_EE;
    procedure Count_NewHire;
    procedure Count_CO_States;
    procedure Count_CO_Locals;
    procedure Count_CO_Tax_Agencies;
    procedure Count_PretaxCodes;
    procedure Count_Pretax;
    procedure Count_TimeOfAccrual;
    procedure Count_TimeOfAccrualCodes;
    procedure Count_1099(CoLevel: Boolean; d: TDateTime);
    procedure Count_W2(CoLevel: Boolean; d: TDateTime);
    procedure Count_1095(CoLevel: Boolean; d: TDateTime; const ACA_Type: string);
    procedure Count_ACA_Active(const prNbr: integer);
    procedure Count_ACA_YearToDate(const d: TDateTime);
    procedure Count_CO_Analytics;
    procedure Count_CO_Per_Override_Analytics(const syField, coField: String);
    procedure Count_EE_Enabled_For_Analytics;    
    procedure CalculateAmount(CoLevel: Boolean);
    procedure LocateSB_SERVICES_CALCULATIONS(Qty: integer; QtyLow: Integer = -1);
    procedure PopulateInvoiceMaster(InvoiceNumber, DBDTNumber: Integer);
    function GetWorkersCompAsSui(sSQL: String; iPrNbr: Integer): Double;
    procedure CheckDbdtSetupForPayroll;
  public
    PRCheckDate: TDateTime;
    FISCAL_YEAR_END: integer;
    IsScheduled: boolean;
    DoIt: boolean;
    LAST_SCHEDULED_PRCheckDate: TDateTime;
    { Public declarations }

  end;

function ProcessBilling(prNumber: integer; ForPayroll: boolean;
              InvoiceNumber: integer = -1; ReturnDS: Boolean = False): Variant;
function ProcessBillingForServices(ClNbr: Integer; CoNbr: Integer; const List: String; const EffDate: TDateTime; const Notes: string; var bManualACH: Boolean): Real;

implementation

uses SPD_MiscCheck, SReportSettings, SDataDictclient,DateUtils;

{$R *.DFM}

function GetNumberOfCopies: Integer;
begin
  with TevPrRepPrnSettings.Create(nil) do
  try
    ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.FieldByName('RUN_PARAMS')));
    Result := Copies;
  finally
    Free;
  end;
end;

procedure TINVOICE_DM.LocateSB_SERVICES_CALCULATIONS(Qty: integer; QtyLow: Integer = -1);
begin
  if QtyLow = -1 then
    QtyLow := Qty;

  if AnsiIndexText(DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString, [SB_SERVICES_BASED_ON_FLAT, SB_SERVICES_BASED_ON_FLAT_AMOUNT]) > -1 then
  begin
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filter :=
      'SB_SERVICES_NBR=' + DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SB_SERVICES_NBR').AsString;

    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filtered := True;

    if (DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.RecordCount = 0) then
      raise EInconsistentData.CreateHelp('SB_SERVICES_CALCULATIONS record missing for SB_SERVICES_NBR = ' +
        DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SERVICE_NAME').AsString, IDH_InconsistentData);

    if not (DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.RecordCount = 1) then
      raise EInconsistentData.CreateHelp('Too many SB_SERVICES_CALCULATIONS Records for SB_SERVICES_NBR = ' +
        DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SERVICE_NAME').AsString, IDH_InconsistentData);
  end
  else
  if AnsiIndexText(DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString,
                [SB_SERVICES_BASED_ON_ALL_CHECKS,
                  SB_SERVICES_BASED_ON_BILLING_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_ALL_BUT_TAX,
                  SB_SERVICES_BASED_ON_ALL_BUT_AGENCY,
                  SB_SERVICES_BASED_ON_AGENCY_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_MANUAL_CHECKS,
                  SB_SERVICES_BASED_ON_TAX_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_ALL_BUT_BILLING,
                  SB_SERVICES_BASED_ON_PAYROLL_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_REGULAR_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_COBRA_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_PER_NEW_CHECK,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE,
                  SB_SERVICES_BASED_ON_PER_ALL_EMPLOYEES,
                  SB_SERVICES_BASED_ON_PER_SELFSERVE_EMPLOYEES,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_PAID,
                  SB_SERVICES_BASED_ON_EMPLOYEE_NEW_HIRE_REPORT,
                  SB_SERVICES_BASED_ON_EMPLOYEE_CHANGES,
                  SB_SERVICES_BASED_ON_DIRECT_DEPOSIT,
                  SB_SERVICES_BASED_ON_PER_STATE,
                  SB_SERVICES_BASED_ON_PER_LOCAL,
                  SB_SERVICES_BASED_ON_PER_TAX_AGENCY,
                  SB_SERVICES_BASED_ON_PER_REPORT,
                  SB_SERVICES_BASED_ON_PER_SPECIFIC_REPORT,
                  SB_SERVICES_BASED_ON_PER_REPORT_NUMBER,
                  SB_SERVICES_BASED_ON_1099,
                  SB_SERVICES_BASED_ON_W2,
                  SB_SERVICES_BASED_ON_NEW_HIRED_EMPLOYEES,
                  SB_SERVICES_BASED_ON_TERMED_EMPLOYEES,
                  SB_SERVICES_BASED_ON_ALL_TERMED_EMPLOYEES,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX_CODES,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA_CODES,
                  SB_SERVICES_BASED_ON_PER_WEB_USER,
                  SB_SERVICES_BASED_ON_PER_NEW_PRENOTE,
                  SB_SERVICES_BASED_ON_PER_VMR_VOUCHER,
                  SB_SERVICES_BASED_ON_ED_CODE_COUNT,
                  SB_SERVICES_BASED_ON_PER_VMR_REPORT,
                  SB_SERVICES_BASED_ON_PRINTED_CHECKS_ONLY,
                  SB_SERVICES_BASED_ON_PER_HOURLY_EMPLOYEE,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_PER_MONTH,
                  SB_SERVICES_BASED_ON_PER_EMPLOYEE_PAID_PER_MONTH,
                  SB_SERVICES_BASED_ON_1095Bs,
                  SB_SERVICES_BASED_ON_1095Cs,
                  SB_SERVICES_BASED_ON_ACA_ACTIVE,
                  SB_SERVICES_BASED_ON_ACA_YEAR_TO_DATE,
                  SB_SERVICES_BASED_ON_ANALYTICS,
                  SB_SERVICES_BASED_ON_PER_OVERRIDE_DASHBOARD,
                  SB_SERVICES_BASED_ON_PER_OVERRIDE_USER,
                  SB_SERVICES_BASED_ON_PER_OVERRIDE_LOOKBACKYEAR,
                  SB_SERVICES_BASED_ON_PER_ACTIVE_EE_ENABLED_FOR_ANALYTICS,
                  SB_SERVICES_BASED_ON_PER_EVOLUTION_PAYROLL_USER]) > -1 then
  begin
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filter :=
      'SB_SERVICES_NBR=' + DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SB_SERVICES_NBR').AsString +
      ' AND (MINIMUM_QUANTITY <= ' + IntToStr(Qty) + ' OR MINIMUM_QUANTITY is null) ' +
      ' AND (MAXIMUM_QUANTITY >= ' + IntToStr(QtyLow) + ' OR MAXIMUM_QUANTITY is null)';

    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filtered := True;

    if DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.EOF and DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.BOF then
      raise EInconsistentData.CreateHelp('Missing SB_SERVICES_CALCULATIONS_NBR : Qty=' + IntToStr(Qty) + ' SB_SERVICES: ' +
        DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SERVICE_NAME').AsString, IDH_InconsistentData);
  end
  else
  if AnsiIndexText(DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString, [
                  SB_SERVICES_BASED_ON_TRUST_IMPOUND,
                  SB_SERVICES_BASED_ON_ED_CODE,
                  SB_SERVICES_BASED_ON_ED_GROUP,
                  SB_SERVICES_BASED_ON_TAX_IMPOUND,
                  SB_SERVICES_BASED_ON_TAX_ER_IMPOUND,
                  SB_SERVICES_BASED_ON_PERC_EARNINGS,
                  SB_SERVICES_BASED_ON_WORKERS_COMP]) > -1 then
  begin
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filter :=
      'SB_SERVICES_NBR=0';
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filtered := True;
  end
  else
    raise EInconsistentData.CreateHelp('Unrecognized SB_SERVICES Type for SB_SERVICES : ' +
      DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SERVICE_NAME').AsString, IDH_InconsistentData);
end;

function IsSpecificReportPrinted(ClientNbr, PrNbr, ReportNbr: Integer): Boolean;
begin
  if not DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []) then
    DM_PAYROLL.PR.DataRequired('pr_nbr=' + IntToStr(PrNbr));
  DM_COMPANY.CO_REPORTS.DataRequired('co_nbr=' + IntToStr(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger));
  DM_SERVICE_BUREAU.SB_REPORTS.DataRequired('ALL');
  Result := DM_SERVICE_BUREAU.SB_REPORTS.Locate('SB_REPORTS_NBR', ReportNbr, []) and
     DM_COMPANY.CO_REPORTS.Locate('REPORT_LEVEL;REPORT_WRITER_REPORTS_NBR', DM_SERVICE_BUREAU.SB_REPORTS['REPORT_LEVEL;REPORT_WRITER_REPORTS_NBR'], []) and
    (GetNumberOfCopies <> 0);
end;

function ColumnSum(aDataSet: TevClientDataSet; FieldName: string; Nbr: Integer): Currency;
begin
  result := 0;
  with aDataSet do
  begin
    First;
    while not EOF do
    begin
      if Nbr = FieldValues['CO_BILLING_HISTORY_NBR'] then
        result := result + FieldByName(FieldName).AsCurrency;
      Next;
    end;
  end;
end;

procedure TINVOICE_DM.CO_INVOICE_MASTERCalcFields(DataSet: TDataSet);

  procedure CalculateAddressPrintLines(
    var Line1, Line2, Line3: string;
    Address1, Address2, City, State, Zip: string);
  begin
    Line1 := Address1;
    Line2 := Address2;
    Line3 := City;

    if Length(State) > 0 then
      if Length(Line3) > 0 then
        Line3 := Line3 + ', ' + State
      else
        Line3 := State;

    if Length(Zip) > 0 then
      if Length(Line3) > 0 then
        Line3 := Line3 + '  ' + Zip
      else
        Line3 := Zip;

    if Length(Line2) = 0 then
    begin
      Line2 := Line3;
      Line3 := '';
    end;

    if Length(Line1) = 0 then
    begin
      Line1 := Line2;
      Line2 := Line3;
      Line3 := '';
    end;
  end;

var
  Line1, Line2, Line3: string;
begin
  CalculateAddressPrintLines(
    Line1, Line2, Line3,
    DataSet.FieldByName('ADDRESS1').AsString,
    DataSet.FieldByName('ADDRESS2').AsString,
    DataSet.FieldByName('CITY').AsString,
    DataSet.FieldByName('STATE').AsString,
    DataSet.FieldByName('ZIP_CODE').AsString);

  DataSet.FieldByName('AddressPrintLine1').AsString := Line1;
  DataSet.FieldByName('AddressPrintLine2').AsString := Line2;
  DataSet.FieldByName('AddressPrintLine3').AsString := Line3;
end;

{                   ForPayroll   ForPreview InvoiceNumber
   NewInvoice         Y          N          -1
   FollowupPrinting   Y          N           nonzero
   Preview            N          Y           nonzero
   ForAR              N          N           nonzero
}

procedure TINVOICE_DM.Count_All_Termed_EE;
begin
  with TExecDSWrapper.Create('GenericSelectCurrentWithConditionGrouped') do
  begin
    SetMacro('Columns', sLevel + ', count(*) cnt');
    SetMacro('TableName', 'EE');
    SetMacro('Condition', 'CO_NBR = :CoNbr and CURRENT_TERMINATION_CODE <> ''' + EE_TERM_ACTIVE + '''');
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetMacro('Group', sLevel);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  DBDT_Tables.Add('ALL_EE');
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_SelfServe_EE;
begin
  with TExecDSWrapper.Create('GenericSelectCurrentWithConditionGrouped') do
  begin
    SetMacro('Columns', sLevel + ', count(*) cnt');
    SetMacro('TableName', 'EE');
    SetMacro('Condition', 'CO_NBR = :CoNbr and CURRENT_TERMINATION_CODE = '''+ EE_TERM_ACTIVE +
                                       ''' and SELFSERVE_ENABLED <> '''+GROUP_BOX_NO+''''); //includes GROUP_BOX_YES, GROUP_BOX_FULL_ACCESS
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetMacro('Group', sLevel);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  DBDT_Tables.Add('ALL_EE');
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_EE;
begin
  with TExecDSWrapper.Create('GenericSelectCurrentNBRWithConditionGrouped') do
  begin
    SetMacro('TableName', 'EE');
    SetMacro('NbrField', 'CO');
    SetParam('RecordNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetMacro('Condition', 'CURRENT_TERMINATION_CODE=:Code');
    SetParam('Code', EE_TERM_ACTIVE);
    SetMacro('Columns', sLevel + ', Count(*) cnt');
    SetMacro('Group', sLevel);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  DBDT_Tables.Add('ALL_EE');
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_EE_Prev_Month(const d: TDateTime);
var
  Q: IevQuery;
  pDate: TDateTime;
begin

  pDate := GetBeginMonth(d)-1;
  Q := TevQuery.Create('select #Columns from EE where {AsOfDate<EE>} and ' +
                       ' CURRENT_TERMINATION_CODE=:Code and CO_NBR=:CO_NBR group by #Group');
  Q.Macros.AddValue('Columns', sLevel + ', Count(*) cnt');
  Q.Macros.AddValue('Group', sLevel);
  Q.Params.AddValue('CO_NBR', DM_COMPANY.CO['CO_NBR']);
  Q.Params.AddValue('Code', EE_TERM_ACTIVE);
  Q.Params.AddValue('StatusDate', pDate);
  Q.execute;

  DBDT_Tables.Add('ALL_EE');
  Q.Result.First;
  while not Q.Result.Eof do
  begin
    AddToDBDT(Q.Result.FieldByName(sLevel).AsInteger, Q.Result.FieldByName('cnt').AsFloat);
    Q.Result.Next;
  end;
end;


procedure TINVOICE_DM.Count_NewHire;
begin
  with DM_EMPLOYEE.EE do
  begin
    DBDT_Tables.Add('ALL_EE');
    First;
    while not EOF do
    begin
      if (FieldByName('NEW_HIRE_REPORT_SENT').AsString = NEW_HIRE_REPORT_PENDING) and
         (FieldByName('CURRENT_HIRE_DATE').AsDateTime < Date + 1) then
        AddToDBDT(DM_EMPLOYEE.EE.FieldByName(sLevel).AsInteger, 1);
      Next;
    end;
  end;
{  if Result > 0 then
    LogException.Add(
      DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger,
      DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger,
      PE_NEW_HIRE_NOT_BILLED,
      '');}
end;

procedure TINVOICE_DM.Count_CO_States;
var
  Q: IevQuery;
begin
  Q := TevQuery.Create('select Count(*) cnt from CO_STATES where {AsOfNow<CO_STATES>} and STATE_NON_PROFIT <> ''Y'' and CO_NBR=:CO_NBR');
  Q.Param['CO_NBR'] := DM_COMPANY.CO['CO_NBR'];
  DistrToDBDT(Q.Result['cnt']);
end;

procedure TINVOICE_DM.Count_CO_Locals;
begin
  with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
  begin
    SetMacro('Columns', 'Count(*) cnt');
    SetMacro('TableName', 'CO_LOCAL_TAX');
    SetMacro('NbrField', 'CO');
    SetParam('RecordNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetMacro('Condition', 'LOCAL_ACTIVE = :Active');
    SetParam('Active', GROUP_BOX_YES);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;

  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  DistrToDBDT(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger);
end;

procedure TINVOICE_DM.Count_PretaxCodes;
begin
  with TExecDSWrapper.Create('GetPretaxCount') do
  begin
    SetMacro('Columns', 'C.EE_NBR||E.CL_E_DS_NBR');
    SetMacro('Group', sLevel);
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetParam('CodeType', ED_ST_SB_PREAX_INS);
    SetParam('Code', EE_TERM_ACTIVE);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  DBDT_Tables.Add('ALL_EE');
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_Pretax;
begin
  with TExecDSWrapper.Create('GetPretaxCount') do
  begin
    SetMacro('Columns', 'C.EE_NBR');
    SetMacro('Group', sLevel);
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetParam('CodeType', ED_ST_SB_PREAX_INS);
    SetParam('Code', EE_TERM_ACTIVE);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;

  DBDT_Tables.Add('ALL_EE');
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_TimeOfAccrual;
begin
  with TExecDSWrapper.Create('GetCheckTimeOfAccruals') do
  begin
    SetMacro('Columns', 'E.EE_NBR');
    SetMacro('Group', sLevel);
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetParam('Code', EE_TERM_ACTIVE);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;

  DBDT_Tables.Add('ALL_EE');
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_TimeOfAccrualCodes;
begin
  with TExecDSWrapper.Create('GetCheckTimeOfAccruals') do
  begin
    SetMacro('Columns', 'EE_TIME_OFF_ACCRUAL_NBR');
    SetMacro('Group', sLevel);
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
    SetParam('Code', EE_TERM_ACTIVE);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;

  DBDT_Tables.Add('ALL_EE');
  ctx_DataAccess.CUSTOM_VIEW.Close;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_1099(CoLevel: Boolean; d: TDateTime);
begin
  with TExecDSWrapper.Create('CheckW2_1099Existence') do
  begin
    SetParam('InvoiceDate', d);
    SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
    if CoLevel then
    begin
      SetMacro('Lvl', 'CO');
      SetParam('ServiceNbr', DM_COMPANY.CO_SERVICES['CO_SERVICES_NBR']);
    end
    else
    begin
      SetMacro('Lvl', 'SB');
      SetParam('ServiceNbr', DM_SERVICE_BUREAU.SB_SERVICES['SB_SERVICES_NBR']);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;
  if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
    Exit;

  with TExecDSWrapper.Create('Count1099_EEs') do
  begin
    SetParam('CoNbr', DM_COMPANY.CO_SERVICES['CO_NBR']);
    SetMacro('Group', sLevel);
    if DM_COMPANY.CO['APPLY_MISC_LIMIT_TO_1099'] = GROUP_BOX_NO then
      SetMacro('Margin', '0')
    else
      SetMacro('Margin', '599.999999');
    SetParam('BeginYear', GetBeginYear(d - 180));
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;

  DBDT_Tables.Add('ALL_EE');
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_W2(CoLevel: Boolean; d: TDateTime);
begin
  with TExecDSWrapper.Create('CheckW2_1099Existence') do
  begin
    SetParam('InvoiceDate', d);
    SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
    if CoLevel then
    begin
      SetMacro('Lvl', 'CO');
      SetParam('ServiceNbr', DM_COMPANY.CO_SERVICES['CO_SERVICES_NBR']);
    end
    else
    begin
      SetMacro('Lvl', 'SB');
      SetParam('ServiceNbr', DM_SERVICE_BUREAU.SB_SERVICES['SB_SERVICES_NBR']);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;
  if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
    Exit;

  with TExecDSWrapper.Create('CountW2_EEs') do
  begin
    SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
    SetMacro('CountCondition1','COMPANY_OR_INDIVIDUAL_NAME = ''I'' AND W2_Type IN (''F'', ''B'', ''P'')');
    SetMacro('CountCondition2','COMPANY_OR_INDIVIDUAL_NAME = ''C'' AND W2_Type =''B''');
    SetMacro('Group', sLevel);

    if DM_COMPANY.CO['APPLY_MISC_LIMIT_TO_1099'] = GROUP_BOX_NO then
      SetMacro('Margin', '0')
    else
      SetMacro('Margin', '599.999999');

    SetParam('BeginYear', GetBeginYear(d - 180));
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;

  DBDT_Tables.Add('ALL_EE');
  while not ctx_DataAccess.CUSTOM_VIEW.Eof do
  begin
    AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
    ctx_DataAccess.CUSTOM_VIEW.Next;
  end;
end;

procedure TINVOICE_DM.Count_1095(CoLevel: Boolean; d: TDateTime; const ACA_Type: string);
var
    Q: IevQuery;
begin
  with TExecDSWrapper.Create('CheckW2_1099Existence') do
  begin
    SetParam('InvoiceDate', d);
    SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
    if CoLevel then
    begin
      SetMacro('Lvl', 'CO');
      SetParam('ServiceNbr', DM_COMPANY.CO_SERVICES['CO_SERVICES_NBR']);
    end
    else
    begin
      SetMacro('Lvl', 'SB');
      SetParam('ServiceNbr', DM_SERVICE_BUREAU.SB_SERVICES['SB_SERVICES_NBR']);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
  end;
  if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
    Exit;

    Q := TevQuery.Create(
'select e.#group, count(distinct e.EE_NBR) cnt from ee e,'#13 +
'           (select Distinct a.#group, a.EE_NBR from ee a,'#13 +
'                            (   select distinct a1.#group, a1.EE_NBR from ee a1 ' +
                                    'where ((#CountCondition1) or (#CountCondition2)) '+
                                    'and a1.co_nbr = :CoNbr and '+
                                        '{AsOfNow<a1>} and ' +
                                        ' 0 < ( select  ConvertNullDouble(sum(cl.Amount),0) ' +
                                        ' from pr pr, pr_check pc , pr_check_lines cl, Cl_E_Ds ed ' +
                                        ' where  {AsOfNow<pr>} and {AsOfNow<pc>} and  {AsOfNow<cl>} and {AsOfNow<ed>} ' +
                                        '       and cl.pr_check_nbr = pc.pr_check_nbr '+
                                        '       and pr.pr_nbr = pc.pr_nbr '+
                                        '       and ed.Cl_E_Ds_Nbr = cl.Cl_E_Ds_Nbr '+
                                        '       and ed.E_D_CODE_TYPE NOT IN ('''+ED_ST_EXEMPT_EARNINGS+''','''+ED_ST_SP_TAXED_1099_EARNINGS+''') and ed.E_D_CODE_TYPE <> ''D1'' ' +
                                        '       and pc.ee_nbr  = a1.ee_nbr '+
                                        '       and BeginYear(pr.check_date) = :beginDate '+
                                        '       and pr.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''','''+ PAYROLL_STATUS_VOIDED+ ''') ' +
                                        '       and not ( (a1.Current_Termination_Code = ''S'' AND ed.E_D_Code_Type = ''M4'') OR'#13 +
                                        '                 (a1.Current_Termination_Code = ''P'' AND ed.E_D_Code_Type = ''M4'') OR'#13 +
                                        '                 (a1.Current_Termination_Code = ''J'' AND ed.E_D_Code_Type = ''M4'') ) ) ' +
''#13 +
                                  ' union ' +
''#13 +
                                  'select distinct a2.#group, a2.EE_NBR from ee a2 ' +
                                  'where a2.W2_Type IN (''O'', ''B'') '+
                                       'and a2.co_nbr = :CoNbr and {AsOfNow<a2>} and ' +
                                        ' #Margin < ( select ConvertNullDouble(sum(cl.Amount),0) ' +
                                        ' from pr pr, pr_check pc , pr_check_lines cl, Cl_E_Ds ed ' +
                                        ' where {AsOfNow<pr>} and {AsOfNow<pc>} and {AsOfNow<cl>} and {AsOfNow<ed>}' +
                                        '       and cl.pr_check_nbr = pc.pr_check_nbr '+
                                        '       and pr.pr_nbr = pc.pr_nbr '+
                                        '       and ed.Cl_E_Ds_Nbr = cl.Cl_E_Ds_Nbr '+

                                        '       and ed.E_D_CODE_TYPE IN ('''+ED_ST_EXEMPT_EARNINGS+''','''+ED_ST_SP_TAXED_1099_EARNINGS+''') '+
                                        '       and ed.E_D_CODE_TYPE <> ''D1'' ' +
                                        '       and pc.ee_nbr  = a2.ee_nbr '+
                                        '       and BeginYear(pr.check_date) = :beginDate '+

                                        '       and pr.STATUS in ('''+ PAYROLL_STATUS_PROCESSED + ''','''+ PAYROLL_STATUS_VOIDED+ ''')' + ' ) ' +

'                             ) c'#13 +
'                           where'#13 +
'                                a.co_nbr = :CoNbr and'#13 +
'                                a.#group = c.#group and'#13 +
'                                a.EE_NBR = c.EE_NBR and'#13 +
'                                {AsOfDate<a>} and ' +
'                                a.ACA_TYPE = ''' + ACA_Type[1] + ''' and ' +
'                                0 < ( select  ConvertNullDouble(sum(cl.Amount),0)'#13 +
'                                from pr pr, pr_check pc , pr_check_lines cl'#13 +
'                                where {AsOfNow<pr>} and {AsOfNow<pc>} and {AsOfNow<cl>} ' +
'                                      and cl.pr_check_nbr = pc.pr_check_nbr'#13 +
'                                      and pr.pr_nbr = pc.pr_nbr'#13 +
'                                      and pc.ee_nbr  = a.ee_nbr'#13 +
'                                      and BeginYear(pr.check_date) = :beginDate'#13 +
'                                      and pr.STATUS in (''P'', ''V'') )'#13 +
''#13 +
'           union'#13 +
''#13 +
'           SELECT DISTINCT  t16.#group,  t16.Ee_Nbr'#13 +
'                    FROM   Pr_Check t15, Ee t16, Cl_Person t17, Pr t18, Pr_Check_Lines t19, Cl_E_Ds t20'#13 +
'                    WHERE t16.co_nbr = :CoNbr and'#13 +
'                           {AsOfNow<t15>} and {AsOfNow<t16>} and {AsOfNow<t17>} and {AsOfNow<t18>} and {AsOfNow<t19>} and {AsOfNow<t20>} ' +
'                       and t16.Ee_Nbr = t15.Ee_Nbr'#13 +
'                       and t17.Cl_Person_Nbr = t16.Cl_Person_Nbr'#13 +
'                       and t18.Pr_Nbr = t15.Pr_Nbr'#13 +
'                       and t15.Pr_Check_Nbr = t19.Pr_Check_Nbr'#13 +
'                       and t20.Cl_E_Ds_Nbr = t19.Cl_E_Ds_Nbr'#13 +
'                       and'#13 +
'                           (((t16.Current_Termination_Code = ''E'' AND t20.E_D_Code_Type = ''M4'') OR'#13 +
'                             (t16.Current_Termination_Code = ''Y'' AND t20.E_D_Code_Type = ''M4'') OR'#13 +
'                             (t16.Current_Termination_Code = ''Z'' AND t20.E_D_Code_Type = ''M4'') OR'#13 +
'                             (t16.Current_Termination_Code = ''V'' AND t20.E_D_Code_Type = ''M4'')))  AND'#13 +
'                             BeginYear(t18.Check_Date) = :beginDate'#13 +
''#13 +
'           union'#13 +
''#13 +
'           SELECT  t3.#group, t3.Ee_Nbr'#13 +
'               FROM  Ee t3, Co_Benefit_Subtype t6, Cl_Person t7, Ee_Benefits t21, Co_Benefits t8'#13 +
'                    WHERE t3.co_nbr = :CoNbr and'#13 +
'                           {AsOfNow<t3>} and {AsOfNow<t6>} and {AsOfNow<t7>} and {AsOfNow<t21>} and {AsOfNow<t8>} and ' +
'                       t6.Co_Benefit_Subtype_Nbr = t3.Aca_Co_Benefit_Subtype_Nbr  and'#13 +
'                       t7.Cl_Person_Nbr = t3.Cl_Person_Nbr  and'#13 +
'                       t8.Co_Benefits_Nbr = t6.Co_Benefits_Nbr  and'#13 +
'                       t8.Co_Benefits_Nbr = t21.Co_Benefits_Nbr and'#13 +
'                       t21.Cobra_Status = ''Y'' and'#13 +
'                       t8.ACA_BENEFIT = ''Y'' and'#13 +
'                       t21.Ee_Nbr = t3.Ee_Nbr and'#13 +
'                       t21.Expiration_Date > :beginDate'#13 +
''#13 +
'            ) d '#13 +
''#13 +
' where e.EE_NBR = d.EE_NBR and'#13 +
'       {AsOfDate<e>} and ' +
'       e.ACA_TYPE = ''' + ACA_Type[1] + ''' and ' +
'        e.#group = d.#group '#13 +
' group by e.#group ');

    Q.Macros.AddValue('CountCondition1','COMPANY_OR_INDIVIDUAL_NAME = ''I'' AND W2_Type IN (''F'', ''B'', ''P'')');
    Q.Macros.AddValue('CountCondition2','COMPANY_OR_INDIVIDUAL_NAME = ''C'' AND W2_Type =''B''');

    if DM_COMPANY.CO['APPLY_MISC_LIMIT_TO_1099'] = GROUP_BOX_NO then
      Q.Macros.AddValue('Margin', '0')
    else
      Q.Macros.AddValue('Margin', '599.999999');

    Q.Params.AddValue('CoNbr', DM_COMPANY.CO['CO_NBR']);
    Q.Params.AddValue('StatusDate', GetEndYear(d - 180));
    Q.Macros.AddValue('group',  sLevel);
    Q.Params.AddValue('beginDate', GetBeginYear(d - 180));
    Q.execute;

    DBDT_Tables.Add('ALL_EE');
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      AddToDBDT(Q.Result.FieldByName(sLevel).AsInteger, Q.Result.FieldByName('cnt').AsFloat);
      Q.Result.Next;
    end;
end;

procedure TINVOICE_DM.Count_ACA_Active(const prNbr: integer);
var
    Q: IevQuery;
begin
    Q := TevQuery.Create(
    'select #group, count(distinct EE_NBR) cnt from ee ' +
               ' where co_nbr = :CoNbr and ' +
               '      {AsOfNow<EE>} and ' +
               '      ACA_TYPE <> ''N'' and ' +
               '      0 < ( select  ConvertNullDouble(sum(cl.Amount),0) ' +
               '      from pr pr, pr_check pc , pr_check_lines cl ' +
               '      where {AsOfNow<pr>} and {AsOfNow<pc>} and {AsOfNow<cl>} ' +
               '            and cl.pr_check_nbr = pc.pr_check_nbr ' +
               '            and pr.pr_nbr = pc.pr_nbr ' +
               '            and pc.ee_nbr  = ee.ee_nbr ' +
               '            and pr.PR_NBR = :prNbr ' +
               '            and pr.STATUS in (''P'', ''V'') ) ' +
               '      group by #group ');

    Q.Params.AddValue('CoNbr', DM_COMPANY.CO['CO_NBR']);
    Q.Macros.AddValue('group',  sLevel);
    Q.Params.AddValue('prNbr', prNbr);
    Q.execute;

    DBDT_Tables.Add('ALL_EE');   
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      AddToDBDT(Q.Result.FieldByName(sLevel).AsInteger, Q.Result.FieldByName('cnt').AsFloat);
      Q.Result.Next;
    end;
end;

procedure TINVOICE_DM.Count_ACA_YearToDate(const d: TDateTime);
var
    Q: IevQuery;
begin
    Q := TevQuery.Create(
    'select #group, count(distinct EE_NBR) cnt from ee ' +
               ' where co_nbr = :CoNbr and ' +
               '    ((EFFECTIVE_DATE <= :BeginYear and   EFFECTIVE_UNTIL>= :BeginYear) or ' +
               '     (EFFECTIVE_DATE >= :BeginYear and   EFFECTIVE_DATE <= :EndYear)) and ' +
               '      ACA_TYPE <> ''N'' ' +
               ' group by #group ');

    Q.Params.AddValue('CoNbr', DM_COMPANY.CO['CO_NBR']);
    Q.Macros.AddValue('group',  sLevel);
    Q.Params.AddValue('BeginYear', GetBeginYear(d));
    Q.Params.AddValue('EndYear', d);

    Q.execute;

    DBDT_Tables.Add('ALL_EE');   
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      AddToDBDT(Q.Result.FieldByName(sLevel).AsInteger, Q.Result.FieldByName('cnt').AsFloat);
      Q.Result.Next;
    end;
end;

procedure TINVOICE_DM.Count_CO_Analytics;
begin
  if (DM_COMPANY.CO.FieldByName('ENABLE_ANALYTICS').AsString[1]  = 'Y') then
     DistrToDBDT(1);
end;

procedure TINVOICE_DM.Count_CO_Per_Override_Analytics(const syField, coField: String);
var
  cnt: integer;
begin
  if not DM_SYSTEM_MISC.SY_ANALYTICS_TIER.Active then
     DM_SYSTEM_MISC.SY_ANALYTICS_TIER.DataRequired('ALL');

  if (DM_COMPANY.CO.FieldByName('ENABLE_ANALYTICS').AsString[1]  = 'Y') and
      DM_SYSTEM_MISC.SY_ANALYTICS_TIER.Locate('SY_ANALYTICS_TIER_NBR', DM_COMPANY.CO.FieldByName('SY_ANALYTICS_TIER_NBR').AsInteger, []) then
  begin
    cnt:= DM_COMPANY.CO.FieldByName(coField).AsInteger - DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName(syField).AsInteger;
    if cnt > 0 then
       DistrToDBDT(cnt);
  end;
end;

procedure TINVOICE_DM.Count_EE_Enabled_For_Analytics;
var
  Q: IevQuery;
begin
  if (DM_COMPANY.CO.FieldByName('ENABLE_ANALYTICS').AsString[1]  = 'Y') then
  begin
    Q := TevQuery.Create('select Count(*) cnt from EE where {AsOfNow<EE>} and ENABLE_ANALYTICS = ''Y'' and CO_NBR=:CO_NBR and '  +
                     ' CURRENT_TERMINATION_CODE in ('''+ EE_TERM_ACTIVE + ''',''' + EE_TERM_MILITARY_LEAVE + ''',''' +
                                                         EE_TERM_LEAVE + ''',''' +  EE_TERM_SUSPENDED + ''',''' +
                                                         EE_TERM_FMLA + ''',''' +   EE_TERM_JURYDUTY + ''') ');
    Q.Param['CO_NBR'] := DM_COMPANY.CO['CO_NBR'];
    DistrToDBDT(Q.Result['cnt']);
  end;
end;



function ProcessBilling;
var
  PrInvoiceStatus: string;
  PaymentMethod: string;
  InvoiceType: string;
  INVOICE_DM: TINVOICE_DM;

  procedure CountPRRecordEntries(TableName: string; WhereClause: string);
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('Columns', 'Count(*) cnt');
      SetMacro('TableName', TableName);
      SetMacro('NbrField', 'PR');
      SetParam('RecordNbr', prNumber);
      SetMacro('Condition', WhereClause);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;

    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    INVOICE_DM.DistrToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsInteger);
  end;

  procedure CountPRandEERecordEntries(TableName: string; WhereClause: string);
  begin
    with TExecDSWrapper.Create('GenericSelect2CurrentNBRWithConditionGrouped') do
    begin
      SetMacro('Columns', 'T1.' + INVOICE_DM.sLevel + ', Count(*) cnt');
      SetMacro('Table1', 'EE');
      SetMacro('Table2', TableName);
      SetMacro('NbrField', 'PR');
      SetMacro('JoinField', 'EE');
      SetParam('RecordNbr', prNumber);
      SetMacro('Condition', WhereClause);
      SetMacro('Group', 'T1.' + INVOICE_DM.sLevel);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    INVOICE_DM.DBDT_Tables.Add('EE');
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
  end;

  procedure Checks_PR;
  begin
    CountPRandEERecordEntries('PR_CHECK', '1=1');
  end;

  procedure Checks_Regular;
  begin
    CountPRandEERecordEntries('PR_CHECK', 'CHECK_TYPE=''' + CHECK_TYPE2_REGULAR + ''' and NET_WAGES is not null and NET_WAGES > 0');
  end;

  procedure Checks_Cobra;
  begin
    CountPRandEERecordEntries('PR_CHECK', 'CHECK_TYPE=''' + CHECK_TYPE2_COBRA_CREDIT+ '''');
  end;

  procedure Checks_Void;
  begin
    CountPRandEERecordEntries('PR_CHECK', 'CHECK_TYPE=''' + CHECK_TYPE2_VOID + '''');
    CountPRRecordEntries('PR_MISCELLANEOUS_CHECKS', 'MISCELLANEOUS_CHECK_TYPE=''' + MISC_CHECK_TYPE_AGENCY_VOID + '''');
  end;

  procedure Checks_Manual;
  begin
    CountPRandEERecordEntries('PR_CHECK', 'CHECK_TYPE in (''' + CHECK_TYPE2_MANUAL + ''', ''' + CHECK_TYPE2_3RD_PARTY + ''')');
  end;

  procedure Checks_MISC;
  begin
    CountPRRecordEntries('PR_MISCELLANEOUS_CHECKS', 'MISCELLANEOUS_CHECK_TYPE in (''' + MISC_CHECK_TYPE_AGENCY + ''', ''' + MISC_CHECK_TYPE_AGENCY_VOID + ''')');
  end;

  procedure Checks_CO_TAX_DEP;
  begin
    CountPRRecordEntries('CO_TAX_DEPOSITS ', 'DEPOSIT_TYPE <> ''' + TAX_DEPOSIT_TYPE_NOTICE + '''');
  end;

  procedure Count_No_of_Reports;
  var
    Qty: Integer;
  begin
    with INVOICE_DM do
    begin
      Qty := 0;

      DM_Company.CO_REPORTS.DataRequired('co_nbr=' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));

      with DM_COMPANY.CO_REPORTS do
      begin
        First;
        while not Eof do
        begin
          if GetNumberOfCopies <> 0 then
            INC(Qty);
          Next;
        end;
      end;
      DistrToDBDT(Qty);
    end;
  end;

  procedure Count_WebUsers;
  begin
    with INVOICE_DM do
    begin
      with TExecDSWrapper.Create('CountWebUsers') do
      begin
        SetParam('ClNbr', ctx_DataAccess.ClientID);
        ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      end;

      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
      ctx_DataAccess.SB_CUSTOM_VIEW.Open;
      DistrToDBDT(ctx_DataAccess.SB_CUSTOM_VIEW.Fields[0].AsInteger);
    end;
  end;

  procedure Count_EvolutionPayrollUsers;
  begin
    with INVOICE_DM do
    begin
      with TExecDSWrapper.Create(
'select count(DISTINCT a.SB_USER_NBR) from SB_USER a,'#13 +
'      ( select a.SB_USER_NBR from SB_SEC_CLIENTS a,  SB_USER b'#13 +
'            where  {AsOfNow<a>}  and'#13 +
'                   {AsOfNow<b>}  and'#13 +
'                    a.SB_USER_NBR = b.SB_USER_NBR and'#13 +
'                    b.USER_FUNCTIONS not like ''-1%'' and'#13 +
'                    b.ACTIVE_USER = ''Y'' and'#13 +
'                    b.EVOLUTION_PRODUCT = ''P'' and'#13 +
'                    a.CL_NBR = :CLNbr and'#13 +
'                    a.SB_SEC_GROUPS_NBR is null'#13 +
'        union'#13 +
'        select a.SB_USER_NBR from SB_SEC_GROUP_MEMBERS a, SB_USER b'#13 +
'            where {AsOfNow<a>} and'#13 +
'                  {AsOfNow<b>} and'#13 +
'                    a.SB_USER_NBR = b.SB_USER_NBR and'#13 +
'                    b.USER_FUNCTIONS not like ''-1%'' and'#13 +
'                    b.ACTIVE_USER = ''Y'' and'#13 +
'                    b.EVOLUTION_PRODUCT = ''P'' and'#13 +
'                    SB_SEC_GROUPS_NBR in ('#13 +
'                         select SB_SEC_GROUPS_NBR from  SB_SEC_CLIENTS '#13 +
'                              where'#13 +
'                                {AsOfNow<SB_SEC_CLIENTS>} and'#13 +
'                                 CL_NBR = :CLNbr and SB_USER_NBR is null)'#13 +
'       ) b'#13 +
' where  {AsOfNow<a>} and'#13 +
'   a.SB_USER_NBR = b.SB_USER_NBR') do
      begin
        SetParam('ClNbr', ctx_DataAccess.ClientID);
        ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      end;

      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
      ctx_DataAccess.SB_CUSTOM_VIEW.Open;
      DistrToDBDT(ctx_DataAccess.SB_CUSTOM_VIEW.Fields[0].AsInteger);
    end;
  end;

  procedure Count_Vouchers(const Sign:integer = 1);
  var
    cdEeSetup,cdDirectEmailVoucgher: TevClientDataSet;
    sEeSetupCondition: string;
    DirectEmailVoucgherFlag:boolean;
  begin
    if not CheckVmrSetupActive then
      exit;
    cdEeSetup := TevClientDataSet.Create(nil);
    cdDirectEmailVoucgher := TevClientDataSet.Create(nil);
    DirectEmailVoucgherFlag := false;
    with INVOICE_DM do
    begin
      with TExecDSWrapper.Create('GenericSelect2CurrentNBRWithConditionGrouped') do
      begin
        SetMacro('Columns', 'T1.EE_NBR, T1.' + INVOICE_DM.sLevel + ', Count(*) cnt');
        SetMacro('Table1', 'EE');
        SetMacro('Table2', 'PR_CHECK');
        SetMacro('NbrField', 'PR');
        SetMacro('JoinField', 'EE');
        SetParam('RecordNbr', prNumber);
        SetMacro('Condition', 'CHECK_TYPE=''' + CHECK_TYPE2_REGULAR + ''' and (NET_WAGES is  null or NET_WAGES = 0)');
        SetMacro('Group', 'T1.EE_NBR, T1.' + INVOICE_DM.sLevel);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      DBDT_Tables.Add('EE');
      with ctx_DataAccess.CUSTOM_VIEW.GetFieldValueList('EE_NBR', True) do
      begin
        try
          if Count = 0 then
            sEeSetupCondition := ''
          else if Count < 14000 then
            sEeSetupCondition := 't1.EE_NBR in ('+ CommaText+ ')'
          else
            sEeSetupCondition := 't1.EE_NBR > 0';
        finally
          Free;
        end;
      end;

      if sEeSetupCondition <> '' then
      begin
        with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
        begin
          SetMacro('CONDITION', sEeSetupCondition);
          SetMacro('COLUMNS', 't1.EE_NBR,t2.WEB_PASSWORD,t2.E_MAIL_ADDRESS, t1.CUSTOM_EMPLOYEE_NUMBER, t2.WEB_PASSWORD,t1.PRINT_VOUCHER, t2.E_MAIL_ADDRESS EMAIL');
          SetMacro('TABLE1', 'EE');
          SetMacro('TABLE2', 'CL_PERSON');
          SetMacro('JOINFIELD', 'CL_PERSON');
          cdEeSetup.IndexFieldNames := 'EE_NBR';
          ctx_DataAccess.GetCustomData(cdEeSetup, AsVariant);
        end;
        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('COLUMNS', 'CL_MAIL_BOX_GROUP_NBR, OPTION_STRING_VALUE');
          SetMacro('TABLENAME', 'CL_MAIL_BOX_GROUP_OPTION');
          SetMacro('CONDITION', 'OPTION_TAG=:OTag and CL_MAIL_BOX_GROUP_NBR=:MB_NBR');
          SetParam('OTag', VmrDirectEmailVouchers);
          SetParam('MB_NBR', DM_CLIENT.CL.FieldByName('PR_CHECK_MB_GROUP_NBR').AsInteger);
          ctx_DataAccess.GetCustomData(cdDirectEmailVoucgher, AsVariant);
          DirectEmailVoucgherFlag := Trim(cdDirectEmailVoucgher.FieldByName('OPTION_STRING_VALUE').AsString) = 'Y';
        end;

      end;
      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
          if cdEeSetup.Active and DirectEmailVoucgherFlag and cdEeSetup.FindKey([ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsInteger]) then
          begin
            if Sign < 0 then
            begin
              if (cdEeSetup['PRINT_VOUCHER'] = 'N') or
                 ((cdEeSetup['WEB_PASSWORD'] <>'') and
                  (cdEeSetup['WEB_PASSWORD'] <>null) and
                  (cdEeSetup['E_MAIL_ADDRESS'] <>'') and
                  (cdEeSetup['E_MAIL_ADDRESS'] <>null)) then
                     INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat*Sign);
            end else
              if (cdEeSetup['PRINT_VOUCHER'] = 'Y')and
                 (cdEeSetup['WEB_PASSWORD'] <>'') and
                 (cdEeSetup['WEB_PASSWORD'] <>null) and
                 (cdEeSetup['E_MAIL_ADDRESS'] <>'') and
                 (cdEeSetup['E_MAIL_ADDRESS'] <>null)then
                     INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat*Sign);

          end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
    end;
  end;

  procedure Count_InternalPrintedChecks;
  var
    cdDirectEmailVoucgher: TevClientDataSet;
    DirectEmailVoucgherFlag:boolean;
  begin
    if not CheckVmrSetupActive then
    begin
      with INVOICE_DM do
      begin
        with TExecDSWrapper.Create('GenericSelect2CurrentNBRWithConditionGrouped') do
        begin
          SetMacro('Columns', 'T1.EE_NBR, T1.' + INVOICE_DM.sLevel + ', T1.PRINT_VOUCHER, T2.NET_WAGES , Count(*) cnt');
          SetMacro('Table1', 'EE');
          SetMacro('Table2', 'PR_CHECK');
          SetMacro('NbrField', 'PR');
          SetMacro('JoinField', 'EE');
          SetParam('RecordNbr', prNumber);
          SetMacro('Condition', 'CHECK_TYPE=''' + CHECK_TYPE2_REGULAR + '''');
          SetMacro('Group', 'T1.EE_NBR, T1.' + INVOICE_DM.sLevel + ', T1.PRINT_VOUCHER, T2.NET_WAGES ');
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.Open;
        DBDT_Tables.Add('EE');
        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
          if (ctx_DataAccess.CUSTOM_VIEW['PRINT_VOUCHER']='N') and
             (ConvertNull(ctx_DataAccess.CUSTOM_VIEW['NET_WAGES'],0)=0) // direct deposit
          then INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat*(-1));
          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;
      end;
    end
    else begin
      with INVOICE_DM do
      begin
        with TExecDSWrapper.Create('select T1.EE_NBR, T1.' + INVOICE_DM.sLevel + ', T1.PRINT_VOUCHER, T2.WEB_PASSWORD, T2.E_MAIL_ADDRESS, T3.NET_WAGES, Count(*) cnt '+
                                   'from PR_CHECK T3, EE T1, CL_PERSON T2 where T3.PR_NBR = :prnbr and T3.EE_NBR = T1.EE_NBR and T1.CL_PERSON_NBR = T2.CL_PERSON_NBR '+
                                   'and {AsOfNow<T1>} and {AsOfNow<T2>} and {AsOfNow<T3>} and T3.CHECK_TYPE=''R'' '+
                                   'group by T1.EE_NBR, T1.' + INVOICE_DM.sLevel + ', T1.PRINT_VOUCHER, T2.WEB_PASSWORD, T2.E_MAIL_ADDRESS, T3.NET_WAGES ') do
        begin
          SetParam('prnbr', prNumber);
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.Open;
        DBDT_Tables.Add('EE');

        cdDirectEmailVoucgher := TevClientDataSet.Create(nil);
        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('COLUMNS', 'CL_MAIL_BOX_GROUP_NBR, OPTION_STRING_VALUE');
          SetMacro('TABLENAME', 'CL_MAIL_BOX_GROUP_OPTION');
          SetMacro('CONDITION', 'OPTION_TAG=:OTag and CL_MAIL_BOX_GROUP_NBR=:MB_NBR');
          SetParam('OTag', VmrDirectEmailVouchers);
          SetParam('MB_NBR', DM_CLIENT.CL.FieldByName('PR_CHECK_MB_GROUP_NBR').AsInteger);
          ctx_DataAccess.GetCustomData(cdDirectEmailVoucgher, AsVariant);
          DirectEmailVoucgherFlag := Trim(cdDirectEmailVoucgher.FieldByName('OPTION_STRING_VALUE').AsString) = 'Y';
        end;

        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
          if (ctx_DataAccess.CUSTOM_VIEW['PRINT_VOUCHER'] = 'Y') and
             (DirectEmailVoucgherFlag and
              (ctx_DataAccess.CUSTOM_VIEW['WEB_PASSWORD'] <>'') and
              (ctx_DataAccess.CUSTOM_VIEW['WEB_PASSWORD'] <>null) and
              (ctx_DataAccess.CUSTOM_VIEW['E_MAIL_ADDRESS'] <>'') and
              (ctx_DataAccess.CUSTOM_VIEW['E_MAIL_ADDRESS'] <>null))and
             (ConvertNull(ctx_DataAccess.CUSTOM_VIEW['NET_WAGES'],0)=0)
          then
           INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat*(-1))
         else if (ctx_DataAccess.CUSTOM_VIEW['PRINT_VOUCHER'] = 'N') and
                 (ConvertNull(ctx_DataAccess.CUSTOM_VIEW['NET_WAGES'],0)=0) then
           INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat*(-1));

          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;
      end;
    end;
  end;

  procedure Count_PrintedChecks;
  begin
     CountPRandEERecordEntries('PR_CHECK',
     'CHECK_TYPE = ''' + CHECK_TYPE2_REGULAR + '''');
     Count_InternalPrintedChecks;
  end;

  procedure Count_VMR(const OptionTag: string);
  begin
    with INVOICE_DM do
    begin
      with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
      begin
        SetMacro('Columns', 'count(*) cnt');
        SetMacro('Table1', 'SB_MAIL_BOX');
        SetMacro('Table2', 'SB_MAIL_BOX_OPTION');
        SetMacro('JoinField', 'SB_MAIL_BOX');
        SetMacro('Condition', 'T1.pr_nbr = :PrNbr and T1.cl_nbr = :ClNbr and T2.option_tag=:Tag');
        SetParam('PrNbr', prNumber);
        SetParam('ClNbr', ctx_DataAccess.ClientID);
        SetParam('Tag', OptionTag);
        ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      end;

      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
      ctx_DataAccess.SB_CUSTOM_VIEW.Open;
      DistrToDBDT(ctx_DataAccess.SB_CUSTOM_VIEW.Fields[0].AsInteger);
    end;
  end;

  procedure Count_TotalNumberOfCopies;
  var
    Qty: Integer;
  begin
    with INVOICE_DM do
    begin
      Qty := 0;

      DM_Company.CO_REPORTS.DataRequired('co_nbr=' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));

      with DM_COMPANY.CO_REPORTS do
      begin
        First;
        while not Eof do
        begin
          Inc(Qty, GetNumberOfCopies);
          Next;
        end;
      end;
      DistrToDBDT(Qty);
    end;
  end;

  procedure Checks_Billing_Payment;
  var
    Qty: Integer;
  begin
    Qty := Integer(PaymentMethod[1] in [BILLING_PAYMENT_METHOD_CLIENT, BILLING_PAYMENT_METHOD_SB]);
    INVOICE_DM.DistrToDBDT(Qty);
  end;

  procedure Checks_EE;
  begin
    with INVOICE_DM do
    begin
      with TExecDSWrapper.Create('GenericSelect2CurrentGrouped') do
      begin
        SetMacro('Columns', 'T1.' + INVOICE_DM.sLevel + ', Count(distinct T1.EE_NBR) cnt');
        SetMacro('Table1', 'EE');
        SetMacro('Table2', 'PR_CHECK');
        SetMacro('JoinField', 'EE');
        SetMacro('NbrField', 'PR');
        SetParam('RecordNbr', prNumber);
        SetMacro('Group', 'T1.' + INVOICE_DM.sLevel);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;

      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.Open;

      DBDT_Tables.Add('EE');

      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
    end;
  end;

  function GetPreviousPayrollProcessDate: TDateTime;
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', 'Max(PROCESS_DATE)');
      SetMacro('TableName', 'PR');
      SetMacro('Condition', 'CO_NBR = :CoNbr and PROCESS_DATE < :ProcessDate and PAYROLL_TYPE in (''' + PAYROLL_TYPE_REGULAR + ''',''' + PAYROLL_TYPE_SUPPLEMENTAL + ''',''' + PAYROLL_TYPE_CL_CORRECTION + ''')');
      SetParam('ProcessDate', DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime);
      SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    Result := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsDateTime;
  end;

  procedure Count_New_Prenotes;
  begin
    with TExecDSWrapper.Create('NewPrenotes') do
    begin
      SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
      SetMacro('Level', INVOICE_DM.sLevel);
      SetParam('PrevCheckDate', GetPreviousPayrollProcessDate);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    INVOICE_DM.DBDT_Tables.Add('EE');

    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.Fields[1].AsFloat);
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
  end;

  procedure Count_EE_Change;
  var
    PrevCheckDate: TDateTime;

    function AlwaysGetVersionFieldAudit(const ATable, AField: String; const ANbr: Integer; const ADate :TDateTime):IisParamsCollection;
    var
      lvData :IisListOfValues;
    begin
      try
        lvData:=ctx_DBAccess.GetVersionFieldAudit(ATable, AField, ANbr, ADate);
        if Assigned(lvData) and (lvData.Count = 1) then
          AlwaysGetVersionFieldAudit := IInterface(lvData[0].Value) as IisParamsCollection
        else
          AlwaysGetVersionFieldAudit := nil;
      except
        on EAssertionFailed do
          AlwaysGetVersionFieldAudit := nil;
      end;
    end;

    function GetFieldValueForDeletedRecord(const ATable, AField: String; const ANbr: Integer; const ADate :TDateTime):Variant;
    var
      pcData: IisParamsCollection;
      i: Integer;
      ct, ct1: TDateTime;
      ci, ci1: Integer;
      ds :IEvDataSet;
      res: Variant;
    begin
      pcData :=  AlwaysGetVersionFieldAudit(ATable, AField, ANbr, ADate);
      if not Assigned(pcData) then
      begin
        GetFieldValueForDeletedRecord:=null;
        exit;
      end;
      if pcData.Count>0 then
      begin
        ci := 0;
        ci1 := ci;
        ct := pcData[0].Value['ChangeTime'];
        ct1 := ct;
        for i:=1 to pcData.Count-1 do
          if (pcData[i].Value['ChangeTime']>ct) then
          begin
            ci1 := ci;
            ct1 := ct;
            ci := i;
            ct := pcData[i].Value['ChangeTime'];
          end
          else if (ct1 = ct) or (pcData[i].Value['ChangeTime']>ct1) then
          begin
            ci1 := i;
            ct1 := pcData[i].Value['ChangeTime'];
          end;
        ds := IInterface(pcData[ci1].Value['Data']) as IevDataSet;
        res := null;
        ds.First();
        while not ds.Eof do
        begin
          if (ds['begin_date']<=ADate) and (ds['end_date']>ADate) then
            res := ds['value'];
          ds.Next();
        end
      end;
      GetFieldValueForDeletedRecord := res;
    end;

    procedure AddEEData(AData: IEvDataSet; AEmployee: Integer; ACompany: Integer; ADBDT: String);
    var
     Q: IEvQuery;
     Value: Variant;
     liPerson :Integer;
     liDBDT :Integer;
     lDTNow :TDateTime;
    begin
      if AData.Locate('ee_nbr', AEmployee, []) then
        exit;
      Q := TEvQuery.CreateFmt('SELECT EE_NBR, CL_PERSON_NBR, %s FROM EE WHERE EE_NBR=%d and CO_NBR=%d and {AsOfNow<EE>}',[ADBDT, AEmployee, ACompany]);
      Q.Result.First();
      if not Q.Result.Eof then
      begin
        AData.AppendRecord([
          Q.Result['ee_nbr'],
          Q.Result['cl_person_nbr'],
          Q.Result[ADBDT],
          'U',0]);
      end
      else
      begin
        lDTNow:=Now();
        Value := GetFieldValueForDeletedRecord('EE', 'CO_NBR', AEmployee, lDTNow);
        if (not VarIsNull(Value)) then
          if (Value=ACompany) then
          begin
            Value:=GetFieldValueForDeletedRecord('EE', 'CL_PERSON_NBR', AEmployee, lDTNow);
            if VarIsNull(Value) then
              liPerson := 0
            else
              liPerson := Value;
            Value:=GetFieldValueForDeletedRecord('EE', ADBDT, AEmployee, lDTNow);
            if VarIsNull(Value) then
              liDBDT := 0
            else
              liDBDT := Value;
            AData.AppendRecord([
              Q.Result['record_nbr'],
              liPerson,
              liDBDT,
              'D',0]);
          end;
      end;
    end;

    procedure IncEEData(const AData: IEvDataSet; const AEmployee: Integer; AVal :Double; ACompany: Integer; ADBDT: String);
    begin
      AddEEData(AData,AEmployee,ACompany,ADBDT);
      if AData.Locate('ee_nbr',AEmployee,[]) then
      begin
        AData.Edit();
        if AData['type']='I' then
          AData['cnt'] := AVal
        else
          AData['cnt'] := AData['cnt']+AVal;
        AData.Post();
      end;
    end;

    procedure PrepareEE(const AData: IEvDataSet; const ACompany: Integer; const ADBDT: String; const AStart: TDateTime);
    var
     Q: IEvQuery;
     Value: Variant;
     liPerson :Integer;
     liDBDT :Integer;
    begin
      AData.Clear();
      // Get exists EE
      Q := TEvQuery.CreateFmt('SELECT EE_NBR, CL_PERSON_NBR, %s FROM EE WHERE CO_NBR=%d and {AsOfNow<EE>}',[ADBDT,ACompany]);
      Q.Result.First();
      while (not Q.Result.Eof) do
      begin
        AData.AppendRecord([
          Q.Result['EE_NBR'],
          Q.Result['CL_PERSON_NBR'],
          Q.Result[ADBDT],
          'U',0]);
        Q.Result.Next();
      end;
      // Get deleted EE
      Q := TEvQuery.Create('{SelectDeletedRecordsNbrForBilling<'+GetDBNameByTable('EE')+',EE>}');
      Q.Param['time_b'] := AStart;
      Q.Result.First();
      while (not Q.Result.Eof) do
      begin
        if (not AData.Locate('EE_NBR', Q.Result['record_nbr'],[])) then
        begin
          Value:=GetFieldValueForDeletedRecord('EE', 'CO_NBR', Q.Result['record_nbr'], Q.Result['commit_time']);
          if (not VarIsNull(Value)) then
            if (Value=ACompany) then
            begin
              Value:=GetFieldValueForDeletedRecord('EE', 'CL_PERSON_NBR', Q.Result['record_nbr'], Q.Result['commit_time']);
              if VarIsNull(Value) then
                liPerson := 0
              else
                liPerson := Value;
              Value:=GetFieldValueForDeletedRecord('EE', ADBDT, Q.Result['record_nbr'], Q.Result['commit_time']);
              if VarIsNull(Value) then
                liDBDT := 0
              else
                liDBDT := Value;
              AData.AppendRecord([
                Q.Result['record_nbr'],
                liPerson,
                liDBDT,
                'D',0]);
            end;
        end;
        Q.Result.Next();
      end;
      // Mark inserted EE
      Q := TEvQuery.Create('{SelectInsertedRecordsNbrForBilling<'+GetDBNameByTable('EE')+',EE>}');
      Q.Param['time_b'] := AStart;
      Q.Result.First();
      while (not Q.Result.Eof) do
      begin
        if AData.Locate('EE_NBR', Q.Result['record_nbr'],[]) then
        begin
          AData.Edit();
          AData['type']:='I';
          AData.Post();
        end;
        Q.Result.Next();
      end
    end;

    procedure Count_Fields_Table_Changes(const ATable: string; const AData: IEvDataSet; const ACompany: Integer;
                      const ADBDT: String; const AStart: TDateTime; const AFields :String);
    var
     Q, Q1: IEvQuery;
     lvRes: Variant;
     liNbr: Integer;
     liTNbr: Integer;
     lbFound: Boolean;
     lssFields: IisStringList;
    begin
      lssFields := TisStringList.CreateUnique();
      lssFields.CommaText := AFields;

      Q := TEvQuery.CreateFmt('{SelectTableChangesForBilling<%s,%s>}',[GetDBNameByTable(ATable),ATable]);
      Q.Param['time_b'] := AStart;
      Q.Result.First();
      while (not Q.Result.Eof) do
      begin
        liNbr := Q.Result['record_nbr'];
        liTNbr := Q.Result['nbr'];
        lbFound := lssFields.Count=0;
        if not lbFound then
        begin
          Q1 := TEvQuery.CreateFmt('{SelectFieldChangesForBilling<%s,%d,%d>}',[GetDBNameByTable(ATable),liNbr,liTNbr]);
          Q1.Result.First();
          while not Q1.Result.Eof and not lbFound do
          begin
            lbFound:=(lssFields.IndexOf(Q1.Result.FieldByName('name').AsString)<>-1) or (Q1.Result.FieldByName('change_type').AsString[1]='I') or (Q1.Result.FieldByName('change_type').AsString[1]='D');
            Q1.Result.Next();
          end;
        end;
        if lbFound then
        begin
          if ATable='EE' then
            lvRes := liNbr
          else
          begin
            Q1 := TEvQuery.CreateFmt('SELECT EE_NBR FROM %s WHERE %s_NBR=%d and {AsOfNow<%s>}',[ATable,ATable,liNbr,ATable]);
            Q1.Result.First();
            if not Q1.Result.Eof then
              lvRes := Q1.Result['EE_NBR']
            else
              lvRes := GetFieldValueForDeletedRecord(ATable, 'EE_NBR', liNbr, AStart);
          end;
          if not VarIsNull(lvRes) then
            IncEEData(AData, lvRes, 1, ACompany, ADBDT);
        end;
        Q.Result.Next();
      end;
    end;

    procedure Count_Table_Changes(const ATable: string; const AData: IEvDataSet; const ACompany: Integer; const ADBDT: String; const AStart: TDateTime);
    begin
      Count_Fields_Table_Changes(ATable, AData, ACompany, ADBDT, AStart, '');
    end;

    procedure Count_CL_PERSON_Changes(const AData:  IEvDataSet; const ACompany: Integer; const ADBDT: String; const AStart: TDateTime);
    var
     Q, Q1: IEvQuery;
     liNbr: Integer;
     liCnt: Integer;
    begin
      Q := TEvQuery.CreateFmt('{SelectTableChangesForBilling<%s,%s>}',[GetDBNameByTable('CL_PERSON'),'CL_PERSON']);
      Q.Param['time_b'] := AStart;
      Q.Result.First();
      while (not Q.Result.Eof) do
      begin
        liNbr := Q.Result['record_nbr'];
        Q1 := TEvQuery.CreateFmt('SELECT ee_nbr FROM ee WHERE (cl_person_nbr=%d) and (co_nbr=%d) and {AsOfNow<ee>}',[liNbr, ACompany]);
        if Q1.Result.RecordCount>0 then
        begin
          liCnt := Q1.Result.RecordCount;
          Q1.Result.First();
          while (not Q1.Result.Eof) do
          begin
            IncEEData(AData, Q1.Result['ee_nbr'], 1/liCnt, ACompany, ADBDT);
            Q1.Result.Next();
          end
        end;
        Q.Result.Next();
      end;
    end;

  var
    BilledFields: String;
    i: Integer;
    EEData: IevDataSet;
  const

    EeBilledFields: array[0..83] of String = (
      'CO_DIVISION_NBR',
      'CO_BRANCH_NBR',
      'CO_DEPARTMENT_NBR',
      'CO_TEAM_NBR',
      'CUSTOM_EMPLOYEE_NUMBER',
      'CO_HR_APPLICANT_NBR',
      'CO_HR_RECRUITERS_NBR',
      'CO_HR_REFERRALS_NBR',
      'ADDRESS1',
      'ADDRESS2',
      'CITY',
      'STATE',
      'ZIP_CODE',
      'COUNTY',
      'PHONE1',
      'DESCRIPTION1',
      'PHONE2',
      'DESCRIPTION2',
      'PHONE3',
      'DESCRIPTION3',
      'E_MAIL_ADDRESS',
      'TIME_CLOCK_NUMBER',
      'ORIGINAL_HIRE_DATE',
      'CURRENT_HIRE_DATE',
      'CURRENT_TERMINATION_DATE',
      'CURRENT_TERMINATION_CODE',
      'CO_HR_SUPERVISORS_NBR',
      'SALARY_AMOUNT',
      'STANDARD_HOURS',
      'AUTOPAY_CO_SHIFTS_NBR',
      'CO_HR_POSITIONS_NBR',
      'POSITION_EFFECTIVE_DATE',
      'POSITION_STATUS',
      'CO_HR_PERFORMANCE_RATINGS_NBR',
      'REVIEW_DATE',
      'NEXT_REVIEW_DATE',
      'PAY_FREQUENCY',
      'NEXT_RAISE_DATE',
      'NEXT_RAISE_AMOUNT',
      'NEXT_RAISE_PERCENTAGE',
      'NEXT_RAISE_RATE',
      'NEXT_PAY_FREQUENCY',
      'TIPPED_DIRECTLY',
      'ALD_CL_E_D_GROUPS_NBR',
      'DISTRIBUTE_TAXES',
      'CO_PAY_GROUP_NBR',
      'CL_DELIVERY_GROUP_NBR',
      'WORKERS_COMP_WAGE_LIMIT',
      'GROUP_TERM_POLICY_AMOUNT',
      'CO_UNIONS_NBR',
      'EIC',
      'FLSA_EXEMPT',
      'W2_TYPE',
      'W2_PENSION',
      'W2_DEFERRED_COMP',
      'W2_DECEASED',
      'W2_STATUTORY_EMPLOYEE',
      'W2_LEGAL_REP',
      'HOME_TAX_EE_STATES_NBR',
      'EXEMPT_EMPLOYEE_OASDI',
      'EXEMPT_EMPLOYEE_MEDICARE',
      'EXEMPT_EXCLUDE_EE_FED',
      'EXEMPT_EMPLOYER_OASDI',
      'EXEMPT_EMPLOYER_MEDICARE',
      'EXEMPT_EMPLOYER_FUI',
      'OVERRIDE_FED_TAX_VALUE',
      'OVERRIDE_FED_TAX_TYPE',
      'GOV_GARNISH_PRIOR_CHILD_SUPPT',
      'SECURITY_CLEARANCE',
      'CALCULATED_SALARY',
      'BADGE_ID',
      'ON_CALL_FROM',
      'ON_CALL_TO',
      'BASE_RETURNS_ON_THIS_EE',
      'GENERAL_LEDGER_TAG',
      'COMPANY_OR_INDIVIDUAL_NAME',
      'FEDERAL_MARITAL_STATUS',
      'NUMBER_OF_DEPENDENTS',
      'DISTRIBUTION_CODE_1099R',
      'TAX_AMT_DETERMINED_1099R',
      'TOTAL_DISTRIBUTION_1099R',
      'PENSION_PLAN_1099R',
      'MAKEUP_FICA_ON_CLEANUP_PR',
      'SY_HR_EEO_NBR');

    EeSchedEDsBilledFields: array[0..40] of String = (
      'CL_AGENCY_NBR',
      'CL_E_DS_NBR',
      'CL_E_D_GROUPS_NBR',
      'EE_DIRECT_DEPOSIT_NBR',
      'CO_BENEFIT_SUBTYPE_NBR',
      'PLAN_TYPE',
      'AMOUNT',
      'PERCENTAGE',
      'MINIMUM_WAGE_MULTIPLIER',
      'TARGET_AMOUNT',
      'FREQUENCY',
      'EXCLUDE_WEEK_1',
      'EXCLUDE_WEEK_2',
      'EXCLUDE_WEEK_3',
      'EXCLUDE_WEEK_4',
      'EXCLUDE_WEEK_5',
      'EFFECTIVE_START_DATE',
      'EFFECTIVE_END_DATE',
      'MINIMUM_PAY_PERIOD_AMOUNT',
      'MINIMUM_PAY_PERIOD_PERCENTAGE',
      'MIN_PPP_CL_E_D_GROUPS_NBR',
      'MAXIMUM_PAY_PERIOD_AMOUNT',
      'MAXIMUM_PAY_PERIOD_PERCENTAGE',
      'MAX_PPP_CL_E_D_GROUPS_NBR',
      'ANNUAL_MAXIMUM_AMOUNT',
      'TARGET_ACTION',
      'NUMBER_OF_TARGETS_REMAINING',
      'WHICH_CHECKS',
      'CALCULATION_TYPE',
      'EE_CHILD_SUPPORT_CASES_NBR',
      'CHILD_SUPPORT_STATE',
      'MAXIMUM_GARNISH_PERCENT',
      'TAKE_HOME_PAY',
      'DEDUCT_WHOLE_CHECK',
      'ALWAYS_PAY',
      'SCHEDULED_E_D_GROUPS_NBR',
      'PRIORITY_NUMBER',
      'GARNISNMENT_ID',
      'THRESHOLD_AMOUNT',
      'BENEFIT_AMOUNT_TYPE',
      'THRESHOLD_E_D_GROUPS_NBR');

   EeToaBilledFields: array[0..7] of String = (
      'CO_TIME_OFF_ACCRUAL_NBR',
      'EFFECTIVE_ACCRUAL_DATE',
      'NEXT_ACCRUE_DATE',
      'ANNUAL_ACCRUAL_MAXIMUM',
      'OVERRIDE_RATE',
      'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR',
      'ROLLOVER_DATE',
      'NEXT_RESET_DATE');

   EEDSFldsInfo: array [0..4] of TDSFieldDef = (
    (FieldName: 'ee_nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'cl_person_nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'dbdt';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'type';  DataType: ftString;  Size: 1;  Required: False),
    (FieldName: 'cnt';  DataType: ftFloat;  Size: 0;  Required: False));

  begin
    PrevCheckDate := GetPreviousPayrollProcessDate;

    EEData := TevDataSet.Create(EEDSFldsInfo);
    EEData.vclDataSet.LogChanges := false;
    PrepareEE(EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);

    Count_CL_PERSON_Changes(EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);

    BilledFields := '';
    for i := Low(EeBilledFields) to High(EeBilledFields) do
      BilledFields := BilledFields + '"' + EeBilledFields[i] + '",';
    SetLength(BilledFields,(Length(BilledFields))-1);

    Count_Fields_Table_Changes('EE', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate, BilledFields);
    Count_Table_Changes('EE_CHILD_SUPPORT_CASES', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);
    Count_Table_Changes('EE_DIRECT_DEPOSIT', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);
    Count_Table_Changes('EE_LOCALS', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);
    Count_Table_Changes('EE_STATES', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);
    Count_Table_Changes('EE_RATES', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);

    BilledFields := '';
    for i := Low(EeSchedEDsBilledFields) to High(EeSchedEDsBilledFields) do
      BilledFields := BilledFields + '"'+EeSchedEDsBilledFields[i] + '",';
    SetLength(BilledFields,(Length(BilledFields))-1);

    Count_Fields_Table_Changes('EE_SCHEDULED_E_DS', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate, BilledFields);

    BilledFields := '';
    for i := Low(EeToaBilledFields) to High(EeToaBilledFields) do
      BilledFields := BilledFields + '"'+EeToaBilledFields[i] + '",';
    SetLength(BilledFields,(Length(BilledFields))-1);

    Count_Fields_Table_Changes('EE_TIME_OFF_ACCRUAL', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate, BilledFields);
    Count_Table_Changes('EE_WORK_SHIFTS', EEData, DM_COMPANY.CO['CO_NBR'], INVOICE_DM.sLevel, PrevCheckDate);

    INVOICE_DM.DBDT_Tables.Add('ALL_EE');
    EEData.First();
    while not EEData.Eof do
    begin
      INVOICE_DM.AddToDBDT(EEData.FieldByName('dbdt').AsInteger,EEData.FieldByName('cnt').AsFloat);
      EEData.Next();
    end;

  end;

  procedure Count_EE_Paid_Per_Month(const d: TDateTime);
  var
    Q: IevQuery;
    bDate, eDate: TDateTime;
  begin
    bDate := GetBeginMonth(GetBeginMonth(d)-1);    //  begin of prev month
    eDate := GetBeginMonth(d)-1;                  //  end of prev month

    Q := TevQuery.Create(
              'select #Columns from EE a, PR_CHECK b, PR c where ' +
               '{AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
               'a.EE_NBR = b.EE_NBR and ' +
               'b.PR_NBR = c.PR_NBR and ' +
               'a.CO_NBR= :CO_NBR and ' +
               'c.CHECK_DATE >= :bDate and c.CHECK_DATE <= :eDate and ' +
               'c.STATUS =''P'' and ' +               
               'c.PAYROLL_TYPE <> ''E'' and ' +
               'c.PAYROLL_TYPE <> ''A'' and ' +
               'c.PAYROLL_TYPE <> ''I'' ' +
              'group by #Group ');

    Q.Macros.AddValue('Columns', 'a.'+ INVOICE_DM.sLevel + ', Count(distinct a.EE_NBR) cnt');
    Q.Macros.AddValue('Group', 'a.'+ INVOICE_DM.sLevel);
    Q.Params.AddValue('CO_NBR', DM_COMPANY.CO['CO_NBR']);
    Q.Params.AddValue('bDate', bDate);
    Q.Params.AddValue('eDate', eDate);
    Q.execute;

    INVOICE_DM.DBDT_Tables.Add('ALL_EE');
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      INVOICE_DM.AddToDBDT(Q.Result.FieldByName(INVOICE_DM.sLevel).AsInteger, Q.Result.FieldByName('cnt').AsFloat);
      Q.Result.Next;
    end;
  end;

  procedure Count_Status_EE(DateFieldName: string);
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentWithConditionGrouped') do
    begin
      SetMacro('Columns', INVOICE_DM.sLevel + ', count(*) cnt');
      SetMacro('TableName', 'EE');
      SetMacro('Condition', 'CO_NBR = :CoNbr and ' + DateFieldName + ' > :PrevCheckDate and ' + DateFieldName + ' <= :CheckDate');
      SetParam('PrevCheckDate', GetPreviousPayrollProcessDate);
      SetParam('CheckDate', DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime);
      SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
      SetMacro('Group', INVOICE_DM.sLevel);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    INVOICE_DM.DBDT_Tables.Add('ALL_EE');
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
  end;

  procedure Count_New_Hired_EE;
  begin
    Count_Status_EE('CURRENT_HIRE_DATE');
  end;

  procedure Count_Termed_EE;
  begin
    Count_Status_EE('CURRENT_TERMINATION_DATE');
  end;

  procedure Count_CheckLines_with_DD;
  begin
    with TExecDSWrapper.Create('CheckLinesWithDD') do
    begin
      SetMacro('Group', 'E.' + INVOICE_DM.sLevel);
      SetParam('PrNbr', prNumber);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    INVOICE_DM.DBDT_Tables.Add('EE');
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
  end;

  procedure CountActiveHourlyEE;
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentWithConditionGrouped') do
    begin
      SetMacro('Columns', INVOICE_DM.sLevel + ', count(*) cnt');
      SetMacro('TableName', 'EE');
      SetMacro('Condition', '(CO_NBR = :CoNbr) and (CURRENT_TERMINATION_CODE in ('+
                Format('''%s'',''%s'',''%s'',''%s'',''%s''',[EE_TERM_ACTIVE, EE_TERM_SEASONAL, EE_TERM_LEAVE, EE_TERM_MILITARY_LEAVE, EE_TERM_FMLA])+
                ')) and (coalesce(SALARY_AMOUNT,0)=0)');
//      SetParam('TermCodes', Format('''%s'',''%s'',''%s'',''%s'',''%s''',[EE_TERM_ACTIVE, EE_TERM_SEASONAL, EE_TERM_LEAVE, EE_TERM_MILITARY_LEAVE, EE_TERM_FMLA]));
      SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
      SetMacro('Group', INVOICE_DM.sLevel);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    INVOICE_DM.DBDT_Tables.Add('ALL_EE');
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      INVOICE_DM.AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, ctx_DataAccess.CUSTOM_VIEW.FieldByName('cnt').AsFloat);
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;
  end;

  type
    TValidMonths = set of 1..12;

  function LastPRForMonth(PRCheckDate: TDateTime; ValidMonths: TValidMonths): boolean;
  var
    Next, t: TDateTime;
    Year, Month, NextMonth, Day: Word;
    Delta: Integer;
  begin
    result := False;
    DecodeDate(PRCheckDate, Year, Month, Day);
    if not (Month in ValidMonths) then
      Exit;

    if INVOICE_DM.IsScheduled then
      if Copy(DM_COMPANY.CO_SERVICES.FieldByName('WEEK_NUMBER').AsString, 1, 1) = SCHED_FREQ_LAST_SCHED_PAY then
      begin
        Next := ctx_PayrollCalculation.NextCheckDate(PRCheckDate);
        if Next <> 0 then
          DecodeDate(Next, Year, NextMonth, Day)
        else
          NextMonth := 0;
        DecodeDate(PRCheckDate, Year, Month, Day);
        result := Month <> NextMonth;
      end
      else if Copy(DM_COMPANY.CO_SERVICES.FieldByName('WEEK_NUMBER').AsString, 1, 1) = SCHED_FREQ_FIRST_SCHED_PAY then
      begin
        DecodeDate(PRCheckDate, Year, Month, Day);
        Next := EncodeDate(Year, Month, 1) - 1;
        Next := ctx_PayrollCalculation.NextCheckDate(Next);
        result := Next = PRCheckDate;
      end
      else if Copy(DM_COMPANY.CO_SERVICES.FieldByName('WEEK_NUMBER').AsString, 1, 1) = SCHED_FREQ_MID_SCHED_PAY then
      begin
        DecodeDate(PRCheckDate, Year, Month, Day);
        Next := EncodeDate(Year, Month, 1) - 1;
        t := Next;
        Delta := 16;
        while Next <> 0 do
        begin
          t := Next;
          Next := ctx_PayrollCalculation.NextCheckDate(t);
          if Delta > Trunc(Abs(Next - EncodeDate(Year, Month, 15))) then
            Delta := Trunc(Abs(Next - EncodeDate(Year, Month, 15)))
          else
            Break;
        end;
        result := PRCheckDate = t;
      end;
  end;

  function IsLastPRForMonth(ValidMonths: TValidMonths): boolean;
  var
    Rec: Integer;
  begin
    with DM_PAYROLL.PR_SCHEDULED_EVENT do
    begin
      with ctx_DataAccess.CUSTOM_VIEW do
      begin
        if Active then
          Close;
        with TExecDSWrapper.Create('CheckServiceInBilling') do
        begin
          SetParam('CheckDate', INVOICE_DM.PRCheckDate);
          SetParam('CoServicesNbr', DM_COMPANY.CO_SERVICES['CO_SERVICES_NBR']);
          DataRequest(AsVariant);
        end;
        Open;
        if Fields[0].AsInteger > 0 then
        begin
          Result := False;
          Exit;
        end;
      end;

      Result := LastPRForMonth(INVOICE_DM.PRCheckDate, ValidMonths);
      if not Result then
      begin
        with ctx_DataAccess.CUSTOM_VIEW do
        begin
          if Active then
            Close;
          with TExecDSWrapper.Create('GenericSelectCurrentOrdered') do
          begin
            SetMacro('Columns', 'PR_NBR, CHECK_DATE, STATUS');
            SetMacro('TableName', 'PR');
            SetMacro('Order', 'CHECK_DATE, PR_NBR');
            DataRequest(AsVariant);
          end;
          Open;
        end;
        IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
        Last;
        while not BOF and
              (FieldByName('SCHEDULED_CHECK_DATE').AsDateTime >= INVOICE_DM.PRCheckDate) do
          Prior;
        while not BOF and not Result and
              (DM_COMPANY.CO_SERVICES.FieldByName('EFFECTIVE_START_DATE').AsDateTime <= FieldByName('SCHEDULED_CHECK_DATE').AsDateTime) and
              ((FieldByName('PR_NBR').AsInteger = prNumber) or
               (VarToStr(ctx_DataAccess.CUSTOM_VIEW.Lookup('PR_NBR', FieldByName('PR_NBR').Value, 'STATUS')) <> PAYROLL_STATUS_PROCESSED)) do
        begin
          Rec := Recno;
          Result := LastPRForMonth(FieldByName('SCHEDULED_CHECK_DATE').AsDateTime, ValidMonths);
          IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
          Recno := Rec;
          Prior;
        end;
        IndexFieldNames := '';
      end;
    end;
  end;

  function IsLastPRForQuarter: boolean;
  var
    vm: TValidMonths;
    i: Integer;
  begin
    vm := [];
    result := False;
    with INVOICE_DM do
      if FISCAL_YEAR_END <> 0 then
      begin
        for i := 1 to 12 do
          if FISCAL_YEAR_END mod 3 = i mod 3 then
            Include(vm, i);
        result := IsLastPRForMonth(vm);
      end;
  end;

  function IsLastPRForSemiAnnual: boolean;
  var
    vm: TValidMonths;
    i: Integer;
  begin
    vm := [];
    result := False;
    with INVOICE_DM do
      if FISCAL_YEAR_END <> 0 then
      begin
        for i := 1 to 12 do
          if FISCAL_YEAR_END mod 6 = i mod 6 then
            Include(vm, i);
        result := IsLastPRForMonth(vm);
      end;
  end;

  function IsLastPRForAnnual: boolean;
  begin
    result := False;
    with INVOICE_DM do
      if FISCAL_YEAR_END <> 0 then
        result := IsLastPRForMonth([FISCAL_YEAR_END]);
  end;
  Procedure Clear_cdsDBDTQty;
  begin
      with INVOICE_DM do
      begin
         cdsDBDT.First;
         while not cdsDBDT.Eof do
         begin
           cdsDBDT.Edit;
           cdsDBDT['Qty'] := 0;
           cdsDBDT.Post;
           cdsDBDT.Next;
         end;
      end;
  end;
  Procedure Apply_DiscontTo_cdsDBDT;
    var
      Discount: Currency;
  begin
    Discount :=0;
     with INVOICE_DM do
     begin
        cdsDBDT.First;
        while not cdsDBDT.Eof do
        begin
          case DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_TYPE').AsString[1] of
            'A':
              if cdsDBDT.FieldByName('Qty').AsFloat < 0 then
                Discount := -DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').AsCurrency
              else
                Discount := DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').AsCurrency;
            'P': Discount := cdsDBDT.FieldByName('Qty').AsFloat * DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').AsCurrency * 0.01;
          end;

          // this is if always = false
          if (cdsDBDT.FieldByName('Qty').AsFloat < 0) and (Discount < cdsDBDT.FieldByName('Qty').AsFloat) and
             (cdsDBDT.FieldByName('Qty').AsFloat >= 0) and (Discount > cdsDBDT.FieldByName('Qty').AsFloat) then
            Discount := cdsDBDT.FieldByName('Qty').AsFloat;

          cdsDBDT.Edit;
          cdsDBDT.FieldByName('Amount').AsFloat := cdsDBDT.FieldByName('Qty').AsFloat;
          cdsDBDT.FieldByName('Tax').AsFloat := 0;
          cdsDBDT.FieldByName('Discount').AsFloat := Discount;
          cdsDBDT.Post;
          cdsDBDT.Next;
        end;
     end;
  end;

  Procedure Apply_InvoiceDiscontTo_cdsDBDT;
    var
      //Discount: Currency;
      Invoice_Discount: Currency;
      PrCheckDate:TDateTime;
      InvStartDate,InvEndDate:TDateField;
  begin
    //Discount :=0;
    InvStartDate := DM_COMPANY.CO.DISCOUNT_START_DATE;
    InvEndDate := DM_COMPANY.CO.DISCOUNT_END_DATE;
    PrCheckDate := INVOICE_DM.PRCheckDate;

    if ((InvStartDate.IsNull)or(InvStartDate.Value <= PrCheckDate))
      and ((InvEndDate.IsNull)or(InvEndDate.Value >= PrCheckDate))
    then
        Invoice_Discount:= DM_COMPANY.CO.INVOICE_DISCOUNT.AsCurrency
    else
      Invoice_Discount  := 0;

     with INVOICE_DM do
     begin
        cdsDBDT.First;
        while not cdsDBDT.Eof do
        begin
          cdsDBDT.Edit;

          case DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_TYPE').AsString[1] of
            'A','N':
            begin
             cdsDBDT.FieldByName('Discount').AsFloat := cdsDBDT.FieldByName('Discount').AsFloat +
                (cdsDBDT.FieldByName('Qty').AsFloat{-cdsDBDT.FieldByName('Discount').AsFloat}) *
                 Invoice_Discount * 0.01;
            end;
            'P':

              cdsDBDT.FieldByName('Discount').AsFloat := cdsDBDT.FieldByName('Discount').AsFloat +
                cdsDBDT.FieldByName('Qty').AsFloat * Invoice_Discount * 0.01;

          end;
          cdsDBDT.Post;
          cdsDBDT.Next;
        end;
     end;
  end;


  procedure LocateSB_Services;
  var
    s: String;
  begin
    if not DM_SERVICE_BUREAU.SB_SERVICES.Locate('SB_SERVICES_NBR', DM_COMPANY.CO_SERVICES.FieldByName('SB_SERVICES_NBR').AsString, []) then
    begin
      s := 'The value (CO_SERVICES.SB_SERVICES_NBR = '+
           DM_COMPANY.CO_SERVICES.FieldByName('SB_SERVICES_NBR').AsString
           +') not found in the table S_BUREAU.SB_SERVICES ( field SB_SERVICES_NBR )';
      raise EInconsistentData.CreateHelp(s{'Missing SB_SERVICES_NBR'}, IDH_InconsistentData);
    end;
  end;

  function AddServicesToDetail(HistoryDetailNumber: integer): boolean;
  var
    Qty, Amount, Tax, Discount: Currency;
    PrCheckNbr: Integer;
    aExact: real;
    aRound: Currency;
    sCheckFilter: string;
    dds: TevClientDataSet;
    Nbr: Integer;
    bBlockNegTrust: Boolean;
    sECodes, sDCodes: string;
    CalcAmountIsNeeded : boolean;
    CalcInvoiceDiscontNeeded : boolean;
    CalcAmountType :boolean;
    tMinAmount, pAmount, tMaxAmount: Currency;
    bMinAmount : boolean;

  begin
    result := false;
    CalcAmountIsNeeded:= false;
    CalcInvoiceDiscontNeeded := true;
    CalcAmountType := true;
    with INVOICE_DM do
    begin

      if PRCheckDate < Trunc(DM_COMPANY.CO_SERVICES.FieldByName('EFFECTIVE_START_DATE').AsDateTime) then
        exit;

      if not DM_COMPANY.CO_SERVICES.FieldByName('EFFECTIVE_END_DATE').IsNull then
        if Trunc(DM_COMPANY.CO_SERVICES.FieldByName('EFFECTIVE_END_DATE').AsDateTime) > 0 then
          if PRCheckDate >= Trunc(DM_COMPANY.CO_SERVICES.FieldByName('EFFECTIVE_END_DATE').AsDateTime) then
            exit;

      LocateSB_Services();

      DoIt := False;

      if DM_COMPANY.CO_SERVICES.FieldByName('MONTH_NUMBER').AsString <> '' then
        FISCAL_YEAR_END := StrToInt(Trim(DM_COMPANY.CO_SERVICES.FieldByName('MONTH_NUMBER').AsString))
      else
        FISCAL_YEAR_END := 0;

      if DM_COMPANY.CO_SERVICES.FieldByName('FREQUENCY').AsString = 'N' then
      begin
        DM_COMPANY.CO_SERVICES.Edit;
        DM_COMPANY.CO_SERVICES.FieldByName('FREQUENCY').AsString := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('FREQUENCY').AsString;
        DM_COMPANY.CO_SERVICES.Post;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_SERVICES]);
        Sleep(1000);
      end;
      case DM_COMPANY.CO_SERVICES.FieldByName('FREQUENCY').AsString[1] of
        SCHED_FREQ_DAILY: DoIt := True;
        SCHED_FREQ_SCHEDULED_PAY: DoIt := IsScheduled;
        SCHED_FREQ_SCHEDULED_MONTHLY: DoIt := IsLastPRForMonth([1,2,3,4,5,6,7,8,9,10,11,12]);
        SCHED_FREQ_QUARTERLY: DoIt := IsLastPRForQuarter;
        SCHED_FREQ_SEMI_ANNUALLY: DoIt := IsLastPRForSemiAnnual;
        SCHED_FREQ_ANNUALLY: DoIt := IsLastPRForAnnual;
        SCHED_FREQ_ONE_TIME:
          if Copy(DM_COMPANY.CO_SERVICES.FieldByName('FILLER').AsString, 5, 1) = 'Y' then
            DoIt := False
          else
          begin
            DoIt := True;
            DM_COMPANY.CO_SERVICES.Edit;
            DM_COMPANY.CO_SERVICES.FieldByName('Filler').AsString := PutIntoFiller(DM_COMPANY.CO_SERVICES.FieldByName('Filler').AsString, 'Y', 5, 1);
            DM_COMPANY.CO_SERVICES.Post;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_SERVICES]);
          end;
      end;
      if DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').AsString = GROUP_BOX_CONDITIONAL then
          if DM_PAYROLL.PR_SERVICES.Locate('PR_NBR;CO_SERVICES_NBR',VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,DM_COMPANY.CO_SERVICES.FieldByName('CO_SERVICES_NBR').Value]),[]) then
              if DM_PAYROLL.PR_SERVICES.EXCLUDE.Value = 'Y' then
              begin
                 DoIt := false;
              end;
      if not DoIt then
        Exit;
      Clear_cdsDBDTQty;



      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ALL_CHECKS then
      begin
        Checks_PR;
        Checks_MISC;
        Checks_CO_TAX_DEP;
        Checks_Billing_Payment;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_BILLING_CHECKS_ONLY then
        Checks_Billing_Payment
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_MANUAL_CHECKS then
        Checks_Manual
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ALL_BUT_TAX then
      begin
          Checks_PR;
          Checks_MISC;
          Checks_Billing_Payment;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ALL_BUT_AGENCY then
      begin
          Checks_PR;
          Checks_CO_TAX_DEP;
          Checks_Billing_Payment;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_AGENCY_CHECKS_ONLY then
          Checks_MISC
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_TAX_CHECKS_ONLY then
           Checks_CO_TAX_DEP
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ALL_BUT_BILLING then
      begin
          Checks_PR;
          Checks_MISC;
          Checks_CO_TAX_DEP;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PAYROLL_CHECKS_ONLY then
         Checks_PR
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_REGULAR_CHECKS_ONLY then
         Checks_Regular
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_COBRA_CHECKS_ONLY then
         Checks_Cobra
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_NEW_CHECK then
         Checks_Void
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE then
        Count_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_ALL_EMPLOYEES then
      begin
          Count_EE;
          Count_All_Termed_EE;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_PER_MONTH then
        Count_EE_Prev_Month(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_PAID_PER_MONTH then
        Count_EE_Paid_Per_Month(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_1095Bs then
         Count_1095(True, DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime,EE_ACA_TYPE_1095B)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_1095Cs then
         Count_1095(True, DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime,EE_ACA_TYPE_1095C)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ACA_ACTIVE then
         Count_ACA_Active(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger)
       else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ACA_YEAR_TO_DATE then
         Count_ACA_YearToDate(DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ANALYTICS then
         Count_CO_Analytics
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_OVERRIDE_DASHBOARD then
         Count_CO_Per_Override_Analytics('DASHBOARD_DEFAULT_COUNT', 'DASHBOARD_COUNT')
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_OVERRIDE_USER then
         Count_CO_Per_Override_Analytics('USERS_DEFAULT_COUNT', 'USERS_COUNT')
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_OVERRIDE_LOOKBACKYEAR then
         Count_CO_Per_Override_Analytics('LOOKBACK_YEARS_DEFAULT', 'LOOKBACK_YEARS')
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_ACTIVE_EE_ENABLED_FOR_ANALYTICS then
         Count_EE_Enabled_For_Analytics
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_SELFSERVE_EMPLOYEES then
         Count_SelfServe_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_PAID then
         Checks_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_EMPLOYEE_NEW_HIRE_REPORT then
         Count_NewHire
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_EMPLOYEE_CHANGES then
         Count_EE_Change
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_NEW_HIRED_EMPLOYEES then
         Count_New_Hired_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_TERMED_EMPLOYEES then
         Count_Termed_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ALL_TERMED_EMPLOYEES then
         Count_All_Termed_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_DIRECT_DEPOSIT then
         Count_CheckLines_with_DD
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_STATE then
         Count_CO_States
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_LOCAL then
         Count_CO_Locals
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_TAX_AGENCY then
        Count_CO_Tax_Agencies
      else
      if AnsiIndexText(DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString, [SB_SERVICES_BASED_ON_FLAT_AMOUNT,
          SB_SERVICES_BASED_ON_TRUST_IMPOUND,
          SB_SERVICES_BASED_ON_ED_CODE,
          SB_SERVICES_BASED_ON_ED_CODE_COUNT,
          SB_SERVICES_BASED_ON_ED_GROUP,
          SB_SERVICES_BASED_ON_TAX_IMPOUND,
          SB_SERVICES_BASED_ON_TAX_ER_IMPOUND,
          SB_SERVICES_BASED_ON_WORKERS_COMP,
          SB_SERVICES_BASED_ON_PERC_EARNINGS,
          SB_SERVICES_BASED_ON_FLAT]) > -1 then
          DistrToDBDT(1)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_REPORT then
        Count_No_of_Reports
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_REPORT_NUMBER then
        Count_TotalNumberOfCopies
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_SPECIFIC_REPORT then
        DistrToDBDT(Integer(IsSpecificReportPrinted(ctx_DataAccess.ClientID, prNumber,
           DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SB_REPORTS_NBR').AsInteger)))
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX_CODES then
         Count_PretaxCodes
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX then
        Count_Pretax
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA then
        Count_TimeOfAccrual
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA_CODES then
        Count_TimeOfAccrualCodes
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_VMR_VOUCHER then
        Count_Vouchers
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PRINTED_CHECKS_ONLY then
        Count_PrintedChecks
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_VMR_REPORT then
        Count_VMR(VMR_PACKAGED_EMAILED_COUNTER)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_WEB_USER then
        Count_WebUsers
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_NEW_PRENOTE then
        Count_New_Prenotes
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_1099 then
        Count_1099(True, DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_W2 then
        Count_W2(True, DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_HOURLY_EMPLOYEE then
        CountActiveHourlyEE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EVOLUTION_PAYROLL_USER then
        Count_EvolutionPayrollUsers;

      Qty := 0;
      cdsDBDT.First;
      while not cdsDBDT.Eof do
      begin
        Qty := Qty + cdsDBDT.FieldByName('Qty').AsFloat;
        cdsDBDT.Next;
      end;
      if Qty = 0 then
        Exit;

      if (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PERC_EARNINGS) and
         (DM_COMPANY.CO_SERVICES.OVERRIDE_FLAT_FEE.AsFloat <> 0) then
      begin
        Clear_cdsDBDTQty;

        with TExecDSWrapper.Create('DistrEarningsForACH') do
        begin
          SetParam('PrNbr', prNumber);
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.Open;
        end;
        DBDT_Tables.Add('pr_check_lines');
        PayrollNbr := prNumber;
        aRound := DM_COMPANY.CO_SERVICES.OVERRIDE_FLAT_FEE.AsFloat / 100;
        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
          if INVOICE_DM.sLevel = 'CO_NBR' then
            AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat * aRound, 2))
          else
            AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat * aRound, 2));
          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;

        cdsDBDT.First;
        while not cdsDBDT.Eof do
        begin
          cdsDBDT.Edit;
          cdsDBDT.FieldByName('Amount').AsFloat := cdsDBDT.FieldByName('Qty').AsFloat;
          cdsDBDT.FieldByName('Tax').AsFloat := 0;
          cdsDBDT.FieldByName('Discount').AsFloat := 0;
          cdsDBDT.Post;
          cdsDBDT.Next;
        end;
      end
      else if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_WORKERS_COMP then
      begin
        Clear_cdsDBDTQty;
        dds := TevClientDataSet.Create(nil);
        try
          if DM_COMPANY.CO_SERVICES.CO_WORKERS_COMP_NBR.IsNull then
            ctx_PayrollCalculation.CalcWorkersComp(prNumber, INVOICE_DM.sLevel, dds)
          else
            ctx_PayrollCalculation.CalcWorkersComp(prNumber, INVOICE_DM.sLevel, dds, DM_COMPANY.CO_SERVICES.CO_WORKERS_COMP_NBR.Value);
          if DM_COMPANY.CO_SERVICES.OVERRIDE_FLAT_FEE.AsFloat <> 0 then
            aRound := 1 + DM_COMPANY.CO_SERVICES.OVERRIDE_FLAT_FEE.AsFloat / 100
          else
            aRound := 1;
          dds.First;
          while not dds.Eof do
          begin
            AddToDBDT(dds.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(dds.FieldByName('Amount').AsCurrency * aRound, 2));
            dds.Next;
          end
        finally
          dds.Free
        end;
        Apply_DiscontTo_cdsDBDT;
      end
      else if (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_TAX_IMPOUND) or
              (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_TAX_ER_IMPOUND) then
      begin
        Clear_cdsDBDTQty;
        try
          if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_TAX_ER_IMPOUND then
          begin
            ctx_PayrollCalculation.GetDistrTaxes(prNumber, cdsTaxDistr, 2);
            if DM_SERVICE_BUREAU.SB_SERVICES.TAX_TYPE.Value = TAX_LIABILITY_TYPE_EE_OASDI then
              cdsTaxDistr.Filter := 'fname=''ER_OASDI_TAX'''
            else if DM_SERVICE_BUREAU.SB_SERVICES.TAX_TYPE.Value = TAX_LIABILITY_TYPE_ER_MEDICARE then
              cdsTaxDistr.Filter := 'fname=''ER_MEDICARE_TAX'''
            else if DM_SERVICE_BUREAU.SB_SERVICES.TAX_TYPE.Value = TAX_LIABILITY_TYPE_ER_FUI then
              cdsTaxDistr.Filter := 'fname=''ER_FUI_TAX'''
            else if DM_SERVICE_BUREAU.SB_SERVICES.TAX_TYPE.Value = TAX_LIABILITY_TYPE_ER_SDI then
              cdsTaxDistr.Filter := 'fname=''ER_SDI_TAX'''
            else if DM_SERVICE_BUREAU.SB_SERVICES.TAX_TYPE.Value = TAX_LIABILITY_TYPE_ER_SUI then
              cdsTaxDistr.Filter := 'fname=''SUI_TAX''';
            cdsTaxDistr.Filtered := DM_SERVICE_BUREAU.SB_SERVICES.TAX_TYPE.Value <> GROUP_BOX_NOT_APPLICATIVE;
          end
          else
            ctx_PayrollCalculation.GetDistrTaxes(prNumber, cdsTaxDistr);
          DBDT_Tables.Add('pr_check_lines');
          DBDT_Tables.Add('EE');
          while not cdsTaxDistr.Eof do
          begin
            if INVOICE_DM.sLevel = 'CO_NBR' then
              AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(cdsTaxDistr.FieldByName('tax').AsFloat, 2))
            else
              AddToDBDT(cdsTaxDistr.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(cdsTaxDistr.FieldByName('tax').AsFloat, 2));
            cdsTaxDistr.Next;
          end;
          cdsTaxDistr.Close;
        finally
          cdsTaxDistr.Filter := '';
          cdsTaxDistr.Filtered := False;
        end;
        Apply_DiscontTo_cdsDBDT;
      end
      else if (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ED_CODE_COUNT)then
      begin
        Clear_cdsDBDTQty;
        sECodes := '';
        sDCodes := '';

        DM_COMPANY.CO_E_D_CODES.Activate;
        Assert(DM_COMPANY.CO_E_D_CODES.Locate('CO_E_D_CODES_NBR', DM_COMPANY.CO_SERVICES['CO_E_D_CODES_NBR'], []), 'E/D Code for service ' + DM_COMPANY.CO_SERVICES['NAME'] + ' is not attached correctly');
        sECodes := DM_COMPANY.CO_E_D_CODES.CL_E_DS_NBR.AsString;

        with TExecDSWrapper.Create('DistrEDCOUNTForACH') do
        begin
          SetParam('PrNbr', prNumber);
          SetMacro('CodeNbr', sECodes);
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.Open;
        end;
        DBDT_Tables.Add('pr_check_lines');
        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
          if INVOICE_DM.sLevel = 'CO_NBR' then
            AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, 1)
          else
            AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, 1);
          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;
        Apply_DiscontTo_cdsDBDT;
        CalcAmountIsNeeded := true;
        CalcAmountType := INVOICE_DM.sLevel = 'CO_NBR';
      end
      else if (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ED_CODE) or
              (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ED_GROUP) then
      begin
        Clear_cdsDBDTQty;
        sECodes := '';
        sDCodes := '';
        if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ED_CODE then
        begin
          DM_COMPANY.CO_E_D_CODES.Activate;
          Assert(DM_COMPANY.CO_E_D_CODES.Locate(
              'CO_E_D_CODES_NBR',
              DM_COMPANY.CO_SERVICES['CO_E_D_CODES_NBR'], []), 'E/D Code for service ' + DM_COMPANY.CO_SERVICES['NAME'] + ' is not attached correctly');
          if Copy(DM_COMPANY.CO_E_D_CODES['E_D_Code_Type'], 1, 1) = 'E' then
            sECodes := DM_COMPANY.CO_E_D_CODES.CL_E_DS_NBR.AsString
          else
            sDCodes := DM_COMPANY.CO_E_D_CODES.CL_E_DS_NBR.AsString;
        end
        else
        begin
          DM_COMPANY.CL_E_DS.DataRequired();
          DM_COMPANY.CL_E_D_GROUP_CODES.DataRequired('CL_E_D_GROUPS_NBR=' + IntToStr(DM_COMPANY.CO_SERVICES.CL_E_D_GROUPS_NBR.Value));
          DM_COMPANY.CL_E_D_GROUP_CODES.First;
          while not DM_COMPANY.CL_E_D_GROUP_CODES.Eof do
          begin
            Assert(DM_CLIENT.CL_E_DS.ShadowDataSet.FindKey([DM_COMPANY.CL_E_D_GROUP_CODES.CL_E_DS_NBR.Value]));
            if LeftStr(DM_CLIENT.CL_E_DS.ShadowDataSet.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString, 1) = 'E' then
              sECodes := sECodes + DM_COMPANY.CL_E_D_GROUP_CODES.CL_E_DS_NBR.AsString + ','
            else
              sDCodes := sDCodes + DM_COMPANY.CL_E_D_GROUP_CODES.CL_E_DS_NBR.AsString + ',';
            DM_COMPANY.CL_E_D_GROUP_CODES.Next;
          end;
          Delete(sECodes, Length(sECodes), 1);
          Delete(sDCodes, Length(sDCodes), 1);
        end;

        if sECodes <> '' then
        begin
          with TExecDSWrapper.Create('DistrEDEForACH') do
          begin
            SetParam('PrNbr', prNumber);
            SetMacro('CodeNbr', sECodes);
            ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            ctx_DataAccess.CUSTOM_VIEW.Close;
            ctx_DataAccess.CUSTOM_VIEW.Open;
          end;
        end;
        if sDCodes <> '' then
        begin
          with TExecDSWrapper.Create('DistrEDDForACH') do
          begin
            SetParam('PrNbr', prNumber);
            SetMacro('PrNbr', IntToStr(prNumber));
            SetParam('dt', SysTime);
            SetMacro('CodeNbr', sDCodes);
            ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            ctx_DataAccess.CUSTOM_VIEW.Close;
            ctx_DataAccess.CUSTOM_VIEW.Open;
          end;
        end;
        //---------

        aExact := 0;
        aRound := 0;
        tMaxAmount := 0;
        tMinAmount := 0;
        bMinAmount := false;
        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
          pAmount := ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat;
          if not DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MINIMUM_AMOUNT').IsNull then
          begin
             tMinAmount := tMinAmount + pAmount;
             bMinAmount := true;
          end;

          if not DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MAXIMUM_AMOUNT').IsNull then
          begin
            if (tMaxAmount + pAmount) > DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MAXIMUM_AMOUNT').AsCurrency then
               pAmount := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MAXIMUM_AMOUNT').AsCurrency - tMaxAmount;

            if tMaxAmount >= DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MAXIMUM_AMOUNT').AsCurrency then
               pAmount := 0;
            tMaxAmount := tMaxAmount + pAmount;
          end;

          aExact := aExact + pAmount;
          aRound := aRound + RoundAll(pAmount, 2);
          if pAmount <> 0 then
          begin
              if INVOICE_DM.sLevel = 'CO_NBR' then
                  AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(pAmount, 2))
              else
                  AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(pAmount, 2));
          end;

          if (Abs(RoundAll(aExact, 2) - aRound) >= 0.005) then
          begin
              if INVOICE_DM.sLevel = 'CO_NBR' then
                AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(aExact, 2) -aRound )
              else
                AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(aExact, 2)- aRound );
              aExact := 0;
              aRound := 0;   //aRound +aRound - RoundAll(aExact, 2);
          end;
          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;

        if bMinAmount and
           (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MINIMUM_AMOUNT').AsCurrency > tMinAmount) then
        begin
          tMinAmount := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MINIMUM_AMOUNT').AsCurrency - tMinAmount;
          if INVOICE_DM.sLevel = 'CO_NBR' then
              AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(tMinAmount, 2))
          else
          begin
              ctx_DataAccess.CUSTOM_VIEW.first;
              AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(tMinAmount, 2));
          end
        end;

        Apply_DiscontTo_cdsDBDT;
      end
      else if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_TRUST_IMPOUND then
      begin
        Clear_cdsDBDTQty;
        //mb_GlobalFlagsManager.TryLock('SB_SERVICES_BASED_ON_TRUST_IMPOUND');
        bBlockNegTrust := DM_SERVICE_BUREAU.SB.IMPOUND_TRUST_MONIES_AS_RECEIV.Value = GROUP_BOX_NO;

        if DM_COMPANY.CO.FieldByName('TRUST_SERVICE').AsString[1]  = TRUST_SERVICE_ALL then
        begin
           with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
           begin
             SetMacro('Columns', '*');
             SetMacro('TABLENAME', 'PR_CHECK');
             SetMacro('CONDITION', 'CHECK_TYPE=''' + CHECK_TYPE2_MANUAL + '''');
             ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
           end;
           ctx_DataAccess.CUSTOM_VIEW.Close;

           DM_PAYROLL.PR_CHECK.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'PR_NBR=' + IntToStr(prNumber));
           ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW, DM_PAYROLL.PR_CHECK]);

           sCheckFilter := '';

           with DM_PAYROLL.PR_CHECK do
           begin
             First;
             while not Eof do
             begin
//            if FieldByName('NET_WAGES').AsCurrency <> 0 then
               begin
                 if FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR then
                   sCheckFilter := sCheckFilter + FieldByName('PR_CHECK_NBR').AsString + ' ';

                 if (FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_VOID) and not bBlockNegTrust then
                 begin
                    // See if it was originally a manual check
                   if ExtractFromFiller(FieldByName('FILLER').AsString, 1, 8) <> '' then
                   begin
                     if ctx_DataAccess.CUSTOM_VIEW.Lookup('PR_CHECK_NBR', StrToInt(ExtractFromFiller(FieldByName('FILLER').AsString, 1, 8)), 'CHECK_TYPE') <> CHECK_TYPE2_MANUAL then
                       sCheckFilter := sCheckFilter + FieldByName('PR_CHECK_NBR').AsString + ' '
                   end
                   else
                   begin
                     if ExtractFromFiller(DM_PAYROLL.PR.FieldByName('FILLER').AsString, 1, 8) <> '' then
                     begin
                       if ctx_DataAccess.CUSTOM_VIEW.Lookup('PR_NBR;EE_NBR;NET_WAGES', VarArrayOf([StrToInt(ExtractFromFiller(DM_PAYROLL.PR.FieldByName('FILLER').AsString, 1, 8)), FieldByName('EE_NBR').Value, FieldByName('NET_WAGES').Value * (-1)]), 'CHECK_TYPE') <> CHECK_TYPE2_MANUAL then
                         sCheckFilter := sCheckFilter + FieldByName('PR_CHECK_NBR').AsString + ' '
                     end
                     else
                       sCheckFilter := sCheckFilter + FieldByName('PR_CHECK_NBR').AsString + ' '
                   end;
                 end;
               end;
               Next;
             end;
           end;

           with TExecDSWrapper.Create('DistrEarningsForACH') do
           begin
             SetParam('PrNbr', prNumber);
             ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
             ctx_DataAccess.CUSTOM_VIEW.Close;
             ctx_DataAccess.CUSTOM_VIEW.Open;
           end;
           DBDT_Tables.Add('pr_check_lines');
           while not ctx_DataAccess.CUSTOM_VIEW.Eof do
           begin
             if Pos(' ' + ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_CHECK_NBR').AsString + ' ', ' ' + sCheckFilter + ' ') > 0 then
               if INVOICE_DM.sLevel = 'CO_NBR' then
                 AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat, 2))
               else
                 AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat, 2));
              ctx_DataAccess.CUSTOM_VIEW.Next;
           end;

           with TExecDSWrapper.Create('DistrDeductionsForACH') do
           begin
             SetParam('PrNbr', prNumber);
             SetMacro('PrNbr', IntToStr(prNumber));
             SetParam('dt', SysTime);
             ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
             ctx_DataAccess.CUSTOM_VIEW.Close;
             ctx_DataAccess.CUSTOM_VIEW.Open;
           end;

           aExact := 0;
           aRound := 0;
           PrCheckNbr := ctx_DataAccess.CUSTOM_VIEW.FieldByName('pr_check_nbr').AsInteger;
           while not ctx_DataAccess.CUSTOM_VIEW.Eof do
           begin
             if PrCheckNbr <> ctx_DataAccess.CUSTOM_VIEW.FieldByName('pr_check_nbr').AsInteger then
             begin
               ctx_DataAccess.CUSTOM_VIEW.Prior;
               if (Pos(' ' + IntToStr(PrCheckNbr) + ' ', ' ' + sCheckFilter + ' ') > 0) and
                  (Abs(RoundAll(aExact, 2) - aRound) >= 0.005) then
                 if INVOICE_DM.sLevel = 'CO_NBR' then
                   AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, aRound - RoundAll(aExact, 2))
                 else
                   AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, aRound - RoundAll(aExact, 2));
               aExact := 0;
               aRound := 0;
               ctx_DataAccess.CUSTOM_VIEW.Next;
               PrCheckNbr := ctx_DataAccess.CUSTOM_VIEW.FieldByName('pr_check_nbr').AsInteger;
             end;
             if Pos(' ' + IntToStr(PrCheckNbr) + ' ', ' ' + sCheckFilter + ' ') > 0 then
               if INVOICE_DM.sLevel = 'CO_NBR' then
                 AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, -RoundAll(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsFloat, 2))
               else
                 AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, -RoundAll(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsFloat, 2));
             aExact := aExact + ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsFloat;
             aRound := aRound + RoundAll(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsFloat, 2);
             ctx_DataAccess.CUSTOM_VIEW.Next;
           end;
           if (Pos(' ' + IntToStr(PrCheckNbr) + ' ', ' ' + sCheckFilter + ' ') > 0) and
              (RoundAll(aExact, 2) - aRound <> 0) then
             if INVOICE_DM.sLevel = 'CO_NBR' then
               AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, aRound - RoundAll(aExact, 2))
             else
               AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, aRound - RoundAll(aExact, 2));

           ctx_PayrollCalculation.GetDistrTaxes(prNumber, cdsTaxDistr, 1);

           while not cdsTaxDistr.Eof do
           begin
             if Pos(' ' + cdsTaxDistr.FieldByName('PR_CHECK_NBR').AsString + ' ', ' ' + sCheckFilter + ' ') > 0 then
               if INVOICE_DM.sLevel = 'CO_NBR' then
                 AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, -RoundAll(cdsTaxDistr.FieldByName('tax').AsFloat, 2))
               else
                 AddToDBDT(cdsTaxDistr.FieldByName(INVOICE_DM.sLevel).AsInteger, -RoundAll(cdsTaxDistr.FieldByName('tax').AsFloat, 2));
             cdsTaxDistr.Next;
           end;
           cdsTaxDistr.Close;
        end;

        if DM_COMPANY.CO.FieldByName('TRUST_SERVICE').AsString[1] in [ TRUST_SERVICE_ALL,TRUST_SERVICE_AGENCY] then
        begin
           with TExecDSWrapper.Create('MiscChecksForACH') do
           begin
             SetParam('PrNbr', prNumber);
             ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
             ctx_DataAccess.CUSTOM_VIEW.Close;
             ctx_DataAccess.CUSTOM_VIEW.Open;
           end;
           DBDT_Tables.Add('pr_check_lines');
           PayrollNbr := prNumber;
           while not ctx_DataAccess.CUSTOM_VIEW.Eof do
           begin
             if (ctx_DataAccess.CUSTOM_VIEW.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString <> MISC_CHECK_TYPE_AGENCY_VOID) or not bBlockNegTrust then
               if INVOICE_DM.sLevel = 'CO_NBR' then
                  AddToDBDT(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, RoundAll(ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat, 2))
               else
                  AddToDBDT(ctx_DataAccess.CUSTOM_VIEW.FieldByName(INVOICE_DM.sLevel).AsInteger, RoundAll(ctx_DataAccess.CUSTOM_VIEW.FieldByName('amount').AsFloat, 2));
             ctx_DataAccess.CUSTOM_VIEW.Next;
           end;
        end;
        Apply_DiscontTo_cdsDBDT;
        //mb_GlobalFlagsManager.TryUnlock('SB_SERVICES_BASED_ON_TRUST_IMPOUND');
      end
      else if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_SELFSERVE_EMPLOYEES then
      begin
        Apply_DiscontTo_cdsDBDT;
        CalcAmountIsNeeded := true;
        CalcAmountType := INVOICE_DM.sLevel = 'CO_NBR';

      end
      else if DM_COMPANY.CO['PRORATE_FLAT_FEE_FOR_DBDT'] = GROUP_BOX_YES then
      begin
//        CalculateAmount(True)
        CalcAmountIsNeeded := true;
        CalcAmountType := true;
      end
      else
      begin
        CalcInvoiceDiscontNeeded := false;
        CalcAmountIsNeeded := false;
        cdsDBDT.First;
        while not cdsDBDT.Eof do
        begin
          Nbr := cdsDBDT[sLevel];
          cdsDBDT.Filter := sLevel + '=' + cdsDBDT.FieldByName(sLevel).AsString;
          cdsDBDT.Filtered := True;
          Apply_InvoiceDiscontTo_cdsDBDT;
          CalculateAmount(True);
          cdsDBDT.Filter := '';
          cdsDBDT.Filtered := False;
          cdsDBDT.Locate(sLevel, Nbr, []);
          cdsDBDT.Next;
        end;
      end;
      if CalcInvoiceDiscontNeeded  then
         Apply_InvoiceDiscontTo_cdsDBDT;
      if CalcAmountIsNeeded then
         CalculateAmount(CalcAmountType);


      cdsDBDT.First;
      while not cdsDBDT.Eof do
      begin
        if cdsDBDT.FieldByName('Qty').AsFloat <> 0 then
        begin
          Assert(CO_INVOICE_MASTER.Locate(sLevel, cdsDBDT[sLevel], []));
          with CO_INVOICE_DETAIL do
          begin
            Append;
            FieldByName('CL_NBR').AsString := CO_INVOICE_MASTER.FieldByName('CL_NBR').AsString;
            FieldByName('CO_BILLING_HISTORY_NBR').AsString := CO_INVOICE_MASTER.FieldByName('CO_BILLING_HISTORY_NBR').AsString;
            FieldByName('CO_BILLING_HISTORY_DETAIL_NBR').AsInteger := HistoryDetailNumber;
            FieldByName('CO_SERVICES_NBR').AsString := DM_COMPANY.CO_SERVICES.FieldByName('CO_SERVICES_NBR').AsString;

            Amount := RoundTwo(cdsDBDT.FieldByName('Amount').AsFloat);
            Tax := RoundTwo(cdsDBDT.FieldByName('Tax').AsFloat);
            Discount := RoundTwo(cdsDBDT.FieldByName('Discount').AsFloat);

            FieldByName('QUANTITY').AsFloat := cdsDBDT.FieldByName('Qty').AsFloat;
            FieldByName('AMOUNT').AsCurrency := Amount - Discount;
            FieldByName('DISCOUNT').AsCurrency := Discount;
            FieldByName('TAX').AsCurrency := Tax;
            FieldByName('TOTAL').AsCurrency := Amount - Discount + Tax;

            CO_INVOICE_DETAIL.Post;

            result := True;
          end;
        end;
        cdsDBDT.Next;
      end;
    end;
  end;

  procedure CalculateInvoiceMasterTotals;
  begin
    with INVOICE_DM.CO_INVOICE_MASTER do
    begin
      FieldByName('INVOICE_TAX').Value := ColumnSum(INVOICE_DM.CO_INVOICE_DETAIL, 'TAX', INVOICE_DM.CO_INVOICE_MASTER['CO_BILLING_HISTORY_NBR']);
      FieldByName('INVOICE_TOTAL').AsCurrency := ColumnSum(INVOICE_DM.CO_INVOICE_DETAIL, 'TOTAL', INVOICE_DM.CO_INVOICE_MASTER['CO_BILLING_HISTORY_NBR']);
      FieldByName('INVOICE_DISCOUNT').AsCurrency := ColumnSum(INVOICE_DM.CO_INVOICE_DETAIL, 'Discount', INVOICE_DM.CO_INVOICE_MASTER['CO_BILLING_HISTORY_NBR']);
      FieldByName('INVOICE_AMOUNT').AsCurrency := ColumnSum(INVOICE_DM.CO_INVOICE_DETAIL, 'AMOUNT', INVOICE_DM.CO_INVOICE_MASTER['CO_BILLING_HISTORY_NBR']);
    end;
  end;

  procedure UpdateDetailDescription;
  var
    Qty: Currency;
    cds: TevClientDataSet;
  begin
    cds := TevClientDataSet.Create(nil);
    try
      cds.CloneCursor(INVOICE_DM.CO_INVOICE_DETAIL, True);
      cds.Filtered := True;
      with INVOICE_DM.CO_INVOICE_DETAIL do
      begin
        First;

        while not EOF do
        begin

          if not FieldByName('CO_SERVICES_NBR').IsNull then
          begin
            if not DM_COMPANY.CO_SERVICES.Locate('CO_SERVICES_NBR', FieldByName('CO_SERVICES_NBR').AsInteger, []) then
              raise EInconsistentData.CreateHelp('Missing CO_SERVICES', IDH_InconsistentData);
            LocateSB_Services;
          end
          else
            DM_SERVICE_BUREAU.SB_SERVICES.Locate('SB_SERVICES_NBR', FieldByName('SB_SERVICES_NBR').Value, []);

          Qty := 0;

          if not FieldByName('CO_SERVICES_NBR').IsNull then
            cds.Filter := 'CO_SERVICES_NBR = ' + FieldByName('CO_SERVICES_NBR').AsString
          else
            cds.Filter := 'SB_SERVICES_NBR = ' + FieldByName('SB_SERVICES_NBR').AsString;
          while not cds.Eof do
          begin
            Qty := Qty + cds.FieldByName('QUANTITY').AsFloat;
            cds.Next;
          end;

          Edit;

          if not FieldByName('CO_SERVICES_NBR').IsNull then
          begin
            FieldByName('NAME').AsString := DM_COMPANY.CO_SERVICES.FieldByName('NAME').AsString;
            if not DM_COMPANY.CO_SERVICES.FieldByName('CO_E_D_CODES_NBR').IsNull then
            begin
              DM_COMPANY.CO_E_D_CODES.Activate;
              if DM_COMPANY.CO_E_D_CODES.Locate('CO_E_D_CODES_NBR', DM_COMPANY.CO_SERVICES['CO_E_D_CODES_NBR'], []) then
                FieldByName('NAME').AsString := FieldByName('NAME').AsString + ' ' + DM_COMPANY.CO_E_D_CODES['ED_Lookup'];
            end;
          end
          else
            FieldByName('NAME').AsString := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SERVICE_NAME').AsString;

          if Qty >= 1 then
          begin
            Invoice_DM.LocateSB_SERVICES_CALCULATIONS(Round(Qty));

            if DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('PER_ITEM_RATE').AsCurrency > 0 then
              FieldByName('NAME').AsString := FieldByName('NAME').AsString + ' (' + FormatFloat('#,##0.##', FieldByName('QUANTITY').AsFloat) + ')';
            DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filtered := False;
          end;

          Post;

          Next;
        end;
      end;
    finally
      cds.Free;
    end;
  end;

  procedure CalculateNewInvoice;
  begin
    // Create new invoice number
    if not Assigned(INVOICE_DM.DBDT_Tables) then
       INVOICE_DM.DBDT_Tables := TisStringList.CreateUnique;
    with INVOICE_DM do
    begin
      sLink := Format('%8.8d', [RandomInteger(0, 99999999)]);
      cdsDBDT.First;
      while not cdsDBDT.Eof do
      begin
        InvoiceNumber := GetNextInvoiceNumber;
        PopulateInvoiceMaster(InvoiceNumber, cdsDBDT[sLevel]);
        CO_INVOICE_MASTER.Post;
        cdsDBDT.Next;
      end;
    end;

   // Calculate the invoice details
    with DM_COMPANY.CO_SERVICES do
    begin
      First;
      while not EOF do
      begin
        INVOICE_DM.DBDT_Tables.Clear;
        AddServicesToDetail(ctx_DBAccess.GetGeneratorValue('G_CO_BILLING_HISTORY_DETAIL', dbtClient, 1));
        Next;
      end;
    end;
    UpdateDetailDescription;

    // Populate the master records

    INVOICE_DM.CO_INVOICE_MASTER.First;
    with INVOICE_DM.CO_INVOICE_MASTER do
      while not Eof do
      begin
        Edit;

//        FieldByName('INVOICE_NUMBER').AsInteger := InvoiceNumber;

        CalculateInvoiceMasterTotals;

        // Transfer records from CO_INVOICE_MASTER to CO_BILLING_HISTORY

        DM_COMPANY.CO_BILLING_HISTORY.Append;

        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_BILLING_HISTORY_NBR').Value :=
          FieldByName('CO_BILLING_HISTORY_NBR').Value;

        DM_COMPANY.CO_BILLING_HISTORY.UpdateBlobData('NOTES', DM_COMPANY.CO.GetBlobData('INVOICE_NOTES'));

        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_NUMBER').Value :=
          FieldByName('INVOICE_NUMBER').Value;

        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_DATE').Value :=
          FieldByName('INVOICE_DATE').Value;

        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('PR_NBR').Value :=
          FieldByName('PR_NBR').Value;

        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('EXPORTED_TO_A_R').AsString := EXPORTED_TO_A_R_STATUS_NO;


        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('SALES_TAX_AMOUNT').AsCurrency :=
          FieldByName('INVOICE_TAX').AsCurrency;

        DM_COMPANY.CO_BILLING_HISTORY['CO_DIVISION_NBR'] := FieldValues['CO_DIVISION_NBR'];
        DM_COMPANY.CO_BILLING_HISTORY['CO_BRANCH_NBR'] := FieldValues['CO_BRANCH_NBR'];
        DM_COMPANY.CO_BILLING_HISTORY['CO_DEPARTMENT_NBR'] := FieldValues['CO_DEPARTMENT_NBR'];
        DM_COMPANY.CO_BILLING_HISTORY['CO_TEAM_NBR'] := FieldValues['CO_TEAM_NBR'];

        DM_COMPANY.CO_BILLING_HISTORY['FILLER'] := FieldValues['FILLER'];

        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_INTERNAL_CHECK
        else
        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_EXTERNAL_CHECK then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_EXTERNAL_CHECK
        else
        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_WIRE_TRANSFER then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_WIRE_TRANSFER
        else
        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_CASH then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CASH
        else
        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_OTHER then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_OTHER
        else
        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_ACH then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_READY_FOR_ACH
        else
        if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_NONE then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_IGNORE
        else
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CHECK;

        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
        case PaymentMethod[1] of

          BILLING_PAYMENT_METHOD_CLIENT:
            begin
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CHECK;

              ctx_DataAccess.UnlockPRSimple;
              try
                DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CHECK_SERIAL_NUMBER').AsString :=
                  CreatePRMiscCheck(prNumber,
                  'BILLING_CL_BANK_ACCOUNT_NBR',
                  FieldByName('INVOICE_TOTAL').AsCurrency);
              finally
                ctx_DataAccess.LockPRSimple;
              end;
            end;


          BILLING_PAYMENT_METHOD_SB:
            begin
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CHECK;

              ctx_DataAccess.UnlockPRSimple;
              try
                DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CHECK_SERIAL_NUMBER').AsString :=
                  CreatePRMiscCheck(prNumber,
                  'BILLING_SB_BANK_ACCOUNT_NBR',
                  FieldByName('INVOICE_TOTAL').AsCurrency);
              finally
                ctx_DataAccess.LockPRSimple;
              end;
            end;

          BILLING_PAYMENT_METHOD_DEBIT:
            begin
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_READY_FOR_ACH;
            end;

          BILLING_PAYMENT_METHOD_INVOICE:
            begin
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_IGNORE;
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('EXPORTED_TO_A_R').AsString := EXPORTED_TO_A_R_STATUS_NO;
            end;
        end;
        
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_INTERNAL_CHECK
        else
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_EXTERNAL_CHECK then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_EXTERNAL_CHECK
        else
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_WIRE_TRANSFER then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_WIRE_TRANSFER
        else
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_CASH then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CASH
        else
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_OTHER then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_OTHER
        else
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_ACH then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_READY_FOR_ACH
        else
        if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_NONE then
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_IGNORE
        else
          DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CHECK;

        // Post both INVOICE_MASTER and CO_BILLING_HISTORY

        if DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString = CO_BILLING_STATUS_READY_FOR_ACH then
          FieldByName('TERMS').AsString := 'Paid'
        else if not DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CHECK_SERIAL_NUMBER').IsNull then
          FieldByName('TERMS').AsString := 'Check #' + DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CHECK_SERIAL_NUMBER').AsString;

        DM_COMPANY.CO_BILLING_HISTORY.Post;
        Post;
        Next;
      end;

    with INVOICE_DM.CO_INVOICE_DETAIL do
    begin
      First;
      while not EOF do
      begin
        if not DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Active then
          DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Open;

        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Append;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('CO_BILLING_HISTORY_NBR').Value := FieldByName('CO_BILLING_HISTORY_NBR').Value;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('CO_SERVICES_NBR').Value := FieldByName('CO_SERVICES_NBR').Value;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('QUANTITY').Value := FieldByName('QUANTITY').Value;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('TAX').Value := FieldByName('TAX').Value;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('DISCOUNT').Value := FieldByName('DISCOUNT').Value;
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Post;
        Next;
      end;
    end;

    PrInvoiceStatus := INVOICE_PRINT_STATUS_Billing_Calculated;

    if InvoiceType = CLIENT_INVOICE_PRINT_TYPE_None then
      PrInvoiceStatus := INVOICE_PRINT_STATUS_calculated_do_not_print;
  end;

  procedure ReadOldInvoice(InvoiceNumber: integer);
  begin
    while not DM_COMPANY.CO_BILLING_HISTORY.Eof do
    begin
      DM_COMPANY.CO_BILLING_HISTORY_DETAIL.DataRequired('CO_BILLING_HISTORY_NBR = ' + DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_BILLING_HISTORY_NBR').AsString);
      with DM_COMPANY.CO_BILLING_HISTORY_DETAIL do
        while not EOF do
        begin
          if not INVOICE_DM.CO_INVOICE_DETAIL.Active then
            INVOICE_DM.CO_INVOICE_DETAIL.Open;

          INVOICE_DM.CO_INVOICE_DETAIL.Append;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('CL_NBR').Value := DM_CLIENT.CL.FieldByName('CL_NBR').AsString;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('CO_BILLING_HISTORY_NBR').Value := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_BILLING_HISTORY_NBR').Value;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('CO_SERVICES_NBR').Value := FieldByName('CO_SERVICES_NBR').Value;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('SB_SERVICES_NBR').Value := FieldByName('SB_SERVICES_NBR').Value;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('QUANTITY').Value := FieldByName('QUANTITY').Value;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('TOTAL').Value := FieldByName('AMOUNT').Value + FieldByName('TAX').AsCurrency;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('TAX').Value := FieldByName('TAX').Value;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('AMOUNT').AsCurrency := FieldByName('AMOUNT').AsCurrency;
          INVOICE_DM.CO_INVOICE_DETAIL.FieldByName('DISCOUNT').Value := FieldByName('DISCOUNT').Value;
          INVOICE_DM.CO_INVOICE_DETAIL.Post;
          Next;
        end;

      INVOICE_DM.PopulateInvoiceMaster(DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_NUMBER').AsInteger, -1);
      if (DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString = CO_BILLING_STATUS_READY_FOR_ACH) or
         (DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString = CO_BILLING_STATUS_ACH_SENT) then
        INVOICE_DM.CO_INVOICE_MASTER.FieldByName('TERMS').AsString := 'Paid'
      else if not DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CHECK_SERIAL_NUMBER').IsNull then
        INVOICE_DM.CO_INVOICE_MASTER.FieldByName('TERMS').AsString := 'Check #' + DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CHECK_SERIAL_NUMBER').AsString;

      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('CO_BILLING_HISTORY_NBR').Value := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_BILLING_HISTORY_NBR').Value;
      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('INVOICE_DATE').Value := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_DATE').Value;
      INVOICE_DM.CO_INVOICE_MASTER.UpdateBlobData('NOTES', DM_COMPANY.CO_BILLING_HISTORY.GetBlobData('NOTES'));

      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('CO_DIVISION_NBR').AsInteger := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_DIVISION_NBR').AsInteger;
      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('CO_BRANCH_NBR').AsInteger := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_BRANCH_NBR').AsInteger;
      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('CO_DEPARTMENT_NBR').AsInteger := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('CO_TEAM_NBR').AsInteger := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_TEAM_NBR').AsInteger;

      INVOICE_DM.CO_INVOICE_MASTER.FieldByName('FILLER').AsString := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('FILLER').AsString;

      CalculateInvoiceMasterTotals;
      INVOICE_DM.CO_INVOICE_MASTER.Post;
      DM_COMPANY.CO_BILLING_HISTORY.Next;
    end;

    UpdateDetailDescription;
  end;

  procedure ChangePayrollPrintInvoiceStatus(PrInvoiceStatus: string);
  begin
    if not (DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString = PrInvoiceStatus) then
    begin
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString := PrInvoiceStatus;
      DM_PAYROLL.PR.Post;
    end;
  end;

var
  AllOK: Boolean;
  edw: TExecDSWrapper;
  i: Integer;
begin

{ Recognized parameter values
                   ForPayroll   ForPreview InvoiceNumber
   NewInvoice         Y          N          -1
   FollowupPrinting   Y          N           nonzero
   Preview            N          Y           nonzero
   ForAR              N          N           nonzero
};

  INVOICE_DM := TINVOICE_DM.Create(nil);
  ctx_StartWait;
  try
    ctx_DataAccess.OpenClient(ctx_DataAccess.ClientID);
    INVOICE_DM.PayrollNbr :=prNumber;
    INVOICE_DM.CheckDbdtSetupForPayroll;
    if not DM_PAYROLL.PR.Active then
      DM_PAYROLL.PR.DataRequired('PR_NBR = ' + IntToStr(prNumber));
    if not DM_PAYROLL.PR_SERVICES.Active then
      DM_PAYROLL.PR_SERVICES.DataRequired('');

    if (prNumber <> 0) and not DM_PAYROLL.PR.Locate('PR_NBR', prNumber, []) then
      prNumber := 0;
    Assert(not ForPayroll or (prNumber <> 0), 'Payroll table needs to have been read by calling routine');

    if prNumber <> 0 then
      DM_COMPANY.CO_BILLING_HISTORY.DataRequired('(CO_NBR = ' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString +
        ' AND PR_NBR = ' + IntToStr(prNumber) + ')')
    else
    begin
      DM_COMPANY.CO_BILLING_HISTORY.DataRequired('INVOICE_NUMBER = ' + IntToStr(InvoiceNumber));
      if not DM_PAYROLL.PR.Locate('PR_NBR', DM_COMPANY.CO_BILLING_HISTORY['PR_NBR'], []) then
        DM_PAYROLL.PR.DataRequired('PR_NBR = ' + IntToStr(DM_COMPANY.CO_BILLING_HISTORY.FieldByName('PR_NBR').AsInteger));
    end;

    if ForPayroll or (prNumber <> 0) then
      DM_COMPANY.CO.DataRequired('CO_NBR = ' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString);

    if (DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_NBR').AsInteger > 0) and
       (not DM_COMPANY.CO.Active or
        not DM_COMPANY.CO.Locate('CO_NBR', DM_COMPANY.CO_BILLING_HISTORY['CO_NBR'], [])) then
      DM_COMPANY.CO.DataRequired('CO_NBR = ' + DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_NBR').AsString);

    //Add (ForPayroll) and at the begining of the if statement below
    if (ForPayroll) and (prNumber <> 0) and (DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').AsString = 'Y') then
    begin
      if ForPayroll and (InvoiceNumber = -1) then
      begin
        ctx_DataAccess.UnlockPR;
        AllOK := False;
        try
          ChangePayrollPrintInvoiceStatus(INVOICE_PRINT_STATUS_Excluded);
          ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
          AllOK := True;
        finally
          ctx_DataAccess.LockPR(AllOK);
        end;
      end;
      Exit;
    end;

    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    DM_SERVICE_BUREAU.SB_SALES_TAX_STATES.DataRequired('ALL');
    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.DataRequired('ALL');
    DM_CLIENT.CL.DataRequired('ALL');
    DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
    DM_SERVICE_BUREAU.SB.DataRequired('ALL');
    DM_SERVICE_BUREAU.SB_SERVICES.DataRequired('ALL');
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.DataRequired('ALL');
    DM_EMPLOYEE.EE.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

    DM_COMPANY.CO_SERVICES.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    DM_COMPANY.CO_STATES.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    DM_CLIENT.CL_BILLING.DataRequired('CL_BILLING_NBR = ' +
      DM_COMPANY.CO.FieldByName('CL_BILLING_NBR').AsString);

    DM_COMPANY.CO_DIVISION.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    DM_COMPANY.CO_BRANCH.DataRequired('');
    DM_COMPANY.CO_DEPARTMENT.DataRequired('');
    DM_COMPANY.CO_TEAM.DataRequired('');

    INVOICE_DM.cdsDBDT.FieldDefs.Clear;
    INVOICE_DM.cdsDBDT.IndexName := '';

    edw := nil;
    if DM_COMPANY.CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_DIVISION then
    begin
      INVOICE_DM.cdsDBDT.FieldDefs.Assign(DM_COMPANY.CO_DIVISION.FieldDefs);
      INVOICE_DM.cdsDBDT.IndexDefs.Add('Search', 'CO_DIVISION_NBR', []);
      INVOICE_DM.sLevel := 'CO_DIVISION_NBR';
    end
    else if DM_COMPANY.CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_BRANCH then
    begin
      edw := TExecDSWrapper.Create('BranchQuery');
      INVOICE_DM.cdsDBDT.IndexDefs.Add('Search', 'CO_BRANCH_NBR', []);
      INVOICE_DM.sLevel := 'CO_BRANCH_NBR';
    end
    else if DM_COMPANY.CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_DEPT then
    begin
      edw := TExecDSWrapper.Create('DepartmentQuery');
      INVOICE_DM.cdsDBDT.IndexDefs.Add('Search', 'CO_DEPARTMENT_NBR', []);
      INVOICE_DM.sLevel := 'CO_DEPARTMENT_NBR';
    end
    else if DM_COMPANY.CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_TEAM then
    begin
      edw := TExecDSWrapper.Create('TeamQuery');
      INVOICE_DM.cdsDBDT.IndexDefs.Add('Search', 'CO_TEAM_NBR', []);
      INVOICE_DM.sLevel := 'CO_TEAM_NBR';
    end
    else
    begin
      INVOICE_DM.sLevel := 'CO_NBR';
      with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
      begin
        DataType := ftInteger;
        InternalCalcField := True;
        Name := 'CO_NBR';
      end;
    end;

    if Assigned(edw) then
    begin
      with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
      begin
        DataType := ftInteger;
        Name := INVOICE_DM.sLevel;
      end;
      edw.SetParam('Co_Nbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(edw.AsVariant);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.Open;
    end;

    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'QTY';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'Amount';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'Tax';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'Discount';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftBoolean;
      InternalCalcField := True;
      Name := 'Extra';
    end;

    if DM_COMPANY.CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_COMPANY then
    begin
      INVOICE_DM.cdsDBDT.CreateDataSet;
      INVOICE_DM.cdsDBDT.Append;
      INVOICE_DM.cdsDBDT['CO_NBR'] := DM_COMPANY.CO['CO_NBR'];
      INVOICE_DM.cdsDBDT.Post;
    end
    else if DM_COMPANY.CO.FieldByName('BILLING_LEVEL').AsString = CLIENT_LEVEL_DIVISION then
    begin
      INVOICE_DM.cdsDBDT.CreateFields;
      INVOICE_DM.cdsDBDT.FieldDefs.Clear;
      for i := 0 to Pred(INVOICE_DM.cdsDBDT.FieldCount) do
        INVOICE_DM.cdsDBDT.Fields[i].Required := False;
      INVOICE_DM.cdsDBDT.Data := DM_COMPANY.CO_DIVISION.Data;
      INVOICE_DM.cdsDBDT.IndexName := 'Search';
    end
    else
    begin
      INVOICE_DM.cdsDBDT.CreateFields;
      INVOICE_DM.cdsDBDT.FieldDefs.Clear;
      INVOICE_DM.cdsDBDT.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
      INVOICE_DM.cdsDBDT.IndexName := 'Search';
    end;

    if INVOICE_DM.sLevel <> 'CO_NBR' then
    begin
      with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
      begin
        SetMacro('Columns', 'distinct T1.' + INVOICE_DM.sLevel);
        SetMacro('Table1', 'EE');
        SetMacro('Table2', 'PR_CHECK');
        SetMacro('NbrField', 'PR');
        SetMacro('JoinField', 'EE');
        SetParam('RecordNbr', prNumber);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;

      ctx_DataAccess.CUSTOM_VIEW.IndexFieldNames := INVOICE_DM.sLevel;
      try
        INVOICE_DM.cdsDBDT.First;
        while not INVOICE_DM.cdsDBDT.Eof do
          if ctx_DataAccess.CUSTOM_VIEW.FindKey([INVOICE_DM.cdsDBDT[INVOICE_DM.sLevel]]) then
            INVOICE_DM.cdsDBDT.Next
          else
            INVOICE_DM.cdsDBDT.Delete;
      finally
        ctx_DataAccess.CUSTOM_VIEW.IndexFieldNames := '';
      end;
    end;

    DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    if not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Active then
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired(AlwaysFalseCond);

    with INVOICE_DM do
    begin
      IsScheduled := False;
      if (prNumber <> 0) or (DM_COMPANY.CO_BILLING_HISTORY.FieldByName('PR_NBR').AsInteger <> 0) then
        PRCheckDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime;
      if prNumber <> 0 then
      begin
        IsScheduled := DM_PAYROLL.PR.FieldByName('SCHEDULED').AsString = 'Y';
        PrInvoiceStatus := DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString;
        LAST_SCHEDULED_PRCheckDate := PRCheckDate - 14;
      end;
    end;

    if DM_PAYROLL.PR.BILLING_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
      PaymentMethod := BILLING_PAYMENT_METHOD_CLIENT
    else
      PaymentMethod := DM_CLIENT.CL_BILLING.FieldByName('PAYMENT_METHOD').AsString;

    InvoiceType := DM_CLIENT.CL_BILLING.FieldByName('INVOICE_SEPARATELY').AsString;

    INVOICE_DM.CO_INVOICE_DETAIL.Close;
    INVOICE_DM.CO_INVOICE_MASTER.Close;

    INVOICE_DM.CO_INVOICE_DETAIL.FieldDefs.Clear;
    INVOICE_DM.CO_INVOICE_MASTER.FieldDefs.Clear;

    INVOICE_DM.CO_INVOICE_DETAIL.CreateDataSet;
    INVOICE_DM.CO_INVOICE_MASTER.CreateDataSet;

    if DM_COMPANY.CO_BILLING_HISTORY.RecordCount > 0 then
    begin
      if (InvoiceNumber = -1) then
        raise ECanNotPerformOperation.CreateHelp('Invoice already exists for payroll', IDH_CanNotPerformOperation);

      if not DM_COMPANY.CO_BILLING_HISTORY.Locate('INVOICE_NUMBER', InvoiceNumber, []) then
        raise EInvalidParameters.CreateHelp('Invoice number parameter does not match the number in the billing record', IDH_InvalidParameters);

//      SaveDSStates([DM_COMPANY.CO_BILLING_HISTORY, DM_COMPANY.CO_BILLING_HISTORY_DETAIL]);
      try
//        DM_COMPANY.CO_BILLING_HISTORY.DataRequired('INVOICE_NUMBER = ' + IntToStr(InvoiceNumber));
        if Trim(Copy(DM_COMPANY.CO_BILLING_HISTORY.FieldByName('FILLER').AsString, 1, 8)) <> '' then
          DM_COMPANY.CO_BILLING_HISTORY.DataRequired('FILLER starting with ''' + Copy(DM_COMPANY.CO_BILLING_HISTORY.FieldByName('FILLER').AsString, 1, 8) + '''');

        ReadOldInvoice(InvoiceNumber);
      finally
//        LoadDSStates([DM_COMPANY.CO_BILLING_HISTORY, DM_COMPANY.CO_BILLING_HISTORY_DETAIL]);
      end;
    end
    else
    begin
      if not (InvoiceNumber = -1) then
        raise EInvalidParameters.CreateHelp('Could not find Billing history for given Invoice number', IDH_InvalidParameters);

      DM_COMPANY.CO_BILLING_HISTORY.DataRequired(AlwaysFalseCond);
      DM_COMPANY.CO_BILLING_HISTORY_DETAIL.DataRequired(AlwaysFalseCond);

      CalculateNewInvoice;
    end;

    if ForPayroll then
    begin
      ctx_DataAccess.UnlockPR;
      AllOK := False;
      try
        Sleep(1000);
        ctx_DataAccess.PostDataSets(
          [
  //          DM_SERVICE_BUREAU.SB, // Invoice number incremented
            DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS, // Check number
            DM_CLIENT.CL_BANK_ACCOUNT, // Check number
  //          DM_PAYROLL.PR, // Invoice print status
            DM_PAYROLL.PR_MISCELLANEOUS_CHECKS, // Billing check
            DM_COMPANY.CO_BILLING_HISTORY, // invoice
            DM_COMPANY.CO_BILLING_HISTORY_DETAIL // invoice detail
            ]);
        AllOK := True;
      finally
        ctx_DataAccess.LockPR(AllOK);
      end;
    end;

    if ForPayroll then
    begin
      ctx_DataAccess.UnlockPR;
      AllOK := False;
      try
        ChangePayrollPrintInvoiceStatus(PrInvoiceStatus);
        ctx_DataAccess.PostDataSets(
          [
            DM_PAYROLL.PR // Invoice print status
            ]);
        AllOK := True;
      finally
        ctx_DataAccess.LockPR(AllOK);
      end;
    end;

    if ReturnDS then
      Result := VarArrayOf([INVOICE_DM.CO_INVOICE_MASTER.Data, INVOICE_DM.CO_INVOICE_DETAIL.Data])
    else
      Result := unAssigned;

  finally
    ctx_EndWait;
    INVOICE_DM.Free;
  end;

end;

function ProcessBillingForServices(ClNbr: Integer; CoNbr: Integer; const List: String; const EffDate: TDateTime; const Notes: string; var bManualACH: Boolean): Real;
var
  InvoiceNumber: Integer;
  t: TStringList;
  i: Integer;
  INVOICE_DM: TINVOICE_DM;

  function AddServices(DS: TevClientDataSet; CoLevel: Boolean): real;
  begin
    Result := 0;
    if CoLevel and
       ((Date < Trunc(DS.FieldByName('EFFECTIVE_START_DATE').AsDateTime)) or
        (Trunc(DS.FieldByName('EFFECTIVE_END_DATE').AsDateTime) > 0) and
        (Date >= Trunc(DS.FieldByName('EFFECTIVE_END_DATE').AsDateTime))) then
      Exit;

    if CoLevel and (DS.FieldByName('FREQUENCY').AsString = SCHED_FREQ_ONE_TIME) then
      if Copy(DS.FieldByName('FILLER').AsString, 5, 1) = 'Y' then
        Exit
      else
      begin
        DS.Edit;
        DS.FieldByName('Filler').AsString := PutIntoFiller(DS.FieldByName('Filler').AsString, 'Y', 5, 1);
        DS.Post;
      end;

    INVOICE_DM.cdsDBDT.Edit;
    INVOICE_DM.cdsDBDT.FieldByName('QTY').Value := 0;
    INVOICE_DM.cdsDBDT.Post;
    with INVOICE_DM do
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE then
        Count_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_ALL_EMPLOYEES then
      begin
          Count_EE;
          Count_All_Termed_EE;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ALL_TERMED_EMPLOYEES then
          Count_All_Termed_EE
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_EMPLOYEE_NEW_HIRE_REPORT then
          Count_NewHire
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_STATE then
          Count_CO_States
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_LOCAL then
          Count_CO_Locals
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_TAX_AGENCY then
          Count_CO_Tax_Agencies
      else
      if AnsiIndexText(DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString, [SB_SERVICES_BASED_ON_FLAT_AMOUNT, SB_SERVICES_BASED_ON_FLAT]) > -1 then
      begin
          cdsDBDT.Edit;
          cdsDBDT.FieldByName('QTY').AsFloat := 1;
          cdsDBDT.Post;
      end
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX_CODES then
         Count_PretaxCodes
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_SB_PRETAX then
         Count_Pretax
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA then
        Count_TimeOfAccrual
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_EMPLOYEE_TOA_CODES then
        Count_TimeOfAccrualCodes
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_1099 then
        Count_1099(CoLevel, Date)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_W2 then
        Count_W2(CoLevel, Date)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_1095Bs then
        Count_1095(True, Date,EE_ACA_TYPE_1095B)
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_1095Cs then
        Count_1095(True, Date,EE_ACA_TYPE_1095C)
      else  
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_ANALYTICS then
        Count_CO_Analytics
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_OVERRIDE_DASHBOARD then
        Count_CO_Per_Override_Analytics('DASHBOARD_DEFAULT_COUNT', 'DASHBOARD_COUNT')
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_OVERRIDE_USER then
        Count_CO_Per_Override_Analytics('USERS_DEFAULT_COUNT', 'USERS_COUNT')
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_OVERRIDE_LOOKBACKYEAR then
        Count_CO_Per_Override_Analytics('LOOKBACK_YEARS_DEFAULT', 'LOOKBACK_YEARS')
      else
      if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('BASED_ON_TYPE').AsString = SB_SERVICES_BASED_ON_PER_ACTIVE_EE_ENABLED_FOR_ANALYTICS then
        Count_EE_Enabled_For_Analytics;

    if Round(INVOICE_DM.cdsDBDT.FieldByName('QTY').AsFloat) = 0 then
      Exit;

    INVOICE_DM.CalculateAmount(CoLevel);

    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Append;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('CO_BILLING_HISTORY_NBR').Value := DM_COMPANY.CO_BILLING_HISTORY.FieldByName('CO_BILLING_HISTORY_NBR').Value;
    if CoLevel then
      DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('CO_SERVICES_NBR').Value := DM_COMPANY.CO_SERVICES.FieldByName('CO_SERVICES_NBR').Value
    else
      DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('SB_SERVICES_NBR').Value := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SB_SERVICES_NBR').Value;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('QUANTITY').Value := INVOICE_DM.cdsDBDT.FieldByName('Qty').AsFloat;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('AMOUNT').Value := INVOICE_DM.cdsDBDT.FieldByName('Amount').AsFloat - INVOICE_DM.cdsDBDT.FieldByName('Discount').AsFloat;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('TAX').Value := INVOICE_DM.cdsDBDT.FieldByName('Tax').AsFloat;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('DISCOUNT').Value := INVOICE_DM.cdsDBDT.FieldByName('Discount').AsFloat;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Post;
    Result := INVOICE_DM.cdsDBDT.FieldByName('Amount').AsFloat - INVOICE_DM.cdsDBDT.FieldByName('Discount').AsFloat + INVOICE_DM.cdsDBDT.FieldByName('Tax').AsFloat;
  end;

begin
  INVOICE_DM := TINVOICE_DM.Create(nil);
  ctx_StartWait;
  t := TStringList.Create;
  t.Text := List;
  try
    ctx_DataAccess.OpenClient(clNbr);

    DM_COMPANY.CO_BILLING_HISTORY.Close;
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Close;

    DM_SYSTEM_STATE.SY_STATES.CheckDSCondition('ALL');
    DM_SERVICE_BUREAU.SB_SALES_TAX_STATES.CheckDSCondition('ALL');
    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE.CheckDSCondition('ALL');
    DM_COMPANY.CO_BILLING_HISTORY.CheckDSConditionAndClient(clNbr, 'CO_BILLING_HISTORY_NBR = 0');
    DM_COMPANY.CO_BILLING_HISTORY_DETAIL.CheckDSConditionAndClient(clNbr, 'CO_BILLING_HISTORY_NBR = 0');
    DM_COMPANY.CO.CheckDSConditionAndClient(clNbr, 'CO_NBR = ' + IntToStr(CoNbr));
    DM_SERVICE_BUREAU.SB.CheckDSCondition('ALL');
    DM_SERVICE_BUREAU.SB_SERVICES.CheckDSCondition('ALL');
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.CheckDSCondition('ALL');
    DM_COMPANY.CO_SERVICES.CheckDSConditionAndClient(clNbr, 'CO_NBR = ' + IntToStr(CoNbr));
    DM_EMPLOYEE.EE.CheckDSConditionAndClient(clNbr, 'CO_NBR = ' + IntToStr(CoNbr));
    DM_COMPANY.CO_STATES.CheckDSConditionAndClient(clNbr, 'CO_NBR = ' + IntToStr(CoNbr));
    DM_COMPANY.CO_MANUAL_ACH.CheckDSCondition('STATUS=''' + COMMON_REPORT_STATUS__PENDING + '''');

    ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE, DM_COMPANY.CO_BILLING_HISTORY, DM_COMPANY.CO_BILLING_HISTORY_DETAIL,
                  DM_COMPANY.CO, DM_SERVICE_BUREAU.SB, DM_SERVICE_BUREAU.SB_SERVICES, DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS,
                  DM_COMPANY.CO_SERVICES, DM_EMPLOYEE.EE, DM_COMPANY.CO_STATES, DM_SERVICE_BUREAU.SB_SALES_TAX_STATES,
                  DM_COMPANY.CO_MANUAL_ACH]);
    DM_CLIENT.CL_BILLING.DataRequired('CL_BILLING_NBR = ' + DM_COMPANY.CO.FieldByName('CL_BILLING_NBR').AsString);

    if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_ACH then
       bManualACH :=True;

    INVOICE_DM.cdsDBDT.FieldDefs.Clear;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftInteger;
      InternalCalcField := True;
      Name := 'CO_NBR';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'QTY';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'Amount';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'Tax';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      InternalCalcField := True;
      Name := 'Discount';
    end;
    with INVOICE_DM.cdsDBDT.FieldDefs.AddFieldDef do
    begin
      DataType := ftBoolean;
      InternalCalcField := True;
      Name := 'Extra';
    end;

    INVOICE_DM.cdsDBDT.CreateDataSet;
    INVOICE_DM.cdsDBDT.Append;
    INVOICE_DM.cdsDBDT['CO_NBR'] := DM_COMPANY.CO['CO_NBR'];
    INVOICE_DM.cdsDBDT.Post;
    INVOICE_DM.sLevel := 'CO_NBR';

    if not Assigned(INVOICE_DM.DBDT_Tables) then
       INVOICE_DM.DBDT_Tables := TisStringList.CreateUnique;
    InvoiceNumber := GetNextInvoiceNumber;

    DM_COMPANY.CO_BILLING_HISTORY.Insert;

    Result := 0;
    for i := 0 to Pred(t.Count) do
      if DM_SERVICE_BUREAU.SB_SERVICES.Locate('SB_SERVICES_NBR', t[i], []) then
        if DM_COMPANY.CO_SERVICES.Locate('SB_SERVICES_NBR', DM_SERVICE_BUREAU.SB_SERVICES['SB_SERVICES_NBR'], []) then
          Result := Result + AddServices(DM_COMPANY.CO_SERVICES, True)
        else
          Result := Result + AddServices(DM_SERVICE_BUREAU.SB_SERVICES, False);

    if DM_COMPANY.CO_BILLING_HISTORY_DETAIL.RecordCount > 0 then
    begin
      DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_NUMBER').Value := InvoiceNumber;
      DM_COMPANY.CO_BILLING_HISTORY.FieldByName('INVOICE_DATE').Value := Date;
      DM_COMPANY.CO_BILLING_HISTORY.FieldByName('EXPORTED_TO_A_R').AsString := EXPORTED_TO_A_R_STATUS_NO;
      DM_COMPANY.CO_BILLING_HISTORY.FieldByName('SALES_TAX_AMOUNT').AsCurrency := ColumnSum(DM_COMPANY.CO_BILLING_HISTORY_DETAIL, 'TAX', DM_COMPANY.CO_BILLING_HISTORY['CO_BILLING_HISTORY_NBR']);

      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_INTERNAL_CHECK
      else
      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_EXTERNAL_CHECK then
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_EXTERNAL_CHECK
      else
      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_WIRE_TRANSFER then
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_WIRE_TRANSFER
      else
      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_CASH then
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_CASH
      else
      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_OTHER then
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_OTHER
      else
      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_NONE then
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_IGNORE
      else
        DM_COMPANY.CO_BILLING_HISTORY.FieldByName('STATUS').AsString := CO_BILLING_STATUS_READY_FOR_ACH;
      DM_COMPANY.CO_BILLING_HISTORY.Post;

      if DM_COMPANY.CO.BILLING_IMPOUND.AsString = PAYMENT_TYPE_ACH then
      begin
        DM_COMPANY.CO_MANUAL_ACH.Append;
        DM_COMPANY.CO_MANUAL_ACH.STATUS.Value := COMMON_REPORT_STATUS__PENDING;
        DM_COMPANY.CO_MANUAL_ACH.DEBIT_DATE.Value := EffDate;
        DM_COMPANY.CO_MANUAL_ACH.CREDIT_DATE.Value := EffDate;
        DM_COMPANY.CO_MANUAL_ACH.PURPOSE_NOTES.Value := Notes;//'Global Billing';
        DM_COMPANY.CO_MANUAL_ACH.AMOUNT.Value := RoundAll(Result, 2);
        DM_COMPANY.CO_MANUAL_ACH.FROM_CL_BANK_ACCOUNT_NBR.AsVariant := DM_COMPANY.CO.BILLING_CL_BANK_ACCOUNT_NBR.AsVariant;
        DM_COMPANY.CO_MANUAL_ACH.TO_SB_BANK_ACCOUNTS_NBR.AsVariant := DM_COMPANY.CO.BILLING_SB_BANK_ACCOUNT_NBR.AsVariant;
        // save invoice number in Filler to relate Manual ACH with Invoice to update
        // Invoice status when ACH file is sent, ticket #62878
        DM_COMPANY.CO_MANUAL_ACH.FILLER.Value := IntToStr(InvoiceNumber);
        DM_COMPANY.CO_MANUAL_ACH.Post;
      end;
    end
    else
      DM_COMPANY.CO_BILLING_HISTORY.Cancel;

    ctx_DataAccess.PostDataSets(
      [
//        DM_SERVICE_BUREAU.SB, // Invoice number incremented
        DM_COMPANY.CO_SERVICES,
        DM_COMPANY.CO_BILLING_HISTORY, // invoice
        DM_COMPANY.CO_BILLING_HISTORY_DETAIL, // invoice detail
        DM_COMPANY.CO_MANUAL_ACH
        ]);

  finally
    t.Free;
    ctx_EndWait;
    INVOICE_DM.Free;
  end;
end;

procedure TINVOICE_DM.CalculateAmount(CoLevel: Boolean);
var
  SQty: integer;
  Qty: Currency;
  TempAmount, Rate, Flat: Currency;
  Minimum_Amount_Charged: boolean;
  Amount, Tax, Discount: real;
  rAmount, rTax, rDiscount: Currency;
  Invoice_Discount:real;
  InvStartDate,InvEndDate:TDateField;
begin
    InvStartDate := DM_COMPANY.CO.DISCOUNT_START_DATE;
    InvEndDate := DM_COMPANY.CO.DISCOUNT_END_DATE;
    //PrCheckDate := INVOICE_DM.PRCheckDate;

    if ((InvStartDate.IsNull)or(InvStartDate.Value <= PrCheckDate))
      and ((InvEndDate.IsNull)or(InvEndDate.Value >= PrCheckDate))
    then
        Invoice_Discount:= DM_COMPANY.CO.INVOICE_DISCOUNT.AsCurrency
    else
      Invoice_Discount  := 0;

  cdsDBDT.First;
  Qty := 0;
  Discount := 0;
  Tax := 0;
  while not cdsDBDT.Eof do
  begin
    Qty := Qty + cdsDBDT.FieldByName('Qty').AsFloat;
    cdsDBDT.Next;
  end;

  if Qty = 0 then
    Exit;

  if CoLevel and not DM_COMPANY.CO_SERVICES.FieldByName('OVERRIDE_FLAT_FEE').IsNull then
    Amount := DM_COMPANY.CO_SERVICES.FieldByName('OVERRIDE_FLAT_FEE').AsCurrency
  else if CoLevel and (Copy(DM_COMPANY.CO_SERVICES.FieldByName('Filler').AsString, 1, 1) = 'Y') then
  begin
    LocateSB_SERVICES_CALCULATIONS(Round(Qty), 0);
    Amount := 0;
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.First;
    while not DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Eof do
    begin
      Rate := DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('PER_ITEM_RATE').AsCurrency;
      Flat := DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('FLAT_AMOUNT').AsCurrency;

      if (Round(Qty) >= DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('MINIMUM_QUANTITY').AsInteger) and
         (Round(Qty) < DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('MAXIMUM_QUANTITY').AsInteger) then
        SQty := Round(Qty) - DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('MINIMUM_QUANTITY').AsInteger + 1
      else
        SQty := DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('MAXIMUM_QUANTITY').AsInteger -
                DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('MINIMUM_QUANTITY').AsInteger + 1;

      Amount := Amount + SQty * Rate + Flat;

      DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Next;
    end;
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filtered := False;
  end
  else
  begin
    LocateSB_SERVICES_CALCULATIONS(Round(Qty));
    Rate := DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('PER_ITEM_RATE').AsCurrency;
    Flat := DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.FieldByName('FLAT_AMOUNT').AsCurrency;
    Amount := Qty * Rate + Flat;
    DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS.Filtered := False;
  end;

  Minimum_Amount_Charged := False;
  if not DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MINIMUM_AMOUNT').IsNull then
  begin
    TempAmount := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MINIMUM_AMOUNT').AsCurrency;
    if TempAmount > Amount then
    begin
      Amount := TempAmount;
      Minimum_Amount_Charged := True;
    end;
  end;

  if not DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MAXIMUM_AMOUNT').IsNull then
  begin
    TempAmount := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('MAXIMUM_AMOUNT').AsCurrency;
    if TempAmount < Amount then
      Amount := TempAmount;
  end;

  if CoLevel and not Minimum_Amount_Charged and
     not DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_START_DATE').IsNull and
     (PRCheckDate >= Trunc(DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_START_DATE').AsDateTime)) and
     (DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_END_DATE').IsNull or
      (PRCheckDate <= Trunc(DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_END_DATE').AsDateTime))) and
     not DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').IsNull and
     not DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_TYPE').IsNull then
  begin
    case DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT_TYPE').AsString[1] of
      'A':
      begin
        if cdsDBDT.FieldByName('Qty').AsFloat < 0 then
          Discount := -DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').AsCurrency
        else
          Discount :=  DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').AsCurrency;
      end;
      'P': Discount := Amount * DM_COMPANY.CO_SERVICES.FieldByName('DISCOUNT').AsCurrency * 0.01;
    end;

    if (Amount < 0) and (Discount < Amount) and
       (Amount >= 0) and (Discount > Amount) then
      Discount := Amount;
  end;

  if {CoLevel and not Minimum_Amount_Charged and} Invoice_Discount <> 0 then
  begin
      Discount := Discount + Amount * Invoice_Discount * 0.01;

    if (Amount < 0) and (Discount < Amount) and
       (Amount >= 0) and (Discount > Amount) then
      Discount := Amount;
  end;

  if CoLevel and (DM_COMPANY.CO_SERVICES.FieldByName('SALES_TAX').AsString = 'Y') or
     not CoLevel and (DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SALES_TAXABLE').AsString = 'Y') then
  begin
    if DM_COMPANY.CO.FieldByName('HOME_CO_STATES_NBR').IsNull then
      raise EInconsistentData.CreateHelp('Missing home company states', IDH_InconsistentData);

    if not DM_COMPANY.CO_STATES.Locate('CO_STATES_NBR', DM_COMPANY.CO.FieldByName('HOME_CO_STATES_NBR').AsString, []) then
      raise EInconsistentData.CreateHelp('Missing row for CO_STATES_NBR', IDH_InconsistentData);

    if not DM_COMPANY.CO_SERVICES.OVERRIDE_TAX_RATE.IsNull then
      Tax := (Amount - Discount) * DM_COMPANY.CO_SERVICES.OVERRIDE_TAX_RATE.AsCurrency / 100
    else
    if not DM_COMPANY.CO.SALES_TAX_PERCENTAGE.IsNull then
      Tax := (Amount - Discount) * DM_COMPANY.CO.SALES_TAX_PERCENTAGE.AsCurrency / 100
    else if DM_SERVICE_BUREAU.SB_SALES_TAX_STATES.Locate('STATE', DM_COMPANY.CO_STATES.FieldByName('STATE').AsString, []) and
       not DM_SERVICE_BUREAU.SB_SALES_TAX_STATES.FieldByName('SALES_TAX_PERCENTAGE').IsNull then
      Tax := (Amount - Discount) * DM_SERVICE_BUREAU.SB_SALES_TAX_STATES.FieldByName('SALES_TAX_PERCENTAGE').AsCurrency / 100
    else
    begin
      if not DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_COMPANY.CO_STATES.FieldByName('STATE').AsString, []) then
        raise EInconsistentData.CreateHelp('Missing row for SY_STATE', IDH_InconsistentData);

      if not DM_SYSTEM_STATE.SY_STATES.FieldByName('SALES_TAX_PERCENTAGE').IsNull then
        Tax := (Amount - Discount) * DM_SYSTEM_STATE.SY_STATES.FieldByName('SALES_TAX_PERCENTAGE').AsCurrency;
    end;
  end;

  rAmount := 0;
  rTax := 0;
  rDiscount := 0;
  cdsDBDT.Last;
  while not cdsDBDT.Bof do
  begin
    rAmount := rAmount + RoundAll(cdsDBDT.FieldByName('Qty').AsFloat/Qty * Amount, 2);
    rTax := rTax + RoundAll(cdsDBDT.FieldByName('Qty').AsFloat/Qty * Tax, 2);
    rDiscount := rDiscount + RoundAll(cdsDBDT.FieldByName('Qty').AsFloat/Qty * Discount, 2);
    cdsDBDT.Edit;
    cdsDBDT.FieldByName('Amount').AsCurrency := RoundAll(cdsDBDT.FieldByName('Qty').AsFloat/Qty * Amount, 2);
    cdsDBDT.FieldByName('Tax').AsCurrency := RoundAll(cdsDBDT.FieldByName('Qty').AsFloat/Qty * Tax, 2);
    cdsDBDT.FieldByName('Discount').AsCurrency := RoundAll(cdsDBDT.FieldByName('Qty').AsFloat/Qty * Discount, 2);

    if cdsDBDT.RecNo = 1 then
    begin
      cdsDBDT.FieldByName('Amount').AsCurrency := cdsDBDT.FieldByName('Amount').AsCurrency + Amount - rAmount;
      cdsDBDT.FieldByName('Tax').AsCurrency := cdsDBDT.FieldByName('Tax').AsCurrency + Tax - rTax;
      cdsDBDT.FieldByName('Discount').AsCurrency := cdsDBDT.FieldByName('Discount').AsCurrency + Discount - rDiscount;
    end;
    cdsDBDT.Post;
    cdsDBDT.Prior;
  end;
end;

procedure TINVOICE_DM.PopulateInvoiceMaster(InvoiceNumber: integer; DBDTNumber: Integer);
//  var
//    DBDTText: string;


  function GetWrongEEs(const ForPayrollOnly:boolean):string;
  var
    L:variant;
    var
      sql,SqlFrom,SqlWhere,SqlWhere2,SqlWhere3,t:string;
      cdEEs:TEvBasicClientDataSet;
      P :IisListOfValues;

  begin
     result := '';
     DM_COMPANY.CO.Open;
     L := DM_COMPANY.CO['DBDT_LEVEL'];
     sql := 'select e.Ee_Nbr, e.Custom_Employee_Number, clp.First_Name,clp.Last_Name ' ;
     SQlFrom := ' From  Ee e, Cl_Person clp';
     SqlWhere :=' WHERE (clp.Cl_Person_Nbr = e.Cl_Person_Nbr) and (e.Co_Nbr = :CoNbr )';
     SqlWhere3 := '( {AsOfNow<e>} and {AsOfNow<clp>} ';
     SqlWhere2 := ' ( ' + AlwaysFalseCond + ' ';
     if ForPayrollOnly then
     begin
       SQlFrom :=SQlFrom+ ', Pr_Check  c ';
       SqlWhere := SqlWhere +' and (c.Pr_Nbr = :PrNbr) and (e.Ee_Nbr = c.Ee_Nbr) ';
       SqlWhere3 := SqlWhere3 + ' and {AsOfNow<c>} ';
     end;
     if DBDTNumber = 0 then
        SqlWhere := SqlWhere  + ' and e.' + sLevel + ' is null'
     else
        SqlWhere := SqlWhere  + ' and e.' + sLevel + ' = ' + IntToStr(DBDTNumber);

     if (L>0) then
     begin
       Sql := Sql + ',e.CO_DIVISION_NBR ';
     end ;

     if (L>1) then
     begin
        Sql := Sql + ',e.CO_BRANCH_NBR ';
     end;

    if (L>2) then
    begin
       Sql := Sql + ',e.CO_DEPARTMENT_NBR '
    end;

    if (L>3) then
    begin
       Sql := Sql + ',e.CO_Team_NBR '
    end;
    SqlWhere2 := SqlWhere2 + ' ) ';
    SqlWhere3 := SqlWhere3 + ' ) ';
    sql := sql + SqlFrom + SqlWhere + {' and ' + SqlWhere2 +} ' and ' + SqlWhere3;
    cdEEs := TEvBasicClientDataSet.Create(nil);
    P := TisListOfValues.Create;
    try
      P.AddValue('CONBR',DM_COMPANY.CO['CO_NBR']);
      P.AddValue('PrNbr',PayrollNbr);
      CustomQuery(cdEEs, sql ,CH_DATABASE_CLIENT, P);
      if cdEEs.RecordCount = 0 then
         exit;
      t := 'Can not perform billing, because next EEs have wrong DBDT setup:'+#13;
      while not cdEEs.Eof do
      begin
         t := t + cdEEs['Custom_Employee_Number'] + ';  ' +cdEEs['First_Name'] +' ' +cdEEs['Last_Name'] +#13;
         cdEEs.Next;
      end;
      Result := t;
    finally
      cdEEs.Free;
    end;
  end;
  
  function GetWrongChecks: String;
  var
    L:variant;
    sql, SqlFrom, SqlWhere, SqlWhere3, t: String;
    cdEEs:TEvBasicClientDataSet;
    P :IisListOfValues;
  begin
     result := '';
     DM_COMPANY.CO.Open;
     L := DM_COMPANY.CO['DBDT_LEVEL'];
     sql := 'select distinct e.Ee_Nbr, e.Custom_Employee_Number, clp.First_Name,clp.Last_Name ' + #13;
     SQlFrom := ' From Pr_Check_Lines cl, Ee e, Cl_Person clp, Pr_Check  c ';
     SqlWhere :=' WHERE (clp.Cl_Person_Nbr = e.Cl_Person_Nbr) and (e.Ee_Nbr = c.Ee_Nbr) and (e.Co_Nbr = :CoNbr ) and (c.Pr_Nbr = :PrNbr)';
     if DBDTNumber = 0 then
        SqlWhere := SqlWhere  + ' and cl.' + sLevel + ' is null'
     else
        SqlWhere := SqlWhere  + ' and cl.' + sLevel + ' = ' + IntToStr(DBDTNumber);
    SqlWhere3 := '({AsOfNow<cl>} and  {AsOfNow<e>} and {AsOfNow<clp>} and {AsOfNow<c>} ';
    SqlWhere3 := SqlWhere3 + ' ) ';
    sql := sql + SqlFrom + SqlWhere + ' and ' + SqlWhere3;
    cdEEs := TEvBasicClientDataSet.Create(nil);
    P := TisListOfValues.Create;
    try
      P.AddValue('CONBR',DM_COMPANY.CO['CO_NBR']);
      P.AddValue('PrNbr',PayrollNbr);
      CustomQuery(cdEEs, sql ,CH_DATABASE_CLIENT, P);
      if cdEEs.RecordCount = 0 then
         exit;
      t := 'Can not perform billing, because next EEs have checks with wrong DBDT setup:'+#13;
      while not cdEEs.Eof do
      begin
         t := t + cdEEs['Custom_Employee_Number'] + ';  ' +cdEEs['First_Name'] +' ' +cdEEs['Last_Name'] +#13;
         cdEEs.Next;
      end;
      Result := t;
    finally
      cdEEs.Free;
    end;
  end;




  procedure GetWrongDBDT_setup;
    var s:string;
  begin
     s := '';

     DBDT_Tables.CaseSensitive := false;
     if DBDT_Tables.IndexOf('EE')<>-1 then
        s := GetWrongEEs(true);
     if DBDT_Tables.IndexOf('ALL_EE')<>-1 then
          s := s+  GetWrongEEs(false);
     if (DBDT_Tables.IndexOf('pr_check_lines')<>-1) then
          s := s + GetWrongChecks;
     if s= '' then
     begin
        s := GetWrongEEs(false);
        s := s + GetWrongChecks;
     end;
     if s = '' then
        s := 'Wrong DBDT setup somewhere!!! Please, ask administrator!';
     s := 'Service name:'+ DM_COMPANY.CO_SERVICES.NAME.Value + #13 + s ;
     Raise Exception.Create(s);
  end;
begin
  with CO_INVOICE_MASTER do
  begin
    Append;

    FieldByName('PR_NBR').AsString := DM_PAYROLL.PR.FieldByName('PR_NBR').AsString;
    FieldByName('CHECK_DATE').AsDateTime := PRCheckDate;
    FieldByName('CO_NBR').AsString := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;

    FieldByName('CUSTOM_COMPANY_NUMBER').AsString := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
    FieldByName('CO_NAME').AsString := DM_COMPANY.CO.FieldByName('NAME').AsString;
    FieldByName('CL_BILLING_NBR').AsString := DM_COMPANY.CO.FieldByName('CL_BILLING_NBR').AsString;

    FieldByName('NAME').AsString := DM_CLIENT.CL_BILLING.FieldByName('NAME').AsString;
    FieldByName('ADDRESS1').AsString := DM_CLIENT.CL_BILLING.FieldByName('ADDRESS1').AsString;
    FieldByName('ADDRESS2').AsString := DM_CLIENT.CL_BILLING.FieldByName('ADDRESS2').AsString;
    FieldByName('CITY').AsString := DM_CLIENT.CL_BILLING.FieldByName('CITY').AsString;
    FieldByName('STATE').AsString := DM_CLIENT.CL_BILLING.FieldByName('STATE').AsString;
    FieldByName('ZIP_CODE').AsString := DM_CLIENT.CL_BILLING.FieldByName('ZIP_CODE').AsString;

    FieldByName('CL_NBR').AsString := DM_CLIENT.CL.FieldByName('CL_NBR').AsString;

    FieldByName('INVOICE_DATE').AsDateTime := Trunc(DM_PAYROLL.PR.FieldByName('PROCESS_DATE').AsDateTime);
    FieldByName('INVOICE_AMOUNT').AsCurrency := 0;
    FieldByName('INVOICE_TAX').AsCurrency := 0;
    FieldByName('INVOICE_DISCOUNT').AsCurrency := 0;
    FieldByName('INVOICE_TOTAL').AsCurrency := 0;

    FieldByName('CO_BILLING_HISTORY_NBR').AsInteger := ctx_DBAccess.GetGeneratorValue('G_CO_BILLING_HISTORY', dbtClient, 1);


//      FieldByName ('INVOICE_NUMBER').AsString := '-1' ;
    FieldByName('INVOICE_NUMBER').AsInteger := InvoiceNumber;

    FieldByName('TERMS').AsString := 'Due upon receipt';

    FieldByName('FILLER').AsString := PutIntoFiller(FieldByName('FILLER').AsString, sLink, 1, 8);

    if DBDTNumber <> -1 then
    begin
      FieldByName(sLevel).AsInteger := DBDTNumber;
//        DBDTText := '';
      if sLevel = 'CO_TEAM_NBR' then
      begin
        if DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', DBDTNumber, []) then
        begin
          if (DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DM_COMPANY.CO_TEAM['CO_DEPARTMENT_NBR'], [])) then
          begin
             FieldByName('CO_DEPARTMENT_NBR').AsInteger := DM_COMPANY.CO_TEAM['CO_DEPARTMENT_NBR'];
             if(DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'], [])) then
             begin
                FieldByName('CO_BRANCH_NBR').AsInteger := DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'];
                if(DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'], []))then
                begin
                   FieldByName('CO_DIVISION_NBR').AsInteger := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
                   { DBDTText := 'Division ' + DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString + #13#10 +
                     'Branch ' + DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString + #13#10 +
                     'Department ' + DM_COMPANY.CO_DEPARTMENT.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString + #13#10 +
                     'Team ' + DM_COMPANY.CO_TEAM.FieldByName('CUSTOM_TEAM_NUMBER').AsString + #13#10#13#10;}
                end else GetWrongDBDT_setup;
             end else GetWrongDBDT_setup;
          end else GetWrongDBDT_setup;
        end else GetWrongDBDT_setup;
      end
      else if sLevel = 'CO_DEPARTMENT_NBR' then
      begin
        if (DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DBDTNumber, [])) then
        begin
           if (DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'], [])) then
           begin
              FieldByName('CO_BRANCH_NBR').AsInteger := DM_COMPANY.CO_DEPARTMENT['CO_BRANCH_NBR'];
              if(DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'], [])) then
              begin
                 FieldByName('CO_DIVISION_NBR').AsInteger := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
                 {DBDTText := 'Division ' + DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString + #13#10 +
                    'Branch ' + DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString + #13#10 +
                    'Department ' + DM_COMPANY.CO_DEPARTMENT.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString + #13#10#13#10;}
              end else GetWrongDBDT_setup;
           end else GetWrongDBDT_setup;
        end else GetWrongDBDT_setup;
      end
      else if sLevel = 'CO_BRANCH_NBR' then
      begin
        if (DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DBDTNumber, [])) then
        begin
           if (DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'], [])) then
           begin
              FieldByName('CO_DIVISION_NBR').AsInteger := DM_COMPANY.CO_BRANCH['CO_DIVISION_NBR'];
              {DBDTText := 'Division ' + DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString + #13#10 +
                    'Branch ' + DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString + #13#10#13#10;}
           end else GetWrongDBDT_setup;
        end else GetWrongDBDT_setup;
      end
      else if sLevel = 'CO_DIVISION_NBR' then
      begin
        if(not DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DBDTNumber, [])) then
          GetWrongDBDT_setup;
//          DBDTText := 'Division ' + DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString + #13#10#13#10;
      end;

//        FieldByName('NOTES').AsString := DBDTText;
    end;
  end;
end;

procedure TINVOICE_DM.AddToDBDT(DBDTNbr: Integer; Amount: real);
begin
  if sLevel = 'CO_NBR' then
  begin
    Assert(cdsDBDT.RecordCount = 1);
    cdsDBDT.Edit;
    cdsDBDT.FieldByName('QTY').AsFloat := cdsDBDT.FieldByName('QTY').AsFloat + Amount;
    cdsDBDT.Post;
  end
  else
  begin
    if cdsDBDT.FindKey([DBDTNbr]) then
      cdsDBDT.Edit
    else
    begin
      cdsDBDT.Append;
      cdsDBDT[sLevel] := DBDTNbr;
      cdsDBDT['Extra'] := True;
      PopulateInvoiceMaster(GetNextInvoiceNumber, cdsDBDT[sLevel]);
      CO_INVOICE_MASTER.Post;
    end;
    cdsDBDT.FieldByName('QTY').AsFloat := cdsDBDT.FieldByName('QTY').AsFloat + Amount;
    cdsDBDT.Post;
  end;
end;

procedure TINVOICE_DM.DistrToDBDT(Amount: real);
var
  ds: TevClientDataSet;
  c: Integer;
  aExact: real;
  aRound: Currency;
begin
  ds := TevClientDataSet.Create(nil);
  try
    cdsDBDT.First;
    c := 0;
    while not cdsDBDT.Eof do
    begin
      if not cdsDBDT.FieldByName('Extra').AsBoolean then
        Inc(c);
      cdsDBDT.Next;
    end;

    ds.Data := cdsDBDT.Data;
    aRound := 0;
    aExact := 0;
    while not ds.Eof do
    begin
      if (c = cdsDBDT.RecordCount) or not ConvertNull(cdsDBDT.Lookup(sLevel, ds[sLevel], 'Extra'), False) then
      begin
        AddToDBDT(ds.FieldByName(sLevel).AsInteger, RoundAll(Amount / c, 2));
        aRound := aRound + RoundAll(Amount / c, 2);
        aExact := aExact + Amount / c;
      end;
      ds.Next;
    end;
    if RoundAll(aExact - aRound, 2) <> 0 then
      AddToDBDT(ds.FieldByName(sLevel).AsInteger, RoundAll(aExact - aRound, 2));
  finally
    ds.Free;
  end;
end;

procedure TINVOICE_DM.Count_CO_Tax_Agencies;
var
  t: TStringList;
  State:string;
begin
  t := TStringList.Create;
  try
    t.Sorted := True;
    t.Duplicates := dupIgnore;

    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('Columns', 'SY_LOCALS_NBR');
      SetMacro('TableName', 'CO_LOCAL_TAX');
      SetMacro('NbrField', 'CO');
      SetParam('RecordNbr', DM_COMPANY.CO.FieldByName('CO_NBR').Value);
      SetMacro('Condition', 'LOCAL_ACTIVE = :Active');
      SetParam('Active', GROUP_BOX_YES);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;

    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired();
    while not ctx_DataAccess.CUSTOM_VIEW.Eof do
    begin
      if DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.CachedFindKey(ctx_DataAccess.CUSTOM_VIEW['SY_LOCALS_NBR']) and
         (DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.FieldByName('SY_LOCAL_TAX_PMT_AGENCY_NBR').AsString <> '') then
         State := Trim(DM_SYSTEM.SY_STATES.Lookup('SY_STATES_NBR',VarArrayOf([DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.FieldByName('SY_STATES_NBR').AsInteger]),'STATE'));
         t.Add(
           DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.FieldByName('SY_LOCAL_TAX_PMT_AGENCY_NBR').AsString
           + '_'
           + BoolToYN((DM_SYSTEM_LOCAL.SY_LOCALS.ShadowDataSet.FieldByName('LOCAL_TYPE').AsString='C') and (State = 'PA'))
           );
      ctx_DataAccess.CUSTOM_VIEW.Next;
    end;

    DistrToDBDT(t.Count);
  finally
    t.Free;
  end;
end;

function TINVOICE_DM.GetWorkersCompAsSui(sSQL: String;
  iPrNbr: Integer): Double;
var
  Q: TevClientDataSet;
begin
  Q := TevClientDataSet.Create(nil);
  try
    Q.ProviderName := 'CL_CUSTOM_PROV';
    with TExecDSWrapper.Create(sSQL) do
    begin
      SetMacro('PrNbr', IntToStr(iPrNbr));
      SetParam('PrNbr', iPrNbr);
      SetParam('dt', SysTime);
      Q.DataRequest(AsVariant);
      Q.Close;
      Q.Open;
    end;
    Result := Q.FieldByName('Tax').AsFloat;
  finally
    FreeAndNil(Q);
  end;
end;

procedure TINVOICE_DM.CO_INVOICE_DETAILBeforePost(DataSet: TDataSet);
var
  V: Variant;
  A: Double;
begin
  if DataSet.State = dsInsert then
  begin
    if DataSet.FieldByName('SB_SERVICES_NBR').IsNull then
    begin
      V := DM_COMPANY.CO_SERVICES.Lookup('CO_SERVICES_NBR', DataSet['CO_SERVICES_NBR'], 'SB_SERVICES_NBR');
      V := DM_SERVICE_BUREAU.SB_SERVICES.Lookup('SB_SERVICES_NBR', V, 'BASED_ON_TYPE');
    end
    else
    begin
      V := DM_SERVICE_BUREAU.SB_SERVICES.Lookup('SB_SERVICES_NBR', DataSet['SB_SERVICES_NBR'], 'BASED_ON_TYPE');
    end;

    if not VarIsNull(V) then
    begin
      if V = SB_SERVICES_BASED_ON_TAX_IMPOUND then
      begin
        A := GetWorkersCompAsSui('DistrEESUIOtherTaxesForACH', CO_INVOICE_MASTERPR_NBR.AsInteger);
        DataSet.FieldByName('AMOUNT').AsFloat := DataSet.FieldByName('AMOUNT').AsFloat + A;
        DataSet.FieldByName('TOTAL').AsFloat := DataSet.FieldByName('TOTAL').AsFloat + A;
        DataSet.FieldByName('QUANTITY').AsFloat := DataSet.FieldByName('QUANTITY').AsFloat + A;
      end
      else if V = SB_SERVICES_BASED_ON_TAX_ER_IMPOUND then
      begin
        A := GetWorkersCompAsSui('DistrERSUIOtherTaxesForACH', CO_INVOICE_MASTERPR_NBR.AsInteger);
        DataSet.FieldByName('AMOUNT').AsFloat := DataSet.FieldByName('AMOUNT').AsFloat + A;
        DataSet.FieldByName('TOTAL').AsFloat := DataSet.FieldByName('TOTAL').AsFloat + A;
        DataSet.FieldByName('QUANTITY').AsFloat := DataSet.FieldByName('QUANTITY').AsFloat + A;
      end;
    end;
  end;
end;

procedure TINVOICE_DM.CheckDbdtSetupForPayroll;
 procedure CheckEeDBDT;
  var
    L:variant;
    var
      sql,SqlFrom,SqlWhere,SqlWhere2,SqlWhere3,t:string;
      cdEEs:TEvBasicClientDataSet;
      P :IisListOfValues;

  begin

     DM_COMPANY.CO.Open;
     L := DM_COMPANY.CO['DBDT_LEVEL'];
     sql := 'select e.Ee_Nbr, e.Custom_Employee_Number, clp.First_Name,clp.Last_Name ' + #13;
     SQlFrom := ' From  Ee e, Cl_Person clp, Pr_Check  c ';
     SqlWhere :=' WHERE (clp.Cl_Person_Nbr = e.Cl_Person_Nbr) and (e.Ee_Nbr = c.Ee_Nbr) and (e.Co_Nbr = :CoNbr ) and (c.Pr_Nbr = :PrNbr)';
     SqlWhere2 := ' ( ' + AlwaysFalseCond + ' ';
     SqlWhere3 := '( {AsOfNow<e>} and {AsOfNow<clp>} and {AsOfNow<c>} ';
     if (L>0) then
     begin
       SqlFrom := SqlFrom + ', Co_Division dv ';
       SqlWhere :=SqlWhere +' and (e.Co_Division_Nbr =+ dv.Co_Division_Nbr) ';
       SqlWhere2 := SqlWhere2 + ' or dv.Co_Division_Nbr IS NULL ';
       SqlWhere3 := SqlWhere3 + ' and {AsOfNow<dv>} ';
     end ;

     if (L>1) then
     begin
        SqlFrom := SqlFrom + ', Co_Branch br ';
        SqlWhere :=SqlWhere +' and (e.Co_Branch_Nbr =+ br.Co_Branch_Nbr) ';
        SqlWhere2 := SqlWhere2 + ' or br.Co_Branch_Nbr IS NULL ';
        SqlWhere3 := SqlWhere3 + ' and {AsOfNow<br>} ';
     end;

    if (L>2) then
    begin
        SqlFrom := SqlFrom + ', Co_Department  dp ';
        SqlWhere :=SqlWhere +' and (e.Co_Department_Nbr =+ dp.Co_Department_Nbr) ';
        SqlWhere2 := SqlWhere2 + ' or dp.Co_Department_Nbr IS NULL ';
        SqlWhere3 := SqlWhere3 + ' and {AsOfNow<dp>} ';
    end;

    if (L>3) then
    begin
        SqlFrom := SqlFrom + ', Co_Team tm ';
        SqlWhere :=SqlWhere +' and (e.Co_Team_Nbr =+ tm.Co_Team_Nbr) ';
        SqlWhere2 := SqlWhere2 + ' or tm.Co_Team_Nbr  IS NULL ';
        SqlWhere3 := SqlWhere3 + ' and {AsOfNow<tm>} ';
    end;
    SqlWhere2 := SqlWhere2 + ' ) ';
    SqlWhere3 := SqlWhere3 + ' ) ';
    sql := sql + SqlFrom + SqlWhere + ' and ' + SqlWhere2 + ' and ' + SqlWhere3;
    cdEEs := TEvBasicClientDataSet.Create(nil);
    P := TisListOfValues.Create;
    try
      P.AddValue('CONBR',DM_COMPANY.CO['CO_NBR']);
      P.AddValue('PrNbr',PayrollNbr);
      CustomQuery(cdEEs, sql ,CH_DATABASE_CLIENT, P);
      if cdEEs.RecordCount = 0 then
         exit;
      t := 'Can not perform billing, because next EEs have wrong DBDT setup:'+#13;
      while not cdEEs.Eof do
      begin
         t := t + cdEEs['Custom_Employee_Number'] + ';  ' +cdEEs['First_Name'] +' ' +cdEEs['Last_Name'] +#13;
         cdEEs.Next;
      end;
      raise EevException.Create(t);
    finally
      cdEEs.Free;
    end;
  end;


begin
   if sLevel = 'CO_NBR' then
     exit;
  // CheckEeDBDT;
end;

end.
