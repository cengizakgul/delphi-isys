// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STaxableTipsCleanup;

interface

uses
  controls, classes,  db, dialogs, windows, SysUtils, EvUtils, EvContext, EvClientDataSet, DateUtils, Types;

procedure CleanupTaxableTips(PeriodBegin, PeriodEnd: TDateTime);

implementation

uses
  SQuarterEndProcessing, SQuarterEndCleanup, SDataStructure, 
  EvConsts;

procedure CleanupTaxableTips(PeriodBegin, PeriodEnd: TDateTime);
var
  FULL_SY_FED_EXEMPTIONS, FULL_CL_E_DS: TevClientDataSet;
  TaxWages, TaxTips, WagesAndTips: Real;
  EDCodeType: String;
  CheckDate: TDateTime;
  AllOK: Boolean;

  function TypeIsTips(const ACode: String): Boolean;
  begin
    if CompareDate(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2013, 12, 31)) = GreaterThanValue then
      Result := TypeIsTipsAfter2013(ACode)
    else
      Result := TypeIsTipsBefore2014(ACode);
  end;

begin
  DM_PAYROLL.PR.DataRequired('ALL');
  DM_PAYROLL.PR_CHECK.DataRequired('ALL');
  DM_PAYROLL.PR_CHECK_LINES.DataRequired('ALL');

  CreateFullTable('SY_FED_EXEMPTIONS', FULL_SY_FED_EXEMPTIONS);
  CreateFullTable('CL_E_DS', FULL_CL_E_DS);

  ctx_DataAccess.UnlockPR;
  AllOK := False;
  with DM_PAYROLL do
  try
    PR_CHECK.Filtered := True;
    PR_CHECK_LINES.Filtered := True;

    PR.First;
    while not PR.EOF do
    begin
      if (PR.FieldByName('CHECK_DATE').AsDateTime >= PeriodBegin) and (PR.FieldByName('CHECK_DATE').AsDateTime <= PeriodEnd) and (PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED]) then
      begin
        PR_CHECK.Filter := 'PR_NBR=' + PR.FieldByName('PR_NBR').AsString;
        PR_CHECK.First;
        CheckDate := PR.FieldByName('CHECK_DATE').AsDateTime;
        while not PR_CHECK.EOF do
        begin
          if (ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value, 0) <> 0)
          or (ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value, 0) <> 0)
          or (ConvertNull(PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').Value, 0) <> 0)
          or (ConvertNull(PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value, 0) <> 0) then
          begin
            TaxWages := 0;
            TaxTips := 0;
            PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;

            if (ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value, 0) <> 0)
            or (ConvertNull(PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value, 0) <> 0) then
            begin
              PR_CHECK_LINES.First;
              while not PR_CHECK_LINES.EOF do
              begin
                EDCodeType := GetAsOfDateValue(FULL_CL_E_DS, 'CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE', CheckDate);
                if TypeIsTaxable(EDCodeType) then
                  if GetAsOfDateValue(FULL_SY_FED_EXEMPTIONS, 'E_D_CODE_TYPE', EDCodeType, 'EXEMPT_EMPLOYEE_OASDI', CheckDate) <> 'E' then
                    if GetAsOfDateValue(FULL_CL_E_DS, 'CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'EE_EXEMPT_EXCLUDE_OASDI', CheckDate) <> 'E' then
                    begin
                      if TypeIsTips(EDCodeType) then
                        TaxTips := TaxTips + ConvertDeduction(EDCodeType, PR_CHECK_LINES.FieldByName('AMOUNT').Value)
                      else
                        TaxWages := TaxWages + ConvertDeduction(EDCodeType, PR_CHECK_LINES.FieldByName('AMOUNT').Value);
                    end;
                PR_CHECK_LINES.Next;
              end;
              PR_CHECK.Edit;
              WagesAndTips := PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value + PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value;
              if Abs(TaxTips) > Abs(WagesAndTips) then
              begin
                PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value := 0;
                PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value := WagesAndTips;
              end
              else
              begin
                PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').Value := WagesAndTips - TaxTips;
                PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').Value := TaxTips;
              end;
              PR_CHECK.Post;
            end;

            if (ConvertNull(PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').Value, 0) <> 0)
            or (ConvertNull(PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value, 0) <> 0) then
            begin
              PR_CHECK.Edit;
              WagesAndTips := PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').Value + PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value;
              PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').Value := WagesAndTips;
              PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0;
              PR_CHECK.Post;
            end;
          end;
          PR_CHECK.Next;
        end;
      end;
      PR.Next;
    end;
    ctx_DataAccess.PostDataSets([PR_CHECK]);
    AllOK := True;
  finally
    FULL_SY_FED_EXEMPTIONS.Free;
    FULL_CL_E_DS.Free;
    PR_CHECK.Filtered := False;
    PR_CHECK_LINES.Filtered := False;
    ctx_DataAccess.LockPR(AllOK);
  end;
end;

end.
