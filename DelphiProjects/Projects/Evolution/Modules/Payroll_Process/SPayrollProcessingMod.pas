// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPayrollProcessingMod;

interface

uses
  SysUtils, Classes, DB, EvTypes, EvBasicUtils, EvUtils, SSecurityInterface,
  EvStreamUtils,  EvConsts, SDataStructure, SLoadYTD,
  STMP_INVOICE_DM, Variants, EvContext, isBaseClasses, EvCommonInterfaces,
  EvMainboard, EvExceptions, SDueDateCalculation, evAchBase,
  EvDataset, EvDataAccessComponents, EvClientDataSet, evAch;

implementation

uses
  PayrollLog, SQuarterEndProcessing, SQuarterEndCleanup, STaxableTipsCleanup,
  SProcessTCImport, SProcessingEngine, SPayrollReport, SReportSettings,
  ISZippingRoutines, evAchPrenote, evAchManual, STimeOffUtils,
  SDataDictclient, SPAMonthEndCleanup, EvCopyPayroll, IsBasicUtils;

type

  TevPayrollProcessing = class(TisInterfacedObject, IevPayrollProcessing)
  private
    FTaxCalculatorPR_CHECK: TevClientDataset;
  protected
    procedure TaxCalculatorBeforePost(DataSet: TDataSet);
    procedure TaxCalculatorAfterInsert(DataSet: TDataSet);
    procedure TaxCalculator(aPR, aPR_BATCH, aPR_CHECK, aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI, aPR_CHECK_LOCALS: IisStream; DisableYTDs, DisableShortfalls, NetToGross: Boolean);
    function VoidCheck(CheckToVoid: Integer; PrBatchNbr: Integer): Integer;
    procedure RedistributeDBDT(const PrCheckLineNbr: Integer; Redistribution: IisParamsCollection);
    procedure UnvoidCheck(CheckNbr: Integer);
    function GetPayrollBatchDefaults(const PrNbr: Integer): IisListOfValues;
    function GrossToNet(var Warnings: String; CheckNumber: Integer; ApplyUpdatesInTransaction: Boolean; JustPreProcess: Boolean;
      FetchPayrollData: Boolean; SecondCheck: Boolean): Boolean; // Result indicates if there were any shortfalls
    function ProcessBilling(prNumber: Integer; ForPayroll: Boolean;
                             InvoiceNumber: Integer; ReturnDS: Boolean): Variant;
    function  DoQuarterEndCleanup(CoNbr: Integer; QTREndDate: TDateTime; MinTax: Double;
      CleanupAction: Integer; const ReportFilePath, EECustomNbr: string;
      ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean; const LockType: TResourceLockType; ExtraOptions: Variant; out Warnings: String): Boolean;
    function  DoPAMonthEndCleanup(CoNbr: Integer; MonthEndDate: TDateTime; MinTax: Double;
                                  const AutoProcess: Boolean; const EECustomNbr: string;
                                  const PrintReports: Boolean;
                                  const LockType: TResourceLockType; out Warnings: String): Boolean;
    function IsQuarterEndCleanupNecessary(CONbr: Integer; QTREndDate: TDateTime): Boolean;
    function ProcessPayroll(PayrollNumber: Integer; JustPreProcess: Boolean; out Log: string;
      const CheckReturnQueue: Boolean;
      const LockType: TResourceLockType = rlTryOnce;
      const PrBatchNbr: Integer = 0): String;
    procedure DoERTaxAdjPayroll(aDate: TDateTime);
    procedure DoQtrEndTaxLiabAdj(CoNbr: Integer; aDate: TDateTime; MinLiability: Double);
    procedure CreateTaxAdjCheck(aDate: TDateTime);
    procedure CleanupTaxableTips(PeriodBegin: TDateTime; PeriodEnd: TDateTime);
    function CreatePR(Co_Nbr: Integer; CheckDate: TDateTime): Integer;
    procedure RefreshSchduledEDsCheck(const CheckNbr: Integer);
    procedure CreatePRBatchDetails(PrBatchNumber: Integer; TwoChecksPerEE, CalcCheckLines: Boolean;
      CheckType, Check945Type: String; NumberOfChecks, SalaryOrHourly: Integer; EENumbers: Variant;
      const TcFileIn, TcFileOut: IisStream;
      const TcLookupOption: TCImportLookupOption; const TcDBDTOption: TCImportDBDTOption;
      const TcFourDigitYear: Boolean; const TcFileFormat: TCFileFormatOption;
      const TcAutoImportJobCodes: Boolean; const TcUseEmployeePayRates: boolean; const UpdateBalance: Boolean;
      const IncludeTimeOffRequests: Boolean);
    procedure RefreshSchduledEDsBatch(const BatchNbr: Integer; const RegularChecksOnly: Boolean);      
    function  LoadYTD(Cl_Nbr, Co_Nbr, Year, EeNbr: Integer): Variant;
    function  GetEEYTD(const AYear: Integer; const AEeNbr: Integer): IisListOfValues;
    function ProcessBillingForServices(ClNbr: Integer; CoNbr: Integer; const List: string; const EffDate: TDateTime; const Notes: string; var bManualACH : Boolean): Real;
    function PrintPayroll(PrNbr: Integer; PrintChecks: Boolean;UseSbMiskCheckForm: Boolean = False; DefaultMiskCheckForm: String = ''): IisStream;
    function ReprintPayroll(PrNbr: Integer; PrintReports, PrintInvoices, CurrentTOA: Boolean;
      PayrollChecks, AgencyChecks, TaxChecks, BillingChecks, CheckForm, MiscCheckForm: String;
      CheckDate: TDateTime; SkipCheckStubCheck: Boolean; DontPrintBankInfo: Boolean; HideBackground: Boolean;
      DontUseVMRSettings: Boolean; ForcePrintVoucher: Boolean; PrReprintHistoryNBR: Integer; Reason: String; out AWarnings, AErrors: String): IisStream;
    procedure ReRunPayrolReportsForVmr(const PrNbr: Integer);
    procedure ReCreateVmrHistory(const CoNbr: Integer; const PeriodBeginDate, PeriodEndDate: TDateTime);
    procedure ProcessManualACH(ACHOptions: IevACHOptions; bPreprocess: Boolean; BatchBANK_REGISTER_TYPE: String; ACHDataStream: IEvDualStream;
      out AchFile, AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
    procedure ProcessPrenoteACH(ACHOptions: IevACHOptions; APrenoteCLAccounts: Boolean; ACHDataStream: IEvDualStream;
      out AchFile, AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String; out AExceptions: String; ACHFolder: String);
    // Payroll routines
    procedure CopyPayroll(const APayrollNbr: Integer);
    procedure VoidPayroll(const APayrollNbr: Integer; Const VoidTaxChecks, VoidAgencyChecks,
                                VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling: Boolean);
    procedure DeletePayroll(const APayrollNbr: Integer);
    function DeleteBatch(const ABatchNbr: Integer): boolean;    
    procedure SetPayrollLock(const APrNbr: Integer; const AUnlocked: Boolean);
  end;


function GetPayrollProcessing: IevPayrollProcessing;
begin
  Result := TevPayrollProcessing.Create;
end;

{ TPayrollCalculationsComp }

function TevPayrollProcessing.ProcessBilling;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Result := STMP_INVOICE_DM.ProcessBilling(prNumber, ForPayroll, InvoiceNumber, ReturnDS);
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

procedure TevPayrollProcessing.CleanupTaxableTips(PeriodBegin, PeriodEnd: TDateTime);
begin
  ctx_Security.EnableSystemAccountRights;
  try
    with DM_PAYROLL do
    try
      SaveDSStates([PR]);
      STaxableTipsCleanup.CleanupTaxableTips(PeriodBegin, PeriodEnd);
    finally
      LoadDSStates([PR]);
    end;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

procedure TevPayrollProcessing.CreateTaxAdjCheck(aDate: TDateTime);
begin
  ctx_Security.EnableSystemAccountRights;
  try
    SQuarterEndProcessing.CreateTaxAdjCheck(aDate);
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

procedure TevPayrollProcessing.DoERTaxAdjPayroll(aDate: TDateTime);
begin
  ctx_Security.EnableSystemAccountRights;
  try
    SQuarterEndProcessing.DoERTaxAdjPayroll(aDate);
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

procedure TevPayrollProcessing.DoQtrEndTaxLiabAdj(CoNbr: Integer; aDate: TDateTime;
  MinLiability: Double);
begin
  ctx_Security.EnableSystemAccountRights;
  try
    with DM_PAYROLL do
    try
      SaveDSStates([PR]);
      SQuarterEndProcessing.DoQtrEndTaxLiabAdj(CoNbr, aDate, MinLiability);
    finally
      LoadDSStates([PR]);
    end;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.DoQuarterEndCleanup(CoNbr: Integer; QTREndDate: TDateTime; MinTax: Double;
  CleanupAction: Integer; const ReportFilePath, EECustomNbr: string;
  ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean; const LockType: TResourceLockType; ExtraOptions: Variant; out Warnings: String): Boolean;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    with DM_PAYROLL do
    try
      PR_CHECK.LookupsEnabled := False;
      PR_CHECK_LINES.LookupsEnabled := False;
      SaveDSStates([PR]);
      Result := SQuarterEndCleanup.DoQuarterEndCleanup(CONbr, QTREndDate, MinTax, TCleanupPayrollAction(CleanupAction), ReportFilePath, EECustomNbr, ForceCleanup, PrintReports, CurrentQuarterOnly, LockType, ExtraOptions, Warnings);
    finally
      LoadDSStates([PR]);
      PR_CHECK.LookupsEnabled := True;
      PR_CHECK_LINES.LookupsEnabled := True;
    end;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.DoPAMonthEndCleanup(CoNbr: Integer;
  MonthEndDate: TDateTime; MinTax: Double; const AutoProcess: Boolean;
  const EECustomNbr: string; const PrintReports: Boolean;
  const LockType: TResourceLockType; out Warnings: String): Boolean;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    with DM_PAYROLL do
    try
      PR_CHECK.LookupsEnabled := False;
      PR_CHECK_LINES.LookupsEnabled := False;
      SaveDSStates([PR]);
      Result := SPAMonthEndCleanup.DoPAMonthEndCleanup(CONbr, MonthEndDate, MinTax,
        AutoProcess, EECustomNbr, PrintReports, LockType, Warnings);
    finally
      LoadDSStates([PR]);
      PR_CHECK.LookupsEnabled := True;
      PR_CHECK_LINES.LookupsEnabled := True;
    end;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.IsQuarterEndCleanupNecessary(CONbr: Integer; QTREndDate: TDateTime): Boolean;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Result := ctx_DataAccess.QecRecommended(CONbr, QTREndDate);
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.ProcessPayroll(PayrollNumber: Integer;
  JustPreProcess: Boolean; out Log: string;
  const CheckReturnQueue: Boolean;
  const LockType: TResourceLockType = rlTryOnce;
  const PrBatchNbr: Integer = 0): String;
var
  PayrollLog: TPayrollLog;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    PayrollLog := TPayrollLog.Create;
    try
      PayrollLog.Add('Starting Payroll Processing');
      ctx_PayrollCalculation.ProcessFeedback(True);
      try
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Close;
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
        DM_PAYROLL.PR.DataRequired('PR_NBR=' + IntToStr(PayrollNumber));

        LockResource(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), 'A payroll for this client is already being processed by ', LockType);
        LockResource(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + DM_PAYROLL.PR.CO_NBR.AsString, 'A payroll for this company is already being processed by ', LockType);

        ctx_DataAccess.Validate := False;
        DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.LookupsEnabled := False;
        DM_CLIENT.CL_BANK_ACCOUNT.LookupsEnabled := False;
        DM_CLIENT.CL_E_DS.LookupsEnabled := False;
        DM_COMPANY.CO.LookupsEnabled := False;
        DM_COMPANY.CO_PR_ACH.LookupsEnabled := False;
        DM_COMPANY.CO_FED_TAX_LIABILITIES.LookupsEnabled := False;
        DM_COMPANY.CO_STATE_TAX_LIABILITIES.LookupsEnabled := False;
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.LookupsEnabled := True;
        DM_COMPANY.CO_SUI_LIABILITIES.LookupsEnabled := False;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.LookupsEnabled := False;
        DM_EMPLOYEE.EE.LookupsEnabled := False;
        DM_EMPLOYEE.EE_RATES.LookupsEnabled := False;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.LookupsEnabled := False;
        DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.LookupsEnabled := False;
        DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.LookupsEnabled := False;
        DM_EMPLOYEE.EE_STATES.LookupsEnabled := False;
        DM_EMPLOYEE.EE_LOCALS.LookupsEnabled := False;
        DM_PAYROLL.PR.LookupsEnabled := False;
        DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
        DM_PAYROLL.PR_CHECK.AlwaysCallCalcFields := True;
        DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;
        DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := False;
        DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := False;
        DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := False;
        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.LookupsEnabled := False;

        try
          try
            Result := DoMainProcessing(PayrollNumber, JustPreProcess, PayrollLog, CheckReturnQueue, PrBatchNbr);
            if not JustPreProcess then
            begin
              ctx_PayrollCalculation.ProcessFeedback(True, 'Processing invoices...');
              ctx_DataAccess.PayrollIsProcessing := True;
              try
                ctx_PayrollProcessing.ProcessBilling(PayrollNumber, True, -1, False);
              finally
                ctx_DataAccess.PayrollIsProcessing := False;
              end
            end
          except
            on E: Exception do
            begin
              if Result = '' then
                E.Message := 'Error: "' + E.Message + '" while ' + ctx_PayrollCalculation.GetLastFeedBack
              else
                E.Message := 'Warning messages:'#13#10 + Result + #13#10#13#10'Error: "'
                  + E.Message + '" while ' + ctx_PayrollCalculation.GetLastFeedBack;
              raise;
            end;
          end;
        finally
          try
            UnlockResource(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + DM_PAYROLL.PR.CO_NBR.AsString, LockType);
          finally
            UnlockResource(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), LockType);
          end;

          DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS.LookupsEnabled := True;
          DM_CLIENT.CL_BANK_ACCOUNT.LookupsEnabled := True;
          DM_CLIENT.CL_E_DS.LookupsEnabled := True;
          DM_COMPANY.CO.LookupsEnabled := True;
          DM_COMPANY.CO_PR_ACH.LookupsEnabled := True;
          DM_COMPANY.CO_FED_TAX_LIABILITIES.LookupsEnabled := True;
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.LookupsEnabled := True;
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.LookupsEnabled := True;
          DM_COMPANY.CO_SUI_LIABILITIES.LookupsEnabled := True;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.LookupsEnabled := True;
          DM_EMPLOYEE.EE.LookupsEnabled := True;
          DM_EMPLOYEE.EE_RATES.LookupsEnabled := True;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.LookupsEnabled := True;
          DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.LookupsEnabled := True;
          DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.LookupsEnabled := True;
          DM_EMPLOYEE.EE_STATES.LookupsEnabled := True;
          DM_EMPLOYEE.EE_LOCALS.LookupsEnabled := True;
          DM_PAYROLL.PR.LookupsEnabled := True;
          DM_PAYROLL.PR_CHECK.AlwaysCallCalcFields := False;
          DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
          DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
          DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := True;
          DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := True;
          DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := True;
          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.LookupsEnabled := True;
          ctx_DataAccess.Validate := True;
        end;
      finally
        ctx_PayrollCalculation.ProcessFeedback(False);
      end;
    finally
      try
        try
          PayrollLog.Add('Ending Payroll Processing');
        finally
          Log := PayrollLog.Text;
        end;
      finally
        FreeAndNil(PayrollLog);
      end;
    end;

  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.LoadYTD(Cl_Nbr, Co_Nbr, Year, EeNbr: Integer): Variant;
begin
  Result := SLoadYTD.LoadYTD(Cl_Nbr, Co_Nbr, Year, EeNbr);
  Result := VarArrayOf([(IInterface(Result[0]) as IevDataSetHolder).DataSet.Data,
                        (IInterface(Result[1]) as IevDataSetHolder).DataSet.Data]);
end;

function  TevPayrollProcessing.GetEEYTD(const AYear: Integer; const AEeNbr: Integer): IisListOfValues;
var
  v: Variant;
  q: IevQuery;
  ds: IevDataSet;
begin
  q := TevQuery.Create('select CO_NBR from EE a where a.EE_NBR='+IntToStr(AEeNbr)+' and {AsOfNow<a>} ');
  Result := TIsListOfValues.Create;
  v := SLoadYTD.LoadYTD(ctx_DataAccess.ClientID, q.Result.FieldByName('CO_NBR').AsInteger, AYear, AEeNbr);

  ds := TevDataSet.Create((IInterface(v[0]) as IevDataSetHolder).DataSet);
  Result.AddValue('EarningsDeductions', ds);
  ds := TevDataSet.Create((IInterface(v[1]) as IevDataSetHolder).DataSet);
  Result.AddValue('Taxes', ds);
end;

procedure TevPayrollProcessing.CreatePRBatchDetails(PrBatchNumber: Integer; TwoChecksPerEE, CalcCheckLines: Boolean;
  CheckType, Check945Type: String; NumberOfChecks, SalaryOrHourly: Integer; EENumbers: Variant;
  const TcFileIn, TcFileOut: IisStream;
  const TcLookupOption: TCImportLookupOption; const TcDBDTOption: TCImportDBDTOption;
  const TcFourDigitYear: Boolean; const TcFileFormat: TCFileFormatOption;
  const TcAutoImportJobCodes: Boolean; const TcUseEmployeePayRates: boolean; const UpdateBalance: Boolean;
  const IncludeTimeOffRequests: Boolean);
var
  MyCount, NbrOfCheckLines, NbrOfChecks, TemplateNbr, NextCheckSerialNumber, I: Integer;
  GlobalTransactionManager: TTransactionManager;
  StatusDate: TDateTime;
  iGenNbr: Integer;

  procedure AddTimeOffRequests;
  var
    DS: TevClientDataSet;
    Used: Real;
    CL_E_DS_NBR, EE_TIME_OFF_ACCRUAL_NBR: Integer;
  begin
    DS := TevClientDataSet.Create(nil);
    try
      DS.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
      with TExecDSWrapper.Create('TimeOffEeOperations') do
      begin
        SetParam('EE_NBR', DM_PAYROLL.PR_CHECK.EE_NBR.Value);
        SetParam('BEGIN_DATE', DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE.AsDateTime);
        SetParam('END_DATE', DM_PAYROLL.PR_BATCH.PERIOD_END_DATE.AsDateTime);
        DS.DataRequest(AsVariant);
      end;
      DS.Open;
      if not DS.IsEmpty then
      begin
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('ACCRUAL_DATE BETWEEN '''+DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE.AsString+
          ''' AND '''+DM_PAYROLL.PR_BATCH.PERIOD_END_DATE.AsString+'''');

        DS.First;
        Used := 0;
        CL_E_DS_NBR := DS.FieldByName('CL_E_DS_NBR').AsInteger;
        EE_TIME_OFF_ACCRUAL_NBR := DS.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger;
        while not DS.Eof do
        begin
           Assert(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Locate('EE_TIME_OFF_ACCRUAL_OPER_NBR', DS.FieldByName('EE_TIME_OFF_ACCRUAL_OPER_NBR').Value, []));

           if EE_TIME_OFF_ACCRUAL_NBR <> DS.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger then
           begin
             DM_PAYROLL.PR_CHECK_LINES.Append;
             DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value;
             DM_PAYROLL.PR_CHECK_LINES.PR_NBR.Value := DM_PAYROLL.PR_CHECK.PR_NBR.Value;
             DM_PAYROLL.PR_CHECK_LINES.LINE_TYPE.Value := CHECK_LINE_TYPE_USER;
             DM_PAYROLL.PR_CHECK_LINES.CL_E_DS_NBR.Value := CL_E_DS_NBR;
             DM_PAYROLL.PR_CHECK_LINES.HOURS_OR_PIECES.Value := Used;
             DM_PAYROLL.PR_CHECK_LINES.Post;
             Used := 0;
           end;
           Used := Used + DS.FieldByName('USED').Value;
           EE_TIME_OFF_ACCRUAL_NBR := DS.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger;
           CL_E_DS_NBR := DS.FieldByName('CL_E_DS_NBR').AsInteger;

           DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
           DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_NBR.Value := DM_PAYROLL.PR_CHECK.PR_NBR.Value;
           DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_BATCH_NBR.Value := DM_PAYROLL.PR_CHECK.PR_BATCH_NBR.Value;
           DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value;
           DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;

           DS.Next;

           if DS.Eof then
           begin
             DM_PAYROLL.PR_CHECK_LINES.Append;
             DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value;
             DM_PAYROLL.PR_CHECK_LINES.PR_NBR.Value := DM_PAYROLL.PR_CHECK.PR_NBR.Value;
             DM_PAYROLL.PR_CHECK_LINES.LINE_TYPE.Value := CHECK_LINE_TYPE_USER;
             DM_PAYROLL.PR_CHECK_LINES.CL_E_DS_NBR.Value := CL_E_DS_NBR;
             DM_PAYROLL.PR_CHECK_LINES.HOURS_OR_PIECES.Value := Used;
             DM_PAYROLL.PR_CHECK_LINES.Post;
           end;
        end;
      end;
    finally
      DS.Free;
    end;
  end;

  procedure CreatePRCheckDetails(PaySalary, PayHourly: Boolean);
  begin
    ctx_DataAccess.Validate := False;
    try
      try
        ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
          DM_PAYROLL.PR,
          DM_PAYROLL.PR_BATCH,
          DM_PAYROLL.PR_CHECK,
          DM_PAYROLL.PR_CHECK_LINES,
          DM_PAYROLL.PR_CHECK_STATES,
          DM_PAYROLL.PR_CHECK_SUI,
          DM_PAYROLL.PR_CHECK_LOCALS,
          DM_PAYROLL.PR_CHECK_LINE_LOCALS,
          DM_PAYROLL.PR_SCHEDULED_E_DS,
          DM_CLIENT.CL_E_DS,
          DM_CLIENT.CL_PERSON,
          DM_CLIENT.CL_PENSION,
          DM_COMPANY.CO,
          DM_COMPANY.CO_E_D_CODES,
          DM_COMPANY.CO_STATES,
          DM_EMPLOYEE.EE,
          DM_EMPLOYEE.EE_SCHEDULED_E_DS,
          DM_EMPLOYEE.EE_STATES,
          DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
          DM_SYSTEM_STATE.SY_STATES,
          False,
          PaySalary,
          PayHourly,
          False,
          True
        );
        if IncludeTimeOffRequests
        and (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR)
        and (DM_PAYROLL.PR_CHECK.CHECK_TYPE.AsString = CHECK_TYPE2_REGULAR) then
          AddTimeOffRequests;
      except
        if DM_PAYROLL.PR_CHECK_LINES.Active then
          DM_PAYROLL.PR_CHECK_LINES.CancelUpdates;
        if DM_PAYROLL.PR_CHECK_LOCALS.Active then
          DM_PAYROLL.PR_CHECK_LOCALS.CancelUpdates;
        if DM_PAYROLL.PR_CHECK_SUI.Active then
          DM_PAYROLL.PR_CHECK_SUI.CancelUpdates;
        if DM_PAYROLL.PR_CHECK_STATES.Active then
          DM_PAYROLL.PR_CHECK_STATES.CancelUpdates;
        if DM_PAYROLL.PR_CHECK_LINE_LOCALS.Active then
          DM_PAYROLL.PR_CHECK_LINE_LOCALS.CancelUpdates;
        if ctx_DataAccess.CUSTOM_VIEW.Active then
          ctx_DataAccess.CUSTOM_VIEW.CancelUpdates;
        raise;
      end;
    finally
      ctx_DataAccess.Validate := True;
    end;
  end;

  procedure DoCheckAfterInsert;
  begin
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CheckType;
    DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value;
    ctx_PayrollCalculation.AssignCheckDefaults(TemplateNbr, NextCheckSerialNumber);
    Inc(NextCheckSerialNumber);
    ctx_DataAccess.TemplateNumber := TemplateNbr;
    DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := ctx_PayrollCalculation.GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
    DM_PAYROLL.PR_CHECK.FieldByName('ABA_NUMBER').Value := ctx_PayrollCalculation.GetABANumber;
    DM_PAYROLL.PR_CHECK.UpdateBlobData('NOTES_NBR', DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').AsString);
  end;

  procedure ApplyTemplate;
  begin
    if (ctx_DataAccess.TemplateNumber = 0) and not DM_CLIENT.PR_BATCH.CO_PR_CHECK_TEMPLATES_NBR.IsNull then
      ctx_DataAccess.TemplateNumber := DM_CLIENT.PR_BATCH.CO_PR_CHECK_TEMPLATES_NBR.AsInteger;
    try
      if ctx_DataAccess.TemplateNumber <> 0 then
      with DM_CLIENT do
      begin
        EE_STATES.Filter := 'EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString;
        EE_LOCALS.Filter := 'EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString;
        ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, PR_CHECK, PR_CHECK_STATES, PR_CHECK_LOCALS,
          DM_COMPANY.CO_BATCH_STATES_OR_TEMPS, DM_COMPANY.CO_BATCH_LOCAL_OR_TEMPS, DM_EMPLOYEE.EE_STATES,
          DM_EMPLOYEE.EE_LOCALS);
      end;
    finally
      ctx_DataAccess.TemplateNumber := 0; 
    end;
  end;
  
  procedure AddStates;
  var
    TempNbr: Integer;
  begin
    if DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD] then
    begin
      TempNbr := DM_EMPLOYEE.EE.Lookup('EE_NBR', DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value, 'HOME_TAX_EE_STATES_NBR');
      if not DM_PAYROLL.PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').Value, TempNbr]), []) then
      begin
        DM_PAYROLL.PR_CHECK_STATES.Insert;
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value := TempNbr;
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').Value := 'N';
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value := 'N';
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').Value := 'N';
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').Value := 'N';
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE;
        DM_PAYROLL.PR_CHECK_STATES.Post;
      end;
      DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', TempNbr, []);
      DM_COMPANY.CO_SUI.Filter := 'CO_STATES_NBR=' + DM_EMPLOYEE.EE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').AsString;
      DM_COMPANY.CO_SUI.Filtered := True;
      try
        DM_COMPANY.CO_SUI.First;
        while not DM_COMPANY.CO_SUI.Eof do
        begin
          DM_PAYROLL.PR_CHECK_SUI.Insert;
          DM_PAYROLL.PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value := DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').Value;
          DM_PAYROLL.PR_CHECK_SUI.Post;
          DM_COMPANY.CO_SUI.Next;
        end;
      finally
        DM_COMPANY.CO_SUI.Filtered := False;
        DM_COMPANY.CO_SUI.Filter := '';
      end;
    end;
  end;

begin
  ctx_Security.EnableSystemAccountRights;
  try
    DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;
    DM_EMPLOYEE.EE.LookupsEnabled := False;
    DM_EMPLOYEE.EE_RATES.LookupsEnabled := False;
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Close;
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
    DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=' + IntToStr(PRBatchNumber));
    ctx_DataAccess.NewBatchInserted := True;
    try
      DM_PAYROLL.PR.DataRequired('PR_NBR=' + DM_PAYROLL.PR_BATCH.FieldByName('PR_NBR').AsString);
      if not DM_PAYROLL.PR.IsBlobLoaded('PAYROLL_COMMENTS') then
         DM_PAYROLL.PR.GetBlobData('PAYROLL_COMMENTS');
      DM_COMPANY.CO.DataRequired('CO_NBR=' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString);
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_REGULAR, PAYROLL_TYPE_SUPPLEMENTAL,
      PAYROLL_TYPE_CL_CORRECTION, PAYROLL_TYPE_SB_CORRECTION, PAYROLL_TYPE_SETUP, PAYROLL_TYPE_BACKDATED] then
      begin
        ctx_PayrollCalculation.ProcessFeedback(True, '');
        GlobalTransactionManager := TTransactionManager.Create(nil);
        DM_EMPLOYEE.EE.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
        DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
        with DM_EMPLOYEE, DM_CLIENT, DM_PAYROLL do
        try
          PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=0');
          PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=0');
          PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=0');
          EE_STATES.Filtered := True;
          EE_LOCALS.Filtered := True;
          GlobalTransactionManager.Initialize([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI,
            PR_CHECK_LOCALS, DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER]);
          ctx_DataAccess.OpenEEDataSetForCompany(EE_STATES, PR.FieldByName('CO_NBR').AsInteger);
          ctx_DataAccess.OpenEEDataSetForCompany(EE_LOCALS, PR.FieldByName('CO_NBR').AsInteger);
          PR_CHECK.DataRequired('PR_BATCH_NBR=' + PR_BATCH.FieldByName('PR_BATCH_NBR').AsString);
          PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=0');
          EE.Filtered := False;
          EE.Filter := 'PAY_FREQUENCY=''' + PR_BATCH.FieldByName('FREQUENCY').AsString + '''';
          if (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP)  and VarIsNull(EENumbers) then
            EE.Filter := EE.Filter + ' and CURRENT_TERMINATION_CODE=''' + EE_TERM_ACTIVE
            + ''' and CURRENT_HIRE_DATE<=''' + PR_BATCH.FieldByName('PERIOD_END_DATE').AsString + '''';
          if not DM_PAYROLL.PR_BATCH.FieldByName('CO_PR_FILTERS_NBR').IsNull then
          begin
            DM_COMPANY.CO_PR_FILTERS.DataRequired('CO_PR_FILTERS_NBR=' + DM_PAYROLL.PR_BATCH.FieldByName('CO_PR_FILTERS_NBR').AsString);
            if not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DIVISION_NBR').IsNull then
              EE.Filter := EE.Filter + ' and CO_DIVISION_NBR=' + DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DIVISION_NBR').AsString;
            if not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_BRANCH_NBR').IsNull then
              EE.Filter := EE.Filter + ' and CO_BRANCH_NBR=' + DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_BRANCH_NBR').AsString;
            if not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DEPARTMENT_NBR').IsNull then
              EE.Filter := EE.Filter + ' and CO_DEPARTMENT_NBR=' + DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DEPARTMENT_NBR').AsString;
            if not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_TEAM_NBR').IsNull then
              EE.Filter := EE.Filter + ' and CO_TEAM_NBR=' + DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_TEAM_NBR').AsString;
            if not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_PAY_GROUP_NBR').IsNull then
              EE.Filter := EE.Filter + ' and CO_PAY_GROUP_NBR=' + DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_PAY_GROUP_NBR').AsString;
          end;
          if SalaryOrHourly = 1 then
            EE.Filter := EE.Filter + ' and SALARY_AMOUNT is not null';
          if SalaryOrHourly = 2 then
            EE.Filter := EE.Filter + ' and SALARY_AMOUNT is null';
          if not VarIsNull(EENumbers) then
          begin
            EE.FilterListFieldName := 'EE_NBR';
            EE.FilterList.Clear;
            for I := VarArrayLowBound(EENumbers, 1) to VarArrayHighBound(EENumbers, 1) do
              EE.FilterList.Add(VarToStr(EENumbers[I]));
          end;
          EE.Filtered := True;

    // Speed optimization begin
          NbrOfChecks := EE.RecordCount;
          NbrOfCheckLines := 0;
          CL_E_DS.DataRequired('ALL');
          ctx_DataAccess.OpenEEDataSetForCompany(EE_SCHEDULED_E_DS, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger);
          EE.First;
          while not EE.EOF do
          begin
            if (PR_BATCH.FieldByName('PAY_SALARY').Value = 'Y') and (ConvertNull(EE.FieldByName('SALARY_AMOUNT').Value, 0) <> 0) then
              Inc(NbrOfCheckLines);
            if PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value = 'Y' then
              Inc(NbrOfCheckLines);
            if EE.FieldByName('GENERATE_SECOND_CHECK').Value = 'Y' then
            begin
              Inc(NbrOfChecks);
              if (PR_BATCH.FieldByName('PAY_SALARY').Value = 'Y') and (ConvertNull(EE.FieldByName('SALARY_AMOUNT').Value, 0) <> 0) then
                Inc(NbrOfCheckLines);
              if PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value = 'Y' then
                Inc(NbrOfCheckLines);
            end;
            EE.Next;
          end;
          if TwoChecksPerEE then
            NbrOfChecks := NbrOfChecks * 2;

         iGenNbr := ctx_DBAccess.GetGeneratorValue('G_PR_CHECK', dbtClient, NbrOfChecks * NumberOfChecks);
         ctx_DataAccess.NextInternalCheckNbr := iGenNbr - NbrOfChecks * NumberOfChecks + 1;
         ctx_DataAccess.LastInternalCheckNbr := iGenNbr;

         iGenNbr := ctx_DBAccess.GetGeneratorValue('G_NEXT_PAYMENT_SERIAL_NBR', dbtClient, NbrOfChecks * NumberOfChecks * (-1));
         NextCheckSerialNumber := -100000000 - (iGenNbr + NbrOfChecks * NumberOfChecks - 1);

          with ctx_DataAccess.CUSTOM_VIEW do
          begin
            if Active then
              Close;
            with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
            begin
              SetMacro('Columns', 'CL_E_DS_NBR, GENERATE_SECOND_CHECK');
              SetMacro('Table1', 'EE');
              SetMacro('Table2', 'EE_SCHEDULED_E_DS');
              SetMacro('JoinField', 'EE');
              SetMacro('Condition', StringReplace(EE.Filter, 'EE_NBR', 'T1.EE_NBR', [rfReplaceAll]));
              DataRequest(AsVariant);
            end;
            Open;
            NbrOfCheckLines := NbrOfCheckLines + RecordCount;
            First;
            while not EOF do
            begin
              if CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE') = ED_OEARN_PIECEWORK then
              begin
                Inc(NbrOfCheckLines);
                if FieldByName('GENERATE_SECOND_CHECK').Value = 'Y' then
                  Inc(NbrOfCheckLines);
              end;
              if ConvertNull(CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE'), TIP_MINWAGE_NONE) <> TIP_MINWAGE_NONE then
                if CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE') <> ED_OEARN_BANQUET_TIPS then
                  if not VarIsNull(CL_E_DS.NewLookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'CL_E_D_GROUPS_NBR')) then
                  begin
                    Inc(NbrOfCheckLines);
                    if FieldByName('GENERATE_SECOND_CHECK').Value = 'Y' then
                      Inc(NbrOfCheckLines);
                  end;
              Next;
            end;
          end;
          if TwoChecksPerEE then
            NbrOfCheckLines := NbrOfCheckLines * 2;

         iGenNbr := ctx_DBAccess.GetGeneratorValue('G_PR_CHECK_LINES', dbtClient, NbrOfCheckLines * NumberOfChecks);
         ctx_DataAccess.NextInternalCheckLineNbr := iGenNbr - NbrOfCheckLines + 1;
         ctx_DataAccess.LastInternalCheckLineNbr := iGenNbr;

    // Speed optimization end

          MyCount := 0;
          StatusDate := 0;
          ctx_PayrollCalculation.GetLimitedEDs(PR.FieldByName('CO_NBR').AsInteger, PR.FieldByName('PR_NBR').AsInteger,
            GetBeginYear(PR.FieldByName('CHECK_DATE').AsDateTime), PR.FieldByName('CHECK_DATE').AsDateTime);
          ctx_DataAccess.StartNestedTransaction([dbtClient]);
          try
            if not CalcCheckLines then
              ctx_DataAccess.RecalcLineBeforePost := False;
            try
              EE.First;
              I := 0;
              while not EE.EOF do
              begin
                Inc(MyCount);
                Inc(I);
                ctx_PayrollCalculation.ProcessFeedback(True, 'Creating check(s) for EE #' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, '')) +
                  '  (' + IntToStr(MyCount) + ' of ' + IntToStr(EE.RecordCount * NumberOfChecks) + ')...');
                try
                  TemplateNbr := ConvertNull(DM_PAYROLL.PR_BATCH.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').Value, 0);
                  PR_CHECK.Insert;
                  DoCheckAfterInsert;
                  if UpdateBalance then
                    PR_CHECK.UPDATE_BALANCE.AsString := GROUP_BOX_YES
                  else
                    PR_CHECK.UPDATE_BALANCE.AsString := GROUP_BOX_NO;

                  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP then
                  begin
                    PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_YTD;
                    PR_CHECK.FieldByName('UPDATE_BALANCE').Value := GROUP_BOX_NO;
                    if TwoChecksPerEE then
                    begin
                      if StatusDate <> 0 then
                        PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := StatusDate;
                      PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := Check945Type;
                      PR_CHECK.Post;
                      if StatusDate = 0 then
                        StatusDate := PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime;
                      CreatePRCheckDetails((PR_BATCH.FieldByName('PAY_SALARY').Value = 'Y'), (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value = 'Y'));
                      ctx_DataAccess.NewCheckInserted := False;
                      ApplyTemplate;
                      AddStates;
                      PR_CHECK.Insert;
                      DoCheckAfterInsert;
                      PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_QTD;
                      PR_CHECK.FieldByName('UPDATE_BALANCE').Value := GROUP_BOX_NO;
                    end;
                  end;

                  if EE.FieldByName('GENERATE_SECOND_CHECK').Value = 'Y' then
                  begin
                    if StatusDate <> 0 then
                      PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := StatusDate;
                    PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := Check945Type;
                    PR_CHECK.Post;
                    if StatusDate = 0 then
                      StatusDate := PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime;
                    CreatePRCheckDetails((PR_BATCH.FieldByName('PAY_SALARY').Value = 'Y'), (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value = 'Y'));
                    ctx_DataAccess.NewCheckInserted := False;
                    ApplyTemplate;
                    AddStates;
                    if not EE.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').IsNull then
                      TemplateNbr := EE.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').AsInteger;
                    PR_CHECK.Insert;
                    DoCheckAfterInsert;
                    if CheckType <> CHECK_TYPE2_REGULAR then
                      PR_CHECK.FieldByName('CHECK_TYPE').Value := CheckType;
                  end;

                  if StatusDate <> 0 then
                    PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := StatusDate;
                  PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := Check945Type;
                  if PR_CHECK.FieldByName('TAX_FREQUENCY').IsNull then
                    PR_CHECK.FieldByName('TAX_FREQUENCY').Value := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value;
                  PR_CHECK.Post;
                  if StatusDate = 0 then
                    StatusDate := PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime;
                  CreatePRCheckDetails((PR_BATCH.FieldByName('PAY_SALARY').Value = 'Y'), (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value = 'Y'));
                  ctx_DataAccess.NewCheckInserted := False;
                  ApplyTemplate;
                  AddStates;
                except
                  on E:Exception do
                  begin
                    if (Length(E.Message) > 0) and (E.Message[Length(E.Message)] = '.')
                    and (pos('#' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, '')), E.Message) > 0) then
                    else
                      E.Message := E.Message + ' while creating check for EE #' + Trim(ConvertNull(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, ''));
                    raise;
                  end;
                end;

                if PR_CHECK_LINES.RecordCount > 500 then
                begin
                  ctx_PayrollCalculation.ProcessFeedback(True, 'Saving...');
                  GlobalTransactionManager.ApplyUpdates;
                  PR_CHECK_LINES.Close;
                  PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=0');
                end;

                if I = NumberOfChecks then
                begin
                  I := 0;
                  EE.Next;
                end;
                DM_EMPLOYEE.EE_STATES.Filtered := False;
                DM_EMPLOYEE.EE_LOCALS.Filtered := False;
              end;
              ctx_DataAccess.TemplateNumber := 0;
              ctx_PayrollCalculation.ProcessFeedback(True, 'Saving...');
              GlobalTransactionManager.ApplyUpdates;
              if Assigned(TcFileIn) then
              begin
                PopulateDataSets([DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT,
                  DM_COMPANY.CO_TEAM, DM_CLIENT.CL_PERSON], '', False);
                PopulateDataSets([DM_COMPANY.CO_DIVISION,  DM_COMPANY.CO_JOBS], 'CO_NBR='+ PR.FieldByName('CO_NBR').AsString, False);
                ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_JOBS, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT,
                  DM_COMPANY.CO_TEAM, DM_CLIENT.CL_PERSON]);
                ctx_PayrollCalculation.ProcessFeedback(True, 'Importing...');
                ProcessTCImport(TcFileIn, TcFileOut, TcLookupOption, TcDBDTOption, TcFourDigitYear,
                  TcFileFormat, TcAutoImportJobCodes, TcUseEmployeePayRates);
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS, PR_CHECK, PR_CHECK_LINES]);
              end;
            finally
              EE.Filtered := False;
              EE.Filter := '';
              if not CalcCheckLines then
                ctx_DataAccess.RecalcLineBeforePost := True;
            end;

            ctx_DataAccess.CommitNestedTransaction;
          except
            ctx_DataAccess.RollbackNestedTransaction;
            raise;
          end;
        finally
          EE.FilterListFieldName := '';
          EE.FilterList.Clear;
          ctx_PayrollCalculation.EraseLimitedEDs;
          GlobalTransactionManager.Free;
          EE_STATES.Filtered := False;
          EE_LOCALS.Filtered := False;
          EE_STATES.Filter := '';
          EE_LOCALS.Filter := '';
          ctx_PayrollCalculation.ProcessFeedback(False, '');
        end;
      end;
    finally
      ctx_PayrollCalculation.ReleaseTempCheckLines;
      ctx_DataAccess.NewBatchInserted := False;
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
      DM_EMPLOYEE.EE.LookupsEnabled := True;
      DM_EMPLOYEE.EE_RATES.LookupsEnabled := True;
    end;

  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.ProcessBillingForServices(ClNbr, CoNbr: Integer; const List: string; const EffDate: TDateTime; const Notes: string; var bManualACH: Boolean): Real;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Result := STMP_INVOICE_DM.ProcessBillingForServices(ClNbr, CoNbr, List, EffDate, Notes, bManualACH);
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.CreatePR(Co_Nbr: Integer; CheckDate: TDateTime): Integer;
var
  B: Boolean;
  Freq: String;
  J: Integer;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Result := 0;  
    ctx_PayrollCalculation.ProcessFeedback(True, '');
    DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(Co_Nbr));
    ctx_PayrollCalculation.ProcessFeedback(True, 'Creating payroll...');
    DM_PAYROLL.PR.DataRequired('PR_NBR=0');
    DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=0');
    DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=0');
    DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_STATES_NBR=0');
    DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_CHECK_SUI_NBR=0');
    DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_CHECK_LOCALS_NBR=0');
    DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR=0');

    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    with DM_PAYROLL do
    try
      PR.Insert;
      PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
      PR.FieldByName('CHECK_DATE').Value := CheckDate;
      PR.FieldByName('CHECK_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      PR.FieldByName('SCHEDULED').Value := 'Y';
      PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_REGULAR;
      PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING;
      PR.FieldByName('EXCLUDE_ACH').Value := 'N';
      PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := 'N';
      PR.FieldByName('EXCLUDE_AGENCY').Value := 'N';
      PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := 'N';
      PR.FieldByName('EXCLUDE_BILLING').Value := 'N';
      PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_NONE;


      try
        PR_SCHEDULED_EVENT.DisableControls;
        PR.Post;
        ctx_DataAccess.PostDataSets([PR, PR_SCHEDULED_EVENT]);
        Result := PR.FieldByName('PR_NBR').AsInteger;
      finally
        ctx_DataAccess.CancelDataSets([PR_SCHEDULED_EVENT]);
        PR_SCHEDULED_EVENT.EnableControls;
      end;

      Freq := ctx_PayrollCalculation.GetCompanyPayFrequencies;
      while Pos(#9, Freq) <> 0 do
      begin
        Delete(Freq, 1, Pos(#9, Freq));
        Delete(Freq, 2, 1);
      end;
      for J := 1 to Length(Freq) do
      begin
        ctx_PayrollCalculation.ProcessFeedback(True, 'Creating batch...');
        PR_BATCH.Insert;
        PR_BATCH.FieldByName('FREQUENCY').Value := Freq[J];
        B := ctx_PayrollCalculation.FindNextPeriodBeginEndDate;
        if B then
          PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE']
        else
          PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := ctx_PayrollCalculation.NextPeriodBeginDate;
        PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
        if B then
          PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE']
        else
          PR_BATCH.FieldByName('PERIOD_END_DATE').Value := ctx_PayrollCalculation.NextPeriodEndDate;
        PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;
        PR_BATCH.FieldByName('PAY_SALARY').Value := 'Y';
        PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'Y';
        PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'Y';
        PR_BATCH.Post;
        ctx_DataAccess.PostDataSets([PR_BATCH]);

        ctx_PayrollCalculation.ProcessFeedback(False);
        CreatePRBatchDetails(PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger, False, True, CHECK_TYPE2_REGULAR, CHECK_TYPE_945_NONE, 1, 0, Null,
          nil, nil, loByName, loFull, False, loFixed, False, False, True, False);
      end;
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.CancelDataSets([PR, PR_BATCH, PR_SCHEDULED_EVENT, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS]);
      ctx_DataAccess.RollbackNestedTransaction;
      ctx_PayrollCalculation.ProcessFeedback(False);
      raise;
    end;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.PrintPayroll(PrNbr: Integer; PrintChecks: Boolean; UseSbMiskCheckForm: Boolean = False; DefaultMiskCheckForm: String = ''): IisStream;
var
  Errors, Warnings, sCO: String;
  i: Integer;
  v: Variant;
  ReportParams: TrwReportParams;
  CO_INVOICE_MASTER: TevClientDataSet;
  CO_INVOICE_DETAIL: TevClientDataSet;
  AllOK: Boolean;
  MiskCheckForm:string;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    ctx_PayrollCalculation.ProcessFeedback(True);
    try
      DM_PAYROLL.PR.DataRequired('PR_NBR=' + IntToStr(PrNbr));
      sCO := 'CO_NBR=' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString;
      DM_COMPANY.CO.DataRequired(sCO);
      DM_EMPLOYEE.EE.DataRequired(sCO);
      DM_SERVICE_BUREAU.SB.DataRequired('');
      ctx_RWLocalEngine.StartGroup;
      try
        Errors := '';
        Warnings := '';
        try
        // Print payroll and miscellaneous checks
          if PrintChecks and (DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString[1] in [GROUP_BOX_REPORTS, GROUP_BOX_NONE]) then
          begin

            if (UseSbMiskCheckForm) then // get MiskCheckForm from SB
            begin
               if DefaultMiskCheckForm <> '' then
                 MiskCheckForm := DefaultMiskCheckForm
               else
                 MiskCheckForm := DM_SERVICE_BUREAU.SB.MISC_CHECK_FORM.AsString;
            end else  //Use Company MiskCheckForm
            begin
               MiskCheckForm := DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString ;

            end;
            ctx_PayrollCalculation.ProcessFeedback(True, 'Preparing payroll checks for printing...');
            ctx_PayrollCheckPrint.PrintChecks(PrNbr, nil, ctRegular, False, True, DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString);

            ctx_PayrollCalculation.ProcessFeedback(True, 'Preparing tax checks for printing...');
            ctx_PayrollCheckPrint.PrintChecks(PrNbr, nil, ctTax, False, True, MiskCheckForm);

            ctx_PayrollCalculation.ProcessFeedback(True, 'Preparing agency checks for printing...');
            ctx_PayrollCheckPrint.PrintChecks(PrNbr, nil, ctAgency, False, True, DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString);
          end
          else begin
            // Prepare payroll checks only but don't print (ticket 43222) 
            ctx_PayrollCalculation.ProcessFeedback(True, 'Preparing payroll checks for printing...');
            ctx_PayrollCheckPrint.PrintChecks(PrNbr, nil, ctRegular, False, False, DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString);
          end;

          Assert(DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []), 'Could not locate payroll during printing of reports');

        // Doing billing
          if (DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString[1] in [GROUP_BOX_CHECKS, GROUP_BOX_NONE]) and
          (DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString[1] in [INVOICE_PRINT_STATUS_Billing_Calculated, INVOICE_PRINT_STATUS_Invoice_Printed]) then
          begin
            ctx_PayrollCalculation.ProcessFeedback(True, 'Printing invoices...');
            DM_COMPANY.CO_BILLING_HISTORY.DataRequired('PR_NBR=' + IntToStr(PrNbr));
            v := ctx_PayrollProcessing.ProcessBilling(0, False, DM_COMPANY.CO_BILLING_HISTORY['INVOICE_NUMBER'], True);
            ReportParams := TrwReportParams.Create;
            CO_INVOICE_MASTER := TevClientDataSet.Create(nil);
            CO_INVOICE_DETAIL := TevClientDataSet.Create(nil);
            try
              CO_INVOICE_MASTER.Data := v[0];
              CO_INVOICE_DETAIL.Data := v[1];

              if not DM_COMPANY.CO.FieldByName('NUMBER_OF_INVOICE_COPIES').IsNull then
                ReportParams.Copies := DM_COMPANY.CO.FieldByName('NUMBER_OF_INVOICE_COPIES').AsInteger;
              ReportParams.Add('DataSets', DataSetsToVarArray([CO_INVOICE_MASTER, CO_INVOICE_DETAIL]), False);
              ReportParams.Add('PrintNote', '1', False);

              ctx_RWLocalEngine{(NotQueued)}.CalcPrintReport('Invoice', ReportParams);
            finally
              ReportParams.Free;
              CO_INVOICE_MASTER.Free;
              CO_INVOICE_DETAIL.Free;
            end;
            if DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString = INVOICE_PRINT_STATUS_Billing_Calculated then
            begin
              ctx_DataAccess.UnlockPR;
              AllOK := False;
              try
                DM_PAYROLL.PR.Edit;
                DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_Invoice_Printed;
                DM_PAYROLL.PR.Post;
                ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
                AllOK := True;
              finally
                ctx_DataAccess.LockPR(AllOK);
              end;
            end;
          end;
        // Print billing checks
          if PrintChecks and (DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString[1] in [GROUP_BOX_REPORTS, GROUP_BOX_NONE]) then
          begin
            ctx_PayrollCalculation.ProcessFeedback(True, 'Preparing billing checks for printing...');
            ctx_PayrollCheckPrint.PrintChecks(PrNbr, nil, ctBilling, False, True, DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString);
          end;

          if DM_PAYROLL.PR.FieldByName('EXCLUDE_R_C_B_0R_N').AsString[1] in [GROUP_BOX_CHECKS, GROUP_BOX_NONE] then
          begin
        // Printing reports
            ctx_PayrollCalculation.ProcessFeedback(True, 'Preparing reports for printing...');
            PayrollReports(PrNbr, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger, ctx_DataAccess.ClientID);
          end;
        except
          ctx_RWLocalEngine.EndGroup(rdtCancel);
          raise;
        end;
        ctx_PayrollCalculation.ProcessFeedback(True, 'Printing checks and reports...');
        Result := ctx_RWLocalEngine.EndGroup(rdtPrepareVMR,
          DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+ ' Payroll '+ DateToStr(DM_PAYROLL.PR['CHECK_DATE'])+ '#'+ IntToStr(DM_PAYROLL.PR['RUN_NUMBER']),
          DM_PAYROLL.PR['CO_NBR'], DM_PAYROLL.PR['PR_NBR'], DM_PAYROLL.PR['CHECK_DATE']);
        Warnings := ctx_RWLocalEngine.GetLastWarnings;
        Errors := ctx_RWLocalEngine.GetLastExceptions;
      finally
        if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in
              [PAYROLL_TYPE_REGULAR, PAYROLL_TYPE_SUPPLEMENTAL, PAYROLL_TYPE_SETUP,
               PAYROLL_TYPE_SB_CORRECTION, PAYROLL_TYPE_CL_CORRECTION] then
        begin
          with DM_EMPLOYEE.EE do
          begin
            First;
            while not EOF do
            begin
              if (FieldByName('NEW_HIRE_REPORT_SENT').AsString = NEW_HIRE_REPORT_PENDING) and
               (FieldByName('CURRENT_HIRE_DATE').Value < DM_PAYROLL.PR.FieldByName('STATUS_DATE').Value) then
              begin
                Edit;
                FieldByName('NEW_HIRE_REPORT_SENT').AsString := NEW_HIRE_REPORT_COMPLETED;
                Post;
              end;
              Next;
            end;
          end;
          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE]);
        end;
      end;

      if not Assigned(Result) then
        Result := TisStream.Create;
      i := Result.Size;
      Result.Position := i;
      Result.WriteString(Errors);
      Result.WriteString(Warnings);
      Result.WriteInteger(i);
      Result.Position := 0;
    finally
      ctx_PayrollCalculation.ProcessFeedback(False);
    end;

  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

function TevPayrollProcessing.ReprintPayroll(PrNbr: Integer; PrintReports, PrintInvoices, CurrentTOA: Boolean;
  PayrollChecks, AgencyChecks, TaxChecks, BillingChecks, CheckForm, MiscCheckForm: String;
  CheckDate: TDateTime; SkipCheckStubCheck: Boolean; DontPrintBankInfo: Boolean; HideBackground: Boolean;
  DontUseVMRSettings: Boolean; ForcePrintVoucher: Boolean; PrReprintHistoryNBR: Integer; Reason: String; out AWarnings, AErrors: String): IisStream;
var
  slPayrollChecks, slAgencyChecks, slTaxChecks, slBillingChecks: TStringList;
  sCO: String;
  ReportParams: TrwReportParams;
  RepDest: TrwReportDestination;
  v: Variant;
  CO_INVOICE_MASTER, CO_INVOICE_DETAIL: TevClientDataSet;
  I: Integer;
  Warnings: String;

  function ManualCheckPrinted(PrNbr, CheckNbr: String): Boolean;
  var
    Q: IevQuery;
  begin
    Q := TevQuery.Create('GenericSelect2CurrentWithCondition');
    Q.Macro['COLUMNS'] := 'count(*)';
    Q.Macro['TABLE1'] := 'PR_REPRINT_HISTORY';
    Q.Macro['TABLE2'] := 'PR_REPRINT_HISTORY_DETAIL';
    Q.Macro['JOINFIELD'] := 'PR_REPRINT_HISTORY';
    Q.Macro['CONDITION'] := 't2.MISCELLANEOUS_CHECK=''N'' and t1.PR_NBR=' + PrNbr
          + ' and t2.BEGIN_CHECK_NUMBER<=' + CheckNbr + ' and t2.END_CHECK_NUMBER>=' + CheckNbr;
    if Q.Result.Fields[0].Value > 0 then
      Result := True
    else
      Result := False;
  end;

  procedure LogReprint(CheckNumbers: TStringList; MiscCheck: Boolean; PrNbr: Integer; Reason: String);
  var
    MySortedList: TStringList;
    BeginNumber, EndNumber: string;
    I: Integer;
  begin
    if CheckNumbers = nil then
      Exit;

    DM_PAYROLL.PR_REPRINT_HISTORY.DataRequired('PR_NBR=' + IntToStr(PrNbr));
    DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.DataRequired('PR_REPRINT_HISTORY_NBR=1');
    DM_PAYROLL.PR_REPRINT_HISTORY.Insert;
    DM_PAYROLL.PR_REPRINT_HISTORY.FieldByName('PR_NBR').Value := PrNbr;
    DM_PAYROLL.PR_REPRINT_HISTORY.FieldByName('REPRINT_DATE').Value := Now;
    DM_PAYROLL.PR_REPRINT_HISTORY.FieldByName('REASON').AsString := Reason;
    DM_PAYROLL.PR_REPRINT_HISTORY.Post;
    MySortedList := TStringList.Create;
    try
      MySortedList.Sorted := True;
      for I := 0 to CheckNumbers.Count - 1 do
        MySortedList.Add(PadStringLeft(CheckNumbers.Strings[I], '0', 9));
      BeginNumber := MySortedList.Strings[0];
      EndNumber := MySortedList.Strings[0];
      for I := 1 to MySortedList.Count - 1 do
      begin
        if StrToInt(MySortedList.Strings[I]) <> StrToInt(EndNumber) + 1 then
        begin
          DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.Insert;
          if MiscCheck then
            DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('MISCELLANEOUS_CHECK').AsString := 'Y'
          else
            DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('MISCELLANEOUS_CHECK').AsString := 'N';
          DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('BEGIN_CHECK_NUMBER').Value := StrToInt(BeginNumber);
          DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('END_CHECK_NUMBER').Value := StrToInt(EndNumber);
          DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger := DM_PAYROLL.PR_REPRINT_HISTORY.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger;
          DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.Post;
          BeginNumber := MySortedList.Strings[I];
        end;
        EndNumber := MySortedList.Strings[I];
      end;
      DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.Insert;
      if MiscCheck then
        DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('MISCELLANEOUS_CHECK').AsString := 'Y'
      else
        DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('MISCELLANEOUS_CHECK').AsString := 'N';
      DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('BEGIN_CHECK_NUMBER').Value := StrToInt(BeginNumber);
      DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('END_CHECK_NUMBER').Value := StrToInt(EndNumber);
      DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger := DM_PAYROLL.PR_REPRINT_HISTORY.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger;
      DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL.Post;
    finally
      MySortedList.Free;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_REPRINT_HISTORY, DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL]);
  end;


begin

  AWarnings := '';
  AErrors := '';
  slPayrollChecks := TStringList.Create;
  slAgencyChecks := TStringList.Create;
  slTaxChecks := TStringList.Create;
  slBillingChecks := TStringList.Create;
  try
    slPayrollChecks.CommaText := PayrollChecks;
    slAgencyChecks.CommaText := AgencyChecks;
    slTaxChecks.CommaText := TaxChecks;
    slBillingChecks.CommaText := BillingChecks;
    ctx_RWLocalEngine.StartGroup;
    try
      DM_PAYROLL.PR.DataRequired('PR_NBR=' + IntToStr(PrNbr));
      sCO := 'CO_NBR=' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString;
      DM_COMPANY.CO.DataRequired(sCO);
      if not (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED]) then
      begin
        if slPayrollChecks.Count = 0 then
          Exit;
        ctx_StartWait('Recalculating manual checks...');
        ctx_DataAccess.UnlockPRSimple;
        try
          for I := 0 to slPayrollChecks.Count - 1 do
          begin
            if not ManualCheckPrinted(IntToStr(PrNbr), slPayrollChecks[I]) then
            begin
              ctx_PayrollCalculation.GrossToNet(Warnings, StrToInt(slPayrollChecks[I]), True, True, True, False);
              ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINE_LOCALS]);
            end;  
          end;
        finally
          ctx_DataAccess.LockPRSimple;
          ctx_EndWait;
        end;
      end;
      if slPayrollChecks.Count > 0 then
      begin
        ctx_StartWait('Preparing regular checks for printing...');
        try
          ctx_PayrollCheckPrint.PrintChecks(PrNbr, slPayrollChecks, ctRegular, CurrentTOA, True, CheckForm, False, CheckDate,
            SkipCheckStubCheck, DontPrintBankInfo, HideBackground, ForcePrintVoucher);
          DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
        finally
          ctx_EndWait;
        end;
        LogReprint(slPayrollChecks, False, PrNbr, Reason);
      end;

      if slAgencyChecks.Count > 0 then
      begin
        slAgencyChecks.Sort;
        ctx_StartWait('Preparing agency checks for printing...');
        try
          ctx_PayrollCheckPrint.PrintChecks(0, slAgencyChecks, ctAgency, False, True, MiscCheckForm, False, CheckDate,
            SkipCheckStubCheck, DontPrintBankInfo, HideBackground);
        finally
          ctx_EndWait;
        end;
        LogReprint(slAgencyChecks, True, PrNbr, Reason);          
      end;
      if slTaxChecks.Count > 0 then
      begin
        slTaxChecks.Sort;
        ctx_StartWait('Preparing tax checks for printing...');
        try
          ctx_PayrollCheckPrint.PrintChecks(0, slTaxChecks, ctTax, False, True, MiscCheckForm, False, CheckDate,
            SkipCheckStubCheck, DontPrintBankInfo, HideBackground);
        finally
          ctx_EndWait;
        end;
      end;
      if slBillingChecks.Count > 0 then
      begin
        slBillingChecks.Sort;
        ctx_StartWait('Preparing billing checks for printing...');
        try
          ctx_PayrollCheckPrint.PrintChecks(0, slBillingChecks, ctBilling, False, True, MiscCheckForm, False, CheckDate,
            SkipCheckStubCheck, DontPrintBankInfo, HideBackground);
        finally
          ctx_EndWait;
        end;
      end;

      if PrintInvoices then
      begin
        ctx_StartWait('Printing invoices...');
        try
          DM_COMPANY.CO_BILLING_HISTORY.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
          DM_COMPANY.CO_BILLING_HISTORY.First;
          with DM_COMPANY.CO_BILLING_HISTORY do
            if not Eof then
            begin
              v := ProcessBilling(0, False, FieldByName('INVOICE_NUMBER').AsInteger, True);

              ReportParams := TrwReportParams.Create;
              CO_INVOICE_MASTER := TevClientDataSet.Create(nil);
              CO_INVOICE_DETAIL := TevClientDataSet.Create(nil);
              try
                CO_INVOICE_MASTER.Data := v[0];
                CO_INVOICE_DETAIL.Data := v[1];

                if not DM_COMPANY.CO.FieldByName('NUMBER_OF_INVOICE_COPIES').IsNull then
                  ReportParams.Copies := DM_COMPANY.CO.FieldByName('NUMBER_OF_INVOICE_COPIES').AsInteger;
                ReportParams.Add('DataSets', DataSetsToVarArray([CO_INVOICE_MASTER, CO_INVOICE_DETAIL]), False);
                ReportParams.Add('PrintNote', '1', False);

                ctx_RWLocalEngine.CalcPrintReport('Invoice', ReportParams);
              finally
                ReportParams.Free;
                CO_INVOICE_MASTER.Free;
                CO_INVOICE_DETAIL.Free;
              end;
//              Next;
            end;
        finally
          ctx_EndWait;
        end;
      end;

      if PrintReports then
        PayrollReports(PrNbr, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger, ctx_DataAccess.ClientID, rdtPrinter);
    except
      ctx_RWLocalEngine.EndGroup(rdtCancel);
      raise;
    end;
    ctx_StartWait('Printing checks/reports...');
    try
      if DontUseVMRSettings then
        RepDest := rdtNone
      else
        RepDest := rdtPrepareVMR;
      Result := ctx_RWLocalEngine.EndGroup(RepDest,
        'Repr.Pr '+ DateToStr(DM_PAYROLL.PR['CHECK_DATE'])+ '#'+ IntToStr(DM_PAYROLL.PR['RUN_NUMBER']),
        DM_PAYROLL.PR['CO_NBR'], DM_PAYROLL.PR['PR_NBR'], DM_PAYROLL.PR['CHECK_DATE'], PrReprintHistoryNBR);

      AWarnings := ctx_RWLocalEngine.GetLastWarnings;
      AErrors := ctx_RWLocalEngine.GetLastExceptions;
    finally
      ctx_EndWait;
    end;
  finally
    slPayrollChecks.Free;
    slAgencyChecks.Free;
    slTaxChecks.Free;
    slBillingChecks.Free;
  end;
end;

procedure TevPayrollProcessing.ReRunPayrolReportsForVmr(const PrNbr: Integer);
var
  ms: IisStream;
  W, E, R: String;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Assert(ctx_VmrEngine <> nil, 'VMR engine is not loaded');
    Assert(CheckVmrActive, 'VMR is not licensed or temporarily blocked');
    DM_CLIENT.CL.DataRequired('');
    Assert(CheckVmrSetupActive, 'VMR is not setup for this client');
    ms := ReprintPayroll(PrNbr, True, True, False, '', '', '', '', '', '', 0, False, False, False, False, False, 0,R, W,E);
    ctx_VmrEngine.VmrStorePostProcessReportResult(ms);
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

procedure TevPayrollProcessing.ReCreateVmrHistory(
  const CoNbr: Integer; const PeriodBeginDate, PeriodEndDate: TDateTime);
var
  ReportResults: TrwReportResults;
  tmpReportResults: TrwReportResults;
  tmpResult: TrwReportResult;
  sJobDescr, sReportDescr: string;

  function SubProcess: TrwReportResult;
  var
    i: Integer;
    r: TrwReportResults;
    BlobStream: IisStream;
  begin
    Result := ReportResults.Add;
    BlobStream := DM_CLIENT.CL_BLOB.GetBlobData('DATA');

    if BlobStream.Size > 0 then
    begin
      i := BlobStream.ReadInteger;
      if i = $FF then // new format
      begin
        r := TrwReportResults.Create;
        try
          r.SetFromStream(UnCompressStreamFromStream(BlobStream));
          Assert(r.Count > 0);
          Result.Assign(r[0]);
        finally
          r.Free;
        end;
      end
      else
      begin
        Result.ReportType := TReportType(i);
        Result.Data := TEvDualStreamHolder.Create;
        InflateStream(BlobStream.RealStream, Result.Data.RealStream);
      end;
    end;

    Result.ReportName := sReportDescr;
    Result.Layers := [0, 2];
  end;

var
  FPrDataSet, FVmrPrDataSet, FReturnDataSet, FVmrReturnDataSet: TEvClientDataSet;
  d: TDateTime;
  k: integer;
  QNbr, ENbr: Variant;
begin
  ctx_Security.EnableSystemAccountRights;
  try
    Assert(ctx_VmrEngine <> nil, 'VMR engine is not loaded');
    Assert(CheckVmrActive, 'VMR is not licensed or temporarily blocked');
    DM_CLIENT.CL.DataRequired('');
    Assert(CheckVmrSetupActive, 'VMR is not setup for this client');
    FPrDataSet := TevClientDataSet.Create(nil);
    FVmrPrDataSet := TevClientDataSet.Create(nil);
    FReturnDataSet := TevClientDataSet.Create(nil);
    FVmrReturnDataSet := TevClientDataSet.Create(nil);
    ctx_StartWait('Processing returns');
    try
      with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
      begin
        SetMacro('COLUMNS', 'distinct CAST(Trim(StrCopy(T1.DESCRIPTION, 200, 8)) as VARCHAR(8)) EVENT_DATE_STR, '+
          'CAST(Trim(StrCopy(T1.DESCRIPTION, 0, 100)) as VARCHAR(100)) JOB_DESCR, '+
          'CAST(Trim(StrCopy(T1.DESCRIPTION, 100, 100)) as VARCHAR(100)) REPORT_DESCR');
        SetMacro('TABLE1', 'SB_MAIL_BOX_CONTENT');
        SetMacro('TABLE2', 'SB_MAIL_BOX');
        SetMacro('JOINFIELD', 'SB_MAIL_BOX');
        SetMacro('CONDITION', 't2.cl_nbr=:ClNbr and Trim(StrCopy(T1.DESCRIPTION, 200, 8)) >= :BegDate and '+
          'Trim(StrCopy(T1.DESCRIPTION, 200, 8)) <= :EndDate and CAST(Trim(StrCopy(T1.DESCRIPTION, 208, 10)) as INTEGER) = :CONBR ');
        SetParam('ClNbr', ctx_DataAccess.ClientID);
        SetParam('CONBR', CoNbr);
        SetParam('BegDate', FormatDateTime('YYYYMMDD', PeriodBeginDate));
        SetParam('EndDate', FormatDateTime('YYYYMMDD', PeriodEndDate));
        ctx_DataAccess.GetSbCustomData(FVmrReturnDataSet, AsVariant);
        FVmrReturnDataSet.IndexFieldNames := 'EVENT_DATE_STR;JOB_DESCR;REPORT_DESCR';
      end;
      DM_SYSTEM_MISC.SY_REPORTS_GROUP.CheckDSCondition('');
      DM_SYSTEM_MISC.SY_REPORTS.CheckDSCondition('');
      DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.CheckDSCondition('');
      DM_COMPANY.CO.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR='+ IntToStr(CoNbr));
      ctx_DataAccess.OpenDataSets([DM_SYSTEM_MISC.SY_REPORTS_GROUP, DM_SYSTEM_MISC.SY_REPORTS,
                     DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT, DM_COMPANY.CO]);
      d := GetEndMonth(PeriodBeginDate);
      while d <= PeriodEndDate do
      begin
        with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
        begin
          SetMacro('COLUMNS', 'T1.CO_TAX_RETURN_QUEUE_NBR, T1.SY_REPORTS_GROUP_NBR, T1.SY_GL_AGENCY_REPORT_NBR, T2.CL_BLOB_NBR, T1.CL_CO_CONSOLIDATION_NBR, T2.CHANGE_DATE, T2.EE_NBR');
          SetMacro('TABLE1', 'CO_TAX_RETURN_QUEUE');
          SetMacro('TABLE2', 'CO_TAX_RETURN_RUNS');
          SetMacro('JOINFIELD', 'CO_TAX_RETURN_QUEUE');
          SetMacro('CONDITION', 'T2.CL_BLOB_NBR is not null and T1.PERIOD_END_DATE = :D and T1.CO_NBR = :CONBR  '+
            ' and T1.CLIENT_COPY_PRINTED = '+ QuotedStr(GROUP_BOX_YES)+
            ' and T1.STATUS in (''' + TAX_RETURN_STATUS_PROCESSED + ''', ''' + TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED + ''' )');
          SetParam('D', d);
          SetParam('CONBR', CoNbr);
          ctx_DataAccess.GetCustomData(FReturnDataSet, AsVariant);
          FReturnDataSet.IndexFieldNames := 'CO_TAX_RETURN_QUEUE_NBR;EE_NBR;CHANGE_DATE;SY_REPORTS_GROUP_NBR;SY_GL_AGENCY_REPORT_NBR;CL_BLOB_NBR'
        end;
        ReportResults := TrwReportResults.Create;
        try
          sJobDescr := 'Tax Returns for '+ DateToStr(d);
          ReportResults.Descr := sJobDescr;
          FReturnDataSet.Last;
          QNbr := 0;
          ENbr := NULL;
          while not FReturnDataSet.Bof do
          begin
            if FReturnDataSet.EOF or (FReturnDataSet.FieldByName('CO_TAX_RETURN_QUEUE_NBR').Value <> QNbr) or
              (FReturnDataSet.FieldByName('EE_NBR').Value <> ENbr) then
            begin
              if VarIsNull(FReturnDataSet['SY_GL_AGENCY_REPORT_NBR']) then
               sReportDescr := DM_SYSTEM_MISC.SY_REPORTS.ShadowDataSet.CachedLookup(StrToIntDef(
                 DM_SYSTEM_MISC.SY_REPORTS_GROUP.ShadowDataSet.CachedLookup(FReturnDataSet['SY_REPORTS_GROUP_NBR'],
                 'SY_REPORTS_NBR'), 0), 'DESCRIPTION')+ '-'+ DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString
              else
               sReportDescr := DM_SYSTEM_MISC.SY_REPORTS_GROUP.ShadowDataSet.CachedLookup(StrToIntDef(
                 DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ShadowDataSet.CachedLookup(FReturnDataSet['SY_GL_AGENCY_REPORT_NBR'],
                 'SY_REPORTS_GROUP_NBR'), 0), 'NAME')+ '-'+ DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;

              if not FVmrReturnDataSet.FindKey([FormatDateTime('YYYYMMDD', d), sJobDescr, sReportDescr]) then
              begin
                DM_CLIENT.CL_BLOB.Close;
                DM_CLIENT.CL_BLOB.DataRequired('CL_BLOB_NBR='+ FReturnDataSet.FieldByName('CL_BLOB_NBR').AsString);
                with SubProcess do
                begin
                  TaxReturnVmrTag := MakeString([ctx_DataAccess.ClientID, CoNbr], ';');
                  VmrCoNbr := CoNbr;
                  VmrEventDate := d;
                  VmrConsolidation := FReturnDataSet['CL_CO_CONSOLIDATION_NBR'] <> null;
                end;
              end;
              QNbr := FReturnDataSet.FieldByName('CO_TAX_RETURN_QUEUE_NBR').Value;
              ENbr := FReturnDataSet.FieldByName('EE_NBR').Value;
            end;
            FReturnDataSet.Prior;
          end;
          if ReportResults.Count > 0 then
          begin
            tmpReportResults := TrwReportResults.Create;
            try
              tmpReportResults.PrinterName := ReportResults.PrinterName;
              tmpReportResults.Descr := ReportResults.Descr;

              // not consolidated
              for k := 0 to ReportResults.Count-1 do
                if not ReportResults[k].VmrConsolidation then
                begin
                  tmpResult := tmpReportResults.Add;
                  tmpResult.Assign(ReportResults[k]);
                  tmpResult.VmrConsolidation := ReportResults[k].VmrConsolidation;
                  tmpResult.TaxReturnVmrTag := ReportResults[k].TaxReturnVmrTag;
                end;

              if tmpReportResults.Count > 0 then
                ctx_VmrEngine.VmrStorePostProcessReportResult(tmpReportResults);

              // consolidated
              tmpReportResults.Clear;
              for k := 0 to ReportResults.Count-1 do
                if ReportResults[k].VmrConsolidation then
                begin
                  tmpResult := tmpReportResults.Add;
                  tmpResult.Assign(ReportResults[k]);
                  tmpResult.VmrConsolidation := ReportResults[k].VmrConsolidation;
                  tmpResult.TaxReturnVmrTag := ReportResults[k].TaxReturnVmrTag;
                end;

              if tmpReportResults.Count > 0 then
                ctx_VmrEngine.VmrStorePostProcessReportResult(tmpReportResults);
            finally
              FreeAndNil(tmpReportResults);
            end;
          end;
        finally
          ReportResults.Free;
        end;
        d := GetEndMonth(d+1);
      end;
      //
      ctx_UpdateWait('Processing payrolls');
      with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
      begin
        SetMacro('COLUMNS', 'distinct CAST(ConvertNullString(Trim(StrCopy(t1.DESCRIPTION, 218, 10)), ''0'') as INTEGER) PR_NBR');
        SetMacro('TABLE1', 'SB_MAIL_BOX_CONTENT');
        SetMacro('TABLE2', 'SB_MAIL_BOX');
        SetMacro('JOINFIELD', 'SB_MAIL_BOX');
        SetMacro('CONDITION', 't2.cl_nbr=:ClNbr');
        SetParam('ClNbr', ctx_DataAccess.ClientID);
        ctx_DataAccess.GetSbCustomData(FVmrPrDataSet, AsVariant);
        FVmrPrDataSet.IndexFieldNames := 'PR_NBR';
      end;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('COLUMNS', 'pr_nbr');
        SetMacro('TABLENAME', 'PR');
        SetMacro('CONDITION',
          ' CO_NBR = '+ IntToStr(CoNbr)+
          ' and CHECK_DATE between '+ QuotedStr(DateToStr(PeriodBeginDate))+ ' and '+ QuotedStr(DateToStr(PeriodEndDate))+
          ' and PAYROLL_TYPE in ('+ QuotedStr(PAYROLL_TYPE_REGULAR)+ ','+ QuotedStr(PAYROLL_TYPE_IMPORT)+ ')'+
          ' and STATUS = '+ QuotedStr(PAYROLL_STATUS_PROCESSED));
        ctx_DataAccess.GetCustomData(FPrDataSet, AsVariant);
        FPrDataSet.IndexFieldNames := 'PR_NBR';
      end;
      FPrDataSet.First;
      while not FPrDataSet.Eof do
      begin
        if not FVmrPrDataSet.FindKey([FPrDataSet.FieldByName('PR_NBR').AsInteger]) then
          ReRunPayrolReportsForVmr(FPrDataSet.FieldByName('PR_NBR').AsInteger);
        FPrDataSet.Next;
      end;
    finally
      ctx_EndWait;
      FPrDataSet.Free;
      FVmrPrDataSet.Free;
      FReturnDataSet.Free;
      FVmrReturnDataSet.Free;
    end;
  finally
    ctx_Security.DisableSystemAccountRights;
  end;
end;

procedure TevPayrollProcessing.ProcessPrenoteACH(
  ACHOptions: IevACHOptions; APrenoteCLAccounts: Boolean; ACHDataStream: IEvDualStream; out AchFile,
  AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String;
  out AExceptions: String; ACHFolder: String);
var
  ACH: TevAchPrenote;

begin

  AExceptions := '';
  AchFile := nil;
  AchRegularReport := nil;
  AchDetailedReport := nil;
  AchSave := nil;

  ACH := TevAchPrenote.Create(nil);
  try
    ACH.ACHFolder := ACHFolder;
    ACH.SetACHOptions(ACHOptions);
    ACH.PrenoteCLAccounts := APrenoteCLAccounts;
    ACHDataStream.Position := 0;
    ACH.Companies.LoadFromUniversalStream(ACHDataStream);

    try
      ACH.SaveACH;
      AchFileName := ACH.FileName;
      AchSave := ACH.ACHFiles;
    except
      on E: Exception do
        AExceptions := E.Message;
    end;

    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(True, True, ACH.FileSaved, ACH.FileName, ACH.ACHFolder, ACH.ACHFile, ACH.ACHDescription); // ACH File Report
    finally
      AchFile := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;

    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(False, False, ACH.FileSaved, ACH.FileName, ACH.ACHFolder, ACH.ACHFile, ACH.ACHDescription); // Regular ACH Report
    finally
      AchRegularReport := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;

    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(False, True, ACH.FileSaved, ACH.FileName, ACH.ACHFolder, ACH.ACHFile, ACH.ACHDescription); // Detailed ACH Report
    finally
      AchDetailedReport := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;

  finally
    ACH.Free;
  end;

end;

procedure TevPayrollProcessing.ProcessManualACH(ACHOptions: IevACHOptions;
  bPreprocess: Boolean; BatchBANK_REGISTER_TYPE: String; ACHDataStream: IEvDualStream; out AchFile,
  AchRegularReport, AchDetailedReport: IEvDualStream; out AchSave: IisListOfValues; out AchFileName: String;
  out AExceptions: String; ACHFolder: String);
var
  ACH: TevAchManual;
begin
  AExceptions := '';
  AchFile := nil;
  AchRegularReport := nil;
  AchDetailedReport := nil;
  AchSave := nil;

  ACH := TevAchManual.Create;
  try
    ACH.ACHFolder := ACHFolder;
    ACH.SetACHOptions(ACHOptions);
    ACHDataStream.Position := 0;
    ACH.Trans.LoadFromUniversalStream(ACHDataStream);
    ACH.OverrideBatchBANK_REGISTER_TYPE := BatchBANK_REGISTER_TYPE;
    try
      if bPreprocess then
      begin
        ACH.Preprocess;
      end
      else
      begin
        ACH.Process;
        AchFileName := ACH.FileName;
        AchSave := ACH.ACHFiles;
      end;
    except
      on E: Exception do
        AExceptions := E.Message;
    end;

    AExceptions := AExceptions + ACH.Exceptions;

    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(True, True, not bPreprocess, ACH.FileName, ACH.ACHFolder, ACH.ACHFile, 'Manual ACH'); // ACH File Report
    finally
      AchFile := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;

    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(False, False, not bPreprocess, ACH.FileName, ACH.ACHFolder, ACH.ACHFile, 'Manual ACH'); // Regular ACH Report
    finally
      AchRegularReport := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;

    ctx_RWLocalEngine.StartGroup;
    try
      GenerateAchReport(False, True, not bPreprocess, ACH.FileName, ACH.ACHFolder, ACH.ACHFile, 'Manual ACH'); // Detailed ACH Report
    finally
      AchDetailedReport := ctx_RWLocalEngine.EndGroup(rdtNone);
    end;

  finally
    ACH.Free;
  end;
end;

procedure TevPayrollProcessing.CopyPayroll(const APayrollNbr: Integer);
var
  Scheduled, tmpEXCLUDE_BILLING : String;
begin
  try
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    try
      with DM_PAYROLL do
      begin
        with ctx_DataAccess.CUSTOM_VIEW do
        begin
          if Active then
            Close;
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR');
            SetParam('RecordNbr', APayrollNbr);
            DataRequest(AsVariant);
          end;
          Open;
          First;

          if FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED then
            raise ECanNotCopyVoidedPayroll.CreateHelp('You can not copy voided payroll!', IDH_CanNotPerformOperation);

          if FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL then
            raise ECanNotCopyReversalPayroll.CreateHelp('You can not copy reversal payroll!', IDH_CanNotPerformOperation);

          CO.DataRequired('CO_NBR=' + FieldByName('CO_NBR').AsString);
          PR.DataRequired('PR_NBR=0');
          if not PR_SCHEDULED_EVENT.Active then
            PR_SCHEDULED_EVENT.Open;

          PR.Insert;
          PR.FieldByName('CHECK_DATE').Value := FieldByName('CHECK_DATE').Value;
          PR.FieldByName('CHECK_DATE_STATUS').Value := FieldByName('CHECK_DATE_STATUS').Value;
          PR.FieldByName('SCHEDULED').Value := 'N';
          Scheduled := FieldByName('SCHEDULED').Value;
          PR.FieldByName('PAYROLL_TYPE').Value := FieldByName('PAYROLL_TYPE').Value;
          PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING;
          PR.FieldByName('EXCLUDE_ACH').Value := FieldByName('EXCLUDE_ACH').Value;
          PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := FieldByName('EXCLUDE_TAX_DEPOSITS').Value;
          PR.FieldByName('EXCLUDE_AGENCY').Value := FieldByName('EXCLUDE_AGENCY').Value;
          PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := FieldByName('MARK_LIABS_PAID_DEFAULT').Value;
          PR.FieldByName('EXCLUDE_BILLING').Value := FieldByName('EXCLUDE_BILLING').Value;
          PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := FieldByName('EXCLUDE_R_C_B_0R_N').Value;
          PR.FieldByName('SCHEDULED_CALL_IN_DATE').Value := FieldByName('SCHEDULED_CALL_IN_DATE').Value;
          PR.FieldByName('ACTUAL_CALL_IN_DATE').Value := FieldByName('ACTUAL_CALL_IN_DATE').Value;
          PR.FieldByName('SCHEDULED_PROCESS_DATE').Value := FieldByName('SCHEDULED_PROCESS_DATE').Value;
          PR.FieldByName('SCHEDULED_DELIVERY_DATE').Value := FieldByName('SCHEDULED_DELIVERY_DATE').Value;
          PR.FieldByName('SCHEDULED_CHECK_DATE').Value := FieldByName('SCHEDULED_CHECK_DATE').Value;
          PR.FieldByName('INVOICE_PRINTED').Value := INVOICE_PRINT_STATUS_Pending;
          PR.FieldByName('PAYROLL_COMMENTS').Value := FieldByName('PAYROLL_COMMENTS').Value;
          PR.FieldByName('EXCLUDE_TIME_OFF').Value := FieldByName('EXCLUDE_TIME_OFF').Value;

          PR.FieldByName('TAX_IMPOUND').Value := FieldByName('TAX_IMPOUND').Value;
          PR.FieldByName('WC_IMPOUND').Value := FieldByName('WC_IMPOUND').Value;
          PR.FieldByName('BILLING_IMPOUND').Value := FieldByName('BILLING_IMPOUND').Value;
          PR.FieldByName('DD_IMPOUND').Value := FieldByName('DD_IMPOUND').Value;
          PR.FieldByName('TRUST_IMPOUND').Value := FieldByName('TRUST_IMPOUND').Value;

          PR.FieldByName('FILLER').Value := FieldByName('FILLER').Value;
          tmpEXCLUDE_BILLING := FieldByName('EXCLUDE_BILLING').AsString;


          with TExecDSWrapper.Create('NextRunNumberForCheckDate') do
          begin
            SetParam('Co_Nbr', FieldByName('CO_NBR').AsInteger);
            SetParam('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
            ctx_DataAccess.CUSTOM_VIEW.Close;
            DataRequest(AsVariant);
          end;
          Open;
          First;
          PR.FieldByName('RUN_NUMBER').Value := ConvertNull(FieldByName('max_run').Value, 0) + 1;
          Close;
        end;
        PR.Post;
        PR.Edit;
        PR.FieldByName('SCHEDULED').Value := Scheduled;
        PR.Post;

        if tmpEXCLUDE_BILLING = GROUP_BOX_CONDITIONAL then
        begin
          if ctx_DataAccess.CUSTOM_VIEW.Active then
            ctx_DataAccess.CUSTOM_VIEW.Close;
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_SERVICES');
            SetParam('RecordNbr', APayrollNbr);
            ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          end;
          ctx_DataAccess.CUSTOM_VIEW.Open;
          PR_SERVICES.Open;
          ctx_DataAccess.CUSTOM_VIEW.First;
          while not ctx_DataAccess.CUSTOM_VIEW.eof do
          begin
            PR_SERVICES.Insert;
            PR_SERVICES.FieldByName('PR_NBR').Value := PR.FieldByName('PR_NBR').AsInteger;
            PR_SERVICES.FieldByName('CO_SERVICES_NBR').Value := ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_SERVICES_NBR').Value;
            PR_SERVICES.FieldByName('EXCLUDE').Value := ctx_DataAccess.CUSTOM_VIEW.FieldByName('EXCLUDE').Value;
            PR_SERVICES.post;
            ctx_DataAccess.CUSTOM_VIEW.next;
          end;
          ctx_DataAccess.CUSTOM_VIEW.close
        end;

        ctx_DataAccess.PostDataSets([PR, PR_SCHEDULED_EVENT, PR_SERVICES]);

        EvCopyPayroll.CopyPayroll(APayrollNbr, PR.FieldByName('PR_NBR').AsInteger, False);

      end;
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;

  finally
    if DM_PAYROLL.PR.Active then
      DM_PAYROLL.PR.Close;
    if DM_PAYROLL.CO.Active then
      DM_PAYROLL.CO.Close;
    if DM_PAYROLL.PR_CHECK_LOCALS.Active then
      DM_PAYROLL.PR_CHECK_LOCALS.Close;
    if DM_PAYROLL.PR_CHECK_SUI.Active then
      DM_PAYROLL.PR_CHECK_SUI.Close;
    if DM_PAYROLL.PR_CHECK_STATES.Active then
      DM_PAYROLL.PR_CHECK_STATES.Close;
    if DM_PAYROLL.PR_CHECK_LINES.Active then
      DM_PAYROLL.PR_CHECK_LINES.Close;
  end;
end;

procedure TevPayrollProcessing.VoidPayroll(const APayrollNbr: Integer;const VoidTaxChecks, VoidAgencyChecks, VoidBillingChecks, PrintReports, DeleteInvoice, BlockBilling : Boolean);
var
  TempClientDataSet: TevClientDataSet;
  MyTransactionManager: TTransactionManager;
  TempType, Scheduled: String;
begin
// Void payroll
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  with DM_PAYROLL do
  try
    with ctx_DataAccess.CUSTOM_VIEW do
    begin
      if Active then
        Close;
      with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
      begin
        SetMacro('NbrField', 'PR');
        SetMacro('Columns', '*');
        SetMacro('TableName', 'PR');
        SetParam('RecordNbr', APayrollNbr);
        DataRequest(AsVariant);
      end;
      Open;
      First;

      CO.DataRequired('CO_NBR=' + FieldByName('CO_NBR').AsString);
      PR.DataRequired('PR_NBR=0');
      if not PR_SCHEDULED_EVENT.Active then
         PR_SCHEDULED_EVENT.Open;

      PR.Insert;
      PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
      PR.FieldByName('CHECK_DATE').Value := FieldByName('CHECK_DATE').Value;
      PR.FieldByName('CHECK_DATE_STATUS').Value := FieldByName('CHECK_DATE_STATUS').Value;
      PR.FieldByName('SCHEDULED').Value := 'N';
      Scheduled := FieldByName('SCHEDULED').Value;
      PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_REVERSAL;
      PR.FieldByName('STATUS').Value := PAYROLL_STATUS_HOLD;
      PR.FieldByName('EXCLUDE_ACH').Value := FieldByName('EXCLUDE_ACH').Value;
      PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := FieldByName('EXCLUDE_TAX_DEPOSITS').Value;
      PR.FieldByName('EXCLUDE_AGENCY').Value := FieldByName('EXCLUDE_AGENCY').Value;
      PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := FieldByName('MARK_LIABS_PAID_DEFAULT').Value;
      if BlockBilling then
        PR.FieldByName('EXCLUDE_BILLING').Value := 'Y'
      else
        PR.FieldByName('EXCLUDE_BILLING').Value := 'N';
      if PrintReports then
        PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := FieldByName('EXCLUDE_R_C_B_0R_N').Value
      else
      begin
        if FieldByName('EXCLUDE_R_C_B_0R_N').AsString[1] in [GROUP_BOX_NONE, GROUP_BOX_REPORTS] then
          PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_REPORTS;
        if FieldByName('EXCLUDE_R_C_B_0R_N').AsString[1] in [GROUP_BOX_CHECKS, GROUP_BOX_BOTH2] then
          PR.FieldByName('EXCLUDE_R_C_B_0R_N').Value := GROUP_BOX_BOTH2;
      end;
      PR.FieldByName('SCHEDULED_CALL_IN_DATE').Value := FieldByName('SCHEDULED_CALL_IN_DATE').Value;
      PR.FieldByName('ACTUAL_CALL_IN_DATE').Value := FieldByName('ACTUAL_CALL_IN_DATE').Value;
      PR.FieldByName('SCHEDULED_PROCESS_DATE').Value := FieldByName('SCHEDULED_PROCESS_DATE').Value;
      PR.FieldByName('SCHEDULED_DELIVERY_DATE').Value := FieldByName('SCHEDULED_DELIVERY_DATE').Value;
      PR.FieldByName('SCHEDULED_CHECK_DATE').Value := FieldByName('SCHEDULED_CHECK_DATE').Value;
      PR.FieldByName('INVOICE_PRINTED').Value := FieldByName('INVOICE_PRINTED').Value;
      PR.FieldByName('PAYROLL_COMMENTS').Value := FieldByName('PAYROLL_COMMENTS').Value;
      PR.FieldByName('EXCLUDE_TIME_OFF').Value := FieldByName('EXCLUDE_TIME_OFF').Value;

      PR.FieldByName('TAX_IMPOUND').Value := FieldByName('TAX_IMPOUND').Value;
      PR.FieldByName('WC_IMPOUND').Value := FieldByName('WC_IMPOUND').Value;
      PR.FieldByName('BILLING_IMPOUND').Value := FieldByName('BILLING_IMPOUND').Value;
      PR.FieldByName('DD_IMPOUND').Value := FieldByName('DD_IMPOUND').Value;
      PR.FieldByName('TRUST_IMPOUND').Value := FieldByName('TRUST_IMPOUND').Value;

      PR.FieldByName('FILLER').Value := PutIntoFiller(ConvertNull(FieldByName('FILLER').Value, ''), IntToStr(APayrollNbr), 1, 8);
      with TExecDSWrapper.Create('NextRunNumberForCheckDate') do
      begin
        SetParam('Co_Nbr', FieldByName('CO_NBR').AsInteger);
        SetParam('CheckDate', PR.FieldByName('CHECK_DATE').AsDateTime);
        ctx_DataAccess.CUSTOM_VIEW.Close;
        DataRequest(AsVariant);
      end;
      Open;
      First;
      PR.FieldByName('RUN_NUMBER').Value := ConvertNull(FieldByName('max_run').Value, 0) + 1;
      Close;
    end;
    PR.Post;
    PR.Edit;
    PR.FieldByName('SCHEDULED').Value := Scheduled;
    PR.Post;
    ctx_DataAccess.PostDataSets([PR, PR_SCHEDULED_EVENT]);

    SetPayrollLock(APayrollNbr, True);
    try
      EvCopyPayroll.CopyPayroll(APayrollNbr, PR.FieldByName('PR_NBR').AsInteger, True);
    finally
      SetPayrollLock(APayrollNbr, False);
    end;

    // rollback time off accrual
    VoidPrTimeOff(APayrollNbr, PR['CHECK_DATE'], PR['PR_NBR']);
    {with TExecDSWrapper.Create(';ExecProc') do
    begin
      SetMacro('PROCNAME', 'VOID_PR_TIME_OFF');
      SetMacro('INPUT', '(' + IntToStr(APayrollNbr) + ',' + IntToStr(CurrentUserNbr) + ')');
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;}

    if DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Active then
       DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Close;
//    if DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Active then
//       DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Close;

    with ctx_DataAccess.CUSTOM_VIEW do
    begin
// Void miscellaneous checks
      TempClientDataSet := TevClientDataSet.Create(nil);
      MyTransactionManager := TTransactionManager.Create(nil);
      try
        if VoidAgencyChecks or VoidTaxChecks or VoidBillingChecks then
        try
          ctx_DataAccess.UnlockPRSimple;
          if Active then
            Close;
          with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_MISCELLANEOUS_CHECKS');
            SetParam('RecordNbr', APayrollNbr);
            DataRequest(AsVariant);
          end;
          Open;
          TempClientDataSet.Data := Data;
          Close;
          PR_MISCELLANEOUS_CHECKS.Activate;
          MyTransactionManager.Initialize([PR_MISCELLANEOUS_CHECKS]);
          TempClientDataSet.First;
          while not TempClientDataSet.EOF do
          begin
            TempType := TempClientDataSet.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString;
            if ((TempType[1] = MISC_CHECK_TYPE_AGENCY) and VoidAgencyChecks)
              or ((TempType[1] = MISC_CHECK_TYPE_TAX) and VoidTaxChecks)
              or ((TempType[1] = MISC_CHECK_TYPE_BILLING) and VoidBillingChecks) then
            begin
              PR_MISCELLANEOUS_CHECKS.Insert;
              PR_MISCELLANEOUS_CHECKS.FieldByName('FILLER').Value :=
                PutIntoFiller(ConvertNull(PR_MISCELLANEOUS_CHECKS.FieldByName('FILLER').Value, ''),
                TempClientDataSet.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString, 1, 8);
              if (TempType[1] = MISC_CHECK_TYPE_AGENCY) and VoidAgencyChecks then
                PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_AGENCY_VOID;
              if (TempType[1] = MISC_CHECK_TYPE_TAX) and VoidTaxChecks then
                PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_TAX_VOID;
              if (TempType[1] = MISC_CHECK_TYPE_BILLING) and VoidBillingChecks then
                PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_BILLING_VOID;
              PR_MISCELLANEOUS_CHECKS.Post; // Before post event handler should populate the rest of the fields
            end;
            TempClientDataSet.Next;
          end;
          MyTransactionManager.ApplyUpdates;
        finally
          ctx_DataAccess.LockPRSimple;
        end;

// Delete Invoice
        if DeleteInvoice then
        begin
          DM_COMPANY.CO_BILLING_HISTORY.Activate;
          MyTransactionManager.Initialize([DM_COMPANY.CO_BILLING_HISTORY]);
          while DM_COMPANY.CO_BILLING_HISTORY.Locate('PR_NBR', APayrollNbr, []) do
            DM_COMPANY.CO_BILLING_HISTORY.Delete;
          MyTransactionManager.ApplyUpdates;
        end;

        SetPayrollLock(APayrollNbr, True);

        try
// Mark prior payroll as voided
          if Active then
            Close;
          with TExecDSWrapper.Create('pr;GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR');
            SetParam('RecordNbr', APayrollNbr);
            DataRequest(AsVariant);
          end;
          Open;
          First;
          MyTransactionManager.Initialize([ctx_DataAccess.CUSTOM_VIEW]);
          Edit;
          FieldByName('STATUS').AsString := PAYROLL_STATUS_VOIDED;
          Post;
          MyTransactionManager.ApplyUpdates;
  // Mark prior payroll checks as voided
          if Active then
            Close;
          with TExecDSWrapper.Create('pr_check;GenericSelectCurrentNBR') do
          begin
            SetMacro('NbrField', 'PR');
            SetMacro('Columns', '*');
            SetMacro('TableName', 'PR_CHECK');
            SetParam('RecordNbr', APayrollNbr);
            DataRequest(AsVariant);
          end;
          Open;
          First;
          MyTransactionManager.Initialize([ctx_DataAccess.CUSTOM_VIEW]);
          while not EOF do
          begin
            Edit;
            FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_VOID;
            Post;
            Next;
          end;
          MyTransactionManager.ApplyUpdates;
        finally
          SetPayrollLock(APayrollNbr, False);
        end;
      finally
        TempClientDataSet.Free;
        if Assigned(MyTransactionManager) then
          MyTransactionManager.Free;
      end;
    end;
// Clear Bank Account Register
    {DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_NBR=' + IntToStr(APayrollNbr));
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
    while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
    begin
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Cleared;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CLEARED_DATE').Value := Now;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);}
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  if DM_COMPANY.CO_BILLING_HISTORY.Active then
     DM_COMPANY.CO_BILLING_HISTORY.Close;
  if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Active then
     DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Close;
  if DM_PAYROLL.PR_CHECK_LOCALS.Active then
    DM_PAYROLL.PR_CHECK_LOCALS.Close;
  if DM_PAYROLL.PR_CHECK_SUI.Active then
    DM_PAYROLL.PR_CHECK_SUI.Close;
  if DM_PAYROLL.PR_CHECK_STATES.Active then
    DM_PAYROLL.PR_CHECK_STATES.Close;
  if DM_PAYROLL.PR_CHECK_LINES.Active then
    DM_PAYROLL.PR_CHECK_LINES.Close;
  if DM_PAYROLL.PR.Active then
    DM_PAYROLL.PR.Close;
  if DM_PAYROLL.CO.Active then
    DM_PAYROLL.CO.Close;
  if DM_PAYROLL.PR_SCHEDULED_EVENT.Active then
    DM_PAYROLL.PR_SCHEDULED_EVENT.Close;

//  DM_PAYROLL.PR.Open;
//  DM_PAYROLL.PR.Locate('PR_NBR', APayrollNbr, []);

end;

procedure TevPayrollProcessing.DeletePayroll(const APayrollNbr: Integer);

  procedure DeleteRelatedEeTimeOpers(const PrNbr: Integer);
  var
    ds: TevClientDataSet;
  begin
    ds := TevClientDataSet.Create(nil);
    try
      ds.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
      with TExecDSWrapper.Create('SelectPrRelatedEeTimeOpers') do
      begin
        SetParam('PrNbr', PrNbr);
        ds.DataRequest(AsVariant);
      end;
      ctx_DataAccess.OpenDataSets([ds]);
      ds.CustomProviderTableName := 'EE_TIME_OFF_ACCRUAL_OPER';
      ds.Filter := 'PR_NBR <> '+ IntToStr(PrNbr) + ' or PR_NBR IS NULL ';
      ds.Filtered := True;
      while ds.RecordCount > 0 do
        ds.Delete;
      ctx_DataAccess.PostDataSets([ds]);

      ds.CustomProviderTableName := 'EE_TIME_OFF_ACCRUAL_OPER';
      ds.Filtered := False;
      DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_NBR='+ IntToStr(PrNbr));
      while ds.RecordCount > 0 do
      begin
        if DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Locate('EE_TIME_OFF_ACCRUAL_OPER_NBR', DS['EE_TIME_OFF_ACCRUAL_OPER_NBR'], []) then
        begin
          if (DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.OPERATION_CODE.AsString <> EE_TOA_OPER_CODE_REQUEST_PENDING)
          and (DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.OPERATION_CODE.AsString <> EE_TOA_OPER_CODE_REQUEST_APPROVED)
          and (DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.OPERATION_CODE.AsString <> EE_TOA_OPER_CODE_REQUEST_DENIED) then
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Delete
          else
          begin
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := Null;
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := Null;
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := Null;
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
          end
        end
        else
          Assert(False);
        ds.Delete;
      end;
      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER]);
    finally
      ds.Free;
    end;
  end;

var
  S : String;
  CalcDueDates: TevCalcDueDates;
  Q: IevQuery;
begin
  DM_PAYROLL.PR.DataRequired('PR_NBR=' + IntToStr(APayrollNbr));
  ctx_DataAccess.UnlockPR;
  try
    if not (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1]
                 in [PAYROLL_TYPE_SETUP, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT,
                     PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT,
                     PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT, PAYROLL_TYPE_IMPORT]) then
    Begin
      CalcDueDates := TevCalcDueDates.Create;
      try
        CalcDueDates.Calculate(-1, DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime, S,
                                 APayrollNbr, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      finally
        FreeAndNil(CalcDueDates);
      end;
    end;

    if ((DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_REGULAR, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT]) and
       (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])) then
    begin
      DM_CLIENT.CO_QEC_RUN.DataRequired('CO_NBR = ' + DM_PAYROLL.PR.FieldByName('CO_NBR').AsString +
                                        ' and QUARTER_BEGIN_DATE =''' + DateToStr(GetBeginQuarter(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime)) + '''');
      if DM_CLIENT.CO_QEC_RUN.RecordCount > 0 then
      begin
         DM_CLIENT.CO_QEC_RUN.Delete;
         ctx_DataAccess.PostDataSets([DM_CLIENT.CO_QEC_RUN]);
      end;
    end;

    DeleteRelatedEeTimeOpers(APayrollNbr);
    DM_PAYROLL.PR.Delete;
    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
    begin
      Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
      Q.Execute;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.LockPR(False);
    raise;
  end;
end;

function TevPayrollProcessing.DeleteBatch(const ABatchNbr: Integer): Boolean;
var
  Q: IevQuery;
begin
  result:= false;
  Assert(ABatchNbr > 0, 'Batch number has to be bigger than zero.');
  DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR=' + IntToStr(ABatchNbr));
  if DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger > 0 then
  begin
      DM_PAYROLL.PR.DataRequired('PR_NBR=' + IntToStr(DM_PAYROLL.PR_BATCH.FieldByName('PR_NBR').AsInteger));

      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
      begin
        ctx_DBAccess.StartTransaction([dbtClient]);
        try
          Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
          Q.Execute;
          DM_PAYROLL.PR_BATCH.Delete;
          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, DM_PAYROLL.PR_BATCH]);
          ctx_DBAccess.CommitTransaction;
          result:= True;
        except
          ctx_DBAccess.RollbackTransaction;
          raise;
        end;
      end;
  end;

end;

procedure TevPayrollProcessing.SetPayrollLock(const APrNbr: Integer; const AUnlocked: Boolean);
var
  T: IevTable;
  Q: IevQuery;
begin
  ctx_DBAccess.StartTransaction([dbtClient]);
  try
    Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
    Q.Execute;
    try
      T := TevTable.Create('PR', 'unlocked', 'pr_nbr = ' + IntToStr(aPrNbr));
      T.Open;

      if T.RecordCount = 1 then
      begin
        T.Edit;
        if aUnlocked then
          T.FieldByName('unlocked').AsString := 'Y'
        else
          T.FieldByName('unlocked').AsString := 'N';
        T.Post;
        T.SaveChanges;
      end;
    finally
      Q := TevQuery.Create('{ClearTransactionVar<CL_, PR_LOCK_CHANGE>}', False);
      Q.Execute;
    end;

    ctx_DBAccess.CommitTransaction;
  except
    ctx_DBAccess.RollbackTransaction;
    raise;
  end;
end;

function TevPayrollProcessing.GrossToNet(var Warnings: String;
  CheckNumber: Integer; ApplyUpdatesInTransaction, JustPreProcess,
  FetchPayrollData, SecondCheck: Boolean): Boolean;
begin
  Result := Context.PayrollCalculation.GrossToNet(Warnings, CheckNumber, ApplyUpdatesInTransaction, JustPreProcess, FetchPayrollData, SecondCheck);
end;

function TevPayrollProcessing.VoidCheck(CheckToVoid: Integer; PrBatchNbr: Integer): Integer;
var
  TempDataSet, TEMP_PR_CHECK_LINE_LOCALS: TevClientDataSet;
  I, PrToVoid, CurrentPrNbr, EeNbr, NewCheckNbr: Integer;
  AllOK, WasManualCheck, Was3rdPartyCheck: Boolean;
  Q: IevQuery;
begin

  if ctx_AccountRights.Functions.GetState('FIELDS_RATE_OF_') <> stEnabled then
    raise ENoRights.CreateHelp('You have no rights to rates and wages and hence cannot void a check.', IDH_SecurityViolation);
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    Q := TevQuery.Create('select PR_NBR from PR_BATCH where PR_BATCH_NBR='+IntToStr(PrBatchNbr));
    CurrentPrNbr := Q.Result['PR_NBR'];

    Q := TevQuery.Create('select EE_NBR from PR_CHECK where PR_CHECK_NBR='+IntToStr(CheckToVoid));
    EeNbr := Q.Result['EE_NBR'];

    DM_CLIENT.PR.DataRequired('PR_NBR='+IntToStr(CurrentPrNbr));
    DM_CLIENT.PR_BATCH.DataRequired('PR_BATCH_NBR='+IntToStr(PrBatchNbr));

    DM_CLIENT.PR_CHECK.DataRequired('PR_CHECK_NBR=0');

    DM_PAYROLL.PR_CHECK.Insert;
    DM_PAYROLL.PR_CHECK['PR_NBR'] := CurrentPrNbr;
    DM_PAYROLL.PR_CHECK['EE_NBR'] := EeNbr;
    DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'] := PrBatchNbr;
    DM_PAYROLL.PR_CHECK['CHECK_TYPE'] := CHECK_TYPE2_VOID;
    DM_PAYROLL.PR_CHECK['FILLER'] := PutIntoFiller(ConvertNull(DM_PAYROLL.PR_CHECK['FILLER'], ''), IntToStr(CheckToVoid), 1, 8);
    DM_PAYROLL.PR_CHECK.Post;

    Result := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
    NewCheckNbr := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];

    ctx_DataAccess.UnlockPRSimple;
    TempDataSet := TevClientDataSet.Create(nil);

    Q := TevQuery.Create('GenericSelectCurrentNBR');
    Q.Macro['NbrField'] := 'PR_CHECK';
    Q.Macro['Columns'] := '*';
    Q.Macro['TableName'] := 'PR_CHECK';
    Q.Param['RecordNbr'] := CheckToVoid;
    PrToVoid := Q.Result['PR_NBR'];

    WasManualCheck := Q.Result.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL;
    Was3rdPartyCheck := Q.Result.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_3RD_PARTY;
    try
      DM_CLIENT.PR_CHECK.Edit;
// Copy Check
      TempDataSet.Data := Q.Result.Data;

      for I := 0 to TempDataSet.FieldCount - 1 do
      if (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') and
      (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE') and
      (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD') and
      (TempDataSet.Fields[I].FieldName <> 'NOTES_NBR') and
      (TempDataSet.Fields[I].FieldName <> 'DATA_NBR') and
      (TempDataSet.Fields[I].FieldName <> 'CHECK_TYPE') and (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
      (TempDataSet.Fields[I].FieldName <> 'PR_BATCH_NBR') and (TempDataSet.Fields[I].FieldName <> 'FILLER') then
      begin
        if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) or (TempDataSet.Fields[I].FieldName = 'EE_OASDI_TAXABLE_TIPS')
        or (TempDataSet.Fields[I].FieldName = 'ER_OASDI_TAXABLE_TIPS') or (TempDataSet.Fields[I].FieldName = 'OR_CHECK_BACK_UP_WITHHOLDING') then
          DM_CLIENT.PR_CHECK[TempDataSet.Fields[I].FieldName] := - TempDataSet.Fields[I].Value
        else
          DM_CLIENT.PR_CHECK[TempDataSet.Fields[I].FieldName] := TempDataSet.Fields[I].Value;
      end;
      DM_CLIENT.PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_VOID;
      DM_CLIENT.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
      DM_CLIENT.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] := DM_CLIENT.PR_CHECK['FEDERAL_TAX'];
      DM_CLIENT.PR_CHECK['OR_CHECK_OASDI'] := DM_CLIENT.PR_CHECK['EE_OASDI_TAX'];
      DM_CLIENT.PR_CHECK['OR_CHECK_MEDICARE'] := DM_CLIENT.PR_CHECK['EE_MEDICARE_TAX'];
      DM_CLIENT.PR_CHECK['OR_CHECK_EIC'] := DM_CLIENT.PR_CHECK['EE_EIC_TAX'];
// Deal with Check shortfalls
      if DM_COMPANY.CO['MAKE_UP_TAX_DEDUCT_SHORTFALLS'] = 'Y' then
      begin
        Q := TevQuery.Create('CheckFederalShortfalls');
        Q.Param['EENbr'] := DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').AsInteger;
        Q.Param['CheckToVoid'] := CheckToVoid;
        Q.Result.Last;
        DM_CLIENT.PR_CHECK['FEDERAL_SHORTFALL'] := Q.Result['FEDERAL_SHORTFALL'] - DM_CLIENT.PR_CHECK['FEDERAL_SHORTFALL'];
        Q.Result.Prior;
        DM_CLIENT.PR_CHECK['FEDERAL_SHORTFALL'] := Q.Result['FEDERAL_SHORTFALL'] - DM_CLIENT.PR_CHECK['FEDERAL_SHORTFALL'];
      end;
      DM_CLIENT.PR_CHECK.Post;

      begin
        TEMP_PR_CHECK_LINE_LOCALS := TevClientDataSet.Create(Nil);
        try
  // Get PR_CHECK_LINE_LOCALS
          DM_CLIENT.PR_CHECK_LINE_LOCALS.DataRequired('PR_CHECK_LINES_NBR=0');
          Q := TevQuery.Create('GenericSelect2CurrentNbr');
          Q.Macro['Columns'] := 'T1.*';
          Q.Macro['TABLE1'] := 'PR_CHECK_LINE_LOCALS';
          Q.Macro['TABLE2'] := 'PR_CHECK_LINES';
          Q.Macro['NBRFIELD'] := 'PR_CHECK';
          Q.Macro['JOINFIELD'] := 'PR_CHECK_LINES';
          Q.Param['RecordNbr'] := CheckToVoid;
          TEMP_PR_CHECK_LINE_LOCALS.Data := Q.Result.Data;

  // Copy Check States
          Q := TevQuery.Create('GenericSelectCurrentNBR');
          Q.Macro['NbrField'] := 'PR_CHECK';
          Q.Macro['Columns'] := '*';
          Q.Macro['TableName'] := 'PR_CHECK_STATES';
          Q.Param['RecordNbr'] := CheckToVoid;
          TempDataSet.Data := Q.Result.Data;

          TempDataSet.First;
          DM_CLIENT.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + IntToStr(NewCheckNbr));
          while not TempDataSet.Eof do
          begin
            DM_CLIENT.PR_CHECK_STATES.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_STATES_NBR') then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) then
                DM_CLIENT.PR_CHECK_STATES[TempDataSet.Fields[I].FieldName] := -TempDataSet.Fields[I].Value
              else
                DM_CLIENT.PR_CHECK_STATES[TempDataSet.Fields[I].FieldName] := TempDataSet.Fields[I].Value;
            end;
            DM_CLIENT.PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
            DM_CLIENT.PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := DM_CLIENT.PR_CHECK_STATES['STATE_TAX'];
            DM_CLIENT.PR_CHECK_STATES['EE_SDI_OVERRIDE'] := DM_CLIENT.PR_CHECK_STATES['EE_SDI_TAX'];
  // Deal with Check States shortfalls
            if DM_COMPANY.CO['MAKE_UP_TAX_DEDUCT_SHORTFALLS'] = 'Y' then
            begin
              Q := TevQuery.Create('CheckStatesShortfalls');
              Q.Param['EEStatesNbr'] := DM_CLIENT.PR_CHECK_STATES.FieldByName('EE_STATES_NBR').AsInteger;
              Q.Param['CheckToVoid'] := CheckToVoid;
              Q.Result.Last;
              DM_CLIENT.PR_CHECK_STATES['STATE_SHORTFALL'] := Q.Result['STATE_SHORTFALL'] - DM_CLIENT.PR_CHECK_STATES['STATE_SHORTFALL'];
              DM_CLIENT.PR_CHECK_STATES['EE_SDI_SHORTFALL'] := Q.Result['EE_SDI_SHORTFALL'] - DM_CLIENT.PR_CHECK_STATES['EE_SDI_SHORTFALL'];
              Q.Result.Prior;
              DM_CLIENT.PR_CHECK_STATES['STATE_SHORTFALL'] := Q.Result['STATE_SHORTFALL'] - DM_CLIENT.PR_CHECK_STATES['STATE_SHORTFALL'];
              DM_CLIENT.PR_CHECK_STATES['EE_SDI_SHORTFALL'] := Q.Result['EE_SDI_SHORTFALL'] - DM_CLIENT.PR_CHECK_STATES['EE_SDI_SHORTFALL'];
            end;
            DM_CLIENT.PR_CHECK_STATES['PR_CHECK_NBR'] := NewCheckNbr;
            DM_CLIENT.PR_CHECK_STATES.Post;
            TempDataSet.Next;
          end;

  // Copy Check Locals
          Q := TevQuery.Create('GenericSelectCurrentNBR');
          Q.Macro['NbrField'] := 'PR_CHECK';
          Q.Macro['Columns'] := '*';
          Q.Macro['TableName'] := 'PR_CHECK_LOCALS';
          Q.Param['RecordNbr'] := CheckToVoid;
          TempDataSet.Data := Q.Result.Data;

          TempDataSet.First;
          DM_CLIENT.PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + IntToStr(NewCheckNbr));
          while not TempDataSet.Eof do
          begin
            DM_CLIENT.PR_CHECK_LOCALS.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_LOCALS_NBR') then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) then
                DM_CLIENT.PR_CHECK_LOCALS[TempDataSet.Fields[I].FieldName] := -TempDataSet.Fields[I].Value
              else
                DM_CLIENT.PR_CHECK_LOCALS[TempDataSet.Fields[I].FieldName] := TempDataSet.Fields[I].Value;
            end;
            DM_CLIENT.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := DM_CLIENT.PR_CHECK_LOCALS['LOCAL_TAX'];
  // Deal with Check Locals shortfalls
            if DM_COMPANY.CO['MAKE_UP_TAX_DEDUCT_SHORTFALLS'] = 'Y' then
            begin
              Q := TevQuery.Create('CheckLocalsShortfalls');
              Q.Param['EELocalsNbr'] := DM_CLIENT.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger;
              Q.Param['CheckToVoid'] := CheckToVoid;
              Q.Result.Last;
              DM_CLIENT.PR_CHECK_LOCALS['LOCAL_SHORTFALL'] := Q.Result['LOCAL_SHORTFALL'] - DM_CLIENT.PR_CHECK_LOCALS['LOCAL_SHORTFALL'];
              Q.Result.Prior;
              DM_CLIENT.PR_CHECK_LOCALS['LOCAL_SHORTFALL'] := Q.Result['LOCAL_SHORTFALL'] - DM_CLIENT.PR_CHECK_LOCALS['LOCAL_SHORTFALL'];
            end;
            DM_CLIENT.PR_CHECK_LOCALS['PR_CHECK_NBR'] := NewCheckNbr;
            DM_CLIENT.PR_CHECK_LOCALS.Post;
            TempDataSet.Next;
          end;

  // Copy Check SUI
          Q := TevQuery.Create('GenericSelectCurrentNBR');
          Q.Macro['NbrField'] := 'PR_CHECK';
          Q.Macro['Columns'] := '*';
          Q.Macro['TableName'] := 'PR_CHECK_SUI';
          Q.Param['RecordNbr'] := CheckToVoid;
          TempDataSet.Data := Q.Result.Data;

          TempDataSet.First;

          DM_CLIENT.PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + IntToStr(NewCheckNbr));
          while not TempDataSet.EOF do
          begin
            DM_CLIENT.PR_CHECK_SUI.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_SUI_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') and (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and
              (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE') and (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD') then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) then
                DM_CLIENT.PR_CHECK_SUI[TempDataSet.Fields[I].FieldName] := -TempDataSet.Fields[I].Value
              else
                DM_CLIENT.PR_CHECK_SUI[TempDataSet.Fields[I].FieldName] := TempDataSet.Fields[I].Value;
            end;
            DM_CLIENT.PR_CHECK_SUI['OVERRIDE_AMOUNT'] := DM_CLIENT.PR_CHECK_SUI['SUI_TAX'];
            DM_CLIENT.PR_CHECK_SUI['PR_CHECK_NBR'] := NewCheckNbr;
            DM_CLIENT.PR_CHECK_SUI.Post;
            TempDataSet.Next;
          end;

  // Copy Check Lines
          DM_CLIENT.CL_E_DS.DataRequired('ALL');
          Q := TevQuery.Create('GenericSelectCurrentNBR');
          Q.Macro['NbrField'] := 'PR_CHECK';
          Q.Macro['Columns'] := '*';
          Q.Macro['TableName'] := 'PR_CHECK_LINES';
          Q.Param['RecordNbr'] := CheckToVoid;
          TempDataSet.Data := Q.Result.Data;

          TempDataSet.First;
          DM_CLIENT.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + IntToStr(NewCheckNbr));
          while not TempDataSet.Eof do
          begin
            DM_CLIENT.PR_CHECK_LINES.Insert;
            for I := 0 to TempDataSet.FieldCount - 1 do
            if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_LINES_NBR') and (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') and
              (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE') and
              (TempDataSet.Fields[I].FieldName <> 'CO_DIVISION_NBR') and (TempDataSet.Fields[I].FieldName <> 'CO_BRANCH_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'CO_DEPARTMENT_NBR') and (TempDataSet.Fields[I].FieldName <> 'CO_TEAM_NBR') and
              (TempDataSet.Fields[I].FieldName <> 'PR_CHECK_NBR') and (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD')  then
            begin
              if ThisIsTaxOrWages(TempDataSet.Fields[I].FieldName) or (TempDataSet.Fields[I].FieldName = 'HOURS_OR_PIECES')
                or (TempDataSet.Fields[I].FieldName = 'AMOUNT') or (TempDataSet.Fields[I].FieldName = 'E_D_DIFFERENTIAL_AMOUNT') then
                DM_CLIENT.PR_CHECK_LINES[TempDataSet.Fields[I].FieldName] := -TempDataSet.Fields[I].Value
              else
                DM_CLIENT.PR_CHECK_LINES[TempDataSet.Fields[I].FieldName] := TempDataSet.Fields[I].Value;
            end;
            DM_CLIENT.PR_CHECK_LINES['CO_DIVISION_NBR'] := TempDataSet.FieldByName('CO_DIVISION_NBR').Value;
            DM_CLIENT.PR_CHECK_LINES['CO_BRANCH_NBR'] := TempDataSet.FieldByName('CO_BRANCH_NBR').Value;
            DM_CLIENT.PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := TempDataSet.FieldByName('CO_DEPARTMENT_NBR').Value;
            DM_CLIENT.PR_CHECK_LINES['CO_TEAM_NBR'] := TempDataSet.FieldByName('CO_TEAM_NBR').Value;
            DM_CLIENT.PR_CHECK_LINES['WORK_ADDRESS_OVR'] := TempDataSet.FieldByName('WORK_ADDRESS_OVR').Value;

            if DM_CLIENT.PR_CHECK_LINES['AGENCY_STATUS'] = CHECK_LINE_AGENCY_STATUS_SENT then
            begin
              DM_CLIENT.PR_CHECK_LINES['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_PENDING;
              DM_CLIENT.PR_CHECK_LINES['PR_MISCELLANEOUS_CHECKS_NBR'] := Null;
            end;
  // Deal with Check Lines Deduction shortfalls
            if DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_CLIENT.PR_CHECK_LINES['CL_E_DS_NBR'], 'MAKE_UP_DEDUCTION_SHORTFALL') = 'Y' then
            begin
              Q := TevQuery.Create('CheckDeductionShortfalls');
              Q.Param['EENbr'] := DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').AsInteger;
              Q.Param['EDNbr'] := DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger;
              Q.Param['CheckToVoid'] := CheckToVoid;
              Q.Result.Last;
              DM_CLIENT.PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := Q.Result['DEDUCTION_SHORTFALL_CARRYOVER'] - DM_CLIENT.PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'];
              Q.Result.Prior;
              DM_CLIENT.PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'] := Q.Result['DEDUCTION_SHORTFALL_CARRYOVER'] - DM_CLIENT.PR_CHECK_LINES['DEDUCTION_SHORTFALL_CARRYOVER'];
            end;
            DM_CLIENT.PR_CHECK_LINES['PR_CHECK_NBR'] := NewCheckNbr;
            DM_CLIENT.PR_CHECK_LINES.Post;
  // Copy Check Line Locals
            TEMP_PR_CHECK_LINE_LOCALS.Filter := 'PR_CHECK_LINES_NBR=' + TempDataSet.FieldByName('PR_CHECK_LINES_NBR').AsString;
            TEMP_PR_CHECK_LINE_LOCALS.Filtered := True;
            TEMP_PR_CHECK_LINE_LOCALS.First;
            while not TEMP_PR_CHECK_LINE_LOCALS.EOF do
            begin
              DM_CLIENT.PR_CHECK_LINE_LOCALS.Insert;
              for I := 0 to TEMP_PR_CHECK_LINE_LOCALS.FieldCount - 1 do
              begin
                if (TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName <> 'PR_CHECK_LINES_NBR') and
                   (TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName <> 'PR_CHECK_LINE_LOCALS_NBR') and
                   Assigned(DM_CLIENT.PR_CHECK_LINE_LOCALS.FindField(TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName)) then
                  DM_CLIENT.PR_CHECK_LINE_LOCALS[TEMP_PR_CHECK_LINE_LOCALS.Fields[I].FieldName] := TEMP_PR_CHECK_LINE_LOCALS.Fields[I].Value;
              end;
              DM_CLIENT.PR_CHECK_LINE_LOCALS.Post;
              TEMP_PR_CHECK_LINE_LOCALS.Next;
            end;
            TEMP_PR_CHECK_LINE_LOCALS.Filtered := False;
  // End Copy Check Line Locals
            TempDataSet.Next;
          end;
          ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK, DM_CLIENT.PR_CHECK_LINES, DM_CLIENT.PR_CHECK_STATES,
            DM_CLIENT.PR_CHECK_SUI, DM_CLIENT.PR_CHECK_LOCALS, DM_CLIENT.PR_CHECK_LINE_LOCALS]);
          if WasManualCheck or Was3rdPartyCheck then
          begin
            DM_CLIENT.PR_CHECK.Edit;
            if WasManualCheck then
              DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_MANUAL;
            if Was3rdPartyCheck then
              DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_3RD_PARTY;
            ctx_DataAccess.NewCheckInserted := True;
            try
              DM_CLIENT.PR_CHECK.Post;
            finally
              ctx_DataAccess.NewCheckInserted := False;
            end;
            ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK]);
          end
          else
          begin
  // Void prior check
            DM_CLIENT.PR_CHECK.DataRequired('PR_CHECK_NBR='+IntToStr(CheckToVoid));
            DM_CLIENT.PR.DataRequired('PR_NBR=' + IntToStr(PrToVoid));

            ctx_DataAccess.UnlockPR;
            AllOK := False;
            DM_CLIENT.PR_CHECK.Edit;
            DM_CLIENT.PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_VOID;
            DM_CLIENT.PR_CHECK.Post;
            try
              ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK]);
              AllOK := True;
            finally
              ctx_DataAccess.LockPR(AllOK);
            end;
          end;
          ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK]);
        finally
          TEMP_PR_CHECK_LINE_LOCALS.Free;
        end;
      end;
      ctx_DataAccess.CommitNestedTransaction;
    finally
      TempDataSet.Free;
      ctx_DataAccess.LockPRSimple;
    end;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

function TevPayrollProcessing.GetPayrollBatchDefaults(
  const PrNbr: Integer): IisListOfValues;
var
  i: Integer;
  b: Boolean;
begin
  Result := TisListOfValues.Create;

  DM_CLIENT.PR.DataRequired('PR_NBR=' + IntToStr(PrNbr));
  DM_COMPANY.CO.DataRequired('CO_NBR=' + DM_CLIENT.PR.CO_NBR.AsString);
  DM_CLIENT.PR_BATCH.DataRequired('PR_NBR=' + IntToStr(PrNbr) + ' AND PR_BATCH_NBR=0');
  DM_CLIENT.PR_BATCH.Insert;

  with ctx_PayrollCalculation do
  begin
    if DM_PAYROLL.PR.FieldByName('SCHEDULED').Value = 'Y' then
    begin
      b := FindNextPeriodBeginEndDate;
      if DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecordCount > 0 then
      begin
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
        with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
        try
          DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Strings[0];
        finally
          Free;
        end;
      end
      else
        DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);

      if not b then
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := NextPeriodBeginDate;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := NextPeriodEndDate;
      end
      else
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE'];
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE'];
      end;
    end
    else
    begin
      DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := NextPeriodBeginDate;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := NextPeriodEndDate;
    end;
    DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
    DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;
  end;

  for i := 0 to DM_CLIENT.PR_BATCH.FieldCount - 1 do
    Result.AddValue(DM_CLIENT.PR_BATCH.Fields[i].FieldName, DM_CLIENT.PR_BATCH.Fields[i].Value);

  DM_CLIENT.PR_BATCH.Cancel;
  DM_CLIENT.PR_BATCH.MergeChangeLog;

end;


procedure TevPayrollProcessing.RefreshSchduledEDsBatch(
  const BatchNbr: Integer; const RegularChecksOnly: Boolean);
var
  NbrOfCheckLines: Integer;
  iGenNbr, k: Integer;
  Q: IevQuery;
  SecRecInfo: IevSecRecordInfo;
  s, SQL: String;

  function CreateInterbaseFilter(const FieldName: string; OriginalCondition: string): string;
  var
    i: Integer;
    s: string;
  begin
    Assert(Length(OriginalCondition) > 0);
    Result := '';
    while (Length(OriginalCondition) > 0) do
    begin
      s := '';
      i := 0;
      while (Length(OriginalCondition) > 0) and (i < 1000) do
      begin
        if s = '' then
          s := GetNextStrValue(OriginalCondition, ',')
        else
          s := s+ ','+ GetNextStrValue(OriginalCondition, ',');
        Inc(i);
      end;
      if Result = '' then
        Result := FieldName+ ' not in ('+ s+ ')'
      else
        Result := Result+ ' and '+ FieldName+ ' not in ('+ s+ ')'
    end;
    Result := '('+ Result+ ')';
  end;

begin

  DM_CLIENT.PR_CHECK.DataRequired('PR_BATCH_NBR='+IntToStr(BatchNbr));
  DM_CLIENT.PR_BATCH.DataRequired('PR_BATCH_NBR='+IntToStr(BatchNbr));
  DM_CLIENT.PR.DataRequired('PR_NBR='+DM_CLIENT.PR_BATCH.PR_NBR.AsString);   

  s := ' ';
  for k := 0 to ctx_AccountRights.Records.Count - 1 do
  begin
    SecRecInfo := ctx_AccountRights.Records.GetSecRecordInfo(k);
    if (SecRecInfo.DBType = dbtClient) and (SecRecInfo.ClientID = ctx_DBAccess.CurrentClientNbr) then
    begin
      if (SecRecInfo.FilterType = 'CO') then
        s := s + '(' + CreateInterbaseFilter('EE.CO_NBR', SecRecInfo.Filter)+ ' or EE.CO_NBR is null) and '
      else if (SecRecInfo.FilterType = 'DV') then
        s := s + '(' + CreateInterbaseFilter('EE.CO_DIVISION_NBR', SecRecInfo.Filter)+ ' or EE.CO_DIVISION_NBR is null) and '
      else if (SecRecInfo.FilterType = 'BR') then
        s := s + '(' + CreateInterbaseFilter('EE.CO_BRANCH_NBR', SecRecInfo.Filter)+ ' or EE.CO_BRANCH_NBR is null) and '
      else if (SecRecInfo.FilterType = 'DP') then
        s := s + '(' + CreateInterbaseFilter('EE.CO_DEPARTMENT_NBR', SecRecInfo.Filter)+ ' or EE.CO_DEPARTMENT_NBR is null) and '
      else if (SecRecInfo.FilterType = 'TM') then
        s := s + '(' + CreateInterbaseFilter('EE.CO_TEAM_NBR', SecRecInfo.Filter)+ ' or EE.CO_TEAM_NBR is null) and ';
    end;
  end;

  SQL := 'select PR_CHECK_NBR, CHECK_TYPE from PR_CHECK where PR_BATCH_NBR='+IntToStr(BatchNbr)+' and {AsOfNow<PR_CHECK>}';

  if Trim(s) <> '' then
    SQL := SQL + ' and EE_NBR in (select EE_NBR from EE where CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString+' and '+s+' {AsOfNow<EE>})';

  Q := TevQuery.Create(SQL);

  DM_CLIENT.CO.DataRequired('CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString);
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString);
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString);
  DM_CLIENT.PR_SCHEDULED_E_DS.DataRequired('PR_NBR='+DM_CLIENT.PR_CHECK.PR_NBR.AsString);

  DM_SYSTEM.SY_FED_TAX_TABLE.DataRequired('');
  DM_SYSTEM.SY_STATES.DataRequired('');
  DM_CLIENT.CL_PENSION.DataRequired('');
  DM_CLIENT.CL_E_DS.DataRequired('');
  DM_CLIENT.EE.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_CLIENT.EE_SCHEDULED_E_DS.DataRequired('');
  DM_CLIENT.EE_STATES.DataRequired('');
  DM_CLIENT.CL_PERSON.DataRequired('');

  NbrOfCheckLines := 1;

  ctx_DataAccess.Validate := False;
  DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
  DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;
  DM_EMPLOYEE.EE.LookupsEnabled := False;
  DM_EMPLOYEE.EE_RATES.LookupsEnabled := False;

  if Q.Result.RecordCount > 0 then
  try
    ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger);

    if not RegularChecksOnly then
    begin
      Q.Result.First;
      while not Q.Result.EOF do
      begin
        if Q.Result['CHECK_TYPE'] = CHECK_TYPE2_MANUAL then
          VerifyManualCheckChange(DM_PAYROLL.PR.FieldByName('PR_NBR').AsString, Q.Result.FieldByName('PR_CHECK_NBR').AsString);
        Q.Result.Next;
      end;
    end;

    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
    begin
      SetMacro('Columns', 'T1.*');
      SetMacro('TABLE1', DM_PAYROLL.PR_CHECK_LINES.Name);
      SetMacro('TABLE2', 'PR_CHECK');
      SetMacro('NBRFIELD', 'PR_BATCH');
      SetMacro('JOINFIELD', 'PR_CHECK');
      SetParam('RecordNbr', BatchNbr);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    DM_PAYROLL.PR_CHECK_LINES.ClientID := ctx_DataAccess.ClientID;
    DM_PAYROLL.PR_CHECK_LINES.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
    DM_PAYROLL.PR_CHECK_LINES.SetOpenCondition('PR_BATCH_NBR = ' + IntToStr(BatchNbr));

    DM_PAYROLL.PR_CHECK_LINES.First;
    while not DM_PAYROLL.PR_CHECK_LINES.EOF do
    begin
      if (DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_SCHEDULED)
      or (DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_DISTR_SCHEDULED) then
      begin
        if Q.Result.Locate('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'], [])
        and (Q.Result.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_MANUAL]) then
        begin
          DM_PAYROLL.PR_CHECK_LINES.Delete;
          Inc(NbrOfCheckLines);
        end
        else
          DM_PAYROLL.PR_CHECK_LINES.Next;
      end
      else
        DM_PAYROLL.PR_CHECK_LINES.Next;
    end;

    iGenNbr := ctx_DBAccess.GetGeneratorValue('G_PR_CHECK_LINES', dbtClient, NbrOfCheckLines);
    ctx_DataAccess.NextInternalCheckLineNbr := iGenNbr - NbrOfCheckLines + 1;
    ctx_DataAccess.LastInternalCheckLineNbr := iGenNbr;

    ctx_PayrollCalculation.GetLimitedEDs(DM_PAYROLL.PR['CO_NBR'], DM_PAYROLL.PR['PR_NBR'], GetBeginYear(DM_PAYROLL.PR['CHECK_DATE']), DM_PAYROLL.PR['CHECK_DATE']);
    ctx_DataAccess.NewBatchInserted := True;

    try
      Q.Result.First;
      while not Q.Result.EOF do
      begin
        if (DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', Q.Result['PR_CHECK_NBR'], []))
        and (DM_PAYROLL.PR_CHECK['CHECK_TYPE'] = CHECK_TYPE2_REGULAR) or
        not RegularChecksOnly and (DM_PAYROLL.PR_CHECK['CHECK_TYPE'] = CHECK_TYPE2_MANUAL) then
        begin
          ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
            DM_PAYROLL.PR,
            DM_PAYROLL.PR_BATCH,
            DM_PAYROLL.PR_CHECK,
            DM_PAYROLL.PR_CHECK_LINES,
            DM_PAYROLL.PR_CHECK_STATES,
            DM_PAYROLL.PR_CHECK_SUI,
            DM_PAYROLL.PR_CHECK_LOCALS,
            DM_PAYROLL.PR_CHECK_LINE_LOCALS,
            DM_PAYROLL.PR_SCHEDULED_E_DS,
            DM_PAYROLL.CL_E_DS,
            DM_PAYROLL.CL_PERSON,
            DM_PAYROLL.CL_PENSION,
            DM_PAYROLL.CO,
            DM_COMPANY.CO_E_D_CODES,
            DM_COMPANY.CO_STATES,
            DM_EMPLOYEE.EE,
            DM_EMPLOYEE.EE_SCHEDULED_E_DS,
            DM_EMPLOYEE.EE_STATES,
            DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
            DM_SYSTEM_STATE.SY_STATES,
            False,
            (DM_PAYROLL.PR_BATCH['PAY_SALARY'] = 'Y'),
            (DM_PAYROLL.PR_BATCH['PAY_STANDARD_HOURS'] = 'Y'),
            True
          );
        end;
        Q.Result.Next;
      end;
    finally
      ctx_DataAccess.NewBatchInserted := False;
      ctx_PayrollCalculation.EraseLimitedEDs;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINES]);
  finally
    ctx_DataAccess.Validate := True;
    DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
    DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
    DM_EMPLOYEE.EE.LookupsEnabled := True;
    DM_EMPLOYEE.EE_RATES.LookupsEnabled := True;
  end;
end;

procedure TevPayrollProcessing.RefreshSchduledEDsCheck(
  const CheckNbr: Integer);
begin
  DM_CLIENT.PR_CHECK.DataRequired('PR_CHECK_NBR='+IntToStr(CheckNbr));
  DM_CLIENT.PR.DataRequired('PR_NBR='+DM_CLIENT.PR_CHECK.PR_NBR.AsString);
  DM_CLIENT.CO.DataRequired('CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString);
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString);
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString);
  DM_CLIENT.PR_SCHEDULED_E_DS.DataRequired('PR_NBR='+DM_CLIENT.PR_CHECK.PR_NBR.AsString);
  DM_CLIENT.PR_BATCH.DataRequired('PR_BATCH_NBR='+DM_CLIENT.PR_CHECK.PR_BATCH_NBR.AsString);
  DM_SYSTEM.SY_FED_TAX_TABLE.DataRequired('');
  DM_SYSTEM.SY_STATES.DataRequired('');
  DM_CLIENT.CL_PENSION.DataRequired('');
  DM_CLIENT.CL_E_DS.DataRequired('');
  DM_CLIENT.EE.DataRequired('EE_NBR='+DM_CLIENT.PR_CHECK.EE_NBR.AsString);
  DM_CLIENT.EE_SCHEDULED_E_DS.DataRequired('EE_NBR='+DM_CLIENT.PR_CHECK.EE_NBR.AsString);
  DM_CLIENT.EE_STATES.DataRequired('EE_NBR='+DM_CLIENT.PR_CHECK.EE_NBR.AsString);
  DM_CLIENT.CL_PERSON.DataRequired('CL_PERSON_NBR='+DM_CLIENT.EE.CL_PERSON_NBR.AsString);

  if (DM_CLIENT.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])
  or ((DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR)
  and (DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_MANUAL)) then
    Exit;

  DM_CLIENT.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR='+IntToStr(CheckNbr));
  DM_CLIENT.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR='+IntToStr(CheckNbr));
  DM_CLIENT.PR_CHECK_SUI.DataRequired('PR_CHECK_NBR='+IntToStr(CheckNbr));
  DM_CLIENT.PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR='+IntToStr(CheckNbr));
  DM_CLIENT.PR_CHECK_LINE_LOCALS.DataRequired('PR_CHECK_LINE_LOCALS_NBR=0');

  DM_CLIENT.PR_CHECK_LINES.First;
  while not DM_CLIENT.PR_CHECK_LINES.Eof do
  begin
    if (DM_CLIENT.PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_SCHEDULED)
    or (DM_CLIENT.PR_CHECK_LINES['LINE_TYPE'] = CHECK_LINE_TYPE_DISTR_SCHEDULED) then
    begin
      DM_CLIENT.PR_CHECK_LINES.Delete;
    end
    else
      DM_CLIENT.PR_CHECK_LINES.Next;
  end;

  ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
    DM_CLIENT.PR,
    DM_CLIENT.PR_BATCH,
    DM_CLIENT.PR_CHECK,
    DM_CLIENT.PR_CHECK_LINES,
    DM_CLIENT.PR_CHECK_STATES,
    DM_CLIENT.PR_CHECK_SUI,
    DM_CLIENT.PR_CHECK_LOCALS,
    DM_CLIENT.PR_CHECK_LINE_LOCALS,
    DM_CLIENT.PR_SCHEDULED_E_DS,
    DM_CLIENT.CL_E_DS,
    DM_CLIENT.CL_PERSON,
    DM_CLIENT.CL_PENSION,
    DM_CLIENT.CO,
    DM_CLIENT.CO_E_D_CODES,
    DM_CLIENT.CO_STATES,
    DM_CLIENT.EE,
    DM_CLIENT.EE_SCHEDULED_E_DS,
    DM_CLIENT.EE_STATES,
    DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
    DM_SYSTEM.SY_STATES,
    False,
    (DM_CLIENT.PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y'),
    (DM_CLIENT.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
    True
  );

 ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_LINES]);

end;

procedure TevPayrollProcessing.TaxCalculatorBeforePost(DataSet: TDataSet);
begin
  Dataset['PR_CHECK_NBR'] := FTaxCalculatorPR_CHECK['PR_CHECK_NBR'];
end;

procedure TevPayrollProcessing.TaxCalculator(aPR, aPR_BATCH, aPR_CHECK,
  aPR_CHECK_LINES, aPR_CHECK_LINE_LOCALS, aPR_CHECK_STATES, aPR_CHECK_SUI,
  aPR_CHECK_LOCALS: IisStream; DisableYTDs, DisableShortfalls,
  NetToGross: Boolean);

  procedure DeleteERSUI;
  var
    V: Variant;
  begin
    DM_COMPANY.CO_SUI.First;
    while not DM_COMPANY.CO_SUI.EOF do
    begin
      V := DM_SYSTEM_STATE.SY_SUI.Lookup('SY_SUI_NBR', DM_COMPANY.CO_SUI['SY_SUI_NBR'], 'EE_ER_OR_EE_OTHER_OR_ER_OTHER');
      if VarIsNull(V) or Is_ER_SUI(V) then
        DM_COMPANY.CO_SUI.Delete
      else
        DM_COMPANY.CO_SUI.Next;
    end;
  end;

  procedure CalculateTax(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals, pr_check_line_locals: TevClientDataSet;
                         const disableYTD, disableShortfalls, netToGross: boolean; const net: Double);
  var
    RegularClientED: Integer;
    AmountHigh, NetHigh: Double;
    AmountLow, NetLow: Double;
    AmountOrig: Double;
    InBracket: Boolean;
    direction: integer;
    counter: integer;
  begin
    RegularClientED := 0;

    PR_CHECK.CheckBrowseMode;
    PR_CHECK_LINES.CheckBrowseMode;
    PR_CHECK_STATES.CheckBrowseMode;
    PR_CHECK_SUI.CheckBrowseMode;
    PR_CHECK_LOCALS.CheckBrowseMode;

    PR_CHECK_STATES.First;
    while not PR_CHECK_STATES.EOF do
    begin
      if not PR_CHECK_STATES.FieldByName('STATE_TAX').IsNull then
      begin
        PR_CHECK_STATES.Edit;
        PR_CHECK_STATES['STATE_TAX'] := 0;
        PR_CHECK_STATES.Post;
      end;
      PR_CHECK_STATES.Next;
    end;

    ctx_StartWait;
    ctx_PayrollCalculation.AssignTempCheckLines(PR_CHECK_LINES);
    try
      if netToGross then
      begin
        try
          RegularClientED := DM_CLIENT.CL_E_DS.Lookup('E_D_CODE_TYPE', ED_OEARN_REGULAR, 'CL_E_DS_NBR');
        except
          raise EInconsistentData.CreateHelp('Regular earning is not setup properly for the company! Can not calculate check.', IDH_InconsistentData);
        end;
        if not PR_CHECK_LINES.Locate('CL_E_DS_NBR', RegularClientED, []) then
        begin
          PR_CHECK_LINES.Insert;

  {TODO?  it happens in TevClientDataSet.DoOnNewRecord

          GlobalNameSpace.BeginWrite;
          try
            Dec(NewKeyValue);
            PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').Value := NewKeyValue;
          finally
            GlobalNameSpace.EndWrite;
          end;
  }

          PR_CHECK_LINES['PR_NBR'] := PR_CHECK['PR_NBR'];
          PR_CHECK_LINES['PR_CHECK_NBR'] := PR_CHECK['PR_CHECK_NBR'];
          PR_CHECK_LINES['CL_E_DS_NBR'] := RegularClientED;
          PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
          PR_CHECK_LINES['USER_OVERRIDE'] := GROUP_BOX_NO;
          PR_CHECK_LINES['AMOUNT'] := net * 2;
  //for WebClient: Assigned default value 'N'
          PR_CHECK_LINES['REDUCED_HOURS'] := GROUP_BOX_NO;
          PR_CHECK_LINES.Post;
        end;
      end;

      DM_COMPANY.CO_SUI.Close;
      DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_SUI]);
      try
        ctx_PayrollCalculation.GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
          PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, disableYTD, disableShortfalls, NetToGross);
      finally
        DeleteERSUI;
      end;

      DM_COMPANY.CO_SUI.Close;
      DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_SUI]);
      if netToGross then
      begin
        try
          PR_CHECK_LINES.Locate('CL_E_DS_NBR', RegularClientED, []);
          NetHigh := PR_CHECK.FieldByName('NET_WAGES').Value;
          NetLow := PR_CHECK.FieldByName('NET_WAGES').Value;
          AmountHigh := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          AmountLow := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          AmountOrig := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          direction := 0;
          counter := 0;
          InBracket := True;
          while Abs(net - PR_CHECK.FieldByName('NET_WAGES').AsFloat) >= 0.005 do
          begin
            PR_CHECK_LINES.Locate('CL_E_DS_NBR', RegularClientED, []);
            PR_CHECK_LINES.Edit;
            if direction < 0 then
              if not InBracket then
              begin
                AmountLow := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetLow := PR_CHECK.FieldByName('NET_WAGES').Value;
              end
              else
              begin
                AmountHigh := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetHigh := PR_CHECK.FieldByName('NET_WAGES').Value;
              end
            else if direction > 0 then
              if not InBracket then
              begin
                AmountHigh := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetHigh := PR_CHECK.FieldByName('NET_WAGES').Value;
              end
              else
              begin
                AmountLow := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
                NetLow := PR_CHECK.FieldByName('NET_WAGES').Value;
              end;

            direction := 0;
            InBracket := True;
            if Abs(net - PR_CHECK.FieldByName('NET_WAGES').AsFloat) < 0.025 then
            begin
              PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat + net - PR_CHECK.FieldByName('NET_WAGES').Value;
              counter := counter + 1;
              if counter > 5 then
                EevException.Create('There is no Regular Earning amount that will produce the ' + FloatToStrF(Net, ffFixed, 15, 2) + ' net amount. In order to achieve this amount, you can adjust and then plug taxes.');
            end
            else
            begin
              InBracket := (Net <= NetHigh) and (Net >= NetLow);
              if not InBracket and (Net > NetHigh) or
                 InBracket and (Net >= (NetHigh + NetLow) / 2) then
                direction := 1
              else if not InBracket and (Net < NetLow) or
                 InBracket and (Net < (NetHigh + NetLow) / 2) then
                direction := -1;

              if not InBracket then
              begin
                if (NetHigh = NetLow) and (AmountOrig = 0) then
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat + direction * RoundTwo(Abs(NetHigh - Net))
                else
                  PR_CHECK_LINES.FieldByName('AMOUNT').Value := PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat + direction * (RoundTwo(Abs(NetHigh - NetLow)) + AmountOrig);
                AmountOrig := 0;
              end
              else
                PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundTwo((AmountHigh + AmountLow) / 2);
            end;
            PR_CHECK_LINES.Post;
            PR_CHECK_STATES.First;
            while not PR_CHECK_STATES.EOF do
            begin
              PR_CHECK_STATES.Edit;
              PR_CHECK_STATES.FieldByName('STATE_TAX').Value := 0;
              PR_CHECK_STATES.Post;
              PR_CHECK_STATES.Next;
            end;
            ctx_PayrollCalculation.GrossToNetForTaxCalculator(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS,
              PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, disableYTD, disableShortfalls, netToGross);
          end;
        finally
          DeleteERSUI;
        end;
      end;
    finally
      if DM_PAYROLL.PR_CHECK_LINES.Active then
        ctx_PayrollCalculation.AssignTempCheckLines(DM_PAYROLL.PR_CHECK_LINES)
      else
        ctx_PayrollCalculation.ReleaseTempCheckLines;
      ctx_EndWait;
    end;
  end;

  procedure CalculateCL(const pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals: TevClientDataSet);
  var
    Nbr: Integer;
  begin
    Nbr := PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').AsInteger;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Activate;
    ctx_StartWait;
    PR_CHECK_LINES.DisableControls;
    ctx_PayrollCalculation.AssignTempCheckLines(PR_CHECK_LINES);
    try
      DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired('ALL');
      DM_CLIENT.CL_PIECES.DataRequired('ALL');
      DM_COMPANY.CO_SHIFTS.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_RATES, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_EMPLOYEE.EE_WORK_SHIFTS, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);

      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.Eof do
      begin
        PR_CHECK_LINES.Edit;
        ctx_PayrollCalculation.CreateCheckLineDefaults(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_EMPLOYEE.EE_SCHEDULED_E_DS);
        if PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull then
          ctx_PayrollCalculation.CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_E_D_GROUP_CODES, DM_CLIENT.CL_PERSON,
            DM_CLIENT.CL_PIECES, DM_COMPANY.CO_SHIFTS, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
            DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_WORK_SHIFTS, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
            DM_SYSTEM_STATE.SY_STATES)
        else
        begin
          if not ((PR_CHECK_LINES.FieldByName('LINE_TYPE').AsString = CHECK_LINE_TYPE_USER)
          and (Abs(PR_CHECK_LINES.FieldByName('AMOUNT').AsCurrency) > 0.005)) then
          begin
            Assert(DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []));
            PR_CHECK_LINES.FieldByName('AMOUNT').Value := 0;
  //          if DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE') <> ED_DIRECT_DEPOSIT then
              ctx_PayrollCalculation.ScheduledEDRecalc(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE'),
                  PR_CHECK.FieldByName('GROSS_WAGES').AsFloat, PR_CHECK.FieldByName('NET_WAGES').AsFloat, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES,
                  PR_CHECK_SUI, PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES,
                  DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
                  DM_EMPLOYEE.EE_STATES, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
                  DM_SYSTEM_STATE.SY_STATES);
          end;
        end;
        PR_CHECK_LINES.FieldByName('AMOUNT').Value := RoundAll(ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0), 2);
        PR_CHECK_LINES.Post;
        PR_CHECK_LINES.Next;
      end;
    finally
      if DM_PAYROLL.PR_CHECK_LINES.Active then
        ctx_PayrollCalculation.AssignTempCheckLines(DM_PAYROLL.PR_CHECK_LINES)
      else
        ctx_PayrollCalculation.ReleaseTempCheckLines;
      PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', Nbr, []);
      PR_CHECK_LINES.EnableControls;
      ctx_EndWait;
    end;
  end;

var
  PR, PR_BATCH, PR_CHECK,
  PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_SUI,
  PR_CHECK_LOCALS: TevClientDataSet;
  Net: Real;
begin
  PR := TevClientDataSet.Create(nil);
  PR.Name := 'PR';
  PR_BATCH := TevClientDataSet.Create(nil);
  PR_BATCH.Name := 'PR_BATCH';
  PR_CHECK := TevClientDataSet.Create(nil);
  PR_CHECK.Name := 'PR_CHECK';
  FTaxCalculatorPR_CHECK := PR_CHECK;
  PR_CHECK_LINES := TevClientDataSet.Create(nil);
  PR_CHECK_LINES.Name := 'PR_CHECK_LINES';
  PR_CHECK_LINE_LOCALS := TevClientDataSet.Create(nil);
  PR_CHECK_LINE_LOCALS.Name := 'PR_CHECK_LINE_LOCALS';
  PR_CHECK_STATES := TevClientDataSet.Create(nil);
  PR_CHECK_STATES.Name := 'PR_CHECK_STATES';
  PR_CHECK_SUI := TevClientDataSet.Create(nil);
  PR_CHECK_SUI.Name := 'PR_CHECK_SUI';
  PR_CHECK_LOCALS := TevClientDataSet.Create(nil);
  PR_CHECK_LOCALS.Name := 'PR_CHECK_LOCALS';

  PR_CHECK_LINES.BeforePost := TaxCalculatorBeforePost;
  PR_CHECK_LINES.AfterInsert := TaxCalculatorAfterInsert;
  PR_CHECK_STATES.BeforePost := TaxCalculatorBeforePost;
  PR_CHECK_SUI.BeforePost := TaxCalculatorBeforePost;
  PR_CHECK_LOCALS.BeforePost := TaxCalculatorBeforePost;

  try
    PR.Data := aPR;
    PR_BATCH.Data := aPR_BATCH;
    PR_CHECK.Data := aPR_CHECK;
    PR_CHECK_LINES.Data := aPR_CHECK_LINES;
    PR_CHECK_LINE_LOCALS.Data := aPR_CHECK_LINE_LOCALS;
    PR_CHECK_STATES.Data := aPR_CHECK_STATES;
    PR_CHECK_SUI.Data := aPR_CHECK_SUI;
    PR_CHECK_LOCALS.Data := aPR_CHECK_LOCALS;

    DM_CLIENT.CO.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);

    if not DM_PAYROLL.PR.Active then
      DM_PAYROLL.PR.DataRequired('CO_NBR=' + DM_COMPANY.CO.CO_NBR.AsString);

    if not DM_PAYROLL.PR_CHECK.Active then
      DM_PAYROLL.PR_CHECK.DataRequired('EE_NBR=' + pr_check.FieldByName('ee_nbr').AsString);

    DM_PAYROLL.PR_CHECK.Locate('EE_NBR', pr_check.FieldByName('ee_nbr').Value, []);

    CalculateCL(pr, pr_batch, pr_check, pr_check_lines, pr_check_states, pr_check_sui, pr_check_locals);
    if NetToGross then
      Net := PR_CHECK.FieldByName('NET_WAGES').AsFloat
    else
      Net := 0;
    CalculateTax(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
      DisableYTDs, DisableShortfalls, NetToGross, Net);

    if NetToGross then
      CalculateTax(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
        DisableYTDs, DisableShortfalls, False, 0);

    if Net = 0 then
    begin
      CalculateCL(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS);
      CalculateTax(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
        DisableYTDs, DisableShortfalls, NetToGross, 0);
    end;

    aPR.Clear;
    aPR_BATCH.Clear;
    aPR_CHECK.Clear;
    aPR_CHECK_LINES.Clear;
    aPR_CHECK_LINE_LOCALS.Clear;
    aPR_CHECK_STATES.Clear;
    aPR_CHECK_SUI.Clear;
    aPR_CHECK_LOCALS.Clear;

    aPR.CopyFrom(PR.GetDataStream(True, True));
    aPR_BATCH.CopyFrom(PR_BATCH.GetDataStream(False, True));
    aPR_CHECK.CopyFrom(PR_CHECK.GetDataStream(False, True));
    aPR_CHECK_LINES.CopyFrom(PR_CHECK_LINES.GetDataStream(False, True));
    aPR_CHECK_LINE_LOCALS.CopyFrom(PR_CHECK_LINE_LOCALS.GetDataStream(False, True));
    aPR_CHECK_STATES.CopyFrom(PR_CHECK_STATES.GetDataStream(False, True));
    aPR_CHECK_SUI.CopyFrom(PR_CHECK_SUI.GetDataStream(False, True));
    aPR_CHECK_LOCALS.CopyFrom(PR_CHECK_LOCALS.GetDataStream(False, True));

  finally
    PR.Free;
    PR_BATCH.Free;
    PR_CHECK.Free;
    PR_CHECK_LINES.Free;
    PR_CHECK_LINE_LOCALS.Free;
    PR_CHECK_STATES.Free;
    PR_CHECK_SUI.Free;
    PR_CHECK_LOCALS.Free;
  end;
end;

procedure TevPayrollProcessing.TaxCalculatorAfterInsert(
  DataSet: TDataSet);
begin
  Dataset['PR_CHECK_NBR'] := FTaxCalculatorPR_CHECK['PR_CHECK_NBR'];
  DataSet['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
end;

procedure TevPayrollProcessing.UnvoidCheck(CheckNbr: Integer);
var
  VoidCheckNbr, VoidPrNbr: Integer;
  AllOK: Boolean;
  Q: IevQuery;
begin
  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    Q := TevQuery.Create('select PR_CHECK_NBR, PR_NBR, FILLER, cast(trim(FILLER) as integer) from pr_check where FILLER is not null and trim(FILLER) <> '''' and cast(trim(FILLER) as integer) = '+IntToStr(CheckNbr));
    if Q.Result.RecordCount <> 1 then
      raise EevException.Create('Void check not found.');
    VoidCheckNbr := Q.Result['PR_CHECK_NBR'];
    VoidPrNbr := Q.Result['PR_NBR'];

    DM_CLIENT.PR.DataRequired('PR_NBR='+IntToStr(VoidPrNbr));
    DM_CLIENT.PR_CHECK.DataRequired('PR_CHECK_NBR='+IntToStr(VoidCheckNbr));

    ctx_DataAccess.UnlockPR;
    AllOK := False;
    DM_CLIENT.PR_CHECK.Delete;
    try
      ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK]);
      AllOK := True;
    finally
      ctx_DataAccess.LockPR(AllOK);
    end;

// Clear Bank Account Register
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckNbr));
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
    while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
    begin
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);

    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

procedure TevPayrollProcessing.RedistributeDBDT(
  const PrCheckLineNbr: Integer; Redistribution: IisParamsCollection);
var
  Hours, Amount: Currency;
  v, vlt: Variant;
  t: TStringList;
  FieldList: string;
  DivisionNbr, BranchNbr, DepartmentNbr, TeamNbr, WorkersCompNbr, JobsNbr, LineItemDate: Variant;
  i: Integer;
  Item: IisListOfValues;
  rl: Boolean;
  bp: TDataSetNotifyEvent;
begin
  DM_CLIENT.PR_CHECK_LINES.DataRequired('PR_CHECK_LINES_NBR='+IntToStr(PrCheckLineNbr));
  DM_CLIENT.PR.DataRequired('PR_NBR=' + DM_CLIENT.PR_CHECK_LINES.PR_NBR.AsString);

  Amount := ConvertNull(DM_CLIENT.PR_CHECK_LINES['AMOUNT'], 0);
  for i := 0 to Redistribution.Count - 1 do
  begin
    Item := Redistribution.Params[i];
    if Item.Value['CO_DIVISION_NBR'] = 0 then
      Item.Value['CO_DIVISION_NBR'] := Null;
    if Item.Value['CO_BRANCH_NBR'] = 0 then
      Item.Value['CO_BRANCH_NBR'] := Null;
    if Item.Value['CO_DEPARTMENT_NBR'] = 0 then
      Item.Value['CO_DEPARTMENT_NBR'] := Null;
    if Item.Value['CO_TEAM_NBR'] = 0 then
      Item.Value['CO_TEAM_NBR'] := Null;
    if Item.Value['CO_WORKERS_COMP_NBR'] = 0 then
      Item.Value['CO_WORKERS_COMP_NBR'] := Null;
    if Item.Value['CO_JOBS_NBR'] = 0 then
      Item.Value['CO_JOBS_NBR'] := Null;
    if Item.Value['LINE_ITEM_DATE'] = 0 then
      Item.Value['LINE_ITEM_DATE'] := Null;
    Amount := Amount - Item.Value['Amount'];
  end;
  if Abs(Amount) > 0.005 then
    raise Exception.Create('Sum of hours and amounts must be equal to original hours and amount');

  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    ctx_DataAccess.UnlockPR;
    t := TStringList.Create;
    bp := DM_PAYROLL.PR_CHECK_LINES.BeforePost;
    DM_PAYROLL.PR_CHECK_LINES.BeforePost := nil;
    rl := ctx_DataAccess.RecalcLineBeforePost;
    ctx_DataAccess.RecalcLineBeforePost := False;
    try
      try
        DM_PAYROLL.PR_CHECK_LINES.GetFieldNames(t);
        t.Delete(t.IndexOf('PR_CHECK_LINES_NBR'));
        t.Delete(t.IndexOf('AMOUNT'));
        FieldList := StringReplace(t.CommaText, ',', ';', [rfReplaceAll]);
        v := DM_PAYROLL.PR_CHECK_LINES[FieldList];
        vlt := DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'];
        Hours := 0;
        Amount := 0;
        DivisionNbr := DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'];
        BranchNbr := DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'];
        DepartmentNbr := DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'];
        TeamNbr := DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'];
        WorkersCompNbr := DM_PAYROLL.PR_CHECK_LINES['CO_WORKERS_COMP_NBR'];
        JobsNbr := DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'];
        LineItemDate := DM_PAYROLL.PR_CHECK_LINES['LINE_ITEM_DATE'];

        for i := 0 to Redistribution.Count - 1 do
        begin
          Item := Redistribution.Params[i];
          if (DivisionNbr = Item.Value['CO_DIVISION_NBR']) and
             (BranchNbr = Item.Value['CO_BRANCH_NBR']) and
             (DepartmentNbr = Item.Value['CO_DEPARTMENT_NBR']) and
             (TeamNbr = Item.Value['CO_TEAM_NBR']) and
             (WorkersCompNbr = Item.Value['CO_WORKERS_COMP_NBR']) and
             (JobsNbr = Item.Value['CO_JOBS_NBR']) and
             (LineItemDate = Item.Value['LINE_ITEM_DATE'])
              then
          begin
            Continue;
          end
          else
          begin
            Hours := Hours - Item.Value['Hours'];
            Amount := Amount - Item.Value['Amount'];
          end;
          DM_PAYROLL.PR_CHECK_LINES.Insert;
          DM_PAYROLL.PR_CHECK_LINES[FieldList] := v;
          DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := vlt;
          DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'] := Item.Value['CO_DIVISION_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'] := Item.Value['CO_BRANCH_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := Item.Value['CO_DEPARTMENT_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'] := Item.Value['CO_TEAM_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['CO_WORKERS_COMP_NBR'] := Item.Value['CO_WORKERS_COMP_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'] := Item.Value['CO_JOBS_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['LINE_ITEM_DATE'] := Item.Value['LINE_ITEM_DATE'];
          if Item.Value['Hours'] <> 0 then
            DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'] := Item.Value['Hours']
          else
            DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Clear;
          if Item.Value['AMOUNT'] <> 0 then
            DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat := Item.Value['AMOUNT']
          else
            DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').Clear;
          DM_PAYROLL.PR_CHECK_LINES.Post;
        end;
        if (Hours <> 0) or (Amount <> 0) then
        begin
          DM_PAYROLL.PR_CHECK_LINES.Insert;
          DM_PAYROLL.PR_CHECK_LINES[FieldList] := v;
          DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := vlt;
          if Hours <> 0 then
            DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'] := Hours
          else
            DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Clear;
          if Amount <> 0 then
            DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := Amount
          else
            DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').Clear;
          DM_PAYROLL.PR_CHECK_LINES.Post;
        end;
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINES]);
      except
        DM_PAYROLL.PR_CHECK_LINES.CancelUpdates;
        raise;
      end;
    finally
      DM_PAYROLL.PR_CHECK_LINES.BeforePost := bp;
      ctx_DataAccess.RecalcLineBeforePost := rl;    
      t.Free;
      ctx_DataAccess.LockPR(True);
    end;
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPayrollProcessing, IevPayrollProcessing, 'Payroll Processing Module');

end.
