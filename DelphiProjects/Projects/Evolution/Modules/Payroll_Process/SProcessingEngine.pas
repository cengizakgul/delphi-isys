// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SProcessingEngine;

interface

uses
  PayrollLog, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, Db, Grids, DBGrids, StdCtrls,  ExtCtrls, EvTypes, Variants,
  EvBasicUtils, EvContext, isThreadManager, EvExceptions, evMainboard,
  IsBasicUtils, EvDataSet, EvStreamUtils, EvDataAccessComponents, EvClientDataSet;


function DoMainProcessing(PayrollNumber: Integer; JustPreProcess: Boolean; PayrollLog: TPayrollLog; ACheckReturnQueue: Boolean; PrBatchNbr: Integer): String;

implementation

uses
  EvUtils, SDataStructure, EvConsts, SPD_DM_HISTORICAL_TAXES,  STaxCalculations,
  SProcessTOAccrual, SDueDateCalculation, SReturnQueueEnlistProcedures,
  DateUtils, SDataDictclient, ISKbmMemDataSet, Types, evCommonInterfaces,
  isBaseClasses, SDDClasses, XLSFile, XLSWorkbook, EvPayroll;

function DoMainProcessing(PayrollNumber: Integer; JustPreProcess: Boolean; PayrollLog: TPayrollLog; ACheckReturnQueue: Boolean; PrBatchNbr: Integer): String;
var
  PostACH, GetRidOfCheck, HasQTDs, IsImport, Backdated, CheckHasShortfall, AllOK, WasSetupRun, SecondCheck: Boolean;
  MyTransactionManager: TTransactionManager;
  Tax941: Currency;
  EDType, Temp: String;
  AsOfDate: TDateTime;
  Wages1, Wages2, Wages3, Tax, TaxRate, QTDAmount, Impound: Real;
  WagesPayroll, WagesQTD, SUITax: Real;
  nvcd: TevClientDataSet;
  PR_CHECK_2, PR_CHECK_LINES_2, PR_CHECK_STATES_2, PR_CHECK_SUI_2, PR_CHECK_LOCALS_2: TevClientDataSet;
  PR_MISCELLANEOUS_CHECKS_2, TempDataSet: TevClientDataSet;
  SB_BANK_ACCOUNTSQuery: IevQuery;
  NewPayrollNbr, OldPayrollNbr, PriorEENbr, BankAcctNbr, CheckCount, I, j, k: Integer;
  SaveFilter: String;
  SaveFiltered: Boolean;
  s: String;
  TotalPayroll: Currency;
  Category: String;
  SQLQuery, Q: IevQuery;
  LocalLiab: IisParamsCollection;
  LocalLiabName, LocalName: String;
  TmpClientID: Integer;
  Locations, LocationsM, PaStored: IevDataset;
  Index, OrigNbr: Integer;
  PaWarningList: IisListOfValues;
  CheckProcessingIndex: IisListOfValues; 
const
  CREATE_TAX_CHECK = True;
  CREATE_TRUST_CHECK = False;


  procedure AddToPayrollLog(Msg: string);
  begin
    PayrollLog.Add(Msg);
  end;

  procedure PostFeedback(bShow: boolean; sDisplayString: string);
  begin
    ctx_PayrollCalculation.ProcessFeedback(bShow, sDisplayString);
    AddToPayrollLog(sDisplayString);
  end;

  procedure CheckReturnQueueEx(const PeriodEndDate: TDateTime); // Adds company to return queue
  var
    cd: TevClientDataSet;
    v: array of Integer;
    i: Integer;
  begin
    if DM_CLIENT.CO['AUTO_ENLIST'] = ReturnAutoEnlist_None then
      Exit;
    cd := TevClientDataSet.Create(nil);
    cd.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
    try
      with TExecDSWrapper.Create('GetUnprocessedPayrolls') do
      begin
        SetParam('CoNbr', DM_CLIENT.CO['CO_NBR']);
        SetParam('CurrDate', DM_CLIENT.PR['CHECK_DATE']);
        SetParam('EndDate', PeriodEndDate);
        cd.DataRequest(AsVariant);
      end;
      cd.Open;
      if cd.FieldByName('res').AsInteger = 0 then
      begin
        PostFeedback(True, 'Adding company to return queue...');
        if not VarIsNull(DM_CLIENT.CO['cl_co_consolidation_nbr']) then
        begin
          cd.Close;
          with TExecDSWrapper.Create('GetConsolidatedCompanies1') do
          begin
            SetParam('Nbr', DM_CLIENT.CO['CL_CO_CONSOLIDATION_NBR']);
            cd.DataRequest(AsVariant);
          end;
          cd.Open;
        end;
        if cd.Active and
            (not VarIsNull(DM_CLIENT.CO['CL_CO_CONSOLIDATION_NBR'])) and
           (cd.RecordCount > 0)
           then
        begin
          SetLength(v, 1);
          cd.First;
          while not cd.Eof do
          begin
            v[0] := cd.FieldValues['CO_NBR'];
            EnlistCompany(v, PeriodEndDate, Null,cd.FieldByName('CONSOLIDATION_TYPE').AsString, cd.FieldByName('SCOPE').AsString);
            cd.Next;
          end;
          SetLength(v, cd.RecordCount);
          v[0] := cd.FieldValues['PRIMARY_CO_NBR'];
          i := 1;
          cd.First;
          while not cd.Eof do
          begin
            if (cd.FieldValues['CO_NBR'] <> cd.FieldValues['PRIMARY_CO_NBR'])
            and (i <= VarArrayHighBound(v, 1)) then
            begin
              v[i] := cd.FieldValues['CO_NBR'];
              Inc(i);
            end;
            cd.Next;
          end;
          if not cd.Locate('CO_NBR', cd.FieldValues['PRIMARY_CO_NBR'], []) then
            raise EInconsistentData.CreateHelp('Consolidated Reporting setup is incorrect.', IDH_InconsistentData);
          EnlistCompany(v, PeriodEndDate, cd.FieldValues['CL_CO_CONSOLIDATION_NBR'],
                                cd.FieldByName('CONSOLIDATION_TYPE').AsString, cd.FieldByName('SCOPE').AsString);
        end
        else
        begin
          SetLength(v, 1);
          v[0] := DM_CLIENT.CO['CO_NBR'];
          EnlistCompany(v, PeriodEndDate, Null);
        end;
      end;
      cd.Close;
    finally
      cd.Free;
    end;
  end;

  procedure CheckReturnQueue; // Adds company to return queue
  begin
    Assert(DM_CLIENT.PR.Locate('PR_NBR', PayrollNumber, []));
    if (DM_CLIENT.PR.PAYROLL_TYPE.AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT)
    or ACheckReturnQueue then
    begin
      if GetDay(DM_CLIENT.PR['CHECK_DATE']) <= 15 then
        CheckReturnQueueEx(GetBeginMonth(DM_CLIENT.PR['CHECK_DATE'])+ 14); // the 15th
      CheckReturnQueueEx(GetEndMonth(DM_CLIENT.PR['CHECK_DATE']));
    end;  
  end;

// 16-20

  procedure CalcEROASDITaxes(var OASDITax, OASDITaxWages, OASDITaxTips, OASDIGrossWages: Real);
  var
    YTDOASDITaxWagesTips, YTDOASDITax, OASDIWageLimit: Real;
    EENumber: Integer;
    EDCodeType: string;

    function TypeIsTips(const ACode: String): Boolean;
    begin
      if CompareDate(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2013, 12, 31)) = GreaterThanValue then
        Result := TypeIsTipsAfter2013(ACode)
      else
        Result := TypeIsTipsBefore2014(ACode);
    end;

  begin
    OASDITaxWages := 0;
    OASDITaxTips := 0;
    OASDIGrossWages := 0;
    OASDITax := 0;
    with DM_HISTORICAL_TAXES do
    begin
      ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(DM_CLIENT.PR_CHECK.FieldByName('PR_CHECK_NBR').Value, 0, ltEROASDI, DM_CLIENT.PR, DM_CLIENT.PR_CHECK, nil, nil, nil,
        YTDOASDITaxWagesTips, YTDOASDITax, 0, GetEndYear(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime));
      OASDIWageLimit := SY_FED_TAX_TABLE.FieldByName('OASDI_WAGE_LIMIT').Value;
      if DM_CLIENT.CO.FieldByName('FED_TAX_EXEMPT_ER_OASDI').Value = 'Y' then Exit;
      EENumber := DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').Value;
      if EE.ShadowDataSet.CachedLookup(EENumber, 'EXEMPT_EMPLOYER_OASDI') = 'Y' then Exit;
      DM_CLIENT.PR_CHECK_LINES.First;
      while not DM_CLIENT.PR_CHECK_LINES.EOF do
      begin
        EDCodeType := CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'E_D_CODE_TYPE');
        if TypeIsTaxable(EDCodeType) then
          if SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_EMPLOYER_OASDI') <> 'Y' then
            if CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'ER_EXEMPT_EXCLUDE_OASDI') <> 'E' then
            begin
              if TypeIsTips(EDCodeType) then
                OASDITaxTips := OASDITaxTips + ConvertDeduction(EDCodeType, DM_CLIENT.PR_CHECK_LINES.FieldByName('AMOUNT').Value)
              else
                OASDITaxWages := OASDITaxWages + ConvertDeduction(EDCodeType, DM_CLIENT.PR_CHECK_LINES.FieldByName('AMOUNT').Value);
            end;
        DM_CLIENT.PR_CHECK_LINES.Next;
      end;
      OASDIGrossWages := OASDITaxWages + OASDITaxTips;
      if YTDOASDITaxWagesTips >= OASDIWageLimit then
      begin
        if SY_FED_TAX_TABLE.FieldByName('ER_OASDI_RATE').Value * OASDIWageLimit <> YTDOASDITax then
          OASDITax := SY_FED_TAX_TABLE.FieldByName('ER_OASDI_RATE').Value * OASDIWageLimit - YTDOASDITax;
        OASDITaxWages := 0;
        OASDITaxTips := 0;
        Exit;
      end;
      if OASDITaxWages + YTDOASDITaxWagesTips > OASDIWageLimit then
      begin
        OASDITaxWages := OASDIWageLimit - YTDOASDITaxWagesTips;
        OASDITaxTips := 0;
      end
      else
      begin
        if OASDITaxWages + OASDITaxTips + YTDOASDITaxWagesTips > OASDIWageLimit then
          OASDITaxTips := OASDIWageLimit - YTDOASDITaxWagesTips - OASDITaxWages;
      end;
      if DM_CLIENT.PR_CHECK.FieldByName('EXCLUDE_EMPLOYER_OASDI').Value = 'Y' then
        Exit;
      OASDITax := SY_FED_TAX_TABLE.FieldByName('ER_OASDI_RATE').Value * (OASDITaxWages + OASDITaxTips);
      if Abs(SY_FED_TAX_TABLE.FieldByName('ER_OASDI_RATE').Value * YTDOASDITaxWagesTips - YTDOASDITax) > TaxShortfallPickupLimit then
        OASDITax := OASDITax + SY_FED_TAX_TABLE.FieldByName('ER_OASDI_RATE').Value * YTDOASDITaxWagesTips - YTDOASDITax;
    end;
  end;

// 16-21

  procedure CalcERMedicareTaxes(var MedicareTax, MedicareTaxWages, MedicareGrossWages: Real);
  var
    YTDMedicareTaxWages, YTDMedicareTax, MedicareWageLimit: Real;
    EENumber: Integer;
    EDCodeType: string;
  begin
    MedicareTax := 0;
    MedicareTaxWages := 0;
    MedicareGrossWages := 0;
    with DM_HISTORICAL_TAXES do
    begin
      ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(DM_CLIENT.PR_CHECK.FieldByName('PR_CHECK_NBR').Value, 0, ltERMedicare, DM_CLIENT.PR, DM_CLIENT.PR_CHECK, nil, nil, nil,
        YTDMedicareTaxWages, YTDMedicareTax, 0, GetEndYear(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime));
      MedicareWageLimit := SY_FED_TAX_TABLE.FieldByName('MEDICARE_WAGE_LIMIT').Value;
      if DM_CLIENT.CO.FieldByName('FED_TAX_EXEMPT_ER_MEDICARE').Value = 'Y' then Exit;
      EENumber := DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').Value;
      if EE.ShadowDataSet.CachedLookup(EENumber, 'EXEMPT_EMPLOYER_MEDICARE') = 'Y' then Exit;
      DM_CLIENT.PR_CHECK_LINES.First;
      while not DM_CLIENT.PR_CHECK_LINES.EOF do
      begin
        EDCodeType := CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'E_D_CODE_TYPE');
        if TypeIsTaxable(EDCodeType) then
          if SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_EMPLOYER_MEDICARE') <> 'Y' then
            if CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'ER_EXEMPT_EXCLUDE_MEDICARE') <> 'E' then
              MedicareTaxWages := MedicareTaxWages + ConvertDeduction(EDCodeType, DM_CLIENT.PR_CHECK_LINES.FieldByName('AMOUNT').Value);
        DM_CLIENT.PR_CHECK_LINES.Next;
      end;
      MedicareGrossWages := MedicareTaxWages;
      if MedicareTaxWages + YTDMedicareTaxWages > MedicareWageLimit then
        MedicareTaxWages := MedicareWageLimit - YTDMedicareTaxWages;
      if DM_CLIENT.PR_CHECK.FieldByName('EXCLUDE_EMPLOYER_MEDICARE').Value = 'Y' then
        Exit;
      MedicareTax := SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value * MedicareTaxWages;
      if Abs(SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value * YTDMedicareTaxWages - YTDMedicareTax) > TaxShortfallPickupLimit then
        MedicareTax := MedicareTax + SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value * YTDMedicareTaxWages - YTDMedicareTax;
    end;
  end;

// 16-22


  procedure GetFuiYTD(PayrollNbr, CheckNbr, EENbr: Integer; BeginDate, EndDate: TDateTime;
    PR_CHECK: TevClientDataSet; var YTDTaxWage, YTDTax: Real);
  const
    FedTemplate = 'select sum(#F1) TAX_WAGE, sum(#F2) TAX, sum(#F3) TAX_TIP ' +
        'from PR_CHECK C ' +
        'join PR P on C.PR_NBR=P.PR_NBR and {AsOfNow<P>} and P.PR_NBR<>:PayrollNbr and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.STATUS in (#STATUS) ' +
        'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} and E.CL_PERSON_NBR=:ClPersonNbr ' +
        'join CO CO on E.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and (CO.CL_COMMON_PAYMASTER_NBR=(select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
        'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
        'where CO.CO_NBR=:CONbr '+

        'and C.CHECK_STATUS<>''V'' ' +
        'and c.CHECK_TYPE <> ''V'' and c.PR_CHECK_NBR not in (select cast(trim(v.FILLER) as integer) from pr_check v where v.ee_nbr=c.ee_nbr and v.FILLER is not null and trim(v.FILLER) <> '''') ' +

        'and M.COMBINE_#COMBINETAX=''Y'' and {AsOfNow<CO>} and {AsOfNow<M>}' +
        ') and P.CO_NBR<>:CONbr or (P.CO_NBR=:CONbr and (C.EE_NBR=:EENbr and :Act=1 or C.EE_NBR<>:EENbr and :Act=2 or :Act=3))) ' +
        'where {AsOfNow<C>} ';

  var
    CheckWasEdited: Boolean;
    q: IevQuery;
    CLPersonNbr: Integer;
    ees: array of Integer;
    iCount: Integer;

    function isEEinList(const Nbr: Integer): Boolean;
    var
      i: Integer;
    begin
      Result := False;
      for i := 0 to High(ees) do
        if ees[i] >= Nbr then
        begin
          Result := ees[i] = Nbr;
          Break;
        end;
    end;

  begin
    CLPersonNbr := StrToIntDef(DM_EMPLOYEE.EE.ShadowDataSet.CachedLookup(EENbr, 'CL_PERSON_NBR'), 0);
    try
      DM_EMPLOYEE.EE.ShadowDataSet.IndexFieldNames := 'CL_PERSON_NBR;EE_NBR';
      DM_EMPLOYEE.EE.ShadowDataSet.SetRange([CLPersonNbr], [CLPersonNbr]);
      SetLength(ees, DM_EMPLOYEE.EE.ShadowDataSet.RecordCount);
      iCount := 0;
      while not DM_EMPLOYEE.EE.ShadowDataSet.Eof do
      begin
        ees[iCount] := DM_EMPLOYEE.EE.ShadowDataSet.FieldByName('EE_NBR').AsInteger;
        Inc(iCount);
        DM_EMPLOYEE.EE.ShadowDataSet.Next;
      end;
    finally
      DM_EMPLOYEE.EE.ShadowDataSet.CancelRange;
      DM_EMPLOYEE.EE.ShadowDataSet.IndexFieldNames := 'EE_NBR';
    end;

    q := TevQuery.Create('FedTemplate');
    q.Macro['F1'] := 'ER_FUI_TAXABLE_WAGES';
    q.Macro['F2'] := 'ER_FUI_TAX';
    q.Macro['COMBINETAX'] := 'FUI';
    q.Macro['F3'] := '0';
    q.Param['CoNbr'] := DM_EMPLOYEE.EE.Lookup('EE_NBR', EENbr, 'CO_NBR');
    q.Param['Act'] := 3;
    q.Param['EENbr'] := EENbr;
    q.Param['ClPersonNbr'] := CLPersonNbr;
    q.Param['RecordNbr'] := 0;
    q.Param['PayrollNbr'] := PayrollNbr;
    q.Param['BeginDate'] := BeginDate;
    q.Param['EndDate'] := EndDate;
    q.Macro['Status'] := '''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''';

    YTDTaxWage := ConvertNull(q.Result['TAX_WAGE'], 0);
    YTDTax := ConvertNull(q.Result['TAX'], 0);

    CheckWasEdited := (PR_CHECK.State in [dsInsert, dsEdit]);
    if CheckWasEdited then
      PR_CHECK.Post;
    PR_CHECK := PR_CHECK;
    PR_CHECK.DisableControls;
    try
      PR_CHECK.First;
      while not PR_CHECK.Eof do
      begin
        if CheckNbr = PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger then
          break;
        if (PR_CHECK.FieldByName('PR_NBR').AsInteger = PayrollNbr) and isEEinList(PR_CHECK.FieldByName('EE_NBR').AsInteger) then
        begin
          YTDTaxWage := YTDTaxWage + ConvertNull(PR_CHECK['ER_FUI_TAXABLE_WAGES'], 0);
          YTDTax := YTDTax + ConvertNull(PR_CHECK['ER_FUI_TAX'], 0);
        end;
        PR_CHECK.Next;
      end;
      PR_CHECK.Locate('PR_CHECK_NBR', CheckNbr, []);
      if CheckWasEdited then
        PR_CHECK.Edit;
    finally
      PR_CHECK.EnableControls;
    end;
  end;

  procedure CalcERFUITaxes(var FUITax, FUITaxWages, FUIGrossWages: Real);
  var
    YTDFUITaxWages, YTDFUITax, FUIWageLimit, FUIRate, YTDCalc: Real;
    EENumber,  CheckNbr, PrNbr: Integer;
    EDCodeType: String;
    q, r: IevQuery;
    CheckWasEdited: Boolean;
  begin
    EENumber := DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').AsInteger;

    FUITax := 0;
    FUITaxWages := 0;
    FUIGrossWages := 0;

    DM_CLIENT.EE.First;
    DM_CLIENT.EE.Locate('EE_NBR', EENumber, []);

    if not CheckProcessingIndex.ValueExists(DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsString) then
      CheckProcessingIndex.AddValue(DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsString, CheckProcessingIndex.Count);

    with DM_HISTORICAL_TAXES do
    begin
      if DM_CLIENT.CO.FieldByName('FUI_TAX_EXEMPT').Value = 'Y' then
        Exit;
      EENumber := DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').Value;
      if EE.ShadowDataSet.CachedLookup(EENumber, 'EXEMPT_EMPLOYER_FUI') = 'Y' then
        Exit;
      CheckWasEdited := (DM_CLIENT.PR_CHECK.State in [dsInsert, dsEdit]);
      if CheckWasEdited then
        DM_CLIENT.PR_CHECK.Post;
      try
        GetFuiYTD(DM_CLIENT.PR_CHECK['PR_NBR'], DM_CLIENT.PR_CHECK['PR_CHECK_NBR'], DM_CLIENT.PR_CHECK['EE_NBR'],
          GetBeginYear(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime), GetEndYear(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime),
          DM_CLIENT.PR_CHECK, YTDFUITaxWages, YTDFUITax);
      finally
        if CheckWasEdited then
          DM_CLIENT.PR_CHECK.Edit;
      end;
      FUIWageLimit := SY_FED_TAX_TABLE.FieldByName('FUI_WAGE_LIMIT').Value;
      DM_CLIENT.PR_CHECK_LINES.First;
      while not DM_CLIENT.PR_CHECK_LINES.EOF do
      begin
        EDCodeType := CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'E_D_CODE_TYPE');
        if TypeIsTaxable(EDCodeType) then
          if SY_FED_EXEMPTIONS.Lookup('E_D_CODE_TYPE', EDCodeType, 'EXEMPT_FUI') <> 'Y' then
            if CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'ER_EXEMPT_EXCLUDE_FUI') <> 'E' then
              FUITaxWages := FUITaxWages + ConvertDeduction(EDCodeType, DM_CLIENT.PR_CHECK_LINES.FieldByName('AMOUNT').Value);
        DM_CLIENT.PR_CHECK_LINES.Next;
      end;
      FUIGrossWages := FUITaxWages;
      if YTDFUITaxWages >= FUIWageLimit then
      begin
        FUITaxWages := 0;
        FUITax := 0;
        Exit;
      end;
      if FUITaxWages + YTDFUITaxWages > FUIWageLimit then
        FUITaxWages := FUIWageLimit - YTDFUITaxWages;

      if DM_CLIENT.PR_CHECK.FieldByName('EXCLUDE_EMPLOYER_FUI').Value = 'Y' then
        Exit;

      if ConvertNull(EE.ShadowDataSet.CachedLookup(EENumber, 'FUI_RATE_CREDIT_OVERRIDE'), '') <> '' then
        FUIRate := SY_FED_TAX_TABLE.FieldByName('FUI_RATE_REAL').Value - StrToFloat(EE.ShadowDataSet.CachedLookup(EENumber, 'FUI_RATE_CREDIT_OVERRIDE'))
      else
      if not DM_CLIENT.CO.FieldByName('FUI_RATE_OVERRIDE').IsNull then
        FUIRate := SY_FED_TAX_TABLE.FieldByName('FUI_RATE_REAL').Value - DM_CLIENT.CO.FieldByName('FUI_RATE_OVERRIDE').Value
          {This is actually FUI credit override}
      else
        FUIRate := SY_FED_TAX_TABLE.FieldByName('FUI_RATE_REAL').Value - SY_FED_TAX_TABLE.FieldByName('FUI_RATE_CREDIT').Value;
      FUITax := FUIRate * FUITaxWages;
      if Abs(FUIRate * YTDFUITaxWages - YTDFUITax) > TaxShortfallPickupLimit then
      begin
        if (ConvertNull(EE.ShadowDataSet.CachedLookup(EENumber, 'FUI_RATE_CREDIT_OVERRIDE'), '') = '')
        and DM_CLIENT.CO.FieldByName('FUI_RATE_OVERRIDE').IsNull then
        begin
          CheckNbr := DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsInteger;
          PrNbr := DM_CLIENT.PR_CHECK.PR_NBR.AsInteger;
          q := TevQuery.Create(
              'select sum(ER_FUI_TAXABLE_WAGES) TAX_WAGE, sum(ER_FUI_TAX) TAX, P.CHECK_DATE ' +
              'from PR_CHECK C ' +
              'join PR P on C.PR_NBR=P.PR_NBR and ' +
               '{AsOfNow<P>} and P.PR_NBR<>:PayrollNbr ' +
              'and P.CHECK_DATE>=:BeginDate and P.CHECK_DATE<=:EndDate and P.STATUS in (''P'',''V'') ' +
              'join EE E on C.EE_NBR=E.EE_NBR and {AsOfNow<E>} and E.CL_PERSON_NBR=:ClPersonNbr ' +
              'join CO CO on E.CO_NBR=CO.CO_NBR and {AsOfNow<CO>} and ' +
              '(CO.CL_COMMON_PAYMASTER_NBR=(select CO.CL_COMMON_PAYMASTER_NBR from CO CO ' +
              'join CL_COMMON_PAYMASTER M on M.CL_COMMON_PAYMASTER_NBR=CO.CL_COMMON_PAYMASTER_NBR ' +
              'where CO.CO_NBR=:CONbr and M.COMBINE_FUI=''Y'' and {AsOfNow<CO>} and {AsOfNow<M>} ' +
              ') or CO.CO_NBR=:CONbr) ' +
              'where {AsOfNow<C>} '+
              'group by P.CHECK_DATE order by P.CHECK_DATE ');

          q.Params.AddValue('CoNbr', DM_CLIENT.CO.CO_NBR.AsInteger);
          q.Params.AddValue('EENbr', DM_CLIENT.EE.EE_NBR.AsInteger);
          q.Params.AddValue('ClPersonNbr', StrToIntDef(DM_CLIENT.EE.ShadowDataSet.CachedLookup(DM_CLIENT.EE.EE_NBR.AsInteger, 'CL_PERSON_NBR'), 0));
          q.Params.AddValue('PayrollNbr', PrNbr);
          q.Params.AddValue('BeginDate', GetBeginYear(DM_CLIENT.PR.CHECK_DATE.AsDateTime));
          q.Params.AddValue('EndDate', DM_CLIENT.PR.CHECK_DATE.AsDateTime);

          YTDCalc := 0;
          while not q.Result.Eof do
          begin

            r := TevQuery.Create('select FUI_RATE_REAL, FUI_RATE_CREDIT from SY_FED_TAX_TABLE where {AsOfDate<SY_FED_TAX_TABLE>}');
            r.Param['StatusDate'] := q.Result.FieldByName('CHECK_DATE').Value;

            YTDCalc := YTDCalc + q.Result.FieldByName('TAX_WAGE').AsCurrency *
              (r.Result.FieldByName('FUI_RATE_REAL').Value - r.Result.FieldByName('FUI_RATE_CREDIT').Value);
            q.Result.Next;
          end;

          CheckWasEdited := (DM_CLIENT.PR_CHECK.State in [dsInsert, dsEdit]);
          if CheckWasEdited then
            DM_CLIENT.PR_CHECK.Post;

          DM_CLIENT.PR_CHECK.First;
          while not DM_CLIENT.PR_CHECK.EOF and (DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsInteger <> CheckNbr) do
          begin
            if (DM_CLIENT.PR_CHECK.PR_NBR.AsInteger = PrNbr)
            and (DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').AsInteger = DM_CLIENT.EE.EE_NBR.AsInteger)
            then
            begin
              YTDCalc := YTDCalc + ConvertNull(DM_CLIENT.PR_CHECK.ER_FUI_TAX.Value, 0);
            end;
            DM_CLIENT.PR_CHECK.Next;
          end;
          DM_CLIENT.PR_CHECK.Locate('PR_CHECK_NBR', CheckNbr, []);

          if CheckWasEdited then
            DM_CLIENT.PR_CHECK.Edit;

          FUITax := FUITax + YTDCalc - YTDFUITax;
        end
        else
          FUITax := FUITax + FUIRate * YTDFUITaxWages - YTDFUITax;
      end;
    end;
  end;
  
// 16-23

  procedure CalcERSDITaxes(EEStateNumber: Integer; var SDITax, SDITaxWages, SDIGrossWages: Real);
  var
    YTDSDITaxWages, YTDSDITax, SDIWageLimit: Real;
    COStateNumber, SYStateNumber: Integer;
    EDCodeType, State: String;
    FollowingCheckDate: TDateTime;
  begin
    DM_CLIENT.CO_STATES.DataRequired('CO_NBR=' + DM_CLIENT.CO.FieldByName('CO_NBR').AsString);
    SDITax := 0;
    SDITaxWages := 0;
    SDIGrossWages := 0;
    with DM_HISTORICAL_TAXES do
    begin
      COStateNumber := EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'CO_STATES_NBR');
      State := DM_CLIENT.CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'STATE');
      SYStateNumber := SY_STATES.Lookup('STATE', State, 'SY_STATES_NBR');

      if ConvertNull(SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE'), 0) = 0 then
        Exit;
      if CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'ER_SDI_EXEMPT') = 'Y' then
        Exit;
      if EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SDI_EXEMPT_EXCLUDE') = 'E' then
        Exit;

      if State = 'NM' then
      begin
        if SecondCheck then
          Exit;
        if DM_CLIENT.PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value = 'Y' then
          Exit;
        if EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SDI_EXEMPT_EXCLUDE') = 'X' then
          Exit;
        FollowingCheckDate := ctx_PayrollCalculation.GetNextCheckDate(DM_CLIENT.PR.FieldByName('CHECK_DATE').Value, DM_CLIENT.CO.FieldByName('PAY_FREQUENCIES').AsString[1]);
        if GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').Value) <> GetQuarterNumber(FollowingCheckDate) then
        begin
          SDITax := SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE');
          if (EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'EE_SDI_EXEMPT_EXCLUDE') <> GROUP_BOX_INCLUDE)
          or (CO_STATES.Lookup('CO_STATES_NBR', COStateNumber, 'EE_SDI_EXEMPT') = 'Y') then
            SDITax := SDITax + SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'EE_SDI_RATE');
        end;
        Exit;
      end;

      ctx_PayrollCalculation.GetLimitedTaxAndDedYTDforGTN(DM_CLIENT.PR_CHECK.FieldByName('PR_CHECK_NBR').Value, EEStateNumber, ltERSDI, DM_CLIENT.PR, DM_CLIENT.PR_CHECK, DM_CLIENT.PR_CHECK_STATES, nil,
        nil, YTDSDITaxWages, YTDSDITax, 0, GetEndYear(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime));
      DM_CLIENT.PR_CHECK_LINES.First;
      while not DM_CLIENT.PR_CHECK_LINES.EOF do
      begin
        if EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([DM_CLIENT.PR_CHECK.FieldByName('EE_NBR').Value, EE_STATES.Lookup('EE_STATES_NBR', DM_CLIENT.PR_CHECK_LINES.FieldByName('EE_STATES_NBR').Value , 'SDI_APPLY_CO_STATES_NBR')]), 'EE_STATES_NBR') = EEStateNumber then
        begin
          EDCodeType := CL_E_DS.ShadowDataSet.CachedLookup(DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'E_D_CODE_TYPE');
          if TypeIsTaxable(EDCodeType) then
            if SY_STATE_EXEMPTIONS.Lookup('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([SYStateNumber, EDCodeType]), 'EXEMPT_EMPLOYER_SDI') <> 'Y' then
              if CL_E_D_STATE_EXMPT_EXCLD.Lookup('SY_STATES_NBR;CL_E_DS_NBR', VarArrayOf([SYStateNumber,
                DM_CLIENT.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value]), 'EMPLOYER_EXEMPT_EXCLUDE_SDI') <> GROUP_BOX_EXE then
              begin
                SDITaxWages := SDITaxWages + ConvertDeduction(EDCodeType, DM_CLIENT.PR_CHECK_LINES.FieldByName('AMOUNT').Value);
              end;
        end;
        DM_CLIENT.PR_CHECK_LINES.Next;
      end;
      SDIGrossWages := SDITaxWages;
      SDIWageLimit := ConvertNull(SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_MAXIMUM_WAGE'), 0);
      if SDITaxWages + YTDSDITaxWages > SDIWageLimit then
        SDITaxWages := SDIWageLimit - YTDSDITaxWages;
      if (DM_CLIENT.PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value = 'Y')
      or (EE_STATES.Lookup('EE_STATES_NBR', EEStateNumber, 'ER_SDI_EXEMPT_EXCLUDE') = 'X') then
      begin
        if (YTDSDITaxWages > SDIWageLimit) and (SDIWageLimit <> 0) then
          SDITaxWages := 0;
        Exit;
      end;

      if (YTDSDITaxWages >= SDIWageLimit) and (SDIWageLimit <> 0) then
      begin
        if SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE') * YTDSDITaxWages <> YTDSdITax then
          SDITax := SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE') * YTDSDITaxWages - YTDSDITax;
        SDITaxWages := 0;
        Exit;
      end;
      SDITax := ConvertNull(SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE'), 0) * SDITaxWages;
      if Abs(ConvertNull(SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE'), 0) * YTDSDITaxWages - YTDSDITax) > TaxShortfallPickupLimit then
        SDITax := SDITax + ConvertNull(SY_STATES.Lookup('SY_STATES_NBR', SYStateNumber, 'ER_SDI_RATE'), 0) * YTDSDITaxWages - YTDSDITax;
    end;
  end;

// 16-3

  function GenerateCheckNumber(SBBankAcctNbr, CLBankAcctNbr: Integer): Integer;
  begin
    if (SBBankAcctNbr = 0) and (CLBankAcctNbr = 0) then
      raise EInvalidParameters.CreateHelp('SBBankAcctNbr and CLBankAcctNbr can not both equal 0 in GenerateCheckNumber function!', IDH_InvalidParameters);

    if CLBankAcctNbr <> 0 then
    begin
      DM_CLIENT.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', CLBankAcctNbr, []);
      Result := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('NEXT_CHECK_NUMBER').Value;
      DM_CLIENT.CL_BANK_ACCOUNT.Edit;
      DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('NEXT_CHECK_NUMBER').Value := DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('NEXT_CHECK_NUMBER').Value + 1;
      DM_CLIENT.CL_BANK_ACCOUNT.Post;
    end
    else
    begin
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', SBBankAcctNbr, []);
      Result := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').Value;

      SB_BANK_ACCOUNTSQuery.Result.Locate('SB_BANK_ACCOUNTS_NBR', SBBankAcctNbr, []);

      if SB_BANK_ACCOUNTSQuery.Result.FieldByName('EFFECTIVE_DATE').AsDateTime > SysTime then
        raise EEvException.Create('SB_BANK_ACCOUNTS.EFFECTIVE_DATE > SysTime');

      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Edit;
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').Value :=
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').Value + 1;
      if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').Value = DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('END_CHECK_NUMBER').Value then
      begin
        if ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_BEGIN_CHECK_NUMBER').Value, 0) = 0 then
        begin
          DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Cancel;
          raise EInconsistentData.CreateHelp('Next beginning check number for '
            + DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NAME_DESCRIPTION').AsString + ' bank account is not setup.', IDH_InconsistentData);
        end;
        if ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_END_CHECK_NUMBER').Value, 0) = 0 then
        begin
          DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Cancel;
          raise EInconsistentData.CreateHelp('Next ending check number for '
            + DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NAME_DESCRIPTION').AsString + ' bank account is not setup.', IDH_InconsistentData);
        end;
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').Value :=
          DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_BEGIN_CHECK_NUMBER').Value;
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('END_CHECK_NUMBER').Value := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_END_CHECK_NUMBER').Value;
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_BEGIN_CHECK_NUMBER').Value := 0;
        DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NEXT_END_CHECK_NUMBER').Value := 0;
      end;
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Post;
    end;
  end;

  procedure CreateAgencyChecks;
  var
    CUSTOM_VIEW_2: TevClientDataSet;
    OneCheckPerEE, MiscCheckCreated: Boolean;
    EEList, PRList: TStringList;
    I: Integer;
    Total: Real;
    BeginDate, EndDate: TDateTime;
    Q: IevQuery;
  begin
    DM_SERVICE_BUREAU.SB_AGENCY.Activate;
    DM_CLIENT.CL_AGENCY.DataRequired('ALL');

    with DM_CLIENT, DM_SERVICE_BUREAU do
    begin
      CL_AGENCY.First;
      while not CL_AGENCY.EOF do
      begin
        if CL_AGENCY.FieldByName('PAYMENT_TYPE').Value = Payment_Type_None then
        begin
          CL_AGENCY.Next;
          Continue;
        end;


        if (CL_AGENCY.FieldByName('FREQUENCY').AsString <> FREQUENCY_TYPE_DAILY)
        and (DM_CLIENT.PR.FieldByName('SCHEDULED').Value = 'N')
        and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT) then
        begin
          CL_AGENCY.Next;
          Continue;
        end;

        case CL_AGENCY.FieldByName('FREQUENCY').AsString[1] of
        FREQUENCY_TYPE_MONTHLY:
          if GetMonth(PR.FieldByName('CHECK_DATE').Value) = GetMonth(ctx_PayrollCalculation.NextCheckDate(PR.FieldByName('CHECK_DATE').Value)) then
          begin
            CL_AGENCY.Next;
            Continue;
          end;
        FREQUENCY_TYPE_QUARTERLY:
          if GetQuarterNumber(PR.FieldByName('CHECK_DATE').Value) = GetQuarterNumber(ctx_PayrollCalculation.NextCheckDate(PR.FieldByName('CHECK_DATE').Value)) then
          begin
            CL_AGENCY.Next;
            Continue;
          end;
        FREQUENCY_TYPE_SEMI_ANNUAL:
          if GetMonth(PR.FieldByName('CHECK_DATE').Value) = GetMonth(ctx_PayrollCalculation.NextCheckDate(PR.FieldByName('CHECK_DATE').Value)) then
          begin
            CL_AGENCY.Next;
            Continue;
          end
          else
          begin
            if (GetMonth(PR.FieldByName('CHECK_DATE').Value) <> StrToInt(ConvertNull(CL_AGENCY.FieldByName('MONTH_NUMBER').Value, '6')))
            and (GetMonth(PR.FieldByName('CHECK_DATE').Value) <> StrToInt(ConvertNull(CL_AGENCY.FieldByName('MONTH_NUMBER').Value, '6')) + 6) then
            begin
              CL_AGENCY.Next;
              Continue;
            end;
          end;
        FREQUENCY_TYPE_ANNUAL:
          if (GetMonth(PR.FieldByName('CHECK_DATE').Value) = GetMonth(ctx_PayrollCalculation.NextCheckDate(PR.FieldByName('CHECK_DATE').Value)))
          or (GetMonth(PR.FieldByName('CHECK_DATE').Value) <> StrToInt(ConvertNull(CL_AGENCY.FieldByName('MONTH_NUMBER').Value, '12'))) then
          begin
            CL_AGENCY.Next;
            Continue;
          end;
        end;

        if (CL_AGENCY.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').Value = 'W') and (CO['WORKERS_COMP_CL_AGENCY_NBR'] = CL_AGENCY['CL_AGENCY_NBR']) then // Base on workers comp for all payrolls in agency period
        begin
          PR_MISCELLANEOUS_CHECKS.Insert;
          PR_MISCELLANEOUS_CHECKS.FieldByName('CL_AGENCY_NBR').Value := CL_AGENCY.FieldByName('CL_AGENCY_NBR').Value;
          Total := ctx_PayrollCalculation.CalcWorkersComp(PayrollNumber, 'CO_NBR', nil,0, True);
          if CL_AGENCY.FieldByName('FREQUENCY').AsString <> FREQUENCY_TYPE_DAILY then
          begin
            case CL_AGENCY.FieldByName('FREQUENCY').AsString[1] of
            FREQUENCY_TYPE_MONTHLY:
            begin
              BeginDate := GetBeginMonth(PR.FieldByName('CHECK_DATE').Value);
              EndDate := GetEndMonth(PR.FieldByName('CHECK_DATE').Value);
            end;
            FREQUENCY_TYPE_QUARTERLY:
            begin
              BeginDate := GetBeginQuarter(PR.FieldByName('CHECK_DATE').Value);
              EndDate := GetEndQuarter(PR.FieldByName('CHECK_DATE').Value);
            end;
            FREQUENCY_TYPE_SEMI_ANNUAL:
            begin
              EndDate := GetEndMonth(PR.FieldByName('CHECK_DATE').Value);
              BeginDate := EndDate;
              for I := 1 to 6 do
                BeginDate := GetBeginMonth(BeginDate - 1);
            end;
            FREQUENCY_TYPE_ANNUAL:
            begin
              EndDate := GetEndMonth(PR.FieldByName('CHECK_DATE').Value);
              BeginDate := IncYear(EndDate, -1) + 1;
            end
            else
              BeginDate := PR.FieldByName('CHECK_DATE').Value;
              EndDate := PR.FieldByName('CHECK_DATE').Value;
            end;

            with ctx_DataAccess.CUSTOM_VIEW do
            begin
              if Active then
                Close;
              with TExecDSWrapper.Create('MaxCheckDateForCo') do
              begin
                SetParam('CheckDate', BeginDate);
                SetParam('Co_Nbr', PR.FieldByName('CO_NBR').AsInteger);
                DataRequest(AsVariant);
              end;
              Open;
              First;
              BeginDate := FieldByName('MAX_CHECK_DATE').AsDateTime + 1;

              Close;
              with TExecDSWrapper.Create('SelectSortedPayrollByCheckDateRunNumber') do
              begin
                SetParam('BeginDate', BeginDate);
                SetParam('EndDate', EndDate);
                SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
                DataRequest(AsVariant);
              end;
              Open;
              First;
              while not EOF do
              begin
                Total := Total + ctx_PayrollCalculation.CalcWorkersComp(FieldByName('PR_NBR').AsInteger, 'CO_NBR', nil,0, False);
                Next;
              end;
              Close;
            end;
          end;
          //Issue#42254 - do not create check with zero amount
          if Total <> 0 then
          begin
            PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value := Total;
            PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_AGENCY;
            PR_MISCELLANEOUS_CHECKS.FieldByName('CHECK_STATUS').Value := MISC_CHECK_STATUS_OUTSTANDING;
            PR_MISCELLANEOUS_CHECKS.Post;
            ctx_DataAccess.PostDataSets([PR_MISCELLANEOUS_CHECKS]);
          end
          else
            PR_MISCELLANEOUS_CHECKS.Cancel;
        end;

        OneCheckPerEE := (CL_AGENCY.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').Value = 'E') or (CL_AGENCY.FieldByName('CLIENT_AGENCY_CUSTOM_FIELD').Value = 'S');
        if ctx_DataAccess.CUSTOM_VIEW.Active then
          ctx_DataAccess.CUSTOM_VIEW.Close;
        EEList := TStringList.Create;
        PRList := TStringList.Create;
        CUSTOM_VIEW_2 := TevClientDataSet.Create(nil);
        try
          if OneCheckPerEE or (CL_AGENCY.FieldByName('FREQUENCY').Value = FREQUENCY_TYPE_TARGET_MET) then
          begin
            if CL_AGENCY.FieldByName('FREQUENCY').Value <> FREQUENCY_TYPE_TARGET_MET then
            with TExecDSWrapper.Create('CalcAgencyChecksByEE') do
            begin
              SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
              SetParam('Status', PAYROLL_STATUS_PROCESSED);
              SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
              SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
              SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end
            else
            with TExecDSWrapper.Create('CalcAgencyChecksByEETargetMet') do
            begin
              SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
              SetParam('Status', PAYROLL_STATUS_PROCESSED);
              SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
              SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
              SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end;
            ctx_DataAccess.CUSTOM_VIEW.Open;
            ctx_DataAccess.CUSTOM_VIEW.First;
            Total := 0;
            while not ctx_DataAccess.CUSTOM_VIEW.EOF do
            begin
              if OneCheckPerEE then
                EEList.Add(ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsString + '=' + FloatToStr(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, 0)))
              else
                Total := Total + ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, 0);
              ctx_DataAccess.CUSTOM_VIEW.Next;
            end;
            if not OneCheckPerEE then
              EEList.Add('0=' + FloatToStr(Total));
          end
          else
          begin
            with TExecDSWrapper.Create('SumAgencyChecks') do
            begin
              SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
              SetParam('Status', PAYROLL_STATUS_PROCESSED);
              SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
              SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
              SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end;
            ctx_DataAccess.CUSTOM_VIEW.Open;
            ctx_DataAccess.CUSTOM_VIEW.First;
            EEList.Add('0=' + FloatToStr(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0)));
          end;
          ctx_DataAccess.CUSTOM_VIEW.Close;

          if CL_AGENCY.FieldByName('FREQUENCY').Value <> FREQUENCY_TYPE_TARGET_MET then
          with TExecDSWrapper.Create('CalcCheckLineForAgencyCheck') do
          begin
            SetParam('PrNbr', PayrollNumber);
            SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
            SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
            ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          end
          else
          with TExecDSWrapper.Create('CalcCheckLineForAgencyCheckTargetMet') do
          begin
            SetParam('PrNbr', PayrollNumber);
            SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
            SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
            ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          end;
          ctx_DataAccess.CUSTOM_VIEW.Open;
          ctx_DataAccess.CUSTOM_VIEW.First;
          while not ctx_DataAccess.CUSTOM_VIEW.EOF do
          begin
            if OneCheckPerEE then
            begin
              if EEList.IndexOfName(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsString) = -1 then
                EEList.Add(ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsString + '=' + FloatToStr(ConvertNull(PR_CHECK_LINES.Lookup('PR_CHECK_LINES_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_CHECK_LINES_NBR').Value, 'AMOUNT'), 0)))
              else
                EEList.Values[ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsString] := FloatToStr(StrToFloat(EEList.Values[ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsString])
                  + ConvertNull(PR_CHECK_LINES.Lookup('PR_CHECK_LINES_NBR', ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_CHECK_LINES_NBR').Value, 'AMOUNT'), 0))
            end
            else
              EEList.Values['0'] := FloatToStr(StrToFloat(EEList.Values['0']) + ConvertNull(PR_CHECK_LINES.Lookup('PR_CHECK_LINES_NBR',
                ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_CHECK_LINES_NBR').Value, 'AMOUNT'), 0));
            ctx_DataAccess.CUSTOM_VIEW.Next;
          end;

          CUSTOM_VIEW_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
          ctx_DataAccess.CUSTOM_VIEW.Close;
          MiscCheckCreated := False;

          for I := 0 to EEList.Count - 1 do
          if (StrToFloat(EEList.Values[EEList.Names[I]]) > 0) or ((StrToFloat(EEList.Values[EEList.Names[I]]) < 0)
          and (CL_AGENCY.FieldByName('PAYMENT_TYPE').Value = Payment_Type_DirectDeposit)
          and (SB_AGENCY.Lookup('SB_AGENCY_NBR', CL_AGENCY.FieldByName('SB_AGENCY_NBR').Value, 'NEGATIVE_DIRECT_DEP_ALLOWED') = 'Y')) then
          begin
            PostACH := True;
            MiscCheckCreated := True;
            PR_MISCELLANEOUS_CHECKS.Insert;
            PR_MISCELLANEOUS_CHECKS.FieldByName('CL_AGENCY_NBR').Value := CL_AGENCY.FieldByName('CL_AGENCY_NBR').Value;
            PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value := StrToFloat(EEList.Values[EEList.Names[I]]);
            EEList.Values[EEList.Names[I]] := PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString;
            PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value := MISC_CHECK_TYPE_AGENCY;
            PR_MISCELLANEOUS_CHECKS.FieldByName('CHECK_STATUS').Value := MISC_CHECK_STATUS_OUTSTANDING;
            PR_MISCELLANEOUS_CHECKS.Post;
          end
          else
            EEList.Values[EEList.Names[I]] := '0';

          if not MiscCheckCreated and (PR_MISCELLANEOUS_CHECKS.RecordCount > 0) then
          begin
            PR_MISCELLANEOUS_CHECKS.First;
            while not PR_MISCELLANEOUS_CHECKS.Eof do
            begin
              if (ConvertNull(CL_AGENCY.Lookup('CL_AGENCY_NBR', PR_MISCELLANEOUS_CHECKS['CL_AGENCY_NBR'], 'PAYMENT_TYPE'),'') = Payment_Type_DirectDeposit)
              and (SB_AGENCY.Lookup('SB_AGENCY_NBR', ConvertNull(CL_AGENCY.Lookup('CL_AGENCY_NBR', PR_MISCELLANEOUS_CHECKS['CL_AGENCY_NBR'], 'SB_AGENCY_NBR'),0), 'NEGATIVE_DIRECT_DEP_ALLOWED') = 'Y') then
              begin
                PostACH := True;
                Break;
              end;
              PR_MISCELLANEOUS_CHECKS.Next;
            end;
          end;

          if MiscCheckCreated then
          begin
// Change agency status of all pending agency check lines in the current payroll to "sent" and assign new misc. check number to them
            for I := 0 to EEList.Count - 1 do
            if EEList.Values[EEList.Names[I]] <> '0' then
            begin
              CUSTOM_VIEW_2.First;
              ctx_DataAccess.RecalcLineBeforePost := False;
              try
                while not CUSTOM_VIEW_2.EOF do
                begin
                  if PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', CUSTOM_VIEW_2.FieldByName('PR_CHECK_LINES_NBR').Value, [])
                  and ((PR_CHECK.Lookup('PR_CHECK_NBR', PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value, 'EE_NBR') = StrToInt(EEList.Names[I])) or (not OneCheckPerEE)) then
                  begin
                    PR_CHECK_LINES.Edit;
                    PR_CHECK_LINES.FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_SENT;
                    PR_CHECK_LINES.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString := EEList.Values[EEList.Names[I]];
                    PR_CHECK_LINES.Post;
                  end;
                  CUSTOM_VIEW_2.Next;
                end;
              finally
                ctx_DataAccess.RecalcLineBeforePost := True;
              end;
            end;

// Change agency status of all pending agency check lines in prior processed payrolls to "sent" and assign new misc. check number to them
            ctx_DataAccess.CUSTOM_VIEW.Close;
            if CL_AGENCY.FieldByName('FREQUENCY').Value = FREQUENCY_TYPE_TARGET_MET then
            with TExecDSWrapper.Create('CalcAgencyChecksForEETargetMet') do
            begin
              SetMacro('Columns', 'P.PR_NBR,C.EE_NBR');
              SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
              SetParam('Status', PAYROLL_STATUS_PROCESSED);
              SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
              SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
              SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end
            else
            if OneCheckPerEE then
            with TExecDSWrapper.Create('CalcAgencyChecksForEE') do
            begin
              SetMacro('Columns', 'P.PR_NBR,C.EE_NBR');
              SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
              SetParam('Status', PAYROLL_STATUS_PROCESSED);
              SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
              SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
              SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end
            else
            with TExecDSWrapper.Create('CalcAgencyChecks') do
            begin
              SetMacro('Columns', 'distinct L.PR_NBR');
              SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
              SetParam('Status', PAYROLL_STATUS_PROCESSED);
              SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
              SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
              SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end;
            ctx_DataAccess.CUSTOM_VIEW.Open;
            for I := 0 to EEList.Count - 1 do
            if EEList.Values[EEList.Names[I]] <> '0' then
            begin
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                if (not OneCheckPerEE) or (ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsString = EEList.Names[I]) then
                begin
                  if PRList.IndexOfName(ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_NBR').AsString) = -1 then
                    PRList.Add(ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_NBR').AsString);
                end;
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;
            end;

            for I := 0 to PRList.Count - 1 do
              ctx_PayrollProcessing.SetPayrollLock(StrToInt(PRList[I]), True);

            try
              ctx_DataAccess.CUSTOM_VIEW.Close;
              if CL_AGENCY.FieldByName('FREQUENCY').Value = FREQUENCY_TYPE_TARGET_MET then
              with TExecDSWrapper.Create('pr_check_lines;CalcAgencyChecksForEETargetMet') do
              begin
                SetMacro('Columns', 'L.*,C.EE_NBR');
                SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
                SetParam('Status', PAYROLL_STATUS_PROCESSED);
                SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
                SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
                SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end
              else
              if OneCheckPerEE then
              with TExecDSWrapper.Create('pr_check_lines;CalcAgencyChecksForEE') do
              begin
                SetMacro('Columns', 'L.*,C.EE_NBR');
                SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
                SetParam('Status', PAYROLL_STATUS_PROCESSED);
                SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
                SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
                SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end
              else
              with TExecDSWrapper.Create('pr_check_lines;CalcAgencyChecks') do
              begin
                SetMacro('Columns', 'L.*');
                SetParam('CoNbr', PR.FieldByName('CO_NBR').AsInteger);
                SetParam('Status', PAYROLL_STATUS_PROCESSED);
                SetParam('AgencyNbr', CL_AGENCY.FieldByName('CL_AGENCY_NBR').AsInteger);
                SetParam('AgencyStatus', CHECK_LINE_AGENCY_STATUS_PENDING);
                SetParam('CheckDate', DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              for I := 0 to EEList.Count - 1 do
              if EEList.Values[EEList.Names[I]] <> '0' then
              begin
                ctx_DataAccess.CUSTOM_VIEW.First;
                while not ctx_DataAccess.CUSTOM_VIEW.EOF do
                begin
                  if (not OneCheckPerEE) or (ctx_DataAccess.CUSTOM_VIEW.FieldByName('EE_NBR').AsString = EEList.Names[I]) then
                  begin
                    ctx_DataAccess.CUSTOM_VIEW.Edit;
                    ctx_DataAccess.CUSTOM_VIEW.FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_SENT;
                    ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString := EEList.Values[EEList.Names[I]];
                    ctx_DataAccess.CUSTOM_VIEW.Post;
                  end;
                  ctx_DataAccess.CUSTOM_VIEW.Next;
                end;
              end;
              Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
              Q.Execute;
              ctx_DataAccess.PostDataSets([PR_MISCELLANEOUS_CHECKS, ctx_DataAccess.CUSTOM_VIEW]);
            finally
              for I := 0 to PRList.Count - 1 do
                ctx_PayrollProcessing.SetPayrollLock(StrToInt(PRList[I]), False);
            end;
          end;
        finally
          EEList.Free;
          PRList.Free;
          CUSTOM_VIEW_2.Free;
        end;
        ctx_DataAccess.CUSTOM_VIEW.Close;
        CL_AGENCY.Next;
      end;
    end;
  end;

  procedure CreateImpoundCheck(ACheckType: Boolean; Amount: Real);
  begin
    if Amount <> 0 then
    begin
      DM_CLIENT.PR_MISCELLANEOUS_CHECKS.Insert;
      if ACheckType = CREATE_TAX_CHECK then
      begin
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.CUSTOM_PR_BANK_ACCT_NUMBER.Value :=
          DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', DM_CLIENT.CO.TAX_CL_BANK_ACCOUNT_NBR.Value, 'CUSTOM_BANK_ACCOUNT_NUMBER');
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.EFTPS_TAX_TYPE.Value := 'T';
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_AMOUNT.Value := Amount;
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.PAYMENT_SERIAL_NUMBER.Value := ctx_PayrollCalculation.GetNextPaymentSerialNbr; //GenerateCheckNumber(0, CO.FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value);
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.OVERRIDE_INFORMATION.AsString := 'Tax Impound Check';
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_TYPE.Value := MISC_CHECK_TYPE_BILLING;
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.CHECK_STATUS.Value := MISC_CHECK_STATUS_OUTSTANDING;
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.Post;
      end
      else
      if ACheckType = CREATE_TRUST_CHECK then
      begin
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.CUSTOM_PR_BANK_ACCT_NUMBER.Value :=
          DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', DM_CLIENT.CO.PAYROLL_CL_BANK_ACCOUNT_NBR.Value, 'CUSTOM_BANK_ACCOUNT_NUMBER');
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.EFTPS_TAX_TYPE.Value := 'R';
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_AMOUNT.Value := Amount;
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.PAYMENT_SERIAL_NUMBER.Value := ctx_PayrollCalculation.GetNextPaymentSerialNbr; //GenerateCheckNumber(0, CO.FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value);
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.OVERRIDE_INFORMATION.AsString := 'Trust Impound Check';
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.MISCELLANEOUS_CHECK_TYPE.Value := MISC_CHECK_TYPE_BILLING;
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.CHECK_STATUS.Value := MISC_CHECK_STATUS_OUTSTANDING;
        DM_CLIENT.PR_MISCELLANEOUS_CHECKS.Post;
      end;
    end;
  end;

  procedure DeleteIt(aDataSet: TevClientDataSet; CheckNbr: Integer);
  begin
    aDataSet.First;
    while not aDataSet.EOF do
    begin
      if aDataSet.FieldByName('PR_CHECK_NBR').AsInteger = CheckNbr then
        aDataSet.Delete
      else
        aDataSet.Next;
    end;
  end;

  procedure FlipPostACHforTax;
  begin
    if PostACH then
      Exit;
    if (DM_CLIENT.CO.FieldByName('TAX_SERVICE').Value = 'Y')
    and (DM_CLIENT.PR.TAX_IMPOUND.AsString <> PAYMENT_TYPE_NONE)
    and (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
      PostACH := True;
  end;

  procedure PostFederalLiability(vAmount: Variant; LiabType, ThirdParty: Char);
  begin
    if ConvertNull(vAmount, 0) <> 0 then
    begin
      DM_CLIENT.CO_FED_TAX_LIABILITIES.Insert;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('DUE_DATE').Value := DM_CLIENT.PR.FieldByName('CHECK_DATE').Value;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('CHECK_DATE').Value := DM_CLIENT.PR.FieldByName('CHECK_DATE').Value;
      if (ThirdParty = 'Y') or IsImport or Backdated then
        DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
      else
        DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
      if Backdated then
        DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').Value := LiabType;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(vAmount);
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('THIRD_PARTY').Value := ThirdParty;
      DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('ACH_KEY').AsString := FormatDateTime('MM/DD/YYYY', GetEndQuarter(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime))+ Format('%1u', [GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime)]);
      DM_CLIENT.CO_FED_TAX_LIABILITIES.Post;
      FlipPostACHforTax;
      if ThirdParty = 'N' then
        Impound := Impound + vAmount;
    end;
  end;

  procedure PostStateLiability(vAmount: Variant; LiabType, ThirdParty: Char; StateNbr, StateDepFreqNbr, SyStateNbr: Integer);
  begin
    if ConvertNull(vAmount, 0) <> 0 then
    begin
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.Insert;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('CO_STATES_NBR').Value := StateNbr;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('DUE_DATE').Value := DM_CLIENT.PR.FieldByName('CHECK_DATE').Value;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('CHECK_DATE').Value := DM_CLIENT.PR.FieldByName('CHECK_DATE').Value;
      if (ThirdParty = 'Y') or IsImport or Backdated then
        DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
      else
        DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
      if Backdated then
        DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('TAX_TYPE').Value := LiabType;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(vAmount);
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('THIRD_PARTY').Value := ThirdParty;

      DM_CLIENT.CO_STATE_TAX_LIABILITIES.FieldByName('ACH_KEY').AsString :=
                  ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(STATE_TAX_DESC, StateDepFreqNbr, SyStateNbr,
                                              DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime);

      DM_CLIENT.CO_STATE_TAX_LIABILITIES.Post;
      FlipPostACHforTax;
      if ThirdParty = 'N' then
        Impound := Impound + vAmount;
    end;
  end;

  procedure AddHint(const s: string);
  begin
    if Result <> '' then
      Result := Result+ #13;
    Result := Result+ s;
  end;

var
  CONYStateNbr: integer;
  CalcDueDates: TevCalcDueDates;
  Warnings: String;
  CheckAccounts: IisParamsCollection;
  CheckInfo: IisListOfValues;
  nbr, PAcnt, PAidx, SyLocNbr: Integer;
  PaTaxes, PaChecks, Params, Locs: IisListOfValues;
  PALocals: IisParamsCollection;
  LocTax, LocWage, LocWageTotal, LocTaxAmount: Real;
  NonResNbr: String;
begin
// Initialization part
  PaChecks := TisListOfValues.Create;
  CheckProcessingIndex := TisListOfValues.Create;
  AddToPayrollLog('Starting Payroll Main Processing');
  AbortIfCurrentThreadTaskTerminated;
  CalcDueDates := TevCalcDueDates.Create;
  try
    Result := '';
    HasQTDs := False;
    NewPayrollNbr := 0;

    if DM_CLIENT.PR.Active then
      DM_CLIENT.PR.Close;
    DM_CLIENT.PR.DataRequired('PR_NBR=' + IntToStr(PayrollNumber));

    if DM_CLIENT.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSING then
      raise ECanNotPerformOperation.CreateHelp('This payroll is already being processed. Can not process the same payroll twice!', IDH_CanNotPerformOperation);
    if DM_CLIENT.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PREPROCESSING then
      raise ECanNotPerformOperation.CreateHelp('This payroll is already being pre-processed. Can not process the same payroll twice!', IDH_CanNotPerformOperation);
    if not JustPreProcess and (DM_CLIENT.PR.FieldByName('STATUS').Value <> PAYROLL_STATUS_COMPLETED) then
      raise ECanNotPerformOperation.CreateHelp('The payroll is not marked as completed. Can not process.', IDH_CanNotPerformOperation);

    if PrBatchNbr <> 0 then
      DM_CLIENT.PR_CHECK.DataRequired('PR_BATCH_NBR=' + IntToStr(PrBatchNbr))
    else
      DM_CLIENT.PR_CHECK.DataRequired('PR_NBR=' + IntToStr(PayrollNumber));

    if DM_CLIENT.PR_CHECK.Indexes.Get('ChecksSorting') = nil then
      DM_CLIENT.PR_CHECK.AddIndex('ChecksSorting', 'EE_NBR;SortCheckType;PAYMENT_SERIAL_NUMBER', []);

    DM_CLIENT.PR_CHECK.IndexName := 'ChecksSorting';

    if PrBatchNbr <> 0 then
      DM_CLIENT.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=0')
    else
      DM_CLIENT.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=' + IntToStr(PayrollNumber));
  // Make sure the payroll has checks
    if ((DM_CLIENT.PR_CHECK.RecordCount = 0) and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT) and
    (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_TAX_DEPOSIT))
    or ((DM_CLIENT.PR_MISCELLANEOUS_CHECKS.RecordCount = 0) and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString[1] in
    [PAYROLL_TYPE_TAX_DEPOSIT])) then
      raise ECanNotPerformOperation.CreateHelp('The payroll has no checks! Nothing to process.', IDH_CanNotPerformOperation);

    ctx_DataAccess.PayrollIsProcessing := True;

    DM_CLIENT.PR.Edit;
    if JustPreProcess then
      DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PREPROCESSING
    else
      DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSING;
    DM_CLIENT.PR.FieldByName('STATUS_DATE').Value := Now;
    DM_CLIENT.PR.Post;

    ctx_DataAccess.PostDataSets([DM_CLIENT.PR]);

    try
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
      DM_CLIENT.CL.DataRequired('ALL');
      DM_CLIENT.CL_E_DS.DataRequired('ALL');
      DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired('ALL');
      DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
      DM_CLIENT.CO.DataRequired('ALL');
      Assert(DM_CLIENT.CO.Locate('CO_NBR', DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger, []));
      if not (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT]) then
      begin
        if DM_CLIENT.CO.FieldByName('CREDIT_HOLD').AsString = CREDIT_HOLD_LEVEL_MAINT then
          raise ECanNotPerformOperation.CreateHelp('The company is under maintenance. Can not process payroll.', IDH_CanNotPerformOperation);
        if (DM_CLIENT.CO.FieldByName('CREDIT_HOLD').AsString = CREDIT_HOLD_LEVEL_MEDIUM)
        or (DM_CLIENT.CO.FieldByName('CREDIT_HOLD').AsString = CREDIT_HOLD_LEVEL_HIGH) then
          raise ECanNotPerformOperation.CreateHelp('The company is on Medium or High level of Credit Hold. Can not process payroll.', IDH_CanNotPerformOperation);
      end;
      DM_CLIENT.CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_BANK_ACCOUNT_REGISTER_NBR=0'{'CO_NBR=' + DM_CLIENT.PR.FieldByName('CO_NBR').AsString});

      DM_CLIENT.CO_FED_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
      DM_CLIENT.CO_STATE_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
      DM_CLIENT.CO_SUI_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
      DM_CLIENT.CO_LOCAL_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');

      DM_CLIENT.CO_TAX_DEPOSITS.DataRequired('CO_NBR=' + DM_CLIENT.PR.FieldByName('CO_NBR').AsString);
      DM_CLIENT.CO_PR_ACH.DataRequired('CO_NBR=' + DM_CLIENT.PR.FieldByName('CO_NBR').AsString);
      DM_CLIENT.EE.DataRequired('CO_NBR=' + DM_CLIENT.PR.FieldByName('CO_NBR').AsString);

      ctx_DataAccess.OpenEEDataSetForCompany(DM_CLIENT.EE_SCHEDULED_E_DS, DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_CLIENT.EE_RATES, DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_CLIENT.EE_CHILD_SUPPORT_CASES, DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_CLIENT.EE_PENSION_FUND_SPLITS, DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_CLIENT.EE_STATES, DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.OpenEEDataSetForCompany(DM_CLIENT.EE_LOCALS, DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger);

      OpenDataSetForPayroll(DM_CLIENT.PR_CHECK_LINES, PayrollNumber);
      OpenDataSetForPayroll(DM_CLIENT.PR_CHECK_STATES, PayrollNumber);
      OpenDataSetForPayroll(DM_CLIENT.PR_CHECK_SUI, PayrollNumber);
      OpenDataSetForPayroll(DM_CLIENT.PR_CHECK_LOCALS, PayrollNumber);

      with ctx_DataAccess.CUSTOM_VIEW, DM_CLIENT do
      if not PR_CHECK_LINE_LOCALS.Active or (PR_CHECK_LINE_LOCALS.ClientID <> ClientID)
      or (PR_CHECK_LINE_LOCALS.OpenCondition <> 'PR_NBR = ' + IntToStr(PayrollNumber)) then
      begin
        if Active then
          Close;
        if PR_CHECK_LINE_LOCALS.Active then
          PR_CHECK_LINE_LOCALS.Close;
        with TExecDSWrapper.Create('SelectCheckLineLocals') do
        begin
          SetParam('PrNbr', PayrollNumber);
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        end;
        ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
        PR_CHECK_LINE_LOCALS.ClientID := ctx_DataAccess.ClientID;
        PR_CHECK_LINE_LOCALS.Data := Data;
        PR_CHECK_LINE_LOCALS.SetOpenCondition('PR_NBR = ' + IntToStr(PayrollNumber));
      end;

      if ((DM_CLIENT.CO.FieldByName('TRUST_SERVICE').Value = 'Y') and not (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT]))
      or ((DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT]) and (DM_CLIENT.CO.FieldByName('TAX_SERVICE').Value = 'Y')) then
        if (DM_CLIENT.PR.TRUST_IMPOUND.AsString <> PAYMENT_TYPE_NONE) and (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
          PostACH := True;

      IsImport := (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_IMPORT);
      Backdated := (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_BACKDATED) and (DM_CLIENT.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value = 'Y');

      PR_CHECK_2 := TevClientDataSet.Create(Nil);
      try
        ctx_DataAccess.StartNestedTransaction([dbtClient, dbtBureau]);
        try
          MyTransactionManager := TTransactionManager.Create(nil);

          with DM_CLIENT do
          if (PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP) and (not JustPreprocess) then
      // Create an empty shell for a new payroll for YTDs
          begin
            PR_CHECK.First;
            while not PR_CHECK.EOF do
            begin
              if PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_QTD then
              begin
                HasQTDs := True;
                Break;
              end;
              PR_CHECK.Next;
              AbortIfCurrentThreadTaskTerminated;
            end;

            if HasQTDs then
            begin
              if GetQuarterNumber(PR.FieldByName('CHECK_DATE').Value) = 1 then
                raise EInconsistentData.CreateHelp('First quarter setup run can not have quarter to date checks.', IDH_InconsistentData);
              MyTransactionManager.Initialize([PR]);
              TempDataSet := TevClientDataSet.Create(nil);
              try
                TempDataSet.Data := PR.Data;
                TempDataSet.Locate('PR_NBR', PayrollNumber, []);
                PR.Insert;
                for I := 0 to TempDataSet.FieldCount - 1 do
                begin
                  if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') { plymouth and (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE')}
                  and (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY') and (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE')
                  {and (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD') plymouth} then
                    PR.Fields[I].Value := TempDataSet.Fields[I].Value;
                end;
                PR.TAX_IMPOUND.Value := DM_CLIENT.CO.TAX_IMPOUND.Value;
                PR.TRUST_IMPOUND.Value := DM_CLIENT.CO.TRUST_IMPOUND.Value;
                PR.BILLING_IMPOUND.Value := DM_CLIENT.CO.BILLING_IMPOUND.Value;
                PR.DD_IMPOUND.Value := DM_CLIENT.CO.DD_IMPOUND.Value;
                PR.WC_IMPOUND.Value := DM_CLIENT.CO.WC_IMPOUND.Value;
                PR.FieldByName('PAYROLL_TYPE').Value := PAYROLL_TYPE_SETUP;
                PR.FieldByName('SCHEDULED').Value := 'N';
                PR.FieldByName('CHECK_DATE').Value := GetBeginQuarter(PR.FieldByName('CHECK_DATE').Value) - 1;
                PR.FieldByName('RUN_NUMBER').Value := ctx_PayrollCalculation.GetNextRunNumber(PR.FieldByName('CHECK_DATE').Value);
                PR.Post;
              finally
                TempDataSet.Free;
              end;
              MyTransactionManager.ApplyUpdates;
              NewPayrollNbr := PR.FieldByName('PR_NBR').Value;
            end;
          end;

          DM_CLIENT.PR_BATCH.DataRequired('PR_NBR=' + IntToStr(PayrollNumber));

          MyTransactionManager.Initialize([DM_CLIENT.PR_BATCH, DM_CLIENT.PR_CHECK, DM_CLIENT.PR_CHECK_STATES, DM_CLIENT.PR_CHECK_SUI
            , DM_CLIENT.PR_CHECK_LOCALS, DM_CLIENT.PR_MISCELLANEOUS_CHECKS, DM_CLIENT.PR_CHECK_LINES, DM_CLIENT.CO_BANK_ACCOUNT_REGISTER
              , DM_CLIENT.CL_BANK_ACCOUNT, DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS, DM_CLIENT.EE_SCHEDULED_E_DS, DM_CLIENT.PR]);
          DM_CLIENT.PR.Locate('PR_NBR', PayrollNumber, []);

          with DM_CLIENT do
          begin
            ctx_PayrollCalculation.EraseLimitedEDs;
            ctx_PayrollCalculation.GetLimitedEDs(PR.FieldByName('CO_NBR').AsInteger, PR.FieldByName('PR_NBR').AsInteger,
              GetBeginYear(PR.FieldByName('CHECK_DATE').AsDateTime), PR.FieldByName('CHECK_DATE').AsDateTime);
            ctx_PayrollCalculation.GetLimitedTaxes(PR.FieldByName('CO_NBR').AsInteger, PR.FieldByName('PR_NBR').AsInteger,
              GetBeginYear(PR.FieldByName('CHECK_DATE').AsDateTime), PR.FieldByName('CHECK_DATE').AsDateTime);
          end;

          PR_CHECK_LINES_2 := TevClientDataSet.Create(Nil);
          PR_CHECK_STATES_2 := TevClientDataSet.Create(Nil);
          PR_CHECK_SUI_2 := TevClientDataSet.Create(Nil);
          PR_CHECK_LOCALS_2 := TevClientDataSet.Create(Nil);

        // 16-1
          with DM_CLIENT, DM_SERVICE_BUREAU do
          try
            if ctx_DataAccess.CUSTOM_VIEW.Active then
              ctx_DataAccess.CUSTOM_VIEW.Close;
            with TExecDSWrapper.Create(
              'select #Columns ' +
              'from PR_CHECK C ' +
              'join EE E on C.EE_NBR=E.EE_NBR ' +
              'join CL_PERSON P on E.CL_PERSON_NBR=P.CL_PERSON_NBR ' +
              'left outer join CO_DIVISION V on E.CO_DIVISION_NBR=V.CO_DIVISION_NBR and {AsOfNow<V>} ' +
              'left outer join CO_BRANCH A on E.CO_BRANCH_NBR=A.CO_BRANCH_NBR and {AsOfNow<A>} ' +
              'left outer join CO_DEPARTMENT D on E.CO_DEPARTMENT_NBR=D.CO_DEPARTMENT_NBR and {AsOfNow<D>} ' +
              'left outer join CO_TEAM T on E.CO_TEAM_NBR=T.CO_TEAM_NBR and {AsOfNow<T>} ' +
              'where #NBRFIELD_NBR=:RecordNbr and {AsOfNow<C>} and {AsOfNow<E>} and {AsOfNow<P>}') do
            begin
              SetMacro('Columns', 'P.SOCIAL_SECURITY_NUMBER, P.LAST_NAME, P.FIRST_NAME, E.CUSTOM_EMPLOYEE_NUMBER, '
                + 'E.CO_DIVISION_NBR, E.CO_BRANCH_NBR, E.CO_DEPARTMENT_NBR, E.CO_TEAM_NBR, C.PR_CHECK_NBR, C.EE_NBR, C.UPDATE_BALANCE,'
                + 'C.CHECK_TYPE, C.CHECK_TYPE SortCheckType, C.CHECK_STATUS, C.PAYMENT_SERIAL_NUMBER, '
                + 'C.OR_CHECK_FEDERAL_VALUE, C.OR_CHECK_OASDI, C.OR_CHECK_MEDICARE, C.OR_CHECK_EIC, C.OR_CHECK_BACK_UP_WITHHOLDING, V.CUSTOM_DIVISION_NUMBER, '
                + 'A.CUSTOM_BRANCH_NUMBER, D.CUSTOM_DEPARTMENT_NUMBER, T.CUSTOM_TEAM_NUMBER, C.PR_NBR, C.RECIPROCATE_SUI');
              if PrBatchNbr <> 0 then
              begin
                SetMacro('NbrField', 'C.PR_BATCH');
                SetParam('RecordNbr', PrBatchNbr);
              end
              else
              begin
                SetMacro('NbrField', 'PR');
                SetParam('RecordNbr', PayrollNumber);
              end;
              ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
            end;
            ctx_DataAccess.CUSTOM_VIEW.Open;
            PR_CHECK_2.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
            ctx_DataAccess.CUSTOM_VIEW.Close;
            PR_CHECK_2.First;
            while not PR_CHECK_2.EOF do
            begin
              PR_CHECK_2.Edit;
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
                PR_CHECK_2.FieldByName('SortCheckType').Value := 'A';
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOUCHER then
                PR_CHECK_2.FieldByName('SortCheckType').Value := 'B';
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_MANUAL then
              begin
                if PR_CHECK_2.FieldByName('OR_CHECK_OASDI').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_MEDICARE').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_EIC').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').IsNull then
                  PR_CHECK_2.FieldByName('SortCheckType').Value := 'D'
                else
                  PR_CHECK_2.FieldByName('SortCheckType').Value := 'C'
              end
              else
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_YTD then
                PR_CHECK_2.FieldByName('SortCheckType').Value := 'E';
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_QTD then
                PR_CHECK_2.FieldByName('SortCheckType').Value := 'F';
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_REGULAR then
              begin
                if PR_CHECK_2.FieldByName('OR_CHECK_OASDI').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_MEDICARE').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_EIC').IsNull
                and PR_CHECK_2.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').IsNull then
                  PR_CHECK_2.FieldByName('SortCheckType').Value := 'H'
                else
                  PR_CHECK_2.FieldByName('SortCheckType').Value := 'G'
              end
              else
              if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_3RD_PARTY then
                PR_CHECK_2.FieldByName('SortCheckType').Value := 'I';
              PR_CHECK_2.Post;
              PR_CHECK_2.Next;
            end;
            if PR_CHECK_2.Indexes.Get('ChecksSorting') = nil then
              PR_CHECK_2.AddIndex('ChecksSorting', StringReplace(ctx_PayrollCalculation.ConstructCheckSortIndex, 'CHECK_TYPE', 'SortCheckType', []), []);
            {with PR_CHECK_2.IndexDefs.AddIndexDef do
            begin
              Name := 'ChecksSorting';
              Fields := StringReplace(ctx_PayrollCalculation.ConstructCheckSortIndex, 'CHECK_TYPE', 'SortCheckType', []);
            end;}
            PR_CHECK_2.IndexName := 'ChecksSorting';

            if HasQTDs then
            // Backout QTD from YTD
            begin
              PostFeedback(True, 'Backing out QTDs from YTDs...');
              PR_CHECK_2.First;
              while not PR_CHECK_2.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_QTD then
                  if PR_CHECK.Locate('EE_NBR;CHECK_TYPE', VarArrayOf([PR_CHECK_2.FieldByName('EE_NBR').Value, CHECK_TYPE2_YTD]), []) then
                  begin
                    PR_CHECK.Edit;
                    PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value := PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value -
                      ConvertNull(PR_CHECK_2.FieldByName('OR_CHECK_FEDERAL_VALUE').Value, 0);
                    PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := PR_CHECK.FieldByName('OR_CHECK_OASDI').Value -
                      ConvertNull(PR_CHECK_2.FieldByName('OR_CHECK_OASDI').Value, 0);
                    PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value -
                      ConvertNull(PR_CHECK_2.FieldByName('OR_CHECK_MEDICARE').Value, 0);
                    PR_CHECK.FieldByName('OR_CHECK_EIC').Value := PR_CHECK.FieldByName('OR_CHECK_EIC').Value -
                      ConvertNull(PR_CHECK_2.FieldByName('OR_CHECK_EIC').Value, 0);
                    PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value := PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value -
                      ConvertNull(PR_CHECK_2.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value, 0);
                    PR_CHECK.Post;

                    with PR_CHECK_LINES_2 do
                    begin
                      Data := PR_CHECK_LINES.Data;
                      Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
                      Filtered := True;
                      PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                      PR_CHECK_LINES.Filtered := True;
                      try
                        First;
                        while not EOF do
                        begin
                          AbortIfCurrentThreadTaskTerminated;                        
                          if PR_CHECK_LINES.Locate('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, []) then
                          begin
                            PR_CHECK_LINES.Edit;
                            PR_CHECK_LINES.FieldByName('AMOUNT').Value := ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0) -
                              ConvertNull(FieldByName('AMOUNT').Value, 0);
                            PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value := ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0) -
                              ConvertNull(FieldByName('HOURS_OR_PIECES').Value, 0);
                            PR_CHECK_LINES.Post;
                          end;
                          Next;
                        end;
                      finally
                        Filtered := False;
                        Filter := '';
                        PR_CHECK_LINES.Filtered := False;
                        PR_CHECK_LINES.Filter := '';
                      end;
                    end;

                    with PR_CHECK_STATES_2 do
                    begin
                      Data := PR_CHECK_STATES.Data;
                      Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
                      Filtered := True;
                      PR_CHECK_STATES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                      PR_CHECK_STATES.Filtered := True;
                      try
                        First;
                        while not EOF do
                        begin
                          AbortIfCurrentThreadTaskTerminated;
                          if PR_CHECK_STATES.Locate('EE_STATES_NBR', FieldByName('EE_STATES_NBR').Value, []) then
                          begin
                            PR_CHECK_STATES.Edit;
                            PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value := PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value
                              - ConvertNull(FieldByName('STATE_OVERRIDE_VALUE').Value, 0);
                            PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value := PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value
                              - ConvertNull(FieldByName('EE_SDI_OVERRIDE').Value, 0);
                            PR_CHECK_STATES.Post;
                          end;
                          Next;
                        end;
                      finally
                        Filtered := False;
                        Filter := '';
                        PR_CHECK_STATES.Filtered := False;
                        PR_CHECK_STATES.Filter := '';
                      end;
                    end;

                    with PR_CHECK_SUI_2 do
                    begin
                      Data := PR_CHECK_SUI.Data;
                      Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
                      Filtered := True;
                      PR_CHECK_SUI.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                      PR_CHECK_SUI.Filtered := True;
                      try
                        First;
                        while not EOF do
                        begin
                          AbortIfCurrentThreadTaskTerminated;
                          if PR_CHECK_SUI.Locate('CO_SUI_NBR', FieldByName('CO_SUI_NBR').Value, []) then
                          begin
                            PR_CHECK_SUI.Edit;
                            PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value := PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value -
                              ConvertNull(FieldByName('OVERRIDE_AMOUNT').Value, 0);
                            PR_CHECK_SUI.Post;
                          end;
                          Next;
                        end;
                      finally
                        Filtered := False;
                        Filter := '';
                        PR_CHECK_SUI.Filtered := False;
                        PR_CHECK_SUI.Filter := '';
                      end;
                    end;

                    with PR_CHECK_LOCALS_2 do
                    begin
                      Data := PR_CHECK_LOCALS.Data;
                      Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
                      Filtered := True;
                      PR_CHECK_LOCALS.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                      PR_CHECK_LOCALS.Filtered := True;
                      try
                        First;
                        while not EOF do
                        begin
                          AbortIfCurrentThreadTaskTerminated;
                          if PR_CHECK_LOCALS.Locate('EE_LOCALS_NBR', FieldByName('EE_LOCALS_NBR').Value, []) then
                          begin
                            PR_CHECK_LOCALS.Edit;
                            PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value := PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value -
                              ConvertNull(FieldByName('OVERRIDE_AMOUNT').Value, 0);
                            PR_CHECK_LOCALS.Post;
                          end;
                          Next;
                        end;
                      finally
                        Filtered := False;
                        Filter := '';
                        PR_CHECK_LOCALS.Filtered := False;
                        PR_CHECK_LOCALS.Filter := '';
                      end;
                    end;
                  end;
                PR_CHECK_2.Next;
              end;
            end;

            CheckCount := 0;
            PriorEENbr := 0;
            AsOfDate := PR.FieldByName('CHECK_DATE').AsDateTime + 1 - 1 / 1440;
            DM_HISTORICAL_TAXES.LoadDataAsOfDate(AsOfDate);
            if PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT then
            try
              PR_CHECK_2.First;
              while not PR_CHECK_2.EOF do
              begin
                Inc(CheckCount);
                AbortIfCurrentThreadTaskTerminated;
                PostFeedback(True, 'Calculating check for EE #' + Trim(ConvertNull(DM_CLIENT.EE.Lookup('EE_NBR',
                  PR_CHECK_2.FieldByName('EE_NBR').Value, 'CUSTOM_EMPLOYEE_NUMBER'), '')) + '  (' + IntToStr(CheckCount) + ' of ' +
                  IntToStr(PR_CHECK_2.RecordCount) + ')...');
                SecondCheck := PriorEENbr = PR_CHECK_2.FieldByName('EE_NBR').Value;
                PriorEENbr := PR_CHECK_2.FieldByName('EE_NBR').Value;
      // Do "Gross To Net"
                if PR_CHECK_2.FieldByName('CHECK_STATUS').Value <> CHECK_STATUS_VOID then
                begin
                  if PR_CHECK_2.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_MANUAL then
                  begin
                    Warnings := '';
                    CheckHasShortfall := ctx_PayrollCalculation.GrossToNet(Warnings, PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, False, JustPreProcess, True, SecondCheck);
                    PaChecks.AddValue(PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString, PaTaxes);

                    DM_CLIENT.PR_CHECK_LINE_LOCALS.First;
                    DM_CLIENT.PR_CHECK_LINES.CancelRange;
                    while not DM_CLIENT.PR_CHECK_LINE_LOCALS.Eof do
                    begin
                      AbortIfCurrentThreadTaskTerminated;
                      if DM_CLIENT.PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', DM_CLIENT.PR_CHECK_LINE_LOCALS.PR_CHECK_LINES_NBR.Value, []) then
                        DM_CLIENT.PR_CHECK_LINE_LOCALS.Next
                      else
                        DM_CLIENT.PR_CHECK_LINE_LOCALS.Delete;
                    end;

                    PR_CHECK_LINES.SetRange([PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger]);

                    ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_LINES, DM_CLIENT.PR_CHECK_LINE_LOCALS]);

                    if Warnings <> '' then
                      AddHint(Warnings);
                  end
                  else
                  begin
                    SQLQuery := TevQuery.Create('GenericSelect2CurrentWithCondition');
                    SQLQuery.Macros.AddValue('COLUMNS', 'count(*)');
                    SQLQuery.Macros.AddValue('TABLE1', 'PR_REPRINT_HISTORY');
                    SQLQuery.Macros.AddValue('TABLE2', 'PR_REPRINT_HISTORY_DETAIL');
                    SQLQuery.Macros.AddValue('JOINFIELD', 'PR_REPRINT_HISTORY');
                    SQLQuery.Macros.AddValue('CONDITION', 't2.MISCELLANEOUS_CHECK=''N'' and t1.PR_NBR=' +
                          IntToStr(PayrollNumber) + ' and t2.BEGIN_CHECK_NUMBER<=' +
                          PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString + ' and t2.END_CHECK_NUMBER>=' +
                          PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString);
                    SQLQuery.Execute;
                    if SQLQuery.Result.Fields[0].Value = 0 then
                    begin
                      Warnings := '';
                      CheckHasShortfall := ctx_PayrollCalculation.GrossToNet(Warnings, PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, False, JustPreProcess, True, SecondCheck);
                      PaChecks.AddValue(PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString, PaTaxes);
                      ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_LINE_LOCALS]);
                      if Warnings <> '' then
                        AddHint(Warnings);
                    end
                    else
                    begin
                      if not DM_CLIENT.CO_LOCAL_TAX.Active then
                        DM_CLIENT.CO_LOCAL_TAX.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
                      PR_CHECK_LINES.SetLockedIndexFieldNames('PR_CHECK_NBR');
                      PR_CHECK_LINES.SetRange([PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger]);
                    end;
                  end;
                end;
      // Update scheduled E/D balances for void checks
                if not JustPreprocess and (PR_CHECK_2.FieldByName('CHECK_STATUS').Value = CHECK_STATUS_VOID) then
                with PR_CHECK_LINES, DM_CLIENT do
                begin
                  PR_CHECK_LINES.SetLockedIndexFieldNames('PR_CHECK_NBR');
                  PR_CHECK_LINES.SetRange([PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger]);
                  First;
                  while not EOF do
                  begin
                    AbortIfCurrentThreadTaskTerminated;
                    if not FieldByName('EE_SCHEDULED_E_DS_NBR').IsNull and (PR_CHECK_2.FieldByName('UPDATE_BALANCE').AsString = GROUP_BOX_YES) then
                    begin
                      EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', FieldByName('EE_SCHEDULED_E_DS_NBR').Value, []);
                      if (ConvertNull(EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value, 0) > 0) and (ConvertNull(FieldByName('AMOUNT').Value, 0) < 0) then
                      begin
                        EE_SCHEDULED_E_DS.Edit;
                        EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value := ConvertNull(EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value, 0) +
                          FieldByName('AMOUNT').Value;
                        if EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value < 0 then
                          EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value := 0;
                        EE_SCHEDULED_E_DS.Post;
                      end;
                      if (ConvertNull(EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value, 0) < 0) and (ConvertNull(FieldByName('AMOUNT').Value, 0) > 0) then
                      begin
                        EE_SCHEDULED_E_DS.Edit;
                        EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value := ConvertNull(EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value, 0) +
                          FieldByName('AMOUNT').Value;
                        if EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value > 0 then
                          EE_SCHEDULED_E_DS.FieldByName('BALANCE').Value := 0;
                        EE_SCHEDULED_E_DS.Post;
                      end;
                    end;
                    Next;
                  end;
                end;

                ctx_DataAccess.PostDataSets([DM_CLIENT.EE_SCHEDULED_E_DS]);

                PR_CHECK.Locate('PR_CHECK_NBR', PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, []);

                if (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_TAX_ADJUSTMENT)
                and (PR_CHECK_2.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_MANUAL)
                and (PR_CHECK_2.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_3RD_PARTY)
                and (PR_CHECK_2.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_COBRA_CREDIT)
                and (PR_CHECK_2.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_VOID) then
                begin
      // Get rid of all checks that do not have positive earnings and/or negative deductions unless they are manuals, 3rd party or voids
                  with PR_CHECK_LINES, DM_CLIENT do
                  begin
                    GetRidOfCheck := True;
                    First;
                    while not EOF do
                    begin
                      AbortIfCurrentThreadTaskTerminated;
                      if PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_SETUP] then
                      begin
                        GetRidOfCheck := False;
                        Break;
                      end;

                      EDType := CL_E_DS.ShadowDataSet.CachedLookup(FieldByName('CL_E_DS_NBR').AsInteger, 'E_D_CODE_TYPE');
                      //EDType := CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE');
                      if ((ConvertNull(FieldByName('AMOUNT').Value, 0) > 0) and (EDType[1] = 'E'))
                      or ((ConvertNull(FieldByName('AMOUNT').Value, 0) < 0) and (EDType[1] = 'D')) then
                      begin
                        GetRidOfCheck := False;
                        Break;
                      end;
                      Next;
                    end;
                  end;
      // Also take care of empty checks, but don't touch tax adjustment checks
                  if (PR_CHECK_LINES.BOF and PR_CHECK_LINES.EOF) or GetRidOfCheck then
                  begin
                    if CheckHasShortfall or (not CheckHasTaxOverrides(PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger)) then
                    begin
                      ctx_DataAccess.UnlockPRSimple;
                      try
                        DeleteIt(PR_CHECK_LINES, PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
                        DeleteIt(PR_CHECK_STATES, PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
                        DeleteIt(PR_CHECK_SUI, PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
                        DeleteIt(PR_CHECK_LOCALS, PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
                        PR_CHECK.Delete;
                        PR_CHECK_2.Next;
                        ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_LINES]);
                      finally
                        ctx_DataAccess.LockPRSimple;
                      end;
                      PR_CHECK_LINES.CancelRange;
                      PR_CHECK_LINES.ResetLockedIndexFieldNames('');
                      Continue;
                    end;
                  end;
                end;

                if not IsImport and not PostACH then
                begin
                  PR_CHECK_LINES.First;
                  while not PR_CHECK_LINES.EOF do
                  begin
                    AbortIfCurrentThreadTaskTerminated;
                    EDType := CL_E_DS.ShadowDataSet.CachedLookup(PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsInteger, 'E_D_CODE_TYPE');
                    if EDType = ED_DIRECT_DEPOSIT then
                    begin
                      if (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
                        PostACH := True;
                      Break;
                    end;
                    if DM_CLIENT.EE_SCHEDULED_E_DS.ShadowDataSet.CachedLookup(PR_CHECK_LINES.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger, 'EE_DIRECT_DEPOSIT_NBR') <> '' then
                    begin
                      if (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
                        PostACH := True;
                      Break;
                    end;
                    PR_CHECK_LINES.Next;
                  end;
                end;

                if PR_CHECK_2.FieldByName('CHECK_STATUS').Value <> CHECK_STATUS_VOID then
                begin
                  PR_CHECK.Edit;
                  if (not IsImport) or (IsImport and (PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').IsNull
                  or PR_CHECK.FieldByName('ER_OASDI_TAX').IsNull)) then
                  begin
                    CalcEROASDITaxes(Tax, Wages1, Wages2, Wages3);
                    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('ER_OASDI_TAX').IsNull) then
                      PR_CHECK.FieldByName('ER_OASDI_TAX').Value := RoundTwo(Tax);
                    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').IsNull) then
                    begin
                      PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').Value := RoundTwo(Wages1);
                      PR_CHECK.FieldByName('ER_OASDI_GROSS_WAGES').Value := RoundTwo(Wages3);
                    end;
                    PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := RoundTwo(Wages2);
                  end
                  else
                    PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0;
                  if PR_CHECK.FieldByName('ER_OASDI_GROSS_WAGES').IsNull then
                    PR_CHECK.FieldByName('ER_OASDI_GROSS_WAGES').Value := PR_CHECK.FieldByName('ER_OASDI_TAXABLE_WAGES').Value;

                  if (not IsImport) or (IsImport and (PR_CHECK.FieldByName('ER_MEDICARE_TAXABLE_WAGES').IsNull
                  or PR_CHECK.FieldByName('ER_MEDICARE_TAX').IsNull)) then
                  begin
                    CalcERMedicareTaxes(Tax, Wages1, Wages2);
                    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('ER_MEDICARE_TAX').IsNull) then
                      PR_CHECK.FieldByName('ER_MEDICARE_TAX').Value := RoundTwo(Tax);
                    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('ER_MEDICARE_TAXABLE_WAGES').IsNull) then
                    begin
                      PR_CHECK.FieldByName('ER_MEDICARE_TAXABLE_WAGES').Value := RoundTwo(Wages1);
                      PR_CHECK.FieldByName('ER_MEDICARE_GROSS_WAGES').Value := RoundTwo(Wages2);
                    end;
                  end;
                  if PR_CHECK.FieldByName('ER_MEDICARE_GROSS_WAGES').IsNull then
                    PR_CHECK.FieldByName('ER_MEDICARE_GROSS_WAGES').Value := PR_CHECK.FieldByName('ER_MEDICARE_TAXABLE_WAGES').Value;

                  if (not IsImport) or (IsImport and (PR_CHECK.FieldByName('ER_FUI_TAXABLE_WAGES').IsNull
                  or PR_CHECK.FieldByName('ER_FUI_TAX').IsNull)) then
                  begin
                    CalcERFUITaxes(Tax, Wages1, Wages2);
                    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('ER_FUI_TAX').IsNull) then
                      PR_CHECK.FieldByName('ER_FUI_TAX').Value := RoundTwo(Tax);
                    if (not IsImport) or (IsImport and PR_CHECK.FieldByName('ER_FUI_TAXABLE_WAGES').IsNull) then
                    begin
                      PR_CHECK.FieldByName('ER_FUI_TAXABLE_WAGES').Value := RoundTwo(Wages1);
                      PR_CHECK.FieldByName('ER_FUI_GROSS_WAGES').Value := RoundTwo(Wages2);
                    end;
                  end;
                  if PR_CHECK.FieldByName('ER_FUI_GROSS_WAGES').IsNull then
                    PR_CHECK.FieldByName('ER_FUI_GROSS_WAGES').Value := PR_CHECK.FieldByName('ER_FUI_TAXABLE_WAGES').Value;

                  PR_CHECK_STATES.SetLockedIndexFieldNames('PR_CHECK_NBR');
                  PR_CHECK_STATES.SetRange([PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger]);
                  try
                    PR_CHECK_STATES.First;
                    while not PR_CHECK_STATES.EOF do
                    begin
                      AbortIfCurrentThreadTaskTerminated;
                      if (not IsImport) or (IsImport and (PR_CHECK_STATES.FieldByName('ER_SDI_TAXABLE_WAGES').IsNull
                      or PR_CHECK_STATES.FieldByName('ER_SDI_TAX').IsNull)) then
                      begin
                        CalcERSDITaxes(PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value, Tax, Wages1, Wages2);
                        PR_CHECK_STATES.Edit;
                        if (not IsImport) or (IsImport and PR_CHECK_STATES.FieldByName('ER_SDI_TAX').IsNull) then
                          PR_CHECK_STATES.FieldByName('ER_SDI_TAX').Value := RoundTwo(Tax);
                        if (not IsImport) or (IsImport and PR_CHECK_STATES.FieldByName('ER_SDI_TAXABLE_WAGES').IsNull) then
                        begin
                          PR_CHECK_STATES.FieldByName('ER_SDI_TAXABLE_WAGES').Value := RoundTwo(Wages1);
                          PR_CHECK_STATES.FieldByName('ER_SDI_GROSS_WAGES').Value := RoundTwo(Wages2);
                        end;
                        if PR_CHECK_STATES.FieldByName('ER_SDI_GROSS_WAGES').IsNull then
                          PR_CHECK_STATES.FieldByName('ER_SDI_GROSS_WAGES').Value := PR_CHECK_STATES.FieldByName('ER_SDI_TAXABLE_WAGES').Value;
                        PR_CHECK_STATES.Post;
                      end;
                      PR_CHECK_STATES.Next;
                    end;
                  finally
                    PR_CHECK_STATES.CancelRange;
                    PR_CHECK_STATES.ResetLockedIndexFieldNames('');
                  end;

                  PR_CHECK_SUI.SetLockedIndexFieldNames('PR_CHECK_NBR');
                  PR_CHECK_SUI.SetRange([PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger]);
                  try
                    PR_CHECK_SUI.First;
                    while not PR_CHECK_SUI.EOF do
                    begin
                      AbortIfCurrentThreadTaskTerminated;
                      if not Is_ER_SUI(DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR',
                      PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, 'SY_SUI_NBR'), 'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) then
                      // We're dealing with Employee SUI
                      begin
                        PR_CHECK_SUI.Next;
                        Continue;
                      end;
                      if (not IsImport) or (IsImport and (PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').IsNull
                      or PR_CHECK_SUI.FieldByName('SUI_TAX').IsNull)) then
                      begin
                        CalcSUITaxes(PR_CHECK.FieldByName('PR_CHECK_NBR').Value, PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, True {Employer SUI},
                          Tax, Wages1, Wages2, PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, False, False, PR_CHECK_2);

                        PR_CHECK_SUI.Edit;
                        if (not IsImport) or (IsImport and PR_CHECK_SUI.FieldByName('SUI_TAX').IsNull) then
                          PR_CHECK_SUI.FieldByName('SUI_TAX').Value := RoundTwo(Tax);
                        if (not IsImport) or (IsImport and PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').IsNull) then
                        begin
                          if (DM_CLIENT.EE.FieldByName('COMPANY_OR_INDIVIDUAL_NAME').AsString = GROUP_BOX_COMPANY) and
                          (DM_CLIENT.EE_STATES.NewLookup('SUI_APPLY_CO_STATES_NBR', PR_CHECK_SUI.FieldByName('CO_SUI_NBR').Value, 'CALCULATE_TAXABLE_WAGES_1099') = 'N') then
                            PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value := 0
                          else
                            PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value := RoundTwo(Wages1);
                          PR_CHECK_SUI.FieldByName('SUI_GROSS_WAGES').Value := RoundTwo(Wages2);
                        end;
                        if PR_CHECK_SUI.FieldByName('SUI_GROSS_WAGES').IsNull then
                          PR_CHECK_SUI.FieldByName('SUI_GROSS_WAGES').Value := PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').Value;
                        PR_CHECK_SUI.Post;
                      end;
                      PR_CHECK_SUI.Next;
                    end;
                  finally
                    PR_CHECK_SUI.CancelRange;
                    PR_CHECK_SUI.ResetLockedIndexFieldNames('');
                  end;

                  PR_CHECK_LOCALS.SetLockedIndexFieldNames('PR_CHECK_NBR');
                  PR_CHECK_LOCALS.SetRange([PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger], [PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger]);
                  try
                    PR_CHECK_LOCALS.First;
                    while not PR_CHECK_LOCALS.EOF do
                    begin
                      AbortIfCurrentThreadTaskTerminated;
                      if DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR',
                      DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', DM_CLIENT.EE_LOCALS.Lookup('EE_LOCALS_NBR',
                      PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value, 'CO_LOCAL_TAX_NBR'), 'SY_LOCALS_NBR'),
                      'TAX_TYPE') = GROUP_BOX_EE then
                      // We're dealing with Employee Local
                      begin
                        PR_CHECK_LOCALS.Next;
                        Continue;
                      end;
                      if (not IsImport) or (IsImport and (PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').IsNull
                      or PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').IsNull)) then
                        CalcEmployerLocalTax(PR_CHECK.FieldByName('PR_CHECK_NBR').Value,
                          DM_CLIENT.EE_LOCALS.Lookup('EE_LOCALS_NBR', PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value, 'CO_LOCAL_TAX_NBR'),
                          TaxRate, Tax, Wages1, Wages2, PR, PR_CHECK, PR_CHECK_LINES, PR_CHECK_LINE_LOCALS, PR_CHECK_STATES, PR_CHECK_LOCALS);
                      PR_CHECK_LOCALS.Edit;
                      if (not IsImport) or IsImport and (PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').IsNull) then
                      begin
                        PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := RoundTwo(Wages1);
                        PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').Value := RoundTwo(Wages2);
                      end;
                      if (not IsImport) or IsImport and (PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').IsNull) then
                        PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := RoundTwo(Tax);
                      if PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').IsNull then
                        PR_CHECK_LOCALS.FieldByName('LOCAL_GROSS_WAGES').Value := PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value;
                      PR_CHECK_LOCALS.Post;
                      PR_CHECK_LOCALS.Next;
                    end;
                  finally
                    PR_CHECK_LOCALS.CancelRange;
                    PR_CHECK_LOCALS.ResetLockedIndexFieldNames('');
                  end;
                
                  PR_CHECK.Post;
                end;
                PR_CHECK_LINES.CancelRange;
                PR_CHECK_LINES.ResetLockedIndexFieldNames('');
                PR_CHECK_2.Next;
              end;
              ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK_LINES]);
            finally
              PR_CHECK_LINES.CancelRange;
              PR_CHECK_LINES.ResetLockedIndexFieldNames('');
              DM_CLIENT.PR_CHECK_LINES.Filtered := False;
              DM_CLIENT.PR_CHECK_LINES.Filter := '';
              DM_CLIENT.EE_SCHEDULED_E_DS.Filtered := False;
              DM_CLIENT.EE_SCHEDULED_E_DS.Filter := '';
            end;

  // Make sure the payroll still has checks
            if ((PR_CHECK.RecordCount = 0) and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT) and
            (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_TAX_DEPOSIT) and (PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP))
            or ((DM_CLIENT.PR_MISCELLANEOUS_CHECKS.RecordCount = 0) and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString[1] in
            [PAYROLL_TYPE_TAX_DEPOSIT])) then
              raise ECanNotPerformOperation.CreateHelp('The payroll has only zero checks. Nothing to process.', IDH_CanNotPerformOperation);

            if not JustPreProcess then
            begin
              PostFeedback(True, 'Waiting to flag client...');
              ctx_DBAccess.LockTableInTransaction('CL'); // locking the table to prevent deadlocks when reading check lines for agency checks and liabilities
            end;

            if (not JustPreProcess) and (not IsImport) then
            begin
              PostFeedback(True, 'Calculating misc. checks...');
              if (PR.FieldByName('EXCLUDE_AGENCY').Value = 'N') and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_REVERSAL) then
                CreateAgencyChecks;

             // if DM_CLIENT.CO.FieldByName('IMPOUND_WORKERS_COMP').AsString = GROUP_BOX_YES then
                if not PostACH then
                  if ctx_PayrollCalculation.CalcWorkersComp(PayrollNumber, 'CO_NBR', nil,0, True) <> 0 then
                    if (DM_CLIENT.PR.WC_IMPOUND.AsString <> PAYMENT_TYPE_NONE) and (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
                      PostACH := True;

              WasSetupRun := False;
              OldPayrollNbr := 0;
              if DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL then
              begin
                OldPayrollNbr := StrToIntDef(ExtractFromFiller(ConvertNull(DM_CLIENT.PR.FieldByName('FILLER').Value, ''), 1, 8), 0);
                if OldPayrollNbr <> 0 then
                with ctx_DataAccess.CUSTOM_VIEW do
                begin
                  if Active then
                    Close;
                  with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
                  begin
                    SetMacro('NbrField', 'PR');
                    SetMacro('Columns', '*');
                    SetMacro('TableName', 'PR');
                    SetParam('RecordNbr', OldPayrollNbr);
                    DataRequest(AsVariant);
                  end;
                  Open;
                  if RecordCount > 0 then
                    WasSetupRun := FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_SETUP;
                  Close;
                end;
              end;

      // Create a second Setup Payroll
              if HasQTDs then
              begin
                PostFeedback(True, 'Creating second setup payroll...');

                DM_CLIENT.PR.Locate('PR_NBR', NewPayrollNbr, []);
                TempDataSet := TevClientDataSet.Create(nil);
                try
                  TempDataSet.Data := PR_BATCH.Data;
                  TempDataSet.First;
                  while not TempDataSet.EOF do
                  begin
                    AbortIfCurrentThreadTaskTerminated;
                    PR_BATCH.Insert;
                    for I := 0 to TempDataSet.FieldCount - 1 do
                    begin
                      if (TempDataSet.Fields[I].FieldName <> 'PR_NBR') and (TempDataSet.Fields[I].FieldName <> 'PR_BATCH_NBR')
                        {and (TempDataSet.Fields[I].FieldName <> 'EFFECTIVE_DATE') }and (TempDataSet.Fields[I].FieldName <> 'CHANGED_BY')
                        and (TempDataSet.Fields[I].FieldName <> 'CREATION_DATE'){ and (TempDataSet.Fields[I].FieldName <> 'ACTIVE_RECORD') }then
                        PR_BATCH.FieldByName(TempDataSet.Fields[I].FieldName).Value := TempDataSet.Fields[I].Value;
                    end;
                    PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    PR_BATCH.FieldByName('PERIOD_END_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    PR_BATCH.Post;

                    PR_CHECK.First;
                    while not PR_CHECK.EOF do
                    begin
                      AbortIfCurrentThreadTaskTerminated;
                      if (PR_CHECK.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_YTD) and (PR_CHECK.FieldByName('PR_BATCH_NBR').Value = TempDataSet.FieldByName('PR_BATCH_NBR').Value) then
                      begin
                        PR_CHECK.Edit;
                        PR_CHECK.FieldByName('PR_NBR').Value := NewPayrollNbr;
                        PR_CHECK.FieldByName('PR_BATCH_NBR').Value := PR_BATCH.FieldByName('PR_BATCH_NBR').Value;
                        PR_CHECK.Post;

                        PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                        PR_CHECK_LINES.Filtered := True;
                        try
                          PR_CHECK_LINES.First;
                          while not PR_CHECK_LINES.EOF do
                          begin
                            PR_CHECK_LINES.Edit;
                            PR_CHECK_LINES.FieldByName('PR_NBR').Value := NewPayrollNbr;
                            PR_CHECK_LINES.Post;
                            PR_CHECK_LINES.Next;
                          end;
                        finally
                          PR_CHECK_LINES.Filtered := False;
                          PR_CHECK_LINES.Filter := '';
                        end;

                        PR_CHECK_STATES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                        PR_CHECK_STATES.Filtered := True;
                        try
                          PR_CHECK_STATES.First;
                          while not PR_CHECK_STATES.EOF do
                          begin
                            PR_CHECK_STATES.Edit;
                            PR_CHECK_STATES.FieldByName('PR_NBR').Value := NewPayrollNbr;
                            PR_CHECK_STATES.Post;
                            PR_CHECK_STATES.Next;
                          end;
                        finally
                          PR_CHECK_STATES.Filtered := False;
                          PR_CHECK_STATES.Filter := '';
                        end;

                        PR_CHECK_SUI.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                        PR_CHECK_SUI.Filtered := True;
                        try
                          PR_CHECK_SUI.First;
                          while not PR_CHECK_SUI.EOF do
                          begin
                            PR_CHECK_SUI.Edit;
                            PR_CHECK_SUI.FieldByName('PR_NBR').Value := NewPayrollNbr;
                            PR_CHECK_SUI.Post;
                            PR_CHECK_SUI.Next;
                          end;
                        finally
                          PR_CHECK_SUI.Filtered := False;
                          PR_CHECK_SUI.Filter := '';
                        end;

                        PR_CHECK_LOCALS.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
                        PR_CHECK_LOCALS.Filtered := True;
                        try
                          PR_CHECK_LOCALS.First;
                          while not PR_CHECK_LOCALS.EOF do
                          begin
                            PR_CHECK_LOCALS.Edit;
                            PR_CHECK_LOCALS.FieldByName('PR_NBR').Value := NewPayrollNbr;
                            PR_CHECK_LOCALS.Post;
                            PR_CHECK_LOCALS.Next;
                          end;
                        finally
                          PR_CHECK_LOCALS.Filtered := False;
                          PR_CHECK_LOCALS.Filter := '';
                        end;
                      end;
                      PR_CHECK.Next;
                    end;

                    TempDataSet.Next;
                  end;
                finally
                  TempDataSet.Free;
                end;
                PR.Edit;
                PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSED;
                PR.FieldByName('STATUS_DATE').Value := Now;
                PR.Post;

                DM_CLIENT.PR.Locate('PR_NBR', PayrollNumber, []);
              end;
            end;

            if (not JustPreProcess) and (PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP) then
            begin
              PR.Edit;
              PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSED;
              PR.FieldByName('STATUS_DATE').Value := Now;
              PR.Post;
            end;

            PR_CHECK_STATES.Activate;
            PR_CHECK_SUI.Activate;
            PR_CHECK_LOCALS.Activate;
            DM_CLIENT.EE_SCHEDULED_E_DS.Activate;

            // check limitations

            if (PR.FieldByName('APPROVED_BY_TAX').Value <> PR_APPROVED_YES)
            or (PR.FieldByName('APPROVED_BY_FINANCE').Value <> PR_APPROVED_YES)
            or (PR.FieldByName('APPROVED_BY_MANAGEMENT').Value <> PR_APPROVED_YES) then
            begin

              DM_CLIENT.CO.Locate('CO_NBR', DM_CLIENT.PR.CO_NBR.Value, []);

              if not DM_CLIENT.CO.MAXIMUM_DOLLARS_ON_CHECK.IsNull
              and (DM_CLIENT.CO.MAXIMUM_DOLLARS_ON_CHECK.AsCurrency > 0.005)
              and not DM_CLIENT.CO.CO_PAYROLL_PROCESS_LIMITATIONS.IsNull
              and (DM_CLIENT.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString <> CO_PAYROLL_PROCESS_LIMITATIONS_NONE) then
              begin
                DM_CLIENT.PR_CHECK.First;
                while not DM_CLIENT.PR_CHECK.Eof do
                begin
                  AbortIfCurrentThreadTaskTerminated;
                  if DM_CLIENT.PR_CHECK.GROSS_WAGES.AsCurrency > DM_CLIENT.CO.MAXIMUM_DOLLARS_ON_CHECK.AsCurrency then
                  begin
                    DM_CLIENT.EE.Locate('EE_NBR', DM_CLIENT.PR_CHECK.EE_NBR.Value, []);
                    DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_CLIENT.EE.CL_PERSON_NBR.Value, []);
                    s := Format('Employee %s %s: Gross Wages %0.2f exceeds the set limitation of %0.2f',
                               [Trim(DM_CLIENT.EE.CUSTOM_EMPLOYEE_NUMBER.AsString),
                                Trim(DM_CLIENT.CL_PERSON.FIRST_NAME.AsString + ' ' + DM_CLIENT.CL_PERSON.LAST_NAME.AsString),
                                DM_CLIENT.PR_CHECK.GROSS_WAGES.AsCurrency,
                                DM_CLIENT.CO.MAXIMUM_DOLLARS_ON_CHECK.AsCurrency]);
                    Result := Result + #13#10 + s
                  end;
                  DM_CLIENT.PR_CHECK.Next;
                end;
              end;

              if not DM_CLIENT.CO.CO_MAX_AMOUNT_FOR_PAYROLL.IsNull
              and (DM_CLIENT.CO.CO_MAX_AMOUNT_FOR_PAYROLL.AsCurrency > 0.005)
              and not DM_CLIENT.CO.CO_PAYROLL_PROCESS_LIMITATIONS.IsNull
              and (DM_CLIENT.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString <> CO_PAYROLL_PROCESS_LIMITATIONS_NONE) then
              begin
                PostFeedback(True, 'Checking for payroll limitations...');
                TotalPayroll := 0;
                DM_CLIENT.PR_CHECK.First;
                while not DM_CLIENT.PR_CHECK.Eof do
                begin
                  AbortIfCurrentThreadTaskTerminated;
                  TotalPayroll := TotalPayroll + DM_CLIENT.PR_CHECK.NET_WAGES.AsCurrency;
                  DM_CLIENT.PR_CHECK.Next;
                end;
                if {(TotalPayroll < 0) or} (TotalPayroll > DM_CLIENT.CO.CO_MAX_AMOUNT_FOR_PAYROLL.AsCurrency) then
                begin
                  if TotalPayroll > DM_CLIENT.CO.CO_MAX_AMOUNT_FOR_PAYROLL.AsCurrency then
                    s := Format('Payroll net total %0.2f exceeds the set limitation of %0.2f.',
                      [TotalPayroll, DM_CLIENT.CO.CO_MAX_AMOUNT_FOR_PAYROLL.AsCurrency])
                  else
                    s := 'Payroll net total is negative.';

                  if not JustPreProcess
                  and (DM_CLIENT.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_STOP) then
                    s := s + #13#10 + 'Your payroll has been put on Hold.  It will need to be approved before it can be processed.';

                  if (DM_CLIENT.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_WARN)
                  or JustPreProcess then
                    Result := Result + #13#10 + s
                  else
                    raise EPayrollLimitation.CreateHelp(s, IDH_InconsistentData);
                end;
              end;
            end;

            PostFeedback(True, 'Saving data...');

            MyTransactionManager.ApplyUpdates;
            PR_BATCH.Close;

            if JustPreProcess or (PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP) then
            begin
              PostFeedback(True, 'Saving data...');
              if JustPreProcess then
              begin
                DM_CLIENT.PR.Edit;
                DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING;
                DM_CLIENT.PR.FieldByName('STATUS_DATE').Value := Now;
                DM_CLIENT.PR.Post;

                ctx_DataAccess.PostDataSets([DM_CLIENT.PR]);
              end;
              AbortIfCurrentThreadTaskTerminated;
              ctx_DataAccess.CommitNestedTransaction;
              Exit;
            end;

            // ---- 16-50 Begin / added 07-JUN-99 Sergei Serdyuk
            Assert(DM_CLIENT.CO['CO_NBR'] = DM_CLIENT.PR['CO_NBR']);
            if (DM_CLIENT.CO['TIME_OFF_ACCRUAL'] = GROUP_BOX_YES)
            and not (PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_REVERSAL])
            and (not IsImport) then // company uses time off accrual
            begin
              PostFeedback(True, 'Calculating time off accrual...');
              CalculateTimeOffAccrual(Result);
            end;

            PostFeedback(True, 'Updating liabilities...');
            Impound := 0;
      // Write Liabilities for all payroll types but Setup Run (if they are not excluded, of course)
            if DM_CLIENT.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString[1] in [GROUP_BOX_DEPOSIT, GROUP_BOX_NONE] then
            with DM_CLIENT do
            begin
              MyTransactionManager.Initialize([DM_CLIENT.CO_TAX_DEPOSITS, DM_CLIENT.CO_FED_TAX_LIABILITIES, DM_CLIENT.CO_STATE_TAX_LIABILITIES,
                DM_CLIENT.CO_SUI_LIABILITIES, DM_CLIENT.CO_LOCAL_TAX_LIABILITIES]);

        // Create a tax deposit record
              if Backdated then
              begin
                DM_CLIENT.CO_TAX_DEPOSITS.Insert;
                DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_NBR').AsInteger := DM_CLIENT.CO.FieldByName('CO_NBR').AsInteger;
                DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID;
                DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('STATUS_DATE').Value := Now;
                DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := 'Setup Run ' + DM_CLIENT.PR.FieldByName('CHECK_DATE').AsString;
                DM_CLIENT.CO_TAX_DEPOSITS.Post;
              end;

        // Federal Liabilities
            // Non third-party
              if ctx_DataAccess.CUSTOM_VIEW.Active then
                ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('Sum945TaxesForPr') do
              begin
                SetMacro('Filter', 'CHECK_TYPE<>:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              Tax := ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0);
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('SumAllTaxesForPr') do
              begin
                SetMacro('Filter', 'CHECK_TYPE<>:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              PostFederalLiability(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0) - Tax, TAX_LIABILITY_TYPE_FEDERAL, 'N');
              if Tax <> 0 then
                PostFederalLiability(Tax, TAX_LIABILITY_TYPE_FEDERAL_945, 'N');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, TAX_LIABILITY_TYPE_EE_OASDI, 'N');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value, TAX_LIABILITY_TYPE_EE_MEDICARE, 'N');
              PostFederalLiability(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, 0) * (-1), TAX_LIABILITY_TYPE_EE_EIC, 'N');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value, TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING, 'N');
            // Third-party
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('Sum945TaxesForPr') do
              begin
                SetMacro('Filter', 'CHECK_TYPE=:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              Tax := ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0);
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('SumAllTaxesForPr') do
              begin
                SetMacro('Filter', 'CHECK_TYPE=:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              PostFederalLiability(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0) - Tax, TAX_LIABILITY_TYPE_FEDERAL, 'Y');
              if Tax <> 0 then
                PostFederalLiability(Tax, TAX_LIABILITY_TYPE_FEDERAL_945, 'Y');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, TAX_LIABILITY_TYPE_EE_OASDI, 'Y');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value, TAX_LIABILITY_TYPE_EE_MEDICARE, 'Y');
              PostFederalLiability(ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, 0) * (-1), TAX_LIABILITY_TYPE_EE_EIC, 'Y');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value, TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING, 'Y');
            // Third-party independent
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
              begin
                SetMacro('Columns', 'sum(ER_OASDI_TAX), sum(ER_MEDICARE_TAX), sum(ER_FUI_TAX)');
                SetMacro('TableName', 'PR_CHECK');
                SetMacro('NbrField', 'PR');
                SetParam('RecordNbr', PayrollNumber);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, TAX_LIABILITY_TYPE_ER_OASDI, 'N');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, TAX_LIABILITY_TYPE_ER_MEDICARE, 'N');
              PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value, TAX_LIABILITY_TYPE_ER_FUI, 'N');
            // Cobra Credit
              if CL_E_DS.Locate('E_D_CODE_TYPE', ED_MEMO_COBRA_CREDIT, []) then
              begin
                ctx_DataAccess.CUSTOM_VIEW.Close;
                with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
                begin
                  SetMacro('Columns', 'sum(AMOUNT)');
                  SetMacro('TableName', 'PR_CHECK_LINES');
                  SetMacro('Condition', 'PR_NBR=' + IntToStr(PayrollNumber) + ' and CL_E_DS_NBR=' + CL_E_DS.FieldByName('CL_E_DS_NBR').AsString);
                  ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                end;
                ctx_DataAccess.CUSTOM_VIEW.Open;
                ctx_DataAccess.CUSTOM_VIEW.First;
                PostFederalLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, TAX_LIABILITY_TYPE_COBRA_CREDIT, 'N');
              end;

        // State Liabilities
              DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Activate;
              DM_SYSTEM_STATE.SY_STATES.Activate;
              DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Activate;
              DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.Activate;
            // Non third-party
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('ThirdPartyStateLiab') do
              begin
                SetMacro('Filter', 'C.CHECK_TYPE<>:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if ctx_DataAccess.CUSTOM_VIEW.Fields[3].IsNull then
                  raise EInconsistentData.CreateHelp('State deposit frequency is not set for state ' + DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value, 'STATE'), IDH_InconsistentData);
                PostStateLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, TAX_LIABILITY_TYPE_STATE, 'N', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value);
                PostStateLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value, TAX_LIABILITY_TYPE_EE_SDI, 'N', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value);
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;
            // Third-party
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('ThirdPartyStateLiab') do
              begin
                SetMacro('Filter', 'C.CHECK_TYPE=:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if ctx_DataAccess.CUSTOM_VIEW.Fields[3].IsNull then
                  raise EInconsistentData.CreateHelp('State deposit frequency is not set for state ' + DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value, 'STATE'), IDH_InconsistentData);
                PostStateLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, TAX_LIABILITY_TYPE_STATE, 'Y', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value);
                PostStateLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value, TAX_LIABILITY_TYPE_EE_SDI, 'Y', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[4].Value);
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;
            // Third-party independent
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('ThirdPartyIndependent') do
              begin
                SetParam('PrNbr', PayrollNumber);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if ctx_DataAccess.CUSTOM_VIEW.Fields[2].IsNull then
                  raise EInconsistentData.CreateHelp('State deposit frequency is not set for state ' + DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value, 'STATE'), IDH_InconsistentData);
                PostStateLiability(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, TAX_LIABILITY_TYPE_ER_SDI, 'N', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value, ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value);
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;


        (*
        
            // NV Business Tax is different from 1/1/2010:
            // QTD wages are < 62500:  wages * .005
            // QTD wages are > 62500:  wages * .0117
            // YTDTaxWages = QTD for NV Business Tax
            if SUI_Type_ER and (SYSUINumber = 118) and
              (CompareDate(CheckDate, EncodeDate(2009, 12, 31)) = GreaterThanValue) then
            begin
              if DoubleCompare(YTDTaxWages, coGreaterOrEqual, 62500.00) then
                SUITax := 0.0117*SUITaxWages
              else
              if DoubleCompare(SUITaxWages + YTDTaxWages, coLessOrEqual, 62500.00) then
                SUITax := SUITaxWages * 0.005
              else
              begin
                SUITax := ((62500.00 - YTDTaxWages) * 0.005) +
                          (SUITaxWages + YTDTaxWages - 62500.00) * 0.0117;
              end;
            end;
        *)


        // SUI Liabilities
            // Non third-party
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('ThirdPartySUILiab') do
              begin
                SetMacro('Filter', 'C.CHECK_TYPE<>:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, 0) <> 0 then
                begin
                  CO_SUI_LIABILITIES.Insert;
                  CO_SUI_LIABILITIES.FieldByName('CO_SUI_NBR').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                  CO_SUI_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                  CO_SUI_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                  if IsImport or Backdated then
                    CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
                  else
                    CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                  CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                  if Backdated then
                    CO_SUI_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                  CO_SUI_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value);
                  CO_SUI_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
                  CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                  CO_SUI_LIABILITIES.FieldByName('ACH_KEY').AsString := FormatDateTime('MM/DD/YYYY', GetEndQuarter(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime))+ Format('%1u', [GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime)]);
                  CO_SUI_LIABILITIES.Post;
                  FlipPostACHforTax;
                  Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
                end
                else
                begin
                  // NV Business Tax
                  DM_CLIENT.CO_SUI.DataRequired('CO_NBR=' + DM_CLIENT.PR.FieldByName('CO_NBR').AsString);
                  if (DM_CLIENT.CO_SUI.Lookup('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_SUI_NBR') = 118)
                  and (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2009, 12, 31)) = GreaterThanValue) then
                  begin

                    WagesPayroll := 0;
                    nvcd := TevClientDataSet.Create(nil);
                    nvcd.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
                    try
                      with TExecDSWrapper.Create(
                        'select SUM(p.SUI_TAXABLE_WAGES) as WAGES '+
                        'from PR_CHECK_SUI p '+
                        'where p.PR_NBR=:PR_NBR '+
                        'and p.CO_SUI_NBR=:CO_SUI_NBR '+
                        'and {AsOfNow<p>} ') do
                      begin
                        SetParam('PR_NBR', PayrollNumber);
                        SetParam('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value);
                        nvcd.DataRequest(AsVariant);
                      end;
                      nvcd.Open;
                      WagesPayroll := nvcd.Fields[0].AsCurrency;
                    finally
                      nvcd.Free;
                    end;

                    WagesQTD := 0;

                    nvcd := TevClientDataSet.Create(nil);
                    nvcd.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
                    try
                      with TExecDSWrapper.Create(
                        'select SUM(p.SUI_TAXABLE_WAGES) '+
                        'from PR_CHECK_SUI p '+
                        'join PR r on r.PR_NBR=p.PR_NBR '+
                        'where p.CO_SUI_NBR=:CO_SUI_NBR '+
                        'and r.{PayrollProcessedClause} '+
                        'and r.CHECK_DATE between :BeginDate and :EndDate '+
                        'and p.PR_NBR<>:PR_NBR '+
                        'and {AsOfNow<p>} and {AsOfNow<r>}'
                      ) do
                      begin
                        SetParam('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value);
                        SetParam('PR_NBR', PayrollNumber);
                        SetParam('BeginDate', GetBeginQuarter(PR['CHECK_DATE']));
                        SetParam('EndDate', GetEndQuarter(PR['CHECK_DATE']));
                        nvcd.DataRequest(AsVariant);
                      end;
                      nvcd.Open;
                      WagesQTD := nvcd.Fields[0].AsCurrency;
                    finally
                      nvcd.Free;
                    end;

                    SUITax := 0;
                    
                    if CompareDate(PR.CHECK_DATE.AsDateTime, EncodeDate(2015,  6, 30)) = GreaterThanValue then
                    begin
                      if DoubleCompare(WagesQTD, coGreaterOrEqual, 50000.00) then
                        SUITax := 0.01475*WagesPayroll
                      else
                      if DoubleCompare(WagesPayroll + WagesQTD, coLessOrEqual, 50000.00) then
                        SUITax := 0
                      else
                      begin
                        SUITax := (WagesPayroll + WagesQTD - 50000.00) * 0.01475;
                      end;
                    end
                    else
                    if CompareDate(PR.CHECK_DATE.AsDateTime, EncodeDate(2013,  6, 30)) = GreaterThanValue then
                    begin
                      if DoubleCompare(WagesQTD, coGreaterOrEqual, 85000.00) then
                        SUITax := 0.0117*WagesPayroll
                      else
                      if DoubleCompare(WagesPayroll + WagesQTD, coLessOrEqual, 85000.00) then
                        SUITax := 0
                      else
                      begin
                        SUITax := (WagesPayroll + WagesQTD - 85000.00) * 0.0117;
                      end;
                    end
                    else
                    if CompareDate(PR.CHECK_DATE.AsDateTime, EncodeDate(2011,  6, 30)) = GreaterThanValue then
                    begin
                      if DoubleCompare(WagesQTD, coGreaterOrEqual, 62500.00) then
                        SUITax := 0.0117*WagesPayroll
                      else
                      if DoubleCompare(WagesPayroll + WagesQTD, coLessOrEqual, 62500.00) then
                        SUITax := 0
                      else
                      begin
                        SUITax := (WagesPayroll + WagesQTD - 62500.00) * 0.0117;
                      end;
                    end
                    else
                    begin
                      // old logic
                      if DoubleCompare(WagesQTD, coGreaterOrEqual, 62500.00) then
                        SUITax := 0.0117*WagesPayroll
                      else
                      if DoubleCompare(WagesPayroll + WagesQTD, coLessOrEqual, 62500.00) then
                        SUITax := WagesPayroll * 0.005
                      else
                      begin
                        SUITax := ((62500.00 - WagesQTD) * 0.005) +
                                  (WagesPayroll + WagesQTD - 62500.00) * 0.0117;
                      end;
                    end;

                    if Abs(SUITax) > 0.005 then
                    begin
                      CO_SUI_LIABILITIES.Insert;
                      CO_SUI_LIABILITIES.FieldByName('CO_SUI_NBR').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                      CO_SUI_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                      CO_SUI_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                      if IsImport or Backdated then
                        CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
                      else
                        CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                      CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                      if Backdated then
                        CO_SUI_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                      CO_SUI_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(SUITax);
                      CO_SUI_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
                      CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                      CO_SUI_LIABILITIES.FieldByName('ACH_KEY').AsString := FormatDateTime('MM/DD/YYYY', GetEndQuarter(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime))+ Format('%1u', [GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime)]);
                      CO_SUI_LIABILITIES.Post;
                    end;  
                    FlipPostACHforTax;
                    Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
                  end;
                end;
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;
            // Third-party
              ctx_DataAccess.CUSTOM_VIEW.Close;
              with TExecDSWrapper.Create('ThirdPartySUILiab') do
              begin
                SetMacro('Filter', 'C.CHECK_TYPE=:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, 0) <> 0 then
                begin
                  CO_SUI_LIABILITIES.Insert;
                  CO_SUI_LIABILITIES.FieldByName('CO_SUI_NBR').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                  CO_SUI_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                  CO_SUI_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                  CO_SUI_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value);
                  CO_SUI_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
                  if Is_ER_SUI(DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR',
                  ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_SUI_NBR'), 'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) then
                  begin
                    if IsImport or Backdated then
                      CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
                    else
                      CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                    CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                    if Backdated then
                      CO_SUI_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                    CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                    Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
                  end
                  else
                  begin
                    if DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_SUI_NBR') = 112 {PA EE SUI} then
                    begin
                      CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                      CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                      CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                    end
                    else
                    begin
                      CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID;
                      CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                      CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'Y';
                    end;
                  end;
                  CO_SUI_LIABILITIES.FieldByName('ACH_KEY').AsString := FormatDateTime('MM/DD/YYYY', GetEndQuarter(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime))+ Format('%1u', [GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime)]);
                  CO_SUI_LIABILITIES.Post;
                  FlipPostACHforTax;
                end
                else
                begin
                  DM_CLIENT.CO_SUI.DataRequired('CO_NBR=' + DM_CLIENT.PR.FieldByName('CO_NBR').AsString);
                  if (DM_CLIENT.CO_SUI.Lookup('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_SUI_NBR') = 118)
                  and (CompareDate(PR.FieldByName('CHECK_DATE').AsDateTime, EncodeDate(2009, 12, 31)) = GreaterThanValue) then
                  begin
                    CO_SUI_LIABILITIES.Insert;
                    CO_SUI_LIABILITIES.FieldByName('CO_SUI_NBR').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                    CO_SUI_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    CO_SUI_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    CO_SUI_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(777);
                    CO_SUI_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
                    if Is_ER_SUI(DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR',
                    ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_SUI_NBR'), 'EE_ER_OR_EE_OTHER_OR_ER_OTHER')) then
                    begin
                      if IsImport or Backdated then
                        CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
                      else
                        CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                      CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                      if Backdated then
                        CO_SUI_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                      CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                      Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
                    end
                    else
                    begin
                      if DM_HISTORICAL_TAXES.CO_SUI.Lookup('CO_SUI_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_SUI_NBR') = 112 {PA EE SUI} then
                      begin
                        CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                        CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                        CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                      end
                      else
                      begin
                        CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID;
                        CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                        CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'Y';
                      end;
                    end;
                    CO_SUI_LIABILITIES.FieldByName('ACH_KEY').AsString := FormatDateTime('MM/DD/YYYY', GetEndQuarter(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime))+ Format('%1u', [GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime)]);
                    CO_SUI_LIABILITIES.Post;
                    FlipPostACHforTax;
                  end;
                end;
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;

              DM_HISTORICAL_TAXES.CO_SUI.First;
              while not DM_HISTORICAL_TAXES.CO_SUI.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if (DM_HISTORICAL_TAXES.CO_SUI.FieldByName('CO_NBR').Value = DM_CLIENT.PR.FieldByName('CO_NBR').Value)
                and (DM_HISTORICAL_TAXES.CO_SUI.FieldByName('FINAL_TAX_RETURN').Value = GROUP_BOX_NO) // Really means "SUI Inactive"
                and (DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').Value, 'SUI_ACTIVE') = 'Y')
                and not VarIsNull(DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').Value, 'E_D_CODE_TYPE')) then
                begin
                  if ctx_DataAccess.CUSTOM_VIEW.Active then
                    ctx_DataAccess.CUSTOM_VIEW.Close;
                  with TExecDSWrapper.Create('GetTotalForEDType') do
                  begin
                    SetParam('PrNbr', PayrollNumber);
                    SetParam('EDType', String(DM_HISTORICAL_TAXES.SY_SUI.Lookup('SY_SUI_NBR', DM_HISTORICAL_TAXES.CO_SUI.FieldByName('SY_SUI_NBR').Value, 'E_D_CODE_TYPE')));
                    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                  end;
                  ctx_DataAccess.CUSTOM_VIEW.Open;
                  ctx_DataAccess.CUSTOM_VIEW.First;
                  if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0) <> 0 then
                  begin
                    CO_SUI_LIABILITIES.Insert;
                    CO_SUI_LIABILITIES.FieldByName('CO_SUI_NBR').Value := DM_HISTORICAL_TAXES.CO_SUI.FieldByName('CO_SUI_NBR').Value;
                    CO_SUI_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    CO_SUI_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    if IsImport or Backdated then
                      CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
                    else
                      CO_SUI_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                    CO_SUI_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                    if Backdated then
                      CO_SUI_LIABILITIES.FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                    CO_SUI_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value);
                    CO_SUI_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
                    CO_SUI_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                    CO_SUI_LIABILITIES.FieldByName('ACH_KEY').AsString := FormatDateTime('MM/DD/YYYY', GetEndQuarter(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime))+ Format('%1u', [GetQuarterNumber(DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime)]);
                    CO_SUI_LIABILITIES.Post;
                    FlipPostACHforTax;
                    Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                  end;
                end;
                DM_HISTORICAL_TAXES.CO_SUI.Next;
              end;

              // Local Liabilities
              DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ.Activate;
              DM_SYSTEM_STATE.SY_LOCALS.Activate;
              DM_CLIENT.CO_LOCAL_TAX.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);

              // Non third-party
              LocalLiab := TisParamsCollection.Create;

              SQLQuery := TevQuery.Create(
                ' select distinct C.EE_NBR, L.EE_LOCALS_NBR, L.NONRES_EE_LOCALS_NBR'+
                ' from PR_CHECK_LOCALS L'+
                ' join PR_CHECK C on C.PR_CHECK_NBR=L.PR_CHECK_NBR'+
                ' join EE_LOCALS E on E.EE_LOCALS_NBR=L.EE_LOCALS_NBR'+
                ' where C.PR_NBR=:PrNbr and'+
                ' L.PR_NBR=:PrNbr and L.LOCAL_TAX <> 0 and'+
                ' C.CHECK_TYPE<>''' + CHECK_TYPE2_3RD_PARTY + ''' and ' +
                ' {AsOfNow<L>} and {AsOfNow<C>} and {AsOfNow<E>}');
              SQLQuery.Params.AddValue('PrNbr', PayrollNumber);

              PaWarningList := TisListOfValues.Create;

              SQLQuery.Result.First;
              while not SQLQuery.Result.Eof do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if not PaWarningList.ValueExists(SQLQuery.Result.FieldByName('EE_NBR').AsString)
                and HT.IsAct32(SQLQuery.Result['EE_LOCALS_NBR']) then
                begin
                  if SQLQuery.Result.FieldByName('NONRES_EE_LOCALS_NBR').AsInteger = 0 then
                  begin
                    PaWarningList.AddValue(SQLQuery.Result.FieldByName('EE_NBR').AsString, 1);                  
                    DM_CLIENT.EE.Locate('EE_NBR', SQLQuery.Result['EE_NBR'], []);
                      AddHint('PA locals are not set up correctly for EE#' + DM_CLIENT.EE.CUSTOM_EMPLOYEE_NUMBER.AsString + '.  Liabilities may not have the correct Work Location attached.');
                  end;
                end;

                SQLQuery.Result.Next;
              end;

              SQLQuery := TevQuery.Create(
                ' select E.CO_LOCAL_TAX_NBR, L.CO_LOCATIONS_NBR,'+
                ' N.CO_LOCAL_TAX_NBR NONRES_CO_LOCAL_TAX_NBR,'+
                ' sum(L.LOCAL_TAX) LOCAL_TAX'+
                ' from PR_CHECK_LOCALS L'+
                ' join PR_CHECK C on C.PR_CHECK_NBR=L.PR_CHECK_NBR'+
                ' join EE_LOCALS E on E.EE_LOCALS_NBR=L.EE_LOCALS_NBR'+
                ' left join EE_LOCALS N on N.EE_LOCALS_NBR=L.NONRES_EE_LOCALS_NBR and {AsOfNow<N>} '+
                ' where C.PR_NBR=:PrNbr and'+
                ' L.PR_NBR=:PrNbr and L.LOCAL_TAX <> 0 and'+
                ' C.CHECK_TYPE<>''' + CHECK_TYPE2_3RD_PARTY + ''' and ' +
                ' {AsOfNow<L>} and'+
                ' {AsOfNow<C>} and'+
                ' {AsOfNow<E>} '+
                ' group by 1,2,3'+
                ' order by 1,2,3');

              SQLQuery.Params.AddValue('PrNbr', PayrollNumber);

              SQLQuery.Result.First;
              while not SQLQuery.Result.Eof do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if Abs(SQLQuery.Result['LOCAL_TAX']) > 0.005 then
                begin
                  CO_LOCAL_TAX_LIABILITIES.Insert;
                  CO_LOCAL_TAX_LIABILITIES['CO_LOCAL_TAX_NBR'] := SQLQuery.Result['CO_LOCAL_TAX_NBR'];
                  CO_LOCAL_TAX_LIABILITIES['DUE_DATE'] := PR['CHECK_DATE'];
                  CO_LOCAL_TAX_LIABILITIES['CHECK_DATE'] := PR['CHECK_DATE'];
                  if IsImport or Backdated then
                    CO_LOCAL_TAX_LIABILITIES['STATUS'] := TAX_DEPOSIT_STATUS_PAID
                  else
                    CO_LOCAL_TAX_LIABILITIES['STATUS'] := TAX_DEPOSIT_STATUS_PENDING;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                  if Backdated then
                    CO_LOCAL_TAX_LIABILITIES['CO_TAX_DEPOSITS_NBR'] := DM_CLIENT.CO_TAX_DEPOSITS['CO_TAX_DEPOSITS_NBR'];

                  CO_LOCAL_TAX_LIABILITIES['AMOUNT'] := SQLQuery.Result['LOCAL_TAX'];

                  CO_LOCAL_TAX_LIABILITIES['PR_NBR'] := PayrollNumber;
                  CO_LOCAL_TAX_LIABILITIES['THIRD_PARTY'] := 'N';

                  SyLocNbr := DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', SQLQuery.Result['CO_LOCAL_TAX_NBR'], 'SY_LOCALS_NBR');

                  CO_LOCAL_TAX_LIABILITIES['ACH_KEY'] :=
                    ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(LOCAL_TAX_DESC,
                         DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', SQLQuery.Result['CO_LOCAL_TAX_NBR'], 'SY_LOCAL_DEPOSIT_FREQ_NBR'),
                         SyLocNbr, DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime);

                  CO_LOCAL_TAX_LIABILITIES['CO_LOCATIONS_NBR'] := SQLQuery.Result['CO_LOCATIONS_NBR'];

                  if SQLQuery.Result.FieldByName('NONRES_CO_LOCAL_TAX_NBR').AsInteger <> 0 then
                  begin
                    if HT.SY_LOCALS.Lookup('SY_LOCALS_NBR', SyLocNbr, 'COMBINE_FOR_TAX_PAYMENTS') = 'N' then
                      CO_LOCAL_TAX_LIABILITIES.FieldByName('NONRES_CO_LOCAL_TAX_NBR').AsString := SQLQuery.Result['CO_LOCAL_TAX_NBR']
                    else
                      CO_LOCAL_TAX_LIABILITIES.FieldByName('NONRES_CO_LOCAL_TAX_NBR').AsString := SQLQuery.Result['NONRES_CO_LOCAL_TAX_NBR'];

                    CO_LOCAL_TAX_LIABILITIES.FieldByName('FILLER').AsString :=
                      PutIntoFiller(CO_LOCAL_TAX_LIABILITIES.FieldByName('FILLER').AsString,
                        CO_LOCAL_TAX_LIABILITIES.FieldByName('NONRES_CO_LOCAL_TAX_NBR').AsString, 32, 10);

                    DM_CLIENT.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', SQLQuery.Result['NONRES_CO_LOCAL_TAX_NBR'], []);

                    CO_LOCAL_TAX_LIABILITIES.FieldByName('FILLER').AsString :=
                      PutIntoFiller(CO_LOCAL_TAX_LIABILITIES.FieldByName('FILLER').AsString,
                        DM_CLIENT.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsString, 22, 10);

                  end;

                  CO_LOCAL_TAX_LIABILITIES.Post;
                  FlipPostACHforTax;
                  Impound := Impound + CO_LOCAL_TAX_LIABILITIES['AMOUNT'];

                end;
                SQLQuery.Result.Next;
              end;


              // Third-party
              ctx_DataAccess.CUSTOM_VIEW.Close;

              with TExecDSWrapper.Create('ThirdPartyLocalLiab') do
              begin
                SetMacro('Filter', 'C.CHECK_TYPE=:CheckType');
                SetParam('PrNbr', PayrollNumber);
                SetParam('CheckType', CHECK_TYPE2_3RD_PARTY);
                ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
              end;
              ctx_DataAccess.CUSTOM_VIEW.Open;
              ctx_DataAccess.CUSTOM_VIEW.First;
              while not ctx_DataAccess.CUSTOM_VIEW.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value, 0) <> 0 then
                begin
                  CO_LOCAL_TAX_LIABILITIES.Insert;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('CO_LOCAL_TAX_NBR').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('AMOUNT').Value := RoundTwo(ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value);
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('PR_NBR').Value := PayrollNumber;
                  if DM_HISTORICAL_TAXES.SY_LOCALS.Lookup('SY_LOCALS_NBR', DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR',
                  ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_LOCALS_NBR'), 'TAX_TYPE') = GROUP_BOX_EE then
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'Y'
                  else
                  begin
                    if not IsImport then
                      CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('THIRD_PARTY').Value := 'N';
                    Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
                  end;
                  CO_LOCAL_TAX_LIABILITIES.FieldByName('ACH_KEY').AsString := ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(LOCAL_TAX_DESC,
                                                                                 DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_LOCAL_DEPOSIT_FREQ_NBR'),
                                                                                 DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 'SY_LOCALS_NBR'),
                                                                                 DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime);
                  CO_LOCAL_TAX_LIABILITIES.Post;
                  FlipPostACHforTax;
                end;
                ctx_DataAccess.CUSTOM_VIEW.Next;
              end;

              DM_CLIENT.CO_LOCAL_TAX.First;
              while not DM_CLIENT.CO_LOCAL_TAX.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if DM_CLIENT.CO_LOCAL_TAX.FieldByName('CO_NBR').Value = DM_CLIENT.PR.FieldByName('CO_NBR').Value then
                if not DM_CLIENT.CO_LOCAL_TAX.FieldByName('CL_E_DS_NBR').IsNull then
                begin
                  ctx_DataAccess.CUSTOM_VIEW.Close;
                  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
                  begin
                    SetMacro('Columns', 'sum(AMOUNT)');
                    SetMacro('TableName', 'PR_CHECK_LINES');
                    SetMacro('Condition', 'PR_NBR=' + IntToStr(PayrollNumber) + ' and CL_E_DS_NBR=' + DM_CLIENT.CO_LOCAL_TAX.FieldByName('CL_E_DS_NBR').AsString);
                    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                  end;
                  ctx_DataAccess.CUSTOM_VIEW.Open;
                  ctx_DataAccess.CUSTOM_VIEW.First;
                  if ConvertNull(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value, 0) <> 0 then
                  with CO_LOCAL_TAX_LIABILITIES do
                  begin
                    CO_LOCAL_TAX_LIABILITIES.Insert;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('CO_LOCAL_TAX_NBR').Value := DM_CLIENT.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').Value;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('DUE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    if IsImport or Backdated then
                      CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID
                    else
                      CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PENDING;
                    CO_LOCAL_TAX_LIABILITIES.FieldByName('STATUS_DATE').Value := Now;
                    if Backdated then
                      FieldByName('CO_TAX_DEPOSITS_NBR').Value := DM_CLIENT.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                    FieldByName('AMOUNT').Value := RoundTwo(ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value);
                    FieldByName('PR_NBR').Value := PayrollNumber;
                    FieldByName('THIRD_PARTY').Value := 'N';
                    FieldByName('ACH_KEY').AsString := ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(LOCAL_TAX_DESC,
                                                                                   DM_CLIENT.CO_LOCAL_TAX.FieldByName('SY_LOCAL_DEPOSIT_FREQ_NBR').Value,
                                                                                   DM_CLIENT.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').Value,
                                                                                   DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime);

                    CO_LOCAL_TAX_LIABILITIES.Post;
                    FlipPostACHforTax;
                    Impound := Impound + ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
                  end;
                end;
                DM_CLIENT.CO_LOCAL_TAX.Next;
              end;

              MyTransactionManager.ApplyUpdates;
            end;

            if not IsImport and not (PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_REVERSAL]) then
            begin
              if PR.TAX_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
                CreateImpoundCheck(CREATE_TAX_CHECK, Impound);
              if PR.TRUST_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
              begin
                Impound := 0;
                PR_CHECK.First;
                with PR_CHECK do
                while not EOF do
                begin
                  if FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_VOID] then
                    Impound := Impound + FieldByName('NET_WAGES').Value;
                  Next;
                end;
                PR_MISCELLANEOUS_CHECKS.First;
                with PR_MISCELLANEOUS_CHECKS do
                while not EOF do
                begin
                  AbortIfCurrentThreadTaskTerminated;
                  if PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY, MISC_CHECK_TYPE_AGENCY_VOID] then
                    Impound := Impound + FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value;
                  Next;
                end;
                if PR.TRUST_IMPOUND.AsString = PAYMENT_TYPE_INTERNAL_CHECK then
                  CreateImpoundCheck(CREATE_TRUST_CHECK, Impound);
              end;
              ctx_DataAccess.PostDataSets([DM_CLIENT.PR_MISCELLANEOUS_CHECKS]);
            end;

            // Status payroll as processed, update calendar and Clear Qurter End Processing speed optimization flag
            MyTransactionManager.Initialize([DM_CLIENT.CO, PR, PR_SCHEDULED_EVENT]);

            if ExtractFromFiller(ConvertNull(DM_CLIENT.CO.FieldByName('FILLER').Value, ''), 41, 10) <> '' then
              PutIntoFiller(DM_CLIENT.CO.FieldByName('FILLER').AsString, '', 41, 10);
            if ExtractFromFiller(ConvertNull(DM_CLIENT.CO.FieldByName('FILLER').Value, ''), 51, 10) <> '' then
              PutIntoFiller(DM_CLIENT.CO.FieldByName('FILLER').AsString, '', 51, 10);

            PR.Edit;
            PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSED;
            PR.FieldByName('STATUS_DATE').Value := Now;
            PR.Post;

            if (PR.FieldByName('SCHEDULED').AsString = 'Y') and (not IsImport) then
            begin
              PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + PR.FieldByName('CO_NBR').AsString);
              if PR_SCHEDULED_EVENT.Locate('PR_NBR', PR.FieldByName('PR_NBR').Value, []) then
              begin
                PR_SCHEDULED_EVENT.Edit;
                PR_SCHEDULED_EVENT.FieldByName('NORMAL_CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                PR_SCHEDULED_EVENT.FieldByName('ACTUAL_PROCESS_DATE').Value := PR.FieldByName('PROCESS_DATE').Value;
                PR_SCHEDULED_EVENT.Post;
              end;
            end;

            MyTransactionManager.ApplyUpdates;
          finally
            MyTransactionManager.CancelUpdates;
            MyTransactionManager.Free;
            ctx_DataAccess.PayrollIsProcessing := False;
            PR_CHECK_LINES_2.Free;
            PR_CHECK_STATES_2.Free;
            PR_CHECK_SUI_2.Free;
            PR_CHECK_LOCALS_2.Free;
            ctx_PayrollCalculation.EraseLimitedEDs;
            ctx_PayrollCalculation.EraseLimitedTaxes;
          end;
          ctx_DataAccess.PayrollIsProcessing := True;
          ctx_DataAccess.UnlockPR;
          AllOK := False;
          try
      // Do Tax Deposits
            if not IsImport
            and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_TAX_DEPOSIT)
            and (DM_CLIENT.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString[1] in [GROUP_BOX_LIABILITIES, GROUP_BOX_NONE]) then
            begin
              PostFeedback(True, 'Processing tax deposits...');
              CalcDueDates.Calculate(PayrollNumber, DM_CLIENT.PR.FieldByName('CHECK_DATE').AsDateTime, Result);
            end;

      // Check if there is any direct debit billing
            if (not PostACH) and (not IsImport) and (DM_CLIENT.PR.FieldByName('EXCLUDE_BILLING').Value <> GROUP_BOX_YES) then
            begin
              if (DM_CLIENT.PR.BILLING_IMPOUND.AsString <> PAYMENT_TYPE_NONE) and (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
                PostAch := True;
            end;

            // Restore printed tax returns
            if (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT)
            and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_TAX_DEPOSIT)
            and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_IMPORT)
            and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_SETUP) then
            begin
              with ctx_DataAccess.CUSTOM_VIEW do
              begin
                Close;
                with TExecDSWrapper.Create('CheckReturnQueueForProcessedReturns') do
                begin
                  SetParam('CoNbr', DM_CLIENT.CO['CO_NBR']);
                  SetMacro('Dates', ''''+ DateToStr(GetEndMonth(DM_CLIENT.PR['CHECK_DATE']))+''','''+
                    DateToStr(GetEndQuarter(DM_CLIENT.PR['CHECK_DATE']))+''','''+
                    DateToStr(GetEndYear(DM_CLIENT.PR['CHECK_DATE']))+'''');
                  DataRequest(AsVariant);
                end;
                Open;
                if RecordCount > 0 then
                begin
                  PostFeedback(True, 'Statusing tax returns accordingly...');
                  DM_CLIENT.CO.Close;
                  DM_CLIENT.CO.DataRequired('ALL');
                  Assert(DM_CLIENT.CO.Locate('CO_NBR', DM_CLIENT.PR.FieldByName('CO_NBR').AsInteger, []));
                  DM_CLIENT.CO_TAX_RETURN_QUEUE.DataRequired('period_end_date in ('+
                    ''''+ DateToStr(GetEndMonth(DM_CLIENT.PR['CHECK_DATE']))+''','''+
                    DateToStr(GetEndQuarter(DM_CLIENT.PR['CHECK_DATE']))+''','''+
                    DateToStr(GetEndYear(DM_CLIENT.PR['CHECK_DATE']))+''')');
                  First;
                  while not Eof do
                  begin
                    Assert(DM_CLIENT.CO_TAX_RETURN_QUEUE.Locate('CO_TAX_RETURN_QUEUE_NBR', FieldValues['CO_TAX_RETURN_QUEUE_NBR'], []));
                    DM_CLIENT.CO_TAX_RETURN_QUEUE.Edit;
                    if (DM_CLIENT.CO_TAX_RETURN_QUEUE['STATUS'] = TAX_RETURN_STATUS_PROCESSED) then
                      DM_CLIENT.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED;
                    //DM_CLIENT.CO_TAX_RETURN_QUEUE['sb_copy_printed;client_copy_printed;agency_copy_printed'] :=
                    //  VarArrayOf([GROUP_BOX_NO, GROUP_BOX_NO, GROUP_BOX_NO]);
                    DM_CLIENT.CO_TAX_RETURN_QUEUE.Post;
                    Next;
                  end;
                  ctx_DataAccess.PostDataSets([DM_CLIENT.CO_TAX_RETURN_QUEUE]);
                  AddHint('Tax information has been changed. '+ IntToStr(RecordCount)+ ' processed tax return(s) have to be reprocessed.');
                end;
                Close;
              end;
              DM_CLIENT.CO.Edit;

              if StartsWith(DM_CLIENT.CO.FieldByName('LAST_PREPROCESS_MESSAGE').AsString,'Manually Reconciled-2015 940 Credit Adjustment') and
                 (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString[1] = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT) and
                 (DM_CLIENT.PR.FieldByName('SCHEDULED').AsString[1] = 'N') then
              begin
                 DM_CLIENT.CO['LAST_PREPROCESS'] := GetEndQuarter(DM_CLIENT.PR['CHECK_DATE']);
                 AddHint('Tax information has been changed. Company manually reconciled.');
              end
              else
              begin
                if (ConvertNull(DM_CLIENT.CO['LAST_QUARTER_END_CORRECTION'], 0) = GetEndQuarter(DM_CLIENT.PR['CHECK_DATE']))
                or (ConvertNull(DM_CLIENT.CO['LAST_QUARTER_END_CORRECTION'], 0) = GetEndMonth(DM_CLIENT.PR['CHECK_DATE']))
                or (ConvertNull(DM_CLIENT.CO['LAST_QUARTER_END_CORRECTION'], 0) = GetEndYear(DM_CLIENT.PR['CHECK_DATE'])) then
                  DM_CLIENT.CO['LAST_QUARTER_END_CORRECTION'] := Null;
                if (ConvertNull(DM_CLIENT.CO['LAST_PREPROCESS'], 0) = GetEndQuarter(DM_CLIENT.PR['CHECK_DATE']))
                or (ConvertNull(DM_CLIENT.CO['LAST_PREPROCESS'], 0) = GetEndMonth(DM_CLIENT.PR['CHECK_DATE']))
                or (ConvertNull(DM_CLIENT.CO['LAST_PREPROCESS'], 0) = GetEndYear(DM_CLIENT.PR['CHECK_DATE'])) then
                begin
                  DM_CLIENT.CO['LAST_PREPROCESS'] := Null;
                  AddHint('Tax information has been changed. Company needs to be repreprocessed.');
                end;
              end;
              DM_CLIENT.CO.Post;
              ctx_DataAccess.PostDataSets([DM_CLIENT.CO]);
              // Enlist company if it is the last schedulled payroll in month
              CheckReturnQueue;

              if DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL then
              begin
                // Post a warning if a NY payroll was voided that has tax
                // payments made against it
                ctx_DataAccess.CUSTOM_VIEW.Close;
                with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
                begin
                  SetMacro('TableName', 'CO_STATES');
                  SetMacro('Columns', 'CO_STATES_NBR');
                  SetMacro('Condition', 'STATE = ''NY''');
                  ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                end;
                ctx_DataAccess.CUSTOM_VIEW.Open;
                try
                  ctx_DataAccess.CUSTOM_VIEW.First;
                  if not ctx_DataAccess.CUSTOM_VIEW.EOF then
                    CONYStateNbr := ctx_DataAccess.CUSTOM_VIEW.FieldByName('CO_STATES_NBR').AsInteger
                  else
                    CONYStateNbr := -1; // temp value to indicate there is no NY state for this co
                finally
                  ctx_DataAccess.CUSTOM_VIEW.Close;
                end;
                if CONYStateNbr > -1 then begin
                  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
                  begin
                    SetMacro('TableName', 'CO_STATE_TAX_LIABILITIES');
                    SetMacro('Columns', '*');
                    SetMacro('Condition', 'PR_NBR = '+ IntToStr(OldPayrollNbr) + ' and '+
                      'CO_STATES_NBR = '+ IntToStr(CONYStateNbr) + ' and '+
                      'CO_TAX_DEPOSITS_NBR is not null');
                    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
                  end;
                  ctx_DataAccess.CUSTOM_VIEW.Open;
                  try
                    ctx_DataAccess.CUSTOM_VIEW.First;
                    if not ctx_DataAccess.CUSTOM_VIEW.EOF then AddHint(
                      'This voided payroll had NY tax payments made against it. '+
                      'This means that the due dates may be incorrect. Please check all '+
                      'due dates for this company.');
                  finally
                    ctx_DataAccess.CUSTOM_VIEW.Close;
                  end;
                end;
              end;
            end;

            if not IsImport then
            begin
              PostFeedback(True, 'Preparing to assign check numbers...');

              CheckAccounts := TisParamsCollection.Create;
              SB_BANK_ACCOUNTSQuery := TevQuery.Create('SELECT SB_BANK_ACCOUNTS_NBR, EFFECTIVE_DATE FROM SB_BANK_ACCOUNTS a WHERE {AsOfNow<a>} ');
              SB_BANK_ACCOUNTSQuery.Execute;
              SB_BANK_ACCOUNTSQuery.Result.IndexFieldNames := 'SB_BANK_ACCOUNTS_NBR';

              DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Close;
              ctx_DBAccess.EnableReadTransaction;
              try
                DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;
              finally
                ctx_DBAccess.DisableReadTransaction;
              end;
              DM_CLIENT.CL_BANK_ACCOUNT.Close;
              DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
              DM_CLIENT.CO.DataRequired('ALL');
              DM_CLIENT.CO.Locate('CO_NBR', DM_CLIENT.PR.FieldByName('CO_NBR').Value, []);

              PR_CHECK_2.First;

              DM_CLIENT.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';

              while not PR_CHECK_2.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
//                if DM_CLIENT.PR_CHECK.Locate('PR_CHECK_NBR', PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value, []) then
                if DM_CLIENT.PR_CHECK.FindKey([PR_CHECK_2.FieldByName('PR_CHECK_NBR').Value]) then
                begin
                  if not (DM_CLIENT.PR_CHECK.CHECK_TYPE.AsString[1] in [CHECK_TYPE2_VOID, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER, CHECK_TYPE2_3RD_PARTY]) then
                  begin
                    CheckInfo := CheckAccounts.AddParams(DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsString);

                    if (DM_CLIENT.PR_CHECK.FieldByName('NET_WAGES').Value = 0)
                    and (DM_CLIENT.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsInteger < 0) then
                      CheckInfo.AddValue('Method', 'ctx_PayrollCalculation.GetNextPaymentSerialNbr')
                    else
                    begin
                      if ctx_PayrollCalculation.GetBankAccountNumber(DM_CLIENT.PR_CHECK, BankAcctNbr) then
                      begin
                        CheckInfo.AddValue('Method', 'GenerateCheckNumber(BankAcctNbr, 0)');
                        CheckInfo.AddValue('BankAcctNbr', BankAcctNbr);
                      end
                      else
                      begin
                        CheckInfo.AddValue('Method', 'GenerateCheckNumber(0, BankAcctNbr)');
                        CheckInfo.AddValue('BankAcctNbr', BankAcctNbr);
                      end;
                    end;

                    CheckInfo.AddValue('CUSTOM_PR_BANK_ACCT_NUMBER', ctx_PayrollCalculation.GetCustomBankAccountNumber(DM_CLIENT.PR_CHECK));
                    CheckInfo.AddValue('ABA_NUMBER', ctx_PayrollCalculation.GetABANumber);

                  end;
                end;
                PR_CHECK_2.Next;
              end;

              PostFeedback(True, 'Waiting to assign check numbers...');
              ctx_DBAccess.LockTableInTransaction('SB_BANK_ACCOUNTS'); // locking the table to prevent check number dups

              PostFeedback(True, 'Assigning check numbers to regular checks...');
    // Assign Check Numbers to Regular Checks

              DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Close;
              DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Activate;

              DM_CLIENT.CL_BANK_ACCOUNT.Close;
              DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
              DM_CLIENT.CO.DataRequired('ALL');
              DM_CLIENT.CO.Locate('CO_NBR', DM_CLIENT.PR.FieldByName('CO_NBR').Value, []);

              PR_CHECK_2.First;
              while not PR_CHECK_2.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                nbr := PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsInteger;
                PR_CHECK_2.Delete;
                if DM_CLIENT.PR_CHECK.FindKey([nbr]) then
                begin
                  CheckInfo := CheckAccounts.ParamsByName(IntToStr(nbr));
                  if not (DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_VOID, CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER, CHECK_TYPE2_3RD_PARTY])
                  and Assigned(CheckInfo) then
                  begin
                    DM_CLIENT.PR_CHECK.Edit;
                    repeat
                      if CheckInfo.Value['Method'] = 'ctx_PayrollCalculation.GetNextPaymentSerialNbr' then
                        DM_CLIENT.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := ctx_PayrollCalculation.GetNextPaymentSerialNbr
                      else
                      if CheckInfo.Value['Method'] = 'GenerateCheckNumber(BankAcctNbr, 0)' then
                        DM_CLIENT.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GenerateCheckNumber(CheckInfo.Value['BankAcctNbr'], 0)
                      else
                        DM_CLIENT.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GenerateCheckNumber(0, CheckInfo.Value['BankAcctNbr']);
                    until VarIsNull(PR_CHECK_2.Lookup('PAYMENT_SERIAL_NUMBER', DM_CLIENT.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value, 'PR_CHECK_NBR'));

                    DM_CLIENT.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').Value := CheckInfo.Value['CUSTOM_PR_BANK_ACCT_NUMBER'];
                    DM_CLIENT.PR_CHECK.FieldByName('ABA_NUMBER').Value := CheckInfo.Value['ABA_NUMBER'];
                    DM_CLIENT.PR_CHECK.Post;
                  end;
                end;
              end;

              PostFeedback(True, 'Assigning check numbers to misc. checks...');
      // Assign Check Numbers to Misc. Checks
              DM_CLIENT.CL_AGENCY.Activate;
              PR_MISCELLANEOUS_CHECKS_2 := TevClientDataSet.Create(Nil);
              with DM_CLIENT, DM_CLIENT do
              try
                PR_MISCELLANEOUS_CHECKS_2.Data := PR_MISCELLANEOUS_CHECKS.Data;
                PR_MISCELLANEOUS_CHECKS_2.First;
                while not PR_MISCELLANEOUS_CHECKS_2.EOF do
                begin
                  AbortIfCurrentThreadTaskTerminated;
                  Assert(PR_MISCELLANEOUS_CHECKS.Locate('PR_MISCELLANEOUS_CHECKS_NBR', PR_MISCELLANEOUS_CHECKS_2.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value, []));
                  PR_MISCELLANEOUS_CHECKS_2.Delete;
                  if not PR_MISCELLANEOUS_CHECKS.FieldByName('CL_AGENCY_NBR').IsNull then
                  begin
                    if CL_AGENCY.Lookup('CL_AGENCY_NBR', PR_MISCELLANEOUS_CHECKS.FieldByName('CL_AGENCY_NBR').Value, 'PAYMENT_TYPE') = Payment_Type_DirectDeposit then
                    begin
                      PR_MISCELLANEOUS_CHECKS.Edit;
                      PR_MISCELLANEOUS_CHECKS.FieldByName('CHECK_STATUS').Value := MISC_CHECK_STATUS_PENDING_FOR_ACH;
                      PR_MISCELLANEOUS_CHECKS.Post;
                      Continue;
                    end;
                  end;

                  if (DM_CLIENT.PR.DD_IMPOUND.AsString <> PAYMENT_TYPE_NONE) and (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
                    PostACH := True;

                  if PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1]
                  in [MISC_CHECK_TYPE_AGENCY, MISC_CHECK_TYPE_TAX, MISC_CHECK_TYPE_BILLING] then
                  begin
                    PR_MISCELLANEOUS_CHECKS.Edit;
                    repeat
                      if ctx_PayrollCalculation.GetBankAccountNumberForMisc(PR_MISCELLANEOUS_CHECKS, BankAcctNbr) then
                        PR_MISCELLANEOUS_CHECKS.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GenerateCheckNumber(BankAcctNbr, 0)
                      else
                        PR_MISCELLANEOUS_CHECKS.FieldByName('PAYMENT_SERIAL_NUMBER').Value := GenerateCheckNumber(0, BankAcctNbr);
                    until VarIsNull(PR_MISCELLANEOUS_CHECKS_2.Lookup('PAYMENT_SERIAL_NUMBER',
                      PR_MISCELLANEOUS_CHECKS.FieldByName('PAYMENT_SERIAL_NUMBER').Value, 'PR_MISCELLANEOUS_CHECKS_NBR'));
                    PR_MISCELLANEOUS_CHECKS.Post;
                  end;
                end;
              finally
                PR_MISCELLANEOUS_CHECKS_2.Free;
              end;
            end;

            if not WasSetupRun then
            with DM_CLIENT, DM_CLIENT do
            begin
              PostFeedback(True, 'Posting to bank account register...');
      // Post Regular Checks to check recon.
              PR_CHECK.First;
              while not PR_CHECK.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if (not JustPreProcess) and (PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_REGULAR, CHECK_TYPE2_VOID]) then
                begin
                  if (not VarIsNull(PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value)) and
                  (ConvertNull(PR_CHECK.FieldByName('NET_WAGES').Value, 0) <> 0) then
                  if ctx_PayrollCalculation.GetBankAccountNumber(PR_CHECK, BankAcctNbr) then
                  begin
//                    PostACH := True;
                    CO_BANK_ACCOUNT_REGISTER.Insert;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').Value := BankAcctNbr;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_SERIAL_NUMBER').Value := PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').Value := BANK_REGISTER_STATUS_Outstanding;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS_DATE').Value := Now;
                    if PR.FieldByName('CHECK_DATE').Value < Int(SysTime) then
                      CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := Int(SysTime)
                    else
                      CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').Value := PR_CHECK.FieldByName('NET_WAGES').Value * (-1);
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').Value := PR_CHECK.FieldByName('CHECK_TYPE').Value;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').Value := BANK_REGISTER_MANUALTYPE_None;
                    CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').Value := Int(SysTime);
                    CO_BANK_ACCOUNT_REGISTER.Post;
                  end;
                end;
                PR_CHECK.Next;
              end;
            end;

    // Post Misc. Checks to check recon.
            DM_CLIENT.PR_MISCELLANEOUS_CHECKS.First;
            with DM_CLIENT, DM_CLIENT do
            while not PR_MISCELLANEOUS_CHECKS.EOF do
            begin
              AbortIfCurrentThreadTaskTerminated;
              if PR_MISCELLANEOUS_CHECKS.FieldByName('CHECK_STATUS').Value <> MISC_CHECK_STATUS_PENDING_FOR_ACH then
              if ctx_PayrollCalculation.GetBankAccountNumberForMisc(PR_MISCELLANEOUS_CHECKS, BankAcctNbr) then
              begin
                if not (PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_TAX,
                MISC_CHECK_TYPE_TAX_VOID]) and (DM_CLIENT.PR.EXCLUDE_ACH.AsString <> GROUP_BOX_YES) then
                  PostACH := True;

                CO_BANK_ACCOUNT_REGISTER.Insert;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').Value := BankAcctNbr;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value :=
                  PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_SERIAL_NUMBER').Value :=
                  PR_MISCELLANEOUS_CHECKS.FieldByName('PAYMENT_SERIAL_NUMBER').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').Value := BANK_REGISTER_STATUS_Outstanding;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS_DATE').Value := Now;
                if PR.FieldByName('CHECK_DATE').Value < Int(SysTime) then
                  CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := Int(SysTime)
                else
                  CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := PR.FieldByName('CHECK_DATE').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').Value := PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_AMOUNT').Value * (-1);
                CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').Value := PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').Value := BANK_REGISTER_MANUALTYPE_None;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').Value := Int(SysTime);
                CO_BANK_ACCOUNT_REGISTER.Post;
              end;

              PR_MISCELLANEOUS_CHECKS.Next;
            end;

      // Mark liabilities related to tax deposit misc. checks as "paid"
            if DM_CLIENT.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString[1] in [GROUP_BOX_DEPOSIT, GROUP_BOX_NONE] then
            if not IsImport then
            with DM_CLIENT, DM_CLIENT do
            begin
              DM_CLIENT.CO_FED_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
              DM_CLIENT.CO_STATE_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
              DM_CLIENT.CO_SUI_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
              DM_CLIENT.CO_LOCAL_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
              PR_MISCELLANEOUS_CHECKS.First;
              while not PR_MISCELLANEOUS_CHECKS.EOF do
              begin
                AbortIfCurrentThreadTaskTerminated;
                if (not PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull) and
                  (PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value <> MISC_CHECK_TYPE_TAX_VOID) then
                begin
                  if CO_TAX_DEPOSITS.Locate('CO_TAX_DEPOSITS_NBR', PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').Value, []) then
                  begin
                    CO_TAX_DEPOSITS.Edit;
                    CO_TAX_DEPOSITS.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID;
                    CO_TAX_DEPOSITS.FieldByName('STATUS_DATE').Value := Now;
                    CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').Value := 'Check# ' + PR_MISCELLANEOUS_CHECKS.FieldByName('PAYMENT_SERIAL_NUMBER').AsString;
                    CO_TAX_DEPOSITS.Post;
                    ctx_PayrollCalculation.UpdateAllLiabilityTablesStatus(PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger);
                  end;
                end;
                PR_MISCELLANEOUS_CHECKS.Next;
              end;
            end;
            // #67074 + #68085
            // PF 4.4 #68085
            if not ((DM_CLIENT.PR.TRUST_IMPOUND.Value = PAYMENT_TYPE_NONE)
            and (DM_CLIENT.PR.DD_IMPOUND.Value = PAYMENT_TYPE_NONE)
            and (DM_CLIENT.PR.BILLING_IMPOUND.Value = PAYMENT_TYPE_NONE)
            and (DM_CLIENT.PR.WC_IMPOUND.Value = PAYMENT_TYPE_NONE)) or PostACH then
            begin
              Tax941 := 0;
              if DM_CLIENT.CO.CO_EXCEPTION_PAYMENT_TYPE.AsString <> PAYMENT_TYPE_NONE then
              begin
                if not DM_CLIENT.CO_FED_TAX_LIABILITIES.Active then
                  DM_CLIENT.CO_FED_TAX_LIABILITIES.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');

                SaveFiltered := DM_CLIENT.CO_FED_TAX_LIABILITIES.Filtered;
                SaveFilter := DM_CLIENT.CO_FED_TAX_LIABILITIES.Filter;
                DM_CLIENT.CO_FED_TAX_LIABILITIES.Filter := 'STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + ''' AND PR_NBR='+IntToStr(PayrollNumber);
                DM_CLIENT.CO_FED_TAX_LIABILITIES.Filtered := True;
                try
                  DM_CLIENT.CO_FED_TAX_LIABILITIES.First;
                  while not DM_CLIENT.CO_FED_TAX_LIABILITIES.Eof do
                  begin
                    if
                   (    (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_FEDERAL)
                     or (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_OASDI)
                     or (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_ER_OASDI)
                     or (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_MEDICARE)
                     or (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_ER_MEDICARE)
                     or (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_EIC)
                     or (DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING)
                   )then
                      Tax941 := Tax941 + DM_CLIENT.CO_FED_TAX_LIABILITIES.FieldByName('AMOUNT').AsCurrency;
                    DM_CLIENT.CO_FED_TAX_LIABILITIES.Next;
                  end;
                finally
                  DM_CLIENT.CO_FED_TAX_LIABILITIES.Filtered := SaveFiltered;
                  DM_CLIENT.CO_FED_TAX_LIABILITIES.Filter := SaveFilter;
                end;
              end;

              if DM_CLIENT.PR.PAYROLL_TYPE.Value <> PAYROLL_TYPE_TAX_DEPOSIT then
              if PostACH then
              begin
                MyTransactionManager := TTransactionManager.Create(nil);
                try
                  MyTransactionManager.Initialize([DM_CLIENT.CO_PR_ACH]);

                  DM_CLIENT.CO_PR_ACH.Insert;
                  DM_CLIENT.CO_PR_ACH.PR_NBR.AsInteger := PayrollNumber;

                  DM_CLIENT.CO_PR_ACH.TAX_IMPOUND.Value := DM_CLIENT.PR.TAX_IMPOUND.Value;

                  if Tax941 >= 100000 then
                  begin
                    if DM_CLIENT.CO.CO_EXCEPTION_PAYMENT_TYPE.AsString <> PAYMENT_TYPE_NONE then
                      DM_CLIENT.CO_PR_ACH.TAX_IMPOUND.Value := DM_CLIENT.CO.CO_EXCEPTION_PAYMENT_TYPE.AsString;
                  end;

                  DM_CLIENT.CO_PR_ACH.TRUST_IMPOUND.Value := DM_CLIENT.PR.TRUST_IMPOUND.Value;
                  DM_CLIENT.CO_PR_ACH.DD_IMPOUND.Value := DM_CLIENT.PR.DD_IMPOUND.Value;
                  DM_CLIENT.CO_PR_ACH.BILLING_IMPOUND.Value := DM_CLIENT.PR.BILLING_IMPOUND.Value;
                  DM_CLIENT.CO_PR_ACH.WC_IMPOUND.Value := DM_CLIENT.PR.WC_IMPOUND.Value;

                  DM_CLIENT.CO_PR_ACH.STATUS.Value := COMMON_REPORT_STATUS__PENDING;
                  DM_CLIENT.CO_PR_ACH.ACH_DATE.Value := Int(SysTime);
                  DM_CLIENT.CO_PR_ACH.AMOUNT.Value := 0;
                  DM_CLIENT.CO_PR_ACH.STATUS_DATE.Value := Now;
                  DM_CLIENT.CO_PR_ACH.Post;

                  MyTransactionManager.ApplyUpdates;
                finally
                  MyTransactionManager.CancelUpdates;
                  MyTransactionManager.Free;
                end;
              end;

            end;

            ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS, DM_CLIENT.CL_BANK_ACCOUNT,
              DM_CLIENT.PR_CHECK, DM_CLIENT.PR_MISCELLANEOUS_CHECKS,
              DM_CLIENT.CO_TAX_DEPOSITS, DM_CLIENT.CO_BANK_ACCOUNT_REGISTER,
              DM_CLIENT.CO_FED_TAX_LIABILITIES, DM_CLIENT.CO_STATE_TAX_LIABILITIES,
              DM_CLIENT.CO_SUI_LIABILITIES, DM_CLIENT.CO_LOCAL_TAX_LIABILITIES]);
            AbortIfCurrentThreadTaskTerminated;

            if (DM_PAYROLL.PR.RecordCount <> 0) and
              (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString  = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT) then
              ctx_DataAccess.DoOnQecFinished(DM_CLIENT.CO.CO_NBR.AsInteger, DM_PAYROLL.PR.CHECK_DATE.AsDateTime);

            ctx_DataAccess.CommitNestedTransaction;

            DM_CLIENT.EE_SCHEDULED_E_DS.Close;
            AllOK := True;
          finally
            ctx_DataAccess.LockPR(AllOK);
            ctx_DataAccess.PayrollIsProcessing := False;
          end;
        except
//          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      finally
        if Assigned(PR_CHECK_2) then
          PR_CHECK_2.Free;
      end;
    except
      on E: Exception do
      begin
        if (DM_CLIENT.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSING)
        or (DM_CLIENT.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PREPROCESSING) then
        begin
          ctx_DataAccess.RollbackNestedTransaction;
          ctx_DataAccess.StartNestedTransaction([dbtClient]);
          try
            ctx_DataAccess.UnlockPR;
            try

              ctx_DataAccess.PayrollIsProcessing := True;
              DM_CLIENT.PR.Edit;
              if JustPreProcess then
                DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING
              else
              if (E is EPayrollLimitation) then
                DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_HOLD
              else
                DM_CLIENT.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
              DM_CLIENT.PR.FieldByName('STATUS_DATE').Value := Now;
              DM_CLIENT.PR.Post;
              ctx_DataAccess.PostDataSets([DM_CLIENT.PR]);
              ctx_DataAccess.PayrollIsProcessing := False;
            finally
              ctx_DataAccess.LockPR(True);
            end;
            ctx_DataAccess.CommitNestedTransaction;
          except
            on E: Exception do
            begin
              ctx_DataAccess.RollbackNestedTransaction;
              raise;
            end;
          end;
        end;
        raise;
      end;
    end;
  finally
    FreeAndNil(CalcDueDates);
    AddToPayrollLog('Ending Payroll Main Processing');
  end;
end;

end.

