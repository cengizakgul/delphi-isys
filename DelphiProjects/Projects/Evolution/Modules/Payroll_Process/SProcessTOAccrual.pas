// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SProcessTOAccrual;

interface

uses
  Variants, STimeOffUtils,  EvContext, EvExceptions,
  EvDataSet, EvCommonInterfaces, EvDataAccessComponents, EvClientDataSet;

type
  TArrayOfIntegers = array of Integer;
  TPayrollToaSetupAccess = class(THistoricToaSetupAccess)
  private
    FPeriodBegDate: TDateTime;
    FBatchEndDate: TDateTime;
    FBatchNbr: Integer;
    FPayrollNbr: Integer;
    FCoNbr : Integer;
    FPayrollCheckDate: TDateTime;
    FPayrollNote: string;
    FPayrollAdditionalInfo: set of (aiFirst, aiLast, aiMiddlest);
    FExcludeAccrueOption: Char;
    FPR_BATCH: TevClientDataSet;
    FCL_E_D_GROUP_CODES: TevClientDataSet;
    FBatchEE: TevClientDataSet;
    FBatchEeToa: TevClientDataSet;
    FUsedEds,
    FAccruedEds: TArrayOfIntegers;
    FWarnings: string;
    function InPeriodIfCombineMonthly(const PayrollOfMonth: Char): Boolean;
    function InPeriodIfCombineNotMonthly(const PayrollOfMonth: Char; const MonthNumber, iMonthsInPeriod: Integer): Boolean;
    function CheckToFreq(const d: TDateTime; const Offset: Integer;
      const Freq, PayrollOfMonth: Char; const MonthNumber: Integer): Boolean;
    function IsInArrayOfIntegers(const a: TArrayOfIntegers; const Ed: Integer): Boolean;
    function IsUsedEd(const Ed: Integer): Boolean;
    function IsAccruedEd(const Ed: Integer): Boolean;
    procedure AddWarning(const EeCustNumber, Warning: string);
  public
    property PeriodBegDate: TDateTime read FPeriodBegDate;
    property PayrollNbr: Integer read FPayrollNbr;
    property BatchNbr: Integer read FBatchNbr;
    property PayrollCheckDate: TDateTime read FPayrollCheckDate;
    property CoNbr: Integer read FCoNbr;
    property ExcludeAccrueOption: Char read FExcludeAccrueOption;
    property PR_BATCH: TevClientDataSet read FPR_BATCH;
    property BatchEE: TevClientDataSet read FBatchEE;
    property BatchEeToa: TevClientDataSet read FBatchEeToa;
    property Warnings: string read FWarnings;
    function TimeToReset: Boolean;
    function TimeToRollover: Boolean;
    function TimeToAccrue: Boolean;
    function Rate: Double;
    function Prorated(const Hours: Double; out Prorator: Double): Boolean;
    function MaxHoursToAccrueOn: Double;
    function MinHoursBeforeAccrual: Double;
    function IsDateInPeriod(const d: TDateTime): Boolean;
    procedure CalcCheckDateBalance(out Accrued, Used: Double);
    constructor Create(const PayrollNbr: Integer); overload;
    destructor Destroy; override;
    procedure SetBatchData; overload;
    procedure SetBatchData(const PrBatchNbr: Integer); overload;
    procedure IntLocateEeToaSetup(const EeToaNbr: Integer);
    function AddAccrualPrOper(const PrCheckNbr: Variant;
      const AccrualBase, Rate: Double; const AccrualBaseCap: Variant): Integer;
    function AddPrOper(const AdjToaOperNbr, ConToaOperNbr, PrCheckNbr: Variant;
      const OperCode: Char; const Note: string;
      const Accrued, Used: Double; const AccruedCapped: Variant; const OverideEeToaNbr: Integer =-1): Integer;
    procedure MainProc;
  end;

procedure CalculateTimeOffAccrual(var Warnings: String);

implementation

uses
  EvConsts, EvUtils, EvBasicUtils, SDataStructure, SysUtils, EvTypes, Db,
  Math, DateUtils, SFieldCodeValues;

const
  ocUse = 1;
  ocReset = 2;
  ocRollover = 3;
  oc1652 = 4;
  oc1653 = 5;
  oc1654 = 6;
  ocCEDP = 7;

{ TPayrollToaSetupAccess }

function TPayrollToaSetupAccess.AddPrOper(const AdjToaOperNbr,
  ConToaOperNbr, PrCheckNbr: Variant; const OperCode: Char;
  const Note: string; const Accrued, Used: Double;
  const AccruedCapped: Variant; const OverideEeToaNbr: Integer =-1): Integer;
begin
  if Note <> '' then
    Result := AddOper(AdjToaOperNbr, ConToaOperNbr, PayrollNbr, BatchNbr, PrCheckNbr, OperCode,
      FPayrollNote+ ' '+ Note, PayrollCheckDate, Accrued, Used, AccruedCapped, OverideEeToaNbr)
  else
    Result := AddOper(AdjToaOperNbr, ConToaOperNbr, PayrollNbr, BatchNbr, PrCheckNbr, OperCode,
      FPayrollNote, PayrollCheckDate, Accrued, Used, AccruedCapped, OverideEeToaNbr);
end;

procedure TPayrollToaSetupAccess.CalcCheckDateBalance(out Accrued,
  Used: Double);
begin
  CalcAnyDayEndBalance(PayrollCheckDate, Accrued, Used);
end;

function TPayrollToaSetupAccess.InPeriodIfCombineMonthly(const PayrollOfMonth: Char): Boolean;
begin
  Result := ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_FIRST) and (aiFirst in
    FPayrollAdditionalInfo))
    or ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_LAST) and (aiLast in
    FPayrollAdditionalInfo))
    or ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_MIDDLEST) and (aiMiddlest in
    FPayrollAdditionalInfo));
end;

function TPayrollToaSetupAccess.InPeriodIfCombineNotMonthly(const PayrollOfMonth: Char; const MonthNumber, iMonthsInPeriod: Integer): Boolean;
var
  QtrMonth: Integer;
begin
  QtrMonth := ((GetMonth(PayrollCheckDate)- 1) mod iMonthsInPeriod) + 1;
  if (QtrMonth = MonthNumber) or (MonthNumber > iMonthsInPeriod) then
    Result := InPeriodIfCombineMonthly(PayrollOfMonth)
  else
    Result := False;
end;

function TPayrollToaSetupAccess.CheckToFreq(const d: TDateTime; const Offset: Integer; const Freq,
  PayrollOfMonth: Char; const MonthNumber: Integer): Boolean;
  function HireDateInPeriod(const iMonthsInPeriod: Integer): Boolean;
    function Process(const PeriodDate: TDateTime): Boolean;
    var
      y, m, d: Word;
      ps, p: Integer;
      h: TDateTime;
    begin
      DecodeDate(HireDate, y, m, d);
      ps := (m-1) mod iMonthsInPeriod;
      p := ((GetMonth(PeriodDate)-1) div iMonthsInPeriod)* iMonthsInPeriod;
      h := DaySafeEncode(YearOf(PeriodDate), p+ ps+ 1, d);
      Result := IsDateInPeriod(h);
    end;
  begin
    Result := Process(PeriodBegDate) or Process(PeriodEndDate);
  end;
begin
  case Freq of
  TIME_OFF_ACCRUAL_FREQ_NONE:
    Result := False;
  TIME_OFF_ACCRUAL_FREQ_ONCE:
    Result := IsDateInPeriod(d);
  TIME_OFF_ACCRUAL_FREQ_ONCE_AFTER_DAYS:
    Result := IsDateInPeriod(HireDate+ Offset);
  TIME_OFF_ACCRUAL_FREQ_ONCE_AFTER_MONTHS:
    Result := IsDateInPeriod(AddMonths(HireDate, OffSet));
  TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD:
    Result := True;
  TIME_OFF_ACCRUAL_FREQ_NONTHLY:
    Result := InPeriodIfCombineMonthly(PayrollOfMonth)
      or ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE) and HireDateInPeriod(1));
  TIME_OFF_ACCRUAL_FREQ_QUARTERLY:
    Result := InPeriodIfCombineNotMonthly(PayrollOfMonth, MonthNumber, 3)
      or ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE) and HireDateInPeriod(3));
  TIME_OFF_ACCRUAL_FREQ_SEMI_ANNUAL:
    Result := InPeriodIfCombineNotMonthly(PayrollOfMonth, MonthNumber, 6)
      or ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE) and HireDateInPeriod(6));
  TIME_OFF_ACCRUAL_FREQ_ANNUAL:
    Result := InPeriodIfCombineNotMonthly(PayrollOfMonth, MonthNumber, 12)
      or ((PayrollOfMonth = TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE) and HireDateInPeriod(12));
  else
    Assert(False);
    Result := False; //to avoid compile warning
  end;
end;

constructor TPayrollToaSetupAccess.Create(const PayrollNbr: Integer);
begin
  SetLength(FUsedEds, 0);
  SetLength(FAccruedEds, 0);
  FPayrollNbr := PayrollNbr;
  FBatchNbr := -1;
  Assert(DM_PAYROLL.PR.Locate('PR_NBR', PayrollNbr, []));
  FPayrollCheckDate := DM_PAYROLL.PR['CHECK_DATE'];
  FCoNbr := DM_PAYROLL.PR['CO_NBR'];
  FPayrollNote := 'Pr:'+ FormatDateTime('MM/DD/YYYY', DM_PAYROLL.PR['CHECK_DATE'])+ '-'+
    IntToStr(DM_PAYROLL.PR['RUN_NUMBER']);
  FExcludeAccrueOption := string(ConvertNull(DM_PAYROLL.PR['EXCLUDE_TIME_OFF'],
    TIME_OFF_EXCLUDE_ACCRUAL_NONE))[1];
  FPR_BATCH := TevClientDataSet.CreateWithChecksDisabled(nil);
  FBatchEE := TevClientDataSet.CreateWithChecksDisabled(nil);
  FBatchEeToa := TevClientDataSet.CreateWithChecksDisabled(nil);
  FPR_BATCH.IndexFieldNames := 'PR_BATCH_NBR';
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('TABLENAME', 'PR_BATCH');
    SetMacro('COLUMNS', '*');
    SetMacro('CONDITION', 'PR_NBR = :PrNbr');
    SetParam('PrNbr', PayrollNbr);
    ctx_DataAccess.GetCustomData(FPR_BATCH, AsVariant);
  end;
  FCL_E_D_GROUP_CODES := TevClientDataSet.CreateWithChecksDisabled(nil);
  FCL_E_D_GROUP_CODES.IndexFieldNames := 'CL_E_D_GROUPS_NBR';
  with TExecDSWrapper.Create('GenericSelectCurrent') do
  begin
    SetMacro('TABLENAME', 'CL_E_D_GROUP_CODES');
    SetMacro('COLUMNS', 'CL_E_D_GROUPS_NBR, CL_E_DS_NBR');
    ctx_DataAccess.GetCustomData(FCL_E_D_GROUP_CODES, AsVariant);
  end;
  inherited Create(DM_PAYROLL.PR['CO_NBR'], PayrollCheckDate);
  FBatchEE.CloneCursor(CurrentEE, True);
  FBatchEE.IndexFieldNames := 'CO_NBR;PAY_FREQUENCY';
  FBatchEeToa.CloneCursor(CurrentEE_TIME_OFF_ACCRUAL, True);
  FBatchEeToa.IndexFieldNames := 'EE_NBR;EE_TIME_OFF_ACCRUAL_NBR';
end;

destructor TPayrollToaSetupAccess.Destroy;
begin
  FreeAndNil(FCL_E_D_GROUP_CODES);
  FreeAndNil(FPR_BATCH);
  FreeAndNil(FBatchEE);
  FreeAndNil(FBatchEeToa);
  inherited;
end;

function TPayrollToaSetupAccess.IsDateInPeriod(
  const d: TDateTime): Boolean;
begin
  Result := GenericIsDateInPeriod(d, PeriodBegDate, PeriodEndDate);
end;

procedure TPayrollToaSetupAccess.IntLocateEeToaSetup(const EeToaNbr: Integer);
var
  i: Integer;
  procedure AddItem(var a: TArrayOfIntegers; const Value: Integer);
  begin
    a[i] := Value;
    Inc(i);
  end;
begin
  LocateEeToaSetup(EeToaNbr, FBatchEndDate);
  FCL_E_D_GROUP_CODES.SetRange([CO_TIME_OFF_ACCRUAL['USED_CL_E_D_GROUPS_NBR']], [CO_TIME_OFF_ACCRUAL['USED_CL_E_D_GROUPS_NBR']]);
  SetLength(FUsedEds, FCL_E_D_GROUP_CODES.RecordCount+ 1);
  i := 0;
  AddItem(FUsedEds, CO_TIME_OFF_ACCRUAL['CL_E_DS_NBR']);
  FCL_E_D_GROUP_CODES.First;
  while not FCL_E_D_GROUP_CODES.Eof do
  begin
    AddItem(FUsedEds, FCL_E_D_GROUP_CODES['CL_E_DS_NBR']);
    FCL_E_D_GROUP_CODES.Next;
  end;
  FCL_E_D_GROUP_CODES.SetRange([CO_TIME_OFF_ACCRUAL['CL_E_D_GROUPS_NBR']], [CO_TIME_OFF_ACCRUAL['CL_E_D_GROUPS_NBR']]);
  SetLength(FAccruedEds, FCL_E_D_GROUP_CODES.RecordCount);
  i := 0;
  FCL_E_D_GROUP_CODES.First;
  while not FCL_E_D_GROUP_CODES.Eof do
  begin
    AddItem(FAccruedEds, FCL_E_D_GROUP_CODES['CL_E_DS_NBR']);
    FCL_E_D_GROUP_CODES.Next;
  end;
end;

procedure TPayrollToaSetupAccess.MainProc;
var
  HoursUsed, HoursAccrued, Checks,
  EeLogOpers, EeDeactivatedToa: TevClientDataSet;
  QDoubleTOA : IevQuery;
  HoursWorked_1654, HoursWorked2_1654, HoursWorked_Cedp, HoursWorked2_Cedp: TevClientDataSet;
  procedure DoUsed;
  var
    dHours: Double;
    iCheckNbr: Integer;
    procedure ProcessHours;
    begin
      if (iCheckNbr <> -1) then
        AddPrOper(Null, Null, iCheckNbr, EE_TOA_OPER_CODE_USED, '', 0, dHours, Null);
      dHours := 0;
    end;
  begin
    iCheckNbr := -1;
    HoursUsed.SetRange([EE_TIME_OFF_ACCRUAL['EE_NBR'], PR_BATCH['PR_BATCH_NBR']],
      [EE_TIME_OFF_ACCRUAL['EE_NBR'], PR_BATCH['PR_BATCH_NBR']]);
    HoursUsed.First;
    while not HoursUsed.Eof do
    begin
      if iCheckNbr <> HoursUsed['PR_CHECK_NBR'] then
      begin
        ProcessHours;
        iCheckNbr := HoursUsed['PR_CHECK_NBR'];
      end;
      if IsUsedEd(HoursUsed['CL_E_DS_NBR']) then
      begin
        dHours := dHours+ HoursUsed['hours'];
        if not VarIsNull(CO_TIME_OFF_ACCRUAL['CO_HR_ATTENDANCE_TYPES_NBR']) then
        begin
          EE_HR_ATTENDANCE.Append;
          EE_HR_ATTENDANCE['EE_NBR'] := EE_TIME_OFF_ACCRUAL['EE_NBR'];
          EE_HR_ATTENDANCE['CO_HR_ATTENDANCE_TYPES_NBR'] := CO_TIME_OFF_ACCRUAL['CO_HR_ATTENDANCE_TYPES_NBR'];
          EE_HR_ATTENDANCE['ATTENDANCE_TYPE_NUMBER'] := 1;
          EE_HR_ATTENDANCE['PERIOD_FROM'] := HoursUsed['LINE_ITEM_DATE'];
          EE_HR_ATTENDANCE['PERIOD_TO'] := HoursUsed['LINE_ITEM_END_DATE'];
          EE_HR_ATTENDANCE['HOURS_USED'] := HoursUsed['hours'];
          EE_HR_ATTENDANCE['EXCUSED'] := GROUP_BOX_YES;
          EE_HR_ATTENDANCE['WARNING_ISSUED'] := GROUP_BOX_NO;
          EE_HR_ATTENDANCE['PR_NBR'] := PR_BATCH['PR_NBR'];
          EE_HR_ATTENDANCE.Post;
        end;
      end;
      HoursUsed.Next;
    end;
    ProcessHours;
  end;
  procedure Do1652;
  var
    dHoursAccrued: Double;
    iCheckNbr: Integer;
    procedure ProcessHoursAccrued;
    begin
      if (iCheckNbr <> -1)
      and (dHoursAccrued >= MinHoursBeforeAccrual) then
        AddAccrualPrOper(iCheckNbr, dHoursAccrued, Rate, MaxHoursToAccrueOn);
      dHoursAccrued := 0;
    end;
  begin
    if CO_TIME_OFF_ACCRUAL['ACCRUE_ON_STANDARD_HOURS'] = GROUP_BOX_YES then
    begin
      dHoursAccrued := ConvertNull(EE['STANDARD_HOURS'], 0);
      if (dHoursAccrued >= MinHoursBeforeAccrual) then
      begin
        Checks.SetRange([EE['EE_NBR'], PR_BATCH['PR_BATCH_NBR']], [EE['EE_NBR'], PR_BATCH['PR_BATCH_NBR']]);
        try
          Checks.First;
          while not Checks.Eof do
          begin
            AddAccrualPrOper(Checks['PR_CHECK_NBR'], dHoursAccrued, Rate, MaxHoursToAccrueOn);
            Checks.Next;
          end;
        finally
          Checks.CancelRange;
        end;
      end;
    end
    else
    begin
      iCheckNbr := -1;
      HoursAccrued.SetRange([EE_TIME_OFF_ACCRUAL['EE_NBR'], PR_BATCH['PR_BATCH_NBR']],
        [EE_TIME_OFF_ACCRUAL['EE_NBR'], PR_BATCH['PR_BATCH_NBR']]);
      HoursAccrued.First;
      while not HoursAccrued.Eof do
      begin
        if iCheckNbr <> HoursAccrued['PR_CHECK_NBR'] then
        begin
          ProcessHoursAccrued;
          iCheckNbr := HoursAccrued['PR_CHECK_NBR'];
        end;
        if IsAccruedEd(HoursAccrued['CL_E_DS_NBR']) then
          dHoursAccrued := dHoursAccrued+ HoursAccrued['hours'];
        HoursAccrued.Next;
      end;
      ProcessHoursAccrued;
    end;
  end;
  procedure Do1653(const CheckNbr: Variant);
  var
    dHoursWorked: Double;
    dProrator: Double;
    bAccrue: Boolean;
  begin
    if DM_PAYROLL.PR['SCHEDULED'] <> GROUP_BOX_YES then
      raise EAccrualException.CreateFmtHelp('Time off accrual "%s". The calculation method requires a scheduled payroll',
        [CO_TIME_OFF_ACCRUAL['DESCRIPTION']], IDH_CanNotPerformOperation);
    HoursAccrued.SetRange([EE_TIME_OFF_ACCRUAL['EE_NBR']], [EE_TIME_OFF_ACCRUAL['EE_NBR']]);
    dHoursWorked := 0;
    HoursAccrued.First;
    while not HoursAccrued.Eof do
    begin
      if IsAccruedEd(HoursAccrued['CL_E_DS_NBR']) then
        dHoursWorked := dHoursWorked+ HoursAccrued['hours'];
      HoursAccrued.Next;
    end;
    if CO_TIME_OFF_ACCRUAL['FREQUENCY'] = TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD then
      bAccrue := dHoursWorked >= MinHoursBeforeAccrual
    else
      bAccrue := TimeToAccrue;
    if bAccrue then
    begin
      if not Prorated(dHoursWorked, dProrator) then
        AddPrOper(Null, Null, CheckNbr, EE_TOA_OPER_CODE_ACCRUAL, 'Px'+ FloatToStr(Rate), Rate, 0, Null)
      else
        AddPrOper(Null, Null, CheckNbr, EE_TOA_OPER_CODE_ACCRUAL, 'Px'+ FloatToStr(Rate)+ ' '+
          FloatToStr(dProrator*100)+ '%', Rate* dProrator, 0, Null);
    end;
  end;
  procedure Do1654(const CheckNbr, CoToNbr: Variant);
    function GetValue(const cd: TevClientDataSet; const FieldName: string): Variant;
    begin
      if cd.FindKey([EE['EE_NBR'], CoToNbr]) then
        Result := cd[FieldName]
      else
        Result := 0;
    end;
  var
    dAverageHours, dHoursWorked: Double;
    iPeriodsWorked: Integer;
  begin
    if IsDateInPeriod(SameDayAnotherYear(HireDate, PeriodBegDate))
    or IsDateInPeriod(SameDayAnotherYear(HireDate, PeriodEndDate)) then
    begin // we are in a right period, going into accrue procedure
      dHoursWorked := GetValue(HoursWorked_1654, 'HoursWorked');
      if dHoursWorked >= MinHoursBeforeAccrual then
      begin
        iPeriodsWorked := GetValue(HoursWorked2_1654, 'PeriodsWorked');
        if iPeriodsWorked = 0 then
          dAverageHours := 0
        else
        if EE['PAY_FREQUENCY'] = FREQUENCY_TYPE_QUARTERLY then
          dAverageHours := dHoursWorked / (iPeriodsWorked * 52 / 4)
        else
        if EE['PAY_FREQUENCY'] = FREQUENCY_TYPE_MONTHLY then
          dAverageHours := dHoursWorked / (iPeriodsWorked * 52 / 12)
        else
          if EE['PAY_FREQUENCY'] = FREQUENCY_TYPE_SEMI_MONTHLY then
          dAverageHours := dHoursWorked / (iPeriodsWorked * 52 / 24)
        else
          if EE['PAY_FREQUENCY'] = FREQUENCY_TYPE_BIWEEKLY then
          dAverageHours := dHoursWorked / (iPeriodsWorked * 52 / 26)
        else
          if EE['PAY_FREQUENCY'] = FREQUENCY_TYPE_DAILY then
          dAverageHours := dHoursWorked / (iPeriodsWorked * 52 / 365)
        else
          dAverageHours := dHoursWorked / iPeriodsWorked;
        AddAccrualPrOper(CheckNbr, dAverageHours, Rate, Null);
      end;
    end;
  end;
  procedure DoReset;
  begin
    AddPrOper(Null, Null, Null, EE_TOA_OPER_CODE_RESET, '', 0, 0, Null);
  end;
  procedure DoRollover(const iRollOverToNbr: Integer);
  var
    iRolloverEeToaOperNbr: Integer;
  begin
    iRolloverEeToaOperNbr := AddPrOper(Null, Null, Null, EE_TOA_OPER_CODE_ROLLOVER_IN, '', 0, 0, Null, iRollOverToNbr);
    AddPrOper(Null, iRolloverEeToaOperNbr, Null, EE_TOA_OPER_CODE_ROLLOVER, '', 0, 0, Null);
    if string(CO_TIME_OFF_ACCRUAL['ROLLOVER_FREQ'])[1] in [TIME_OFF_ACCRUAL_FREQ_NONE, TIME_OFF_ACCRUAL_FREQ_ONCE,
      TIME_OFF_ACCRUAL_FREQ_ONCE_AFTER_DAYS, TIME_OFF_ACCRUAL_FREQ_ONCE_AFTER_MONTHS] then
      if CurrentEE_TIME_OFF_ACCRUAL['STATUS'] <> GROUP_BOX_NO then
      begin
        CurrentEE_TIME_OFF_ACCRUAL.Edit;
        CurrentEE_TIME_OFF_ACCRUAL['JUST_RESET'] := GROUP_BOX_NO;
        CurrentEE_TIME_OFF_ACCRUAL['STATUS'] := GROUP_BOX_NO;
        CurrentEE_TIME_OFF_ACCRUAL.Post;
        EeDeactivatedToa.AppendRecord([EeToaNbr]);
      end;
  end;
  procedure DoCEDP(const CheckNbr: Variant; const LastAccrualBeginDate, LastAccrualBeginDate2: TDateTime);
    function BenefitProrator(const dHours: Double): Double;
    begin
      case Trunc(dHours) of
      0..19:
        Result := 0;
      20..39:
        Result := 0.25;
      40..79:
        Result := 0.5;
      80..119:
        Result := 0.75;
      else
        Result := 1;
      end;
    end;
    function AddAccrualCEDPOper(const PrCheckNbr: Variant; const AccrualBase, Rate: Double): Integer;
    begin
      Result := AddPrOper(Null, Null, PrCheckNbr, EE_TOA_OPER_CODE_ACCRUAL,
        FloatToStr(Rate)+ ' '+
        IntToStr(Trunc(BenefitProrator(AccrualBase)*100))+'%',
        Rate* BenefitProrator(AccrualBase), 0, Null)
    end;
  var
    dHoursWorked: Double;
  begin
    dHoursWorked := 0;
    if HireDate <= LastAccrualBeginDate then
      if HoursWorked_Cedp.FindKey([EE_TIME_OFF_ACCRUAL['EE_NBR']]) then
        dHoursWorked := ConvertNull(HoursWorked_Cedp['HoursWorked'], 0);
    if HireDate <= LastAccrualBeginDate2 then
      if HoursWorked2_Cedp.FindKey([EE_TIME_OFF_ACCRUAL['EE_NBR']]) then
        dHoursWorked := dHoursWorked+ ConvertNull(HoursWorked2_Cedp['HoursWorked'], 0);
    if (dHoursWorked <> 0)
    and (dHoursWorked >= MinHoursBeforeAccrual) then
      AddAccrualCEDPOper(CheckNbr, dHoursWorked, Rate);
  end;
  function CheckLogForOperation(const Oper: Integer): Boolean;
  begin
    Result := EeLogOpers.FindKey([Oper, EeToaNbr]);
  end;
  procedure AddToOperLog(const Oper: Integer; const EeToNbr2, PrCheckNbr: Variant);
  begin
    EeLogOpers.Append;
    EeLogOpers['NBR'] := EeLogOpers.RecordCount;
    EeLogOpers['EE_TO_NBR'] := EeToaNbr;
    EeLogOpers['CO_TO_NBR'] := CO_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'];
    EeLogOpers['EE_TO_NBR2'] := EeToNbr2;
    Assert(not VarIsNull(CO_TIME_OFF_ACCRUAL['PROCESS_ORDER']) and (Length(string(CO_TIME_OFF_ACCRUAL['PROCESS_ORDER'])) = 1));
    case string(CO_TIME_OFF_ACCRUAL['PROCESS_ORDER'])[1] of
    TIME_OFF_PROCESS_ORDER_ACCRUE_USE_RESET_ROLLOVER:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 2;
      ocReset: EeLogOpers['PRIORITY'] := 3;
      ocRollover: EeLogOpers['PRIORITY'] := 4;
      else EeLogOpers['PRIORITY'] := 1;
      end;
    TIME_OFF_PROCESS_ORDER_ACCRUE_USE_ROLLOVER_RESET:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 2;
      ocReset: EeLogOpers['PRIORITY'] := 4;
      ocRollover: EeLogOpers['PRIORITY'] := 3;
      else EeLogOpers['PRIORITY'] := 1;
      end;
    TIME_OFF_PROCESS_ORDER_ACCRUE_RESET_USE_ROLLOVER:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 3;
      ocReset: EeLogOpers['PRIORITY'] := 2;
      ocRollover: EeLogOpers['PRIORITY'] := 4;
      else EeLogOpers['PRIORITY'] := 1;
      end;
    TIME_OFF_PROCESS_ORDER_ACCRUE_RESET_ROLLOVER_USE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 4;
      ocReset: EeLogOpers['PRIORITY'] := 2;
      ocRollover: EeLogOpers['PRIORITY'] := 3;
      else EeLogOpers['PRIORITY'] := 1;
      end;
    TIME_OFF_PROCESS_ORDER_ACCRUE_ROLLOVER_USE_RESET:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 3;
      ocReset: EeLogOpers['PRIORITY'] := 4;
      ocRollover: EeLogOpers['PRIORITY'] := 2;
      else EeLogOpers['PRIORITY'] := 1;
      end;
    TIME_OFF_PROCESS_ORDER_ACCRUE_ROLLOVER_RESET_USE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 4;
      ocReset: EeLogOpers['PRIORITY'] := 3;
      ocRollover: EeLogOpers['PRIORITY'] := 2;
      else EeLogOpers['PRIORITY'] := 1;
      end;
    TIME_OFF_PROCESS_ORDER_USE_RESET_ROLLOVER_ACCRUE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 1;
      ocReset: EeLogOpers['PRIORITY'] := 2;
      ocRollover: EeLogOpers['PRIORITY'] := 3;
      else EeLogOpers['PRIORITY'] := 4;
      end;
    TIME_OFF_PROCESS_ORDER_USE_RESET_ACCRUE_ROLLOVER:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 1;
      ocReset: EeLogOpers['PRIORITY'] := 2;
      ocRollover: EeLogOpers['PRIORITY'] := 4;
      else EeLogOpers['PRIORITY'] := 3;
      end;
    TIME_OFF_PROCESS_ORDER_USE_ROLLOVER_RESET_ACCRUE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 1;
      ocReset: EeLogOpers['PRIORITY'] := 3;
      ocRollover: EeLogOpers['PRIORITY'] := 2;
      else EeLogOpers['PRIORITY'] := 4;
      end;
    TIME_OFF_PROCESS_ORDER_USE_ROLLOVER_ACCRUE_RESET:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 1;
      ocReset: EeLogOpers['PRIORITY'] := 4;
      ocRollover: EeLogOpers['PRIORITY'] := 2;
      else EeLogOpers['PRIORITY'] := 3;
      end;
    TIME_OFF_PROCESS_ORDER_USE_ACCRUE_ROLLOVER_RESET:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 1;
      ocReset: EeLogOpers['PRIORITY'] := 4;
      ocRollover: EeLogOpers['PRIORITY'] := 3;
      else EeLogOpers['PRIORITY'] := 2;
      end;
    TIME_OFF_PROCESS_ORDER_USE_ACCRUE_RESET_ROLLOVER:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 1;
      ocReset: EeLogOpers['PRIORITY'] := 3;
      ocRollover: EeLogOpers['PRIORITY'] := 4;
      else EeLogOpers['PRIORITY'] := 2;
      end;
    TIME_OFF_PROCESS_ORDER_RESET_USE_ROLLOVER_ACCRUE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 2;
      ocReset: EeLogOpers['PRIORITY'] := 1;
      ocRollover: EeLogOpers['PRIORITY'] := 3;
      else EeLogOpers['PRIORITY'] := 4;
      end;
    TIME_OFF_PROCESS_ORDER_RESET_USE_ACCRUE_ROLLOVER:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 2;
      ocReset: EeLogOpers['PRIORITY'] := 1;
      ocRollover: EeLogOpers['PRIORITY'] := 4;
      else EeLogOpers['PRIORITY'] := 3;
      end;
    TIME_OFF_PROCESS_ORDER_RESET_ROLLOVER_USE_ACCRUE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 3;
      ocReset: EeLogOpers['PRIORITY'] := 1;
      ocRollover: EeLogOpers['PRIORITY'] := 2;
      else EeLogOpers['PRIORITY'] := 4;
      end;
    TIME_OFF_PROCESS_ORDER_RESET_ROLLOVER_ACCRUE_USE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 4;
      ocReset: EeLogOpers['PRIORITY'] := 1;
      ocRollover: EeLogOpers['PRIORITY'] := 2;
      else EeLogOpers['PRIORITY'] := 3;
      end;
    TIME_OFF_PROCESS_ORDER_RESET_ACCRUE_USE_ROLLOVER:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 3;
      ocReset: EeLogOpers['PRIORITY'] := 1;
      ocRollover: EeLogOpers['PRIORITY'] := 4;
      else EeLogOpers['PRIORITY'] := 2;
      end;
    TIME_OFF_PROCESS_ORDER_RESET_ACCRUE_ROLLOVER_USE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 4;
      ocReset: EeLogOpers['PRIORITY'] := 1;
      ocRollover: EeLogOpers['PRIORITY'] := 3;
      else EeLogOpers['PRIORITY'] := 2;
      end;
    TIME_OFF_PROCESS_ORDER_ROLLOVER_USE_RESET_ACCRUE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 2;
      ocReset: EeLogOpers['PRIORITY'] := 3;
      ocRollover: EeLogOpers['PRIORITY'] := 1;
      else EeLogOpers['PRIORITY'] := 4;
      end;
    TIME_OFF_PROCESS_ORDER_ROLLOVER_USE_ACCRUE_RESET:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 2;
      ocReset: EeLogOpers['PRIORITY'] := 4;
      ocRollover: EeLogOpers['PRIORITY'] := 1;
      else EeLogOpers['PRIORITY'] := 3;
      end;
    TIME_OFF_PROCESS_ORDER_ROLLOVER_RESET_USE_ACCRUE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 3;
      ocReset: EeLogOpers['PRIORITY'] := 2;
      ocRollover: EeLogOpers['PRIORITY'] := 1;
      else EeLogOpers['PRIORITY'] := 4;
      end;
    TIME_OFF_PROCESS_ORDER_ROLLOVER_RESET_ACCRUE_USE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 4;
      ocReset: EeLogOpers['PRIORITY'] := 2;
      ocRollover: EeLogOpers['PRIORITY'] := 1;
      else EeLogOpers['PRIORITY'] := 3;
      end;
    TIME_OFF_PROCESS_ORDER_ROLLOVER_ACCRUE_USE_RESET:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 3;
      ocReset: EeLogOpers['PRIORITY'] := 4;
      ocRollover: EeLogOpers['PRIORITY'] := 1;
      else EeLogOpers['PRIORITY'] := 2;
      end;
    TIME_OFF_PROCESS_ORDER_ROLLOVER_ACCRUE_RESET_USE:
      case Oper of
      ocUse: EeLogOpers['PRIORITY'] := 4;
      ocReset: EeLogOpers['PRIORITY'] := 3;
      ocRollover: EeLogOpers['PRIORITY'] := 1;
      else EeLogOpers['PRIORITY'] := 2;
      end;
    else
      Assert(False)
    end;
    EeLogOpers['OPERATION'] := Oper;
    EeLogOpers['PR_BATCH_NBR'] := PR_BATCH['PR_BATCH_NBR'];
    EeLogOpers['PR_CHECK_NBR'] := PrCheckNbr;
    EeLogOpers['CUSTOM_EMPLOYEE_NUMBER'] := CurrentEE['CUSTOM_EMPLOYEE_NUMBER'];
    EeLogOpers.Post;
  end;
  procedure ProcessPostponed;
  var
    dLastAccrualEndDate: TDateTime;
    dLastAccrualBeginDate: TDateTime;
    dLastAccrualEndDate2: TDateTime;
    dLastAccrualBeginDate2: TDateTime;
  begin
    dLastAccrualBeginDate := 0;
    dLastAccrualBeginDate2 := 0;
    
    EeLogOpers.IndexFieldNames := 'CO_TO_NBR;PRIORITY;PR_BATCH_NBR;EE_TO_NBR;NBR;PR_CHECK_NBR';
    EeLogOpers.First;
    while not EeLogOpers.Eof do
    begin
      if not EeDeactivatedToa.FindKey([EeLogOpers['EE_TO_NBR']]) then
      try
        SetBatchData(EeLogOpers['PR_BATCH_NBR']);
        IntLocateEeToaSetup(EeLogOpers['EE_TO_NBR']);
        case EeLogOpers['OPERATION'] of
        ocUse:
          DoUsed;
        ocReset:
          DoReset;
        ocRollover:
          DoRollover(EeLogOpers['EE_TO_NBR2']);
        oc1652:
          Do1652;
        oc1653:
          Do1653(EeLogOpers['PR_CHECK_NBR']);
        oc1654:
          begin
            if not HoursWorked_1654.Active then
            begin
              with TExecDSWrapper.Create('AmountsWorked') do
              begin
                SetParam('StartDate', GetYearAgoDate(PayrollCheckDate));
                SetParam('EndDate', PayrollCheckDate - 1);
                SetParam('CoNbr', CoNbr);
                ctx_DataAccess.GetCustomData(HoursWorked_1654, AsVariant);
              end;
              with TExecDSWrapper.Create('SchedulePayrollCount') do
              begin
                SetParam('StartDate', GetYearAgoDate(PayrollCheckDate));
                SetParam('EndDate', PayrollCheckDate - 1);
                SetParam('CoNbr', CoNbr);
                ctx_DataAccess.GetCustomData(HoursWorked2_1654, AsVariant);
              end;
            end;
            Do1654(EeLogOpers['PR_CHECK_NBR'], EeLogOpers['CO_TO_NBR']);
          end;
        ocCEDP:
          begin
            if not HoursWorked_Cedp.Active or (HoursWorked_Cedp.Tag <> EeLogOpers['CO_TO_NBR']) then
            begin
              HoursWorked_Cedp.Tag := EeLogOpers['CO_TO_NBR'];
              dLastAccrualBeginDate := GetBeginMonth(GetBeginMonth(PayrollCheckDate)-1);
              dLastAccrualEndDate := dLastAccrualBeginDate+ 14; // the 15'th
              with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
              begin
                SetMacro('COLUMNS', 't2.ee_nbr, sum(t1.HOURS_OR_PIECES) HoursWorked');
                SetMacro('TABLE1', 'PR_CHECK_LINES');
                SetMacro('TABLE2', 'PR_CHECK');
                SetMacro('TABLE3', 'PR_BATCH');
                SetMacro('JOINFIELD2', 'PR_BATCH');
                SetMacro('JOINFIELD', 'PR_CHECK');
                if VarIsNull(CO_TIME_OFF_ACCRUAL['cl_e_d_groups_nbr']) then
                  SetMacro('CONDITION', 't3.PERIOD_END_DATE between :chd1 and :chd2 and (t2.CHECK_STATUS = :CS1 or t2.CHECK_STATUS = :CS2) and t2.CHECK_TYPE = :CT GROUP BY t2.ee_nbr')
                else
                begin
                  SetMacro('CONDITION', 't3.PERIOD_END_DATE between :chd1 and :chd2 and (t2.CHECK_STATUS = :CS1 or t2.CHECK_STATUS = :CS2) and t2.CHECK_TYPE = :CT and exists (select g.CL_E_D_GROUP_CODES_nbr from CL_E_D_GROUP_CODES g '+
                    'where {AsOfNow<g>} and g.cl_e_ds_nbr = t1.cl_e_ds_nbr and '+
                    'g.cl_e_d_groups_nbr = :Nbr2 ) GROUP BY t2.ee_nbr');
                  SetParam('Nbr2', CO_TIME_OFF_ACCRUAL.FieldByName('cl_e_d_groups_nbr').AsInteger);
                end;
                SetParam('chd1', dLastAccrualBeginDate);
                SetParam('chd2', dLastAccrualEndDate);
                SetParam('CT', CHECK_TYPE2_REGULAR);
                SetParam('CS1', CHECK_STATUS_OUTSTANDING);
                SetParam('CS2', CHECK_STATUS_CLEARED);
                ctx_DataAccess.GetCustomData(HoursWorked_Cedp, AsVariant);
              end;
              dLastAccrualBeginDate2 := dLastAccrualEndDate+ 1;
              dLastAccrualEndDate2 := GetEndMonth(dLastAccrualBeginDate2);
              with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
              begin
                SetMacro('COLUMNS', 't2.ee_nbr, sum(t1.HOURS_OR_PIECES) HoursWorked');
                SetMacro('TABLE1', 'PR_CHECK_LINES');
                SetMacro('TABLE2', 'PR_CHECK');
                SetMacro('TABLE3', 'PR_BATCH');
                SetMacro('JOINFIELD2', 'PR_BATCH');
                SetMacro('JOINFIELD', 'PR_CHECK');
                if VarIsNull(CO_TIME_OFF_ACCRUAL['cl_e_d_groups_nbr']) then
                  SetMacro('CONDITION', 't3.PERIOD_END_DATE between :chd1 and :chd2 and (t2.CHECK_STATUS = :CS1 or t2.CHECK_STATUS = :CS2) and t2.CHECK_TYPE = :CT GROUP BY t2.ee_nbr')
                else
                begin
                  SetMacro('CONDITION', 't3.PERIOD_END_DATE between :chd1 and :chd2 and (t2.CHECK_STATUS = :CS1 or t2.CHECK_STATUS = :CS2) and t2.CHECK_TYPE = :CT and exists (select g.CL_E_D_GROUP_CODES_nbr from CL_E_D_GROUP_CODES g '+
                    'where {AsOfNow<g>} and g.cl_e_ds_nbr = t1.cl_e_ds_nbr and '+
                    'g.cl_e_d_groups_nbr = :Nbr2 ) GROUP BY t2.ee_nbr');
                  SetParam('Nbr2', CO_TIME_OFF_ACCRUAL.FieldByName('cl_e_d_groups_nbr').AsInteger);
                end;
                SetParam('chd1', dLastAccrualBeginDate2);
                SetParam('chd2', dLastAccrualEndDate2);
                SetParam('CT', CHECK_TYPE2_REGULAR);
                SetParam('CS1', CHECK_STATUS_OUTSTANDING);
                SetParam('CS2', CHECK_STATUS_CLEARED);
                ctx_DataAccess.GetCustomData(HoursWorked2_Cedp, AsVariant);
              end;
            end;
            DoCEDP(EeLogOpers['PR_CHECK_NBR'], dLastAccrualBeginDate, dLastAccrualBeginDate2);
          end;
        else
          Assert(False);
        end;
      except
        on E: EAccrualException do
          AddWarning(EeLogOpers['CUSTOM_EMPLOYEE_NUMBER'], E.Message);
      end;
      EeLogOpers.Next;
    end;
  end;
  procedure ProcessEeToa;
  var
    vRolloverCoToaNbr, vRolloverEeToaNbr: Variant;
    vCheckNbr: Variant;
    bQDoubleTOA : Boolean;
  begin
    IntLocateEeToaSetup(BatchEeToa['EE_TIME_OFF_ACCRUAL_NBR']);
    if (EE_TIME_OFF_ACCRUAL['STATUS'] = GROUP_BOX_YES) and (HireDate <= PeriodEndDate) then
    begin
      // checking for reset
      if TimeToReset and
        ((CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_EES) or
         ((EE['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) and (CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_ACTIVE_EES)) or
         ((CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_EES_HAVE_CHECK) and
             Checks.FindKey([EE['EE_NBR'], PR_BATCH['PR_BATCH_NBR']]))  ) and (not CheckLogForOperation(ocReset)) then
           AddToOperLog(ocReset, Null, Null);

      // checking for rollovers
      if TimeToRollover
      and not CheckLogForOperation(ocRollover) then
      begin // rollover is going on
        vRolloverCoToaNbr := ConvertNull(EE_TIME_OFF_ACCRUAL['RLLOVR_CO_TIME_OFF_ACCRUAL_NBR'],
          CO_TIME_OFF_ACCRUAL['RLLOVR_CO_TIME_OFF_ACCRUAL_NBR']);
        if VarIsNull(vRolloverCoToaNbr) then
          raise EAccrualException.CreateFmtHelp('Employee #%s. Rollover Time Off Accrual reference is missing. Company default is not set.',
          [EE['CUSTOM_EMPLOYEE_NUMBER']], IDH_InconsistentData);
        vRolloverEeToaNbr := CurrentEE_TIME_OFF_ACCRUAL.Lookup('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR',
          VarArrayOf([EE_TIME_OFF_ACCRUAL['EE_NBR'], vRolloverCoToaNbr]), 'EE_TIME_OFF_ACCRUAL_NBR');
        if VarIsNull(vRolloverEeToaNbr) then
          vRolloverEeToaNbr := AddEeToa(EE_TIME_OFF_ACCRUAL['EE_NBR'], vRolloverCoToaNbr,
            EE_TIME_OFF_ACCRUAL['EFFECTIVE_ACCRUAL_DATE'], EE_TIME_OFF_ACCRUAL['NEXT_ACCRUE_DATE'],
            EE_TIME_OFF_ACCRUAL['ANNUAL_ACCRUAL_MAXIMUM'], EE_TIME_OFF_ACCRUAL['OVERRIDE_RATE']);
        // real amounts are posted later
        AddToOperLog(ocRollover, vRolloverEeToaNbr, Null);
      end;
      // deducting used
      AddToOperLog(ocUse, Null, Null);
      if ExcludeAccrueOption = TIME_OFF_EXCLUDE_ACCRUAL_NONE then
        if (CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_EES) or
        ((EE['CURRENT_TERMINATION_CODE'] = EE_TERM_ACTIVE) and (CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_ACTIVE_EES)) or
        ((CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_EES_HAVE_CHECK) and Checks.FindKey([EE['EE_NBR'], PR_BATCH['PR_BATCH_NBR']])) then
        begin
          if CO_TIME_OFF_ACCRUAL['ACCRUAL_ACTIVE'] = TIME_OFF_ACCRUAL_ACTIVE_ALL_EES_HAVE_CHECK then
            vCheckNbr := Checks['PR_CHECK_NBR']
          else
            vCheckNbr := Null;
          case string(CO_TIME_OFF_ACCRUAL['CALCULATION_METHOD'])[1] of
          TIME_OFF_ACCRUAL_CALC_METHOD_BALANCES_ONLY: ;
          TIME_OFF_ACCRUAL_CALC_METHOD_ACCRUE_HOURS:
            AddToOperLog(oc1652, Null, Null);
          TIME_OFF_ACCRUAL_CALC_METHOD_ACCRUE_BY_FREQ:
           begin
            if CO_TIME_OFF_ACCRUAL['FREQUENCY'] = TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD then
              bQDoubleTOA := QDoubleTOA.Result.Locate('CO_TIME_OFF_ACCRUAL_NBR;EE_NBR;Period_Begin_Date;Period_End_Date',VarArrayOf([CO_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'], CurrentEE['EE_NBR'],PeriodBegDate, PeriodEndDate]),[])
            else
              bQDoubleTOA := QDoubleTOA.Result.Locate('CO_TIME_OFF_ACCRUAL_NBR;EE_NBR',VarArrayOf([CO_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR'], CurrentEE['EE_NBR']]),[]);
            if not bQDoubleTOA then
              if not CheckLogForOperation(oc1653) then
                 AddToOperLog(oc1653, Null, vCheckNbr);
           end;
          TIME_OFF_ACCRUAL_CALC_METHOD_AVERAGE_THE_HOURS:
            if not CheckLogForOperation(oc1654) then
              AddToOperLog(oc1654, Null, vCheckNbr);
          TIME_OFF_ACCRUAL_CALC_METHOD_COLUMBIA_EDP:
            begin
              if PR_BATCH['FREQUENCY'] <> FREQUENCY_TYPE_SEMI_MONTHLY then
                raise EInconsistentData.CreateHelp('Employee #' + EE['CUSTOM_EMPLOYEE_NUMBER'] + ' is set up with Columbia EDP time off and pay frequency is not Semi-Monthly.', IDH_InconsistentData);
              if GetDay(PayrollCheckDate) in [11..19] then
                if not CheckLogForOperation(ocCEDP) then
                  AddToOperLog(ocCEDP, Null, vCheckNbr);
            end;
          else
            Assert(False);
          end;
        end;
    end;
  end;
  procedure ProcessEe;
  begin
    BatchEeToa.SetRange([BatchEE['EE_NBR']], [BatchEE['EE_NBR']]);
    BatchEeToa.First;
    while not BatchEeToa.Eof do
    begin
      try
        ProcessEeToa;
      except
        on E: EAccrualException do
          AddWarning(BatchEE['CUSTOM_EMPLOYEE_NUMBER'], E.Message);
      end;
      BatchEeToa.Next;
    end;
  end;
var
 s : String;
begin
  if ExcludeAccrueOption <> TIME_OFF_EXCLUDE_ACCRUAL_ALL then
  begin
    Checks := TevClientDataSet.CreateWithChecksDisabled(nil);
    Checks.IndexFieldNames := 'ee_nbr;pr_batch_nbr';
    HoursUsed := TevClientDataSet.CreateWithChecksDisabled(nil);
    HoursUsed.IndexFieldNames := 'ee_nbr;pr_batch_nbr;pr_check_nbr';
    HoursAccrued := TevClientDataSet.CreateWithChecksDisabled(nil);
    HoursAccrued.IndexFieldNames := 'ee_nbr;pr_batch_nbr;pr_check_nbr';
    EeLogOpers := TevClientDataSet.CreateWithChecksDisabled(nil);
    EeLogOpers.FieldDefs.Add('NBR', ftInteger);
    EeLogOpers.FieldDefs.Add('EE_TO_NBR', ftInteger);
    EeLogOpers.FieldDefs.Add('EE_TO_NBR2', ftInteger);
    EeLogOpers.FieldDefs.Add('CO_TO_NBR', ftInteger);
    EeLogOpers.FieldDefs.Add('PRIORITY', ftInteger);
    EeLogOpers.FieldDefs.Add('OPERATION', ftInteger);
    EeLogOpers.FieldDefs.Add('PR_BATCH_NBR', ftInteger);
    EeLogOpers.FieldDefs.Add('PR_CHECK_NBR', ftInteger);
    EeLogOpers.FieldDefs.Add('CUSTOM_EMPLOYEE_NUMBER', ftString, 20);
    EeLogOpers.IndexFieldNames := 'OPERATION;EE_TO_NBR';
    EeLogOpers.CreateDataSet;
    EeDeactivatedToa := TevClientDataSet.CreateWithChecksDisabled(nil);
    EeDeactivatedToa.FieldDefs.Add('EE_TO_NBR', ftInteger);
    EeDeactivatedToa.IndexFieldNames := 'EE_TO_NBR';
    EeDeactivatedToa.CreateDataSet;
    HoursWorked_1654 := TevClientDataSet.CreateWithChecksDisabled(nil);
    HoursWorked_1654.IndexFieldNames := 'EE_NBR;CO_TIME_OFF_ACCRUAL_NBR';
    HoursWorked2_1654 := TevClientDataSet.CreateWithChecksDisabled(nil);
    HoursWorked2_1654.IndexFieldNames := 'EE_NBR;CO_TIME_OFF_ACCRUAL_NBR';
    HoursWorked_Cedp := TevClientDataSet.CreateWithChecksDisabled(nil);
    HoursWorked_Cedp.IndexFieldNames := 'EE_NBR';
    HoursWorked2_Cedp := TevClientDataSet.CreateWithChecksDisabled(nil);
    HoursWorked2_Cedp.IndexFieldNames := 'EE_NBR';
    try

     s :=
     'SELECT ' +
      't3.Co_Time_Off_Accrual_Nbr, t2.Ee_Nbr, t3.FREQUENCY, ' +
      't1.Accrual_Date, t4.Period_Begin_Date, t4.Period_End_Date ' +
     'FROM ' +
       'Ee_Time_Off_Accrual_Oper  t1, Ee_Time_Off_Accrual  t2, ' +
       'Co_Time_Off_Accrual  t3, Pr_Batch  t4 ' +
     'WHERE ' +
      't1.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
    ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') and ' +
      '{AsOfNow<t1>} and {AsOfNow<t2>} and ' +
      '{AsOfNow<t3>} and {AsOfNow<t4>} and ' +
      't2.Ee_Time_Off_Accrual_Nbr = t1.Ee_Time_Off_Accrual_Nbr  AND ' +
      't3.Co_Time_Off_Accrual_Nbr = t2.Co_Time_Off_Accrual_Nbr  AND ' +
      't4.Pr_Batch_Nbr = t1.Pr_Batch_Nbr ' +
     'AND ' +
      't1.Accrued > 0 AND t1.Operation_Code = ''A'' AND ' +
      '((t3.Frequency IN (''1'') OR ' +
      '(t3.Frequency IN (''2'', ''3'', ''4'', ''5'') AND ' +
      'EXTRACTMONTH(t1.Accrual_Date) = EXTRACTMONTH(:CheckDate ) AND  ' +
      'EXTRACTYEAR(t1.Accrual_Date) =  EXTRACTYEAR(:CheckDate))))';
     QDoubleTOA := TevQuery.Create(s);
     QDoubleTOA.Result.IndexFieldNames := 'CO_TIME_OFF_ACCRUAL_NBR;EE_NBR';
     QDoubleTOA.Params.AddValue('CheckDate', PayrollCheckDate);
     QDoubleTOA.Execute;

      // process voids
      // process accrual
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('TABLENAME', 'PR_CHECK');
        SetMacro('COLUMNS', 'DISTINCT EE_NBR, PR_BATCH_NBR, PR_CHECK_NBR');
        SetMacro('CONDITION', 'PR_NBR=:PrNbr and CHECK_TYPE in (:CheckTypeReg,:CheckTypeManual) and '+
          'EXCLUDE_TIME_OFF_ACCURAL=:GroupBoxNo');
        SetParam('PrNbr', PayrollNbr);
        SetParam('CheckTypeReg', CHECK_TYPE2_REGULAR);
        SetParam('CheckTypeManual', CHECK_TYPE2_MANUAL);
        SetParam('GroupBoxNo', GROUP_BOX_NO);
        ctx_DataAccess.GetCustomData(Checks, AsVariant);
      end;
      with TExecDSWrapper.Create('PayrollTimeOffEdHours') do
      begin
        SetParam('AsOfDate', PayrollCheckDate);
        SetParam('PrNbr', PayrollNbr);
        SetParam('CheckTypeReg', CHECK_TYPE2_REGULAR);
        SetParam('CheckTypeManual', CHECK_TYPE2_MANUAL);
        SetParam('StatusVoid', CHECK_STATUS_VOID);
        SetMacro('EXCLUDES', ':ExcludeNone,:ExcludeAccrual');
        SetParam('ExcludeNone', TIME_OFF_EXCLUDE_ACCRUAL_NONE);
        SetParam('ExcludeAccrual', TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL);
        ctx_DataAccess.GetCustomData(HoursUsed, AsVariant);
      end;
      with TExecDSWrapper.Create('PayrollTimeOffEdHours') do
      begin
        SetParam('AsOfDate', PayrollCheckDate);
        SetParam('PrNbr', PayrollNbr);
        SetParam('CheckTypeReg', CHECK_TYPE2_REGULAR);
        SetParam('CheckTypeManual', CHECK_TYPE2_MANUAL);
        SetParam('StatusVoid', CHECK_STATUS_VOID);
        SetMacro('EXCLUDES', ':ExcludeNone');
        SetParam('ExcludeNone', TIME_OFF_EXCLUDE_ACCRUAL_NONE);
        ctx_DataAccess.GetCustomData(HoursAccrued, AsVariant);
      end;
      PR_BATCH.First;
      while not PR_BATCH.Eof do
      begin
        SetBatchData;
        BatchEE.First;
        while not BatchEE.Eof do
        begin
          ProcessEe;
          BatchEE.Next;
        end;
        PR_BATCH.Next;
      end;
      ProcessPostponed;
      ReadjustOperations(PayrollNbr, PayrollCheckDate);
    finally
      HoursWorked_1654.Free;
      HoursWorked2_1654.Free;
      HoursWorked_Cedp.Free;
      HoursWorked2_Cedp.Free;
      HoursAccrued.Free;
      HoursUsed.Free;
      Checks.Free;
      EeLogOpers.Free;
      EeDeactivatedToa.Free;
    end;
    PostChanges;
  end;
end;

function TPayrollToaSetupAccess.MinHoursBeforeAccrual: Double;
begin
  Result := ConvertNull(CO_TIME_OFF_ACCRUAL['MINIMUM_HOURS_BEFORE_ACCRUAL'], 0);
end;

function TPayrollToaSetupAccess.Rate: Double;
begin
  Result := RoundAll(ConvertNull(EE_TIME_OFF_ACCRUAL['OVERRIDE_RATE'], CO_TIME_OFF_ACCRUAL_RATES['HOURS']), RoundingPrecision);
end;

procedure TPayrollToaSetupAccess.SetBatchData;
  procedure SetBatchAdditionalInfo(const BatchFreq: string);
  var
    PriorCheckDate, NextCheckDate: TDateTime;
  begin
    NextCheckDate := ctx_PayrollCalculation.GetNextCheckDate(PayrollCheckDate, BatchFreq[1]);
    ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('BatchAdditionalInfo') do
    begin
      SetParam('CheckDate', PayrollCheckDate);
      SetParam('CoNbr', CoNbr);
      SetParam('BatchFreq', BatchFreq);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    ctx_DataAccess.CUSTOM_VIEW.First;
    PriorCheckDate := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['MAX_CHECK_DATE'], 0);
    ctx_DataAccess.CUSTOM_VIEW.Close;

    FPayrollAdditionalInfo := [];
    if GetMonth(PayrollCheckDate) <> GetMonth(PriorCheckDate) then
      FPayrollAdditionalInfo := FPayrollAdditionalInfo + [aiFirst];
    if GetMonth(PayrollCheckDate) <> GetMonth(NextCheckDate) then
      FPayrollAdditionalInfo := FPayrollAdditionalInfo + [aiLast];
    if (Abs(GetDay(PayrollCheckDate) - 15) <= Abs(GetDay(NextCheckDate) - 15))
      and (Abs(GetDay(PayrollCheckDate) - 15) < Abs(GetDay(PriorCheckDate) - 15)) then
      FPayrollAdditionalInfo := FPayrollAdditionalInfo + [aiMiddlest];
  end;
begin
  FBatchNbr := PR_BATCH['PR_BATCH_NBR'];
  FPeriodBegDate := PR_BATCH['PERIOD_BEGIN_DATE'];
  FBatchEndDate := PR_BATCH['PERIOD_END_DATE'];
  SetBatchAdditionalInfo(PR_BATCH['FREQUENCY']);
  BatchEE.SetRange([CoNbr, PR_BATCH['FREQUENCY']], [CoNbr, PR_BATCH['FREQUENCY']]);
end;

procedure TPayrollToaSetupAccess.SetBatchData(const PrBatchNbr: Integer);
begin
  if PrBatchNbr <> FBatchNbr then
  begin
    Assert(PR_BATCH.FindKey([PrBatchNbr]));
    SetBatchData;
  end;
end;

function TPayrollToaSetupAccess.TimeToAccrue: Boolean;
var
  i: Integer;
  s: string;
begin
  s := ConvertNull(CO_TIME_OFF_ACCRUAL['ACCRUAL_MONTH_NUMBER'], '');
  if s = '' then
    i := 1
  else
    i := StrToInt(s);
  Result := CheckToFreq(HireDate, 0, string(CO_TIME_OFF_ACCRUAL['FREQUENCY'])[1],
    string(CO_TIME_OFF_ACCRUAL['PAYROLL_OF_THE_MONTH_TO_ACCRUE'])[1],
    i);
end;

function TPayrollToaSetupAccess.TimeToReset: Boolean;
var
  s: string;
  i: Integer;
  pom: Char;
  d,
  PriorResetPeriodBeginDate,
  PriorResetPeriodEndDate: TDateTime;

  procedure GetPriorResetBatchDatePeriod;
  var
    LastReset_CheckDate: variant;
    qText: string;
  begin
    ctx_DataAccess.CUSTOM_VIEW.Close;
    // Get check date when the last TOA reset occured
    qText := 'SELECT Max(t5.Check_Date) MAX_CHECK_DATE ' +
             'FROM Ee_Time_Off_Accrual_Oper t1, Ee_Time_Off_Accrual t3, Co_Time_Off_Accrual t4, Pr t5 '+
             'WHERE t3.Ee_Time_Off_Accrual_Nbr = t1.Ee_Time_Off_Accrual_Nbr AND t4.Co_Time_Off_Accrual_Nbr = t3.Co_Time_Off_Accrual_Nbr AND '+

             't1.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
                      ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') and ' +

             ' t5.Pr_Nbr = t1.Pr_Nbr AND t1.Operation_Code = ''R'' AND '+
             ' t3.Ee_Nbr = :EE_NBR AND t4.Co_Time_Off_Accrual_Nbr = :CO_TIME_OFF_ACCRUAL_NBR AND ' +
             ' {AsOfNow<t1>} and {AsOfNow<t3>} and {AsOfNow<t4} ';
    with TExecDSWrapper.Create( qText ) do
    begin
      SetParam('EE_NBR', EE['EE_NBR']);
      SetParam('CO_TIME_OFF_ACCRUAL_NBR', CO_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR']);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    LastReset_CheckDate := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['MAX_CHECK_DATE'], 0);

    ctx_DataAccess.CUSTOM_VIEW.Close;
    // Get batch date preiod the last TOA reset occured
    qText := 'SELECT t2.Period_Begin_Date, t2.Period_End_Date ' +
             'FROM Ee_Time_Off_Accrual_Oper t1, Pr_Batch  t2, Ee_Time_Off_Accrual t3, Co_Time_Off_Accrual t4, Pr t5 '+
             'WHERE t2.Pr_Batch_Nbr = t1.Pr_Batch_Nbr AND t3.Ee_Time_Off_Accrual_Nbr = t1.Ee_Time_Off_Accrual_Nbr AND t4.Co_Time_Off_Accrual_Nbr = t3.Co_Time_Off_Accrual_Nbr AND '+
               't1.OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
                 ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') and ' +

             ' t5.Pr_Nbr = t2.Pr_Nbr AND t1.Operation_Code = ''R'' AND t5.CHECK_Date = :CheckDate AND '+
             ' t3.Ee_Nbr = :EE_NBR AND t4.Co_Time_Off_Accrual_Nbr = :CO_TIME_OFF_ACCRUAL_NBR AND ' +
             ' {AsOfNow<t1>} and {AsOfNow<t3>} and {AsOfNow<t4>} ';

    with TExecDSWrapper.Create( qText ) do
    begin
      SetParam('EE_NBR', EE['EE_NBR']);
      SetParam('CO_TIME_OFF_ACCRUAL_NBR', CO_TIME_OFF_ACCRUAL['CO_TIME_OFF_ACCRUAL_NBR']);
      SetParam('CheckDate', LastReset_CheckDate);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    PriorResetPeriodBeginDate := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['Period_Begin_Date'], 0);
    PriorResetPeriodEndDate := ConvertNull(ctx_DataAccess.CUSTOM_VIEW['Period_End_Date'], 0);
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;

  function IsDateInResetPeriod(const d: TDateTime): Boolean;
  begin
    // if this date "d" exists in previous batch date period TOA resest occured last time then
    // it should not happen again, ticket #58559
    if (PriorResetPeriodBeginDate = 0) and (PriorResetPeriodEndDate = 0) then
      GetPriorResetBatchDatePeriod;

    Result := not GenericIsDateInPeriod(d, PriorResetPeriodBeginDate, PriorResetPeriodEndDate);
    if Result then
      Result := GenericIsDateInPeriod(d, PeriodBegDate, PeriodEndDate);
  end;
begin
  s := ConvertNull(CO_TIME_OFF_ACCRUAL['RESET_MONTH_NUMBER'], '');
  if s = '' then
    i := 1
  else
    i := StrToInt(s);

  PriorResetPeriodBeginDate := 0;
  PriorResetPeriodEndDate := 0;
  pom := string(CO_TIME_OFF_ACCRUAL['RESET_PAYROLL_OF_MONTH'])[1];
  d := ConvertNull(CO_TIME_OFF_ACCRUAL['RESET_DATE'], HireDate);
  if ConvertNull(CO_TIME_OFF_ACCRUAL['ANNUAL_RESET_CODE'], ' ') <> ' ' then
  begin
    case string(ConvertNull(CO_TIME_OFF_ACCRUAL['ANNUAL_RESET_CODE'], ''))[1] of
    TIME_OFF_ACCRUAL_RESET_CODE_NONE2: Result := False; // TODO: needs to be removed. Added because of the incorrect conversion for Plymouth on 05/17/2013
    TIME_OFF_ACCRUAL_RESET_CODE_NONE:
      Result := False;
    TIME_OFF_ACCRUAL_RESET_CODE_HIRE_DATE_AFTER_DAYS:
      Result := IsDateInResetPeriod(HireDate+ ConvertNull(CO_TIME_OFF_ACCRUAL['RESET_OFFSET'], 0));
    TIME_OFF_ACCRUAL_RESET_CODE_HIRE_DATE_AFTER_MONTHS:
      Result := IsDateInResetPeriod(AddMonths(HireDate, ConvertNull(CO_TIME_OFF_ACCRUAL['RESET_OFFSET'], 0)));
    TIME_OFF_ACCRUAL_RESET_CODE_PER_PAY_PERIOD:
      Result := True;
    TIME_OFF_ACCRUAL_RESET_CODE_MONTHLY:
      Result := InPeriodIfCombineMonthly(pom);
    TIME_OFF_ACCRUAL_RESET_CODE_QARTERLY:
      Result := InPeriodIfCombineNotMonthly(pom, i, 3);
    TIME_OFF_ACCRUAL_RESET_CODE_SEMI_ANNUAL:
      Result := InPeriodIfCombineNotMonthly(pom, i, 6);
    TIME_OFF_ACCRUAL_RESET_CODE_ANNUAL:
      Result := InPeriodIfCombineNotMonthly(pom, i, 12)
        or ((pom = TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE)
          and (IsDateInResetPeriod(SameDayAnotherYear(d, PeriodBegDate)) or IsDateInResetPeriod(SameDayAnotherYear(d, PeriodEndDate))));
    TIME_OFF_ACCRUAL_RESET_CODE_ANNUAL_HIRE_DATE:
      Result := IsDateInResetPeriod(SameDayAnotherYear(HireDate, PeriodBegDate)) or IsDateInResetPeriod(SameDayAnotherYear(HireDate, PeriodEndDate));
    TIME_OFF_ACCRUAL_RESET_CODE_ANNUAL_RESET_DATE:
    begin
      d := ConvertNull(CO_TIME_OFF_ACCRUAL['RESET_DATE'], 0);
      Result := (d <> 0) and (IsDateInResetPeriod(SameDayAnotherYear(d, PeriodBegDate)) or IsDateInResetPeriod(SameDayAnotherYear(d, PeriodEndDate)));
    end
    else
      Assert(False);
      Result := False; //to avoid compile warning
    end;
  end
  else
    Result := False;
end;

function TPayrollToaSetupAccess.TimeToRollover: Boolean;
var
  v: Variant;
begin
  case string(CO_TIME_OFF_ACCRUAL['ROLLOVER_FREQ'])[1] of
  TIME_OFF_ACCRUAL_FREQ_NONE:
    Result := IsDateInPeriod(ConvertNull(EE_TIME_OFF_ACCRUAL['ROLLOVER_DATE'], 0));
  else
    v := ConvertNull(EE_TIME_OFF_ACCRUAL['ROLLOVER_DATE'], CO_TIME_OFF_ACCRUAL['ROLLOVER_DATE']);
    if VarIsNull(v) then
      {raise EAccrualException.CreateFmtHelp('Employee #%s. Missing rollover date. Default rollover is not set.',
      [CurrentEE['CUSTOM_EMPLOYEE_NUMBER']], IDH_InconsistentData);}
      v := HireDate;
    Result := CheckToFreq(v, ConvertNull(CO_TIME_OFF_ACCRUAL['ROLLOVER_OFFSET'], 0),
      string(CO_TIME_OFF_ACCRUAL['ROLLOVER_FREQ'])[1], string(CO_TIME_OFF_ACCRUAL['ROLLOVER_PAYROLL_OF_MONTH'])[1],
      ConvertNull(CO_TIME_OFF_ACCRUAL['ROLLOVER_MONTH_NUMBER'], 0));
  end;
end;

procedure CalculateTimeOffAccrual(var Warnings: String);
var
  PayrollAccess: TPayrollToaSetupAccess;
  HistAccess: THistoricToaSetupAccess;
  dStartingDate: TDateTime;
  cdVoidedEeToaOpers: TevClientDataSet;
begin
  cdVoidedEeToaOpers := TevClientDataSet.CreateWithChecksDisabled(nil);
  try
    with TExecDSWrapper.Create('GenericSelect2DiffNbrWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.EE_TIME_OFF_ACCRUAL_OPER_NBR, t1.accrual_date, t2.pr_check_nbr, t2.pr_batch_nbr');
      SetMacro('TABLE1', 'EE_TIME_OFF_ACCRUAL_OPER');
      SetMacro('TABLE2', 'PR_CHECK');
      SetMacro('CONDITION',
        'OPERATION_CODE not in (''' + EE_TOA_OPER_CODE_REQUEST_PENDING + ''', ''' + EE_TOA_OPER_CODE_REQUEST_APPROVED +
             ''', ''' + EE_TOA_OPER_CODE_REQUEST_DENIED + ''') and ' +
        '{AsOfNow<t1>} and {AsOfNow<t2>} and '+
        't2.pr_nbr=:PrNbr and t2.CHECK_STATUS=:Ct and t1.pr_check_nbr=''0''||Trim(StrCopy(t2.FILLER, 0, 8))');
      SetParam('PrNbr', DM_PAYROLL.PR['PR_NBR']);
      SetParam('Ct', CHECK_STATUS_VOID);
      ctx_DataAccess.GetCustomData(cdVoidedEeToaOpers, AsVariant);
    end;
    if cdVoidedEeToaOpers.RecordCount > 0 then
    begin
      cdVoidedEeToaOpers.First;
      dStartingDate := cdVoidedEeToaOpers['ACCRUAL_DATE'];
      while not cdVoidedEeToaOpers.Eof do
      begin
        dStartingDate := Min(cdVoidedEeToaOpers['ACCRUAL_DATE'], dStartingDate);
        cdVoidedEeToaOpers.Next;
      end;
      HistAccess := THistoricToaSetupAccess.Create(DM_PAYROLL.PR['CO_NBR'], dStartingDate);
      try
        cdVoidedEeToaOpers.First;
        while not cdVoidedEeToaOpers.Eof do
        begin
          Assert(HistAccess.ShadowCollapsedEeOpers.FindKey([cdVoidedEeToaOpers['EE_TIME_OFF_ACCRUAL_OPER_NBR']]));
          HistAccess.AddOper(cdVoidedEeToaOpers['EE_TIME_OFF_ACCRUAL_OPER_NBR'], Null, DM_PAYROLL.PR['PR_NBR'],
            cdVoidedEeToaOpers['pr_batch_nbr'], cdVoidedEeToaOpers['pr_check_nbr'],
            EE_TOA_OPER_CODE_VOID, '', DM_PAYROLL.PR['CHECK_DATE'],
            -HistAccess.ShadowCollapsedEeOpers['adj_accrued'], -HistAccess.ShadowCollapsedEeOpers['adj_used'],
            -HistAccess.ShadowCollapsedEeOpers['adj_accrued_capped'],
            HistAccess.ShadowCollapsedEeOpers['ee_time_off_accrual_nbr']);
          cdVoidedEeToaOpers.Next;
        end;
        HistAccess.ReadjustOperations(DM_PAYROLL.PR['PR_NBR'], DM_PAYROLL.PR['CHECK_DATE']);
        HistAccess.PostChanges;
      finally
        HistAccess.Free;
      end;
    end;
  finally
    cdVoidedEeToaOpers.Free;
  end;
  PayrollAccess := TPayrollToaSetupAccess.Create(DM_PAYROLL.PR['PR_NBR']);
  try
    PayrollAccess.MainProc;
    if PayrollAccess.Warnings <> '' then
      if Warnings <> '' then
        Warnings := Warnings+ #13+ PayrollAccess.Warnings
      else
        Warnings := PayrollAccess.Warnings;
  finally
    PayrollAccess.Free;
  end;
end;

function TPayrollToaSetupAccess.MaxHoursToAccrueOn: Double;
begin
  Result := ConvertNull(CO_TIME_OFF_ACCRUAL['MAXIMUM_HOURS_TO_ACCRUE_ON'], 999999999999);
end;

function TPayrollToaSetupAccess.AddAccrualPrOper(const PrCheckNbr: Variant; const AccrualBase, Rate: Double;
  const AccrualBaseCap: Variant): Integer;
var
  dProrator: Double;
begin
  if VarIsNull(AccrualBaseCap) or (AccrualBaseCap >= AccrualBase) then
  begin
    if not Prorated(AccrualBase, dProrator) then
      Result := AddPrOper(Null, Null, PrCheckNbr, EE_TOA_OPER_CODE_ACCRUAL,
        FloatToStr(AccrualBase)+'x'+ FloatToStr(Rate), AccrualBase* Rate, 0, Null)
    else
      Result := AddPrOper(Null, Null, PrCheckNbr, EE_TOA_OPER_CODE_ACCRUAL,
        FloatToStr(AccrualBase)+'x'+ FloatToStr(Rate)+ ' '+ FloatToStr(dProrator*100)+ '%',
        AccrualBase* Rate* dProrator, 0, Null);
  end
  else
  begin
    if not Prorated(AccrualBaseCap, dProrator) then
      Result := AddPrOper(Null, Null, PrCheckNbr, EE_TOA_OPER_CODE_ACCRUAL,
        FloatToStr(AccrualBaseCap)+ '('+ FloatToStr(AccrualBase)+')x'+ FloatToStr(Rate),
        AccrualBaseCap* Rate, 0, Null)
    else
      Result := AddPrOper(Null, Null, PrCheckNbr, EE_TOA_OPER_CODE_ACCRUAL,
        FloatToStr(AccrualBaseCap)+ '('+ FloatToStr(AccrualBase)+')x'+ FloatToStr(Rate)+ ' '+
        FloatToStr(dProrator*100)+ '%',
        AccrualBaseCap* Rate* dProrator, 0, Null);
  end;
end;

function TPayrollToaSetupAccess.IsAccruedEd(const Ed: Integer): Boolean;
begin
  Result := IsInArrayOfIntegers(FAccruedEds, Ed);
end;

function TPayrollToaSetupAccess.IsUsedEd(const Ed: Integer): Boolean;
begin
  Result := IsInArrayOfIntegers(FUsedEds, Ed);
end;

function TPayrollToaSetupAccess.IsInArrayOfIntegers(
  const a: TArrayOfIntegers; const Ed: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to High(a) do
    if a[i] = Ed then
    begin
      Result := True;
      Break;
    end;
end;

procedure TPayrollToaSetupAccess.AddWarning(const EeCustNumber,
  Warning: string);
var
  s: string;
begin
  s := 'Employee '+ EeCustNumber+ ' failed to accrue with message: '+ Warning;
  if FWarnings <> '' then
    FWarnings := FWarnings+ #13+ s
  else
    FWarnings := s;
end;

function TPayrollToaSetupAccess.Prorated(const Hours: Double; out Prorator: Double): Boolean;
begin
  Result := LocateEeProratorSetup(EeToaNbr, Hours, PeriodEndDate);
  if Result then
    Prorator := CO_TIME_OFF_ACCRUAL_TIERS['RATE'];
end;

end.
