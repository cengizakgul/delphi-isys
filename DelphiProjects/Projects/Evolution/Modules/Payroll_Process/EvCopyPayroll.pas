unit EvCopyPayroll;

interface

uses
  SysUtils, DB,
  isTypes, isBaseClasses,
  EvCommonInterfaces, EvDataSet, EvUtils, EvConsts, EvContext;

procedure CopyPayroll(const ASrcPrNbr, ADestPrNbr: Integer; const AVoidSourcePr: Boolean);

implementation

const
  BatchFieldList =
    'pr_batch_nbr,' +
    'pr_nbr,' +
    'co_pr_check_templates_nbr,' +
    'co_pr_filters_nbr,' +
    'period_begin_date,' +
    'period_begin_date_status,' +
    'period_end_date,' +
    'period_end_date_status,' +
    'frequency,' +
    'pay_salary,' +
    'pay_standard_hours,' +
    'load_dbdt_defaults,' +
    'exclude_time_off,' +
    'filler';

  CheckSUIsFieldList =
    'pr_check_sui_nbr,'#13 +
    'pr_check_nbr,'#13 +
    'pr_nbr,'#13 +
    'co_sui_nbr,'#13 +
    'sui_taxable_wages,'#13 +
    'sui_gross_wages,'#13 +
    'sui_tax,'#13 +
    'override_amount';

  CheckLineLocalsFieldList =  
    'pr_check_line_locals_nbr,'#13 +
    'pr_check_lines_nbr,'#13 +
    'ee_locals_nbr,'#13 +
    'exempt_exclude';

  CheckLinesFieldList =  
    'pr_check_lines_nbr,' +
    'pr_check_nbr,' +
    'pr_nbr,' +
    'agency_status,' +
    'line_type,' +
    'line_item_date,' +
    'line_item_end_date,' +
    'co_division_nbr,' +
    'co_branch_nbr,' +
    'co_department_nbr,' +
    'co_team_nbr,' +
    'co_jobs_nbr,' +
    'ee_states_nbr,' +
    'ee_sui_states_nbr,' +
    'co_workers_comp_nbr,' +
    'cl_agency_nbr,' +
    'rate_number,' +
    'rate_of_pay,' +
    'hours_or_pieces,' +
    'amount,' +
    'e_d_differential_amount,' +
    'co_shifts_nbr,' +
    'cl_e_ds_nbr,' +
    'cl_pieces_nbr,' +
    'ee_scheduled_e_ds_nbr,' +
    'deduction_shortfall_carryover,' +
    'filler,' +
    'punch_in,' +
    'punch_out,' +
    'user_override,' +
    'user_grouping,' +
    'reducing_cl_e_d_groups_nbr,' +
    'reduced_hours,'+
    'work_address_ovr';

  CheckLocalsFieldList =
    'pr_check_locals_nbr,'#13 +
    'pr_check_nbr,'#13 +
    'pr_nbr,'#13 +
    'ee_locals_nbr,'#13 +
    'local_taxable_wage,'#13 +
    'local_gross_wages,'#13 +
    'local_tax,'#13 +
    'exclude_local,'#13 +
    'override_amount,'#13 +
    'local_shortfall,'#13 +
    'NONRES_EE_LOCALS_NBR,'#13 +
    'CO_LOCATIONS_NBR,'+
    'reportable';

  CheckStatesFieldList =
    'pr_check_states_nbr,'#13 +
    'pr_check_nbr,'#13 +
    'pr_nbr,'#13 +
    'ee_states_nbr,'#13 +
    'tax_at_supplemental_rate,'#13 +
    'exclude_state,'#13 +
    'exclude_sdi,'#13 +
    'exclude_sui,'#13 +
    'exclude_additional_state,'#13 +
    'state_override_type,'#13 +
    'state_override_value,'#13 +
    'state_shortfall,'#13 +
    'state_taxable_wages,'#13 +
    'state_gross_wages,'#13 +
    'state_tax,'#13 +
    'ee_sdi_taxable_wages,'#13 +
    'ee_sdi_gross_wages,'#13 +
    'ee_sdi_tax,'#13 +
    'ee_sdi_override,'#13 +
    'er_sdi_taxable_wages,'#13 +
    'er_sdi_gross_wages,'#13 +
    'er_sdi_tax,'#13 +
    'ee_sdi_shortfall';

  ChecksFieldList =  
    'pr_check_nbr,' +
    'pr_nbr,' +
    'pr_batch_nbr,' +
    'payment_serial_number,' +
    'notes_nbr,' +
    'ee_nbr,' +
    'custom_pr_bank_acct_number,' +
    'check_type,' +
    'salary,' +
    'exclude_dd,' +
    'exclude_dd_except_net,' +
    'exclude_time_off_accural,' +
    'exclude_auto_distribution,' +
    'exclude_all_sched_e_d_codes,' +
    'exclude_sch_e_d_from_agcy_chk,' +
    'exclude_sch_e_d_except_pension,' +
    'prorate_scheduled_e_ds,' +
    'or_check_federal_value,' +
    'or_check_federal_type,' +
    'or_check_oasdi,' +
    'or_check_medicare,' +
    'or_check_eic,' +
    'or_check_back_up_withholding,' +
    'exclude_federal,' +
    'exclude_additional_federal,' +
    'exclude_employee_oasdi,' +
    'exclude_employer_oasdi,' +
    'exclude_employee_medicare,' +
    'exclude_employer_medicare,' +
    'exclude_employee_eic,' +
    'exclude_employer_fui,' +
    'tax_at_supplemental_rate,' +
    'tax_frequency,' +
    'check_status,' +
    'status_change_date,' +
    'gross_wages,' +
    'net_wages,' +
    'federal_taxable_wages,' +
    'federal_gross_wages,' +
    'federal_tax,' +
    'ee_oasdi_taxable_wages,' +
    'ee_oasdi_gross_wages,' +
    'ee_oasdi_taxable_tips,' +
    'ee_oasdi_tax,' +
    'ee_medicare_taxable_wages,' +
    'ee_medicare_gross_wages,' +
    'ee_medicare_tax,' +
    'ee_eic_tax,' +
    'er_oasdi_taxable_wages,' +
    'er_oasdi_gross_wages,' +
    'er_oasdi_taxable_tips,' +
    'er_oasdi_tax,' +
    'er_medicare_taxable_wages,' +
    'er_medicare_gross_wages,' +
    'er_medicare_tax,' +
    'er_fui_taxable_wages,' +
    'er_fui_gross_wages,' +
    'er_fui_tax,' +
    'exclude_from_agency,' +
    'check_type_945,' +
    'federal_shortfall,' +
    'filler,' +
    'calculate_override_taxes,' +
    'user_grouping,' +
    'reciprocate_sui,' +
    'disable_shortfalls,' +
    'update_balance';

type

  TevCopyPayrollHelper = class
  private
    FDestPrNbr: Integer;
    FVoidSourcePr: Boolean;
    FDstBatches: IevTable;
    FDstChecks: IevTable;
    FDstCheckStates: IevTable;
    FDstCheckSUIs: IevTable;
    FDstCheckLocals: IevTable;
    FDstCheckLines: IevTable;
    FDstCheckLineLocals: IevTable;
    procedure CopyBatches(const ASrcPrNbr: Integer);
    procedure CopyChecks(const ASrcBatchNbr, ADestBatchNbr: Integer);
    procedure CopyCheckLines(const ASrcCheckNbr, ADestCheckNbr, AEENbr : Integer);
    procedure CopyCheckLineLocals(const ASrcLineNbr, ADestLineNbr: Integer);
    procedure CopyCheckStates(const ASrcCheckNbr, ADestCheckNbr: Integer);
    procedure CopyCheckSUIs(const ASrcCheckNbr, ADestCheckNbr: Integer);
    procedure CopyCheckLocals(const ASrcCheckNbr, ADestCheckNbr: Integer);
  public
    procedure Execute(const ASrcPrNbr, ADestPrNbr: Integer; const AVoidSourcePr: Boolean);
  end;

procedure CopyPayroll(const ASrcPrNbr, ADestPrNbr: Integer; const AVoidSourcePr: Boolean);
begin
  with TevCopyPayrollHelper.Create do
  try
    Execute(ASrcPrNbr, ADestPrNbr, AVoidSourcePr);
  finally
    Free;
  end;
end;

{ TevCopyPayrollHelper }

procedure TevCopyPayrollHelper.CopyBatches(const ASrcPrNbr: Integer);
var
  Q: IevQuery;
  SrcBatches: IevDataSet;
  i: Integer;
begin
  // Open source data
  Q := TevQuery.Create('SELECT ' + BatchFieldList + ' FROM pr_batch WHERE pr_nbr = :pr_nbr AND {AsOfNow<pr_batch>}');
  Q.Param['pr_nbr'] := ASrcPrNbr;
  Q.Execute;
  SrcBatches := Q.Result;

  // Open destination table
  FDstBatches := TevTable.Create('pr_batch', BatchFieldList, AlwaysFalseCond, SysDate);
  FDstBatches.Open;

  // Open destination table
  FDstCheckSUIs := TevTable.Create('pr_check_sui', CheckSUIsFieldList, AlwaysFalseCond, SysDate);
  FDstCheckSUIs.Open;

  // Open destination table
  FDstCheckLineLocals := TevTable.Create('pr_check_line_locals', CheckLineLocalsFieldList , AlwaysFalseCond, SysDate);
  FDstCheckLineLocals.Open;

  // Open destination table
  FDstCheckLines := TevTable.Create('pr_check_lines', CheckLinesFieldList,
     AlwaysFalseCond, SysDate);
  FDstCheckLines.Open;

  // Open destination table
  FDstCheckLocals := TevTable.Create('pr_check_locals', CheckLocalsFieldList,  AlwaysFalseCond, SysDate);
  FDstCheckLocals.Open;

  // Open destination table
  FDstCheckStates := TevTable.Create('pr_check_states', CheckStatesFieldList, AlwaysFalseCond, SysDate);
  FDstCheckStates.Open;

  // Open destination table
  FDstChecks := TevTable.Create('pr_check', ChecksFieldList, AlwaysFalseCond, SysDate);
  FDstChecks.Open;

  while not SrcBatches.Eof do
  begin
    FDstBatches.Append;
    FDstBatches.Fields[1].AsInteger := FDestPrNbr;
    for i := 2 to SrcBatches.Fields.Count - 1 do
      FDstBatches.Fields[i].Value := SrcBatches.Fields[i].Value;
    FDstBatches.Post;

    CopyChecks(SrcBatches.Fields[0].AsInteger, FDstBatches.Fields[0].AsInteger);

    SrcBatches.Next;
  end;

  FDstBatches.SaveChanges;
  FDstChecks.SaveChanges;
  FDstCheckStates.SaveChanges;
  FDstCheckSUIs.SaveChanges;
  FDstCheckLocals.SaveChanges;
  FDstCheckLines.SaveChanges;
  FDstCheckLineLocals.SaveChanges;
  
end;

procedure TevCopyPayrollHelper.CopyCheckLineLocals(const ASrcLineNbr, ADestLineNbr: Integer);
var
  Q: IevQuery;
  SrcCheckLineLocals: IevDataSet;
  i: Integer;
begin
  // Open source data
  Q := TevQuery.Create('SELECT ' + CheckLineLocalsFieldList + ' FROM pr_check_line_locals WHERE pr_check_lines_nbr = :pr_check_lines_nbr AND {AsOfNow<pr_check_line_locals>}');
  Q.Param['pr_check_lines_nbr'] := ASrcLineNbr;
  Q.Execute;
  SrcCheckLineLocals := Q.Result;

  while not SrcCheckLineLocals.Eof do
  begin
    FDstCheckLineLocals.Append;
    FDstCheckLineLocals.Fields[1].AsInteger := ADestLineNbr;
    for i := 2 to SrcCheckLineLocals.Fields.Count - 1 do
      FDstCheckLineLocals.Fields[i].Value := SrcCheckLineLocals.Fields[i].Value;
    FDstCheckLineLocals.Post;

    SrcCheckLineLocals.Next;
  end;
end;

procedure TevCopyPayrollHelper.CopyCheckLines(const ASrcCheckNbr, ADestCheckNbr, AEENbr: Integer);
var
  Q: IevQuery;
  SrcCheckLines: IevDataSet;
  i: Integer;

  procedure InvertValue(const FildName: String);
  var
    Fld: TField;
  begin
    Fld := FDstCheckLines.FieldByName(FildName);
    if not Fld.IsNull then
      Fld.AsFloat := - Fld.AsFloat;
  end;

  procedure CalcDeductionShortfall;
  var
    Q: IevQuery;
    Fld: Tfield;
  begin
    Fld := FDstCheckLines.FieldByName('DEDUCTION_SHORTFALL_CARRYOVER');

    Q := TevQuery.Create(
      'SELECT deduction_shortfall_carryover'#13 +
      'FROM pr_check_lines'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check_lines>} AND'#13 +
      '  pr_check_lines_nbr = ('#13 +
      '    SELECT MAX(pr_check_lines_nbr)'#13 +
      '    FROM pr_check c, pr_check_lines l'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<c>} AND'#13 +
      '      {AsOfNow<l>} AND'#13 +
      '      l.pr_check_nbr = c.pr_check_nbr AND'#13 +
      '      c.pr_check_nbr < :src_check_nbr AND'#13 +
      '      c.ee_nbr = :ee_nbr AND'#13 +
      '      l.cl_e_ds_nbr = :cl_e_ds_nbr)');

    Q.Param['src_check_nbr'] := ASrcCheckNbr;
    Q.Param['ee_nbr'] := AEENbr;
    Q.Param['cl_e_ds_nbr'] := SrcCheckLines['cl_e_ds_nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      Fld.AsFloat := Fld.AsFloat - Q.Result.Fields[0].AsFloat;

    Q := TevQuery.Create(
      'SELECT deduction_shortfall_carryover'#13 +
      'FROM pr_check_lines'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check_lines>} AND'#13 +
      '  pr_check_lines_nbr = ('#13 +
      '    SELECT MAX(pr_check_lines_nbr)'#13 +
      '    FROM pr_check c, pr_check_lines l'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<c>} AND'#13 +
      '      {AsOfNow<l>} AND'#13 +
      '      l.pr_check_nbr = c.pr_check_nbr AND'#13 +
      '      c.ee_nbr = :ee_nbr AND'#13 +
      '      l.cl_e_ds_nbr = :cl_e_ds_nbr)');

    Q.Param['ee_nbr'] := AEENbr;
    Q.Param['cl_e_ds_nbr'] := SrcCheckLines['cl_e_ds_nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      Fld.AsFloat := Q.Result.Fields[0].AsFloat - Fld.AsFloat;
  end;


begin
  // Open source data
  Q := TevQuery.Create('SELECT ' + CheckLinesFieldList + ' FROM pr_check_lines WHERE pr_check_nbr = :pr_check_nbr AND {AsOfNow<pr_check_lines>}');
  Q.Param['pr_check_nbr'] := ASrcCheckNbr;
  Q.Execute;
  SrcCheckLines := Q.Result;

  while not SrcCheckLines.Eof do
  begin
    FDstCheckLines.Append;

    FDstCheckLines.Fields[1].AsInteger := ADestCheckNbr;
    FDstCheckLines.Fields[2].AsInteger := FDestPrNbr;
    FDstCheckLines.Fields[3].AsString := CHECK_LINE_AGENCY_STATUS_PENDING;

    for i := 4 to SrcCheckLines.Fields.Count - 1 do
      FDstCheckLines.Fields[i].Value := SrcCheckLines.Fields[i].Value;

    if FVoidSourcePr then
    begin
      InvertValue('HOURS_OR_PIECES');
      InvertValue('AMOUNT');
      InvertValue('E_D_DIFFERENTIAL_AMOUNT');
      CalcDeductionShortfall;
    end;

    FDstCheckLines.Post;

    CopyCheckLineLocals(SrcCheckLines.Fields[0].AsInteger, FDstCheckLines.Fields[0].AsInteger);

    SrcCheckLines.Next;
  end;
end;

procedure TevCopyPayrollHelper.CopyCheckLocals(const ASrcCheckNbr, ADestCheckNbr: Integer);
var
  Q: IevQuery;
  SrcCheckLocals: IevDataSet;
  i: Integer;

  procedure InvertValue(const FildName: String);
  var
    Fld: TField;
  begin
    Fld := FDstCheckLocals.FieldByName(FildName);
    if not Fld.IsNull then
      Fld.AsFloat := - Fld.AsFloat;
  end;

  procedure CalcShortfall;
  var
    Q: IevQuery;
    Fld: TField;
  begin
    Fld := FDstCheckLocals.FieldByName('LOCAL_SHORTFALL');

    Q := TevQuery.Create(
      'SELECT local_shortfall'#13 +
      'FROM pr_check_locals'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check_locals>} AND'#13 +
      '  pr_check_locals_nbr = ('#13 +
      '    SELECT MAX(pr_check_locals_nbr)'#13 +
      '    FROM pr_check_locals'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<pr_check_locals>} AND'#13 +      
      '      pr_check_locals_nbr < :src_check_locals_nbr AND'#13 +
      '      ee_locals_nbr = :ee_locals_nbr)');

    Q.Param['src_check_locals_nbr'] := SrcCheckLocals['pr_check_locals_nbr'];
    Q.Param['ee_locals_nbr'] := SrcCheckLocals['ee_locals_nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      Fld.AsFloat := Fld.AsFloat - Q.Result.Fields[0].AsFloat;

    Q := TevQuery.Create(
      'SELECT local_shortfall'#13 +
      'FROM pr_check_locals'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check_locals>} AND'#13 +
      '  pr_check_locals_nbr = ('#13 +
      '    SELECT MAX(pr_check_locals_nbr)'#13 +
      '    FROM pr_check_locals'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<pr_check_locals>} AND'#13 +      
      '      ee_locals_nbr = :ee_locals_nbr)');

    Q.Param['ee_states_nbr'] := SrcCheckLocals['ee_locals_nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      Fld.AsFloat := Q.Result.Fields[0].AsFloat - Fld.AsFloat;
  end;

begin
  // Open source data
  Q := TevQuery.Create('SELECT ' + CheckLocalsFieldList + ' FROM pr_check_locals WHERE pr_check_nbr = :pr_check_nbr AND {AsOfNow<pr_check_locals>}');
  Q.Param['pr_check_nbr'] := ASrcCheckNbr;
  Q.Execute;
  SrcCheckLocals := Q.Result;

  while not SrcCheckLocals.Eof do
  begin
    FDstCheckLocals.Append;

    FDstCheckLocals.Fields[1].AsInteger := ADestCheckNbr;
    FDstCheckLocals.Fields[2].AsInteger := FDestPrNbr;

    for i := 3 to SrcCheckLocals.Fields.Count - 1 do
      FDstCheckLocals.Fields[i].Value := SrcCheckLocals.Fields[i].Value;

    if FVoidSourcePr then
    begin
      InvertValue('LOCAL_TAXABLE_WAGE');
      InvertValue('LOCAL_GROSS_WAGES');
      InvertValue('LOCAL_TAX');
      InvertValue('OVERRIDE_AMOUNT');

      CalcShortfall;
    end;

    FDstCheckLocals.Post;

    SrcCheckLocals.Next;
  end;
end;

procedure TevCopyPayrollHelper.CopyChecks(const ASrcBatchNbr, ADestBatchNbr: Integer);
var
  SrcChecks: IevTable;
  i: Integer;

  procedure InvertValue(const FildName: String);
  var
    Fld: TField;
  begin
    Fld := FDstChecks.FieldByName(FildName);
    if not Fld.IsNull then
      Fld.AsFloat := - Fld.AsFloat;
  end;

  procedure CalcFederalShortfall;
  var
    Q: IevQuery;
    Fld: Tfield;
  begin
    Fld := FDstChecks.FieldByName('federal_shortfall');

    Q := TevQuery.Create(
      'SELECT federal_shortfall'#13 +
      'FROM pr_check'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check>} AND'#13 +
      '  pr_check_nbr = ('#13 +
      '    SELECT MAX(pr_check_nbr)'#13 +
      '    FROM pr_check'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<pr_check>} AND'#13 +
      '      pr_check_nbr < :src_check_nbr AND'#13 +
      '      ee_nbr = :ee_nbr)');

    Q.Param['src_check_nbr'] := SrcChecks['Pr_Check_Nbr'];
    Q.Param['ee_nbr'] := SrcChecks['Ee_Nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      Fld.AsFloat := Fld.AsFloat - Q.Result.Fields[0].AsFloat;

    Q := TevQuery.Create(
      'SELECT federal_shortfall'#13 +
      'FROM pr_check'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check>} AND'#13 +
      '  pr_check_nbr = ('#13 +
      '    SELECT MAX(pr_check_nbr)'#13 +
      '    FROM pr_check'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<pr_check>} AND'#13 +
      '      ee_nbr = :ee_nbr)');

    Q.Param['ee_nbr'] := SrcChecks['Ee_Nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      Fld.AsFloat := Q.Result.Fields[0].AsFloat - Fld.AsFloat;
  end;

begin
  SrcChecks := TevTable.Create('pr_check', ChecksFieldList,
    Format('pr_batch_nbr = %d AND check_status <> ''%s''', [ASrcBatchNbr, PAYROLL_STATUS_VOIDED]),
    SysDate);
  SrcChecks.Open;

  SrcChecks.First;
  while not SrcChecks.Eof do
  begin
    FDstChecks.Append;

    FDstChecks.Fields[1].AsInteger := FDestPrNbr;
    FDstChecks.Fields[2].AsInteger := ADestBatchNbr;
    if FVoidSourcePr then
      FDstChecks.Fields[3].AsInteger := SrcChecks.Fields[3].Value
    else
      FDstChecks.Fields[3].AsInteger := ctx_PayrollCalculation.GetNextPaymentSerialNbr;

    for i := 5 to SrcChecks.Fields.Count - 1 do
      FDstChecks.Fields[i].Value := SrcChecks.Fields[i].Value;

    if not SrcChecks.FieldByName('notes_nbr').IsNull then
      FDstChecks.UpdateBlobData('notes_nbr', SrcChecks.GetBlobData('notes_nbr'));

    if not FVoidSourcePr then
      FDstChecks['Check_Status'] := CHECK_STATUS_OUTSTANDING
    else
    begin
      FDstChecks['Check_Status'] := CHECK_STATUS_VOID;
      FDstChecks['Status_Change_Date'] := SysTime;

      InvertValue('GROSS_WAGES');
      InvertValue('NET_WAGES');
      InvertValue('FEDERAL_TAXABLE_WAGES');
      InvertValue('FEDERAL_GROSS_WAGES');
      InvertValue('FEDERAL_TAX');
      InvertValue('EE_OASDI_TAXABLE_WAGES');
      InvertValue('EE_OASDI_GROSS_WAGES');
      InvertValue('EE_OASDI_TAXABLE_TIPS');
      InvertValue('EE_OASDI_TAX');
      InvertValue('EE_MEDICARE_TAXABLE_WAGES');
      InvertValue('EE_MEDICARE_GROSS_WAGES');
      InvertValue('EE_MEDICARE_TAX');
      InvertValue('EE_EIC_TAX');
      InvertValue('ER_OASDI_TAXABLE_WAGES');
      InvertValue('ER_OASDI_GROSS_WAGES');
      InvertValue('ER_OASDI_TAXABLE_TIPS');
      InvertValue('ER_OASDI_TAX');
      InvertValue('ER_MEDICARE_TAXABLE_WAGES');
      InvertValue('ER_MEDICARE_GROSS_WAGES');
      InvertValue('ER_MEDICARE_TAX');
      InvertValue('ER_FUI_TAXABLE_WAGES');
      InvertValue('ER_FUI_GROSS_WAGES');
      InvertValue('ER_FUI_TAX');
      InvertValue('OR_CHECK_FEDERAL_VALUE');
      InvertValue('OR_CHECK_OASDI');
      InvertValue('OR_CHECK_MEDICARE');
      InvertValue('OR_CHECK_EIC');
      InvertValue('OR_CHECK_BACK_UP_WITHHOLDING');
      CalcFederalShortfall;

      if (SrcChecks['Check_Type'] <> CHECK_TYPE2_MANUAL) and
         (SrcChecks['Check_Type'] <> CHECK_TYPE2_3RD_PARTY) then
      begin
        //Mark source check as voided
        SrcChecks.Edit;
        SrcChecks['Check_Status'] := CHECK_STATUS_VOID;
        SrcChecks['Status_Change_Date'] := SysTime;
        SrcChecks.Post;

        FDstChecks['Check_Type'] := CHECK_TYPE2_VOID;
      end;
    end;

    FDstChecks.Post;

    CopyCheckLines(SrcChecks.Fields[0].AsInteger, FDstChecks.Fields[0].AsInteger, SrcChecks.Fields[5].AsInteger);
    CopyCheckStates(SrcChecks.Fields[0].AsInteger, FDstChecks.Fields[0].AsInteger);
    CopyCheckSUIs(SrcChecks.Fields[0].AsInteger, FDstChecks.Fields[0].AsInteger);
    CopyCheckLocals(SrcChecks.Fields[0].AsInteger, FDstChecks.Fields[0].AsInteger);

    SrcChecks.Next;
  end;

  SrcChecks.SaveChanges;
end;

procedure TevCopyPayrollHelper.CopyCheckStates(const ASrcCheckNbr, ADestCheckNbr: Integer);
var
  Q: IevQuery;
  SrcCheckStates: IevDataSet;
  i: Integer;

  procedure InvertValue(const FildName: String);
  var
    Fld: TField;
  begin
    Fld := FDstCheckStates.FieldByName(FildName);
    if not Fld.IsNull then
      Fld.AsFloat := - Fld.AsFloat;
  end;

  procedure CalcShortfall;
  var
    Q: IevQuery;
    FldState, FldSDI: Tfield;
  begin
    FldState := FDstCheckStates.FieldByName('STATE_SHORTFALL');
    FldSDI := FDstCheckStates.FieldByName('EE_SDI_SHORTFALL');

    Q := TevQuery.Create(
      'SELECT state_shortfall, ee_sdi_shortfall'#13 +
      'FROM pr_check_states'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check_states>} AND'#13 +
      '  pr_check_states_nbr = ('#13 +
      '    SELECT MAX(pr_check_states_nbr)'#13 +
      '    FROM pr_check_states'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<pr_check_states>} AND'#13 +
      '      pr_check_states_nbr < :src_check_states_nbr AND'#13 +
      '      ee_states_nbr = :ee_states_nbr)');

    Q.Param['src_check_states_nbr'] := SrcCheckStates['pr_check_states_nbr'];
    Q.Param['ee_states_nbr'] := SrcCheckStates['ee_states_nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      FldState.AsFloat := FldState.AsFloat - Q.Result.Fields[0].AsFloat;

    if not Q.Result.Fields[1].IsNull then
      FldSDI.AsFloat := FldSDI.AsFloat - Q.Result.Fields[1].AsFloat;

    Q := TevQuery.Create(
      'SELECT state_shortfall, ee_sdi_shortfall'#13 +
      'FROM pr_check_states'#13 +
      'WHERE'#13 +
      '  {AsOfNow<pr_check_states>} AND'#13 +
      '  pr_check_states_nbr = ('#13 +
      '    SELECT MAX(pr_check_states_nbr)'#13 +
      '    FROM pr_check_states'#13 +
      '    WHERE'#13 +
      '      {AsOfNow<pr_check_states>} AND'#13 +
      '      ee_states_nbr = :ee_states_nbr)');

    Q.Param['ee_states_nbr'] := SrcCheckStates['ee_states_nbr'];
    Q.Execute;

    if not Q.Result.Fields[0].IsNull then
      FldState.AsFloat := Q.Result.Fields[0].AsFloat - FldState.AsFloat;

    if not Q.Result.Fields[1].IsNull then
      FldSDI.AsFloat := Q.Result.Fields[1].AsFloat - FldSDI.AsFloat;
  end;

begin
  // Open source data
  Q := TevQuery.Create('SELECT ' + CheckStatesFieldList + ' FROM pr_check_states WHERE pr_check_nbr = :pr_check_nbr AND {AsOfNow<pr_check_states>}');
  Q.Param['pr_check_nbr'] := ASrcCheckNbr;
  Q.Execute;
  SrcCheckStates := Q.Result;

  while not SrcCheckStates.Eof do
  begin
     FDstCheckStates.Append;

    FDstCheckStates.Fields[1].AsInteger := ADestCheckNbr;
    FDstCheckStates.Fields[2].AsInteger := FDestPrNbr;

    for i := 3 to SrcCheckStates.Fields.Count - 1 do
      FDstCheckStates.Fields[i].Value := SrcCheckStates.Fields[i].Value;

    if FVoidSourcePr then
    begin
      InvertValue('STATE_TAXABLE_WAGES');
      InvertValue('STATE_GROSS_WAGES');
      InvertValue('STATE_TAX');
      InvertValue('EE_SDI_TAXABLE_WAGES');
      InvertValue('EE_SDI_GROSS_WAGES');
      InvertValue('EE_SDI_TAX');
      InvertValue('ER_SDI_TAXABLE_WAGES');
      InvertValue('ER_SDI_GROSS_WAGES');
      InvertValue('ER_SDI_TAX');
      InvertValue('EE_SDI_OVERRIDE');
      InvertValue('STATE_OVERRIDE_VALUE');

      CalcShortfall;
    end;

    FDstCheckStates.Post;

    SrcCheckStates.Next;
  end;
end;

procedure TevCopyPayrollHelper.CopyCheckSUIs(const ASrcCheckNbr, ADestCheckNbr: Integer);
var
  Q: IevQuery;
  SrcCheckSUIs: IevDataSet;
  i: Integer;

  procedure InvertValue(const FildName: String);
  var
    Fld: TField;
  begin
    Fld := FDstCheckSUIs.FieldByName(FildName);
    if not Fld.IsNull then
      Fld.AsFloat := - Fld.AsFloat;
  end;

begin
  // Open source data
  Q := TevQuery.Create('SELECT ' + CheckSUIsFieldList + ' FROM pr_check_sui WHERE pr_check_nbr = :pr_check_nbr AND {AsOfNow<pr_check_sui>}');
  Q.Param['pr_check_nbr'] := ASrcCheckNbr;
  Q.Execute;
  SrcCheckSUIs := Q.Result;

  while not SrcCheckSUIs.Eof do
  begin
    FDstCheckSUIs.Append;

    FDstCheckSUIs.Fields[1].AsInteger := ADestCheckNbr;
    FDstCheckSUIs.Fields[2].AsInteger := FDestPrNbr;

    for i := 3 to SrcCheckSUIs.Fields.Count - 1 do
      FDstCheckSUIs.Fields[i].Value := SrcCheckSUIs.Fields[i].Value;

    if FVoidSourcePr then
    begin
      InvertValue('SUI_TAXABLE_WAGES');
      InvertValue('SUI_GROSS_WAGES');
      InvertValue('SUI_TAX');
      InvertValue('OVERRIDE_AMOUNT');
    end;

    FDstCheckSUIs.Post;

    SrcCheckSUIs.Next;
  end;
end;

procedure TevCopyPayrollHelper.Execute(const ASrcPrNbr, ADestPrNbr: Integer; const AVoidSourcePr: Boolean);
begin
  FDestPrNbr := ADestPrNbr;
  FVoidSourcePr := AVoidSourcePr;
  CopyBatches(ASrcPrNbr);
end;

end.
