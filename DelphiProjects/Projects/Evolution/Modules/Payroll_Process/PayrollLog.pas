unit PayrollLog;

interface

uses
  Classes;

type
  TPayrollLog = class(TObject)
  private
    FStringList: TStringList;
    function GetText: string;
  protected
    property StringList: TStringList read FStringList write FStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(Msg: string);
    property Text: string read GetText;
  end;

implementation

uses SysUtils;

constructor TPayrollLog.Create;
begin
  inherited Create;
  FStringList := TStringList.Create;
end;

destructor TPayrollLog.Destroy;
begin
  FreeAndNil(FStringList);
  inherited Destroy;
end;

procedure TPayrollLog.Add(Msg: string);
begin
  StringList.Add(FormatDateTime('mm"/"dd"/"yyyy" "hh":"nn":"ss   "' + Msg + '"', Now));
end;

function TPayrollLog.GetText: string;
begin
  Result := StringList.Text;
end;

end.

