// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDueDateCalculation;

interface

uses
  SysUtils, Db, Classes, DateUtils, Math,
  Variants, EvBasicUtils, EvContext, isBasicUtils, EvCommonInterfaces, EvDataSet,
  EvExceptions, EvUtils, SDataStructure, EvConsts, SPD_ProcessException,
  SMiscCheckProcedures, EvTypes, EvDataAccessComponents, isBaseClasses, EvClientDataSet;

type
  TevCalcDueDates = class
  private
    FedNextDayDueDate, FedNextDayEndDate : IisListOfValues;
    lCO: TevClientDataSet;
    lCO_STATES: TevClientDataSet;
    lCO_SUI: TevClientDataSet;
    lCO_LOCAL_TAX: TevClientDataSet;
    function HasFullTaxService: boolean;
    function SbSendsPaymentsForThisCompany: boolean;
    function FedFrequency: string;
    function FedFUIFrequency: string;
    function Fed945Frequency: string;
    function StateFrequency: integer;
    function LocalFrequency: integer;
    function SyLocalNbr: integer;
    function CompanyStateUsesTresholds: boolean;
    function CurrentState: string;
    function CurrentPR_NBR: integer;
    function FED_TAX_DEPOSIT_TRESHOLD: Currency;
    function PrIsBackdated: Boolean;
    function CO_NBR: integer;
    function CL_NBR: integer;
    procedure NextCompanyTaxAvailableCheckNumber(var SerialNumber: integer; var BankAccount: string; var MiscCheckType: string);
    function  CalculateFUIDueDate(FUITaxDepositFrequency: string; CheckDate: TDateTime): TDateTime;
    function  CalculateFUIDueDate2(CheckDate: TDateTime): TDateTime;
    function  PayrollExcludedFromTaxDeposit: boolean;

  public
    procedure Calculate(const MainPrNbr: Integer; const MainCheckDate : TDateTime;
                         var Warnings: String;
                         const exceptPrNbr: integer = -1;
                         const CurrentCoNbr : integer = 0;
                         const CalcTaxType : Char = '0';
                         const sStateFilter : String = '');
  end;

implementation

uses SDataDictclient;

const
  FUIThreshold: Currency = 500;
  Fed941FilterWithCobra    = '(TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_ER_FUI + '''  and TAX_TYPE <> '''+ TAX_LIABILITY_TYPE_FEDERAL_945+ ''' and TAX_TYPE <> '''+ TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING+ ''')';
  Fed941FilterWithoutCobra = '(TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_ER_FUI + '''  and TAX_TYPE <> '''+ TAX_LIABILITY_TYPE_FEDERAL_945+ ''' and TAX_TYPE <> '''+ TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING+ ''' and TAX_TYPE <> ''' + TAX_LIABILITY_TYPE_COBRA_CREDIT + ''')';
  Fed940Filter = 'TAX_TYPE = ''' + TAX_LIABILITY_TYPE_ER_FUI + '''';
  Fed945Filter = '(TAX_TYPE= '''+ TAX_LIABILITY_TYPE_FEDERAL_945+ ''' or TAX_TYPE = '''+ TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING+ ''')';


function TevCalcDueDates.HasFullTaxService: boolean;
begin
  Result := (DM_PAYROLL.PR.CHECK_DATE.AsDateTime >= DM_COMPANY.CO.TAX_SERVICE_START_DATE.AsDateTime)
    and ((DM_PAYROLL.PR.CHECK_DATE.AsDateTime < DM_COMPANY.CO.TAX_SERVICE_END_DATE.AsDateTime)
      or (DM_COMPANY.CO.TAX_SERVICE_END_DATE.IsNull))
    and (DM_COMPANY.CO.TAX_SERVICE.AsString = GROUP_BOX_YES);
end;

function TevCalcDueDates.SbSendsPaymentsForThisCompany: boolean;
begin
  if lCo.FindKey([DM_COMPANY.CO['CO_NBR']]) then
    result := SbSendsTaxPaymentsForCompany(lCO.FieldByName('TAX_SERVICE').AsString)
  else
    result := SbSendsTaxPaymentsForCompany(DM_COMPANY.CO.FieldByName('TAX_SERVICE').AsString);
end;

function TevCalcDueDates.FedFrequency: string;
begin
  if lCo.FindKey([DM_COMPANY.CO['CO_NBR']]) then
    result := lCO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString
  else
    result := DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString;
end;

function TevCalcDueDates.FedFUIFrequency: string;
begin
  if lCo.FindKey([DM_COMPANY.CO['CO_NBR']]) then
    result := lCO.FieldByName('FUI_TAX_DEPOSIT_FREQUENCY').AsString
  else
    result := DM_COMPANY.CO.FUI_TAX_DEPOSIT_FREQUENCY.AsString;
end;

function TevCalcDueDates.Fed945Frequency: string;
begin
  if lCo.FindKey([DM_COMPANY.CO['CO_NBR']]) then
    result := lCO.FieldByName('FED_945_TAX_DEPOSIT_FREQUENCY').AsString
  else
    result := DM_COMPANY.CO.FieldByName('FED_945_TAX_DEPOSIT_FREQUENCY').AsString;
end;

function TevCalcDueDates.StateFrequency: integer;
begin
  if lCO_STATES.FindKey([DM_COMPANY.CO_STATES['CO_STATES_NBR']]) then
    result := lCO_STATES.FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').AsInteger
  else
    result := DM_COMPANY.CO_STATES.FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').AsInteger;
end;

function TevCalcDueDates.LocalFrequency: integer;
begin
  if lCO_LOCAL_TAX.FindKey([DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']]) then
    result := lCO_LOCAL_TAX.FieldByName('SY_LOCAL_DEPOSIT_FREQ_NBR').AsInteger
  else
    result := DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCAL_DEPOSIT_FREQ_NBR').AsInteger;
end;

function TevCalcDueDates.SyLocalNbr: integer;
begin
  result := DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').AsInteger;
end;

function TevCalcDueDates.CompanyStateUsesTresholds: boolean;
begin
  result := not (DM_COMPANY.CO_STATES.FieldByName('IGNORE_STATE_TAX_DEP_THRESHOLD').AsString = 'Y');
end;

function TevCalcDueDates.CurrentState: string;
begin
  result := DM_COMPANY.CO_STATES.FieldByName('STATE').AsString;
end;

function TevCalcDueDates.CurrentPR_NBR: integer;
begin
  result := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
end;

function TevCalcDueDates.FED_TAX_DEPOSIT_TRESHOLD: Currency;
begin
  result := 100000.00;
end;

function TevCalcDueDates.PrIsBackdated: Boolean;
begin
  result := (DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime <= Today);
end;

function TevCalcDueDates.CO_NBR: integer;
begin
  result := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
end;

function TevCalcDueDates.CL_NBR: integer;
begin
  result := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
end;

procedure TevCalcDueDates.NextCompanyTaxAvailableCheckNumber(var SerialNumber: integer; var BankAccount: string; var MiscCheckType: string);
begin
  if HasFullTaxService then
    NextCompanyAvailableCheckNumber('TAX_SB_BANK_ACCOUNT_NBR', SerialNumber, BankAccount, MiscCheckType, True)
  else
    NextCompanyAvailableCheckNumber('TAX_CL_BANK_ACCOUNT_NBR', SerialNumber, BankAccount, MiscCheckType, True);
  SerialNumber := ctx_PayrollCalculation.GetNextPaymentSerialNbr;
end;

function TevCalcDueDates.CalculateFUIDueDate(FUITaxDepositFrequency: string; CheckDate: TDateTime): TDateTime;
begin
  if FUITaxDepositFrequency = FREQUENCY_TYPE_ANNUAL then
    // 31st of Jan of next year
    Result := EncodeDate(YearOf(CheckDate) + 1, 1, 31)
  else
    // Last day of the first month of the next quarter
    Result := GetEndMonth(GetEndQuarter(CheckDate) + 15);
end;

function TevCalcDueDates.CalculateFUIDueDate2(CheckDate: TDateTime): TDateTime;
begin
  result := GetEndMonth(GetEndQuarter(CheckDate) + 15);
end;

function TevCalcDueDates.PayrollExcludedFromTaxDeposit: boolean;
begin
  result := DM_PAYROLL.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString = 'Y';
end;


procedure TevCalcDueDates.Calculate(const MainPrNbr: Integer; const MainCheckDate : TDateTime;
                                      var Warnings: String;
                                      const exceptPrNbr: integer=-1;
                                      const CurrentCoNbr: integer = 0;
                                       const CalcTaxType: Char = '0';const sStateFilter : String = '');
const
  StandartLiabilityFilterCondition = ' and (ADJUSTMENT_TYPE = ''' + TAX_LIABILITY_ADJUSTMENT_TYPE_NONE + ''' or ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP+ ''' or ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION+ ''')';

  // we filter on check_date and if it's unpaid during DataRequired call.
  // we can't filter on ADJUSTMENT_TYPE then because we need it for non-tax
  // clients to cut a check if it is time and there are only adjusting liabilities
  // in the period. But they shouldn't be involved in due_date changing and threshhold
  // calcualtion, so these records are filtered out mostly all the time.
var
  CurrentLiabilityFilter: string;

  newcalcBeginDate, newcalcEndDate, newcalcDueDate, ActualDueDate : TDateTime;
  Fed941BeginDate, Fed941EndDate, Fed941DueDate : TDateTime;
  Fed945BeginDate,  Fed945EndDate, Fed945DueDate: TDateTime;
  NYStateEndDate, NYStateDueDate: TDateTime;

  cdSelectedCo: TevClientDataset;
  cdStateDepositNbr, cdLocalDepositNbr: TevClientDataset;
  cdFiltered: TevClientDataset;
  cdTCDLocals: IevDataSet;

  procedure LogException_Add(CL_NBR, CO_NBR: integer; ExceptionType: string; aDescription: string = ''; TableName: string = '';
    FieldName: string = ''; RecordNbr: Integer = 0; SystemException: Boolean = False);
  var
    i: Integer;
  begin
    if Warnings <> '' then
      Warnings := Warnings+ #13;
    for i := 1 to NumExceptionDescriptions do
      if ExceptionDescriptions[i].T = ExceptionType then
      begin
        Warnings := Warnings+ ExceptionDescriptions[i].D;
        if aDescription <> '' then
          Warnings := Warnings+ '; '+ aDescription;
        Break;
      end;
  end;

  procedure UpdateLiability(
    DataSet1: TevClientDataset; // required
    RangeBeginDate, RangeEndDate: TDateTime;
    PeriodBeginDate, PeriodEndDate: TDateTime;
    DueDate: TDateTime; // 0 = N/A
    PR_NBR: integer; // -1 = N/A
    NewTaxDepositId: integer = -1; // -1 = N/A
    FilterCondition: string = ''; // '' = N/A
    KeepSystemFilter: Boolean = True;
    SyStateDepFreq: Integer = -1;
    sAchPeriodEndDate : String = '';
    FedDepFreq941Date: TDateTime = 0;
    FedDepFreq945Date: TDateTime = 0);
  var
    Status: string;
    d: TevClientDataSet;
    list: IisListOfValues;
    i: Integer;
    s:String;
  begin
    Status := '';
    if NewTaxDepositId <> -1 then
      Status := TAX_DEPOSIT_STATUS_PAID;
    d := TevClientDataset.Create(nil);
    try
      d.CloneCursor(DataSet1, False);
      if not KeepSystemFilter then
        d.Filter := '';
      if FilterCondition <> '' then
        if d.Filter <> '' then
          d.Filter := d.Filter + ' and ' + FilterCondition
        else
          d.Filter := FilterCondition;
      Assert(d.Filtered);
      Assert(d.IndexName = '1');
      if RangeBeginDate <> 0 then
         d.SetRange([RangeBeginDate], [RangeEndDate]);
      d.First;
      list := TisListOfValues.Create;
      while not d.Eof do
      begin
        list.AddValue(d.FieldByName(Dataset1.Name + '_NBR').AsString, d.FieldByName(Dataset1.Name + '_NBR').AsInteger);
        d.Next;
      end;

      for i := 0 to list.Count - 1 do
      begin
        if d.Locate(Dataset1.Name + '_NBR', list.Values[i].Value, []) then
        begin
          if (d.FieldByName('ADJUSTMENT_TYPE').AsString = TAX_LIABILITY_ADJUSTMENT_TYPE_NONE)
             or (d.FieldByName('ADJUSTMENT_TYPE').AsString = TAX_LIABILITY_ADJUSTMENT_TYPE_NEW_CLIENT_SETUP)
             or (d.FieldByName('ADJUSTMENT_TYPE').AsString = TAX_LIABILITY_ADJUSTMENT_TYPE_CONVRESION) then
          begin
             if (PR_NBR = -1) or ((d.FieldByName('PR_NBR').AsInteger = PR_NBR)) then
             begin
               if d.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
               begin
                  if not (Status = '') and not (d.FieldByName('STATUS').AsString = Status) then
                  begin
                    if d.State in [dsBrowse] then
                      d.Edit;
                    d.FieldByName('PREV_STATUS').AsString := d.FieldByName('STATUS').AsString;
                    d.FieldByName('STATUS').AsString := Status;
                  end;

                  if not (DueDate = 0)
                  and ((d.FieldByName('DUE_DATE').AsDateTime <> DueDate) or
                       (d.FieldByName('PERIOD_BEGIN_DATE').AsDateTime <> PeriodBeginDate) or
                       (d.FieldByName('PERIOD_END_DATE').AsDateTime <> PeriodEndDate)) then
                  begin
                    if d.State in [dsBrowse] then
                      d.Edit;
                    d.FieldByName('DUE_DATE').AsDateTime := DueDate;
                    if PeriodBeginDate <> 0 then
                      d.FieldByName('PERIOD_BEGIN_DATE').AsDateTime := PeriodBeginDate;
                    if PeriodEndDate <> 0 then
                    begin
                      s := ExtractFromFiller(d.FieldByName('FILLER').AsString , 41, 10);
                      if (s <> '') then
                      begin
                        d.FieldByName('FILLER').AsString := PutIntoFiller(d.FieldByName('FILLER').AsString, '', 41, 10);
                      end;
                      d.FieldByName('PERIOD_END_DATE').AsDateTime := PeriodEndDate;
                    end;
                    if SyStateDepFreq <> -1 then
                      d.FieldByName('FILLER').AsString := PutIntoFiller(d.FieldByName('FILLER').AsString, Format('%8u', [SyStateDepFreq]), 12, 8);
                  end;

                  if (Length(sAchPeriodEndDate) > 0)  then
                  begin
                    if d.State in [dsBrowse] then
                      d.Edit;
                    d.FieldByName('ACH_KEY').AsString := sAchPeriodEndDate
                  end;

                  if (NewTaxDepositId <> -1) then
                  begin
                    if d.State in [dsBrowse] then
                      d.Edit;
                    d.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger := NewTaxDepositId;
                  end;

                  if (FedDepFreq941Date <> 0) then
                  begin
                     if d.State in [dsBrowse] then
                       d.Edit;
                     d.FieldByName('FILLER').AsString := PutIntoFiller(d.FieldByName('FILLER').AsString, DateToStr(FedDepFreq941Date), 21, 10);
                  end;

                  if (FedDepFreq945Date <> 0) then
                  begin
                    if d.State in [dsBrowse] then
                      d.Edit;
                     d.FieldByName('FILLER').AsString := PutIntoFiller(d.FieldByName('FILLER').AsString, DateToStr(FedDepFreq945Date), 31, 10);
                  end;
                  if d.State in [dsEdit] then
                    d.Post;
               end
               else
               begin
                    if not (DueDate = 0) and (d.FieldByName('PERIOD_END_DATE').AsDateTime <> PeriodEndDate) then
                    begin
                      d.Edit;
                      d.FieldByName('FILLER').AsString := PutIntoFiller(d.FieldByName('FILLER').AsString, DateToStr(PeriodEndDate), 41, 10);
                      d.Post;
                    end;
               end;
             end;
          end;
        end;
      end;
    finally
      d.Free;
    end;
  end;

  function GetTaxLiabilities(const BeginDate, EndDate: TDateTime; const LiabilityDataSet: TevClientDataset; const DataSetFieldName: string;
    const FieldValue: Integer; const FilterCondition: string = ''; const KeepSystemFilter: Boolean = True): Currency;
  var
    d: TevClientDataset;
  begin
    Result := 0;
    if not LiabilityDataSet.Active then
      exit;
    d := TevClientDataset.Create(nil);
    with d do
    try
      CloneCursor(LiabilityDataSet, False);
      Assert(IndexName = '1');
      SetRange([BeginDate], [EndDate]);
      Assert(Filtered);
      if not KeepSystemFilter then
        Filter := '';
      if DataSetFieldName <> '' then
      begin
        if Filter <> '' then
          Filter := Filter + ' and ' + DataSetFieldName + ' = ' + IntToStr(FieldValue)
        else
          Filter := DataSetFieldName + ' = ' + IntToStr(FieldValue);
      end;
      if FilterCondition <> '' then
      begin
        if Filter <> '' then
          Filter := Filter + ' and ' + FilterCondition
        else
          Filter := FilterCondition;
      end;
      First;
      while not Eof do
      begin
        Result := Result + FieldByName('AMOUNT').AsCurrency;
        Next;
      end;
    finally
      Free;
    end;
  end;

  function GetFieldValueList(const ds : TevClientDataset; const FieldName : String;
                 const FilterCondition: string = ''; const KeepSystemFilter: Boolean = True): TStringList;
  var
    s: ShortString;
  begin
    Result := TStringList.Create;
    Result.Sorted := True;
    if not ds.Active then
      exit;
    with TevClientDataset.Create(nil) do
    try
      CloneCursor(ds, False);
      Assert(Filtered);
      if not KeepSystemFilter then
        Filter := '';
      if FilterCondition <> '' then
      begin
        if Filter <> '' then
          Filter := Filter + ' and ' + FilterCondition
        else
          Filter := FilterCondition;
      end;
      First;
      while not Eof do
      begin
        s := FieldByName(FieldName).AsString;
        if Result.IndexOf(s) = -1 then
          Result.Append(s);
        Next;
      end;
    finally
      Free;
    end;
  end;

  procedure SortDateList(const l: TStringList);
  var
    i: Integer;
    s, sDate: string;
  begin
    s := l.Text;
    l.Clear;
    l.Sorted := False;
    while s <> '' do
    begin
      sDate := GetNextStrValue(s, #13#10);
      for i := 0 to l.Count do
        if i > l.Count-1 then
          l.Append(sDate)
        else
        if StrToDateTime(sDate) < StrToDateTime(l[i]) then
        begin
          l.Insert(i, sDate);
          Break;
        end;
    end;
  end;

  procedure SetStateFrequency(aState: string; NewFrequency: integer);
  begin
    with TevClientDataset.Create(nil) do
    try
      CloneCursor(DM_COMPANY.CO_STATES, True);
      First;
      while not EOF do
      begin
        if (FieldValues['State'] = aState) and cdSelectedCo.Locate('CO_NBR', FieldValues['CO_NBR'], []) then
        begin
          Edit;
          FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').AsInteger := NewFrequency;
          Post;
        end;
        Next;
      end;
    finally
      Free;
    end;
    with TevClientDataset.Create(nil) do
    try
      CloneCursor(lCO_STATES, True);
      First;
      while not EOF do
      begin
        if (FieldValues['State'] = aState) and cdSelectedCo.Locate('CO_NBR', FieldValues['CO_NBR'], []) then
        begin
          Edit;
          FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').AsInteger := NewFrequency;
          Post;
        end;
        Next;
      end;
    finally
      Free;
    end;
  end;

  procedure SetFedFrequency(Frequency: string; sField : String);
  begin
    with TevClientDataset.Create(nil) do
    begin
      try
        CloneCursor(DM_COMPANY.CO, True);
        First;
        while not Eof do
        begin
          if cdSelectedCo.Locate('CO_NBR', FieldValues['CO_NBR'], []) then
          begin
            Edit;
            FieldByName(sField).AsString := Frequency;
            Post;
          end;
          Next;
        end;
      finally
        Free;
      end;
    end;
    with TevClientDataset.Create(nil) do
    begin
      try
        CloneCursor(lCO, True);
        First;
        while not Eof do
        begin
          if cdSelectedCo.Locate('CO_NBR', FieldValues['CO_NBR'], []) then
          begin
            Edit;
            FieldByName(sField).AsString := Frequency;
            Post;
          end;
          Next;
        end;
      finally
        Free;
      end;
    end;
  end;

  procedure FillCdSelectedCo(const AdditTable, Condition1, Condition2, ExtraFields: string);
  begin
    if Assigned(cdSelectedCo) then
    begin
      if cdSelectedCo.Active then
      begin
        cdSelectedCo.Close;
        cdSelectedCo.IndexFieldNames := '';
        cdSelectedCo.IndexDefs.Clear;
      end
    end
    else
      cdSelectedCo := TevClientDataset.Create(nil);
    with ctx_DataAccess.CUSTOM_VIEW do
    try
      Close;
      with TExecDSWrapper.Create('CustomCompanySelect') do
      begin
        SetMacro('AdditTable', AdditTable);
        SetMacro('ExtraFields', ExtraFields);

        if SbSendsPaymentsForThisCompany then
        begin // tax clients can have a few comapnies with same EIN
          SetMacro('Condition', Condition1 + Condition2);
        end
        else
        begin // non-tax clients are always considered as a single EIN companies
          SetMacro('Condition', Condition1 + ' and co.CO_NBR = ' + IntToStr(DM_COMPANY.CO['CO_NBR']));
        end;

        DataRequest(AsVariant);
      end;
      Open;
      cdSelectedCo.Data := Data;
    finally
      Close;
    end;
  end;

  procedure LoadLiabilityTable(const ds: TevClientDataset);
  var
   exceptPrFilter: String;
   beginYear, endYear : TDateTime;
  begin
    ds.Close;
    ds.Filtered := False;
    ds.Filter := '';
    beginYear := GetBeginYear(MainCheckDate);
    endYear := GetEndYear(MainCheckDate);

    exceptPrFilter:='';      //   because of procedure PayrollDelete  
    if (exceptPrNbr <> -1) then
        exceptPrFilter:=' and (PR_NBR <> ' + IntToStr(exceptPrNbr) + ') ' ;

    ds.DataRequired('THIRD_PARTY=''N'' ' + exceptPrFilter +
                         ' and (PERIOD_END_DATE is null or (PERIOD_END_DATE >= ' +
                QuotedStr(DateToStr(MainCheckDate)) + ')) and CHECK_DATE >= ' + QuotedStr(DateToStr(beginYear)) +
                                                        ' and CHECK_DATE <= ' + QuotedStr(DateToStr(endYear)));
  end;

  procedure PrepareTable(const ds: TevClientDataset);
  var
    i: Integer;
  begin
    ds.Filter := '';
    ds.Filtered := True;
    ds.IndexName := '';
    i := ds.IndexDefs.IndexOf('1');
    if i <> -1 then ds.IndexDefs.Delete(i);
    ds.IndexDefs.Add('1', 'CHECK_DATE', []);
    ds.IndexName := '1';
  end;

  procedure FilterLiabilityTable(const ds: TevClientDataset; const FieldName: string);
  var
    StandartLiabilityCalcEndDateFilter : String;
  begin
    StandartLiabilityCalcEndDateFilter := ' and (CALC_PERIOD_END_DATE is null or (CALC_PERIOD_END_DATE >= ' + QuotedStr(DateToStr(MainCheckDate)) + '))';

    CurrentLiabilityFilter := '';
    cdSelectedCo.First;
    while not cdSelectedCo.Eof do
    begin
      if CurrentLiabilityFilter <> '' then CurrentLiabilityFilter := CurrentLiabilityFilter + ',';
      CurrentLiabilityFilter := CurrentLiabilityFilter + IntToStr(cdSelectedCo[FieldName]);
      cdSelectedCo.Next;
    end;
    CurrentLiabilityFilter := FieldName + ' in (' + CurrentLiabilityFilter + ')';
    ds.Filter := CurrentLiabilityFilter + StandartLiabilityFilterCondition + StandartLiabilityCalcEndDateFilter;
  end;

  procedure UnPrepareTable(const ds: TevClientDataset);
  begin
    ds.CancelRange;
    ds.Filtered := False;
    ds.Filter := '';
    ds.IndexName := '';
    ds.IndexDefs.Delete(ds.IndexDefs.IndexOf('1'));
  end;


  function GetTaxLiabilitiesByPrNbr(const LiabilityDataSet: TevClientDataset;
                         const FilterCondition: string = ''; const KeepSystemFilter: Boolean = True): Currency;
  var
      d: TevClientDataset;
  begin
      Result := 0;
      if not LiabilityDataSet.Active then
        exit;
      d := TevClientDataset.Create(nil);
      with d do
      try
        CloneCursor(LiabilityDataSet, False);
        Assert(IndexName = '1');
        Assert(Filtered);
        if not KeepSystemFilter then
          Filter := '';
        if FilterCondition <> '' then
        begin
          if Filter <> '' then
            Filter := Filter + ' and ' + FilterCondition
          else
            Filter := FilterCondition;
        end;
        First;
        while not Eof do
        begin
          Result := Result + FieldByName('AMOUNT').AsCurrency;
          Next;
        end;
      finally
        Free;
      end;
  end;

  Procedure SetFilter(const ds: TevClientDataset; const Filter : String = '1=1');
  Begin
        if Length(ds.Filter) > 0 then
          ds.Filter := ds.Filter + ' and ' + Filter
        else
          ds.Filter := Filter;
        ds.Filtered := True;
  end;

  Procedure GetPrNbrList(const cdLiabList  : TevClientDataset; var calcPrList : String; var ControlMainPr : Boolean);
  var
    PrNbrList : TStringList;
  begin
     PrNbrList := GetFieldValueList(cdLiabList,'PrNbr','CheckDate = '+ QuotedStr(DateToStr(cdLiabList.fieldByName('CheckDate').AsDateTime)));
     try
       calcPrList := calcPrList + ',' + PrNbrList.CommaText;
       if PrNbrList.IndexOf(IntToStr(MainPrNbr)) <> -1 then
          ControlMainPr := True;
     Finally
         PrNbrList.Free;
     end;
  end;

  Procedure SetCurrentDates(const beginDate,endDate,dueDate : TDateTime);
  begin
     newcalcDueDate := dueDate;
     newcalcEndDate := endDate;
     newcalcBeginDate := beginDate;
  end;

  procedure DoFederalTaxClient;
  var
    cdLiabList: TevClientDataset;
    prevCheckDate, calcBeginDate, calcEndDate, calcDueDate : TDateTime;
    PeriodList : TStringList;
    calcPrList : String;
    LiabTotal: Currency;
    ControlMainPr : Boolean;
    SY_GLOBAL_AGENCY_NBR: Integer;
    iTaxPeriod: integer;
    tmpFedDepFreq941Date, FDepFreq941ChangedDate: TDateTime;
    tmpFedDepFreq945Date, FDepFreq945ChangedDate: TDateTime;
    tmpFilter:String;

    s:String;

    Procedure SetVariables;
    begin
       ControlMainPr := false;
       calcPrList := '-1';
       calcDueDate := 0;
       calcEndDate := 0;
    end;

    function GetFed941FreqAsOfCheckDate(CheckDate: TDateTime): string;
    begin
      Result:= FedFrequency[1];
      if (FDepFreq941ChangedDate <> 0) and (FDepFreq941ChangedDate > CheckDate) then
          Result := FREQUENCY_TYPE_MONTHLY;
    end;

    function GetFed945FreqAsOfCheckDate(CheckDate: TDateTime): string;
    begin
      Result:= Fed945Frequency[1];
      if (FDepFreq945ChangedDate <> 0) and (FDepFreq945ChangedDate > CheckDate) then
          Result := FREQUENCY_TYPE_MONTHLY;
    end;


  begin

    SY_GLOBAL_AGENCY_NBR := ConvertNull(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR'), 0);
    ctx_PayrollCalculation.GetFedTaxDepositPeriod(FedFrequency[1], MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);

    if (FedFrequency = FREQUENCY_TYPE_SEMI_WEEKLY) and PrIsBackdated then
        LogException_Add(CL_NBR, CO_NBR, PE_FED_CHECK_DUE_DATE);

  //    Federal 941  section..

    tmpFilter:=DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter;

    SetFilter(DM_COMPANY.CO_FED_TAX_LIABILITIES, Fed941FilterWithCobra);
    try
      if DM_COMPANY.CO_FED_TAX_LIABILITIES.RecordCount > 0 then
      begin

          cdLiabList := TevClientDataSet.Create(nil);
          try
            cdLiabList.FieldDefs.Add('PrNbr', ftInteger, 0, True);
            cdLiabList.FieldDefs.Add('CheckDate', ftDateTime, 0, True);
            cdLiabList.FieldDefs.Add('BeginDate', ftDateTime, 0, True);
            cdLiabList.CreateDataSet;
            cdLiabList.LogChanges := False;

            cdLiabList.AddIndex('idxCheckDate', 'CheckDate', []);
            cdLiabList.IndexDefs.Update;
            cdLiabList.IndexName := 'idxCheckDate';

            tmpFedDepFreq941Date:=0;
            FDepFreq941ChangedDate := 0;

            DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
            while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
            begin
              s := ExtractFromFiller(DM_COMPANY.CO_FED_TAX_LIABILITIES.fieldByName('FILLER').AsString , 21, 10);
              if s <> '' then
              begin
                 FDepFreq941ChangedDate :=  StrToDate(s);
                 break;
              end;

              DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
            end;

            DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
            while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
            begin
               if not cdLiabList.Locate('CheckDate;PrNbr',
                               VarArrayOf([DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                           DM_COMPANY.CO_FED_TAX_LIABILITIES.PR_NBR.AsInteger]), []) then
               begin
                  ctx_PayrollCalculation.GetFedTaxDepositPeriod(GetFed941FreqAsOfCheckDate(DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime)[1],
                                                          DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                                           calcBeginDate, calcEndDate, calcDueDate);

                  cdLiabList.AppendRecord([DM_COMPANY.CO_FED_TAX_LIABILITIES.PR_NBR.AsInteger,
                                            DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                            calcBeginDate]);
               end;
               DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
            end;

            PeriodList := cdLiabList.GetFieldValueList('BeginDate',True);
            try
               if PeriodList.Count > 0 then
               begin
                 for iTaxPeriod := 0 to PeriodList.Count - 1 do
                 begin
                   cdLiabList.Filter :='BeginDate = '+ QuotedStr(PeriodList[iTaxPeriod]);
                   cdLiabList.Filtered := true;

                   if cdLiabList.RecordCount > 0 then
                   begin
                          prevCheckDate := 0;
                          SetVariables;

                          cdLiabList.First;
                          while not cdLiabList.Eof do
                          begin
                            if prevCheckDate <> cdLiabList.fieldByName('CheckDate').AsDateTime then
                            begin
                                 prevCheckDate := cdLiabList.fieldByName('CheckDate').AsDateTime;
                                 if (calcEndDate < cdLiabList.fieldByName('CheckDate').AsDateTime) and (calcEndDate <> 0 ) then
                                 begin
                                     UpdateLiability(DM_COMPANY.CO_FED_TAX_LIABILITIES, 0,0, calcBeginDate, calcEndDate,
                                            calcDueDate, -1, -1, ' PR_NBR in ( ' +calcPrList + ') and ' + Fed941FilterWithCobra,
                                            True,-1,'', tmpFedDepFreq941Date);
                                    SetVariables;
                                 end;

                                 ctx_PayrollCalculation.GetFedTaxDepositPeriod(GetFed941FreqAsOfCheckDate(cdLiabList.fieldByName('CheckDate').AsDateTime)[1],
                                                         cdLiabList.fieldByName('CheckDate').AsDateTime,
                                                          calcBeginDate, calcEndDate, calcDueDate);

                                 GetPrNbrList(cdLiabList, calcPrList, ControlMainPr);

                                 LiabTotal :=  GetTaxLiabilitiesByPrNbr(DM_COMPANY.CO_FED_TAX_LIABILITIES,
                                                      ' PR_NBR in ( ' + calcPrList + ')' + ' and ' + Fed941FilterWithoutCobra);

                                 if LiabTotal >= FED_TAX_DEPOSIT_TRESHOLD then
                                 begin
                                   if ControlMainPr then
                                      LogException_Add(CL_NBR, CO_NBR, PE_FED_100000_TRESHOLD_MET);
                                   if ControlMainPr and (FedFrequency[1] = FREQUENCY_TYPE_MONTHLY) then
                                   begin
                                       tmpFedDepFreq941Date:= cdLiabList.fieldByName('CheckDate').AsDateTime + 1;
                                       SetFedFrequency(FREQUENCY_TYPE_SEMI_WEEKLY,'FEDERAL_TAX_DEPOSIT_FREQUENCY');
                                       if DM_COMPANY.CO_STATES.Locate('CO_NBR;STATE', VarArrayOf([CO_NBR, 'CA']), []) then
                                         if (StateFrequency = 201) {Monthly-Fed File}
                                         or (StateFrequency = 172) {Qtrly-Fed File} then
                                           SetStateFrequency('CA', 204); {Semi-Weekly-Fed File}
                                   end;
                                   calcDueDate := ctx_PayrollCalculation.CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 1, cdLiabList.fieldByName('CheckDate').AsDateTime + 1, cdLiabList.fieldByName('CheckDate').AsDateTime); // bump it to next banking day
                                   calcEndDate := cdLiabList.fieldByName('CheckDate').AsDateTime;

                                   FedNextDayDueDate.AddValue(cdLiabList.fieldByName('CheckDate').AsString, calcDueDate);
                                   FedNextDayEndDate.AddValue(cdLiabList.fieldByName('CheckDate').AsString, calcEndDate);
                                 end;

                                 if ControlMainPr then
                                 begin
                                  Fed941DueDate := calcDueDate;
                                  Fed941EndDate := calcEndDate;
                                  Fed941BeginDate := calcBeginDate;
                                 end;

                            end;
                            cdLiabList.Next;
                          end;
                          if calcEndDate <> 0 then
                               UpdateLiability(DM_COMPANY.CO_FED_TAX_LIABILITIES, 0,0,calcBeginDate, calcEndDate,
                                       calcDueDate, -1, -1, ' PR_NBR in ( ' +calcPrList + ') and ' + Fed941FilterWithCobra,
                                       True,-1,'', tmpFedDepFreq941Date);
                   end;
                 end;
               end;
            finally
             PeriodList.free;
            end;
          finally
           cdLiabList.Free;
          end;
      end;
    finally
       DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := tmpFilter;
    end;

  //    Federal 945  section..

    SetFilter(DM_COMPANY.CO_FED_TAX_LIABILITIES, Fed945Filter);

    if DM_COMPANY.CO_FED_TAX_LIABILITIES.RecordCount > 0 then
    begin
        cdLiabList := TevClientDataSet.Create(nil);
        try
          cdLiabList.FieldDefs.Add('PrNbr', ftInteger, 0, True);
          cdLiabList.FieldDefs.Add('CheckDate', ftDateTime, 0, True);
          cdLiabList.FieldDefs.Add('BeginDate', ftDateTime, 0, True);
          cdLiabList.CreateDataSet;
          cdLiabList.LogChanges := False;

          cdLiabList.AddIndex('idxCheckDate', 'CheckDate', []);
          cdLiabList.IndexDefs.Update;
          cdLiabList.IndexName := 'idxCheckDate';

          tmpFedDepFreq945Date:=0;
          FDepFreq945ChangedDate := 0;

          DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
          while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
          begin
            s := ExtractFromFiller(DM_COMPANY.CO_FED_TAX_LIABILITIES.fieldByName('FILLER').AsString , 31, 10);
            if s <> '' then
            begin
               FDepFreq945ChangedDate :=  StrToDate(s);
               break;
            end;
            DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
          end;


          DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
          while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
          begin
           if not cdLiabList.Locate('CheckDate;PrNbr',
                           VarArrayOf([DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                       DM_COMPANY.CO_FED_TAX_LIABILITIES.PR_NBR.AsInteger]), []) then
           begin

             ctx_PayrollCalculation.GetFedTaxDepositPeriod(GetFed945FreqAsOfCheckDate(DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime)[1],
                                                            DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                                             calcBeginDate, calcEndDate, calcDueDate);

             cdLiabList.AppendRecord([DM_COMPANY.CO_FED_TAX_LIABILITIES.PR_NBR.AsInteger,
                                      DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                      calcBeginDate]);
           end;
            DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
          end;

          PeriodList := cdLiabList.GetFieldValueList('BeginDate',True);
          try
             if PeriodList.Count > 0 then
             begin
               for iTaxPeriod := 0 to PeriodList.Count - 1 do
               begin
                 cdLiabList.Filter :='BeginDate = '+ QuotedStr(PeriodList[iTaxPeriod]);
                 cdLiabList.Filtered := true;

                 if cdLiabList.RecordCount > 0 then
                 begin
                        SetVariables;
                        prevCheckDate := 0;

                        cdLiabList.First;
                        while not cdLiabList.Eof do
                        begin
                           if prevCheckDate <> cdLiabList.fieldByName('CheckDate').AsDateTime then
                           begin
                             prevCheckDate := cdLiabList.fieldByName('CheckDate').AsDateTime;
                             if (calcEndDate < cdLiabList.fieldByName('CheckDate').AsDateTime) and (calcEndDate <> 0 ) then
                             begin
                                 UpdateLiability(DM_COMPANY.CO_FED_TAX_LIABILITIES, 0,0, calcBeginDate, calcEndDate,
                                        calcDueDate, -1, -1, ' PR_NBR in ( ' +calcPrList + ') and ' + Fed945Filter,
                                          True,-1,'', 0, tmpFedDepFreq945Date);
                                 SetVariables;
                             end;
                             ctx_PayrollCalculation.GetFedTaxDepositPeriod(GetFed945FreqAsOfCheckDate(cdLiabList.fieldByName('CheckDate').AsDateTime)[1],
                                                                          cdLiabList.fieldByName('CheckDate').AsDateTime,
                                                                           calcBeginDate, calcEndDate, calcDueDate);

                             GetPrNbrList(cdLiabList, calcPrList, ControlMainPr);

                             LiabTotal := GetTaxLiabilitiesByPrNbr(DM_COMPANY.CO_FED_TAX_LIABILITIES,
                                                          ' PR_NBR in ( ' +calcPrList + ')' + ' and ' + Fed945Filter);

                             if LiabTotal >= FED_TAX_DEPOSIT_TRESHOLD then
                             begin
                               if ControlMainPr then
                                  LogException_Add(CL_NBR, CO_NBR, PE_FED_100000_TRESHOLD_MET);
                               if ControlMainPr and (Fed945Frequency[1] = FREQUENCY_TYPE_MONTHLY) then
                               begin
                                 tmpFedDepFreq945Date:= cdLiabList.fieldByName('CheckDate').AsDateTime + 1;
                                 SetFedFrequency(FREQUENCY_TYPE_SEMI_WEEKLY,'FED_945_TAX_DEPOSIT_FREQUENCY');
                                 if DM_COMPANY.CO_STATES.Locate('CO_NBR;STATE', VarArrayOf([CO_NBR, 'CA']), []) then
                                   if (StateFrequency = 201) {Monthly-Fed File}
                                   or (StateFrequency = 172) {Qtrly-Fed File} then
                                     SetStateFrequency('CA', 204); {Semi-Weekly-Fed File}
                               end;
                               calcDueDate := ctx_PayrollCalculation.CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 1, cdLiabList.fieldByName('CheckDate').AsDateTime+ 1, cdLiabList.fieldByName('CheckDate').AsDateTime); // bump it to next banking day
                               calcEndDate := cdLiabList.fieldByName('CheckDate').AsDateTime;
                             end;

                             if ControlMainPr then
                             begin
                              Fed945DueDate := calcDueDate;
                              Fed945EndDate := calcEndDate;
                              Fed945BeginDate := calcBeginDate;
                             end;

                           end;
                           cdLiabList.Next;
                        end;
                        if calcEndDate <> 0 then
                             UpdateLiability(DM_COMPANY.CO_FED_TAX_LIABILITIES, 0,0, calcBeginDate, calcEndDate,
                                     calcDueDate, -1, -1, ' PR_NBR in ( ' +calcPrList + ') and ' + Fed945Filter,
                                     True,-1,'', 0, tmpFedDepFreq945Date);
                 end;
               end;
             end;
          finally
           PeriodList.free;
          end;
        finally
         cdLiabList.Free;
        end;
    end;
  end;

  procedure DoStateTaxClient(const WithSUI: Boolean = False);
  var
    cdLiabList: TevClientDataset;
    prevCheckDate, calcBeginDate, calcEndDate, calcDueDate  : TDateTime;
    PeriodList: TStringList;
    NewStateFrequency, CO_STATES_NBR: Integer;
    stmpAchKeyDate,  calcPrList : String;
    ControlMainPr : Boolean;
    FedDueDateValue, FedEndDateValue : IisNamedValue;
    iTaxPeriod: integer;

    function GetDepFreqCurrent_State(CO_STATES_NBR: integer): Integer;
    var
        s : String;
        Q : IevQuery;
    begin
       result := 0;
       s := 'select a.SY_STATE_DEPOSIT_FREQ_NBR from CO_STATES a ' +
            'where {AsOfNow<a>} and a.CO_STATES_NBR = :pCO_STATES_NBR';
       Q := TevQuery.Create(s);
       Q.Params.AddValue('pCO_STATES_NBR', CO_STATES_NBR);
       Q.Execute;
       if Q.Result.Fields[0].AsInteger > 0 then
         Result := Q.Result.Fields[0].AsInteger;
    end;

    function RecalculateStateTresholds(const prList : String; const endDate: TDateTime) : integer;
    var
        TreshholdPeriod: string;
        ChangeDB : Boolean;
        LiabTotal: Currency;
        lFreqNbr: Integer;

        function FrequencyTypeIsForNY: boolean;
        var
            T: string;
        begin
            T := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.FieldByName('FREQUENCY_TYPE').AsString;
            Result := (T = FREQUENCY_TYPE_CHECK_DATE_NY) or (T = FREQUENCY_TYPE_QUARTERLY_NY);
        end;

    begin
          Result := StateFrequency;
          if StateFrequency = 0 then exit;

          ChangeDB := False;
          while True do
          begin
            with DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ do
            begin
              if not Locate('SY_STATE_DEPOSIT_FREQ_NBR', Result, []) then
                raise EInconsistentData.CreateHelp('State deposit frequency is not set for state ' + CurrentState+ ' as of '+ DateToStr(MainCheckDate), IDH_InconsistentData);
              if FieldByName('THRESHOLD_AMOUNT').AsCurrency = 0 then Break;
              lFreqNbr := FieldByName('THRESHOLD_DEP_FREQUENCY_NUMBER').AsInteger;
              if lFreqNbr <> 0 then
              begin
                TreshholdPeriod := FieldByName('THRESHOLD_PERIOD').AsString;
                if FrequencyTypeIsForNY then
                begin
                  LiabTotal := GetTaxLiabilitiesByPrNbr(DM_COMPANY.CO_STATE_TAX_LIABILITIES,' PR_NBR in ( ' + prList + ') ' ) +
                               GetTaxLiabilitiesByPrNbr(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,' PR_NBR in ( ' + prList + ') ' )
                end
                else
                  LiabTotal := GetTaxLiabilitiesByPrNbr(DM_COMPANY.CO_STATE_TAX_LIABILITIES,' PR_NBR in ( ' + prList + ') ' );

                if (FieldByName('THRESHOLD_AMOUNT').AsCurrency > LiabTotal) then break;
                Result := lFreqNbr;
                if (CurrentState<>'CA') and (CurrentState<>'NY') then
                  LogException_Add(CL_NBR, CO_NBR, PE_STATE_TRESHHOLD_EXCEEDED, 'State=' + CurrentState);
              end
               else break;
              ChangeDB := (FieldByName('CHANGE_STATUS_ON_THRESHOLD').AsString = GROUP_BOX_YES);
            end;
          end;

          if (ChangeDB) and (GetDepFreqCurrent_State(CO_STATES_NBR) <> Result) then
          begin
                SetStateFrequency(CurrentState, Result);
                if (CurrentState<>'CA') and (CurrentState<>'NY') then
                    LogException_Add(CL_NBR, CO_NBR, PE_STATE_TRESHHOLD_SB_CHANGED, 'State=' + CurrentState);
          end;
    end;

    Procedure SetVariables;
    begin
       ControlMainPr := false;
       calcPrList := '-1';

       NewStateFrequency := 0;
       calcDueDate := 0;
       calcEndDate := 0;
    end;

    Procedure SetStateUpdateLiability(const prList : String) ;
    begin
       UpdateLiability(DM_COMPANY.CO_STATE_TAX_LIABILITIES, 0, 0, calcBeginDate, calcEndDate,
              calcDueDate, -1, -1, ' PR_NBR in ( ' +prList + ')', True, NewStateFrequency, stmpAchKeyDate);
       if CurrentState = 'NY' then
          UpdateLiability(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 0, 0, calcBeginDate, calcEndDate,
              calcDueDate, -1, -1, ' PR_NBR in ( ' +prList + ')', True, NewStateFrequency, stmpAchKeyDate);
       SetVariables;
    end;

  begin

      CO_STATES_NBR := DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger;
      if WithSui then
      begin
        if not DM_COMPANY.CO_SUI.Locate('CO_STATES_NBR', CO_STATES_NBR, []) then
          raise EInconsistentData.CreateFmtHelp('Missing required SUI setup for %s state.',
            [CurrentState], IDH_InconsistentData);
        ctx_PayrollCalculation.GetSuiTaxDepositPeriod(DM_COMPANY.CO_SUI['SY_SUI_NBR'], MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
        UpdateLiability(DM_COMPANY.CO_STATE_TAX_LIABILITIES, 0, 99999999, GetBeginQuarter(MainCheckDate), GetEndQuarter(MainCheckDate), calcDueDate, CurrentPR_NBR);
        SetCurrentDates(calcBeginDate, calcEndDate, calcDueDate);
        Exit;
      end;

      ctx_PayrollCalculation.GetStateTaxDepositPeriod(CurrentState, StateFrequency, MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
      ActualDueDate := calcDueDate;

      if (((CurrentState <> 'NY') and (DM_COMPANY.CO_STATE_TAX_LIABILITIES.RecordCount = 0)) or
          ((CurrentState =  'NY') and (DM_COMPANY.CO_STATE_TAX_LIABILITIES.RecordCount = 0) and
                                      (DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.RecordCount = 0))) then
         exit;

      cdLiabList := TevClientDataSet.Create(nil);
      try
        cdLiabList.FieldDefs.Add('PrNbr', ftInteger, 0, True);
        cdLiabList.FieldDefs.Add('CheckDate', ftDateTime, 0, True);
        cdLiabList.FieldDefs.Add('BeginDate', ftDateTime, 0, True);

        cdLiabList.CreateDataSet;
        cdLiabList.LogChanges := False;

        cdLiabList.AddIndex('idxCheckDate', 'CheckDate', []);
        cdLiabList.IndexDefs.Update;
        cdLiabList.IndexName := 'idxCheckDate';

        DM_COMPANY.CO_STATE_TAX_LIABILITIES.First;
        while not DM_COMPANY.CO_STATE_TAX_LIABILITIES.Eof do
        begin
         if not cdLiabList.Locate('CheckDate;PrNbr',
                         VarArrayOf([DM_COMPANY.CO_STATE_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                     DM_COMPANY.CO_STATE_TAX_LIABILITIES.PR_NBR.AsInteger]), []) then
         begin
           ctx_PayrollCalculation.GetStateTaxDepositPeriod(CurrentState, StateFrequency,
                                          DM_COMPANY.CO_STATE_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                          calcBeginDate, calcEndDate, calcDueDate);
           cdLiabList.AppendRecord([DM_COMPANY.CO_STATE_TAX_LIABILITIES.PR_NBR.AsInteger,
                                    DM_COMPANY.CO_STATE_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                    calcBeginDate]);
         end;
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.Next;
        end;

        if (CurrentState =  'NY') then
        begin
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.First;
          while not DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Eof do
          begin
           if not cdLiabList.Locate('CheckDate;PrNbr',
                           VarArrayOf([DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                       DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.PR_NBR.AsInteger]), []) then
           begin
             ctx_PayrollCalculation.GetStateTaxDepositPeriod(CurrentState, StateFrequency,
                                            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                            calcBeginDate, calcEndDate, calcDueDate);
             cdLiabList.AppendRecord([DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.PR_NBR.AsInteger,
                                      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                      calcBeginDate]);
           end;
            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Next;
          end;
        end;

        PeriodList := cdLiabList.GetFieldValueList('BeginDate',True);
        try
           if PeriodList.Count > 0 then
           begin
             DM_SYSTEM_STATE.SY_STATES.Activate;
             for iTaxPeriod := 0 to PeriodList.Count - 1 do
             begin
               cdLiabList.Filter :='BeginDate = '+ QuotedStr(PeriodList[iTaxPeriod]);
               cdLiabList.Filtered := true;

               if cdLiabList.RecordCount > 0 then
               begin
                      SetVariables;
                      prevCheckDate := 0;

                      cdLiabList.First;
                      while not cdLiabList.Eof do
                      begin
                        if prevCheckDate <> cdLiabList.fieldByName('CheckDate').AsDateTime then
                        begin
                             prevCheckDate := cdLiabList.fieldByName('CheckDate').AsDateTime;

                             if (calcEndDate < cdLiabList.fieldByName('CheckDate').AsDateTime) and  (calcEndDate <> 0 ) then
                                 SetStateUpdateLiability(calcPrList);

                             GetPrNbrList(cdLiabList, calcPrList, ControlMainPr);

                             if CompanyStateUsesTresholds then
                                 NewStateFrequency := RecalculateStateTresholds(calcPrList, prevCheckDate)
                             else
                                 NewStateFrequency := StateFrequency;

                             Assert(DM_SYSTEM_STATE.SY_STATES.Locate('STATE', CurrentState,[]));
                             ctx_PayrollCalculation.GetStateTaxDepositPeriod(CurrentState, NewStateFrequency,
                                                                  cdLiabList.fieldByName('CheckDate').AsDateTime,
                                                                    calcBeginDate, calcEndDate, calcDueDate);
                             stmpAchKeyDate := ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(STATE_TAX_DESC, NewStateFrequency,
                                                            DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsInteger,
                                                             cdLiabList.fieldByName('CheckDate').AsDateTime,
                                                             calcEndDate);

                             if FedNextDayDueDate.Count > 0 then
                             begin
                                Assert(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Locate('SY_STATE_DEPOSIT_FREQ_NBR', NewStateFrequency, []));
                                if (DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.FieldByName('FREQUENCY_TYPE').AsString[1] = FREQUENCY_TYPE_FOLLOW_FED)
                                or (DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.FieldByName('WHEN_DUE_TYPE').AsString[1] = WHEN_DUE_FOLLOW_FEDERAL) then
                                begin
                                  FedDueDateValue := FedNextDayDueDate.FindValue(cdLiabList.fieldByName('CheckDate').AsString);
                                  if Assigned(FedDueDateValue) then
                                     calcDueDate :=  FedDueDateValue.Value;

                                  FedEndDateValue := FedNextDayEndDate.FindValue(cdLiabList.fieldByName('CheckDate').AsString);
                                  if Assigned(FedEndDateValue) then
                                     calcEndDate :=  FedEndDateValue.Value;
                                end;
                             end;

                             if (CurrentState = 'NY') and ControlMainPr then
                             begin
                                  NYStateEndDate := calcEndDate;
                                  NYStateDueDate := calcDueDate;
                             end;

                             if ControlMainPr then
                                SetCurrentDates(calcBeginDate, calcEndDate, calcDueDate);

                        end;
                        cdLiabList.Next;
                      end;

                      if calcEndDate <> 0 then
                         SetStateUpdateLiability(calcPrList)
               end;
             end;
           end;
        finally
         PeriodList.free;
        end;
      finally
        cdLiabList.Free;
      end;
  end;

  // TCD
  function isItTCDLocalTax(const pCO_LOCAL_TAX_NBR: integer): boolean;
  begin
   result := false;
   if cdTCDLocals.Locate('CO_LOCAL_TAX_NBR', pCO_LOCAL_TAX_NBR, []) then
   begin
     Assert(DM_SYSTEM.SY_LOCALS.Locate('SY_LOCALS_NBR', cdTCDLocals.FieldByName('SY_LOCALS_NBR').AsInteger, []));
     if (DM_SYSTEM_STATE.SY_LOCALS.FieldByName('COMBINE_FOR_TAX_PAYMENTS').AsString = GROUP_BOX_YES) and
        (DM_SYSTEM_STATE.SY_LOCALS.FieldByName('LOCAL_TYPE').AsString[1] in [LOCAL_TYPE_SCHOOL, LOCAL_TYPE_INC_RESIDENTIAL,LOCAL_TYPE_INC_NON_RESIDENTIAL]) then
        result:= not cdTCDLocals.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull;
   end;
  end;
  // TCD

  procedure DoLocalTaxClient(const DoNotCalculateDueDates: Boolean);
  var
    cdLiabList: TevClientDataset;
    prevCheckDate, calcBeginDate, calcEndDate, calcDueDate : TDateTime;
    PeriodList : TStringList;
    stmpAchKeyDate, calcPrList : String;
    CO_LOCAL_TAX_NBR, NewLocalFrequency, ACHSeqNumber: Integer;
    ControlMainPr : Boolean;
    s : String;
    iTaxPeriod: integer;

    function RecalculateLocalTresholds(const LocalNbr: integer;const PrList : String; const endDate:TDateTime): integer;
    var
        TreshholdPeriod: string;
        ChangeDB : Boolean;
        LiabTotal: Currency;
        lFreqNbr: Integer;

        procedure SetLocalFrequency(LocalNbr, NewFrequency: integer);
        begin
          with TevClientDataset.Create(nil) do
          try
            CloneCursor(DM_COMPANY.CO_LOCAL_TAX, True);
            First;
            while not EOF do
            begin
              if (FieldValues['SY_LOCALS_NBR'] = LocalNbr) and cdSelectedCo.Locate('CO_NBR', FieldValues['CO_NBR'], []) then
              begin
                Edit;
                FieldByName('SY_LOCAL_DEPOSIT_FREQ_NBR').AsInteger := NewFrequency;
                Post;
              end;
              Next;
            end;
          finally
            Free;
          end;
          with TevClientDataset.Create(nil) do
          try
            CloneCursor(lCO_LOCAL_TAX, True);
            First;
            while not EOF do
            begin
              if (FieldValues['SY_LOCALS_NBR'] = LocalNbr) and cdSelectedCo.Locate('CO_NBR', FieldValues['CO_NBR'], []) then
              begin
                Edit;
                FieldByName('SY_LOCAL_DEPOSIT_FREQ_NBR').AsInteger := NewFrequency;
                Post;
              end;
              Next;
            end;
          finally
            Free;
          end;
        end;

    begin
          Result := LocalFrequency;
          if LocalFrequency = 0 then  exit;

          ChangeDB := False;
          while True do
          begin
            with DM_SYSTEM_STATE.SY_LOCAL_DEPOSIT_FREQ do
            begin
              if not Locate('SY_LOCAL_DEPOSIT_FREQ_NBR', Result, []) then
                raise EInconsistentData.CreateHelp('Local deposit frequency is not set for state ' + CurrentState+ ' as of '+ DateToStr(MainCheckDate), IDH_InconsistentData);
              if FieldByName('FIRST_THRESHOLD_AMOUNT').AsCurrency = 0 then Break;
              lFreqNbr := FieldByName('FIRST_THRESHOLD_DEP_FREQ_NBR').AsInteger;
              if lFreqNbr <> 0 then
              begin
                TreshholdPeriod := FieldByName('FIRST_THRESHOLD_PERIOD').AsString;
                LiabTotal := GetTaxLiabilitiesByPrNbr(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,' PR_NBR in ( ' +PrList + ')' );

                if (FieldByName('FIRST_THRESHOLD_AMOUNT').AsCurrency > LiabTotal) then break;
                Result := lFreqNbr;
                LogException_Add(CL_NBR, CO_NBR, PE_LOCAL_TRESHHOLD_EXCEEDED, 'Local=' + IntToStr(LocalNbr));
              end
               else break;
              ChangeDB := (FieldByName('CHANGE_FREQ_ON_THRESHOLD').AsString = GROUP_BOX_YES);
            end;
          end;

          if (ChangeDB) and (LocalFrequency <> Result) then
          begin
             SetLocalFrequency(LocalNbr, Result);
             LogException_Add(CL_NBR, CO_NBR, PE_LOCAL_TRESHHOLD_SB_CHANGED, 'Local=' + IntToStr(LocalNbr));
          end;
    end;

    Procedure SetVariables;
    begin
      ControlMainPr := false;
      NewLocalFrequency := 0;
      calcPrList := '-1';
      calcEndDate := 0;
      calcDueDate := 0;
    end;

  begin

    if DoNotCalculateDueDates then Exit;
    CO_LOCAL_TAX_NBR := DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'];

 //  TCD
    if isItTCDLocalTax(CO_LOCAL_TAX_NBR) then
    begin
      s := DM_SYSTEM.SY_STATES.Lookup('SY_STATES_NBR', cdTCDLocals.FieldByName('SY_STATES_NBR').AsInteger, 'STATE');
      DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.Activate;
      if not DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', cdTCDLocals.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger, []) then
          raise EInconsistentData.CreateHelp('Frequency is not set for local ' + s + '-' + DM_SYSTEM.SY_LOCALS['Name'], IDH_InconsistentData);
      ctx_PayrollCalculation.GetTaxDepositPeriodFromDataSet(DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ,
                                                             DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime,
                                                             DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger,
                                                             calcBeginDate, calcEndDate, calcDueDate, '');

      ACHSeqNumber := ctx_PayrollCalculation.GetSequenceNumber(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime,
                                                               DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.FieldByName('FREQUENCY_TYPE').AsString[1]);

      UpdateLiability(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, calcBeginDate, calcEndDate, calcBeginDate, calcEndDate, calcDueDate, CurrentPR_NBR, -1, '', True, -1,
                             FormatDateTime('mm/dd/yyyy', calcEndDate) + Format('%1u', [ACHSeqNumber]));
      SetCurrentDates(calcBeginDate, calcEndDate, calcDueDate);
      exit;   //  .... EXIT ...
    end;
  //  end TCD

    ctx_PayrollCalculation.GetLocalTaxDepositPeriod(SyLocalNbr, LocalFrequency, MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
    SetCurrentDates(calcBeginDate, calcEndDate, calcDueDate);

    if DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.RecordCount = 0 then
     exit;

    if DM_SYSTEM_LOCAL.SY_LOCALS['TAX_DEPOSIT_FOLLOW_STATE'] <> GROUP_BOX_YES then
    begin

        cdLiabList := TevClientDataSet.Create(nil);
        try
          cdLiabList.FieldDefs.Add('PrNbr', ftInteger, 0, True);
          cdLiabList.FieldDefs.Add('CheckDate', ftDateTime, 0, True);
          cdLiabList.FieldDefs.Add('BeginDate', ftDateTime, 0, True);
          cdLiabList.CreateDataSet;
          cdLiabList.LogChanges := False;

          cdLiabList.AddIndex('idxCheckDate', 'CheckDate', []);
          cdLiabList.IndexDefs.Update;
          cdLiabList.IndexName := 'idxCheckDate';

          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.First;
          while not DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Eof do
          begin
           if not cdLiabList.Locate('CheckDate;PrNbr',
                           VarArrayOf([DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                       DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.PR_NBR.AsInteger]), []) then
           begin
             ctx_PayrollCalculation.GetStateTaxDepositPeriod(CurrentState, StateFrequency,
                                            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                            calcBeginDate, calcEndDate, calcDueDate);
             cdLiabList.AppendRecord([DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.PR_NBR.AsInteger,
                                      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CHECK_DATE.AsDateTime,
                                      calcBeginDate]);
           end;
            DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Next;
          end;

          PeriodList := cdLiabList.GetFieldValueList('BeginDate',True);
          try
             if PeriodList.Count > 0 then
             begin
               for iTaxPeriod := 0 to PeriodList.Count - 1 do
               begin
                 cdLiabList.Filter :='BeginDate = '+ QuotedStr(PeriodList[iTaxPeriod]);
                 cdLiabList.Filtered := true;

                 if cdLiabList.RecordCount > 0 then
                 begin
                        SetVariables;
                        prevCheckDate := 0;
                        DM_SYSTEM_STATE.SY_LOCALS.Activate;
                        cdLiabList.First;
                        while not cdLiabList.Eof do
                        begin
                           if prevCheckDate <> cdLiabList.fieldByName('CheckDate').AsDateTime then
                           begin
                             prevCheckDate := cdLiabList.fieldByName('CheckDate').AsDateTime;
                             if (calcEndDate < cdLiabList.fieldByName('CheckDate').AsDateTime) and (calcEndDate <> 0 ) then
                             begin
                                UpdateLiability(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 0, 0, calcBeginDate, calcEndDate,
                                        calcDueDate, -1, -1, ' PR_NBR in ( ' + calcPrList + ')', True, NewLocalFrequency, stmpAchKeyDate);
                                SetVariables;
                             end;

                             GetPrNbrList(cdLiabList, calcPrList, ControlMainPr);

                             NewLocalFrequency := RecalculateLocalTresholds(SyLocalNbr, calcPrList, prevCheckDate);
                             Assert(DM_SYSTEM_STATE.SY_LOCALS.Locate('SY_LOCALS_NBR',SyLocalNbr,[]));
                             ctx_PayrollCalculation.GetLocalTaxDepositPeriod(SyLocalNbr, NewLocalFrequency,
                                   cdLiabList.fieldByName('CheckDate').AsDateTime, calcBeginDate, calcEndDate, calcDueDate);
                             stmpAchKeyDate :=  ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(LOCAL_TAX_DESC, NewLocalFrequency,
                                                   SyLocalNbr, cdLiabList.fieldByName('CheckDate').AsDateTime, calcEndDate);
                             if ControlMainPr then
                                SetCurrentDates(calcBeginDate, calcEndDate, calcDueDate);

                           end;
                           cdLiabList.Next;
                        end;
                        if calcEndDate <> 0 then
                             UpdateLiability(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 0, 0, calcBeginDate, calcEndDate,
                                        calcDueDate, -1, -1, ' PR_NBR in ( ' + calcPrList + ')', True, NewLocalFrequency, stmpAchKeyDate);
                 end;
               end;
             end;
          finally
           PeriodList.free;
          end;
        finally
         cdLiabList.Free;
        end;
    end
    else
    begin
     stmpAchKeyDate := ctx_PayrollCalculation.LiabilityACH_KEY_CalcMethod(LOCAL_TAX_DESC, LocalFrequency, SyLocalNbr, MainCheckDate);
     UpdateLiability(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, calcBeginDate, calcEndDate, calcBeginDate, calcEndDate, calcDueDate, CurrentPR_NBR, -1, '', True, -1, stmpAchKeyDate);
    end;

  end;

  function CreateNewTaxDeposit(const Reference, TAX_DEPOSIT_TYPE, TAX_DEPOSIT_STATUS: string; const OriginalPR_NBR: Integer;
    Global_Agency_Nbr: integer): integer;
  begin
    with DM_COMPANY.CO_TAX_DEPOSITS do
    begin
      Append;
      FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := Reference;
      FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS;
      FieldByName('PR_NBR').AsInteger := OriginalPR_NBR;
      FieldByName('DEPOSIT_TYPE').AsString := TAX_DEPOSIT_TYPE;
      if Global_Agency_Nbr <> -1 then
        FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger := GetGlobalAgencyFieldOffice(Global_Agency_Nbr);
      Post;
      Result := FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
    end;
  end;

  procedure UpdateMiscCheck(const CoTaxDepositNbr: Integer; const Amount: Currency);
  begin
    with DM_PAYROLL.PR_MISCELLANEOUS_CHECKS do
    begin
      Assert(Active);
      Assert(Locate('CO_TAX_DEPOSITS_NBR', CoTaxDepositNbr, []));
      Edit;
      FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency := FieldByName('MISCELLANEOUS_CHECK_AMOUNT').AsCurrency+ Amount;
      Post;
    end;
  end;

  function ConvertTaxPayment(const ForPRProcessing: boolean; const aPR_NBR: integer; const aPR_CHECK_DATE: TDateTime; const TaxType:
    string; const TaxPaymentMethod: string; const Amount: Currency; var CoStateDepositNbr: Integer; Global_Agency_Nbr: integer = -1): integer;
  var
    NewTaxDepositId: Integer;
    Reference: string;
    SerialNumber: Integer;
    BankAccount: string;
    MiscCheckType: string;
    NewTaxDepositStatus: string;
    TAX_DEPOSIT_TYPE: string;
  begin
    SerialNumber := -1;
    NewTaxDepositId := -1;
    case TaxPaymentMethod[1] of
      TAX_PAYMENT_METHOD_CREDIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_CREDIT;
      TAX_PAYMENT_METHOD_DEBIT,
        TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_DEBIT;
      TAX_PAYMENT_METHOD_NOTICES:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_NOTICE;
      TAX_PAYMENT_METHOD_CHECK,
        TAX_PAYMENT_METHOD_NOTICES_CHECKS:
        TAX_DEPOSIT_TYPE := TAX_DEPOSIT_TYPE_CHECK;
    end;
    case TaxPaymentMethod[1] of
      TAX_PAYMENT_METHOD_CREDIT,
      TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT:
        if ForPRProcessing then
           raise ECanNotPerformOperation.CreateHelp('EFT not offered for non-tax tax client - ' + TaxType + ' for CO_NBR=' + IntToStr(CO_NBR), IDH_CanNotPerformOperation);
      TAX_PAYMENT_METHOD_DEBIT,
      TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT:
        begin
          Result := 0;
          Exit;
        end;
      TAX_PAYMENT_METHOD_NOTICES:
        begin
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE;
          Reference := 'Notice';
        end;
      TAX_PAYMENT_METHOD_NOTICES_CHECKS:
        begin
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID;
          NewTaxDepositId := CoStateDepositNbr;
          if NewTaxDepositId = -1 then
          begin
            NextCompanyTaxAvailableCheckNumber(SerialNumber, BankAccount, MiscCheckType);
            Reference := 'Notice - Check# ' + IntToStr(SerialNumber);
            if TaxType = 'Federal' then
              Global_Agency_Nbr := -1;
          end;
        end;
      TAX_PAYMENT_METHOD_CHECK:
        begin
          NewTaxDepositStatus := TAX_DEPOSIT_STATUS_PAID;
          NewTaxDepositId := CoStateDepositNbr;
          if NewTaxDepositId = -1 then
          begin
            NextCompanyTaxAvailableCheckNumber(SerialNumber, BankAccount, MiscCheckType);
            Reference := 'Check# ' + IntToStr(SerialNumber);
            if TaxType = 'Federal' then
              Global_Agency_Nbr := -1;
          end;
        end;
    else
      raise EInconsistentData.CreateHelp('Unknown ' + TaxType + ' tax payment for CO_NBR=' + IntToStr(CO_NBR), IDH_InconsistentData);
    end;
    if NewTaxDepositId = -1 then
    begin
      NewTaxDepositId := CreateNewTaxDeposit(Reference, TAX_DEPOSIT_TYPE, NewTaxDepositStatus, aPR_NBR, Global_Agency_Nbr);
      if SerialNumber <> -1 then
        CreateMiscCheck(aPR_NBR, MiscCheckType, Amount, SerialNumber, BankAccount, IntToStr(NewTaxDepositId), Global_Agency_Nbr);
    end
    else
      UpdateMiscCheck(NewTaxDepositId, Amount);
    case TaxPaymentMethod[1] of
      TAX_PAYMENT_METHOD_NOTICES_CHECKS,
      TAX_PAYMENT_METHOD_CHECK:
        CoStateDepositNbr := NewTaxDepositId;
    end;
    Result := NewTaxDepositId;
  end;

  procedure DoFederalNonTaxClient;
  var
    Amount: Currency;
    NewTaxDepositId, StateDepositNbr: Integer;
    i: Integer;
    s: string;
    l: TStringList;
    CurrentLiabilityFilterForUnPaidLiab : String;
  begin
    DoFederalTaxClient;
    if MainPrNbr < 0 then exit;

    CurrentLiabilityFilterForUnPaidLiab := CurrentLiabilityFilter + 'and (CO_TAX_DEPOSITS_NBR is null ) ';

    if ctx_PayrollCalculation.NextCheckDate(MainCheckDate) > Fed941EndDate then
      // it's the last payroll in the period. i assume there is only one company because it's non-tax.
      s := '<='
    else
      s := '<';
    Amount := 0;
    with DM_COMPANY, DM_SYSTEM_FEDERAL do
    begin
      l := GetFieldValueList(CO_FED_TAX_LIABILITIES, 'DUE_DATE', Fed941FilterWithCobra + ' and ' + CurrentLiabilityFilterForUnPaidLiab +
        ' and DUE_DATE '+ s+ '''' + DateToStr(Fed941DueDate) + '''', False);
      SortDateList(l);
      with l do
      try
        for i := 0 to Count - 1 do
        begin
          if StrToDate(Strings[i]) < Fed941DueDate then
            LogException_Add(DM_CLIENT.CL['CL_NBR'], CO['CO_NBR'], PE_CHECK_WAS_CUT_FOR_LATE);
          Amount := Amount+ GetTaxLiabilities(0, 99999999, CO_FED_TAX_LIABILITIES, '', 0, Fed941FilterWithCobra + ' and ' + CurrentLiabilityFilterForUnPaidLiab +
            ' and DUE_DATE = ''' + Strings[i] + '''', False);
          if Amount > 0 then
          begin
            StateDepositNbr := -1;
            NewTaxDepositId := ConvertTaxPayment(True, MainPrNbr, MainCheckDate, 'Federal', CO['FEDERAL_TAX_PAYMENT_METHOD'], Amount,
              StateDepositNbr,
              ConvertNull(SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR'), -1));
            if NewTaxDepositId <> 0 then
            begin
              UpdateLiability(CO_FED_TAX_LIABILITIES, 0, 99999999, Fed941BeginDate, Fed941EndDate, 0, -1, NewTaxDepositId,
                      Fed941FilterWithCobra + ' and ' + CurrentLiabilityFilterForUnPaidLiab + ' and DUE_DATE <= ''' + Strings[i] + '''', False);
              Amount := 0;
            end;
          end;
        end;
      finally
        Free
      end;
    end;
    if ctx_PayrollCalculation.NextCheckDate(MainCheckDate) > Fed945EndDate then
      // it's the last payroll in the period. i assume there is only one company because it's non-tax.
      s := '<='
    else
      s := '<';
    Amount := 0;
    with DM_COMPANY, DM_SYSTEM_FEDERAL do
    begin
      l := GetFieldValueList(CO_FED_TAX_LIABILITIES, 'DUE_DATE', Fed945Filter + ' and ' + CurrentLiabilityFilterForUnPaidLiab +
        ' and DUE_DATE '+ s+ '''' + DateToStr(Fed945DueDate) + '''', False);
      SortDateList(l);
      with l do
      try
        for i := 0 to Count - 1 do
        begin
          if StrToDate(Strings[i]) < Fed945DueDate then
            LogException_Add(DM_CLIENT.CL['CL_NBR'], CO['CO_NBR'], PE_CHECK_WAS_CUT_FOR_LATE);
          Amount := Amount+ GetTaxLiabilities(0, 99999999, CO_FED_TAX_LIABILITIES, '', 0, Fed945Filter + ' and ' + CurrentLiabilityFilterForUnPaidLiab +
            ' and DUE_DATE = ''' + Strings[i] + '''', False);
          if Amount > 0 then
          begin
            StateDepositNbr := -1;
            NewTaxDepositId := ConvertTaxPayment(True, MainPrNbr, MainCheckDate, 'Federal', CO['FEDERAL_TAX_PAYMENT_METHOD'], Amount,
              StateDepositNbr,
              ConvertNull(SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR'), -1));
            if NewTaxDepositId <> 0 then
            begin
              UpdateLiability(CO_FED_TAX_LIABILITIES, 0, 99999999, Fed945BeginDate, Fed945EndDate, 0, -1, NewTaxDepositId,
                       Fed945Filter + ' and ' + CurrentLiabilityFilterForUnPaidLiab + ' and DUE_DATE = ''' + Strings[i] + '''', False);
              Amount := 0;
            end;
          end;
        end;
      finally
        Free
      end;
    end;
  end;

  procedure DoStateNonTaxClient(const KeepSystemFilter: Boolean = False);
  var
    Amount: Currency;
    NewTaxDepositId, StateDepositNbr: Integer;
    i {, j}: Integer;
    s, DueDateFilter: string;
    l: TStringList;
    CurrentLiabilityFilterForUnPaidLiab : String;
  begin
    DoStateTaxClient;
    if MainPrNbr < 0 then exit;

    CurrentLiabilityFilterForUnPaidLiab := CurrentLiabilityFilter + 'and (CO_TAX_DEPOSITS_NBR is null ) ';

    if ((DM_COMPANY.CO_STATES['STATE'] = 'NY') and (ActualDueDate = newcalcDueDate))
    or {'NJ'}(((DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'] = 88) or (DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'] = 89)) and (DayOfTheMonth(newcalcDueDate)=30)) then
      // nothing
    else
    begin
      if {'NJ'}(((DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'] = 88) or (DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'] = 89)) and (MonthOfTheYear(MainCheckDate) in [1, 4, 7, 10])) then
        // do not pick last quarter until second month of quarter
        s := '='
      else
      if ctx_PayrollCalculation.NextCheckDate(MainCheckDate) > newcalcEndDate then
        // it's the last payroll in the period. i assume there is only one company because it's non-tax.
        s := '<='
      else
        s := '<';
      Amount := 0;
      with DM_COMPANY, DM_SYSTEM_STATE do
      begin
        l := GetFieldValueList(CO_STATE_TAX_LIABILITIES, 'DUE_DATE', CurrentLiabilityFilterForUnPaidLiab + ' and DUE_DATE '+ s+ '''' +
          DateToStr(newcalcDueDate) + '''', KeepSystemFilter);
        SortDateList(l);
        DueDateFilter := '';
        with l do
        try
          for i := 0 to Count - 1 do
          begin
            if StrToDate(Strings[i]) < newcalcDueDate then
              LogException_Add(DM_CLIENT.CL['CL_NBR'], CO['CO_NBR'], PE_CHECK_WAS_CUT_FOR_LATE);
            Amount := Amount+ GetTaxLiabilities(0, 99999999, CO_STATE_TAX_LIABILITIES, '', 0, CurrentLiabilityFilterForUnPaidLiab + ' and DUE_DATE = ''' +
              Strings[i] + '''', False);
            if DueDateFilter <> '' then
              DueDateFilter := DueDateFilter+ ' or ';
            DueDateFilter := DueDateFilter+ '(DUE_DATE='+ QuotedStr(Strings[i])+ ')';
            if Amount > 0 then
            begin
              StateDepositNbr := -1;
              NewTaxDepositId := ConvertTaxPayment(True, MainPrNbr, MainCheckDate, 'State ' + CO_STATES['STATE'],
                CO_STATES['STATE_TAX_DEPOSIT_METHOD'], Amount, StateDepositNbr,
                ConvertNull(SY_STATES.Lookup('STATE', CO_STATES['STATE'], 'SY_STATE_TAX_PMT_AGENCY_NBR'), -1));
              if StateDepositNbr <> -1 then
                cdStateDepositNbr.AppendRecord([CO_STATES['STATE'], NewTaxDepositId, CO_STATES['STATE_EIN']]);
              if NewTaxDepositId <> 0 then
              begin
                UpdateLiability(CO_STATE_TAX_LIABILITIES, 0, 99999999, newcalcBeginDate, newcalcEndDate, 0, -1, NewTaxDepositId,
                         CurrentLiabilityFilterForUnPaidLiab + ' and ('+ DueDateFilter+ ')', True);
                DueDateFilter := '';
                Amount := 0;
              end;
            end;
          end;
        finally
          Free
        end;
      end;
    end;
  end;

  procedure DoLocalNonTaxClient(const DoNotCalculateDueDates: Boolean);
  var
    Amount: Currency;
    NewTaxDepositId, StateDepositNbr: Integer;
    i, j: Integer;
    sFilter, s: string;
    l: TStringList;
    CurrentLiabilityFilterForUnPaidLiab : String;
  begin
    DoLocalTaxClient(DoNotCalculateDueDates);
    if MainPrNbr < 0 then exit;

    CurrentLiabilityFilterForUnPaidLiab := CurrentLiabilityFilter + 'and (CO_TAX_DEPOSITS_NBR is null ) ';

    if DoNotCalculateDueDates and (NYStateEndDate <> 0) then
    begin
      newcalcEndDate := NYStateEndDate;
      newcalcDueDate := NYStateDueDate;
    end;
    if ctx_PayrollCalculation.NextCheckDate(MainCheckDate) > newcalcEndDate then
      // it's the last payroll in the period. i assume there is only one company because it's non-tax.
      s := '<='
    else
      s := '<';
    with DM_COMPANY, DM_SYSTEM_LOCAL, DM_SYSTEM_STATE do
    begin
      with GetFieldValueList(CO_LOCAL_TAX_LIABILITIES, 'paymentCO_LOCAL_TAX_NBR', '', False) do
      try
        for j := 0 to Count - 1 do
        begin
          sFilter := CurrentLiabilityFilterForUnPaidLiab + ' and paymentCO_LOCAL_TAX_NBR = ' + Strings[j];
          Assert(CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', StrToInt(Strings[j]), []));
          Amount := 0;
          l := GetFieldValueList(CO_LOCAL_TAX_LIABILITIES, 'DUE_DATE', sFilter + ' and DUE_DATE '+ s+ '''' +
            DateToStr(newcalcDueDate) + '''', False);
          SortDateList(l);
          with l do
          try
            for i := 0 to Count - 1 do
            begin
              if StrToDate(Strings[i]) < newcalcDueDate then
                LogException_Add(DM_CLIENT.CL['CL_NBR'], CO['CO_NBR'], PE_CHECK_WAS_CUT_FOR_LATE);
              Amount := Amount+ GetTaxLiabilities(0, 99999999, CO_LOCAL_TAX_LIABILITIES, '', 0, sFilter + ' and DUE_DATE = '+ QuotedStr(Strings[i]),
                False);
              if Amount > 0 then
              begin
                Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', CO_LOCAL_TAX['SY_LOCALS_NBR'], []));
                Assert(SY_STATES.Locate('SY_STATES_NBR', SY_LOCALS['SY_STATES_NBR'], []));


                StateDepositNbr := -1;
                if (SY_LOCALS['USE_MISC_TAX_RETURN_CODE'] = GROUP_BOX_YES)
                and cdStateDepositNbr.Locate('STATE;EIN', VarArrayOf([SY_STATES['STATE'], CO_LOCAL_TAX['LOCAL_EIN_NUMBER']]), []) then
                  StateDepositNbr := cdStateDepositNbr['CO_TAX_DEPOSIT_NBR'];

                if (StateDepositNbr = -1) and
                    cdLocalDepositNbr.Locate('SY_LOCALS_NBR;EIN', VarArrayOf([SY_LOCALS['SY_LOCALS_NBR'], CO_LOCAL_TAX['LOCAL_EIN_NUMBER']]), []) then
                  StateDepositNbr := cdLocalDepositNbr['CO_TAX_DEPOSIT_NBR'];

                NewTaxDepositId := ConvertTaxPayment(True, MainPrNbr, MainCheckDate, 'Local ' + SY_LOCALS['NAME'],
                  CO_LOCAL_TAX['PAYMENT_METHOD'], Amount, StateDepositNbr, ConvertNull(SY_LOCALS['SY_LOCAL_TAX_PMT_AGENCY_NBR'], -1));

                if (StateDepositNbr <> -1) and
                   not cdLocalDepositNbr.Locate('SY_LOCALS_NBR;EIN',VarArrayOf([SY_LOCALS['SY_LOCALS_NBR'], CO_LOCAL_TAX['LOCAL_EIN_NUMBER']]), [])
                   then
                   cdLocalDepositNbr.AppendRecord([SY_LOCALS['SY_LOCALS_NBR'], CO_LOCAL_TAX['LOCAL_EIN_NUMBER'], NewTaxDepositId]);

                if NewTaxDepositId <> 0 then
                begin
                  UpdateLiability(CO_LOCAL_TAX_LIABILITIES, 0, 99999999, newcalcBeginDate, newcalcEndDate, 0, -1, NewTaxDepositId,
                             sFilter + ' and DUE_DATE <= ' + QuotedStr(Strings[i]), False);
                  Amount := 0;
                end;
              end;
            end;
          finally
            Free;
          end;
        end;
      finally
        Free
      end;
    end;
  end;

  procedure DoFUI;
  var
    calcBeginDate, calcEndDate, calcDueDate : TDateTime;
    ACHSeqNumber : integer;

    function GetPeriodBeginDate(CheckDate: TDateTime): TDateTime;
    begin
      if FedFUIFrequency = FREQUENCY_TYPE_ANNUAL then
        Result := GetBeginYear(CheckDate)
      else
        Result := GetBeginQuarter(CheckDate);
    end;

    function GetPeriodEndDate(CheckDate: TDateTime): TDateTime;
    begin
      if FedFUIFrequency = FREQUENCY_TYPE_ANNUAL then
        Result := GetEndYear(CheckDate)
      else
        Result := GetEndQuarter(CheckDate);
    end;


    function GetDueDate(FUITaxDepositFrequency: string; CheckDate: TDateTime): TDateTime;
    begin
     Result := ctx_PayrollCalculation.CheckDueDateForHoliday(
                 CalculateFUIDueDate(FUITaxDepositFrequency, CheckDate),
                  ConvertNull(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'], 'SY_GLOBAL_AGENCY_NBR'), 0));
    end;

    function FUIThresholdMet: Boolean;
      // If FUI liabilities in the year of the checkdate total over $500 the
      // FUI frequency will be set to quarterly, only for current year!
    var
      flt: String;
      FUILiabYTD: Currency;
    begin
        //Make sure to only include due dates = Annual or due dates = check dates quarter end
        flt := Fed940Filter +
              ' and CHECK_DATE >= ''' + DateToStr(GetBeginYear(MainCheckDate)) + ''''+
              ' and CHECK_DATE < ''' + DateToStr(GetEndYear(MainCheckDate) + 1) + ''''+
              ' and (DUE_DATE = ''' + DateToStr(GetDueDate(FREQUENCY_TYPE_ANNUAL, MainCheckDate)) + ''''+
              ' or DUE_DATE = ''' + DateToStr(GetDueDate(FREQUENCY_TYPE_QUARTERLY, MainCheckDate)) + '''' +
              ' or PR_NBR = ''' + IntToStr(MainPrNbr) + ''')';

        FUILiabYTD := SumDataSet(DM_COMPANY.CO_FED_TAX_LIABILITIES, 'AMOUNT', flt);

        Result := FUILiabYTD >= FUIThreshold;
    end;

    procedure ChangeFUIFreqFromAnualToQuarterly;
    begin
      // ... and update outstanding FUI liabilities
      DM_COMPANY.CO_FED_TAX_LIABILITIES.SaveState;
      try
        DM_COMPANY.CO_FED_TAX_LIABILITIES.CancelRange;
        DM_COMPANY.CO_FED_TAX_LIABILITIES.Filter := Fed940Filter;

        DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
        while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
        begin
          //Make sure only the current payroll and annually due liabilities are updated with a check date of Current Payrolls QuaterEndDate(checkdate) or less
          if ((DM_COMPANY.CO_FED_TAX_LIABILITIES.DUE_DATE.AsDateTime = GetDueDate(FREQUENCY_TYPE_ANNUAL, MainCheckDate)) or (DM_COMPANY.CO_FED_TAX_LIABILITIES.PR_NBR.AsInteger = MainPrNbr)) then
          begin
            if (DM_COMPANY.CO_FED_TAX_LIABILITIES.CHECK_DATE.AsDateTime <= GetEndQuarter(MainCheckDate)) then
            begin
              DM_COMPANY.CO_FED_TAX_LIABILITIES.Edit;
              DM_COMPANY.CO_FED_TAX_LIABILITIES.PERIOD_END_DATE.Value := calcEndDate;
              DM_COMPANY.CO_FED_TAX_LIABILITIES.DUE_DATE.Value := calcDueDate;
              DM_COMPANY.CO_FED_TAX_LIABILITIES.ACH_KEY.Value := FormatDateTime('mm/dd/yyyy', calcEndDate) + Format('%1u', [ACHSeqNumber]);
              DM_COMPANY.CO_FED_TAX_LIABILITIES.Post;
            end;
          end;
          DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
        end;
      finally
        //Put back the applied Range and make sure the table is in the same condition we started with
        DM_COMPANY.CO_FED_TAX_LIABILITIES.ApplyRange;
        DM_COMPANY.CO_FED_TAX_LIABILITIES.LoadState;
      end;
    end;

  begin

    calcBeginDate := GetPeriodBeginDate(MainCheckDate);
    calcEndDate := GetPeriodEndDate(MainCheckDate);
    calcDueDate := GetDueDate(FedFUIFrequency, MainCheckDate);

    if DM_COMPANY.CO.FUI_TAX_DEPOSIT_FREQUENCY.AsString = FREQUENCY_TYPE_ANNUAL then
    begin
      ACHSeqNumber := 0;
      if FUIThresholdMet then
      begin
        calcEndDate := GetEndQuarter(MainCheckDate);
        calcDueDate := GetDueDate(FREQUENCY_TYPE_QUARTERLY, MainCheckDate);
        ACHSeqNumber := ((GetMonth(calcEndDate) - 1) div 3) + 1;
        ChangeFUIFreqFromAnualToQuarterly;
      end;
      UpdateLiability(DM_COMPANY.CO_FED_TAX_LIABILITIES, 0, 99999999, calcBeginDate, calcEndDate, calcDueDate, CurrentPR_NBR, -1,
                                  Fed940Filter, True, -1, FormatDateTime('mm/dd/yyyy', calcEndDate) + Format('%1u', [ACHSeqNumber]));
    end
    else
    begin
      calcDueDate := ctx_PayrollCalculation.CheckDueDateForHoliday(CalculateFUIDueDate2(MainCheckDate),
        ConvertNull(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'], 'SY_GLOBAL_AGENCY_NBR'), 0));
      UpdateLiability(DM_COMPANY.CO_FED_TAX_LIABILITIES, 0, 99999999, GetBeginQuarter(MainCheckDate), GetEndQuarter(MainCheckDate), calcDueDate, CurrentPR_NBR, -1, Fed940Filter);
    end;
  end;

  procedure DoSUI;
  var
    calcBeginDate, calcEndDate, calcDueDate : TDateTime;
  begin
    ctx_PayrollCalculation.GetSuiTaxDepositPeriod(DM_COMPANY.CO_SUI['SY_SUI_NBR'], MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
    UpdateLiability(DM_COMPANY.CO_SUI_LIABILITIES, 0, 99999999, GetBeginQuarter(MainCheckDate), GetEndQuarter(MainCheckDate), calcDueDate, CurrentPR_NBR, -1);
  end;

  procedure FilterNYLocalLiabsForEIN(const EIN: string);
  var
    s: string;
  begin
    with DM_COMPANY, DM_SYSTEM_LOCAL, DM_SYSTEM_STATE do
    begin
      s := 'CO_LOCAL_TAX_NBR = -1';
      CO_LOCAL_TAX.First;
      while not CO_LOCAL_TAX.Eof do
      begin
        if not SY_LOCALS.Locate('SY_LOCALS_NBR', CO_LOCAL_TAX['SY_LOCALS_NBR'], []) then
          raise EInconsistentData.CreateHelp('Local(s) are not configured correctly as of '+ DateToStr(MainCheckDate), IDH_InconsistentData);
        Assert(SY_STATES.Locate('SY_STATES_NBR', SY_LOCALS['SY_STATES_NBR'], []));

             //   MCT Mobility Tax   # 69883
        if (SY_STATES['STATE'] = 'NY')  and
           (ConvertNull(CO_LOCAL_TAX['LOCAL_EIN_NUMBER'], '') = EIN) then
        begin
         if not ((SY_LOCALS['SY_LOCALS_NBR'] =  5724) and
            (CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR'] = 5138)) then
             s := s+ ' or CO_LOCAL_TAX_NBR = '+ IntToStr(CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']);
        end;
        CO_LOCAL_TAX.Next;
      end;
      Assert(CO_LOCAL_TAX_LIABILITIES.Filtered);
      CO_LOCAL_TAX_LIABILITIES.Filter := '('+ s+ ')' + StandartLiabilityFilterCondition;
    end;
  end;

  Function FilterNYLocalLiabs:boolean;
  var
    s: string;
  begin
    Result := True;
    with DM_COMPANY, DM_SYSTEM_LOCAL, DM_SYSTEM_STATE do
    begin
      s := 'CO_LOCAL_TAX_NBR = -1';
      CO_LOCAL_TAX.First;
      while not CO_LOCAL_TAX.Eof do
      begin
        if not SY_LOCALS.Locate('SY_LOCALS_NBR', CO_LOCAL_TAX['SY_LOCALS_NBR'], []) then
          raise EInconsistentData.CreateHelp('Local(s) are not configured correctly as of '+ DateToStr(MainCheckDate), IDH_InconsistentData);
        Assert(SY_STATES.Locate('SY_STATES_NBR', SY_LOCALS['SY_STATES_NBR'], []));

             //   MCT Mobility Tax   # 69883
        if (SY_LOCALS['SY_LOCALS_NBR'] = 5724) and
           (CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR']= 5138) and
           (SY_STATES['STATE'] = 'NY')
         then
         begin
          Result := False;
          s := s+ ' or CO_LOCAL_TAX_NBR = '+ IntToStr(CO_LOCAL_TAX['CO_LOCAL_TAX_NBR']);
         end;
        CO_LOCAL_TAX.Next;
      end;

      Assert(CO_LOCAL_TAX_LIABILITIES.Filtered);

      if CO_LOCAL_TAX_LIABILITIES.Filter <> '' then
          CO_LOCAL_TAX_LIABILITIES.Filter := CO_LOCAL_TAX_LIABILITIES.Filter + ' and ' + '('+ s+ ')' + StandartLiabilityFilterCondition
        else
      CO_LOCAL_TAX_LIABILITIES.Filter := '('+ s+ ')' + StandartLiabilityFilterCondition;
    end;
  end;

var
  sSavedFilter: string;
  bNYFilter : boolean;
  calcBeginDate, calcEndDate, calcDueDate : TDateTime;

begin
  Sleep(1000);
  cdSelectedCo := nil;

  cdStateDepositNbr := TevClientDataset.Create(nil);
  cdStateDepositNbr.FieldDefs.Add('STATE', ftString, 2, True);
  cdStateDepositNbr.FieldDefs.Add('CO_TAX_DEPOSIT_NBR', ftInteger, 0, True);
  cdStateDepositNbr.FieldDefs.Add('EIN', ftString, 15, True);
  cdStateDepositNbr.CreateDataSet;
  cdStateDepositNbr.LogChanges := False;
  cdStateDepositNbr.IndexFieldNames := 'STATE;EIN';

  cdLocalDepositNbr := TevClientDataset.Create(nil);
  cdLocalDepositNbr.FieldDefs.Add('SY_LOCALS_NBR', ftInteger, 0, True);
  cdLocalDepositNbr.FieldDefs.Add('EIN', ftString, 15, True);
  cdLocalDepositNbr.FieldDefs.Add('CO_TAX_DEPOSIT_NBR', ftInteger, 0, True);
  cdLocalDepositNbr.CreateDataSet;
  cdLocalDepositNbr.LogChanges := False;
  cdLocalDepositNbr.IndexFieldNames := 'SY_LOCALS_NBR;EIN';

  lCO := TevClientDataset.Create(nil);
  lCO_STATES := TevClientDataset.Create(nil);
  lCO_SUI := TevClientDataset.Create(nil);
  lCO_LOCAL_TAX := TevClientDataset.Create(nil);

  DM_COMPANY.CO_FED_TAX_LIABILITIES.AlwaysCallCalcFields :=True;
  DM_COMPANY.CO_STATE_TAX_LIABILITIES.AlwaysCallCalcFields :=True;
  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.AlwaysCallCalcFields := True;
  DM_COMPANY.CO_SUI_LIABILITIES.AlwaysCallCalcFields :=True;
  with DM_PAYROLL, DM_COMPANY  do
  try

    lCO.ProviderName := CO.ProviderName;
    lCO.AsOfDate := Trunc(MainCheckDate);
    lCO_STATES.ProviderName := CO_STATES.ProviderName;
    lCO_STATES.AsOfDate := Trunc(MainCheckDate);
    lCO_SUI.ProviderName := CO_SUI.ProviderName;
    lCO_SUI.AsOfDate := Trunc(MainCheckDate);
    lCO_LOCAL_TAX.ProviderName := CO_LOCAL_TAX.ProviderName;
    lCO_LOCAL_TAX.AsOfDate := Trunc(MainCheckDate);
    PopulateDataSets([lCO, lCO_STATES, lCO_SUI, lCO_LOCAL_TAX], '', False);   // ???

    ctx_DataAccess.OpenDataSets([lCO, lCO_STATES, lCO_SUI, lCO_LOCAL_TAX]);
    lCO.IndexFieldNames := 'CO_NBR';
    lCO_STATES.IndexFieldNames := 'CO_STATES_NBR';
    lCO_SUI.IndexFieldNames := 'CO_SUI_NBR';
    lCO_LOCAL_TAX.IndexFieldNames := 'CO_LOCAL_TAX_NBR';

    DM_COMPANY.CO.DataRequired('ALL');
    CO.Filtered := False;
    if CurrentCoNbr > 0 then
    begin
     if not CO.Locate('CO_NBR', CurrentCoNbr, []) then
       raise EInconsistentData.CreateHelp('Can''t locate company...', IDH_InconsistentData)
    end
    else
     if not CO.Locate('CO_NBR', PR['CO_NBR'], []) then
       raise EInconsistentData.CreateHelp('Can''t locate company, current payroll belongs to', IDH_InconsistentData);

    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');
    DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
    DM_CLIENT.CL_CO_CONSOLIDATION.DataRequired('ALL');
    DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.DataRequired('ALL');
    DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');
    DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
    DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.DataRequired('ALL');
    DM_SYSTEM_LOCAL.SY_AGENCY_DEPOSIT_FREQ.DataRequired('ALL');   //   TCD
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');
    DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.DataRequired('ALL');
    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.DataRequired('ALL');

    DM_COMPANY.CO_STATES.DataRequired('ALL');
    DM_COMPANY.CO_STATES.IndexFieldNames := 'CO_NBR';

    DM_COMPANY.CO_SUI.DataRequired('ALL');
    DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
    DM_COMPANY.CO_LOCAL_TAX.IndexFieldNames := 'CO_NBR';
    if not DM_COMPANY.CO_TAX_DEPOSITS.Active then
       CO_TAX_DEPOSITS.DataRequired('CO_TAX_DEPOSITS_NBR=-1');

    // federal part

    FillCdSelectedCo('', '1=1', ' and CO.FEIN = ' + QuotedStr(ConvertNull(CO['FEIN'], '')), '');
    if cdSelectedCo.RecordCount = 0 then
      raise EInvalidParameters.CreateHelp('Can''t load company list. Empty result returned.', IDH_InvalidParameters);

    ctx_PayrollCalculation.GetFedTaxDepositPeriod(FedFrequency[1], MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
    LoadLiabilityTable(CO_FED_TAX_LIABILITIES);

    PrepareTable(CO_FED_TAX_LIABILITIES);
    FilterLiabilityTable(CO_FED_TAX_LIABILITIES, 'CO_NBR');

    FedNextDayDueDate := TisListOfValues.Create;
    FedNextDayEndDate := TisListOfValues.Create;

    try
      if (MainPrNbr > 0) and (CalcTaxType = ALL_TAX_DESC) then
         DoFUI;

      if SbSendsPaymentsForThisCompany then
        DoFederalTaxClient
      else
        if not PayrollExcludedFromTaxDeposit then
          DoFederalNonTaxClient;
    finally
      UnPrepareTable(CO_FED_TAX_LIABILITIES);
    end;

    // state part

    NYStateEndDate := 0;
    cdFiltered := TevClientDataset.Create(nil);
    try
      LoadLiabilityTable(CO_STATE_TAX_LIABILITIES);
      LoadLiabilityTable(CO_LOCAL_TAX_LIABILITIES);
      CO_STATES.Filtered := False;
      cdFiltered.CloneCursor(CO_STATES, False);
      if CalcTaxType = STATE_TAX_DESC then
        cdFiltered.Filter := 'CO_NBR=' + IntToStr(CO['CO_NBR']) + ' and ' + sStateFilter
      else
        cdFiltered.Filter := 'CO_NBR=' + IntToStr(CO['CO_NBR']);

      cdFiltered.Filtered := True;
      PrepareTable(CO_STATE_TAX_LIABILITIES);
      try
        cdFiltered.First;
        while not cdFiltered.Eof do
        begin
          FillCdSelectedCo(',CO_STATES s ', 's.STATE = ''' + cdFiltered['STATE'] +
            ''' and s.CO_NBR=co.CO_NBR and {AsOfNow<s>}', ' and s.STATE_EIN = ' + QuotedStr(ConvertNull(cdFiltered['STATE_EIN'], '')),
            ', CO_STATES_NBR');
          if cdSelectedCo.RecordCount = 0 then
            raise EInvalidParameters.CreateHelp('Can''t load company list. Empty result returned.', IDH_InvalidParameters);

          Assert(CO_STATES.Locate('CO_STATES_NBR', cdFiltered['CO_STATES_NBR'], []));
          Assert(DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR', CO_STATES['SY_STATES_NBR'], []));
          if cdFiltered['STATE'] = 'NY' then // load local liabs for NY counties
          begin
            PrepareTable(CO_LOCAL_TAX_LIABILITIES);
            FilterNYLocalLiabsForEIN(cdFiltered['STATE_EIN']);
          end;

          ctx_PayrollCalculation.GetStateTaxDepositPeriod(CurrentState, StateFrequency, MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
          if DM_SYSTEM_STATE.SY_STATES['PAY_SDI_WITH'] <> GROUP_BOX_YES then
          begin
            FilterLiabilityTable(CO_STATE_TAX_LIABILITIES, 'CO_STATES_NBR');
            try
              if cdFiltered['STATE'] = 'NM' then
              begin
                sSavedFilter := CO_STATE_TAX_LIABILITIES.Filter;
                CO_STATE_TAX_LIABILITIES.Filter := sSavedFilter + ' and TAX_TYPE ='''+ TAX_LIABILITY_TYPE_STATE+ '''';
                if SbSendsPaymentsForThisCompany then
                  DoStateTaxClient
                else
                  if not PayrollExcludedFromTaxDeposit then
                    DoStateNonTaxClient;
                CO_STATE_TAX_LIABILITIES.Filter := sSavedFilter + ' and TAX_TYPE <>'''+ TAX_LIABILITY_TYPE_STATE+ '''';
                if SbSendsPaymentsForThisCompany then
                  DoStateTaxClient(True);
              end
              else
              begin
                if SbSendsPaymentsForThisCompany then
                  DoStateTaxClient
                else
                  if not PayrollExcludedFromTaxDeposit then
                  DoStateNonTaxClient;
              end;
            finally
              if cdFiltered['STATE'] = 'NY' then // unload local liabs for NY counties
                UnPrepareTable(CO_LOCAL_TAX_LIABILITIES);
            end;
          end
          else
          begin
            FilterLiabilityTable(CO_STATE_TAX_LIABILITIES, 'CO_STATES_NBR');
            try
              sSavedFilter := CO_STATE_TAX_LIABILITIES.Filter;
              CO_STATE_TAX_LIABILITIES.Filter := sSavedFilter + ' and TAX_TYPE ='''+ TAX_LIABILITY_TYPE_STATE+ '''';
              if SbSendsPaymentsForThisCompany then
                DoStateTaxClient
              else
                if not PayrollExcludedFromTaxDeposit then
                  DoStateNonTaxClient(True);
              CO_STATE_TAX_LIABILITIES.Filter := sSavedFilter + ' and TAX_TYPE <>'''+ TAX_LIABILITY_TYPE_STATE+ '''';

              DoStateTaxClient(True);
            finally
              if cdFiltered['STATE'] = 'NY' then // unload local liabs for NY counties
                UnPrepareTable(CO_LOCAL_TAX_LIABILITIES);
            end;
          end;
          cdFiltered.Next;
        end;
      finally
        cdFiltered.Filtered := False;
        cdFiltered.Close;
        UnPrepareTable(CO_STATE_TAX_LIABILITIES);
      end;

      //    SUI part

      if (MainPrNbr > 0) and (CalcTaxType = ALL_TAX_DESC) then
      begin
          LoadLiabilityTable(CO_SUI_LIABILITIES);
          CO_SUI.Filtered := False;
          cdFiltered.CloneCursor(CO_SUI, False);
          cdFiltered.Filter := 'CO_NBR='+ IntToStr(CO['CO_NBR']);
          cdFiltered.Filtered := True;
          PrepareTable(CO_SUI_LIABILITIES);
          try
            cdFiltered.First;
            while not cdFiltered.Eof do
            begin
              FillCdSelectedCo(',CO_STATES s, CO_SUI sui ', 'sui.SY_SUI_NBR = '''+
                IntToStr(cdFiltered['SY_SUI_NBR'])+ ''' and s.CO_NBR=co.CO_NBR and sui.CO_STATES_NBR=s.CO_STATES_NBR and {AsOfNow<s>} and {AsOfNow<sui>}',
                ' and s.SUI_EIN = '+ QuotedStr(ConvertNull(CO_STATES.Lookup('CO_STATES_NBR', cdFiltered['CO_STATES_NBR'], 'SUI_EIN'), '')),
                ', CO_SUI_NBR');
              if cdSelectedCo.RecordCount > 0 then
              begin

                Assert(CO_SUI.Locate('CO_SUI_NBR', cdFiltered['CO_SUI_NBR'], []));
                Assert(CO_STATES.Locate('CO_STATES_NBR', CO_SUI['CO_STATES_NBR'], []));
                ctx_PayrollCalculation.GetStateTaxDepositPeriod (CurrentState, StateFrequency, MainCheckDate, calcBeginDate,calcEndDate, calcDueDate) ;
                FilterLiabilityTable(CO_SUI_LIABILITIES, 'CO_SUI_NBR');

                DoSUI;
              end;
              cdFiltered.Next;
            end;
          finally
            cdFiltered.Filtered := False;
            cdFiltered.Close;
            UnPrepareTable(CO_SUI_LIABILITIES);
          end;
      end;

      // local part

      cdTCDLocals:= CreatecdTCDLocalsDataset;    //  TCD

      if (CalcTaxType in [ALL_TAX_DESC, LOCAL_TAX_DESC]) then
      begin
          CO_LOCAL_TAX.Filtered := False;
          cdFiltered.CloneCursor(CO_LOCAL_TAX, False);
          cdFiltered.Filter := 'CO_NBR=' + IntToStr(CO['CO_NBR']);
          cdFiltered.Filtered := True;
          PrepareTable(CO_LOCAL_TAX_LIABILITIES);
          try
            cdFiltered.First;
            while not cdFiltered.Eof do
            begin
                Assert(DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', cdFiltered['SY_LOCALS_NBR'], []));
                Assert(DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR', DM_SYSTEM_LOCAL.SY_LOCALS['SY_STATES_NBR'], []));
                FillCdSelectedCo(',CO_LOCAL_TAX s ', 's.SY_LOCALS_NBR=' + IntToStr(cdFiltered['SY_LOCALS_NBR']) +
                    ' and s.CO_NBR=co.CO_NBR and {AsOfNow<s>}', ' and s.LOCAL_EIN_NUMBER = ' + QuotedStr(ConvertNull(cdFiltered['LOCAL_EIN_NUMBER'], '')),
                    ', CO_LOCAL_TAX_NBR');
                if cdSelectedCo.RecordCount = 0 then
                  raise EInvalidParameters.CreateHelp('Can''t load company list. Empty result returned.', IDH_InvalidParameters);

                Assert(CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', cdFiltered['CO_LOCAL_TAX_NBR'], []));
                ctx_PayrollCalculation.GetLocalTaxDepositPeriod(SyLocalNbr, LocalFrequency, MainCheckDate, calcBeginDate, calcEndDate, calcDueDate);
                FilterLiabilityTable(CO_LOCAL_TAX_LIABILITIES, 'CO_LOCAL_TAX_NBR');

                bNYFilter := false;
                if DM_SYSTEM_STATE.SY_STATES['STATE'] = 'NY' then
                begin
                bNYFilter := True;
                  if cdFiltered['SY_LOCALS_NBR'] = 5724 then
                  begin
                   bNYFilter:= FilterNYLocalLiabs;
                   Assert(CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', cdFiltered['CO_LOCAL_TAX_NBR'], []));
                  end;
                end;

                if SbSendsPaymentsForThisCompany then
                  DoLocalTaxClient(bNYFilter)
                else
                  if not PayrollExcludedFromTaxDeposit then
                  DoLocalNonTaxClient(bNYFilter);
              cdFiltered.Next;
            end;
          finally
            cdFiltered.Filtered := False;
            cdFiltered.Close;
            UnPrepareTable(CO_LOCAL_TAX_LIABILITIES);
          end;
      end;
    finally
      cdFiltered.Free;
    end;

    if MainPrNbr < 0 then
    ctx_DataAccess.PostDataSets(
      [
        DM_COMPANY.CO,
        DM_COMPANY.CO_STATES,
        DM_COMPANY.CO_LOCAL_TAX,
        DM_COMPANY.CO_FED_TAX_LIABILITIES,
        DM_COMPANY.CO_STATE_TAX_LIABILITIES,
        DM_COMPANY.CO_SUI_LIABILITIES,
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES
        ])
    else
    ctx_DataAccess.PostDataSets(
      [
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS,
        DM_PAYROLL.PR,
        DM_COMPANY.CO,
        DM_COMPANY.CO_STATES,
        DM_COMPANY.CO_LOCAL_TAX,
        DM_COMPANY.CO_TAX_DEPOSITS,
        DM_COMPANY.CO_FED_TAX_LIABILITIES,
        DM_COMPANY.CO_STATE_TAX_LIABILITIES,
        DM_COMPANY.CO_SUI_LIABILITIES,
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
        DM_CLIENT.CL_BANK_ACCOUNT,
        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS
        ]);
  finally
    cdSelectedCo.Free;
    cdStateDepositNbr.Free;
    cdLocalDepositNbr.Free;    
    lCO.Free;
    lCO_STATES.Free;
    lCO_SUI.Free;
    lCO_LOCAL_TAX.Free;

    DM_COMPANY.CO_FED_TAX_LIABILITIES.AlwaysCallCalcFields :=false;
    DM_COMPANY.CO_STATE_TAX_LIABILITIES.AlwaysCallCalcFields :=false;
    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.AlwaysCallCalcFields :=false;
    DM_COMPANY.CO_SUI_LIABILITIES.AlwaysCallCalcFields :=false;
  end;
end;

end.

