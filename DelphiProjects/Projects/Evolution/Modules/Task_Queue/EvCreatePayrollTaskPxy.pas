unit EvCreatePayrollTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvConsts, EvTypes, EvMainboard, EvClasses;

implementation

type
  TEvCreatePayrollTaskProxy = class(TEvCustomTaskProxy, IevCreatePayrollTask)
  private
    FAutoCommit: Boolean;
    FTcAutoImportJobCodes: Boolean;
    FBatchFreq: String;
    FBlock401K: Integer;
    FBlockACH: Integer;
    FBlockAgencies: Integer;
    FBlockBilling: Integer;
    FBlockChecks: Integer;
    FBlockTaxDeposits: Integer;
    FBlockTimeOff: Integer;
    FCheckComments: String;
    FCheckDate: TDateTime;
    FCheckDateReplace: Integer;
    FCheckFreq: String;
    FCLNbr: Integer;
    FCONbr: Integer;
    FEEList: string;
    FLoadDefaults: Integer;
    FPayHours: Integer;
    FPaySalary: Integer;
    FPeriodBeginDate: TDateTime;
    FPeriodEndDate: TDateTime;
    FPRFilterNbr: Integer;
    FPrList: string;
    FPRTemplateNbr: Integer;
    FSalaryOrHourly: Integer;
    FTcDBDTOption: TCImportDBDTOption;
    FTcFileFormat: TCFileFormatOption;
    FTcFourDigitYear: Boolean;
    FTcImportSource: string;
    FTcImportSourceFile: IevDualStream;
    FTcLookupOption: TCImportLookupOption;
    FTcUseEmployeePayRates: Boolean;
    FIncludeTimeOffRequests: Boolean;
  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function   Revision: TisRevision; override;
    function GetAutoCommit: Boolean;
    function GetTcAutoImportJobCodes: Boolean;
    function GetBatchFreq: String;
    function GetBlock401K: Integer;
    function GetBlockACH: Integer;
    function GetBlockAgencies: Integer;
    function GetBlockBilling: Integer;
    function GetBlockChecks: Integer;
    function GetBlockTaxDeposits: Integer;
    function GetBlockTimeOff: Integer;
    function GetCheckComments: String;
    function GetCheckDate: TDateTime;
    function GetCheckDateReplace: Integer;
    function GetCheckFreq: String;
    function GetCLNbr: Integer;
    function GetCONbr: Integer;
    function GetEEList: string;
    function GetLoadDefaults: Integer;
    function GetPayHours: Integer;
    function GetPaySalary: Integer;
    function GetPeriodBeginDate: TDateTime;
    function GetPeriodEndDate: TDateTime;
    function GetPRFilterNbr: Integer;
    function GetPrList: string;
    function GetPRTemplateNbr: Integer;
    function GetSalaryOrHourly: Integer;
    function GetTcDBDTOption: TCImportDBDTOption;
    function GetTcFileFormat: TCFileFormatOption;
    function GetTcFourDigitYear: Boolean;
    function GetTcImportSource: string;
    function GetTcImportSourceFile: IevDualStream;
    function GetTcLookupOption: TCImportLookupOption;
    function GetTcUseEmployeePayRates: Boolean;
    function GetIncludeTimeOffRequests: Boolean;
    procedure SetAutoCommit(const AValue: Boolean);
    procedure SetTcAutoImportJobCodes(const AValue: Boolean);
    procedure SetBatchFreq(const AValue: String);
    procedure SetBlock401K(const AValue: Integer);
    procedure SetBlockACH(const AValue: Integer);
    procedure SetBlockAgencies(const AValue: Integer);
    procedure SetBlockBilling(const AValue: Integer);
    procedure SetBlockChecks(const AValue: Integer);
    procedure SetBlockTaxDeposits(const AValue: Integer);
    procedure SetBlockTimeOff(const AValue: Integer);
    procedure SetCheckComments(const AValue: String);
    procedure SetCheckDate(const AValue: TDateTime);
    procedure SetCheckDateReplace(const AValue: Integer);
    procedure SetCheckFreq(const AValue: String);
    procedure SetCLNbr(const AValue: Integer);
    procedure SetCONbr(const AValue: Integer);
    procedure SetEEList(const AValue: string);
    procedure SetLoadDefaults(const AValue: Integer);
    procedure SetPayHours(const AValue: Integer);
    procedure SetPaySalary(const AValue: Integer);
    procedure SetPeriodBeginDate(const AValue: TDateTime);
    procedure SetPeriodEndDate(const AValue: TDateTime);
    procedure SetPRFilterNbr(const AValue: Integer);
    procedure SetPrList(const AValue: string);
    procedure SetPRTemplateNbr(const AValue: Integer);
    procedure SetSalaryOrHourly(const AValue: Integer);
    procedure SetTcDBDTOption(const AValue: TCImportDBDTOption);
    procedure SetTcFileFormat(const AValue: TCFileFormatOption);
    procedure SetTcFourDigitYear(const AValue: Boolean);
    procedure SetTcImportSource(const AValue: string);
    procedure SetTcImportSourceFile(const AValue: IevDualStream);
    procedure SetTcLookupOption(const AValue: TCImportLookupOption);
    procedure SetTcUseEmployeePayRates(const AValue: Boolean);
    procedure SetIncludeTimeOffRequests(const AValue: Boolean);
  public
    class function GetTypeID: String; override;
  end;

{ TEvCreatePayrollTaskProxy }

function TEvCreatePayrollTaskProxy.GetAutoCommit: Boolean;
begin
  Result := FAutoCommit;
end;

function TEvCreatePayrollTaskProxy.GetTcAutoImportJobCodes: Boolean;
begin
  Result := FTcAutoImportJobCodes;
end;

function TEvCreatePayrollTaskProxy.GetBatchFreq: String;
begin
  Result := FBatchFreq;
end;

function TEvCreatePayrollTaskProxy.GetBlock401K: Integer;
begin
  Result := FBlock401K;
end;

function TEvCreatePayrollTaskProxy.GetBlockACH: Integer;
begin
  Result := FBlockACH;
end;

function TEvCreatePayrollTaskProxy.GetBlockAgencies: Integer;
begin
  Result := FBlockAgencies;
end;

function TEvCreatePayrollTaskProxy.GetBlockBilling: Integer;
begin
  Result := FBlockBilling;
end;

function TEvCreatePayrollTaskProxy.GetBlockChecks: Integer;
begin
  Result := FBlockChecks;
end;

function TEvCreatePayrollTaskProxy.GetBlockTaxDeposits: Integer;
begin
  Result := FBlockTaxDeposits;
end;

function TEvCreatePayrollTaskProxy.GetBlockTimeOff: Integer;
begin
  Result := FBlockTimeOff;
end;

function TEvCreatePayrollTaskProxy.GetCheckComments: String;
begin
  Result := FCheckComments;
end;

function TEvCreatePayrollTaskProxy.GetCheckDate: TDateTime;
begin
  Result := FCheckDate;
end;

function TEvCreatePayrollTaskProxy.GetCheckDateReplace: Integer;
begin
  Result := FCheckDateReplace;
end;

function TEvCreatePayrollTaskProxy.GetCheckFreq: String;
begin
  Result := FCheckFreq;
end;

function TEvCreatePayrollTaskProxy.GetCLNbr: Integer;
begin
  Result := FCLNbr;
end;

function TEvCreatePayrollTaskProxy.GetCONbr: Integer;
begin
  Result := FCONbr;
end;

function TEvCreatePayrollTaskProxy.GetEEList: string;
begin
  Result := FEEList;
end;

function TEvCreatePayrollTaskProxy.GetLoadDefaults: Integer;
begin
  Result := FLoadDefaults;
end;

function TEvCreatePayrollTaskProxy.GetPayHours: Integer;
begin
  Result := FPayHours;
end;

function TEvCreatePayrollTaskProxy.GetPaySalary: Integer;
begin
  Result := FPaySalary;
end;

function TEvCreatePayrollTaskProxy.GetPeriodBeginDate: TDateTime;
begin
  Result := FPeriodBeginDate;
end;

function TEvCreatePayrollTaskProxy.GetPeriodEndDate: TDateTime;
begin
  Result := FPeriodEndDate;
end;

function TEvCreatePayrollTaskProxy.GetPRFilterNbr: Integer;
begin
  Result := FPRFilterNbr;
end;

function TEvCreatePayrollTaskProxy.GetPrList: string;
begin
  Result := FPrList;
end;

function TEvCreatePayrollTaskProxy.GetPRTemplateNbr: Integer;
begin
  Result := FPRTemplateNbr;
end;

function TEvCreatePayrollTaskProxy.GetSalaryOrHourly: Integer;
begin
  Result := FSalaryOrHourly;
end;

function TEvCreatePayrollTaskProxy.GetTcDBDTOption: TCImportDBDTOption;
begin
  Result := FTcDBDTOption;
end;

function TEvCreatePayrollTaskProxy.GetTcFileFormat: TCFileFormatOption;
begin
  Result := FTcFileFormat;
end;

function TEvCreatePayrollTaskProxy.GetTcFourDigitYear: Boolean;
begin
  Result := FTcFourDigitYear;
end;

function TEvCreatePayrollTaskProxy.GetTcImportSource: string;
begin
  Result := FTcImportSource;
end;

function TEvCreatePayrollTaskProxy.GetTcImportSourceFile: IevDualStream;
begin
  Result := FTcImportSourceFile;
end;

function TEvCreatePayrollTaskProxy.GetTcLookupOption: TCImportLookupOption;
begin
  Result := FTcLookupOption;
end;

function TEvCreatePayrollTaskProxy.GetTcUseEmployeePayRates: Boolean;
begin
  Result := FTcUseEmployeePayRates;
end;

class function TEvCreatePayrollTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_CREATE_PAYROLL;
end;


procedure TEvCreatePayrollTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FAutoCommit := AStream.ReadBoolean;
  FTcAutoImportJobCodes := AStream.ReadBoolean;
  FBatchFreq := AStream.ReadString;
  FBlock401K := AStream.ReadInteger;
  FBlockACH := AStream.ReadInteger;
  FBlockAgencies := AStream.ReadInteger;
  FBlockBilling := AStream.ReadInteger;
  FBlockChecks := AStream.ReadInteger;
  FBlockTaxDeposits := AStream.ReadInteger;
  FBlockTimeOff := AStream.ReadInteger;
  FCheckComments := AStream.ReadString;
  FCheckDate := AStream.ReadDouble;
  FCheckDateReplace := AStream.ReadInteger;
  FCheckFreq := AStream.ReadString;
  FCLNbr := AStream.ReadInteger;
  FCONbr := AStream.ReadInteger;
  FEEList := AStream.ReadString;
  FLoadDefaults := AStream.ReadInteger;
  FPayHours := AStream.ReadInteger;
  FPaySalary := AStream.ReadInteger;
  FPeriodBeginDate := AStream.ReadDouble;
  FPeriodEndDate := AStream.ReadDouble;
  FPRFilterNbr := AStream.ReadInteger;
  FPrList := AStream.ReadString;
  FPRTemplateNbr := AStream.ReadInteger;
  FSalaryOrHourly := AStream.ReadInteger;
  FTcDBDTOption := TCImportDBDTOption(AStream.ReadInteger);
  FTcFileFormat := TCFileFormatOption(AStream.ReadInteger);
  FTcFourDigitYear := AStream.ReadBoolean;
  FTcImportSource := AStream.ReadString;
  FTcImportSourceFile := AStream.ReadStream(nil, True);
  FTcLookupOption := TCImportLookupOption(AStream.ReadInteger);
  FTcUseEmployeePayRates := AStream.ReadBoolean;
  if ARevision >= 7 then
    FIncludeTimeOffRequests := AStream.ReadBoolean
  else
    FIncludeTimeOffRequests := False;
end;

procedure TEvCreatePayrollTaskProxy.SetAutoCommit(const AValue: Boolean);
begin
  FAutoCommit := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcAutoImportJobCodes(const AValue: Boolean);
begin
  FTcAutoImportJobCodes := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBatchFreq(const AValue: String);
begin
  FBatchFreq := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlock401K(const AValue: Integer);
begin
  FBlock401K := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlockACH(const AValue: Integer);
begin
  FBlockACH := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlockAgencies(const AValue: Integer);
begin
  FBlockAgencies := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlockBilling(const AValue: Integer);
begin
  FBlockBilling := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlockChecks(const AValue: Integer);
begin
  FBlockChecks := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlockTaxDeposits(const AValue: Integer);
begin
  FBlockTaxDeposits := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetBlockTimeOff(const AValue: Integer);
begin
  FBlockTimeOff := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetCheckComments(const AValue: String);
begin
  FCheckComments := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetCheckDate(const AValue: TDateTime);
begin
  FCheckDate := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetCheckDateReplace(const AValue: Integer);
begin
  FCheckDateReplace := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetCheckFreq(const AValue: String);
begin
  FCheckFreq := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetCLNbr(const AValue: Integer);
begin
  FCLNbr := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetCONbr(const AValue: Integer);
begin
  FCONbr := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetEEList(const AValue: string);
begin
  FEEList := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetLoadDefaults(const AValue: Integer);
begin
  FLoadDefaults := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPayHours(const AValue: Integer);
begin
  FPayHours := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPaySalary(const AValue: Integer);
begin
  FPaySalary := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPeriodBeginDate(const AValue: TDateTime);
begin
  FPeriodBeginDate := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPeriodEndDate(const AValue: TDateTime);
begin
  FPeriodEndDate := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPRFilterNbr(const AValue: Integer);
begin
  FPRFilterNbr := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPrList(const AValue: string);
begin
  FPrList := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetPRTemplateNbr(const AValue: Integer);
begin
  FPRTemplateNbr := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetSalaryOrHourly(const AValue: Integer);
begin
  FSalaryOrHourly := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcDBDTOption(const AValue: TCImportDBDTOption);
begin
  FTcDBDTOption := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcFileFormat(const AValue: TCFileFormatOption);
begin
  FTcFileFormat := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcFourDigitYear(const AValue: Boolean);
begin
  FTcFourDigitYear := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcImportSource(const AValue: string);
begin
  FTcImportSource := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcImportSourceFile(const AValue: IevDualStream);
begin
  FTcImportSourceFile := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcLookupOption(const AValue: TCImportLookupOption);
begin
  FTcLookupOption := AValue;
end;

procedure TEvCreatePayrollTaskProxy.SetTcUseEmployeePayRates(const AValue: Boolean);
begin
  FTcUseEmployeePayRates := AValue;
end;

procedure TEvCreatePayrollTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FAutoCommit);
  AStream.WriteBoolean(FTcAutoImportJobCodes);
  AStream.WriteString(FBatchFreq);
  AStream.WriteInteger(FBlock401K);
  AStream.WriteInteger(FBlockACH);
  AStream.WriteInteger(FBlockAgencies);
  AStream.WriteInteger(FBlockBilling);
  AStream.WriteInteger(FBlockChecks);
  AStream.WriteInteger(FBlockTaxDeposits);
  AStream.WriteInteger(FBlockTimeOff);
  AStream.WriteString(FCheckComments);
  AStream.WriteDouble(FCheckDate);
  AStream.WriteInteger(FCheckDateReplace);
  AStream.WriteString(FCheckFreq);
  AStream.WriteInteger(FCLNbr);
  AStream.WriteInteger(FCONbr);
  AStream.WriteString(FEEList);
  AStream.WriteInteger(FLoadDefaults);
  AStream.WriteInteger(FPayHours);
  AStream.WriteInteger(FPaySalary);
  AStream.WriteDouble(FPeriodBeginDate);
  AStream.WriteDouble(FPeriodEndDate);
  AStream.WriteInteger(FPRFilterNbr);
  AStream.WriteString(FPrList);
  AStream.WriteInteger(FPRTemplateNbr);
  AStream.WriteInteger(FSalaryOrHourly);
  AStream.WriteInteger(Ord(FTcDBDTOption));
  AStream.WriteInteger(Ord(FTcFileFormat));
  AStream.WriteBoolean(FTcFourDigitYear);
  AStream.WriteString(FTcImportSource);
  AStream.WriteStream(FTcImportSourceFile);
  AStream.WriteInteger(Ord(FTcLookupOption));
  AStream.WriteBoolean(FTcUseEmployeePayRates);
  AStream.WriteBoolean(FIncludeTimeOffRequests);
end;

function TEvCreatePayrollTaskProxy.Revision: TisRevision;
begin
  Result := 7;
end;

function TEvCreatePayrollTaskProxy.GetIncludeTimeOffRequests: Boolean;
begin
  Result := FIncludeTimeOffRequests;
end;

procedure TEvCreatePayrollTaskProxy.SetIncludeTimeOffRequests(
  const AValue: Boolean);
begin
  FIncludeTimeOffRequests := AValue;
end;

initialization
  ObjectFactory.Register(TEvCreatePayrollTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvCreatePayrollTaskProxy);

end.
