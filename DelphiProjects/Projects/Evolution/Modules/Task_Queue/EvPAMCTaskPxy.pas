unit EvPAMCTaskPxy;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTaskPxy, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, evClasses;

implementation

type
  TevPAMCTaskProxy = class(TEvPrintableTaskProxy, IevPAMCTask)
  private
    FClNbr: Integer;
    FCoNbr: Integer;
    FMonthEndDate: TDateTime;
    FMinTax: Double;
    FAutoProcess: Boolean;
    FEECustomNbr: String;
    FPrintReports: Boolean;
    FFilter: IevQECTaskFilter;
    FResults: IevQECTaskResult;
  protected
    procedure  DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetClNbr: Integer;
    procedure SetClNbr(const AValue: Integer);
    function  GetCoNbr: Integer;
    procedure SetCoNbr(const AValue: Integer);
    function  GetMonthEndDate: TDateTime;
    procedure SetMonthEndDate(const AValue: TDateTime);
    function  GetMinTax: Double;
    procedure SetMinTax(const AValue: Double);
    function  GetAutoProcess: Boolean;
    procedure SetAutoProcess(const AValue: Boolean);
    function  GetEECustomNbr: String;
    procedure SetEECustomNbr(const AValue: String);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetFilter: IevQECTaskFilter;
  public
    class function GetTypeID: String; override;
  end;


{ TevPAMCTaskProxy }
procedure TevPAMCTaskProxy.DoOnConstruction;
begin
  inherited;
  FFilter := TevQECTaskFilter.Create;
end;

function TevPAMCTaskProxy.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevPAMCTaskProxy.GetCoNbr: Integer;
begin
  Result := FCoNbr;
end;

function TevPAMCTaskProxy.GetEECustomNbr: String;
begin
  Result := FEECustomNbr;
end;

function TevPAMCTaskProxy.GetMinTax: Double;
begin
  Result := FMinTax;
end;

function TevPAMCTaskProxy.GetPrintReports: Boolean;
begin
  Result := FPrintReports;
end;

function TevPAMCTaskProxy.GetFilter: IevQECTaskFilter;
begin
  Result := FFilter;
end;

class function TevPAMCTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PAMC;
end;

procedure TevPAMCTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoNbr := AStream.ReadInteger;
  FMonthEndDate := AStream.ReadDouble;
  FMinTax := AStream.ReadDouble;
  FAutoProcess := AStream.ReadBoolean;
  FEECustomNbr := AStream.ReadString;
  FPrintReports := AStream.ReadBoolean;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevQECTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TevPAMCTaskProxy.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevPAMCTaskProxy.SetCoNbr(const AValue: Integer);
begin
  FCoNbr := AValue;
end;

procedure TevPAMCTaskProxy.SetEECustomNbr(const AValue: String);
begin
  FEECustomNbr := AValue;
end;

procedure TevPAMCTaskProxy.SetMinTax(const AValue: Double);
begin
  FMinTax := AValue;
end;

procedure TevPAMCTaskProxy.SetPrintReports(const AValue: Boolean);
begin
  FPrintReports := AValue;
end;

procedure TevPAMCTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteInteger(FCoNbr);
  AStream.WriteDouble(FMonthEndDate);
  AStream.WriteDouble(FMinTax);
  AStream.WriteBoolean(FAutoProcess);
  AStream.WriteString(FEECustomNbr);
  AStream.WriteBoolean(FPrintReports);

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

function TevPAMCTaskProxy.GetAutoProcess: Boolean;
begin
  Result := FAutoProcess;
end;

function TevPAMCTaskProxy.GetMonthEndDate: TDateTime;
begin
  Result := FMonthEndDate;
end;

procedure TevPAMCTaskProxy.SetAutoProcess(const AValue: Boolean);
begin
  FAutoProcess := AValue;
end;

procedure TevPAMCTaskProxy.SetMonthEndDate(const AValue: TDateTime);
begin
  FMonthEndDate := AValue;
end;

initialization
  ObjectFactory.Register(TevPAMCTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TevPAMCTaskProxy);

end.
