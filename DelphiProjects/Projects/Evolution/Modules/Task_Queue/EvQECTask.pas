unit EvQECTask;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTask, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, EvClasses;

implementation

type
  TevQECTask = class(TEvPrintableTask, IevQECTask)
  private
    FClNbr: Integer;
    FCoNbr: Integer;
    FQTREndDate: TDateTime;
    FMinTax: Double;
    FCleanupAction: Integer;
    FEECustomNbr: String;
    FForceCleanup: Boolean;
    FPrintReports: Boolean;
    FCurrentQuarterOnly: Boolean;
    FExtraOptions: String;
    FFilter: IevQECTaskFilter;
    FResults: IevQECTaskResult;
    FNextFilterItem: Integer;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure DoStoreTaskState(const AStream: IevDualStream); override;
    procedure DoRestoreTaskState(const AStream: IevDualStream); override;

    function  GetClNbr: Integer;
    procedure SetClNbr(const AValue: Integer);
    function  GetCoNbr: Integer;
    procedure SetCoNbr(const AValue: Integer);
    function  GetQTREndDate: TDateTime;
    procedure SetQTREndDate(const AValue: TDateTime);
    function  GetMinTax: Double;
    procedure SetMinTax(const AValue: Double);
    function  GetCleanupAction: Integer;
    procedure SetCleanupAction(const AValue: Integer);
    function  GetEECustomNbr: String;
    procedure SetEECustomNbr(const AValue: String);
    function  GetForceCleanup: Boolean;
    procedure SetForceCleanup(const AValue: Boolean);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetCurrentQuarterOnly: Boolean;
    procedure SetCurrentQuarterOnly(const AValue: Boolean);
    function  GetExtraOptions: String;
    procedure SetExtraOptions(const AValue: String);
    function  GetFilter: IevQECTaskFilter;
    function  GetResults: IevQECTaskResult;    

    procedure RegisterTaskPhases; override;
    procedure Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure Phase_RunReports(const AInputRequests: TevTaskRequestList);

    procedure  Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);
    function   Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues; override;


  public
    class function GetTypeID: String; override;
  end;


{ TevQECTask }

procedure TevQECTask.DoOnConstruction;
begin
  inherited;
  FFilter := TevQECTaskFilter.Create;
  SetDestination(rdtRemotePrinter);
end;
procedure TevQECTask.DoRestoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  FNextFilterItem := AStream.ReadInteger;


  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevQECTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;

end;

procedure TevQECTask.DoStoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  AStream.WriteInteger(FNextFilterItem);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);

end;

function TevQECTask.GetCleanupAction: Integer;
begin
  Result := FCleanupAction;
end;

function TevQECTask.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevQECTask.GetCoNbr: Integer;
begin
  Result := FCoNbr;
end;

function TevQECTask.GetCurrentQuarterOnly: Boolean;
begin
  Result := FCurrentQuarterOnly;
end;

function TevQECTask.GetEECustomNbr: String;
begin
  Result := FEECustomNbr;
end;

function TevQECTask.GetExtraOptions: String;
begin
  Result := FExtraOptions;
end;

function TevQECTask.GetForceCleanup: Boolean;
begin
  Result := FForceCleanup;
end;

function TevQECTask.GetMinTax: Double;
begin
  Result := FMinTax;
end;

function TevQECTask.GetPrintReports: Boolean;
begin
  Result := FPrintReports;
end;

function TevQECTask.GetQTREndDate: TDateTime;
begin
  Result := FQTREndDate;
end;

class function TevQECTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_QEC;
end;

procedure TevQECTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
begin
  inherited;
  FNextFilterItem := 0;
  FResults := TevQECTaskResult.Create;
  Phase_GoToNextCompany(AInputRequests);

end;


procedure TevQECTask.Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
  V: Variant;
  Flt: IevQECTaskFilterItem;
  i, n, j: Integer;
  Res: IevQECTaskResultItem;
  S: IevDualStream;
begin

  if Length(AInputRequests) = 1 then
    if AnsiSameText(AInputRequests[0].TaskRoutineName, 'QuarterEndCleanup') then
    begin
      AddLogEvent('Phase "RunReports" activated');
      Phase_RunReports(AInputRequests) // processing is done, go to run report
    end
    else
      if FNextFilterItem > 0 then          // collect results
        for i := Low(AInputRequests) to High(AInputRequests) do
        begin
          if AInputRequests[0].GetExceptions = '' then
          begin
            Res := FResults.Add;

            S := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IevDualStream;
            if Assigned(S) then
              Res.SetQECReports(S);
          end;
        end;

  // continue to add as many requests as GetMaxThreads specifies
  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    if FNextFilterItem >= FFilter.Count then
      break;

     Flt := FFilter[FNextFilterItem];

     Params := TisListOfValues.Create;
     Params.AddValue('ClientID', Flt.ClNbr);
     Params.AddValue('CoNbr', Flt.CoNbr);

     Params.AddValue('QTREndDate', FQTREndDate);
     Params.AddValue('MinTax', FMinTax);
     Params.AddValue('CleanupAction', FCleanupAction);
     Params.AddValue('ReportFilePath', '');
     Params.AddValue('EECustomNbr', FEECustomNbr);
     Params.AddValue('ForceCleanup', FForceCleanup);
     Params.AddValue('PrintReports', FPrintReports);
     Params.AddValue('CurrentQuarterOnly', FCurrentQuarterOnly);
     Params.AddValue('LockType', Ord(rlDoNotLock));

     V := VarArrayCreate([0, Length(FExtraOptions) - 1], varVariant);

     for j := 1 to Length(FExtraOptions) do
       V[j-1] := FExtraOptions[j];

     Params.AddValue('ExtraOptions', V);

     AddRequest(Flt.Caption, 'QuarterEndCleanup', Params, 'GoToNextCompany.Fork',
       GF_CL_PAYROLL_OPERATION + '.' + IntToStr(Flt.ClNbr) + ',' +
       GF_CO_PAYROLL_OPERATION + '.' + IntToStr(Flt.ClNbr) + '_' + IntToStr(Flt.CoNbr));

    Inc(FNextFilterItem);
  end;

  // Are all companies processed?
  if (FNextFilterItem >= FFilter.Count) and AllRequestsAreFinished then
  begin
    Params := TisListOfValues.Create;
    AddRequest('Process Report Results', 'ProcessReportResults', Params, 'PostProcessReportResults.Merge');
  end;

end;


procedure TevQECTask.Phase_RunReports(const AInputRequests: TevTaskRequestList);
var
  i : Integer;
  Params: IisListOfValues;
  R: IevTaskRequest;
begin

  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
   R := AInputRequests[i];

   if R.GetExceptions = '' then
   begin
    // If there are no errors
    AddWarning(R.GetWarnings);

    if not R.Results.Value[PARAM_RESULT] then
    begin
      if TCleanupPayrollAction(FCleanupAction) = cpaProcess then
        AddWarning('Quarter End Cleanup payroll was not processed due to pending EE tax adjustments or previous Quarter End Cleanup was not finished.')
      else if TCleanupPayrollAction(FCleanupAction) = cpaQueue then
        AddWarning('Quarter End Cleanup payroll was not sent to queue due to pending EE tax adjustments or previous Quarter End Cleanup was not finished.');
    end
    else
      AddNotes('Quarter End Cleanup was successful or was not necessary.');

      if FPrintReports and (R.Results.Value['PayrollNumber'] <> 0) then
      begin
        Params := TisListOfValues.Create;
//        Params.AddValue('ClientID', FClNbr);
        Params.AddValue('ClientID', R.Params.Value['ClientID']);

        Params.AddValue('PrNbr', R.Results.Value['PayrollNumber']);
        Params.AddValue('PrintChecks', False);
        Params.AddValue('UseSbMiskCheckForm', False);

        AddRequest('Running reports for QEC - CLNbr#' + IntToStr(R.Params.Value['ClientID']) + '- CONbr#' +
                                               IntToStr(R.Params.Value['CoNbr']),
                     'PrintPayroll', Params, 'GoToNextCompany.Fork');
      end;
    end
   else

   begin
     AddException(R.GetExceptions);
     AddWarning(R.GetWarnings);
   end;
  end;
end;

function TevQECTask.Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues;
var
  i: Integer;
begin
  AParams.Clear;

  for i := 0 to FResults.Count - 1 do
    if (FResults[i].QECReports <> nil) and (FResults[i].QECReports.Size > 0) then
      AParams.AddValue('Report' + IntToStr(i), FResults[i].QECReports);

  Result := inherited Call_ProcessReportResults(AParams);
end;


procedure TevQECTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoNbr := AStream.ReadInteger;
  FQTREndDate := AStream.ReadDouble;
  FMinTax := AStream.ReadDouble;
  FCleanupAction := AStream.ReadInteger;
  FEECustomNbr := AStream.ReadString;
  FForceCleanup := AStream.ReadBoolean;
  FPrintReports := AStream.ReadBoolean;
  FCurrentQuarterOnly := AStream.ReadBoolean;
  FExtraOptions := AStream.ReadString;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevQECTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TevQECTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('RunReports', Phase_RunReports);
  RegisterTaskPhase('GoToNextCompany', Phase_GoToNextCompany);
end;

procedure TevQECTask.SetCleanupAction(const AValue: Integer);
begin
  FCleanupAction := AValue;
end;

procedure TevQECTask.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevQECTask.SetCoNbr(const AValue: Integer);
begin
  FCoNbr := AValue;
end;

procedure TevQECTask.SetCurrentQuarterOnly(const AValue: Boolean);
begin
  FCurrentQuarterOnly := AValue;
end;

procedure TevQECTask.SetEECustomNbr(const AValue: String);
begin
  FEECustomNbr := AValue;
end;

procedure TevQECTask.SetExtraOptions(const AValue: String);
begin
  FExtraOptions := AValue;
end;

procedure TevQECTask.SetForceCleanup(const AValue: Boolean);
begin
  FForceCleanup := AValue;
end;

procedure TevQECTask.SetMinTax(const AValue: Double);
begin
  FMinTax := AValue;
end;

procedure TevQECTask.SetPrintReports(const AValue: Boolean);
begin
  FPrintReports := AValue;
end;

procedure TevQECTask.SetQTREndDate(const AValue: TDateTime);
begin
  FQTREndDate := AValue;
end;

function TevQECTask.GetFilter: IevQECTaskFilter;
begin
  Result := FFilter;
end;

function TevQECTask.GetResults: IevQECTaskResult;
begin
  Result := FResults;
end;

procedure TevQECTask.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteInteger(FCoNbr);
  AStream.WriteDouble(FQTREndDate);
  AStream.WriteDouble(FMinTax);
  AStream.WriteInteger(FCleanupAction);
  AStream.WriteString(FEECustomNbr);
  AStream.WriteBoolean(FForceCleanup);
  AStream.WriteBoolean(FPrintReports);
  AStream.WriteBoolean(FCurrentQuarterOnly);
  AStream.WriteString(FExtraOptions);

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;


initialization
  ObjectFactory.Register(TevQECTask);

finalization
  SafeObjectFactoryUnRegister(TevQECTask);

end.
