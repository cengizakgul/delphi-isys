// Module "Task Queue"
unit mod_Task_Queue;

interface        

uses
  EvTaskQueueMod,
  EvCustomTask,
  EvRunReportTask,
  EvProcessPayrollTask,
  EvPreprocessPayrollTask,
  EvReprintPayrollTask,
  EvProcessTaxPaymentsTask,
  EvCashManagementTask,
  EvProcessPrenoteACHTask,
  EvProcessManualACHTask,
  EvRebuildTempTablesTask,
  EvCreatePayrollTask,
  EvPreprocessQuarterEndTask,
  EvQECTask,
  EvPAMCTask,
  EvRebuildVMRHistoryTask,
  EvProcessTaxReturnsTask,
  EvEvoXImportTask,
  EvBenefitEnrollmentNotificationsTask,
  EvACAUpdateTask;

implementation  

end.
