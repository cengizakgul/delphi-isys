unit EvBenefitEnrollmentNotificationsTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

uses Classes, Math;

type
  TEvBenefitEnrollmentNotificationsTask = class(TEvCustomTask, IevBenefitEnrollmentNotificationsTask)
  private
    FClientNumbers: String;
    FForAllClients: boolean;

    FClientsProcessed: Integer;
    FClientsFailed: Integer;
    FClientsToProcess: IisStringList;
    FEnableLogging: boolean;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;

    function   GetForAllClients: Boolean;
    function   GetClientNumbers: String;
    procedure  SetClientNumbers(const AValue: String);
    function   GetEnableLogging: Boolean;
    procedure  SetEnableLogging(const AValue: Boolean);

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_GoToNextClient(const AInputRequests: TevTaskRequestList);
  public
    class function GetTypeID: String; override;
  end;

{ TEvBenefitEnrollmentNotificationsTask }

procedure TEvBenefitEnrollmentNotificationsTask.DoOnConstruction;
begin
  inherited;
  FClientsToProcess := TisStringList.Create;
  FEnableLogging := True;
  FForAllClients := Trim(FClientNumbers) = '';
end;

procedure TEvBenefitEnrollmentNotificationsTask.DoRestoreTaskState(
  const AStream: IevDualStream);
begin
  inherited;
  FClientsProcessed := AStream.ReadInteger;
  FClientsFailed := AStream.ReadInteger;
  FClientsToProcess.LoadFromStream(AStream.RealStream);
  FEnableLogging := AStream.ReadBoolean;
end;

procedure TEvBenefitEnrollmentNotificationsTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteInteger(FClientsProcessed);
  AStream.WriteInteger(FClientsFailed);
  FClientsToProcess.SaveToStream(AStream.RealStream);
  AStream.WriteBoolean(FEnableLogging);
end;

function TEvBenefitEnrollmentNotificationsTask.GetClientNumbers: String;
begin
  Result := FClientNumbers;
end;

function TEvBenefitEnrollmentNotificationsTask.GetEnableLogging: Boolean;
begin
  Result := FEnableLogging;
end;

function TEvBenefitEnrollmentNotificationsTask.GetForAllClients: Boolean;
begin
  Result := FForAllClients;
end;

class function TEvBenefitEnrollmentNotificationsTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS;
end;

procedure TEvBenefitEnrollmentNotificationsTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('ForAllClients', FForAllClients);
  Params.AddValue('ClientNumbers', FClientNumbers);

  AddRequest('Prepare for sending notifications', 'PrepareForGetBenefitEmailNotifications', Params, 'GoToNextClient.Fork');
end;

procedure TEvBenefitEnrollmentNotificationsTask.Phase_GoToNextClient(const AInputRequests: TevTaskRequestList);
var
  i, n: Integer;
  Params: IisListOfValues;
  NotificationsList: IisListOfValues;
  Err: String;

  procedure SendNotifications(const ANotificationsList: IisListOfValues);
  var
    i: integer;
    EEMailMessage: IevMailMessage;
    s, sClientId, sRowNum, sDescription: String;
  begin
    for i := 0 to ANotificationsList.Count - 1 do
    begin
      s := ANotificationsList.Values[i].Name;
      sClientId := GetNextStrValue(s, ',');
      sRowNum := GetNextStrValue(s, ',');
      sDescription := GetNextStrValue(s, ',');

      EEMailMessage := IInterface(ANotificationsList.Values[i].Value) as IevMailMessage;

      if FEnableLogging then
        AddNotes('Created notification. Client #' + sClientId + '. "' + sDescription + '"');

      try
        Context.EMailer.SendMail(EEMailMessage);
      except
        on E: Exception do
        begin
          AddException('Cannot send email. To:"' + EEMailMessage.AddressTo + '". CC: "' +
            EEMailMessage.AddressCC + '". Error "' + E.Message + '"');
        end;
      end;  // try
    end;  // for
  end;
begin
  if (Length(AInputRequests) = 1) and AnsiSameText(AInputRequests[0].TaskRoutineName, 'PrepareForGetBenefitEmailNotifications') then
  begin
    // For the very first time we need to define client list
    StopOnExceptions(AInputRequests);
    FClientsToProcess.Text := AInputRequests[0].Results.Value[PARAM_RESULT];
  end

  else
    for i := Low(AInputRequests) to High(AInputRequests) do
    begin
      AddWarning(AInputRequests[i].GetWarnings);
      Err := AInputRequests[i].GetExceptions;
      if Err = '' then
      begin
        Inc(FClientsProcessed);
        NotificationsList := IInterface(AInputRequests[i].Results.Value[PARAM_RESULT]) as IisListOfValues;
        SendNotifications(NotificationsList);
      end
      else
      begin
        Inc(FClientsFailed);
        AddException('Client #' + String(AInputRequests[i].Params.Value['ClientID']) + ': ' + Err);
      end;
    end;

  SetProgressText('Clients Processed: ' + IntToStr(FClientsProcessed) + '; Clients Failed:' + IntToStr(FClientsFailed) + '; Clients Left:' + IntToStr(FClientsToProcess.Count));

  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    // add as many requests as MaxThreads specifies
    if FClientsToProcess.Count = 0 then
      break;
    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', StrToInt(FClientsToProcess[0]));
    AddRequest('Create notifications for Client ' + FClientsToProcess[0], 'GetBenefitEmailNotifications', Params, 'GoToNextClient.Fork');
    FClientsToProcess.Delete(0);
  end;
end;

procedure TEvBenefitEnrollmentNotificationsTask.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClientNumbers := AStream.ReadString;
  FEnableLogging := AStream.ReadBoolean;
  FForAllClients := Trim(FClientNumbers) = '';
end;

procedure TEvBenefitEnrollmentNotificationsTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('GoToNextClient', Phase_GoToNextClient);
end;

procedure TEvBenefitEnrollmentNotificationsTask.SetClientNumbers(const AValue: String);
begin
  FClientNumbers := AValue;
  FForAllClients := Trim(FClientNumbers) = '';
end;

procedure TEvBenefitEnrollmentNotificationsTask.SetEnableLogging(const AValue: Boolean);
begin
  FEnableLogging := AValue;
end;

procedure TEvBenefitEnrollmentNotificationsTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FClientNumbers);
  AStream.WriteBoolean(FEnableLogging);
end;

initialization
  ObjectFactory.Register(TEvBenefitEnrollmentNotificationsTask);

finalization
  SafeObjectFactoryUnRegister(TEvBenefitEnrollmentNotificationsTask);

end.
 