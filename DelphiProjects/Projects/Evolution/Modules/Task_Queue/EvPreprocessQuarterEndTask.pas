unit EvPreprocessQuarterEndTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;


implementation

{%File 'EvPreprocessQuarterEndTask.jpg'}

type
  TEvPreprocessQuarterEndTask = class(TEvPrintableTask, IevPreprocessQuarterEndTask)
  private
    FBegDate: TDateTime;
    FEndDate: TDateTime;
    FQecProduceReports: Boolean;
    FQecAdjustmentLimit: Currency;
    FTaxAdjustmentLimit: Currency;
    FTaxServiceFilter: TTaxServiceFilterType;
    FFilter: IevPreprocessQuarterEndTaskFilter;
    FResults: IevPreprocessQuarterEndTaskResult;
    FFlagsAreSet: Boolean;
    FNextFilterItem: Integer;
    procedure  SetFlags(const AValue: Boolean);
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;
    procedure  DoOnActivate; override;
    procedure  AfterTaskFinished; override;

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_PreprocessQuarterEnd(const AInputRequests: TevTaskRequestList);
    function   Call_AnalyzeCompanyList(const AParams: IisListOfValues): IisListOfValues;
    procedure  Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);
    function   Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues; override;

    function  GetBegDate: TDateTime;
    function  GetEndDate: TDateTime;
    function  GetFilter: IevPreprocessQuarterEndTaskFilter;
    function  GetQecAdjustmentLimit: Currency;
    function  GetQecProduceReports: Boolean;
    function  GetResults: IevPreprocessQuarterEndTaskResult;
    function  GetTaxAdjustmentLimit: Currency;
    function  GetTaxServiceFilter: TTaxServiceFilterType;
    procedure SetBegDate(const AValue: TDateTime);
    procedure SetEndDate(const AValue: TDateTime);
    procedure SetQecAdjustmentLimit(const AValue: Currency);
    procedure SetQecProduceReports(const AValue: Boolean);
    procedure SetTaxAdjustmentLimit(const AValue: Currency);
    procedure SetTaxServiceFilter(const AValue: TTaxServiceFilterType);
  public
    class function GetTypeID: String; override;
  end;


{ TEvPreprocessQuarterEndTask }

procedure TEvPreprocessQuarterEndTask.AfterTaskFinished;
begin
  inherited;
  SetFlags(False);
end;

function TEvPreprocessQuarterEndTask.Call_AnalyzeCompanyList(const AParams: IisListOfValues): IisListOfValues;
begin
  // Dummy local request
  SetFlags(True);
  FResults := TevPreprocessQuarterEndTaskResult.Create;
  Result := TisListOfValues.Create;
end;

procedure TEvPreprocessQuarterEndTask.DoOnActivate;
begin
  inherited;
  SetFlags(True);
end;

procedure TEvPreprocessQuarterEndTask.DoOnConstruction;
begin
  inherited;
  FFilter := TevPreprocessQuarterEndTaskFilter.Create;
  SetDestination(rdtPrinter);
end;

procedure TEvPreprocessQuarterEndTask.DoRestoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  FNextFilterItem := AStream.ReadInteger;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevPreprocessQuarterEndTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TEvPreprocessQuarterEndTask.DoStoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  AStream.WriteInteger(FNextFilterItem);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

function TEvPreprocessQuarterEndTask.GetBegDate: TDateTime;
begin
  Result := FBegDate;
end;

function TEvPreprocessQuarterEndTask.GetEndDate: TDateTime;
begin
  Result := FEndDate;
end;

function TEvPreprocessQuarterEndTask.GetFilter: IevPreprocessQuarterEndTaskFilter;
begin
  Result := FFilter;
end;

function TEvPreprocessQuarterEndTask.GetQecAdjustmentLimit: Currency;
begin
  Result := FQecAdjustmentLimit;
end;

function TEvPreprocessQuarterEndTask.GetQecProduceReports: Boolean;
begin
  Result := FQecProduceReports;
end;

function TEvPreprocessQuarterEndTask.GetResults: IevPreprocessQuarterEndTaskResult;
begin
  Result := FResults;
end;

function TEvPreprocessQuarterEndTask.GetTaxAdjustmentLimit: Currency;
begin
  Result := FTaxAdjustmentLimit;
end;

function TEvPreprocessQuarterEndTask.GetTaxServiceFilter: TTaxServiceFilterType;
begin
  Result := FTaxServiceFilter;
end;

class function TEvPreprocessQuarterEndTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PREPROCESS_QUARTER_END;
end;

procedure TEvPreprocessQuarterEndTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;
  if FFilter.Count = 0 then
  begin
    Params := TisListOfValues.Create;
    Params.AddValue('TaxServiceFilter', Ord(FTaxServiceFilter));
    Params.AddValue('EndDate', FEndDate);

    AddRequest('Prepare company list', 'PrepareQuarterEndPreprocCompanyFilter',  Params, 'PreprocessQuarterEnd.Merge');
  end
  else
  begin
   if (FFilter.Count = 1) and (FFilter[0].ClNbr = -1) and (FFilter[0].CoNbr = -1) then
      FFilter := TevPreprocessQuarterEndTaskFilter.Create;

   Phase_PreprocessQuarterEnd(AInputRequests);
  end;
end;

procedure TEvPreprocessQuarterEndTask.Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);

  function MakeCaption(const s: string; const cons: Boolean): string;
  begin
    if cons then
      Result := s + ' (C)'
    else
      Result := s;
    Result := '#' + Result;
  end;

var
  i, n: Integer;
  Flt: IevPreprocessQuarterEndTaskFilterItem;
  Params: IisListOfValues;
  Res: IevPreprocessQuarterEndTaskResultItem;
begin
  // collect results
  if FNextFilterItem > 0 then
    for i := Low(AInputRequests) to High(AInputRequests) do
    begin
      if AInputRequests[i].GetExceptions = '' then
      begin
        Res := FResults.Add;
        Res.SetTimeStampStarted(AInputRequests[i].Results.Value['TimeStampStarted']);
        Res.SetCoCustomNumber(AInputRequests[i].Results.Value['CoCustomNumber']);
        Res.SetCoName(AInputRequests[i].Results.Value['CoName']);
        Res.SetConsolidation(AInputRequests[i].Results.Value['Consolidation']);
        Res.SetTimeStampFinished(AInputRequests[i].Results.Value['TimeStampFinished']);
        Res.SetErrorMessage(AInputRequests[i].Results.Value['ErrorMessage']);
        Res.SetTaxAdjReports(IInterface(AInputRequests[i].Results.Value['TaxAdjReports']) as IevDualStream);

        AddNotes(AInputRequests[i].Results.Value['Log']);
      end
      else
       AddException(AInputRequests[i].GetExceptions);
      if AInputRequests[i].GetWarnings <> '' then
        AddWarning(AInputRequests[i].GetWarnings);
    end;

  // continue to add as many requests as GetMaxThreads specifies
  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    if FNextFilterItem >= FFilter.Count then
      break;

    Flt := FFilter[FNextFilterItem];

    Params := TisListOfValues.Create;
    Params.AddValue('Cl_Nbr', Flt.ClNbr);
    Params.AddValue('Co_Nbr', Flt.CoNbr);
    Params.AddValue('ForceProcessing', True);
    Params.AddValue('Consolidation', Flt.Consolidation);
    Params.AddValue('BegDate', FBegDate);
    Params.AddValue('EndDate', FEndDate);
    Params.AddValue('QecCleanUpLimit', FQecAdjustmentLimit);
    Params.AddValue('TaxAdjLimit', FTaxAdjustmentLimit);
    Params.AddValue('PrintReports', FQecProduceReports);

    AddRequest(MakeCaption(Flt.Caption, Flt.Consolidation), 'PreprocessQuarterEnd', Params, 'GoToNextCompany.Fork',
      GF_CL_PAYROLL_OPERATION + '.' + IntToStr(Flt.ClNbr) + ',' +
      GF_CO_PAYROLL_OPERATION + '.' + IntToStr(Flt.ClNbr) + '_' + IntToStr(Flt.CoNbr));

    Inc(FNextFilterItem);
  end;

  // Are all companies processed?
  if (FNextFilterItem >= FFilter.Count) and AllRequestsAreFinished then
  begin
    Params := TisListOfValues.Create;
    AddRequest('Process Report Results', 'ProcessReportResults', Params, 'PostProcessReportResults.Merge');
  end;
end;

procedure TEvPreprocessQuarterEndTask.Phase_PreprocessQuarterEnd(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  StopOnExceptions(AInputRequests);
  if Length(AInputRequests) > 0 then
    (FFilter as IisInterfacedObject).AsStream := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IevDualStream;

  Params := TisListOfValues.Create;
  AddRequest('Analyze company list', 'AnalyzeCompanyList', Params, 'GoToNextCompany.Fork');
end;

function TEvPreprocessQuarterEndTask.Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues;
var
  i: Integer;
begin
  AParams.Clear;
  for i := 0 to FResults.Count - 1 do
    if (FResults[i].TaxAdjReports <> nil) and (FResults[i].TaxAdjReports.Size > 0) then
      AParams.AddValue('Report' + IntToStr(i), FResults[i].TaxAdjReports);

  Result := inherited Call_ProcessReportResults(AParams);
end;

procedure TEvPreprocessQuarterEndTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;

  FBegDate := AStream.ReadDouble;
  FEndDate := AStream.ReadDouble;
  FQecProduceReports := AStream.ReadBoolean;
  FQecAdjustmentLimit := AStream.ReadCurrency;
  FTaxAdjustmentLimit := AStream.ReadCurrency;
  FTaxServiceFilter := TTaxServiceFilterType(AStream.ReadByte);

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevPreprocessQuarterEndTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TEvPreprocessQuarterEndTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('PreprocessQuarterEnd', Phase_PreprocessQuarterEnd);
  RegisterTaskPhase('GoToNextCompany', Phase_GoToNextCompany);
  RegisterTaskCall('AnalyzeCompanyList', Call_AnalyzeCompanyList);
end;

procedure TEvPreprocessQuarterEndTask.SetBegDate(const AValue: TDateTime);
begin
  FBegDate := AValue;
end;

procedure TEvPreprocessQuarterEndTask.SetEndDate(const AValue: TDateTime);
begin
  FEndDate := AValue;
end;

procedure TEvPreprocessQuarterEndTask.SetFlags(const AValue: Boolean);
var
  i: Integer;
  Flt: IevPreprocessQuarterEndTaskFilterItem;
  s: String;
begin
  if (FFilter.Count = 0) or (AValue and FFlagsAreSet) or (not FFlagsAreSet and not AValue) then
    Exit;

  for i := 0 to FFilter.Count - 1 do
  begin
    Flt := FFilter[i];
    s := 'TaxPreProc'+ FormatDateTime('MMDDYYYY', FEndDate) + '.' + MakeString([Flt.ClNbr, Flt.CoNbr, BoolToYN(Flt.Consolidation)], ';');
    if AValue then
      mb_GlobalFlagsManager.SetFlag(s)
    else
      mb_GlobalFlagsManager.ReleaseFlag(s);    
  end;

  FFlagsAreSet := AValue;
end;

procedure TEvPreprocessQuarterEndTask.SetQecAdjustmentLimit(const AValue: Currency);
begin
  FQecAdjustmentLimit := AValue;
end;

procedure TEvPreprocessQuarterEndTask.SetQecProduceReports(const AValue: Boolean);
begin
  FQecProduceReports := AValue;
end;

procedure TEvPreprocessQuarterEndTask.SetTaxAdjustmentLimit(const AValue: Currency);
begin
  FTaxAdjustmentLimit := AValue;
end;

procedure TEvPreprocessQuarterEndTask.SetTaxServiceFilter(const AValue: TTaxServiceFilterType);
begin
  FTaxServiceFilter := AValue;
end;

procedure TEvPreprocessQuarterEndTask.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteDouble(FBegDate);
  AStream.WriteDouble(FEndDate);
  AStream.WriteBoolean(FQecProduceReports);
  AStream.WriteCurrency(FQecAdjustmentLimit);
  AStream.WriteCurrency(FTaxAdjustmentLimit);
  AStream.WriteByte(Ord(FTaxServiceFilter));

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

initialization
  ObjectFactory.Register(TEvPreprocessQuarterEndTask);

finalization
  SafeObjectFactoryUnRegister(TEvPreprocessQuarterEndTask);

end.
