unit EvCustomTask;
                      
interface

uses Classes, Windows, SysUtils, isBaseClasses, isBasicUtils, EvCommonInterfaces, EvContext, EvMainboard, EvStreamUtils,
     EvConsts, EvClasses, EvLegacy, EvDataSet, EvTypes, IsErrorUtils, EvAsyncCall, SReportSettings, isVCLBugFix,
     EvBasicUtils, EvUtils, isStatistics;

type
  TevTaskRequestList = array of IevTaskRequest;
  TevPhaseProc = procedure (const AInputRequests: TevTaskRequestList) of object;

  TevCustomTask = class(TisCollection, IevTask, IevTaskInQueue, IevTaskExt)
  private
    FLocalAsyncCallHandler: IevAsyncCalls;
    FNbr: Integer;
    FState: TevTaskState;
    FLastUpdate: TDateTime;
    FUserInfo: TevUserInfo;
    FContext:   IevContext;
    FPhaseEntrypoints: TStringList;
    FPriority: Integer;
    FMaxThreads: Integer;
    FNotificationEmail: string;
    FSendEmailNotification: Boolean;
    FEmailSendRule: TevEmailSendRule;
    FCaption: String;
    FProgressText: string;
    FLog: String;
    FExceptions: String;
    FWarnings: String;
    FNotes: String;
    FSeenByUser: Boolean;
    FTaskStorageLocation: String;
    FStateStorageLocation: String;
    FPhaseCounter: Integer;
    FFirstRunAt: TDateTime;
    FStatisticsEnabled: Boolean;
    FRunStatistics: IisStatEvent;

    procedure  PulseRequests;
    function   RunRequests: Boolean;
    procedure  RunTaskPhase(const APhaseName: String; const AInputRequests: IisList);
    procedure  FinalizeTask;
    procedure  SendEmailNotificationToUser;

    procedure  StoreFullTask;
    procedure  StoreFullTaskState;
    procedure  RestoreFullTaskState;
    procedure  StoreTaskState;
    procedure  SetState(const AValue: TevTaskState);
  protected
    FDaysStayInQueue: Integer;
    function   MakeAsyncCall(const ATaskRoutineName: String; const AParams: IisListOfValues): TisGUID; virtual;    
    function   Revision: TisRevision; override;
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IisStream); override;
    procedure  ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure  WriteChildrenToStream(const AStream: IisStream); override;
    procedure  ReadChildrenFromStream(const AStream: IisStream); override;

    procedure  RegisterTaskPhase(const ATaskPhase: String; const AEntryPoint: TevPhaseProc);
    procedure  RegisterTaskCall(const ATaskRoutine: String; const AEntryPoint: TevAsyncCallProc);
    procedure  AddRequest(const AName: String; const ATaskRoutineName: String; const AParams: IisListOfValues;
                          const ANextTaskPhase: String = ''; const AGlobalFlagName: string = '');
    function   RequestCount: Integer;
    function   AllRequestsAreFinished: Boolean;
    function   GetRequest(const AIndex: Integer): IevTaskRequest;
    function   FindRequest(const ARequestName: String): IevTaskRequest;
    procedure  AddLogEvent(const AText: string);
    procedure  SetProgressText(const AText: string);
    procedure  SetLastUpdateToNow;
    procedure  UpdateStorage;
    function   MakeUniqueRequestName(const ABaseName: String): String;
    function   GetDefaultPriority: Integer; virtual;
    function   GetDefaultMaxThreads: Integer; virtual;
    procedure  StopOnExceptions(const AInputRequests: TevTaskRequestList);
    procedure  DoStoreTaskState(const AStream: IisStream); virtual;
    procedure  DoRestoreTaskState(const AStream: IisStream); virtual;
    procedure  SendEmail(const ATo, ASubject, AMessage: String);
    function   BeforeTaskStart: Boolean; virtual;
    procedure  AfterTaskFinished; virtual;
    function   CanExecute: Boolean; virtual;

    // Interface
    function   GetID: TisGUID;
    procedure  SetID(const AValue: TisGUID);
    function   GetNbr: Integer;
    procedure  SetNbr(const AValue: Integer);
    function   GetTaskType: String;
    function   GetUser: String;
    function   GetDomain: String;
    function   GetPriority: Integer;
    procedure  SetPriority(const AValue: Integer);
    function   GetMaxThreads: Integer;
    procedure  SetMaxThreads(const AValue: Integer);
    function   GetNotificationEmail: string;
    procedure  SetNotificationEmail(const AValue: string);
    function   GetSendEmailNotification: Boolean;
    procedure  SetSendEmailNotification(const AValue: Boolean);
    function   GetEmailSendRule: TevEmailSendRule;
    procedure  SetEmailSendRule(const AValue: TevEmailSendRule);
    function   GetLastUpdate: TDateTime;
    function   GetState: TevTaskState;
    function   GetCaption: String;
    procedure  SetCaption(const AValue: string);
    function   GetProgressText: string;
    function   GetLog: String;
    function   GetExceptions: String;
    function   GetWarnings: String;
    function   GetNotes: String;
    function   GetSeenByUser: Boolean;
    procedure  SetSeenByUser(const AValue: Boolean);
    function   GetDaysToStayInQueue: Integer;
    function   GetFirstRunAt: TDateTime;
    procedure  AddException(const AText: string);
    procedure  AddWarning(const AText: string);
    procedure  AddNotes(const AText: string);
    function   TaskPulse: Boolean;
    procedure  Terminate;
    procedure  SetTaskStorageLocation(const ALocation: String);
    function   GetRequests: IisList;
    procedure  ModifyTaskProperty(const APropName: String; const ANewValue: Variant);
    procedure  SetDefaults; virtual;
    procedure  DoOnActivate; virtual;
    function   GetRunStatistics: IisStatEvent;

    procedure SetAccountInfo(const ADomain, AUser: String);

    // Task logic
    procedure  RegisterTaskPhases; virtual;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); virtual;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); virtual;
  public
    destructor Destroy; override;
  end;


  TEvPrintableTask = class(TevCustomTask, IevPrintableTask)
  private
    FDestination: TrwReportDestination;
    FThereAreUnprintedReports: Boolean;
    procedure  SendToPrinter(const AData: IisStream);
    procedure  SendToStorage(const AData: IisStream);
    procedure  PrepareByVMR(var AData: IisStream);
  protected
    FReportResults: IisStream;
    function   MakeAsyncCall(const ATaskRoutineName: String; const AParams: IisListOfValues): TisGUID; override;
    procedure  WriteSelfToStream(const AStream: IisStream); override;
    procedure  ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IisStream); override;
    procedure  DoRestoreTaskState(const AStream: IisStream); override;
    procedure  RegisterTaskPhases; override;
    function   Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues; virtual;
    procedure  Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList); virtual;
    procedure  Phase_PostProcessReportResults(const AInputRequests: TevTaskRequestList);
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
    function   AlwaysSendToVMR: Boolean; virtual;

    function   GetDestination: TrwReportDestination;
    procedure  SetDestination(const AValue: TrwReportDestination);
    function   ReportResults: IisStream;
  end;


implementation

const
  sTaskPhase_Begin    = 'Begin';
  sTaskPhase_End      = 'End';
  sTaskPhase_ProcessReportResults = 'ProcessReportResults';
  sTaskPhase_PostProcessReportResults = 'PostProcessReportResults';
  sTaskPhase_Postfix_Fork = '.Fork';
  sTaskPhase_Postfix_Merge = '.Merge';
  sTaskPhase_Postfix_Stop = '.Stop';

  sTaskCall_ProcessReportResults = 'ProcessReportResults';

  sStateFileExt = '.tmp';


type
  TevTRRunStatus = (rsOK, rsFailed, rsNoResources, rsGlobalFlagLocked);

  TevTaskRequest = class(TisNamedObject, IevTaskRequest, IevContextCallbacks)
  private
    FContext:   IevContext;
    FTaskRoutineName: String;
    FNextTaskPhase: String;
    FPhaseNumber: Integer;
    FGlobalFlagName: String;
    FGlobalFlagLocked: Boolean;
    FParams:  IisListOfValues;
    FResults: IisListOfValues;
    FState:   TevTaskRequestState;
    FExceptions: String;
    FWarnings: String;
    FProgressInfo: IevProgressInfo;
    FAsyncCallID: TisGUID;
    FRequestStorageLocation: String;
    FStartedAt: TDateTime;
    FFinishedAt: TDateTime;
    FProgressInfoStack: IisList;
    FStatisticsEvent: IisStatEvent;

    function   Task: TevCustomTask;
    function   Run: TevTRRunStatus;
    procedure  SetRequestStorageLocation(const ALocation: String);
    procedure  StoreRequestState;
    procedure  RestoreRequestState;
    procedure  UpdatePriority;
    procedure  Terminate;

    // callbacks
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);

  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IisStream); override;
    procedure  ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;

    function  RequestName: String;
    function  TaskRoutineName: String;
    function  NextTaskPhase: String;
    function  Params:  IisListOfValues;
    function  Results: IisListOfValues;
    function  State:   TevTaskRequestState;
    function  GetExceptions: String;
    function  GetWarnings: String;
    function  GetProgressText: String;
    function  GetStartedAt: TDateTime;
    function  GetFinishedAt: TDateTime;
  public
    class function GetTypeID: String; override;
    constructor Create(const AName: String; const ATaskRoutineName: String; const AParams: IisListOfValues;
                       const ANextTaskPhase: String; const APhaseNumber: Integer; const AGlobalFlagName: string); reintroduce;
    destructor  Destroy; override;
  end;




{ TevCustomTask }

function TevCustomTask.RequestCount: Integer;
begin
  Result := ChildCount;
end;

procedure TevCustomTask.RunTaskPhase(const APhaseName: String; const AInputRequests: IisList);
var
  i: Integer;
  RequestResults: TevTaskRequestList;
  EntryPoint: TMethod;
begin
  if ctx_Statistics.Enabled then
    ctx_Statistics.BeginEvent('Phase "' + APhaseName + '"');

  try
    SetState(tsExecuting);

    i := FPhaseEntrypoints.IndexOf(APhaseName);
    Assert(i <> -1, 'Task phase "' + APhaseName + '" is not found');

    AddLogEvent('Phase "' + APhaseName + '" activated');

    EntryPoint.Code := Pointer(FPhaseEntrypoints.Objects[i]);
    EntryPoint.Data := Self;

    if Assigned(AInputRequests) then
    begin
      SetLength(RequestResults, AInputRequests.Count);
      for i := Low(RequestResults) to High(RequestResults) do
        RequestResults[i] := AInputRequests[i] as IevTaskRequest;
    end
    else
      SetLength(RequestResults, 0);

    Inc(FPhaseCounter);
    TevPhaseProc(EntryPoint)(RequestResults);
    StoreFullTaskState;
  finally
    if ctx_Statistics.Enabled and Assigned(ctx_Statistics.ActiveEvent) then
      ctx_Statistics.EndEvent;
  end;
end;

destructor TevCustomTask.Destroy;
begin
  FreeAndNil(FPhaseEntrypoints);
  inherited;
end;

procedure TevCustomTask.DoOnConstruction;
begin
  inherited;
  InitLock;

  FLocalAsyncCallHandler := TevAsyncCalls.Create;

  SetName(GetUniqueID);

  FPriority := GetDefaultPriority;
  FDaysStayInQueue := 3;
  FMaxThreads := GetDefaultMaxThreads;

  FPhaseEntrypoints := TStringList.Create;
  FPhaseEntrypoints.Duplicates := dupError;
  FPhaseEntrypoints.Sorted := True;
  FPhaseEntrypoints.CaseSensitive := False;

  RegisterTaskPhases;

  // All task activity happens under user-owner account.
  // However user password is not stored in task due to security concerns.
  // So, task uses a system account password instead and security module is aware of this "back door"
  // Please note: System account password randomly generates on each system startup,
  // so there should be no security concerns
  FUserInfo.Domain := Context.UserAccount.Domain;
  FUserInfo.UserName := Context.UserAccount.User;
  FUserInfo.Password := Context.DomainInfo.SystemAccountInfo.Password;

  FStatisticsEnabled := ctx_Statistics.Enabled;

  SetState(tsInactive);
end;

procedure TevCustomTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
begin
  FFirstRunAt := Now;
end;

procedure TevCustomTask.Phase_End(const AInputRequests: TevTaskRequestList);
begin
  if ctx_Statistics.Enabled then
    ctx_Statistics.EndEvent;

  FinalizeTask;
  mb_TaskQueue.Pulse;
end;

procedure TevCustomTask.RegisterTaskPhase(const ATaskPhase: String; const AEntryPoint: TevPhaseProc);
begin
  FPhaseEntrypoints.AddObject(ATaskPhase, @AEntryPoint);
end;

procedure TevCustomTask.RegisterTaskPhases;
begin
  RegisterTaskPhase(sTaskPhase_Begin, Phase_Begin);
  RegisterTaskPhase(sTaskPhase_End, Phase_End);
end;

function TevCustomTask.TaskPulse: Boolean;
begin
  Mainboard.ContextManager.StoreThreadContext;
  Lock;
  try
    try
      CheckCondition(FState <> tsFinished, 'Attempt to run finished task');

      if not Assigned(FContext) then
      begin
        FContext := Mainboard.ContextManager.CreateContext(
          EncodeUserAtDomain(FUserInfo.UserName, FUserInfo.Domain), FUserInfo.Password);
        FContext.Statistics.Enabled := FStatisticsEnabled;
        if FContext.Statistics.Enabled then
        begin
          FContext.Statistics.BeginEvent('Task "' + GetTypeID + '"');
          FContext.Statistics.ActiveEvent.Properties.AddValue('Caption', GetCaption);
        end;
      end;

      Mainboard.ContextManager.SetThreadContext(FContext);

      if FPhaseCounter = 0 then
      begin
        if BeforeTaskStart then
          RunTaskPhase(sTaskPhase_Begin, nil)
        else
        begin
          // Task is not ready to start. But it pretends it's started. Thus TaskQueue will proceed pulsing other tasks.
          Result := True;
          Exit;
        end;
      end
      else
        PulseRequests;

      if AllRequestsAreFinished and (FState <> tsFinished) then
        RunTaskPhase(sTaskPhase_End, nil);

      if FState <> tsFinished then
        if not RunRequests then
        begin
          // Some requests are waiting for global flags. But it pretends it's started. Thus TaskQueue will proceed pulsing other tasks.
          Result := True;
          Exit;
        end;

    except
      on E: Exception do
      begin
        if not (E is EAbort) then
          AddException(BuildStackedErrorStr(E));
        Terminate;
      end;
    end;

    Result := (FState = tsExecuting) or (FState = tsFinished);
  finally
    Unlock;
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevCustomTask.AddRequest(const AName, ATaskRoutineName: String; const AParams: IisListOfValues;
  const ANextTaskPhase: String = ''; const AGlobalFlagName: string = '');
var
  R: IisInterfacedObject;
begin
  R := TevTaskRequest.Create(AName, ATaskRoutineName, AParams, ANextTaskPhase, FPhaseCounter, AGlobalFlagName);
  AddChild(R);
  AddLogEvent('Request "' + AName + '" added');
  mb_TaskQueue.Pulse;
end;


function TevCustomTask.GetRequest(const AIndex: Integer): IevTaskRequest;
begin
  Result := GetChild(AIndex) as IevTaskRequest;
end;

function TevCustomTask.RunRequests: Boolean;
var
  i: Integer;
  R: IevTaskRequest;
  fl_Executing: Boolean;
  RState: TevTRRunStatus;
begin
  Result := True;
  fl_Executing := False;

  for i := 0 to RequestCount - 1 do
  begin
    R := GetRequest(i);
    if (R.State = rsWaiting) and CanExecute then
    begin
      RState := TevTaskRequest((R as IisInterfacedObject).GetImplementation).Run;
      if RState in [rsFailed, rsNoResources] then
        break
      else if RState = rsGlobalFlagLocked then
        Result := False
      else if RState = rsOK then
        fl_Executing := True;
    end

    else if R.State = rsExecuting then
      fl_Executing := True;
  end;

  if fl_Executing then
    SetState(tsExecuting)
  else
    SetState(tsWaiting);
end;


function TevCustomTask.GetState: TevTaskState;
begin
  Result := FState;
end;


function TevCustomTask.GetCaption: String;
begin
  Result := FCaption;
end;

function TevCustomTask.GetDaysToStayInQueue: Integer;
begin
  Result := FDaysStayInQueue;
end;

function TevCustomTask.GetExceptions: String;
begin
  Result := FExceptions;
end;

function TevCustomTask.GetLog: String;
begin
  Result := FLog;
end;

function TevCustomTask.GetNotificationEmail: string;
begin
  Result := FNotificationEmail;
end;

function TevCustomTask.GetPriority: Integer;
begin
  Result := FPriority;
end;

function TevCustomTask.GetSendEmailNotification: Boolean;
begin
  Result := FSendEmailNotification;
end;

function TevCustomTask.GetID: TisGUID;
begin
  Result := GetName;
end;

function TevCustomTask.GetProgressText: string;
begin
  Result := FProgressText;
end;

procedure TevCustomTask.SetNotificationEmail(const AValue: string);
begin
  FNotificationEmail := AValue;
end;

procedure TevCustomTask.SetPriority(const AValue: Integer);
var
  i: Integer;
begin
  FPriority := AValue;

  Lock;
  try
    for i := 0 to RequestCount - 1 do
      TevTaskRequest((GetRequest(i) as IisInterfacedObject).GetImplementation).UpdatePriority;
  finally
    Unlock;
  end;
end;

procedure TevCustomTask.SetSendEmailNotification(const AValue: Boolean);
begin
  FSendEmailNotification := AValue;
end;

function TevCustomTask.GetUser: String;
begin
  Result := FUserInfo.UserName;
end;

function TevCustomTask.GetWarnings: String;
begin
  Result := FWarnings;
end;


procedure TevCustomTask.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  Header: IevTaskInfo;
  s: IisStream;
begin
  inherited;

  Header := TevTaskInfo.Create;
  (Header as IisInterfacedObject).ReadFromStream(AStream);

  CheckCondition(GetTypeID = Header.TaskType, 'Data is not compatible');
  SetName(HEader.ID);
  FNbr := Header.Nbr;

  FUserInfo.Domain := Header.Domain;
  FUserInfo.UserName := Header.User;

  FState := Header.State;
  FLastUpdate := Header.LastUpdate;
  FPriority := Header.Priority;
  FNotificationEmail := Header.NotificationEmail;
  FSendEmailNotification := Header.SendEmailNotification;
  FEmailSendRule := Header.EmailSendRule;
  FCaption := Header.Caption;
  FProgressText := Header.ProgressText;
  FSeenByUser := Header.SeenByUser;
  FDaysStayInQueue := Header.GetDaysToStayInQueue;
  FFirstRunAt := Header.FirstRunAt;

  if ARevision >= 1 then
    FMaxThreads := AStream.ReadInteger;

  if ARevision < 2 then
    AStream.ReadShortString; // skip password

  FLog := AStream.ReadString;
  FNotes := AStream.ReadString;
  FExceptions := AStream.ReadString;
  FWarnings := AStream.ReadString;

  if ARevision >= 3 then
  begin
    s := AStream.ReadStream(nil, True);
    if Assigned(s) then
    begin
      FRunStatistics := ObjectFactory.CreateInstanceOf('StatEvt') as IisStatEvent;
      FRunStatistics.AsStream := s;
    end
    else
      FRunStatistics := nil;
  end
  else
    FRunStatistics := nil;
end;

procedure TevCustomTask.WriteSelfToStream(const AStream: IisStream);
var
  Header: IisInterfacedObject;
  s: IisStream;
begin
  inherited;
  Header := TevTaskInfo.CreateFromTask(Self);
  Header.WriteToStream(AStream);

  AStream.WriteInteger(FMaxThreads);
  AStream.WriteString(FLog);
  AStream.WriteString(FNotes);
  AStream.WriteString(FExceptions);
  AStream.WriteString(FWarnings);

  if Assigned(FRunStatistics) then
    s := FRunStatistics.AsStream
  else
    s := nil;  
  AStream.WriteStream(s);
end;

function TevCustomTask.GetTaskType: String;
begin
  Result := GetTypeID;
end;

procedure TevCustomTask.AddException(const AText: string);
begin
  if AText <> '' then
  begin
    if FExceptions <> '' then
      FExceptions := FExceptions + #13 + AText
    else
      FExceptions := AText;
    SetLastUpdateToNow;
  end;
end;

procedure TevCustomTask.AddLogEvent(const AText: string);
begin
  SetLastUpdateToNow;
  if AText <> '' then
    FLog := FLog + #13 + FormatDateTime('mm/dd/yy h:nn:ss am/pm', FLastUpdate) + '   ' + AText;
end;

procedure TevCustomTask.AddWarning(const AText: string);
begin
  if AText <> '' then
  begin
    if FWarnings <> '' then
      FWarnings := FWarnings + #13 + AText
    else
      FWarnings := AText;
    SetLastUpdateToNow;
  end;
end;

procedure TevCustomTask.SetProgressText(const AText: string);
begin
  FProgressText := AText;
  SetLastUpdateToNow;
end;


procedure TevCustomTask.SetTaskStorageLocation(const ALocation: String);
begin
  FTaskStorageLocation := ALocation;
  FStateStorageLocation := ChangeFileExt(FTaskStorageLocation, '') + '\';

  try

    if not FileExists(FTaskStorageLocation) then
      StoreFullTask;

    if not DirectoryExists(FStateStorageLocation) then
    begin
      if FState <> tsFinished then
      begin
        ClearDir(FStateStorageLocation);
        ForceDirectories(FStateStorageLocation);
        StoreFullTaskState;
      end;
    end
    else
      RestoreFullTaskState;
  except
    on E: Exception do
      raise Exception.Create('Unable to save task information to "'+FTaskStorageLocation+'" : ' + E.Message);
  end;
end;

procedure TevCustomTask.StoreFullTask;
var
  Stream: IisStream;
begin
  if FTaskStorageLocation <> '' then
  begin
    Stream := TisStream.CreateFromNewFile(FTaskStorageLocation);
    WriteToStream(Stream);
  end;  
end;

procedure TevCustomTask.StoreFullTaskState;
var
  i: Integer;
  R: IevTaskRequest;
  RequestDir: String;
begin
  if (FTaskStorageLocation = '') or (GetState = tsFinished) then
    Exit;

  // store requests
  if FPhaseCounter > 0 then
  begin
    RequestDir := FStateStorageLocation + 'Phase' + IntToStr(FPhaseCounter) + '\';
    ClearDir(RequestDir, True);
  end;

  for i := 0 to RequestCount - 1 do
  begin
    R := GetRequest(i);
    TevTaskRequest((R as IisInterfacedObject).GetImplementation).SetRequestStorageLocation(RequestDir + 'request' + IntToStr(i) + sStateFileExt);
  end;

  // store task state
  StoreTaskState;

 // cleanup previous step 
{TODO  if FPhaseCounter - 1 > 0 then
  begin
    RequestDir := FStateStorageLocation + 'Phase' + IntToStr(FPhaseCounter - 1) + '\';
    ClearDir(RequestDir, True);
  end;}
end;


procedure TevCustomTask.RestoreFullTaskState;
var
  sFile: String;

  procedure RestoreTask;
  var
    Stream: IisStream;
  begin
    if FileExists(sFile) then
    begin
      Stream := TisStream.CreateFromFile(sFile);
      DoRestoreTaskState(Stream);
      Stream := nil;
    end;
  end;

  procedure RestoreRequests;
  var
    DosCode: Integer;
    FileInfo: TSearchRec;
    RequestDir: String;
    R: IevTaskRequest;
  begin
    RequestDir := FStateStorageLocation + 'Phase' + IntToStr(FPhaseCounter) + '\';

    DosCode := FindFirst(RequestDir + '*' + sStateFileExt, faAnyFile, FileInfo);
    try
      while DosCode = 0 do
      begin
        R := TevTaskRequest.Create('', '', nil, '', 0, '');
        TevTaskRequest((R as IisInterfacedObject).GetImplementation).SetRequestStorageLocation(RequestDir + FileInfo.Name);

        if R.State <> rsFinished then
          TevTaskRequest((R as IisInterfacedObject).GetImplementation).FState := rsWaiting;

        AddChild(R as IisInterfacedObject);
        DosCode := FindNext(FileInfo);
      end;
    finally
      FindClose(FileInfo);
    end;
  end;

begin
  try
    sFile := FStateStorageLocation + 'state' + sStateFileExt;
    RestoreTask;
    RestoreRequests;

    if FState = tsExecuting then
      FState := tsWaiting;
  except
    // in case if task state files are broken
    on E: Exception do
    begin
      RemoveAllChildren;
      SetProgressText('Finished with Exceptions');
      AddException('Task creation error: ' + BuildStackedErrorStr(E));
      FState := tsFinished;
      StoreFullTask;
      ClearDir(FStateStorageLocation, True);
    end;
  end;
end;

function TevCustomTask.GetDomain: String;
begin
  Result := FUserInfo.Domain;
end;

function TevCustomTask.GetNbr: Integer;
begin
  Result := FNbr;
end;

procedure TevCustomTask.SetNbr(const AValue: Integer);
begin
  FNbr := AValue;
  FStatisticsEnabled := ctx_Statistics.Enabled;
end;

procedure TevCustomTask.FinalizeTask;

  procedure SetFinalStatusText;
  begin
    if FExceptions <> '' then
      SetProgressText('Finished with Exceptions')
    else if FWarnings <> '' then
      SetProgressText('Finished with Warnings')
    else
      SetProgressText('Finished Successfully');
  end;

var
  Ctx: IevContext;
begin
  Ctx := FContext;

  FContext := nil;
  SetState(tsFinished);
  SetFinalStatusText;
  AddLogEvent('Task finished');
  AfterTaskFinished;
  try
    SendEmailNotificationToUser;

    if Assigned(Ctx) and Ctx.Statistics.Enabled then
    begin
      FRunStatistics := Ctx.Statistics.ActiveEvent;
      Ctx.Statistics.EndEvent;
    end;

    SetFinalStatusText; // warnings could be added if email sending issue has happened, so let's update the status
  finally
    StoreFullTask;
    ClearDir(FStateStorageLocation, True);
  end;
end;

function TevCustomTask.GetNotes: String;
begin
  Result := FNotes;
end;

procedure TevCustomTask.AddNotes(const AText: string);
begin
  if AText <> '' then
  begin
    FNotes := FNotes + #13 + AText;
    SetLastUpdateToNow;
  end;
end;

function TevCustomTask.GetLastUpdate: TDateTime;
begin
  Result := FLastUpdate;
end;

procedure TevCustomTask.SetLastUpdateToNow;
begin
  FLastUpdate := Now;
end;

procedure TevCustomTask.SetState(const AValue: TevTaskState);
begin
  Lock;
  try
    FState := AValue;
    SetLastUpdateToNow;
  finally
    Unlock;
  end;
end;

procedure TevCustomTask.SetCaption(const AValue: string);
begin
  FCaption := AValue;
end;

function TevCustomTask.GetSeenByUser: Boolean;
begin
  Result := FSeenByUser;
end;

procedure TevCustomTask.SetSeenByUser(const AValue: Boolean);
begin
  FSeenByUser := AValue;
end;

procedure TevCustomTask.UpdateStorage;
begin
  StoreFullTask;
  StoreFullTaskState;
end;


function TevCustomTask.GetRequests: IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  Lock;
  try
    Result.Capacity := RequestCount;
    for i := 0 to RequestCount - 1 do
      Result.Add(GetRequest(i));
  finally
    Unlock;
  end;
end;

procedure TevCustomTask.ModifyTaskProperty(const APropName: String; const ANewValue: Variant);
begin
  if AnsiSameText(APropName, 'Priority') then
    SetPriority(ANewValue)
  else if AnsiSameText(APropName, 'NotificationEmail') then
    SetNotificationEmail(ANewValue)
  else if AnsiSameText(APropName, 'SendEmailNotification') then
    SetSendEmailNotification(ANewValue)
  else if AnsiSameText(APropName, 'SeenByUser') then
    SetSeenByUser(ANewValue)
  else if AnsiSameText(APropName, 'Notes') then
    FNotes := ANewValue
  else
    Assert(False);

  UpdateStorage;
end;


procedure TevCustomTask.Terminate;
var
  i: Integer;
begin
  Lock;
  try
    try
      AddLogEvent('Task terminating');
      for i := 0 to RequestCount - 1 do
        try
          TevTaskRequest((GetRequest(i) as IisInterfacedObject).GetImplementation).Terminate;
        except
          on E: Exception do
            AddException('Terminate request error: ' + E.Message);
        end;
      RemoveAllChildren;
      AddException('Task has been terminated');
    finally
      FinalizeTask;
    end;
  finally
    Unlock;
  end;

  mb_TaskQueue.Pulse;
end;

procedure TevCustomTask.SetDefaults;
var
  Q: IevQuery;
  s: String;
begin
  FNotificationEmail := Context.UserAccount.EMail;

  // Set priority
  // User level
  Q := TevQuery.Create('GenericSelectCurrentWithCondition');
  Q.Macros.AddValue('TABLENAME', 'SB_QUEUE_PRIORITY');
  Q.Macros.AddValue('COLUMNS', 'PRIORITY, THREADS');
  Q.Macros.AddValue('CONDITION', 'SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr) +
    ' AND UpperCase(METHOD_NAME) = ''' + AnsiUpperCase(TaskTypeToMethodName(GetTaskType)) + '''');
  FPriority := Q.Result.Fields[0].AsInteger;
  FMaxThreads := Q.Result.Fields[1].AsInteger;

  if (FPriority =  0) or (FMaxThreads = 0) then
  begin
    // Sb Group Level
    s := 'SELECT Max(Priority), Max(THREADS) FROM SB_QUEUE_PRIORITY p, SB_SEC_GROUP_MEMBERS s ' +
    'WHERE {AsOfNow<p>} and {AsOfNow<s>} and ' +
    'p.SB_SEC_GROUPS_NBR = s.SB_SEC_GROUPS_NBR and s.Sb_User_Nbr = ' + IntToStr(Context.UserAccount.InternalNbr)+ ' and ' +
    'UpperCase(Method_Name) = ''' + AnsiUpperCase(TaskTypeToMethodName(GetTaskType)) + '''';
    Q := TevQuery.Create(s);
    if FPriority =  0 then
      FPriority := Q.Result.Fields[0].AsInteger;
    if FMaxThreads =  0 then
      FMaxThreads := Q.Result.Fields[1].AsInteger;

    if (FPriority =  0) or (FMaxThreads = 0) then
    begin
      // Sb Level
      Q := TevQuery.Create('GenericSelectCurrentWithCondition');
      Q.Macros.AddValue('TABLENAME', 'SB_QUEUE_PRIORITY');
      Q.Macros.AddValue('COLUMNS', 'PRIORITY, THREADS');
      Q.Macros.AddValue('CONDITION', 'SB_USER_NBR IS NULL AND SB_SEC_GROUPS_NBR IS NULL AND ' +
        'UpperCase(METHOD_NAME) = ''' + AnsiUpperCase(TaskTypeToMethodName(GetTaskType)) + '''');
      if FPriority =  0 then
        FPriority := Q.Result.Fields[0].AsInteger;
      if FMaxThreads =  0 then
        FMaxThreads := Q.Result.Fields[1].AsInteger;

      if (FPriority =  0) or (FMaxThreads = 0) then
      begin
        // System Level
        Q := TevQuery.Create('GenericSelectCurrentWithCondition');
        Q.Macros.AddValue('TABLENAME', 'SY_QUEUE_PRIORITY');
        Q.Macros.AddValue('COLUMNS', 'PRIORITY, THREADS');
        Q.Macros.AddValue('CONDITION', 'UpperCase(METHOD_NAME) = ''' + AnsiUpperCase(TaskTypeToMethodName(GetTaskType)) + '''');

        if FPriority =  0 then
        begin
          FPriority := Q.Result.Fields[0].AsInteger;
          if FPriority =  0 then
            FPriority := GetDefaultPriority;
        end;

        if FMaxThreads =  0 then
        begin
          FMaxThreads := Q.Result.Fields[1].AsInteger;
          if FMaxThreads =  0 then
            FMaxThreads := GetDefaultMaxThreads;
        end;
      end;
    end;
  end;
end;


function TevCustomTask.GetDefaultPriority: Integer;
begin
  Result := 1000;
end;

procedure TevCustomTask.SendEmail(const ATo, ASubject, AMessage: String);
begin
  try
    ctx_EMailer.SendQuickMail(
        ATo,
        'Queue_manager@' + mb_GlobalSettings.EmailInfo.NotificationDomainName,
        ASubject, AMessage);
  except
    on E: Exception do
      AddWarning('Email sending problem: Email Notification Settings are set up incorrectly.  Please see the system administrator.');
  end;
end;

procedure TevCustomTask.PulseRequests;
var
  i, j: Integer;
  R: IevTaskRequest;
  RequestResults: IisList;
  s: String;
  MergeGroups: IisStringList;
  bAllFinished: Boolean;
begin
  // Checking for finished requests which should FORK but stay in request list
  for i := 0 to RequestCount - 1 do
  begin
    R := GetRequest(i);
    if (R.State = rsFinished) and FinishesWith(R.NextTaskPhase, sTaskPhase_Postfix_Stop) then
    begin
      RequestResults := TisList.Create;
      RequestResults.Add(R);
      s := R.NextTaskPhase;
      s := GetNextStrValue(s, '.');
      RunTaskPhase(s, RequestResults);
      TevTaskRequest((R as IisInterfacedObject).GetImplementation).FNextTaskPhase := ''; // it says don't do anything with this request after
      RequestResults := nil;
    end
  end;

  // Checking for finished requests which should FORK
  for i := RequestCount - 1 downto 0 do
  begin
    R := GetRequest(i);
    if (R.State = rsFinished) and FinishesWith(R.NextTaskPhase, sTaskPhase_Postfix_Fork) then
    begin
      RequestResults := TisList.Create;
      RequestResults.Add(R);
      RemoveChild(R as IisInterfacedObject); // that's why it's "downto"
      s := R.NextTaskPhase;
      s := GetNextStrValue(s, '.');
      RunTaskPhase(s, RequestResults);
      RequestResults := nil;
    end
  end;

  // Checking for finished requests which should MERGE
  MergeGroups := TisStringList.CreateUnique;
  // Collect groups
  for i := 0 to RequestCount - 1 do
  begin
    R := GetRequest(i);
    if (R.State = rsFinished) and FinishesWith(R.NextTaskPhase, sTaskPhase_Postfix_Merge) then
      MergeGroups.Add(R.NextTaskPhase);
  end;

  for i := 0 to MergeGroups.Count - 1 do
  begin
    // checking that all request in the group are finished
    bAllFinished := True;
    RequestResults := TisList.Create;
    RequestResults.Capacity := RequestCount;
    for j := 0 to RequestCount - 1 do
    begin
      R := GetRequest(j);
      if AnsiSameText(R.NextTaskPhase, MergeGroups[i]) then
        if R.State = rsFinished then
          RequestResults.Add(R)
        else
        begin
          bAllFinished := False;
          break;
        end;
    end;

    if bAllFinished then
    begin
      // Run merging phase
      for j := 0 to RequestResults.Count - 1 do
        RemoveChild(RequestResults[j] as IisInterfacedObject);

      s := MergeGroups[i];
      s := GetNextStrValue(s, '.');
      RunTaskPhase(s, RequestResults);
    end;
    RequestResults := nil;
  end;
end;

procedure TevCustomTask.StopOnExceptions(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  bStop: Boolean;
begin
  bStop := False;
  for i := Low(AInputRequests) to High(AInputRequests) do
    if AInputRequests[i].GetExceptions <> '' then
    begin
      AddException('Request "' + AInputRequests[i].RequestName + '"'#13 + AInputRequests[i].GetExceptions + #13);
      bStop := True;
    end;

  if bStop then
    AbortEx;
end;

procedure TevCustomTask.DoStoreTaskState(const AStream: IisStream);
begin
  AStream.WriteByte(Ord(FState));
  AStream.WriteDouble(FLastUpdate);
  AStream.WriteInteger(FPhaseCounter);
  AStream.WriteInteger(FPriority);
  AStream.WriteShortString(FCaption);
  AStream.WriteString(FProgressText);
  AStream.WriteString(FLog);
  AStream.WriteString(FExceptions);
  AStream.WriteString(FWarnings);
end;

procedure TevCustomTask.DoRestoreTaskState(const AStream: IisStream);
begin
  FState := TevTaskState(AStream.ReadByte);
  FLastUpdate := AStream.ReadDouble;
  FPhaseCounter := AStream.ReadInteger;
  FPriority := AStream.ReadInteger;
  FCaption := AStream.ReadShortString;
  FProgressText := AStream.ReadString;
  FLog := AStream.ReadString;
  FExceptions := AStream.ReadString;
  FWarnings := AStream.ReadString;
end;

procedure TevCustomTask.RegisterTaskCall(const ATaskRoutine: String; const AEntryPoint: TevAsyncCallProc);
begin
  FLocalAsyncCallHandler.RegisterCall(ATaskRoutine, Self, AEntryPoint);
end;

function TevCustomTask.MakeAsyncCall(const ATaskRoutineName: String; const AParams: IisListOfValues): TisGUID;
var
  Res: Boolean;

begin
  Result := GetUniqueID;

  if FLocalAsyncCallHandler.IsRegistered(ATaskRoutineName) then
    Res := FLocalAsyncCallHandler.MakeCall(ATaskRoutineName, AParams, GetPriority, Result) // run local routine
  else
    Res := mb_AsyncFunctions.MakeCall(ATaskRoutineName, AParams, GetPriority, Result); // run remote routine

  if not Res then
    Result := '';
end;

function TevCustomTask.AllRequestsAreFinished: Boolean;
var
  i: Integer;
  R: IevTaskRequest;
begin
  Result := True;

  for i := 0 to RequestCount - 1 do
  begin
    R := GetRequest(i);
    if R.State <> rsFinished then
    begin
      Result := False;
      break;
    end
  end;
end;

function TevCustomTask.GetFirstRunAt: TDateTime;
begin
  Result := FFirstRunAt;
end;

function TevCustomTask.BeforeTaskStart: Boolean;
begin
  Result := True;
end;

procedure TevCustomTask.AfterTaskFinished;
begin
end;

procedure TevCustomTask.SetAccountInfo(const ADomain, AUser: String);
begin
  //todo Only for Lincoln! Needs to be removed after.
  FUserInfo.Domain := ADomain;
  FUserInfo.UserName := AUser;
end;

procedure TevCustomTask.SetID(const AValue: TisGUID);
begin
  SetName(AValue);
end;

procedure TevCustomTask.DoOnActivate;
begin
end;


procedure TevCustomTask.StoreTaskState;
var
  Stream: IisStream;
  sNewStateFile: String;
  sStateFile: String;
begin
  sNewStateFile := FStateStorageLocation + 'new_state' + sStateFileExt;
  Stream := TisStream.CreateFromNewFile(sNewStateFile);
  DoStoreTaskState(Stream);
  Stream := nil;

  sStateFile := FStateStorageLocation + 'state' + sStateFileExt;
  CopyFile(sNewStateFile, sStateFile, False);

  DeleteFile(sNewStateFile);
end;

procedure TevCustomTask.ReadChildrenFromStream(const AStream: IisStream);
begin
  // don't read children
end;

procedure TevCustomTask.WriteChildrenToStream(const AStream: IisStream);
begin
  // don't write children
end;

function TevCustomTask.GetEmailSendRule: TevEmailSendRule;
begin
  Result := FEmailSendRule;
end;

procedure TevCustomTask.SetEmailSendRule(const AValue: TevEmailSendRule);
begin
  FEmailSendRule := AValue;
end;

procedure TevCustomTask.SendEmailNotificationToUser;
var
  bSend: boolean;
begin
  if not (FSendEmailNotification and (FNotificationEmail <> '')) then
    Exit;

   bSend := false;
   case FEmailSendRule of
    esrAlways:      bSend := True;
    esrSuccessfull:begin
                    bSend := True;
                    if ((FExceptions <> '') or (FWarnings <> '')) then
                        bSend := false;
                   end;
    esrExceptions:  if FExceptions <> '' then
                       bSend := True;
    esrExceptionWarnings:
                    if (FExceptions <> '') or (FWarnings <> '') then
                       bSend := True;
   end;

  if bSend then
    SendEmail(FNotificationEmail,
      'Your task ' + GetProgressText,
      '#' + IntToStr(GetNbr) + #10#13 + GetTaskType + #10#13 + GetCaption+ #10#13 + GetProgressText);
end;

function TevCustomTask.GetDefaultMaxThreads: Integer;
begin
  Result := 50;
end;

function TevCustomTask.GetMaxThreads: Integer;
begin
  Result := FMaxThreads;
end;

procedure TevCustomTask.SetMaxThreads(const AValue: Integer);
begin
  FMaxThreads := AValue;
end;

function TevCustomTask.Revision: TisRevision;
begin
  Result := 3;
end;

function TevCustomTask.GetRunStatistics: IisStatEvent;
begin
  Result := FRunStatistics;
end;

function TevCustomTask.MakeUniqueRequestName(const ABaseName: String): String;
var
  i: Integer;
begin
  Lock;
  try
    i := 1;
    Result := ABaseName;
    while FindRequest(Result) <> nil do
    begin
      Result := ABaseName + ' #' + IntToStr(i);
      Inc(i);
    end;
  finally
    Unlock;
  end;
end;

function TevCustomTask.FindRequest(const ARequestName: String): IevTaskRequest;
begin
  Result := FindChildByName(ARequestName) as IevTaskRequest;
end;

function TevCustomTask.CanExecute: Boolean;
begin
  Result := not IsMaintenanceMode;
end;

{ TevTaskRequest }

constructor TevTaskRequest.Create(const AName, ATaskRoutineName: String; const AParams: IisListOfValues;
                                  const ANextTaskPhase: String; const APhaseNumber: Integer;
                                  const AGlobalFlagName: string);
begin
  inherited Create;

  if AName = '' then
    SetName(GetUniqueID)
  else
    SetName(AName);

  FTaskRoutineName := ATaskRoutineName;
  FParams := AParams;

  FNextTaskPhase := ANextTaskPhase;
  if FNextTaskPhase = '' then
    FNextTaskPhase := sTaskPhase_End + sTaskPhase_Postfix_Merge;

  FPhaseNumber := APhaseNumber;
  FGlobalFlagName := AGlobalFlagName;
  FGlobalFlagLocked := False;
end;

destructor TevTaskRequest.Destroy;
begin
  if Assigned(FContext) then // Request is not finished
  begin
    mb_TaskQueue.Pulse;
    FContext := nil; 
  end;

  inherited;
end;

procedure TevTaskRequest.EndWait;
begin
  if FProgressInfoStack.Count > 0 then
  begin
    FProgressInfo := FProgressInfoStack[FProgressInfoStack.Count - 1] as IevProgressInfo;
    FProgressInfoStack.Delete(FProgressInfoStack.Count - 1);
  end;
end;

function TevTaskRequest.GetExceptions: String;
begin
  Result := FExceptions;
end;

class function TevTaskRequest.GetTypeID: String;
begin
  Result := 'TaskRequest';
end;

function TevTaskRequest.GetWarnings: String;
begin
  Result := FWarnings;
end;

function TevTaskRequest.Params: IisListOfValues;
begin
  Result := FParams;
end;

procedure TevTaskRequest.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
var
  S: IisStream;
begin
  inherited;

  SetName(AStream.ReadShortString);

  FNextTaskPhase := AStream.ReadShortString;
  FPhaseNumber := AStream.ReadInteger;
  FGlobalFlagName := AStream.ReadShortString;
  FTaskRoutineName := AStream.ReadShortString;
  FState := TevTaskRequestState(AStream.ReadByte);

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FParams := TisListOfValues.Create;
    FParams.AsStream := S;
  end
  else
    FParams := nil;

  FExceptions := AStream.ReadString;
  FWarnings := AStream.ReadString;

  S := AStream.ReadStream(nil, True);    
  if Assigned(S) then
  begin
    FResults := TisListOfValues.Create;
    FResults.AsStream := S;
  end
  else
    FResults := nil;
end;

function TevTaskRequest.RequestName: String;
begin
  Result := GetName;
end;

function TevTaskRequest.Results: IisListOfValues;
begin
  Result := FResults;
end;

procedure TevTaskRequest.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
var
  v: IisNamedValue;
begin
  try
    Task.Lock;
    try
      try
        Mainboard.ContextManager.SetThreadContext(FContext);
        try
          if Assigned(AResults) then
          begin
            V := AResults.FindValue(PARAM_ERROR);
            if Assigned(V) then
            begin
              FExceptions := V.Value;
              AResults.DeleteValue(V.Name);
            end;

            V := AResults.FindValue(PARAM_WARNING);
            if Assigned(V) then
            begin
              FWarnings := V.Value;
              AResults.DeleteValue(V.Name);
            end;

            if Assigned(FStatisticsEvent) then
            begin
              V := AResults.FindValue(PARAM_STATISTICS);
              if Assigned(V) then
                ctx_Statistics.ActiveEvent.AddEvent(IInterface(V.Value) as IisStatEvent);

              FStatisticsEvent.AddEvent(ctx_Statistics.ActiveEvent);
              ctx_Statistics.EndEvent;
            end;
          end;

          FResults := AResults;

          FState := rsFinished;
          FFinishedAt := Now;
          Task.AddLogEvent('Request "' + GetName + '" finished');

          if FGlobalFlagLocked then
            if mb_GlobalFlagsManager.TryUnlock(FGlobalFlagName) then
              FGlobalFlagLocked := False;

          StoreRequestState;
          Task.StoreTaskState;
        finally
          Mainboard.ContextManager.DestroyThreadContext;
          FAsyncCallID := '';
          FContext := nil;
        end;
      except
        on E: Exception do
        begin
          FExceptions := 'Fatal request failure:' + E.Message;
          FState := rsFinished;
          FFinishedAt := Now;
          StoreRequestState;
        end;
      end;
    finally
      Task.UnLock;
    end;
  finally
    mb_TaskQueue.Pulse;  // it should notify Queue no matter what
  end;
end;

function TevTaskRequest.Run: TevTRRunStatus;
var
  Res: IisListOfValues;
begin
  Assert(FState = rsWaiting);

  try
    FStartedAt := Now;
    FState := rsStarting;
    Result := rsFailed;
    FContext := Mainboard.ContextManager.CopyContext(Task.FContext);
    FContext.SetCallbacks(Self);

    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.SetThreadContext(FContext);

      ctx_Statistics.Enabled := Task.FContext.Statistics.Enabled;
      FStatisticsEvent := Task.FContext.Statistics.ActiveEvent;
      if ctx_Statistics.Enabled then
      begin
        ctx_Statistics.BeginEvent('Request');
        ctx_Statistics.ActiveEvent.Properties.AddValue('Caption', RequestName);
      end;

      if (FGlobalFlagName <> '') then
        if mb_GlobalFlagsManager.TryLock(FGlobalFlagName) then
          FGlobalFlagLocked := True
        else
        begin
          FProgressInfo.ProgressText := 'Waiting for locked flag ' + FGlobalFlagName;
          Result := rsGlobalFlagLocked;
          FState := rsWaiting;
          Exit;
        end;

      try
        if FTaskRoutineName = '' then
        begin
          // Dummy request
          FState := rsExecuting;
          Res := TisListOfValues.Create;
          FAsyncCallID := '';
          AsyncCallReturn(FAsyncCallID, Res);
          Result := rsOK;
        end
        else
        begin
          // Remote request
          FAsyncCallID := Task.MakeAsyncCall(FTaskRoutineName, FParams);
          if FAsyncCallID <> '' then
          begin
            Result := rsOK;
            FState := rsExecuting;
            Task.AddLogEvent('Request "' + GetName + '" started');
          end
          else
          begin
            Result := rsNoResources;
            FState := rsWaiting;
          end;
        end;
      finally
        if (Result <> rsOK) and FGlobalFlagLocked then
        begin
          CheckCondition(mb_GlobalFlagsManager.TryUnlock(FGlobalFlagName), 'Flag unlocking failed');
          FGlobalFlagLocked := False;
        end;
      end;

    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

  except
    on E: Exception do
    begin
      FExceptions := BuildStackedErrorStr(E);
      Terminate;
      AsyncCallReturn(FAsyncCallID, nil);
      Result := rsFailed;
    end;
  end;
end;

procedure TevTaskRequest.StartWait(const AText: string; AMaxProgress: Integer);
var
  s: String;
begin
  if AText = '' then
    s := FProgressInfo.ProgressText
  else
    s := AText;

  FProgressInfoStack.Add(FProgressInfo);

  FProgressInfo := TevProgressInfo.Create;
  FProgressInfo.ProgressMax := AMaxProgress;
  FProgressInfo.ProgressText := s;
end;

function TevTaskRequest.State: TevTaskRequestState;
begin
  Result := FState;
end;

function TevTaskRequest.Task: TevCustomTask;
begin
  if GetOwner <> nil then
    Result := TevCustomTask(GetOwner.GetImplementation)
  else
    Result := nil;
end;

function TevTaskRequest.TaskRoutineName: String;
begin
  Result := FTaskRoutineName;
end;

procedure TevTaskRequest.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  FProgressInfo.ProgressText := AText;
  FProgressInfo.ProgressCurr := ACurrentProgress;
  if AMaxProgress <> -1 then
    FProgressInfo.ProgressMax := AMaxProgress;
end;

procedure TevTaskRequest.WriteSelfToStream(const AStream: IisStream);
var
  S: IisStream;
begin
  inherited;

  AStream.WriteShortString(GetName);

  AStream.WriteShortString(FNextTaskPhase);
  AStream.WriteInteger(FPhaseNumber);
  AStream.WriteShortString(FGlobalFlagName);
  AStream.WriteShortString(FTaskRoutineName);
  AStream.WriteByte(Ord(FState));

  if Assigned(FParams) then
    S := FParams.AsStream
  else
    S := nil;
  AStream.WriteStream(S);

  AStream.WriteString(FExceptions);
  AStream.WriteString(FWarnings);

  if Assigned(FResults) then
    S := FResults.AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

procedure TevTaskRequest.SetRequestStorageLocation(const ALocation: String);
begin
  FRequestStorageLocation := ALocation;

  if not FileExists(FRequestStorageLocation) then
  begin
    ForceDirectories(ExtractFilePath(FRequestStorageLocation));
    StoreRequestState;
  end
  else
    RestoreRequestState;
end;

procedure TevTaskRequest.RestoreRequestState;
var
  Stream: IisStream;
begin
  Stream := TisStream.CreateFromFile(FRequestStorageLocation);
  ReadFromStream(Stream);
end;

procedure TevTaskRequest.StoreRequestState;
var
  Stream: IisStream;
begin
  Stream := TisStream.CreateFromNewFile(FRequestStorageLocation);
  WriteToStream(Stream);
end;


function TevTaskRequest.GetProgressText: String;
begin
  Result := FProgressInfo.ProgressText;
end;

function TevTaskRequest.GetStartedAt: TDateTime;
begin
  Result := FStartedAt;
end;

procedure TevTaskRequest.UpdatePriority;
begin
  if FAsyncCallID <> '' then
    mb_AsyncFunctions.SetCallPriority(FAsyncCallID, Task.GetPriority);
end;

procedure TevTaskRequest.Terminate;
begin
  if Assigned(FContext) then
    FContext.SetCallbacks(nil);

  if FAsyncCallID <> '' then
  begin
    mb_AsyncFunctions.TerminateCall(FAsyncCallID);
    FAsyncCallID := '';
    FState := rsFinished;
  end;

  if FGlobalFlagLocked then
  begin
    mb_GlobalFlagsManager.ForcedUnlock(FGlobalFlagName);
    FGlobalFlagLocked := False;
  end;
end;


function TevTaskRequest.NextTaskPhase: String;
begin
  Result := FNextTaskPhase;
end;


function TevTaskRequest.GetFinishedAt: TDateTime;
begin
  Result := FFinishedAt;
end;

procedure TevTaskRequest.DoOnConstruction;
begin
  inherited;
  FProgressInfoStack := TisList.Create;
  FProgressInfo := TevProgressInfo.Create;
end;

procedure TevTaskRequest.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

{ TEvPrintableTask }

function TEvPrintableTask.GetDestination: TrwReportDestination;
begin
  Result := FDestination;
end;

procedure TEvPrintableTask.Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList);
var
  V: IisNamedValue;
  i: Integer;
  RepRes: IisStream;
  Params: IisListOfValues;
  sReqName: String;
  sVMRChecksExtraInfo : IisStringList;
begin
  Params := TisListOfValues.Create;

  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    AddException(AInputRequests[i].GetExceptions);
    AddWarning(AInputRequests[i].GetWarnings);

    V := AInputRequests[i].Results.FindValue(PARAM_RESULT);
    if Assigned(V) then
    begin
      RepRes := IInterface(V.Value) as IisStream;
      Params.AddValue('Report' + IntToStr(i), RepRes);
    end;

    V := AInputRequests[i].Results.FindValue('DashboardData');
    if Assigned(V) then
      Params.AddValue('DashboardData' + IntToStr(i), IInterface(V.Value) as IisListOfValues);

    //This Param will be used in VMR to Send Self Serve Email if EE is Print_voucher = 'Y'.
    if (ctx_VmrEngine <> nil) and Context.License.VMR then
    begin
      V := AInputRequests[i].Results.FindValue('CHECKS_EXTRA_INFO');
      if Assigned(V) then
      begin
        sVMRChecksExtraInfo := IInterface(V.Value) as IisStringList;
        Params.AddValue('CHECKS_EXTRA_INFO', sVMRChecksExtraInfo.Text);
      end;
    end;
  end;

  if Params.Count > 0 then
  begin
    if GetDestination = rdtPrinter then
      sReqName := MakeUniqueRequestName('Print reports')
    else if GetDestination = rdtVMR then
      sReqName := MakeUniqueRequestName('Send reports to VMR')
    else
      sReqName := MakeUniqueRequestName('Store reports');

    AddRequest(sReqName, sTaskCall_ProcessReportResults, Params,
      sTaskPhase_PostProcessReportResults + sTaskPhase_Postfix_Merge);
  end;
end;

procedure TEvPrintableTask.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
begin
  inherited;
  FDestination := TrwReportDestination(AStream.ReadByte);
  FReportResults := AStream.ReadStream(nil, True);
end;

procedure TEvPrintableTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase(sTaskPhase_ProcessReportResults, Phase_ProcessReportResults);
  RegisterTaskPhase(sTaskPhase_PostProcessReportResults, Phase_PostProcessReportResults);
  RegisterTaskCall(sTaskCall_ProcessReportResults, Call_ProcessReportResults);
end;

function TEvPrintableTask.ReportResults: IisStream;
begin
  Result := FReportResults;
end;

procedure TEvPrintableTask.SendToPrinter(const AData: IisStream);
begin
  if mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview'] then
    FThereAreUnprintedReports := True
  else
    ctx_RWLocalEngine.Print(AData);
end;

procedure TEvPrintableTask.PrepareByVMR(var AData: IisStream);
begin
  if ctx_VmrEngine <> nil then
    if Context.License.VMR and ctx_VmrEngine.VmrPostProcessReportResult(AData) then
    begin
      AddNotes('Some of the reports/checks have been submitted to mail room');
      FThereAreUnprintedReports := Assigned(AData);
    end;
end;

procedure TEvPrintableTask.SetDestination(const AValue: TrwReportDestination);
begin
  FDestination := AValue;
end;

function TEvPrintableTask.Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues;
var
  i: Integer;
  S, Res: IisStream;
  CombinedResults: TrwReportResults;
  sVMRChecksExtraInfo : IisStringList;

  procedure ProcessDashboardData(AData: IisListOfValues);
  var
    i: Integer;
  begin
    for i := 0 to AData.Count - 1 do
      (Mainboard.DashboardDataProvider as IevDashboardDataProviderExt).StoreReportData(AData[i].Value);
  end;

  procedure AddToResult(const OneRepRes: IisStream);
  begin
    if not Assigned(CombinedResults) and not Assigned(Res) then
      Res := OneRepRes

    else
    begin
      if not Assigned(CombinedResults) then
      begin
        Res.Position := 0;
        CombinedResults := TrwReportResults.Create(Res);
        Res := nil;
      end;

      CombinedResults.AddReportResults(OneRepRes);
    end;
  end;

  procedure ProcessRes(const AData:IisStream);
  begin
    if Assigned(AData) then
    begin
      if GetDestination = rdtRemotePrinter then
        SendToPrinter(AData)
      else if GetDestination = rdtRemoteStorage then
        SendToStorage(AData);
      AddToResult(AData);
    end;
  end;

begin
  Res := nil;
  CombinedResults := nil;
  try
    //This Param will be used in VMR to Send Self Serve Email
    if ( ctx_VmrEngine <> nil ) and Context.License.VMR then
    begin
      if AParams.ValueExists('CHECKS_EXTRA_INFO') then
      begin
        sVMRChecksExtraInfo := TisStringList.Create;
        sVMRChecksExtraInfo.Text := AParams.Value['CHECKS_EXTRA_INFO'];
        ctx_VmrEngine.SetChecksExtraInfo(sVMRChecksExtraInfo);
      end;
    end;

    if AlwaysSendToVMR or (GetDestination = rdtVMR) then
    begin
      for i := 0 to AParams.Count - 1 do
        if StartsWith(AParams[i].Name, 'Report') then
        begin
          S := IInterface(AParams[i].Value) as IisStream;
          PrepareByVMR(S);
          ProcessRes(S);
        end
        else if StartsWith(AParams[i].Name, 'DashboardData') then
          ProcessDashboardData(IInterface(AParams[i].Value) as IisListOfValues);
    end
    else
      for i := 0 to AParams.Count - 1 do
        if StartsWith(AParams[i].Name, 'Report') then
        begin
          S := IInterface(AParams[i].Value) as IisStream;
          ProcessRes(S);
        end
        else if StartsWith(AParams[i].Name, 'DashboardData') then
          ProcessDashboardData(IInterface(AParams[i].Value) as IisListOfValues);

    if Assigned(CombinedResults) and (CombinedResults.Count > 0) then
      Res := CombinedResults.GetAsStream;
  finally
    FreeAndNil(CombinedResults);
  end;

  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, Res);
end;

procedure TEvPrintableTask.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteByte(Ord(FDestination));
  AStream.WriteStream(FReportResults);
end;

procedure TEvPrintableTask.Phase_PostProcessReportResults(const AInputRequests: TevTaskRequestList);
var
  V: IisNamedValue;
  i: Integer;
  RepRes: IisStream;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    AddException(AInputRequests[i].GetExceptions);
    AddWarning(AInputRequests[i].GetWarnings);

    V := AInputRequests[i].Results.FindValue(PARAM_RESULT);
    if Assigned(V) then
      RepRes := IInterface(V.Value) as IisStream
    else
      RepRes := nil;

    if (Low(AInputRequests) = High(AInputRequests)) and not Assigned(FReportResults) then
      FReportResults := RepRes
    else if Assigned(RepRes) then
    begin
      if not Assigned(FReportResults) then
        FReportResults := TisStream.Create;
      FReportResults.CopyFrom(RepRes, 0); // Dump all report results in one stream (hard to say when it may happen)
    end;
  end;
end;

procedure TEvPrintableTask.DoRestoreTaskState(const AStream: IisStream);
begin
  inherited;
  FReportResults := AStream.ReadStream(FReportResults, True);
end;

procedure TEvPrintableTask.DoStoreTaskState(const AStream: IisStream);
begin
  inherited;
  AStream.WriteStream(FReportResults);
end;

procedure TEvPrintableTask.Phase_End(const AInputRequests: TevTaskRequestList);
begin
  inherited;
  if Assigned(FReportResults) and ((GetDestination = rdtPrinter) or FThereAreUnprintedReports) and Assigned(Mainboard.GlobalCallbacks) then
    Mainboard.GlobalCallbacks.NotifyTaskQueueEvent(EncodeUserAtDomain(GetUser, GetDomain), tqeTaskReadyToPrint, GetID);
end;

function TEvPrintableTask.AlwaysSendToVMR: Boolean;
begin
  Result := False;
end;


procedure TEvPrintableTask.SendToStorage(const AData: IisStream);
begin
  ctx_RWLocalEngine.SaveToFile(AData);
end;

function TEvPrintableTask.MakeAsyncCall(const ATaskRoutineName: String;
  const AParams: IisListOfValues): TisGUID;
var
  Stream: IisStream;
  RepList: TrwReportList;
  i: Integer;
  ASCIIPrefix: String;
  PathPatched: Boolean;
  WarningMessage: String;

  function PathIsSafe(const APath: String): Boolean;
  begin
    Result := (pos('\\', APath) = 0) and (pos('..', APath) = 0) and (pos(':', APath) = 0);
  end;

begin

  ASCIIPrefix := ctx_DomainInfo.FileFolders.ASCIIPrefix;

  if (ASCIIPrefix <> '') and ((ATaskRoutineName = 'RunReports')) then
  begin
    PathPatched := False;
    Stream := IInterface(AParams.Value['ReportList']) as IevDualStream;
    RepList := TrwReportList.Create;
    try
      Stream.Position := 0;
      RepList.SetFromStream(Stream);
      Stream.Position := 0;
      for i := 0 to RepList.Count - 1 do
      begin
        if RepList[i].Params.FileName <> '' then
        begin
          PathPatched := True;
          if FDestination = rdtPreview then
            WarningMessage := 'File will be saved to '
          else
            WarningMessage := 'File saved to ';

          if PathIsSafe(RepList[i].Params.FileName) then
          begin
            RepList[i].Params.FileName := NormalizePath(ASCIIPrefix) + RepList[i].Params.FileName;
          end
          else
          begin
            AddWarning('Output file name ' + RepList[i].Params.FileName + ' is invalid path; please edit to use ASCII Export Prefix');
            RepList[i].Params.FileName := NormalizePath(ASCIIPrefix) + ExtractFileName(RepList[i].Params.FileName);
            AddWarning(WarningMessage + RepList[i].Params.FileName);
          end;
          AddLogEvent(WarningMessage + RepList[i].Params.FileName);
        end;
      end;
      if PathPatched then
      begin
        Stream := RepList.GetAsStream;
        Stream.Position := 0;
        AParams.Value['ReportList'] := Stream;
      end;
    finally
      RepList.Free;
    end;
  end;

  Result := inherited MakeAsyncCall(ATaskRoutineName, AParams);
end;

end.

