unit EvProcessTaxPaymentsTaskPxy;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTaskPxy, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, EvClasses;


implementation

type
  TevProcessTaxPaymentsTaskProxy = class(TEvCustomTaskProxy, IevProcessTaxPaymentsTask)
  private
    FParams: IevTaxPaymentsTaskParams;
    FForceChecks: Boolean;
    FSbTaxPaymentNbr: Integer;
    FEftpReference: String;
    FDefaultMiscCheckForm: String;
    FChecks: IevDualStream;
    FAchFile: IevDualStream;
    FAchReport: IevDualStream;
    FEftFile: IevDualStream;
    FNyDebitFile: IevDualStream;
    FMaDebitFile: IevDualStream;
    FCaDebitFile: IevDualStream;
    FUnivDebitFile: IevDualStream;
    FOneMiscCheck: IevDualStream;
    FOneMiscCheckReport: IevDualStream;
    FPostProcessReportName: String;
  protected
    procedure  DoOnConstruction; override;
    function   Revision: TisRevision; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetPostProcessReportName: String;
    procedure SetPostProcessReportName(const AValue: String);
    function  GetParams: IevTaxPaymentsTaskParams;
    function  GetForceChecks: Boolean;
    procedure SetForceChecks(const AValue: Boolean);
    function  GetSbTaxPaymentNbr: Integer;
    procedure SetSbTaxPaymentNbr(const AValue: Integer);
    function  GetEftpReference: String;
    procedure SetEftpReference(const AValue: String);
    function  GetDefaultMiscCheckForm: String;
    procedure SetDefaultMiscCheckForm(const AValue: String);
    function  GetChecks: IevDualStream;
    function  GetAchFile: IevDualStream;
    function  GetAchReport: IevDualStream;
    function  GetEftFile: IevDualStream;
    function  GetNyDebitFile: IevDualStream;
    function  GetMaDebitFile: IevDualStream;
    function  GetCaDebitFile: IevDualStream;
    function  GetUnivDebitFile: IevDualStream;
    function  GetOneMiscCheck: IevDualStream;
    function  GetOneMiscCheckReport: IevDualStream;
  public
    class function GetTypeID: String; override;
  end;

{ TevProcessTaxPaymentsTaskProxy }

procedure TevProcessTaxPaymentsTaskProxy.DoOnConstruction;
begin
  inherited;
  FParams := TevTaxPaymentsTaskParams.Create;
end;

function TevProcessTaxPaymentsTaskProxy.GetAchFile: IevDualStream;
begin
  Result := FAchFile;
end;

function TevProcessTaxPaymentsTaskProxy.GetAchReport: IevDualStream;
begin
  Result := FAchReport;
end;

function TevProcessTaxPaymentsTaskProxy.GetCaDebitFile: IevDualStream;
begin
  Result := FCaDebitFile;
end;

function TevProcessTaxPaymentsTaskProxy.GetChecks: IevDualStream;
begin
  Result := FChecks;
end;

function TevProcessTaxPaymentsTaskProxy.GetEftFile: IevDualStream;
begin
  Result := FEftFile;
end;

function TevProcessTaxPaymentsTaskProxy.GetEftpReference: String;
begin
  Result := FEftpReference;
end;

function TevProcessTaxPaymentsTaskProxy.GetForceChecks: Boolean;
begin
  Result := FForceChecks;
end;

function TevProcessTaxPaymentsTaskProxy.GetSbTaxPaymentNbr: Integer;
begin
  Result := FSbTaxPaymentNbr;
end;

function TevProcessTaxPaymentsTaskProxy.GetDefaultMiscCheckForm: String;
begin
  Result := FDefaultMiscCheckForm;
end;

function TevProcessTaxPaymentsTaskProxy.GetMaDebitFile: IevDualStream;
begin
  Result := FMaDebitFile;
end;

function TevProcessTaxPaymentsTaskProxy.GetNyDebitFile: IevDualStream;
begin
  Result := FNyDebitFile;
end;

function TevProcessTaxPaymentsTaskProxy.GetParams: IevTaxPaymentsTaskParams;
begin
  Result := FParams;
end;

class function TevProcessTaxPaymentsTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_TAX_PAYMENTS;
end;

function TevProcessTaxPaymentsTaskProxy.GetUnivDebitFile: IevDualStream;
begin
  Result := FUnivDebitFile;
end;

function TevProcessTaxPaymentsTaskProxy.GetOneMiscCheck: IevDualStream;
begin
  Result := FOneMiscCheck;
end;

function TevProcessTaxPaymentsTaskProxy.GetOneMiscCheckReport: IevDualStream;
begin
  Result := FOneMiscCheckReport;
end;


procedure TevProcessTaxPaymentsTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FParams as IisInterfacedObject).ReadFromStream(AStream);
  FForceChecks := AStream.ReadBoolean;
  FEftpReference := AStream.ReadString;
  FChecks := AStream.ReadStream(nil);
  FAchFile := AStream.ReadStream(nil);
  FAchReport := AStream.ReadStream(nil);
  FEftFile := AStream.ReadStream(nil);
  FNyDebitFile := AStream.ReadStream(nil);
  FMaDebitFile := AStream.ReadStream(nil);
  FCaDebitFile := AStream.ReadStream(nil);
  FUnivDebitFile := AStream.ReadStream(nil);
  FSbTaxPaymentNbr := AStream.ReadInteger;
  FOneMiscCheck := AStream.ReadStream(nil);
  FPostProcessReportName := AStream.ReadString;

  if ARevision >= 4 then
    FOneMiscCheckReport := AStream.ReadStream(nil)
  else
    FOneMiscCheckReport := nil;

  if ARevision >= 5 then
    FDefaultMiscCheckForm := AStream.ReadString
  else
    FDefaultMiscCheckForm := '';
end;

procedure TevProcessTaxPaymentsTaskProxy.SetEftpReference(const AValue: String);
begin
  FEftpReference := AValue;
end;

procedure TevProcessTaxPaymentsTaskProxy.SetForceChecks(const AValue: Boolean);
begin
  FForceChecks := AValue;
end;

procedure TevProcessTaxPaymentsTaskProxy.SetSbTaxPaymentNbr(const AValue: Integer);
begin
  FSbTaxPaymentNbr := AValue;
end;

procedure TevProcessTaxPaymentsTaskProxy.SetDefaultMiscCheckForm(const AValue: String);
begin
  FDefaultMiscCheckForm := AValue;
end;


procedure TevProcessTaxPaymentsTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FParams as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteBoolean(FForceChecks);
  AStream.WriteString(FEftpReference);
  AStream.WriteStream(FChecks);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchReport);
  AStream.WriteStream(FEftFile);
  AStream.WriteStream(FNyDebitFile);
  AStream.WriteStream(FMaDebitFile);
  AStream.WriteStream(FCaDebitFile);
  AStream.WriteStream(FUnivDebitFile);
  AStream.WriteInteger(FSbTaxPaymentNbr);
  AStream.WriteStream(FOneMiscCheck);
  AStream.WriteString(FPostProcessReportName);
  AStream.WriteStream(FOneMiscCheckReport);
  AStream.WriteString(FDefaultMiscCheckForm);
end;

function TevProcessTaxPaymentsTaskProxy.GetPostProcessReportName: String;
begin
  Result := FPostProcessReportName;
end;

procedure TevProcessTaxPaymentsTaskProxy.SetPostProcessReportName(
  const AValue: String);
begin
  FPostProcessReportName := AValue;
end;

function TevProcessTaxPaymentsTaskProxy.Revision: TisRevision;
begin
  Result := 5;
end;

initialization
  ObjectFactory.Register(TevProcessTaxPaymentsTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TevProcessTaxPaymentsTaskProxy);

end.
