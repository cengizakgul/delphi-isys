unit EvProcessManualACHTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses;

implementation

type
  TEvProcessManualACHTaskProxy = class(TEvCustomTask, IEvProcessManualACHTask)
  private
    FACHOptions: IevACHOptions;
    FBatchBANK_REGISTER_TYPE: String;
    FPreprocess: Boolean;
    FCTS: Boolean;
    FACHDataStream: IEvDualStream;
    FAchFile: IEvDualStream;
    FAchRegularReport: IEvDualStream;
    FAchDetailedReport: IEvDualStream;
    FAchSave: IEvDualStream;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetACHOptions: IevACHOptions;
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetPreprocess: Boolean;
    procedure SetPreprocess(const AValue: Boolean);
    function  GetCTS: Boolean;
    procedure SetCTS(const AValue: Boolean);
    function  GetBatchBANK_REGISTER_TYPE: String;
    procedure SetBatchBANK_REGISTER_TYPE(const AValue: String);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;

  public
    class function GetTypeID: String; override;
  end;

{ TEvProcessManualACHTaskProxy }

procedure TEvProcessManualACHTaskProxy.DoOnConstruction;
begin
  inherited;
  FACHOptions := TevACHOptions.Create;
  FACHDataStream := TEvDualStreamHolder.Create;
  FDaysStayInQueue := 10;
end;

function TEvProcessManualACHTaskProxy.GetACHDataStream: IEvDualStream;
begin
  Result := FACHDataStream;
end;

function TEvProcessManualACHTaskProxy.GetAchDetailedReport: IEvDualStream;
begin
  Result := FAchDetailedReport;
end;

function TEvProcessManualACHTaskProxy.GetAchFile: IEvDualStream;
begin
  Result := FAchFile;
end;

function TEvProcessManualACHTaskProxy.GetAchRegularReport: IEvDualStream;
begin
  Result := FAchRegularReport;
end;

function TEvProcessManualACHTaskProxy.GetAchSave: IEvDualStream;
begin
  Result := FAchSave;
end;

class function TEvProcessManualACHTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_MANUAL_ACH;
end;

procedure TEvProcessManualACHTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).ReadFromStream(AStream);
  FACHDataStream := AStream.ReadStream(nil);
  FBatchBANK_REGISTER_TYPE := AStream.ReadString;
  FPreprocess := AStream.ReadBoolean;
  FAchFile := AStream.ReadStream(nil, True);
  FAchRegularReport := AStream.ReadStream(nil, True);
  FAchDetailedReport := AStream.ReadStream(nil, True);
  FAchSave := AStream.ReadStream(nil, True);
end;

procedure TEvProcessManualACHTaskProxy.SetACHDataStream(const AValue: IEvDualStream);
begin
  FACHDataStream := AValue;
end;

procedure TEvProcessManualACHTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteStream(FACHDataStream);
  AStream.WriteString(FBatchBANK_REGISTER_TYPE);
  AStream.WriteBoolean(FPreprocess);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchRegularReport);
  AStream.WriteStream(FAchDetailedReport);
  AStream.WriteStream(FAchSave);
end;

function TEvProcessManualACHTaskProxy.GetPreprocess: Boolean;
begin
  Result := FPreprocess;
end;

procedure TEvProcessManualACHTaskProxy.SetPreprocess(
  const AValue: Boolean);
begin
  FPreprocess := AValue;
end;

function TEvProcessManualACHTaskProxy.GetACHOptions: IevACHOptions;
begin
  Result := FACHOptions;
end;

function TEvProcessManualACHTaskProxy.GetBatchBANK_REGISTER_TYPE: String;
begin
  Result := FBatchBANK_REGISTER_TYPE;
end;

procedure TEvProcessManualACHTaskProxy.SetBatchBANK_REGISTER_TYPE(
  const AValue: String);
begin
  FBatchBANK_REGISTER_TYPE := AValue;
end;

function TEvProcessManualACHTaskProxy.GetCTS: Boolean;
begin
  Result := FCTS;
end;

procedure TEvProcessManualACHTaskProxy.SetCTS(const AValue: Boolean);
begin
  FCTS := AValue;
end;

initialization
  ObjectFactory.Register(TEvProcessManualACHTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvProcessManualACHTaskProxy);

end.
