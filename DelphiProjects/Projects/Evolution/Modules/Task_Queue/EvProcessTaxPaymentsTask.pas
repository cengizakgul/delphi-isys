unit EvProcessTaxPaymentsTask;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTask, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, EvClasses;


implementation

type
  TevProcessTaxPaymentsTask = class(TEvCustomTask, IevProcessTaxPaymentsTask)
  private
    FNextThreadNbr: Integer;
    FParams: IevTaxPaymentsTaskParams;
    FForceChecks: Boolean;
    FSbTaxPaymentNbr: Integer;
    FPostProcessReportName: String;
    FEftpReference: String;
    FDefaultMiscCheckForm: String;
    FChecks: IevDualStream;
    FAchFile: IevDualStream;
    FAchReport: IevDualStream;
    FEftFile: IevDualStream;
    FNyDebitFile: IevDualStream;
    FMaDebitFile: IevDualStream;
    FCaDebitFile: IevDualStream;
    FUnivDebitFile: IevDualStream;
    FOneMiscCheck: IevDualStream;
    FOneMiscCheckReport: IevDualStream;
    FThreadResults: IevDualStream;
    FThreadResultCount: Integer;
  protected
    procedure  DoOnConstruction; override;
    function   Revision: TisRevision; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;

    function  GetParams: IevTaxPaymentsTaskParams;
    function  GetForceChecks: Boolean;
    procedure SetForceChecks(const AValue: Boolean);
    function  GetSbTaxPaymentNbr: Integer;
    procedure SetSbTaxPaymentNbr(const AValue: Integer);
    procedure SetPostProcessReportName(const AValue: String);
    function  GetPostProcessReportName: String;
    function  GetEftpReference: String;
    procedure SetEftpReference(const AValue: String);
    function  GetDefaultMiscCheckForm: String;
    procedure SetDefaultMiscCheckForm(const AValue: String);
    function  GetChecks: IevDualStream;
    function  GetAchFile: IevDualStream;
    function  GetAchReport: IevDualStream;
    function  GetEftFile: IevDualStream;
    function  GetNyDebitFile: IevDualStream;
    function  GetMaDebitFile: IevDualStream;
    function  GetCaDebitFile: IevDualStream;
    function  GetUnivDebitFile: IevDualStream;
    function  GetOneMiscCheck: IevDualStream;
    function  GetOneMiscCheckReport: IevDualStream;


    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_GoToNextClient(const AInputRequests: TevTaskRequestList);
    procedure  Phase_RunReports(const AInputRequests: TevTaskRequestList);
    function   Call_PrepareForFinalizing(const AParams: IisListOfValues): IisListOfValues;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;

{ TevProcessTaxPaymentsTask }

function TevProcessTaxPaymentsTask.Call_PrepareForFinalizing(const AParams: IisListOfValues): IisListOfValues;
begin
  // Dummy local request
  Result := TisListOfValues.Create;
  Result.AddValue(PARAM_RESULT, AParams.Value['TaxPaymentResult']);
end;

procedure TevProcessTaxPaymentsTask.DoOnConstruction;
begin
  inherited;
  FDaysStayInQueue := 10;
  FParams := TevTaxPaymentsTaskParams.Create;
end;

procedure TevProcessTaxPaymentsTask.DoRestoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  FNextThreadNbr := AStream.ReadInteger;
  FThreadResults := AStream.ReadStream(nil, True);

  if AStream.Position < AStream.Size then  // todo: remove "IF" in January 2009
    FThreadResultCount := AStream.ReadInteger;
end;

procedure TevProcessTaxPaymentsTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteInteger(FNextThreadNbr);
  AStream.WriteStream(FThreadResults);
  AStream.WriteInteger(FThreadResultCount);
end;

function TevProcessTaxPaymentsTask.GetAchFile: IevDualStream;
begin
  Result := FAchFile;
end;

function TevProcessTaxPaymentsTask.GetAchReport: IevDualStream;
begin
  Result := FAchReport;
end;

function TevProcessTaxPaymentsTask.GetCaDebitFile: IevDualStream;
begin
  Result := FCaDebitFile;
end;

function TevProcessTaxPaymentsTask.GetChecks: IevDualStream;
begin
  Result := FChecks;
end;

function TevProcessTaxPaymentsTask.GetEftFile: IevDualStream;
begin
  Result := FEftFile;
end;

function TevProcessTaxPaymentsTask.GetEftpReference: String;
begin
  Result := FEftpReference;
end;

function TevProcessTaxPaymentsTask.GetForceChecks: Boolean;
begin
  Result := FForceChecks;
end;

function TevProcessTaxPaymentsTask.GetSbTaxPaymentNbr: Integer;
begin
  Result := FSbTaxPaymentNbr;
end;

function TevProcessTaxPaymentsTask.GetDefaultMiscCheckForm: String;
begin
  Result := FDefaultMiscCheckForm;
end;

function TevProcessTaxPaymentsTask.GetMaDebitFile: IevDualStream;
begin
  Result := FMaDebitFile;
end;

function TevProcessTaxPaymentsTask.GetNyDebitFile: IevDualStream;
begin
  Result := FNyDebitFile;
end;

function TevProcessTaxPaymentsTask.GetParams: IevTaxPaymentsTaskParams;
begin
  Result := FParams;
end;

class function TevProcessTaxPaymentsTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_TAX_PAYMENTS;
end;

function TevProcessTaxPaymentsTask.GetUnivDebitFile: IevDualStream;
begin
  Result := FUnivDebitFile;
end;

function TevProcessTaxPaymentsTask.GetOneMiscCheck: IevDualStream;
begin
  Result := FOneMiscCheck;
end;

function TevProcessTaxPaymentsTask.GetOneMiscCheckReport: IevDualStream;
begin
  Result := FOneMiscCheckReport;
end;


procedure TevProcessTaxPaymentsTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
begin
  inherited;
  FNextThreadNbr := 0;
  FThreadResultCount := 0;

  FThreadResults := TevDualStreamHolder.Create;
  FThreadResults.WriteString(GetEftpReference);

  Phase_GoToNextClient(AInputRequests);
end;

procedure TevProcessTaxPaymentsTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  ResStr: IevDualStream;
begin
  StopOnExceptions(AInputRequests);
  
  if (Length(AInputRequests) > 0) and (AInputRequests[0].GetExceptions = '') then
  begin
    ResStr := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IevDualStream;
    ResStr.Position := 0;

    FChecks := TevDualStreamHolder.Create;
    ResStr.ReadStream(FChecks);

    FOneMiscCheck := TevDualStreamHolder.Create;
    ResStr.ReadStream(FOneMiscCheck);

    FOneMiscCheckReport := TevDualStreamHolder.Create;
    ResStr.ReadStream(FOneMiscCheckReport);

    FNyDebitFile := TevDualStreamHolder.Create;
    ResStr.ReadStream(FNyDebitFile);

    FMaDebitFile := TevDualStreamHolder.Create;
    ResStr.ReadStream(FMaDebitFile);

    FCaDebitFile := TevDualStreamHolder.Create;
    ResStr.ReadStream(FCaDebitFile);

    FUnivDebitFile := TevDualStreamHolder.Create;
    ResStr.ReadStream(FUnivDebitFile);

    FEftFile := TevDualStreamHolder.Create;
    ResStr.ReadStream(FEftFile);

    FAchFile := TevDualStreamHolder.Create;
    ResStr.ReadStream(FAchFile);

    FAchReport := TevDualStreamHolder.Create;
    ResStr.ReadStream(FAchReport);
  end;

  inherited;
end;

procedure TevProcessTaxPaymentsTask.Phase_GoToNextClient(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
  i, n: Integer;
  S: IevDualStream;
  ACHOptions: IisStringList;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
    if AnsiSameText(AInputRequests[i].TaskRoutineName, 'PayTaxes') then
    begin
      AddLogEvent('Phase "RunReports" activated');
      Phase_RunReports(AInputRequests) // processing is done, go to run report
    end
    else
    begin
      // collect "RunReports" processing results
      AddWarning(AInputRequests[i].GetExceptions);  // yes, it's a warning
      if AInputRequests[i].Results.ValueExists(PARAM_RESULT) then
      begin
        S := IInterface(AInputRequests[i].Results.Value[PARAM_RESULT]) as IevDualStream;
        if Assigned(S) then
        begin
          FThreadResults.Position := FThreadResults.Size;
          FThreadResults.WriteString(AInputRequests[i].Params.Value['CustomClientNumber']);
          FThreadResults.WriteStream(S);
        end;
      end;

      Inc(FThreadResultCount); //Result is done
    end;

  // continue to add as many requests as MaxThreads specifies
  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    if FNextThreadNbr >= FParams.Count then
      break;

    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', FParams[FNextThreadNbr].ClientID);
    Params.AddValue('CustomClientNumber', FParams[FNextThreadNbr].CustomClientNumber);
    Params.AddValue('FedTaxLiabilityList', FParams[FNextThreadNbr].FedTaxLiabilityList);
    Params.AddValue('StateTaxLiabilityList', FParams[FNextThreadNbr].StateTaxLiabilityList);
    Params.AddValue('SUITaxLiabilityList', FParams[FNextThreadNbr].SUITaxLiabilityList);
    Params.AddValue('LocalTaxLiabilityList', FParams[FNextThreadNbr].LocalTaxLiabilityList);
    Params.AddValue('ForceChecks', FForceChecks);
    Params.AddValue('EftpReference', FEftpReference);
    Params.AddValue('SbTaxPaymentNbr', FSbTaxPaymentNbr);
    Params.AddValue('PostProcessReportName', FPostProcessReportName);

    AddRequest('Client '+ FParams[FNextThreadNbr].CustomClientNumber, 'PayTaxes', Params, 'GoToNextClient.Fork',
      GF_CL_PAYROLL_OPERATION + '.' + IntToStr(FParams[FNextThreadNbr].ClientID));

    Inc(FNextThreadNbr);
  end;


  if (FThreadResultCount = FParams.Count) and AllRequestsAreFinished then
  begin
    // fililize tax payments
    Params := TisListOfValues.Create;
    Params.AddValue('Params', FThreadResults);
    if FileExists(AppDir + 'ACHOptions.ini') then
    begin
      ACHOptions := TisStringList.Create;
      ACHOptions.LoadFromFile(AppDir + 'ACHOptions.ini');
    end
    else
      ACHOptions := nil;
    Params.AddValue('ACHOptions.ini', ACHOptions);
    Params.AddValue('SbTaxPaymentNbr', FSbTaxPaymentNbr);
    Params.AddValue('PostProcessReportName', FPostProcessReportName);
    Params.AddValue('DefaultMiscCheckForm', FDefaultMiscCheckForm);

    AddRequest('Finalizing tax payment', 'FinalizeTaxPayments', Params);
  end;
end;

procedure TevProcessTaxPaymentsTask.Phase_RunReports(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  Params: IisListOfValues;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    if AInputRequests[i].GetExceptions <> '' then
      AddException('Client ' + AInputRequests[i].Params.Value['CustomClientNumber'] + ': ' +
        AInputRequests[i].GetExceptions);

    if AInputRequests[i].GetWarnings <> '' then
      AddWarning('Client ' + AInputRequests[i].Params.Value['CustomClientNumber'] + ': ' +
        AInputRequests[i].GetWarnings);

    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', AInputRequests[i].Params.Value['ClientID']);
    Params.AddValue('CustomClientNumber', AInputRequests[i].Params.Value['CustomClientNumber']);

    if AInputRequests[i].GetExceptions = '' then
    begin
      if AInputRequests[i].Results.Value['TaxPayrollFilter'] <> '' then
      begin
        Params.AddValue('TaxPaymentResult', AInputRequests[i].Results.Value[PARAM_RESULT]);
        Params.AddValue('TaxPayrollFilter', AInputRequests[i].Results.Value['TaxPayrollFilter']);
        Params.AddValue('DefaultMiscCheckForm', FDefaultMiscCheckForm);

        AddRequest('Printing client '+ AInputRequests[i].Params.Value['CustomClientNumber'],
          'PrintChecksAndCouponsForTaxes', Params, 'GoToNextClient.Fork');
      end
      else
      begin
        Params.AddValue('TaxPaymentResult', AInputRequests[i].Results.Value[PARAM_RESULT]);
        AddRequest('Preparing client '+ AInputRequests[i].Params.Value['CustomClientNumber'],
          'PrepareForFinalizing', Params, 'GoToNextClient.Fork');
      end;
    end
    else
      Inc(FThreadResultCount); // need to count this as a result!
  end;
end;

procedure TevProcessTaxPaymentsTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FParams as IisInterfacedObject).ReadFromStream(AStream);
  FForceChecks := AStream.ReadBoolean;
  FEftpReference := AStream.ReadString;
  FChecks := AStream.ReadStream(nil);
  FAchFile := AStream.ReadStream(nil);
  FAchReport := AStream.ReadStream(nil);
  FEftFile := AStream.ReadStream(nil);
  FNyDebitFile := AStream.ReadStream(nil);
  FMaDebitFile := AStream.ReadStream(nil);
  FCaDebitFile := AStream.ReadStream(nil);
  FUnivDebitFile := AStream.ReadStream(nil);
  FSbTaxPaymentNbr := AStream.ReadInteger;
  FOneMiscCheck := AStream.ReadStream(nil);
  FPostProcessReportName := AStream.ReadString;

  if ARevision >= 4 then
    FOneMiscCheckReport := AStream.ReadStream(nil)
  else
    FOneMiscCheckReport := nil;

  if ARevision >= 5 then
    FDefaultMiscCheckForm := AStream.ReadString
  else
    FDefaultMiscCheckForm := '';
end;

procedure TevProcessTaxPaymentsTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('GoToNextClient', Phase_GoToNextClient);
  RegisterTaskPhase('RunReports', Phase_RunReports);
  RegisterTaskCall('PrepareForFinalizing', Call_PrepareForFinalizing);
end;

procedure TevProcessTaxPaymentsTask.SetEftpReference(const AValue: String);
begin
  FEftpReference := AValue;
end;

procedure TevProcessTaxPaymentsTask.SetForceChecks(const AValue: Boolean);
begin
  FForceChecks := AValue;
end;

procedure TevProcessTaxPaymentsTask.SetSbTaxPaymentNbr(const AValue: Integer);
begin
  FSbTaxPaymentNbr := AValue;
end;

procedure TevProcessTaxPaymentsTask.SetDefaultMiscCheckForm(const AValue: String);
begin
  FDefaultMiscCheckForm := AValue;
end;


procedure TevProcessTaxPaymentsTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FParams as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteBoolean(FForceChecks);
  AStream.WriteString(FEftpReference);
  AStream.WriteStream(FChecks);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchReport);
  AStream.WriteStream(FEftFile);
  AStream.WriteStream(FNyDebitFile);
  AStream.WriteStream(FMaDebitFile);
  AStream.WriteStream(FCaDebitFile);
  AStream.WriteStream(FUnivDebitFile);
  AStream.WriteInteger(FSbTaxPaymentNbr);
  AStream.WriteStream(FOneMiscCheck);
  AStream.WriteString(FPostProcessReportName);
  AStream.WriteStream(FOneMiscCheckReport);
  AStream.WriteString(FDefaultMiscCheckForm);
end;


function TevProcessTaxPaymentsTask.GetPostProcessReportName: String;
begin
  Result := FPostProcessReportName;
end;

procedure TevProcessTaxPaymentsTask.SetPostProcessReportName(
  const AValue: String);
begin
  FPostProcessReportName := AValue;
end;

function TevProcessTaxPaymentsTask.Revision: TisRevision;
begin
  Result := 5;
end;

initialization
  ObjectFactory.Register(TevProcessTaxPaymentsTask);

finalization
  SafeObjectFactoryUnRegister(TevProcessTaxPaymentsTask);

end.
