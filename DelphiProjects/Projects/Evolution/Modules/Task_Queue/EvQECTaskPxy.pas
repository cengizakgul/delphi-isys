unit EvQECTaskPxy;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTaskPxy, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, evClasses;

implementation

type
  TevQECTaskProxy = class(TEvPrintableTaskProxy, IevQECTask)
  private
    FClNbr: Integer;
    FCoNbr: Integer;
    FQTREndDate: TDateTime;
    FMinTax: Double;
    FCleanupAction: Integer;
    FEECustomNbr: String;
    FForceCleanup: Boolean;
    FPrintReports: Boolean;
    FCurrentQuarterOnly: Boolean;
    FExtraOptions: String;
    FFilter: IevQECTaskFilter;
    FResults: IevQECTaskResult;    
  protected
    procedure  DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetClNbr: Integer;
    procedure SetClNbr(const AValue: Integer);
    function  GetCoNbr: Integer;
    procedure SetCoNbr(const AValue: Integer);
    function  GetQTREndDate: TDateTime;
    procedure SetQTREndDate(const AValue: TDateTime);
    function  GetMinTax: Double;
    procedure SetMinTax(const AValue: Double);
    function  GetCleanupAction: Integer;
    procedure SetCleanupAction(const AValue: Integer);
    function  GetEECustomNbr: String;
    procedure SetEECustomNbr(const AValue: String);
    function  GetForceCleanup: Boolean;
    procedure SetForceCleanup(const AValue: Boolean);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetCurrentQuarterOnly: Boolean;
    procedure SetCurrentQuarterOnly(const AValue: Boolean);
    function  GetExtraOptions: String;
    procedure SetExtraOptions(const AValue: String);
    function  GetFilter: IevQECTaskFilter;
  public
    class function GetTypeID: String; override;
  end;


{ TevQECTaskProxy }
procedure TevQECTaskProxy.DoOnConstruction;
begin
  inherited;
  FFilter := TevQECTaskFilter.Create;
end;

function TevQECTaskProxy.GetCleanupAction: Integer;
begin
  Result := FCleanupAction;
end;

function TevQECTaskProxy.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevQECTaskProxy.GetCoNbr: Integer;
begin
  Result := FCoNbr;
end;

function TevQECTaskProxy.GetCurrentQuarterOnly: Boolean;
begin
  Result := FCurrentQuarterOnly;
end;

function TevQECTaskProxy.GetEECustomNbr: String;
begin
  Result := FEECustomNbr;
end;

function TevQECTaskProxy.GetExtraOptions: String;
begin
  Result := FExtraOptions;
end;

function TevQECTaskProxy.GetForceCleanup: Boolean;
begin
  Result := FForceCleanup;
end;

function TevQECTaskProxy.GetMinTax: Double;
begin
  Result := FMinTax;
end;

function TevQECTaskProxy.GetPrintReports: Boolean;
begin
  Result := FPrintReports;
end;

function TevQECTaskProxy.GetQTREndDate: TDateTime;
begin
  Result := FQTREndDate;
end;

function TevQECTaskProxy.GetFilter: IevQECTaskFilter;
begin
  Result := FFilter;
end;

class function TevQECTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_QEC;
end;

procedure TevQECTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoNbr := AStream.ReadInteger;
  FQTREndDate := AStream.ReadDouble;
  FMinTax := AStream.ReadDouble;
  FCleanupAction := AStream.ReadInteger;
  FEECustomNbr := AStream.ReadString;
  FForceCleanup := AStream.ReadBoolean;
  FPrintReports := AStream.ReadBoolean;
  FCurrentQuarterOnly := AStream.ReadBoolean;
  FExtraOptions := AStream.ReadString;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevQECTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TevQECTaskProxy.SetCleanupAction(const AValue: Integer);
begin
  FCleanupAction := AValue;
end;

procedure TevQECTaskProxy.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevQECTaskProxy.SetCoNbr(const AValue: Integer);
begin
  FCoNbr := AValue;
end;

procedure TevQECTaskProxy.SetCurrentQuarterOnly(const AValue: Boolean);
begin
  FCurrentQuarterOnly := AValue;
end;

procedure TevQECTaskProxy.SetEECustomNbr(const AValue: String);
begin
  FEECustomNbr := AValue;
end;

procedure TevQECTaskProxy.SetExtraOptions(const AValue: String);
begin
  FExtraOptions := AValue;
end;

procedure TevQECTaskProxy.SetForceCleanup(const AValue: Boolean);
begin
  FForceCleanup := AValue;
end;

procedure TevQECTaskProxy.SetMinTax(const AValue: Double);
begin
  FMinTax := AValue;
end;

procedure TevQECTaskProxy.SetPrintReports(const AValue: Boolean);
begin
  FPrintReports := AValue;
end;

procedure TevQECTaskProxy.SetQTREndDate(const AValue: TDateTime);
begin
  FQTREndDate := AValue;
end;

procedure TevQECTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteInteger(FCoNbr);
  AStream.WriteDouble(FQTREndDate);
  AStream.WriteDouble(FMinTax);
  AStream.WriteInteger(FCleanupAction);
  AStream.WriteString(FEECustomNbr);
  AStream.WriteBoolean(FForceCleanup);
  AStream.WriteBoolean(FPrintReports);
  AStream.WriteBoolean(FCurrentQuarterOnly);
  AStream.WriteString(FExtraOptions);

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

initialization
  ObjectFactory.Register(TevQECTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TevQECTaskProxy);

end.
