unit EvBenefitEnrollmentNotificationsTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

uses Classes, Math;

type
  TEvBenefitEnrollmentNotificationsTaskPxy = class(TEvCustomTask, IevBenefitEnrollmentNotificationsTask)
  private
    FClientNumbers: String;
    FForAllClients: boolean;

    FEnableLogging: boolean;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function   GetForAllClients: Boolean;
    function   GetClientNumbers: String;
    procedure  SetClientNumbers(const AValue: String);
    function   GetEnableLogging: Boolean;
    procedure  SetEnableLogging(const AValue: Boolean);

  public
    class function GetTypeID: String; override;
  end;

{ TEvBenefitEnrollmentNotificationsTaskPxy }

procedure TEvBenefitEnrollmentNotificationsTaskPxy.DoOnConstruction;
begin
  inherited;
  FEnableLogging := True;
  FForAllClients := Trim(FClientNumbers) = '';
end;

function TEvBenefitEnrollmentNotificationsTaskPxy.GetClientNumbers: String;
begin
  Result := FClientNumbers;
end;

function TEvBenefitEnrollmentNotificationsTaskPxy.GetEnableLogging: Boolean;
begin
  Result := FEnableLogging;
end;

function TEvBenefitEnrollmentNotificationsTaskPxy.GetForAllClients: Boolean;
begin
  Result := FForAllClients;
end;

class function TEvBenefitEnrollmentNotificationsTaskPxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS;
end;

procedure TEvBenefitEnrollmentNotificationsTaskPxy.ReadSelfFromStream(
  const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClientNumbers := AStream.ReadString;
  FEnableLogging := AStream.ReadBoolean;
  FForAllClients := Trim(FClientNumbers) = '';
end;

procedure TEvBenefitEnrollmentNotificationsTaskPxy.SetClientNumbers(const AValue: String);
begin
  FClientNumbers := AValue;
  FForAllClients := Trim(FClientNumbers) = '';
end;

procedure TEvBenefitEnrollmentNotificationsTaskPxy.SetEnableLogging(const AValue: Boolean);
begin
  FEnableLogging := AValue;
end;

procedure TEvBenefitEnrollmentNotificationsTaskPxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FClientNumbers);
  AStream.WriteBoolean(FEnableLogging);
end;

initialization
  ObjectFactory.Register(TEvBenefitEnrollmentNotificationsTaskPxy);

finalization
  SafeObjectFactoryUnRegister(TEvBenefitEnrollmentNotificationsTaskPxy);

end.

