unit EvPreprocessPayrollTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard;

implementation

type
  TEvPreprocessPayrollTaskProxy = class(TEvCustomTaskProxy, IevPreprocessPayrollTask)
  private
    FPrList: string;
    FPRMessage: string;
    FPrInfo: IisListOfValues;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetPrList: string;
    function  GetPRMessage: string;
    procedure SetPrList(const AValue: string);
    procedure SetPRMessage(const AValue: string);
  public
    class function GetTypeID: String; override;
  end;


{ TEvPreprocessPayrollTaskProxy }

procedure TEvPreprocessPayrollTaskProxy.DoOnConstruction;
begin
  inherited;
  FPrInfo := TisListOfValues.Create;
end;

function TEvPreprocessPayrollTaskProxy.GetPrList: string;
begin
  Result := FPrList; 
end;

function TEvPreprocessPayrollTaskProxy.GetPRMessage: string;
begin
  Result := FPRMessage;
end;

class function TEvPreprocessPayrollTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PREPROCESS_PAYROLL;
end;

procedure TEvPreprocessPayrollTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FPrList :=AStream.ReadString;
  FPRMessage := AStream.ReadString;
  FPrInfo.ReadFromStream(AStream);  
end;

procedure TEvPreprocessPayrollTaskProxy.SetPrList(const AValue: string);
begin
  FPrList := AValue;
end;

procedure TEvPreprocessPayrollTaskProxy.SetPRMessage(const AValue: string);
begin
  FPRMessage := AValue;
end;

procedure TEvPreprocessPayrollTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FPrList);
  AStream.WriteString(FPRMessage);
  FPrInfo.WriteToStream(AStream);  
end;

initialization
  ObjectFactory.Register(TEvPreprocessPayrollTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvPreprocessPayrollTaskProxy);



end.
