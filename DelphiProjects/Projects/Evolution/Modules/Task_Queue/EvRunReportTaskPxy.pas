unit EvRunReportTaskPxy;

interface

uses Variants, isBaseClasses, EvCustomTaskPxy, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvConsts, EvMainboard;

implementation

type
  TEvRunReportTaskProxy = class(TEvPrintableTaskProxy, IevRunReportTask)
  private
    FReports: IevDualStream;
  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function   GetReports: IevDualStream;
    procedure  SetReports(const AValue: IevDualStream);
  public
    class function GetTypeID: String; override;
  end;


{ TEvRunReportTaskProxy }

function TEvRunReportTaskProxy.GetReports: IevDualStream;
begin
  Result := FReports;
end;

class function TEvRunReportTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_RUN_REPORT;
end;

procedure TEvRunReportTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FReports := AStream.ReadStream(nil, True);
end;

procedure TEvRunReportTaskProxy.SetReports(const AValue: IevDualStream);
begin
  FReports := AValue;
end;

procedure TEvRunReportTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteStream(FReports);
end;


initialization
  ObjectFactory.Register(TEvRunReportTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvRunReportTaskProxy);

end.
