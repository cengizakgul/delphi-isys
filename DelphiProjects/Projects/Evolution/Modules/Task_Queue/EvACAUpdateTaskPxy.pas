unit EvACAUpdateTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses;

implementation

type
  TEvACAUpdateTaskProxy = class(TEvCustomTaskProxy, IevACAUpdateTask)
  private
    FFilter: IevACATaskCompanyFilter;
    FResults: IevACATaskResult;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetFilter: IevACATaskCompanyFilter;
    function  GetResults: IevACATaskResult;

  public
    class function GetTypeID: String; override;
  end;


{ TEvPreprocessPayrollTaskProxy }

procedure TEvACAUpdateTaskProxy.DoOnConstruction;
begin
  inherited;
  FFilter := TevACATaskCompanyFilter.Create;
end;

class function TEvACAUpdateTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_ACA_STATUS_UPDATE;
end;

function TEvACAUpdateTaskProxy.GetFilter: IevACATaskCompanyFilter;
begin
  Result := FFilter;
end;

function TEvACAUpdateTaskProxy.GetResults: IevACATaskResult;
begin
  Result := FResults;
end;

procedure TEvACAUpdateTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevACATaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;


procedure TEvACAUpdateTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;

  AStream.WriteStream(S);
end;

initialization
  ObjectFactory.Register(TEvACAUpdateTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvACAUpdateTaskProxy);
end.
