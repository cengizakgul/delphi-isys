unit EvProcessManualACHTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

type
  TEvProcessManualACHTask = class(TEvCustomTask, IEvProcessManualACHTask)
  private
    FACHOptions: IevACHOptions;
    FPreprocess: Boolean;
    FBatchBANK_REGISTER_TYPE: String;
    FACHDataStream: IEvDualStream;
    FAchFile: IEvDualStream;
    FAchRegularReport: IEvDualStream;
    FAchDetailedReport: IEvDualStream;
    FAchSave: IEvDualStream;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function   BeforeTaskStart: Boolean; override;
    procedure  AfterTaskFinished; override;

    function  GetACHOptions: IevACHOptions;
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetPreprocess: Boolean;
    procedure SetPreprocess(const AValue: Boolean);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;
    function  GetBatchBANK_REGISTER_TYPE: String;
    procedure SetBatchBANK_REGISTER_TYPE(const AValue: String);

    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;

{ TEvProcessManualACHTask }

procedure TEvProcessManualACHTask.DoOnConstruction;
begin
  inherited;
  FACHOptions := TevACHOptions.Create;
  FACHDataStream := TEvDualStreamHolder.Create;
  FDaysStayInQueue := 10;
end;

function TEvProcessManualACHTask.GetACHDataStream: IEvDualStream;
begin
  Result := FACHDataStream;
end;

function TEvProcessManualACHTask.GetAchDetailedReport: IEvDualStream;
begin
  Result := FAchDetailedReport;
end;

function TEvProcessManualACHTask.GetAchFile: IEvDualStream;
begin
  Result := FAchFile;
end;

function TEvProcessManualACHTask.GetAchRegularReport: IEvDualStream;
begin
  Result := FAchRegularReport;
end;

function TEvProcessManualACHTask.GetAchSave: IEvDualStream;
begin
  Result := FAchSave;
end;

class function TEvProcessManualACHTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_MANUAL_ACH;
end;

function TEvProcessManualACHTask.BeforeTaskStart: Boolean;
begin
  Result := inherited BeforeTaskStart and mb_GlobalFlagsManager.TryLock(GF_SB_ACH_PROCESS);
end;

procedure TEvProcessManualACHTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin                                        
  inherited;                        

  Params := TisListOfValues.Create;
  Params.AddValue('ACHOptions', FACHOptions);
  Params.AddValue('ACHFolder', Context.DomainInfo.FileFolders.ACHFolder);
  Params.AddValue('bPreprocess', FPreprocess);
  Params.AddValue('BatchBANK_REGISTER_TYPE', FBatchBANK_REGISTER_TYPE);
  Params.AddValue('ACHDataStream', FACHDataStream);

  AddRequest(GetCaption, 'ProcessManualACH',  Params);
end;

procedure TEvProcessManualACHTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  ACHFolder: String;
  ACHFiles: IisListOfValues;
  i: Integer;
begin
  if Length(AInputRequests) > 0 then
  begin
    AddException(AInputRequests[0].GetExceptions);
    AddWarning(AInputRequests[0].GetWarnings);

//    if AInputRequests[0].GetExceptions = '' then
    begin
      FAchFile := IInterface(AInputRequests[0].Results.Value['AchFile']) as IevDualStream;
      FAchRegularReport := IInterface(AInputRequests[0].Results.Value['AchRegularReport']) as IevDualStream;
      FAchDetailedReport := IInterface(AInputRequests[0].Results.Value['AchDetailedReport']) as IevDualStream;
      ACHFiles := IInterface(AInputRequests[0].Results.Value['AchSave']) as IisListOfValues;

      if Assigned(ACHFiles) and (ACHFiles.Count > 0) then
      begin
        FAchSave := TevDualStreamHolder.CreateInMemory;
        (IInterface(ACHFiles[0].Value) as IisStringList).SaveToStream(FAchSave.RealStream);
      end;

      ACHFolder := Context.DomainInfo.FileFolders.ACHFolder;
      if Length(ACHFolder) > 0 then
      begin
        if Assigned(FAchSave) and ForceDirectories(ACHFolder) then
          if Assigned(ACHFiles) then
            for i := 0 to ACHFiles.Count - 1 do
              (IInterface(ACHFiles[i].Value) as IisStringList).SaveToFile(ACHFolder + '\' + ExtractFileName(ACHFiles[i].Name));
      end;

      if Assigned(ACHFiles) and (ACHFiles.Count > 0) then
        if ACHFiles.Count > 1 then
        begin
          FAchSave := nil;
          if Length(ACHFolder) > 0 then
            AddException(Format('%d ACH files were saved into default ACH directory %s',[ACHFiles.Count, ACHFolder]))
          else
            AddException(Format('%d ACH files were created but not saved',[ACHFiles.Count, ACHFolder]));
        end;

    end;
  end;

  inherited;
end;

procedure TEvProcessManualACHTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).ReadFromStream(AStream);
  FACHDataStream := AStream.ReadStream(nil);
  FBatchBANK_REGISTER_TYPE := AStream.ReadString;
  FPreprocess := AStream.ReadBoolean;
  FAchFile := AStream.ReadStream(nil, True);
  FAchRegularReport := AStream.ReadStream(nil, True);
  FAchDetailedReport := AStream.ReadStream(nil, True);
  FAchSave := AStream.ReadStream(nil, True);
end;

procedure TEvProcessManualACHTask.SetACHDataStream(const AValue: IEvDualStream);
begin
  FACHDataStream := AValue;
end;

procedure TEvProcessManualACHTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteStream(FACHDataStream);
  AStream.WriteString(FBatchBANK_REGISTER_TYPE);
  AStream.WriteBoolean(FPreprocess);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchRegularReport);
  AStream.WriteStream(FAchDetailedReport);
  AStream.WriteStream(FAchSave);
end;

procedure TEvProcessManualACHTask.AfterTaskFinished;
begin
  inherited;
  Assert(mb_GlobalFlagsManager.TryUnlock(GF_SB_ACH_PROCESS));
end;

function TEvProcessManualACHTask.GetPreprocess: Boolean;
begin
  Result := FPreprocess;
end;

procedure TEvProcessManualACHTask.SetPreprocess(
  const AValue: Boolean);
begin
  FPreprocess := AValue;
end;

function TEvProcessManualACHTask.GetACHOptions: IevACHOptions;
begin
  Result := FACHOptions;
end;

function TEvProcessManualACHTask.GetBatchBANK_REGISTER_TYPE: String;
begin
  Result := FBatchBANK_REGISTER_TYPE;
end;

procedure TEvProcessManualACHTask.SetBatchBANK_REGISTER_TYPE(
  const AValue: String);
begin
  FBatchBANK_REGISTER_TYPE := AValue;
end;

initialization
  ObjectFactory.Register(TEvProcessManualACHTask);

finalization
  SafeObjectFactoryUnRegister(TEvProcessManualACHTask);

end.
