unit EvPALiabilitiesTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses;

implementation

type
  TevPALiabilitiesTaskProxy = class(TEvCustomTaskProxy, IevPALiabilitiesTask)
  private
    FCompanies: IisParamsCollection;
  protected
    function GetCompanies: IisParamsCollection;
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
  public
    class function GetTypeID: String; override;
  end;


{ TevPALiabilitiesTaskProxy }

procedure TevPALiabilitiesTaskProxy.DoOnConstruction;
begin
  inherited;
  FCompanies := TisParamsCollection.Create;
end;

function TevPALiabilitiesTaskProxy.GetCompanies: IisParamsCollection;
begin
  Result := FCompanies;
end;

class function TevPALiabilitiesTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PA_LIABILITY_UTILITY;
end;

procedure TevPALiabilitiesTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FCompanies as IisInterfacedObject).ReadFromStream(AStream);
end;

procedure TevPALiabilitiesTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FCompanies as IisInterfacedObject).WriteToStream(AStream);
end;

initialization
  ObjectFactory.Register(TevPALiabilitiesTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TevPALiabilitiesTaskProxy);

end.
