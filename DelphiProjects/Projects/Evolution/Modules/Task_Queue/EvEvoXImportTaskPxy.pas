unit EvEvoXImportTaskPxy;

interface

uses Classes, Windows, SysUtils, isBaseClasses, isBasicUtils, EvCommonInterfaces, EvContext, EvMainboard, EvStreamUtils,
     EvConsts, EvClasses, EvLegacy, EvDataSet, EvTypes, IsErrorUtils, EvAsyncCall, isVCLBugFix,
     EvBasicUtils, EvUtils, EvCustomTaskPxy;

implementation

uses
   EvExchangeMapFile, EvExchangePackage;

type
  TEvEvoXImportTaskPxy = class(TEvCustomTaskProxy, IevEvoXImportTask)
  private
    FInputDatasets: IisListOfValues;
    FMapFile: IEvExchangeMapFile;
    FPackage: IevExchangePackage;
    FResults: IisListOfValues;
    FEffectiveDate: TDateTime;
    FOptions: IisStringList;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetInputDatasets: IisListOfValues;
    procedure SetInputDataSets(const AInputDataSets: IisListOfValues);
    function  GetMapFile: IEvExchangeMapFile;
    procedure SetMapFile(const AMapFile: IEvExchangeMapFile);
    function  GetPackage: IevExchangePackage;
    procedure SetPackage(const APackage: IevExchangePackage);
    function  GetEffectiveDate: TDateTime;
    procedure SetEffectiveDate(const AEffectiveDate: TDateTime);
    function  GetOptions: IisStringList;
    procedure SetOptions(const AOptions: IisStringList);
    function  GetResults: IisListofValues;
  public
    class function GetTypeID: String; override;
  end;

{ TEvEvoXImportTaskPxy }

procedure TEvEvoXImportTaskPxy.DoOnConstruction;
begin
  inherited;
  FEffectiveDate := 0;
  FInputDatasets := TisListOfValues.Create;
  FResults := TisListOfValues.Create;
  FMapFile := TEvExchangeMapFile.Create;
  FPackage := TevExchangePackage.Create;
  FOptions := TisStringList.Create;
end;

function TEvEvoXImportTaskPxy.GetEffectiveDate: TDateTime;
begin
  Result := FEffectiveDate;
end;

function TEvEvoXImportTaskPxy.GetInputDatasets: IisListOfValues;
begin
  Result := FInputDatasets;
end;

function TEvEvoXImportTaskPxy.GetMapFile: IEvExchangeMapFile;
begin
  Result := FMapFile;
end;

function TEvEvoXImportTaskPxy.GetOptions: IisStringList;
begin
  Result := FOptions;
end;

function TEvEvoXImportTaskPxy.GetPackage: IevExchangePackage;
begin
  Result := FPackage;
end;

function TEvEvoXImportTaskPxy.GetResults: IisListofValues;
begin
  Result := FResults;
end;

class function TEvEvoXImportTaskPxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_EVOX_IMPORT;
end;

procedure TEvEvoXImportTaskPxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FInputDatasets.ReadFromStream(AStream);
  FMapFile.ReadFromStream(AStream);
  FPackage.ReadFromStream(AStream);
  FEffectiveDate := AStream.ReadDouble;
  FResults.ReadFromStream(AStream);
  FOptions.Text := AStream.ReadString;
end;

procedure TEvEvoXImportTaskPxy.SetEffectiveDate(const AEffectiveDate: TDateTime);
begin
  FEffectiveDate := AEffectiveDate;
end;

procedure TEvEvoXImportTaskPxy.SetInputDataSets(
  const AInputDataSets: IisListOfValues);
begin
  CheckCondition(Assigned(AInputDataSets), 'Input Datasets object should be assigned!');
  FInputDatasets := AInputDataSets;
end;

procedure TEvEvoXImportTaskPxy.SetMapFile(const AMapFile: IEvExchangeMapFile);
begin
  CheckCondition(Assigned(AMapFile), 'Map file should be assigned!');
  FMapFile := AMapFile;
end;

procedure TEvEvoXImportTaskPxy.SetOptions(const AOptions: IisStringList);
begin
  CheckCondition(Assigned(AOptions), 'Options should be assigned');
  FOptions := AOptions;
end;

procedure TEvEvoXImportTaskPxy.SetPackage(const APackage: IevExchangePackage);
begin
  CheckCondition(Assigned(APackage), 'Package should be assigned');
  FPackage := APackage;
end;

procedure TEvEvoXImportTaskPxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  FInputDatasets.WriteToStream(AStream);
  FMapFile.WriteToStream(AStream);
  FPackage.WriteToStream(AStream);
  AStream.WriteDouble(FEffectiveDate);
  FResults.WriteToStream(AStream);
  AStream.WriteString(FOptions.Text);
end;

initialization
  ObjectFactory.Register(TEvEvoXImportTaskPxy);

finalization
  SafeObjectFactoryUnRegister(TEvEvoXImportTaskPxy);
end.

