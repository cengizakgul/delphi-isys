unit EvProcessTaxReturnsTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;


implementation

{%File 'EvProcessTaxReturnsTask.jpg'}

type
  TevProcessTaxReturnsTask = class(TevCustomTask, IevProcessTaxReturnsTask)
  private
    FBegDate: TDateTime;
    FEndDate: TDateTime;
    FFilter: IisStringList;
    FTaxFilter: TProcessTaxReturnsFilterSelections;
    FEeSortMode: TProcessTaxReturnsEeSort;
    FAskForRefund: Boolean;
    FSecureReport: Boolean;
    FResults: IisListOfValues;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_ProcessTaxReturns(const AInputRequests: TevTaskRequestList);
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;

    function  GetBegDate: TDateTime;
    function  GetEndDate: TDateTime;
    procedure SetEndDate(const AValue: TDateTime);
    function  GetAskForRefund: Boolean;
    function  GetEeSortMode: TProcessTaxReturnsEeSort;
    procedure SetAskForRefund(const AValue: Boolean);
    procedure SetEeSortMode(const AValue: TProcessTaxReturnsEeSort);
    function  GetFilter: IisStringList;
    function  GetTaxFilter: TProcessTaxReturnsFilterSelections;
    procedure SetTaxFilter(const AValue: TProcessTaxReturnsFilterSelections);
    function  GetResults: IisListOfValues;
    function  GetSecureReport: Boolean;
    procedure SetSecureReport(const AValue: Boolean);
  public
    class function GetTypeID: String; override;
  end;


{ TevProcessTaxReturnsTask }

procedure TevProcessTaxReturnsTask.DoOnConstruction;
begin
  inherited;
  FFilter := TisStringList.Create;
  FTaxFilter := fsAll;
  FEndDate := GetEndQuarter(GetBeginMonth(Date)- 1);
  FBegDate := GetBeginQuarter(FEndDate);
  FEeSortMode:=  eesLastName;
end;

function TevProcessTaxReturnsTask.GetFilter: IisStringList;
begin
  Result := FFilter;
end;

function TevProcessTaxReturnsTask.GetBegDate: TDateTime;
begin
  Result := FBegDate;
end;

function TevProcessTaxReturnsTask.GetEndDate: TDateTime;
begin
  Result := FEndDate;
end;

class function TevProcessTaxReturnsTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_TAX_RETURNS;
end;

procedure TevProcessTaxReturnsTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('BegDate', FBegDate);
  Params.AddValue('EndDate', FEndDate);
  Params.AddValue('Filter',  FFilter);
  Params.AddValue('TaxFilter', Ord(FTaxFilter));
  Params.AddValue('EeSortMode', Ord(FEeSortMode));
  Params.AddValue('AskForRefund', FAskForRefund);
  Params.AddValue('SecureReport', FSecureReport);

  AddRequest('Prepare tax returns list', 'PrepareForProcessTaxReturns',  Params, 'ProcessTaxReturns.Fork');
end;

procedure TevProcessTaxReturnsTask.Phase_ProcessTaxReturns(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  Results, RepInfo, Params: IisListOfValues;
begin
  StopOnExceptions(AInputRequests);

  Results := AInputRequests[0].Results;

  for i := 0 to Results.Count - 1 do
  begin
    RepInfo := IInterface(Results[i].Value) as IisListOfValues;

    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', RepInfo.Value['ClientID'] );
    Params.AddValue('ReportList', RepInfo.Value['ReportList']);

    AddRequest(RepInfo.Value['Caption'], 'ProcessTaxReturns',  Params);
  end;
end;

procedure TevProcessTaxReturnsTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IevDualStream;
begin
  inherited;
  FEndDate := AStream.ReadDouble;
  FEeSortMode := TProcessTaxReturnsEeSort(AStream.ReadByte);
  FAskForRefund := AStream.ReadBoolean;
  FTaxFilter := TProcessTaxReturnsFilterSelections(AStream.ReadByte);
  FSecureReport := AStream.ReadBoolean;  

  S := AStream.ReadStream(S);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TisListOfValues.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end;
end;

procedure TevProcessTaxReturnsTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('ProcessTaxReturns', Phase_ProcessTaxReturns);
end;

procedure TevProcessTaxReturnsTask.SetEndDate(const AValue: TDateTime);
begin
  FEndDate := AValue;
  FBegDate := GetBeginYear(FEndDate);
end;

procedure TevProcessTaxReturnsTask.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IevDualStream;
begin
  inherited;
  AStream.WriteDouble(FEndDate);
  AStream.WriteByte(Ord(FEeSortMode));
  AStream.WriteBoolean(FAskForRefund);
  AStream.WriteByte(Ord(FTaxFilter));
  AStream.WriteBoolean(FSecureReport);

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;

  AStream.WriteStream(S);
end;

function TevProcessTaxReturnsTask.GetAskForRefund: Boolean;
begin
  Result := FAskForRefund;
end;

function TevProcessTaxReturnsTask.GetEeSortMode: TProcessTaxReturnsEeSort;
begin
  Result := FEeSortMode;
end;

procedure TevProcessTaxReturnsTask.SetAskForRefund(const AValue: Boolean);
begin
  FAskForRefund := AValue;
end;

procedure TevProcessTaxReturnsTask.SetEeSortMode(const AValue: TProcessTaxReturnsEeSort);
begin
  FEeSortMode := AValue;
end;

function TevProcessTaxReturnsTask.GetTaxFilter: TProcessTaxReturnsFilterSelections;
begin
  Result := FTaxFilter;
end;

procedure TevProcessTaxReturnsTask.SetTaxFilter(const AValue: TProcessTaxReturnsFilterSelections);
begin
  FTaxFilter := AValue;
end;

function TevProcessTaxReturnsTask.GetResults: IisListOfValues;
begin
  Result := FResults;
end;

procedure TevProcessTaxReturnsTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  i, j: Integer;
  RepRes, ResItem: IisListOfValues;
begin
  FResults := TisListOfValues.Create;

  for i := 0 to High(AInputRequests) do
  begin
    RepRes := AInputRequests[i].Results;
    ResItem := TisListOfValues.Create;

    if AInputRequests[i].GetExceptions <> '' then
     AddException(AInputRequests[i].RequestName + ':  ' + AInputRequests[i].GetExceptions);
    if AInputRequests[i].GetWarnings <> '' then
     AddWarning(AInputRequests[i].RequestName + ':  ' + AInputRequests[i].GetWarnings);

    for j := 0 to RepRes.Count - 1 do
      if StartsWith(RepRes.Values[j].Name, 'Report') then
      begin
        ResItem.AddValue('CompanyDesc', AInputRequests[i].RequestName);
        ResItem.AddValue('ReportDesc', (IInterface(RepRes[j].Value) as IisListOfValues).Value['ReportDesc']);
        ResItem.AddValue('Message', (IInterface(RepRes[j].Value) as IisListOfValues).Value['Message']);
      end;

    FResults.AddValue(IntToStr(i), ResItem);
  end;

  inherited;
end;

function TevProcessTaxReturnsTask.GetSecureReport: Boolean;
begin
  Result := FSecureReport;
end;

procedure TevProcessTaxReturnsTask.SetSecureReport(const AValue: Boolean);
begin
  FSecureReport := AValue;
end;

initialization
  ObjectFactory.Register(TevProcessTaxReturnsTask);

finalization
  SafeObjectFactoryUnRegister(TevProcessTaxReturnsTask);

end.
