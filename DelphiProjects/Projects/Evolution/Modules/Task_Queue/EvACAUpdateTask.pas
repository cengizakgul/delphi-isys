unit EvACAUpdateTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;

implementation

type
  TEvACAUpdateTask = class(TEvCustomTask, IevACAUpdateTask)
  private
    FFilter: IevACATaskCompanyFilter;
    FResults: IevACATaskResult;
    FNextFilterItem: Integer;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;
    procedure  DoOnActivate; override;

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_ProcessACAUpdate(const AInputRequests: TevTaskRequestList);
    procedure  Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);

    function  GetFilter: IevACATaskCompanyFilter;
    function  GetResults: IevACATaskResult;

  public
    class function GetTypeID: String; override;
  end;


{ TEvACAUpdateTask }

procedure TEvACAUpdateTask.DoOnActivate;
begin
  inherited;

end;

procedure TEvACAUpdateTask.DoOnConstruction;
begin
  inherited;
  FFilter := TevACATaskCompanyFilter.Create;
end;

procedure TEvACAUpdateTask.DoRestoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  FNextFilterItem := AStream.ReadInteger;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevACATaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TEvACAUpdateTask.DoStoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  AStream.WriteInteger(FNextFilterItem);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

function TEvACAUpdateTask.GetFilter: IevACATaskCompanyFilter;
begin
  Result := FFilter;
end;

function TEvACAUpdateTask.GetResults: IevACATaskResult;
begin
  Result := FResults;
end;

class function TEvACAUpdateTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_ACA_STATUS_UPDATE;
end;

procedure TEvACAUpdateTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;
  if FFilter.Count = 0 then
  begin
    Params := TisListOfValues.Create;
    AddRequest('Prepare company list', 'PrepareTaskCompanyFilter',  Params, 'ProcessACAUpdate.Merge');
  end
  else
   Phase_ProcessACAUpdate(AInputRequests);
end;

procedure TEvACAUpdateTask.Phase_ProcessACAUpdate(const AInputRequests: TevTaskRequestList);
begin
  StopOnExceptions(AInputRequests);
  if Length(AInputRequests) > 0 then
    (FFilter as IisInterfacedObject).AsStream := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IevDualStream;

  FResults := TevACATaskResult.Create;

  Phase_GoToNextCompany(AInputRequests);
end;

procedure TEvACAUpdateTask.Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);
var
  i, n: Integer;
  Flt: IevACATaskCompanyFilterItem;
  Params: IisListOfValues;
  Res: IevACATaskCompanyEE;
begin
  // collect results
  if FNextFilterItem > 0 then
    for i := Low(AInputRequests) to High(AInputRequests) do
    begin
      if AInputRequests[i].GetExceptions = '' then
      begin
        Res := FResults.Add;
        if AInputRequests[i].Results.FindValue('ACATaskCompanyEE') <> nil then
          (Res as IisInterfacedObject).AsStream := IInterface(AInputRequests[i].Results.Value['ACATaskCompanyEE']) as IevDualStream;
      end
      else
       AddException(AInputRequests[i].GetExceptions);
      if AInputRequests[i].GetWarnings <> '' then
        AddWarning(AInputRequests[i].GetWarnings);
    end;

  // continue to add as many requests as GetMaxThreads specifies
  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    if FNextFilterItem >= FFilter.Count then
      break;

    Flt := FFilter[FNextFilterItem];

    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', Flt.ClNbr);
    Params.AddValue('CoFilter', Flt.CoFilter);
    AddRequest('Client# '  + IntToStr(Flt.ClNbr) + ' Company# '+ Flt.CoFilter, 'ProcessACAUpdate', Params, 'GoToNextCompany.Fork');

    Inc(FNextFilterItem);
  end;
end;


procedure TEvACAUpdateTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('ProcessACAUpdate', Phase_ProcessACAUpdate);
  RegisterTaskPhase('GoToNextCompany', Phase_GoToNextCompany);
end;

procedure TEvACAUpdateTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevACATaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TEvACAUpdateTask.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;

  AStream.WriteStream(S);

end;

initialization
  ObjectFactory.Register(TEvACAUpdateTask);

finalization
  SafeObjectFactoryUnRegister(TEvACAUpdateTask);
end.
