unit EvTaskQueueMod;

interface

uses Windows, SysUtils, SyncObjs, Classes, Math, Types, DateUtils, isBaseClasses,
     isBasicUtils, EvCommonInterfaces, EvContext, evMainboard, isThreadManager,
     EvStreamUtils, EvClasses, EvTypes, EvConsts, EvBasicUtils, isLogFile,
     evExceptions;


implementation

const
  sTaskFileExt = '.tsk';


type
  TevTaskQueue = class;

  TevTaskPulser = class(TReactiveThread)
  private
    function TaskQueueObj: TevTaskQueue;
  protected
    procedure DoOnStart; override;
    procedure DoWork; override;
    procedure DoOnStop; override;
  end;


  IevTaskInfoInQueue = interface(IevTaskInfo)
  ['{693DD452-19A5-4B07-9882-528B2FD68CE8}']
    function   GetTask: IevTask;
    procedure  ActivateTask;
    procedure  DeactivateTask;
    function   BelongsToContext: Boolean;
    procedure  ModifyTaskProperty(const APropName: String; const ANewValue: Variant);
    function   ExpiredTask: Boolean;
  end;


  TevTaskInfoInQueue = class(TevTaskInfo, IevTaskInfo, IevTaskInfoInQueue)
  private
    FTaskFile: String;
    FTask:     IevTask;
    function   OpenTask: IevTask;
    function   TaskQueueObj: TevTaskQueue;
  protected
    function   GetTask: IevTask;
    procedure  ActivateTask;
    procedure  DeactivateTask;

    function  GetLastUpdate: TDateTime; override;
    function  GetPriority: Integer; override;
    function  GetNotificationEmail: string; override;
    function  GetSendEmailNotification: Boolean; override;
    function  GetState: TevTaskState; override;
    function  GetCaption: String; override;
    function  GetProgressText: string; override;
    function  GetSeenByUser: Boolean; override;
    function  GetFirstRunAt: TDateTime; override;
    procedure ModifyTaskProperty(const APropName: String; const ANewValue: Variant);
    function  BelongsToContext: Boolean;
    function  ExpiredTask: Boolean;
  public
    constructor CreateFromTask(const ATask: IevTask); override;
    constructor CreateFromTaskFile(const AFileName: String); override;
  end;


  TevActiveTasks = class(TisList)
  protected
    function  CompareItems(const Item1, Item2: IInterface): Integer; override;
  end;


// User Notifications

  IevUserNotificationEvent = interface
  ['{EB2A8D4E-BD8E-485A-8D21-6702A1D428CE}']
    function GetUserAtDomain: String;
    function GetEvent: TevTaskQueueEvent;
    function GetTaskID: TisGUID;
  end;

  TevUserNotificationEvent = class(TisInterfacedObject, IevUserNotificationEvent)
  private
    FUserAtDomain: String;
    FEvent: TevTaskQueueEvent;
    FTaskID: TisGUID;
    function GetUserAtDomain: String;
    function GetEvent: TevTaskQueueEvent;
    function GetTaskID: TisGUID;
  public
    constructor Create(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID); reintroduce;
  end;

  TevUserNotificationSender = class(TReactiveThread)
  private
    FEventsList: IisStringList;
    procedure AddEvent(const AUserAtDomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
  protected
    procedure DoOnStart; override;
    procedure DoWork; override;
    procedure DoOnStop; override;
  public
    procedure AfterConstruction; override;
  end;


// Task Queue main object

  TevTaskQueue = class(TisCollection, IevTaskQueue)
  private
    FActive: Boolean;
    FLoadedFromStorage: Boolean;
    FPulser: TevTaskPulser;
    FNotificationSender: TevUserNotificationSender;
    FActiveTasks: IisList;
    FTaskNbrGen: Integer;
    FLastCleanup: TDateTime;
    FDomainRotation: IisStringList;
    procedure InternalAddTask(const ATask: IevTask);
    procedure LoadTasksFromLocation(const APath: String);
    procedure AddTaskInfo(const ATaskInfo: IevTaskInfo);
    procedure RemoveTaskInfo(const ATaskInfo: IevTaskInfo);
    procedure RemoveTask(const ATaskInfo: IevTaskInfo); overload;
    procedure NotifyUsers(const AUser: String; const ADomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
    function  FindTaskInfo(const AID: TisGUID): IevTaskInfo;
    function  GetTaskInfo(Index: Integer): IevTaskInfo; overload;
    function  GetNextNbr: Integer;
    procedure ResortTasksByPriority;
    function  DoCreateTask(const ATaskType: String): IevTask;
    procedure DeleteExpiredTasks;
    procedure InitializeIfNeeds;
  protected
    procedure DoOnConstruction; override;
    procedure LoadChildrenFromStorage; override;

    function  CreateTask(const ATaskType: String; const ApplyDefaults: Boolean): IevTask;
    procedure AddTask(const ATask: IevTask);
    procedure AddTasks(const ATaskList: IisList); // list of IevTask
    procedure RemoveTask(const AID: TisGUID); overload;
    function  GetTask(const AID: TisGUID): IevTask;
    function  GetTaskInfo(const AID: TisGUID): IevTaskInfo; overload;
    function  GetTaskRequests(const AID: TisGUID): IisListOfValues;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetUserTaskStatus: IisListOfValues;
    function  GetUserTaskInfoList: IisListOfValues;
    procedure ModifyTaskProperty(const AID: TisGUID; const APropName: String; const ANewValue: Variant);
    procedure Pulse;
  public
    destructor Destroy; override;
  end;



function GetTaskQueue: IevTaskQueue;
begin
  Result := TevTaskQueue.Create;
end;


{ TevTaskPulser }

procedure TevTaskPulser.DoOnStart;
begin
  inherited;
  Mainboard.ContextManager.CreateThreadContext(sGuestUserName, '');

  try
    TaskQueueObj.InitializeIfNeeds;
  except
    on E: Exception do
      mb_LogFile.AddContextEvent(etError, 'Task Queue', E.Message, '');
  end;
end;

procedure TevTaskPulser.DoOnStop;
begin
  Mainboard.ContextManager.DestroyThreadContext;
  inherited;
end;

procedure TevTaskPulser.DoWork;
var
  i: Integer;
  Tinf: IevTaskInfoInQueue;
  NotifyList: IisStringList;
  CurrentTaskList: IisList;
  s: String;
  bNoResources: Boolean;
begin
  // Don't let this thread to die from errors
  try
    // Copy active tasks first
    CurrentTaskList := TisList.Create;
    TaskQueueObj.Lock;
    try
      CurrentTaskList.Capacity := TaskQueueObj.FActiveTasks.Count;
      for i := 0 to TaskQueueObj.FActiveTasks.Count - 1 do
      begin
        Tinf := TaskQueueObj.FActiveTasks[i] as IevTaskInfoInQueue;
        if Tinf.State <> tsFinished then
          CurrentTaskList.Add(Tinf);
      end;
    finally
      TaskQueueObj.Unlock;
    end;

    // go by domains first. It guarantees to spread tasks evenly
    bNoResources := False;
    while CurrentTaskList.Count > 0 do
    begin
      // Pulse task and go to next domain
      for i := 0 to CurrentTaskList.Count - 1 do
      begin
        AbortIfCurrentThreadTaskTerminated;

        Tinf := CurrentTaskList[i] as IevTaskInfoInQueue;
        if (Tinf.State <> tsFinished) and AnsiSameText(Tinf.Domain, TaskQueueObj.FDomainRotation[0]) then
        begin
          if not (Tinf.GetTask as IevTaskInQueue).TaskPulse then
            bNoResources := True
          else
            CurrentTaskList.Delete(i);
          break;
        end;
      end;

      if bNoResources then
        break;

      // Remove finished tasks from list to avoid never ending loop
      for i := CurrentTaskList.Count - 1 downto 0 do
      begin
        Tinf := CurrentTaskList[i] as IevTaskInfoInQueue;
        if Tinf.State = tsFinished then
          CurrentTaskList.Delete(i);
      end;

      // move this domain to the end of the list
      TaskQueueObj.FDomainRotation.Move(0, TaskQueueObj.FDomainRotation.Count - 1);
    end;
    CurrentTaskList := nil;

    // Check what tasks are finished
    NotifyList := TisStringList.CreateUnique;

    TaskQueueObj.Lock;
    try
      for i := TaskQueueObj.FActiveTasks.Count - 1 downto 0 do
      begin
        AbortIfCurrentThreadTaskTerminated;

        Tinf := TaskQueueObj.FActiveTasks[i] as IevTaskInfoInQueue;

        if Tinf.State = tsFinished then
        begin
          Tinf.DeactivateTask;
          NotifyList.Add(Tinf.GetUser + '.' + Tinf.GetDomain);
        end;
      end;

    finally
      TaskQueueObj.Unlock;
    end;

    // send notification to users
    for i := 0 to NotifyList.Count - 1 do
    begin
      s := NotifyList[i];
      TaskQueueObj.NotifyUsers(GetNextStrValue(s, '.'), s, tqeTaskListChange, '');
    end;

    TaskQueueObj.DeleteExpiredTasks;

  except
    on E: Exception do
      mb_LogFile.AddContextEvent(etError, 'Task Queue', E.Message, '');
  end;
end;


function TevTaskPulser.TaskQueueObj: TevTaskQueue;
begin
  Result := TevTaskQueue((mb_TaskQueue as IisInterfacedObject).GetImplementation);
end;

{ TevTaskQueue }

procedure TevTaskQueue.DoOnConstruction;
begin
  inherited;
  InitLock;
  FActiveTasks := TevActiveTasks.Create;
  FDomainRotation := TisStringList.Create;
  FNotificationSender := TevUserNotificationSender.Create('Task Queue notifications');
end;

destructor TevTaskQueue.Destroy;
begin
  FreeAndNil(FPulser);
  FreeAndNil(FNotificationSender);
  inherited;
end;

procedure TevTaskQueue.Pulse;
begin
  if GetActive then
    FPulser.Wakeup;
end;

procedure TevTaskQueue.AddTask(const ATask: IevTask);
begin
  Lock;
  try
    InternalAddTask(ATask);
    ResortTasksByPriority;
  finally
    Unlock;
  end;

  NotifyUsers(ATask.User, ATask.Domain, tqeTaskListChange, '');
  Pulse;
end;

function TevTaskQueue.GetTask(const AID: TisGUID): IevTask;
var
  Tinf: IevTaskInfoInQueue;
begin
  Tinf := FindTaskInfo(AID) as IevTaskInfoInQueue;

  if Assigned(Tinf) and Tinf.BelongsToContext then
    Result := (Tinf as IevTaskInfoInQueue).GetTask
  else
    Result := nil;
end;

function TevTaskQueue.CreateTask(const ATaskType: String; const ApplyDefaults: Boolean): IevTask;
begin
  Result := DoCreateTask(ATaskType);
  if ApplyDefaults then
    (Result as IevTaskInQueue).SetDefaults;
end;


procedure TevTaskQueue.LoadChildrenFromStorage;
var
  i: Integer;
  Tinf: IevTaskInfo;
begin
  inherited;
  RemoveAllChildren;

  // load all domains independently
  mb_GlobalSettings.DomainInfoList.Lock;
  try
    for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      LoadTasksFromLocation(mb_GlobalSettings.DomainInfoList[i].FileFolders.TaskQueueFolder);
  finally
    mb_GlobalSettings.DomainInfoList.Unlock;
  end;

  // initialize task nbr generator
  for i := 0 to ChildCount - 1 do
  begin
    Tinf := GetTaskInfo(i);
    if FTaskNbrGen < Tinf.Nbr then
      FTaskNbrGen := Tinf.Nbr;
  end;

  DeleteExpiredTasks;
  ResortTasksByPriority;

  Pulse;
end;

procedure TevTaskQueue.LoadTasksFromLocation(const APath: String);
var
  DosCode: Integer;
  FileInfo: TSearchRec;
  Folder: String;

  procedure LoadTask(const ATaskFile: String);
  var
    TaskInfo: IevTaskInfo;
  begin
    TaskInfo := nil;
    try
      TaskInfo := TevTaskInfoInQueue.CreateFromTaskFile(ATaskFile);
    except
      // eat exception  if task file is corrupted
    end;

    if Assigned(TaskInfo) then
    begin
      if (TaskInfo as IevTaskInfoInQueue).ExpiredTask then
        RemoveTask(TaskInfo)
      else
        AddTaskInfo(TaskInfo);
    end;
  end;

begin
  Folder := NormalizePath(APath);

  if Folder = '' then
    Exit;
    
  ForceDirectories(Folder);

  DosCode := FindFirst(Folder + '*' + sTaskFileExt, faAnyFile, FileInfo);
  try
    while DosCode = 0 do
    begin
      LoadTask(Folder + FileInfo.Name);
      DosCode := FindNext(FileInfo);
    end;
  finally
    FindClose(FileInfo);
  end;
end;

function TevTaskQueue.GetActive: Boolean;
begin
  Result := FActive;
end;

procedure TevTaskQueue.SetActive(const AValue: Boolean);

  procedure PrepareDomainList;
  var
    i: Integer;
  begin
    FDomainRotation.Clear;
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
        FDomainRotation.Add(mb_GlobalSettings.DomainInfoList[i].DomainName);
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
  end;

begin
  Lock;
  try
    if GetActive <> AValue then
      if AValue then
      begin
        PrepareDomainList;
        FPulser := TevTaskPulser.Create('Task Queue pulser');
        FActive := True;
        Pulse;
      end
      else
      begin
        FActive := False;
        FreeAndNil(FPulser);
        mb_AsyncFunctions.TerminateCall('');
      end;
  finally
    Unlock;
  end;
end;

procedure TevTaskQueue.RemoveTaskInfo(const ATaskInfo: IevTaskInfo);
begin
  (ATaskInfo as IevTaskInfoInQueue).DeactivateTask;
  (ATaskInfo as IisInterfacedObject).Owner := nil;
end;

procedure TevTaskQueue.AddTaskInfo(const ATaskInfo: IevTaskInfo);
begin
  (ATaskInfo as IisInterfacedObject).Owner := Self;

  if ATaskInfo.State <> tsFinished then
    (ATaskInfo as IevTaskInfoInQueue).ActivateTask
  else
    (ATaskInfo as IevTaskInfoInQueue).DeactivateTask;
end;

procedure TevTaskQueue.RemoveTask(const AID: TisGUID);
var
  Tinf: IevTaskInfoInQueue;
  Tsk: IevTaskInQueue;
  bNotify: Boolean;
begin
  Tsk := nil;
  bNotify := False;
  Lock;
  try
    Tinf := FindTaskInfo(AID) as IevTaskInfoInQueue;

    if Assigned(Tinf) and Tinf.BelongsToContext then
    begin
      if Tinf.State = tsExecuting then
      begin
        Tsk := Tinf.GetTask as IevTaskInQueue;
        if Context.UserAccount.AccountType <> uatSBAdmin then
          raise EevException.Create('Only ADMIN user can delete executing tasks!');
      end;
      RemoveTask(TInf);
      bNotify := True;
    end;
  finally
    Unlock;
  end;

  if Assigned(Tsk) then
    Tsk.Terminate;

  if bNotify then
    NotifyUsers(Tinf.User, Tinf.Domain, tqeTaskListChange, '');
end;

procedure TevTaskQueue.NotifyUsers(const AUser: String; const ADomain: String; const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
begin
  FNotificationSender.AddEvent(EncodeUserAtDomain(AUser, ADomain), AEvent, ATaskID);
end;

function TevTaskQueue.GetUserTaskStatus: IisListOfValues;
var
  OtherActive, OtherFinished: Integer;
  UserActive, UserFinished, UserNotSeen: Integer;
  L: IisListOfValues;
  Tinf: IevTaskInfo;
  i: Integer;
begin
  L := GetUserTaskInfoList;

  OtherActive := 0;
  OtherFinished := 0;
  UserActive := 0;
  UserFinished := 0;
  UserNotSeen := 0;

  for i := 0 to L.Count - 1 do
  begin
    Tinf := IInterface(L[i].Value) as IevTaskInfo;

    if AnsiSameText(Tinf.User, Context.UserAccount.User) then
    begin
      if Tinf.State = tsFinished then
      begin
        Inc(UserFinished);
        if not Tinf.SeenByUser then
          Inc(UserNotSeen);
      end
      else
        Inc(UserActive);
    end

    else
    begin
      if Tinf.State = tsFinished then
        Inc(OtherFinished)
      else
        Inc(OtherActive);
    end;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('UserActiveTasks', UserActive);
  Result.AddValue('UserFinishedTasks', UserFinished);
  Result.AddValue('UserNotSeenTasks', UserNotSeen);
  Result.AddValue('OtherActiveTasks', OtherActive);
  Result.AddValue('OtherFinishedTasks', OtherFinished);
end;


function TevTaskQueue.FindTaskInfo(const AID: TisGUID): IevTaskInfo;
begin
  Result := FindChildByName(AID) as IevTaskInfo;
end;

function TevTaskQueue.GetNextNbr: Integer;
begin
  Result := InterlockedIncrement(FTaskNbrGen);
end;

function TevTaskQueue.GetTaskInfo(Index: Integer): IevTaskInfo;
begin
  Result := GetChild(Index) as IevTaskInfo;
end;

function TevTaskQueue.GetUserTaskInfoList: IisListOfValues;
var
  i: Integer;
  Tinf: IevTaskInfoInQueue;
begin
  Result := TisListOfValues.Create;

  Lock;
  try
    for i := 0 to ChildCount - 1 do
    begin
      Tinf := GetTaskInfo(i) as IevTaskInfoInQueue;
      if Tinf.BelongsToContext then
        Result.AddValue(Tinf.ID, Tinf);
    end;
  finally
    Unlock;
  end;
end;


function TevTaskQueue.GetTaskRequests(const AID: TisGUID): IisListOfValues;
var
  Tinf: IevTaskInfoInQueue;
  Tsk: IevTaskInQueue;
  R: IevTaskRequestInfo;
  L: IisList;
  i: Integer;
begin
  Result := TisListOfValues.Create;

  Tinf := FindTaskInfo(AID) as IevTaskInfoInQueue;

  if Assigned(Tinf) and Tinf.BelongsToContext then
  begin
    Tsk := (Tinf as IevTaskInfoInQueue).GetTask as IevTaskInQueue;
    L := Tsk.GetRequests;
    for i := 0 to L.Count - 1 do
    begin
      R := TevTaskRequestInfo.CreateFromRequest(L[i] as IevTaskRequest);
      Result.AddValue(R.Name, R);
    end;
  end;
end;

function TevTaskQueue.GetTaskInfo(const AID: TisGUID): IevTaskInfo;
var
  Tinf: IevTaskInfoInQueue;
begin
  Tinf := FindTaskInfo(AID) as IevTaskInfoInQueue;
  if Assigned(Tinf) and Tinf.BelongsToContext then
    Result := Tinf
  else
    Result := nil;
end;

procedure TevTaskQueue.ModifyTaskProperty(const AID: TisGUID; const APropName: String; const ANewValue: Variant);
var
  Tinf: IevTaskInfoInQueue;
begin
  Tinf := FindTaskInfo(AID) as IevTaskInfoInQueue;

  if Assigned(Tinf) and Tinf.BelongsToContext then
  begin
    Tinf.ModifyTaskProperty(APropName, ANewValue);

    if AnsiSameText(APropName, 'SeenByUser') then
      NotifyUsers(Tinf.User, Tinf.Domain, tqeTaskListChange, '')

    else if AnsiSameText(APropName, 'Priority') then
    begin
      ResortTasksByPriority;
      Pulse;
    end;
  end;
end;

procedure TevTaskQueue.ResortTasksByPriority;
begin
  Lock;
  try
    FActiveTasks.Sort;
  finally
    Unlock;
  end;
end;

function TevTaskQueue.DoCreateTask(const ATaskType: String): IevTask;
begin
  Result := ObjectFactory.CreateInstanceOf(ATaskType) as IevTask;
end;


procedure TevTaskQueue.DeleteExpiredTasks;
var
  i: Integer;
  Tinf: IevTaskInfoInQueue;
  NotifyList: IisStringList;
  s: String;
begin
  if DaysBetween(Now, FLastCleanup) >= 1 then
  begin
    NotifyList := TisStringList.CreateUnique;

    Lock;
    try
      for i := ChildCount - 1 downto 0 do
      begin
        Tinf := GetTaskInfo(i) as IevTaskInfoInQueue;
        if (Tinf.State = tsFinished) and Tinf.ExpiredTask then
        begin
          NotifyList.Add(Tinf.User + '.' + Tinf.Domain);
          RemoveTask(Tinf);
        end;
      end;
    finally
      Unlock;
    end;

    // send notification to users
    for i := 0 to NotifyList.Count - 1 do
    begin
      s := NotifyList[i];
      NotifyUsers(GetNextStrValue(s, '.'), s, tqeTaskListChange, '');
    end;

    FLastCleanup := Now;
  end;
end;

procedure TevTaskQueue.RemoveTask(const ATaskInfo: IevTaskInfo);
var
  s: String;
begin
  RemoveTaskInfo(ATaskInfo);

  s := TevTaskInfoInQueue((ATaskInfo as IisInterfacedObject).GetImplementation).FTaskFile;
  DeleteFile(s);
  s := ChangeFileExt(s, '');
  ClearDir(s, True);
end;


procedure TevTaskQueue.AddTasks(const ATaskList: IisList);
var
  i: Integer;
begin
  if ATaskList.Count = 0 then
    Exit;

  Lock;
  try
    for i := 0 to ATaskList.Count - 1 do
      InternalAddTask(ATaskList[i] as IevTask);
    ResortTasksByPriority;
  finally
    Unlock;
  end;

  NotifyUsers((ATaskList[0] as IevTask).User, (ATaskList[0] as IevTask).Domain, tqeTaskListChange, '');
  Pulse;
end;

procedure TevTaskQueue.InternalAddTask(const ATask: IevTask);
var
  T: IevTaskInfo;
begin
  InitializeIfNeeds;

  (ATask as IevTaskInQueue).SetNbr(GetNextNbr);
  (ATask as IevTaskInQueue).SetLastUpdateToNow;
  T := TevTaskInfoInQueue.CreateFromTask(ATask);

  AddTaskInfo(T);
end;

procedure TevTaskQueue.InitializeIfNeeds;
begin
  // Initial stored tasks loading
  Lock;
  try
    if not FLoadedFromStorage then
    begin
      FLoadedFromStorage := True;
      LoadChildrenFromStorage;
    end;
  finally
    Unlock;
  end;
end;

{ TevTaskInfoInQueue }

procedure TevTaskInfoInQueue.ActivateTask;
begin
  try
    TaskQueueObj.Lock;
    try
      if not Assigned(FTask) then
      begin
        FTask := OpenTask;
        AssignFromTask(FTask);
      end;

      if TaskQueueObj.FActiveTasks.IndexOf(Self as IevTaskInfo) = -1 then
        TaskQueueObj.FActiveTasks.Add(Self as IevTaskInfo);
    finally
      TaskQueueObj.Unlock;
    end;

    (FTask as IevTaskInQueue).DoOnActivate;
  except
    on E: Exception do
    begin
      mb_LogFile.AddContextEvent(etError, 'Task Queue', 'Cannot activate task: ' + FTaskFile + #13 + E.Message, '');
      DeactivateTask;
    end;
  end;
end;

function TevTaskInfoInQueue.BelongsToContext: Boolean;
begin
  Result := AnsiSameText(Context.UserAccount.Domain, GetDomain) and
    ((ctx_AccountRights.Functions.GetState('ALLOW_ADMIN_QUEUE') = stEnabled) or AnsiSameText(Context.UserAccount.User, GetUser));
end;

constructor TevTaskInfoInQueue.CreateFromTask(const ATask: IevTask);
var
  D: IevDomainInfo;
begin
  inherited;
  D := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ATask.Domain);
  CheckCondition(Assigned(D), 'Domain ' + ATask.Domain + ' is not registered');
  FTaskFile := NormalizePath(D.FileFolders.TaskQueueFolder) + IntToStr(ATask.Nbr) + sTaskFileExt;
  FTask := ATask;
  (FTask as IevTaskInQueue).SetTaskStorageLocation(FTaskFile);
end;

constructor TevTaskInfoInQueue.CreateFromTaskFile(const AFileName: String);
begin
  inherited;
  FTaskFile := AFileName;
end;

procedure TevTaskInfoInQueue.DeactivateTask;
begin
  TaskQueueObj.Lock;
  try
    TaskQueueObj.FActiveTasks.Remove(Self as IevTaskInfo);

    if Assigned(FTask) then
    begin
      AssignFromTask(FTask);
      FTask := nil;
    end;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.ExpiredTask: Boolean;
  function CountDate(const d: TDateTime; count: Integer): TDateTime;
  begin
    Result := d;
    while count > 0 do
    begin
      repeat
        Result := Result + 1;
      until DayOfTheWeek(Result) in [1..5];
      Dec(count);
    end;
  end;
begin
  Result := (GetSeenByUser and (CountDate(GetLastUpdate, GetDaysToStayInQueue) < Now))
              or (not GetSeenByUser and (CountDate(GetLastUpdate, Max(GetDaysToStayInQueue, 10)) < Now));
end;

function TevTaskInfoInQueue.GetCaption: String;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.Caption
    else
      Result := inherited GetCaption;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetFirstRunAt: TDateTime;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.FirstRunAt
    else
      Result := inherited GetFirstRunAt;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetLastUpdate: TDateTime;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.LastUpdate
    else
      Result := inherited GetLastUpdate;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetNotificationEmail: string;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.NotificationEmail
    else
      Result := inherited GetNotificationEmail;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetPriority: Integer;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.Priority
    else
      Result := inherited GetPriority;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetProgressText: string;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.ProgressText
    else
      Result := inherited GetProgressText;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetSeenByUser: Boolean;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.SeenByUser
    else
      Result := inherited GetSeenByUser;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetSendEmailNotification: Boolean;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.SendEmailNotification
    else
      Result := inherited GetSendEmailNotification;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetState: TevTaskState;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask.State
    else
      Result := inherited GetState;
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.GetTask: IevTask;
begin
  TaskQueueObj.Lock;
  try
    if Assigned(FTask) then
      Result := FTask
    else
      Result := OpenTask;
  finally
    TaskQueueObj.Unlock;
  end;
end;


procedure TevTaskInfoInQueue.ModifyTaskProperty(const APropName: String; const ANewValue: Variant);
var
  Tsk: IevTask;
begin
  TaskQueueObj.Lock;
  try
    Tsk := GetTask;
    (Tsk as IevTaskInQueue).ModifyTaskProperty(APropName, ANewValue);
    AssignFromTask(Tsk);
  finally
    TaskQueueObj.Unlock;
  end;
end;

function TevTaskInfoInQueue.OpenTask: IevTask;
var
  S: IevDualStream;
begin
  Result := nil;

  try
    Result := TaskQueueObj.DoCreateTask(GetTaskType);
  except
    // in case if there is no such task type
  end;

  if Assigned(Result) then
  begin
    S := TEvDualStreamHolder.CreateFromFile(FTaskFile);
    S.Position := 0;
    (Result as IisInterfacedObject).ReadFromStream(S);
    S := nil;
    (Result as IevTaskInQueue).SetTaskStorageLocation(FTaskFile);
  end;
end;

function TevTaskInfoInQueue.TaskQueueObj: TevTaskQueue;
begin
  Result := TevTaskQueue((mb_TaskQueue as IisInterfacedObject).GetImplementation);
end;

{ TevActiveTasks }

function TevActiveTasks.CompareItems(const Item1, Item2: IInterface): Integer;
var
 T1, T2: IevTaskInfo;
begin
 T1 := Item1 as IevTaskInfo;
 T2 := Item2 as IevTaskInfo;

 Result := CompareValue(T1.Priority, T2.Priority); // ASC

 // DESC
 if Result = EqualsValue then
   if T1.State < T2.State then
     Result := GreaterThanValue
   else if T1.State > T2.State then
     Result := LessThanValue
   else
     Result := CompareValue(T1.LastUpdate, T2.LastUpdate); //ASC
end;

{ TevUserNotificationSender }

procedure TevUserNotificationSender.AddEvent(const AUserAtDomain: String;
  const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
var
  s: String;
  E: IevUserNotificationEvent;
begin
  s := AUserAtDomain + ' ' + IntToStr(Ord(AEvent)) + ' ' + ATaskID;

  FEventsList.Lock;
  try
    if FEventsList.IndexOf(s) = -1 then
    begin
      E := TevUserNotificationEvent.Create(AUserAtDomain, AEvent, ATaskID);
      FEventsList.AddObject(s, E);
      Wakeup;
    end;
  finally
    FEventsList.UnLock;
  end;
end;

procedure TevUserNotificationSender.AfterConstruction;
begin
  inherited;
  FEventsList := TisStringList.CreateUnique(True);
end;

procedure TevUserNotificationSender.DoOnStart;
begin
  inherited;
  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
end;

procedure TevUserNotificationSender.DoOnStop;
begin
  inherited;
  if Mainboard <> nil then
    Mainboard.ContextManager.DestroyThreadContext;
end;

procedure TevUserNotificationSender.DoWork;
var
  i: Integer;
  E: IevUserNotificationEvent;
begin
  FEventsList.Lock;
  try
    for i := 0 to FEventsList.Count - 1 do
      if Mainboard.GlobalCallbacks <> nil then
      begin
        E := FEventsList.Objects[i] as IevUserNotificationEvent;
        Mainboard.GlobalCallbacks.NotifyTaskQueueEvent(E.GetUserAtDomain, E.GetEvent, E.GetTaskID);
      end;

    FEventsList.Clear;
  finally
    FEventsList.Unlock;
  end;

  Snooze(1000);  // In order to avoid flood wait 1 second before sending another banch of events
end;

{ TevUserNotificationEvent }

constructor TevUserNotificationEvent.Create(const AUserAtDomain: String;
  const AEvent: TevTaskQueueEvent; const ATaskID: TisGUID);
begin
  inherited Create;
  FUserAtDomain := AUserAtDomain;
  FEvent := AEvent;
  FTaskID := ATaskID;
end;

function TevUserNotificationEvent.GetEvent: TevTaskQueueEvent;
begin
  Result := FEvent;
end;

function TevUserNotificationEvent.GetTaskID: TisGUID;
begin
  Result := FTaskID;
end;

function TevUserNotificationEvent.GetUserAtDomain: String;
begin
  Result := FUserAtDomain;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTaskQueue, IevTaskQueue, 'Task Queue');

end.

