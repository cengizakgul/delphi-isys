unit EvCreatePayrollTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext, SFieldCodeValues;

implementation

type
  TEvCreatePayrollTask = class(TEvCustomTask, IevCreatePayrollTask)
  private
    FAutoCommit: Boolean;
    FTcAutoImportJobCodes: Boolean;
    FBatchFreq: String;
    FBlock401K: Integer;
    FBlockACH: Integer;
    FBlockAgencies: Integer;
    FBlockBilling: Integer;
    FBlockChecks: Integer;
    FBlockTaxDeposits: Integer;
    FBlockTimeOff: Integer;
    FCheckComments: String;
    FCheckDate: TDateTime;
    FCheckDateReplace: Integer;
    FCheckFreq: String;
    FCLNbr: Integer;
    FCONbr: Integer;
    FEEList: string;
    FLoadDefaults: Integer;
    FPayHours: Integer;
    FPaySalary: Integer;
    FPeriodBeginDate: TDateTime;
    FPeriodEndDate: TDateTime;
    FPRFilterNbr: Integer;
    FPrList: string;
    FPRTemplateNbr: Integer;
    FSalaryOrHourly: Integer;
    FTcDBDTOption: TCImportDBDTOption;
    FTcFileFormat: TCFileFormatOption;
    FTcFourDigitYear: Boolean;
    FTcImportSource: string;
    FTcImportSourceFile: IevDualStream;
    FTcLookupOption: TCImportLookupOption;
    FTcUseEmployeePayRates: Boolean;
    FPrNbr: Integer;
    FIncludeTimeOffRequests: Boolean;
  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;
    function   Revision: TisRevision; override;

    function GetAutoCommit: Boolean;
    function GetTcAutoImportJobCodes: Boolean;
    function GetBatchFreq: String;
    function GetBlock401K: Integer;
    function GetBlockACH: Integer;
    function GetBlockAgencies: Integer;
    function GetBlockBilling: Integer;
    function GetBlockChecks: Integer;
    function GetBlockTaxDeposits: Integer;
    function GetBlockTimeOff: Integer;
    function GetCheckComments: String;
    function GetCheckDate: TDateTime;
    function GetCheckDateReplace: Integer;
    function GetCheckFreq: String;
    function GetCLNbr: Integer;
    function GetCONbr: Integer;
    function GetEEList: string;
    function GetLoadDefaults: Integer;
    function GetPayHours: Integer;
    function GetPaySalary: Integer;
    function GetPeriodBeginDate: TDateTime;
    function GetPeriodEndDate: TDateTime;
    function GetPRFilterNbr: Integer;
    function GetPrList: string;
    function GetPRTemplateNbr: Integer;
    function GetSalaryOrHourly: Integer;
    function GetTcDBDTOption: TCImportDBDTOption;
    function GetTcFileFormat: TCFileFormatOption;
    function GetTcFourDigitYear: Boolean;
    function GetTcImportSource: string;
    function GetTcImportSourceFile: IevDualStream;
    function GetTcLookupOption: TCImportLookupOption;
    function GetTcUseEmployeePayRates: Boolean;
    function GetIncludeTimeOffRequests: Boolean;
    procedure SetAutoCommit(const AValue: Boolean);
    procedure SetTcAutoImportJobCodes(const AValue: Boolean);
    procedure SetBatchFreq(const AValue: String);
    procedure SetBlock401K(const AValue: Integer);
    procedure SetBlockACH(const AValue: Integer);
    procedure SetBlockAgencies(const AValue: Integer);
    procedure SetBlockBilling(const AValue: Integer);
    procedure SetBlockChecks(const AValue: Integer);
    procedure SetBlockTaxDeposits(const AValue: Integer);
    procedure SetBlockTimeOff(const AValue: Integer);
    procedure SetCheckComments(const AValue: String);
    procedure SetCheckDate(const AValue: TDateTime);
    procedure SetCheckDateReplace(const AValue: Integer);
    procedure SetCheckFreq(const AValue: String);
    procedure SetCLNbr(const AValue: Integer);
    procedure SetCONbr(const AValue: Integer);
    procedure SetEEList(const AValue: string);
    procedure SetLoadDefaults(const AValue: Integer);
    procedure SetPayHours(const AValue: Integer);
    procedure SetPaySalary(const AValue: Integer);
    procedure SetPeriodBeginDate(const AValue: TDateTime);
    procedure SetPeriodEndDate(const AValue: TDateTime);
    procedure SetPRFilterNbr(const AValue: Integer);
    procedure SetPrList(const AValue: string);
    procedure SetPRTemplateNbr(const AValue: Integer);
    procedure SetSalaryOrHourly(const AValue: Integer);
    procedure SetTcDBDTOption(const AValue: TCImportDBDTOption);
    procedure SetTcFileFormat(const AValue: TCFileFormatOption);
    procedure SetTcFourDigitYear(const AValue: Boolean);
    procedure SetTcImportSource(const AValue: string);
    procedure SetTcImportSourceFile(const AValue: IevDualStream);
    procedure SetTcLookupOption(const AValue: TCImportLookupOption);
    procedure SetTcUseEmployeePayRates(const AValue: Boolean);
    procedure SetIncludeTimeOffRequests(const AValue: Boolean);

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_CreatePayroll(const AInputRequests: TevTaskRequestList);
    procedure  Phase_AfterCreatePayroll(const AInputRequests: TevTaskRequestList);
  public
    class function GetTypeID: String; override;
  end;


{ TEvCreatePayrollTask }

procedure TEvCreatePayrollTask.DoRestoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  FPrNbr := AStream.ReadInteger;
end;

procedure TEvCreatePayrollTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteInteger(FPrNbr);
end;

function TEvCreatePayrollTask.GetAutoCommit: Boolean;
begin
  Result := FAutoCommit;
end;

function TEvCreatePayrollTask.GetTcAutoImportJobCodes: Boolean;
begin
  Result := FTcAutoImportJobCodes;
end;

function TEvCreatePayrollTask.GetBatchFreq: String;
begin
  Result := FBatchFreq;
end;

function TEvCreatePayrollTask.GetBlock401K: Integer;
begin
  Result := FBlock401K;
end;

function TEvCreatePayrollTask.GetBlockACH: Integer;
begin
  Result := FBlockACH;
end;

function TEvCreatePayrollTask.GetBlockAgencies: Integer;
begin
  Result := FBlockAgencies;
end;

function TEvCreatePayrollTask.GetBlockBilling: Integer;
begin
  Result := FBlockBilling;
end;

function TEvCreatePayrollTask.GetBlockChecks: Integer;
begin
  Result := FBlockChecks;
end;

function TEvCreatePayrollTask.GetBlockTaxDeposits: Integer;
begin
  Result := FBlockTaxDeposits;
end;

function TEvCreatePayrollTask.GetBlockTimeOff: Integer;
begin
  Result := FBlockTimeOff;
end;

function TEvCreatePayrollTask.GetCheckComments: String;
begin
  Result := FCheckComments;
end;

function TEvCreatePayrollTask.GetCheckDate: TDateTime;
begin
  Result := FCheckDate;
end;

function TEvCreatePayrollTask.GetCheckDateReplace: Integer;
begin
  Result := FCheckDateReplace;
end;

function TEvCreatePayrollTask.GetCheckFreq: String;
begin
  Result := FCheckFreq;
end;

function TEvCreatePayrollTask.GetCLNbr: Integer;
begin
  Result := FCLNbr;
end;

function TEvCreatePayrollTask.GetCONbr: Integer;
begin
  Result := FCONbr;
end;

function TEvCreatePayrollTask.GetEEList: string;
begin
  Result := FEEList;
end;

function TEvCreatePayrollTask.GetLoadDefaults: Integer;
begin
  Result := FLoadDefaults;
end;

function TEvCreatePayrollTask.GetPayHours: Integer;
begin
  Result := FPayHours;
end;

function TEvCreatePayrollTask.GetPaySalary: Integer;
begin
  Result := FPaySalary;
end;

function TEvCreatePayrollTask.GetPeriodBeginDate: TDateTime;
begin
  Result := FPeriodBeginDate;
end;

function TEvCreatePayrollTask.GetPeriodEndDate: TDateTime;
begin
  Result := FPeriodEndDate;
end;

function TEvCreatePayrollTask.GetPRFilterNbr: Integer;
begin
  Result := FPRFilterNbr;
end;

function TEvCreatePayrollTask.GetPrList: string;
begin
  Result := FPrList;
end;

function TEvCreatePayrollTask.GetPRTemplateNbr: Integer;
begin
  Result := FPRTemplateNbr;
end;

function TEvCreatePayrollTask.GetSalaryOrHourly: Integer;
begin
  Result := FSalaryOrHourly;
end;

function TEvCreatePayrollTask.GetTcDBDTOption: TCImportDBDTOption;
begin
  Result := FTcDBDTOption;
end;

function TEvCreatePayrollTask.GetTcFileFormat: TCFileFormatOption;
begin
  Result := FTcFileFormat;
end;

function TEvCreatePayrollTask.GetTcFourDigitYear: Boolean;
begin
  Result := FTcFourDigitYear;
end;

function TEvCreatePayrollTask.GetTcImportSource: string;
begin
  Result := FTcImportSource;
end;

function TEvCreatePayrollTask.GetTcImportSourceFile: IevDualStream;
begin
  Result := FTcImportSourceFile;
end;

function TEvCreatePayrollTask.GetTcLookupOption: TCImportLookupOption;
begin
  Result := FTcLookupOption;
end;

function TEvCreatePayrollTask.GetTcUseEmployeePayRates: Boolean;
begin
  Result := FTcUseEmployeePayRates;
end;

class function TEvCreatePayrollTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_CREATE_PAYROLL;
end;


procedure TEvCreatePayrollTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  if (FBatchFreq = '') and ((FCheckFreq = '') or (Pos(FCheckFreq, PayFrequencies_ComboChoices) = 0)) then
    Exit;

  Params := TisListOfValues.Create;
  Params.AddValue('BatchFreq', FBatchFreq);
  Params.AddValue('Block401K', FBlock401K);
  Params.AddValue('BlockACH', FBlockACH);
  Params.AddValue('BlockAgencies', FBlockAgencies);
  Params.AddValue('BlockBilling', FBlockBilling);
  Params.AddValue('BlockChecks', FBlockChecks);
  Params.AddValue('BlockTaxDeposits', FBlockTaxDeposits);
  Params.AddValue('BlockTimeOff', FBlockTimeOff);
  Params.AddValue('CheckComments', FCheckComments);
  Params.AddValue('CheckDate', FCheckDate);
  Params.AddValue('CheckDateReplace', FCheckDateReplace);
  Params.AddValue('CheckFreq', FCheckFreq);
  Params.AddValue('ClNbr', FCLNbr);
  Params.AddValue('CoNbr', FCONbr);
  Params.AddValue('EEList', FEEList);
  Params.AddValue('LoadDefaults', FLoadDefaults);
  Params.AddValue('PayHours', FPayHours);
  Params.AddValue('PaySalary', FPaySalary);
  Params.AddValue('PeriodBeginDate', FPeriodBeginDate);
  Params.AddValue('PeriodEndDate', FPeriodEndDate);
  Params.AddValue('PRFilterNbr', FPRFilterNbr);
  Params.AddValue('PRTemplateNbr', FPRTemplateNbr);
  Params.AddValue('TcImportSource', FTcImportSource);

  AddRequest('Prepare for payroll creation', 'PrepareForPayrollCreation', Params, 'CreatePayroll.Fork');
end;

procedure TEvCreatePayrollTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FAutoCommit := AStream.ReadBoolean;
  FTcAutoImportJobCodes := AStream.ReadBoolean;
  FBatchFreq := AStream.ReadString;
  FBlock401K := AStream.ReadInteger;
  FBlockACH := AStream.ReadInteger;
  FBlockAgencies := AStream.ReadInteger;
  FBlockBilling := AStream.ReadInteger;
  FBlockChecks := AStream.ReadInteger;
  FBlockTaxDeposits := AStream.ReadInteger;
  FBlockTimeOff := AStream.ReadInteger;
  FCheckComments := AStream.ReadString;
  FCheckDate := AStream.ReadDouble;
  FCheckDateReplace := AStream.ReadInteger;
  FCheckFreq := AStream.ReadString;
  FCLNbr := AStream.ReadInteger;
  FCONbr := AStream.ReadInteger;
  FEEList := AStream.ReadString;
  FLoadDefaults := AStream.ReadInteger;
  FPayHours := AStream.ReadInteger;
  FPaySalary := AStream.ReadInteger;
  FPeriodBeginDate := AStream.ReadDouble;
  FPeriodEndDate := AStream.ReadDouble;
  FPRFilterNbr := AStream.ReadInteger;
  FPrList := AStream.ReadString;
  FPRTemplateNbr := AStream.ReadInteger;
  FSalaryOrHourly := AStream.ReadInteger;
  FTcDBDTOption := TCImportDBDTOption(AStream.ReadInteger);
  FTcFileFormat := TCFileFormatOption(AStream.ReadInteger);
  FTcFourDigitYear := AStream.ReadBoolean;
  FTcImportSource := AStream.ReadString;
  FTcImportSourceFile := AStream.ReadStream(nil, True);
  FTcLookupOption := TCImportLookupOption(AStream.ReadInteger);
  FTcUseEmployeePayRates := AStream.ReadBoolean;
  if ARevision >= 7 then
    FIncludeTimeOffRequests := AStream.ReadBoolean
  else
    FIncludeTimeOffRequests := False;
end;

procedure TEvCreatePayrollTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('CreatePayroll', Phase_CreatePayroll);
  RegisterTaskPhase('AfterCreatePayroll', Phase_AfterCreatePayroll);
end;

procedure TEvCreatePayrollTask.SetAutoCommit(const AValue: Boolean);
begin
  FAutoCommit := AValue;
end;

procedure TEvCreatePayrollTask.SetTcAutoImportJobCodes(const AValue: Boolean);
begin
  FTcAutoImportJobCodes := AValue;
end;

procedure TEvCreatePayrollTask.SetBatchFreq(const AValue: String);
begin
  FBatchFreq := AValue;
end;

procedure TEvCreatePayrollTask.SetBlock401K(const AValue: Integer);
begin
  FBlock401K := AValue;
end;

procedure TEvCreatePayrollTask.SetBlockACH(const AValue: Integer);
begin
  FBlockACH := AValue;
end;

procedure TEvCreatePayrollTask.SetBlockAgencies(const AValue: Integer);
begin
  FBlockAgencies := AValue;
end;

procedure TEvCreatePayrollTask.SetBlockBilling(const AValue: Integer);
begin
  FBlockBilling := AValue;
end;

procedure TEvCreatePayrollTask.SetBlockChecks(const AValue: Integer);
begin
  FBlockChecks := AValue;
end;

procedure TEvCreatePayrollTask.SetBlockTaxDeposits(const AValue: Integer);
begin
  FBlockTaxDeposits := AValue;
end;

procedure TEvCreatePayrollTask.SetBlockTimeOff(const AValue: Integer);
begin
  FBlockTimeOff := AValue;
end;

procedure TEvCreatePayrollTask.SetCheckComments(const AValue: String);
begin
  FCheckComments := AValue;
end;

procedure TEvCreatePayrollTask.SetCheckDate(const AValue: TDateTime);
begin
  FCheckDate := AValue;
end;

procedure TEvCreatePayrollTask.SetCheckDateReplace(const AValue: Integer);
begin
  FCheckDateReplace := AValue;
end;

procedure TEvCreatePayrollTask.SetCheckFreq(const AValue: String);
begin
  FCheckFreq := AValue;
end;

procedure TEvCreatePayrollTask.SetCLNbr(const AValue: Integer);
begin
  FCLNbr := AValue;
end;

procedure TEvCreatePayrollTask.SetCONbr(const AValue: Integer);
begin
  FCONbr := AValue;
end;

procedure TEvCreatePayrollTask.SetEEList(const AValue: string);
begin
  FEEList := AValue;
end;

procedure TEvCreatePayrollTask.SetLoadDefaults(const AValue: Integer);
begin
  FLoadDefaults := AValue;
end;

procedure TEvCreatePayrollTask.SetPayHours(const AValue: Integer);
begin
  FPayHours := AValue;
end;

procedure TEvCreatePayrollTask.SetPaySalary(const AValue: Integer);
begin
  FPaySalary := AValue;
end;

procedure TEvCreatePayrollTask.SetPeriodBeginDate(const AValue: TDateTime);
begin
  FPeriodBeginDate := AValue;
end;

procedure TEvCreatePayrollTask.SetPeriodEndDate(const AValue: TDateTime);
begin
  FPeriodEndDate := AValue;
end;

procedure TEvCreatePayrollTask.SetPRFilterNbr(const AValue: Integer);
begin
  FPRFilterNbr := AValue;
end;

procedure TEvCreatePayrollTask.SetPrList(const AValue: string);
begin
  FPrList := AValue;
end;

procedure TEvCreatePayrollTask.SetPRTemplateNbr(const AValue: Integer);
begin
  FPRTemplateNbr := AValue;
end;

procedure TEvCreatePayrollTask.SetSalaryOrHourly(const AValue: Integer);
begin
  FSalaryOrHourly := AValue;
end;

procedure TEvCreatePayrollTask.SetTcDBDTOption(const AValue: TCImportDBDTOption);
begin
  FTcDBDTOption := AValue;
end;

procedure TEvCreatePayrollTask.SetTcFileFormat(const AValue: TCFileFormatOption);
begin
  FTcFileFormat := AValue;
end;

procedure TEvCreatePayrollTask.SetTcFourDigitYear(const AValue: Boolean);
begin
  FTcFourDigitYear := AValue;
end;

procedure TEvCreatePayrollTask.SetTcImportSource(const AValue: string);
begin
  FTcImportSource := AValue;
end;

procedure TEvCreatePayrollTask.SetTcImportSourceFile(const AValue: IevDualStream);
begin
  FTcImportSourceFile := AValue;
end;

procedure TEvCreatePayrollTask.SetTcLookupOption(const AValue: TCImportLookupOption);
begin
  FTcLookupOption := AValue;
end;

procedure TEvCreatePayrollTask.SetTcUseEmployeePayRates(const AValue: Boolean);
begin
  FTcUseEmployeePayRates := AValue;
end;

procedure TEvCreatePayrollTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FAutoCommit);
  AStream.WriteBoolean(FTcAutoImportJobCodes);
  AStream.WriteString(FBatchFreq);
  AStream.WriteInteger(FBlock401K);
  AStream.WriteInteger(FBlockACH);
  AStream.WriteInteger(FBlockAgencies);
  AStream.WriteInteger(FBlockBilling);
  AStream.WriteInteger(FBlockChecks);
  AStream.WriteInteger(FBlockTaxDeposits);
  AStream.WriteInteger(FBlockTimeOff);
  AStream.WriteString(FCheckComments);
  AStream.WriteDouble(FCheckDate);
  AStream.WriteInteger(FCheckDateReplace);
  AStream.WriteString(FCheckFreq);
  AStream.WriteInteger(FCLNbr);
  AStream.WriteInteger(FCONbr);
  AStream.WriteString(FEEList);
  AStream.WriteInteger(FLoadDefaults);
  AStream.WriteInteger(FPayHours);
  AStream.WriteInteger(FPaySalary);
  AStream.WriteDouble(FPeriodBeginDate);
  AStream.WriteDouble(FPeriodEndDate);
  AStream.WriteInteger(FPRFilterNbr);
  AStream.WriteString(FPrList);
  AStream.WriteInteger(FPRTemplateNbr);
  AStream.WriteInteger(FSalaryOrHourly);
  AStream.WriteInteger(Ord(FTcDBDTOption));
  AStream.WriteInteger(Ord(FTcFileFormat));
  AStream.WriteBoolean(FTcFourDigitYear);
  AStream.WriteString(FTcImportSource);
  AStream.WriteStream(FTcImportSourceFile);
  AStream.WriteInteger(Ord(FTcLookupOption));
  AStream.WriteBoolean(FTcUseEmployeePayRates);
  AStream.WriteBoolean(FIncludeTimeOffRequests);
end;

procedure TEvCreatePayrollTask.Phase_CreatePayroll(const AInputRequests: TevTaskRequestList);
var
  R, Params: IisListOfValues;
  i: Integer;
  ImpFile: IEvDualStream;
begin
  inherited;

  StopOnExceptions(AInputRequests);

  for i := 0 to AInputRequests[0].Results.Count - 1 do
  begin
    R := IInterface(AInputRequests[0].Results[i].Value) as IisListOfValues;

    FPrNbr := R.Value['PrNbr'];

    Params := TisListOfValues.Create;

    Params.AddValue('ClientID', FClNbr);
    Params.AddValue('PrBatchNumber', R.Value['PrBatchNbr']);
    Params.AddValue('TwoChecksPerEE', False);
    Params.AddValue('CalcCheckLines', True);
    Params.AddValue('CheckType', CHECK_TYPE2_REGULAR);
    Params.AddValue('Check945Type', CHECK_TYPE_945_NONE);
    Params.AddValue('NumberOfChecks', 1);
    Params.AddValue('SalaryOrHourly', FSalaryOrHourly);
    Params.AddValue('EENumbers', FEEList);

    ImpFile := FTcImportSourceFile;
    if not Assigned(FTcImportSourceFile) and FileExists(R.Value['ImportFileName']) then
      ImpFile := TEvDualStreamHolder.CreateFromFile(R.Value['ImportFileName']);
    Params.AddValue('TcFileIn', ImpFile);

    Params.AddValue('TcLookupOption', FTcLookupOption);
    Params.AddValue('TcDBDTOption', FTcDBDTOption);
    Params.AddValue('TcFourDigitYear', FTcFourDigitYear);
    Params.AddValue('TcFileFormat', FTcFileFormat);
    Params.AddValue('TcAutoImportJobCodes', FTcAutoImportJobCodes);
    Params.AddValue('TcUseEmployeePayRates', FTcUseEmployeePayRates);
    Params.AddValue('UpdateBalance', True);
    Params.AddValue('IncludeTimeOffRequests', FIncludeTimeOffRequests);

    AddRequest('#' + R.Value['CompanyNumber'] + ' ' + R.Value['CheckDate'], 'CreatePayroll', Params, 'AfterCreatePayroll.Merge');
  end;
end;

procedure TEvCreatePayrollTask.Phase_AfterCreatePayroll(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  StopOnExceptions(AInputRequests);
  AddWarning(AInputRequests[0].GetWarnings);

  if FAutoCommit then
  begin
    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', FClNbr);
    Params.AddValue('PrNbr', FPrNbr);

    AddRequest('Finalizing', 'AfterCreatePayroll', Params);
  end;  
end;

procedure TEvCreatePayrollTask.Phase_End(const AInputRequests: TevTaskRequestList);
begin
  FPrList := IntToStr(FCLNbr) + ';' + IntToStr(FPrNbr);
  inherited;
end;

function TEvCreatePayrollTask.Revision: TisRevision;
begin
  Result := 7;
end;

function TEvCreatePayrollTask.GetIncludeTimeOffRequests: Boolean;
begin
  Result := FIncludeTimeOffRequests;
end;

procedure TEvCreatePayrollTask.SetIncludeTimeOffRequests(
  const AValue: Boolean);
begin
  FIncludeTimeOffRequests := AValue;
end;

initialization
  ObjectFactory.Register(TEvCreatePayrollTask);

finalization
  SafeObjectFactoryUnRegister(TEvCreatePayrollTask);

end.
