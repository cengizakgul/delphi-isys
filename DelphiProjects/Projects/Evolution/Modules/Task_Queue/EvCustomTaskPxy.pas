unit EvCustomTaskPxy;

interface

uses Classes, isBaseClasses, EvCommonInterfaces, EvStreamUtils, evProxy, EvTypes,
     isBasicUtils, EvClasses, EvLegacy, isStatistics;

type
  TevCustomTaskProxy = class(TevProxyModule, IevTask, IevTaskExt)
  private
    FID: TisGUID;
    FNbr: Integer;
    FState: TevTaskState;
    FLastUpdate: TDateTime;
    FUserInfo: TevUserInfo;
    FPriority: Integer;
    FMaxThreads: Integer;
    FNotificationEmail: string;
    FSendEmailNotification: Boolean;
    FEmailSendRule: TevEmailSendRule;
    FCaption: String;
    FProgressText: string;
    FLog: String;
    FExceptions: String;
    FWarnings: String;
    FNotes: String;
    FSeenByUser: Boolean;
    FFirstRunAt: TDateTime;
    FDaysStayInQueue: Integer;
    FRunStatistics: IisStatEvent;
  protected
    function   Revision: TisRevision; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure  SetAccountInfo(const ADomain, AUser: String);

    function   GetID: TisGUID;
    procedure  SetID(const AValue: TisGUID);
    function   GetNbr: Integer;
    function   GetTaskType: String;
    function   GetUser: String;
    function   GetDomain: String;
    function   GetPriority: Integer;
    procedure  SetPriority(const AValue: Integer);
    function   GetMaxThreads: Integer;
    procedure  SetMaxThreads(const AValue: Integer);
    function   GetNotificationEmail: string;
    procedure  SetNotificationEmail(const AValue: string);
    function   GetSendEmailNotification: Boolean;
    procedure  SetSendEmailNotification(const AValue: Boolean);
    function   GetEmailSendRule: TevEmailSendRule;
    procedure  SetEmailSendRule(const AValue: TevEmailSendRule);
    function   GetLastUpdate: TDateTime;
    function   GetState: TevTaskState;
    function   GetCaption: String;
    procedure  SetCaption(const AValue: string);
    function   GetProgressText: string;
    function   GetLog: String;
    function   GetExceptions: String;
    function   GetWarnings: String;
    function   GetNotes: String;
    function   GetSeenByUser: Boolean;
    procedure  SetSeenByUser(const AValue: Boolean);
    function   GetDaysToStayInQueue: Integer;
    function   GetFirstRunAt: TDateTime;
    function   GetRunStatistics: IisStatEvent;    
    procedure  AddException(const AText: string);
    procedure  AddWarning(const AText: string);
    procedure  AddNotes(const AText: string);
  end;


  TEvPrintableTaskProxy = class(TevCustomTaskProxy, IevPrintableTask)
  private
    FDestination: TrwReportDestination;
  protected
    FReportResults: IevDualStream;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function   GetDestination: TrwReportDestination;
    procedure  SetDestination(const AValue: TrwReportDestination);
    function   ReportResults: IevDualStream;
  end;


implementation

{ TevCustomTaskProxy }

function TevCustomTaskProxy.GetState: TevTaskState;
begin
  Result := FState;
end;


function TevCustomTaskProxy.GetCaption: String;
begin
  Result := FCaption;
end;

function TevCustomTaskProxy.GetDaysToStayInQueue: Integer;
begin
  Result := FDaysStayInQueue;
end;

function TevCustomTaskProxy.GetExceptions: String;
begin
  Result := FExceptions;
end;

function TevCustomTaskProxy.GetLog: String;
begin
  Result := FLog;
end;

function TevCustomTaskProxy.GetNotificationEmail: string;
begin
  Result := FNotificationEmail;
end;

function TevCustomTaskProxy.GetPriority: Integer;
begin
  Result := FPriority;
end;

function TevCustomTaskProxy.GetSendEmailNotification: Boolean;
begin
  Result := FSendEmailNotification;
end;

function TevCustomTaskProxy.GetID: TisGUID;
begin
  Result := FID;
end;

function TevCustomTaskProxy.GetProgressText: string;
begin
  Result := FProgressText;
end;

procedure TevCustomTaskProxy.SetNotificationEmail(const AValue: string);
begin
  FNotificationEmail := AValue;
end;

procedure TevCustomTaskProxy.SetPriority(const AValue: Integer);
begin
  FPriority := AValue;
end;

procedure TevCustomTaskProxy.SetSendEmailNotification(const AValue: Boolean);
begin
  FSendEmailNotification := AValue;
end;

function TevCustomTaskProxy.GetUser: String;
begin
  Result := FUserInfo.UserName;
end;

function TevCustomTaskProxy.GetWarnings: String;
begin
  Result := FWarnings;
end;


procedure TevCustomTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  Header: IevTaskInfo;
  s: IisStream;
begin
  inherited;

  Header := TevTaskInfo.Create;
  (Header as IisInterfacedObject).ReadFromStream(AStream);

  CheckCondition(GetTypeID = Header.TaskType, 'Data is not compatible');
  FID := Header.ID;
  FNbr := Header.Nbr;

  FUserInfo.Domain := Header.Domain;
  FUserInfo.UserName := Header.User;

  FState := Header.State;
  FLastUpdate := Header.LastUpdate;
  FPriority := Header.Priority;
  FNotificationEmail := Header.NotificationEmail;
  FSendEmailNotification := Header.SendEmailNotification;
  FEmailSendRule := Header.EmailSendRule;
  FCaption := Header.Caption;
  FProgressText := Header.ProgressText;
  FSeenByUser := Header.SeenByUser;
  FDaysStayInQueue := Header.GetDaysToStayInQueue;
  FFirstRunAt := Header.FirstRunAt;

  if ARevision >= 1 then
    FMaxThreads := AStream.ReadInteger;

  if ARevision < 2 then
    AStream.ReadShortString; // skip password

  FLog := AStream.ReadString;
  FNotes := AStream.ReadString;  
  FExceptions := AStream.ReadString;
  FWarnings := AStream.ReadString;

  if ARevision >= 3 then
  begin
    s := AStream.ReadStream(nil, True);
    if Assigned(s) then
    begin
      FRunStatistics := ObjectFactory.CreateInstanceOf('StatEvt') as IisStatEvent;
      FRunStatistics.AsStream := s;
    end
    else
      FRunStatistics := nil;
  end
  else
    FRunStatistics := nil;
end;

procedure TevCustomTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  Header: IisInterfacedObject;
  s: IisStream;
begin
  inherited;
  Header := TevTaskInfo.CreateFromTask(Self);
  Header.WriteToStream(AStream);

  AStream.WriteInteger(FMaxThreads);
  AStream.WriteString(FLog);
  AStream.WriteString(FNotes);
  AStream.WriteString(FExceptions);
  AStream.WriteString(FWarnings);

  if Assigned(FRunStatistics) then
    s := FRunStatistics.AsStream
  else
    s := nil;  
  AStream.WriteStream(s);
end;

function TevCustomTaskProxy.GetTaskType: String;
begin
  Result := GetTypeID;
end;

procedure TevCustomTaskProxy.AddException(const AText: string);
begin
  if AText <> '' then
    FExceptions := FExceptions + #13 + AText;
end;

procedure TevCustomTaskProxy.AddWarning(const AText: string);
begin
  if AText <> '' then
    FWarnings := FWarnings + #13 + AText;
end;


function TevCustomTaskProxy.GetDomain: String;
begin
  Result := FUserInfo.Domain;
end;

function TevCustomTaskProxy.GetNbr: Integer;
begin
  Result := FNbr;
end;

function TevCustomTaskProxy.GetNotes: String;
begin
  Result := FNotes;
end;

procedure TevCustomTaskProxy.AddNotes(const AText: string);
begin
  if AText <> '' then
    FNotes := FNotes + #13 + AText;
end;

function TevCustomTaskProxy.GetLastUpdate: TDateTime;
begin
  Result := FLastUpdate;
end;

procedure TevCustomTaskProxy.SetCaption(const AValue: string);
begin
  FCaption := AValue;
end;

function TevCustomTaskProxy.GetSeenByUser: Boolean;
begin
  Result := FSeenByUser;
end;

procedure TevCustomTaskProxy.SetSeenByUser(const AValue: Boolean);
begin
  FSeenByUser := AValue;
end;

function TevCustomTaskProxy.GetFirstRunAt: TDateTime;
begin
  Result := FFirstRunAt;
end;

procedure TevCustomTaskProxy.SetID(const AValue: TisGUID);
begin
  FID := AValue;
end;


function TevCustomTaskProxy.GetEmailSendRule: TevEmailSendRule;
begin
  Result := FEmailSendRule;
end;

procedure TevCustomTaskProxy.SetEmailSendRule(const AValue: TevEmailSendRule);
begin
  FEmailSendRule := AValue;
end;

procedure TevCustomTaskProxy.SetAccountInfo(const ADomain, AUser: String);
begin
  //todo Only for Lincoln! Needs to be removed after.
  FUserInfo.Domain := ADomain;
  FUserInfo.UserName := AUser;
end;

function TevCustomTaskProxy.GetMaxThreads: Integer;
begin
  Result := FMaxThreads;
end;

procedure TevCustomTaskProxy.SetMaxThreads(const AValue: Integer);
begin
  FMaxThreads := AValue;
end;

function TevCustomTaskProxy.Revision: TisRevision;
begin
  Result := 3;
end;

function TevCustomTaskProxy.GetRunStatistics: IisStatEvent;
begin
  Result := FRunStatistics;
end;

{ TEvPrintableTaskProxy }

function TEvPrintableTaskProxy.GetDestination: TrwReportDestination;
begin
  Result := FDestination;
end;

procedure TEvPrintableTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FDestination := TrwReportDestination(AStream.ReadByte);
  FReportResults := AStream.ReadStream(nil, True);
end;

function TEvPrintableTaskProxy.ReportResults: IevDualStream;
begin
  Result := FReportResults;
end;

procedure TEvPrintableTaskProxy.SetDestination(const AValue: TrwReportDestination);
begin
  FDestination := AValue;
end;

procedure TEvPrintableTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteByte(Ord(FDestination));
  AStream.WriteStream(FReportResults);
end;

end.

