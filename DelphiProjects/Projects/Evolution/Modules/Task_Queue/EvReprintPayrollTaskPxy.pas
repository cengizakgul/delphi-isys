unit EvReprintPayrollTaskPxy;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTaskPxy, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils;

implementation

type
  TEvReprintPayrollTaskProxy = class(TEvPrintableTaskProxy, IevReprintPayrollTask)
  private
    FClientID: Integer;
    FPrNbr: Integer;
    FCheckDate: TDateTime;
    FPrintReports: Boolean;
    FPrintInvoices: Boolean;
    FCurrentTOA: Boolean;
    FPayrollChecks: String;
    FAgencyChecks: String;
    FTaxChecks: String;
    FBillingChecks: String;
    FCheckForm: String;
    FMiscCheckForm: String;
    FSkipCheckStubCheck: Boolean;
    FDontPrintBankInfo: Boolean;
    FHideBackground: Boolean;
    FDontUseVMRSettings: Boolean;
    FPrReprintHistoryNBR: Integer;
    FReason: String;
  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetClientID: Integer;
    procedure SetClientID(const AValue: Integer);
    function  GetPrNbr: Integer;
    procedure SetPrNbr(const AValue: Integer);
    function  GetCheckDate: TDateTime;
    procedure SetCheckDate(const AValue: TDateTime);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetPrintInvoices: Boolean;
    procedure SetPrintInvoices(const AValue: Boolean);
    function  GetCurrentTOA: Boolean;
    procedure SetCurrentTOA(const AValue: Boolean);
    function  GetPayrollChecks: String;
    procedure SetPayrollChecks(const AValue: String);
    function  GetAgencyChecks: String;
    procedure SetAgencyChecks(const AValue: String);
    function  GetTaxChecks: String;
    procedure SetTaxChecks(const AValue: String);
    function  GetBillingChecks: String;
    procedure SetBillingChecks(const AValue: String);
    function  GetCheckForm: String;
    procedure SetCheckForm(const AValue: String);
    function  GetMiscCheckForm: String;
    procedure SetMiscCheckForm(const AValue: String);
    function  GetSkipCheckStubCheck: Boolean;
    procedure SetSkipCheckStubCheck(const AValue: Boolean);
    function  GetDontPrintBankInfo: Boolean;
    procedure SetDontPrintBankInfo(const AValue: Boolean);
    function  GetHideBackground: Boolean;
    procedure SetHideBackground(const AValue: Boolean);
    function  GetDontUseVMRSettings: Boolean;
    procedure SetDontUseVMRSettings(const AValue: Boolean);
    function  GetPrReprintHistoryNBR: Integer;
    procedure SetPrReprintHistoryNBR(const AValue: Integer);
    function  GetReason: String;
    procedure SetReason(const AValue: String);

  public
    class function GetTypeID: String; override;
  end;


{ TEvReprintPayrollTaskProxy }

function TEvReprintPayrollTaskProxy.GetAgencyChecks: String;
begin
  Result := FAgencyChecks;
end;

function TEvReprintPayrollTaskProxy.GetBillingChecks: String;
begin
  Result := FBillingChecks;
end;

function TEvReprintPayrollTaskProxy.GetCheckDate: TDateTime;
begin
  Result := FCheckDate;
end;

function TEvReprintPayrollTaskProxy.GetCheckForm: String;
begin
  Result := FCheckForm;
end;

function TEvReprintPayrollTaskProxy.GetClientID: Integer;
begin
  Result := FClientID;
end;

function TEvReprintPayrollTaskProxy.GetCurrentTOA: Boolean;
begin
  Result := FCurrentTOA;
end;

function TEvReprintPayrollTaskProxy.GetMiscCheckForm: String;
begin
  Result := FMiscCheckForm;
end;

function TEvReprintPayrollTaskProxy.GetPayrollChecks: String;
begin
  Result := FPayrollChecks;
end;

function TEvReprintPayrollTaskProxy.GetPrintInvoices: Boolean;
begin
  Result := FPrintInvoices;
end;

function TEvReprintPayrollTaskProxy.GetPrintReports: Boolean;
begin
  Result := FPrintReports;
end;

function TEvReprintPayrollTaskProxy.GetPrNbr: Integer;
begin
  Result := FPrNbr;
end;

function TEvReprintPayrollTaskProxy.GetSkipCheckStubCheck: Boolean;
begin
  Result := FSkipCheckStubCheck;
end;

function TEvReprintPayrollTaskProxy.GetDontPrintBankInfo: Boolean;
begin
  Result := FDontPrintBankInfo;
end;

function TEvReprintPayrollTaskProxy.GetDontUseVMRsettings: Boolean;
begin
  Result := FDontUseVMRSettings;
end;

function TEvReprintPayrollTaskProxy.GetTaxChecks: String;
begin
  Result := FTaxChecks;
end;

class function TEvReprintPayrollTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_REPRINT_PAYROLL;
end;

procedure TEvReprintPayrollTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClientID := AStream.ReadInteger;
  FPrNbr := AStream.ReadInteger;
  FCheckDate := AStream.ReadDouble;
  FPrintReports := AStream.ReadBoolean;
  FPrintInvoices := AStream.ReadBoolean;
  FCurrentTOA := AStream.ReadBoolean;
  FPayrollChecks := AStream.ReadString;
  FAgencyChecks := AStream.ReadString;
  FTaxChecks := AStream.ReadString;
  FBillingChecks := AStream.ReadString;
  FCheckForm := AStream.ReadString;
  FMiscCheckForm := AStream.ReadString;
  FSkipCheckStubCheck := AStream.ReadBoolean;
  FDontPrintBankInfo := AStream.ReadBoolean;
  FHideBackground := AStream.ReadBoolean;
  FDontUseVMRSettings := AStream.ReadBoolean;
  FPrReprintHistoryNBR := AStream.ReadInteger;
  FReason := AStream.ReadString;
end;

procedure TEvReprintPayrollTaskProxy.SetAgencyChecks(const AValue: String);
begin
  FAgencyChecks := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetBillingChecks(const AValue: String);
begin
  FBillingChecks := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetCheckDate(const AValue: TDateTime);
begin
  FCheckDate := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetCheckForm(const AValue: String);
begin
  FCheckForm := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetClientID(const AValue: Integer);
begin
  FClientID := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetCurrentTOA(const AValue: Boolean);
begin
  FCurrentTOA := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetMiscCheckForm(const AValue: String);
begin
  FMiscCheckForm := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetPayrollChecks(const AValue: String);
begin
  FPayrollChecks := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetPrintInvoices(const AValue: Boolean);
begin
  FPrintInvoices := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetPrintReports(const AValue: Boolean);
begin
  FPrintReports := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetPrNbr(const AValue: Integer);
begin
  FPrNbr := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetSkipCheckStubCheck(const AValue: Boolean);
begin
  FSkipCheckStubCheck := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetDontPrintBankInfo(const AValue: Boolean);
begin
  FDontPrintBankInfo := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetDontUseVMRSettings(const AValue: Boolean);
begin
  FDontUseVMRSettings := AValue;
end;

procedure TEvReprintPayrollTaskProxy.SetTaxChecks(const AValue: String);
begin
  FTaxChecks := AValue;
end;

procedure TEvReprintPayrollTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClientID);
  AStream.WriteInteger(FPrNbr);
  AStream.WriteDouble(FCheckDate);
  AStream.WriteBoolean(FPrintReports);
  AStream.WriteBoolean(FPrintInvoices);
  AStream.WriteBoolean(FCurrentTOA);
  AStream.WriteString(FPayrollChecks);
  AStream.WriteString(FAgencyChecks);
  AStream.WriteString(FTaxChecks);
  AStream.WriteString(FBillingChecks);
  AStream.WriteString(FCheckForm);
  AStream.WriteString(FMiscCheckForm);
  AStream.WriteBoolean(FSkipCheckStubCheck);
  AStream.WriteBoolean(FDontPrintBankInfo);
  AStream.WriteBoolean(FHideBackground);
  AStream.WriteBoolean(FDontUseVMRSettings);
  AStream.WriteInteger(FPrReprintHistoryNBR);
  AStream.WriteString(FReason);
end;

function TEvReprintPayrollTaskProxy.GetHideBackground: Boolean;
begin
  Result := FHideBackground;
end;

procedure TEvReprintPayrollTaskProxy.SetHideBackground(
  const AValue: Boolean);
begin
  FHideBackground := AValue;
end;

function TEvReprintPayrollTaskProxy.GetPrReprintHistoryNBR: Integer;
begin
  Result := FPrReprintHistoryNBR;
end;

procedure TEvReprintPayrollTaskProxy.SetPrReprintHistoryNBR(
  const AValue: Integer);
begin
  FPrReprintHistoryNBR := AValue;
end;

function TEvReprintPayrollTaskProxy.GetReason: String;
begin
  Result := FReason;
end;

procedure TEvReprintPayrollTaskProxy.SetReason(const AValue: String);
begin
  FReason := AValue;
end;

initialization
  ObjectFactory.Register(TEvReprintPayrollTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvReprintPayrollTaskProxy);

end.
