unit EvPAMCTask;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTask, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, EvClasses;

implementation

type
  TevPAMCTask = class(TEvPrintableTask, IevPAMCTask)
  private
    FClNbr: Integer;
    FCoNbr: Integer;
    FMonthEndDate: TDateTime;
    FMinTax: Double;
    FAutoProcess: Boolean;
    FEECustomNbr: String;
    FPrintReports: Boolean;
    FFilter: IevQECTaskFilter;
    FResults: IevQECTaskResult;
    FNextFilterItem: Integer;
  protected
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure DoStoreTaskState(const AStream: IevDualStream); override;
    procedure DoRestoreTaskState(const AStream: IevDualStream); override;

    function  GetClNbr: Integer;
    procedure SetClNbr(const AValue: Integer);
    function  GetCoNbr: Integer;
    procedure SetCoNbr(const AValue: Integer);
    function  GetMonthEndDate: TDateTime;
    procedure SetMonthEndDate(const AValue: TDateTime);
    function  GetMinTax: Double;
    procedure SetMinTax(const AValue: Double);
    function  GetAutoProcess: Boolean;
    procedure SetAutoProcess(const AValue: Boolean);
    function  GetEECustomNbr: String;
    procedure SetEECustomNbr(const AValue: String);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetFilter: IevQECTaskFilter;
    function  GetResults: IevQECTaskResult;    

    procedure RegisterTaskPhases; override;
    procedure Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure Phase_RunReports(const AInputRequests: TevTaskRequestList);

    procedure  Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);
    function   Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues; override;


  public
    class function GetTypeID: String; override;
  end;


{ TevPAMCTask }

procedure TevPAMCTask.DoOnConstruction;
begin
  inherited;
  FFilter := TevQECTaskFilter.Create;
  SetDestination(rdtRemotePrinter);
end;

procedure TevPAMCTask.DoRestoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  FNextFilterItem := AStream.ReadInteger;


  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevQECTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;

end;

procedure TevPAMCTask.DoStoreTaskState(const AStream: IevDualStream);
var
  S: IEvDualStream;
begin
  inherited;

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  AStream.WriteInteger(FNextFilterItem);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);

end;

function TevPAMCTask.GetAutoProcess: Boolean;
begin
  Result := FAutoProcess;
end;

function TevPAMCTask.GetClNbr: Integer;
begin
  Result := FClNbr;
end;

function TevPAMCTask.GetCoNbr: Integer;
begin
  Result := FCoNbr;
end;

function TevPAMCTask.GetEECustomNbr: String;
begin
  Result := FEECustomNbr;
end;

function TevPAMCTask.GetMinTax: Double;
begin
  Result := FMinTax;
end;

function TevPAMCTask.GetPrintReports: Boolean;
begin
  Result := FPrintReports;
end;

function TevPAMCTask.GetMonthEndDate: TDateTime;
begin
  Result := FMonthEndDate;
end;

class function TevPAMCTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PAMC;
end;

procedure TevPAMCTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
begin
  inherited;
  FNextFilterItem := 0;
  FResults := TevQECTaskResult.Create;
  Phase_GoToNextCompany(AInputRequests);

end;

procedure TevPAMCTask.Phase_GoToNextCompany(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
  Flt: IevQECTaskFilterItem;
  i, n: Integer;
  Res: IevQECTaskResultItem;
  S: IevDualStream;
begin

  if Length(AInputRequests) = 1 then
    if AnsiSameText(AInputRequests[0].TaskRoutineName, 'PAMonthEndCleanup') then
    begin
      AddLogEvent('Phase "RunReports" activated');
      Phase_RunReports(AInputRequests) // processing is done, go to run report
    end
    else
      if FNextFilterItem > 0 then          // collect results
        for i := Low(AInputRequests) to High(AInputRequests) do
        begin
          if AInputRequests[0].GetExceptions = '' then
          begin
            Res := FResults.Add;

            S := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IevDualStream;
            if Assigned(S) then
              Res.SetQECReports(S);
          end;
        end;

  // continue to add as many requests as GetMaxThreads specifies
  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    if FNextFilterItem >= FFilter.Count then
      break;

     Flt := FFilter[FNextFilterItem];

     Params := TisListOfValues.Create;
     Params.AddValue('ClientID', Flt.ClNbr);
     Params.AddValue('CoNbr', Flt.CoNbr);

     Params.AddValue('MonthEndDate', FMonthEndDate);
     Params.AddValue('MinTax', FMinTax);
     Params.AddValue('AutoProcess', FAutoProcess);
     Params.AddValue('EECustomNbr', FEECustomNbr);
     Params.AddValue('PrintReports', FPrintReports);
     Params.AddValue('LockType', Ord(rlDoNotLock));

     AddRequest(Flt.Caption, 'PAMonthEndCleanup', Params, 'GoToNextCompany.Fork',
       GF_CL_PAYROLL_OPERATION + '.' + IntToStr(Flt.ClNbr) + ',' +
       GF_CO_PAYROLL_OPERATION + '.' + IntToStr(Flt.ClNbr) + '_' + IntToStr(Flt.CoNbr));

    Inc(FNextFilterItem);
  end;

  // Are all companies processed?
  if (FNextFilterItem >= FFilter.Count) and AllRequestsAreFinished then
  begin
    Params := TisListOfValues.Create;
    AddRequest('Process Report Results', 'ProcessReportResults', Params, 'PostProcessReportResults.Merge');
  end;

end;

procedure TevPAMCTask.Phase_RunReports(const AInputRequests: TevTaskRequestList);
var
  i : Integer;
  Params: IisListOfValues;
  R: IevTaskRequest;
begin

  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
   R := AInputRequests[i];

   if R.GetExceptions = '' then
   begin
    // If there are no errors
    AddWarning(R.GetWarnings);

    if not R.Results.Value[PARAM_RESULT] then
    begin
      if FAutoProcess then
        AddWarning('Month End Cleanup payroll was not sent to queue due to pending EE tax adjustments or previous Quarter End Cleanup was not finished.');
    end
    else
      AddNotes('Month End Cleanup was successful or was not necessary.');

      if FPrintReports and (R.Results.Value['PayrollNumber'] <> 0) then
      begin
        Params := TisListOfValues.Create;
//        Params.AddValue('ClientID', FClNbr);
        Params.AddValue('ClientID', R.Params.Value['ClientID']);

        Params.AddValue('PrNbr', R.Results.Value['PayrollNumber']);
        Params.AddValue('PrintChecks', False);
        Params.AddValue('UseSbMiskCheckForm', False);

        AddRequest('Running reports for QEC - CLNbr#' + IntToStr(R.Params.Value['ClientID']) + '- CONbr#' +
                                               IntToStr(R.Params.Value['CoNbr']),
                     'PrintPayroll', Params, 'GoToNextCompany.Fork');
      end;
    end
   else

   begin
     AddException(R.GetExceptions);
     AddWarning(R.GetWarnings);
   end;
  end;
end;

function TevPAMCTask.Call_ProcessReportResults(const AParams: IisListOfValues): IisListOfValues;
var
  i: Integer;
begin
  AParams.Clear;

  for i := 0 to FResults.Count - 1 do
    if (FResults[i].QECReports <> nil) and (FResults[i].QECReports.Size > 0) then
      AParams.AddValue('Report' + IntToStr(i), FResults[i].QECReports);

  Result := inherited Call_ProcessReportResults(AParams);
end;

procedure TevPAMCTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;
  FClNbr := AStream.ReadInteger;
  FCoNbr := AStream.ReadInteger;
  FMonthEndDate := AStream.ReadDouble;
  FMinTax := AStream.ReadDouble;
  FAutoProcess := AStream.ReadBoolean;
  FEECustomNbr := AStream.ReadString;
  FPrintReports := AStream.ReadBoolean;

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TevQECTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TevPAMCTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('RunReports', Phase_RunReports);
  RegisterTaskPhase('GoToNextCompany', Phase_GoToNextCompany);
end;

procedure TevPAMCTask.SetAutoProcess(const AValue: Boolean);
begin
  FAutoProcess := AValue;
end;

procedure TevPAMCTask.SetClNbr(const AValue: Integer);
begin
  FClNbr := AValue;
end;

procedure TevPAMCTask.SetCoNbr(const AValue: Integer);
begin
  FCoNbr := AValue;
end;

procedure TevPAMCTask.SetEECustomNbr(const AValue: String);
begin
  FEECustomNbr := AValue;
end;

procedure TevPAMCTask.SetMinTax(const AValue: Double);
begin
  FMinTax := AValue;
end;

procedure TevPAMCTask.SetPrintReports(const AValue: Boolean);
begin
  FPrintReports := AValue;
end;

procedure TevPAMCTask.SetMonthEndDate(const AValue: TDateTime);
begin
  FMonthEndDate := AValue;
end;

function TevPAMCTask.GetFilter: IevQECTaskFilter;
begin
  Result := FFilter;
end;

function TevPAMCTask.GetResults: IevQECTaskResult;
begin
  Result := FResults;
end;

procedure TevPAMCTask.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteInteger(FClNbr);
  AStream.WriteInteger(FCoNbr);
  AStream.WriteDouble(FMonthEndDate);
  AStream.WriteDouble(FMinTax);
  AStream.WriteBoolean(FAutoProcess);
  AStream.WriteString(FEECustomNbr);
  AStream.WriteBoolean(FPrintReports);

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;


initialization
  ObjectFactory.Register(TevPAMCTask);

finalization
  SafeObjectFactoryUnRegister(TevPAMCTask);

end.
