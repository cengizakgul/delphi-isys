unit EvProcessPrenoteACHTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses;

implementation

type
  TEvProcessPrenoteACHTaskProxy = class(TEvCustomTask, IevProcessPrenoteACHTask)
  private
    FACHOptions: IevACHOptions;
    FPrenoteCLAccounts: Boolean;
    FACHDataStream: IEvDualStream;
    FAchFile: IEvDualStream;
    FAchRegularReport: IEvDualStream;
    FAchDetailedReport: IEvDualStream;
    FAchSave: IEvDualStream;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetACHOptions: IevACHOptions;
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetPrenoteCLAccounts: Boolean;
    procedure SetPrenoteCLAccounts(const AValue: Boolean);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;

  public
    class function GetTypeID: String; override;
  end;

{ TEvProcessPrenoteACHTaskProxy }

procedure TEvProcessPrenoteACHTaskProxy .DoOnConstruction;
begin
  inherited;
  FACHOptions := TevACHOptions.Create;
  FACHDataStream := TEvDualStreamHolder.Create;
  FDaysStayInQueue := 10;
end;

function TEvProcessPrenoteACHTaskProxy.GetACHDataStream: IEvDualStream;
begin
  Result := FACHDataStream;
end;

function TEvProcessPrenoteACHTaskProxy.GetAchDetailedReport: IEvDualStream;
begin
  Result := FAchDetailedReport;
end;

function TEvProcessPrenoteACHTaskProxy.GetAchFile: IEvDualStream;
begin
  Result := FAchFile;
end;

function TEvProcessPrenoteACHTaskProxy.GetAchRegularReport: IEvDualStream;
begin
  Result := FAchRegularReport;
end;

function TEvProcessPrenoteACHTaskProxy.GetAchSave: IEvDualStream;
begin
  Result := FAchSave;
end;

class function TEvProcessPrenoteACHTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_PRENOTE_ACH;
end;

procedure TEvProcessPrenoteACHTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).ReadFromStream(AStream);
  FACHDataStream := AStream.ReadStream(nil);
  FPrenoteCLAccounts := AStream.ReadBoolean;
  FAchFile := AStream.ReadStream(nil, True);
  FAchRegularReport := AStream.ReadStream(nil, True);
  FAchDetailedReport := AStream.ReadStream(nil, True);
  FAchSave := AStream.ReadStream(nil, True);
end;

procedure TEvProcessPrenoteACHTaskProxy.SetACHDataStream(const AValue: IEvDualStream);
begin
  FACHDataStream := AValue;
end;

procedure TEvProcessPrenoteACHTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteStream(FACHDataStream);
  AStream.WriteBoolean(FPrenoteCLAccounts);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchRegularReport);
  AStream.WriteStream(FAchDetailedReport);
  AStream.WriteStream(FAchSave);
end;

function TEvProcessPrenoteACHTaskProxy.GetPrenoteCLAccounts: Boolean;
begin
  Result := FPrenoteCLAccounts;
end;

procedure TEvProcessPrenoteACHTaskProxy.SetPrenoteCLAccounts(
  const AValue: Boolean);
begin
  FPrenoteCLAccounts := AValue;
end;

function TEvProcessPrenoteACHTaskProxy.GetACHOptions: IevACHOptions;
begin
  Result := FACHOptions;
end;

initialization
  ObjectFactory.Register(TEvProcessPrenoteACHTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvProcessPrenoteACHTaskProxy);

end.
