unit EvEvoXImportTask;

interface

uses Classes, Windows, SysUtils, isBaseClasses, isBasicUtils, EvCommonInterfaces, EvContext, EvMainboard, EvStreamUtils,
     EvConsts, EvClasses, EvLegacy, EvDataSet, EvTypes, IsErrorUtils, EvAsyncCall, isVCLBugFix,
     EvBasicUtils, EvUtils, EvCustomTask;

implementation

uses
   EvExchangeMapFile, EvExchangePackage, EvExchangeResultLog;

type
  TEvEvoXImportTask = class(TEvCustomTask, IevEvoXImportTask)
  private
    FInputDatasets: IisListOfValues;
    FMapFile: IEvExchangeMapFile;
    FPackage: IevExchangePackage;
    FResults: IisListOfValues;
    FEffectiveDate: TDateTime;
    FOptions: IisStringList;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetInputDatasets: IisListOfValues;
    procedure SetInputDataSets(const AInputDataSets: IisListOfValues);
    function  GetMapFile: IEvExchangeMapFile;
    procedure SetMapFile(const AMapFile: IEvExchangeMapFile);
    function  GetPackage: IevExchangePackage;
    procedure SetPackage(const APackage: IevExchangePackage);
    function  GetEffectiveDate: TDateTime;
    procedure SetEffectiveDate(const AEffectiveDate: TDateTime);
    function  GetOptions: IisStringList;
    procedure SetOptions(const AOptions: IisStringList);
    function  GetResults: IisListofValues;

    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;

{ TEvEvoXImportTask }

procedure TEvEvoXImportTask.DoOnConstruction;
begin
  inherited;
  FEffectiveDate := 0;
  FInputDatasets := TisListOfValues.Create;
  FResults := TisListOfValues.Create;
  FMapFile := TEvExchangeMapFile.Create;
  FPackage := TevExchangePackage.Create;
  FOptions := TisStringList.Create;
end;

function TEvEvoXImportTask.GetEffectiveDate: TDateTime;
begin
  Result := FEffectiveDate;
end;

function TEvEvoXImportTask.GetInputDatasets: IisListOfValues;
begin
  Result := FInputDatasets;
end;

function TEvEvoXImportTask.GetMapFile: IEvExchangeMapFile;
begin
  Result := FMapFile;
end;

function TEvEvoXImportTask.GetOptions: IisStringList;
begin
  Result := FOptions;
end;

function TEvEvoXImportTask.GetPackage: IevExchangePackage;
begin
  Result := FPackage;
end;

function TEvEvoXImportTask.GetResults: IisListofValues;
begin
  Result := FResults;
end;

class function TEvEvoXImportTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_EVOX_IMPORT;
end;

procedure TEvEvoXImportTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
  i: integer;
begin
  inherited;

  for i := 0 to FInputDatasets.Count - 1 do
  begin
    Params := TisListOfValues.Create;
    Params.AddValue('Name', FInputDatasets.Values[i].Name);
    Params.AddValue('InputData', IInterface(FInputDatasets.Values[i].Value) as IevDataSet);
    Params.AddValue('MapFile', FMapFile.GetClone);
    Params.AddValue('Package', FPackage.GetClone);
    Params.AddValue('Options', FOptions);
    Params.AddValue('CustomCoNbr', '');
    Params.AddValue('EffectiveDate', FEffectiveDate);

    AddRequest('EvoX import of "' + FInputDatasets.Values[i].Name + '" file', 'EvoXEngineImport',  Params);
  end;
end;

procedure TEvEvoXImportTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  i: integer;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    if AInputRequests[i].GetExceptions <> '' then
      AddException('File: "' + AInputRequests[i].Params.Value['Name'] + '". ' + AInputRequests[i].GetExceptions);
    AddWarning(AInputRequests[i].GetWarnings);

    FResults.AddValue(AInputRequests[i].Params.Value['Name'], IInterface(AInputRequests[i].Results.Value[PARAM_RESULT]) as IisListOfValues);
  end;

  inherited;
end;

procedure TEvEvoXImportTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FInputDatasets.ReadFromStream(AStream);
  FMapFile.ReadFromStream(AStream);
  FPackage.ReadFromStream(AStream);
  FEffectiveDate := AStream.ReadDouble;
  FResults.ReadFromStream(AStream);
  FOptions.Text := AStream.ReadString;
end;

procedure TEvEvoXImportTask.SetEffectiveDate(const AEffectiveDate: TDateTime);
begin
  FEffectiveDate := AEffectiveDate;
end;

procedure TEvEvoXImportTask.SetInputDataSets(const AInputDataSets: IisListOfValues);
begin
  CheckCondition(Assigned(AInputDataSets), 'Input Datasets object should be assigned!');
  FInputDatasets := AInputDataSets;
end;

procedure TEvEvoXImportTask.SetMapFile(const AMapFile: IEvExchangeMapFile);
begin
  CheckCondition(Assigned(AMapFile), 'Map file should be assigned!');
  FMapFile := AMapFile;
end;

procedure TEvEvoXImportTask.SetOptions(const AOptions: IisStringList);
begin
  CheckCondition(Assigned(AOptions), 'Options should be assigned');
  FOptions := AOptions;
end;

procedure TEvEvoXImportTask.SetPackage(const APackage: IevExchangePackage);
begin
  CheckCondition(Assigned(APackage), 'Package should be assigned');
  FPackage := APackage;
end;

procedure TEvEvoXImportTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  FInputDatasets.WriteToStream(AStream);
  FMapFile.WriteToStream(AStream);
  FPackage.WriteToStream(AStream);
  AStream.WriteDouble(FEffectiveDate);
  FResults.WriteToStream(AStream);
  AStream.WriteString(FOptions.Text);
end;

initialization
  ObjectFactory.Register(TEvEvoXImportTask);

finalization
  SafeObjectFactoryUnRegister(TEvEvoXImportTask);
end.
