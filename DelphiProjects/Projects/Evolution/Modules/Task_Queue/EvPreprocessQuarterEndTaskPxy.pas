unit EvPreprocessQuarterEndTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;


implementation

type
  TEvPreprocessQuarterEndTaskProxy = class(TEvPrintableTaskProxy, IevPreprocessQuarterEndTask)
  private
    FBegDate: TDateTime;
    FEndDate: TDateTime;
    FQecProduceReports: Boolean;
    FQecAdjustmentLimit: Currency;
    FTaxAdjustmentLimit: Currency;
    FTaxServiceFilter: TTaxServiceFilterType;
    FFilter: IevPreprocessQuarterEndTaskFilter;
    FResults: IevPreprocessQuarterEndTaskResult;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetBegDate: TDateTime;
    function  GetEndDate: TDateTime;
    function  GetFilter: IevPreprocessQuarterEndTaskFilter;
    function  GetQecAdjustmentLimit: Currency;
    function  GetQecProduceReports: Boolean;
    function  GetResults: IevPreprocessQuarterEndTaskResult;
    function  GetTaxAdjustmentLimit: Currency;
    function  GetTaxServiceFilter: TTaxServiceFilterType;
    procedure SetBegDate(const AValue: TDateTime);
    procedure SetEndDate(const AValue: TDateTime);
    procedure SetQecAdjustmentLimit(const AValue: Currency);
    procedure SetQecProduceReports(const AValue: Boolean);
    procedure SetTaxAdjustmentLimit(const AValue: Currency);
    procedure SetTaxServiceFilter(const AValue: TTaxServiceFilterType);
  public
    class function GetTypeID: String; override;
  end;


{ TEvPreprocessQuarterEndTaskProxy }

procedure TEvPreprocessQuarterEndTaskProxy.DoOnConstruction;
begin
  inherited;
  FFilter := TEvPreprocessQuarterEndTaskFilter.Create;
end;

function TEvPreprocessQuarterEndTaskProxy.GetBegDate: TDateTime;
begin
  Result := FBegDate;
end;

function TEvPreprocessQuarterEndTaskProxy.GetEndDate: TDateTime;
begin
  Result := FEndDate;
end;

function TEvPreprocessQuarterEndTaskProxy.GetFilter: IevPreprocessQuarterEndTaskFilter;
begin
  Result := FFilter;
end;

function TEvPreprocessQuarterEndTaskProxy.GetQecAdjustmentLimit: Currency;
begin
  Result := FQecAdjustmentLimit;
end;

function TEvPreprocessQuarterEndTaskProxy.GetQecProduceReports: Boolean;
begin
  Result := FQecProduceReports;
end;

function TEvPreprocessQuarterEndTaskProxy.GetResults: IevPreprocessQuarterEndTaskResult;
begin
  Result := FResults;
end;

function TEvPreprocessQuarterEndTaskProxy.GetTaxAdjustmentLimit: Currency;
begin
  Result := FTaxAdjustmentLimit;
end;

function TEvPreprocessQuarterEndTaskProxy.GetTaxServiceFilter: TTaxServiceFilterType;
begin
  Result := FTaxServiceFilter;
end;

class function TEvPreprocessQuarterEndTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PREPROCESS_QUARTER_END;
end;

procedure TEvPreprocessQuarterEndTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IEvDualStream;
begin
  inherited;

  FBegDate := AStream.ReadDouble;
  FEndDate := AStream.ReadDouble;
  FQecProduceReports := AStream.ReadBoolean;
  FQecAdjustmentLimit := AStream.ReadCurrency;
  FTaxAdjustmentLimit := AStream.ReadCurrency;
  FTaxServiceFilter := TTaxServiceFilterType(AStream.ReadByte);

  S := AStream.ReadStream(nil);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TEvPreprocessQuarterEndTaskResult.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end
  else
    FResults := nil;
end;

procedure TEvPreprocessQuarterEndTaskProxy.SetBegDate(const AValue: TDateTime);
begin
  FBegDate := AValue;
end;

procedure TEvPreprocessQuarterEndTaskProxy.SetEndDate(const AValue: TDateTime);
begin
  FEndDate := AValue;
end;

procedure TEvPreprocessQuarterEndTaskProxy.SetQecAdjustmentLimit(const AValue: Currency);
begin
  FQecAdjustmentLimit := AValue;
end;

procedure TEvPreprocessQuarterEndTaskProxy.SetQecProduceReports(const AValue: Boolean);
begin
  FQecProduceReports := AValue;
end;

procedure TEvPreprocessQuarterEndTaskProxy.SetTaxAdjustmentLimit(const AValue: Currency);
begin
  FTaxAdjustmentLimit := AValue;
end;

procedure TEvPreprocessQuarterEndTaskProxy.SetTaxServiceFilter(const AValue: TTaxServiceFilterType);
begin
  FTaxServiceFilter := AValue;
end;

procedure TEvPreprocessQuarterEndTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IEvDualStream;
begin
  inherited;
  AStream.WriteDouble(FBegDate);
  AStream.WriteDouble(FEndDate);
  AStream.WriteBoolean(FQecProduceReports);
  AStream.WriteCurrency(FQecAdjustmentLimit);
  AStream.WriteCurrency(FTaxAdjustmentLimit);
  AStream.WriteByte(Ord(FTaxServiceFilter));

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;
  AStream.WriteStream(S);
end;

initialization
  ObjectFactory.Register(TEvPreprocessQuarterEndTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvPreprocessQuarterEndTaskProxy);

end.
