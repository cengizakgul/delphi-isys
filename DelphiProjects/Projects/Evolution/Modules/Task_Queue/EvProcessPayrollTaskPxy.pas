unit EvProcessPayrollTaskPxy;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTaskPxy, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils;

implementation

type
  TEvProcessPayrollTaskProxy = class(TEvPrintableTaskProxy, IevProcessPayrollTask)
  private
    FAllPayrolls: Boolean;
    FPayrollAge: TDateTime;
    FPrList: string;
    FPrintChecks: Boolean;
    FPRMessage: string;
    FCoList: string;
    FPrInfo: IisListOfValues;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetAllPayrolls: Boolean;
    function  GetCoList: string;
    function  GetPayrollAge: TDateTime;
    function  GetPrintChecks: Boolean;
    function  GetPrList: string;
    function  GetPRMessage: string;
    procedure SetAllPayrolls(const AValue: Boolean);
    procedure SetCoList(const AValue: string);
    procedure SetPayrollAge(const AValue: TDateTime);
    procedure SetPrintChecks(const AValue: Boolean);
    procedure SetPrList(const AValue: string);
    procedure SetPRMessage(const AValue: string);
  public
    class function GetTypeID: String; override;
  end;


{ TEvProcessPayrollTaskProxy }

procedure TEvProcessPayrollTaskProxy.DoOnConstruction;
begin
  inherited;
  FPrInfo := TisListOfValues.Create;
end;

function TEvProcessPayrollTaskProxy.GetAllPayrolls: Boolean;
begin
  Result := FAllPayrolls;
end;

function TEvProcessPayrollTaskProxy.GetCoList: string;
begin
  Result := FCoList;
end;

function TEvProcessPayrollTaskProxy.GetPayrollAge: TDateTime;
begin
  Result := FPayrollAge;
end;

function TEvProcessPayrollTaskProxy.GetPrintChecks: Boolean;
begin
  Result := FPrintChecks;
end;

function TEvProcessPayrollTaskProxy.GetPrList: string;
begin
  Result := FPrList;
end;

function TEvProcessPayrollTaskProxy.GetPRMessage: string;
begin
  Result := FPRMessage;
end;


class function TEvProcessPayrollTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_PAYROLL;
end;

procedure TEvProcessPayrollTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FAllPayrolls := AStream.ReadBoolean;
  FPayrollAge := AStream.ReadDouble;
  FPrList :=AStream.ReadString;
  FPrintChecks := AStream.ReadBoolean;
  FPRMessage := AStream.ReadString;
  FCoList := AStream.ReadString;
  FPrInfo.ReadFromStream(AStream);
end;

procedure TEvProcessPayrollTaskProxy.SetAllPayrolls(const AValue: Boolean);
begin
  FAllPayrolls := AValue;
end;

procedure TEvProcessPayrollTaskProxy.SetCoList(const AValue: string);
begin
  FCoList := AValue;
end;

procedure TEvProcessPayrollTaskProxy.SetPayrollAge(const AValue: TDateTime);
begin
  FPayrollAge := AValue;
end;

procedure TEvProcessPayrollTaskProxy.SetPrintChecks(const AValue: Boolean);
begin
  FPrintChecks := AValue;
end;

procedure TEvProcessPayrollTaskProxy.SetPrList(const AValue: string);
begin
  FPrList := AValue;
end;

procedure TEvProcessPayrollTaskProxy.SetPRMessage(const AValue: string);
begin
  FPRMessage := AValue;
end;

procedure TEvProcessPayrollTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FAllPayrolls);
  AStream.WriteDouble(FPayrollAge);
  AStream.WriteString(FPrList);
  AStream.WriteBoolean(FPrintChecks);
  AStream.WriteString(FPRMessage);
  AStream.WriteString(FCoList);
  FPrInfo.WriteToStream(AStream);
end;

initialization
  ObjectFactory.Register(TEvProcessPayrollTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvProcessPayrollTaskProxy);

end.
