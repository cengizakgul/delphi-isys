// Proxy Module "Task Queue"
unit pxy_Task_Queue;

interface

uses
  EvTaskQueuePxy,
  EvCustomTaskPxy,
  EvRunReportTaskPxy,
  EvProcessPayrollTaskPxy,
  EvPreprocessPayrollTaskPxy,
  EvReprintPayrollTaskPxy,
  EvProcessTaxPaymentsTaskPxy,
  EvCashManagementTaskPxy,
  EvProcessPrenoteACHTaskPxy,
  EvProcessManualACHTaskPxy,
  EvRebuildTempTablesTaskPxy,
  EvCreatePayrollTaskPxy,
  EvPreprocessQuarterEndTaskPxy,
  EvQECTaskPxy,
  EvPAMCTaskPxy,
  EvRebuildVMRHistoryTaskPxy,
  EvProcessTaxReturnsTaskPxy, 
  EvEvoXImportTaskPxy,
  EvBenefitEnrollmentNotificationsTaskPxy,
  EvACAUpdateTaskPxy;

implementation  

end.
