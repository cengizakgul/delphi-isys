unit EvRebuildTempTablesTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

uses Classes;

type

{ Algorithm




                     [1]
  BEGIN ----> PrepareToRebuild ---->
                                   |
                                   |
                                   |                  --------------<------------------------------
                                   |                  |                                            |
                                   |                  |          [MaxThreads - RequestCount]       |
                                   -----> GoToNextClient.Fork ----------> RebuildClient ---------->
                                                           |
                                                           |
                                                           |
                                                           --------------------------------> END

}


  TEvRebuildTempTablesTask = class(TEvCustomTask, IevRebuildTempTablesTask)
  private
    FFullRebuild: Boolean;
    FUseRange: Boolean;
    FUseNumbers: Boolean;
    FFromClient: Integer;
    FToClient: Integer;
    FClientNumbers: String;

    FClientsRebuilt: Integer;
    FClientsFailed: Integer;
    FClientsToRebuild: IisStringList;
    FTempClients: IisStringList;

  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;
    function   CanExecute: Boolean; override;

    function  GetFullRebuild: Boolean;
    procedure SetFullRebuild(const AValue: Boolean);
    function  GetUseRange: Boolean;
    procedure SetUseRange(const AValue: Boolean);
    function  GetUseNumbers: Boolean;
    procedure SetUseNumbers(const AValue: Boolean);
    function  GetFromClient: Integer;
    procedure SetFromClient(const AValue: Integer);
    function  GetToClient: Integer;
    procedure SetToClient(const AValue: Integer);
    function  GetClientNumbers: String;
    procedure SetClientNumbers(const AValue: String);

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_GoToNextClient(const AInputRequests: TevTaskRequestList);
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;


{ TEvRebuildTempTablesTask }

function TEvRebuildTempTablesTask.CanExecute: Boolean;
var
  DBList: IisStringList;
begin
  if FFullRebuild then
  begin
    DBList := ctx_DBAccess.GetDisabledDBs;
    DBList.CaseSensitive := False;
    Result := (DBList.IndexOf('*') = -1) and (DBList.IndexOf('SYSTEM') = -1) and (DBList.IndexOf('S_BUREAU') = -1);
  end
  else
    Result := inherited CanExecute;
end;

procedure TEvRebuildTempTablesTask.DoOnConstruction;
begin
  inherited;
  FClientsToRebuild := TisStringList.Create;
  FTempClients := TisStringList.Create;
end;

procedure TEvRebuildTempTablesTask.DoRestoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  FClientsRebuilt := AStream.ReadInteger;
  FClientsFailed := AStream.ReadInteger;
  FClientsToRebuild.LoadFromStream(AStream.RealStream);
end;

procedure TEvRebuildTempTablesTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteInteger(FClientsRebuilt);
  AStream.WriteInteger(FClientsFailed);
  FClientsToRebuild.SaveToStream(AStream.RealStream);
end;

function TEvRebuildTempTablesTask.GetClientNumbers: String;
begin
  Result := FClientNumbers;
end;

function TEvRebuildTempTablesTask.GetFromClient: Integer;
begin
  Result := FFromClient;
end;

function TEvRebuildTempTablesTask.GetFullRebuild: Boolean;
begin
  Result := FFullRebuild;
end;

function TEvRebuildTempTablesTask.GetToClient: Integer;
begin
  Result := FToClient;
end;

class function TEvRebuildTempTablesTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_REBUILD_TMP_TABLES;
end;

function TEvRebuildTempTablesTask.GetUseNumbers: Boolean;
begin
  Result := FUseNumbers;
end;

function TEvRebuildTempTablesTask.GetUseRange: Boolean;
begin
  Result := FUseRange;
end;

procedure TEvRebuildTempTablesTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('FullRebuild', FFullRebuild);
  Params.AddValue('UseRange', FUseRange);
  Params.AddValue('UseNumbers', FUseNumbers);
  Params.AddValue('FromClient', FFromClient);
  Params.AddValue('ToClient', FToClient);
  Params.AddValue('ClientNumbers', FClientNumbers);

  AddRequest('Prepare for rebuilding', 'PrepareForRebuildTempTables', Params, 'GoToNextClient.Fork');
end;

procedure TEvRebuildTempTablesTask.Phase_End(const AInputRequests: TevTaskRequestList);
begin
  if (Length(AInputRequests) = 1) and AnsiSameText(AInputRequests[0].TaskRoutineName, 'FinalizeRebuildAllClients') then
  begin
    AddException(AInputRequests[0].GetExceptions);
    AddWarning(AInputRequests[0].GetWarnings);
  end;

  if FFullRebuild and (GetExceptions = '') then
    AddNotes('Full rebuild of temp tables has finished successfully!');

  inherited;
end;

procedure TEvRebuildTempTablesTask.Phase_GoToNextClient(const AInputRequests: TevTaskRequestList);
var
  i, n: Integer;
  Params: IisListOfValues;
  Err: String;
begin
  if (Length(AInputRequests) = 1) and AnsiSameText(AInputRequests[0].TaskRoutineName, 'PrepareForRebuildTempTables') then
  begin
    // For the very first time we need to define client list
    StopOnExceptions(AInputRequests);
    FClientsToRebuild.Text := AInputRequests[0].Results.Value[PARAM_RESULT];
    FTempClients.Text := AInputRequests[0].Results.Value['TEMPCLIENTS'];
  end

  else
    for i := Low(AInputRequests) to High(AInputRequests) do
    begin
      AddWarning(AInputRequests[i].GetWarnings);
      Err := AInputRequests[i].GetExceptions;
      if Err = '' then
        Inc(FClientsRebuilt)
      else
      begin
        Inc(FClientsFailed);
        AddException('Client #' + String(AInputRequests[i].Params.Value['ClientID']) + ': ' + Err);
      end;
    end;

  SetProgressText('Clients Rebuilt: ' + IntToStr(FClientsRebuilt) + '; Clients Failed:' + IntToStr(FClientsFailed) + '; Clients Left:' + IntToStr(FClientsToRebuild.Count));

  n := GetMaxThreads - RequestCount;
  for i := 1 to n do
  begin
    // add as many requests as MaxThreads specifies
    if FClientsToRebuild.Count = 0 then
      break;
    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', StrToInt(FClientsToRebuild[0]));
    Params.AddValue('AllClientsMode', FFullRebuild);
    Params.AddValue('TempClientsMode', (FTempClients.IndexOf(FClientsToRebuild[0]) >= 0));
    AddRequest('Rebuild Client ' + FClientsToRebuild[0], 'RebuildTempTables', Params, 'GoToNextClient.Fork');
    FClientsToRebuild.Delete(0);
  end;

  if (RequestCount = 0) and FFullRebuild then
  begin
    Params := TisListOfValues.Create;
    AddRequest('Finalize full rebuild', 'FinalizeRebuildAllClients', Params);
  end;
end;

procedure TEvRebuildTempTablesTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FFullRebuild := AStream.ReadBoolean;
  FUseRange := AStream.ReadBoolean;
  FUseNumbers := AStream.ReadBoolean;
  FFromClient := AStream.ReadInteger;
  FToClient := AStream.ReadInteger;
  FClientNumbers := AStream.ReadString;
end;

procedure TEvRebuildTempTablesTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('GoToNextClient', Phase_GoToNextClient);
end;

procedure TEvRebuildTempTablesTask.SetClientNumbers(const AValue: String);
begin
  FClientNumbers := AValue;
end;

procedure TEvRebuildTempTablesTask.SetFromClient(const AValue: Integer);
begin
  FFromClient := AValue;
end;

procedure TEvRebuildTempTablesTask.SetFullRebuild(const AValue: Boolean);
begin
  FFullRebuild := AValue;
end;

procedure TEvRebuildTempTablesTask.SetToClient(const AValue: Integer);
begin
  FToClient := AValue;
end;

procedure TEvRebuildTempTablesTask.SetUseNumbers(const AValue: Boolean);
begin
  FUseNumbers := AValue;
end;

procedure TEvRebuildTempTablesTask.SetUseRange(const AValue: Boolean);
begin
  FUseRange := AValue;
end;

procedure TEvRebuildTempTablesTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FFullRebuild);
  AStream.WriteBoolean(FUseRange);
  AStream.WriteBoolean(FUseNumbers);
  AStream.WriteInteger(FFromClient);
  AStream.WriteInteger(FToClient);
  AStream.WriteString(FClientNumbers);
end;

initialization
  ObjectFactory.Register(TEvRebuildTempTablesTask);

finalization
  SafeObjectFactoryUnRegister(TEvRebuildTempTablesTask);

end.
