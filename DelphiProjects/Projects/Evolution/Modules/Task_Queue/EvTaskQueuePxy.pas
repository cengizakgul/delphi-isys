unit EvTaskQueuePxy;

interface

uses SysUtils, EvCommonInterfaces, isBaseClasses, EvMainboard, evRemoteMethods, EvTransportDatagrams,
     evProxy, EvContext, EvTransportInterfaces, evStreamUtils;

implementation

type
  TevTaskQueueProxy = class(TevProxyModule, IevTaskQueue)
  private
  protected
    function  CreateTask(const ATaskType: String; const ApplyDefaults: Boolean): IevTask;
    procedure AddTask(const ATask: IevTask);
    procedure AddTasks(const ATaskList: IisList);
    procedure RemoveTask(const AID: TisGUID);
    function  GetTaskInfo(const AID: TisGUID): IevTaskInfo;
    function  GetTask(const AID: TisGUID): IevTask;
    function  GetTaskRequests(const AID: TisGUID): IisListOfValues;
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    procedure Pulse;
    function  GetUserTaskStatus: IisListOfValues;
    function  GetUserTaskInfoList: IisListOfValues;
    procedure ModifyTaskProperty(const AID: TisGUID; const APropName: String; const ANewValue: Variant);
  public
  end;


function GetTaskQueueProxy: IevTaskQueue;
begin
  Result := TevTaskQueueProxy.Create;
end;


{ TevTaskQueueProxy }

procedure TevTaskQueueProxy.AddTask(const ATask: IevTask);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_ADDTASK);
  D.Params.AddValue('ATask', ATask);
  D := SendRequest(D, False);
end;

procedure TevTaskQueueProxy.AddTasks(const ATaskList: IisList);
var
  D: IevDatagram;
  S: IevDualStream;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_ADDTASKS);
  S := TEvDualStreamHolder.Create;
  ATaskList.SaveItemsToStream(S);
  D.Params.AddValue('ATaskList', S);
  D := SendRequest(D, False);
end;

function TevTaskQueueProxy.CreateTask(const ATaskType: String; const ApplyDefaults: Boolean): IevTask;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_CREATETASK);
  D.Params.AddValue('ATaskType', ATaskType);
  D.Params.AddValue('ApplyDefaults', ApplyDefaults);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevTask;
end;

function TevTaskQueueProxy.GetActive: Boolean;
begin
  Result := True;
  NotRemotableMethod;
end;

function TevTaskQueueProxy.GetTask(const AID: TisGUID): IevTask;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_GETTASK);
  D.Params.AddValue('AID', AID);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevTask;
end;

function TevTaskQueueProxy.GetTaskInfo(const AID: TisGUID): IevTaskInfo;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_GETTASKINFO);
  D.Params.AddValue('AID', AID);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IevTaskInfo;
end;

function TevTaskQueueProxy.GetTaskRequests(const AID: TisGUID): IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_GETTASKREQUESTS);
  D.Params.AddValue('AID', AID);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevTaskQueueProxy.GetUserTaskInfoList: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_GETUSERTASKINFOLIST);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

function TevTaskQueueProxy.GetUserTaskStatus: IisListOfValues;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_GETUSERTASKSTATUS);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IisListOfValues;
end;

procedure TevTaskQueueProxy.ModifyTaskProperty(const AID: TisGUID; const APropName: String;
  const ANewValue: Variant);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_MODIFYTASKPROPERTY);
  D.Params.AddValue('AID', AID);
  D.Params.AddValue('APropName', APropName);
  D.Params.AddValue('ANewValue', ANewValue);
  D := SendRequest(D, False);
end;

procedure TevTaskQueueProxy.Pulse;
begin
  NotRemotableMethod;
end;

procedure TevTaskQueueProxy.RemoveTask(const AID: TisGUID);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_TASK_QUEUE_REMOVETASK);
  D.Params.AddValue('AID', AID);
  D := SendRequest(D, False);
end;

procedure TevTaskQueueProxy.SetActive(const AValue: Boolean);
begin
  NotRemotableMethod;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTaskQueueProxy, IevTaskQueue, 'Task Queue');

end.
