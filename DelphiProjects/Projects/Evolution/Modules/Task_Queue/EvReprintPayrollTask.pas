unit EvReprintPayrollTask;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTask, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, evContext;

implementation

type
  TEvReprintPayrollTask = class(TEvPrintableTask, IevReprintPayrollTask)
  private
    FClientID: Integer;
    FPrNbr: Integer;
    FCheckDate: TDateTime;
    FPrintReports: Boolean;
    FPrintInvoices: Boolean;
    FCurrentTOA: Boolean;
    FPayrollChecks: String;
    FAgencyChecks: String;
    FTaxChecks: String;
    FBillingChecks: String;
    FCheckForm: String;
    FMiscCheckForm: String;
    FSkipCheckStubCheck: Boolean;
    FDontPrintBankInfo: Boolean;
    FHideBackground: Boolean;
    FDontUseVMRSettings: Boolean;
    FPrReprintHistoryNBR: Integer;
    FReason: String;

  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList); override;

    function  GetClientID: Integer;
    procedure SetClientID(const AValue: Integer);
    function  GetPrNbr: Integer;
    procedure SetPrNbr(const AValue: Integer);
    function  GetCheckDate: TDateTime;
    procedure SetCheckDate(const AValue: TDateTime);
    function  GetPrintReports: Boolean;
    procedure SetPrintReports(const AValue: Boolean);
    function  GetPrintInvoices: Boolean;
    procedure SetPrintInvoices(const AValue: Boolean);
    function  GetCurrentTOA: Boolean;
    procedure SetCurrentTOA(const AValue: Boolean);
    function  GetPayrollChecks: String;
    procedure SetPayrollChecks(const AValue: String);
    function  GetAgencyChecks: String;
    procedure SetAgencyChecks(const AValue: String);
    function  GetTaxChecks: String;
    procedure SetTaxChecks(const AValue: String);
    function  GetBillingChecks: String;
    procedure SetBillingChecks(const AValue: String);
    function  GetCheckForm: String;
    procedure SetCheckForm(const AValue: String);
    function  GetMiscCheckForm: String;
    procedure SetMiscCheckForm(const AValue: String);
    function  GetSkipCheckStubCheck: Boolean;
    procedure SetSkipCheckStubCheck(const AValue: Boolean);
    function  GetDontPrintBankInfo: Boolean;
    procedure SetDontPrintBankInfo(const AValue: Boolean);
    function  GetHideBackground: Boolean;
    procedure SetHideBackground(const AValue: Boolean);
    function  GetDontUseVMRSettings: Boolean;
    procedure SetDontUseVMRSettings(const AValue: Boolean);
    function  GetPrReprintHistoryNBR: Integer;
    procedure SetPrReprintHistoryNBR(const AValue: Integer);
    function  GetReason: String;
    procedure SetReason(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;

{ TEvReprintPayrollTask }

function TEvReprintPayrollTask.GetAgencyChecks: String;
begin
  Result := FAgencyChecks;
end;

function TEvReprintPayrollTask.GetBillingChecks: String;
begin
  Result := FBillingChecks;
end;

function TEvReprintPayrollTask.GetCheckDate: TDateTime;
begin
  Result := FCheckDate;
end;

function TEvReprintPayrollTask.GetCheckForm: String;
begin
  Result := FCheckForm;
end;

function TEvReprintPayrollTask.GetClientID: Integer;
begin
  Result := FClientID;
end;

function TEvReprintPayrollTask.GetCurrentTOA: Boolean;
begin
  Result := FCurrentTOA;
end;

function TEvReprintPayrollTask.GetMiscCheckForm: String;
begin
  Result := FMiscCheckForm;
end;

function TEvReprintPayrollTask.GetPayrollChecks: String;
begin
  Result := FPayrollChecks;
end;

function TEvReprintPayrollTask.GetPrintInvoices: Boolean;
begin
  Result := FPrintInvoices;
end;

function TEvReprintPayrollTask.GetPrintReports: Boolean;
begin
  Result := FPrintReports;
end;

function TEvReprintPayrollTask.GetPrNbr: Integer;
begin
  Result := FPrNbr;
end;

function TEvReprintPayrollTask.GetSkipCheckStubCheck: Boolean;
begin
  Result := FSkipCheckStubCheck;
end;

function TEvReprintPayrollTask.GetDontPrintBankInfo: Boolean;
begin
  Result := FDontPrintBankInfo;
end;

function TEvReprintPayrollTask.GetDontUseVMRSettings: Boolean;
begin
  Result := FDontUseVMRSettings;
end;

function TEvReprintPayrollTask.GetTaxChecks: String;
begin
  Result := FTaxChecks;
end;

class function TEvReprintPayrollTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_REPRINT_PAYROLL;
end;

procedure TEvReprintPayrollTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('ClientID', FClientID);
  Params.AddValue('PrNbr', FPrNbr);
  Params.AddValue('PrintReports', FPrintReports);
  Params.AddValue('PrintInvoices', FPrintInvoices);
  Params.AddValue('CurrentTOA', FCurrentTOA);
  Params.AddValue('PayrollChecks', FPayrollChecks);
  Params.AddValue('AgencyChecks', FAgencyChecks);
  Params.AddValue('TaxChecks', FTaxChecks);
  Params.AddValue('BillingChecks', FBillingChecks);
  Params.AddValue('CheckForm', FCheckForm);
  Params.AddValue('MiscCheckForm', FMiscCheckForm);
  Params.AddValue('CheckDate', FCheckDate);
  Params.AddValue('SkipCheckStubCheck', FSkipCheckStubCheck);
  Params.AddValue('DontPrintBankInfo', FDontPrintBankInfo);
  Params.AddValue('HideBackground', FHideBackground);
  Params.AddValue('DontUseVMRSettings', FDontUseVMRSettings);
  Params.AddValue('ForcePrintVoucher', False);
  Params.AddValue('PrReprintHistoryNBR', FPrReprintHistoryNBR);
  Params.AddValue('Reason', FReason);

  AddRequest(GetCaption, 'ReprintPayroll', Params, 'ProcessReportResults.Fork');
end;

procedure TEvReprintPayrollTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FClientID := AStream.ReadInteger;
  FPrNbr := AStream.ReadInteger;
  FCheckDate := AStream.ReadDouble;
  FPrintReports := AStream.ReadBoolean;
  FPrintInvoices := AStream.ReadBoolean;
  FCurrentTOA := AStream.ReadBoolean;
  FPayrollChecks := AStream.ReadString;
  FAgencyChecks := AStream.ReadString;
  FTaxChecks := AStream.ReadString;
  FBillingChecks := AStream.ReadString;
  FCheckForm := AStream.ReadString;
  FMiscCheckForm := AStream.ReadString;
  FSkipCheckStubCheck := AStream.ReadBoolean;
  FDontPrintBankInfo := AStream.ReadBoolean;
  FHideBackground := AStream.ReadBoolean;
  FDontUseVMRSettings := AStream.ReadBoolean;
  FPrReprintHistoryNBR := AStream.ReadInteger;
  FReason := AStream.ReadString;
end;

procedure TEvReprintPayrollTask.SetAgencyChecks(const AValue: String);
begin
  FAgencyChecks := AValue;
end;

procedure TEvReprintPayrollTask.SetBillingChecks(const AValue: String);
begin
  FBillingChecks := AValue;
end;

procedure TEvReprintPayrollTask.SetCheckDate(const AValue: TDateTime);
begin
  FCheckDate := AValue;
end;

procedure TEvReprintPayrollTask.SetCheckForm(const AValue: String);
begin
  FCheckForm := AValue;
end;

procedure TEvReprintPayrollTask.SetClientID(const AValue: Integer);
begin
  FClientID := AValue;
end;

procedure TEvReprintPayrollTask.SetCurrentTOA(const AValue: Boolean);
begin
  FCurrentTOA := AValue;
end;

procedure TEvReprintPayrollTask.SetMiscCheckForm(const AValue: String);
begin
  FMiscCheckForm := AValue;
end;

procedure TEvReprintPayrollTask.SetPayrollChecks(const AValue: String);
begin
  FPayrollChecks := AValue;
end;

procedure TEvReprintPayrollTask.SetPrintInvoices(const AValue: Boolean);
begin
  FPrintInvoices := AValue;
end;

procedure TEvReprintPayrollTask.SetPrintReports(const AValue: Boolean);
begin
  FPrintReports := AValue;
end;

procedure TEvReprintPayrollTask.SetPrNbr(const AValue: Integer);
begin
  FPrNbr := AValue;
end;

procedure TEvReprintPayrollTask.SetSkipCheckStubCheck(const AValue: Boolean);
begin
  FSkipCheckStubCheck := AValue;
end;

procedure TEvReprintPayrollTask.SetDontPrintBankInfo(const AValue: Boolean);
begin
  FDontPrintBankInfo := AValue;
end;

procedure TEvReprintPayrollTask.SetDontUseVMRSettings(const AValue: Boolean);
begin
  FDontUseVMRSettings := AValue;
end;

procedure TEvReprintPayrollTask.SetTaxChecks(const AValue: String);
begin
  FTaxChecks := AValue;
end;

procedure TEvReprintPayrollTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteInteger(FClientID);
  AStream.WriteInteger(FPrNbr);
  AStream.WriteDouble(FCheckDate);
  AStream.WriteBoolean(FPrintReports);
  AStream.WriteBoolean(FPrintInvoices);
  AStream.WriteBoolean(FCurrentTOA);
  AStream.WriteString(FPayrollChecks);
  AStream.WriteString(FAgencyChecks);
  AStream.WriteString(FTaxChecks);
  AStream.WriteString(FBillingChecks);
  AStream.WriteString(FCheckForm);
  AStream.WriteString(FMiscCheckForm);
  AStream.WriteBoolean(FSkipCheckStubCheck);
  AStream.WriteBoolean(FDontPrintBankInfo);
  AStream.WriteBoolean(FHideBackground);
  AStream.WriteBoolean(FDontUseVMRSettings);
  AStream.WriteInteger(FPrReprintHistoryNBR);
  AStream.WriteString(FReason);
end;

function TEvReprintPayrollTask.GetHideBackground: Boolean;
begin
  Result := FHideBackground;
end;

procedure TEvReprintPayrollTask.SetHideBackground(const AValue: Boolean);
begin
  FHideBackground := AValue;
end;

procedure TEvReprintPayrollTask.Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList);
begin
  inherited;
//The following code should be existed in TEvPrintableTask.Call_ProcessReportResults
(*  if (ctx_VmrEngine <> nil) and Context.License.VMR then
  begin
    EI :=  TisStringList.Create;
    for i := Low(AInputRequests) to High(AInputRequests) do
      if AInputRequests[i].Results.ValueExists('CHECKS_EXTRA_INFO') then
        EI.AddStrings(IInterface(AInputRequests[i].Results.Value['CHECKS_EXTRA_INFO']) as IisStringList);
    ctx_VmrEngine.SetChecksExtraInfo(EI);
  end;
*)
end;

function TEvReprintPayrollTask.GetPrReprintHistoryNBR: Integer;
begin
  Result := FPrReprintHistoryNBR;
end;

procedure TEvReprintPayrollTask.SetPrReprintHistoryNBR(
  const AValue: Integer);
begin
  FPrReprintHistoryNBR := AValue;
end;

function TEvReprintPayrollTask.GetReason: String;
begin
  Result := FReason;
end;

procedure TEvReprintPayrollTask.SetReason(const AValue: String);
begin
  FReason := AValue;
end;

initialization
  ObjectFactory.Register(TEvReprintPayrollTask);

finalization
  SafeObjectFactoryUnRegister(TEvReprintPayrollTask);

end.
