unit EvCashManagementTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvAch;

implementation

type
  TEvCashManagementTaskProxy = class(TEvCustomTaskProxy, IevCashManagementTask)
  private
    FOptions: IevACHOptions;
    FReversalACH: Boolean;
    FPeriodFrom: TDateTime;
    FPeriodTo: TDateTime;
    FStatusFromIndex: Integer;
    FStatusToIndex: Integer;
    FBlockNegativeChecks: Boolean;
    FBlockNegativeLiabilities: Boolean;
    FBlockCompanyDD: Boolean;
    FIncludeOffsets: Integer;
    FSB_BANK_ACCOUNTS_NBR: Integer;
    FCombine: Integer;
    FThreshold: Currency;
    FUseSBEIN: Boolean;
    FCTS: Boolean;
    FPreprocess: Boolean;
    FACHDataStream: IEvDualStream;
    FAchFile: IEvDualStream;
    FAchRegularReport: IEvDualStream;
    FAchDetailedReport: IEvDualStream;
    FAchSave: IEvDualStream;
    FNonAchReport: IEvDualStream;
    FWTFile: IEvDualStream;
    FSbData: IisListOfValues;
    FRequestParams: IisParamsCollection;
    FCompanies: IisListOfValues;
    FNextCompanyIndex: Integer;
    FHSA: Boolean;
  protected
    function  GetHSA: Boolean;
    procedure SetHSA(const Value: Boolean);
    function  GetBlockNegativeLiabilities: Boolean;
    procedure SetBlockNegativeLiabilities(const AValue: Boolean);
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function  GetBlockCompanyDD: Boolean;
    procedure SetBlockCompanyDD(const AValue: Boolean);
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetACHOptions: IevACHOptions;
    function  GetReversalACH: Boolean;
    procedure SetReversalACH(const AValue: Boolean);
    function  GetPeriodFrom: TDateTime;
    procedure SetPeriodFrom(const AValue: TDateTime);
    function  GetPeriodTo: TDateTime;
    procedure SetPeriodTo(const AValue: TDateTime);
    function  GetStatusFromIndex: Integer;
    procedure SetStatusFromIndex(const AValue: Integer);
    function  GetStatusToIndex: Integer;
    procedure SetStatusToIndex(const AValue: Integer);
    function  GetBlockNegativeChecks: Boolean;
    procedure SetBlockNegativeChecks(const AValue: Boolean);
    function  GetIncludeOffsets: Integer;
    procedure SetIncludeOffsets(const AValue: Integer);
    function  GetSB_BANK_ACCOUNTS_NBR: Integer;
    procedure SetSB_BANK_ACCOUNTS_NBR(const AValue: Integer);
    function  GetCombine: Integer;
    procedure SetCombine(const AValue: Integer);
    function  GetThreshold: Currency;
    procedure SetThreshold(const AValue: Currency);
    function  GetUseSBEIN: Boolean;
    procedure SetUseSBEIN(const AValue: Boolean);
    function  GetCTS: Boolean;
    procedure SetCTS(const AValue: Boolean);
    function  GetPreprocess: Boolean;
    procedure SetPreprocess(const AValue: Boolean);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;
    function  GetNonAchReport: IEvDualStream;
    function  GetWTFile: IEvDualStream;
  public
    class function GetTypeID: String; override;
  end;


{ TEvCashManagementTaskProxy }

procedure TEvCashManagementTaskProxy.DoOnConstruction;
begin
  inherited;
  FOptions := TevACHOptions.Create;
  FACHDataStream := TEvDualStreamHolder.Create;
  FSbData := TisListOfValues.Create;
  FRequestParams := TisParamsCollection.Create;
  FCompanies := TisListOfValues.Create;
  FNextCompanyIndex := 0;
end;

function TEvCashManagementTaskProxy.GetACHDataStream: IEvDualStream;
begin
  Result := FACHDataStream;
end;

function TEvCashManagementTaskProxy.GetAchDetailedReport: IEvDualStream;
begin
  Result := FAchDetailedReport;
end;

function TEvCashManagementTaskProxy.GetAchFile: IEvDualStream;
begin
  Result := FAchFile;
end;

function TEvCashManagementTaskProxy.GetACHOptions: IevACHOptions;
begin
  Result := FOptions;
end;

function TEvCashManagementTaskProxy.GetAchRegularReport: IEvDualStream;
begin
  Result := FAchRegularReport;
end;

function TEvCashManagementTaskProxy.GetAchSave: IEvDualStream;
begin
  Result := FAchSave;
end;

function TEvCashManagementTaskProxy.GetBlockCompanyDD: Boolean;
begin
  Result := FBlockCompanyDD;
end;

function TEvCashManagementTaskProxy.GetBlockNegativeChecks: Boolean;
begin
  Result := FBlockNegativeChecks;
end;

function TEvCashManagementTaskProxy.GetBlockNegativeLiabilities: Boolean;
begin
  Result := FBlockNegativeLiabilities;
end;

procedure TEvCashManagementTaskProxy.SetBlockNegativeLiabilities(
  const AValue: Boolean);
begin
  FBlockNegativeLiabilities := AValue;
end;

function TEvCashManagementTaskProxy.GetCombine: Integer;
begin
  Result := FCombine;
end;

function TEvCashManagementTaskProxy.GetCTS: Boolean;
begin
  Result := FCTS;
end;

function TEvCashManagementTaskProxy.GetIncludeOffsets: Integer;
begin
  Result := FIncludeOffsets;
end;

function TEvCashManagementTaskProxy.GetNonAchReport: IEvDualStream;
begin
  Result := FNonAchReport;
end;

function TEvCashManagementTaskProxy.GetPeriodFrom: TDateTime;
begin
  Result := FPeriodFrom;
end;

function TEvCashManagementTaskProxy.GetPeriodTo: TDateTime;
begin
  Result := FPeriodTo;
end;

function TEvCashManagementTaskProxy.GetPreprocess: Boolean;
begin
  Result := FPreprocess;
end;

function TEvCashManagementTaskProxy.GetReversalACH: Boolean;
begin
  Result := FReversalACH;
end;

function TEvCashManagementTaskProxy.GetSB_BANK_ACCOUNTS_NBR: Integer;
begin
  Result := FSB_BANK_ACCOUNTS_NBR;
end;

function TEvCashManagementTaskProxy.GetStatusFromIndex: Integer;
begin
  Result := FStatusFromIndex;
end;

function TEvCashManagementTaskProxy.GetStatusToIndex: Integer;
begin
  Result := FStatusToIndex;
end;

function TEvCashManagementTaskProxy.GetThreshold: Currency;
begin
  Result := FThreshold;
end;

class function TEvCashManagementTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PAYROLL_ACH;
end;

function TEvCashManagementTaskProxy.GetUseSBEIN: Boolean;
begin
  Result := FUseSBEIN;
end;

procedure TEvCashManagementTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FACHDataStream := AStream.ReadStream(nil);
  (FOptions as IisInterfacedObject).ReadFromStream(AStream);
  FReversalACH := AStream.ReadBoolean;
  FPeriodFrom := AStream.ReadDouble;
  FPeriodTo := AStream.ReadDouble;
  FStatusFromIndex := AStream.ReadInteger;
  FStatusToIndex := AStream.ReadInteger;
  FBlockNegativeChecks := AStream.ReadBoolean;
  FBlockCompanyDD := AStream.ReadBoolean;
  FBlockNegativeLiabilities := AStream.ReadBoolean;
  FIncludeOffsets := AStream.ReadInteger;
  FSB_BANK_ACCOUNTS_NBR := AStream.ReadInteger;
  FCombine := AStream.ReadInteger;
  FThreshold := AStream.ReadCurrency;
  FUseSBEIN := AStream.ReadBoolean;
  FCTS := AStream.ReadBoolean;
  FPreprocess := AStream.ReadBoolean;
  FAchFile := AStream.ReadStream(nil, True);
  FAchRegularReport := AStream.ReadStream(nil, True);
  FAchDetailedReport := AStream.ReadStream(nil, True);
  FAchSave := AStream.ReadStream(nil, True);
  FNonAchReport := AStream.ReadStream(nil, True);
  FWTFile := AStream.ReadStream(nil, True);
  (FSbData as IisInterfacedObject).ReadFromStream(AStream);
  (FRequestParams as IisInterfacedObject).ReadFromStream(AStream);
  (FCompanies as IisInterfacedObject).ReadFromStream(AStream);
  FNextCompanyIndex := AStream.ReadInteger;
  FHSA := AStream.ReadBoolean;
end;

procedure TEvCashManagementTaskProxy.SetACHDataStream(const AValue: IEvDualStream);
begin
  FACHDataStream := AValue;
end;

procedure TEvCashManagementTaskProxy.SetBlockCompanyDD(
  const AValue: Boolean);
begin
  FBlockCompanyDD := AValue;
end;

procedure TEvCashManagementTaskProxy.SetBlockNegativeChecks(const AValue: Boolean);
begin
  FBlockNegativeChecks := AValue;
end;

procedure TEvCashManagementTaskProxy.SetCombine(const AValue: Integer);
begin
  FCombine := AValue;
end;

procedure TEvCashManagementTaskProxy.SetCTS(const AValue: Boolean);
begin
  FCTS := AValue;
end;

procedure TEvCashManagementTaskProxy.SetIncludeOffsets(const AValue: Integer);
begin
  FIncludeOffsets := AValue;
end;

procedure TEvCashManagementTaskProxy.SetPeriodFrom(const AValue: TDateTime);
begin
  FPeriodFrom := AValue;
end;

procedure TEvCashManagementTaskProxy.SetPeriodTo(const AValue: TDateTime);
begin
  FPeriodTo := AValue;
end;

procedure TEvCashManagementTaskProxy.SetPreprocess(const AValue: Boolean);
begin
  FPreprocess := AValue;
end;

procedure TEvCashManagementTaskProxy.SetReversalACH(const AValue: Boolean);
begin
  FReversalACH := AValue;
end;

procedure TEvCashManagementTaskProxy.SetSB_BANK_ACCOUNTS_NBR(
  const AValue: Integer);
begin
  FSB_BANK_ACCOUNTS_NBR := AValue;
end;

procedure TEvCashManagementTaskProxy.SetStatusFromIndex(const AValue: Integer);
begin
  FStatusFromIndex := AValue;
end;

procedure TEvCashManagementTaskProxy.SetStatusToIndex(const AValue: Integer);
begin
  FStatusToIndex := AValue;
end;

procedure TEvCashManagementTaskProxy.SetThreshold(const AValue: Currency);
begin
  FThreshold := AValue;
end;

procedure TEvCashManagementTaskProxy.SetUseSBEIN(const AValue: Boolean);
begin
  FUseSBEIN := AValue;
end;

procedure TEvCashManagementTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteStream(FACHDataStream);
  (FOptions as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteBoolean(FReversalACH);
  AStream.WriteDouble(FPeriodFrom);
  AStream.WriteDouble(FPeriodTo);
  AStream.WriteInteger(FStatusFromIndex);
  AStream.WriteInteger(FStatusToIndex);
  AStream.WriteBoolean(FBlockNegativeChecks);
  AStream.WriteBoolean(FBlockCompanyDD);
  AStream.WriteBoolean(FBlockNegativeLiabilities);
  AStream.WriteInteger(FIncludeOffsets);
  AStream.WriteInteger(FSB_BANK_ACCOUNTS_NBR);
  AStream.WriteInteger(FCombine);
  AStream.WriteCurrency(FThreshold);
  AStream.WriteBoolean(FUseSBEIN);
  AStream.WriteBoolean(FCTS);
  AStream.WriteBoolean(FPreprocess);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchRegularReport);
  AStream.WriteStream(FAchDetailedReport);
  AStream.WriteStream(FAchSave);
  AStream.WriteStream(FNonAchReport);
  AStream.WriteStream(FWTFile);
  (FSbData as IisInterfacedObject).WriteToStream(AStream);
  (FRequestParams as IisInterfacedObject).WriteToStream(AStream);
  (FCompanies as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteInteger(FNextCompanyIndex);
  AStream.WriteBoolean(FHSA);
end;

function TEvCashManagementTaskProxy.GetWTFile: IEvDualStream;
begin
  Result := FWTFile;
end;

function TEvCashManagementTaskProxy.GetHSA: Boolean;
begin
  Result := FHSA;
end;

procedure TEvCashManagementTaskProxy.SetHSA(const Value: Boolean);
begin
  FHSA := Value;
end;

initialization
  ObjectFactory.Register(TEvCashManagementTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvCashManagementTaskProxy);

end.
