unit EvRebuildVMRHistoryTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;


implementation

type
  TevRebuildVMRHistoryTaskProxy = class(TevCustomTaskProxy, IevRebuildVMRHistoryTask)
  private
    FPeriodBegDate: TDateTime;
    FPeriodEndDate: TDateTime;
    FCoList: IisStringList;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetPeriodBegDate: TDateTime;
    procedure SetPeriodBegDate(const AValue: TDateTime);
    function  GetPeriodEndDate: TDateTime;
    procedure SetPeriodEndDate(const AValue: TDateTime);
    function  GetCoList: IisStringList;
  public
    class function GetTypeID: String; override;
  end;


{ TevRebuildVMRHistoryTaskProxy }

procedure TevRebuildVMRHistoryTaskProxy.DoOnConstruction;
begin
  inherited;
  FCoList := TisStringList.CreateUnique;
end;

function TevRebuildVMRHistoryTaskProxy.GetCoList: IisStringList;
begin
  Result := FCoList;
end;

function TevRebuildVMRHistoryTaskProxy.GetPeriodBegDate: TDateTime;
begin
  Result := FPeriodBegDate;
end;

function TevRebuildVMRHistoryTaskProxy.GetPeriodEndDate: TDateTime;
begin
  Result := FPeriodEndDate;
end;

class function TevRebuildVMRHistoryTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_REBUILD_VMR_HIST;
end;

procedure TevRebuildVMRHistoryTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IevDualStream;
begin
  inherited;
  FPeriodBegDate := AStream.ReadDouble;
  FPeriodEndDate := AStream.ReadDouble;

  S := AStream.ReadStream(S);
  (FCoList as IisInterfacedObject).AsStream := S;
end;

procedure TevRebuildVMRHistoryTaskProxy.SetPeriodBegDate(const AValue: TDateTime);
begin
  FPeriodBegDate := AValue;
end;

procedure TevRebuildVMRHistoryTaskProxy.SetPeriodEndDate(const AValue: TDateTime);
begin
  FPeriodEndDate := AValue;
end;

procedure TevRebuildVMRHistoryTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IevDualStream;
begin
  inherited;
  AStream.WriteDouble(FPeriodBegDate);
  AStream.WriteDouble(FPeriodEndDate);

  S := (FCoList as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);
end;

initialization
  ObjectFactory.Register(TevRebuildVMRHistoryTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TevRebuildVMRHistoryTaskProxy);

end.
