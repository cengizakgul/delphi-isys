unit EvCashManagementTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

type
  TEvCashManagementTask = class(TEvCustomTask, IevCashManagementTask)
  private
    FOptions: IevACHOptions;
    FReversalACH: Boolean;
    FPeriodFrom: TDateTime;
    FPeriodTo: TDateTime;
    FStatusFromIndex: Integer;
    FStatusToIndex: Integer;
    FBlockNegativeChecks: Boolean;
    FBlockCompanyDD: Boolean;
    FIncludeOffsets: Integer;
    FSB_BANK_ACCOUNTS_NBR: Integer;
    FCombine: Integer;
    FThreshold: Currency;
    FUseSBEIN: Boolean;
    FCTS: Boolean;
    FPreprocess: Boolean;
    FACHDataStream: IisStream;
    FAchFile: IisStream;
    FAchRegularReport: IisStream;
    FNonAchReport: IisStream;
    FWTFile: IisStream;
    FAchDetailedReport: IisStream;
    FAchSave: IisStream;
    FBlockNegativeLiabilities: Boolean;
    FSbData: IisListOfValues;
    FRequestParams: IisParamsCollection;
    FCompanies: IisListOfValues;
    FNextCompanyIndex: Integer;
    FHSA: Boolean;
    function GetMaxRequestCount: Integer;
  protected
    procedure DoStoreTaskState(const AStream: IevDualStream); override;
    procedure DoRestoreTaskState(const AStream: IevDualStream); override;
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IisStream); override;
    procedure ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision); override;
    function  BeforeTaskStart: Boolean; override;
    procedure AfterTaskFinished; override;
    function  GetBlockCompanyDD: Boolean;
    procedure SetBlockCompanyDD(const AValue: Boolean);
    function  GetACHDataStream: IisStream;
    procedure SetACHDataStream(const AValue: IisStream);
    function  GetACHOptions: IevACHOptions;
    function  GetReversalACH: Boolean;
    procedure SetReversalACH(const AValue: Boolean);
    function  GetPeriodFrom: TDateTime;
    procedure SetPeriodFrom(const AValue: TDateTime);
    function  GetPeriodTo: TDateTime;
    procedure SetPeriodTo(const AValue: TDateTime);
    function  GetStatusFromIndex: Integer;
    procedure SetStatusFromIndex(const AValue: Integer);
    function  GetStatusToIndex: Integer;
    procedure SetStatusToIndex(const AValue: Integer);
    function  GetBlockNegativeChecks: Boolean;
    procedure SetBlockNegativeChecks(const AValue: Boolean);
    function  GetIncludeOffsets: Integer;
    procedure SetIncludeOffsets(const AValue: Integer);
    function  GetSB_BANK_ACCOUNTS_NBR: Integer;
    procedure SetSB_BANK_ACCOUNTS_NBR(const AValue: Integer);
    function  GetCombine: Integer;
    procedure SetCombine(const AValue: Integer);
    function  GetThreshold: Currency;
    procedure SetThreshold(const AValue: Currency);
    function  GetUseSBEIN: Boolean;
    procedure SetUseSBEIN(const AValue: Boolean);
    function  GetCTS: Boolean;
    procedure SetCTS(const AValue: Boolean);
    function  GetHSA: Boolean;
    procedure SetHSA(const AValue: Boolean);
    function  GetPreprocess: Boolean;
    procedure SetPreprocess(const AValue: Boolean);
    function  GetAchFile: IisStream;
    function  GetAchRegularReport: IisStream;
    function  GetAchDetailedReport: IisStream;
    function  GetAchSave: IisStream;
    function  GetNonAchReport: IisStream;
    function  GetWTFile: IisStream;
    function  GetBlockNegativeLiabilities: Boolean;
    procedure SetBlockNegativeLiabilities(const AValue: Boolean);
    procedure RegisterTaskPhases; override;

    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
    procedure  ProcessCompanies(const AInputRequests: TevTaskRequestList);
    procedure  FinalizeCashManagement(const AInputRequests: TevTaskRequestList);
  public
    class function GetTypeID: String; override;
  end;

{ TEvCashManagementTask }

procedure TEvCashManagementTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('ProcessCompanies', ProcessCompanies);
  RegisterTaskPhase('FinalizeCashManagement', FinalizeCashManagement);
end;

procedure TEvCashManagementTask.DoOnConstruction;
begin
  inherited;
  FOptions := TevACHOptions.Create;
  FACHDataStream := TisStream.Create;
  FDaysStayInQueue := 10;
  FSbData := TisListOfValues.Create;
  FRequestParams := TisParamsCollection.Create;
  FCompanies := TisListOfValues.Create;

  FAchFile := TisStream.Create;
  FAchRegularReport := TisStream.Create;
  FAchDetailedReport := TisStream.Create;

  FNonAchReport := TisStream.Create;
  FWTFile := TisStream.Create;
  FAchSave := TisStream.Create;

  FNextCompanyIndex := 0;    
end;

function TEvCashManagementTask.GetACHDataStream: IisStream;
begin
  Result := FACHDataStream;
end;

function TEvCashManagementTask.GetAchDetailedReport: IisStream;
begin
  Result := FAchDetailedReport;
end;

function TEvCashManagementTask.GetAchFile: IisStream;
begin
  Result := FAchFile;
end;

function TEvCashManagementTask.GetACHOptions: IevACHOptions;
begin
  Result := FOptions;
end;

function TEvCashManagementTask.GetAchRegularReport: IisStream;
begin
  Result := FAchRegularReport;
end;

function TEvCashManagementTask.GetAchSave: IisStream;
begin
  Result := FAchSave;
end;

function TEvCashManagementTask.GetBlockNegativeChecks: Boolean;
begin
  Result := FBlockNegativeChecks;
end;

function TEvCashManagementTask.GetCombine: Integer;
begin
  Result := FCombine;
end;

function TEvCashManagementTask.GetCTS: Boolean;
begin
  Result := FCTS;
end;

function TEvCashManagementTask.GetIncludeOffsets: Integer;
begin
  Result := FIncludeOffsets;
end;

function TEvCashManagementTask.GetPeriodFrom: TDateTime;
begin
  Result := FPeriodFrom;
end;

function TEvCashManagementTask.GetPeriodTo: TDateTime;
begin
  Result := FPeriodTo;
end;

function TEvCashManagementTask.GetPreprocess: Boolean;
begin
  Result := FPreprocess;
end;

function TEvCashManagementTask.GetReversalACH: Boolean;
begin
  Result := FReversalACH;
end;

function TEvCashManagementTask.GetSB_BANK_ACCOUNTS_NBR: Integer;
begin
  Result := FSB_BANK_ACCOUNTS_NBR;
end;

function TEvCashManagementTask.GetStatusFromIndex: Integer;
begin
  Result := FStatusFromIndex;
end;

function TEvCashManagementTask.GetStatusToIndex: Integer;
begin
  Result := FStatusToIndex;
end;

function TEvCashManagementTask.GetThreshold: Currency;
begin
  Result := FThreshold;
end;

class function TEvCashManagementTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PAYROLL_ACH;
end;

function TEvCashManagementTask.GetUseSBEIN: Boolean;
begin
  Result := FUseSBEIN;
end;

function TEvCashManagementTask.BeforeTaskStart: Boolean;
begin
  Result := inherited BeforeTaskStart and mb_GlobalFlagsManager.TryLock(GF_SB_ACH_PROCESS);
end;

procedure TEvCashManagementTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;

  if Length(FOptions.CoId) = 0 then
    FOptions.CoId := '1'
  else if FOptions.CoId = '@' then
    FOptions.CoId := ' ';

  if (FStatusToIndex <> PR_ACH_STATUS_TO_SENT) and (FStatusFromIndex <> PR_ACH_STATUS_FROM_SENT) then
  begin
    SetProgressText('Changed cash management status from "' + DateToStr(FPeriodFrom) + '" to "' + DateToStr(FPeriodTo) + '".');
    Params.AddValue('StatusToIndex', FStatusToIndex);
    Params.AddValue('ACHDataStream', FACHDataStream);
    AddRequest(GetCaption, 'UpdateAchStatus',  Params);
  end
  else
  begin
    SetProgressText('Processing cash management from "' + DateToStr(FPeriodFrom) + '" to "' + DateToStr(FPeriodTo) + '".');
    Params.AddValue('ACHDataStream', FACHDataStream);
    Params.AddValue('Options', FOptions);
    AddRequest(GetCaption, 'PrepareCashManagement',  Params, 'ProcessCompanies.Merge');
  end;
end;

procedure TEvCashManagementTask.ProcessCompanies(
  const AInputRequests: TevTaskRequestList);
var
  CompanyData: IisListOfValues;
  Companies: IisParamsCollection;
  Params: IisListOfValues;
  i, n: Integer;
begin
  Companies := IInterface(AInputRequests[0].Results.FindValue('Companies').Value) as IisParamsCollection;
  FSbData := IInterface(AInputRequests[0].Results.FindValue('SbData').Value) as IisListOfValues;

  for i := 0 to Companies.Count - 1 do
  begin
    CompanyData := Companies[i];
    CompanyData.AddValue('ReversalACH', FReversalACH);
    CompanyData.AddValue('StatusFromIndex', FStatusFromIndex);
    CompanyData.AddValue('StatusToIndex', FStatusToIndex);
    CompanyData.AddValue('BlockNegativeChecks', FBlockNegativeChecks);
    CompanyData.AddValue('BlockCompanyDD', FBlockCompanyDD);
    CompanyData.AddValue('BlockNegativeLiabilities', FBlockNegativeLiabilities);
    CompanyData.AddValue('Threshold', FThreshold);
    CompanyData.AddValue('UseSBEIN', FUseSBEIN);
    CompanyData.AddValue('CTS', FCTS);
    CompanyData.AddValue('HSA', FHSA);

    Params := FRequestParams.AddParams;
    Params.AddValue('CompanyData', CompanyData);
    Params.AddValue('Options', FOptions);
    Params.AddValue('SbData', FSbData);
    Params.AddValue('IncludeOffsets', FIncludeOffsets);
    Params.AddValue('RequestComment', 'Processing company '+CompanyData.Value['CUSTOM_COMPANY_NUMBER']);
  end;

  n := GetMaxRequestCount - RequestCount;
  for i := 1 to n do
  begin
    if FNextCompanyIndex < FRequestParams.Count then
    begin
      Params := FRequestParams.Params[FNextCompanyIndex];
      AddRequest(Params.Value['RequestComment'], 'ProcessCashManagementForCompany', Params, 'FinalizeCashManagement.Fork');
      FNextCompanyIndex := FNextCompanyIndex + 1;
    end
    else
      break;
  end;

end;

procedure TEvCashManagementTask.FinalizeCashManagement(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
  CompanyData: IisListOfValues;
  i, n: Integer;
  CompanyName: String;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    if not AInputRequests[i].Results.ValueExists('CompanyData') then
    begin
      CompanyData := IInterface(AInputRequests[i].Params.Value['CompanyData']) as IisListOfValues;
      CompanyName := CompanyData.Value['CUSTOM_COMPANY_NUMBER'];
      CompanyData.AddValue('Exceptions', CompanyName + ' - ' + AInputRequests[i].GetExceptions);
    end
    else
      CompanyData := IInterface(AInputRequests[i].Results.Value['CompanyData']) as IisListOfValues;
    FCompanies.AddValue(CompanyData.Value['CL_NBR']+':'+CompanyData.Value['Co_NBR'], CompanyData);
  end;

  if FCompanies.Count = FRequestParams.Count then
  begin
    Params := TisListOfValues.Create;
    Params.AddValue('Companies', FCompanies);
    Params.AddValue('Options', FOptions);
    Params.AddValue('SbData', FSbData);
    Params.AddValue('Preprocess', FPreprocess);
    Params.AddValue('IncludeOffsets', FIncludeOffsets);
    Params.AddValue('From', FPeriodFrom);
    Params.AddValue('To', FPeriodTo);
    Params.AddValue('ACHFolder', Context.DomainInfo.FileFolders.ACHFolder);

    AddRequest('Finalizing Cash Management', 'FinalizeCashManagement',  Params);
  end
  else
  begin
    n := GetMaxRequestCount - RequestCount;
    for i := 1 to n do
    begin
      if FNextCompanyIndex < FRequestParams.Count then
      begin
        Params := FRequestParams.Params[FNextCompanyIndex];
        AddRequest(Params.Value['RequestComment'], 'ProcessCashManagementForCompany', Params, 'FinalizeCashManagement.Fork');
        FNextCompanyIndex := FNextCompanyIndex + 1;
      end
      else
        break;
    end;
  end;
  
end;

procedure TEvCashManagementTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  ACHFolder: String;
  i: Integer;
  ACHFiles: IisListOfValues;
begin
  if Length(AInputRequests) > 0 then
  begin
    AddException(AInputRequests[0].GetExceptions);
    AddWarning(AInputRequests[0].GetWarnings);

//    if AInputRequests[0].GetExceptions = '' then
    begin
      if AInputRequests[0].Results.FindValue('AchFile') <> nil then
        FAchFile := IInterface(AInputRequests[0].Results.Value['AchFile']) as IisStream;
      if AInputRequests[0].Results.FindValue('AchRegularReport') <> nil then
        FAchRegularReport := IInterface(AInputRequests[0].Results.Value['AchRegularReport']) as IisStream;
      if AInputRequests[0].Results.FindValue('AchDetailedReport') <> nil then
        FAchDetailedReport := IInterface(AInputRequests[0].Results.Value['AchDetailedReport']) as IisStream;
      if AInputRequests[0].Results.FindValue('AchSave') <> nil then
        ACHFiles := IInterface(AInputRequests[0].Results.Value['AchSave']) as IisListOfValues;
      if AInputRequests[0].Results.FindValue('NonAchReport') <> nil then
        FNonAchReport := IInterface(AInputRequests[0].Results.Value['NonAchReport']) as IisStream;
      if AInputRequests[0].Results.FindValue('WTFile') <> nil then
        FWTFile := IInterface(AInputRequests[0].Results.Value['WTFile']) as IisStream;

      if Assigned(ACHFiles) and (ACHFiles.Count > 0) then
      begin
        FAchSave := TevDualStreamHolder.CreateInMemory;
        (IInterface(ACHFiles[0].Value) as IisStringList).SaveToStream(FAchSave.RealStream);
      end;

      ACHFolder := Context.DomainInfo.FileFolders.ACHFolder;
      if Length(ACHFolder) > 0 then
      begin
        if Assigned(FAchSave) and ForceDirectories(ACHFolder) then
          if Assigned(ACHFiles) then
            for i := 0 to ACHFiles.Count - 1 do
              (IInterface(ACHFiles[i].Value) as IisStringList).SaveToFile(ACHFolder + '\' + ExtractFileName(ACHFiles[i].Name));
      end;

      if Assigned(ACHFiles) and (ACHFiles.Count > 0) then
        if ACHFiles.Count > 1 then
        begin
          FAchSave := nil;
          if Length(ACHFolder) > 0 then
            AddWarning(Format('%d ACH files were saved into ACH directory %s',[ACHFiles.Count, ACHFolder]))
          else
            AddException(Format('%d ACH files were created but not saved',[ACHFiles.Count, ACHFolder]));
        end;
    end;
  end;

  inherited;
end;

procedure TEvCashManagementTask.ReadSelfFromStream(const AStream: IisStream; const ARevision: TisRevision);
begin
  inherited;
  FACHDataStream := AStream.ReadStream(nil);
  (FOptions as IisInterfacedObject).ReadFromStream(AStream);
  FReversalACH := AStream.ReadBoolean;
  FPeriodFrom := AStream.ReadDouble;
  FPeriodTo := AStream.ReadDouble;
  FStatusFromIndex := AStream.ReadInteger;
  FStatusToIndex := AStream.ReadInteger;
  FBlockNegativeChecks := AStream.ReadBoolean;
  FBlockCompanyDD := AStream.ReadBoolean;
  FBlockNegativeLiabilities := AStream.ReadBoolean;
  FIncludeOffsets := AStream.ReadInteger;
  FSB_BANK_ACCOUNTS_NBR := AStream.ReadInteger;
  FCombine := AStream.ReadInteger;
  FThreshold := AStream.ReadCurrency;
  FUseSBEIN := AStream.ReadBoolean;
  FCTS := AStream.ReadBoolean;
  FPreprocess := AStream.ReadBoolean;
  FAchFile := AStream.ReadStream(nil);
  FAchRegularReport := AStream.ReadStream(nil);
  FAchDetailedReport := AStream.ReadStream(nil);
  FAchSave := AStream.ReadStream(nil);
  FNonAchReport := AStream.ReadStream(nil);
  FWTFile := AStream.ReadStream(nil);
  (FSbData as IisInterfacedObject).ReadFromStream(AStream);
  (FRequestParams as IisInterfacedObject).ReadFromStream(AStream);
  (FCompanies as IisInterfacedObject).ReadFromStream(AStream);
  FNextCompanyIndex := AStream.ReadInteger;
  FHSA := AStream.ReadBoolean;
end;

procedure TEvCashManagementTask.SetACHDataStream(const AValue: IisStream);
begin
  FACHDataStream := AValue;
end;

procedure TEvCashManagementTask.SetBlockNegativeChecks(const AValue: Boolean);
begin
  FBlockNegativeChecks := AValue;
end;

procedure TEvCashManagementTask.SetCombine(const AValue: Integer);
begin
  FCombine := AValue;
end;

procedure TEvCashManagementTask.SetCTS(const AValue: Boolean);
begin
  FCTS := AValue;
end;

procedure TEvCashManagementTask.SetIncludeOffsets(const AValue: Integer);
begin
  FIncludeOffsets := AValue;
end;

procedure TEvCashManagementTask.SetPeriodFrom(const AValue: TDateTime);
begin
  FPeriodFrom := AValue;
end;

procedure TEvCashManagementTask.SetPeriodTo(const AValue: TDateTime);
begin
  FPeriodTo := AValue;
end;

procedure TEvCashManagementTask.SetPreprocess(const AValue: Boolean);
begin
  FPreprocess := AValue;
end;

procedure TEvCashManagementTask.SetReversalACH(const AValue: Boolean);
begin
  FReversalACH := AValue;
end;

procedure TEvCashManagementTask.SetSB_BANK_ACCOUNTS_NBR(
  const AValue: Integer);
begin
  FSB_BANK_ACCOUNTS_NBR := AValue;
end;

procedure TEvCashManagementTask.SetStatusFromIndex(const AValue: Integer);
begin
  FStatusFromIndex := AValue;
end;

procedure TEvCashManagementTask.SetStatusToIndex(const AValue: Integer);
begin
  FStatusToIndex := AValue;
end;

procedure TEvCashManagementTask.SetThreshold(const AValue: Currency);
begin
  FThreshold := AValue;
end;

procedure TEvCashManagementTask.SetUseSBEIN(const AValue: Boolean);
begin
  FUseSBEIN := AValue;
end;

procedure TEvCashManagementTask.WriteSelfToStream(const AStream: IisStream);
begin
  inherited;
  AStream.WriteStream(FACHDataStream);
  (FOptions as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteBoolean(FReversalACH);
  AStream.WriteDouble(FPeriodFrom);
  AStream.WriteDouble(FPeriodTo);
  AStream.WriteInteger(FStatusFromIndex);
  AStream.WriteInteger(FStatusToIndex);
  AStream.WriteBoolean(FBlockNegativeChecks);
  AStream.WriteBoolean(FBlockCompanyDD);
  AStream.WriteBoolean(FBlockNegativeLiabilities);
  AStream.WriteInteger(FIncludeOffsets);
  AStream.WriteInteger(FSB_BANK_ACCOUNTS_NBR);
  AStream.WriteInteger(FCombine);
  AStream.WriteCurrency(FThreshold);
  AStream.WriteBoolean(FUseSBEIN);
  AStream.WriteBoolean(FCTS);
  AStream.WriteBoolean(FPreprocess);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchRegularReport);
  AStream.WriteStream(FAchDetailedReport);
  AStream.WriteStream(FAchSave);
  AStream.WriteStream(FNonAchReport);
  AStream.WriteStream(FWTFile);
  (FSbData as IisInterfacedObject).WriteToStream(AStream);
  (FRequestParams as IisInterfacedObject).WriteToStream(AStream);
  (FCompanies as IisInterfacedObject).WriteToStream(AStream);
  AStream.WriteInteger(FNextCompanyIndex);
  AStream.WriteBoolean(FHSA);
end;

procedure TEvCashManagementTask.AfterTaskFinished;
begin
  inherited;
  Assert(mb_GlobalFlagsManager.TryUnlock(GF_SB_ACH_PROCESS));
end;

function TEvCashManagementTask.GetNonAchReport: IisStream;
begin
  Result := FNonAchReport;
end;

function TEvCashManagementTask.GetWTFile: IisStream;
begin
  Result := FWTFile;
end;

function TEvCashManagementTask.GetBlockCompanyDD: Boolean;
begin
  Result := FBlockCompanyDD;
end;

procedure TEvCashManagementTask.SetBlockCompanyDD(const AValue: Boolean);
begin
  FBlockCompanyDD := AValue;
end;

function TEvCashManagementTask.GetBlockNegativeLiabilities: Boolean;
begin
  Result := FBlockNegativeLiabilities;
end;

procedure TEvCashManagementTask.SetBlockNegativeLiabilities(
  const AValue: Boolean);
begin
  FBlockNegativeLiabilities := AValue;
end;

function TEvCashManagementTask.GetMaxRequestCount: Integer;
begin
//  Result := GetMaxThreads;
  Result := 8;
end;
function TEvCashManagementTask.GetHSA: Boolean;
begin
  Result := FHSA;
end;

procedure TEvCashManagementTask.SetHSA(const AValue: Boolean);
begin
  FHSA := AValue;
end;

procedure TEvCashManagementTask.DoRestoreTaskState(
  const AStream: IevDualStream);
begin
  inherited;
  FNextCompanyIndex := AStream.ReadInteger;
end;

procedure TEvCashManagementTask.DoStoreTaskState(
  const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteInteger(FNextCompanyIndex);
end;

initialization
  ObjectFactory.Register(TEvCashManagementTask);

finalization
  SafeObjectFactoryUnRegister(TEvCashManagementTask);

end.

