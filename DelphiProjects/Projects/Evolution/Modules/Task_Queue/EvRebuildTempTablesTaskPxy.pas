unit EvRebuildTempTablesTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

uses Classes;

type
  TEvRebuildTempTablesTaskProxy = class(TEvCustomTaskProxy, IevRebuildTempTablesTask)
  private
    FFullRebuild: Boolean;
    FUseRange: Boolean;
    FUseNumbers: Boolean;
    FFromClient: Integer;
    FToClient: Integer;
    FClientNumbers: String;
  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetFullRebuild: Boolean;
    procedure SetFullRebuild(const AValue: Boolean);
    function  GetUseRange: Boolean;
    procedure SetUseRange(const AValue: Boolean);
    function  GetUseNumbers: Boolean;
    procedure SetUseNumbers(const AValue: Boolean);
    function  GetFromClient: Integer;
    procedure SetFromClient(const AValue: Integer);
    function  GetToClient: Integer;
    procedure SetToClient(const AValue: Integer);
    function  GetClientNumbers: String;
    procedure SetClientNumbers(const AValue: String);
  public
    class function GetTypeID: String; override;
  end;

{ TEvRebuildTempTablesTaskProxy }

function TEvRebuildTempTablesTaskProxy.GetClientNumbers: String;
begin
  Result := FClientNumbers;
end;

function TEvRebuildTempTablesTaskProxy.GetFromClient: Integer;
begin
  Result := FFromClient;
end;

function TEvRebuildTempTablesTaskProxy.GetFullRebuild: Boolean;
begin
  Result := FFullRebuild;
end;

function TEvRebuildTempTablesTaskProxy.GetToClient: Integer;
begin
  Result := FToClient;
end;

class function TEvRebuildTempTablesTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_REBUILD_TMP_TABLES;
end;

function TEvRebuildTempTablesTaskProxy.GetUseNumbers: Boolean;
begin
  Result := FUseNumbers;
end;

function TEvRebuildTempTablesTaskProxy.GetUseRange: Boolean;
begin
  Result := FUseRange;
end;

procedure TEvRebuildTempTablesTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FFullRebuild := AStream.ReadBoolean;
  FUseRange := AStream.ReadBoolean;
  FUseNumbers := AStream.ReadBoolean;
  FFromClient := AStream.ReadInteger;
  FToClient := AStream.ReadInteger;
  FClientNumbers := AStream.ReadString;
end;

procedure TEvRebuildTempTablesTaskProxy.SetClientNumbers(const AValue: String);
begin
  FClientNumbers := AValue;
end;

procedure TEvRebuildTempTablesTaskProxy.SetFromClient(const AValue: Integer);
begin
  FFromClient := AValue;
end;

procedure TEvRebuildTempTablesTaskProxy.SetFullRebuild(const AValue: Boolean);
begin
  FFullRebuild := AValue;
end;

procedure TEvRebuildTempTablesTaskProxy.SetToClient(const AValue: Integer);
begin
  FToClient := AValue;
end;

procedure TEvRebuildTempTablesTaskProxy.SetUseNumbers(const AValue: Boolean);
begin
  FUseNumbers := AValue;
end;

procedure TEvRebuildTempTablesTaskProxy.SetUseRange(const AValue: Boolean);
begin
  FUseRange := AValue;
end;

procedure TEvRebuildTempTablesTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FFullRebuild);
  AStream.WriteBoolean(FUseRange);
  AStream.WriteBoolean(FUseNumbers);
  AStream.WriteInteger(FFromClient);
  AStream.WriteInteger(FToClient);
  AStream.WriteString(FClientNumbers);
end;

initialization
  ObjectFactory.Register(TEvRebuildTempTablesTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TEvRebuildTempTablesTaskProxy);


end.
