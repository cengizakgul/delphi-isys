unit EvRebuildVMRHistoryTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;


implementation

{%File 'EvRebuildVMRHistoryTask.jpg'}

type
  TevRebuildVMRHistoryTask = class(TevCustomTask, IevRebuildVMRHistoryTask)
  private
    FPeriodBegDate: TDateTime;
    FPeriodEndDate: TDateTime;
    FCoList: IisStringList;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_RebuildVMRHistory(const AInputRequests: TevTaskRequestList);
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;

    function  GetPeriodBegDate: TDateTime;
    procedure SetPeriodBegDate(const AValue: TDateTime);
    function  GetPeriodEndDate: TDateTime;
    procedure SetPeriodEndDate(const AValue: TDateTime);
    function  GetCoList: IisStringList;
  public
    class function GetTypeID: String; override;
  end;


{ TevRebuildVMRHistoryTask }

procedure TevRebuildVMRHistoryTask.DoOnConstruction;
begin
  inherited;
  FCoList := TisStringList.CreateUnique;
end;

function TevRebuildVMRHistoryTask.GetCoList: IisStringList;
begin
  Result := FCoList;
end;

function TevRebuildVMRHistoryTask.GetPeriodBegDate: TDateTime;
begin
  Result := FPeriodBegDate;
end;

function TevRebuildVMRHistoryTask.GetPeriodEndDate: TDateTime;
begin
  Result := FPeriodEndDate;
end;

class function TevRebuildVMRHistoryTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_REBUILD_VMR_HIST;
end;

procedure TevRebuildVMRHistoryTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('CoList', FCoList);

  AddRequest('Prepare companies', 'PrepareCompsRebuildVMRHist',  Params, 'RebuildVMRHistory.Fork');
end;

procedure TevRebuildVMRHistoryTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    AddException(AInputRequests[i].GetExceptions);
    AddWarning(AInputRequests[i].GetWarnings);
  end;

  inherited;
end;

procedure TevRebuildVMRHistoryTask.Phase_RebuildVMRHistory(const AInputRequests: TevTaskRequestList);
var
  CoInfoList: IisStringList;
  i: Integer;
  Params: IisListOfValues;
  s: String;
begin
  CoInfoList := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IisStringList;

  for i := 0 to CoInfoList.Count - 1 do
  begin
    s := CoInfoList[i];

    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', StrToInt(GetNextStrValue(s, ',')));
    Params.AddValue('CoNbr', StrToInt(GetNextStrValue(s, ',')));
    Params.AddValue('PeriodBegDate', FPeriodBegDate);
    Params.AddValue('PeriodEndDate', FPeriodEndDate);

    AddRequest('Company ' + s, 'RebuildVMRHistory',  Params);
  end;
end;

procedure TevRebuildVMRHistoryTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IevDualStream;
begin
  inherited;
  FPeriodBegDate := AStream.ReadDouble;
  FPeriodEndDate := AStream.ReadDouble;

  S := AStream.ReadStream(S);
  (FCoList as IisInterfacedObject).AsStream := S;
end;

procedure TevRebuildVMRHistoryTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('RebuildVMRHistory', Phase_RebuildVMRHistory);
end;

procedure TevRebuildVMRHistoryTask.SetPeriodBegDate(const AValue: TDateTime);
begin
  FPeriodBegDate := AValue;
end;

procedure TevRebuildVMRHistoryTask.SetPeriodEndDate(const AValue: TDateTime);
begin
  FPeriodEndDate := AValue;
end;

procedure TevRebuildVMRHistoryTask.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IevDualStream;
begin
  inherited;
  AStream.WriteDouble(FPeriodBegDate);
  AStream.WriteDouble(FPeriodEndDate);

  S := (FCoList as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);
end;

initialization
  ObjectFactory.Register(TevRebuildVMRHistoryTask);

finalization
  SafeObjectFactoryUnRegister(TevRebuildVMRHistoryTask);

end.
