unit EvPreprocessPayrollTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard;

implementation

type
  TEvPreprocessPayrollTask = class(TEvCustomTask, IevPreprocessPayrollTask)
  private
    FPrList: string;
    FPRMessage: string;
    FPrInfo: IisListOfValues;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_PreprocessPR(const AInputRequests: TevTaskRequestList);
    procedure  Phase_ShowResult(const AInputRequests: TevTaskRequestList);

    function  GetPrList: string;
    function  GetPRMessage: string;
    procedure SetPrList(const AValue: string);
    procedure SetPRMessage(const AValue: string);
  public
    class function GetTypeID: String; override;
  end;


{ TEvPreprocessPayrollTask }

procedure TEvPreprocessPayrollTask.DoOnConstruction;
begin
  inherited;
  FPrInfo := TisListOfValues.Create;
end;

procedure TEvPreprocessPayrollTask.DoRestoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  FPRMessage := AStream.ReadString;
  FPrInfo.ReadFromStream(AStream);  
end;

procedure TEvPreprocessPayrollTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteString(FPRMessage);
  FPrInfo.WriteToStream(AStream);  
end;

function TEvPreprocessPayrollTask.GetPrList: string;
begin
  Result := FPrList; 
end;

function TEvPreprocessPayrollTask.GetPRMessage: string;
begin
  Result := FPRMessage;
end;

class function TEvPreprocessPayrollTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PREPROCESS_PAYROLL;
end;

procedure TEvPreprocessPayrollTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('AllPayrolls', False);
  Params.AddValue('PayrollAge', 0);
  Params.AddValue('PrList', FPrList);
  Params.AddValue('CoList', '');

  AddRequest('Prepare for preprocessing', 'PrepareForPayrollProcessing',  Params, 'PreprocessPR.Fork');
end;

procedure TEvPreprocessPayrollTask.Phase_ShowResult(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  R: IevTaskRequest;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    R := AInputRequests[i];
    AddException(R.GetExceptions);
    AddWarning(R.GetWarnings);

    FPRMessage := FPRMessage + R.RequestName + '; Time: ' + FormatDateTime('hh:nn:ss', R.FinishedAt - R.StartedAt) + #13#10;

    if (R.GetWarnings = '') and (R.GetExceptions = '') then
      FPRMessage := FPRMessage  + 'Success!'#13#10
    else
    begin
      if R.GetExceptions <> '' then
        FPRMessage := FPRMessage + 'Error messages:'#13#10' ' + R.GetExceptions + #13#10;

      if R.GetWarnings <> '' then
        FPRMessage := FPRMessage + 'Warning messages:'#13#10 + R.GetWarnings + #13#10;
    end;
  end;
end;

procedure TEvPreprocessPayrollTask.Phase_PreprocessPR(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  Params: IisListOfValues;
  OnePrInfo: IisListOfValues;
begin
  StopOnExceptions(AInputRequests);

  // Comes from "GetPayrollsForProcessing" call
  FPrInfo.Clear;
  for i := 0 to AInputRequests[0].Results.Count - 1 do
    if StartsWith(AInputRequests[0].Results[i].Name, 'PrInfo') then
      FPrInfo.AddValue(AInputRequests[0].Results[i].Name, AInputRequests[0].Results[i].Value);

  FPrList := AInputRequests[0].Results.Value['PrList'];

  //Add payroll processing requests
  for i := 0 to FPrInfo.Count - 1 do
  begin
    OnePrInfo := IInterface(FPrInfo[i].Value) as IisListOfValues;

    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', OnePrInfo.Value['ClNbr']);
    Params.AddValue('PayrollNumber', OnePrInfo.Value['PrNbr']);

    if OnePrInfo.ValueExists('PrBatchNbr') then
    begin
      Params.AddValue('PrBatchNbr', OnePrInfo.Value['PrBatchNbr']);
      Params.AddValue('Caption', VarToStr(OnePrInfo.Value['CompanyName']) + ' ' +
                                 VarToStr(OnePrInfo.Value['CheckDate']) + '-' +
                                 VarToStr(OnePrInfo.Value['RunNumber']) + '-' +
                                 VarToStr(OnePrInfo.Value['PrBatchNbr']));
    end
    else
      Params.AddValue('Caption', VarToStr(OnePrInfo.Value['CompanyName']) + ' ' +
                                 VarToStr(OnePrInfo.Value['CheckDate']) + '-' +
                                 VarToStr(OnePrInfo.Value['RunNumber']));

    Params.AddValue('JustPreProcess', True);
    Params.AddValue('CheckReturnQueue', True);
    Params.AddValue('LockType', Ord(rlDoNotLock));

    AddRequest('Preprocessing ' + Params.Value['Caption'], 'ProcessPayroll', Params, 'ShowResult.Fork',
      GF_CL_PAYROLL_OPERATION + '.' + String(OnePrInfo.Value['ClNbr']) + ',' +
      GF_CO_PAYROLL_OPERATION + '.' + String(OnePrInfo.Value['ClNbr']) + '_' + String(OnePrInfo.Value['CoNbr']));
  end;
end;

procedure TEvPreprocessPayrollTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FPrList :=AStream.ReadString;
  FPRMessage := AStream.ReadString;
  FPrInfo.ReadFromStream(AStream);  
end;

procedure TEvPreprocessPayrollTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('PreprocessPR', Phase_PreprocessPR);
  RegisterTaskPhase('ShowResult', Phase_ShowResult)
end;

procedure TEvPreprocessPayrollTask.SetPrList(const AValue: string);
begin
  FPrList := AValue;
end;

procedure TEvPreprocessPayrollTask.SetPRMessage(const AValue: string);
begin
  FPRMessage := AValue;
end;

procedure TEvPreprocessPayrollTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteString(FPrList);
  AStream.WriteString(FPRMessage);
  FPrInfo.WriteToStream(AStream);  
end;

initialization
  ObjectFactory.Register(TEvPreprocessPayrollTask);

finalization
  SafeObjectFactoryUnRegister(TEvPreprocessPayrollTask);
end.
