unit EvPALiabilitiesTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

type
  TevPALiabilitiesTask = class(TEvCustomTask, IevPALiabilitiesTask)
  private
    FCompanies: IisParamsCollection;
  protected
    function GetCompanies: IisParamsCollection;
    procedure DoOnConstruction; override;
    procedure WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;

{ TevPALiabilitiesTask }

procedure TevPALiabilitiesTask.DoOnConstruction;
begin
  inherited;
  FCompanies := TisParamsCollection.Create;
  FDaysStayInQueue := 10;
end;

function TevPALiabilitiesTask.GetCompanies: IisParamsCollection;
begin
  Result := FCompanies;
end;

class function TevPALiabilitiesTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PA_LIABILITY_UTILITY;
end;

procedure TevPALiabilitiesTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
  i: Integer;
begin
  inherited;
  SetProgressText('Running PA Utility.');
  for i := 0 to FCompanies.Count - 1 do
  begin
    Params := TisListOfValues.Create;
    Params.AddValue('CL_NBR', FCompanies[i].Value['CL_NBR']);
    Params.AddValue('CO_NBR', FCompanies[i].Value['CO_NBR']);
    Params.AddValue('CUSTOM_COMPANY_NUMBER', FCompanies[i].Value['CUSTOM_COMPANY_NUMBER']);
    AddRequest('Processing company '+FCompanies[i].Value['CUSTOM_COMPANY_NUMBER'], 'PAUtility',  Params);
  end;
end;

procedure TevPALiabilitiesTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    AddException(AInputRequests[i].GetExceptions);
    AddWarning(AInputRequests[i].GetWarnings);
  end;
  inherited;
end;

procedure TevPALiabilitiesTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FCompanies as IisInterfacedObject).ReadFromStream(AStream);
end;

procedure TevPALiabilitiesTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FCompanies as IisInterfacedObject).WriteToStream(AStream);
end;

initialization
  ObjectFactory.Register(TevPALiabilitiesTask);

finalization
  SafeObjectFactoryUnRegister(TevPALiabilitiesTask);

end.
