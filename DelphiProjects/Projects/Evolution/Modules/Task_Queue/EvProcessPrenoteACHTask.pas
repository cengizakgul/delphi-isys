unit EvProcessPrenoteACHTask;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTask,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvContext;

implementation

type
  TEvProcessPrenoteACHTask = class(TEvCustomTask, IevProcessPrenoteACHTask)
  private
    FACHOptions: IevACHOptions;
    FPrenoteCLAccounts: Boolean;
    FACHDataStream: IEvDualStream;
    FAchFile: IEvDualStream;
    FAchRegularReport: IEvDualStream;
    FAchDetailedReport: IEvDualStream;
    FAchSave: IEvDualStream;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    function   BeforeTaskStart: Boolean; override;
    procedure  AfterTaskFinished; override;

    function  GetACHOptions: IevACHOptions;
    function  GetACHDataStream: IEvDualStream;
    procedure SetACHDataStream(const AValue: IEvDualStream);
    function  GetPrenoteCLAccounts: Boolean;
    procedure SetPrenoteCLAccounts(const AValue: Boolean);
    function  GetAchFile: IEvDualStream;
    function  GetAchRegularReport: IEvDualStream;
    function  GetAchDetailedReport: IEvDualStream;
    function  GetAchSave: IEvDualStream;

    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_End(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;

{ TEvProcessPrenoteACHTask }

procedure TEvProcessPrenoteACHTask.DoOnConstruction;
begin
  inherited;
  FACHOptions := TevACHOptions.Create;
  FACHDataStream := TEvDualStreamHolder.Create;
  FDaysStayInQueue := 10;
end;

function TEvProcessPrenoteACHTask.GetACHDataStream: IEvDualStream;
begin
  Result := FACHDataStream;
end;

function TEvProcessPrenoteACHTask.GetAchDetailedReport: IEvDualStream;
begin
  Result := FAchDetailedReport;
end;

function TEvProcessPrenoteACHTask.GetAchFile: IEvDualStream;
begin
  Result := FAchFile;
end;

function TEvProcessPrenoteACHTask.GetAchRegularReport: IEvDualStream;
begin
  Result := FAchRegularReport;
end;

function TEvProcessPrenoteACHTask.GetAchSave: IEvDualStream;
begin
  Result := FAchSave;
end;

class function TEvProcessPrenoteACHTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_PRENOTE_ACH;
end;

function TEvProcessPrenoteACHTask.BeforeTaskStart: Boolean;
begin
  Result := inherited BeforeTaskStart and mb_GlobalFlagsManager.TryLock(GF_SB_ACH_PROCESS);
end;

procedure TEvProcessPrenoteACHTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('ACHOptions', FACHOptions);
  Params.AddValue('ACHFolder', Context.DomainInfo.FileFolders.ACHFolder);
  Params.AddValue('PrenoteCLAccounts', FPrenoteCLAccounts);
  Params.AddValue('ACHDataStream', FACHDataStream);

  AddRequest(GetCaption, 'ProcessPrenoteACH',  Params);
end;

procedure TEvProcessPrenoteACHTask.Phase_End(const AInputRequests: TevTaskRequestList);
var
  ACHFolder: String;
  i: Integer;
  ACHFiles: IIsListOfValues;
begin
  if Length(AInputRequests) > 0 then
  begin
    AddException(AInputRequests[0].GetExceptions);
    AddWarning(AInputRequests[0].GetWarnings);

    if AInputRequests[0].GetExceptions = '' then
    begin
      FAchFile := IInterface(AInputRequests[0].Results.Value['AchFile']) as IevDualStream;
      FAchRegularReport := IInterface(AInputRequests[0].Results.Value['AchRegularReport']) as IevDualStream;
      FAchDetailedReport := IInterface(AInputRequests[0].Results.Value['AchDetailedReport']) as IevDualStream;
      ACHFiles := IInterface(AInputRequests[0].Results.Value['AchSave']) as IisListOfValues;

      if Assigned(ACHFiles) and (ACHFiles.Count > 0) then
      begin
        FAchSave := TevDualStreamHolder.CreateInMemory;
        (IInterface(ACHFiles[0].Value) as IisStringList).SaveToStream(FAchSave.RealStream);
      end;

      ACHFolder := Context.DomainInfo.FileFolders.ACHFolder;
      if Length(ACHFolder) > 0 then
      begin
        if Assigned(FAchSave) and ForceDirectories(ACHFolder) then
          if Assigned(ACHFiles) then
            for i := 0 to ACHFiles.Count - 1 do
              (IInterface(ACHFiles[i].Value) as IisStringList).SaveToFile(ACHFolder + '\' + ExtractFileName(ACHFiles[i].Name));
      end;

      if Assigned(ACHFiles) and (ACHFiles.Count > 0) then
        if ACHFiles.Count > 1 then
        begin
          FAchSave := nil;
          if Length(ACHFolder) > 0 then
            AddException(Format('%d ACH files were saved into default ACH directory %s',[ACHFiles.Count, ACHFolder]))
          else
            AddException(Format('%d ACH files were created but not saved',[ACHFiles.Count, ACHFolder]));
        end;

    end;
  end;

  inherited;
end;

procedure TEvProcessPrenoteACHTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).ReadFromStream(AStream);
  FACHDataStream := AStream.ReadStream(nil);
  FPrenoteCLAccounts := AStream.ReadBoolean;
  FAchFile := AStream.ReadStream(nil, True);
  FAchRegularReport := AStream.ReadStream(nil, True);
  FAchDetailedReport := AStream.ReadStream(nil, True);
  FAchSave := AStream.ReadStream(nil, True);
end;

procedure TEvProcessPrenoteACHTask.SetACHDataStream(const AValue: IEvDualStream);
begin
  FACHDataStream := AValue;
end;

procedure TEvProcessPrenoteACHTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  (FACHOptions as IisInterfacedObject).WriteToStream(AStream);  
  AStream.WriteStream(FACHDataStream);
  AStream.WriteBoolean(FPrenoteCLAccounts);
  AStream.WriteStream(FAchFile);
  AStream.WriteStream(FAchRegularReport);
  AStream.WriteStream(FAchDetailedReport);
  AStream.WriteStream(FAchSave);
end;

procedure TEvProcessPrenoteACHTask.AfterTaskFinished;
begin
  inherited;
  Assert(mb_GlobalFlagsManager.TryUnlock(GF_SB_ACH_PROCESS));
end;

function TEvProcessPrenoteACHTask.GetPrenoteCLAccounts: Boolean;
begin
  Result := FPrenoteCLAccounts;
end;

procedure TEvProcessPrenoteACHTask.SetPrenoteCLAccounts(
  const AValue: Boolean);
begin
  FPrenoteCLAccounts := AValue;
end;

function TEvProcessPrenoteACHTask.GetACHOptions: IevACHOptions;
begin
  Result := FACHOptions;
end;

initialization
  ObjectFactory.Register(TEvProcessPrenoteACHTask);

finalization
  SafeObjectFactoryUnRegister(TEvProcessPrenoteACHTask);

end.
