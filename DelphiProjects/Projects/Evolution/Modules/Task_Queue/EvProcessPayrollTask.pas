unit EvProcessPayrollTask;

interface

uses SysUtils, Variants, isBaseClasses, EvCustomTask, EvCommonInterfaces, EvTypes, EvStreamUtils,
     EvMainboard, EvConsts, isBasicUtils, EvContext;

implementation

type
  TEvProcessPayrollTask = class(TEvPrintableTask, IevProcessPayrollTask)
  private
    FAllPayrolls: Boolean;
    FPayrollAge: TDateTime;
    FPrList: string;
    FPrintChecks: Boolean;
    FPRMessage: string;
    FCoList: string;
    FEmailTo: String;
    FPrInfo: IisListOfValues;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;
    function   AlwaysSendToVMR: Boolean; override;

    procedure  RegisterTaskPhases; override;
    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_ProcessPR(const AInputRequests: TevTaskRequestList);
    procedure  Phase_RunReports(const AInputRequests: TevTaskRequestList);
    procedure  Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList); override;

    function  GetAllPayrolls: Boolean;
    function  GetCoList: string;
    function  GetPayrollAge: TDateTime;
    function  GetPrintChecks: Boolean;
    function  GetPrList: string;
    function  GetPRMessage: string;
    procedure SetAllPayrolls(const AValue: Boolean);
    procedure SetCoList(const AValue: string);
    procedure SetPayrollAge(const AValue: TDateTime);
    procedure SetPrintChecks(const AValue: Boolean);
    procedure SetPrList(const AValue: string);
    procedure SetPRMessage(const AValue: string);
  public
    class function GetTypeID: String; override;
  end;


{ TEvProcessPayrollTask }

procedure TEvProcessPayrollTask.DoOnConstruction;
begin
  inherited;
  FPrInfo := TisListOfValues.Create;
  FPrintChecks := True;
  SetDestination(rdtPreview);
end;

procedure TEvProcessPayrollTask.DoRestoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  FPrList :=AStream.ReadString;
  FPRMessage := AStream.ReadString;
  FEmailTo := AStream.ReadShortString;
  FPrInfo.ReadFromStream(AStream);
end;

procedure TEvProcessPayrollTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteString(FPrList);
  AStream.WriteString(FPRMessage);
  AStream.WriteShortString(FEmailTo);
  FPrInfo.WriteToStream(AStream);
end;

function TEvProcessPayrollTask.GetAllPayrolls: Boolean;
begin
  Result := FAllPayrolls;
end;

function TEvProcessPayrollTask.GetCoList: string;
begin
  Result := FCoList;
end;

function TEvProcessPayrollTask.GetPayrollAge: TDateTime;
begin
  Result := FPayrollAge;
end;

function TEvProcessPayrollTask.GetPrintChecks: Boolean;
begin
  Result := FPrintChecks;
end;

function TEvProcessPayrollTask.GetPrList: string;
begin
  Result := FPrList;
end;

function TEvProcessPayrollTask.GetPRMessage: string;
begin
  Result := FPRMessage;
end;


class function TEvProcessPayrollTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_PAYROLL;
end;

procedure TEvProcessPayrollTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params: IisListOfValues;
begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('AllPayrolls', FAllPayrolls);
  Params.AddValue('PayrollAge', FPayrollAge);
  Params.AddValue('PrList', FPrList);
  Params.AddValue('CoList', FCoList);

  AddRequest('Prepare for processing', 'PrepareForPayrollProcessing',  Params, 'ProcessPR.Fork');
end;

procedure TEvProcessPayrollTask.Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  R: IevTaskRequest;
  RepRes: IEvDualStream;
  V: IisNamedValue;
begin
  for i := Low(AInputRequests) to High(AInputRequests) do
  begin
    R := AInputRequests[i];
    AddException(R.GetExceptions);
    AddWarning(R.GetWarnings);
    V := R.Results.FindValue(PARAM_RESULT);
    if Assigned(V) then
      RepRes := IInterface(V.Value) as IEvDualStream
    else
      RepRes := nil;

    if Assigned(RepRes) then
    begin
      FPRMessage := FPRMessage + R.RequestName + '; Time: ' + FormatDateTime('hh:nn:ss', R.FinishedAt - R.StartedAt) + #13#10;

      if R.GetExceptions <> '' then
        FPRMessage := FPRMessage + 'Error messages:'#13#10' ' + R.GetExceptions + #13#10;

      if R.GetWarnings <> '' then
        FPRMessage := FPRMessage + 'Warning messages:'#13#10 + R.GetWarnings + #13#10;
    end;
  end;

  inherited;
//The following code should be existed in TEvPrintableTask.Call_ProcessReportResults
(*  if (ctx_VmrEngine <> nil) and Context.License.VMR then
  begin
    EI :=  TisStringList.Create;
    for i := Low(AInputRequests) to High(AInputRequests) do
      if AInputRequests[i].Results.ValueExists('CHECKS_EXTRA_INFO') then
        EI.AddStrings(IInterface(AInputRequests[i].Results.Value['CHECKS_EXTRA_INFO']) as IisStringList);
    ctx_VmrEngine.SetChecksExtraInfo(EI);
  end;
*)
end;

procedure TEvProcessPayrollTask.Phase_ProcessPR(const AInputRequests: TevTaskRequestList);
var
  i: Integer;
  Params: IisListOfValues;
  OnePrInfo: IisListOfValues;
begin
  StopOnExceptions(AInputRequests);

  // Comes from "GetPayrollsForProcessing" call
  FPrInfo.Clear;
  for i := 0 to AInputRequests[0].Results.Count - 1 do
    if StartsWith(AInputRequests[0].Results[i].Name, 'PrInfo') then
      FPrInfo.AddValue(AInputRequests[0].Results[i].Name, AInputRequests[0].Results[i].Value);

  FPrList := AInputRequests[0].Results.Value['PrList'];
  FEmailTo := AInputRequests[0].Results.Value['EmailTo'];

  //Add payroll processing requests
  for i := 0 to FPrInfo.Count - 1 do
  begin
    OnePrInfo := IInterface(FPrInfo[i].Value) as IisListOfValues;

    Params := TisListOfValues.Create;
    Params.AddValue('Caption', VarToStr(OnePrInfo.Value['CompanyName']) + ' ' +
                               VarToStr(OnePrInfo.Value['CheckDate']) + '-' +
                               VarToStr(OnePrInfo.Value['RunNumber']));
    Params.AddValue('ClientID', OnePrInfo.Value['ClNbr']);
    Params.AddValue('PayrollNumber', OnePrInfo.Value['PrNbr']);
    Params.AddValue('JustPreProcess', False);
    Params.AddValue('CheckReturnQueue', True);
    Params.AddValue('LockType', Ord(rlDoNotLock));

    AddRequest('Processing ' + Params.Value['Caption'], 'ProcessPayroll', Params, 'RunReports.Fork',
      GF_CL_PAYROLL_OPERATION + '.' + String(OnePrInfo.Value['ClNbr']) + ',' +
      GF_CO_PAYROLL_OPERATION + '.' + String(OnePrInfo.Value['ClNbr']) + '_' + String(OnePrInfo.Value['CoNbr']));
  end;
end;

procedure TEvProcessPayrollTask.Phase_RunReports(const AInputRequests: TevTaskRequestList);
var
  R: IevTaskRequest;
  s: String;
  Params: IisListOfValues;
  sEMailHeader, sEMailBody: string;
begin
  R := AInputRequests[0];

  if R.GetExceptions = '' then
  begin
    // If there are no errors
    AddWarning(R.GetWarnings);

    FPRMessage := FPRMessage + R.RequestName + '; Time: ' + FormatDateTime('hh:nn:ss', R.FinishedAt - R.StartedAt) + #13#10;

    if R.GetWarnings <> '' then
      FPRMessage := FPRMessage + 'Warning messages:'#13#10 + R.GetWarnings + #13#10;

    s := R.Results.Value['Log'];
    if s <> '' then
      AddNotes(s);

    // Run Reports
    Params := TisListOfValues.Create;
    Params.AddValue('ClientID', R.Params.Value['ClientID']);
    Params.AddValue('PrNbr', R.Params.Value['PayrollNumber']);
    Params.AddValue('PrintChecks', FPrintChecks);
    Params.AddValue('UseSbMiskCheckForm', False);

    AddRequest('Calculating Checks/Reports for ' + R.Params.Value['Caption'], 'PrintPayroll', Params, 'ProcessReportResults.Fork');
  end

  else
  begin
    AddException(R.GetExceptions);
    AddWarning(R.GetWarnings);
  end;

  // Send special email notification
  if ((GetWarnings <> '') or (GetExceptions <> '')) and (FEmailTo <> '') then
  begin
    if GetExceptions = '' then
    begin
      sEmailHeader := 'Payroll Processing Warnings';
      sEmailBody := Trim(Trim(FPRMessage) + #10#13) + 'Warning(s): ' + #13#10 + Trim(GetWarnings);
    end
    else
    begin
      if GetWarnings = '' then
      begin
        sEmailHeader := 'Payroll Processing Errors';
        sEmailBody := Trim(Trim(FPRMessage) + #10#13) + 'Error(s): ' + #13#10 + Trim(GetExceptions);
      end
      else
      begin
        sEmailHeader := 'Payroll Processing Errors and Warnings';
        sEmailBody := Trim(Trim(FPRMessage) + #10#13) +
          'Error(s): ' + #13#10 + Trim(GetExceptions) + #10#13#10#13 +
          'Warning(s): ' + #13#10 + Trim(GetWarnings);
      end;
    end;

    SendEmail(FEmailTo, sEMailHeader, sEMailBody);
  end;
end;

procedure TEvProcessPayrollTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FAllPayrolls := AStream.ReadBoolean;
  FPayrollAge := AStream.ReadDouble;
  FPrList :=AStream.ReadString;
  FPrintChecks := AStream.ReadBoolean;
  FPRMessage := AStream.ReadString;
  FCoList := AStream.ReadString;
  FPrInfo.ReadFromStream(AStream);
end;

procedure TEvProcessPayrollTask.RegisterTaskPhases;
begin
  inherited;
  RegisterTaskPhase('ProcessPR', Phase_ProcessPR);
  RegisterTaskPhase('RunReports', Phase_RunReports);
end;

procedure TEvProcessPayrollTask.SetAllPayrolls(const AValue: Boolean);
begin
  FAllPayrolls := AValue;
end;

procedure TEvProcessPayrollTask.SetCoList(const AValue: string);
begin
  FCoList := AValue;
end;

procedure TEvProcessPayrollTask.SetPayrollAge(const AValue: TDateTime);
begin
  FPayrollAge := AValue;
end;

procedure TEvProcessPayrollTask.SetPrintChecks(const AValue: Boolean);
begin
  FPrintChecks := AValue;
end;

procedure TEvProcessPayrollTask.SetPrList(const AValue: string);
begin
  FPrList := AValue;
end;

procedure TEvProcessPayrollTask.SetPRMessage(const AValue: string);
begin
  FPRMessage := AValue;
end;

procedure TEvProcessPayrollTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteBoolean(FAllPayrolls);
  AStream.WriteDouble(FPayrollAge);
  AStream.WriteString(FPrList);
  AStream.WriteBoolean(FPrintChecks);
  AStream.WriteString(FPRMessage);
  AStream.WriteString(FCoList);
  FPrInfo.WriteToStream(AStream);
end;

function TEvProcessPayrollTask.AlwaysSendToVMR: Boolean;
begin
  Result := True;
end;

initialization
  ObjectFactory.Register(TEvProcessPayrollTask);

finalization
  SafeObjectFactoryUnRegister(TEvProcessPayrollTask);

end.
