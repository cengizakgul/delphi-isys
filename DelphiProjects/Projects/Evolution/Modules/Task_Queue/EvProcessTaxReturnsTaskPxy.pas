unit EvProcessTaxReturnsTaskPxy;

interface

uses SysUtils, Variants, IsBaseClasses, IsBasicUtils, EvCommonInterfaces, EvCustomTaskPxy,
     EvStreamUtils, EvTypes, EvConsts, EvMainboard, EvClasses, EvBasicUtils;


implementation

type
  TevProcessTaxReturnsTaskProxy = class(TevCustomTaskProxy, IevProcessTaxReturnsTask)
  private
    FBegDate: TDateTime;
    FEndDate: TDateTime;
    FFilter: IisStringList;
    FTaxFilter: TProcessTaxReturnsFilterSelections;
    FEeSortMode: TProcessTaxReturnsEeSort;
    FAskForRefund: Boolean;
    FSecureReport: Boolean;
    FResults: IisListOfValues;
  protected
    procedure  DoOnConstruction; override;
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;

    function  GetBegDate: TDateTime;
    function  GetEndDate: TDateTime;
    procedure SetEndDate(const AValue: TDateTime);
    function  GetAskForRefund: Boolean;
    function  GetEeSortMode: TProcessTaxReturnsEeSort;
    procedure SetAskForRefund(const AValue: Boolean);
    procedure SetEeSortMode(const AValue: TProcessTaxReturnsEeSort);
    function  GetFilter: IisStringList;
    function  GetTaxFilter: TProcessTaxReturnsFilterSelections;
    procedure SetTaxFilter(const AValue: TProcessTaxReturnsFilterSelections);
    function  GetResults: IisListOfValues;
    function  GetSecureReport: Boolean;
    procedure SetSecureReport(const AValue: Boolean);
  public
    class function GetTypeID: String; override;
  end;


{ TevProcessTaxReturnsTaskProxy }

procedure TevProcessTaxReturnsTaskProxy.DoOnConstruction;
begin
  inherited;
  FFilter := TisStringList.Create;
end;

function TevProcessTaxReturnsTaskProxy.GetFilter: IisStringList;
begin
  Result := FFilter;
end;

function TevProcessTaxReturnsTaskProxy.GetBegDate: TDateTime;
begin
  Result := FBegDate;
end;

function TevProcessTaxReturnsTaskProxy.GetEndDate: TDateTime;
begin
  Result := FEndDate;
end;

class function TevProcessTaxReturnsTaskProxy.GetTypeID: String;
begin
  Result := QUEUE_TASK_PROCESS_TAX_RETURNS;
end;


procedure TevProcessTaxReturnsTaskProxy.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
var
  S: IevDualStream;
begin
  inherited;
  FEndDate := AStream.ReadDouble;
  FEeSortMode := TProcessTaxReturnsEeSort(AStream.ReadByte);
  FAskForRefund := AStream.ReadBoolean;
  FTaxFilter := TProcessTaxReturnsFilterSelections(AStream.ReadByte);
  FSecureReport := AStream.ReadBoolean;  

  S := AStream.ReadStream(S);
  (FFilter as IisInterfacedObject).AsStream := S;

  S := AStream.ReadStream(nil, True);
  if Assigned(S) then
  begin
    FResults := TisListOfValues.Create;
    (FResults as IisInterfacedObject).AsStream := S;
  end;
end;

procedure TevProcessTaxReturnsTaskProxy.SetEndDate(const AValue: TDateTime);
begin
  FEndDate := AValue;
  FBegDate := GetBeginYear(FEndDate);
end;

procedure TevProcessTaxReturnsTaskProxy.WriteSelfToStream(const AStream: IEvDualStream);
var
  S: IevDualStream;
begin
  inherited;
  AStream.WriteDouble(FEndDate);
  AStream.WriteByte(Ord(FEeSortMode));
  AStream.WriteBoolean(FAskForRefund);
  AStream.WriteByte(Ord(FTaxFilter));
  AStream.WriteBoolean(FSecureReport);

  S := (FFilter as IisInterfacedObject).AsStream;
  AStream.WriteStream(S);

  if Assigned(FResults) then
    S := (FResults as IisInterfacedObject).AsStream
  else
    S := nil;

  AStream.WriteStream(S);
end;

function TevProcessTaxReturnsTaskProxy.GetAskForRefund: Boolean;
begin
  Result := FAskForRefund;
end;

function TevProcessTaxReturnsTaskProxy.GetEeSortMode: TProcessTaxReturnsEeSort;
begin
  Result := FEeSortMode;
end;

procedure TevProcessTaxReturnsTaskProxy.SetAskForRefund(const AValue: Boolean);
begin
  FAskForRefund := AValue;
end;

procedure TevProcessTaxReturnsTaskProxy.SetEeSortMode(const AValue: TProcessTaxReturnsEeSort);
begin
  FEeSortMode := AValue;
end;

function TevProcessTaxReturnsTaskProxy.GetTaxFilter: TProcessTaxReturnsFilterSelections;
begin
  Result := FTaxFilter;
end;

procedure TevProcessTaxReturnsTaskProxy.SetTaxFilter(const AValue: TProcessTaxReturnsFilterSelections);
begin
  FTaxFilter := AValue;
end;

function TevProcessTaxReturnsTaskProxy.GetResults: IisListOfValues;
begin
  Result := FResults;
end;

function TevProcessTaxReturnsTaskProxy.GetSecureReport: Boolean;
begin
  Result := FSecureReport;
end;

procedure TevProcessTaxReturnsTaskProxy.SetSecureReport(const AValue: Boolean);
begin
  FSecureReport := AValue;
end;

initialization
  ObjectFactory.Register(TevProcessTaxReturnsTaskProxy);

finalization
  SafeObjectFactoryUnRegister(TevProcessTaxReturnsTaskProxy);

end.
