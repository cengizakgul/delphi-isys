unit EvRunReportTask;

interface

uses Variants, SysUtils, isBaseClasses, EvCustomTask, EvCommonInterfaces, EvTypes, EvStreamUtils,
     SReportSettings, EvMainboard, EvConsts;

implementation

type
  TEvRunReportTask = class(TEvPrintableTask, IevRunReportTask)
  private
    FReports: IevDualStream;
    FNextPhaseReport: IevDualStream;
  protected
    procedure  WriteSelfToStream(const AStream: IEvDualStream); override;
    procedure  ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision); override;
    procedure  DoStoreTaskState(const AStream: IevDualStream); override;
    procedure  DoRestoreTaskState(const AStream: IevDualStream); override;

    function   GetReports: IevDualStream;
    procedure  SetReports(const AValue: IevDualStream);

    procedure  Phase_Begin(const AInputRequests: TevTaskRequestList); override;
    procedure  Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList); override;
  public
    class function GetTypeID: String; override;
  end;


{ TEvRunReportTask }

procedure TEvRunReportTask.DoRestoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  FNextPhaseReport := AStream.ReadStream(FNextPhaseReport, True);
end;

procedure TEvRunReportTask.DoStoreTaskState(const AStream: IevDualStream);
begin
  inherited;
  AStream.WriteStream(FNextPhaseReport);
end;

function TEvRunReportTask.GetReports: IevDualStream;
begin
  Result := FReports;
end;

class function TEvRunReportTask.GetTypeID: String;
begin
  Result := QUEUE_TASK_RUN_REPORT;
end;

procedure TEvRunReportTask.Phase_Begin(const AInputRequests: TevTaskRequestList);
var
  Params, MTParams: IisListOfValues;
 begin
  inherited;

  Params := TisListOfValues.Create;
  Params.AddValue('ReportList', FReports);

  MTParams := TisListOfValues.Create;
  MTParams.AddValue('Funct', __RUN_INIT);
  MTParams.AddValue('Phase', __DO_NOTHING);
//  MTParams.AddValue('Params', VarArrayOf([null]));
  MTParams.AddValue('Params', VarArrayOf([GetMaxThreads]));
  Params.AddValue('MTParams', MTParams);

  AddRequest('Report(s)', 'RunReports', Params, 'ProcessReportResults.Fork');

  SetProgressText('Running reports');
end;

procedure TEvRunReportTask.Phase_ProcessReportResults(const AInputRequests: TevTaskRequestList);
var
  Params, MTResParams, MTRunParams: IisListOfValues;
  RepData, RunRep: IevDualStream;
  MTResult: Variant;
  i: Integer;
  MTPhase: TReportRequestType;
  EmptyInt: IInterface;
begin
  EmptyInt := nil;
  MTResParams := IInterface(AInputRequests[0].Results.TryGetValue('MTParams', EmptyInt)) as IisListOfValues;

  if Assigned(MTResParams) then
  begin
    // continue of MT report running
    MTPhase := MTResParams.Value['Phase'];

    if MTPhase = __DO_SEQUENTIAL then
    begin
      Assert(Length(AInputRequests) = 1);
      FNextPhaseReport := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IEvDualStream;
      Params := TisListOfValues.Create;
      Params.AddValue('ReportList', FNextPhaseReport);
      FNextPhaseReport := nil;

      MTResult := MTResParams.Value['Result'];
      MTRunParams := TisListOfValues.Create;
      MTRunParams.AddValue('Phase', MTPhase);
      MTRunParams.AddValue('Funct', MTResParams.Value['Funct']);
      MTRunParams.AddValue('Params', MTResult);
      Params.AddValue('MTParams', MTRunParams);

      AddRequest('MT Report Sequence', 'RunReports', Params, 'ProcessReportResults.Fork');

      SetProgressText('Running report thread');
    end

    else if MTPhase = __DO_PARALLEL then
    begin
      Assert(Length(AInputRequests) = 1);
      FNextPhaseReport := IInterface(AInputRequests[0].Results.Value[PARAM_RESULT]) as IEvDualStream;
      MTResult := MTResParams.Value['Result'];
      RunRep := IInterface(MTResParams.Value['Report']) as IevDualStream;
      for i := VarArrayLowBound(MTResult, 1) to VarArrayHighBound(MTResult, 1) do
      begin
        Params := TisListOfValues.Create;

        // need to copy stream because it's used in many threads!
        RepData := TevDualStreamHolder.Create(FReports.Size);
        RunRep.Position := 0;
        RepData.CopyFrom(RunRep, RunRep.Size);
        Params.AddValue('ReportList', RepData);

        MTRunParams := TisListOfValues.Create;
        MTRunParams.AddValue('Phase', MTPhase);
        MTRunParams.AddValue('Funct', MTResParams.Value['Funct']);
        MTRunParams.AddValue('Params', VarArrayOf([MTResult[i]]));
        Params.AddValue('MTParams', MTRunParams);

        AddRequest('MT Report Thread #' + IntToStr(i + 1), 'RunReports', Params, 'ProcessReportResults.Merge');
      end;
      SetProgressText('Running report threads');
    end

    else if MTPhase = __DO_COMBINE then
    begin
      Params := TisListOfValues.Create;
      Params.AddValue('ReportList', FNextPhaseReport);
      FNextPhaseReport := nil;
      MTRunParams := TisListOfValues.Create;
      MTRunParams.AddValue('Phase', MTPhase);
      MTRunParams.AddValue('Funct', MTResParams.Value['Funct']);

      StopOnExceptions(AInputRequests);

      MTResult := VarArrayCreate([0, Length(AInputRequests) - 1], varVariant);
      for i := Low(AInputRequests) to High(AInputRequests) do
      begin
        MTResParams := IInterface(AInputRequests[i].Results.Value['MTParams']) as IisListOfValues;
        MTResult[i] := MTResParams.Value['Result'];
      end;
      MTRunParams.AddValue('Params', VarArrayOf([MTResult]));

      Params.AddValue('MTParams', MTRunParams);

      AddRequest('MT Report Combine', 'RunReports', Params, 'ProcessReportResults.Fork');
      SetProgressText('Combining tread results');
    end

    else
    begin
      inherited;
      Exit;
    end;
  end
  else
    inherited;
end;

procedure TEvRunReportTask.ReadSelfFromStream(const AStream: IEvDualStream; const ARevision: TisRevision);
begin
  inherited;
  FReports := AStream.ReadStream(nil, True);
end;

procedure TEvRunReportTask.SetReports(const AValue: IevDualStream);
begin
  FReports := AValue;
end;

procedure TEvRunReportTask.WriteSelfToStream(const AStream: IEvDualStream);
begin
  inherited;
  AStream.WriteStream(FReports);
end;

initialization
  ObjectFactory.Register(TEvRunReportTask);

finalization
  SafeObjectFactoryUnRegister(TEvRunReportTask);

end.
