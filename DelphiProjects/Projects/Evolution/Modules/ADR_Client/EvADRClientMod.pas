unit EvADRClientMod;

interface

uses Classes, SysUtils, Variants, DateUtils,
     isBaseClasses, isBasicUtils, isSettings, isThreadManager,
     EvCommonInterfaces, EvMainboard, evContext, EvBasicUtils,
     isLogFile, EvConsts, isErrorUtils, EvADRCommon, EvDataSet, isDataSet,
     EvControllerInterfaces, EvADRRequestQueue, isSchedule;


implementation

uses evADRClientQueue, evADRClientController, evADRDBChangeListener;

type
  TevADRClient = class(TevCustomADRModule, IevADRClient)
  private
    FDBChangeListener: IevADRDBChangeListener;
    FController: IevAppController;
    FScheduler: IisSchedule;
    function  BuildDBList(const ADataBase: String): IisStringList;
  protected
    procedure DoOnConstruction; override;
    function  GetWorkingDir: String; override;

    // IevADRClient
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    procedure SetActive(const AValue: Boolean); override;
    function  GetDBList: IisStringList;
    procedure SyncDBList(const ATopPosition: Boolean = False);
    procedure CheckDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATopPosition: Boolean = False);
    procedure FullDBCopy(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ACheckQueue: Boolean = False; const ATopPosition: Boolean = False);
    procedure SynchronizeDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATargetDBVersion: String;
                            const ATargetLastTransactionNbr: Integer; const ACheckQueue: Boolean = False;
                            const ATopPosition: Boolean = False);
    procedure CheckDBData(const ADataBase: String; const ATopPosition: Boolean = False);
    procedure CheckDBTransactions(const ADataBase: String; const ATopPosition: Boolean = False);
    function  GetStatus: IisListOfValues;
  public
    class function GetTypeID: String; override;
  end;


  TisADRSchedule = class(TisSchedule)
  protected
    procedure DoOnStart; override;
    procedure RunTaskFor(const AEvent: IisScheduleEvent); override;
    procedure DoOnStop; override;
  end;

function GetADRClient: IevADRClient;
begin
  Result := TevADRClient.Create;
end;


{ TevADRClient }

procedure TevADRClient.CheckDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATopPosition: Boolean);
var
  R: IevADRRequest;
  DBList: IisStringList;
  i: Integer;
begin
  DBList := BuildDBList(ADataBase);

  for i := 0 to DBList.Count - 1 do
  begin
    if RequestQueue.RequestsExist(ADR_Clt_CheckDBStatus, [arsQueued], DBList[i]) then
      Exit;

    if RequestQueue.RequestsExist(ADR_Clt_FullDBCopy, [arsQueued], DBList[i]) then
      Exit;

    LogEvent('DB check enlisted for ' + DBList[i]);
    R := RequestQueue.NewDBRequest(NowUTC, ADR_Clt_CheckDBStatus, [DBList[i], '', 0]);
    if ATopPosition then
      R.MoveToTop;
  end;
end;

procedure TevADRClient.DoOnConstruction;
var
  Evt: IisScheduleEventDaily;
begin
  inherited;
  RequestQueue.ExecSlots := mb_AppConfiguration.GetValue('ADRClient\MaxRequests', RequestQueue.ExecSlots);

  FScheduler := TisADRSchedule.Create('ADR Client Scheduler');
  Evt := TisScheduleEventDaily.Create;
  Evt.TaskID := 'CheckDBStatus';
  Evt.StartDate := Today;
  Evt.StartTime := StrToTime('12:00 AM');
  Evt.RunIfMissed := True;

  try
    Evt.LastRunDate := TextToDate(mb_AppSettings.AsString['LastDailyCheckDBStatus']);
  except
  end;

  FScheduler.AddEvent(Evt);

  FDBChangeListener := CreateDBChangeListener;
  FController := CreateACController;
  FController.Active := True;
end;

function TevADRClient.GetDBInfo(const ADatabase: String): IevADRDBInfo;
var
  PrevADRContext: Boolean;
begin
  PrevADRContext := SetADRContext(False);
  try
    try
      Result := DoGetDBInfo(ADatabase);
    except
      on E: Exception do
        LogError(ADatabase + ': GetDBInfo', E.Message);
    end;
  finally
    SetADRContext(PrevADRContext);
  end;
end;

function TevADRClient.GetDBList: IisStringList;
var
  Q: IevQuery;
  PrevADRContext: Boolean;
begin
  PrevADRContext := SetADRContext(False);
  try
    Q := TevQuery.Create('SELECT CL_NBR FROM TMP_CL ORDER BY CL_NBR');

    Result := TisStringList.Create;
    Result.Capacity := Q.Result.RecordCount;
    Q.Result.First;
    while not Q.Result.Eof do
    begin
      Result.Add(DB_Cl + Q.Result.Fields[0].AsString);
      Q.Result.Next;
    end;

    Result.Insert(0, DB_Cl_Base);
    Result.Insert(1, DB_TempTables);
    Result.Insert(2, DB_S_Bureau);
    Result.Insert(3, DB_System);
  finally
    SetADRContext(PrevADRContext);
  end;
end;

procedure TevADRClient.SetActive(const AValue: Boolean);

  procedure DoInThread(const Params: TTaskParamList);
  begin
    (Mainboard.ADRClient as IevADRModule).AddDBRequestsIfMissing(NowUTC, ADR_Clt_StartupCheck, [False], True);
  end;

begin
  if not GetActive and AValue then
    Context.RunParallelTask(@DoInThread, MakeTaskParams([]), 'ADR Client activation');

  inherited;

  FDBChangeListener.Active := AValue;
  FScheduler.Active := AValue;
end;

procedure TevADRClient.SyncDBList(const ATopPosition: Boolean = False);
var
  R: IevADRRequest;
begin
  if RequestQueue.RequestsExist(ADR_Clt_SyncDBList, [arsQueued]) then
    Exit;

  LogEvent('Sync DB list enlisted');
  R := RequestQueue.NewDBRequest(NowUTC, ADR_Clt_SyncDBList, ['', '', 0]);
  if ATopPosition then
    R.MoveToTop;
end;

procedure TevADRClient.FullDBCopy(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ACheckQueue, ATopPosition: Boolean);
var
  R: IevADRRequest;
  DBList: IisStringList;
  i: Integer;
begin
  DBList := BuildDBList(ADataBase);

  for i := 0 to DBList.Count - 1 do
  begin
    if ACheckQueue then
      if RequestQueue.RequestsExist(ADR_Clt_FullDBCopy, [arsQueued], DBList[i]) then
      begin
        LogEvent(DBList[i] + ' is already in process');
        Exit;
      end;

    // clean up all related requests
    RequestQueue.DeleteDBRequests(DBList[i]);
    Mainboard.ADRServer.DeleteDBRequests(DBList[i]);

    R := RequestQueue.NewDBRequest(AOriginatedAt, ADR_Clt_FullDBCopy, [DBList[i], '', 0]);
    if ATopPosition then
      R.MoveToTop;

    LogEvent('Full DB copy enlisted for ' + DBList[i]);
    CheckDB(AOriginatedAt, DBList[i]);
  end;
end;

procedure TevADRClient.SynchronizeDB(const AOriginatedAt: TUTCDateTime; const ADataBase,
  ATargetDBVersion: String; const ATargetLastTransactionNbr: Integer;
  const ACheckQueue, ATopPosition: Boolean);
var
  R: IevADRRequest;
begin
  if ACheckQueue then
  begin
    if RequestQueue.RequestsExist(ADR_Clt_SyncDBData, [arsQueued], ADataBase) then
    begin
      LogEvent(ADataBase + ' is already in process');
      Exit;
    end;

    if RequestQueue.RequestsExist(ADR_Clt_FullDBCopy, [arsQueued], ADataBase) then
    begin
      LogEvent(ADataBase + ' is already in process');
      Exit;
    end;
  end;

  R := RequestQueue.NewDBRequest(AOriginatedAt, ADR_Clt_SyncDBData, [ADataBase, ATargetDBVersion, ATargetLastTransactionNbr]);
  if ATopPosition then
    R.MoveToTop;

  LogEvent('DB sync enlisted for ' + ADataBase);    
end;

class function TevADRClient.GetTypeID: String;
begin
  Result := 'ADR Client';
end;

procedure TevADRClient.CheckDBData(const ADataBase: String; const ATopPosition: Boolean);
var
  R: IevADRRequest;
  DBList: IisStringList;
  i: Integer;
begin
  DBList := BuildDBList(ADataBase);

  for i := 0 to DBList.Count - 1 do
  begin
    if IsTempDB(DBList[i]) then
      Continue;

    if RequestQueue.RequestsExist(ADR_Clt_CheckDBData, [arsQueued], DBList[i]) then
    begin
      LogEvent(ADataBase + ' is already in process');
      Exit;
    end;

    R := RequestQueue.NewDBRequest(NowUTC, ADR_Clt_CheckDBData, [DBList[i], '', 0]);
    if ATopPosition then
      R.MoveToTop;

    LogEvent('DB Data check enlisted for ' + DBList[i]);
  end;
end;

function TevADRClient.GetWorkingDir: String;
begin
  Result := NormalizePath(mb_AppConfiguration.GetValue('ADRClient\StagingFolder', AppDir + 'ADR')) + 'Client\';
end;

function TevADRClient.GetStatus: IisListOfValues;
var
  Stat: IevADRStatCounter;
  s: String;
  AllRequests: IisList;
  R: IevADRRequest;
  TR: IevADRTransRequest;
  NowTime, MinOrigTime, d: TDateTime;
  i: Integer;
begin
  Result := TisListOfValues.Create;

  Stat := Statistics.GetCounter('LastActivity');
  if Assigned(Stat) then
    s := DateTimeToStr(Stat.LastUpdate) + '  ' + VarToStr(Stat.Value)
  else
    s := '';

  Result.AddValue('LastActivity', s);

  // Latency
  NowTime := UTCToGMTDateTime(NowUTC);
  MinOrigTime := NowTime;

  AllRequests := (Mainboard.ADRClient as IevADRModule).RequestQueue.GetRequests;
  for i := 0 to AllRequests.Count - 1 do
  begin
    R := AllRequests[i] as IevADRRequest;
    if R.ID <> ADR_Clt_CheckDBData then
    begin
      d := UTCToGMTDateTime(R.OriginatedAt);
      if DateTimeCompare(d, coLess, MinOrigTime) then
        MinOrigTime := d;
    end;
  end;
  R := nil;

  if Supports(Mainboard.ADRServer, IevProxyModule) then
  begin
    AllRequests := (Mainboard.ADRServer as IevADRModule).RequestQueue.GetRequests;
    for i := 0 to AllRequests.Count - 1 do
    begin
      TR := AllRequests[i] as IevADRTransRequest;
      if TR.ADRRequestID <> ADR_Srv_CheckDBData then
      begin
        d := UTCToGMTDateTime(TR.OriginatedAt);
        if DateTimeCompare(d, coLess, MinOrigTime) then
          MinOrigTime := d;
      end;
    end;
    TR := nil;
    AllRequests := nil;
  end;

  Result.AddValue('Latency', SecondsBetween(NowTime, MinOrigTime));
end;

function TevADRClient.BuildDBList(const ADataBase: String): IisStringList;
var
  i: Integer;
begin
  if ADataBase = '*' then
    Result := Mainboard.ADRClient.GetDBList

  else if AnsiSameText(ADataBase, 'CL*') then
  begin
    Result := Mainboard.ADRClient.GetDBList;
    i := 0;
    while i < Result.Count do
      if not StartsWith(Result[i], DB_Cl) or AnsiSameText(Result[i], DB_Cl_Base) then
        Result.Delete(i)
      else
        Inc(i);
  end

  else
  begin
    Result := TisStringList.Create;
    Result.Add(ADatabase);
  end;
end;

procedure TevADRClient.CheckDBTransactions(const ADataBase: String; const ATopPosition: Boolean);
var
  R: IevADRRequest;
  DBList: IisStringList;
  i: Integer;
begin
  DBList := BuildDBList(ADataBase);

  for i := 0 to DBList.Count - 1 do
  begin
    if IsTempDB(DBList[i]) then
      Continue;

    if RequestQueue.RequestsExist(ADR_Clt_CheckDBTrans, [arsQueued], DBList[i]) then
    begin
      LogEvent(ADataBase + ' is already in process');
      Exit;
    end;

    R := RequestQueue.NewDBRequest(NowUTC, ADR_Clt_CheckDBTrans, [DBList[i], '', 0]);
    if ATopPosition then
      R.MoveToTop;

    LogEvent('DB transactions check enlisted for ' + DBList[i]);
  end;
end;

{ TisADRSchedule }

procedure TisADRSchedule.DoOnStart;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;
  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
end;

procedure TisADRSchedule.DoOnStop;
begin
  Mainboard.ContextManager.RestoreThreadContext;
  inherited;
end;

procedure TisADRSchedule.RunTaskFor(const AEvent: IisScheduleEvent);
begin
  inherited;
  if AEvent.TaskID = 'CheckDBStatus' then
  begin
    (Mainboard.ADRClient as IevADRModule).AddDBRequestsIfMissing(NowUTC, ADR_Clt_StartupCheck, [True], True);
     AEvent.LastRunDate := Now;
     mb_AppSettings.AsString['LastDailyCheckDBStatus'] := DateToText(AEvent.LastRunDate);
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetADRClient, IevADRClient, 'ADR Client');

end.
