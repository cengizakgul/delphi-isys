unit evADRDBChangeListener;

interface

uses Windows, SysUtils,
     isBaseClasses, isThreadManager, EvStreamUtils, isSocket, isLogFile, isBasicUtils,
     evMainboard, EvConsts, EvBasicUtils, evADRCommon;

type
  IevADRDBChangeListener = interface
  ['{E7DD73BC-56ED-4DAC-95EB-C2114FE3A45D}']
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    property  Active: Boolean read GetActive write SetActive;
  end;

  function CreateDBChangeListener: IevADRDBChangeListener;

implementation

uses EvADRClientMod, EvCommonInterfaces;

type
  TevADRDBChangeListener = class(TisInterfacedObject, IevADRDBChangeListener)
  private
    FTaskID: TTaskID;
    procedure ChangesListeningThread(const Params: TTaskParamList);
    procedure Start;
    procedure Stop;
  protected
    procedure DoOnDestruction; override;

    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
  end;


  TevEventDatagram = record
    Ver:           String;
    EnvironmentID: String;
    Domain:        String;
    ID:            String;
    Data:          String;
  end;

function CreateDBChangeListener: IevADRDBChangeListener;
begin
  Result := TevADRDBChangeListener.Create;
end;



{ TevADRDBChangeListener }

function TevADRDBChangeListener.GetActive: Boolean;
begin
  Result := (FTaskID <> 0) and not GlobalThreadManager.TaskIsTerminated(FTaskID);
  if not Result then
    FTaskID := 0;
end;


procedure TevADRDBChangeListener.SetActive(const AValue: Boolean);
begin
  if AValue then
    Start
  else
    Stop;
end;

procedure TevADRDBChangeListener.Start;
begin
  if not GetActive then
    FTaskID := GlobalThreadManager.RunTask(ChangesListeningThread, Self, MakeTaskParams([]), 'DB Change Listener');
end;

procedure TevADRDBChangeListener.Stop;
begin
  if GetActive then
  begin
    GlobalThreadManager.TerminateTask(FTaskID);
    GlobalThreadManager.WaitForTaskEnd(FTaskID);
    FTaskID := 0;
  end;
end;

procedure TevADRDBChangeListener.ChangesListeningThread(const Params: TTaskParamList);

  procedure RenderDatagram (const Params: TTaskParamList);
  var
    Datagram: TEvEventDatagram;
    DataStr, DB: String;
    EvoDomain: IevDomainInfo;
  begin
    DataStr := Params[0];

    Datagram.Ver := GetNamedValue(DataStr, 'VER', ' ');
    Datagram.EnvironmentID := GetNamedValue(DataStr, 'ENV', ' ');
    Datagram.Domain := GetNamedValue(DataStr, 'EVD', ' ');
    Datagram.ID := GetNamedValue(DataStr, 'ID', ' ');
    Datagram.DATA := GetNamedValue(DataStr, 'DATA', ' ');

    EvoDomain := mb_GlobalSettings.DomainInfoList.GetDomainInfo(Datagram.Domain);
    if not Assigned(EvoDomain) then
      Exit;

    if not mb_AppConfiguration.AsBoolean['ADRClient\Domains\' + EvoDomain.DomainName + '\Active'] then
      Exit;

    (Mainboard.ADRClient as IevADRModule).LogEvent('Evolution event received', Params[0]);

    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sADRUserName, EvoDomain.DomainName), EvoDomain.SystemAccountInfo.Password);
    try
      if Datagram.ID = 'DBCHANGE' then
      begin
        DataStr := Datagram.DATA;
        while DataStr <> '' do
        begin
          DB := GetNextStrValue(DataStr, ',');
          Mainboard.ADRClient.CheckDB(NowUTC, DB);
        end;
      end

      else if Datagram.ID = 'DBCREATED' then
      begin
        DataStr := Datagram.DATA;
        DB := GetNextStrValue(DataStr, ',');
        Mainboard.ADRClient.CheckDB(NowUTC, DB);
      end

      else if Datagram.ID = 'DBLISTCHANGED' then
        Mainboard.ADRClient.SyncDBList;
    finally
      Mainboard.ContextManager.DestroyThreadContext;
    end;
  end;

var
  DataStr: String;
  Datagram: TEvEventDatagram;
  Socket: IisUDPSocket;
  EnvironmentID: String;
begin
  Socket := TisUDPSocket.Create('', DB_Change_Notification_Port);
  EnvironmentID := Mainboard.GlobalSettings.EnvironmentID;

  while not CurrentThreadTaskTerminated do
  begin
    DataStr := Socket.ReceiveString;

    if DataStr <> '' then
    begin
      ZeroMemory(@Datagram, SizeOf(Datagram));
      if GetNextStrValue(DataStr, ' ') = 'EVOEVENT' then
      begin
        Datagram.Ver := GetNamedValue(DataStr, 'VER', ' ');
        Datagram.EnvironmentID := GetNamedValue(DataStr, 'ENV', ' ');
        if Datagram.Ver <> '2' then
          Datagram.EnvironmentID := '';
      end;

      if (Datagram.EnvironmentID <> '') and AnsiSameText(Datagram.EnvironmentID, EnvironmentID) then
        GlobalThreadManager.RunTask(@RenderDatagram, MakeTaskParams([DataStr]), 'DB Change Event Dispatch');
    end;
  end;
end;

procedure TevADRDBChangeListener.DoOnDestruction;
begin
  Stop;
  inherited;
end;

end.
