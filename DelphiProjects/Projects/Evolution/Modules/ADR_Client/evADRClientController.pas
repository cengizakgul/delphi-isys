unit evADRClientController;

interface

uses SysUtils, Variants, DateUtils, Math,
     isBaseClasses, EvTransportShared, EvTransportInterfaces, isLogFile,
     isAppIDs, isExceptions, isErrorUtils, EvControllerInterfaces, evConsts,
     EvMainboard, evRemoteMethods, evCommonInterfaces, EvADRRequestQueue,
     isBasicUtils, EvBasicUtils, evADRCommon, evContext, SEncryptionRoutines,
     isThreadManager;


  function CreateACController: IevAppController;

implementation

type
  TevACController = class(TevCustomTCPServer, IevAppController)
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


  TevACControl = class(TisInterfacedObject, IevACControl)
  private
    procedure CreateContext(const ADomainName: String);
    procedure DestroyContext;
    procedure AddRequestsToList(const ARequestList: IisParamsCollection; const ARequests: IisList);
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
    function  GetStatus: IisParamsCollection;
    function  GetDomainStatus(const ADomainName: String): IisListOfValues;
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetTransRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetDBList(const ADomainName: String): IisStringList;
    procedure SyncDBList(const ADomainName: String);
    procedure CheckDB(const ADomainName: String; const ADataBase: String);
    procedure FullDBCopy(const ADomainName: String; const ADataBase: String);
    procedure CheckDBData(const ADomainName: String; const ADataBase: String);
    procedure CheckDBTransactions(const ADomainName: String; const ADataBase: String);
  end;


  TevACControllerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    FControl: IevACControl;
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetSettings(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetSettings(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStackInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetDomainStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);    
    procedure dmGetRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetTransRequestList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetDBList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSyncDBList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmFullDBCopy(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDBData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDBTransactions(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure DoOnConstruction; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure RegisterHandlers; override;
  end;


function CreateACController: IevAppController;
begin
  Result := TevACController.Create;
end;

{ TevACControl }

procedure TevACControl.AddRequestsToList(const ARequestList: IisParamsCollection; const ARequests: IisList);
var
  i: Integer;
  Request: IevADRRequest;
  RequestInfo: IisListOfValues;
begin
  for i := 0 to ARequests.Count - 1 do
  begin
    Request := ARequests[i] as IevADRRequest;
    RequestInfo := ARequestList.AddParams(IntToStr(ARequestList.Count + 1));
    RequestInfo.AddValue('ID', Request.ID);
    RequestInfo.AddValue('Domain', Request.Domain);    
    RequestInfo.AddValue('QueuedAt', Request.QueuedAt);
    RequestInfo.AddValue('StartedAt', Request.StartedAt);
    RequestInfo.AddValue('FinishedAt', Request.FinishedAt);
    RequestInfo.AddValue('Type', Request.TypeID);
    RequestInfo.AddValue('Caption', Request.Caption);
    RequestInfo.AddValue('Status', ADRRequestStatusNames[Request.Status]);
    RequestInfo.AddValue('Progress', Request.Progress);
  end;
end;

procedure TevACControl.CheckDB(const ADomainName: String; const ADataBase: String);
begin
  CreateContext(ADomainName);
  try
    Mainboard.ADRClient.CheckDB(NowUTC, ADataBase, False);
  finally
    DestroyContext;
  end;
end;

procedure TevACControl.CheckDBData(const ADomainName, ADataBase: String);
begin
  CreateContext(ADomainName);
  try
    Mainboard.ADRClient.CheckDBData(ADataBase, False);
  finally
    DestroyContext;
  end;
end;

procedure TevACControl.CheckDBTransactions(const ADomainName, ADataBase: String);
begin
  CreateContext(ADomainName);
  try
    Mainboard.ADRClient.CheckDBTransactions(ADataBase, False);
  finally
    DestroyContext;
  end;
end;

procedure TevACControl.CreateContext(const ADomainName: String);
var
  DomainInfo: IevDomainInfo;
begin
  Mainboard.ContextManager.StoreThreadContext;

  DomainInfo := Mainboard.GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if not Assigned(DomainInfo) then
    raise EisException.CreateFmt('Domain "%s" is not registered', [ADomainName]);

  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sADRUserName, DomainInfo.DomainName),
    DomainInfo.SystemAccountInfo.Password);
end;

procedure TevACControl.DestroyContext;
begin
  Mainboard.ContextManager.RestoreThreadContext;
end;

procedure TevACControl.FullDBCopy(const ADomainName: String; const ADataBase: String);
begin
  CreateContext(ADomainName);
  try
    Mainboard.ADRClient.FullDBCopy(NowUTC, ADataBase, False);
  finally
    DestroyContext;
  end;
end;

function TevACControl.GetDBList(const ADomainName: String): IisStringList;
begin
  CreateContext(ADomainName);
  try
    Result := Mainboard.ADRClient.GetDBList;
  finally
    DestroyContext;
  end;
end;

function TevACControl.GetDomainStatus(const ADomainName: String): IisListOfValues;
var
  AllRequests, Requests: IisList;
  ActiveRequests: IisParamsCollection;
  Stat: IevADRStatCounter;
  SrvStatus: IisListOfValues;
  s: String;
  bConnected: Boolean;
  i: Integer;  
  NowTime, MinOrigTime, d: TDateTime;  
  R: IevADRRequest;
  TR: IevADRTransRequest;
  clLatency, srvLatency: Integer;

  function FilterByStatus(const AStatus: TevADRRequestStatus): IisList;
  var
    i: Integer;
    R: IevADRRequest;
  begin
    Result := TisList.Create;

    if not Assigned(AllRequests) then
      Exit;

    Result.Capacity := AllRequests.Count;
    for i := 0 to AllRequests.Count - 1 do
    begin
      R := AllRequests[i] as IevADRRequest;
      if R.Status = AStatus then
        Result.Add(R);
    end;
  end;

begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');

    Result := TisListOfValues.Create;
    ActiveRequests := TisParamsCollection.Create;
    Result.AddValue('ActiveRequests', ActiveRequests);

    NowTime := UTCToGMTDateTime(NowUTC);
    MinOrigTime := NowTime;

    if Supports(Mainboard.ADRServer, IevProxyModule) then
    begin
      // Upload Queue
      AllRequests := (Mainboard.ADRServer as IevADRModule).RequestQueue.GetRequests;
      for i := 0 to AllRequests.Count - 1 do
      begin
        TR := AllRequests[i] as IevADRTransRequest;
        if TR.ADRRequestID <> ADR_Srv_CheckDBData then
        begin
          d := UTCToGMTDateTime(TR.OriginatedAt);
          if DateTimeCompare(d, coLess, MinOrigTime) then
            MinOrigTime := d;
        end;
      end;
      TR := nil;
    end;  

    Requests := FilterByStatus(arsExecuting);
    Result.AddValue('UplExecuting', Requests.Count);
    AddRequestsToList(ActiveRequests, Requests);

    Requests := FilterByStatus(arsQueued);
    Result.AddValue('UplQueued', Requests.Count);
    if Requests.Count > 5 then
      Requests.Count := 5;
    AddRequestsToList(ActiveRequests, Requests);

    // ADR Queue
    AllRequests := (Mainboard.ADRClient as IevADRModule).RequestQueue.GetRequests;
    for i := 0 to AllRequests.Count - 1 do
    begin
      R := AllRequests[i] as IevADRRequest;
      if R.ID <> ADR_Clt_CheckDBData then
      begin
        d := UTCToGMTDateTime(R.OriginatedAt);
        if DateTimeCompare(d, coLess, MinOrigTime) then
          MinOrigTime := d;
      end;
    end;
    R := nil;

    clLatency := SecondsBetween(NowTime, MinOrigTime);

    Requests := FilterByStatus(arsExecuting);
    Result.AddValue('RepExecuting', Requests.Count);
    AddRequestsToList(ActiveRequests, Requests);

    Requests := FilterByStatus(arsQueued);
    Result.AddValue('RepQueued', Requests.Count);
    if Requests.Count > 5 then
      Requests.Count := 5;
    AddRequestsToList(ActiveRequests, Requests);


    Stat := (Mainboard.ADRClient as IevADRModule).Statistics.GetCounter('LastActivity');
    if Assigned(Stat) then
      s :=  DateTimeToStr(Stat.LastUpdate) + '   ' + VarToStr(Stat.Value)
    else
      s := '';
    Result.AddValue('ClLastActivity', s);

    // Server State
    if Supports(Mainboard.ADRServer, IevProxyModule) then
      bConnected := (Mainboard.ADRServer as IevProxyModule).Connected
    else
      bConnected := True;
    Result.AddValue('Connected', bConnected);

    s := 'Unknown';
    srvLatency := 0;
    if bConnected then
    begin
      try
        SrvStatus := Mainboard.ADRServer.GetStatus;
      except
        on E: EevTransport do
        else
          raise;
      end;
      if Assigned(SrvStatus) then
      begin
        s := SrvStatus.TryGetValue('LastActivity', '');
        srvLatency := SrvStatus.TryGetValue('Latency', 0);
      end;
    end;
    Result.AddValue('SrvLastActivity', s);
    Result.AddValue('Latency', Max(srvLatency, clLatency));
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevACControl.GetFilteredLogEvents(
  const AQuery: TEventQuery): IisParamsCollection;
var
  L: IisList;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  i: Integer;
begin
  L := GlobalLogFile.GetEvents(AQuery);
  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevACControl.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  Result := TisListOfValues.Create;
  Event:= GlobalLogFile.Items[EventNbr];
  Result.AddValue('EventNbr', Event.Nbr);
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);
end;

function TevACControl.GetLogEvents(const PageNbr, EventsPerPage: Integer;
  out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  GlobalLogFile.Lock;
  try
    L := GlobalLogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := GlobalLogFile.Count div EventsPerPage;
    if GlobalLogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

procedure TevACControl.GetLogFileThresholdInfo(out APurgeRecThreshold,
  APurgeRecCount: Integer);
begin
  APurgeRecThreshold := GlobalLogFile.PurgeRecThreshold;
  APurgeRecCount := GlobalLogFile.PurgeRecCount;
end;

function TevACControl.GetRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
var
  L: IisList;
begin
  L := (Mainboard.ADRClient as IevADRModule).RequestQueue.GetPage(APageNbr, AItemsPerPage, APageCount);
  Result := TisParamsCollection.Create;
  AddRequestsToList(Result, L);
end;


function TevACControl.GetSettings: IisListOfValues;
var
  i: Integer;
  Domains: IisParamsCollection;
  D: IisListOfValues;
  DomainInfo: IevDomainInfo;
  s: String;
begin
  Result := TisListOfValues.Create;
  Result.AddValue('RequestBroker', mb_AppConfiguration.AsString['General\RequestBroker']);
  Result.AddValue('StagingFolder', mb_AppConfiguration.GetValue('ADRClient\StagingFolder', AppDir + 'ADR'));
  Result.AddValue('MaxRequests', mb_AppConfiguration.GetValue('ADRClient\MaxRequests', HowManyProcessors * 2));

  Domains := TisParamsCollection.Create;
  Result.AddValue('Domains', Domains);

  mb_GlobalSettings.DomainInfoList.Lock;
  try
    for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
    begin
      DomainInfo := mb_GlobalSettings.DomainInfoList[i];
      D := Domains.AddParams(DomainInfo.DomainName);
      D.AddValue('Name', DomainInfo.DomainDescription);

      D.AddValue('Active', mb_AppConfiguration.AsBoolean['ADRClient\Domains\' + DomainInfo.DomainName + '\Active']);
      D.AddValue('Host', mb_AppConfiguration.AsString['ADRClient\Domains\' + DomainInfo.DomainName + '\Host']);
      D.AddValue('Port', mb_AppConfiguration.GetValue('ADRClient\Domains\' + DomainInfo.DomainName + '\Port', ADRClient_Port));

      s := mb_AppConfiguration.AsString['ADRClient\Domains\' + DomainInfo.DomainName + '\UserName'];
      D.AddValue('User', DecryptStringLocaly(BinToStr(s), ''));

      s := mb_AppConfiguration.AsString['ADRClient\Domains\' + DomainInfo.DomainName + '\Password'];
      D.AddValue('Password', DecryptStringLocaly(BinToStr(s), ''));
    end;
  finally
    mb_GlobalSettings.DomainInfoList.Unlock;
  end;
end;

function TevACControl.GetStackInfo: String;
begin
  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      Result := GetStack(E);
  end;
end;

function TevACControl.GetStatus: IisParamsCollection;
var
  RepRequests, UplRequests: IisList;

  procedure BuildDomainStatus(const ADomainStatus: IisListOfValues);
  var
    Stat: IevADRStatCounter;
    sDomainName, s: String;
    bConnected: Boolean;
    i: Integer;
    Queued, Executing: Integer;
    R: IevADRRequest;
    TR: IevADRTransRequest;
    NowTime, MinOrigTime, d: TDateTime;
  begin
    sDomainName := ctx_DomainInfo.DomainName;
    ADomainStatus.AddValue('DomainName', ctx_DomainInfo.DomainDescription);

    bConnected := (Mainboard.ADRServer as IevProxyModule).Connected;
    ADomainStatus.AddValue('Connected', bConnected);

    Stat := (Mainboard.ADRClient as IevADRModule).Statistics.GetCounter('LastActivity');
    if Assigned(Stat) then
      s :=  DateTimeToStr(Stat.LastUpdate) + '   ' + VarToStr(Stat.Value)
    else
      s := '';
    ADomainStatus.AddValue('LastActivity', s);

    Queued := 0;
    Executing := 0;
    NowTime := UTCToGMTDateTime(NowUTC);
    MinOrigTime := NowTime;
    for i := 0 to RepRequests.Count - 1 do
    begin
      R := RepRequests[i] as IevADRRequest;
      if AnsiSameText(R.Domain, sDomainName) then
      begin
        case R.Status of
          arsQueued:    Inc(Queued);
          arsExecuting: Inc(Executing);
        end;

        if R.ID <> ADR_Clt_CheckDBData then
        begin
          d := UTCToGMTDateTime(R.OriginatedAt);
          if DateTimeCompare(d, coLess, MinOrigTime) then
            MinOrigTime := d;
        end;
      end;
    end;
    ADomainStatus.AddValue('RepQueued', Queued);
    ADomainStatus.AddValue('RepExecuting', Executing);

    Executing := 0;
    Queued := 0;
    if Assigned(UplRequests) then
      for i := 0 to UplRequests.Count - 1 do
      begin
        TR := UplRequests[i] as IevADRTransRequest;
        if AnsiSameText(TR.Domain, sDomainName) then
        begin
          case TR.Status of
            arsQueued:    Inc(Queued);
            arsExecuting: Inc(Executing);
          end;

          if TR.ADRRequestID <> ADR_Srv_CheckDBData then
          begin
            d := UTCToGMTDateTime(TR.OriginatedAt);
            if DateTimeCompare(d, coLess, MinOrigTime) then
              MinOrigTime := d;
          end;
        end;
      end;
    ADomainStatus.AddValue('UplExecuting', Executing);
    ADomainStatus.AddValue('UplQueued', Queued);

    ADomainStatus.AddValue('Latency', SecondsBetween(NowTime, MinOrigTime));    
  end;

var
  i: Integer;
begin
  Result := TisParamsCollection.Create;

  if Supports(Mainboard.ADRServer, IevProxyModule) then
    UplRequests := (Mainboard.ADRServer as IevADRModule).RequestQueue.QueryRequests('', [arsExecuting, arsQueued]);
  RepRequests := (Mainboard.ADRClient as IevADRModule).RequestQueue.QueryRequests('', []);

  Mainboard.ContextManager.StoreThreadContext;
  try
    for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
    begin
      if mb_GlobalSettings.DomainInfoList[i].DBChangesBroadcast and
         mb_AppConfiguration.AsBoolean['ADRClient\Domains\' + mb_GlobalSettings.DomainInfoList[i].DomainName + '\Active'] then
      begin
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, mb_GlobalSettings.DomainInfoList[i].DomainName), '');
        try
          BuildDomainStatus(Result.AddParams(ctx_DomainInfo.DomainName));
        finally
          Mainboard.ContextManager.DestroyThreadContext;
        end;
      end;
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevACControl.GetTransRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
var
  L: IisList;
begin
  L := (Mainboard.ADRServer as IevADRModule).RequestQueue.GetPage(APageNbr, AItemsPerPage, APageCount);
  Result := TisParamsCollection.Create;
  AddRequestsToList(Result, L);
end;

procedure TevACControl.SetLogFileThresholdInfo(const APurgeRecThreshold,
  APurgeRecCount: Integer);
begin
  Mainboard.AppSettings.AsInteger['General\LogFilePurgeRecThreshold'] := APurgeRecThreshold;
  Mainboard.AppSettings.AsInteger['General\LogFilePurgeRecCount'] := APurgeRecCount;

  GlobalLogFile.PurgeRecCount := APurgeRecCount;
  GlobalLogFile.PurgeRecThreshold := APurgeRecThreshold;
end;

procedure TevACControl.SetSettings(const ASettings: IisListOfValues);
var
  Domains: IisParamsCollection;
  D: IisListOfValues;
  i: Integer;
  L: IisList;
  bNeedToReconnect: Boolean;
begin
  bNeedToReconnect := False;
  if ASettings.ValueExists('RequestBroker') then
    mb_AppConfiguration.AsString['General\RequestBroker'] := ASettings.Value['RequestBroker'];

  if ASettings.ValueExists('StagingFolder') then
    mb_AppConfiguration.AsString['ADRClient\StagingFolder'] := ASettings.Value['StagingFolder'];

  if ASettings.ValueExists('Domains') then
  begin
    mb_AppConfiguration.ClearNode('ADRClient\Domains');
    Domains := IInterface(ASettings.Value['Domains']) as IisParamsCollection;
    for i := 0 to Domains.Count - 1 do
    begin
      D := Domains[i];
      mb_AppConfiguration.AsBoolean['ADRClient\Domains\' + Domains.ParamName(i) + '\Active'] := D.Value['Active'];
      mb_AppConfiguration.AsString['ADRClient\Domains\' + Domains.ParamName(i) + '\Host'] := D.Value['Host'];
      mb_AppConfiguration.AsString['ADRClient\Domains\' + Domains.ParamName(i) + '\Port'] := D.Value['Port'];
      mb_AppConfiguration.AsString['ADRClient\Domains\' + Domains.ParamName(i) + '\UserName'] := StrToBin(EncryptStringLocaly(D.Value['User']));
      mb_AppConfiguration.AsString['ADRClient\Domains\' + Domains.ParamName(i) + '\Password'] := StrToBin(EncryptStringLocaly(D.Value['Password']));
    end;
    bNeedToReconnect := True;
  end;

  if bNeedToReconnect then
  begin
    L := (Mainboard.ADRServer as IisTCPClient).DatagramDispatcher.GetSessions;
    for i := 0 to L.Count - 1 do
      (Mainboard.ADRServer as IisTCPClient).DatagramDispatcher.DeleteSession(L[i] as IevSession);
  end;

  if ASettings.ValueExists('MaxRequests') then
  begin
    mb_AppConfiguration.AsInteger['ADRClient\MaxRequests'] := ASettings.Value['MaxRequests'];
    (Mainboard.ADRClient as IevADRModule).RequestQueue.ExecSlots := mb_AppConfiguration.AsInteger['ADRClient\MaxRequests'];
  end;
end;

procedure TevACControl.SyncDBList(const ADomainName: String);
begin
  CreateContext(ADomainName);
  try
    Mainboard.ADRClient.SyncDBList(False);
  finally
    DestroyContext;
  end;
end;

{ TevACController }

function TevACController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevACControllerDatagramDispatcher.Create;
end;

procedure TevACController.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(ACController_Port));
  SetIPAddress('127.0.0.1');
end;

{ TevACControllerDatagramDispatcher }

function TevACControllerDatagramDispatcher.CheckClientAppID(
  const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoDeployMgrAppInfo.AppID);
end;

procedure TevACControllerDatagramDispatcher.dmCheckDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.CheckDB(ADatagram.Params.Value['ADomainName'], ADatagram.Params.Value['ADatabase']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.dmCheckDBData(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.CheckDBData(ADatagram.Params.Value['ADomainName'], ADatagram.Params.Value['ADatabase']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.dmCheckDBTransactions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.CheckDBTransactions(ADatagram.Params.Value['ADomainName'], ADatagram.Params.Value['ADatabase']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.dmFullDBCopy(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.FullDBCopy(ADatagram.Params.Value['ADomainName'], ADatagram.Params.Value['ADatabase']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.dmGetDBList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := FControl.GetDBList(ADatagram.Params.Value['ADomainName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevACControllerDatagramDispatcher.dmGetDomainStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := FControl.GetDomainStatus(ADatagram.Params.Value['ADomainName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevACControllerDatagramDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

  Res := FControl.GetFilteredLogEvents(Query);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevACControllerDatagramDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := FControl.GetLogEventDetails(ADatagram.Params.Value['EventNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevACControllerDatagramDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PageCount: Integer;
  Res: IisParamsCollection;
begin
  Res := FControl.GetLogEvents(ADatagram.Params.Value['PageNbr'],
            ADatagram.Params.Value['EventsPerPage'],
            PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevACControllerDatagramDispatcher.dmGetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  FControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.Params.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TevACControllerDatagramDispatcher.dmGetRequestList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PageCount: Integer;
  Res: IisParamsCollection;
begin
  Res := FControl.GetRequestList(ADatagram.Params.Value['APageNbr'],
            ADatagram.Params.Value['AItemsPerPage'],
            PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('APageCount', PageCount);
end;

procedure TevACControllerDatagramDispatcher.dmGetSettings(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := FControl.GetSettings;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevACControllerDatagramDispatcher.dmGetStackInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, FControl.GetStackInfo);
end;

procedure TevACControllerDatagramDispatcher.dmGetStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := FControl.GetStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevACControllerDatagramDispatcher.dmGetTransRequestList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PageCount: Integer;
  Res: IisParamsCollection;
begin
  Res := FControl.GetTransRequestList(ADatagram.Params.Value['APageNbr'],
            ADatagram.Params.Value['AItemsPerPage'],
            PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('APageCount', PageCount);
end;

procedure TevACControllerDatagramDispatcher.dmSetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.SetLogFileThresholdInfo(ADatagram.Params.Value['APurgeRecThreshold'],
    ADatagram.Params.Value['APurgeRecCount']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.dmSetSettings(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IisListOfValues;
begin
  S := IInterface(ADatagram.Params.Value['ASettings']) as IisListOfValues;
  FControl.SetSettings(S);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.dmSyncDBList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  FControl.SyncDBList(ADatagram.Params.Value['ADomainName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevACControllerDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  FControl := TevACControl.Create;
end;

procedure TevACControllerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_ACCONTROLLER_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_ACCONTROLLER_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_ACCONTROLLER_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_ACCONTROLLER_GETSETTINGS, dmGetSettings);
  AddHandler(METHOD_ACCONTROLLER_SETSETTINGS, dmSetSettings);
  AddHandler(METHOD_ACCONTROLLER_GETLOGFILETHRESHOLDINFO, dmGetLogFileThresholdInfo);
  AddHandler(METHOD_ACCONTROLLER_SETLOGFILETHRESHOLDINFO, dmSetLogFileThresholdInfo);
  AddHandler(METHOD_ACCONTROLLER_GETSTACKINFO, dmGetStackInfo);
  AddHandler(METHOD_ACCONTROLLER_GETSTATUS, dmGetStatus);
  AddHandler(METHOD_ACCONTROLLER_GETDOMAINSTATUS, dmGetDomainStatus);
  AddHandler(METHOD_ACCONTROLLER_GETREQUESTLIST, dmGetRequestList);
  AddHandler(METHOD_ACCONTROLLER_GETTRANSREQUESTLIST, dmGetTransRequestList);
  AddHandler(METHOD_ACCONTROLLER_GETDBLIST, dmGetDBList);
  AddHandler(METHOD_ACCONTROLLER_SYNCDBLIST, dmSyncDBList);
  AddHandler(METHOD_ACCONTROLLER_CHECKDB, dmCheckDB);
  AddHandler(METHOD_ACCONTROLLER_FULLDBCOPY, dmFullDBCopy);
  AddHandler(METHOD_ACCONTROLLER_CHECKDBDATA, dmCheckDBData);
  AddHandler(METHOD_ACCONTROLLER_CHECKDBTRANSACTIONS, dmCheckDBTransactions);  
end;

end.