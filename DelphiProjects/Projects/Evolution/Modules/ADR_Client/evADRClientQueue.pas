unit evADRClientQueue;

interface

uses  Classes, SysUtils, EvStreamUtils, DB, Variants, DateUtils,
      isThreadManager, isDataSet, isBaseClasses, isBasicUtils,
      evADRRequestQueue, evADRCommon, EvCommonInterfaces,
      evContext, EvConsts, EvExceptions, EvTypes, EvBasicUtils,
      isErrorUtils, evDataset;

implementation

uses EvMainboard;

type
  TevADRStartupCheckRequest = class(TevADRRequest)
  private
    FTransCheck: Boolean;
  protected
    procedure Run; override;
    function  Caption: String; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRFullDBCopyRequest = class(TevADRDBRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRSyncDBDataRequest = class(TevADRDBRequest)
  private
    FFullDBCopyScheduled: Boolean;
  protected
    function  Caption: String; override;
    procedure Run; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRCheckDBStatusRequest = class(TevADRDBRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRSyncDBListRequest = class(TevADRDBRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
    function  DeleteOnError: Boolean; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRDBDataCheckRequest = class(TevADRSyncDBDataRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRDBTranCheckRequest = class(TevADRSyncDBDataRequest)
  protected
    function  Caption: String; override;
    procedure Run; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRDBEnlistingRequest = class(TevADRRequest)
  private
    FDBList: String;
    procedure DoForDB(const ADB: String); virtual; abstract;
    function  MakeFullDBList: IisStringList; virtual;
  protected
    procedure Store; override;
    procedure Restore; override;

    function  DeleteOnError: Boolean; override;
    procedure Run; override;
    function  GetDBList: String;
    function  Description: String; override;
    function  Caption: String; override;
  public
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  TevADRCheckDBStatusEnlisterRequest = class(TevADRDBEnlistingRequest)
  private
    procedure DoForDB(const ADB: String); override;
    function  MakeFullDBList: IisStringList; override;
  protected
    function  Caption: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRDBDataCheckEnlisterRequest = class(TevADRDBEnlistingRequest)
  private
    procedure DoForDB(const ADB: String); override;
    function  MakeFullDBList: IisStringList; override;
  protected
    function  Caption: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRDBTransCheckEnlisterRequest = class(TevADRDBEnlistingRequest)
  private
    procedure DoForDB(const ADB: String); override;
    function  MakeFullDBList: IisStringList; override;
  protected
    function  Caption: String; override;
  public
    class function GetTypeID: String; override;
  end;


  TevADRFullDBCopyEnlisterRequest = class(TevADRDBEnlistingRequest)
  private
    procedure DoForDB(const ADB: String); override;
    function  MakeFullDBList: IisStringList; override;
  protected
    function  Caption: String; override;
  public
    class function GetTypeID: String; override;
  end;

  PIevADRDBInfo = ^IevADRDBInfo;



{ TevADRFullDBCopyRequest }

function TevADRFullDBCopyRequest.Caption: String;
begin
  Result := GetBackupRequestName(Database);
end;

function TevADRFullDBCopyRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

class function TevADRFullDBCopyRequest.GetTypeID: String;
begin
  Result := ADR_Clt_FullDBCopy;
end;

procedure TevADRFullDBCopyRequest.Run;
var
  BkpData: IisStream;
  DBInfo: IevADRDBInfo;
  DBInfoTask: TTaskID;

  procedure GetDBInfo(const Params: TTaskParamList);
  begin
    PIevADRDBInfo(Pointer(Integer(Params[1])))^ :=  Mainboard.ADRClient.GetDBInfo(Params[0]);
  end;

begin
  inherited;

  DBInfoTask := 0;
  try
    SetProgress('Backing up database');

    DBInfoTask := Context.RunParallelTask(@GetDBInfo, MakeTaskParams([Database, @DBInfo]), 'GetDBInfo');

    BkpData := ctx_DBAccess.BackupDB(Database, IsTempDB(Database), False, True);

    AbortIfCurrentThreadTaskTerminated;
    GlobalThreadManager.WaitForTaskEnd(DBInfoTask);
    DBInfoTask := 0;

    Mainboard.ADRServer.RestoreDB(OriginatedAt, Database, DBInfo.Version, DBInfo.LastTransactionNbr, BkpData);
  finally
    if DBInfoTask <> 0 then
    begin
      GlobalThreadManager.TerminateTask(DBInfoTask);
      GlobalThreadManager.WaitForTaskEnd(DBInfoTask);
    end;
    SetProgress('');
  end;
end;


{ TevADRSyncDBDataRequest }

procedure TevADRSyncDBDataRequest.Run;
var
  s: String;
  DBType: TevDBType;
  DataSyncPacket: IisStream;
  LastTrNbr, n: Integer;
begin
  inherited;

  // Check DB Version
  DBType := OpenDB(Database);
  s := ctx_DBAccess.GetDBVersion(DBType);

  if s <> DBVersion then
  begin
    QueueExt.ADRModule.LogError(Database + ': synchronization failed', 'Version mismatch');
    Mainboard.ADRClient.FullDBCopy(OriginatedAt, Database, False, True);
    FFullDBCopyScheduled := True;
    Exit;
  end;

  SetProgress('Looking for DB changes');

  // Check Ev_Transaction Count
  AbortIfCurrentThreadTaskTerminated;
  LastTrNbr := ctx_DBAccess.GetLastTransactionNbr(DBType);

  if (LastTransactionNbr = LastTrNbr) and (ClassType = TevADRSyncDBDataRequest) then
  begin
    QueueExt.ADRModule.LogDebugEvent(Database + ': no syncronization needed', 'It is most likely that DB syncronization was enlisted by DB integrity check request');
    Exit;
  end

  else if LastTransactionNbr > LastTrNbr then
  begin
    QueueExt.ADRModule.LogError(Database + ': synchronization failed', 'Ev_Transaction is suspicious');
    Mainboard.ADRClient.FullDBCopy(OriginatedAt, Database, False, True);
    FFullDBCopyScheduled := True;
    Exit;
  end;

  // Get data sync packet
  AbortIfCurrentThreadTaskTerminated;
  SetProgress('Preparing data sync packet');
  try
    DataSyncPacket := ctx_DBAccess.GetDataSyncPacket(DBType, LastTransactionNbr + 1, n);
    if n > 0 then
      LastTrNbr := n;
  except
    on E: EADRChangeLogCorrupted do
    begin
      QueueExt.ADRModule.LogError(Database + ': synchronization failed', E.Message);
      Mainboard.ADRClient.FullDBCopy(OriginatedAt, Database, False, True);
      FFullDBCopyScheduled := True;
      DataSyncPacket := nil;
    end;

    on E: EADRTooManyChanges do
    begin
      QueueExt.ADRModule.LogWarning(Database + ':' + E.Message, 'Data synchronization would take too long time');
      Mainboard.ADRClient.FullDBCopy(OriginatedAt, Database, False, True);
      FFullDBCopyScheduled := True;
      DataSyncPacket := nil;
    end

    else
      raise;
  end;

  if Assigned(DataSyncPacket) then
  begin
    AbortIfCurrentThreadTaskTerminated;
    Mainboard.ADRServer.ApplySyncData(OriginatedAt, Database, DBVersion, LastTrNbr, DataSyncPacket);
  end;

  SetLastTransactionNbr(LastTrNbr);
  SaveToStorage;
end;

function TevADRSyncDBDataRequest.Caption: String;
begin
  Result := GetGetSyncDataRequestName(Database);
end;

class function TevADRSyncDBDataRequest.GetTypeID: String;
begin
  Result := ADR_Clt_SyncDBData;
end;


function TevADRSyncDBDataRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

{ TevADRCheckDBStatusRequest }

function TevADRCheckDBStatusRequest.Caption: String;
begin
  Result := GetCheckDBRequestName(Database);
end;

function TevADRCheckDBStatusRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

class function TevADRCheckDBStatusRequest.GetTypeID: String;
begin
  Result := ADR_Clt_CheckDBStatus;
end;

procedure TevADRCheckDBStatusRequest.Run;
var
  TargetDBInfo, SourceDBInfo: IevADRDBInfo;
  SourceDBInfoTask, TargetDBInfoTask: TTaskID;

  procedure GetSourceDBInfo(const Params: TTaskParamList);
  begin
    PIevADRDBInfo(Pointer(Integer(Params[1])))^ := Mainboard.ADRClient.GetDBInfo(Params[0]);
  end;

  procedure GetTargetDBInfo(const Params: TTaskParamList);
  begin
    PIevADRDBInfo(Pointer(Integer(Params[1])))^ := Mainboard.ADRServer.GetDBInfo(Params[0]);
  end;

begin
  inherited;

  //Get DB Info form the Target and Source
  SetProgress('Checking DB');
  try
    SourceDBInfoTask := Context.RunParallelTask(@GetSourceDBInfo, MakeTaskParams([DataBase, @SourceDBInfo]), 'GetSourceDBInfo');
    TargetDBInfoTask := Context.RunParallelTask(@GetTargetDBInfo, MakeTaskParams([DataBase, @TargetDBInfo]), 'GetTargetDBInfo');

    SetProgress('Analyzing Source DB');
    Context.WaitForTaskEnd(SourceDBInfoTask);
    SetProgress('Analyzing Target DB');
    Context.WaitForTaskEnd(TargetDBInfoTask);

    AbortIfCurrentThreadTaskTerminated;

    if SourceDBInfo.Status = dsPresent then
    begin
      if TargetDBInfo.Status = dsUnknown then
      begin
        SetProgress('Waiting for ADR Server connection');
        while TargetDBInfo.Status = dsUnknown do
        begin
          AbortIfCurrentThreadTaskTerminated;
          Snooze(5000);
          TargetDBInfo := Mainboard.ADRServer.GetDBInfo(DataBase);
        end;
      end;

      SetProgress('Analyzing received data');

      if (TargetDBInfo.Status = dsError) or (TargetDBInfo.Status = dsPresent) and (SourceDBInfo.Version <> TargetDBInfo.Version) or (TargetDBInfo.Status = dsAbsent) then
        Mainboard.ADRClient.FullDBCopy(OriginatedAt, DataBase, False, True) //Add Full Copy request if versions mismatchs

      else if (TargetDBInfo.Status in [dsPresent, dsPreparing]) and (SourceDBInfo.Version = TargetDBInfo.Version) then
      begin
        if SourceDBInfo.LastTransactionNbr <> TargetDBInfo.LastTransactionNbr then  // there are any changes
        begin
          if SourceDBInfo.LastTransactionNbr > TargetDBInfo.LastTransactionNbr then
            //Add Sync request
            Mainboard.ADRClient.SynchronizeDB(OriginatedAt, DataBase, TargetDBInfo.Version, TargetDBInfo.LastTransactionNbr, True, True)  // sync only difference
          else
            Mainboard.ADRClient.FullDBCopy(OriginatedAt, DataBase, False, True); //If DB has been changed by irregular way then make a full copy
        end
        else
          QueueExt.ADRModule.LogDebugEvent(DataBase + ' is up to date');
      end;
    end

    else
      QueueExt.ADRModule.LogError(SourceDBInfo.Database + ' cannot be found on the source side');

  finally
    SetProgress('');
  end;
end;


{ TevADRSyncDBListRequest }

function TevADRSyncDBListRequest.Caption: String;
begin
  Result := GetSyncDBListName;
end;

function TevADRSyncDBListRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

class function TevADRSyncDBListRequest.GetTypeID: String;
begin
  Result := ADR_Clt_SyncDBList;
end;

procedure TevADRSyncDBListRequest.Run;
var
  DBList: IisStringList;
  i: Integer;
begin
  inherited;

  SetProgress('Getting Source DB List');
  try
    DBList := Mainboard.ADRClient.GetDBList;

    for i := DBList.Count - 1 downto 0 do
      if not IsClientDB(DBList[i]) then
        DBList.Delete(i);

    Mainboard.ADRServer.SyncDBList(DBList);
  finally
    SetProgress('');
  end;
end;


{ TevADRDBDataCheckRequest }

procedure TevADRDBDataCheckRequest.Run;
var
  DBData: IisStream;
  TargetDBInfo: IevADRDBInfo;
begin
  // Check target DB
  SetProgress('Checking Source DB for changes');
  TargetDBInfo := Mainboard.ADRServer.GetDBInfo(Database);
  AbortIfCurrentThreadTaskTerminated;
  SetDBVersion(TargetDBInfo.Version);
  SetLastTransactionNbr(TargetDBInfo.LastTransactionNbr);
  SaveToStorage;

  // Because DB may get changed in between GetDataSyncPacket() and GetDBHash() we need to use a "retry" logic
  repeat
    inherited;

    if FFullDBCopyScheduled then
    begin
      QueueExt.ADRModule.LogDebugEvent(Database + ': DB intergrity checking was canceled', 'Full DB copy was enlisted');
      Exit;
    end;

    AbortIfCurrentThreadTaskTerminated;

    DBData := ctx_DBAccess.GetDBHash(OpenDB(Database), LastTransactionNbr); // will return nil if transaction does not match
    if DBData = nil then
      QueueExt.ADRModule.LogDebugEvent(Database + ': DB intergrity checking was postponed', 'Database is changed. Data synchronization will be performed.');
  until DBData <> nil;

  AbortIfCurrentThreadTaskTerminated;
  Mainboard.ADRServer.CheckDBData(Database, DBVersion, LastTransactionNbr, DBData);
end;

function TevADRDBDataCheckRequest.Caption: String;
begin
  Result := GetGetDBHashRequestName(Database);
end;

class function TevADRDBDataCheckRequest.GetTypeID: String;
begin
  Result := ADR_Clt_CheckDBData;
end;


{ TevADRCheckDBStatusEnlisterRequest }

procedure TevADRCheckDBStatusEnlisterRequest.DoForDB(const ADB: String);
begin
  Mainboard.ADRClient.CheckDB(OriginatedAt, ADB);
end;

function TevADRCheckDBStatusEnlisterRequest.Caption: String;
begin
  Result := ADR_Clt_EnlistCheckDBStatus + ' for ' + inherited Caption;
end;

function TevADRCheckDBStatusEnlisterRequest.MakeFullDBList: IisStringList;
var
  i: Integer;
begin
  Result := inherited MakeFullDBList;

  i := Result.IndexOf(DB_System); //#4
  if i <> -1 then
    Result.Move(i, 0);

  i := Result.IndexOf(DB_S_Bureau); //#3
  if i <> -1 then
    Result.Move(i, 0);

  i := Result.IndexOf(DB_Cl_Base); //#2
  if i <> -1 then
    Result.Move(i, 0);

  i := Result.IndexOf(DB_TempTables); //#1
  if i <> -1 then
    Result.Move(i, 0);
end;

class function TevADRCheckDBStatusEnlisterRequest.GetTypeID: String;
begin
  Result := ADR_Clt_EnlistCheckDBStatus;
end;


{ TevADRDBDataCheckEnlisterRequest }

procedure TevADRDBDataCheckEnlisterRequest.DoForDB(const ADB: String);
begin
  Mainboard.ADRClient.CheckDBData(ADB);
end;

function TevADRDBDataCheckEnlisterRequest.Caption: String;
begin
  Result := ADR_Clt_CheckDBData + ' for ' + inherited Caption;
end;

function TevADRDBDataCheckEnlisterRequest.MakeFullDBList: IisStringList;
var
  i: Integer;
begin
  Result := inherited MakeFullDBList;
  i := Result.IndexOf(DB_TempTables);
  if i <> -1 then
    Result.Delete(i);
end;

class function TevADRDBDataCheckEnlisterRequest.GetTypeID: String;
begin
  Result := ADR_Clt_EnlistCompareDB;
end;


{ TevADRDBEnlistingRequest }

constructor TevADRDBEnlistingRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FDBList := AParams[0];
end;

function TevADRDBEnlistingRequest.GetDBList: String;
begin
  Result := FDBList;
end;

function TevADRDBEnlistingRequest.Description: String;
var
 fmt: String;
 dbs: String;
begin
  fmt := 'Domain: %s'#13 +
         'Databases: %s'#13 +
         'Started at: %s'#13 +
         'Finished: %s'#13;

  if FDBList = '' then
    dbs := 'All DBs'
  else
    dbs := FDBList;

  Result := Format(fmt, [Domain, dbs, DateTimeToText(StartedAt), DateTimeToText(FinishedAt)]);
end;

function TevADRDBEnlistingRequest.Caption: String;
begin
  if FDBList = '' then
    Result := 'All DBs'
  else
    Result := 'Specified DBs';
end;

function TevADRDBEnlistingRequest.MakeFullDBList: IisStringList;
var
  Databases: IisStringList;
  i: Integer;
begin
  Databases := Mainboard.ADRClient.GetDBList;
  Result := TisStringList.Create;
  Result.Capacity := Databases.Count;

  // Shuffle items
  while Databases.Count > 0 do
  begin
    i := RandomInteger(0, Databases.Count - 1);
    Result.Add(Databases[i]);
    Databases.Delete(i);
  end;
end;

procedure TevADRDBEnlistingRequest.Run;
var
  WorkingDBList: IisStringList;
  Progress, ProgressDelta, ProgressStep: Real;
  i: Integer;
begin
  WorkingDBList := TisStringList.Create;

  WorkingDBList.CommaText := FDBList;
  if WorkingDBList.Count = 0  then
    WorkingDBList := MakeFullDBList;

  Progress := 0;
  ProgressDelta := 100 / WorkingDBList.Count;
  ProgressStep := 0;
  SetProgress('Enlisted ' + '0%');

  for i := 0 to WorkingDBList.Count - 1 do
  begin
    if CurrentThreadTaskTerminated then
      break;

    DoForDB(WorkingDBList[i]);

    ProgressStep := ProgressStep + ProgressDelta;
    if ProgressStep >= 1 then
    begin
      Progress := Progress + ProgressStep;
      ProgressStep := 0;
      SetProgress('Enlisted ' + IntToStr(Round(Progress)) + '%');
    end;
  end;
end;

procedure TevADRDBEnlistingRequest.Restore;
begin
  inherited;
  FDBList := Storage.AsString[GetID + '\DBList'];
end;

procedure TevADRDBEnlistingRequest.Store;
begin
  inherited;
  Storage.AsString[GetID + '\DBList'] := FDBList;
end;

function TevADRDBEnlistingRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

{ TevADRFullDBCopyEnlisterRequest }

procedure TevADRFullDBCopyEnlisterRequest.DoForDB(const ADB: String);
begin
  Mainboard.ADRClient.FullDBCopy(OriginatedAt, ADB, True);
end;

function TevADRFullDBCopyEnlisterRequest.Caption: String;
begin
  Result := ADR_Clt_EnlistFullDBCopy + ' for ' + inherited Caption;
end;

function TevADRFullDBCopyEnlisterRequest.MakeFullDBList: IisStringList;
var
  i: Integer;
begin
  Result := inherited MakeFullDBList;
  i := Result.IndexOf(DB_TempTables);
  if i <> -1 then
    Result.Delete(i);
end;

class function TevADRFullDBCopyEnlisterRequest.GetTypeID: String;
begin
  Result := ADR_Clt_EnlistFullDBCopy;
end;


{ TevClientADRDBRequest }

{ TevADRStartupCheckRequest }

function TevADRStartupCheckRequest.Caption: String;
begin
  Result := 'Startup Check';
end;

constructor TevADRStartupCheckRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FTransCheck := AParams[0];
end;

function TevADRStartupCheckRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

class function TevADRStartupCheckRequest.GetTypeID: String;
begin
  Result := ADR_Clt_StartupCheck;
end;

procedure TevADRStartupCheckRequest.Run;
var
  i: Integer;
  EvoDomain: IevDomainInfo;
begin
  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      try
        EvoDomain := mb_GlobalSettings.DomainInfoList[i];
        if mb_AppConfiguration.AsBoolean['ADRClient\Domains\' + EvoDomain.DomainName + '\Active'] then
        begin
          Mainboard.ContextManager.CreateThreadContext(
            EncodeUserAtDomain(sADRUserName, EvoDomain.DomainName),
            EvoDomain.SystemAccountInfo.Password);

          Mainboard.ADRClient.SyncDBList;

          if FTransCheck then
            (QueueObj as IevADRRequestQueueExt).ADRModule.AddDBRequestsIfMissing(NowUTC, ADR_Clt_EnlistCheckDBTrans, ['', '', 0], True)
          else
            (QueueObj as IevADRRequestQueueExt).ADRModule.AddDBRequestsIfMissing(NowUTC, ADR_Clt_EnlistCheckDBStatus, ['', '', 0], True);
        end;
      except
        on E: Exception do
          (QueueObj as IevADRRequestQueueExt).ADRModule.LogError('Cannot schedule initial DB check', MakeFullError(E));
      end;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

    AbortIfCurrentThreadTaskTerminated;
  end;
end;


{ TevADRDBTranCheckRequest }

function TevADRDBTranCheckRequest.Caption: String;
begin
  Result := GetDBTransCheckRequestName(Database);
end;

class function TevADRDBTranCheckRequest.GetTypeID: String;
begin
  Result := ADR_Clt_CheckDBTrans;
end;

procedure TevADRDBTranCheckRequest.Run;
var
  TargetDBInfo: IevADRDBInfo;
  TranStatus: IisListOfValues;
begin
  // Check target DB
  SetProgress('Checking Source DB for changes');
  TargetDBInfo := Mainboard.ADRServer.GetDBInfo(Database);
  AbortIfCurrentThreadTaskTerminated;
  SetDBVersion(TargetDBInfo.Version);
  SetLastTransactionNbr(TargetDBInfo.LastTransactionNbr);
  SaveToStorage;

  // Because DB may get changed in between GetDataSyncPacket() and Query we need to use a "retry" logic
  while not CurrentThreadTaskTerminated do
  begin
    inherited;

    if FFullDBCopyScheduled then
    begin
      QueueExt.ADRModule.LogDebugEvent(Database + ': DB transactions check was canceled', 'Full DB copy was enlisted');
      Exit;
    end;

    AbortIfCurrentThreadTaskTerminated;

    TranStatus := ctx_DBAccess.GetTransactionsStatus(OpenDB(Database));

    if TranStatus.Value['LastTransNbr'] = LastTransactionNbr then
    begin
      Mainboard.ADRServer.CheckDBTransactions(Database, DBVersion, LastTransactionNbr, TranStatus.Value['TransCount']);
      break;
    end
    else
      QueueExt.ADRModule.LogDebugEvent(Database + ': DB transactions checking was postponed', 'Database is changed. Data synchronization will be performed.');
  end;
end;

{ TevADRDBTransCheckEnlisterRequest }

function TevADRDBTransCheckEnlisterRequest.Caption: String;
begin
  Result := ADR_Clt_CheckDBTrans + ' for ' + inherited Caption;
end;

procedure TevADRDBTransCheckEnlisterRequest.DoForDB(const ADB: String);
begin
  Mainboard.ADRClient.CheckDBTransactions(ADB);
end;

class function TevADRDBTransCheckEnlisterRequest.GetTypeID: String;
begin
  Result := ADR_Clt_EnlistCheckDBTrans;
end;

function TevADRDBTransCheckEnlisterRequest.MakeFullDBList: IisStringList;
var
  i: Integer;
begin
  Result := inherited MakeFullDBList;
  i := Result.IndexOf(DB_TempTables);
  if i <> -1 then
    Result.Delete(i);
end;

initialization
  ObjectFactory.Register([TevADRFullDBCopyRequest, TevADRSyncDBDataRequest, TevADRDBDataCheckRequest,
                          TevADRCheckDBStatusRequest, TevADRSyncDBListRequest, TevADRCheckDBStatusEnlisterRequest,
                          TevADRDBDataCheckEnlisterRequest, TevADRFullDBCopyEnlisterRequest,
                          TevADRStartupCheckRequest, TevADRDBTranCheckRequest, TevADRDBTransCheckEnlisterRequest]);

finalization
  SafeObjectFactoryUnRegister([TevADRFullDBCopyRequest, TevADRSyncDBDataRequest, TevADRDBDataCheckRequest,
                          TevADRCheckDBStatusRequest, TevADRSyncDBListRequest, TevADRCheckDBStatusEnlisterRequest,
                          TevADRDBDataCheckEnlisterRequest, TevADRFullDBCopyEnlisterRequest,
                          TevADRStartupCheckRequest, TevADRDBTranCheckRequest, TevADRDBTransCheckEnlisterRequest]);

end.