unit evADRClientPxy;

interface

uses SysUtils,
     isBaseClasses, isSocket, isBasicUtils, isAppIDs, EvConsts,
     evTransportInterfaces, EvTransportShared, EvTransportDatagrams,  EvStreamUtils,
     EvMainboard, evRemoteMethods, evCommonInterfaces, EvBasicUtils,
     evExceptions, evADRRequestQueue, evADRCommon, evContext, SEncryptionRoutines;


implementation

uses isSettings, DateUtils;

const Request_Evo_Trans = 'Receive Data';

type
  // All ACs
  TevADRClientProxy = class(TevCustomADRModule, IevProxyModule, IevADRClient, IevTCPServer)
  private
    FTCPServer: IevTCPServer;
    function  GetContextSession: IevSession;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
  protected
    procedure DoOnConstruction; override;
    function  GetWorkingDir: String; override;

    property  TCPServer: IevTCPServer read FTCPServer implements IevTCPServer;

    function  Connected: Boolean;
    procedure OnDisconnect;

    procedure SetActive(const AValue: Boolean); override;
    function  GetDBInfo(const ADatabase: String): IevADRDBInfo;
    function  GetDBList: IisStringList;
    procedure SyncDBList(const ATopPosition: Boolean = False);
    procedure CheckDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATopPosition: Boolean = False);
    procedure FullDBCopy(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ACheckQueue: Boolean = False; const ATopPosition: Boolean = False);
    procedure SynchronizeDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATargetDBVersion: String;
                            const ATargetLastTransactionNbr: Integer; const ACheckQueue: Boolean = False;
                            const ATopPosition: Boolean = False);
    procedure CheckDBData(const ADataBase: String; const ATopPosition: Boolean = False);
    procedure CheckDBTransactions(const ADataBase: String; const ATopPosition: Boolean = False);
    function  GetStatus: IisListOfValues;
  public
    class function GetTypeID: String; override;
  end;



  // AC -> AS dispatcher
  TevASServerDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmGetDBInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSyncDBList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRestoreDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmApplySyncData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDBData(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmCheckDBTransactions(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmDeleteDBRequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRebuildTT(const ADatagram: IevDatagram; out AResult: IevDatagram);

    procedure dmSyncOfflineStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmStartTrans(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetTransPosition(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmTransFragment(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure RegisterHandlers; override;
    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;
  end;


  IevADRTransRequestExt = interface(IevADRTransRequest)
  ['{EEF44744-EA2C-4A23-B17A-A6F051B8CAF2}']
    function  Complete: Boolean;
    function  TransPosition: Integer;
    procedure AddFragment(const AFragment: IisStream);
  end;

  TevADRTransRequest = class(TevADRDBRequest, IevADRTransRequest, IevADRTransRequestExt)
  private
    FData: IisStream;
    FMethod: String;
    FTransPosition: Integer;
    FDataSize: Integer;
    FComplete: Boolean;
    FLastTransmission: TDateTime;
    function  GetData: IisStream;
    procedure UpdateProgress;
    function  IsOversizedData: Boolean;
    procedure StoreData;
  protected
    procedure Restore; override;
    procedure Store; override;
    procedure RemoveFromStorage; override;

    function  DeleteOnError: Boolean; override;
    function  ReadyToRun: Boolean; override;
    function  Caption: String; override;
    procedure Run; override;
    function  ADRRequestID: String;
    function  Complete: Boolean;
    function  TransPosition: Integer;
    procedure AddFragment(const AFragment: IisStream);
  public
    class function GetTypeID: String; override;
    constructor Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant); override;
  end;


  // AC -> AS connections
  TevTCPServer = class(TevCustomTCPServer)
  private
    procedure RefreshSettings;
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    procedure SetActive(const AValue: Boolean); override;
  end;


function GetADRClient: IevADRClient;
begin
  Result := TevADRClientProxy.Create;
end;


{ TevASServerDispatcher }

procedure TevASServerDispatcher.AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram);
begin
  Mainboard.ContextManager.RestoreThreadContext;
  inherited;
end;

procedure TevASServerDispatcher.BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader);
var
  DomainInfo: IevDomainInfo;
  Session: IevSession;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;

  Session := FindSessionByID(ADatagramHeader.SessionID);
  if (Session.State = ssActive) and not IsTransportDatagram(ADatagramHeader) then
  begin
    DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(Session.Login);
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sADRUserName, DomainInfo.DomainName),
      DomainInfo.SystemAccountInfo.Password, '', ADatagramHeader.ContextID);
    ctx_DBAccess.SetADRContext(True);
  end;
end;

function TevASServerDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoADRClientAppInfo.AppID);
end;

procedure TevASServerDispatcher.dmApplySyncData(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRServer.ApplySyncData(
    ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC),
    ADatagram.Params.Value['ADatabase'],
    ADatagram.Params.Value['ADBVersion'],
    ADatagram.Params.Value['ALastTransactionNbr'],
    IInterface(ADatagram.Params.Value['ASyncData']) as IisStream);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmCheckDBData(const ADatagram: IevDatagram;
  out AResult: IevDatagram);
begin
  Mainboard.ADRServer.CheckDBData(
    ADatagram.Params.Value['ADatabase'],
    ADatagram.Params.Value['ADBVersion'],
    ADatagram.Params.Value['ALastTransactionNbr'],
    IInterface(ADatagram.Params.Value['ADBData']) as IisStream);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmCheckDBTransactions(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRServer.CheckDBTransactions(
    ADatagram.Params.Value['ADatabase'],
    ADatagram.Params.Value['ADBVersion'],
    ADatagram.Params.Value['ALastTransactionNbr'],
    ADatagram.Params.Value['ATransCount']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmDeleteDBRequests(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRServer.DeleteDBRequests(ADatagram.Params.Value['ADatabase']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmGetDBInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  DBInfo: IevADRDBInfo;
begin
  DBInfo := Mainboard.ADRServer.GetDBInfo(ADatagram.Params.Value['ADatabase']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, DBInfo);
end;

procedure TevASServerDispatcher.dmGetStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Mainboard.ADRServer.GetStatus);
end;

procedure TevASServerDispatcher.dmGetTransPosition(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  R: IevADRTransRequestExt;
  TransPos: Integer;
begin
  R := (Mainboard.ADRClient as IevADRModule).RequestQueue.FindRequestByID(ADatagram.Params.Value['Receipt']) as IevADRTransRequestExt;
  if Assigned(R) then
    TransPos := R.TransPosition
  else
    TransPos := -1;  

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, TransPos);
end;

procedure TevASServerDispatcher.dmRebuildTT(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRServer.RebuildTT(
    ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC),
    ADatagram.Params.Value['ADatabase']);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmRestoreDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRServer.RestoreDB(
    ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC),
    ADatagram.Params.Value['ADatabase'],
    ADatagram.Params.Value['ADBVersion'],
    ADatagram.Params.Value['ALastTransactionNbr'],
    IInterface(ADatagram.Params.Value['ADBData']) as IisStream);

  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmStartTrans(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisList;
  i: Integer;
  R: IevADRTransRequestExt;
begin
  // Delete unfinished uploads as they don't make sense anymore
  L := (Mainboard.ADRClient as IevADRModule).RequestQueue.GetRequests('', [arsQueued]);
  for i := 0 to L.Count - 1 do
  begin
    R := L[i] as IevADRTransRequestExt;
    if not R.Complete then
    begin
      (Mainboard.ADRClient as IevADRModule).RequestQueue.DeleteRequest(R);
      R := nil;
    end;
  end;

  R := (Mainboard.ADRClient as IevADRModule).RequestQueue.NewDBRequest(
         ADatagram.Params.TryGetValue('AOriginatedAt', NowUTC),
         Request_Evo_Trans,
         [ADatagram.Params.Value['Database'], ADatagram.Params.Value['DBVersion'],
         ADatagram.Params.Value['LastTransactionNbr'], ADatagram.Params.Value['Method'],
         ADatagram.Params.Value['DataSize']]) as IevADRTransRequestExt;

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, R.ID);
end;

procedure TevASServerDispatcher.dmSyncDBList(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.ADRServer.SyncDBList(IInterface(ADatagram.Params.Value['ADBList']) as IisStringList);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevASServerDispatcher.dmSyncOfflineStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisList;
  R: IevADRDBRequest;
  DBs: IisStringList;
  Res: IisListOfValues;
  i: Integer;
begin
  AResult := CreateResponseFor(ADatagram.Header);

  L := (Mainboard.ADRServer as IevADRModule).RequestQueue.GetRequests('', [arsFinishedWithErrors], '');

//TODO: remove in 2012
  DBs := TisStringList.CreateUnique;
  for i := 0 to L.Count - 1 do
    if Supports(L[i], IevADRDBRequest) then
    begin
      R := L[i] as IevADRDBRequest;
      if R.Database <> '' then
        DBs.Add(R.Database);
    end;
  AResult.Params.AddValue('FullDBCopyList', DBs);
//

  Res := TisListOfValues.Create;
  for i := 0 to L.Count - 1 do
    if Supports(L[i], IevADRDBRequest) then
    begin
      R := L[i] as IevADRDBRequest;
      if R.Database <> '' then
        Res.AddValue(R.Database, R.OriginatedAt);
    end;
  AResult.Params.AddValue('ErroredDBList', Res);
end;

procedure TevASServerDispatcher.dmTransFragment(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  R: IevADRTransRequestExt;
  Receipt: String;
begin
  Receipt := ADatagram.Params.Value['Receipt'];
  R := (Mainboard.ADRClient as IevADRModule).RequestQueue.FindRequestByID(Receipt) as IevADRTransRequestExt;
  if Assigned(R) then
  begin
    R.AddFragment(IInterface(ADatagram.Params.Value['Fragment']) as IisStream);
    AResult := CreateResponseFor(ADatagram.Header);
  end
  else
    raise EADRMissingRequest.CreateFmt('Request %s does not exist anymore', [Receipt]);  
end;

function TevASServerDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
var
  DomainInfo: IevDomainInfo;
  sPassword: String;
  i: Integer;
  S: IevSession;
begin
  try
    Result := inherited DoOnLogin(ADatagram);

    Result := Result and mb_AppConfiguration.AsBoolean['ADRServer\Domains\' + ADatagram.Header.User + '\Active'];
    if Result then
    begin
      sPassword := mb_AppConfiguration.AsString['ADRServer\Domains\' + ADatagram.Header.User + '\Password'];
      sPassword := DecryptStringLocaly(BinToStr(sPassword), '');

      DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADatagram.Header.User);

      if not Assigned(DomainInfo) or not AnsiSameStr(ADatagram.Header.Password, sPassword) then
        raise ESecurityViolation.Create('Invalid ADR user or password');

      S := nil;
      LockSessionList;
      try
        for i := 0 to SessionCount - 1 do
        begin
          S := GetSessionByIndex(i);
          if (S.SessionID <> ADatagram.Header.SessionID) and
              AnsiSameText(S.Login, ADatagram.Header.User) and (S.State = ssActive) then
            break;

          S := nil;
        end;
      finally
        UnlockSessionList;
      end;

      if Assigned(S) then
        if S.CheckIfAlive then
          raise ESecurityViolation.Create('Multiple ADR connections not allowed');
    end;
  except
    on E:Exception do
    begin
      S := FindSessionByID(ADatagram.Header.SessionID);
      (Mainboard.ADRClient as IevADRModule).LogWarning(Format('Cannot authenticate client: %s', [ADatagram.Header.User]),
        Format('IP Address: ', [S.GetRemoteIPAddress]) + #13 + E.Message);
      raise;
    end;
  end;
end;

procedure TevASServerDispatcher.DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState);
begin
  inherited;

  if (APreviousState <> ssActive) and (ASession.State = ssActive) then
    (Mainboard.ADRClient as IevADRModule).LogEvent('Client connected',
      Format('Client IP: %s' + #13 + 'Login: %s', [ASession.GetRemoteIPAddress, ASession.Login]))

  else if (APreviousState = ssActive) and (ASession.State <> ssActive) then
    (Mainboard.ADRClient as IevADRModule).LogEvent('Client disconnected', Format('Login: %s', [ASession.Login]));
end;

procedure TevASServerDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_AS_GETDBINFO, dmGetDBInfo);
  AddHandler(METHOD_AS_SYNCDBLIST, dmSyncDBList);
  AddHandler(METHOD_AS_RESTOREDB, dmRestoreDB);
  AddHandler(METHOD_AS_APPLYSYNCDATA, dmApplySyncData);
  AddHandler(METHOD_AS_CHECKDBDATA, dmCheckDBData);
  AddHandler(METHOD_AS_CHECKDBTRANSACTIONS, dmCheckDBTransactions);
  AddHandler(METHOD_AS_DELETEDBREQUESTS, dmDeleteDBRequests);
  AddHandler(METHOD_AS_GETSTATUS, dmGetStatus);
  AddHandler(METHOD_AS_REBUILDTT, dmRebuildTT);

  AddHandler(METHOD_AS_SYNCOFFLINESTATUS, dmSyncOfflineStatus);
  AddHandler(METHOD_AS_STARTTRANS, dmStartTrans);
  AddHandler(METHOD_AS_GETTRANSPOSITION, dmGetTransPosition);
  AddHandler(METHOD_AS_TRANSFRAGMENT, dmTransFragment);
end;

{ TevTCPServer }

function TevTCPServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevASServerDispatcher.Create;
end;

procedure TevTCPServer.DoOnConstruction;
begin
  inherited;
  RefreshSettings;
end;


procedure TevTCPServer.RefreshSettings;
begin
  SetPort(mb_AppConfiguration.GetValue('ADRServer\Port', ADRClient_Port));
  SetIPAddress(Mainboard.AppSettings.AsString['Network\AStoClientIPAddr']);

  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).SSLRootCertFile := AppDir + 'root.pem';
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).SSLCertFile := AppDir + 'cert.pem';
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).SSLKeyFile := AppDir + 'key.pem';
end;

procedure TevTCPServer.SetActive(const AValue: Boolean);
begin
  if not GetActive and AValue then
    RefreshSettings;
  inherited;
end;

{ TevADRTransRequest }

procedure TevADRTransRequest.AddFragment(const AFragment: IisStream);
begin
  CheckCondition(Status = arsQueued, 'Request is complete');

  GetData.Position := FTransPosition;
  GetData.Size := GetData.Position;
  GetData.CopyFrom(AFragment, 0);
  FTransPosition := GetData.Size;
  StoreData;
  FLastTransmission := Now;
  FComplete := FDataSize = FTransPosition;
  UpdateProgress;

  if FComplete then
    (QueueObj as IevADRRequestQueue).WakeUpPulser;
end;

function TevADRTransRequest.ADRRequestID: String;
begin
  Result := FMethod;
end;

function TevADRTransRequest.Caption: String;
begin
  Result := FMethod;
  if Database <> '' then
    Result := Result + ': ' + Database;
end;

function TevADRTransRequest.Complete: Boolean;
begin
  Result := FComplete;
end;

constructor TevADRTransRequest.Create(const AOriginatedAt: TUTCDateTime; const AParams: array of Variant);
begin
  inherited;
  FMethod := AParams[3];
  FDataSize := AParams[4];
end;

function TevADRTransRequest.DeleteOnError: Boolean;
begin
  Result := True;
end;

function TevADRTransRequest.GetData: IisStream;
var
  S: IisStream;
begin
  if not Assigned(FData) then
  begin
    if IsOversizedData then
    begin
      FData := Storage.AsBlob[GetID + '\Data'];
      if not FComplete and not Assigned(FData) then
      begin
        S := TisStream.Create;
        Storage.AsBlob[GetID + '\Data'] := S;
        FData := Storage.AsBlob[GetID + '\Data'];
      end;
    end
    else
    begin
      FData := TisStream.Create;
      FData.AsString := BinToStr(Storage.AsString[GetID + '\Data']);
    end
  end;

  Result := FData;
end;

class function TevADRTransRequest.GetTypeID: String;
begin
  Result := Request_Evo_Trans;
end;

procedure TevADRTransRequest.Restore;
begin
  inherited;
  FMethod := Storage.AsString[GetID + '\Method'];
  FDataSize := Storage.AsInteger[GetID + '\DataSize'];
  FTransPosition := Storage.AsInteger[GetID + '\TransPosition'];
  FComplete := FDataSize = FTransPosition;
  UpdateProgress;  
end;

function TevADRTransRequest.ReadyToRun: Boolean;
//var
//  i: Integer;
//  Req: IevADRRequest;
//  ExecRequests: IisList;
begin
  Result := FComplete;

  if Result then
  begin
{  // one uploading request per Domain
    ExecRequests := QueueExt.GetRequestsByStatus([arsExecuting]);
    for i := 0 to ExecRequests.Count - 1 do
    begin
      Req := ExecRequests[i] as IevADRRequest;
      if AnsiSameText(Req.Domain, Domain) then
      begin
        Result := False;
        break;
      end;
    end;
 }
  end

  else
  begin
    if Assigned(FData) and (FLastTransmission <> 0) and (MinutesBetween(FLastTransmission, Now) > 10) then
      FData := nil; // close file
  end;
end;

procedure TevADRTransRequest.RemoveFromStorage;
begin
  FData := nil; // to close file
  inherited;
end;

procedure TevADRTransRequest.Run;
var
  D, R: IevDatagram;
begin
  inherited;

  (Mainboard.ADRClient as IevADRModule).Statistics.SetCounter('LastActivity', Caption);
    
  SetProgress('Creating request');
  D := TevDatagram.CreateFromStream(GetData);
  (Mainboard.ADRClient as IevTCPServer).DatagramDispatcher.DispatchDatagram(D, R);
end;

procedure TevADRTransRequest.Store;
begin
  inherited;
  Storage.AsString[GetID + '\Method'] := FMethod;
  Storage.AsInteger[GetID + '\DataSize'] := FDataSize;
  StoreData;
end;

function TevADRTransRequest.TransPosition: Integer;
begin
  Result := FTransPosition;
end;

procedure TevADRTransRequest.UpdateProgress;
var
  s: String;
begin
  if FComplete then
    s := 'Transfer complete. Received ' + SizeToString(FDataSize)
  else
    s := Format('Received %s  (%s of %s)',
    [IntToStr(Round(FTransPosition / FDataSize * 100)) + '%',
     SizeToString(FTransPosition), SizeToString(FDataSize)]);

  SetProgress(s);
end;


function TevADRTransRequest.IsOversizedData: Boolean;
begin
  Result := FDataSize > ADR_TransBlockSize;
end;

procedure TevADRTransRequest.StoreData;
begin
  if Assigned(FData) then
  begin
    Storage.BeginUpdate;
    try
      Storage.AsInteger[GetID + '\TransPosition'] := FTransPosition;
      if IsOversizedData then
        Storage.AsBlob[GetID + '\Data'] := GetData
      else
        Storage.AsString[GetID + '\Data'] := StrToBin(GetData.AsString);
    finally
      Storage.EndUpdate;
    end;
  end;
end;

{ TevADRClientProxy }

procedure TevADRClientProxy.CheckDB(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ATopPosition: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AC_CHECKDB);
  D.Params.AddValue('AOriginatedAt', AOriginatedAt);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ATopPosition', ATopPosition);
  D := SendRequest(D);
end;

procedure TevADRClientProxy.CheckDBData(const ADataBase: String; const ATopPosition: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AC_CHECKDBDATA);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ATopPosition', ATopPosition);
  D := SendRequest(D);
end;

procedure TevADRClientProxy.CheckDBTransactions(const ADataBase: String; const ATopPosition: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AC_CHECKDBTRANSACTIONS);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ATopPosition', ATopPosition);
  D := SendRequest(D);
end;

function TevADRClientProxy.Connected: Boolean;
var
  S: IevSession;
begin
  S := GetContextSession;
  Result := Assigned(S) and (S.State = ssActive);
end;

procedure TevADRClientProxy.DoOnConstruction;
begin
  inherited;
  FTCPServer := TevTCPServer.Create;
  RequestQueue.SetDomainBallancing(False);
end;

procedure TevADRClientProxy.FullDBCopy(const AOriginatedAt: TUTCDateTime; const ADataBase: String; const ACheckQueue, ATopPosition: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AC_FULLDBCOPY);
  D.Params.AddValue('AOriginatedAt', AOriginatedAt);
  D.Params.AddValue('ADatabase', ADatabase);
  D.Params.AddValue('ACheckQueue', ACheckQueue);
  D.Params.AddValue('ATopPosition', ATopPosition);
  D := SendRequest(D);
end;

function TevADRClientProxy.GetContextSession: IevSession;
var
  L: IisList;
  S: IevSession;
  i: Integer;
begin
  Result := nil;
  L := FTCPServer.DatagramDispatcher.GetSessions;
  for i := 0 to L.Count - 1 do
  begin
    S := L[i] as IevSession;
    if AnsiSameText(S.Login, ctx_DomainInfo.DomainName) then
    begin
      Result := S;
      break;
    end;
  end;
end;

function TevADRClientProxy.GetDBInfo(
  const ADatabase: String): IevADRDBInfo;
begin
  Assert(False);
end;

function TevADRClientProxy.GetDBList: IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AC_GETDBLIST);
  D := SendRequest(D);
  if Assigned(D) then
    Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList
  else
    Result := nil;
end;

function TevADRClientProxy.GetStatus: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AC_GETSTATUS);
  D := SendRequest(D);
  if Assigned(D) then
    Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues
  else
    Result := nil;
end;

class function TevADRClientProxy.GetTypeID: String;
begin
  Result := 'ADR Communicator';
end;

function TevADRClientProxy.GetWorkingDir: String;
begin
  Result := NormalizePath(mb_AppConfiguration.GetValue('ADRServer\StagingFolder', AppDir + 'ADR')) + 'ServerTrans\';
end;

procedure TevADRClientProxy.OnDisconnect;
begin
end;

function TevADRClientProxy.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
begin
  S := GetContextSession;

  if Assigned(S) and (S.State = ssActive) then
  begin
    ADatagram.Header.SessionID := S.SessionID;
    ADatagram.Header.User := ctx_DomainInfo.DomainName;
    Result := S.SendRequest(ADatagram);
  end
  else
    Result := nil;  
end;

procedure TevADRClientProxy.SetActive(const AValue: Boolean);
begin
  inherited;
  RequestQueue.ExecSlots := 5;
  FTCPServer.Active := AValue;
end;

procedure TevADRClientProxy.SyncDBList(const ATopPosition: Boolean);
begin
  Assert(False);
end;

procedure TevADRClientProxy.SynchronizeDB(const AOriginatedAt: TUTCDateTime; const ADataBase,
  ATargetDBVersion: String; const ATargetLastTransactionNbr: Integer;
  const ACheckQueue, ATopPosition: Boolean);
begin
  Assert(False);
end;

initialization
  ObjectFactory.Register([TevADRTransRequest]);
  Mainboard.ModuleRegister.RegisterModule(@GetADRClient, IevADRClient, 'ADR Client');

finalization
  SafeObjectFactoryUnRegister([TevADRTransRequest]);

end.