// Module "Evolution ADR Client"
unit mod_ADR_Client;

interface

uses
  EvADRClientMod,
  evADRClientQueue,
  evADRDBChangeListener,
  evADRClientController;

implementation

end.