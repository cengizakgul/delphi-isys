// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvUserSchedulerMod;

interface

uses
  Windows, SysUtils, Classes, isBaseClasses, EvTypes, EvUtils, ISUtils, DB,
  EvCommonInterfaces, EvContext, EvMainboard, isVCLBugFix, EvExceptions, Variants,
  isSchedule, EvDataSet, EvStreamUtils, EvConsts, isErrorUtils,
  isLogFile;

implementation

uses DateUtils;

type
  TevUserScheduler = class(TisSchedule, IevUserScheduler)
  private
    FBaseEvents: IisStringList;
    FContext: IevContext;
    FNeedToRebuildSchedule: Boolean;
    procedure DoRefresh;
    function  FindBaseEvent(const AEventID: String): IisScheduleEvent;
    function  FindWorkEvent(const AEventID: String): IisScheduleEvent;
    procedure AddSnoozedWorkEvent(const AEventID: String; const ASchedTime, ASnoozeUntil: TDateTime);
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure DoOnStart; override;
    function  BeforeCheckingTasks: Boolean; override;
    procedure RunTaskFor(const AEvent: IisScheduleEvent); override;
    procedure DoOnStop; override;

    procedure RefreshTaskList;
    procedure DismissEvent(const AEventID: String);
    procedure SnoozeEvent(const AEventID: String; const ASnoozeUntil: TDateTime);
    function  GetEventsInfo(const ACutOffDate: TDateTime): IisParamsCollection;
  end;

var
  FUserScheduler: TevUserScheduler;

function GetUserScheduler: IevUserScheduler;
begin
  if not Assigned(FUserScheduler) then
    FUserScheduler := TevUserScheduler.Create('User Scheduler');
  Result := FUserScheduler;
end;



{ TevUserScheduler }

procedure TevUserScheduler.AddSnoozedWorkEvent(const AEventID: String; const ASchedTime, ASnoozeUntil: TDateTime);
var
  BaseEvent, WorkEvent: IisScheduleEvent;
begin
  BaseEvent := FindBaseEvent(AEventID);

  WorkEvent := TisScheduleEventOnce.Create;
  WorkEvent.TaskID := BaseEvent.TaskID;
  WorkEvent.RunIfMissed := True;
  WorkEvent.StartTime := TimeOf(ASnoozeUntil);
  WorkEvent.StartDate := DateOf(ASnoozeUntil);
  WorkEvent.Description := BaseEvent.Description;
  WorkEvent.TaskParams.AsStream := BaseEvent.TaskParams.AsStream;
  WorkEvent.TaskParams.AddValue('ScheduledTime', ASchedTime);
  WorkEvent.TaskParams.AddValue('ID', AEventID);

  AddEvent(WorkEvent);
end;

function TevUserScheduler.BeforeCheckingTasks: Boolean;
begin
  Result := inherited BeforeCheckingTasks;

  if FNeedToRebuildSchedule then
  begin
    DoRefresh;
    FNeedToRebuildSchedule := False;
  end;
end;

procedure TevUserScheduler.DismissEvent(const AEventID: String);
var
  BaseEvent, WorkEvent: IisScheduleEvent;
  dNow, NewStartTime, ReminderTime, LastDismiss: TDateTime;
  Q: IevQuery;
begin
  dNow := Now;
  LastDismiss := 0;

  Lock;
  try
    BaseEvent := FindBaseEvent(AEventID);
    if not Assigned(BaseEvent) then
      Exit;

    WorkEvent := FindWorkEvent(AEventID);
    if Assigned(WorkEvent) then
    begin
      LastDismiss := WorkEvent.TaskParams.Value['ScheduledTime'];
      if LastDismiss < dNow then
        LastDismiss := dNow;
      BaseEvent.LastRunDate := LastDismiss;
      DeleteEvent(WorkEvent);
    end;

    dNow := IncMinute(dNow, BaseEvent.TaskParams.Value['Reminder']);
    NewStartTime := BaseEvent.GetClosestStartTimeTo(dNow);

    if NewStartTime <> 0 then
    begin
      ReminderTime := IncMinute(NewStartTime, - BaseEvent.TaskParams.Value['Reminder']);

      WorkEvent := TisScheduleEventOnce.Create;
      WorkEvent.TaskID := BaseEvent.TaskID;
      WorkEvent.RunIfMissed := True;
      WorkEvent.Description := BaseEvent.Description;
      WorkEvent.StartTime := TimeOf(ReminderTime);
      WorkEvent.StartDate := DateOf(ReminderTime);
      WorkEvent.TaskParams.AsStream := BaseEvent.TaskParams.AsStream;
      WorkEvent.TaskParams.AddValue('ScheduledTime', NewStartTime);
      WorkEvent.TaskParams.AddValue('ID', AEventID);

      AddEvent(WorkEvent);
    end;
  finally
    Unlock;
  end;

  if LastDismiss <> 0 then
  begin
    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      Q := TevQuery.Create('UPDATE SB_USER_NOTICE SET LAST_DISMISS = :LAST_DISMISS, NEXT_REMINDER = :NEXT_REMINDER ' +
                           'WHERE SB_USER_NOTICE_NBR = :SB_USER_NOTICE_NBR', False);
      Q.Params.AddValue('LAST_DISMISS', LastDismiss);
      Q.Params.AddValue('NEXT_REMINDER', Null);
      Q.Params.AddValue('SB_USER_NOTICE_NBR', StrToInt(AEventID));
      Q.Execute;
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise
    end
  end;
end;

procedure TevUserScheduler.DoOnConstruction;
begin
  inherited;
  FBaseEvents := TisStringList.CreateAsIndex;
  FContext := Mainboard.ContextManager.CopyContext(Context);
end;

procedure TevUserScheduler.DoOnDestruction;
begin
  FUserScheduler := nil;
  inherited;
end;

procedure TevUserScheduler.DoOnStart;
begin
  inherited;
  Mainboard.ContextManager.SetThreadContext(FContext);
  FNeedToRebuildSchedule := True;
end;

procedure TevUserScheduler.DoOnStop;
begin
  Mainboard.ContextManager.DestroyThreadContext;
  inherited;
end;

procedure TevUserScheduler.DoRefresh;
var
  Q: IevQuery;
  Event: IisScheduleEvent;
  S: IisStream;
begin
  // Base scheduler events are stored in separate list
  // All working events are "Run Once" and Dismiss/Snooze logic controls their maintenance in scheduler

  Q := TevQuery.Create('SELECT * FROM SB_USER_NOTICE WHERE SB_USER_NBR = :SB_USER_NBR');
  Q.Params.AddValue('SB_USER_NBR', Context.UserAccount.InternalNbr);
  Q.Execute;

  Lock;
  try
    Clear;
    FBaseEvents.Clear;
    S := TisStream.Create;
    while not Q.Result.Eof do
    begin
      try
        S.Clear;
        (Q.Result.FieldByName('Task') as TBlobField).SaveToStream(S.RealStream);
        S.Position := 0;

        Event := ObjectFactory.CreateInstanceFromStream(S) as IisScheduleEvent;
        if Event.Enabled then
        begin
          Event.Description := Q.Result.FieldByName('NAME').AsString;
          Event.RunIfMissed := True;
          Event.TaskParams.AddValue('Notes', Q.Result.FieldByName('NOTES').AsString);
          FBaseEvents.AddObject(Q.Result.FieldByName('SB_USER_NOTICE_NBR').AsString, Event);

          if Q.Result.FieldByName('NEXT_REMINDER').IsNull then
          begin
            Event.LastRunDate := Q.Result.FieldByName('LAST_DISMISS').AsDateTime;
            DismissEvent(Q.Result.FieldByName('SB_USER_NOTICE_NBR').AsString);
          end
          else
            AddSnoozedWorkEvent(Q.Result.FieldByName('SB_USER_NOTICE_NBR').AsString,
                                Q.Result.FieldByName('LAST_DISMISS').AsDateTime,
                                Q.Result.FieldByName('NEXT_REMINDER').AsDateTime);
        end;
      except
        on E: Exception do
          mb_LogFile.AddEvent(etError, 'User Scheduler', 'Cannot read event',
            'SB_USER_NOTICE_NBR = ' + Q.Result.FieldByName('SB_USER_NOTICE_NBR').AsString + #13 +
             E.Message +  #13 + GetStack(E));
      end;

      Q.Result.Next;
    end;

  finally
    Unlock;
  end;

  Context.Callbacks.UserSchedulerEvent('', '', 0);
end;

function TevUserScheduler.FindBaseEvent(const AEventID: String): IisScheduleEvent;
var
  i: Integer;
begin
  i := FBaseEvents.IndexOf(AEventID);
  if i <> -1 then
    Result := FBaseEvents.Objects[i] as IisScheduleEvent
  else
    Result := nil;
end;

function TevUserScheduler.FindWorkEvent(const AEventID: String): IisScheduleEvent;
var
  i: Integer;
  Event: IisScheduleEvent;
begin
  Result := nil;
  
  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      Event := GetEventByIndex(i);
      if AnsiSameStr(Event.TaskParams.Value['ID'], AEventID) then
      begin
        Result := Event;
        break;
      end;
    end;
  finally
    Unlock;
  end;
end;

function TevUserScheduler.GetEventsInfo(const ACutOffDate: TDateTime): IisParamsCollection;
var
  i: Integer;
  BaseEvent, WorkEvent: IisScheduleEvent;
  Item: IisListOfValues;
  sID: String;
begin
  Result := TisParamsCollection.Create;

  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      WorkEvent := GetEventByIndex(i);
      if WorkEvent.StartDate + WorkEvent.StartTime <= ACutOffDate then
      begin
        sID := WorkEvent.TaskParams.Value['ID'];
        BaseEvent := FindBaseEvent(sID);
        if Assigned(BaseEvent) then
        begin
          Item := Result.AddParams(IntToStr(Result.Count + 1));
          Item.AddValue('Event', (BaseEvent as IisInterfacedObject).GetClone);
          Item.AddValue('ID', sID);
          Item.AddValue('ScheduledTime', WorkEvent.TaskParams.Value['ScheduledTime']);
        end;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TevUserScheduler.RefreshTaskList;
var
  bActive: Boolean;
begin
  bActive := GetActive;
  SetActive(False);
  FNeedToRebuildSchedule := True;
  SetActive(bActive);
end;

procedure TevUserScheduler.RunTaskFor(const AEvent: IisScheduleEvent);
begin
  inherited;
  AEvent.LastRunDate := Now;
  Context.Callbacks.UserSchedulerEvent(AEvent.TaskParams.Value['ID'],
                                       AEvent.Description,
                                       AEvent.TaskParams.Value['ScheduledTime']);
end;

procedure TevUserScheduler.SnoozeEvent(const AEventID: String; const ASnoozeUntil: TDateTime);
var
  WorkEvent: IisScheduleEvent;
  Q: IevQuery;
begin
  Lock;
  try
    WorkEvent := FindWorkEvent(AEventID);
    if not Assigned(WorkEvent) then
      Exit;

    WorkEvent.StartTime := TimeOf(ASnoozeUntil);
    WorkEvent.StartDate := DateOf(ASnoozeUntil);
  finally
    Unlock;
  end;

  ctx_DataAccess.StartNestedTransaction([dbtBureau]);
  try
    Q := TevQuery.Create('UPDATE SB_USER_NOTICE SET LAST_DISMISS = :LAST_DISMISS, NEXT_REMINDER = :NEXT_REMINDER ' +
                         'WHERE SB_USER_NOTICE_NBR = :SB_USER_NOTICE_NBR', False);
    Q.Params.AddValue('LAST_DISMISS', WorkEvent.TaskParams.Value['ScheduledTime']);
    Q.Params.AddValue('NEXT_REMINDER', ASnoozeUntil);
    Q.Params.AddValue('SB_USER_NOTICE_NBR', StrToInt(AEventID));
    Q.Execute;
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise
  end
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetUserScheduler, IevUserScheduler, 'User Scheduler');

end.
