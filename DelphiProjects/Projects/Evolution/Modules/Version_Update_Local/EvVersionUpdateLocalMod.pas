unit EvVersionUpdateLocalMod;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses Classes, Windows, Controls, SysUtils, isBaseClasses, EvCommonInterfaces, EvMainboard, isBasicUtils,
     EvStreamUtils, evRemoteMethods, EvTransportInterfaces, EvContext, isLogFile,
     SEncryptionRoutines, Dialogs, ISZippingRoutines, isSocket, EvConsts, EvInitApp, is7Zip, isHttp,
     EvUtils, evExceptions, EvUIUtils;

implementation

type
  TevVersionUpdateLocal = class(TisInterfacedObject, IevVersionUpdateLocal)
  private
    FUpdateTmpFolder: String;
    FAppFilesInfo: IisParamsCollection;
    FUpdateInfo: IisParamsCollection;
    FPatchingUtil: String;
    FPatchingUtilParams: String;
    FArchiveFile: String;
    function  GetAppFilesInfo: IisParamsCollection;
    procedure Cleanup;
    procedure FileOperation(const AFileName: String; const FileInfo: IisListOfValues; const TargetFolder: String);
    procedure MergeLogFiles(const UpdAppLogFileName: String);
    function  DownloadUpdatePackFromServer(const AUpdatePackFileName: String): IisParamsCollection;
    function  DownloadUpdatePackFromAltSource(const ALocation: String): IisParamsCollection;
    function  DownloadFile(const AFileLocation: String): IevDualStream;
  protected
    procedure DoOnConstruction; override;
    function  CheckIfVersionValid(out ARequiredVersion: String): Boolean;
    procedure PrepareUpdate(out aUpdatePackFileName: String);
    procedure ApplyUpdate(const aUpdatePackFileName, aAppFile: String);
    function  PrepareUpdateIfNeeds(const AskConfirmation: Boolean): Boolean;
    function  PerformUpdateIfNeeds: Boolean;
  end;


function GetVersionUpdateLocal: IevVersionUpdateLocal;
begin
  Result := TevVersionUpdateLocal.Create;
end;


{ TevVersionUpdateLocal }

function TevVersionUpdateLocal.CheckIfVersionValid(out ARequiredVersion: String): Boolean;
var
  AppFileInfo: IisParamsCollection;
  VersionDescription: IisListOfValues;
begin
  ctx_StartWait('Checking for new version');
  ARequiredVersion := '';
  try
    if Mainboard.VersionUpdateRemote <> nil then
    begin
      AppFileInfo := GetAppFilesInfo;
      Result := Mainboard.VersionUpdateRemote.Check(AppFileInfo, FUpdateInfo);
      if Result then
        FUpdateInfo := nil
      else
      begin
        FAppFilesInfo := AppFileInfo;
        VersionDescription := FUpdateInfo.ParamsByName('Description');
        ARequiredVersion := VersionDescription.Value['Version'];
        mb_LogFile.AddEvent(etInformation, 'Version Update', 'Version mismatch',
          'Current version: ' + AppVersion +
          #13'Required version: ' + VersionDescription.Value['Version'] +
          #13'Estimated size of update pack: ' + SizeToString(VersionDescription.Value['EstimatedSize']));
      end;
    end
    else
    begin
      FUpdateInfo := nil;
      Cleanup;
      Result := True;  // version is valid
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TevVersionUpdateLocal.Cleanup;
begin
  try
    ClearDir(FUpdateTmpFolder, True);
  except
    // The potential issue is when update executable is still running. It's quite possible scenario.
    // Need to suppress all exceptions in here. Anyways it is just a clean up process.
  end;  
end;

function TevVersionUpdateLocal.GetAppFilesInfo: IisParamsCollection;
var
  Item, Files: IisListOfValues;
  sVer, sExt: String;
  ExecutableExt: IisStringList;
  L: IisStringList;
  i: Integer;
begin
  ExecutableExt := TisStringList.CreateAsIndex;
  ExecutableExt.Add('.EXE');
  ExecutableExt.Add('.DLL');
  ExecutableExt.Add('.BPL');

  Result := TisParamsCollection.Create;
  Item := Result.AddParams('Description');
  Item.AddValue('AppID', AppID);
  Item.AddValue('Version', AppVersion);

  Files := Result.AddParams('Files');

  L := GetFilesByMask(AppDir + '*.*');
  for i := 0 to L.Count - 1 do
  begin
    sExt := ExtractFileExt(L[i]);
    if ExecutableExt.IndexOf(sExt) <> -1 then
      sVer := GetFileVersion(L[i])
    else
      sVer := '';

    Item := TisListOfValues.Create;
    Files.AddValue(ExtractFileName(L[i]), Item);
    Item.AddValue('Version', sVer);
  end;
end;

procedure TevVersionUpdateLocal.ApplyUpdate(const aUpdatePackFileName, aAppFile: String);
var
  F: IevDualStream;
  L: IisStringList;
  sCurrVersion, sNewVersion, UpdAppFolder, BkpFolder: String;
  i: Integer;
  sFile, sBackupArchive, sNewVer, s: String;
  iOldMajorVer: integer;
  iNewMajorVer: integer;
  LV, Files, FileInfo: IisListOfValues;

  procedure TerminateAllExes;
  var
    ExeList, NotTerminated: IisStringList;
  begin
    ExeList := GetFilesByMask(UpdAppFolder + '*.exe');
    if not TerminateProcesses(ExeList, NotTerminated) then
      GlobalLogFile.AddEvent(etInformation, 'Version Update',
         'Cannot terminate processes', 'Not terminated processes:'#13 + NotTerminated.CommaText);
  end;

begin
  ctx_StartWait('Preparing for updating...');
  try
    sCurrVersion := GetFileVersion(aAppFile);
    UpdAppFolder := ExtractFilePath(aAppFile);
    BkpFolder := UpdAppFolder + 'Backup\';

    // terminate all running exes
    TerminateAllExes;

    // read update pack
    CheckCondition(FileExists(aUpdatePackFileName), 'Cannot open update pack ' + aUpdatePackFileName);
    F := TevDualStreamHolder.CreateFromFile(aUpdatePackFileName);
    FUpdateInfo := TisParamsCollection.Create;
    FUpdateInfo.ReadFromStream(F);
    sNewVersion := FUpdateInfo.ParamsByName('Description').Value['Version'];

    s := sNewVersion;
    iNewMajorVer := StrToIntDef(GetNextStrValue(s, '.'), 1000);

    s := sCurrVersion;
    iOldMajorVer := StrToIntDef(GetNextStrValue(s, '.'), 0);

    // back up previous version if major nbr is lower than new version
    if iNewMajorVer > iOldMajorVer then
    begin
      ctx_UpdateWait('Creating a backup archive of existing version ' + sCurrVersion + ' Please wait...');
      sBackupArchive := UpdAppFolder +'PrevVersion\' + ChangeFileExt(ExtractFileName(aAppFile), '') +
        '_' + sCurrVersion + '.zip';
      ForceDirectories(ExtractFileDir(sBackupArchive));
      if FileExists(sBackupArchive) then
      begin
        SetFileAttributes(PAnsiChar(sBackupArchive), FILE_ATTRIBUTE_NORMAL);
        DeleteFile(sBackupArchive);
      end;
      AddToFile(sBackupArchive, UpdAppFolder, '*.*');
      SetFileAttributes(PAnsiChar(sBackupArchive), FILE_ATTRIBUTE_ARCHIVE or FILE_ATTRIBUTE_READONLY);
      mb_LogFile.AddEvent(etInformation, 'Version Update', 'Existing version is backed up', 'Backup Archive: ' + sBackupArchive);

    end;

    // Start update process
    F := nil;
    Files := FUpdateInfo.ParamsByName('Files');

    mb_LogFile.AddEvent(etInformation, 'Version Update', 'Apply update pack',
        'Current version: ' + sCurrVersion + #13'Update version: ' + sNewVersion);

    // extract 7-Zip32.dll
    if not FileExists(AppDir + '7-Zip32.dll') and Files.ValueExists('7-Zip32.dll') then
    begin
      FileInfo := IInterface(Files.Value['7-Zip32.dll']) as IisListOfValues;
      F := IInterface(FileInfo.Value['Data']) as IevDualStream;
      F.SaveToFile(AppDir + '7-Zip32.dll');
    end;

    // extract patching utility
    LV := FUpdateInfo.ParamsByName('PatchUtil');
    if Assigned(LV) then
    begin
      sFile := AppDir + LV.Value['Name'];
      F := IInterface(LV.Value['File']) as IevDualStream;
      F.SaveToFile(sFile);
      F := nil;
      FPatchingUtil := sFile;
      FPatchingUtilParams := LV.Value['Params'];
      LV := nil;
    end;

    // extract archive
    LV := FUpdateInfo.ParamsByName('Archive');
    if Assigned(LV) then
    begin
      sFile := AppDir + LV.Value['Name'];
      F := IInterface(LV.Value['File']) as IevDualStream;
      F.SaveToFile(sFile);
      F := nil;
      FArchiveFile := sFile;
      LV := nil;
    end;

    // Update
    ClearDir(BkpFolder);
    ForceDirectories(BkpFolder);
    try
      sNewVer := FUpdateInfo.ParamsByName('Description').Value['Version'];
      ctx_UpdateWait('Updating to version ' + sNewVer + ' Please wait...', 0, Files.Count);
      for i := 0 to Files.Count - 1 do
      begin
        ctx_UpdateWait('Updating to version ' + sNewVer + ' Please wait...'#13#13'Processing File: ' +
                        Files[i].Name, i + 1);
        FileInfo := IInterface(Files[i].Value) as IisListOfValues;
        sFile := UpdAppFolder + Files[i].Name;
        if FileExists(sFile) then
          CopyFile(sFile, BkpFolder + ExtractFileName(sFile), False);  // backup copy

        FileOperation(Files[i].Name, FileInfo, UpdAppFolder);
      end;
      ctx_UpdateWait('Updating to version ' + sNewVer + ' Please wait...', Files.Count, Files.Count);

      mb_LogFile.AddEvent(etInformation, 'Version Update', 'Update completed', 'Current version: ' + sNewVersion);

      ctx_UpdateWait('Restarting...');
    except
      // rollback
      on E: Exception do
      begin
        ctx_UpdateWait(E.Message + #13 + 'Rolling back...');
        L := GetFilesByMask(BkpFolder + '*.*');
        for i := 0 to L.Count - 1 do
          try
            DeleteFile(UpdAppFolder + ExtractFileName(L[i]));
            MoveFile(L[i], UpdAppFolder + ExtractFileName(L[i]));
          except
            // in case if file is locked
          end;
        mb_LogFile.AddEvent(etError, 'Version Update', 'Update rolled back', E.Message);
        MessageDlg('Version update has failed. All changes have been rolled back.', mtError, [mbOK], 0);
        raise;
      end;
    end;

  finally
    try
      MergeLogFiles(ChangeFileExt(aAppFile, '.log')); // Exceptions are not expected
    except
    end;
    ctx_EndWait;
  end;

  if sCurrVersion <> sNewVersion then
    s := 'Autoupdate process has finished successfully!'#13'Current Version: ' +  sNewVersion
  else
    s := 'Autoupdate process has repaired version ' +  sNewVersion;

  MessageDlg(s, mtInformation, [mbOK], 0)
end;

procedure TevVersionUpdateLocal.FileOperation(const AFileName: String; const FileInfo: IisListOfValues;
  const TargetFolder: String);
var
  F, S: IevDualStream;
  sFile, sDiffFile, sOper: String;
  PatchInfo: IisListOfValues;
  i: Integer;

  procedure PatchFile;
  var
    Par: String;
    sNewFile: String;
  begin
    sNewFile := sFile + '.upd';
    Par := StringReplace(FPatchingUtilParams, '%source%', '"' + sFile + '"', [rfReplaceAll]);
    Par := StringReplace(Par, '%delta%', '"' + sDiffFile + '"', [rfReplaceAll]);
    Par := StringReplace(Par, '%target%', '"' + sNewFile + '"', [rfReplaceAll]);
    RunProcess(FPatchingUtil, Par , [roHideWindow, roWaitForClose]);
    DeleteFile(sFile);
    RenameFile(sNewFile, sFile);
  end;

  procedure ExtractFileFromArchive;
  begin
    if AnsiSameText(ExtractFileExt(FArchiveFile), '.7z') then
      ExtractFiles7z(FArchiveFile, ExtractFileName(sFile), TargetFolder)
    else
      raise EevException.CreateFmt('Archive type %s is not supported', [ExtractFileExt(FArchiveFile)]);
  end;

begin
  sFile := TargetFolder + AFileName;
  sOper := FileInfo.Value['Operation'];

  if AnsiSameText(sOper, 'copy') then
  begin
    DeleteFile(sFile);
    F := IInterface(FileInfo.Value['Data']) as IevDualStream;
    if FileInfo.TryGetValue('Compression', '') = '7z' then
      F := DecompressStream7z(F)
    else if FileInfo.TryGetValue('Compression', '') = 'zip' then
    begin
      S := TevDualStreamHolder.Create;
      InflateStream(F.RealStream, S.RealStream);
      F := S;
      S := nil;
    end;

    F.SaveToFile(sFile);
  end

  else if AnsiSameText(sOper, 'delete') then
    DeleteFile(sFile)

  else if AnsiSameText(sOper, 'patch') then
  begin
    PatchInfo := IInterface(FileInfo.Value['Diffs']) as IisListOfValues;
    for i := 0 to PatchInfo.Count - 1 do
    begin
      sDiffFile := TargetFolder + 'diff.dat';
      DeleteFile(sDiffFile);
      try
        F := IInterface(PatchInfo[i].Value) as IevDualStream;
        F.SaveToFile(sDiffFile);
        PatchFile;
      finally
        DeleteFile(sDiffFile);
      end;
    end;
  end

  else if AnsiSameText(sOper, 'run') then
  begin
    mb_LogFile.AddEvent(etInformation, 'Version Update', 'Run ' + sFile, '');
    try
      DeleteFile(sFile);
      F := IInterface(FileInfo.Value['Data']) as IevDualStream;
      F.SaveToFile(sFile);
      F := nil;
      RunProcess(sFile, '', [roHideWindow, roWaitForClose]);
    finally
      DeleteFile(sFile);
    end;
  end

  else if AnsiSameText(sOper, 'extract') then
  begin
    DeleteFile(sFile);
    ExtractFileFromArchive;
  end

  else
    Exit;

  mb_LogFile.AddEvent(etInformation, 'Version Update', AFileName + ' has been processed', 'Operation: ' + sOper);
end;

procedure TevVersionUpdateLocal.PrepareUpdate(out aUpdatePackFileName: String);
var
  F: IevDualStream;
  sUpdVer, sAltSource: String;
  UpdatePack: IisParamsCollection;
begin
  sUpdVer := FUpdateInfo.ParamsByName('Description').Value['Version'];

  // looking at alternative source
  if isStandalone or (Mainboard.TCPClient.Port <> IntToStr(TCPClient_Port)) then // it's SA or not local network
    sAltSource := FUpdateInfo.ParamsByName('Description').TryGetValue('AlternativeSource', '')
  else
    sAltSource := '';

  aUpdatePackFileName := FUpdateTmpFolder + sUpdVer + '_' + AppID + '.eup';

  UpdatePack := nil;
  if sAltSource <> '' then
    try
      UpdatePack := DownloadUpdatePackFromAltSource(sAltSource)  // first try to download from alternative source
    except
      // eat all exceptions
      UpdatePack := nil;
    end;

  if not Assigned(UpdatePack) then
    UpdatePack := DownloadUpdatePackFromServer(aUpdatePackFileName);

  ctx_StartWait;
  try
    if UpdatePack.ParamsByName('Files') = nil then
    begin
      mb_LogFile.AddEvent(etInformation, 'Version Update', 'Update pack downloaded',
        'No actual download needed. Update pack ' + aUpdatePackFileName + ' is ready to apply.');
      F := TEvDualStreamHolder.CreateFromFile(aUpdatePackFileName);
      Cleanup  // this won't delete update pack since it's locked
    end
    else
    begin
      // prepare update folder
      Cleanup;
      ForceDirectories(FUpdateTmpFolder);

      // write update pack to file
      F := TEvDualStreamHolder.CreateFromNewFile(aUpdatePackFileName);
      UpdatePack.WriteToStream(F);

      mb_LogFile.AddEvent(etInformation, 'Version Update', 'Update pack downloaded',
        'Version: ' + sUpdVer + #13'Size: ' + SizeToString(F.Size));
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TevVersionUpdateLocal.MergeLogFiles(const UpdAppLogFileName: String);
var
  i: Integer;
  UpdAppLogFile: ILogFile;
  Event: ILogEvent;
begin
  UpdAppLogFile := TLogFile.Create(UpdAppLogFileName);
  mb_LogFile.Lock;
  try
    for i:= 0 to mb_LogFile.Count - 1 do
    begin
      Event := mb_LogFile[i];
      UpdAppLogFile.AddEvent(Event.EventType, Event.EventClass, Event.Text, Event.Details);
    end;
  finally
    mb_LogFile.UnLock;
  end;
end;

function TevVersionUpdateLocal.PerformUpdateIfNeeds: Boolean;
var
  sParams: String;
begin
  if AppSwitches.ValueExists('UpdatePack') and AppSwitches.ValueExists('AppFile') then
  begin
    ApplyUpdate(AppSwitches.Value['UpdatePack'], AppSwitches.Value['AppFile']);

    sParams := '';
    if AppSwitches.ValueExists('Host') then
      sParams := sParams + ' /Host ' + AppSwitches.Value['Host'];
    if AppSwitches.ValueExists('Port') then
      sParams := sParams + ' /Port ' + AppSwitches.Value['Port'];

    RunIsolatedProcess(AppSwitches.Value['AppFile'], sParams, []); // restart app

    Result := True;
  end
  else
    Result := False;
end;

function TevVersionUpdateLocal.PrepareUpdateIfNeeds(const AskConfirmation: Boolean): Boolean;
var
  sUpdatePack, sEvoUpdate, sAppFile: String;
  ValidVer: Boolean;
  sRequiredVersion: String;

  function CopyRequiredFile(const AFileName: String): String;
  var
    sFile: String;
  begin
    sFile := ExtractFilePath(AppFileName) + AFileName;
    if FileExists(sFile) then
    begin
      Result := FUpdateTmpFolder + AFileName;
      CopyFile(sFile, Result, False);
    end
    else
      Result := '';
  end;

  function RunExternalUpdUtil: Boolean;
  var
    TCPClient: IisAsyncTCPClient;
    Sck: IisTCPSocket;
    sUpdateUtil, sHost: String;
    Hosts: IisStringList;
    i: Integer;
  begin
    Result := False;
    try
      sUpdateUtil := AppDir + 'EvoUpdate.exe';
      if FileExists(sUpdateUtil) and not AnsiSameText(ExtractFileName(AppFileName), 'EvoUpdate.exe') then
      begin
        // make sure there is alive server out there
        TCPClient := Mainboard.TCPClient.DatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPClient;
        TCPClient.UnRegisterSSLHost(Mainboard.TCPClient.Host, StrToInt(Mainboard.TCPClient.Port));

        Hosts := TisStringList.Create;
        Hosts.CommaText := Mainboard.TCPClient.Host;
        sHost := '';
        for i := 0 to Hosts.Count - 1 do
          try
            sHost := Hosts[i];
            Sck := TCPClient.CreateConnectionTo(sHost, StrToInt(Mainboard.TCPClient.Port));
            TCPClient.Disconnect(Sck);
            break;
          except
            sHost := '';
          end;

        if sHost <> '' then
        begin
          // start update process
          RunIsolatedProcess(sUpdateUtil,
            Format('/HOST %s /PORT %s /APPFILE "%s"',
                   [Mainboard.TCPClient.Host, Mainboard.TCPClient.Port, AppFileName]), []);

          Result := True;
        end;
      end;
    except
    end;
  end;

begin
  try
    ValidVer := CheckIfVersionValid(sRequiredVersion);
  except
    on E: Exception   do
      if (E.Message = 'Client request was not successfully executed') or
         (Pos('Cannot establish SSL connection', E.Message) <> 0) then
      begin
        if RunExternalUpdUtil then
        begin
          Result := True;
          Exit;
        end
        else
          raise;
      end
      else
        raise;
  end;

  if not ValidVer then
  begin
    if AskConfirmation and
       (EvMessage('A new version of Evolution is available. Do you want to download and update it now?',
        mtConfirmation, [mbYes, mbNo], mbNo) <> mrYes) then
    begin
      Result := False;
      Exit;
    end;

    PrepareUpdate(sUpdatePack);

    ctx_StartWait('Prepare for update...');
    try
      // copy required files to another location
      CopyRequiredFile('ssleay32.dll');
      CopyRequiredFile('libeay32.dll');
      CopyRequiredFile('msvcr90.dll');
      CopyRequiredFile('Microsoft.VC90.CRT.manifest');
      CopyRequiredFile('7-zip32.dll');
      CopyRequiredFile('cert.pem');
      sEvoUpdate := CopyRequiredFile(ExtractFileName(AppFileName));

      // run update process
      sAppFile := AppSwitches.TryGetValue('APPFILE', AppFileName);
      RunIsolatedProcess(sEvoUpdate,
        Format('/HOST %s /PORT %s /UpdatePack "%s" /AppFile "%s"',
        [Mainboard.TCPClient.Host, Mainboard.TCPClient.Port, sUpdatePack, sAppFile]), []);
    finally
      ctx_EndWait;
    end;

    Result := True;
  end
  else
    Result := False;
end;

procedure TevVersionUpdateLocal.DoOnConstruction;
begin
  inherited;
  FUpdateTmpFolder := AppTempFolder + 'Update\';
end;

function TevVersionUpdateLocal.DownloadUpdatePackFromServer(const AUpdatePackFileName: String): IisParamsCollection;
var
  F: IevDualStream;
begin
  ctx_StartWait('Downloading version ' + FUpdateInfo.ParamsByName('Description').Value['Version'] +
                ' from server');
  try
    if not Assigned(FAppFilesInfo) then
      FAppFilesInfo := GetAppFilesInfo;

    // if file exists and the same then don't download
    if FileExists(AUpdatePackFileName) then
    begin
      F := TEvDualStreamHolder.CreateFromFile(AUpdatePackFileName);
      FAppFilesInfo.AddParams('ExistingPack').AddValue('Hash', CalcMD5(F.RealStream));
    end;

    Result := Mainboard.VersionUpdateRemote.GetUpdatePack(FAppFilesInfo); // download update pack
  finally
    ctx_EndWait;
  end;
end;

function TevVersionUpdateLocal.DownloadUpdatePackFromAltSource(const ALocation: String): IisParamsCollection;
var
  Files, Item, FileInfo, Diffs: IisListOfValues;
  i, j: Integer;
  sFilePath: String;
  F: IevDualStream;

  function GetFile(const AFileInfo: IisListOfValues; URL: String): IevDualStream;
  begin
    if AFileInfo.TryGetValue('Compression', '') <> '' then
      URL := URL + '.' + AFileInfo.Value['Compression'];
    Result := DownloadFile(URL);
  end;

begin
  ctx_StartWait('Downloading version ' + FUpdateInfo.ParamsByName('Description').Value['Version'] +
                ' from ' + ALocation);
  try
    Result := FUpdateInfo.GetClone;

    // download files if it needs
    Files := Result.ParamsByName('Files');
    for i := 0 to Files.Count - 1 do
    begin
      FileInfo := IInterface(Files[i].Value) as IisListOfValues;
      if AnsiSameText(FileInfo.Value['Operation'], 'copy') then
      begin
        sFilePath := ALocation + Files[i].Name;
        F := GetFile(FileInfo, sFilePath);
        FileInfo.AddValue('Data', F);
      end

      else if AnsiSameText(FileInfo.Value['Operation'], 'patch') then
      begin
        Diffs := IInterface(FileInfo.Value['Diffs']) as IisListOfValues;
        for j := 0 to Diffs.Count - 1 do
        begin
          sFilePath := NormalizePath(ALocation + Diffs[j].Name) + Files[i].Name;
          Diffs[j].Value := GetFile(FileInfo, sFilePath);
        end;
      end;
    end;

    // download patch utility if it needs
    Item := Result.ParamsByName('PatchUtil');
    if Assigned(Item) then
    begin
      F := DownloadFile(ALocation + Item.Value['Name']);
      Item.AddValue('File', F);
    end;

    // download archive if it needs
    Item := Result.ParamsByName('Archive');
    if Assigned(Item) then
    begin
      F := DownloadFile(ALocation + Item.Value['Name']);
      Item.AddValue('File', F);
    end;

    // prepare 7-Zip32.dll if it needs
    if not FileExists(AppDir + '7-Zip32.dll') and not Files.ValueExists('7-Zip32.dll') then
    begin
      F := DownloadFile(ALocation + '7-Zip32.dll');
      F.SaveToFile(AppDir + '7-Zip32.dll');
    end;

  finally
    ctx_EndWait;
  end;
end;

function TevVersionUpdateLocal.DownloadFile(const AFileLocation: String): IevDualStream;
begin
  if StartsWith(AFileLocation, 'http://') then
    Result := TisHttpClient.Get(AFileLocation)
  else
    Result := TevDualStreamHolder.CreateFromFile(AFileLocation);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVersionUpdateLocal, IevVersionUpdateLocal, 'Version Update Local');

end.



