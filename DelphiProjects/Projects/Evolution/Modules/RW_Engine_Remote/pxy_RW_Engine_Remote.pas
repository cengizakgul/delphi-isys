// Modules "RW Engine Remote"
unit pxy_RW_Engine_Remote;

interface

uses
  isBaseClasses, EvCommonInterfaces, EvReportWriterProxy, SReportSettings,
  EvStreamUtils, EvMainBoard, EvContext, SysUtils, EvTransportDatagrams,
  Dialogs, evRemoteMethods, evProxy, EvTransportInterfaces;

implementation

type
  TevRWRemoteEngineProxy = class(TevProxyModule, IevRWRemoteEngine)
  protected
    function  RunReports(const AReportList: TrwReportList): IEvDualStream;
    function  RunReports2(const AReportList: TrwReportList): TrwReportResults;
    function  RunQBQuery(const AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer): IEvDualStream;
    procedure StopReportExecution;
    procedure SetLowerRWEnginePriority(const AValue: Boolean);
  end;


function GetRWRemoteEngine: IevRWRemoteEngine;
begin
  Result := TevRWRemoteEngineProxy.Create;
end;

{ TevRWRemoteEngineProxy }

function TevRWRemoteEngineProxy.RunQBQuery(const AQuery: IEvDualStream;
 AClientNbr, ACompanyNbr: Integer): IEvDualStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWREMOTEENGINE_RUNQBQUERY);
  D.Params.AddValue('AQuery', AQuery);
  D.Params.AddValue('AClientNbr', AClientNbr);
  D.Params.AddValue('ACompanyNbr', ACompanyNbr);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IEvDualStream;
end;

function TevRWRemoteEngineProxy.RunReports(
  const AReportList: TrwReportList): IEvDualStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_RWREMOTEENGINE_RUNREPORTS);
  D.Params.AddValue('AReportList', AReportList.GetAsStream);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value['Result']) as IEvDualStream;
end;

function TevRWRemoteEngineProxy.RunReports2(
  const AReportList: TrwReportList): TrwReportResults;
var
  Res: IevDualStream;
begin
  Res := RunReports(AReportList);
  Res.Position := 0;
  Result := TrwReportResults.Create;
  Result.SetFromStream(Res);
end;

procedure TevRWRemoteEngineProxy.SetLowerRWEnginePriority(const AValue: Boolean);
begin
  NotRemotableMethod;
end;

procedure TevRWRemoteEngineProxy.StopReportExecution;
begin
  NotRemotableMethod;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRWRemoteEngine, IevRWRemoteEngine, 'RW Remote Module');

end.
