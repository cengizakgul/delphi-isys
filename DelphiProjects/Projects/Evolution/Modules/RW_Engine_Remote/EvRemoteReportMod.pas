// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvRemoteReportMod;

interface

uses  Windows, MMSystem, Classes, SReportSettings, isBaseClasses, Variants,
      EvStreamUtils, ISBasicUtils, EvBasicUtils, EvContext, EvMainBoard,
       evUtils, SysUtils, EvConsts, extctrls, ISUtils, EvCommonInterfaces,
      EvReportWriterProxy, rwEvUtils, ISErrorUtils, EvDataAccessComponents, DB,
      isThreadManager, EvTypes, isExceptions, EvExceptions, EvTransportShared;

implementation

type
  TevRemoteReport = class(TisInterfacedObject, IevRWRemoteEngine)
  private
    FrwProxy: IevReportWriterProxy;
    function  rwProxy: IevReportWriterProxy;
    function  ReplaceFileNameMacro(const AFileName: String): String;
  protected
    procedure DoOnConstruction; override;
    function  RunReports(const AReportList: TrwReportList): IEvDualStream;
    function  RunReports2(const AReportList: TrwReportList): TrwReportResults;
    function  RunQBQuery(const AQuery: IEvDualStream; AClientNbr: Integer; ACompanyNbr: Integer): IEvDualStream;
    procedure StopReportExecution;
    procedure SetLowerRWEnginePriority(const AValue: Boolean);
  end;


function GetRWRemoteEngine: IevRWRemoteEngine;
begin
  Result := TevRemoteReport.Create;
end;


{ TevRemoteReport }

function TevRemoteReport.rwProxy: IevReportWriterProxy;
begin
  Result := FrwProxy;
end;

function  TevRemoteReport.ReplaceFileNameMacro(const AFileName: String): String;
begin
  Result := AFileName;
  Result := StringReplace(Result,'%DATE%',FormatDateTime('yyyymmdd', Now),[rfReplaceAll]);
  Result := StringReplace(Result,'%TIME%',FormatDateTime('hhmmss', Now),[rfReplaceAll]);
end;

function TevRemoteReport.RunQBQuery(const AQuery: IEvDualStream; AClientNbr, ACompanyNbr: Integer): IEvDualStream;
var
  Q: IEvDualStream;
  EvoParams: TEvBasicParams;
  Prms: TParams;
  Par: TParam;
  ParVal: Variant;
  ParName: String;
  ResDS: TEvBasicClientDataSet;
  sExceptions: String;
begin
  sExceptions := '';
  try
    AQuery.Position := 0;
    Q := TEvDualStreamHolder.Create;
    AQuery.ReadStream(Q);

    Prms := TParams.Create;
    try
      EvoParams := TEvBasicParams.Create;
      try
        AQuery.ReadStream(EvoParams.Stream);
        while not EvoParams.EOS do
        begin
          ParVal := EvoParams.NextItem(ParName);
          Par := TParam(Prms.Add);
          Par.Name := ParName;
          Par.Value := ParVal;
        end;
      finally
        FreeAndNil(EvoParams);
      end;

      ResDS := TEvBasicClientDataSet.Create(nil);
      try
        rwProxy.StartSession;
        try
          Q.Position := 0;
          rwProxy.RunQueryBuilderQuery(Q,  AClientNbr, ACompanyNbr, Prms, ResDS);
        finally
          rwProxy.EndSession;
        end;

        Result := TEvDualStreamHolder.Create;
        ResDS.SaveToStream(Result.RealStream);
        Result.Position := 0;
      finally
        FreeAndNil(ResDS);
      end;
    finally
      FreeAndNil(Prms);
    end;
  except
    on E: Exception do
      sExceptions := MakeFullError(E);
  end;

  sExceptions := StringReplace(sExceptions, #13, MSG_RETURN, [rfReplaceAll]);
  sExceptions := StringReplace(sExceptions, #10, MSG_RETURN, [rfReplaceAll]);

  if sExceptions <> '' then
    raise EevException.CreateDetailed('QB query finished with error', sExceptions);
end;

function TevRemoteReport.RunReports(const AReportList: TrwReportList): IEvDualStream;
var
  Res: TrwReportResults;
begin
  Res := RunReports2(AReportList);
  try
    Result := Res.GetAsStream;
  finally
    Res.Free;
  end;
end;

function TevRemoteReport.RunReports2(const AReportList: TrwReportList): TrwReportResults;
var
  i, j: Integer;
  rt: TReportType;
  mt: string;
  rn, rc, ac, h: string;
  RepData: IEvDualStream;
  RunInfo: TevRWRunReportSettings;
  RepRes: TrwReportResult;
  RepResult: TevRWResultInfo;
  Par: TrwReportParam;

  procedure ClearRepResult;
  begin
    RepResult.Data := nil;
    RepResult.ResultType := rwRTUnknown;
    RepResult.ReturnValue := Null;
    RepResult.DefaultFileName := '';
    SetLength(RepResult.PageInfo, 0);
  end;

begin
  Result := nil;
  ctx_DataAccess.AsOfDate := 0;

  rwProxy.StartSession;
  try
    Result := TrwReportResults.Create;

    try
      ctx_StartWait('Preparation to run reports', AReportList.Count);
      try
        Result.Descr := AReportList.Descr;

        // Prepare all reports for running
        // This process collects information about queries throughout all reports

        for i := 0 to AReportList.Count - 1 do
        begin
          AbortIfCurrentThreadTaskTerminated;

          RepRes := Result.FindReport(AReportList[i].Nbr, AReportList[i].Level);

          Result.Add;
          try
            Result[i].Data := nil;
            Result[i].NBR := AReportList[i].Nbr;
            Result[i].Level := AReportList[i].Level;
            Result[i].Tag := AReportList[i].Tag;
            Result[i].VmrTag := AReportList[i].VmrTag;
            Result[i].VmrCoNbr := AReportList[i].VmrCoNbr;
            Result[i].VmrPrNbr := AReportList[i].VmrPrNbr;
            Result[i].VmrEventDate := AReportList[i].VmrEventDate;

            Result[i].Layers := AReportList[i].Params.Layers;
            Result[i].Copies := AReportList[i].Params.Copies;
            Result[i].FileName := AReportList[i].Params.FileName;

            if not Assigned(RepRes) then
            begin
              // report is not prepared yet
              rn := AReportList[i].Caption;
              RepData := TEvDualStreamHolder.Create;
              try
                GetReportRes(AReportList[i].Nbr, AReportList[i].Level, True, rt, rn, RepData, mt, rc, ac);

                Result[i].ReportType := rt;
                Result[i].MediaType := mt;
                if Length(AReportList[i].Caption) = 0 then
                  Result[i].ReportName := rn
                else
                  Result[i].ReportName := AReportList[i].Caption;

                ctx_UpdateWait('Preparation to run reports' + #13#13 + Result[i].ReportName, i + 1);

                try
                  rwProxy.PrepareReport(RepData);
                except
                  on E: EevTransport do
                    rwProxy.PrepareReport(RepData);  // retry logic if RW has died in the middle
                end;

              finally
                RepData := nil;
              end;
            end

            else
            begin
              // report has been already prepared
              Result[i].ReportType := RepRes.ReportType;
              Result[i].MediaType := RepRes.MediaType;
              if Length(AReportList[i].Caption) = 0 then
                Result[i].ReportName := RepRes.ReportName
              else
                Result[i].ReportName := AReportList[i].Caption;
            end;


          except
            on E: Exception do
              Result[i].ErrorMessage := BuildStackedErrorStr(E);
          end;
        end;

      finally
        ctx_EndWait;
      end;


      //Actual running

      ctx_StartWait('Running reports', Result.Count);
      try
        for i := 0 to Result.Count - 1  do
        begin
          AbortIfCurrentThreadTaskTerminated;

          try
            if Length(Result[i].ErrorMessage) = 0 then
            begin
              ctx_UpdateWait('Running reports' + #13#13 + Result[i].ReportName, i + 1);
              RepData := TEvDualStreamHolder.Create;
              ClearRepResult;
              try
                GetReportRes(Result[i].NBR, Result[i].Level, False, rt, rn, RepData, mt, rc, ac);

                RunInfo.ShowInputForm := False;
                RunInfo.Duplexing := AReportList[i].Params.Duplexing;

                RunInfo.ReturnParamsBack := AReportList[i].ReturnParams;
                Par := AReportList[i].Params.ParamByName(__DASHBOARD); // Report should return data for dashboard
                if Assigned(Par) and (Par.VarType = varBoolean) and Boolean(Par.Value) then
                  RunInfo.ReturnParamsBack := True;

                RunInfo.ReturnPageInfo := True;
                RunInfo.ReportName := Result[i].ReportName;

                try
                  RepResult := rwProxy.RunReport(RepData, AReportList[i].Params, RunInfo);
                except
                  on E: EevTransport do
                    RepResult := rwProxy.RunReport(RepData, AReportList[i].Params, RunInfo); // retry logic if RW has died in the middle
                end;

                if RunInfo.ReturnParamsBack or (Assigned(AReportList[i].Params.ParamByName(__RUN_CONTROL))) then
                  Result[i].ReturnValues := AReportList[i].Params;

                Result[i].PagesInfo.Clear;
                if Assigned(RepResult.Data) then
                begin
                  if RunInfo.ReturnPageInfo then
                  begin
                    for j := Low(RepResult.PageInfo) to High(RepResult.PageInfo) do
                      Result[i].PagesInfo.AddPageInfo(RepResult.PageInfo[j].WidthMM, RepResult.PageInfo[j].HeightMM, RepResult.PageInfo[j].Duplexing);
                    Result[i].PagesInfo.CorrectPageCounts;
                  end;

                  Result[i].Data := RepResult.Data;

                  if RepResult.ResultType in [rwRTASCII, rwRTXML, rwRTXLS]  then
                  begin
                    Result[i].ReportType := rtASCIIFile;
                    if Result[i].FileName = '' then
                      Result[i].FileName := Trim(RepResult.DefaultFileName);
                    Result[i].FileName := ReplaceFileNameMacro(Result[i].FileName);
                  end;

                  if (Result[i].ReportType = rtASCIIFile) and (Result[i].FileName = '') then
                  begin
                    h := NormalizeFileName(Result[i].ReportName);
                    if RepResult.ResultType <> rwRTXLS then
                      Result[i].FileName := 'C:\ASCIIResults\' + h + ' ' + FormatDateTime('ddmmyy_hhmmss', Now) + '.txt'
                    else
                      Result[i].FileName := 'C:\ASCIIResults\' + h + ' ' + FormatDateTime('ddmmyy_hhmmss', Now) + '.xls';

                    Result[i].WarningMessage := 'Default ASCII file name is empty! File name has been assigned to "' + ExtractFileName(Result[i].FileName) + '"';
                  end;
                end;

              finally
                ClearRepResult;
                RepData := nil;
              end;
            end;

          except
            on E: Exception do
            begin
              try
                h := 'Parameters are dumped into:  \\' + GetComputerNameString + '\' + DumpParams(AReportList[i].Params, Result[i].ReportName) + #13;
                E.Message := E.Message + #13 + h;
              except
              end;

              Result[i].ErrorMessage := BuildStackedErrorStr(E);
              ClearRepResult;
            end;
          end;
        end;

      finally
        ctx_EndWait;
      end;

    except
      FreeAndNil(Result);
      raise;
    end;

  finally
    rwProxy.EndSession
  end;
end;

procedure TevRemoteReport.SetLowerRWEnginePriority(const AValue: Boolean);
begin
  rwProxy.SetLowerRWEnginePriority(AValue);
end;

procedure TevRemoteReport.StopReportExecution;
begin
  rwProxy.StopReportExecution;
end;

procedure TevRemoteReport.DoOnConstruction;
begin
  inherited;
  FrwProxy := GetRWProxy;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRWRemoteEngine, IevRWRemoteEngine, 'RW Remote Module');

end.
