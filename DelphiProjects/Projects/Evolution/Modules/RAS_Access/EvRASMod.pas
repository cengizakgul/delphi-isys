// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvRASMod;

interface

uses
  Windows, SysUtils, Classes, ShellAPI, RAS, isBaseClasses,
  EvTypes, EvUtils, ISUtils, EvCommonInterfaces, EvContext,
  EvMainboard, isVCLBugFix, EvExceptions;

implementation

type
  TConnectionStatus = (csConnecting, csDone, csError);

  TevRASAccess = class(TisInterfacedObject, IevRASAccess)
  private
    { Private declarations }
    hRas: THRasConn;
    DialParams: TRasDialParams;
    FConnectionStatus: TConnectionStatus;
    bConnectedInternally: Boolean;
    FName, FUser, FPassword: string;
    FGUICallback: IevRASAccessGUI;
    function GetConnectionHandle(ConnectionName: string): THRasConn;
    function StatusString(state: TRasConnState; error: Longint): string;
    function GetConnectionIPAddress: string;
  protected
    function  GetRASEntries: string;
    procedure BecomeIdle;
    procedure DialRasConnection(ConnectionName, UserName, Password: string);
    procedure SetConnection(ConnectionName: string);
    procedure HangupRasConnection(ConnectionName: string);
    function  GetFirstConnectionName: string;
    function  IsInternallyConnected: Boolean;
    function  RefreshStatus: Boolean;
    procedure SetGUICallback(const AGUICallback: IevRASAccessGUI);
  end;


function GetRASAccess: IevRASAccess;
begin
  Result := TevRASAccess.Create;
end;


// Callback RAS function

procedure RasCallback(msg: Integer; state: TRasConnState; error: Longint); stdcall;
var
  RO: TevRASAccess;
begin
  RO := TevRASAccess((Mainboard.RASAccess as IisInterfacedObject).GetImplementation);

  if State = RASCS_Connected then
  begin
    RO.FConnectionStatus := csDone;
    if Assigned(RO.FGUICallback) then
      RO.FGUICallback.UpdateRASDetails(RO.GetConnectionIPAddress)
    else
      ctx_UpdateWait(RO.GetConnectionIPAddress);
  end;

  if Error = 0 then
    if Assigned(RO.FGUICallback) then
      RO.FGUICallback.UpdateRASStatus(RO.StatusString(state, error))
    else
      ctx_UpdateWait(RO.StatusString(state, error))
  else
  begin
    RO.FConnectionStatus := csError;
    RO.BecomeIdle;
  end;
end;

function TevRASAccess.StatusString(state: TRasConnState;
  error: Integer): string;
var
  c: array[0..100] of Char;
  s: string;
begin
  if error <> 0 then
  begin
    RasGetErrorString(error, c, 100);
    Result := c;
  end
  else
  begin
    s := '';
    case State of
      RASCS_OpenPort:
        s := 'Opening port';
      RASCS_PortOpened:
        s := 'Port opened';
      RASCS_ConnectDevice:
        s := 'Connecting device';
      RASCS_DeviceConnected:
        s := 'Device connected';
      RASCS_AllDevicesConnected:
        s := 'All devices connected';
      RASCS_Authenticate:
        s := 'Start authenticating';
      RASCS_AuthNotify:
        s := 'Authentication: notify';
      RASCS_AuthRetry:
        s := 'Authentication: retry';
      RASCS_AuthCallback:
        s := 'Authentication: callback';
      RASCS_AuthChangePassword:
        s := 'Authentication: change password';
      RASCS_AuthProject:
        s := 'Authentication: projecting';
      RASCS_AuthLinkSpeed:
        s := 'Authentication: link speed';
      RASCS_AuthAck:
        s := 'Authentication: acknowledge';
      RASCS_ReAuthenticate:
        s := 'Authentication: reauthenticate';
      RASCS_Authenticated:
        s := 'Authenticated';
      RASCS_PrepareForCallback:
        s := 'Preparing for callback';
      RASCS_WaitForModemReset:
        s := 'Waiting for modem reset';
      RASCS_WaitForCallback:
        s := 'Waiting for callback';
      RASCS_Projected:
        s := 'Projected';
      RASCS_StartAuthentication:
        s := 'Start authentication';
      RASCS_CallbackComplete:
        s := 'Callback complete';
      RASCS_LogonNetwork:
        s := 'Logging on network';

      RASCS_Interactive:
        s := 'Interactive';
      RASCS_RetryAuthentication:
        s := 'Retry Authentication';
      RASCS_CallbackSetByCaller:
        s := 'Callback set by caller';
      RASCS_PasswordExpired:
        s := 'Password expired';

      RASCS_Connected:
        s := 'Connected';
      RASCS_Disconnected:
        s := 'Disconnected';
    end;
    Result := s;
  end;
end;

procedure TevRASAccess.BecomeIdle;
begin
  if Assigned(FGUICallback) then
    FGUICallback.UpdateRASStatus('Idle...')
end;

procedure TevRASAccess.DialRasConnection(ConnectionName, UserName, Password: string);
var
  fp: LongBool;
  r: Longint;
  c: array[0..100] of Char;
  tempRAS: THRasConn;
  TempName: string;
  bDisconnect: Boolean;
begin
  TempName := GetFirstConnectionName;
  if TempName <> '' then
  begin
    if Assigned(FGUICallback) then
    begin
      bDisconnect := False;
      FGUICallback.OnBusyConnection(TempName, bDisconnect);
      if bDisconnect then
      begin
        HangupRasConnection(TempName);
        FGUICallback.UpdateRASStatus('Waiting');
        Sleep(3000); // wait 3 seconds
      end
      else
        AbortEx;
    end;
  end;

  if bConnectedInternally then
  begin
    ConnectionName := FName;
    UserName := FUser;
    Password := FPassword;
  end;

  FillChar(dialparams, SizeOf(TRasDialParams), 0);
  with dialparams do
  begin
    dwSize := Sizeof(TRasDialParams);
    StrPCopySafe(szEntryName, ConnectionName, Length(szEntryName));
  end;
  r := RasGetEntryDialParams(nil, dialparams, fp);
  if r = 0 then
  begin
    if (UserName <> '') and (Password <> '') then
    begin
      StrPCopySafe(dialparams.szUserName, UserName, Length(dialparams.szUserName));
      StrPCopySafe(dialparams.szPassword, Password, Length(dialparams.szPassword));
    end;
{  with dialparams do
  begin
    txtUserName.Text := szUserName;
    if fp then
      txtPassword.Text := szPassword;
  end}
  end
  else
  begin
    RasGetErrorString(r, c, 100);
    if Assigned(FGUICallback) then
      FGUICallback.ShowMessage('RasGetEntryDialParams failed: ' + c);
    exit;
  end;

  ctx_StartWait;
  try
    try
      FConnectionStatus := csConnecting;

      tempRas := hRas;
      hRas := 0;
        // Async dial
      r := RasDial(nil, // This field is ignored in Windows95
        nil, // Phonebook: use default (not used on Win95)
        dialparams,
        0, // use callback function of type RASDIALFUNC
        @RasCallback, // callback function
        hRas);
      if r <> 0 then
      begin
        RasGetErrorString(r, c, 100);
        RasHangUp(tempRas);
        Sleep(1000); // wait 1 second
        BecomeIdle;
        raise ERASConnectionError.CreateHelp('Dial-Up failed: ' + c, IDH_RASConnectionError);
      end;

      while FConnectionStatus = csConnecting do
      begin
        Sleep(100);
        if Assigned(FGUICallback) then
          FGUICallback.UpdateRASStatus('Connecting');
      end;
    finally
      ctx_EndWait;
    end;
  except
    raise;
  end;

  if FConnectionStatus = csError then
    raise ERASConnectionError.CreateHelp('An error occured while dialing-up.', IDH_RASConnectionError);

  bConnectedInternally := True;
  FName := ConnectionName;
  FUser := UserName;
  FPassword := Password;
end;

function TevRASAccess.GetConnectionHandle(ConnectionName: string): THRasConn;
var
  bufsize: Longint;
  numConnections: Longint;
  Connections: array[1..100] of TRasConn;
  I: Integer;
begin
  Result := 0;
  Connections[1].dwSize := SizeOf(TRasConn);
  bufsize := SizeOf(TRasConn) * 100;
  if RasEnumConnections(@Connections[1], bufsize, numConnections) = 0 then
  begin
    if numConnections > 0 then
      for I := 1 to numConnections do
        if Connections[I].szEntryName = ConnectionName then
          Result := Connections[I].hrasconn;
  end
  else
    if Assigned(FGUICallback) then
      FGUICallback.ShowMessage('Getting Dial-Up Connections failed!');
end;

function TevRASAccess.GetConnectionIPAddress: string;
var
  lpProjection: TRASpppIP;
  lpcb: Longint;
begin
  lpcb := SizeOf(TRASpppIP);
  lpProjection.dwSize := SizeOf(TRASpppIP);
  if RasGetProjectionInfo(hRas, RASP_pppIP, @lpProjection, lpcb) = 0 then
    Result := lpProjection.szIPaddress;
end;

function TevRASAccess.GetFirstConnectionName: string;
var
  bufsize: Longint;
  numConnections: Longint;
  Connections: array[1..100] of TRasConn;
  I: Integer;
  ConnectionStatus: TRasConnStatusA;
begin
  Result := '';
  Connections[1].dwSize := SizeOf(TRasConn);
  bufsize := SizeOf(TRasConn) * 100;
  if RasEnumConnections(@Connections[1], bufsize, numConnections) = 0 then
  begin
    if numConnections > 0 then
      for I := 1 to numConnections do
      begin
        ConnectionStatus.dwSize := SizeOf(TRasConnStatusA);
        RasGetConnectStatus(Connections[I].hrasconn, ConnectionStatus);
        if ConnectionStatus.rasconnstate = RASCS_Connected then
        begin
          Result := Connections[I].szEntryName;
          Break;
        end;
      end;
  end
  else
    if Assigned(FGUICallback) then
      FGUICallback.ShowMessage('Getting Dial-Up Connections failed!');
end;

procedure TevRASAccess.HangupRasConnection(ConnectionName: string);
begin
  hRas := GetConnectionHandle(ConnectionName);
  if RasHangUp(hRas) = 0 then
  begin
    if Assigned(FGUICallback) then
    begin
      FGUICallback.UpdateRASStatus('Disconnecting');
      FGUICallback.UpdateRASDetails('');
    end;
    Sleep(2000); // wait 2 seconds
    BecomeIdle;
    RefreshStatus;
  end
  else
  begin
    if Assigned(FGUICallback) then
      FGUICallback.ShowMessage('Hangup failed.');
    AbortEx;
  end;
  bConnectedInternally := False;
end;

function TevRASAccess.RefreshStatus: Boolean;
var
  ConnectionStatus: TRasConnStatusA;
begin
  ConnectionStatus.dwSize := SizeOf(TRasConnStatusA);
  RasGetConnectStatus(hRas, ConnectionStatus);
  if Assigned(FGUICallback) then
    FGUICallback.UpdateRASStatus('');
  Result := (ConnectionStatus.dwError = 0) and (hRas <> 0) and (ConnectionStatus.rasconnstate = RASCS_Connected);
  if (ConnectionStatus.dwError = 0) and (hRas <> 0) then
  begin
    if Assigned(FGUICallback) then
    begin
      FGUICallback.UpdateRASStatus(StatusString(ConnectionStatus.rasconnstate, 0));
      if ConnectionStatus.rasconnstate = RASCS_Connected then
        FGUICallback.UpdateRASDetails(GetConnectionIPAddress);
    end;
  end
  else
  begin
    if Assigned(FGUICallback) then
      FGUICallback.UpdateRASStatus('Disconnected');
  end;
end;

function TevRASAccess.GetRASEntries: string;
var
  bufsize: Longint;
  numEntries: Longint;
  entries: array[1..100] of TRasEntryName;
  x: Integer;
begin
  Result := '';
  entries[1].dwSize := SizeOf(TRasEntryName);
  bufsize := SizeOf(TRasEntryName) * 100;

  if RasEnumEntries(nil, nil, @entries[1], bufsize, numEntries) = 0 then
    if numEntries > 0 then
      for x := 1 to numEntries do
        Result := Result + entries[x].szEntryName + #13#10;
end;

procedure TevRASAccess.SetConnection(ConnectionName: string);
begin
  hRas := GetConnectionHandle(ConnectionName);
end;

function TevRASAccess.IsInternallyConnected: Boolean;
begin
  Result := bConnectedInternally;
end;

procedure TevRASAccess.SetGUICallback(const AGUICallback: IevRASAccessGUI);
begin
  FGUICallback := AGUICallback;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetRASAccess, IevRASAccess, 'RAS Access');

end.
