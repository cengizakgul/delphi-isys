unit bbExternalInterfaces;

interface

uses
  isBaseClasses, SysUtils;

const
   BBPort = '9499';

   Method_Result = 'Result';

   Method_RB_RunReport = 'RequestBroker.RunReport';
   Method_RB_GetSbInfo = 'RequestBroker.GetSbInfo';
   Method_RB_UpdateSystemReport = 'RequestBroker.UpdateSystemReport';
   Method_RB_SetNewLicense = 'RequestBroker.SetNewLicense';
   Method_RB_GetReportResults = 'RequestBroker.GetReportResults';

   Method_BB_ReportReady = 'BigBrother.ReportReady';
   Method_BB_Register = 'BigBrother.Register';

   SbInfo_Serial = 'SerialNumber';
   SbInfo_SysVer = 'SystemVersion';
   SbInfo_DBVer = 'DBVersion';
   SbInfo_RWUpdateAllowed = 'RWUpdateAllowed';
   SbInfo_LicenseUpdateAllowed = 'LicenseUpdateAllowed';
   SbInfo_Modifier = 'ComputerModifier';

   RunReport_Billing = 'S154';  // Billing Transactions
   RunReport_RunOK = 'OK';
   RunReport_RunError = 'Error';
   RunReport_RunErrorException = 'ErrorException';
   RunReport_RefusedCalculating = 'Calculating';

   UpdateSystemReport_ReportData = 'ReportData';

   APIKey_Key = 'APIKey';
   APIKey_AppName = 'APIKeyApplicationName';
   APIKey_VendorCustomNbr = 'APIKeyVendorCustomNbr';
   APIKey_VendorName = 'APIKeyVendorName';

type

   ENoReadyReports = Exception;
   EDuplicateSerialNumber = Exception;

   IevBBController = interface
   ['{05048705-590D-4477-900F-4BAE004370EA}']
     function  GetSbInfo(const ASerialNumber: String): IisListofValues;

     function  RunReport(const ASerialNumber: String; const AReportNumber: string;
                         const AReportParams: IisListofValues): IisListOfValues;
     function  GetReportResults(const ASerialNumber: String; const AReportNumber: string): IisListOfValues;

     procedure UpdateSystemReport(const ASerialNumber: String; const AReportNumber: string;
                                  const AReportData: IisListOfValues);

     procedure SetNewLicense(const ASerialNumber: String; const ALicense: String);
  end;


  IBigBrother = interface
  ['{60586BB4-2DBF-41C0-900E-B0718C5FB883}']
    procedure ReportReady(const ASerialNumber: String; const AReportNumber: string);
  end;

implementation

end.

