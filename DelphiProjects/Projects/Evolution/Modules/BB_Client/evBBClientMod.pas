// Client for Big Brother

unit evBBClientMod;

interface

uses
  SysUtils, Classes, DB, isBaseClasses, isThreadManager, bbExternalInterfaces,
  EvTransportShared, EvCommonInterfaces, EvTransportInterfaces, ISBasicUtils,
  isSocket, EvMainBoard, EvTransportDatagrams, EvBasicUtils, SReportSettings,
  evContext, evConsts, isLogFile, EvTypes, EvStreamUtils, SDataDictSystem,
  SDataStructure,  ISZippingRoutines, EvClasses, ISErrorUtils,
  evExceptions, Variants, EvUtils;


implementation

const
  ErrorsReturnParam = 'Errors';
  MainHost = 'update.isystemsllc.com';
  SecondaryHost = 'update2.isystemsllc.com';

type
  IevBBTCPClient = interface
  ['{A0DF1410-1657-4A81-BD08-1B3B5FE75A42}']
    procedure Connect;
    procedure Disconnect;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  Connected : boolean;
    function  GetHost: String;
    function  GetPort: String;
  end;


  TevBBTCPClient = class(TevCustomTCPClient, IevBBTCPClient)
  private
    FSessionID: TisGUID;
    FHost: String;
    FPort: String;
  protected
    function  Connected : boolean;
    procedure Connect;
    procedure Disconnect;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  GetHost: String;
    function  GetPort: String;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    procedure DoOnConstruction; override;
    function  Session: IevSession;
  end;


  TBBDatagramDispatcher = class(TevDatagramDispatcher)
  private
    function  BBController: IevBBController;

    procedure dmGetSbInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRunReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetReportResults(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmUpdateSystemReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetNewLicense(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure RegisterHandlers; override;
  end;


  TevBBController = class(TisInterfacedObject, IevBBClient, IevBBController, IBigBrother)
  private
    FTCPClient: IevBBTCPClient;
    FConnectionWatchDogId: TTaskId;
    FReportResultsWatchDogId: TTaskId;
    FStorageLock: IisLock;
    function  CreateContext(const ASerialNumber: String): IevDomainInfo;
    function  CreateGuestContext(const ASerialNumber: String): IevDomainInfo;
    function  RunReportInternal(const ASerialNumber : String; const AReportNumber : string; const AReportParams : IisListofValues) : TisGUID;
    function  isTaskFinished(const ASerialNumber : String; const ATaskId : TisGUID) : integer; // returns: 0 - not finished, 1 - finished, -1 - task not found
    function  GetDomainBySN(const ASerialNumber: String): IevDomainInfo;
    procedure ConnectionWatchDog(const Params : TTaskParamList);
    procedure ReportResultsWatchDog(const Params : TTaskParamList);
    procedure SendEMailNotification(const ASerialNumber: String; const AError: String);
  protected
    procedure DoOnConstruction; override;

    // service calls for RunReport, GetReportResults, ReportReady
    function  GetStorageLock : IisLock;
    function  IsReportResultsReady(const ASerialNumber: String; const AReportNumber : String): Boolean;
    function  GetDoneTaskId(const ASerialNumber: String; const AReportNumber : String) : TisGUID; // returns 'N' if not exists any
    function  GetInProcessTaskId(const ASerialNumber: String; const AReportNumber : String) : TisGUID; // returns 'N' if not exists any
    procedure SetDoneTaskId(const ASerialNumber: String; const AReportNumber : String; AValue : TisGUID = 'N');
    procedure SetinProcessTaskId(const ASerialNumber: String; const AReportNumber : String; AValue : TisGUID = 'N');

    function SerialNumberToDomainName(const ASerialNumber: String): String;

    // interface IevBBController
    function  GetSbInfo(const ASerialNumber: String) : IisListofValues;
    function  RunReport(const ASerialNumber: String; const AReportNumber : string; const AReportParams : IisListofValues) : IisListOfValues;
    function  GetReportResults(const ASerialNumber: String; const AReportNumber : string)  : IisListOfValues;
    procedure UpdateSystemReport(const ASerialNumber: String; const AReportNumber : string; const AReportData : IisListOfValues);
    procedure SetNewLicense(const ASerialNumber: String; const ALicense : String);

    // interface IBigBrother (proxy)
    procedure ReportReady(const ASerialNumber: String; const AReportNumber : string);

    // interface IevBBClient
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  Connected: boolean;
    function  DiagnosticCheck: String;
  public
    destructor Destroy; override;
  end;


function GetBBClient: IevBBClient;
begin
  Result := TevBBController.Create;
end;

{ TevBBController }

procedure TevBBController.ConnectionWatchDog(const Params: TTaskParamList);
begin
  repeat
    if not FTCPClient.Connected then
      try
        FTCPClient.Connect;
      except
        // eat all exceptions
        // todo: Log
      end;
    Snooze(60000)
  until CurrentThreadTaskTerminated;
end;

function TevBBController.CreateContext(const ASerialNumber: String): IevDomainInfo;
begin
  Result := GetDomainBySN(ASerialNumber);
  if not Assigned(Result) then
    raise EevException.Create('SB is not found');

  Mainboard.ContextManager.CreateThreadContext(
            EncodeUserAtDomain(Result.SystemAccountInfo.UserName, Result.DomainName),
            Result.SystemAccountInfo.Password);
end;

destructor TevBBController.Destroy;
begin
  SetActive(False);
  inherited;
end;

function TevBBController.GetDoneTaskId(const ASerialNumber, AReportNumber: String): TisGUID;
var
  S: String;
begin
  Result := 'N';
  S := mb_AppConfiguration.GetValue('BBReports\'+SerialNumberToDomainName(ASerialNumber)+
                                    '\'+AReportNumber, 'N;N');
  if pos(';', S) > 0 then
    Result := copy(S, pos(';', S)+1, 1000);
end;

function TevBBController.GetInProcessTaskId(const ASerialNumber, AReportNumber: String): TisGUID;
var
  S: String;
begin
  Result := 'N';
  S := mb_AppConfiguration.GetValue('BBReports\'+SerialNumberToDomainName(ASerialNumber)+
                                    '\'+AReportNumber, 'N;N');
  if pos(';', S) > 0 then
    Result := copy(S, 1, pos(';', S)-1);
end;

function TevBBController.GetStorageLock: IisLock;
begin
  Result := FStorageLock;
end;

function TevBBController.GetReportResults(const ASerialNumber: String; const AReportNumber: string): IisListOfValues;
var
  sTaskId: TisGUID;
  iReady: Integer;
  Task: IevTask;
  ReportResults: TrwReportResults;
  sErrorMessage : String;
  stream : IevDualStream;

  procedure CheckResultForError(const AReportResults: TrwReportResults);
  var
    ErrorsInResult: TrwReportParam;
  begin
    if AReportResults.Count > 0 then
    begin
      ErrorsInResult := AReportResults.Items[0].ReturnValues.ParamByName(ErrorsReturnParam);
      if Assigned(ErrorsInResult) and (VarToStr(ErrorsInResult.Value) <> '') then
        SendEMailNotification(ASerialNumber, VarToStr(ErrorsInResult.Value));
    end;
  end;
begin
  Result := TisListOfValues.Create;
  Task := nil;

  GetStorageLock.Lock;
  try
    ///////////////////// default domain //////////////////////
    sTaskId := GetDoneTaskId(ASerialNumber, AReportNumber);
    if sTaskId <> 'N' then
    begin
      iReady := isTaskFinished(ASerialNumber, sTaskId);
      if iReady = -1 then
      begin
        SetDoneTaskId(ASerialNumber, AReportNumber, 'N');
        sTaskId := 'N';
        mb_LogFile.AddEvent(etError, 'BB client. GetReportResults call', 'Current RunReport task not exist in queue', 'Current RunReport task not exist in queue for main domain. Serial# ' + ASerialNumber + '. Report# ' + AReportNumber);
      end;
    end
    else
      iReady := 1;

    // raise ENoReadyReports exception if there's no results avaliable!!!
    if (sTaskId = 'N') or (iReady <> 1) then
      raise ENoReadyReports.Create('No report result found. SerialNumber: ' + ASerialNumber + '. ReportNumber: ' + AReportNumber);

    Mainboard.ContextManager.StoreThreadContext;
    try
      CreateContext(ASerialNumber);
      Task := mb_TaskQueue.GetTask(sTaskId);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

    if not Assigned(Task) then
      raise ENoReadyReports.Create('No billing report result found. SerialNumber: ' + ASerialNumber + '. ReportNumber: ' + AReportNumber);
    if not Supports(Task, IevPrintableTask) then
      raise EevException.Create('Billing report results is not printable type! SerialNumber: ' + ASerialNumber + '. ReportNumber: ' + AReportNumber);

    if Task.Exceptions = '' then
    begin
      ReportResults := TrwReportResults.Create((Task as IevPrintableTask).ReportResults);
      try
        CheckResultForError(ReportResults);
        Result.AddValue(Method_Result, ReportResults.Items[0].Data);
      finally
        FreeAndNil(ReportResults);
      end;
    end
    else
    begin
      sErrorMessage := GetDomainBySN(ASerialNumber).DomainName + ' domain' + #13#10 + Task.Exceptions;

      stream := TEvDualStreamHolder.Create;
      stream.Writeln(sErrorMessage);

      Result.AddValue(Method_Result, stream);
    end;

  finally
    GetStorageLock.Unlock;
  end;
end;

function TevBBController.GetSbInfo(const ASerialNumber: String): IisListofValues;
begin
  Result := TisListOfValues.Create;

  Result.AddValue(SbInfo_Serial, ASerialNumber);
  Result.AddValue(SbInfo_SysVer, AppVersion);

  try
    Mainboard.ContextManager.StoreThreadContext;
    try
      CreateContext(ASerialNumber);
      Result.AddValue(SbInfo_DBVer, ctx_DBAccess.GetDBVersion(dbtSystem));
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  except
    on E:Exception do
      Result.AddValue(SbInfo_DBVer, Copy(E.Message, 1, 30));
  end;

  Result.AddValue(SbInfo_RWUpdateAllowed, True);
  Result.AddValue(SbInfo_LicenseUpdateAllowed, True);
  Result.AddValue(SbInfo_Modifier, Integer(Mainboard.MachineInfo.ID));
end;

function TevBBController.IsReportResultsReady(const ASerialNumber, AReportNumber: String): Boolean;
begin
  Result := GetDoneTaskId(ASerialNumber, AReportNumber) <> 'N';
end;

procedure TevBBController.ReportReady(const ASerialNumber: String; const AReportNumber: string);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(Method_BB_ReportReady);
  D.Params.AddValue('ASerialNumber', ASerialNumber);
  D.Params.AddValue('AReportNumber', AReportNumber);

  D := FTCPClient.SendRequest(D);
end;

procedure TevBBController.ReportResultsWatchDog(const Params: TTaskParamList);
const
  MaxErrorCnt = 100;
var
  TaskState: Integer;
  iErrorCnt : integer;
  bFlag : boolean;
  sTaskId: TisGUID;
  sSerialNumber : String;

  procedure DoWork;
  var
    i : integer;
    localDomainsList: IevDomainInfoList;
  begin
    try
      if FTCPClient.Connected then
      begin
        // check if all reports results are ready and call ReportReady
(*        for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
            if IsReportResultsReady(mb_GlobalSettings.DomainInfoList.Items[i].LicenseInfo.SerialNumber, RunReport_Billing) then
              ReportReady(mb_GlobalSettings.DomainInfoList.Items[i].LicenseInfo.SerialNumber, RunReport_Billing);
*)
        // check if task is calculated and move it to done
        GetStorageLock.Lock;
        try
          mb_GlobalSettings.DomainInfoList.Lock;
          try
            localDomainsList := (mb_GlobalSettings.DomainInfoList as IisCollection).GetClone as IevDomainInfoList;
          finally
            mb_GlobalSettings.DomainInfoList.Unlock;
          end;

          for i := 0 to localDomainsList.Count - 1 do
          begin
            sSerialNumber := localDomainsList.Items[i].Licenseinfo.SerialNumber;

            sTaskId := GetInProcessTaskId(sSerialNumber, RunReport_Billing);
            if sTaskId <> 'N' then
            begin
              TaskState := isTaskFinished(sSerialNumber, sTaskId);
              case TaskState of
                -1 : begin
                       SetInProcessTaskId(sSerialNumber, RunReport_Billing, 'N');
                     end;
                 0 : ; // do nothing because it's not ready yet
                 1 : begin  // already finished
                       SetDoneTaskId(sSerialNumber, RunReport_Billing, sTaskid);
                       ReportReady(sSerialNumber, RunReport_Billing);
                     end;
              else
                raise EevException.Create('wrong result of IsTaskFinished');
              end;
            end;
          end;
        finally
          GetStorageLock.Unlock;
        end;
      end;
    except
      on E:Exception do
      begin
        Inc(iErrorCnt);
        if iErrorCnt <= MaxErrorCnt then
          mb_LogFile.AddEvent(etError, 'Rb for BB', 'Error in ReportResultsWatchDog.', 'Error in ReportResultsWatchDog. ' + E.Message)
        else
          if not bFlag then
          begin
            bFlag := true;
            mb_LogFile.AddEvent(etError, 'Rb for BB', 'Too many errors in ReportResultsWatchDog.', 'Logging of errors in ReportResultsWatchDog is stopped. Reason: too many errors');
          end;
      end;
    end;
  end;
begin
  iErrorCnt := 0;
  bFlag := false;
  repeat
    DoWork;
    Snooze(60000);
  until CurrentThreadTaskTerminated;
end;

function TevBBController.RunReport(const ASerialNumber: String; const AReportNumber: string;  const AReportParams: IisListofValues): IisListOfValues;
var
  sTaskId : TisGUID;
  iReady: Integer;
begin
  Result := TisListOfValues.Create;

  GetStorageLock.Lock;
  try
    // checking if report is already run
    sTaskId := GetInProcessTaskId(ASerialNumber, AReportNumber);
    if sTaskId <> 'N' then
    begin
      iReady := isTaskFinished(ASerialNumber, sTaskId);
      if iReady = -1 then
      begin
        SetDoneTaskId(ASerialNumber, AReportNumber, 'N');
        sTaskId := 'N';
        mb_LogFile.AddEvent(etError, 'BB client. RunReport call', 'Current RunReport task not exist in queue', 'Current RunReport task not exist in queue for main domain. Serial# ' + ASerialNumber + '. Report# ' + AReportNumber);
      end else if iReady = 1 then
        sTaskId := 'N';
    end;

    if (sTaskId <> 'N') then
    begin
      Result.AddValue(Method_Result, RunReport_RefusedCalculating);  // already calculating
      Exit;
    end;

    // trying to start report
    sTaskId := 'N';
    try
      sTaskId := RunReportInternal(ASerialNumber, AReportNumber, AReportParams);
      SetInProcessTaskId(ASerialNumber, AReportNumber, sTaskId);
      Result.AddValue(Method_Result, RunReport_RunOK);
    except
      on E:Exception do
      begin
        Result.AddValue(Method_Result, RunReport_RunError);
        Result.AddValue(RunReport_RunErrorException, E.Message);
        SetDoneTaskId(ASerialNumber, AReportNumber, 'N');
        try
          mb_LogFile.AddEvent(etError, 'BB client. RunReport call',
            'Cannot run billing report',
            'Cannot run billing report' + '. Serial# '
            + ASerialNumber + '. Report# ' + AReportNumber + #13#10 + GetStack);
          SendEMailNotification(ASerialNumber, 'Cannot run billing report' +'. Exception: ' + E.Message);
          if sTaskId <> 'N' then
          begin
            Mainboard.ContextManager.StoreThreadContext;
            try
              CreateContext(ASerialNumber);
              mb_TaskQueue.RemoveTask(sTaskId);
            finally
              Mainboard.ContextManager.RestoreThreadContext;
            end;
          end;
        except
        end;
      end;
    end;
  finally
    GetStorageLock.Unlock;
  end;
end;

function TevBBController.RunReportInternal(const ASerialNumber : String; const AReportNumber: string; const AReportParams: IisListofValues): TisGUID;
var
  Report: TrwRepParams;
  Reports: TrwReportList;
  qTask: IevRunReportTask;
  i : integer;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    CreateContext(ASerialNumber);

    Reports := TrwReportList.Create;
    try
      Report := Reports.AddReport;
      Report.Level := UpperCase(copy(AReportNumber,1,1));
      Report.NBR := StrToInt(copy(AReportNumber,2,1000));
      Report.Caption := 'Billing Transactions';
      Report.ReturnParams := true;

      for i := 0 to AReportParams.Count - 1 do
        Report.Params.Add(AReportParams.GetValue(i).Name, AReportParams.GetValue(i).Value);
      Report.Params.Add(ErrorsReturnParam, ''); // have to add this parameter to get back error string, because RW returns back values for input parameters only

      qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_RUN_REPORT, True) as IevRunReportTask;
      qTask.Caption := Report.Caption;
      qTask.Reports := Reports.GetAsStream;
      qTask.Destination := rdtNone;
      qTask.NotificationEmail := Mainboard.AppConfiguration.AsString['EMail\SendAlertsTo'];
      qTask.EmailSendRule := esrExceptions;
      qTask.SendEmailNotification := true;
      mb_TaskQueue.AddTask(qTask);
      Result := qTask.ID;
    finally
      FreeAndNil(Reports);
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevBBController.SerialNumberToDomainName(const ASerialNumber: String): String;
var
  D: IevDomainInfo;
begin
  D := GetDomainBySN(ASerialNumber);
  CheckCondition(Assigned(D), 'SB is not found');
  Result := D.DomainName;
end;

procedure TevBBController.SetDoneTaskId(const ASerialNumber, AReportNumber: String; AValue: TisGUID);
var
  S: String;
begin
  S := 'N;' + AValue;
  mb_AppConfiguration.SetValue('BBReports\' + SerialNumberToDomainName(ASerialNumber) +
                                    '\' + AReportNumber, S);
end;

procedure TevBBController.SetinProcessTaskId(const ASerialNumber, AReportNumber: String; AValue: TisGUID);
var
  S: String;
begin
  S := AValue + ';N';
  mb_AppConfiguration.SetValue('BBReports\' + SerialNumberToDomainName(ASerialNumber) +
                                    '\' + AReportNumber, S);
end;

procedure TevBBController.SetNewLicense(const ASerialNumber: String; const ALicense: String);
var
  D: IevDomainInfo;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    CreateGuestContext(ASerialNumber);
    D := GetDomainBySN(ASerialNumber);
    CheckCondition(Assigned(D), 'SB not found!');
    D.LicenseInfo.Code := ALicense;
    mb_GlobalSettings.Save;
    Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('License');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevBBController.UpdateSystemReport(const ASerialNumber: String; const AReportNumber: string; const AReportData: IisListOfValues);
var
  ms1, ms2 : IisStream;
  ds: TSY_REPORT_WRITER_REPORTS;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    CreateContext(ASerialNumber);

    ms1 := IInterface(AReportData.Value[UpdateSystemReport_ReportData]) as IisStream;
    ms2 := TisStream.Create;

    ds := DM_SYSTEM.SY_REPORT_WRITER_REPORTS;
    ds.RetrieveCondition := 'SY_REPORT_WRITER_REPORTS_NBR=' + AReportNumber;

    ctx_DataAccess.OpenDataSets([ds]);
    if ds.RecordCount = 1 then
    begin
      try
        ctx_DataAccess.StartNestedTransaction([dbtSystem]);
        ds.Edit;
        try
          ms1.Position := 0;
          DeflateStream(ms1.RealStream, ms2.RealStream);
          ms2.Position := 0;

          TBlobField(ds.FieldByName('REPORT_FILE')).LoadFromStream(ms2.RealStream);
          ds.Post;
        except
          ds.Cancel;
          raise;
        end;
        ctx_DataAccess.PostDataSets([ds]);
        ctx_DataAccess.CommitNestedTransaction;
      except
        ctx_DataAccess.RollbackNestedTransaction;
        raise;
      end;
    end
    else
      raise EevException.Create('Report #' + AReportNumber + ' not found in system database');

  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevBBController.isTaskFinished(const ASerialNumber: String; const ATaskId: TisGUID): integer;
var
  TaskInfo: IevTaskInfo;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    CreateContext(ASerialNumber);

    TaskInfo := mb_TaskQueue.GetTaskInfo(ATaskId);
    if not Assigned(TaskInfo) then
      result := -1
    else
      if TaskInfo.State = tsFinished then
        result := 1
      else
        result := 0;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;


function TevBBController.Connected: boolean;
begin
  Result := FTCPClient.Connected;
end;

function TevBBController.GetActive: Boolean;
begin
  Result := FConnectionWatchDogId <> 0;
end;

procedure TevBBController.SetActive(const AValue: Boolean);
begin
  if GetActive <> AValue then
    if not GetActive then
    begin
      // start
      FTCPClient := TevBBTCPClient.Create;
      FConnectionWatchDogId := GlobalThreadManager.RunTask(ConnectionWatchDog, Self, MakeTaskParams([]), 'BB connection watchdog');
      FReportResultsWatchDogId := GlobalThreadManager.RunTask(ReportResultsWatchDog, Self, MakeTaskParams([]), 'BB report results watchdog');
    end
    else
    begin
      // stop
      try
        GlobalThreadManager.TerminateTask(FReportResultsWatchDogId);
        GlobalThreadManager.TerminateTask(FConnectionWatchDogId);
        GlobalThreadManager.WaitForTaskEnd(FConnectionWatchDogId);
        GlobalThreadManager.WaitForTaskEnd(FReportResultsWatchDogId);
      finally
        FConnectionWatchDogId := 0;
        FReportResultsWatchDogId := 0;
        FTCPClient := nil;
      end;
    end;
end;

procedure TevBBController.DoOnConstruction;
begin
  inherited;
  FStorageLock := TisLock.Create;
end;

function TevBBController.GetDomainBySN(const ASerialNumber: String): IevDomainInfo;
var
  i: Integer;
begin
  Result := nil;
  mb_GlobalSettings.DomainInfoList.Lock;
  try
    for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      if (mb_GlobalSettings.DomainInfoList[i].LicenseInfo.SerialNumber = ASerialNumber) then
      begin
        Result := mb_GlobalSettings.DomainInfoList[i];
        break;
      end;
  finally
    mb_GlobalSettings.DomainInfoList.Unlock;
  end;
end;

function TevBBController.CreateGuestContext(const ASerialNumber: String): IevDomainInfo;
begin
  Result := GetDomainBySN(ASerialNumber);
  if not Assigned(Result) then
    raise EevException.Create('SB is not found');

  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, Result.DomainName), '');
end;

function TevBBController.DiagnosticCheck: String;
begin
  Result := '';
  SetActive(False);
  try
    try
      FTCPClient := TevBBTCPClient.Create;
      try
        FTCPClient.Connect;
        Result := 'Successfully connected to BB';
        FTCPClient.Disconnect;
      except
        on E: Exception do
        begin
          Result := Format('Error connecting to BB'#13#10'%s',[E.Message]);
        end;
      end;
    finally
      SetActive(True);
    end;
  except
  end;  
end;

procedure TevBBController.SendEMailNotification(const ASerialNumber: String; const AError: String);
var
  sSubject, sFrom, sTo, sMessage : String;
begin
  sFrom := 'Evolution@' + mb_GlobalSettings.EmailInfo.NotificationDomainName;
  sTo := Mainboard.AppConfiguration.AsString['EMail\SendAlertsTo'];
  sSubject := 'Alert from ' + ExtractFileName(AppFileName) + ' on ' + Mainboard.MachineInfo.Name;

  if (sTo <> '') and (sFrom <> '') then
    try
      Mainboard.ContextManager.StoreThreadContext;
      try
        CreateContext(ASerialNumber);

        sMessage := 'Errors during calculation of "Billing transactions (S154)" report for "' +
                    Context.DomainInfo.DomainName + '" domain.' + #13#10#13#10 + AError;
        ctx_EMailer.SendQuickMail(sTo, sFrom, sSubject, sMessage);
      finally
        Mainboard.ContextManager.RestoreThreadContext;
      end;
    except
      on E:Exception do
        mb_LogFile.AddEvent(etError, 'Email Notifier', 'Email sending problem', E.Message);
    end;
end;

{ TevBBTCPClient }

procedure TevBBTCPClient.Connect;
var
  D: IevDatagram;
  S: IisStringList;
  i: Integer;
  sTmp : String;
begin
  Lock;
  try
    S := TisStringList.Create;
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      begin
        sTmp := Trim(mb_GlobalSettings.DomainInfoList.Items[i].LicenseInfo.SerialNumber);
        if (sTmp <> '0') and (sTmp <> '') then
          S.Add(sTmp);
      end;
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
    if S.Count = 0 then
      Exit;

    // connecting to main host
    FHost := MainHost;
    try
      FSessionID := CreateConnection(FHost, FPort, mb_GlobalSettings.DomainInfoList.Items[0].LicenseInfo.SerialNumber, '', '', '');
    except
      on E: Exception do
      begin
        FHost := SecondaryHost;
        FSessionID := CreateConnection(FHost, FPort, mb_GlobalSettings.DomainInfoList.Items[0].LicenseInfo.SerialNumber, '', '', '');
      end;
    end;
    D := TevDatagram.Create(Method_BB_Register);

    D.Params.AddValue('SerialNumbers', S);
    SendRequest(D);
  finally
    Unlock;
  end;
end;

function TevBBTCPClient.Connected: boolean;
begin
  result := Session <> nil;
end;

function TevBBTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TBBDatagramDispatcher.Create;
end;

procedure TevBBTCPClient.Disconnect;
begin
  if Connected then
    inherited Disconnect(FSessionId);
end;

procedure TevBBTCPClient.DoOnConstruction;
begin
  inherited;
  FHost := MainHost;
  FPort := BBPort;
  SetEncryptionType(etProprietary);
  // client doesn't need so many threads
  GetDatagramDispatcher.ThreadPoolCapacity := 2;
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPClient).ThreadPoolCapacity := 2;
end;

function TevBBTCPClient.GetHost: String;
begin
  Result := FHost;
end;

function TevBBTCPClient.GetPort: String;
begin
  Result := FPort;
end;

function TevBBTCPClient.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
begin
  S := Session;
  if not Assigned(S) then
    raise EevException.Create('Not connected');

  ADatagram.Header.SessionID := FSessionID;
  Result := S.SendRequest(ADatagram);
end;

function TevBBTCPClient.Session: IevSession;
begin
  Result := FindSession(FSessionID);
end;

{ TBBDatagramDispatcher }

function TBBDatagramDispatcher.BBController: IevBBController;
begin
  Result := Mainboard.BBClient as IevBBController;
end;

procedure TBBDatagramDispatcher.dmGetReportResults(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res : IisListofValues;
begin
  Res := BBController.GetReportResults(ADatagram.Params.Value['SerialNumber'],
                                       ADatagram.Params.Value['AReportNumber']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TBBDatagramDispatcher.dmGetSbInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res : IisListofValues;
begin
  Res := BBController.GetSbInfo(ADatagram.Params.Value['SerialNumber']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TBBDatagramDispatcher.dmRunReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res : IisListofValues;
begin
  Res := BBController.RunReport(ADatagram.Params.Value['SerialNumber'], ADatagram.Params.Value['AReportNumber'], IInterface(ADatagram.Params.Value['AReportParams']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TBBDatagramDispatcher.dmSetNewLicense(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  BBController.SetNewLicense(ADatagram.Params.Value['SerialNumber'], ADatagram.Params.Value['ALicense']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TBBDatagramDispatcher.dmUpdateSystemReport(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  BBController.UpdateSystemReport(ADatagram.Params.Value['SerialNumber'], ADatagram.Params.Value['AReportNumber'], IInterface(ADatagram.Params.Value['AReportData']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TBBDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(Method_RB_RunReport, dmRunReport);
  AddHandler(Method_RB_GetSbInfo, dmGetSbInfo);
  AddHandler(Method_RB_UpdateSystemReport, dmUpdateSystemReport);
  AddHandler(Method_RB_SetNewLicense, dmSetNewLicense);
  AddHandler(Method_RB_GetReportResults, dmGetReportResults);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetBBClient, IevBBClient, 'BB Client');

end.

