unit EvEMailerPxy;

interface

uses
  SysUtils, isBaseClasses, isSettings, EvSettings, EvCommonInterfaces, EvMainboard,
  evConsts, EvTypes, isBasicUtils, EvStreamUtils, EvProxy, evRemoteMethods, EvContext,
  evTransportInterfaces, EvClasses, isAppIDs;

implementation

type
  TevEMailerProxy = class(TevProxyModule, IevEMailer)
  private
    FSmtpServer: String;
    FSmtpPort: Integer;
    FUserName: String;
    FPassword: String;
    procedure  AddSmtpParams(const ADatagram: IevDatagram);
  protected
    function  GetSmtpServer: String;
    procedure SetSmtpServer(const AValue: String);
    function  GetSmtpPort: Integer;
    procedure SetSmtpPort(const AValue: Integer);
    function  GetUserName: String;
    procedure SetUserName(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  CreateMessage: IevMailMessage;
    procedure SendMail(const AMessage: IevMailMessage);
    procedure SendQuickMail(const ATo, AFrom, ASubject, ABody: String);
  end;


function GetEMailerProxy: IevEMailer;
begin
  Result := TevEMailerProxy.Create;
end;


{ TevEMailerProxy }

procedure TevEMailerProxy.AddSmtpParams(const ADatagram: IevDatagram);
begin
  if FSmtpServer <> '' then
  begin
    ADatagram.Params.AddValue('Host', FSmtpServer);
    ADatagram.Params.AddValue('Port', FSmtpPort);
    ADatagram.Params.AddValue('User', FUserName);
    ADatagram.Params.AddValue('Password', FPassword);
  end;  
end;

function TevEMailerProxy.CreateMessage: IevMailMessage;
begin
  Result := TevMailMessage.Create;
  Result.AddressFrom := Context.UserAccount.EMail;
end;

function TevEMailerProxy.GetPassword: String;
begin
  Result := FPassword;
end;

function TevEMailerProxy.GetSmtpPort: Integer;
begin
  Result := FSmtpPort;
end;

function TevEMailerProxy.GetSmtpServer: String;
begin
  Result := FSmtpServer;
end;

function TevEMailerProxy.GetUserName: String;
begin
  Result := FUserName;
end;

procedure TevEMailerProxy.SendMail(const AMessage: IevMailMessage);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_EMAILER_SENDMAIL);
  D.Params.AddValue('AMessage', AMessage);
  AddSmtpParams(D);  
  D := SendRequest(D);
end;

procedure TevEMailerProxy.SendQuickMail(const ATo, AFrom, ASubject, ABody: String);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_EMAILER_SENDQUICKMAIL);
  D.Params.AddValue('ATo', ATo);
  D.Params.AddValue('AFrom', AFrom);
  D.Params.AddValue('ASubject', ASubject);
  D.Params.AddValue('ABody', ABody);
  AddSmtpParams(D);  
  D := SendRequest(D);
end;

procedure TevEMailerProxy.SetPassword(const AValue: String);
begin
  FPassword := AValue;
end;

procedure TevEMailerProxy.SetSmtpPort(const AValue: Integer);
begin
  FSmtpPort := AValue;
end;

procedure TevEMailerProxy.SetSmtpServer(const AValue: String);
begin
  FSmtpServer := AValue;
end;

procedure TevEMailerProxy.SetUserName(const AValue: String);
begin
  FUserName := AValue;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEMailerProxy, IevEMailer, 'EMail Module');


end.
