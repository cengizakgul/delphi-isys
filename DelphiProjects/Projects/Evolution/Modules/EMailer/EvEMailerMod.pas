unit EvEMailerMod;

interface

uses Classes, isBaseClasses, EvCommonInterfaces, EvMainboard, IsSMTPClient, EvStreamUtils, SysUtils,
     EvContext, isBasicUtils, evClasses;

implementation


type
  TevEMailerObject = class(TisInterfacedObject, IevEMailer)
  private
    FSmtpServer: String;
    FSmtpPort: Integer;
    FUserName: String;
    FPassword: String;
    function  FixSenderAddress(const AAddress: String): String;
  protected
    procedure DoOnConstruction; override;
    function  GetSmtpServer: String;
    procedure SetSmtpServer(const Value: String);
    function  GetSmtpPort: Integer;
    procedure SetSmtpPort(const Value: Integer);
    function  GetUserName: String;
    procedure SetUserName(const Value: String);
    function  GetPassword: String;
    procedure SetPassword(const Value: String);
    function  CreateMessage: IevMailMessage;
    procedure SendMail(const AMessage: IevMailMessage);
    procedure SendQuickMail(const ATo, AFrom, ASubject, ABody: String);
  end;

function GetEMailer: IevEMailer;
begin
  Result := TevEMailerObject.Create;
end;

{ TevEMailerObject }

procedure TevEMailerObject.DoOnConstruction;
begin
  inherited;
  FSmtpServer := mb_GlobalSettings.EMailInfo.SMTPServer;
  FSmtpPort := StrToIntDef(mb_GlobalSettings.EmailInfo.SMTPPort, 25);
  FUserName := mb_GlobalSettings.EmailInfo.UserName;
  FPassword := mb_GlobalSettings.EmailInfo.Password;
end;

procedure TevEMailerObject.SendMail(const AMessage: IevMailMessage);
const
  Priorities: array [Low(TisMailPriority)..High(TisMailPriority)] of String = ('Low', 'Normal', 'High');
var
  RcptLst, sFromAddr: string;
  SMTPClient: TIsSmtpClient;
  SMTPHeader: TIsSmtpClientHeader;
  SMTPAttachments : TIsMultiPartMixedAttachments;
  i: Integer;
begin
  ctx_StartWait('Sending email...');
  SMTPClient := TIsSMTPClient.Create(FSmtpServer, FSmtpPort, FUserName, FPassword, atAutoSelect);
  SMTPHeader := TIsSmtpClientHeader.Create;
  SMTPAttachments := nil;
  try
    RcptLst := AMessage.AddressTo;
    if AMessage.AddressCC <> '' then
      AddStrValue(RcptLst, AMessage.AddressCC, ';');
    if AMessage.AddressBCC <> '' then
      AddStrValue(RcptLst, AMessage.AddressBCC, ';');
    RcptLst := StringReplace(RcptLst, ',', ';', [rfReplaceAll]);
    sFromAddr := FixSenderAddress(AMessage.AddressFrom);

    SMTPHeader.From := sFromAddr;
    SMTPHeader.SendTo := AMessage.AddressTo;
    SMTPHeader.cc := AMessage.AddressCC;
    SMTPHeader.Subject := AMessage.Subject;
    if AMessage.Confirmation then
      SMTPHeader.DispositionNotificationTo := sFromAddr;
    SMTPHeader.CustomHeaders.Add('X-MSMail-Priority: ' + Priorities[AMessage.Priority]);

    if AMessage.Attachments.Count > 0 then
    begin
      SMTPAttachments := TIsMultiPartMixedAttachments.Create;
      for i := 0 to AMessage.Attachments.Count - 1 do
        SMTPAttachments.Add.AttachStream(AMessage.Attachments[i].Name,
                                         IInterface(AMessage.Attachments[i].Value) as IisStream);

    end;

    SMTPClient.SendMailMultipartMixed(
         sFromAddr,
         RcptLst,
         SMTPHeader,
         AMessage.Body,
         'text/plain',
         SMTPAttachments);
         
  finally
    SMTPAttachments.Free;
    SMTPHeader.Free;
    SMTPClient.Free;
    ctx_EndWait;
  end;
end;

function TevEMailerObject.GetSmtpPort: Integer;
begin
  Result := FSmtpPort;
end;

function TevEMailerObject.GetSmtpServer: String;
begin
  Result := FSmtpServer;
end;

function TevEMailerObject.GetUserName: String;
begin
  Result := FUserName;
end;

procedure TevEMailerObject.SetPassword(const Value: String);
begin
  FPassword := Value;
end;

procedure TevEMailerObject.SetSmtpPort(const Value: Integer);
begin
  FSmtpPort := Value;
end;

procedure TevEMailerObject.SetSmtpServer(const Value: String);
begin
  FSmtpServer := Value;
end;

procedure TevEMailerObject.SetUserName(const Value: String);
begin
  FUserName := Value;
end;

procedure TevEMailerObject.SendQuickMail(const ATo, AFrom, ASubject, ABody: String);
var
  Msg: IevMailMessage;
begin
  Msg := CreateMessage;
  Msg.AddressFrom := AFrom;
  Msg.AddressTo := ATo;
  Msg.Subject := ASubject;
  Msg.Body := ABody;

  SendMail(Msg);
end;

function TevEMailerObject.CreateMessage: IevMailMessage;
begin
  Result := TevMailMessage.Create;
  Result.AddressFrom := Context.UserAccount.EMail;
end;

function TevEMailerObject.GetPassword: String;
begin
  Result := FPassword;
end;

function TevEMailerObject.FixSenderAddress(const AAddress: String): String;
var
  sFrom, sFromName: String;
begin
  sFrom := AAddress;
  sFromName := '';
  if Contains(sFrom, '<') then
  begin
    sFromName := Trim(GetNextStrValue(sFrom, '<'));
    sFrom := Trim(GetNextStrValue(sFrom, '>'));
  end;

  if not Contains(sFrom, '@') then
    sFrom := sFrom + '@' + mb_GlobalSettings.EMailInfo.NotificationDomainName;

  Result := sFromName + '<' + sFrom + '>';
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEMailer, IevEMailer, 'EMail Module');

end.
