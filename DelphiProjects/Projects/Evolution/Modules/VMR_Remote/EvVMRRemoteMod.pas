unit EvVMRRemoteMod;

interface

uses Windows, WinSpool, SyncObjs, Classes, Variants, isBaseClasses, EvCommonInterfaces, EvContext, EvMainboard,
     EvStreamUtils, isBasicUtils, EvTypes, Math, SReportSettings,sPickupSheetsXMLData,XMLDoc,XMLIntf,
     ActiveX, EvExceptions, EvClasses, SVMRUtils, EvDataAccessComponents, EvClientDataSet;

implementation

uses SysUtils, isDataSet, EvDataset, DB,  EvUtils, SDataStructure, EvBasicUtils,
     SVmrClasses,rwPreviewContainerFrm, evConsts, SFieldCodeValues, SLogCheckReprint, isLogFile, StrUtils;

type
  TevVmrRemote = class(TisInterfacedObject, IevVmrRemote)
  private
    FVMRLogFile: IevLogFile;
    function  AddVMRPath(const AFileName: String): String;
    function  CheckInDelimitedList(const SubStr, Str: string; const Delim: Char = ';'): LongInt;
    function  EncodePrinterName(const pr:string):string;
    function  DecodePrinterName(const pr:string):string;
    procedure ADDVMRLogFileEvent(const AEventType: TLogEventType; const AEventClass, AText, ADetails: String);
    procedure AddSBAvailablePrinters( var PrList: IevVMRPrintersList);
    procedure AddNewRemoteLocationName;
  protected
    function  StoreFile(const FileName: string; const Content: IisStream; const AutoRename: Boolean): string;
    function  RetrieveFile(const FileName: string): IisStream;
    function  FileDoesExist(const FileName: string): Boolean;
    procedure DeleteFile(const FileName: string);
    procedure DeleteDirectory(const DirName: string);
    function  CheckScannedBarcode(const BarCode: string; out ExpectedBarcode, LastScannedBarcode: string): Boolean;
    procedure RemoveScannedBarcode(const BarCode: string);
    procedure RemoveScannedBarcodes(const MailBoxBarCode: string);
    function  PutInBoxes(const BarCode, ToBarCode: string): string;
    function  FindInBoxes(const BarCode: string): string;
    procedure RemoveFromBoxes(const BarCode: string);
    function  RetrievePickupSheetsXMLData: IisStream;
    function  GetSBPrinterList : IevVMRPrintersList;
    function  GetPrinterList(const ALocation:string): IevVMRPrintersList;
    procedure SetPrinterList(const ALocation : string; const APrinterList: IevVMRPrintersList; const IsDelete: Boolean);
    procedure PrintRwResults(const ARepResults: TrwReportResults);

    procedure ProcessRelease(const TopBoxNbr: Integer; const ReleaseType:TVmrReleaseTypeSet; out UnprintedReports: TrwReportResults);
    procedure ProcessRevert(const TopBoxNbr: Integer);
    //API
    function  GetVMRReportList(const pCLNbr, pCONbr:Integer; const  pCheckFromDate, pCheckToDate:TDateTime; const pVMRReportType : Integer): IevDataset;
    function  GetVMRReport(const aSelectedReportNbrs: IisStringList; const IsPrint : Boolean; const sComment : string): IisStream;
    procedure PurgeMailBox(const BoxNbr: Integer; const ForceDelete : Boolean);
    procedure PurgeClientMailBox(const ClientNbr: Integer; const ForceDelete : Boolean);
    procedure PurgeMailBoxContentList(const SBMailBoxContentNbrs: IisStringList);
    function GetRemoteVMRReportList (const ALocation:string): IevDataset;
    function GetRemoteVMRReport(const SBMailBoxContentNbr: integer): IisStream;
  end;


var FVmrBoxAccessCS: TCriticalSection;


function GetVmrRemote: IevVmrRemote;
begin
  if Context.License.VMR then
    Result := TevVmrRemote.Create
  else
    Result := nil;
end;

{ TevVmrRemote }

function TevVmrRemote.AddVMRPath(const AFileName: String): String;
var
  s: String;
begin
  // delete drive info  "C:\"
  s := AFileName;
  if Contains(s, ':') then
    GetNextStrValue(s, ':');
  if StartsWith(s, '\') then
    Delete(s, 1, 1);

  Result := NormalizePath(ctx_DomainInfo.FileFolders.VMRFolder) + s;
end;

function TevVmrRemote.CheckInDelimitedList(const SubStr, Str: string; const Delim: Char): LongInt;
begin
  Result := Pos(Delim + SubStr + Delim, Delim + Str + Delim);
end;

function TevVmrRemote.CheckScannedBarcode(const BarCode: string; out ExpectedBarcode, LastScannedBarcode: string): Boolean;
var
  s: string;
  l: IisStringListRO;
  i: Integer;
  p:integer;
begin
  Assert(BarCode <> '');
  Result := False;
  FVmrBoxAccessCS.Enter;
  try
    l := mb_AppSettings.GetValueNames('PrinterQueue');
    try
      for i := 0 to l.Count-1 do
      begin
        s := mb_AppSettings['PrinterQueue\' + l[i]];
        p := Pos(';'+ BarCode+ ';', ';'+ s+ ';');
        if p > 0 then
        begin
            Result := True;
            mb_AppSettings['PrinterLastScanned\' + l[i]] := BarCode;
            Break;
        end
        else if p =  0 then
         //
        else
        begin
          ExpectedBarcode := GetNextStrValue(s, ';');
          LastScannedBarcode := mb_AppSettings['PrinterLastScanned\' + l[i]];
        end;
      end;
    finally
      //l.Free;
    end;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;

procedure TevVmrRemote.DeleteFile(const FileName: string);
begin
  SysUtils.DeleteFile(AddVMRPath(FileName));
end;

function TevVmrRemote.FileDoesExist(const FileName: string): Boolean;
begin
  Result :=FileExists(AddVMRPath(FileName));
end;

procedure AddUserSidePrinter( var PrList : IevVMRPrintersList);
var
  PrinterInfo : IevVMRPrinterInfo;
  PrinterBinInfo : IevVMRPrinterBinInfo;
  MediaList  : IisStringList;
  SbPaperlist : TStringList;
  i, j : integer;
  s, sMediaTypes : string;
begin
   PrinterInfo := PrList.Add;
   PrinterInfo.Name := VMR_SB_SIDE_PRINTER_NAME + IntTostr(PrList.count-1)+'^'+VMR_USER_SIDE_PRINTER_NAME;
   PrinterInfo.Location := '';

   s := MediaType_ComboChoices;
   sMediaTypes := '';
   while s <> '' do
   begin
     GetNextStrValue(s, #9);
     AddStrValue(sMediaTypes, GetNextStrValue(s, #13), #13);
   end;
   MediaList := TisStringList.Create;
   MediaList.CommaText := sMediaTypes;

   DM_SERVICE_BUREAU.SB_PAPER_INFO.DataRequired('');
   SbPaperlist := DM_SERVICE_BUREAU.SB_PAPER_INFO.GetFieldValueList('SB_PAPER_INFO_NBR',True);
   try
     if MediaList.Count > SbPaperlist.Count then
     begin
       j := 0;
       for i := 0 to MediaList.count -1 do
       begin
         PrinterBinInfo := PrinterInfo.AddBin;
         PrinterBinInfo.Name := 'Tray' + IntToStr(i);
         PrinterBinInfo.MediaType  := MediaList[i];
         if ( j mod SbPaperlist.count ) = 0 then j := 0;
         PrinterBinInfo.SbPaperNbr := StrToInt(SbPaperlist[j]);
         inc(j);
       end;
     end
     else begin
       j := 0;
       for i := 0 to SbPaperlist.count -1 do
       begin
         PrinterBinInfo := PrinterInfo.AddBin;
         PrinterBinInfo.Name := 'Tray' + IntToStr(i);
         PrinterBinInfo.SbPaperNbr := StrToInt(SbPaperlist[i]);
         if ( j mod MediaList.count ) = 0 then j := 0;
         PrinterBinInfo.MediaType  := MediaList[j];
         inc(j);
       end;
     end;
   finally
     FreeAndNil(SbPaperlist);
   end;
end;

function TevVmrRemote.GetSBPrinterList : IevVMRPrintersList;
begin
  Result := TevVMRPrintersList.Create;
  AddSBAvailablePrinters(Result);
  AddUserSidePrinter(Result);
end;

procedure TevVmrRemote.AddNewRemoteLocationName;
var
 Options: IevDataSet;
 dsSbOptions: TevClientDataSet;
 Newlist, existList : IisStringList;
 i: integer;
 cnt: integer;
begin
 if CheckRemoteVMRActive then
 begin
  Options := GetSBOption('@%|0LOCATION');
  Newlist := Options.GetColumnValues('OPTION_STRING_VALUE');
  existList := GetRemotePrinterList;
  cnt := existList.count;
  for i := 0 to Newlist.count-1 do
  begin
    if existList.IndexOf(Newlist[i]) < 0 then
      existList.CommaText := existList.CommaText + ifthen(existList.count=0,'',',')+Newlist[i];
  end;
  if cnt = existList.count then exit;

  dsSbOptions := TevClientDataSet.Create(nil);
  try
    dsSbOptions.CheckDSCondition(' OPTION_tag = ''' + VmrRemoteLocation + '''');
    dsSbOptions.ProviderName := DM_SERVICE_BUREAU.SB_OPTION.ProviderName;
    ctx_DataAccess.OpenDataSets([dsSbOptions]);
    dsSbOptions.Edit;
    dsSbOptions['OPTION_STRING_VALUE'] := existList.Commatext ;
    dsSbOptions.Post;
    ctx_DataAccess.PostDataSets([dsSbOptions]);
  finally
    dsSbOptions.Free;
  end;
 end;
end;

function TevVmrRemote.GetPrinterList( const ALocation:string): IevVMRPrintersList;
  procedure LoadPrinterInfoFromDB (aLoc:string);
  var
    j, k: Integer;
    dsPapers: IevDataSet;
    dsSbOptions: IevDataSet;
    PrinterInfo: IevVMRPrinterInfo;
    PrinterBinInfo: IevVMRPrinterBinInfo;
    MaxPrinterNbr, MaxBinNbr: Integer;
  begin
    //aLoc : '', here, there - Sb and User Printer
    //aLoc : @NEWYORK, @LAX - Remote Printer Relay
    if (Trim(aLoc) = '') or ((Length(aLoc)>0) and (aLoc[1]<>'@'))then
      aLoc := VMR_SB_SIDE_PRINTER_NAME;

    if ((Length(aLoc)>0) and (aLoc[1]='@'))then
      aLoc := aLoc+'|';

    dsPapers := GetSbPaperInfo;
    if not assigned(dsPapers) then exit;
    dsPapers.IndexFieldNames := 'SB_PAPER_INFO_NBR';

    dsSbOptions := GetSbOption(aLoc);
    if not assigned(dsSbOptions) then exit;
    dsSbOptions.IndexFieldNames := 'OPTION_TAG';

    MaxPrinterNbr := GetPrinterCount(ALoc);

    for j := 0 to MaxPrinterNbr - 1 do
    begin
      if dsSbOptions.FindKey([aLoc+ IntToStr(j)]) then
      begin
        PrinterInfo := Result.add;

        if aLocation = 'ALL' then // for Printer Set Up Scrreen - not to get duplicated
          PrinterInfo.Name :=  aLoc+IntToStr(j)+'^'+dsSbOptions['OPTION_STRING_VALUE']
        else
          PrinterInfo.Name :=  dsSbOptions['OPTION_STRING_VALUE'];

        PrinterInfo.Active := dsSbOptions['OPTION_INTEGER_VALUE'] <> 0;

        if dsSbOptions.FindKey([aLoc+ IntToStr(j)+ 'LOCATION']) then
          PrinterInfo.Location := ConvertNull(dsSbOptions['OPTION_STRING_VALUE'], '')
        else
          PrinterInfo.Location := '';

        if dsSbOptions.FindKey([aLoc+ IntToStr(j)+ 'HOFFSET']) then
          PrinterInfo.HOffset := ConvertNull(dsSbOptions['OPTION_INTEGER_VALUE'], 0)
        else
          PrinterInfo.HOffset := 0;

        if dsSbOptions.FindKey([aLoc+ IntToStr(j)+ 'VOFFSET']) then
          PrinterInfo.VOffset := ConvertNull(dsSbOptions['OPTION_INTEGER_VALUE'], 0)
        else
          PrinterInfo.VOffset := 0;

        MaxBinNbr := GetTrayCount(aLoc + IntToStr(j)+ 'TRAY%');

        k:= 0;
        While k < MaxBinNbr do
        begin
          if dsSbOptions.FindKey([aLoc+ IntToStr(j)+ 'TRAY'+ IntToStr(k)]) then
          begin
            PrinterBinInfo := PrinterInfo.AddBin;
            PrinterBinInfo.Name := ConvertNull(dsSbOptions['OPTION_STRING_VALUE'], '');

            if dsPapers.FindKey([dsSbOptions['OPTION_INTEGER_VALUE']]) then
            begin
              PrinterBinInfo.SbPaperNbr := dsPapers['SB_PAPER_INFO_NBR'];
              PrinterBinInfo.PaperWeight := ConvertNull(dsPapers['WEIGHT'], 0);
              PrinterBinInfo.MediaType := VarToStrDef(dsPapers['MEDIA_TYPE'], MEDIA_TYPE_PLAIN_LETTER);
            end;
          end;
          inc(k);
        end;
      end;
    end;
  end;

  procedure LoadPaperInfo_NONSAAS;
  var
    i, j, k: Integer;
    dsPapers: TevClientDataSet;
    dsSbOptions: TevClientDataSet;
    PrinterInfo: IevVMRPrinterInfo;
  begin
    dsPapers :=DM_SERVICE_BUREAU.SB_PAPER_INFO;
    dsSbOptions := TevClientDataSet.Create(nil);
    try
      DM_SERVICE_BUREAU.SB_PAPER_INFO.DataRequired('');
      dsPapers.IndexFieldNames := 'SB_PAPER_INFO_NBR';

      dsSbOptions.Close;
      dsSbOptions.CheckDSCondition('OPTION_TAG like (''PRINTER%'')');
      dsSbOptions.ProviderName := DM_SERVICE_BUREAU.SB_OPTION.ProviderName;
      dsSbOptions.IndexFieldNames := 'OPTION_TAG';
      ctx_DataAccess.OpenDataSets([dsSbOptions]);

      dsSbOptions.First;

      for i := 0 to Result.Count - 1 do
      begin
       if dsSbOptions.FindKey(['PRINTER'+ IntToStr(i)]) then
       begin
          for j := 0 to Result.count - 1 do
          begin
              PrinterInfo := Result[j];
              if (dsSbOptions['OPTION_STRING_VALUE'] = PrinterInfo.Name) then
              begin

                PrinterInfo.Active := dsSbOptions['OPTION_INTEGER_VALUE'] <> 0;
                if PrinterInfo.Name <> VMR_USER_SIDE_PRINTER_NAME then
                begin
                  if dsSbOptions.FindKey(['PRINTER'+ IntToStr(i)+ 'LOCATION']) then
                    PrinterInfo.Location := ConvertNull(dsSbOptions['OPTION_STRING_VALUE'], '')
                  else
                    PrinterInfo.Location := '';

                  if dsSbOptions.FindKey(['PRINTER'+ IntToStr(i)+ 'HOFFSET']) then
                    PrinterInfo.HOffset := ConvertNull(dsSbOptions['OPTION_INTEGER_VALUE'], 0)
                  else
                    PrinterInfo.HOffset := 0;

                  if dsSbOptions.FindKey(['PRINTER'+ IntToStr(i)+ 'VOFFSET']) then
                    PrinterInfo.VOffset := ConvertNull(dsSbOptions['OPTION_INTEGER_VALUE'], 0)
                  else
                    PrinterInfo.VOffset := 0;

                  for k := 0 to PrinterInfo.BinCount - 1 do
                    if PrinterInfo.Bins[k].Name <> '' then
                      if dsSbOptions.FindKey(['PRINTER'+ IntToStr(i)+ 'TRAY'+ IntToStr(k)]) then
                        if dsPapers.FindKey([dsSbOptions['OPTION_INTEGER_VALUE']]) then
                        begin
                          PrinterInfo.Bins[k].SbPaperNbr := dsPapers['SB_PAPER_INFO_NBR'];
                          PrinterInfo.Bins[k].PaperWeight := ConvertNull(dsPapers['WEIGHT'], 0);
                          PrinterInfo.Bins[k].MediaType := VarToStrDef(dsPapers['MEDIA_TYPE'], MEDIA_TYPE_PLAIN_LETTER);
                        end;



                  Break;
                end;
            end;
          end;
        end;
      end;
    finally
      dsSbOptions.Free;
    end;
  end;


  procedure LoadAvailablePrinters_NONSAAS;
  type
     TBinName      = array [0..23] of Char;
     TBinNameArray = array of TBinName;
     TBinArray     = array of Word;
  var
    numBinNames, numBins: Integer;
    aBinNames: TBinnameArray;
    aBins: TBinArray;
    j, k: Integer;
    PrinterInfo: IevVMRPrinterInfo;
  begin
    Mainboard.MachineInfo.Printers.Lock;
    try
      Mainboard.MachineInfo.Printers.Load;

      for j := 0 to Mainboard.MachineInfo.Printers.Count- 1 do
      begin
        numBinNames := WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINNAMES, nil, nil);
        numBins     := WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINS, nil, nil);
        if numBins <> numBinNames then
          raise Exception.Create('DeviceCapabilities reports different number of bins and bin names!');

        if numBinNames > 0 then
        begin
          PrinterInfo := Result.Add;
          PrinterInfo.Name := Mainboard.MachineInfo.Printers[j].Name;

          SetLength(aBinNames, numBinNames);
          SetLength(aBins, numBins);
          try
            WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINNAMES, PChar(aBinNames), nil);
            WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINS, PChar(aBins), nil);
            for k := 0 to numBinNames - 1 do
              if not Contains(aBinNames[k], 'AUTO') and not Contains(aBinNames[k], 'MANUAL') then
                PrinterInfo.AddBin.Name := aBinNames[k];
          finally
            SetLength(aBinNames, 0);
            SetLength(aBins, 0);
          end;
        end;
      end;
    finally
      Mainboard.MachineInfo.Printers.Unlock;
    end;
  end;


  procedure AddUserSidePrinter_NONSAAS;
  var
    PrinterInfo : IevVMRPrinterInfo;
    PrinterBinInfo : IevVMRPrinterBinInfo;
    MediaList  : IisStringList;
    SbPaperlist : TStringList;
    i, j : integer;
    s, sMediaTypes : string;
  begin
     PrinterInfo := Result.Add;
     PrinterInfo.Name := VMR_USER_SIDE_PRINTER_NAME;
     PrinterInfo.Location := '';

     s := MediaType_ComboChoices;
     sMediaTypes := '';
     while s <> '' do
     begin
       GetNextStrValue(s, #9);
       AddStrValue(sMediaTypes, GetNextStrValue(s, #13), #13);
     end;
     MediaList := TisStringList.Create;
     MediaList.CommaText := sMediaTypes;

     DM_SERVICE_BUREAU.SB_PAPER_INFO.DataRequired('');
     SbPaperlist := DM_SERVICE_BUREAU.SB_PAPER_INFO.GetFieldValueList('SB_PAPER_INFO_NBR',True);
     try
       if MediaList.Count > SbPaperlist.Count then
       begin
         j := 0;
         for i := 0 to MediaList.count -1 do
         begin
           PrinterBinInfo := PrinterInfo.AddBin;
           PrinterBinInfo.Name := 'Tray' + IntToStr(i);
           PrinterBinInfo.MediaType  := MediaList[i];
           if ( j mod SbPaperlist.count ) = 0 then j := 0;
           PrinterBinInfo.SbPaperNbr := StrToInt(SbPaperlist[j]);
           inc(j);
         end;
       end
       else begin
         j := 0;
         for i := 0 to SbPaperlist.count -1 do
         begin
           PrinterBinInfo := PrinterInfo.AddBin;
           PrinterBinInfo.Name := 'Tray' + IntToStr(i);
           PrinterBinInfo.SbPaperNbr := StrToInt(SbPaperlist[i]);
           if ( j mod MediaList.count ) = 0 then j := 0;
           PrinterBinInfo.MediaType  := MediaList[j];
           inc(j);
         end;
       end;
     finally
       FreeAndNil(SbPaperlist);
     end;
  end;

var
  i: integer;
  alist: IisStringList;
begin
  Result := TevVMRPrintersList.Create;
  if aLocation = 'NON$AA$' then
  begin
    LoadAvailablePrinters_NONSAAS;
    AddUserSidePrinter_NONSAAS;
    LoadPaperInfo_NONSAAS;
    SetPrinterList(aLocation, Result, True);
  end
  else begin
    if aLocation = 'ALL' then
    begin
      LoadPrinterInfoFromDB(VMR_SB_SIDE_PRINTER_NAME);
      //EvoRemoteRelayPrinter User try to update printer list without resgistering remote location name
      //We forcefully register new location name
      AddNewRemoteLocationName;
      alist:= GetRemotePrinterList;
      for i:= 0 to aList.count -1 do
          LoadPrinterInfoFromDB(aList[i]);
    end
    else
      LoadPrinterInfoFromDB(Uppercase(aLocation));
  end;
end;

procedure TevVmrRemote.PrintRwResults(const ARepResults: TrwReportResults);
var
  s: String;
begin
  ctx_RWLocalEngine.Print(ARepResults);

  FVmrBoxAccessCS.Enter;
  try
    s := mb_AppSettings['PrinterQueue\' + EncodePrinterName(ARepResults.PrinterName)];
    if s = '' then
      s := ARepResults.Descr
    else
      s := s+ ';'+ ARepResults.Descr;
    mb_AppSettings['PrinterQueue\' + EncodePrinterName(ARepResults.PrinterName)] := s;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;

function TevVmrRemote.FindInBoxes(const BarCode: string): string;
var
  i: Integer;
  l:IisStringListRO;
  m: IisStringList;
begin
  Result := '';
  Assert(BarCode <> '');
  FVmrBoxAccessCS.Enter;
  try
    m := TisStringList.Create;
    l := mb_AppSettings.GetValueNames('VMR Boxes');
    for i := 0 to l.Count-1 do
    begin
      m.CommaText :=StringReplace( mb_AppSettings['VMR Boxes\' + l[i]], ';', ',', [rfReplaceAll]);
      if m.IndexOf(BarCode) <> -1 then
//      if CheckInDelimitedList(BarCode, mb_AppSettings['VMR Boxes\' + l[i]]) <> 0 then
      begin
        Result := 'BOX # '+ l[i];
        Break;
      end;
    end;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;


function TevVmrRemote.PutInBoxes(const BarCode, ToBarCode: string): string;
var
  i: Integer;
  s: string;
begin
  Assert(BarCode <> '');
  FVmrBoxAccessCS.Enter;
  Result := '';
  try
    if ToBarCode <> '' then
      for i := 1 to 1000 do
      begin
        s := mb_AppSettings['VMR Boxes\' + IntToStr(i)];
        if CheckInDelimitedList(ToBarCode, s) <> 0 then
        begin
          Result := 'BOX # '+ IntToStr(i);
          s := s+ ';'+ BarCode;
          mb_AppSettings['VMR Boxes\' + IntToStr(i)] := s;
          Break;
        end;
      end
    else
      for i := 1 to 1000 do
        if mb_AppSettings.AsString['VMR Boxes\' + IntToStr(i)] = '' then
        begin
          mb_AppSettings['VMR Boxes\' + IntToStr(i)] := BarCode;
          Result := 'BOX # '+ IntToStr(i);
          Break;
        end;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;

procedure TevVmrRemote.RemoveFromBoxes(const BarCode: string);
var
  i,j: Integer;
  s1: string;
  m: IisStringList;
  l:IisStringListRO;
  BC,BM:string;
begin
  Assert(BarCode <> '');
  FVmrBoxAccessCS.Enter;
  m := TisStringList.Create;
  l := mb_AppSettings.GetValueNames('VMR Boxes');
  if (Copy(BarCode,Length(BarCode),Length(BarCode))='*')  then
  begin
    BM := '*';
    BC   := Copy(BarCode, 1, Length(BarCode)-1)
  end else
  begin
    BM := '';
    BC := BarCode;
  end;
  try
    for i := 0 to l.Count-1 do
    begin
      s1 := mb_AppSettings['VMR Boxes\' + l[i]];
      m.CommaText := StringReplace(s1, ';', ',', [rfReplaceAll]);
      for j := m.Count-1 downto 0 do
      begin
          if (BM='*') then
          begin
            if (Copy(m[j], 1, Length(m[j])-4) = BC) then
              m.Delete(j);
          end else
            if m[j] = BC then
              m.Delete(j);
      end;
      mb_AppSettings['VMR Boxes\' + l[i]] := StringReplace(m.CommaText, ',', ';', [rfReplaceAll]);
      if m.Count = 0 then
           mb_AppSettings.DeleteValue('VMR Boxes\'+l[i]);

    end;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;

procedure TevVmrRemote.RemoveScannedBarcode(const BarCode: string);
var
  s: string;
  l: IisStringListRO;
  i: Integer;
begin
  Assert(BarCode <> '');
  FVmrBoxAccessCS.Enter;
  try
    l := mb_AppSettings.GetValueNames('PrinterQueue');
    for i := 0 to l.Count - 1 do
    begin
      s := mb_AppSettings['PrinterQueue\' + l[i]];
      s := StringReplace(';'+ s+ ';', ';'+ BarCode+ ';', ';', [rfReplaceAll]);
      Delete(s, 1, 1);
      if Length(s) > 0 then
        Delete(s, Length(s), 1);
      mb_AppSettings['PrinterQueue\' + l[i]] := s;
    end;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;

procedure TevVmrRemote.RemoveScannedBarcodes(const MailBoxBarCode: string);
var
  l: IisStringListRO;
  m: IisStringList;
  i, j: Integer;
begin
  Assert(MailBoxBarCode <> '');
  FVmrBoxAccessCS.Enter;
  try
    m := TisStringList.Create;
    l := mb_AppSettings.GetValueNames('PrinterQueue');
    for i := 0 to l.Count - 1 do
    begin
      m.CommaText := StringReplace(mb_AppSettings['PrinterQueue\' + l[i]], ';', ',', [rfReplaceAll]);
      for j := m.Count - 1 downto 0 do
        if Copy(m[j], 1, Length(m[j])-4) = MailBoxBarCode then
          m.Delete(j);
      mb_AppSettings['PrinterQueue\' + l[i]] := StringReplace(m.CommaText, ',', ';', [rfReplaceAll]);
    end;
  finally
    FVmrBoxAccessCS.Leave;
  end;
end;

function TevVmrRemote.RetrieveFile(const FileName: string): IisStream;
begin
  Result := TEvDualStreamHolder.CreateFromFile(AddVMRPath(FileName));
end;

function TevVmrRemote.RetrievePickupSheetsXMLData: IisStream;
var
  l: IisStringListRO;
  i: Integer;
  xml :IXMLPickupDataType;
  pq:IXMLPrinterQueueType;
  pqp:IXMLPrinterType;
  vbs:IXMLVMRBoxesType;
  vb:IXMLBOXType;
begin
  FVmrBoxAccessCS.Enter;
  try
    //if GetCurrentThreadId <> MainThreadID then
    CoInitialize(nil);
    Result := TisStream.Create;
    xml := NewPickupData;
    pq := xml.PrinterQueue;

    l := mb_AppSettings.GetValueNames('PrinterQueue');
    try
      for i := 0 to l.Count-1 do
      begin
        pqp := pq.Add;
        pqp.Name := DecodePrinterName(l[i]);
        pqp.Data := mb_AppSettings['PrinterQueue\' + l[i]];
        pqp.LastScanned := mb_AppSettings['PrinterLastScanned\' + l[i]];
      end;
    finally

    end;
    l := mb_AppSettings.GetValueNames('VMR Boxes');
    try
      vbs := xml.VMRBoxes;
      for i := 0 to l.Count-1 do
      begin
        vb := vbs.Add;
        vb.Name := l[i];
        vb.Data := mb_AppSettings['VMR Boxes\' + l[i]];
      end;
    finally

    end;
    xml.OwnerDocument.SaveToStream(Result.RealStream);
  finally
    CoUninitialize;
    FVmrBoxAccessCS.Leave;
  end;
end;

function TevVmrRemote.StoreFile(const FileName: string; const Content: IisStream; const AutoRename: Boolean): string;
var
  fs: TEvUniqueFileStream;
begin
  Result := AddVMRPath(FileName);

  fs := TEvUniqueFileStream.CreateEx(Result, AutoRename, False);
  try
    try
      Content.Position := 0;
      fs.CopyFrom(Content.RealStream, 0);
    except
      DeleteFile(FileName);
      raise;
    end;
  finally
    fs.Free;
  end;

  //Result := ExtractFileName(Result);
  Result := Copy(Result,length(NormalizePath(ctx_DomainInfo.FileFolders.VMRFolder)),Length(Result));
end;

procedure TevVmrRemote.ProcessRelease(const TopBoxNbr: Integer; const ReleaseType:TVmrReleaseTypeSet;
  out UnprintedReports: TrwReportResults);
begin
  UnprintedReports := TrwReportResults.Create;
  
  ctx_RWLocalEngine.SetUnprintedReportsStorage(UnprintedReports); // To catch all unprinted reports
  try
    ctx_VmrEngine.ProcessRelease(TopBoxNbr,ReleaseType);
  finally
    ctx_RWLocalEngine.SetUnprintedReportsStorage(nil);
  end;

  if UnprintedReports.Count = 0 then
    FreeAndNil(UnprintedReports);
end;

procedure TevVmrRemote.ProcessRevert(const TopBoxNbr: Integer);
begin
   ctx_VmrEngine.ProcessRevert(TopBoxNbr);
end;

function TevVmrRemote.DecodePrinterName(const pr:string): string;
begin
  Result := StringReplace(pr, '&|;', '\', [rfReplaceAll]);
  Result := StringReplace(Result, '&_;', '/', [rfReplaceAll]);
  Result := StringReplace(Result, '&amp;', '&', [rfReplaceAll]);
end;

function TevVmrRemote.EncodePrinterName(const pr:string): string;
begin
  Result := StringReplace(pr, '&', '&amp;', [rfReplaceAll]);
  Result := StringReplace(Result, '\', '&|;', [rfReplaceAll]);
  Result := StringReplace(Result, '/', '&_;', [rfReplaceAll]);
end;

function TevVmrRemote.GetVMRReportList(const pCLNbr, pCONbr: Integer;
                                       const pCheckFromDate, pCheckToDate : TDateTime;
                                       const pVMRReportType : Integer ): IevDataset;
//pReportType
//VMR_REP_REPORT = 1;
//VMR_REP_PR_CHECK = 2;
//VMR_REP_MISC_CHECK = 3;
//VMR_REP_ASCII = 4;

const
  TReportType_desc : array[0..8] of string = ('Unknown', 'Report', 'PRCheck', 'MISCCheck', 'ASCIIFile', 'TaxReturn', 'TaxCoupon', 'MultiClient', 'HR');
  dsStruct: array [0..9] of TDSFieldDef = (
            (FieldName: 'SBContentNBR';    DataType: ftInteger;    Size: 0;   Required: True),
            (FieldName: 'CheckDate';       DataType: ftDateTime;   Size: 0;   Required: False),
            (FieldName: 'PeriodBeginDate'; DataType: ftDateTime;   Size: 0;   Required: False),
            (FieldName: 'PeriodEndDate';   DataType: ftDateTime;   Size: 0;   Required: False),
            (FieldName: 'RunNo';       DataType: ftInteger;   Size: 0;    Required: False),
            (FieldName: 'Description'; DataType: ftString;    Size: 100;  Required: False),
            (FieldName: 'ReportNo';    DataType: ftString;    Size: 20;   Required: False),
            (FieldName: 'ReportType';  DataType: ftInteger;   Size: 0;    Required: False),
            (FieldName: 'ReportTypeDesc';  DataType: ftString;   Size: 20;    Required: False),
            (FieldName: 'FileExist';       DataType: ftboolean;   Size: 0;    Required: False)
            );
  sSBMailBOX : string = ' Select SB_MAIL_BOX_NBR from SB_MAIL_BOX a '+
                        ' where a.CL_NBR = :CLNBR and {AsOfNow<a>} and a.SB_DELIVERY_METHOD_NBR in (%s) '+
                        ' and a.RELEASED_TIME is not null and a.PRINTED_TIME  is not null ';

  sContent : string = ' Select max(T1.SB_MAIL_BOX_CONTENT_NBR) SBContentNBR , '+
        ' CAST(Trim(StrCopy(T1.DESCRIPTION, 0, 100)) as VARCHAR(100)) JOB_DESCR, '+
        ' CAST(Trim(StrCopy(T1.DESCRIPTION, 100, 100)) as VARCHAR(100)) REPORT_DESCR, '+
        ' CAST(Trim(StrCopy(T1.DESCRIPTION, 200, 8)) as VARCHAR(8)) EVENT_DATE_STR, '+
        ' CAST(Trim(StrCopy(T1.DESCRIPTION, 208, 10)) as INTEGER) CO_NBR, '+
        ' CAST(Trim(StrCopy(T1.DESCRIPTION, 218, 10)) as INTEGER) PR_NBR, '+
        ' O1.OPTION_INTEGER_VALUE REPORT_TYPE, '+
        ' O2.OPTION_STRING_VALUE REPORT_NO,  T1.FILE_NAME '+ //Report_Level + REPORT_WRITER_REPORTS_NBR
        ' from SB_MAIL_BOX_CONTENT T1 '+
        ' join SB_MAIL_BOX T2 on T2.SB_MAIL_BOX_NBR = T1.SB_MAIL_BOX_NBR '+
        ' left outer join SB_MAIL_BOX_OPTION O1 on O1.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and  {AsOfNow<O1>} and O1.OPTION_TAG= ''REPORT_TYPE'' '+
        ' left outer join SB_MAIL_BOX_OPTION O2 on O2.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and  {AsOfNow<O2>} and O2.OPTION_TAG= ''REPORT_NO'' '+
        ' where {AsOfNow<T1>} and {AsOfNow<T2>} '+
        ' and T2.CL_NBR = :CLNBR '+
        ' and %s ' + //' and T2.SB_MAIL_BOX_NBR in (%s) '+
        ' and CAST(Trim(StrCopy(T1.DESCRIPTION, 208, 10)) as INTEGER) = :CONBR ' +
        ' and Trim(StrCopy(T1.DESCRIPTION, 200, 8)) >= :FromDate '+
        ' and Trim(StrCopy(T1.DESCRIPTION, 200, 8)) <= :ToDate '+
        ' and O1.OPTION_INTEGER_VALUE = :VMRREPORTTYPE'+
        ' %s '+
        ' Group by CAST(Trim(StrCopy(T1.DESCRIPTION, 0, 100)) as VARCHAR(100)) , '+
        '          CAST(Trim(StrCopy(T1.DESCRIPTION, 100, 100)) as VARCHAR(100)) , '+
        '          CAST(Trim(StrCopy(T1.DESCRIPTION, 200, 8)) as VARCHAR(8)) , '+
        '          CAST(Trim(StrCopy(T1.DESCRIPTION, 208, 10)) as INTEGER) , '+
        '          CAST(Trim(StrCopy(T1.DESCRIPTION, 218, 10)) as INTEGER) , '+
        '          O1.OPTION_INTEGER_VALUE, O2.OPTION_STRING_VALUE,  T1.FILE_NAME ';

  sPr : string = ' SELECT P.PR_NBR, P.CHECK_DATE, '+
        ' ( SELECT MIN(B.PERIOD_BEGIN_DATE) FROM PR_BATCH B '+
        '   WHERE B.PR_NBR = P.PR_NBR AND {AsOfNow<B>} ) BEGIN_DATE, '+
        ' ( SELECT MAX(B1.PERIOD_END_DATE) FROM PR_BATCH B1 '+
        '   WHERE B1.PR_NBR = P.PR_NBR AND {AsOfNow<B1>}) END_DATE'+
        ' FROM PR P WHERE {AsOfNow<p>} and P.CO_NBR = :CONBR and %s '; //P.PR_NBR IN (%s)

  sEmailVoucher : string =  ' select t1.SB_MAIL_BOX_CONTENT_NBR '+
        ' from SB_MAIL_BOX_CONTENT T1 '+
        ' left outer join SB_MAIL_BOX_OPTION O3 on O3.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and  {AsOfNow<O3>} and O3.OPTION_TAG= ''PASSWORD'' '+
        ' left outer join SB_MAIL_BOX_OPTION O4 on O4.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and  {AsOfNow<O4>} and O4.OPTION_TAG= ''EMAIL'' '+
        ' left outer join SB_MAIL_BOX_OPTION O5 on O5.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and  {AsOfNow<O5>} and O5.OPTION_TAG= ''EE_NBR'' '+
        ' where %s '+ //where t1.SB_MAIL_BOX_NBR IN (%s)
        ' and ( (O3.OPTION_STRING_VALUE is not null) and (trim(O3.OPTION_STRING_VALUE) <> '''') ) '+
        ' and ( (O4.OPTION_STRING_VALUE is not null) and (trim(O4.OPTION_STRING_VALUE) <> '''') ) '+
        ' and ( (o5.OPTION_INTEGER_VALUE is not null) and (o5.OPTION_INTEGER_VALUE > 0) ) ';
var
  aSbnbrlist, aSbDMnbrlist, aPRNbrList : TStringList;
  SBContentsDataSet, SbMailBoxDS,
  EmailVoucherDataSet, SBTaxReturnContentsDataSet : TevClientDataSet;
  Qry : IevQuery;
  s, sPreFilter, sPageCount : string;
  IsFileExist, sPreFiltered : Boolean;
  tmpStream : IEvDualStream;
  vDate : variant;
  RegularChecks,MISCChecks : IisStringList;
  iReportType : integer;

  function GetPRCheckType (aREPORT_NO : string): boolean;
  var aReport_level : string;
  begin
    Result := false;
    aReport_level := copy(aREPORT_NO,1,1);
    Delete(aREPORT_NO,1,1);
    if (aReport_level = 'S') and (RegularChecks.IndexOf(aREPORT_NO) <> -1) then
      Result := true;
  end;

  function GetMISCCheckType (aREPORT_NO : string): boolean;
  var aReport_level : string;
  begin
    Result := false;
    aReport_level := copy(aREPORT_NO,1,1);
    Delete(aREPORT_NO,1,1);
    if (aReport_level = 'S') and (MISCChecks.IndexOf(aREPORT_NO) <> -1) then
      Result := true;
  end;

  procedure CollectTaxReturnReports;
  begin
    TVarData(vDate).VType := varNull;
    with TExecDSWrapper.Create(Format( sContent,
                               [ BuildInSqlStatement('T2.SB_MAIL_BOX_NBR',aSbnbrlist.commatext), ' and T1.DESCRIPTION like ''Tax Returns for%'' ' + sPageCount])) do
    begin
      SetParam('ClNbr', pCLNbr);
      SetParam('CONbr', pCONbr);
      SetParam('FromDate', FormatDateTime('YYYYMMDD', GetEndMonth(pCheckFromDate)));
      SetParam('ToDate', FormatDateTime('YYYYMMDD', pCheckToDate));
      SetParam('VMRREPORTTYPE',4);//Tax Return
      ctx_DataAccess.GetSbCustomData(SBTaxReturnContentsDataSet, AsVariant);
      SBTaxReturnContentsDataSet.IndexFieldNames := 'SBContentNBR';
    end;
    SBTaxReturnContentsDataSet.First;
    while not SBTaxReturnContentsDataSet.eof do
    begin
      IsFileExist := False;
      try
        //if not found file, IsFileExist = False
        tmpStream := RetrieveFile(Trim(SBTaxReturnContentsDataSet.FieldByname('FILE_NAME').asString));
        IsFileExist := True;
      except
        ;
      end;
      Result.AppendRecord([SBTaxReturnContentsDataSet.FieldByName('SBContentNBR').AsInteger,
                           GetEventDate(SBTaxReturnContentsDataSet.FieldByName('EVENT_DATE_STR').asString),
                           vDate,//BEGIN_DATE - not available
                           vDate,//END_DATE - not available
                           '',//PR Run number
                           Trim(SBTaxReturnContentsDataSet.FieldByName('REPORT_DESCR').AsString),
                           Trim(SBTaxReturnContentsDataSet.FieldByName('REPORT_NO').AsString),
                           IntTostr(5),
                           TReportType_desc[5],
                           IsFileExist]);
      SBTaxReturnContentsDataSet.next;
    end;
  end;

  procedure CollectPayrollReports;
  begin
    with TExecDSWrapper.Create(Format( sContent, [BuildInSqlStatement('T2.SB_MAIL_BOX_NBR', aSbnbrlist.commatext), sPageCount])) do
    begin
      SetParam('ClNbr', pCLNbr);
      SetParam('CONbr', pCONbr);
      SetParam('FromDate', FormatDateTime('YYYYMMDD', pCheckFromDate));
      SetParam('ToDate', FormatDateTime('YYYYMMDD', pCheckToDate));
      SetParam('VMRREPORTTYPE',iReportType);
      ctx_DataAccess.GetSbCustomData(SBContentsDataSet, AsVariant);
      SBContentsDataSet.IndexFieldNames := 'EVENT_DATE_STR';
    end;

    with TExecDSWrapper.Create(Format( sEmailVoucher, [BuildINsqlStatement('T1.SB_MAIL_BOX_NBR', aSbnbrlist.commatext)])) do
    begin
      ctx_DataAccess.GetSbCustomData(EmailVoucherDataSet, AsVariant);
      EmailVoucherDataSet.IndexFieldNames := 'SB_MAIL_BOX_CONTENT_NBR';
    end;

    //Not to include email voucher list
    if EmailVoucherDataSet.RecordCount > 0 then
    begin
      SBContentsDataSet.First;
      while not SBContentsDataSet.eof do
      begin
        if EmailVoucherDataSet.Locate('SB_MAIL_BOX_CONTENT_NBR',
                             SBContentsDataSet['SBContentNBR'], [])
        then SBContentsDataSet.Delete
        else SBContentsDataSet.Next;
      end;
    end;

    aPRNbrList := SBContentsDataSet.GetFieldValueList('PR_NBR',True);
    if aPRNbrList.Count > 0 then
    begin
      ctx_DataAccess.OpenClient(pCLNbr);
      Qry := TevQuery.Create(Format(sPr, [BuildINsqlStatement('P.PR_NBR', aPRNbrList.CommaText)]));
      Qry.Params.AddValue('CONBR', pCONbr);
      Qry.Execute;
      if (Qry.Result.RecordCount > 0) then //to eliminate PR deleted
      begin
        SBContentsDataSet.First;
        while not SBContentsDataSet.eof do
        begin
          if Qry.Result.Locate('PR_NBR',SBContentsDataSet.FieldByName('PR_NBR').AsInteger,[]) then
          begin
            if ( (pVMRReportType = 1) or (pVMRReportType = 4)) //report or ascii
               or ( (pVMRReportType = 2) and GetPRCheckType(SBContentsDataSet.FieldByname('REPORT_NO').asString) )
               or ( (pVMRReportType = 3) and GetMISCCheckType(SBContentsDataSet.FieldByname('REPORT_NO').asString)) then
            begin
              s := Trim(SBContentsDataSet.FieldByName('JOB_DESCR').AsString);
              GetNextStrValue(s, '#');
              IsFileExist := False;
              try
                //if not found file, IsFileExist = False
                tmpStream := RetrieveFile(Trim(SBContentsDataSet.FieldByname('FILE_NAME').asString));
                IsFileExist := True;
              except
                ;
              end;
              Result.AppendRecord([SBContentsDataSet.FieldByName('SBContentNBR').AsInteger,
                                 Qry.Result.FieldByName('CHECK_DATE').asDatetime,
                                 Qry.Result['BEGIN_DATE'],
                                 Qry.Result['END_DATE'],
                                 s,//PR Run number
                                 Trim(SBContentsDataSet.FieldByName('REPORT_DESCR').AsString),
                                 Trim(SBContentsDataSet.FieldByName('REPORT_NO').AsString),
                                 IntTostr(pVMRReportType),
                                 TReportType_desc[pVMRReportType],
                                 IsFileExist]);
            end;
          end;
          SBContentsDataSet.Next;
        end; //while not SBContentsDataSet.eof do
      end;//if (Qry.Result.RecordCount > 0) then
    end;//else no reports
  end;
begin
  Result := TevDataSet.Create(dsStruct); // Even if no data, send dataset with empty

  if ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_PROCESSEDREPORTS_WEB') = stDisabled
  then exit;

  if pVMRReportType > 2 then iReportType :=  pVMRReportType -1
  else iReportType :=  pVMRReportType;

  if pVMRReportType = 2 then
     RegularChecks := GetRegularCheckList
  else if pVMRReportType = 3 then
     MISCChecks := GetMiscCheckList;

  if pVMRReportType = 4 //ascii
  then sPageCount := ''
  else sPageCount := ' AND T1.PAGE_COUNT > 0 '; //to eliminate empty report if it is not ascii

  aSbnbrlist := nil;
  aPRNbrList := nil;
  EmailVoucherDataSet := TevClientDataSet.Create(nil);
  SBContentsDataSet := TevClientDataSet.Create(nil);
  SBTaxReturnContentsDataSet := TevClientDataSet.Create(nil);
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.DataRequired('');
  sPreFilter := DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Filter;
  sPreFiltered := DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Filtered;
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Filter := 'CLASS_NAME = ''TVmrWebDeliveryMethod''';
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Filtered := True;
  aSbDMnbrlist := DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.GetFieldValueList('SB_DELIVERY_METHOD_NBR', True);
  try
    if aSbDMnbrlist.Count > 0 then
    begin
      //To collect reports have been released
      SbMailBoxDS := TevClientDataSet.Create(nil);
      try
        with TExecDSWrapper.Create(Format( sSBMailBOX, [aSbDMnbrlist.CommaText])) do
        begin
          SetParam('ClNBR', pCLNbr);
          ctx_DataAccess.GetSbCustomData(SbMailBoxDS, AsVariant);
        end;
        aSbnbrlist := SbMailBoxDS.GetFieldValueList('SB_MAIL_BOX_NBR', True);
      finally
        SbMailBoxDS.Free;
      end;

      if aSbnbrlist.Count > 0 then
      begin
        // tax return reports include only report
        if (iReportType = 1) then
          CollectTaxReturnReports;

        CollectPayrollReports;
      end;//else no sb mail box with TVmrWebDeliveryMethod
    end;//else no TVmrWebDeliveryMethod
  finally
    if Assigned(aSbDMnbrlist) then aSbDMnbrlist.Free;
    if Assigned(aSbnbrlist) then aSbnbrlist.Free;
    if Assigned(aPRNbrList) then aPRNbrList.Free;
    SBContentsDataSet.Free;
    EmailVoucherDataSet.Free;
    SBTaxReturnContentsDataSet.Free;
    DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Filter := sPreFilter;
    DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Filtered := sPreFiltered;
  end;
end;

function TevVmrRemote.GetVMRReport(const aSelectedReportNbrs: IisStringList; const IsPrint : Boolean; const sComment : string): IisStream;
const
  sContent : string = ' Select CAST(Trim(StrCopy(T1.DESCRIPTION, 100, 100)) as VARCHAR(100)) REPORT_DESCR, '+
        ' CAST(Trim(StrCopy(T1.DESCRIPTION, 218, 10)) as INTEGER) PR_NBR, '+
        ' T1.FILE_NAME, O1.OPTION_INTEGER_VALUE REPORT_TYPE, '+
        ' O2.OPTION_STRING_VALUE PRINT_PASSWORD, '+
        ' O4.OPTION_STRING_VALUE REPORT_NO, T2.CL_NBR '+
        ' from SB_MAIL_BOX_CONTENT T1 '+
        ' join SB_MAIL_BOX T2 on T2.SB_MAIL_BOX_NBR = T1.SB_MAIL_BOX_NBR '+
        ' left outer join SB_MAIL_BOX_OPTION O1 on O1.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and  {AsOfNow<O1>} and O1.OPTION_TAG= ''REPORT_TYPE'' '+
        ' left outer join SB_MAIL_BOX_OPTION O4 on O4.SB_MAIL_BOX_CONTENT_NBR = T1.SB_MAIL_BOX_CONTENT_NBR '+
        ' and {AsOfNow<O4>} and O4.OPTION_TAG= ''REPORT_NO'' '+
        ' left outer join SB_MAIL_BOX_OPTION O2 on O2.SB_MAIL_BOX_NBR = T1.SB_MAIL_BOX_NBR '+
        ' and {AsOfNow<O2>} and O2.OPTION_TAG like :pass1 '+
        ' where {AsOfNow<T1>} and {AsOfNow<T2>} and %s '; //T1.SB_MAIL_BOX_CONTENT_NBR in (%s)

  SPrCheck : string = 'Select PR_CHECK_NBR FROM PR_CHECK a WHERE {AsOfNow<a>} AND a.PR_NBR = :PRNBR';
  SMiscCheck : string = 'Select PR_MISCELLANEOUS_CHECKS_NBR FROM PR_MISCELLANEOUS_CHECKS a WHERE {AsOfNow<a>} AND a.PR_NBR = :PRNBR';
var
  tmpStream : IEvDualStream;
  RptResults: TrwReportResults;
  PDFInfoRec: TrwPDFDocInfo;
  SBContentsDataSet : TevClientDataSet;
  PRCheck, MiscCheck, Check : TStringList;
  i : Integer;
  RegularChecks, MISCChecks : IisStringList;

      function CheckEligibleReprint (aREPORT_NO, aPR_NBR :string): boolean;
      var aReport_level : string;
      begin
        Result := False;
        aReport_level := copy(aREPORT_NO,1,1);
        Delete(aREPORT_NO,1,1);

        if (aReport_level = 'S') and ( RegularChecks.IndexOf(aREPORT_NO) <> -1) then
        begin
          if ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PROCESSED_CHECKS') = stEnabled then
          begin
             PRCheck.Add(aPR_NBR);
             Result := True;
          end
        end
        else if (aReport_level = 'S') and (MISCChecks.IndexOf(aREPORT_NO) <> -1) then
        begin
          if ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_MISC_CHECKS') = stEnabled then
          begin
            MiscCheck.Add(aPR_NBR);
            Result := True;
          end
        end
        else
          if ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PAYROLL_REPORTS') = stEnabled then
             Result := True;

      end;
begin
  //if aSelectedReportNbrs.count = 1 it can be PDF or ASCII file to return
  //if aSelectedReportNbrs.count > 1 it should be PDF file to return

  result := nil;
  RegularChecks := GetRegularCheckList;
  MISCChecks := GetMiscCheckList;

  SBContentsDataSet := TevClientDataSet.Create(nil);
  RptResults := TrwReportResults.Create;
  PRCheck := TStringList.Create;
  MiscCheck := TStringList.Create;
  Check := nil;
  PRCheck.Sorted := True;
  PRCheck.Duplicates := dupIgnore; //to avoid duplicated PR nbr
  MiscCheck.Sorted := True;
  MiscCheck.Duplicates := dupIgnore; //to avoid duplicated PR nbr
  try
    try
      if aSelectedReportNbrs.Count = 0 then
        raise Exception.Create('aSelectedReportNbrs.count = 0');

      with TExecDSWrapper.Create(Format(sContent,[BuildINSqlStatement('T1.SB_MAIL_BOX_CONTENT_NBR', aSelectedReportNbrs.Commatext)])) do
      begin
        SetParam('pass1', 'PM%PRINT_PASSWORD');
        ctx_DataAccess.GetSbCustomData(SBContentsDataSet, AsVariant);
      end;

      if (SBContentsDataSet.RecordCount > 0 )
         and ( SBContentsDataSet.FieldByname('REPORT_TYPE').asInteger = 3 ) then
      begin
        try
          tmpStream := RetrieveFile(Trim(SBContentsDataSet.FieldByname('FILE_NAME').asString));
        except
          raise Exception.Create('Cannot find ' + Trim(SBContentsDataSet.FieldByname('FILE_NAME').asString) +
                                 ' in VMR folder');
        end;
        rptResults.AddReportResult(trim(SBContentsDataSet.FieldByname('REPORT_DESCR').asString),
                  TReportType(SBContentsDataSet['REPORT_TYPE']), tmpStream);

        result := rptResults.items[0].Data;
        //if found Ascii file at first, return only one ascii file
        exit;
      end;


      if aSelectedReportNbrs.Count > 1 then
      begin
        // report is more than one, eliminate Ascii report type
        SBContentsDataSet.Filter := 'REPORT_TYPE <> 3';
        SBContentsDataSet.Filtered := True;
      end;

      while not SBContentsDataSet.eof do
      begin
        if ( IsPrint )
           and not CheckEligibleReprint ( SBContentsDataSet.FieldByname('REPORT_NO').asString,
                                          SBContentsDataSet.FieldByname('PR_NBR').asstring) then
        begin
          SBContentsDataSet.next;
          continue;
        end;

        try
          tmpStream := RetrieveFile(Trim(SBContentsDataSet.FieldByname('FILE_NAME').asString));
        except
          raise Exception.Create('Cannot find ' + Trim(SBContentsDataSet.FieldByname('FILE_NAME').asString) +
                                 ' in VMR folder');
        end;
       rptResults.AddReportResult(trim(SBContentsDataSet.FieldByname('REPORT_DESCR').asString),
                  TReportType(SBContentsDataSet['REPORT_TYPE']), tmpStream);

        SBContentsDataSet.next;
      end;

      if (( PRCheck.Count > 0 ) or ( MiscCheck.Count > 0 ))
         and ( Trim(sComment) = '' )
      then raise Exception.Create('No reason for reprinting check');

      for i := 0 to PRCheck.Count -1 do
      begin
        with TExecDSWrapper.Create(SPRCheck) do
        begin
          SetParam('PRNBR', PRCheck[i]);
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          ctx_DataAccess.CUSTOM_VIEW.Open;
        end;
        Check := ctx_DataAccess.CUSTOM_VIEW.GetFieldValueList('PR_CHECK_NBR',True);
        if Check.Count > 0 then
        begin
          Check.Sort;
          LogCheckReprint(Check, false, StrToInt(PRCheck[i]), Trim(sComment));
        end
      end;

      for i := 0 to MISCCheck.Count -1 do
      begin
        with TExecDSWrapper.Create(SMiscCheck) do
        begin
          SetParam('PRNBR', MISCCheck[i]);
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          ctx_DataAccess.CUSTOM_VIEW.Open;
        end;
        Check := ctx_DataAccess.CUSTOM_VIEW.GetFieldValueList('PR_MISCELLANEOUS_CHECKS_NBR',True);
        if Check.Count > 0 then
        begin
          Check.Sort;
          LogCheckReprint(Check, True, StrToInt(MISCCheck[i]), Trim(sComment));
        end;
      end;

      try
        rptResults := ctx_RWLocalEngine.MergeGraphicReports(rptResults,'VMR reports');
      except
        raise Exception.Create('Cannot merge reports');
      end;
      if rptResults.Count > 0 then
      begin
        PDFInfoRec.Author := 'Evolution Report Writer';
        PDFInfoRec.Creator := 'Evolution Report Engine';
        PDFInfoRec.Subject := '';
        PDFInfoRec.Title   := 'VMR reports';
        PDFInfoRec.Compression := True;

        PDFInfoRec.ProtectionOptions := [];
        PDFInfoRec.UserPassword := '';
        PDFInfoRec.OwnerPassword := '';

        if ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_PROCESSEDREPORTS_WEB') <> stDisabled then
        begin
          //Evolution Payroll - no GUI to enter password
          //PDFInfoRec.OwnerPassword := Trim(ConvertNull(SBContentsDataSet['PRINT_PASSWORD'],''));
          //if (PDFInfoRec.OwnerPassword <> '') then
          //  PDFInfoRec.ProtectionOptions := [pdfPrint, pdfCopyInformation];

          rptResults.items[0].Data := ctx_RWLocalEngine.ConvertRWAtoPDF(rptResults.items[0].Data, PDFInfoRec);
          result := rptResults.items[0].Data
        end;
      end;
    except
      raise;
    end;
  finally
    if Assigned(PRCheck) then PRCheck.Free;
    if Assigned(MiscCheck) then MiscCheck.Free;
    if Assigned(Check) then Check.Free;
    if Assigned(SBContentsDataSet) then SBContentsDataSet.free;
    if Assigned(RptResults) then RptResults.Free;
  end;
end;

procedure TevVmrRemote.SetPrinterList(const ALocation:string; const APrinterList: IevVMRPrintersList; const IsDelete: Boolean);

  procedure SavePrinterList (aLoc:string);
  var
    i, j, x: Integer;
    S: string;
    PrinterInfo: IevVMRPrinterInfo;
    dsSbOptions: TevClientDataSet;

    procedure PostOption(const OptionTag: string; const IntOption, StrOption: Variant);
    begin
      if dsSbOptions.FindKey([OptionTag]) then
        dsSbOptions.Edit
      else begin
        dsSbOptions.Append;
        dsSbOptions['OPTION_TAG'] := OptionTag;
      end;

      dsSbOptions['OPTION_INTEGER_VALUE'] := IntOption;
      dsSbOptions['OPTION_STRING_VALUE'] := StrOption;
      dsSbOptions.Post;
    end;

  begin
    dsSbOptions := TevClientDataSet.Create(nil);
    try
      dsSbOptions.CheckDSCondition('OPTION_TAG like ('''+aLoc+'%'')');
      dsSbOptions.ProviderName := DM_SERVICE_BUREAU.SB_OPTION.ProviderName;
      dsSbOptions.IndexFieldNames := 'OPTION_TAG';
      ctx_DataAccess.OpenDataSets([dsSbOptions]);

      if ISDelete then
      begin
        while dsSbOptions.RecordCount > 0 do
          dsSbOptions.Delete;
      end;

      x:= 0;
      for i := 0 to APrinterList.Count - 1 do
      begin
        s:= APrinterList[i].Name;

        if ( pos('^',APrinterList[i].Name) > 0 ) then // for Printer Set Up Scrreen - not to get duplicated
          GetNextStrValue(s, '^');

        if (aLocation = 'NON$AA$') or
           (((pos('^',APrinterList[i].Name) > 0) and AnsiSameText(copy(APrinterList[i].Name, 1,Length(aLoc)), aLoc)) or
           ((aLoc[1]='@') and (AnsiSameText(APrinterList[i].Location,aLoc))))
        then
        begin
          PrinterInfo := APrinterList[i];
          PostOption(aLoc+ ifthen(aLoc[1]='@', '|', '') +IntToStr(x), Byte(PrinterInfo.Active), s);
          PostOption(aLoc+ ifthen(aLoc[1]='@', '|', '') +IntToStr(x)+ 'LOCATION', Null, PrinterInfo.Location);
          PostOption(aLoc+ ifthen(aLoc[1]='@', '|', '') +IntToStr(x)+ 'HOFFSET', PrinterInfo.HOffset, Null);
          PostOption(aLoc+ ifthen(aLoc[1]='@', '|', '') +IntToStr(x)+ 'VOFFSET', PrinterInfo.VOffset, Null);
          for j := 0 to PrinterInfo.BinCount - 1 do
            PostOption(aLoc+ ifthen(aLoc[1]='@', '|', '') +IntToStr(x)+ 'TRAY'+ IntToStr(j), PrinterInfo.Bins[j].SbPaperNbr, PrinterInfo.Bins[j].Name);

          inc(x);
        end;

      end;
      ctx_DataAccess.PostDataSets([dsSbOptions]);
    finally
      dsSbOptions.Free;
    end;
  end;
var
  x: integer;
  alist: IisStringList;
  TmpLoc: string;
  pInfo: IevVMRPrinterInfo;
begin
  if aLocation = 'ALL' then
  begin
    SavePrinterList(VMR_SB_SIDE_PRINTER_NAME);
    alist:= GetRemotePrinterList;
    for x:= 0 to aList.count -1 do
        SavePrinterList(aList[x]);
  end
  else if aLocation = 'NON$AA$' then
  begin
     SavePrinterList(VMR_SB_SIDE_PRINTER_NAME);
  end
  else begin
    TmpLoc := Uppercase(Trim(aLocation));
    if TmpLoc = '' then
    begin
      if (APrinterList.Count > 0)then
      begin
        pInfo :=APrinterList[0];
        TmpLoc := Uppercase(Trim(pInfo.Location));
      end;
    end;

    //TmpLoc = '' something is wrong
    if TmpLoc <> '' then
      SavePrinterList(TmpLoc);
  end;
end;

procedure TevVmrRemote.DeleteDirectory(const DirName: string);
begin
  ClearDir(NormalizePath(ctx_DomainInfo.FileFolders.VMRFolder) + DirName, True);
end;

procedure TevVmrRemote.PurgeMailBox(const BoxNbr: Integer; const ForceDelete : Boolean);
var
  SBMailBoxContentNbrs: IisStringList;
  sNbrs : string;
begin

  if ForceDelete then
  begin
    DM_SERVICE_BUREAU.SB_MAIL_BOX.DataRequired('UP_LEVEL_MAIL_BOX_NBR ='+ intTostr(BoxNbr));
    DM_SERVICE_BUREAU.SB_MAIL_BOX.First;
    while not DM_SERVICE_BUREAU.SB_MAIL_BOX.Eof do
    begin
      DM_SERVICE_BUREAU.SB_MAIL_BOX.Edit;
      DM_SERVICE_BUREAU.SB_MAIL_BOX['UP_LEVEL_MAIL_BOX_NBR'] := null;
      DM_SERVICE_BUREAU.SB_MAIL_BOX.Post;
      DM_SERVICE_BUREAU.SB_MAIL_BOX.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX]);

    DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired('SB_MAIL_BOX_NBR ='+ intTostr(BoxNbr));
    DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.First;
    while not DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Eof do
       DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Delete;

    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired('SB_MAIL_BOX_NBR ='+ intTostr(BoxNbr));
    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.First;
    while not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Eof do
       DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Delete;

    DM_SERVICE_BUREAU.SB_MAIL_BOX.DataRequired('SB_MAIL_BOX_NBR ='+ intTostr(BoxNbr));
    DM_SERVICE_BUREAU.SB_MAIL_BOX.First;
    if not DM_SERVICE_BUREAU.SB_MAIL_BOX.IsEmpty then
       DM_SERVICE_BUREAU.SB_MAIL_BOX.Delete;
    try
      ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION, DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT, DM_SERVICE_BUREAU.SB_MAIL_BOX]);
    except
     on e: exception do
       ADDVMRLogFileEvent(etWarning,'PurgeMailBox:Delete',e.Message,'[' + intTostr(BoxNbr) + ']');
    end;
  end
  else
  begin
    SBMailBoxContentNbrs := TisStringList.Create;
    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired('SB_MAIL_BOX_NBR ='+ intTostr(BoxNbr));

    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.First;
    while not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Eof do
    begin
      if (not FileDoesExist(DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['FILE_NAME']))then
        SBMailBoxContentNbrs.Add(DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.SB_MAIL_BOX_CONTENT_NBR.AsString);

      DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Next;
    end;

    if SBMailBoxContentNbrs.Count <= 0 then exit;

    sNbrs := BuildINsqlStatement('SB_MAIL_BOX_CONTENT_NBR',SBMailBoxContentNbrs);
    DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired(sNbrs);
    DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.First;
    while not DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Eof do
       DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Delete;

    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired(sNbrs);
    DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.First;
    while not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Eof do
       DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Delete;

    try
      ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION, DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
    except
     on e: exception do
       ADDVMRLogFileEvent(etWarning,'PurgeMailBox:Clean',e.Message,'[' + intTostr(BoxNbr) + ']');
    end;
  end;
end;

procedure TevVmrRemote.PurgeClientMailBox(const ClientNbr: Integer;
  const ForceDelete: Boolean);
var
  qry : IevQuery;
begin
  qry := TevQuery.Create('Select SB_MAIL_BOX_NBR from SB_MAIL_BOX a where a.CL_NBR=:CLNBR and {AsOfNow<a>} ');
  qry.Params.AddValue('CLNBR',ClientNbr);
  qry.Execute;

  while not qry.Result.Eof do
  begin
    PurgeMailBox(qry.Result.FieldByName('SB_MAIL_BOX_NBR').asInteger, ForceDelete);
    qry.Result.Next;
  end;
end;

// To delete SBMailBoxContent which are duplicated records from Tax Return
procedure TevVmrRemote.PurgeMailBoxContentList(const SBMailBoxContentNbrs: IisStringList);
var
  sNbrs : string;
  qry :IevQuery;
begin
  if SBMailBoxContentNbrs.Count <= 0 then exit;

  sNbrs := BuildINsqlStatement('SB_MAIL_BOX_CONTENT_NBR',SBMailBoxContentNbrs);
  qry := TevQuery.Create('Select distinct SB_MAIL_BOX_NBR from SB_MAIL_BOX_Content a where {AsOfNow<a>} and ' + sNbrs );
  qry.Execute;
  while not qry.Result.Eof do
  begin
    qry.Result.Next;
  end;

  DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired(sNbrs);
  DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.First;
  while not DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Eof do
     DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Delete;

  DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired(sNbrs);
  DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.First;
  while not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Eof do
     DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Delete;

  try
    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION, DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
  except
   on e: exception do
     ADDVMRLogFileEvent(etWarning,'PurgeMailBoxContentList',e.Message,'[' +SBMailBoxContentNbrs.commatext + ']');
  end;

end;

procedure TevVmrRemote.ADDVMRLogFileEvent(const AEventType: TLogEventType;
  const AEventClass, AText, ADetails: String);
begin

  if not Assigned(FVMRLogFile) then
  begin
    DeleteFile('VMRCleanUpHistory.log');
    DeleteFile('VMRCleanUpHistory.log.ind');
    try
      FVMRLogFile := TevLogFile.Create(AddVMRPath('VMRCleanUpHistory.log'));
    except
      on E: Exception do
      begin
        FVMRLogFile := nil;
        FVMRLogFile := TevLogFile.Create(AddVMRPath('VMRCleanUpHistory.log'));
      end;
    end;
  end;

  Lock;
  try
    FVMRLogFile.AddEvent(AEventType, AEventClass, AText, ADetails);
  finally
    Unlock;
  end;
end;

function TevVmrRemote.GetRemoteVMRReportList (const ALocation : string): IevDataset;
const
  dsStruct: array [0..9] of TDSFieldDef = (
          (FieldName: 'SBMailBoxNBR';    DataType: ftInteger;    Size: 0;   Required: True),
          (FieldName: 'SBMailBoxContentNBR';       DataType: ftInteger;   Size: 0;   Required: True),
          (FieldName: 'ReportName';  DataType: ftString;   Size: 512;    Required: False),
          (FieldName: 'PrinterName'; DataType: ftString;    Size: 512;  Required: True),
          (FieldName: 'PrinterTray';    DataType: ftString;    Size: 512;   Required: True),
          (FieldName: 'VerticalOffset';  DataType: ftInteger;   Size: 0;    Required: False),
          (FieldName: 'HorizontalOffset';       DataType: ftInteger;   Size: 0;    Required: False),
          (FieldName: 'MediaType';    DataType: ftString;    Size: 2;   Required: True),
          (FieldName: 'PageCount';    DataType: ftInteger;    Size: 0;   Required: True),
          (FieldName: 'PrintedTime';    DataType: ftDateTime;    Size: 0;   Required: False));

  dstmpDS: array [0..10] of TDSFieldDef = (
          (FieldName: 'SBMailBoxNBR';    DataType: ftInteger;    Size: 0;   Required: True),
          (FieldName: 'SBMailBoxContentNBR';       DataType: ftInteger;   Size: 0;   Required: True),
          (FieldName: 'ReportName';  DataType: ftString;   Size: 512;    Required: False),
          (FieldName: 'PrinterName'; DataType: ftString;    Size: 512;  Required: True),
          (FieldName: 'PrinterTray';    DataType: ftString;    Size: 512;   Required: True),
          (FieldName: 'VerticalOffset';  DataType: ftInteger;   Size: 0;    Required: False),
          (FieldName: 'HorizontalOffset';       DataType: ftInteger;   Size: 0;    Required: False),
          (FieldName: 'MediaType';    DataType: ftString;    Size: 2;   Required: True),
          (FieldName: 'PageCount';    DataType: ftInteger;    Size: 0;   Required: True),
          (FieldName: 'PrintedTime';    DataType: ftDateTime;    Size: 0;   Required: False),
          (FieldName: 'JOB_DESCR';    DataType: ftString;  Size: 512;    Required: False));
var
  cd: TevClientDataSet;
  currentMailBoxNbr: integer;
  PrInfo: IevVMRPrintersList;
  tempMailBoxDS: IevDataset;
  mailBoxLocation: string;
  endOfMailBox: boolean;
  function CheckPresentedMediaTypes(const p: IevVMRPrinterInfo; const s: ShortString): ShortString;
  var
    i: Integer;
  begin
    Result := '';
    for i := 0 to P.BinCount - 1 do
      if P.Bins[i].MediaType <> ' ' then
        if Pos(P.Bins[i].MediaType, s) <> 0 then
          Result := Result+ P.Bins[i].MediaType;
  end;

  //Add mailbox to result dataset
  procedure FillResultDataSet;
  begin
    tempMailBoxDS.First;
    while not tempMailBoxDS.Eof do
    begin
      Result.AppendRecord([tempMailBoxDS.FieldByName('SBMailBoxNBR').AsInteger,
         tempMailBoxDS.FieldByName('SBMailBoxContentNBR').AsInteger,
         tempMailBoxDS['ReportName'],
         tempMailBoxDS['PrinterName'],
         tempMailBoxDS['PrinterTray'],
         tempMailBoxDS.FieldByName('VerticalOffset').AsInteger,
         tempMailBoxDS.FieldByName('HorizontalOffset').AsInteger,
         tempMailBoxDS['MediaType'],
         tempMailBoxDS.FieldByName('PageCount').AsInteger,
         tempMailBoxDS['PrintedTime']]);

      tempMailBoxDS.Next;
    end;
    tempMailBoxDS.Clear;
  end;

  //Check which printer and tray must be assigned to each report
  procedure SetPrinterInfo;
  var
    i, k: integer;
    mediaType: string;
    mediaTypes: string;
    iPrinterIndexes: array of Integer;
    idxMostMediaPrinter: integer;
    mostMediaAssigned: integer;
  begin
    if tempMailBoxDS.RecordCount > 0 then
    begin
      tempMailBoxDS.First;
      while not tempMailBoxDS.Eof do
      begin
        mediaType := tempMailBoxDS['MediaType'];
        if Pos(mediaType, mediaTypes) = 0 then
          mediaTypes := mediaTypes + mediaType;
        tempMailBoxDS.Next;
      end;

      //Reset printer tags
      for i := 0 to PrInfo.Count - 1 do
        PrInfo[i].Tag := '';

      //Set tag for printers assigned to media type required to print
      for i := 0 to PrInfo.Count - 1 do
      begin
        //must have PrInfo[i].Location
        if PrInfo[i].Active and AnsiSameText(PrInfo[i].Location,mailBoxLocation) then
        begin
          PrInfo[i].Tag := CheckPresentedMediaTypes(PrInfo[i], mediaTypes);
          SetLength(iPrinterIndexes, Length(iPrinterIndexes)+1);
          iPrinterIndexes[High(iPrinterIndexes)] := i;
        end;
      end;


      for i := 0 to High(iPrinterIndexes) do
      begin
        k := iPrinterIndexes[i];
        tempMailBoxDS.First;
        while not tempMailBoxDS.Eof do
        begin
          if (tempMailBoxDS['PrinterTray'] = '') and
             (Pos(tempMailBoxDS['JOB_DESCR'], VmrCoverSheetsAndLabelsFolder) <> 0) and
             (tempMailBoxDS['MediaType'] = MEDIA_TYPE_PICKUP_SHEET) and
             (Pos(tempMailBoxDS['MediaType'], PrInfo[k].Tag) <> 0) then
          begin
            tempMailBoxDS.Edit;
            tempMailBoxDS['PrinterName'] := PrInfo[k].Name;
            tempMailBoxDS['PrinterTray'] := PrInfo[k].FindBinNameByMediaType((tempMailBoxDS.FieldByName('MediaType').AsString)[1]);
            tempMailBoxDS['VerticalOffset'] := PrInfo[k].VOffset;
            tempMailBoxDS['HorizontalOffset'] := PrInfo[k].HOffset;
            tempMailBoxDS.Post;
            Break;
          end;
          tempMailBoxDS.Next;
        end;
      end;


      //Loop through printers with media assigned, takes the one with the most media by default
      repeat
        idxMostMediaPrinter := -1;
        mostMediaAssigned := 0;
        for i := 0 to High(iPrinterIndexes) do
        begin
          if Length(PrInfo[iPrinterIndexes[i]].Tag) > mostMediaAssigned then
          begin
            mostMediaAssigned := Length(PrInfo[iPrinterIndexes[i]].Tag);
            idxMostMediaPrinter := iPrinterIndexes[i];
          end;
        end;

        if (idxMostMediaPrinter <> -1) then
        begin
          tempMailBoxDS.First;
          while not tempMailBoxDS.Eof do
          begin
            if (tempMailBoxDS['PrinterTray'] = '') and
               (Pos(tempMailBoxDS['MediaType'], PrInfo[idxMostMediaPrinter].Tag) <> 0) then
            begin
              tempMailBoxDS.Edit;
              tempMailBoxDS['PrinterName'] := PrInfo[idxMostMediaPrinter].Name;
              tempMailBoxDS['PrinterTray'] := PrInfo[idxMostMediaPrinter].FindBinNameByMediaType((tempMailBoxDS.FieldByName('MediaType').AsString)[1]);
              tempMailBoxDS['VerticalOffset'] := PrInfo[idxMostMediaPrinter].VOffset;
              tempMailBoxDS['HorizontalOffset'] := PrInfo[idxMostMediaPrinter].HOffset;
              tempMailBoxDS.Post;
            end;
            tempMailBoxDS.Next;
          end;

          //Done with printer empty list of media, go to next printer
          PrInfo[idxMostMediaPrinter].Tag := '';
        end;
      until idxMostMediaPrinter = -1;
    end;
  end;
begin
  Result := TevDataSet.Create(dsStruct); // Even if no data, send dataset with empty
  cd := TevClientDataSet.Create(nil);
  cd.Close;
  cd.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
  try
    //found SB_MAIL_BOX_OPTION where option_tag = 'VMR_PRINT_LOCATIONS' and option_string_value = ALocation
    // and USERSIDE_PRINTED is null
    with TExecDSWrapper.Create('VmrRemotePrintJobs') do
    begin
      SetMacro('CONDITION', ' (T1.SCANNED_TIME is null) and (T1.PRINTED_TIME is null) and (T1.RELEASED_TIME is not null) ' +
                           '  and Upper(o1.OPTION_STRING_VALUE) = '''+Uppercase(ALocation)+''' and (T2.USERSIDE_PRINTED is null)'); //eleminate printered one

      cd.DataRequest(AsVariant);
    end;
    cd.Open;

    tempMailBoxDS := TevDataSet.Create(dstmpDS);

    PrInfo := (ctx_VmrRemote.GetPrinterList(Uppercase(ALocation)) as IisInterfacedObject).GetClone as IevVMRPrintersList;

    cd.First;
    while not cd.Eof do
    begin
      currentMailBoxNbr := cd.FieldByName('SB_MAIL_BOX_NBR').AsInteger;
      mailBoxLocation := ConvertNull(cd.FieldByName('LOCATION').AsString, '');

      //Add record to temp dataset, we will add printer info the end of MailBox record
      tempMailBoxDS.AppendRecord([cd.FieldByName('SB_MAIL_BOX_NBR').AsInteger,
         cd.FieldByName('SB_MAIL_BOX_CONTENT_NBR').AsInteger,
         cd['REP_DESCR'],
         '',
         '',
         0,
         0,
         cd['MEDIA_TYPE'],
         cd.FieldByName('PAGE_COUNT').AsInteger,
         cd['USERSIDE_PRINTED'],
         cd['JOB_DESCR']]);

      cd.Next;

      if cd.Eof then
        endOfMailBox := true
      else if cd.FieldByName('SB_MAIL_BOX_NBR').AsInteger <> currentMailBoxNbr then
        endOfMailBox := true
      else
        endOfMailBox := false;

      //On end of current mailbox, get printer info and add to result dataset
      if endOfMailBox then
      begin
        SetPrinterInfo;
        FillResultDataSet;
      end;
    end;

  finally
    cd.Free;
  end;
end;

function TevVmrRemote.GetRemoteVMRReport(const SBMailBoxContentNbr: integer): IisStream;
var
  cdContent: TevClientDataSet;
  tmpStream : IEvDualStream;
  RptResults: TrwReportResults;
  sReportType: string;
  reportType: TReportType;
  PDFInfoRec: TrwPDFDocInfo;
begin
  result := nil;
  rptResults := TrwReportResults.Create;
  cdContent := TevClientDataSet.Create(nil);
  cdContent.Close;
  try
    cdContent.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    with TExecDSWrapper.Create('VmrRemotePrintContent') do
    begin
      SetMacro('CONDITION', ' T2.SB_MAIL_BOX_CONTENT_NBR = ' + IntToStr(SBMailBoxContentNbr)+' and (T2.USERSIDE_PRINTED is null)');

      cdContent.DataRequest(AsVariant);
    end;
    cdContent.Open;

    if cdContent.RecordCount > 0 then
    begin
      try
        tmpStream := RetrieveFile(Trim(cdContent.FieldByname('FILE_NAME').asString));
      except
        raise Exception.Create('Cannot find ' + Trim(cdContent.FieldByname('FILE_NAME').asString) +
                               ' in VMR folder');
      end;
      sReportType := cdContent.FieldByname('REPORT_TYPE').asString;
      if sReportType = '' then
        reportType := rtUnknown
      else
        reportType := cdContent['REPORT_TYPE'];
      rptResults.AddReportResult(trim(cdContent.FieldByname('REPORT_DESCR').asString),
                reportType, tmpStream);

      //Convert to pdf
      PDFInfoRec.Author := 'Evolution Report Writer';
      PDFInfoRec.Creator := 'Evolution Report Engine';
      PDFInfoRec.Subject := '';
      PDFInfoRec.Title   := 'VMR reports';
      PDFInfoRec.Compression := True;
      PDFInfoRec.ProtectionOptions := [];
      PDFInfoRec.OwnerPassword := '';
      PDFInfoRec.UserPassword := '';
      rptResults.items[0].Data := ctx_RWLocalEngine.ConvertRWAtoPDF(rptResults.items[0].Data, PDFInfoRec);

      result := rptResults.items[0].Data;

      If Assigned(RptResults) then
      begin
        DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.DataRequired('SB_MAIL_BOX_CONTENT_NBR = ' + IntToStr(SBMailBoxContentNbr));
        if not DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Eof then
        begin
          DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Edit;
          DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT['USERSIDE_PRINTED'] := Now;
          DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.Post;
          ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
        end;
      end;
    end;
  finally
    cdContent.Free;
    if Assigned(RptResults) then RptResults.Free;
  end;
end;


procedure TevVmrRemote.AddSBAvailablePrinters( var PrList : IevVMRPrintersList);
type
   TBinName      = array [0..23] of Char;
   TBinNameArray = array of TBinName;
   TBinArray     = array of Word;
var
  numBinNames, numBins: Integer;
  aBinNames: TBinnameArray;
  aBins: TBinArray;
  i, j, k: Integer;
  PrinterInfo: IevVMRPrinterInfo;
begin
  Mainboard.MachineInfo.Printers.Lock;
  try
    Mainboard.MachineInfo.Printers.Load;
    i := 0;

    for j := 0 to Mainboard.MachineInfo.Printers.Count- 1 do
    begin
      numBinNames := WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINNAMES, nil, nil);
      numBins     := WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINS, nil, nil);
      if numBins <> numBinNames then
        raise Exception.Create('DeviceCapabilities reports different number of bins and bin names!');

      if numBinNames > 0 then
      begin
        if (Mainboard.MachineInfo.Printers[j].Name) <> '' then
        begin
          PrinterInfo := PrList.Add;
          PrinterInfo.Name := VMR_SB_SIDE_PRINTER_NAME+IntTostr(i)+'^'+Mainboard.MachineInfo.Printers[j].Name;

          SetLength(aBinNames, numBinNames);
          SetLength(aBins, numBins);
          try
            WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINNAMES, PChar(aBinNames), nil);
            WinSpool.DeviceCapabilities(PAnsiChar(Mainboard.MachineInfo.Printers[j].Name), '', DC_BINS, PChar(aBins), nil);
            for k := 0 to numBinNames - 1 do
              if not Contains(aBinNames[k], 'AUTO') and not Contains(aBinNames[k], 'MANUAL') and (aBinNames[k] <> '')then
                PrinterInfo.AddBin.Name := aBinNames[k];
          finally
            SetLength(aBinNames, 0);
            SetLength(aBins, 0);
          end;
          inc(i);
        end;
      end;
    end;
  finally
    Mainboard.MachineInfo.Printers.Unlock;
  end;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVmrRemote, IevVmrRemote, 'VMR Remote');
  FVmrBoxAccessCS := TCriticalSection.Create;

finalization
  FVmrBoxAccessCS.Free;

end.



