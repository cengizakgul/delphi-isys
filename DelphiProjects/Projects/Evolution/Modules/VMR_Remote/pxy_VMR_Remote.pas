// Proxy Module "VMR Remote"
unit pxy_VMR_Remote;

interface

implementation

uses isBaseClasses, EvCommonInterfaces, evRemoteMethods, EvTransportDatagrams, EvStreamUtils,
     EvMainboard, EvTypes, SReportSettings, EvContext, evProxy, EvTransportInterfaces,SysUtils;

type
  TevVmrRemoteProxy = class(TevProxyModule, IevVmrRemote)
  protected
    function  StoreFile(const FileName: string; const Content: IisStream; const AutoRename: Boolean): string;
    function  RetrieveFile(const FileName: string): IisStream;
    function  FileDoesExist(const FileName: string): Boolean;
    procedure DeleteFile(const FileName: string);
    procedure DeleteDirectory(const DirName: string);
    function  CheckScannedBarcode(const BarCode: string; out ExpectedBarcode, LastScannedBarcode: string): Boolean;
    procedure RemoveScannedBarcode(const BarCode: string);
    procedure RemoveScannedBarcodes(const MailBoxBarCode: string);
    function  PutInBoxes(const BarCode, ToBarCode: string): string;
    function  FindInBoxes(const BarCode: string): string;
    procedure RemoveFromBoxes(const BarCode: string);
    function  RetrievePickupSheetsXMLData: IisStream;
    function  GetSBPrinterList : IevVMRPrintersList;
    function  GetPrinterList(const ALocation:string): IevVMRPrintersList;
    procedure SetPrinterList(const ALocation : string; const APrinterList: IevVMRPrintersList;const IsDelete: Boolean);
    procedure PrintRwResults(const ARepResults: TrwReportResults);

    procedure ProcessRelease(const TopBoxNbr: Integer; const ReleaseType:TVmrReleaseTypeSet; out UnprintedReports: TrwReportResults);
    procedure ProcessRevert(const TopBoxNbr: Integer);
    function  GetVMRReportList(const pCLNbr, pCONbr: Integer; const  pCheckFromDate, pCheckToDate : TDateTime; const pVMRReportType : Integer): IevDataset;
    function  GetVMRReport(const aSelectedReportNbrs: IisStringList; const IsPrint : Boolean; const sComment : string): IisStream;
    procedure PurgeMailBox(const BoxNbr: Integer; const ForceDelete : Boolean);
    procedure PurgeClientMailBox(const ClientNbr: Integer; const ForceDelete : Boolean);
    procedure PurgeMailBoxContentList(const SBMailBoxContentNbrs: IisStringList);
    function GetRemoteVMRReportList(const ALocation:string): IevDataset;
    function GetRemoteVMRReport(const SBMailBoxContentNbr: integer): IisStream;
  end;

function GetVmrRemoteProxy: IevVmrRemote;
begin
  if Context.License.VMR then
    Result := TevVmrRemoteProxy.Create
  else
    Result := nil;
end;


{ TevVmrRemoteProxy }

function TevVmrRemoteProxy.CheckScannedBarcode(const BarCode: string;
  out ExpectedBarcode, LastScannedBarcode: string): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_CHECKSCANNEDBARCODE);
  D.Params.AddValue('BarCode', BarCode);
  D.Params.AddValue('ExpectedBarcode', ExpectedBarcode);
  D.Params.AddValue('LastScannedBarcode', LastScannedBarcode);
  D := SendRequest(D, False);
  ExpectedBarcode := D.Params.Value['ExpectedBarcode'];
  LastScannedBarcode := D.Params.Value['LastScannedBarcode'];
  Result := D.Params.Value[METHOD_RESULT] <> 0;
end;

procedure TevVmrRemoteProxy.DeleteFile(const FileName: string);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_DELETEFILE);
  D.Params.AddValue('FileName', FileName);
  D := SendRequest(D, False);
end;

procedure TevVmrRemoteProxy.DeleteDirectory(const DirName: string);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_DELETEDIRECTORY);
  D.Params.AddValue('DirName', DirName);
  D := SendRequest(D, False);
end;


function TevVmrRemoteProxy.FileDoesExist(const FileName: string): Boolean;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_FILEDOESEXIST);
  D.Params.AddValue('FileName', FileName);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT] <> 0;
end;

function TevVmrRemoteProxy.FindInBoxes(const BarCode: string): string;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_FINDINBOXES);
  D.Params.AddValue('BarCode', BarCode);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevVmrRemoteProxy.GetSBPrinterList: IevVMRPrintersList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_GETSBPRINTERLIST);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevVMRPrintersList;
end;

function TevVmrRemoteProxy.GetPrinterList(const ALocation:string): IevVMRPrintersList;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_GETPRINTERLIST);
  D.Params.AddValue('ALocation', ALocation);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevVMRPrintersList;
end;

procedure TevVmrRemoteProxy.PrintRwResults(
  const ARepResults: TrwReportResults);
var
  D: IevDatagram;
  S: IevDualStream;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PRINTRWRESULTS);
  S := ARepResults.GetAsStream;
  D.Params.AddValue('ARepResults', S);
  D := SendRequest(D, False);
end;

function TevVmrRemoteProxy.PutInBoxes(const BarCode,
  ToBarCode: string): string;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PUTINBOXES);
  D.Params.AddValue('BarCode', BarCode);
  D.Params.AddValue('ToBarCode', ToBarCode);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevVmrRemoteProxy.RemoveFromBoxes(const BarCode: string);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_REMOVEFROMBOXES);
  D.Params.AddValue('BarCode', BarCode);
  D := SendRequest(D, False);
end;

function TevVmrRemoteProxy.RetrievePickupSheetsXMLData: IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_RETRIEVE_PICKUPSHEET_DATA);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;


procedure TevVmrRemoteProxy.RemoveScannedBarcode(const BarCode: string);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_REMOVESCANNEDBARCODE);
  D.Params.AddValue('BarCode', BarCode);
  D := SendRequest(D, False);
end;

procedure TevVmrRemoteProxy.RemoveScannedBarcodes(
  const MailBoxBarCode: string);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_REMOVESCANNEDBARCODES);
  D.Params.AddValue('MailBoxBarCode', MailBoxBarCode);
  D := SendRequest(D, False);
end;

function TevVmrRemoteProxy.RetrieveFile(
  const FileName: string): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_RETRIEVEFILE);
  D.Params.AddValue('FileName', FileName);
  D := SendRequest(D, False);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;



function TevVmrRemoteProxy.StoreFile(const FileName: string;
  const Content: IisStream; const AutoRename: Boolean): string;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_STOREFILE);
  D.Params.AddValue('FileName', FileName);
  D.Params.AddValue('Content', Content);
  D.Params.AddValue('AutoRename', AutoRename);
  D := SendRequest(D, False);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevVmrRemoteProxy.ProcessRelease(const TopBoxNbr: Integer;
  const ReleaseType: TVmrReleaseTypeSet; out UnprintedReports: TrwReportResults);

  function ReleaseTypeSetToString(s:TVmrReleaseTypeSet):string;
  var i:TVmrReleaseType;
      sep:string;
  begin
      sep := '';
      Result :='';
      for i := Low(TVmrReleaseType) to High(TVmrReleaseType) do
      begin
        if TVmrReleaseType(i) in s then
        begin
          Result := Result + sep+ IntToStr(Ord(i));
          sep :=';';
        end;
      end;
  end;

var
  D: IevDatagram;
  S: IisStream;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PROCESSRELEASE);
  D.Params.AddValue('TopBoxNbr', TopBoxNbr);
  D.Params.AddValue('ReleaseType', ReleaseTypeSetToString(ReleaseType));
  D := SendRequest(D, False);

  S := IInterface(D.Params.Value['UnprintedReports']) as IisStream;
  if Assigned(S) then
  begin
    S.Position := 0;
    UnprintedReports := TrwReportResults.Create(S);
  end
  else
    UnprintedReports := nil;
end;

procedure TevVmrRemoteProxy.ProcessRevert(const TopBoxNbr: Integer);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PROCESSREVERT);
  D.Params.AddValue('TopBoxNbr', TopBoxNbr);
  D := SendRequest(D, False);
end;

function TevVmrRemoteProxy.GetVMRReportList(const pCLNbr, pCONbr: Integer;
  const pCheckFromDate, pCheckToDate: TDateTime;
  const pVMRReportType : Integer): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_GETVMRREPORTLIST);
  D.Params.AddValue('pCLNbr', pCLNbr);
  D.Params.AddValue('pCONbr', pCONbr);
  D.Params.AddValue('pCheckFromDate', pCheckFromDate);
  D.Params.AddValue('pCheckToDate', pCheckToDate);
  D.Params.AddValue('pVMRReportType', pVMRReportType);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IEvDataSet;
end;

function TevVmrRemoteProxy.GetVMRReport(const aSelectedReportNbrs: IisStringList; const IsPrint : Boolean; const sComment : string): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_GETVMRREPORT);
  D.Params.AddValue('SelectedReportNbrs', aSelectedReportNbrs);
  D.Params.AddValue('IsPrint', IsPrint);
  D.Params.AddValue('sComment', sComment);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;

procedure TevVmrRemoteProxy.SetPrinterList(const ALocation : string; const APrinterList: IevVMRPrintersList; const IsDelete: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_SETPRINTERLIST);
  D.Params.AddValue('ALocation', ALocation);
  D.Params.AddValue('APrinterList', APrinterList);
  D.Params.AddValue('IsDelete', IsDelete);
  D := SendRequest(D, False);
end;

procedure TevVmrRemoteProxy.PurgeMailBox(const BoxNbr: Integer; const ForceDelete: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PURGEMAILBOX);
  D.Params.AddValue('BoxNbr', BoxNbr);
  D.Params.AddValue('ForceDelete', ForceDelete);
  D := SendRequest(D, False);
end;

procedure TevVmrRemoteProxy.PurgeClientMailBox(const ClientNbr: Integer;
  const ForceDelete: Boolean);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PURGECLIENTMAILBOX);
  D.Params.AddValue('ClientNbr', ClientNbr);
  D.Params.AddValue('ForceDelete', ForceDelete);
  D := SendRequest(D, False);
end;

procedure TevVmrRemoteProxy.PurgeMailBoxContentList(
  const SBMailBoxContentNbrs: IisStringList);
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_PURGEMAILBOXCONTENTLIST);
  D.Params.AddValue('SBMailBoxContentNbrs', SBMailBoxContentNbrs);
  D := SendRequest(D, False);
end;

function TevVmrRemoteProxy.GetRemoteVMRReportList(const ALocation:string): IevDataset;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_GETREMOTEVMRREPORTLIST);
  D.Params.AddValue('ALocation', ALocation);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IevDataset;
end;

function TevVmrRemoteProxy.GetRemoteVMRReport(const SBMailBoxContentNbr: integer): IisStream;
var
  D: IevDatagram;
begin
  D := CreateDatagram(METHOD_VMR_REMOTE_GETREMOTEVMRREPORT);
  D.Params.AddValue('SBMailBoxContentNbr', SBMailBoxContentNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStream;
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVmrRemoteProxy, IevVmrRemote, 'VMR Remote');

end.
