<Include>

<!-- Evolution Client Custom Variables -->
<?define brandDir = "..\Resources\Branding\Paydata\" ?>

<?define splashPictureType = "png" ?>
<?define splashPicture = $(var.brandDir)splash.$(var.splashPictureType) ?>

<?define fileEvolutionBrd = $(var.brandDir)Evolution.brd ?>

<?define regPorts="LAN:9500;T1:9901;DSL:9902;Modem:9903" ?>
<?define regHosts="request.paydata.com,request2.paydata.com" ?>
<?define regDefaultPort="9901" ?>
<?define regCompressionLevel="1" ?>

</Include>