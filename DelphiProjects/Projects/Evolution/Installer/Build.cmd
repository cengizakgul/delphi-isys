@ECHO OFF

IF "%2" NEQ "" GOTO start
ECHO Parameters: 
ECHO    1. WiX directory
ECHO    2. Application Version
exit 1

:start

SET pWiXDir=%1
SET pWiXDir=%pWiXDir:"=%
SET pAppVersion=%2
SET pAppVersion=%pAppVersion:"=%
SET pFilePrefix=_%pAppVersion%

cd .\Projects

ECHO Creating EvConnector.msi 
"%pWiXDir%\candle.exe" .\EvConnector.wxs -wx -out ..\..\..\..\Tmp\EvConnector.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\Tmp\EvConnector.wixobj -out ..\..\..\..\Bin\Evolution\EvConnector.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\Bin\Evolution\EvConnector.wixpdb
IF EXIST ..\..\..\..\Bin\Evolution\EvConnector%pFilePrefix%.msi del ..\..\..\..\Bin\Evolution\EvConnector%pFilePrefix%.msi > nul
ren ..\..\..\..\Bin\Evolution\EvConnector.msi EvConnector%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1


ECHO Creating EvMgmtCon.msi 
"%pWiXDir%\candle.exe" .\EvMgmtCon.wxs -wx -out ..\..\..\..\Tmp\EvMgmtCon.wixobj -dproductVersion="%pAppVersion%"
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\Tmp\EvMgmtCon.wixobj -out ..\..\..\..\Bin\Evolution\EvMgmtCon.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\Bin\Evolution\EvMgmtCon.wixpdb
IF EXIST ..\..\..\..\Bin\Evolution\EvMgmtCon%pFilePrefix%.msi del ..\..\..\..\Bin\Evolution\EvMgmtCon%pFilePrefix%.msi > nul
ren ..\..\..\..\Bin\Evolution\EvMgmtCon.msi EvMgmtCon%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvDeployMgr.msi
"%pWiXDir%\candle.exe" .\EvDeployMgr.wxs -wx -out ..\..\..\..\Tmp\EvDeployMgr.wixobj -dproductVersion="%pAppVersion%" 
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\Tmp\EvDeployMgr.wixobj -out ..\..\..\..\Bin\Evolution\EvDeployMgr.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\Bin\Evolution\EvDeployMgr.wixpdb
IF EXIST ..\..\..\..\Bin\Evolution\EvDeployMgr%pFilePrefix%.msi del ..\..\..\..\Bin\Evolution\EvDeployMgr%pFilePrefix%.msi > nul
ren ..\..\..\..\Bin\Evolution\EvDeployMgr.msi EvDeployMgr%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvClient.msi (Default)
"%pWiXDir%\candle.exe" .\EvClient.wxs -wx -out ..\..\..\..\Tmp\EvClient.wixobj -dproductVersion="%pAppVersion%" -dDefault
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\Tmp\EvClient.wixobj -out ..\..\..\..\Bin\Evolution\EvClient.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\Bin\Evolution\EvClient.wixpdb
IF EXIST ..\..\..\..\Bin\Evolution\EvoClient%pFilePrefix%.msi del ..\..\..\..\Bin\Evolution\EvoClient%pFilePrefix%.msi > nul
ren ..\..\..\..\Bin\Evolution\EvClient.msi EvoClient%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvClient.msi (Wells Fargo)
"%pWiXDir%\candle.exe" .\EvClient.wxs -wx -out ..\..\..\..\Tmp\EvClient.wixobj -dproductVersion="%pAppVersion%" -dWellsFargo
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\Tmp\EvClient.wixobj -out ..\..\..\..\Bin\Evolution\EvClient.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\Bin\Evolution\EvClient.wixpdb
IF EXIST ..\..\..\..\Bin\Evolution\WfEvoClient%pFilePrefix%.msi del ..\..\..\..\Bin\Evolution\WfEvoClient%pFilePrefix%.msi > nul
ren ..\..\..\..\Bin\Evolution\EvClient.msi WfEvoClient%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

ECHO Creating EvClient.msi (Paydata)
"%pWiXDir%\candle.exe" .\EvClient.wxs -wx -out ..\..\..\..\Tmp\EvClient.wixobj -dproductVersion="%pAppVersion%" -dPaydata
IF ERRORLEVEL 1 EXIT 1
"%pWiXDir%\light.exe" ..\..\..\..\Tmp\EvClient.wixobj -out ..\..\..\..\Bin\Evolution\EvClient.msi -ext WixUIExtension  -wx -sw1076
IF ERRORLEVEL 1 EXIT 1
del ..\..\..\..\Bin\Evolution\EvClient.wixpdb
IF EXIST ..\..\..\..\Bin\Evolution\PPSEvoClient%pFilePrefix%.msi del ..\..\..\..\Bin\Evolution\PPSEvoClient%pFilePrefix%.msi > nul
ren ..\..\..\..\Bin\Evolution\EvClient.msi PPSEvoClient%pFilePrefix%.msi > nul
IF ERRORLEVEL 1 EXIT 1

:end
