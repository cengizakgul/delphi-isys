<Include>

<?ifdef customActionDLL ?>

<!-- Splash -->
<?ifndef splashPictureType ?>
  <?define splashPictureType = "png" ?>
  <?define splashPicture = ..\Resources\Branding\Default\splash.$(var.splashPictureType) ?>
<?endif ?>

	<Binary Id="SplashPicture" SourceFile="$(var.splashPicture)" />
	<Property Id="SplashPictureType" Value="$(var.splashPictureType)"/>
	<Binary Id="CustomActionDLL" SourceFile="$(var.customActionDLL)" />
	<CustomAction Id="ShowSplash" BinaryKey="CustomActionDLL" DllEntry="ShowSplash" Impersonate="yes"/>
	
	<InstallUISequence>
		<Custom Action="ShowSplash" Sequence="1" />
	</InstallUISequence>

<?endif ?>

        
<!-- UI -->
	<Property Id="WIXUI_INSTALLDIR" Value="INSTALLDIR" />
	<WixVariable Id="WixUILicenseRtf" Value="$(var.resourceFileDir)EULA.rtf" />
	<UI Id="GUI">
		<UIRef Id="WixUI_InstallDir" />
	</UI>
	<UIRef Id="WixUI_ErrorProgressText" />

</Include>