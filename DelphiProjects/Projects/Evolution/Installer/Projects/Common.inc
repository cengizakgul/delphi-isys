<Include>
	<!-- Common items -->
	<Media Id="1" Cabinet="isystems.cab" CompressionLevel="high" EmbedCab="yes" DiskPrompt="Disk #1"/>
	<Property Id="DiskPrompt" Value="$(var.productName) [1]"/>
	<Property Id="ROOTDRIVE" Value="$(var.defaultVolume)"/>        

	<!-- Uninstall previous version -->
	<Upgrade Id="$(var.productUpgradeCode)">
		<UpgradeVersion Minimum="1.0.0.0" Maximum="99.0.0.0" OnlyDetect="no" Property="PREVIOUSFOUND" />
	</Upgrade>

	<InstallExecuteSequence>
		<RemoveExistingProducts After="InstallInitialize" />
	</InstallExecuteSequence>	

	<!-- Add Uninstall registry items -->
    <Icon Id="ProductIcon" SourceFile="$(var.productIcon)" />	
	
	<Property Id="ARPCONTACT" Value="support@isystemsllc.com" />
	<Property Id="ARPPRODUCTICON" Value="ProductIcon" />
	<Property Id="ARPURLINFOABOUT" Value="www.isystemsllc.com" />
	<Property Id="ARPURLUPDATEINFO" Value="http://www.isystemsllc.com/Contact.html" />
	<Property Id="ARPHELPTELEPHONE" Value="http://www.isystemsllc.com/Contact.html" />
	
	<CustomAction Id="SetARPINSTALLLOCATION" Property="ARPINSTALLLOCATION" Value="[INSTALLDIR]" />

    <InstallExecuteSequence>
		<Custom Action="SetARPINSTALLLOCATION" After="InstallValidate" />
	</InstallExecuteSequence>
	
	<!-- Find previous install location -->
	<Property Id="INSTALLDIR">
		<RegistrySearch Id="FindInstallLocation"
			Root="HKLM"
			Key="Software\Microsoft\Windows\CurrentVersion\Uninstall\[PREVIOUSFOUND]"
			Name="InstallLocation"
			Type="raw" />
	</Property>

	<InstallUISequence>
		<AppSearch After="FindRelatedProducts"/>
	</InstallUISequence>
 </Include>