unit Custom;

interface

uses Windows, Classes, SplashFrm, MsiUtils, JwaMsi, Registry;

function ShowSplash(const hInstall: MSIHANDLE): uint; stdcall;

implementation

function ShowSplash(const hInstall: MSIHANDLE): uint; stdcall;
var
  Frm: TSplash;
  M: IMsiHelper;
  PictData: TMemoryStream;
  sPictType: String;
begin
  Frm := TSplash.Create(nil);
  try
    M := GetMSIHelper(hInstall);
    sPictType := M.GetProperty('SplashPictureType');
    PictData := TMemoryStream.Create;
    try
      M.GetBinaryData('Binary', 'Data', 'Name', 'SplashPicture', PictData);
      if PictData.Size > 0 then
        Frm.ShowPicture(PictData, sPictType);
    finally
      PictData.Free;
    end;
  finally
    Frm.Free;
  end;

  Result := GetExitCode();
end;

// Functions are exposed to installer
exports ShowSplash;

end.
