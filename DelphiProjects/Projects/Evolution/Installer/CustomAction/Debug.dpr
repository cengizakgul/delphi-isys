program Debug;

uses
  Forms,
  JwaMSI,
  Custom;

{$R *.res}

var
  hInstaller: MSIHANDLE;

begin
  Application.Initialize;
  MsiOpenPackage('D:\ISystems\Evolution\Installer\MSI\EvClient.msi', hInstaller);
  ShowSplash(hInstaller);
  MsiCloseHandle(hInstaller);
end.
