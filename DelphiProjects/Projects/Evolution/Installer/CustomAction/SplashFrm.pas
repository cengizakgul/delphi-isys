unit SplashFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  pngimage, DateUtils, ExtCtrls;

const
  CM_SHOW_SPLASH = WM_USER + 1;

type
  TSplash = class(TForm)
    pnlSplash: TPanel;
    SplashImage: TImage;
    procedure FormShow(Sender: TObject);
  private
    function  MakeWindowTransparent(nAlpha: Integer): Boolean;
    procedure CmShowSplash(var Message: TMessage); message CM_SHOW_SPLASH;
  public  
    procedure ShowPicture(const APictData: TMemoryStream; const AType: String);
  end;

implementation

{$R *.dfm}

function TSplash.MakeWindowTransparent(nAlpha: Integer): Boolean;
type
  TSetLayeredWindowAttributes = function(hwnd: HWND; crKey: COLORREF; bAlpha: Byte;
    dwFlags: Longint): Longint; 
  stdcall;
const
  // Use crKey as the transparency color.
  LWA_COLORKEY = 1;
  // Use bAlpha to determine the opacity of the layered window..
  LWA_ALPHA     = 2;
  WS_EX_LAYERED = $80000;
var
  hUser32: HMODULE;
  SetLayeredWindowAttributes: TSetLayeredWindowAttributes;
begin
  Result := False;
  // Here we import the function from USER32.DLL
  hUser32 := GetModuleHandle('USER32.DLL');
  if hUser32 <> 0 then
  begin @SetLayeredWindowAttributes := GetProcAddress(hUser32, 'SetLayeredWindowAttributes');
    // If the import did not succeed, make sure your app can handle it!
    if @SetLayeredWindowAttributes <> nil then
    begin
      // Check the current state of the dialog, and then add the WS_EX_LAYERED attribute
      SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle, GWL_EXSTYLE) or WS_EX_LAYERED);
      // The SetLayeredWindowAttributes function sets the opacity and
      // transparency color key of a layered window
      SetLayeredWindowAttributes(Handle, 0, Trunc((255 / 100) * (100 - nAlpha)), LWA_ALPHA);
      Result := True;
    end;
  end;
end;

procedure TSplash.FormShow(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  MakeWindowTransparent(100);
  PostMessage(Handle, CM_SHOW_SPLASH, 0, 0);
end;

procedure TSplash.CmShowSplash(var Message: TMessage);
var
  i: Integer;
  d: TDateTime;
begin
  for i := 100 downto 0 do
  begin
    MakeWindowTransparent(i);
    Application.ProcessMessages;
    BringToFront;
    Sleep(10);
  end;

  d := Now;
  while MilliSecondsBetween(Now, d) < 4000 do
  begin
    Application.ProcessMessages;
    BringToFront;
    Sleep(100);
  end;

  Close;
end;

procedure TSplash.ShowPicture(const APictData: TMemoryStream; const AType: String);

  function GetTempDir: string;
  var
    Buf: array[0..2047] of char;
  begin
    GetTempPath(2048, @Buf);
    Result := Buf;
  end;

var
  sFile: String;
begin
  sFile := GetTempDir + 'isInstaller_' + IntToStr(GetCurrentProcessId) + '.' + AType;
  APictData.SaveToFile(sFile);
  try
    SplashImage.Picture.LoadFromFile(sFile);
  finally
    DeleteFile(sFile);
  end;

  ShowModal;
end;

end.
