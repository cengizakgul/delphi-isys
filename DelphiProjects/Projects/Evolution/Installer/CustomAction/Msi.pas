{
    Created by CodeGroup A/S. http://www.costestimation.com

    Basically this file contains a Delphi/Pascal version of the Msi.h header file
    included with Visual Studio.

    Method descriptions is taken from the MSDN Library

    -----------------------------------------------------------------------------
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
}

unit Msi;

interface

uses Windows;

const
    ERROR_INSTALL_FAILURE = 1603; // Fatal error during installation.

    MSIRUNMODE_ADMIN = 0; // admin mode install, else product install
    MSIRUNMODE_ADVERTISE = 1; // installing advertisements, else installing or updating product
    MSIRUNMODE_MAINTENANCE = 2; // modifying an existing installation, else new installation
    MSIRUNMODE_ROLLBACKENABLED = 3; // rollback is enabled
    MSIRUNMODE_LOGENABLED = 4; // log file active, enabled prior to install session
    MSIRUNMODE_OPERATIONS = 5; // spooling execute operations, else in determination phase
    MSIRUNMODE_REBOOTATEND = 6; // reboot needed after successful installation (settable)
    MSIRUNMODE_REBOOTNOW = 7; // reboot needed to continue installation (settable)
    MSIRUNMODE_CABINET = 8; // installing files from cabinets and files using Media table
    MSIRUNMODE_SOURCESHORTNAMES = 9; // source LongFileNames suppressed via PID_MSISOURCE summary property
    MSIRUNMODE_TARGETSHORTNAMES = 10; // target LongFileNames suppressed via SHORTFILENAMES property
    MSIRUNMODE_RESERVED11 = 11; // future use
    MSIRUNMODE_WINDOWS9X = 12; // operating systems is Windows9X, else Windows NT
    MSIRUNMODE_ZAWENABLED = 13; // operating system supports demand installation
    MSIRUNMODE_RESERVED14 = 14; // future use
    MSIRUNMODE_RESERVED15 = 15; // future use
    MSIRUNMODE_SCHEDULED = 16; // custom action call from install script execution
    MSIRUNMODE_ROLLBACK = 17; // custom action call from rollback execution script
    MSIRUNMODE_COMMIT = 18; // custom action call from commit execution script

type
    tagINSTALLMESSAGE = (
        INSTALLMESSAGE_FATALEXIT = 0, // premature termination, possibly fatal OOM
        INSTALLMESSAGE_ERROR = 1, // formatted error message
        INSTALLMESSAGE_WARNING = 2, // formatted warning message
        INSTALLMESSAGE_USER = 3, // user request message
        INSTALLMESSAGE_INFO = 4, // informative message for log
        INSTALLMESSAGE_FILESINUSE = 5, // list of files in use that need to be replaced
        INSTALLMESSAGE_RESOLVESOURCE = 6, // request to determine a valid source location
        INSTALLMESSAGE_OUTOFDISKSPACE = 7, // insufficient disk space message
        INSTALLMESSAGE_ACTIONSTART = 8, // start of action: action name & description
        INSTALLMESSAGE_ACTIONDATA = 9, // formatted data associated with individual action item
        INSTALLMESSAGE_PROGRESS = 10, // progress gauge info: units so far, total
        INSTALLMESSAGE_COMMONDATA = 11, // product info for dialog: language Id, dialog caption
        INSTALLMESSAGE_INITIALIZE = 12, // sent prior to UI initialization, no string data
        INSTALLMESSAGE_TERMINATE = 13, // sent after UI termination, no string data
        INSTALLMESSAGE_SHOWDIALOG = 14); // sent prior to display or authored dialog or wizard
    INSTALLMESSAGE = tagINSTALLMESSAGE;

    tagINSTALLSTATE = (
        INSTALLSTATE_NOTUSED = -7, // component disabled
        INSTALLSTATE_BADCONFIG = -6, // configuration data corrupt
        INSTALLSTATE_INCOMPLETE = -5, // installation suspended or in progress
        INSTALLSTATE_SOURCEABSENT = -4, // run from source, source is unavailable
        INSTALLSTATE_MOREDATA = -3, // return buffer overflow
        INSTALLSTATE_INVALIDARG = -2, // invalid function argument
        INSTALLSTATE_UNKNOWN = -1, // unrecognized product or feature
        INSTALLSTATE_BROKEN = 0, // broken
        INSTALLSTATE_ADVERTISED = 1, // advertised feature
        INSTALLSTATE_REMOVED = 1, // component being removed (action state, not settable)
        INSTALLSTATE_ABSENT = 2, // uninstalled (or action state absent but clients remain)
        INSTALLSTATE_LOCAL = 3, // installed on local drive
        INSTALLSTATE_SOURCE = 4, // run from source, CD or net
        INSTALLSTATE_DEFAULT = 5); // use default, local or source
    INSTALLSTATE = tagINSTALLSTATE;

// MSI function declarations

/// <summary>
///    The MsiGetProperty function gets the value for an installer property.
/// </summary>
/// <param name="hInstall">
///    [in] Handle to the installation provided to a DLL custom action or obtained through MsiOpenPackage, MsiOpenPackageEx, or MsiOpenProduct
/// </param>
/// <param name="szName">
///    [in] A null-terminated string that specifies the name of the property.
/// </param>
/// <param name="szValueBuf">
///    [out] Pointer to the buffer that receives the null terminated string containing the value of the property.
///    Do not attempt to determine the size of the buffer by passing in a null (value=0) for szValueBuf. You can
///    get the size of the buffer by passing in an empty string (for example ""). The function will then return
///    ERROR_MORE_DATA and pchValueBuf will contain the required buffer size in TCHARs, not including the terminating
///    null character. On return of ERROR_SUCCESS, pcchValueBuf contains the number of TCHARs written to the buffer,
///    not including the terminating null character.
/// </param>
/// <param name="pchValueBuf">
///    [in, out] Pointer to the variable that specifies the size, in TCHARs, of the buffer pointed to by the variable
///    szValueBuf. When the function returns ERROR_SUCCESS, this variable contains the size of the data copied to
///    szValueBuf, not including the terminating null character. If szValueBuf is not not large enough, the function
///    returns ERROR_MORE_DATA and stores the required size, not including the terminating null character, in the
///    variable pointed to by pchValueBuf.
/// </param>
function MsiGetProperty(hInstall: integer; szName, szValueBuf: PChar; var pchValueBuf: integer): word; stdcall; external 'Msi.dll' name 'MsiGetPropertyA';

/// <summary>The MsiSetProperty function sets the value for an installation property.</summary>
/// <param name="hInstall">[in] Handle to the installation provided to a DLL custom action or obtained through MsiOpenPackage, MsiOpenPackageEx, or MsiOpenProduct.</param>
/// <param name="szName">[in] Specifies the name of the property. Case-sensitive</param>
/// <param name="szValue">[in] Specifies the value of the property. NULL to undefine property</param>
/// <returns>ERROR_FUNCTION_FAILED, ERROR_INVALID_HANDLE, ERROR_INVALID_PARAMETER or ERROR_SUCCESS</returns>
function MsiSetProperty(hInstall: integer; szName, szValue: PChar): Word; stdcall; external 'Msi.dll' name 'MsiSetPropertyA';

/// <summary>The MsiCloseHandle function closes an open installation handle.</summary>
/// <param name="hAny">[in] Specifies any open installation handle.</param>
/// <returns>ERROR_INVALID_HANDLE or ERROR_SUCCESS</returns>
function MsiCloseHandle(hAny: integer): integer; stdcall; external 'Msi.dll' name 'MsiCloseHandle';

/// <summary>The MsiGetFeatureState function gets the requested state of a feature.</summary>
/// <param name="hInstall">[in] Handle to the installation provided to a DLL custom action or obtained through MsiOpenPackage, MsiOpenPackageEx, or MsiOpenProduct.</param>
/// <param name="szFeature">[in] Specifies the feature name within the product.</param>
/// <param name="piInstalled">
///    [out] Specifies the returned current installed state. This parameter must not be null. This parameter can be one of the following values:
///    INSTALLSTATE_BADCONFIG, INSTALLSTATE_INCOMPLETE, INSTALLSTATE_SOURCEABSENT, INSTALLSTATE_MOREDATA, INSTALLSTATE_INVALIDARG,
///    INSTALLSTATE_UNKNOWN, INSTALLSTATE_BROKEN, INSTALLSTATE_ADVERTISED, INSTALLSTATE_ABSENT, INSTALLSTATE_LOCAL,
///    INSTALLSTATE_SOURCE, INSTALLSTATE_DEFAULT
/// </param>
/// <param name="piAction">[out] Receives the action taken during the installation session. This parameter must not be null. For return values, see piInstalled.</param>
/// <returns>ERROR_INVALID_HANDLE, ERROR_SUCCESS, ERROR_UNKNOWN_FEATURE</returns>
function MsiGetFeatureState(hInstall: integer; szFeature: PChar; var piInstalled, piAction: word): integer; stdcall; external 'Msi.dll' name 'MsiGetFeatureStateA';

/// <summary>
///    The MsiCreateRecord function creates a new record object with the specified number of fields. This function returns a handle that should be
///    closed using MsiCloseHandle.
/// </summary>
/// <param name="cParams">[in] Specifies the number of fields the record will have. The maximum number of fields in a record is limited to 65535.</param>
/// <returns>
///    If the function succeeds, the return value is handle to a new record object.
///    If the function fails, the return value is null.
/// </returns>
function MsiCreateRecord(cParams: word): integer; stdcall; external 'Msi.dll';

/// <summary>The MsiProcessMessage function sends an error record to the installer for processing.</summary>
/// <param name="hInstall">[in] Handle to the installation provided to a DLL custom action or obtained through MsiOpenPackage, MsiOpenPackageEx, or MsiOpenProduct.</param>
/// <param name="eMessageType">
///    [in] The eMessage parameter must be a value specifying one of the following message types. To display a message box with
///    push buttons or icons, use OR-operators to add INSTALLMESSAGE_ERROR, INSTALLMESSAGE_WARNING, or INSTALLMESSAGE_USER to
///    the standard message box styles used by the MessageBox and MessageBoxEx functions.
/// </param>
/// <param name="hRecord">[in] Handle to a record containing message format and data.</param>
/// <returns>
///    -1: An invalid parameter or handle was supplied.
///     0: No action was taken.
///    IDABORT: The process was stopped.
///    IDCANCEL: The process was canceled.
///    IDIGNORE: The process was ignored.
///    IDOK: The function succeeded.
///    IDNO: No.
///    IDRETRY: Retry.
///    IDYES: Yes.
/// </returns>
function MsiProcessMessage(hInstall: integer; eMessageType: INSTALLMESSAGE; hRecord: integer): integer; stdcall; external 'Msi.dll';

/// <summary>The MsiRecordSetString function copies a string into the designated field.</summary>
/// <param name="hRecord">[in] Handle to the record.</param>
/// <param name="iField">[in] Specifies the field of the record to set.</param>
/// <param name="szValue">[in] Specifies the string value of the field.</param>
/// <returns>
///    ERROR_INVALID_HANDLE: An invalid or inactive handle was supplied.
///    ERROR_INVALID_PARAMETER: An invalid parameter was passed to the function.
///    ERROR_SUCCESS: The function succeeded.
/// </returns>
function MsiRecordSetString(hRecord: integer; iField: integer; szValue: PChar): integer; stdcall; external 'Msi.dll' name 'MsiRecordSetStringA';

/// <summary>The MsiRecordGetString function returns the string value of a record field.</summary>
/// <param name="hRecord">[in] Handle to the record.</param>
/// <param name="iField">[in] Specifies the field requested.</param>
/// <param name="szValueBuf">
///    [out] Pointer to the buffer that receives the null terminated string containing the value of the record field.
///    Do not attempt to determine the size of the buffer by passing in a null (value=0) for szValueBuf. You can get
///    the size of the buffer by passing in an empty string (for example ""). The function then returns ERROR_MORE_DATA
///    and pchValueBuf contains the required buffer size in TCHARs, not including the terminating null character. On
///    return of ERROR_SUCCESS, pchValueBuf contains the number of TCHARs written to the buffer, not including the
///    terminating null character.
/// </param>
/// <param name="pcchValueBuf">
///    [in, out] Pointer to the variable that specifies the size, in TCHARs, of the buffer pointed to by the variable
///    szValueBuf. When the function returns ERROR_SUCCESS, this variable contains the size of the data copied to
///    szValueBuf, not including the terminating null character. If szValueBuf is not large enough, the function
///    returns ERROR_MORE_DATA and stores the required size, not including the terminating null character, in the
///    variable pointed to by pchValueBuf.
/// </param>
/// <returns>
///    The MsiRecordGetString function returns one of the following values:
///     ERROR_INVALID_HANDLE: An invalid or inactive handle was supplied.
///     ERROR_INVALID_PARAMETER: An invalid parameter was supplied.
///     ERROR_MORE_DATA: The provided buffer was too small to hold the entire value.
///     ERROR_SUCCESS: The function succeeded.
/// </returns>
function MsiRecordGetString(hRecord: integer; iField: integer; szValueBuf: PChar; var pcchValueBuf: integer): integer; stdcall; external 'Msi.dll' name 'MsiRecordGetStringA';

/// <summary>The MsiGetTargetPath function returns the full target path for a folder in the Directory table.</summary>
/// <param name="hInstall">[in] Handle to the installation provided to a DLL custom action or obtained through MsiOpenPackage, MsiOpenPackageEx, or MsiOpenProduct.</param>
/// <param name="szFolder">
///    [in] A null-terminated string that specifies a record of the Directory table. If the directory is a root directory,
///    this can be a value from the DefaultDir column. Otherwise it must be a value from the Directory column.
/// </param>
/// <param name="szPathBuf">
///    [out] Pointer to the buffer that receives the null terminated full target path. Do not attempt to determine
///    the size of the buffer by passing in a null (value=0) for szPathBuf. You can get the size of the buffer by passing
///    in an empty string (for example ""). The function then returns ERROR_MORE_DATA and pcchPathBuf contains the required
///    buffer size in TCHARs, not including the terminating null character. On return of ERROR_SUCCESS, pcchPathBuf contains
///    the number of TCHARs written to the buffer, not including the terminating null character.
/// </param>
/// <param name="pcchPathBuf">
///    [in, out] Pointer to the variable that specifies the size, in TCHARs, of the buffer pointed to by the variable
///    szPathBuf When the function returns ERROR_SUCCESS, this variable contains the size of the data copied to szPathBuf,
///    not including the terminating null character. If szPathBuf is not large enough, the function returns ERROR_MORE_DATA
///    and stores the required size, not including the terminating null character, in the variable pointed to by pcchPathBuf.
/// </param>
/// <returns>
///    The MsiGetTargetPath function returns the following values:
///     ERROR_DIRECTORY: The directory specified was not found in the Directory table.
///     ERROR_INVALID_HANDLE: An invalid or inactive handle was supplied.
///     ERROR_INVALID_PARAMETER: An invalid parameter was supplied.
///     ERROR_MORE_DATA: The buffer passed in was too small to hold the entire value.
///     ERROR_SUCCESS: The function succeeded.
/// </returns>
function MsiGetTargetPath(hInstall: integer; szFolder, szPathBuf: PChar; var pcchPathBuf: integer): integer; stdcall; external 'Msi.dll' name 'MsiGetTargetPathA';


function MsiRecordReadStream(hRecord: integer; iField: UINT; var pcchValueBuf: PChar; pcbDataBuf: PDWORD): integer; stdcall; external 'Msi.dll' name 'MsiRecordReadStream ';

implementation

end.

