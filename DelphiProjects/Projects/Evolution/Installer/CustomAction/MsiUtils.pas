unit MsiUtils;

interface

uses Classes, JwaMsi, JwaMsiQuery, JwaWinType, SysUtils, JwaWinUser, JwaWinError, JwaWinBase;

type
  IMsiHelper = interface
  ['{708535AF-43B6-484C-BBE5-22C00D917934}']
    function  GetProperty(const propertyName: string): string;
    procedure SetProperty(const propertyName, value: string);
    function  GetTargetPath(const msiDirectory: string): string;
    function  FeatureBeingInstalled(const feature: string): boolean;
    function  FeatureBeingUninstalled(const feature: string): boolean;
    procedure LogWriteLine(const msg: string);
    procedure LogCommit;
    procedure LogMessageBox(const msg: string);
    procedure GetBinaryData(const ATable, AField, AKeyField, AKeyValue: string; const AData: TStream);
  end;

function GetMSIHelper(const hInstall: MSIHANDLE): IMsiHelper;
function GetExitCode: uint;

implementation

type
  TMsiHelper = class(TInterfacedObject, IMsiHelper)
  private
      FhInstall: MSIHANDLE;
      FlogText: string;
  public
    constructor Create(const hInstall: MSIHANDLE);

    function  GetProperty(const propertyName: string): string;
    procedure SetProperty(const propertyName, value: string);
    function  GetTargetPath(const msiDirectory: string): string;
    function  FeatureBeingInstalled(const feature: string): boolean;
    function  FeatureBeingUninstalled(const feature: string): boolean;
    procedure LogWriteLine(const msg: string);
    procedure LogCommit;
    procedure LogMessageBox(const msg: string);
    procedure GetBinaryData(const ATable, AField, AKeyField, AKeyValue: string; const AData: TStream);
  end;


function GetMSIHelper(const hInstall: MSIHANDLE): IMsiHelper;
begin
  Result := TMsiHelper.Create(hInstall)
end;


function GetExitCode: uint;
begin
  // - release build -> always return success, or else setup is rolled back
  Result := ERROR_SUCCESS;
end;


{ TMsiHelper }

constructor TMsiHelper.Create(const hInstall: MSIHANDLE);
begin
  FhInstall := hInstall;
end;

function TMsiHelper.FeatureBeingInstalled(const feature: string): boolean;
var
  installed: Integer;
  action: Integer;
begin
  MsiGetFeatureState(FhInstall, PChar(feature), installed, action);
  Result := (INSTALLSTATE(action) = INSTALLSTATE_LOCAL);
end;

function TMsiHelper.FeatureBeingUninstalled(const feature: string): boolean;
var
  installed: Integer;
  action: Integer;
begin
  MsiGetFeatureState(FhInstall, PChar(feature), installed, action);

  Result := (INSTALLSTATE(action) = INSTALLSTATE_ABSENT);
end;

function TMsiHelper.GetProperty(const propertyName: string): string;
var
  value: array[0..512] of char;
  bufLength: Cardinal;
  res: word;
begin
  res := MsiGetProperty(FhInstall, PChar(propertyName), value, bufLength);

  case res of
      ERROR_INVALID_HANDLE:
          raise Exception.Create('An invalid or inactive handle was supplied.');

      ERROR_INVALID_PARAMETER:
          raise Exception.Create('An invalid parameter was passed to the function.');

      ERROR_MORE_DATA:
          raise Exception.Create('The provided buffer was too small to hold the entire value.'); // todo: should only happen when asking for length of value

      ERROR_SUCCESS:
          result := Copy(value, 1, bufLength);
  else
      raise Exception.Create('Unknown returnvalue from MsiGetProperty() method.');
  end;
end;

function TMsiHelper.GetTargetPath(const msiDirectory: string): string;
var
  value: array[0..512] of char;
  bufLength: Cardinal;
  res: word;
begin
  res := MsiGetTargetPath(FhInstall, PChar(msiDirectory), value, bufLength);

  case res of
      ERROR_DIRECTORY:
          raise Exception.Create('The directory specified was not found in the Directory table.');

      ERROR_INVALID_HANDLE:
          raise Exception.Create('An invalid or inactive handle was supplied.');

      ERROR_INVALID_PARAMETER:
          raise Exception.Create('An invalid parameter was passed to the function.');

      ERROR_MORE_DATA:
          raise Exception.Create('The provided buffer was too small to hold the entire value.'); // todo: should only happen when asking for length of value

      ERROR_SUCCESS:
          result := Copy(value, 1, bufLength);

  else
    raise Exception.Create('Unknown returnvalue from MsiGetTargetPath() method.');
  end;
end;

procedure TMsiHelper.SetProperty(const propertyName, value: string);
begin
  MsiSetProperty(FhInstall, PChar(propertyName), PChar(value));
end;

procedure TMsiHelper.LogWriteLine(const msg: string);
const
  NewLine = #13#10;
begin
  if Length(FlogText) = 0 then
    FlogText := msg
  else
    FlogText := FlogText + NewLine + msg;
end;

procedure TMsiHelper.LogCommit;
var
  hRecord: integer;
begin
  if Length(FlogText) = 0 then
      exit;

  hRecord := MsiCreateRecord(1);
  try
    MsiRecordSetString(hRecord, 0, PChar(FlogText));
    MsiProcessMessage(FhInstall, INSTALLMESSAGE(-1), hRecord); // INSTALLMESSAGE(INSTALLMESSAGE_INFO)
  finally
    MsiCloseHandle(hRecord);
  end;
end;

procedure TMsiHelper.LogMessageBox(const msg: string);
var
  hRecord: integer;
begin
  if Length(msg) = 0 then
      exit;

  hRecord := MsiCreateRecord(1);
  try
    MsiRecordSetString(hRecord, 0, PChar(msg));
    MsiProcessMessage(FhInstall, INSTALLMESSAGE(INSTALLMESSAGE_USER), hRecord);
  finally
    MsiCloseHandle(hRecord);
  end;
end;

procedure TMsiHelper.GetBinaryData(const ATable, AField, AKeyField, AKeyValue: string; const AData: TStream);
var
  phDataBase: MSIHANDLE;
  hView: MSIHANDLE;
  SQL: String;
  ExitCode: UINT;
  hRec: Cardinal;
  szValueBuf: PAnsiChar;
  pcchValueBuf: Cardinal;
begin
  phDataBase := MsiGetActiveDatabase(FhInstall);
  if phDataBase = 0 then
     Exit;

  SQL := 'SELECT ' + AField + ' FROM ' + ATable + ' WHERE ' + AKeyField + ' = ''' + AKeyValue + '''';

  ExitCode := MsiDatabaseOpenView(phDataBase, PAnsiChar(SQL), hView);
  if ExitCode <> ERROR_SUCCESS then
    RaiseLastOSError;

  hRec := MsiCreateRecord(1);

  ExitCode := MsiViewExecute(hView, hRec);
  if ExitCode <> ERROR_SUCCESS then
    Exit;

  if MsiViewFetch(hView, hRec) <> ERROR_NO_MORE_ITEMS then
  begin
    pcchValueBuf := 0;
    szValueBuf := nil;
    ExitCode := MsiRecordReadStream(hRec, 1, szValueBuf, pcchValueBuf);
    if ExitCode <> ERROR_SUCCESS then
      exit;

    GetMem(szValueBuf, pcchValueBuf);
    try
      ExitCode := MsiRecordReadStream(hRec, 1, szValueBuf, pcchValueBuf);
      if ExitCode <> ERROR_SUCCESS then
        exit;

      AData.WriteBuffer(szValueBuf^, pcchValueBuf);
    finally
//      FreeMem(szValueBuf);
    end;
  end;
end;

end.
