unit EvRPController;

interface

uses Classes, SysUtils, isBaseClasses,  EvTransportShared, EvConsts,
     isBasicUtils, EvStreamUtils, evRemoteMethods, isAppIDs, isLogFile,
     EvControllerInterfaces, EvTransportInterfaces, EvMainboard, EvBasicUtils;

type
   TevRPController = class(TevCustomTCPServer, IevAppController)
   private
   protected
     procedure DoOnConstruction; override;
     function  CreateDispatcher: IevDatagramDispatcher; override;
   end;


implementation

uses evRPServerMod;

type
  TevRPControllerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmHardware(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmModules(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLoadStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStackInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetExecStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetAppTempFolder(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetAppTempFolder(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetMaintenanceDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetMaintenanceDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure RegisterHandlers; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;
  end;


{ TevRPControllerDatagramDispatcher }

procedure TevRPControllerDatagramDispatcher.dmHardware(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := RPControl.GetHardware;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRPControllerDatagramDispatcher.dmModules(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := RPControl.GetModules;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRPControllerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RPCONTROLLER_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_RPCONTROLLER_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_RPCONTROLLER_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_RPCONTROLLER_GETHARDWARE, dmHardware);
  AddHandler(METHOD_RPCONTROLLER_GETMODULES, dmModules);
  AddHandler(METHOD_RPCONTROLLER_GETLOADSTATUS, dmGetLoadStatus);
  AddHandler(METHOD_RPCONTROLLER_GETSTACKINFO, dmGetStackInfo);
  AddHandler(METHOD_RPCONTROLLER_GETEXECSTATUS, dmGetExecStatus);
  AddHandler(METHOD_RPCONTROLLER_GETAPPTEMPFOLDER, dmGetAppTempFolder);
  AddHandler(METHOD_RPCONTROLLER_SETAPPTEMPFOLDER, dmSetAppTempFolder);
  AddHandler(METHOD_RPCONTROLLER_SETMAINTENANCEDB, dmSetMaintenanceDB);
  AddHandler(METHOD_RPCONTROLLER_GETMAINTENANCEDB, dmGetMaintenanceDB);
  AddHandler(METHOD_RPCONTROLLER_GETLOGFILETHRESHOLDINFO, dmGetLogFileThresholdInfo);
  AddHandler(METHOD_RPCONTROLLER_SETLOGFILETHRESHOLDINFO, dmSetLogFileThresholdInfo);
end;


function TevRPControllerDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoDeployMgrAppInfo.AppID);
end;

procedure TevRPControllerDatagramDispatcher.dmGetLoadStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RPControl.GetLoadStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRPControllerDatagramDispatcher.dmGetExecStatus(const ADatagram: IevDatagram;
  out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := RPControl.GetExecStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRPControllerDatagramDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RPControl.GetLogEventDetails(
    ADatagram.Params.Value['EventNbr']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRPControllerDatagramDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  PageCount: Integer;
begin
  Res := RPControl.GetLogEvents(
    ADatagram.Params.Value['PageNbr'],
    ADatagram.Params.Value['EventsPerPage'],
    PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevRPControllerDatagramDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

  Res := RPControl.GetFilteredLogEvents(Query);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRPControllerDatagramDispatcher.dmGetAppTempFolder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RPControl.GetAppTempFolder);
end;

procedure TevRPControllerDatagramDispatcher.dmSetAppTempFolder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RPControl.SetAppTempFolder(ADatagram.Params.Value['ATempFolder']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRPControllerDatagramDispatcher.dmGetMaintenanceDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RPControl.GetMaintenanceDB);
end;

procedure TevRPControllerDatagramDispatcher.dmSetMaintenanceDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RPControl.SetMaintenanceDB(IInterface(ADatagram.Params.Value['ADBListByDomains']) as IisListOfValues,
    ADatagram.Params.Value['AMaintenance']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRPControllerDatagramDispatcher.AfterDispatchDatagram(
  const ADatagramIn, ADatagramOut: IevDatagram);
begin
  inherited;
  Mainboard.ContextManager.RestoreThreadContext;
end;

procedure TevRPControllerDatagramDispatcher.BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader);
var
  Session: IevSession;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;

  Session := FindSessionByID(ADatagramHeader.SessionID);
  if Session.State = ssActive then
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
end;

procedure TevRPControllerDatagramDispatcher.dmGetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  RPControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.Params.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TevRPControllerDatagramDispatcher.dmSetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RPControl.SetLogFileThresholdInfo(ADatagram.Params.Value['APurgeRecThreshold'],
    ADatagram.Params.Value['APurgeRecCount']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRPControllerDatagramDispatcher.dmGetStackInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RPControl.GetStackInfo);
end;

{ TevRPController }

function TevRPController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRPControllerDatagramDispatcher.Create;
end;

procedure TevRPController.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(RPController_Port));
  SetIPAddress('127.0.0.1');
end;

end.
