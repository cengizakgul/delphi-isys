unit evRPServerMod;

interface

uses SysUtils, Windows, isBaseClasses, EvCommonInterfaces, isSocket, isStreamTCP, isTransmitter,
     EvTransportShared, EvMainboard, EvTransportDatagrams, Dialogs, EvTypes, evRemoteMethods,
     isBasicUtils, evContext, SReportSettings, EvStreamUtils,  EvDataSet, isAppIDs,
     Classes, EvConsts, EvInitApp, ISZippingRoutines, evCommonDatagramDispatcher, EvTransportInterfaces,
     EvControllerInterfaces, isThreadManager, isCPUStat, isLogFile, isSettings, EvBasicUtils,
     EvUtils, isPerfScore, isErrorUtils, isExceptions, ISBasicClasses;

function RPControl: IevRPControl;

implementation

uses EvRPController;

type
  IevRPTCPServer = interface(IevTCPServer)
  ['{240A4610-BD24-45BF-BE4B-2151BF69547C}']
     function Control:  IevRPControl;
     function MaxScore: Cardinal;
  end;


  TevTCPServer = class(TevCustomTCPServer, IevTCPServer, IevRPTCPServer)
  private
    FControl: IevRPControl;
    FController: IevAppController;
    FMaxScore: Cardinal;
    procedure CalcMaxScore;
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    function  Control: IevRPControl;
    function  MaxScore: Cardinal;
  public
    destructor Destroy; override;
  end;


  TevLoopbackTCPClient = class(TisCollection, IevTCPClient)
  private
    FSession: IevSession;
  protected
    procedure DoOnConstruction; override;
    procedure SetCreateConnectionTimeout(const AValue: Integer);
    procedure Logoff;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  GetHost: String;
    function  GetPort: String;
    procedure SetHost(const AValue: String);
    procedure SetPort(const AValue: String);
    function  ConnectedHost: String;
    function  GetEncryptionType: TevEncryptionType;
    procedure SetEncryptionType(const AValue: TevEncryptionType);
    function  GetCompressionLevel: TisCompressionLevel;
    procedure SetCompressionLevel(const AValue: TisCompressionLevel);
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    function  Session: IevSession;
    function  GetArtificialLatency: Integer;
    procedure SetArtificialLatency(const AValue: Integer);
  end;


  TevRPDatagramDispatcher = class(TevCommonDatagramDispatcher)
  private
    procedure dmRPGetCapabilities(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure DoOnConstruction; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession;  const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    procedure RegisterHandlers; override;
  end;


  TevRPControl = class(TisInterfacedObject, IevRPControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    procedure DoOnConstruction; override;
    function  GetHardware: IisStringList;
    function  GetModules: IisStringList;
    function  GetLoadStatus: IisListOfValues;
    function  GetExecStatus: IisParamsCollection;
    function  GetAppTempFolder: String;
    procedure SetAppTempFolder(const ATempFolder: String);
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    function  GetMaintenanceDB: IisListOfValues;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
  end;



function GetTCPServer: IevTCPServer;
begin
  Result := TevTCPServer.Create;
end;

function GetLoopbackTCPClient: IevTCPClient;
begin
  Result := TevLoopbackTCPClient.Create;
end;

function RPControl: IevRPControl;
begin
  Result := (Mainboard.TCPServer as IevRPTCPServer).Control;
end;


{TevRPDatagramDispatcher}

procedure TevRPDatagramDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
begin
  inherited;
  // RW request alway work in lower priority
  if ASession.AppID = EvoRWAdapterAppInfo.AppID then
    SetThreadPriority(GetCurrentThreadId, THREAD_PRIORITY_BELOW_NORMAL);
end;

function TevRPDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  if IsStandAlone then
    Result := AnsiSameStr(AClientAppID, EvoClientAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoRemoteRelayAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoRWAdapterAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoAPIAdapterAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoDNetConnectorAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoDNetMapleConnectorAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoJavaESSAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoJavaWCAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoJavaMyHRAdminAppInfo.AppID)
  else
    Result := AnsiSameStr(AClientAppID, EvoRequestBrokerAppInfo.AppID) or
              AnsiSameStr(AClientAppID, EvoRWAdapterAppInfo.AppID);
end;

procedure TevRPDatagramDispatcher.dmRPGetCapabilities(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('MaxScore', (Mainboard.TCPServer as IevRPTCPServer).MaxScore);
  AResult.Params.AddValue('MaxLongRequests', HowManyProcessors * LONG_REQUESTS_PER_CPU);
end;

procedure TevRPDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  SetThreadPoolCapacity(100);
  ActivateProgressUpdater;
end;

function TevRPDatagramDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
var
  L: IisList;
  i: Integer;
  sRBIP: String;
  S: IevSession;
begin
  // Checking if another RB is trying to connect. Only one RB is allowed!
  S :=  FindSessionByID(ADatagram.Header.SessionID);
  Assert(Assigned(S));
  if AnsiSameStr(S.AppID, EvoRequestBrokerAppInfo.AppID) then
  begin
    sRBIP := Mainboard.TCPClient.Host;
    if sRBIP <> '' then
    begin
      L := GetTransmitter.TCPCommunicator.GetConnectionsBySessionID(ADatagram.Header.SessionID);
      for i := 0 to L.Count - 1 do
        CheckCondition((L[i] as IisTCPStreamAsyncConnection).GetRemoteIPAddress = sRBIP, 'Another RB (' + sRBIP + ') is using this RP');
     end;
  end;   

  Result := inherited DoOnLogin(ADatagram);
end;

procedure TevRPDatagramDispatcher.DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState);
var
  i: Integer;
begin
  inherited;

  if AnsiSameStr(ASession.AppID, EvoRequestBrokerAppInfo.AppID) then

    if (APreviousState = ssPerformingLogin) and (ASession.State = ssActive) then
      Mainboard.LogFile.AddEvent(etInformation, LOG_EVENT_CLASS_COMMUNICATION, 'RB session open', 'Session ID: ' + ASession.SessionID)

    else if (APreviousState = ssActive) and (ASession.State = ssInvalid) then
    begin
      Mainboard.ContextManager.StoreThreadContext;
      try
        // discard security cache for all domains
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
        mb_GlobalSettings.DomainInfoList.Lock;
        try
          for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
          begin
            Mainboard.ContextManager.CreateThreadContext(
              EncodeUserAtDomain(sGuestUserName, mb_GlobalSettings.DomainInfoList[i].DomainName), '');
            try
              Mainboard.GlobalCallbacks.SecurityChange('');
            except
            end;
          end;
        finally
          mb_GlobalSettings.DomainInfoList.Unlock;
        end;
      finally
        Mainboard.ContextManager.RestoreThreadContext;
      end;

      (mb_GlobalSettings as IevProxyModule).OnDisconnect; // Drop cached data
      Mainboard.LogFile.AddEvent(etInformation, LOG_EVENT_CLASS_COMMUNICATION, 'RB session closed', 'Session ID: ' + ASession.SessionID);
    end;
end;

procedure TevRPDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RP_GETCAPABILITIES, dmRPGetCapabilities);

  AddHandler(METHOD_RWREMOTEENGINE_RUNREPORTS, dmRWRemoteEngineRunReports);
  AddHandler(METHOD_RWREMOTEENGINE_RUNQBQUERY, dmRWRemoteEngineRunQBQuery);
  AddHandler(METHOD_RWREMOTEENGINE_SETLOWERRWENGINEPRIORITY, dmRWRemoteEngineSetLowerRWEnginePriority);
  AddHandler(METHOD_RWREMOTEENGINE_STOPREPORTEXECUTION, dmRWRemoteEngineStopReportExecution);

  AddHandler(METHOD_DB_ACCESS_CLEANDELETEDCLIENTSFROMTEMP, dmDBAccessCleanDeletedClientsFromTemp);
  AddHandler(METHOD_DB_ACCESS_GETDBVERSION, dmDBAccessGetDBVersion);
  AddHandler(METHOD_DB_ACCESS_GETGENERATORVALUE, dmDBAccessGetGeneratorValue);
  AddHandler(METHOD_DB_ACCESS_SETGENERATORVALUE, dmDBAccessSetGeneratorValue);
  AddHandler(METHOD_DB_ACCESS_GETCLIENTSLIST, dmDBAccessGetClientsList);
  AddHandler(METHOD_DB_ACCESS_GETDATASETS, dmDBAccessGetDataSets);
  AddHandler(METHOD_DB_ACCESS_OPENQUERY, dmDBAccessOpenQuery);
  AddHandler(METHOD_DB_ACCESS_EXECQUERY, dmDBAccessExecQuery);
  AddHandler(METHOD_DB_ACCESS_EXECSTOREDPROC, dmDBAccessExecStoredProc);
  AddHandler(METHOD_DB_ACCESS_CREATECLIENTDB, dmDBAccessCreateClientDB);
  AddHandler(METHOD_DB_ACCESS_DELETECLIENTDB, dmDBAccessDeleteClientDB);
  AddHandler(METHOD_DB_ACCESS_BEGINREBUILDALLCLIENTS, dmDBAccessBeginRebuildAllClients);
  AddHandler(METHOD_DB_ACCESS_REBUILDTEMPORARYTABLES, dmDBAccessRebuildTemporaryTables);
  AddHandler(METHOD_DB_ACCESS_ENDREBUILDALLCLIENTS, dmDBAccessEndRebuildAllClients);
  AddHandler(METHOD_DB_ACCESS_COMMITTRANSACTIONS, dmDBAccessCommitTransactions);
  AddHandler(METHOD_DB_ACCESS_BACKUPDB, dmDBAccessBackupDB);
  AddHandler(METHOD_DB_ACCESS_RESTOREDB, dmDBAccessRestoreDB);
  AddHandler(METHOD_DB_ACCESS_SWEEPDB, dmDBAccessSweepDB);
  AddHandler(METHOD_DB_ACCESS_DROPDB, dmDBAccessDropDB);
  AddHandler(METHOD_DB_ACCESS_UNDELETECLIENTDB, dmDBAccessUndeleteClientDB);
  AddHandler(METHOD_DB_ACCESS_GETLASTTRANSNBR, dmDBAccessGetLastTransactionNbr);
  AddHandler(METHOD_DB_ACCESS_GETTRANSSTATUS, dmDBAccessGetTransactionsStatus);
  AddHandler(METHOD_DB_ACCESS_GETDATASYNCPACKET, dmDBAccessGetDataSyncPacket);
  AddHandler(METHOD_DB_ACCESS_APPLYDATASYNCPACKET, dmDBAccessApplyDataSyncPacket);
  AddHandler(METHOD_DB_ACCESS_GETDBHASH, dmDBAccessGetDBHash);
  AddHandler(METHOD_DB_ACCESS_CHECKDBHASH, dmDBAccessCheckDBHash);
  AddHandler(METHOD_DB_ACCESS_GETCLIENTROFLAG, dmDBAccessGetClientROFlag);
  AddHandler(METHOD_DB_ACCESS_SETCLIENTROFLAG, dmDBAccessSetClientROFlag);
  AddHandler(METHOD_DB_ACCESS_GETNEWKEYVALUE, dmDBAccessGetNewKeyValue);
  AddHandler(METHOD_DB_ACCESS_GETFIELDVALUES, dmDBAccessGetFieldValues);
  AddHandler(METHOD_DB_ACCESS_GETFIELDVERSIONS, dmDBAccessGetFieldVersions);
  AddHandler(METHOD_DB_ACCESS_UPDATEFIELDVERSION, dmDBAccessUpdateFieldVersion);
  AddHandler(METHOD_DB_ACCESS_UPDATEFIELDVALUE, dmDBAccessUpdateFieldValue);
  AddHandler(METHOD_DB_ACCESS_GETVERSIONFIELDAUDIT, dmDBAccessGetVersionFieldAudit);
  AddHandler(METHOD_DB_ACCESS_GETVERSIONFIELDAUDITCHANGE, dmDBAccessGetVersionFieldAuditChange);


  AddHandler(METHOD_DB_ACCESS_GETRECORDAUDIT, dmDBAccessGetRecordAudit);
  AddHandler(METHOD_DB_ACCESS_GETBLOBAUDITDATA, dmDBAccessGetBlobAuditData);
  AddHandler(METHOD_DB_ACCESS_GETTABLEAUDIT, dmDBAccessGetTableAudit);
  AddHandler(METHOD_DB_ACCESS_GETDATABASEAUDIT, dmDBAccessGetDatabaseAudit);
  AddHandler(METHOD_DB_GETINITIALCREATIONNBRAUDIT, dmDBAccessGetInitialCreationNBRAudit);  

  AddHandler(METHOD_MISC_CALC_FINALIZETAXPAYMENTS, dmMiscCalcFinalizeTaxPayments);
  AddHandler(METHOD_MISC_CALC_PRINTCHECKSANDCOUPONSFORTAXES, dmMiscCalcPrintChecksAndCouponsForTaxes);
  AddHandler(METHOD_MISC_CALC_PAYTAXES, dmMiscCalcPayTaxes);
  AddHandler(METHOD_MISC_CALC_PREPROCESSFORQUARTER, dmMiscCalcPreProcessForQuarter);
  AddHandler(METHOD_MISC_CALC_CALCULATETAXES, dmMiscCalculateTaxes);
  AddHandler(METHOD_MISC_CALC_GETSYSTEMACCOUNTNBR, dmMiscGetSystemAccountNbr);

  AddHandler(METHOD_MISC_CALC_CALCULATECHECKLINES, dmMiscCalculateChecklines);
  AddHandler(METHOD_MISC_CALC_REFRESHEDS, dmMiscRefreshEDS);
  AddHandler(METHOD_MISC_CALC_CREATECHECK, dmMiscCreateCheck);
  AddHandler(METHOD_MISC_CALC_CREATEMANUALCHECK, dmMiscCreateManualCheck);
  AddHandler(METHOD_MISC_CALC_TCIMPORT, dmMiscTCImport);
  AddHandler(METHOD_MISC_CALC_GETW2, dmMiscGetW2);
  AddHandler(METHOD_MISC_CALC_CHANGECHECKSTATUS, dmMiscChangeCheckStatus);
  AddHandler(METHOD_MISC_CALC_HOLIDAYDAY, dmMiscHolidayDay);
  AddHandler(METHOD_MISC_CALC_NEXTPAYROLLDATES, dmMiscNextPayrollDates);
  AddHandler(METHOD_MISC_CALC_REPORT_TO_PDF, dmMiscReportToPDF);
  AddHandler(METHOD_MISC_CALC_ALL_REPORTS_TO_PDF, dmMiscAllReportsToPDF);
  AddHandler(METHOD_MISC_CALC_GET_PDF_REPORT, dmMiscGetPDFReport);
  AddHandler(METHOD_MISC_CALC_GET_ALL_PDF_REPORTS, dmMiscGetAllPDFReports);
  AddHandler(METHOD_MISC_CALC_RUN_REPORT, dmMiscRunReport);
  AddHandler(METHOD_MISC_CALC_GET_TASK_LIST, dmMiscGetTaskList);
  AddHandler(METHOD_MISC_CALC_GET_SYS_REPORT, dmMiscGetSysReportByNbr);
  AddHandler(METHOD_MISC_CALC_GET_REPORT_FILE, dmMiscGetReportFileByNbrAndType);

  AddHandler(METHOD_MISC_CALC_SAVE_API_BILLING, dmMiscSaveAPIBilling);
  AddHandler(METHOD_MISC_CALC_GET_API_BILLING, dmMiscGetAPIBilling);
  AddHandler(METHOD_MISC_CALC_GETEEW2LIST, dmMiscGetEEW2List);
  AddHandler(METHOD_MISC_CALC_GETEESCHEDULED_E_DS, dmMiscGetEEScheduled_E_DS);
  AddHandler(METHOD_MISC_CALC_CALC_EESCHEDULED_E_DS_RATE, dmMiscCalcEEScheduled_E_DS_Rate);
  AddHandler(METHOD_MISC_CALC_CALC_EESCHEDULED_E_DS_RATE2, dmMiscCalcEEScheduled_E_DS_Rate2);
  AddHandler(METHOD_MISC_CALC_GETSBEMAIL, dmMiscGetSbEMail);
  AddHandler(METHOD_MISC_CALC_ADDTOAREQUESTS, dmMiscAddTOARequests);
  AddHandler(METHOD_MISC_CALC_UPDATETOA_REQUESTS, dmMiscUpdateTOARequests);
  AddHandler(METHOD_MISC_CALC_GETTOAREQUESTSFOREE, dmMiscGetTOARequestsForEE);
  AddHandler(METHOD_MISC_CALC_GETTOAREQUESTSFORMGR, dmMiscGetTOARequestsForMgr);
  AddHandler(METHOD_MISC_CALC_PROCESSTOAREQUESTS, dmMiscProcessTOARequests);
  AddHandler(METHOD_MISC_CALC_GETWCREPORTLIST, dmMiscGetWCReportList);
  AddHandler(METHOD_MISC_CALC_GETPAYROLLTAXTOTALS, dmMiscGetPayrollTaxTotals);
  AddHandler(METHOD_MISC_CALC_GETPAYROLLEDTOTALS, dmMiscGetPayrollEdTotals);
  AddHandler(METHOD_MISC_CALC_ADDTOAFORNEWEE, dmMiscAddTOAForNewEE);
  AddHandler(METHOD_MISC_CALC_ADDEDSFORNEWEE, dmMiscAddEDsForNewEE);
  AddHandler(METHOD_MISC_CALC_GETTOAREQUESTSFORCO, dmMiscGetTOARequestsForCO);
  AddHandler(METHOD_MISC_CALC_UPDATETOAREQUESTSFORCO, dmMiscUpdateTOARequestsForCO);
  AddHandler(METHOD_MISC_CALC_DELETEROWSINDATASETS, dmMiscDeleteRowsInDatasets);
  AddHandler(METHOD_MISC_CALC_CREATEEECHANGEREQUEST, dmMiscCreateEEChangeRequest);
  AddHandler(METHOD_MISC_CALC_GETEECHANGEREQUESTLIST, dmMiscGetEEChangeRequestList);
  AddHandler(METHOD_MISC_CALC_FORMGRGETEECHANGEREQUESTLIST, dmMiscForMgrGetEEChangeRequestList);
  AddHandler(METHOD_MISC_CALC_PROCESSEECHANGEREQUESTS, dmMiscProcessEEChangeRequests);
  AddHandler(METHOD_MISC_CALC_SETEEEMAIL, dmMiscProcessSetEeEMail);
// new methods for Web AdHoc Reports
  AddHandler(METHOD_MISC_CALC_STOREREPORTPARAMETERSFROMXML, dmMiscProcessStoreReportParametersFromXML);
  AddHandler(METHOD_MISC_CALC_GETCOREPORTSPARAMETERSASXML, dmMiscProcessGetCoReportsParametersAsXML);
  AddHandler(METHOD_MISC_CALC_CONVERTXMLTORWPARAMETERS, dmMiscConvertXMLtoRWParameters);

  AddHandler(METHOD_MISC_CALC_GETCOCALENDARDEFAULTS, dmMiscGetCoCalendarDefaults);
  AddHandler(METHOD_MISC_CALC_CREATECOCALENDAR, dmMiscCreateCoCalendar);
  AddHandler(METHOD_MISC_CALC_GETBENEFITREQUEST, dmMiscGetBenefitRequest);
  AddHandler(METHOD_MISC_CALC_STOREBENEFITREQUEST, dmMiscStoreBenefitRequest);
  AddHandler(METHOD_MISC_CALC_GETBENEFITREQUESTLIST, dmMiscGetBenefitRequestList);
  AddHandler(METHOD_MISC_CALC_SENDEMAILTOHR, dmMiscSendEmailToHR);
  AddHandler(METHOD_MISC_CALC_SENDEMAILTOMANAGERS, dmMiscSendEmailToManagers);
  AddHandler(METHOD_MISC_CALC_GETEEEXISTINGBENEFITS, dmMiscGetEeExistingBenefits);
  AddHandler(METHOD_MISC_CALC_DELETEBENEFITREQUEST, dmMiscDeleteBenefitRequest);
  AddHandler(METHOD_MISC_CALC_GETEEBENEFITPACKAGEAVALIABLEFORENROLLMENT, dmMiscGetEeBenefitsPackageAvaliableForEnrollment);
  AddHandler(METHOD_MISC_CALC_GETSUMMARYBENEFITENROLLMENTSTATUS, dmMiscGetSummaryBenefitEnrollmentStatus);
  AddHandler(METHOD_MISC_CALC_REVERTBENEFITREQUEST, dmMiscRevertBenefitRequest);
  AddHandler(METHOD_MISC_CALC_GETBENEFITREQUESTCONFIRMATIONREPORT, dmMiscGetBenefitRequestConfirmationReport);
  AddHandler(METHOD_MISC_CALC_SUBMITBENEFITREQUEST, dmMiscSubmitBenefitRequest);
  AddHandler(METHOD_MISC_CALC_SENDBENEFITEMAILTOEE, dmMiscSendBenefitEMailToEE);
  AddHandler(METHOD_MISC_CALC_GETEMPTYBENEFITREQUEST, dmMiscEmptyBenefitRequest);
  AddHandler(METHOD_MISC_CALC_CLOSEBENEFITENROLLMENT, dmMiscClosebenefitEnrollment);
  AddHandler(METHOD_MISC_CALC_GETDATADICTIONARYXML, dmMiscGetDataDictionaryXML);
  AddHandler(METHOD_MISC_CALC_APPROVEBENEFITREQUEST, dmMiscApproveBenefitRequest);
  AddHandler(METHOD_MISC_CALC_GETBENEFITEMAILNOTIFICATIONSLIST, dmMiscGetBenefitEmailNotificationsList);
  AddHandler(METHOD_MISC_CALC_GETMANUALTAXLISTFORCHECK, dmMiscGetManualTaxesListForCheck);

  AddHandler(METHOD_MISC_CALC_DELETEEE, dmMiscDeleteEE);

  AddHandler(METHOD_MISC_CALC_GETACAHISTORY,  dmMiscGetACAHistory);
  AddHandler(METHOD_MISC_CALC_POSTACAHISTORY, dmMiscPostACAHistory);
  AddHandler(METHOD_MISC_CALC_GETACAOFFERCODES,  dmMiscGetACAOfferCodes);
  AddHandler(METHOD_MISC_CALC_GETACARELIEFCODES,  dmMiscGetACAReliefCodes);

  AddHandler(METHOD_PAYROLL_PROCESS_CLEANUPTAXABLETIPS, dmPayrollProcessCleanupTaxableTips);
  AddHandler(METHOD_PAYROLL_PROCESS_CREATEPR, dmPayrollProcessCreatePR);
  AddHandler(METHOD_PAYROLL_PROCESS_GETPAYROLLBATCHDEFAULTS, dmPayrollProcessGetPayrollBatchDefaults);
  AddHandler(METHOD_PAYROLL_PROCESS_CREATEPRBATCHDETAILS, dmPayrollProcessCreatePRBatchDetails);
  AddHandler(METHOD_PAYROLL_PROCESS_GROSSTONET, dmPayrollProcessGrossToNet);
  AddHandler(METHOD_PAYROLL_PROCESS_CREATETAXADJCHECK, dmPayrollProcessCreateTaxAdjCheck);
  AddHandler(METHOD_PAYROLL_PROCESS_TAXCALCULATOR, dmPayrollProcessTaxCalculator);
  AddHandler(METHOD_PAYROLL_PROCESS_DOERTAXADJPAYROLL, dmPayrollProcessDoERTaxAdjPayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_DOQTRENDTAXLIABADJ, dmPayrollProcessDoQtrEndTaxLiabAdj);
  AddHandler(METHOD_PAYROLL_PROCESS_DOQUARTERENDCLEANUP, dmPayrollProcessDoQuarterEndCleanup);
  AddHandler(METHOD_PAYROLL_PROCESS_DOMONTHENDCLEANUP, dmPayrollProcessDoMonthEndCleanup);
  AddHandler(METHOD_PAYROLL_PROCESS_ISQUARTERENDCLEANUPNECESSARY, dmPayrollProcessIsQuarterEndCleanupNecessary);
  AddHandler(METHOD_PAYROLL_PROCESS_LOADYTD, dmPayrollProcessLoadYTD);
  AddHandler(METHOD_PAYROLL_PROCESS_GETEEYTD, dmPayrollProcessGetEEYTD);
  AddHandler(METHOD_PAYROLL_PROCESS_PROCESSBILLING, dmPayrollProcessProcessBilling);
  AddHandler(METHOD_PAYROLL_PROCESS_PROCESSBILLINGFORSERVICES, dmPayrollProcessProcessBillingForServices);
  AddHandler(METHOD_PAYROLL_PROCESS_PROCESSPAYROLL, dmPayrollProcessProcessPayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_PROCESSMANUALACH, dmPayrollProcessProcessManualACH);
  AddHandler(METHOD_PAYROLL_PROCESS_PROCESSPRENOTEACH, dmPayrollProcessProcessPrenoteACH);
  AddHandler(METHOD_PAYROLL_PROCESS_RECREATEVMRHISTORY, dmPayrollProcessRecreateVmrHistory);
  AddHandler(METHOD_PAYROLL_PROCESS_RERUNPAYROLREPORTSFORVMR, dmPayrollProcessReRunPayrolReportsForVmr);
  AddHandler(METHOD_PAYROLL_PROCESS_REPRINTPAYROLL, dmPayrollProcessReprintPayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_PRINTPAYROLL, dmPayrollProcessPrintPayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_COPYPAYROLL, dmPayrollProcessCopyPayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_VOIDPAYROLL, dmPayrollProcessVoidPayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_VOIDCHECK, dmPayrollProcessVoidCheck);
  AddHandler(METHOD_PAYROLL_PROCESS_UNVOIDCHECK, dmPayrollProcessUnvoidCheck);  
  AddHandler(METHOD_PAYROLL_PROCESS_DELETEPAYROLL, dmPayrollProcessDeletePayroll);
  AddHandler(METHOD_PAYROLL_PROCESS_DELETEBATCH, dmPayrollProcessDeleteBatch);
  AddHandler(METHOD_PAYROLL_PROCESS_SETPAYROLLLOCK, dmPayrollSetPayrollLock);
  AddHandler(METHOD_PAYROLL_PROCESS_REFRESHSCHEDULEDEDSCHECK, dmPayrollProcessRefreshScheduledEDsCheck);  
  AddHandler(METHOD_PAYROLL_PROCESS_REFRESHSCHEDULEDEDSBATCH, dmPayrollProcessRefreshScheduledEDsBatch);
  AddHandler(METHOD_PAYROLL_CHECK_PRINT_PRINTCHECKS, dmPrCheckPrintingProxyPrintChecks);
  AddHandler(METHOD_PAYROLL_CHECK_PRINT_CHECKSTUBBLOBTODATASETS, dmPrCheckPrintingProxyCheckStubBlobToDatasets);
  AddHandler(METHOD_PAYROLL_PROCESS_REDISTRIBUTEDBDT, dmPayrollProcessRedistributeDBDT);

  AddHandler(METHOD_SECURITY_GETAUTHORIZATION, dmSecurityGetAuthorization);
  AddHandler(METHOD_SECURITY_GETSECSTRUCTURE, dmSecurityGetSecStructure);
  AddHandler(METHOD_SECURITY_GETUSERSECSTRUCTURE, dmSecurityGetUserSecStructure);
  AddHandler(METHOD_SECURITY_GETUGROUPSECSTRUCTURE, dmSecurityGetGroupSecStructure);
  AddHandler(METHOD_SECURITY_SETGROUPSECSTRUCTURE, dmSecuritySetGroupSecStructure);
  AddHandler(METHOD_SECURITY_SETUSERSECSTRUCTURE, dmSecuritySetUserSecStructure);
  AddHandler(METHOD_SECURITY_CREATEEEACCOUNT, dmSecurityCreateEEAccount);
  AddHandler(METHOD_SECURITY_CHANGEPASSWORD, dmSecurityChangePassword);
  AddHandler(METHOD_SECURITY_ENABLESYSTEMACCOUNTRIGHTS, dmSecurityEnableSystemAccountRights);
  AddHandler(METHOD_SECURITY_GETUGROUPLEVELRIGHTS, dmSecurityGetUserGroupLevelRights);
  AddHandler(METHOD_SECURITY_GETUSERRIGHTS, dmSecurityGetUserRights);
  AddHandler(METHOD_SECURITY_GETGROUPRIGHTS, dmSecurityGetGroupRights);
  AddHandler(METHOD_SECURITY_GETAVAILABLEQUESTIONS, dmSecurityGetAvailableQuestions);
  AddHandler(METHOD_SECURITY_GETAVAILABLEEXTQUESTIONS, dmSecurityGetAvailableExtQuestions);
  AddHandler(METHOD_SECURITY_SETQUESTIONS, dmSecuritySetQuestions);
  AddHandler(METHOD_SECURITY_SETEXTQUESTIONS, dmSecuritySetExtQuestions);
  AddHandler(METHOD_SECURITY_GETQUESTIONS, dmSecurityGetQuestions);
  AddHandler(METHOD_SECURITY_RESETPASSWORD, dmSecurityResetPassword);
  AddHandler(METHOD_SECURITY_CHECKANSWERS, dmSecurityCheckAnswers);
  AddHandler(METHOD_SECURITY_GENERATENEWPASSWORD, dmSecurityGenerateNewPassword);
  AddHandler(METHOD_SECURITY_VALIDATEPASSWORD, dmSecurityValidatePassword);
  AddHandler(METHOD_SECURITY_GETPASSWORDRULES, dmSecurityGetPasswordRules);
  AddHandler(METHOD_SECURITY_GETEXTQUESTIONS, dmSecurityGetExtQuestions);
  AddHandler(METHOD_SECURITY_CHECKEXTANSWERS, dmSecurityCheckExtAnswers);
  AddHandler(METHOD_SECURITY_GETSECURITYRULES, dmSecurityGetSecurityRules);

  AddHandler(METHOD_LICENSE_GETLICENSEINFO, dmLicenseGetLicenseInfo);
  AddHandler(METHOD_TIMESOURCE_GETDATETIME, dmTimeSourceGetDateTime);

  AddHandler(METHOD_ASYNCFUNCTIONS_MAKECALL, dmAsyncFunctionsMakeCall);
  AddHandler(METHOD_ASYNCFUNCTIONS_SETCALLPRIORITY, dmAsyncFunctionsSetCallPriority);
  AddHandler(METHOD_ASYNCFUNCTIONS_TERMINATECALL, dmAsyncFunctionsTerminateCall);
  AddHandler(METHOD_ASYNCFUNCTIONS_SETCALCCAPACITY, dmAsyncFunctionsSetCalcCapacity);

  AddHandler(METHOD_GLOBALCALLBACKS_SECURITYCHANGE, dmGlobalCallbacksSecurityChange);

  AddHandler(METHOD_EVOX_ENGINE_IMPORT, dmEvoXEngineImport);
  AddHandler(METHOD_EVOX_ENGINE_CHECKMAPFILE, dmEvoXEngineCheckMapFile);

  AddHandler(METHOD_DASHBOARD_DATAPROVIDER_GETCOMPANYMESSAGES, dmDashboardDataProviderGetCompanyMessages);  

  if IsStandalone then
  begin
    AddHandler(METHOD_GLOBALFLAGMANAGER_SETFLAG, dmGlobalFlagManagerSetFlag);
    AddHandler(METHOD_GLOBALFLAGMANAGER_TRYLOCK, dmGlobalFlagManagerTryLock);
    AddHandler(METHOD_GLOBALFLAGMANAGER_TRYLOCK2, dmGlobalFlagManagerTryLock2);
    AddHandler(METHOD_GLOBALFLAGMANAGER_TRYUNLOCK, dmGlobalFlagManagerTryUnlock);
    AddHandler(METHOD_GLOBALFLAGMANAGER_TRYUNLOCK2, dmGlobalFlagManagerTryUnlock2);
    AddHandler(METHOD_GLOBALFLAGMANAGER_FORCEDUNLOCK, dmGlobalFlagManagerForcedUnlock);
    AddHandler(METHOD_GLOBALFLAGMANAGER_FORCEDUNLOCK2, dmGlobalFlagManagerForcedUnlock2);
    AddHandler(METHOD_GLOBALFLAGMANAGER_RELEASEFLAG, dmGlobalFlagManagerReleaseFlag);
    AddHandler(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAG, dmGlobalFlagManagerGetLockedFlag);
    AddHandler(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAG2, dmGlobalFlagManagerGetLockedFlag2);
    AddHandler(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAGLIST, dmGlobalFlagManagerGetLockedFlagList);
    AddHandler(METHOD_GLOBALFLAGMANAGER_GETSETFLAGLIST, dmGlobalFlagManagerGetSetFlagList);

    AddHandler(METHOD_VMR_REMOTE_CHECKSCANNEDBARCODE, dmVMRRemoteCheckScannedBarcode);
    AddHandler(METHOD_VMR_REMOTE_DELETEFILE, dmVMRRemoteDeleteFile);
    AddHandler(METHOD_VMR_REMOTE_FILEDOESEXIST, dmVMRRemoteFileDoesExist);
    AddHandler(METHOD_VMR_REMOTE_FINDINBOXES, dmVMRRemoteFindInBoxes);
    AddHandler(METHOD_VMR_REMOTE_RETRIEVE_PICKUPSHEET_DATA, dmVMRRemoteRetrievePickupSheetsXMLData);
    AddHandler(METHOD_VMR_REMOTE_GETPRINTERLIST, dmVMRRemoteGetPrinterList);
    AddHandler(METHOD_VMR_REMOTE_SETPRINTERLIST, dmVMRRemoteSetPrinterList);
    AddHandler(METHOD_VMR_REMOTE_GETSBPRINTERLIST, dmVMRRemoteGetSBPrinterList);
    AddHandler(METHOD_VMR_REMOTE_PRINTRWRESULTS, dmVMRRemotePrintRwResults);
    AddHandler(METHOD_VMR_REMOTE_PUTINBOXES, dmVMRRemotePutInBoxes);
    AddHandler(METHOD_VMR_REMOTE_REMOVEFROMBOXES, dmVMRRemoteRemoveFromBoxes);
    AddHandler(METHOD_VMR_REMOTE_REMOVESCANNEDBARCODE, dmVMRRemoteRemoveScannedBarcode);
    AddHandler(METHOD_VMR_REMOTE_REMOVESCANNEDBARCODES, dmVMRRemoteRemoveScannedBarcodes);
    AddHandler(METHOD_VMR_REMOTE_RETRIEVEFILE, dmVMRRemoteRetrieveFile);
    AddHandler(METHOD_VMR_REMOTE_STOREFILE, dmVMRRemoteStoreFile);
    AddHandler(METHOD_VMR_REMOTE_PROCESSRELEASE,dmVMRProcessRelease );
    AddHandler(METHOD_VMR_REMOTE_PROCESSREVERT,dmVMRProcessRevert );
    AddHandler(METHOD_VMR_REMOTE_GETVMRREPORTLIST,dmVMRGetVMRReportList);
    AddHandler(METHOD_VMR_REMOTE_GETVMRREPORT,    dmVMRGetVMRReport);
    AddHandler(METHOD_VMR_REMOTE_DELETEDIRECTORY, dmVMRDeleteDirectory);
    AddHandler(METHOD_VMR_REMOTE_PURGEMAILBOX,    dmVMRPurgeMailBox);
    AddHandler(METHOD_VMR_REMOTE_PURGECLIENTMAILBOX, dmVMRPurgeClientMailBox);
    AddHandler(METHOD_VMR_REMOTE_PURGEMAILBOXCONTENTLIST, dmVMRPurgeMailBoxContentList);
    AddHandler(METHOD_VMR_REMOTE_GETREMOTEVMRREPORTLIST, dmVMRGetRemoteVMRReportList);
    AddHandler(METHOD_VMR_REMOTE_GETREMOTEVMRREPORT, dmVMRGetRemoteVMRReport);

    AddHandler(METHOD_TASK_QUEUE_CREATETASK, dmTaskQueueCreateTask);
    AddHandler(METHOD_TASK_QUEUE_ADDTASK, dmTaskQueueAddTask);
    AddHandler(METHOD_TASK_QUEUE_ADDTASKS, dmTaskQueueAddTasks);
    AddHandler(METHOD_TASK_QUEUE_REMOVETASK, dmTaskQueueRemoveTask);
    AddHandler(METHOD_TASK_QUEUE_GETTASKINFO, dmTaskQueueGetTaskInfo);
    AddHandler(METHOD_TASK_QUEUE_GETTASK, dmTaskQueueGetTask);
    AddHandler(METHOD_TASK_QUEUE_GETTASKREQUESTS, dmTaskQueueGetTaskRequests);
    AddHandler(METHOD_TASK_QUEUE_GETUSERTASKSTATUS, dmTaskQueueGetUserTaskStatus);
    AddHandler(METHOD_TASK_QUEUE_GETUSERTASKINFOLIST, dmTaskQueueGetUserTaskInfoList);
    AddHandler(METHOD_TASK_QUEUE_MODIFYTASKPROPERTY, dmTaskQueueModifyTaskProperty);

    AddHandler(METHOD_VERSION_UPDATE_REMOTE_CHECK, dmVersionUpdateRemoteCheck);
    AddHandler(METHOD_VERSION_UPDATE_REMOTE_GETUPDATEPACK, dmVersionUpdateRemoteGetUpdatePack);

    AddHandler(METHOD_GLOBALCALLBACKS_SUBSCRIBE, dmGlobalCallbacksSubscribe);
    AddHandler(METHOD_GLOBALCALLBACKS_UNSUBSCRIBE, dmGlobalCallbacksUnsubscribe);

    AddHandler(METHOD_EMAILER_SENDMAIL, dmEMailerSendMail);
    AddHandler(METHOD_EMAILER_SENDQUICKMAIL, dmEMailerSendQuickMail);

    AddHandler(METHOD_DASHBOARD_DATAPROVIDER_GETCOMPANYREPORTS, dmDashboardDataProviderGetCompanyReports);
  end

  else
  begin
    AddHandler(METHOD_GLOBALCALLBACKS_TASKQUEUEEVENT, dmGlobalCallbacksTaskQueueEvent);
    AddHandler(METHOD_GLOBALCALLBACKS_GLOBALFLAGCHANGE, dmGlobalCallbacksGlobalFlagChange);
    AddHandler(METHOD_GLOBALCALLBACKS_GLOBALSETTINGSCHANGE, dmGlobalCallbacksGlobalSettingsChange);
    AddHandler(METHOD_GLOBALCALLBACKS_SETDBMAINTENANCE, dmGlobalCallbacksSetDBMaintenance);    
  end;
end;


{ TevLoopbackTCPClient }

procedure TevLoopbackTCPClient.Logoff;
begin
  // a plug
end;

function TevLoopbackTCPClient.GetCompressionLevel: TisCompressionLevel;
begin
  Result := clNone;
end;

function TevLoopbackTCPClient.GetDatagramDispatcher: IevDatagramDispatcher;
begin
  Result := Mainboard.TCPServer.DatagramDispatcher;
end;

function TevLoopbackTCPClient.GetEncryptionType: TevEncryptionType;
begin
  Result := etNone;
end;

function TevLoopbackTCPClient.GetHost: String;
var
  S: IevSession;
  L: IisList;
begin
  Result := '';

  if IsStandalone then
    Result := Mainboard.MachineInfo.IPaddress
  else if Mainboard.TCPServer.DatagramDispatcher.Transmitter.TCPCommunicator.ConnectionCount > 0 then
  begin
    S := Session;
    if Assigned(S) then
    begin
      L := GetDatagramDispatcher.Transmitter.TCPCommunicator.GetConnectionsBySessionID(S.SessionID);
      if L.Count > 0 then
        Result := (L[0] as IisTCPSocket).GetRemoteIPAddress;
    end;
  end;
end;

function TevLoopbackTCPClient.GetPort: String;
begin
  Result := IntToStr(TCPClient_Port);
end;

function TevLoopbackTCPClient.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
begin
  Result := nil;
  if not IsStandalone then
  begin
    S := Session;
    CheckCondition(Assigned(S), 'No active sessions');
    ADatagram.Header.SessionID := S.SessionID;

    // This is a workaround
    // Callbacks are a fast requests and shouldn't take too long time to run
    // If that's happened it means the other side is not responding or hung
    Result := S.SendRequest(ADatagram, 2 * 60 * 1000); // 2 minutes timeout
    CheckCondition(Assigned(Result), 'RB did not respond on callback request');
  end
  else
    Assert(False);
end;

function TevLoopbackTCPClient.Session: IevSession;
var
  Dispatcher: IevDatagramDispatcher;
  i: Integer;
begin
  Lock;
  try
    if not Assigned(FSession) or (FSession.GetConnectionCount = 0) then
    begin
      Dispatcher := GetDatagramDispatcher;
      Dispatcher.LockSessionList;
      try
        for i := 0 to Dispatcher.SessionCount - 1 do
          if AnsiSameStr(Dispatcher.GetSessionByIndex(i).AppID, EvoRequestBrokerAppInfo.AppID) then
          begin
            FSession := Dispatcher.GetSessionByIndex(i);
            if FSession.State = ssActive then
              break
            else
              FSession := nil;
          end;
      finally
        Dispatcher.UnlockSessionList;
      end;
    end;

    Result := FSession;
  finally
    Unlock;
  end;
end;

procedure TevLoopbackTCPClient.SetCompressionLevel(const AValue: TisCompressionLevel);
begin
  Assert(False);
end;

procedure TevLoopbackTCPClient.SetEncryptionType(const AValue: TevEncryptionType);
begin
  Assert(False);
end;

procedure TevLoopbackTCPClient.SetHost(const AValue: String);
begin
  Assert(False);
end;

procedure TevLoopbackTCPClient.SetPort(const AValue: String);
begin
  Assert(False);
end;


function TevLoopbackTCPClient.ConnectedHost: String;
begin
  Result := GetHost;
end;

procedure TevLoopbackTCPClient.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

procedure TevLoopbackTCPClient.SetCreateConnectionTimeout(const AValue: Integer);
begin
  // a plug
end;

function TevLoopbackTCPClient.GetArtificialLatency: Integer;
begin
  Result := 0;
end;

procedure TevLoopbackTCPClient.SetArtificialLatency(const AValue: Integer);
begin
// nothing
end;

{ TevRPControl }

procedure TevRPControl.DoOnConstruction;
begin
  inherited;
  PerformanceCounter.AddCounter(pcCPUTotal);
  PerformanceCounter.AddCounter(pcProcessCPUTotal);
end;

function TevRPControl.GetAppTempFolder: String;
begin
  Result := AppTempFolder;
end;

function TevRPControl.GetMaintenanceDB: IisListOfValues;
var
  i: Integer;
  L: IisStringList;

  function DomainDBs(const ADomainName: String): IisStringList;
  begin
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    try
      Result := ctx_DBAccess.GetDisabledDBs;
    finally
      Mainboard.ContextManager.DestroyThreadContext;
    end;
  end;

begin
  Result := TisListOfValues.Create;
  Mainboard.ContextManager.StoreThreadContext;
  try
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      begin
        L := DomainDBs(mb_GlobalSettings.DomainInfoList[i].DomainName);
        Result.AddValue(mb_GlobalSettings.DomainInfoList[i].DomainName, L);
      end;
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;  
  end;
end;

procedure TevRPControl.SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
var
  i: Integer;

  procedure DomainDBs(const ADomainName: String; const ADBList: IisStringList);
  begin
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    try
      if AMaintenance then
        ctx_DBAccess.DisableDBs(ADBList)
      else
        ctx_DBAccess.EnableDBs(ADBList);      
    finally
      Mainboard.ContextManager.DestroyThreadContext;
    end;
  end;

begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    for i := 0 to ADBListByDomains.Count - 1 do
      DomainDBs(ADBListByDomains[i].Name, IInterface(ADBListByDomains[i].Value) as IisStringList);
  finally
    Mainboard.ContextManager.RestoreThreadContext;  
  end;
end;

function TevRPControl.GetExecStatus: IisParamsCollection;
var
  i: Integer;
  dmH: IisParamsCollection;
  L: IisList;
  Par: IisListOfValues;
  DMHeader: IevDatagramHeader;
  AsyncCall: IevAsyncCallInfo;
begin
  Result := TisParamsCollection.Create;
  dmH := (Mainboard.TCPServer as IevRPTCPServer).DatagramDispatcher.GetExecutingDatagramInfo;
  dmH.Lock;
  try
    for i := 0 to dmH.Count - 1 do
    begin
      DMHeader := IInterface(dmH[i].Value['dmHeader']) as IevDatagramHeader;
      Par := Result.AddParams('');
      Par.AddValue('Method', DMHeader.Method);
      Par.AddValue('User', DMHeader.User);
      Par.AddValue('StartTime', dmH[i].Value['StartTime']);
    end;
 finally
   dmH.Unlock;
 end;

  L := mb_AsyncFunctions.GetExecSatus;
  for i := 0 to L.Count - 1 do
  begin
    AsyncCall := L[i] as IevAsyncCallInfo;
    Par := Result.AddParams('');
    Par.AddValue('Method', METHOD_ASYNCFUNCTIONS + '.' + AsyncCall.Name);
    Par.AddValue('User', AsyncCall.User);
    Par.AddValue('StartTime', AsyncCall.StartTime);
  end;
end;

function TevRPControl.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  L := Mainboard.LogFile.GetEvents(AQuery);

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevRPControl.GetHardware: IisStringList;
var
  Info: TIsStringGridHwInfo;
  List: TStringList;
  i: Integer;
begin
  Result := TisStringList.Create;
  Info := TIsStringGridHwInfo.Create(nil);
  try
    List := TStringList.Create;
    try
      Info.Initialize;
      Info.FillStringList(List);
      for i := 0 to List.Count - 1 do
        Result.Add(List[i]);
    finally
      List.Free;
    end;
  finally
    Info.Free;
  end;
end;

function TevRPControl.GetLoadStatus: IisListOfValues;
var
  MemoryStatus: TMemoryStatus;
  AllocatedVAS: Cardinal;
  L: IisListOfValues;
  TransReceivingQueueSize, TransSendingQueueSize: Integer;
begin
  MemoryStatus := GetMemStatus;
  AllocatedVAS := MemoryStatus.dwTotalVirtual - MemoryStatus.dwAvailVirtual;

  L := PerformanceCounter.CollectData;

  Result := TisListOfValues.Create;
  Result.AddValue('CPUTotal', L.Value[PC_CPU_TOTAL]);
  Result.AddValue('ProcessCPUTotal', Round((L.Value[PC_CPU_TOTAL] / 100) * L.Value[PC_PROCESS_CPU_TOTAL]));
  Result.AddValue('MemoryLoad', MemoryStatus.dwMemoryLoad);
  Result.AddValue('MemoryProcessLoad', Round((AllocatedVAS / MemoryStatus.dwTotalVirtual) * 100));

  Mainboard.TCPServer.DatagramDispatcher.Transmitter.QueueStatus(TransReceivingQueueSize, TransSendingQueueSize);
  Result.AddValue('TransReceivingQueueSize', TransReceivingQueueSize);
  Result.AddValue('TransSendingQueueSize', TransSendingQueueSize);
end;

function TevRPControl.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  Mainboard.LogFile.Lock;
  try
    CheckCondition((EventNbr >= 0) and (EventNbr < Mainboard.LogFile.Count), 'Event number is incorrect');
    Event := Mainboard.LogFile[EventNbr];
  finally
    Mainboard.LogFile.UnLock;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('EventNbr', Event.Nbr);   
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);  
end;

function TevRPControl.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  Mainboard.LogFile.Lock;
  try
    L := Mainboard.LogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := Mainboard.LogFile.Count div EventsPerPage;
    if Mainboard.LogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    Mainboard.LogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevRPControl.GetModules: IisStringList;
var
  ModuleList: IInterfaceList;
  i: Integer;
begin
  Result := TisStringList.Create;

  ModuleList := Mainboard.ModuleRegister.GetModules;

  for i := 0 to ModuleList.Count - 1 do
    Result.Add((ModuleList[i] as IevModuleDescriptor).ModuleName);
end;

procedure TevRPControl.SetAppTempFolder(const ATempFolder: String);
begin
  IsBasicUtils.SetAppTempFolder(ATempFolder);
  mb_AppConfiguration.SetValue('General\TempFolder', ATempFolder);
end;


procedure TevRPControl.GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
begin
  APurgeRecThreshold := GlobalLogFile.PurgeRecThreshold;
  APurgeRecCount := GlobalLogFile.PurgeRecCount;
end;

procedure TevRPControl.SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
begin
  mb_AppConfiguration.AsInteger['General\LogFilePurgeRecThreshold'] := APurgeRecThreshold;
  mb_AppConfiguration.AsInteger['General\LogFilePurgeRecCount'] := APurgeRecCount;

  GlobalLogFile.PurgeRecCount := APurgeRecCount;
  GlobalLogFile.PurgeRecThreshold := APurgeRecThreshold;
end;


function TevRPControl.GetStackInfo: String;
begin
  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      Result := GetStack(E);
  end;
end;

{ TevTCPServer }

function TevTCPServer.Control: IevRPControl;
begin
  Result := FControl;
end;

function TevTCPServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRPDatagramDispatcher.Create;
  if IsStandAlone then
    Result.SessionTimeout := CLIENT_SESSION_KEEP_ALIVE_TIMEOUT;
end;

destructor TevTCPServer.Destroy;
begin
  FController := nil;
  FControl := nil;
  inherited;
end;

procedure TevTCPServer.DoOnConstruction;
begin
  inherited;
  if IsStandalone then
  begin
    SetPort(IntToStr(TCPClient_Port));
    SetIPAddress(mb_AppConfiguration.AsString['Network\RBToClientIPAddr']);
  end
  else
  begin
    SetPort(IntToStr(TCPServer_Port));
    SetIPAddress(mb_AppConfiguration.AsString['Network\RPtoRBIPAddr']);
    CalcMaxScore;
  end;

  FControl := TevRPControl.Create;
  FController := TevRPController.Create;
  FController.Active := True;

  if IsStandalone then
    CheckLicenses;
end;


function TevTCPServer.MaxScore: Cardinal;
begin
  Result := FMaxScore;
end;


procedure TevTCPServer.CalcMaxScore;
var
  ScoreMeter: TisScoreMeter;
begin
  ScoreMeter := TisScoreMeter.Create;
  try
    FMaxScore := ScoreMeter.CPUScore + ScoreMeter.MEMScore + ScoreMeter.IOScore;
  finally
    ScoreMeter.Free;
  end;
end;


initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTCPServer, IevTCPServer, 'TCP Server');
  Mainboard.ModuleRegister.RegisterModule(@GetLoopbackTCPClient, IevTCPClient, 'Loopback TCP Client');
end.

