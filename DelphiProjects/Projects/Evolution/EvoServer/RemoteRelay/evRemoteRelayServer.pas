unit EvRemoteRelayServer;

interface

uses SysUtils, Classes, isBaseClasses, EvTransportShared, EvTransportDatagrams, isSocket,
     evStreamUtils, isBasicUtils, isThreadManager, evRemoteMethods, EvConsts,
     evTransportInterfaces, EvControllerInterfaces, isSettings, isLogFile,
     isAppIDs, ISZippingRoutines, isStreamTCP, isTransmitter, ISErrorUtils, isExceptions;

type

  IevRRClient = interface
  ['{AD70A610-D30D-4814-A4F7-D31FA16CAAF0}']
    function  CreateConnection(const AHost, APort, AUser, APassword: String;
                               const ASessionID, AContextID: TisGUID;
                               const ALocalIPAddr: String = ''; const AAppID: String = ''): TisGUID;
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    function  GetSession(const ASessionID: TisGUID): IevSession;
    procedure Disconnect(const ASessionID: String);
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
  end;

  IevRRServer = interface
  ['{B2D40875-CFA5-489F-80F2-CA1A6E4FEADE}']
    function  GetPort: String;
    procedure SetPort(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  Active: Boolean read GetActive write SetActive;
    property  Port: String read GetPort write SetPort;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
    function  GetSession(const ASessionID: TisGUID): IevSession;
  end;

  IevRRMainboard = interface
  ['{80630C4D-CEC6-4F33-864B-975806AEAAEC}']
    procedure Initialize;
    function  AppSettings: IisSettings;
    function  MaleConnector: IevRRClient;
    function  FemaleConnector: IevRRServer;
    function  Control: IevRRControl;
  end;

  TevRRMainboard = class(TisInterfacedObject, IevRRMainboard)
  private
    FMaleConnector: IevRRClient;
    FFemaleConnector: IevRRServer;
    FController: IevAppController;
    FAppSettings: IisSettings;
    FControl: IevRRControl;
  protected
    procedure DoOnConstruction; override;
    procedure Initialize;
    function  MaleConnector: IevRRClient;
    function  FemaleConnector: IevRRServer;
    function  AppSettings: IisSettings;
    function  Control: IevRRControl;
  public
    destructor Destroy; override;
  end;

var
  Mainboard: IevRRMainboard;

implementation

type

  // RR -> RB dispatcher
  TevRRClientDispatcher = class(TevDatagramDispatcher)
  protected
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
  end;

  // Clients -> RR dispatcher
  TevRRServerDispatcher = class(TevDatagramDispatcher)
  private
    procedure CreateRBSession(const ADatagramHeader: IevDatagramHeader; const ARRSession: IevSession);
    procedure AddUsersActivityEvent(const AEvent: String; const ASessionID: TisGUID; const ALogIpAddress: boolean); overload;
    procedure AddUsersActivityEvent(const AEvent: String; const ASession: IevSession; const ALogIpAddress: boolean); overload;
  protected
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;
    procedure DoOnHandshake(var AEncryption: TevEncryptionType; var ACompression: TisCompressionLevel); override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
  end;

  // Clients -> RR connections
  TevTCPServer = class(TevCustomTCPServer, IevRRServer)
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    function  GetSession(const ASessionID: TisGUID): IevSession;
  end;

  // RR  -> RB connections
  TevTCPClient = class(TevCustomTCPClient, IevRRClient)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


  // RR Controller
  TevRRController = class(TevCustomTCPServer, IevAppController)
  private
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


  TevRRControl = class(TisInterfacedObject, IevRRControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    function  GetConnectionStatus: IisParamsCollection;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetSessionsStatus: IisParamsCollection;
    function  GetStackInfo: String;
  end;


  TevRRControllerDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetSettings(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetSettings(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetConnectionStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetSessionsStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStackInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure RegisterHandlers; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
  end;

{ TevRRMainboard }

destructor TevRRMainboard.Destroy;
begin
  GlobalLogFile.AddEvent(etInformation, 'Remote Relay', 'Service stopped', '');
  SetGlobalLogFile(nil);
  ClearDir(AppTempFolder, True);  
  inherited;
end;

procedure TevRRMainboard.DoOnConstruction;
begin
  inherited;
  SetGlobalLogFile(TLogFile.Create(ChangeFileExt(AppFileName, '.log')));
end;

function TevRRMainboard.MaleConnector: IevRRClient;
begin
  Result := FMaleConnector;
end;

function TevRRMainboard.FemaleConnector: IevRRServer;
begin
  Result := FFemaleConnector;
end;


function TevRRMainboard.AppSettings: IisSettings;
begin
  Result := FAppSettings;
end;

procedure TevRRMainboard.Initialize;
begin
  FAppSettings := TisSettingsINI.Create(AppDir + sConfigFileName);

  // todo: REMOVE AFTER Norwich !!!!
  if FAppSettings.AsString['General\RequestBroker'] = '' then
    FAppSettings.AsString['General\RequestBroker'] := FAppSettings.AsString['RemoteRelay\RequestBroker'];

  GlobalLogFile.PurgeRecThreshold := AppSettings.GetValue('General\LogFilePurgeRecThreshold', Integer(DefaultPurgeRecThreshold));
  GlobalLogFile.PurgeRecCount := AppSettings.GetValue('General\LogFilePurgeRecCount', Integer(DefaultPurgeRecCount));

  FMaleConnector := TevTCPClient.Create;
  FFemaleConnector := TevTCPServer.Create;
  FFemaleConnector.Active := True;
  FController := TevRRController.Create;
  FController.Active := True;
  FControl := TevRRControl.Create;

  GlobalLogFile.AddEvent(etInformation, 'Remote Relay', 'Service started', '');
end;

function TevRRMainboard.Control: IevRRControl;
begin
  Result := FControl;
end;

{ TevTCPServer }

procedure TevTCPServer.DoOnConstruction;
var
  s: String;
begin
  inherited;
  s := Mainboard.AppSettings.AsString['RemoteRelay\ClientPorts'];
  if s = '' then
  begin
    AddStrValue(s, IntToStr(TCPClient_PortSSL1) + ':SSL', ',');
    AddStrValue(s, IntToStr(TCPClient_PortSSL2) + ':SSL', ',');
    AddStrValue(s, IntToStr(TCPClient_PortSSL3) + ':SSL', ',');
    Mainboard.AppSettings.AsString['RemoteRelay\ClientPorts'] := s; // default
  end;
  SetPort(s);
  SetIPAddress(Mainboard.AppSettings.AsString['Network\RRtoClientIPAddr']);
  SetSSLTimeout(Mainboard.AppSettings.GetValue('RemoteRelay\SSLTimeout', Integer(0)));
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).SSLRootCertFile := AppDir + 'root.pem';
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).SSLCertFile := AppDir + 'cert.pem';
  (GetDatagramDispatcher.Transmitter.TCPCommunicator as IisAsyncTCPServer).SSLKeyFile := AppDir + 'key.pem';  
end;

function TevTCPServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRRServerDispatcher.Create;
  Result.SessionTimeout := CLIENT_SESSION_KEEP_ALIVE_TIMEOUT;
end;

function TevTCPServer.GetSession(const ASessionID: TisGUID): IevSession;
begin
  Result := GetDatagramDispatcher.FindSessionByID(ASessionID);
end;


{ TevRRClientDispatcher }

procedure TevRRClientDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
var
  S: IevSession;
  SentBytes: Cardinal;
begin
  inherited;
  if (ASession.State = ssActive) and not IsTransportDatagram(ADatagramHeader) then
  begin
    S := Mainboard.FemaleConnector.GetSession(ASession.SessionID) as IevSession;
    CheckCondition(Assigned(S), 'Client session is not found');
    if S.State <> ssInvalid then
      S.SendData(AStream, ADatagramHeader, SentBytes);
    Handled := True;
  end;
end;


{ TevRRServerDispatcher }

procedure TevRRServerDispatcher.AddUsersActivityEvent(const AEvent: String; const ASessionID: TisGUID;
  const ALogIpAddress: boolean);
var
  Session: IevSession;
begin
  Session := Mainboard.FemaleConnector.GetSession(ASessionID) as IevSession;
  if Assigned(Session) then
    AddUsersActivityEvent(AEvent, Session, ALogIpAddress);
end;

procedure TevRRServerDispatcher.AddUsersActivityEvent(const AEvent: String; const ASession: IevSession;
  const ALogIpAddress: boolean);
var
  Connections: IisList;
  sRemoteIP: String;
  sText, sDetails, s: String;
  i: integer;
begin
  if Assigned(ASession) then
  begin
    sText :=  AEvent + ' ' +  ASession.Login + ' ' + ASession.SessionID;
    sDetails := 'Application: ' + AppInfoByID(ASession.AppID).Abbr + #13#10;
    sDetails := sDetails + 'Encryption: ' + ASession.EncryptionTypeDscr + #13#10;

    sRemoteIP := '';
    if ALogIpAddress then
    begin
      Connections := ASession.Transmitter.TCPCommunicator.GetConnectionsBySessionID(ASession.SessionID);
      for i := 0 to Connections.Count - 1 do
      begin
        s := (Connections[i] as IisTCPStreamAsyncConnection).GetRemoteIPAddress + ':' + IntToStr((Connections[i] as IisTCPStreamAsyncConnection).GetRemotePort);
        if not Contains(sRemoteIP, s) then
          AddStrValue(sRemoteIP, s, ',');
      end;
      if sRemoteIP <> '' then
        sDetails := sDetails + 'Remote IP: ' + sRemoteIP
      else
        sDetails := sDetails + 'Remote IP: not found';
    end
    else
      sDetails := sDetails + 'Remote IP: not logged';

    GlobalLogFile.AddEvent(etInformation, 'Activity', sText, sDetails);
  end;
end;

procedure TevRRServerDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
var
  S: IevSession;
  SentBytes: Cardinal;
begin
  inherited;
  if not IsTransportDatagram(ADatagramHeader) then
  begin
    S := Mainboard.MaleConnector.GetSession(ASession.SessionID) as IevSession;
    if not Assigned(S) then
    begin
      CreateRBSession(ADatagramHeader, ASession);
      S := Mainboard.MaleConnector.GetSession(ASession.SessionID) as IevSession;
    end;

    CheckCondition(Assigned(S), 'RB session is not found');
    S.SendData(AStream, ADatagramHeader, SentBytes);
    Handled := True;
  end;
end;

function TevRRServerDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoClientAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoRWAdapterAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoDNetConnectorAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoDNetMapleConnectorAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoJavaESSAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoJavaWCAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoJavaMyHRAdminAppInfo.AppID);
end;

procedure TevRRServerDispatcher.CreateRBSession(const ADatagramHeader: IevDatagramHeader; const ARRSession: IevSession);
var
  SessionID: TisGUID;
  BrokerAddress: String;
  User, Password, ContextID: String;
  i: Integer;
begin
  CheckCondition(Assigned(ARRSession), 'Client session is not found');

  BrokerAddress := Mainboard.AppSettings.AsString['General\RequestBroker'];

  User := ADatagramHeader.User;
  Password := ADatagramHeader.Password;
  SessionID := ADatagramHeader.SessionID;
  ContextID := ADatagramHeader.ContextID;
  AddUsersActivityEvent('Authentication', SessionID, true);

  SessionID := Mainboard.MaleConnector.CreateConnection(BrokerAddress, IntToStr(TCPClient_Port),
    User, Password, SessionID, ContextID,
    Mainboard.AppSettings.AsString['Network\RRtoRBIPAddr']);

  CheckCondition(SessionID = ADatagramHeader.SessionID, 'Cannot create RB session');

  if AnsiSameStr(ARRSession.AppID, EvoDNetConnectorAppInfo.AppID) or
     AnsiSameStr(ARRSession.AppID, EvoDNetMapleConnectorAppInfo.AppID) or  
     AnsiSameStr(ARRSession.AppID, EvoJavaESSAppInfo.AppID) or
     AnsiSameStr(ARRSession.AppID, EvoJavaWCAppInfo.AppID) or
     AnsiSameStr(ARRSession.AppID, EvoJavaMyHRAdminAppInfo.AppID) then
  begin
    for i := 2 to 5 do
      Mainboard.MaleConnector.CreateConnection(BrokerAddress, IntToStr(TCPClient_Port),
        User, Password, SessionID, ContextID,
        Mainboard.AppSettings.AsString['Network\RRtoRBIPAddr']);
  end;

  AddUsersActivityEvent('Logged in', SessionID, True);
end;

procedure TevRRServerDispatcher.DoOnHandshake(var AEncryption: TevEncryptionType;
 var ACompression: TisCompressionLevel);
var
  MinCompression: TisCompressionLevel;
begin
  inherited;
  AEncryption := etSSL;
  MinCompression := Mainboard.AppSettings.GetValue('RemoteRelay\MinCompression', clNone);
  if ACompression < MinCompression then
    ACompression := MinCompression;
end;

function TevRRServerDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
begin
  CreateRBSession(ADatagram.Header, FindSessionByID(ADatagram.Header.SessionID));
  Result := True;
end;

procedure TevRRServerDispatcher.DoOnSessionStateChange(
  const ASession: IevSession; const APreviousState: TevSessionState);
begin
  inherited;
  if ASession.State = ssInvalid then
  begin
    Mainboard.MaleConnector.Disconnect(ASession.SessionID);
    AddUsersActivityEvent('Disconnected', ASession, true);
  end;
end;

{ TevTCPClient }

function TevTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRRClientDispatcher.Create;
end;

{ TevRRController }

function TevRRController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRRControllerDispatcher.Create;
end;

procedure TevRRController.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(RRController_Port));
  SetIPAddress('127.0.0.1');  
end;

{ TevRRControllerDispatcher }

function TevRRControllerDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoDeployMgrAppInfo.AppID);
end;

procedure TevRRControllerDispatcher.dmGetConnectionStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := Mainboard.Control.GetConnectionStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRRControllerDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

  Res := Mainboard.Control.GetFilteredLogEvents(Query);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRRControllerDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.Control.GetLogEventDetails(ADatagram.Params.Value['EventNbr']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRRControllerDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PageCount: Integer;
  Res: IisParamsCollection;
begin
  Res := Mainboard.Control.GetLogEvents(ADatagram.Params.Value['PageNbr'],
            ADatagram.Params.Value['EventsPerPage'],
            PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevRRControllerDispatcher.dmGetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  Mainboard.Control.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.Params.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TevRRControllerDispatcher.dmGetSessionsStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := Mainboard.Control.GetSessionsStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRRControllerDispatcher.dmGetSettings(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := Mainboard.Control.GetSettings;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRRControllerDispatcher.dmGetStackInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Mainboard.Control.GetStackInfo);
end;

procedure TevRRControllerDispatcher.dmSetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  Mainboard.Control.SetLogFileThresholdInfo(ADatagram.Params.Value['APurgeRecThreshold'],
    ADatagram.Params.Value['APurgeRecCount']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRRControllerDispatcher.dmSetSettings(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  S: IisListOfValues;
begin
  S := IInterface(ADatagram.Params.Value['ASettings']) as IisListOfValues;
  Mainboard.Control.SetSettings(S);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRRControllerDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RRCONTROLLER_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_RRCONTROLLER_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_RRCONTROLLER_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_RRCONTROLLER_GETSETTINGS, dmGetSettings);
  AddHandler(METHOD_RRCONTROLLER_SETSETTINGS, dmSetSettings);
  AddHandler(METHOD_RRCONTROLLER_GETCONNECTIONSTATUS, dmGetConnectionStatus);
  AddHandler(METHOD_RRCONTROLLER_GETLOGFILETHRESHOLDINFO, dmGetLogFileThresholdInfo);
  AddHandler(METHOD_RRCONTROLLER_SETLOGFILETHRESHOLDINFO, dmSetLogFileThresholdInfo);
  AddHandler(METHOD_RRCONTROLLER_GETSESSIONSSTATUS, dmGetSessionsStatus);
  AddHandler(METHOD_RRCONTROLLER_GETSTACKINFO, dmGetStackInfo);
end;

{ TevRRControl }

function TevRRControl.GetSessionsStatus: IisParamsCollection;
const
  SessionStates: array [Low(TevSessionState)..High(TevSessionState)] of string = (
    'Inactive', 'Performing Handshake', 'Waiting For Login', 'Performing Login',
    'Active', 'Merging', 'Invalid');
var
  i, j: Integer;
  LV: IisListOfValues;
  Session: IevSession;
  s1, s2: String;
  Sessions, Connections: IisList;
  AppInfo: TisAppInfo;
begin
  Result := TisParamsCollection.Create;

  Sessions := Mainboard.FemaleConnector.DatagramDispatcher.GetSessions;

  for i := 0 to Sessions.Count - 1 do
  begin
    Session := Sessions[i] as IevSession;
    LV := Result.AddParams(Session.SessionID);

    AppInfo := AppInfoByID(Session.AppID);
    s1 := AppInfo.Abbr;
    if (Length(s1) > 3) and  StartsWith(s1, 'Evo') then
      Delete(s1, 1, 3);

    LV.AddValue('AppID', s1);
    LV.AddValue('Login', Session.Login);
    LV.AddValue('State', Session.StateDescr);
    LV.AddValue('Encryption', Session.EncryptionTypeDscr);
    LV.AddValue('Compression', Session.CompressionLevel);

    s1 := '';
    Connections := Session.Transmitter.TCPCommunicator.GetConnectionsBySessionID(Session.SessionID);
    for j := 0 to Connections.Count - 1 do
    begin
      s2 := (Connections[j] as IisTCPStreamAsyncConnection).GetRemoteIPAddress;
      if not Contains(s1, s2) then
        AddStrValue(s1, s2, ',');
    end;
    LV.AddValue('Connections', Connections.Count);
    LV.AddValue('IPAddress', s1);
  end;
end;

function TevRRControl.GetConnectionStatus: IisParamsCollection;
var
  L: IisList;
  i: Integer;
  LV: IisListOfValues;
begin
  Result := TisParamsCollection.Create;

  L := Mainboard.FemaleConnector.DatagramDispatcher.Transmitter.TCPCommunicator.GetAllConnections;
  for i := 0 to L.Count - 1 do
  begin
    LV := Result.AddParams(IntToStr(i));
    LV.AddValue('IPAddress', (L[i] as IisTCPSocket).GetRemoteIPAddress);
  end;
end;

function TevRRControl.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  L: IisList;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  i: Integer;
begin
  L := GlobalLogFile.GetEvents(AQuery);
  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevRRControl.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  Result := TisListOfValues.Create;
  Event:= GlobalLogFile.Items[EventNbr];
  Result.AddValue('EventNbr', Event.Nbr);
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);
end;

function TevRRControl.GetLogEvents(const PageNbr, EventsPerPage: Integer;
  out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  GlobalLogFile.Lock;
  try
    L := GlobalLogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := GlobalLogFile.Count div EventsPerPage;
    if GlobalLogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

procedure TevRRControl.GetLogFileThresholdInfo(out APurgeRecThreshold,
  APurgeRecCount: Integer);
begin
  APurgeRecThreshold := GlobalLogFile.PurgeRecThreshold;
  APurgeRecCount := GlobalLogFile.PurgeRecCount;
end;

function TevRRControl.GetSettings: IisListOfValues;
var
  s: String;
begin
  Result := TisListOfValues.Create;
  Result.AddValue('RequestBroker', Mainboard.AppSettings.AsString['General\RequestBroker']);
  s := Mainboard.FemaleConnector.Port;
  s := StringReplace(s, ':SSL', '', [rfReplaceAll, rfIgnoreCase]);
  Result.AddValue('ClientPorts', s);
  Result.AddValue('MinCompression', Mainboard.AppSettings.GetValue('RemoteRelay\MinCompression', clNone));
end;

procedure TevRRControl.SetLogFileThresholdInfo(const APurgeRecThreshold,
  APurgeRecCount: Integer);
begin
  Mainboard.AppSettings.AsInteger['General\LogFilePurgeRecThreshold'] := APurgeRecThreshold;
  Mainboard.AppSettings.AsInteger['General\LogFilePurgeRecCount'] := APurgeRecCount;

  GlobalLogFile.PurgeRecCount := APurgeRecCount;
  GlobalLogFile.PurgeRecThreshold := APurgeRecThreshold;
end;

procedure TevRRControl.SetSettings(const ASettings: IisListOfValues);
var
  s, sPorts: String;
begin
  if ASettings.ValueExists('RequestBroker') then
    Mainboard.AppSettings.AsString['General\RequestBroker'] := ASettings.Value['RequestBroker'];

  if ASettings.ValueExists('ClientPorts') then
  begin
    Mainboard.FemaleConnector.Active := False;
    s := ASettings.Value['ClientPorts'];
    sPorts := '';
    while s <> '' do
      AddStrValue(sPorts, GetNextStrValue(s, ',') + ':SSL', ',');
    Mainboard.AppSettings.AsString['RemoteRelay\ClientPorts'] := sPorts;
    Mainboard.FemaleConnector.Port := sPorts;
    if Mainboard.FemaleConnector.Port <> '' then
      Mainboard.FemaleConnector.Active := True;
  end;

  if ASettings.ValueExists('MinCompression') then
    Mainboard.AppSettings.AsInteger['RemoteRelay\MinCompression'] := ASettings.Value['MinCompression'];
end;

function TevRRControl.GetStackInfo: String;
begin
  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      Result := GetStack(E);
  end;
end;

end.

