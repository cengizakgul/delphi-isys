  {$WARN SYMBOL_PLATFORM OFF}
unit EvDeploymentController;

interface

uses isBaseClasses, Windows, SysUtils, WinSvc, Classes, isSocket, isTransmitter,
     EvTransportShared, EvConsts, isAppIDs, ISZippingRoutines, EvTransportDatagrams,
     isBasicUtils, EvStreamUtils, evRemoteMethods, EvDMInterfaces, SEncryptionRoutines,
     EvTransportInterfaces, isSettings, isLogFile, isExceptions, ISErrorUtils,
     isSingleInstanceApp, EvContext;

type
   IevUDPMessenger = interface
   ['{FCEB1552-6932-4D67-A372-BAB3D4A950D9}']
     procedure ReportLocationBack(const AHost: String; const APort: Integer);   
   end;

   IevDeploymentController = interface
   ['{7A2CBC95-7790-46DE-8C02-E763C855C88D}']
     function  UDPMessenger: IevUDPMessenger;
     function  GetActive: Boolean;
     procedure SetActive(const AValue: Boolean);
     property  Active: Boolean read GetActive write SetActive;
    end;

   TevDeploymentController = class(TevCustomTCPServer, IevDeploymentController)
   private
     FUDPMessenger: IevUDPMessenger;
   protected
     function  UDPMessenger: IevUDPMessenger;
     procedure DoOnConstruction; override;
     function  CreateDispatcher: IevDatagramDispatcher; override;
   public
     destructor Destroy; override;
   end;

var DeploymentController: IevDeploymentController;

implementation

type
  TevDeploymentControllerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmSetAppsInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetAppsInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStackInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmInstallApp(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmUninstallApp(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetAppArchiveFileName(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIsServiceInstalled(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmInstallService(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmUnInstallService(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmIsServiceStarted(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmStartService(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmStopService(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRestartService(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    function  CreateSessionExtension: IInterface; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure RegisterHandlers; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession;  const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
  end;


  IevControlledApps = interface
  ['{2CA3ABEB-1D18-4161-815C-723DF1E0EA61}']
    procedure SetAppsInfo(const AppsInfo: IisParamsCollection);
    function  GetAppsInfo: IisParamsCollection;
    function  ForwardRequestToAppIfNeeds(const AMethod: String; const ADatagram: IEvDualStream): IEvDualStream;
  end;

  TevControlledApps = class(TevCustomTCPClient, IevControlledApps)
  private
    FAppsInfo: IisParamsCollection;
    function  ForwardRequest(const ASession: IevSession; const ADatagram: IEvDualStream): IEvDualStream;
  protected
    procedure DoOnConstruction; override;
    procedure SetAppsInfo(const AppsInfo: IisParamsCollection);
    function  GetAppsInfo: IisParamsCollection;
    function  ForwardRequestToAppIfNeeds(const AMethod: String; const ADatagram: IEvDualStream): IEvDualStream;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  public
  end;


  TevDMControl = class(TisInterfacedObject, IevDMControl)
  private
    FControlledApps: IevControlledApps;
    FDMConfigFile: IisSettings; 
    function  CheckApiCall(const R: Cardinal): Cardinal;
    function  GetServiceConfigParam(const AppName, ServiceName, ParamName: String): String;
    function  GetServicesToDeploy(const AppName: String): IisStringListRO;
    procedure TerminateAllExes(const AppName: String);
  protected
    procedure DoOnConstruction; override;

    procedure SetAppsInfo(const AppsInfo: IisParamsCollection);
    function  GetAppsInfo: IisParamsCollection;
    procedure InstallApp(const AppName: String; const ArchiveFile: String);
    procedure UninstallApp(const AppName: String);
    function  GetAppArchiveFileName(const AppName: String): String;
    function  IsServiceInstalled(const AppName: String; const ServiceName: String): Boolean;
    procedure InstallService(const AppName: String; const ServiceName: String);
    procedure UninstallService(const AppName: String; const ServiceName: String);
    function  IsServiceStarted(const AppName: String; const ServiceName: String): Boolean;
    procedure StartService(const AppName: String; const ServiceName: String);
    procedure StopService(const AppName: String; const ServiceName: String);
    procedure RestartService(const AppName: String; const ServiceName: String);
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetStackInfo: String;
  end;


  // Service Discovery

  TevUDPDataDispatcher = class(TevMessageDispatcher)
  private
    procedure dmReportLocation(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    procedure RegisterHandlers; override;
  end;

  TevUDPMessenger = class(TevCustomUDPMessenger, IevUDPMessenger)
  private
    FDiscoveryService: TisGUID;
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevMessageDispatcher; override;
    procedure ReportLocationBack(const AHost: String; const APort: Integer);
  end;


  PServiceStatusEx = ^TServiceStatusEx;
  TServiceStatusEx = record
    dwServiceType: DWORD;
    dwCurrentState: DWORD;
    dwControlsAccepted: DWORD;
    dwWin32ExitCode: DWORD;
    dwServiceSpecificExitCode: DWORD;
    dwCheckPoint: DWORD;
    dwWaitHint: DWORD;
    dwProcessId: DWORD;
    dwServiceFlags: DWORD;
  end;

function QueryServiceStatusEx(hService: SC_HANDLE; dwInfoLevel: DWORD;
  Buffer: Pointer; dwBufferSize: DWORD; pdwBufferSizeNeeded: PInteger): BOOL; stdcall;
  external advapi32 name 'QueryServiceStatusEx';

const DeploymentCFGFile = 'deployment.cfg';
var AppFolder: String;

{ TevDeploymentControllerDatagramDispatcher }

procedure TevDeploymentControllerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_DM_SETAPPSINFO, dmSetAppsInfo);
  AddHandler(METHOD_DM_GETAPPSINFO, dmGetAppsInfo);

  AddHandler(METHOD_DM_INSTALLAPP, dmInstallApp);
  AddHandler(METHOD_DM_UNINSTALLAPP, dmUninstallApp);
  AddHandler(METHOD_DM_GETAPPARCHIVEFILENAME, dmGetAppArchiveFileName);
  AddHandler(METHOD_DM_ISSERVICEINSTALLED, dmIsServiceInstalled);
  AddHandler(METHOD_DM_INSTALLSERVICE, dmInstallService);
  AddHandler(METHOD_DM_UNINSTALLSERVICE, dmUnInstallService);
  AddHandler(METHOD_DM_ISSERVICESTARTED, dmIsServiceStarted);
  AddHandler(METHOD_DM_STARTSERVICE, dmStartService);
  AddHandler(METHOD_DM_STOPSERVICE, dmStopService);
  AddHandler(METHOD_DM_RESTARTSERVICE, dmRestartService);

  AddHandler(METHOD_DM_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_DM_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_DM_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_DM_GETSTACKINFO, dmGetStackInfo);
end;

function TevDeploymentControllerDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoMgmtConsoleAppInfo.AppID);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmInstallApp(const ADatagram: IevDatagram;
  out AResult: IevDatagram);
var
  s, sInstallFolder: String;
begin
  sInstallFolder := AppFolder + ADatagram.Params.Value['AppName'] + '\';
  s := sInstallFolder + ExtractFileName(ADatagram.Params.Value['ArchiveFile']);
  DeleteFile(s);
  ForceDirectories(sInstallFolder);
  (IInterface(ADatagram.Params.Value['FileContent']) as IevDualStream).SaveToFile(s);
  (ThisSession.Extension as IevDMControl).InstallApp(ADatagram.Params.Value['AppName'], s);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDeploymentControllerDatagramDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
var
  Res: IevDualStream;
  ControlledApps: IevControlledApps;
  SentBytes: Cardinal;
begin
  inherited;
  ControlledApps := TevDMControl((ASession.Extension as IisInterfacedObject).GetImplementation).FControlledApps;

  Res := ControlledApps.ForwardRequestToAppIfNeeds(ADatagramHeader.Method, AStream);
  if Assigned(Res) then
  begin
    ASession.SendData(Res, nil, SentBytes);
    Handled := True;
  end;
end;


{TevDeploymentControl}

function TevDMControl.CheckApiCall(const R: Cardinal): Cardinal;
begin
  if R = 0 then
    RaiseLastOSError;
  Result := R;
end;

function TevDMControl.GetServiceConfigParam(const AppName, ServiceName, ParamName: String): String;
var
  DeploymentCFG: IisSettings;
  S: String;
begin
  s := AppFolder + AppName + '\' + DeploymentCFGFile;
  if FileExists(s) then
  begin
    DeploymentCFG := TisSettingsINI.Create(s);
    Result := DeploymentCFG.AsString[ServiceName + '\' + ParamName];
  end
  else
    Result := '';
end;

procedure TevDMControl.DoOnConstruction;
var
  AppsInfo: IisParamsCollection;
  App: IisListOfValues;
  L: IisStringListRO;
  i: Integer;
  s: String;
begin
  inherited;
  FDMConfigFile := TisSettingsINI.Create(ChangeFileExt(AppFileName, '.cfg'));

  // restore AppsInfo from CFG file
  AppsInfo := TisParamsCollection.Create;
  L := FDMConfigFile.GetChildNodes('Apps');
  for i := 0 to L.Count - 1 do
  begin
    s := 'Apps\' + L[i] + '\';
    if (FDMConfigFile.AsString[s + 'MethodPrefix'] <> '') and
       (FDMConfigFile.AsString[s + 'Port'] <> '') then
    begin
      App := AppsInfo.AddParams(L[i]);
      App.AddValue('MethodPrefix', FDMConfigFile.AsString[s + 'MethodPrefix']);
      App.AddValue('Port', FDMConfigFile.AsString[s + 'Port']);
      App.AddValue('User', DecryptStringLocaly(BinToStr(App.TryGetValue('User', '')), ''));
      App.AddValue('Password', DecryptStringLocaly(BinToStr(App.TryGetValue('Password', '')), ''));
    end;  
  end;
  FControlledApps := TevControlledApps.Create;
  FControlledApps.SetAppsInfo(AppsInfo);
end;

procedure TevDMControl.InstallApp(const AppName: String; const ArchiveFile: String);
var
  Content: IisStringList;
  ServiceStatus: array of record
                            ServiceName: String;
                            WasInstalled: Boolean;
                            WasStarted: Boolean;
                          end;
  sDeploymentFile, sInstallFolder, OldAppArchiveFile: String;
  Services: IisStringListRO;
  i: Integer;
begin
  sInstallFolder := AppFolder + AppName + '\';
  sDeploymentFile := sInstallFolder + DeploymentCFGFile;

  // identify what services are running
  if FileExists(sDeploymentFile) then
  begin
    Services := GetServicesToDeploy(AppName);
    SetLength(ServiceStatus, Services.Count);
    for i := 0 to Services.Count - 1 do
    begin
      ServiceStatus[i].ServiceName := Services[i];
      ServiceStatus[i].WasStarted := IsServiceStarted(AppName, Services[i]);
      if ServiceStatus[i].WasStarted then
        ServiceStatus[i].WasInstalled := True
      else
        ServiceStatus[i].WasInstalled := IsServiceInstalled(AppName, Services[i]);
    end;
  end
  else
    SetLength(ServiceStatus, 0);

  // Stop and uninstall services
  try
    for i := Low(ServiceStatus) to High(ServiceStatus) do
      UninstallService(AppName, ServiceStatus[i].ServiceName);
  finally
    TerminateAllExes(AppName);
  end;  

  ForceDirectories(sInstallFolder);

  // ctx_DataAccess.Copying files from the archive
  ExtractFromFile(ArchiveFile, sInstallFolder, DeploymentCFGFile);
  Services := GetServicesToDeploy(AppName);
  Content := TisStringList.Create;
  for i := 0 to Services.Count - 1 do
  begin
    Content.CommaText := GetServiceConfigParam(AppName, Services[i], 'Content');
    ExtractFromFile(ArchiveFile, sInstallFolder, Content);
  end;

  // Install services which were installed and start which were started
  for i := Low(ServiceStatus) to High(ServiceStatus) do
    if Services.IndexOf(ServiceStatus[i].ServiceName) <> -1 then
    begin
      if ServiceStatus[i].WasInstalled then
        InstallService(AppName, ServiceStatus[i].ServiceName);

      if ServiceStatus[i].WasStarted then
        StartService(AppName, ServiceStatus[i].ServiceName);
    end;

  OldAppArchiveFile := FDMConfigFile.AsString['Versions\' + AppName];
  if not AnsiSameText(OldAppArchiveFile, ArchiveFile) then
  begin
    if OldAppArchiveFile <> '' then
      DeleteFile(OldAppArchiveFile);
    FDMConfigFile.AsString['Versions\' + AppName] := ArchiveFile;
  end;

  GlobalLogFile.AddEvent(etInformation, 'Installer',
    AppName + ' has been installed successfully', 'Application archive file name is ' + ArchiveFile);
end;


procedure TevDeploymentControllerDatagramDispatcher.dmInstallService(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).InstallService(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmIsServiceInstalled(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := (ThisSession.Extension as IevDMControl).IsServiceInstalled(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmIsServiceStarted(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: Boolean;
begin
  Res := (ThisSession.Extension as IevDMControl).IsServiceStarted(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmRestartService(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).RestartService(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmStartService(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).StartService(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmStopService(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).StopService(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmUnInstallService(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).UninstallService(
    ADatagram.Params.Value['AppName'], ADatagram.Params.Value['ServiceName']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmGetAppArchiveFileName(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT,
    (ThisSession.Extension as IevDMControl).GetAppArchiveFileName(ADatagram.Params.Value['AppName']));
end;

procedure TevDeploymentControllerDatagramDispatcher.dmUninstallApp(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).UninstallApp(ADatagram.Params.Value['AppName']);
  AResult := CreateResponseFor(ADatagram.Header);  
end;

procedure TevDeploymentControllerDatagramDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  PageCount: Integer;
begin
  Res := (ThisSession.Extension as IevDMControl).GetLogEvents(
    ADatagram.Params.Value['PageNbr'],
    ADatagram.Params.Value['EventsPerPage'],
    PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := (ThisSession.Extension as IevDMControl).GetLogEventDetails(
    ADatagram.Params.Value['EventNbr']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

  Res := (ThisSession.Extension as IevDMControl).GetFilteredLogEvents(Query);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmGetAppsInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, (ThisSession.Extension as IevDMControl).GetAppsInfo);
end;

procedure TevDeploymentControllerDatagramDispatcher.dmSetAppsInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  (ThisSession.Extension as IevDMControl).SetAppsInfo(
    IInterface(ADatagram.Params.Value['AppsInfo']) as IisParamsCollection);
  AResult := CreateResponseFor(ADatagram.Header);
end;

function TevDeploymentControllerDatagramDispatcher.CreateSessionExtension: IInterface;
begin
  Result := TevDMControl.Create;
end;

procedure TevDeploymentControllerDatagramDispatcher.dmGetStackInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, (ThisSession.Extension as IevDMControl).GetStackInfo);
end;

{ TevDeploymentController }

function TevDeploymentController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevDeploymentControllerDatagramDispatcher.Create;
end;

procedure TevDeploymentController.DoOnConstruction;
begin
  inherited;
  ClearDir(AppTempFolder);
  FUDPMessenger := TevUDPMessenger.Create;

  SetPort(IntToStr(DMController_Port));
  GlobalLogFile.AddEvent(etInformation, 'Deployment Manager', 'Service started', '');
end;

destructor TevDeploymentController.Destroy;
begin
  GlobalLogFile.AddEvent(etInformation, 'Deployment Manager', 'Service stopped', '');
  ClearDir(AppTempFolder, True);  
  inherited;
end;

function TevDeploymentController.UDPMessenger: IevUDPMessenger;
begin
  Result := FUDPMessenger;
end;

{ TevControlledApps }

function TevControlledApps.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevDatagramDispatcher.Create;
end;

function TevControlledApps.ForwardRequest(const ASession: IevSession; const ADatagram: IEvDualStream): IEvDualStream;
var
  DH: IevDatagramHeader;
  OriginalSessionID: TisGUID;
  OriginalSequenceNbr: Int64;
  SentBytes, ReceivedBytes, TrasmissionTime: Cardinal;
begin
  DH := TevDatagram.GetDatagramHeader(ADatagram);
  OriginalSessionID := DH.SessionID;
  OriginalSequenceNbr := DH.SequenceNbr;

  DH.SessionID := ASession.SessionID;
  DH.SequenceNbr := 0;
  TevDatagram.PatchHeader(ADatagram, DH);   // patch datagram header

  Result := ASession.SendRequest(ADatagram, DH.SequenceNbr, True, SentBytes, ReceivedBytes, TrasmissionTime);

  DH.Method := METHOD_RESULT;
  DH.SessionID := OriginalSessionID;
  DH.SequenceNbr := OriginalSequenceNbr;
  TevDatagram.PatchHeader(Result, DH);   // return datagram header back
end;


function TevDMControl.IsServiceInstalled(const AppName, ServiceName: String): Boolean;
var
  Handle: SC_Handle;
  ServiceControlManagerHandle: SC_Handle;
  AppID: String;
begin
  AppID := GetServiceConfigParam(AppName, ServiceName, 'AppID');
  ServiceControlManagerHandle := CheckApiCall(OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS));
  try
    Handle := OpenService(ServiceControlManagerHandle, PChar(AppID), SERVICE_ALL_ACCESS);
    Result := Handle <> 0;
    if Result then
      CloseServiceHandle(Handle);
  finally
    CloseServiceHandle(ServiceControlManagerHandle);
  end;
end;

procedure TevDMControl.InstallService(const AppName, ServiceName: String);
var
  Handle: SC_Handle;
  ServiceControlManagerHandle: SC_Handle;
  FileName: String;
  DisplayName: String;
  AppID: String;
begin
  AppID := GetServiceConfigParam(AppName, ServiceName, 'AppID');
  FileName := GetServiceConfigParam(AppName, ServiceName, 'ServiceFileName');
  DisplayName := GetServiceConfigParam(AppName, ServiceName, 'DisplayName');
  if (AppID = '') or (FileName = '') then
    raise EisException.CreateFmt('Service %s is not described in %s\%s', [ServiceName, AppName, DeploymentCFGFile]);

  FileName := AppFolder + AppName + '\' + FileName;
  if not FileExists(FileName) then
    raise EisException.CreateFmt('File %s does not exist', [FileName]);

  ServiceControlManagerHandle := CheckApiCall(OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS));
  try
    Handle := CheckApiCall(
      CreateService(ServiceControlManagerHandle,
                    PChar(AppID),
                    PChar(DisplayName),
                    SERVICE_ALL_ACCESS,
                    SERVICE_WIN32_OWN_PROCESS,
                    SERVICE_AUTO_START,
                    SERVICE_ERROR_IGNORE,
                    PChar(FileName),
                    nil, nil, nil, nil, nil));
    CloseServiceHandle(Handle);
  finally
    CloseServiceHandle(ServiceControlManagerHandle);
  end;

  GlobalLogFile.AddEvent(etInformation, 'Services',
    'Service ' + AppName + '.' + ServiceName + ' has been installed', '');
end;

procedure TevDMControl.UninstallService(const AppName, ServiceName: String);
var
  Handle: SC_Handle;
  ServiceControlManagerHandle: SC_Handle;
  AppID: String;
begin
  if not IsServiceInstalled(AppName, ServiceName) then
    Exit;

  if IsServiceStarted(AppName, ServiceName) then
    StopService(AppName, ServiceName);

  AppID := GetServiceConfigParam(AppName, ServiceName, 'AppID');
  ServiceControlManagerHandle := CheckApiCall(OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS));
  try
    Handle := CheckApiCall(OpenService(ServiceControlManagerHandle, PChar(AppID), SERVICE_ALL_ACCESS));
    try
      Win32Check(DeleteService(Handle));
      Sleep(500);
    finally
      CloseServiceHandle(Handle);
    end;
  finally
    CloseServiceHandle(ServiceControlManagerHandle);
  end;

  GlobalLogFile.AddEvent(etInformation, 'Services',
    'Service ' + AppName + '.' + ServiceName + ' has been uninstalled', '');
end;

function TevDMControl.IsServiceStarted(const AppName, ServiceName: String): Boolean;
var
  Handle: SC_Handle;
  ServiceControlManagerHandle: SC_Handle;
  ServiceStatus: _SERVICE_STATUS;
  AppID: String;
begin
  AppID := GetServiceConfigParam(AppName, ServiceName, 'AppID');
  ServiceControlManagerHandle := CheckApiCall(OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS));
  try
    Handle := OpenService(ServiceControlManagerHandle, PChar(AppID), SERVICE_ALL_ACCESS);
    if Handle <> 0 then
    begin
      try
        Win32Check(QueryServiceStatus(Handle, ServiceStatus));
        Result := ServiceStatus.dwCurrentState = SERVICE_RUNNING;
      finally
        CloseServiceHandle(Handle);
      end;
      end
    else
      Result := False;
  finally
    CloseServiceHandle(ServiceControlManagerHandle);
  end;

  // Check if it's running as a regular process
  if not Result then
    Result := AppIsRunning(AppID);
end;

procedure TevDMControl.StartService(const AppName, ServiceName: String);
const
  Timeout = 30000;
var
  Handle: SC_Handle;
  ServiceControlManagerHandle: SC_Handle;
  PCharNil: PChar;
  ServiceStatus: _SERVICE_STATUS;
  TimeStamp: Cardinal;
  AppID: String;
begin
  if not IsServiceInstalled(AppName, ServiceName) then
    Exit;

  AppID := GetServiceConfigParam(AppName, ServiceName, 'AppID');
  ServiceControlManagerHandle := CheckApiCall(OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS));
  try
    Handle := CheckApiCall(OpenService(ServiceControlManagerHandle, PChar(AppID), SERVICE_ALL_ACCESS));
    try
      Win32Check(QueryServiceStatus(Handle, ServiceStatus));
      if ServiceStatus.dwCurrentState <> SERVICE_RUNNING then
      begin
        PCharNil := nil;
        Win32Check(WinSvc.StartService(Handle, 0, PCharNil));
        TimeStamp := GetTickCount;
        while ServiceStatus.dwCurrentState <> SERVICE_RUNNING do
        begin
          Sleep(10);
          if GetTickCount > TimeStamp + Timeout then
            Break;
          Win32Check(QueryServiceStatus(Handle, ServiceStatus));
        end;
      end;
    finally
      CloseServiceHandle(Handle);
    end;
  finally
    CloseServiceHandle(ServiceControlManagerHandle);
  end;

  GlobalLogFile.AddEvent(etInformation, 'Services',
    'Service ' + AppName + '.' + ServiceName + ' has been started', '');
end;

procedure TevDMControl.StopService(const AppName, ServiceName: String);
var
  Handle: SC_Handle;
  ServiceControlManagerHandle: SC_Handle;
  ServiceStatus: _SERVICE_STATUS;
  TimeStamp: Cardinal;
  ServiceStatusEx: TServiceStatusEx;
  DummyInteger: Integer;
  ProcessHandle: THandle;
  AppID: String;
const
  Timeout = 60000;
  SC_STATUS_PROCESS_INFO = 0;
begin
  if not IsServiceInstalled(AppName, ServiceName) then
    Exit;

  AppID := GetServiceConfigParam(AppName, ServiceName, 'AppID');
  ServiceControlManagerHandle := CheckApiCall(OpenSCManager('', nil, SC_MANAGER_ALL_ACCESS));
  try
    Handle := CheckApiCall(OpenService(ServiceControlManagerHandle, PChar(AppID), SERVICE_ALL_ACCESS));
    try
      Win32Check(QueryServiceStatus(Handle, ServiceStatus));
      if ServiceStatus.dwCurrentState <> SERVICE_STOPPED then
      begin
        Win32Check(ControlService(Handle, SERVICE_CONTROL_STOP, ServiceStatus));
        TimeStamp := GetTickCount;
        while ServiceStatus.dwCurrentState <> SERVICE_STOPPED do
        begin
          if GetTickCount > TimeStamp + Timeout then
            Break;
          Sleep(100);
          if GetTickCount > TimeStamp + Timeout - 1000 then
          begin
            Win32Check(QueryServiceStatusEx(Handle, SC_STATUS_PROCESS_INFO,
              @ServiceStatusEx, SizeOf(ServiceStatusEx), @DummyInteger));
            ProcessHandle := CheckApiCall(OpenProcess(PROCESS_TERMINATE, False, ServiceStatusEx.dwProcessId));
            try
              TerminateProcess(ProcessHandle, 0);
            finally
              CloseHandle(ProcessHandle);
            end;
            Sleep(1000);
          end;
          Win32Check(QueryServiceStatus(Handle, ServiceStatus));
        end;
      end;
    finally
      CloseServiceHandle(Handle);
    end;
  finally
    CloseServiceHandle(ServiceControlManagerHandle);
  end;

  GlobalLogFile.AddEvent(etInformation, 'Services',
    'Service ' + AppName + '.' + ServiceName + ' has been stopped', '');
end;

procedure TevDMControl.RestartService(const AppName, ServiceName: String);
begin
  if not IsServiceInstalled(AppName, ServiceName) then
    Exit;

  StopService(AppName, ServiceName);
  StartService(AppName, ServiceName);
end;

function TevDMControl.GetServicesToDeploy(const AppName: String): IisStringListRO;
var
  DeploymentCFG: IisSettings;
  i: Integer;
begin
  DeploymentCFG := TisSettingsINI.Create(AppFolder + AppName + '\' + DeploymentCFGFile);
  Result := DeploymentCFG.GetChildNodes('');

  for i := Result.Count - 1 downto 0 do
    if DeploymentCFG.AsString[Result[i] + '\ServiceFileName'] = '' then
      (Result as IisStringList).Delete(i);
end;

function TevDMControl.GetAppArchiveFileName(const AppName: String): String;
begin
  Result := FDMConfigFile.AsString['Versions\' + AppName];
  if FileExists(Result) then
    Result := ExtractFileName(Result)
  else
    Result := '';
end;

procedure TevDMControl.UninstallApp(const AppName: String);
var
  sInstallFolder, sDeploymentFile: String;
  Services: IisStringListRO;
  i: Integer;
begin
  sInstallFolder := AppFolder + AppName + '\';
  sDeploymentFile := sInstallFolder + DeploymentCFGFile;

  // Stop all services
  if FileExists(sDeploymentFile) then
  begin
    Services := GetServicesToDeploy(AppName);
    for i := 0 to Services.Count - 1 do
      UninstallService(AppName, Services[i]);
  end;

  // Clean up app folder
  ClearDir(sInstallFolder, True);

  FDMConfigFile.DeleteValue('Versions\' + AppName);

  GlobalLogFile.AddEvent(etInformation, 'Installer', AppName + ' has been uninstalled successfully', '');
end;

function TevDMControl.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  GlobalLogFile.Lock;
  try
    CheckCondition((EventNbr >= 0) and (EventNbr < GlobalLogFile.Count), 'Event number is incorrect');
    Event := GlobalLogFile[EventNbr];
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('EventNbr', Event.Nbr);  
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);
end;

function TevDMControl.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  GlobalLogFile.Lock;
  try
    L := GlobalLogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := GlobalLogFile.Count div EventsPerPage;
    if GlobalLogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevControlledApps.ForwardRequestToAppIfNeeds(
  const AMethod: String; const ADatagram: IEvDualStream): IEvDualStream;
var
  Session: IevSession;
  AppInfo: IisListOfValues;
  SessionID: TisGUID;
  i: Integer;
begin
  Result := nil;
  if not Assigned(FAppsInfo) then
    Exit;

  for i := 0 to FAppsInfo.Count - 1 do
  begin
    AppInfo := FAppsInfo.Params[i];
    if StartsWith(AMethod, AppInfo.TryGetValue('MethodPrefix', '')) then
      break
    else
      AppInfo := nil;
  end;

  if Assigned(AppInfo) then
  begin
    Lock;
    try
      SessionID := AppInfo.TryGetValue('SessionID', '');
      Session := FindSession(SessionID);
      if not Assigned(Session) then
      begin
        SessionID := CreateConnection('localhost', AppInfo.Value['Port'],
          AppInfo.TryGetValue('User', 'DeploymentManager'),
          AppInfo.TryGetValue('Password', ''),
          '', '');
        Session := FindSession(SessionID);
        AppInfo.AddValue('SessionID', SessionID);
      end;
    finally
      UnLock;
    end;

    Result := ForwardRequest(Session, ADatagram);
  end;
end;

procedure TevControlledApps.SetAppsInfo(const AppsInfo: IisParamsCollection);
var
  i: Integer;
  SessionID: TisGUID;
begin
  Lock;
  try
    if Assigned(FAppsInfo) then
      for i := 0 to FAppsInfo.Count - 1 do
      begin
        SessionID := FAppsInfo.Params[i].TryGetValue('SessionID', '');
        if SessionID <> '' then
          Disconnect(SessionID);
      end;

    FAppsInfo := AppsInfo;
  finally
    Unlock;
  end;
end;

function TevControlledApps.GetAppsInfo: IisParamsCollection;
begin
  Lock;
  try
    Result := FAppsInfo;
  finally
    Unlock;
  end;
end;

procedure TevControlledApps.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

{ TevUDPMessenger }

function TevUDPMessenger.CreateDispatcher: IevMessageDispatcher;
begin
  Result := TevUDPDataDispatcher.Create;
end;

procedure TevUDPMessenger.DoOnConstruction;
begin
  inherited;
  FDiscoveryService := OpenSocketForBroadcast(MCDiscovery_Port, DMDiscovery_Port);
end;

procedure TevUDPMessenger.ReportLocationBack(const AHost: String; const APort: Integer);
var
  D: IevDatagram;
  S: IisAsyncUDPSocket;
  InQueueSize, OutQueueSize: Integer;
  i: Integer;
begin
  Lock;
  try
    D := TevDatagram.Create(METHOD_SERVICEDISCOVERY_LOCATION);
    D.Params.AddValue('AppID', AppID);
    D.Params.AddValue('Host', GetComputerNameString);
    D.Params.AddValue('Version', AppVersion);    

    S := GetMessageDispatcher.Transmitter.UDPCommunicator.FindSocketByID(FDiscoveryService) as IisAsyncUDPSocket;
    if Assigned(S) then
    begin
      S.SetPeerSocket(AHost, APort);
      try
        GetMessageDispatcher.SendMessage(FDiscoveryService, D);
        for i := 1 to 10 do
        begin
          Sleep(100);
          GetMessageDispatcher.Transmitter.QueueStatus(InQueueSize, OutQueueSize);
          if OutQueueSize = 0 then
            break;
        end;
        Sleep(100);        
      finally
        S.SetPeerSocket(BROADCAST_IP, 0);
      end;
    end;
  finally
    Unlock;
  end;  
end;

{ TevUDPDataDispatcher }

procedure TevUDPDataDispatcher.dmReportLocation(const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  DeploymentController.UDPMessenger.ReportLocationBack(ADatagram.Params.Value['Host'],
                                                       ADatagram.Params.Value['Port']);
  AResult := nil;
end;

procedure TevUDPDataDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_SERVICEDISCOVERY_REPORTLOCATION, dmReportLocation);
end;

function TevDMControl.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  L := GlobalLogFile.GetEvents(AQuery);

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

procedure TevDMControl.SetAppsInfo(const AppsInfo: IisParamsCollection);
var
  i: Integer;
  App: IisListOfValues;
  s: String;
begin
  FControlledApps.SetAppsInfo(AppsInfo);

  // store in CFG file just in case
  FDMConfigFile.DeleteNode('Apps');
  for i := 0 to AppsInfo.Count - 1 do
  begin
    App := AppsInfo.Params[i];
    s := 'Apps\' + AppsInfo.ParamName(i) + '\';
    FDMConfigFile.AsString[s + 'MethodPrefix'] := App.Value['MethodPrefix'];
    FDMConfigFile.AsString[s + 'Port'] := App.Value['Port'];
    FDMConfigFile.AsString[s + 'User'] := StrToBin(EncryptStringLocaly(App.Value['User']));
    FDMConfigFile.AsString[s + 'Password'] := StrToBin(EncryptStringLocaly(App.Value['Password']));
  end;
end;

function TevDMControl.GetAppsInfo: IisParamsCollection;
begin
  Result := FControlledApps.GetAppsInfo;
end;

procedure TevDMControl.TerminateAllExes(const AppName: String);
var
  ExeList, NotTerminated: IisStringList;
begin
  ExeList := GetFilesByMask(AppFolder + AppName + '\*.exe');
  if not TerminateProcesses(ExeList, NotTerminated) then
    GlobalLogFile.AddEvent(etInformation, 'Installer',
      AppName + ': Cannot terminate processes', 'Not terminated processes:'#13 + NotTerminated.CommaText);
end;

function TevDMControl.GetStackInfo: String;
begin
  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      Result := GetStack(E);
  end;
end;

initialization
  SetGlobalLogFile(TLogFile.Create(ChangeFileExt(AppFileName, '.log')));
  GlobalLogFile.PurgeRecThreshold := DefaultPurgeRecThreshold;
  GlobalLogFile.PurgeRecCount := DefaultPurgeRecCount;

 AppFolder := AppDir + 'Applications\';
 ForceDirectories(AppFolder);

finalization
  SetGlobalLogFile(nil);

end.
