unit EvDMInterfaces;

interface

uses isBaseClasses, isLogFile;

type
  IevDMControl = interface
  ['{704EF14F-E19F-4500-9EBA-B37F8C11B438}']
    procedure SetAppsInfo(const AppsInfo: IisParamsCollection);
    function  GetAppsInfo: IisParamsCollection;
    procedure InstallApp(const AppName: String; const ArchiveFile: String);
    procedure UninstallApp(const AppName: String);
    function  GetAppArchiveFileName(const AppName: String): String;
    function  IsServiceInstalled(const AppName: String; const ServiceName: String): Boolean;
    procedure InstallService(const AppName: String; const ServiceName: String);
    procedure UninstallService(const AppName: String; const ServiceName: String);
    function  IsServiceStarted(const AppName: String; const ServiceName: String): Boolean;
    procedure StartService(const AppName: String; const ServiceName: String);
    procedure StopService(const AppName: String; const ServiceName: String);
    procedure RestartService(const AppName: String; const ServiceName: String);
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetStackInfo: String;
  end;

implementation

end.

