unit evRBServerMod;

interface

uses Classes, SysUtils, Windows, isBaseClasses, EvCommonInterfaces, isSocket, isStreamTCP, isTransmitter,
     EvTransportShared, EvMainboard, EvTransportDatagrams, Dialogs, EvTypes, evRemoteMethods,
     isBasicUtils, evContext, EvConsts, EvInitApp, evCommonDatagramDispatcher, EvStreamUtils,
     EvTransportInterfaces, ISZippingRoutines, EvBasicUtils, isAppIDs, isLogFile, isThreadManager,
     EvControllerInterfaces,  isErrorUtils, EvUtils, evExceptions, isExceptions,
     ISBasicClasses;


function RBControl: IevRBControl;

implementation

uses isSettings, EvRBController;

type
  TevRBDatagramDispatcher = class(TevCommonDatagramDispatcher)
  protected
    procedure RegisterHandlers; override;
  end;


  // Client commands dispatcher
  TevClientDatagramDispatcher = class(TevRBDatagramDispatcher)
  private
    FInternalRRIPs: IisStringList;
    procedure PrepareInternalRRIP;

    // redeclare this method for security reasons
    procedure dmGlobalSettingsGet(const ADatagram: IevDatagram; out AResult: IevDatagram); reintroduce;

  protected
    procedure DoOnConstruction; override;
    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
    procedure RegisterHandlers; override;
  end;


  // Connections Clients -> RB
  TevTCPServer = class(TevCustomTCPServer, IevTCPServer)
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


  // Pool of Request Processors
  IevRPInPoolInfo = interface
  ['{9B7C5799-29DD-4365-81B3-6FF6920C17DB}']
    function  Host: String;
    function  MaxScore: Integer;
    function  CurrentScore: Integer;
    function  MaxLongRequests: Integer;
    function  LongRequests: Integer;
    function  NoSlotsForLongRequests: Boolean;
    procedure IncNewLongRequestCount;
    procedure DecNewLongRequestCount;
    function  GetNewLongRequestCount: Integer;
    procedure SetEnabled(const AValue: Boolean);
    function  GetEnabled: Boolean;
    function  RequestCount: Integer;
    procedure IncNewRequestCount;
    procedure DecNewRequestCount;
    function  GetNewRequestCount: Integer;
    function  GetSession: IevSession;
    procedure SetSession(const ASession: IevSession);
    function  Connected: Boolean;
    procedure UpdateCurrentState;
    property  Enabled: Boolean read GetEnabled write SetEnabled;
    property  Session: IevSession read GetSession write SetSession;
  end;

  TevRPInPoolInfo = class(TisNamedObject, IevRPInPoolInfo)
  private
    FLock: IisLock;
    FEnabled: Boolean;
    FSession: IevSession;
    FNewRequestCount: Integer;
    FNewLongRequestCount: Integer;
    FRequestCount: Integer;
    FMaxScore: Integer;
    FCurrentScore: Integer;
    FMaxLongRequests: Integer;
    FLongRequests: Integer;
  protected
    function  Host: String;
    function  MaxScore: Integer;
    function  CurrentScore: Integer;
    function  MaxLongRequests: Integer;
    function  LongRequests: Integer;
    function  NoSlotsForLongRequests: Boolean;
    procedure IncNewLongRequestCount;
    procedure DecNewLongRequestCount;
    function  GetNewLongRequestCount: Integer;
    procedure UpdateCurrentState;
    function  Connected: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetEnabled: Boolean;
    function  RequestCount: Integer;
    procedure IncNewRequestCount;
    procedure DecNewRequestCount;
    function  GetNewRequestCount: Integer;
    function  GetSession: IevSession;
    procedure SetSession(const ASession: IevSession);
  public
    constructor Create(const ARPHost: String); reintroduce;
  end;


  TevItemCopyOption = (coAll, coNotConnectedOnly, coConnectedOnly);

  IevRPPool = interface
  ['{4C73A697-8CD7-473A-9A56-9134CF2C7040}']
    procedure ApplyConfiguration;
    function  RPConPerSession: Integer;
    function  GetBestServerForRealTimeRequest(const AMethod: String): IevRPInPoolInfo;
    function  GetBestServerForLongRequest(const ARequestPriority: Integer): IevRPInPoolInfo;
    function  FindServerBySessionID(const ASessionID: TisGUID): IevRPInPoolInfo;
    function  Count: Integer;
    function  GetPSInfo(const AHost: String): IevRPInPoolInfo;
    function  GetItem(AIndex: Integer): IevRPInPoolInfo;
    function  CopyItems(const ACopyOption: TevItemCopyOption): IisList;
    property  Items[Index: Integer]: IevRPInPoolInfo read GetItem; default;
  end;

  TevRPPool = class(TisCollection, IevRPPool)
  private
    FRPConPerSession: Integer;
  protected
    procedure DoOnConstruction; override;
    procedure ApplyConfiguration;
    function  RPConPerSession: Integer;
    function  GetBestServerForRealTimeRequest(const AMethod: String): IevRPInPoolInfo;
    function  GetBestServerForLongRequest(const ARequestPriority: Integer): IevRPInPoolInfo;
    function  FindServerBySessionID(const ASessionID: TisGUID): IevRPInPoolInfo;
    function  Count: Integer;
    function  GetPSInfo(const AHost: String): IevRPInPoolInfo;
    function  GetItem(AIndex: Integer): IevRPInPoolInfo;
    function  CopyItems(const ACopyOption: TevItemCopyOption): IisList;
  end;


  // Executing requests on RP
  IevRPPendingRequest = interface
  ['{8386B2E7-3EEA-4600-827C-2EBCCADBFB95}']
    function  ID: String;
    function  DatagramHeader: IevDatagramHeader;
    function  SourceSession: IevSession;
    function  DestinationSession: IevSession;
    function  IsLongRequest: Boolean;
    function  GetRequestPriority: Integer;
    procedure SetRequestPriority(const AValue: Integer);
  end;

  IevRPPendingRequestList = interface
  ['{FB81BDA9-F629-4024-844E-5353C83F8F46}']
    procedure Lock;
    procedure Unlock;
    procedure AddRequest(const ARequestID: String; const ADatagramHeader: IevDatagramHeader;
                         const ASourceSession, ADestinationSession: IevSession);
    procedure AddLongRequest(const ARequestID: String; const ADatagram: IevDatagram;
                         const ADestinationSession: IevSession);
    function  FindRequest(const ARequestID: String): IevRPPendingRequest;
    function  FindRequestByContextID(const AContextID: TisGUID): IevRPPendingRequest;
    function  RemoveRequest(const ARequestID: String): IevRPPendingRequest;
    procedure DeleteRequest(const ARequest: IevRPPendingRequest);
    function  GetAllRequests: IisList;
    function  GetRequestsByDestSessionID(const ADestinationSessionID: TisGUID): IisList;
    function  GetRequestsBySrcSessionID(const ASourceSessionID: TisGUID): IisList;
    function  GetLongRequestsBySessionID(const ADestinationSessionID: TisGUID): IisList;
    function  Count: Integer;
  end;

  TevRPPendingRequestList = class(TisCollection, IevRPPendingRequestList)
  private
    procedure DoOnConstruction; override;
    procedure AddRequest(const ARequestID: String; const ADatagramHeader: IevDatagramHeader;
                         const ASourceSession, ADestinationSession: IevSession);
    procedure AddLongRequest(const ARequestID: String; const ADatagram: IevDatagram;
                         const ADestinationSession: IevSession);
    function  FindRequest(const ARequestID: String): IevRPPendingRequest;
    function  FindRequestByContextID(const AContextID: TisGUID): IevRPPendingRequest;
    function  RemoveRequest(const ARequestID: String): IevRPPendingRequest;
    procedure DeleteRequest(const ARequest: IevRPPendingRequest);
    function  GetAllRequests: IisList;
    function  GetRequestsByDestSessionID(const ADestinationSessionID: TisGUID): IisList;
    function  GetRequestsBySrcSessionID(const ASourceSessionID: TisGUID): IisList;
    function  GetLongRequestsBySessionID(const ADestinationSessionID: TisGUID): IisList;
    function  Count: Integer;
  end;

  TevRPPendingRequest = class(TisNamedObject, IevRPPendingRequest)
  private
    FDatagramHeader: IevDatagramHeader;
    FDestinationSession: IevSession;
    FSourceSession: IevSession;
    FLongRequestPriority: Integer;
    function  ID: String;
    function  DatagramHeader: IevDatagramHeader;
    function  SourceSession: IevSession;
    function  DestinationSession: IevSession;
    function  IsLongRequest: Boolean;
    function  GetRequestPriority: Integer;
    procedure SetRequestPriority(const AValue: Integer);

    constructor Create(const ARequestID: String; const ADatagramHeader: IevDatagramHeader;
      const ASourceSession, ADestinationSession: IevSession); reintroduce;
  end;


  // Connections RB -> RP
  IevRPTCPClient = interface
  ['{89AC7B11-8ECA-4D69-90EF-A602059F0FEA}']
    function  Control: IevRBControl;
    procedure ForwardDatagram(const ADatagram: IevDualStream; const ASourceSession: IevSession);
    function  RPPool: IevRPPool;
    function  PendingRequestList: IevRPPendingRequestList;
  end;

  TevRPTCPClient = class(TevCustomTCPClient, IevTCPClient, IevRPTCPClient)
  private
    FRPPool: IevRPPool;
    FPendingRequestList: IevRPPendingRequestList;
    FRPPoolWatchDogTaskID: TTaskID;
    FControl: IevRBControl;
    FController: IevAppController;
    procedure RPPoolWatchDog(const Params: TTaskParamList);
    procedure CheckConnectionsToServer(const Params: TTaskParamList); overload;
    procedure CheckConnectionsToServer(const AServer: IevRPInPoolInfo); overload;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  GetHost: String;
    function  GetPort: String;
    procedure SetHost(const AValue: String);
    function  ConnectedHost: String;
    procedure SetPort(const AValue: String);
    function  CreateDispatcher: IevDatagramDispatcher; override;
    procedure ForwardDatagram(const ADatagram: IevDualStream; const ASourceSession: IevSession);
    function  RPPool: IevRPPool;
    function  PendingRequestList: IevRPPendingRequestList;
    function  Session: IevSession;
    function  Control: IevRBControl;
    procedure Logoff;
    procedure SetCreateConnectionTimeout(const AValue: Integer);
    function  GetArtificialLatency: Integer;
    procedure SetArtificialLatency(const AValue: Integer);
  protected
    procedure  DoOnConstruction; override;
    destructor Destroy; override;
  end;

  TevRPDatagramDispatcher = class(TevRBDatagramDispatcher)
  private
    procedure FailPendingRequests(const Params: TTaskParamList);
    procedure dmContextCallbacksAsyncCallReturn(const ADatagram: IevDatagram; out AResult: IevDatagram); reintroduce;
  protected
    procedure DoOnConstruction; override;
    procedure RegisterHandlers; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
    procedure BeforeSendMethodResult(const ASession: IevSession; const AMethod, AResult: IevDatagram); override;
    procedure OnFatalError(const ASession: IevSession; const AError: String; const AData: IevDualStream); override;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
  end;


  // Control used by Deployment Manager
  TevRBControl = class(TisInterfacedObject, IevRBControl)
  private
    function  MaleConnector: IevRPTCPClient;
    function  FemaleConnector: IevTCPServer;
  protected
    function  GetMachineInfo: IisListOfValues;
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetHardware: IisStringList;
    function  GetModules: IisStringList;
    function  GetDomainList: IisListOfValues;
    function  GetLicenseInfo(const ADomainName: String): IisListOfValues;
    procedure SetLicenseInfo(const ADomainName: String; const ASerialNumber, ARegistrationCode: String);
    function  GetFolders(const ADomainName: String): IisListOfValues;
    procedure SetFolders(const ADomainName: String; const AFolders: IisListOfValues);
    procedure GetEMailInfo(var AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String);
    procedure SetEMailInfo(const AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String);
    procedure SetDatabasePasswords(const AParams: IisListOfValues);
    function  GetDatabasePasswords: IisListOfValues;
    procedure SetDatabaseParams(const ADomainName: String; const AParams: IisListOfValues);
    function  GetDatabaseParams(const ADomainName: String): IisListOfValues;
    procedure GetSystemAccount(const ADomainName: String; out AUser: String);
    procedure SetSystemAccount(const ADomainName, AUser: String);
    procedure GetAdminAccount(const ADomainName: String; out AUser: String);
    procedure SetAdminAccount(const ADomainName, AUser: String);
    function  GetRPPoolInfo: IisParamsCollection;
    procedure SetRPPoolInfo(const AInfo: IisParamsCollection);
    function  GetRPPoolStatus: IisParamsCollection;
    function  GetStackInfo: String;
    function  UsersPoolStatus: IisParamsCollection;
    function  GetAppTempFolder: String;
    procedure SetAppTempFolder(const ATempFolder: String);
    function  GetPrintersInfo: IisListOfValues;
    procedure SetPrintersInfo(const APrintersInfo: IisListOfValues);
    procedure SendMessage(const aText: String; const aToUsers: String = '');
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    function  GetMaintenanceDB: IisListOfValues;
    function  GetSystemStatus: IisParamsCollection;
    function  GetAUS: String;
    procedure SetAUS(const AValue: String);
    function  GetInternalRR: IisStringList;
    procedure SetInternalRR(const ARRList: IisStringList);
    function  GetInternalUserRR(const ADomainName: String): Boolean;
    procedure SetInternalUserRR(const ADomainName: String; const AValue: Boolean);
    function  GetBBDiagnosticCheck: String;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetKnownADRServices(const AServices: IisListOfValues);
    procedure ResetAdminPassword(const ADomainName: String; const APassword: String);
  end;


function GetTCPServer: IevTCPServer;
begin
  Result := TevTCPServer.Create;
end;

function GetRPTCPClient: IevTCPClient;
begin
  Result := TevRPTCPClient.Create;
end;

function RBControl: IevRBControl;
begin
  Result := (Mainboard.TCPClient as IevRPTCPClient).Control;
end;

{ TevTCPServer }

procedure TevTCPServer.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(TCPClient_Port));
  SetIPAddress(mb_AppConfiguration.AsString['Network\RBToClientIPAddr']);

  GetDatagramDispatcher.Transmitter.TCPCommunicator.TransmissionTimeout :=
    mb_AppConfiguration.GetValue('RequestBroker\ClientTransmissionTimeout',
    GetDatagramDispatcher.Transmitter.TCPCommunicator.TransmissionTimeout);
end;

function TevTCPServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevClientDatagramDispatcher.Create;
  Result.SessionTimeout := CLIENT_SESSION_KEEP_ALIVE_TIMEOUT;
end;


{ TevRBDatagramDispatcher }

procedure TevRBDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_GLOBALCALLBACKS_SUBSCRIBE, dmGlobalCallbacksSubscribe);
  AddHandler(METHOD_GLOBALCALLBACKS_UNSUBSCRIBE, dmGlobalCallbacksUnsubscribe);
  AddHandler(METHOD_GLOBALCALLBACKS_SECURITYCHANGE, dmGlobalCallbacksSecurityChange);
  AddHandler(METHOD_GLOBALCALLBACKS_TASKSCHEDULERCHANGE, dmGlobalCallbacksTaskSchedulerChange);

  AddHandler(METHOD_GLOBALFLAGMANAGER_SETFLAG, dmGlobalFlagManagerSetFlag);
  AddHandler(METHOD_GLOBALFLAGMANAGER_TRYLOCK, dmGlobalFlagManagerTryLock);
  AddHandler(METHOD_GLOBALFLAGMANAGER_TRYLOCK2, dmGlobalFlagManagerTryLock2);
  AddHandler(METHOD_GLOBALFLAGMANAGER_TRYUNLOCK, dmGlobalFlagManagerTryUnlock);
  AddHandler(METHOD_GLOBALFLAGMANAGER_TRYUNLOCK2, dmGlobalFlagManagerTryUnlock2);
  AddHandler(METHOD_GLOBALFLAGMANAGER_RELEASEFLAG, dmGlobalFlagManagerReleaseFlag);
  AddHandler(METHOD_GLOBALFLAGMANAGER_FORCEDUNLOCK, dmGlobalFlagManagerForcedUnlock);
  AddHandler(METHOD_GLOBALFLAGMANAGER_FORCEDUNLOCK2, dmGlobalFlagManagerForcedUnlock2);
  AddHandler(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAG, dmGlobalFlagManagerGetLockedFlag);
  AddHandler(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAG2, dmGlobalFlagManagerGetLockedFlag2);
  AddHandler(METHOD_GLOBALFLAGMANAGER_GETLOCKEDFLAGLIST, dmGlobalFlagManagerGetLockedFlagList);
  AddHandler(METHOD_GLOBALFLAGMANAGER_GETSETFLAGLIST, dmGlobalFlagManagerGetSetFlagList);

  AddHandler(METHOD_TIMESOURCE_GETDATETIME, dmTimeSourceGetDateTime);

  AddHandler(METHOD_EMAILER_SENDMAIL, dmEMailerSendMail);
  AddHandler(METHOD_EMAILER_SENDQUICKMAIL, dmEMailerSendQuickMail);

  AddHandler(METHOD_VMR_REMOTE_CHECKSCANNEDBARCODE, dmVMRRemoteCheckScannedBarcode);
  AddHandler(METHOD_VMR_REMOTE_DELETEFILE, dmVMRRemoteDeleteFile);
  AddHandler(METHOD_VMR_REMOTE_FILEDOESEXIST, dmVMRRemoteFileDoesExist);
  AddHandler(METHOD_VMR_REMOTE_FINDINBOXES, dmVMRRemoteFindInBoxes);
  AddHandler(METHOD_VMR_REMOTE_RETRIEVE_PICKUPSHEET_DATA, dmVMRRemoteRetrievePickupSheetsXMLData);
  AddHandler(METHOD_VMR_REMOTE_GETSBPRINTERLIST, dmVMRRemoteGetSBPrinterList);
  AddHandler(METHOD_VMR_REMOTE_GETPRINTERLIST, dmVMRRemoteGetPrinterList);
  AddHandler(METHOD_VMR_REMOTE_SETPRINTERLIST, dmVMRRemoteSetPrinterList);
  AddHandler(METHOD_VMR_REMOTE_PRINTRWRESULTS, dmVMRRemotePrintRwResults);
  AddHandler(METHOD_VMR_REMOTE_PUTINBOXES, dmVMRRemotePutInBoxes);
  AddHandler(METHOD_VMR_REMOTE_REMOVEFROMBOXES, dmVMRRemoteRemoveFromBoxes);
  AddHandler(METHOD_VMR_REMOTE_REMOVESCANNEDBARCODE, dmVMRRemoteRemoveScannedBarcode);
  AddHandler(METHOD_VMR_REMOTE_REMOVESCANNEDBARCODES, dmVMRRemoteRemoveScannedBarcodes);
  AddHandler(METHOD_VMR_REMOTE_RETRIEVEFILE, dmVMRRemoteRetrieveFile);
  AddHandler(METHOD_VMR_REMOTE_STOREFILE, dmVMRRemoteStoreFile);
  AddHandler(METHOD_VMR_REMOTE_PROCESSRELEASE,dmVMRProcessRelease );
  AddHandler(METHOD_VMR_REMOTE_PROCESSREVERT,dmVMRProcessRevert );
  AddHandler(METHOD_VMR_REMOTE_GETVMRREPORTLIST,dmVMRGetVMRReportList);
  AddHandler(METHOD_VMR_REMOTE_GETVMRREPORT,    dmVMRGetVMRReport);
  AddHandler(METHOD_VMR_REMOTE_DELETEDIRECTORY, dmVMRDeleteDirectory);
  AddHandler(METHOD_VMR_REMOTE_PURGEMAILBOX,    dmVMRPurgeMailBox);
  AddHandler(METHOD_VMR_REMOTE_PURGECLIENTMAILBOX, dmVMRPurgeClientMailBox);
  AddHandler(METHOD_VMR_REMOTE_PURGEMAILBOXCONTENTLIST, dmVMRPurgeMailBoxContentList);
  AddHandler(METHOD_VMR_REMOTE_GETREMOTEVMRREPORTLIST,dmVMRGetRemoteVMRReportList);
  AddHandler(METHOD_VMR_REMOTE_GETREMOTEVMRREPORT,dmVMRGetRemoteVMRReport);

  AddHandler(METHOD_LICENSE_GETLICENSEINFO, dmLicenseGetLicenseInfo);

  AddHandler(METHOD_TASK_QUEUE_CREATETASK, dmTaskQueueCreateTask);
  AddHandler(METHOD_TASK_QUEUE_ADDTASK, dmTaskQueueAddTask);
  AddHandler(METHOD_TASK_QUEUE_ADDTASKS, dmTaskQueueAddTasks);
  AddHandler(METHOD_TASK_QUEUE_REMOVETASK, dmTaskQueueRemoveTask);
  AddHandler(METHOD_TASK_QUEUE_GETTASKINFO, dmTaskQueueGetTaskInfo);
  AddHandler(METHOD_TASK_QUEUE_GETTASK, dmTaskQueueGetTask);
  AddHandler(METHOD_TASK_QUEUE_GETTASKREQUESTS, dmTaskQueueGetTaskRequests);
  AddHandler(METHOD_TASK_QUEUE_GETUSERTASKSTATUS, dmTaskQueueGetUserTaskStatus);
  AddHandler(METHOD_TASK_QUEUE_GETUSERTASKINFOLIST, dmTaskQueueGetUserTaskInfoList);
  AddHandler(METHOD_TASK_QUEUE_MODIFYTASKPROPERTY, dmTaskQueueModifyTaskProperty);

  AddHandler(METHOD_SINGLESIGNON_GETSWIPECLOCKONETIMEURL, dmSingleSignOnGetSwipeClockOneTimeURL);
  AddHandler(METHOD_SINGLESIGNON_GETMRCONETIMEURL, dmSingleSignOnGetMRCOneTimeURL);
  AddHandler(METHOD_SINGLESIGNON_GETSWIPECLOCKINFO, dmSingleSignOnGetSwipeClockInfo);

  AddHandler(METHOD_DASHBOARD_DATAPROVIDER_GETCOMPANYREPORTS, dmDashboardDataProviderGetCompanyReports);
end;


{ TevRPTCPClient }

function TevRPTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRPDatagramDispatcher.Create;
end;

procedure TevRPTCPClient.DoOnConstruction;
begin
  inherited;
  Mainboard.LogFile.EMailNotifierActive := True;

  FRPPool := TevRPPool.Create;
  FRPPool.ApplyConfiguration;

  FPendingRequestList := TevRPPendingRequestList.Create;
  FControl := TevRBControl.Create;
  FController := TevRBController.Create;
  FController.Active := True;

  FRPPoolWatchDogTaskID := GlobalThreadManager.RunTask(RPPoolWatchDog, Self, MakeTaskParams([]), 'RP Pool Watchdog');

  CheckLicenses;
end;

function TevRPTCPClient.GetHost: String;
begin
  Result := '';
  Assert(False);
end;

function TevRPTCPClient.GetPort: String;
begin
  Result := '';
  Assert(False);
end;

function TevRPTCPClient.PendingRequestList: IevRPPendingRequestList;
begin
  Result := FPendingRequestList;
end;

function TevRPTCPClient.RPPool: IevRPPool;
begin
  Result := FRPPool;
end;

procedure TevRPTCPClient.ForwardDatagram(const ADatagram: IevDualStream; const ASourceSession: IevSession);
var
  Srv: IevRPInPoolInfo;
  DHs, DHd: IevDatagramHeader;
  SrvSession: IevSession;
  SentBytes: Cardinal;
begin
  DHs := TevDatagram.GetDatagramHeader(ADatagram);
  Srv := RPPool.GetBestServerForRealTimeRequest(DHs.Method);
  CheckCondition(Assigned(Srv), 'Cannot resolve remote method ' + DHs.Method);
  try
    SrvSession := Srv.Session;

    DHd := TevDatagram.GetDatagramHeader(ADatagram);
    DHd.SessionID := Srv.Session.SessionID;
    DHd.SequenceNbr := 0;
    DHd.ContextID := DHs.ContextID;
    TevDatagram.PatchHeader(ADatagram, DHd);   // update datagram header

    FPendingRequestList.AddRequest(IntToStr(DHd.SequenceNbr), DHs, ASourceSession, SrvSession);
    try
      SrvSession.SendData(ADatagram, DHd, SentBytes);
    except
      FPendingRequestList.RemoveRequest(IntToStr(DHd.SequenceNbr));
      raise;
    end
  finally
    Srv.DecNewRequestCount;
  end;
end;


function TevRPTCPClient.SendRequest(const ADatagram: IevDatagram): IevDatagram;
var
  Srv: IevRPInPoolInfo;
  SrvSession: IevSession;

  function DoSend: IevDatagram;
  begin
    try
      if Assigned(Srv) then
        SrvSession := Srv.Session;
      ADatagram.Header.SessionID := SrvSession.SessionID;
      Result := SrvSession.SendRequest(ADatagram);
    finally
      if Assigned(Srv) then
        Srv.DecNewRequestCount;
    end;
  end;

  function SendAsynchronousRequest: IevDatagram;
  var
    CallID: TisGUID;
    R: IevRPPendingRequest;
  begin
    Srv := nil;

    // Make Call
    if AnsiSameText(ADatagram.Header.Method, METHOD_ASYNCFUNCTIONS_MAKECALL) then
    begin
      CallID := ADatagram.Params.Value['ACallID'];
      Srv := RPPool.GetBestServerForLongRequest(ADatagram.Params.Value['APriority']);
      // If RP was not found and it's a task request then "send" FALSE back
      if not Assigned(Srv) then
      begin
        Result := TevDatagram.CreateResponseFor(ADatagram.Header);
        Result.Params.AddValue(METHOD_RESULT, False);
        Result.Params.AddValue('ACallID', CallID);
        // no actual sending needs
      end
      else
      begin
        try
          FPendingRequestList.AddLongRequest(CallID, ADatagram, Srv.Session);
        finally
          Srv.DecNewLongRequestCount;
        end;
          
        try
          Result := DoSend;
          if not Result.Params.Value[METHOD_RESULT] then
            FPendingRequestList.RemoveRequest(CallID);
        except
          FPendingRequestList.RemoveRequest(CallID);
          raise;
        end;
      end;
    end

    // Terminate call
    else if AnsiSameText(ADatagram.Header.Method, METHOD_ASYNCFUNCTIONS_TERMINATECALL) then
    begin
      // Delete pending request on async call termination
      R := FPendingRequestList.RemoveRequest(ADatagram.Params.Value['ACallID']);
      if Assigned(R) then
      begin
        SrvSession := R.DestinationSession;
        Result := DoSend;
      end;
    end

    // Set Priority
    else if AnsiSameText(ADatagram.Header.Method, METHOD_ASYNCFUNCTIONS_SETCALLPRIORITY) then
    begin
      R := FPendingRequestList.FindRequest(ADatagram.Params.Value['ACallID']);
      if Assigned(R) then
      begin
        SrvSession := R.DestinationSession;
        Result := DoSend;
        R.SetRequestPriority(Result.Params.Value[METHOD_RESULT]);
      end;
    end;
  end;

begin
  if StartsWith(ADatagram.Header.Method, METHOD_ASYNCFUNCTIONS) then
    Result := SendAsynchronousRequest

  else
  begin
    Srv := RPPool.GetBestServerForRealTimeRequest(ADatagram.Header.Method);
    CheckCondition(Assigned(Srv), 'Cannot resolve remote method ' + ADatagram.Header.Method);
    Result := DoSend;
  end;
end;

procedure TevRPTCPClient.SetHost(const AValue: String);
begin
  Assert(False);
end;

procedure TevRPTCPClient.SetPort(const AValue: String);
begin
  Assert(False);
end;

function TevRPTCPClient.Session: IevSession;
begin
  Result := nil;
end;

procedure TevRPTCPClient.CheckConnectionsToServer(const Params: TTaskParamList);
var
  Server: IevRPInPoolInfo;
begin
  Server := IInterface(Params[0]) as IevRPInPoolInfo;
  CheckConnectionsToServer(Server);
end;

procedure TevRPTCPClient.RPPoolWatchDog(const Params: TTaskParamList);
var
  iConnected: Integer;

  procedure CheckUnconnected;
  var
    L: IisList;
    i: Integer;
    TskList: array of TTaskID;
  begin
    // Compiler's feature workaround
    // L := nil most likely won't destroy the object
    L := FRPPool.CopyItems(coNotConnectedOnly);
    SetLength(TskList, L.Count);
    for i := 0 to L.Count - 1 do
      if not CurrentThreadTaskTerminated then
        TskList[i] := GlobalThreadManager.RunTask(CheckConnectionsToServer, Self, MakeTaskParams([L[i]]), 'RP Pool Watchdog (create connection)');

    for i := Low(TskList) to High(TskList) do
      GlobalThreadManager.WaitForTaskEnd(TskList[i]);
    SetLength(TskList, 0);
  end;

  function CheckConnected: Integer;
  var
    L: IisList;
    i: Integer;
  begin
    // Compiler's feature workaround
    // L := nil most likely won't destroy the object
    L := FRPPool.CopyItems(coConnectedOnly);
    for i := 0 to L.Count - 1 do
      if not CurrentThreadTaskTerminated then
        CheckConnectionsToServer(L[i] as IevRPInPoolInfo);
    Result := L.Count;
  end;

begin
  while not CurrentThreadTaskTerminated do
    if CheckConnected <> FRPPool.Count then
    begin
      while True do
      begin
        CheckUnconnected;
        iConnected := CheckConnected;

        if FRPPool.Count - iConnected <= iConnected then // at least half of registered RPs should be connected
          break;

        Snooze(1000);
      end;
    end
    else
      Snooze(30000);  // check RP connections every 30 seconds
end;

destructor TevRPTCPClient.Destroy;
begin
  if GlobalThreadManagerIsActive then
  begin
    GlobalThreadManager.TerminateTask(FRPPoolWatchDogTaskID);
    GlobalThreadManager.WaitForTaskEnd(FRPPoolWatchDogTaskID);
  end;

  FController := nil;
  FControl := nil;

  inherited;
end;

procedure TevRPTCPClient.CheckConnectionsToServer(const AServer: IevRPInPoolInfo);
var
  i, iServerConCount: Integer;
  SessionID: TisGUID;
begin
  try
    // Create session and first connection
    if not Assigned(AServer.Session) or (AServer.Session.GetConnectionCount = 0) then
    begin
      AServer.Session := nil;
      SessionID := CreateConnection(AServer.Host, IntToStr(TCPServer_Port),
          EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '', '', '',
          mb_AppConfiguration.AsString['Network\RBToRPIPAddr']);

      try
        AServer.Session := FindSession(SessionID);
      except
        DestroyConnection(SessionID);
        raise;
      end;
    end
    else
      SessionID := AServer.Session.SessionID;

    // Synchronize bundle of physical connections associated with this session
    iServerConCount := AServer.Session.GetConnectionCount;
    if iServerConCount <> FRPPool.RPConPerSession then
    begin
      for i := FRPPool.RPConPerSession to iServerConCount - 1 do   // delete unnecessary connections
      begin
        DestroyConnection(SessionID);
        Dec(iServerConCount);
      end;

      for i := iServerConCount to FRPPool.RPConPerSession - 1 do   // Add more connections
        CreateConnection(AServer.Host, IntToStr(TCPServer_Port),
          EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '', SessionID, '',
          mb_AppConfiguration.AsString['Network\RBToRPIPAddr']);

      if iServerConCount = 0 then
        mb_TaskQueue.Pulse; // new slots are available. Wake up queue!
    end;

  except
    on E: Exception do
      Mainboard.LogFile.AddContextEvent(etError, LOG_EVENT_CLASS_COMMUNICATION,
        'RP connection problem' + #13 + E.Message, GetErrorDetailsStack(E));
  end;
end;

function TevRPTCPClient.Control: IevRBControl;
begin
  Result := FControl;
end;

procedure TevRPTCPClient.Logoff;
begin
// do nothing
end;

function TevRPTCPClient.ConnectedHost: String;
begin
  Result := GetHost;
end;

procedure TevRPTCPClient.SetCreateConnectionTimeout(const AValue: Integer);
begin
// do nothing
end;

function TevRPTCPClient.GetArtificialLatency: Integer;
begin
  Result := 0;
end;

procedure TevRPTCPClient.SetArtificialLatency(const AValue: Integer);
begin
// nothing
end;

{ TevRPInPoolInfo }

constructor TevRPInPoolInfo.Create(const ARPHost: String);
begin
  inherited Create;
  FLock := TisLock.Create;  
  SetName(ARPHost);
  FEnabled := mb_AppConfiguration.AsBoolean['RequestBroker\RequestProcessor\' + Host];
end;

procedure TevRPInPoolInfo.DecNewRequestCount;
begin
  FLock.Lock;
  try
    if FNewRequestCount > 0 then
      Dec(FNewRequestCount);
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.RequestCount: Integer;
begin
  FLock.Lock;
  try
    Result := FRequestCount;
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.GetSession: IevSession;
begin
  FLock.Lock;
  try
    Result := FSession;
  finally
    FLock.Unlock;
  end;
end;

procedure TevRPInPoolInfo.IncNewRequestCount;
begin
  FLock.Lock;
  try
    Inc(FNewRequestCount);
  finally
    FLock.Unlock;
  end;
end;

procedure TevRPInPoolInfo.SetSession(const ASession: IevSession);
var
  sHost: String;

  procedure GetServerInfo;
  var
    D: IevDatagram;
  begin
    D := TevDatagram.Create(METHOD_RP_GETCAPABILITIES);
    D.Header.SessionID := ASession.SessionID;
    D := ASession.SendRequest(D);

    FMaxScore := mb_AppConfiguration.GetValue('RequestBroker\RequestProcessor\MaxScore\' + sHost,
                                              D.Params.Value['MaxScore']);

    FMaxLongRequests := mb_AppConfiguration.GetValue('RequestBroker\RequestProcessor\MaxLongRequests\' + sHost,
                                              D.Params.Value['MaxLongRequests']);

    D := TevDatagram.Create(METHOD_ASYNCFUNCTIONS_SETCALCCAPACITY);
    D.Header.SessionID := ASession.SessionID;
    D.Params.AddValue('AValue', FMaxLongRequests);
    D := ASession.SendRequest(D);

    UpdateCurrentState;
  end;

begin
  sHost := Host; // to avoid critical section deadlock

  FLock.Lock;
  try
    if (FSession <> ASession) and Assigned(ASession) then
      GetServerInfo;

    FSession := ASession;
  finally
    FLock.Unlock;
  end;
end;


function TevRPInPoolInfo.GetEnabled: Boolean;
begin
  FLock.Lock;
  try
    Result := FEnabled;
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.Host: String;
begin
  Result := GetName;
end;

procedure TevRPInPoolInfo.SetEnabled(const AValue: Boolean);
begin
  FLock.Lock;
  try
    FEnabled := AValue;
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.MaxScore: Integer;
begin
  Result := FMaxScore;
end;

function TevRPInPoolInfo.CurrentScore: Integer;
begin
  FLock.Lock;
  try
    Result := FCurrentScore;
  finally
    FLock.Unlock;
  end;
end;

procedure TevRPInPoolInfo.UpdateCurrentState;
var
  iPendingReq: Integer;
  L: IisList;
  i: Integer;
begin
  FLock.Lock;
  try
    if Assigned(FSession) then
    begin
      L := (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.GetRequestsByDestSessionID(FSession.SessionID);
      iPendingReq := L.Count;

      FLongRequests := 0;
      for i := 0 to L.Count - 1 do
        if (L[i] as IevRPPendingRequest).IsLongRequest then
          Inc(FLongRequests);
      FLongRequests := FLongRequests + GetNewLongRequestCount;
    end
    else
      iPendingReq := 0;

    FRequestCount := iPendingReq + GetNewRequestCount;
    FCurrentScore := FMaxScore div (FRequestCount + 1);
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.Connected: Boolean;
begin
  FLock.Lock;
  try
    Result := Assigned(FSession) and (FSession.GetConnectionCount = (Mainboard.TCPClient as IevRPTCPClient).RPPool.RPConPerSession);
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.LongRequests: Integer;
begin
  FLock.Lock;
  try
    Result := FLongRequests;
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.MaxLongRequests: Integer;
begin
  Result := FMaxLongRequests;
end;

procedure TevRPInPoolInfo.DecNewLongRequestCount;
begin
  FLock.Lock;
  try
    if FNewLongRequestCount > 0 then
      Dec(FNewLongRequestCount);
  finally
    FLock.Unlock;
  end;
end;

procedure TevRPInPoolInfo.IncNewLongRequestCount;
begin
  FLock.Lock;
  try
    Inc(FNewLongRequestCount);
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.GetNewRequestCount: Integer;
begin
  FLock.Lock;
  try
    Result := FNewRequestCount;
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.GetNewLongRequestCount: Integer;
begin
  FLock.Lock;
  try
    Result := FNewLongRequestCount;
  finally
    FLock.Unlock;
  end;
end;

function TevRPInPoolInfo.NoSlotsForLongRequests: Boolean;
begin
  FLock.Lock;
  try
    Result := FLongRequests >= FMaxLongRequests;
  finally
    FLock.Unlock;
  end;
end;


{ TevRPPool }

procedure TevRPPool.ApplyConfiguration;
var
  i: Integer;
  RP: IisStringListRO;
  RPInfo: IevRPInPoolInfo;
begin
  Lock;
  try
    FRPConPerSession := mb_AppConfiguration.AsInteger['RequestBroker\RPConPerSession'];
    if FRPConPerSession = 0 then
      FRPConPerSession := 5;

    RP := mb_AppConfiguration.GetValueNames('RequestBroker\RequestProcessor');

    // Delete removed servers
    for i := Count - 1 downto 0 do
      if RP.IndexOf(GetItem(i).Host) = -1 then
        RemoveChild(GetChild(i));

    // Add missing servers
    for i := 0 to RP.Count - 1 do
    begin
      if FindChildByName(RP[i]) = nil then
      begin
        RPInfo := TevRPInPoolInfo.Create(RP[i]);
        AddChild(RPInfo as IisInterfacedObject);
      end;
    end;

  finally
    Unlock;
  end;
end;

function TevRPPool.Count: Integer;
begin
  Result := ChildCount;
end;

function TevRPPool.FindServerBySessionID(const ASessionID: TisGUID): IevRPInPoolInfo;
var
  i: Integer;
  PSInfo: IevRPInPoolInfo;
begin
  Lock;
  try
    Result := nil;
    for i := 0 to Count - 1 do
    begin
      PSInfo := GetItem(i);
      if Assigned(PSInfo.Session) and AnsiSameText(PSInfo.Session.SessionID, ASessionID) then
      begin
        Result := PSInfo;
        break;
      end
    end;
  finally
    Unlock;
  end;
end;

function TevRPPool.GetBestServerForRealTimeRequest(const AMethod: String): IevRPInPoolInfo;
var
  Lsrv: IisList;
  i: Integer;

  function CompareServersForRealTimeRequests(const Item1, Item2: IInterface): Integer;
  var
    Srv1, Srv2: IevRPInPoolInfo;
  begin
    Srv1 := Item1 as IevRPInPoolInfo;
    Srv2 := Item2 as IevRPInPoolInfo;

    // Desc
    if Srv1.Enabled > Srv2.Enabled then
      Result := -1
    else if Srv1.Enabled < Srv2.Enabled then
      Result := 1

    // Desc
    else if Srv1.CurrentScore > Srv2.CurrentScore then
      Result := -1
    else if Srv1.CurrentScore < Srv2.CurrentScore then
      Result := 1

    // Desc
    else if Srv1.MaxScore > Srv2.MaxScore then
      Result := -1
    else if Srv1.MaxScore < Srv2.MaxScore then
      Result := 1

    else
      Result := 0;
  end;

begin
  for i := 1 to 120 do
  begin
    Lsrv := CopyItems(coConnectedOnly);
    if Lsrv.Count > 0 then
      break
    else
      Snooze(1000);
    AbortIfCurrentThreadTaskTerminated;  
  end;

  for i := 0 to Lsrv.Count - 1 do
    TevRPInPoolInfo((Lsrv[i] as IisInterfacedObject).GetImplementation).UpdateCurrentState;

  Result := nil;

  Lock;
  try
    Lsrv.SetCompareItemProc(@CompareServersForRealTimeRequests);
    Lsrv.Sort;
    if Lsrv.Count > 0 then
    begin
      Result := Lsrv[0] as IevRPInPoolInfo;
      Result.IncNewRequestCount;
    end;
  finally
    Unlock;
  end;
end;

function TevRPPool.GetItem(AIndex: Integer): IevRPInPoolInfo;
begin
  Result := GetChild(AIndex) as IevRPInPoolInfo;
end;

function TevRPPool.GetPSInfo(const AHost: String): IevRPInPoolInfo;
begin
  Result := FindChildByName(AHost) as IevRPInPoolInfo;
end;

function TevRPPool.CopyItems(const ACopyOption: TevItemCopyOption): IisList;
var
  i: Integer;
  Item: IevRPInPoolInfo;
begin
  Result := TisList.Create;
  Result.Capacity := Count;

  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      Item := GetItem(i);
      case ACopyOption of
      coAll:
        Result.Add(Item);

      coNotConnectedOnly:
        if Item.Enabled and not Item.Connected then
          Result.Add(GetItem(i));

      coConnectedOnly:
        if Item.Enabled and Item.Connected then
          Result.Add(GetItem(i));
      end;
    end;

  finally
    Unlock;
  end;
end;

function TevRPPool.RPConPerSession: Integer;
begin
  Result := FRPConPerSession;
end;


function TevRPPool.GetBestServerForLongRequest(const ARequestPriority: Integer): IevRPInPoolInfo;
var
  Lsrv: IisList;
  Lreq: IisList;
  Srv: IevRPInPoolInfo;
  i: Integer;

  function CompareRequestsByPriorities(const Item1, Item2: IInterface): Integer;
  var
    Req1, Req2: IevRPPendingRequest;
  begin
    Req1 := Item1 as IevRPPendingRequest;
    Req2 := Item2 as IevRPPendingRequest;

    if Req1.GetRequestPriority > Req2.GetRequestPriority then
      Result := 1
    else if Req1.GetRequestPriority  < Req2.GetRequestPriority then
      Result := -1
    else
      Result := 0;
  end;


  function CompareServersForLongRequests(const Item1, Item2: IInterface): Integer;
  var
    Srv1, Srv2: IevRPInPoolInfo;
  begin
    Srv1 := Item1 as IevRPInPoolInfo;
    Srv2 := Item2 as IevRPInPoolInfo;

    // Active Servers first  (Desc)
    if Srv1.Enabled > Srv2.Enabled then
      Result := -1
    else if Srv1.Enabled < Srv2.Enabled then
      Result := 1

    // Servers with greater free slots first (Asc)
    else if Srv1.NoSlotsForLongRequests > Srv2.NoSlotsForLongRequests then
      Result := 1
    else if Srv1.NoSlotsForLongRequests < Srv2.NoSlotsForLongRequests then
      Result := -1

    // Servers with fewer long requests first (Asc)
    else if Srv1.LongRequests > Srv2.LongRequests then
      Result := 1
    else if Srv1.LongRequests < Srv2.LongRequests then
      Result := -1

    // Servers with highest current score first (Desc)
    else if Srv1.CurrentScore > Srv2.CurrentScore then
      Result := -1
    else if Srv1.CurrentScore < Srv2.CurrentScore then
      Result := 1

    // Servers with highest Max score first (Desc)
    else if Srv1.MaxScore > Srv2.MaxScore then
      Result := -1
    else if Srv1.MaxScore < Srv2.MaxScore then
      Result := 1

    else
      Result := 0;
  end;

begin
  while True do
  begin
    Lsrv := CopyItems(coConnectedOnly);
    if Lsrv.Count > 0 then
      break
    else
      Sleep(1000);
    AbortIfCurrentThreadTaskTerminated;
  end;

  for i := 0 to Lsrv.Count - 1 do
    TevRPInPoolInfo((Lsrv[i] as IisInterfacedObject).GetImplementation).UpdateCurrentState;

  Result := nil;
  Lock;
  try
    Lsrv.SetCompareItemProc(@CompareServersForLongRequests);
    Lsrv.Sort;

    if Lsrv.Count > 0 then
    begin
      for i := 0 to Lsrv.Count - 1 do
      begin
        Srv := Lsrv[i] as IevRPInPoolInfo;
        if Srv.LongRequests < Srv.MaxLongRequests then
          Result := Srv

        else
        begin
          // Lookup on this server long requests priorities
          Lreq := (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.GetLongRequestsBySessionID(Srv.Session.SessionID);
          if Lreq.Count >= Srv.MaxLongRequests then
          begin
            if Lreq.Count <= Srv.MaxLongRequests * 4 then  // prevent system to overload
            begin
              Lreq.SetCompareItemProc(@CompareRequestsByPriorities);
              Lreq.Sort;
              if (Lreq[Srv.MaxLongRequests - 1] as IevRPPendingRequest).GetRequestPriority > ARequestPriority then
                Result := Srv;
            end;
          end
          else
            Result := Srv;
        end;

        if Assigned(Result) then
          break;
      end;
    end

    else
      Result := nil;

    if Assigned(Result) then
    begin
      Result.IncNewRequestCount;
      Result.IncNewLongRequestCount;
    end;
  finally
    Unlock;
  end;
end;

procedure TevRPPool.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

{ TevRPDatagramDispatcher }

procedure TevRPDatagramDispatcher.BeforeDispatchIncomingData(const ASession: IevSession;
  const ADatagramHeader: IevDatagramHeader; const AStream: IEvDualStream; var Handled: Boolean);
var
  R: IevRPPendingRequest;
  s: String;
  SentBytes: Cardinal;

  procedure CloseClientSession;
  begin
    try
      if Assigned(R.SourceSession) then
        Mainboard.TCPClient.DatagramDispatcher.DeleteSession(R.SourceSession);  // something is wrong! close client session
    except
    end;
  end;

begin
  // If it's client request result then NO exceptions should rise here,
  // otherwise RP session will be considered as broken and will be closed

  if ADatagramHeader.IsResponse then
  begin
    // Is it remote synchronous request result?
    R := (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.RemoveRequest(IntToStr(ADatagramHeader.SequenceNbr));
    if Assigned(R) then
    begin
      Handled := True;  // Stop dispatching
      try
        R.DatagramHeader.Method := ADatagramHeader.Method;
        TevDatagram.PatchHeader(AStream, R.DatagramHeader);
        R.SourceSession.SendData(AStream, R.DatagramHeader, SentBytes); // Forward result to calling side
      except
        on E: Exception do
        begin
          Mainboard.LogFile.AddContextEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, 'Cannot return client request result', E.Message);
          CloseClientSession;
        end;
      end;
    end;
  end

  else if StartsWith(ADatagramHeader.Method, METHOD_CONTEXT_CALLBACKS) then
  begin
    // Is it context callback?
    R := (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.FindRequest(IntToStr(ADatagramHeader.SequenceNbr));
    if Assigned(R) then
    begin
      Handled := True;  // Stop dispatching
      try
        s := ADatagramHeader.Method;
        ADatagramHeader.Assign(R.DatagramHeader);
        ADatagramHeader.Method := s;
        TevDatagram.PatchHeader(AStream, ADatagramHeader);
        R.SourceSession.SendData(AStream, ADatagramHeader, SentBytes); // Forward data to calling side
      except
        on E: Exception do
        begin
          Mainboard.LogFile.AddContextEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, 'Cannot perform callback to client side', E.Message);
          CloseClientSession;
        end;
      end;
    end;
  end;
end;

procedure TevRPDatagramDispatcher.BeforeSendMethodResult(const ASession: IevSession; const AMethod, AResult: IevDatagram);
const Err = 'Asynchronous call returned invalid datagram. ';
begin
  inherited;
  if not Assigned(AMethod) then
    raise EevException.Create(Err + 'AMethod param is nil');
  if not Assigned(AResult) then
    raise EevException.Create(Err + 'AResult param is nil');
  if AResult.IsError and AnsiSameText(AMethod.Header.Method, METHOD_CONTEXT_CALLBACKS_ASYNCCALLRETURN) then
    raise AResult.GetException;
end;

procedure TevRPDatagramDispatcher.dmContextCallbacksAsyncCallReturn(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  // Remove pending local asynchronous call and continue dispatching
  (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.RemoveRequest(ADatagram.Params.Value['ACallID']);
  inherited dmContextCallbacksAsyncCallReturn(ADatagram, AResult);
end;

procedure TevRPDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  SetThreadPoolCapacity(100);
end;

procedure TevRPDatagramDispatcher.DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState);
var
  ErrorMessage: String;
begin
  inherited;
  if (APreviousState = ssActive) and (ASession.State = ssInvalid) then
  begin
    ErrorMessage := ASession.ErrorMessage;
    if ErrorMessage = '' then
      ErrorMessage := 'RP connection lost';
    // If session was disconnected all pending requests should "fail"
    // Doing it in thread because current call in CS and it should be short, and avoid deadlocks too
    GlobalThreadManager.RunTask(FailPendingRequests, Self,
      MakeTaskParams([ASession.SessionID, ErrorMessage]), 'Fail pending requests');
  end;
end;

procedure TevRPDatagramDispatcher.FailPendingRequests(const Params: TTaskParamList);
var
  SessionID: TisGUID;
  sErrorMessage: String;
  L: IisList;
  R: IevRPPendingRequest;
  i: Integer;
  D: IevDatagram;
  AsyncCallResults: IisListOfValues;
  Ctx: IevContext;
begin
  SessionID := Params[0];
  sErrorMessage := Params[1];

  L := (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.GetRequestsByDestSessionID(SessionID);
  for i := 0 to L.Count - 1 do
  begin
    R := L[i] as IevRPPendingRequest;
    (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList.RemoveRequest(R.ID);

    // all requests should be taken care of, so no single exceptions are allowed
    try
      if Assigned(R.SourceSession) then
      begin
        // Remote synchronous calls
        try
          raise EevTransport.Create(sErrorMessage);
        except
          on E: Exception do
            D := CreateErrorResponseFor(R.DatagramHeader, E);
        end;
        if R.SourceSession.State = ssActive then
          R.SourceSession.SendDatagram(D);
        D := nil;
      end

      else
      begin
        // Local asynchronous calls
        Ctx := Mainboard.ContextManager.FindContextByID(R.DatagramHeader.ContextID);
        if Assigned(Ctx) and Assigned(Ctx.Callbacks) then
        begin
          AsyncCallResults := TisListOfValues.Create;
          AsyncCallResults.AddValue(PARAM_ERROR, sErrorMessage);
          Ctx.Callbacks.AsyncCallReturn(R.ID, AsyncCallResults);
          AsyncCallResults := nil;
        end;
      end;
    except
      on E: Exception do
        Mainboard.LogFile.AddContextEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, 'Cannot respond on client request', E.Message);
    end;
  end;
end;

procedure TevRPDatagramDispatcher.OnFatalError(const ASession: IevSession; const AError: String; const AData: IevDualStream);
begin
  inherited;
  Mainboard.LogFile.AddContextEvent(etError, LOG_EVENT_CLASS_COMMUNICATION, 'RP sent incorrect datagram'#13 + AError, '');
end;

procedure TevRPDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_GLOBALSETTINGS_GET, dmGlobalSettingsGet);

  AddHandler(METHOD_GLOBALCALLBACKS_TASKQUEUEEVENT, dmGlobalCallbacksTaskQueueEvent);
  AddHandler(METHOD_GLOBALCALLBACKS_GLOBALFLAGCHANGE, dmGlobalCallbacksGlobalFlagChange);
  AddHandler(METHOD_GLOBALCALLBACKS_SETDBMAINTENANCE, dmGlobalCallbacksSetDBMaintenance);

  AddHandler(METHOD_CONTEXT_CALLBACKS_UPDATEWAIT, dmContextCallbacksUpdateWait);
  AddHandler(METHOD_CONTEXT_CALLBACKS_ASYNCCALLRETURN, dmContextCallbacksAsyncCallReturn);
end;

{ TevRPPendingRequestList }

procedure TevRPPendingRequestList.AddLongRequest(const ARequestID: String;
  const ADatagram: IevDatagram; const ADestinationSession: IevSession);
var
  R: IisInterfacedObject;
begin
  R := TevRPPendingRequest.Create(ARequestID, ADatagram.Header, nil, ADestinationSession);
  (R as IevRPPendingRequest).SetRequestPriority(ADatagram.Params.Value['APriority']);
  AddChild(R);
end;

procedure TevRPPendingRequestList.AddRequest(const ARequestID: String; const ADatagramHeader: IevDatagramHeader;
  const ASourceSession, ADestinationSession: IevSession);
var
  R: IisInterfacedObject;
begin
  R := TevRPPendingRequest.Create(ARequestID, ADatagramHeader, ASourceSession, ADestinationSession);
  AddChild(R);
end;

function TevRPPendingRequestList.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TevRPPendingRequestList.DeleteRequest( const ARequest: IevRPPendingRequest);
begin
  (ARequest as IisInterfacedObject).Owner := nil;
end;

procedure TevRPPendingRequestList.DoOnConstruction;
begin
  inherited;
  InitLock;
end;

function TevRPPendingRequestList.FindRequest(const ARequestID: String): IevRPPendingRequest;
begin
  Result := FindChildByName(ARequestID) as IevRPPendingRequest;
end;

function TevRPPendingRequestList.FindRequestByContextID(const AContextID: TisGUID): IevRPPendingRequest;
var
  i: Integer;
  R: IevRPPendingRequest;
begin
  // TODO Sorted list
  Lock;
  try
    Result := nil;
    for i := 0 to Count - 1 do
    begin
      R := GetChild(i) as IevRPPendingRequest;
      if AnsiSameText(R.DatagramHeader.ContextID, AContextID) then
      begin
        Result := R;
        break;
      end;
    end;
  finally
    Unlock;
  end;
end;

function TevRPPendingRequestList.GetAllRequests: IisList;
var
  i: Integer;
  R: IevRPPendingRequest;
begin
  Result := TisList.Create;

  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      R := GetChild(i) as IevRPPendingRequest;
      Result.Add(R);
    end;
  finally
    Unlock;
  end;
end;

function TevRPPendingRequestList.GetLongRequestsBySessionID(const ADestinationSessionID: TisGUID): IisList;
var
  i: Integer;
  R: IevRPPendingRequest;
begin
  Result := TisList.Create;

  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      R := GetChild(i) as IevRPPendingRequest;
      if R.IsLongRequest and AnsiSameText(R.DestinationSession.SessionID, ADestinationSessionID) then
        Result.Add(R);
    end;
  finally
    Unlock;
  end;
end;

function TevRPPendingRequestList.GetRequestsByDestSessionID(const ADestinationSessionID: TisGUID): IisList;
var
  i: Integer;
  R: IevRPPendingRequest;
begin
  Result := TisList.Create;

  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      R := GetChild(i) as IevRPPendingRequest;
      if AnsiSameText(R.DestinationSession.SessionID, ADestinationSessionID) then
        Result.Add(R);
    end;
  finally
    Unlock;
  end;
end;

function TevRPPendingRequestList.GetRequestsBySrcSessionID(const ASourceSessionID: TisGUID): IisList;
var
  i: Integer;
  R: IevRPPendingRequest;
begin
  Result := TisList.Create;

  Lock;
  try
    for i := 0 to Count - 1 do
    begin
      R := GetChild(i) as IevRPPendingRequest;
      if Assigned(R.SourceSession)and AnsiSameText(R.SourceSession.SessionID, ASourceSessionID) then
        Result.Add(R);
    end;
  finally
    Unlock;
  end;
end;

function TevRPPendingRequestList.RemoveRequest(const ARequestID: String): IevRPPendingRequest;
begin
  Result := FindRequest(ARequestID);
  if Assigned(Result) then
    RemoveChild(Result as IisInterfacedObject);
end;

{ TevRPPendingRequest }

constructor TevRPPendingRequest.Create(const ARequestID: String; const ADatagramHeader: IevDatagramHeader;
  const ASourceSession, ADestinationSession: IevSession);
begin
  inherited Create;
  SetName(ARequestID);
  FDatagramHeader := ADatagramHeader;
  FSourceSession := ASourceSession;
  FDestinationSession := ADestinationSession;
end;

function TevRPPendingRequest.DatagramHeader: IevDatagramHeader;
begin
  Result := FDatagramHeader;
end;

function TevRPPendingRequest.DestinationSession: IevSession;
begin
  Result := FDestinationSession;
end;

function TevRPPendingRequest.GetRequestPriority: Integer;
begin
  Result := InterlockedExchange(FLongRequestPriority, FLongRequestPriority);
end;

function TevRPPendingRequest.ID: String;
begin
  Result := GetName;
end;

function TevRPPendingRequest.IsLongRequest: Boolean;
begin
  Result := (GetRequestPriority > 0) and not Assigned(FSourceSession);
end;

procedure TevRPPendingRequest.SetRequestPriority(const AValue: Integer);
begin
  InterlockedExchange(FLongRequestPriority, AValue);
end;

function TevRPPendingRequest.SourceSession: IevSession;
begin
  Result := FSourceSession;
end;

{ TevRBControl }

function TevRBControl.GetHardware: IisStringList;
var
  Info: TIsStringGridHwInfo;
  List: TStringList;
  i: Integer;
begin
  Result := TisStringList.Create;
  Info := TIsStringGridHwInfo.Create(nil);
  try
    List := TStringList.Create;
    try
      Info.Initialize;
      Info.FillStringList(List);
      for i := 0 to List.Count - 1 do
        Result.Add(List[i]);
    finally
      List.Free;
    end;
  finally
    Info.Free;
  end;
end;

function TevRBControl.GetModules: IisStringList;
var
  ModuleList: IInterfaceList;
  i: Integer;
begin
  Result := TisStringList.Create;

  ModuleList := Mainboard.ModuleRegister.GetModules;

  for i := 0 to ModuleList.Count - 1 do
    Result.Add((ModuleList[i] as IevModuleDescriptor).ModuleName);

end;

function TevRBControl.GetFolders(const ADomainName: String): IisListOfValues;
var
  DomainInfo: IevDomainInfo;
begin
  DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if Assigned(DomainInfo) then
  begin
    Result := TisListOfValues.Create;
    Result.AddValue('TaskQueue', DomainInfo.FileFolders.TaskQueueFolder);
    Result.AddValue('VMR', DomainInfo.FileFolders.VMRFolder);
    Result.AddValue('ACH', DomainInfo.FileFolders.ACHFolder);
    Result.AddValue('ASCII', DomainInfo.FileFolders.ASCIIPrefix);
  end
  else
    raise EevException.CreateFmt('Domain name %s not found',[ADomainName]);
end;

procedure TevRBControl.SetFolders(const ADomainName: String; const AFolders: IisListOfValues);
var
  DomainInfo: IevDomainInfo;
begin
  DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if Assigned(DomainInfo) then
  begin
    DomainInfo.FileFolders.TaskQueueFolder := AFolders.Value['TaskQueue'];
    DomainInfo.FileFolders.VMRFolder := AFolders.Value['VMR'];
    DomainInfo.FileFolders.ACHFolder := AFolders.Value['ACH'];
    DomainInfo.FileFolders.ASCIIPrefix := AFolders.Value['ASCII'];
    mb_GlobalSettings.Save;
  end
  else
    raise EevException.CreateFmt('Domain name %s not found',[ADomainName]);
end;

function TevRBControl.GetLicenseInfo(const ADomainName: String): IisListOfValues;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
      Result := TisListOfValues.Create;
      Result.AddValue('RegistrationCode', ctx_DomainInfo.LicenseInfo.Code);
      Result.AddValue('SerialNumber', ctx_DomainInfo.LicenseInfo.SerialNumber);
      Result.AddValue('LastChange', ctx_DomainInfo.LicenseInfo.LastChange);
      Result.AddValue('LicenseDetails', Context.License.GetLicenseInfo);
   except
      // in case if license is invalid or expired
      on E: Exception do
        Result.AddValue('Error', E.Message);
   end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRBControl.SetLicenseInfo(const ADomainName,
  ASerialNumber, ARegistrationCode: String);
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    ctx_DomainInfo.LicenseInfo.SerialNumber := ASerialNumber;
    ctx_DomainInfo.LicenseInfo.Code := ARegistrationCode;
    mb_GlobalSettings.Save;
    Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('License');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRBControl.GetEMailInfo(var AHost, APort, AUser,
  APassword, ASentFrom, ASendAlertsTo: String);
begin
 AHost := mb_GlobalSettings.EMailInfo.SMTPServer;
 APort := mb_GlobalSettings.EMailInfo.SMTPPort;
 AUser := mb_GlobalSettings.EMailInfo.UserName;
 APassword := mb_GlobalSettings.EMailInfo.Password;
 ASentFrom := mb_GlobalSettings.EMailInfo.NotificationDomainName;
 ASendAlertsTo := Mainboard.AppConfiguration.AsString['EMail\SendAlertsTo'];
end;

procedure TevRBControl.SetEMailInfo(const AHost, APort, AUser,
  APassword, ASentFrom, ASendAlertsTo: String);
begin
  mb_GlobalSettings.EMailInfo.SMTPServer := AHost;
  mb_GlobalSettings.EMailInfo.SMTPPort := APort;
  mb_GlobalSettings.EMailInfo.UserName := AUser;
  mb_GlobalSettings.EMailInfo.Password := APassword;
  mb_GlobalSettings.EMailInfo.NotificationDomainName := ASentFrom;
  Mainboard.AppConfiguration.AsString['EMail\SendAlertsTo'] := ASendAlertsTo;
  mb_GlobalSettings.Save;
  Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('EMail');
end;


function TevRBControl.GetDatabaseParams(const ADomainName: String): IisListOfValues;
var
  DomainInfo: IevDomainInfo;
  Params: IisListOfValues;

  procedure AddDBLocations(const ADBLocations: IevDBLocationList; const AName: String);
  var
    i: Integer;
    Locations: IisParamsCollection;
  begin
    Locations := TisParamsCollection.Create;
    Result.AddValue(AName, Locations);

    ADBLocations.Lock;
    try
      for i := 0 to ADBLocations.Count - 1 do
      begin
        Params := Locations.AddParams(IntToStr(i));
        Params.AddValue('DBType',ADBLocations.Items[i].DBType);
        Params.AddValue('Path', ADBLocations.Items[i].Path);
        Params.AddValue('BeginRangeNbr', ADBLocations.Items[i].BeginRangeNbr);
        Params.AddValue('EndRangeNbr', ADBLocations.Items[i].EndRangeNbr);
      end;
    finally
      ADBLocations.Unlock;
    end;
  end;

begin
  DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if Assigned(DomainInfo) then
  begin
    Result := TisListOfValues.Create;

    AddDBLocations(DomainInfo.DBLocationList, 'DBLocation');
    AddDBLocations(DomainInfo.ADRDBLocationList, 'ADRDBLocation');
    Result.AddValue('DBChangesBroadcast', DomainInfo.DBChangesBroadcast);
  end
  else
    raise EevException.CreateFmt('Domain name %s not found',[ADomainName]);
end;

procedure TevRBControl.SetDatabaseParams(const ADomainName: String; const AParams: IisListOfValues);
var
  DbLocation: IevDbLocation;

  procedure SetDBLocations(const ADBLocations: IevDBLocationList; const AName: String);
  var
    i: Integer;
    Locations: IisParamsCollection;
  begin
    if not AParams.ValueExists(AName) then
      Exit;

    Locations := IInterface(AParams.Value[AName]) as IisParamsCollection;

    ADBLocations.Lock;
    try
      ADBLocations.Clear;
      for i := 0 to Locations.Count - 1 do
      begin
        DbLocation := ADBLocations.Add;
        DbLocation.DBType := TevDBType(Locations[i].Value['DBType']);
        DbLocation.Path := Locations[i].Value['Path'];
        DbLocation.BeginRangeNbr := Locations[i].Value['BeginRangeNbr'];
        DbLocation.EndRangeNbr := Locations[i].Value['EndRangeNbr'];
      end;
    finally
      ADBLocations.Unlock;
    end;
  end;

begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');

    SetDBLocations(ctx_DomainInfo.DBLocationList, 'DBLocation');
    SetDBLocations(ctx_DomainInfo.ADRDBLocationList, 'ADRDBLocation');

    if AParams.ValueExists('DBChangesBroadcast') then
      ctx_DomainInfo.DBChangesBroadcast := AParams.Value['DBChangesBroadcast'];

    mb_GlobalSettings.Save;
    Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('DB.Path');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevRBControl.GetRPPoolStatus: IisParamsCollection;
var
  i: Integer;
  L: IisList;
  Srv: IevRPInPoolInfo;
  SrvInfo: IisListOfValues;
begin
  Result := TisParamsCollection.Create;

  L := MaleConnector.RPPool.CopyItems(coAll);
  for i := 0  to L.Count - 1 do
  begin
    Srv := L[i] as IevRPInPoolInfo;
    Srv.UpdateCurrentState;

    SrvInfo := Result.AddParams(Srv.Host);
    SrvInfo.AddValue('Host', Srv.Host);
    SrvInfo.AddValue('Connected', BoolToYN(Srv.Connected));
    SrvInfo.AddValue('MaxScore', IntToStr(Srv.MaxScore));
    SrvInfo.AddValue('CurrentScore', IntToStr(Srv.CurrentScore));
    SrvInfo.AddValue('Requests', IntToStr(Srv.RequestCount));
  end;
end;

function TevRBControl.UsersPoolStatus: IisParamsCollection;
var
  i: Integer;
  LV: IisListOfValues;
  Session: IevSession;
  Comm: IisTCPStreamCommunicator;
  s: String;
  Sessions, L: IisList;
  AppInfo: TisAppInfo;
begin
  Result := TisParamsCollection.Create;

  Sessions := FemaleConnector.DatagramDispatcher.GetSessions;

  for i := 0 to Sessions.Count - 1 do
  begin
    Session := Sessions[i] as IevSession;
    LV := Result.AddParams(Session.SessionID);
    LV.AddValue('Login', Session.Login);

    if Assigned(Session.Transmitter) then
    begin
      Comm := (Session.Transmitter as IisTCPStreamTransmitter).TCPCommunicator;
      L := Comm.GetConnectionsBySessionID(Session.SessionID);

      LV.AddValue('Connections', L.Count);
      if L.Count > 0 then
        s := (L[0] as IisTCPStreamAsyncConnection).GetRemoteIPAddress
      else
        s := '';
      LV.AddValue('Address', s)
    end;

    LV.AddValue('SessionState', Session.StateDescr);

    AppInfo := AppInfoByID(Session.AppID);
    s := AppInfo.Abbr;
    if (Length(s) > 3) and  StartsWith(s, 'Evo') then
      Delete(s, 1, 3);

    LV.AddValue('AppType', s);

    L := MaleConnector.PendingRequestList.GetRequestsBySrcSessionID(Session.SessionID);
    LV.AddValue('ExecutingRequests', L.Count);

    if Session.LastActivity = 0 then
      LV.AddValue('LastActivity', '')    
    else
      LV.AddValue('LastActivity', Session.LastActivity);
  end;
end;

function TevRBControl.MaleConnector: IevRPTCPClient;
begin
  Result := Mainboard.TCPClient as IevRPTCPClient;
end;

function TevRBControl.GetDomainList: IisListOfValues;
var
  i:Integer;
begin
  Result := TisListOfValues.Create;
  mb_GlobalSettings.DomainInfoList.Lock;
  try
    for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      Result.AddValue(mb_GlobalSettings.DomainInfoList[i].DomainName,
                      mb_GlobalSettings.DomainInfoList[i].DomainDescription);
  finally
    mb_GlobalSettings.DomainInfoList.Unlock;
  end;
end;

function TevRBControl.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  Mainboard.LogFile.Lock;
  try
    CheckCondition((EventNbr >= 0) and (EventNbr < Mainboard.LogFile.Count), 'Event number is incorrect');
    Event := Mainboard.LogFile[EventNbr];
  finally
    Mainboard.LogFile.UnLock;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);  
end;

function TevRBControl.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  Mainboard.LogFile.Lock;
  try
    L := Mainboard.LogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := Mainboard.LogFile.Count div EventsPerPage;
    if Mainboard.LogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    Mainboard.LogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevRBControl.FemaleConnector: IevTCPServer;
begin
  Result := Mainboard.TCPServer;
end;

function TevRBControl.GetMachineInfo: IisListOfValues;
var
  N: Cardinal;
begin
  N := Mainboard.MachineInfo.ID;
  Result := TisListOfValues.Create;
  Result.AddValue('Name', Mainboard.MachineInfo.Name);
  Result.AddValue('ID', BufferToHex(N, SizeOf(N)));
  Result.AddValue('IP_Address', Mainboard.MachineInfo.IPaddress);
end;

function TevRBControl.GetRPPoolInfo: IisParamsCollection;
var
  i: Integer;
  L: IisList;
  Srv: IevRPInPoolInfo;
  SrvInfo: IisListOfValues;
begin
  Result := TisParamsCollection.Create;

  L := MaleConnector.RPPool.CopyItems(coAll);
  for i := 0  to L.Count - 1 do
  begin
    Srv := L[i] as IevRPInPoolInfo;
    SrvInfo := Result.AddParams(Srv.Host);
    SrvInfo.AddValue('Host', Srv.Host);
    SrvInfo.AddValue('Enabled', Srv.Enabled);
  end;
end;

procedure TevRBControl.SetRPPoolInfo(const AInfo: IisParamsCollection);
var
  i, j: Integer;
  SrvInfo: IisListOfValues;
  MaxScoreList, MaxLongReqList: IisStringList;
  L: IisStringListRO;
begin
  // Delete old nodes
  L := mb_AppConfiguration.GetValueNames('RequestBroker\RequestProcessor');
  for i := 0 to L.Count - 1 do
    mb_AppConfiguration.DeleteValue('RequestBroker\RequestProcessor\' + L[i]);

  MaxScoreList := mb_AppConfiguration.GetValueNames('RequestBroker\RequestProcessor\MaxScore') as IisStringList;
  MaxScoreList.CaseSensitive := False;

  MaxLongReqList := mb_AppConfiguration.GetValueNames('RequestBroker\RequestProcessor\MaxLongRequests') as IisStringList;
  MaxLongReqList.CaseSensitive := False;

  for i := 0 to AInfo.Count - 1 do
  begin
    SrvInfo := AInfo[i];
    mb_AppConfiguration.AsBoolean['RequestBroker\RequestProcessor\' + SrvInfo.Value['Host']] := SrvInfo.Value['Enabled'];

    j := MaxScoreList.IndexOf(SrvInfo.Value['Host']);
    if j <> -1 then
      MaxScoreList.Delete(j);

    j := MaxLongReqList.IndexOf(SrvInfo.Value['Host']);
    if j <> -1 then
      MaxLongReqList.Delete(j);
  end;

  for i := 0 to MaxScoreList.Count - 1 do
    mb_AppConfiguration.DeleteValue('RequestBroker\RequestProcessor\MaxScore\' + MaxScoreList[i]);

  for i := 0 to MaxLongReqList.Count - 1 do
    mb_AppConfiguration.DeleteValue('RequestBroker\RequestProcessor\MaxLongRequests\' + MaxLongReqList[i]);

  MaleConnector.RPPool.ApplyConfiguration;
end;

procedure TevRBControl.GetSystemAccount(const ADomainName: String; out AUser: String);
var
  DomainInfo: IevDomainInfo;
begin
  DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if Assigned(DomainInfo) then
    AUser := DomainInfo.SystemAccountInfo.UserName
  else
    raise EevException.CreateFmt('Domain name %s not found',[ADomainName]);
end;

procedure TevRBControl.SetSystemAccount(const ADomainName, AUser: String);
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    ctx_DomainInfo.SystemAccountInfo.UserName := AUser;
    mb_GlobalSettings.Save;
    Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('SystemAccount');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevRBControl.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  L := Mainboard.LogFile.GetEvents(AQuery);

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TevRBControl.GetAppTempFolder: String;
begin
  Result := AppTempFolder;
end;

procedure TevRBControl.SetAppTempFolder(const ATempFolder: String);
begin
  isBasicUtils.SetAppTempFolder(ATempFolder);
  mb_AppConfiguration.SetValue('General\TempFolder', ATempFolder);
end;

procedure TevRBControl.SendMessage(const aText, aToUsers: String);
begin
  Mainboard.GlobalCallbacks.PopupMessage(aText, 'Administrator', aToUsers);
end;

function TevRBControl.GetPrintersInfo: IisListOfValues;
var
  AvailablePrinters: IisStringList;
  i: Integer;
  Prn: IevPrinterInfo;
begin
  Result := TisListOfValues.Create;

  Result.AddValue('PrintingEnabled', not mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview']);

  Prn := Mainboard.MachineInfo.Printers.FindPrinter(mb_AppSettings.GetValue('Settings\ChecksPrinter', ''));
  if Assigned(Prn) then
  begin
    Result.AddValue('ChecksPrinter', Prn.Name);
    Result.AddValue('ChecksPrinter.VertOffset', Prn.VOffset);
    Result.AddValue('ChecksPrinter.HorizOffset', Prn.HOffset);
  end
  else
  begin
    Result.AddValue('ChecksPrinter', '');
    Result.AddValue('ChecksPrinter.VertOffset', 0);
    Result.AddValue('ChecksPrinter.HorizOffset', 0);
  end;

  Prn := Mainboard.MachineInfo.Printers.FindPrinter(mb_AppSettings.GetValue('Settings\ReportsPrinter', ''));
  if Assigned(Prn) then
  begin
    Result.AddValue('ReportsPrinter', Prn.Name);
    Result.AddValue('ReportsPrinter.VertOffset', Prn.VOffset);
    Result.AddValue('ReportsPrinter.HorizOffset', Prn.HOffset);
  end
  else
  begin
    Result.AddValue('ReportsPrinter', '');
    Result.AddValue('ReportsPrinter.VertOffset', 0);
    Result.AddValue('ReportsPrinter.HorizOffset', 0);
  end;

  Result.AddValue('DuplexMode', mb_AppSettings.GetValue('Settings\DuplexPrinting', 0));

  AvailablePrinters := TisStringList.Create;

  Mainboard.MachineInfo.Printers.Lock;
  try
    Mainboard.MachineInfo.Printers.Load; // refresh
    for i := 0 to Mainboard.MachineInfo.Printers.Count - 1 do
      AvailablePrinters.Add(Mainboard.MachineInfo.Printers[i].Name);
  finally
    Mainboard.MachineInfo.Printers.Unlock;  
  end;

  Result.AddValue('Printers', AvailablePrinters);
end;

procedure TevRBControl.SetPrintersInfo(const APrintersInfo: IisListOfValues);
var
  Prn: IevPrinterInfo;
begin
  mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview'] := not APrintersInfo.TryGetValue('PrintingEnabled', False);

  mb_AppSettings.AsString['Settings\ChecksPrinter'] := APrintersInfo.TryGetValue('ChecksPrinter', '');
  Prn := Mainboard.MachineInfo.Printers.FindPrinter(APrintersInfo.TryGetValue('ChecksPrinter', ''));
  if Assigned(Prn) then
  begin
    Prn.VOffset := APrintersInfo.TryGetValue('ChecksPrinter.VertOffset', 0);
    Prn.HOffset := APrintersInfo.TryGetValue('ChecksPrinter.HorizOffset', 0);
  end;

  mb_AppSettings.AsString['Settings\ReportsPrinter'] := APrintersInfo.TryGetValue('ReportsPrinter', '');
  Prn := Mainboard.MachineInfo.Printers.FindPrinter(APrintersInfo.TryGetValue('ReportsPrinter', ''));
  if Assigned(Prn) then
  begin
    Prn.VOffset := APrintersInfo.TryGetValue('ReportsPrinter.VertOffset', 0);
    Prn.HOffset := APrintersInfo.TryGetValue('ReportsPrinter.HorizOffset', 0);
  end;

  mb_AppSettings.AsInteger['Settings\DuplexPrinting'] := APrintersInfo.TryGetValue('DuplexMode', 0);
end;

function TevRBControl.GetMaintenanceDB: IisListOfValues;
var
  i: Integer;
  L: IisStringList;

  function DomainDBs(const ADomainName: String): IisStringList;
  begin
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    try
      Result := ctx_DBAccess.GetDisabledDBs;
    finally
      Mainboard.ContextManager.DestroyThreadContext;
    end;
  end;

begin
  Result := TisListOfValues.Create;
  Mainboard.ContextManager.StoreThreadContext;
  try
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      begin
        L := DomainDBs(mb_GlobalSettings.DomainInfoList[i].DomainName);
        Result.AddValue(mb_GlobalSettings.DomainInfoList[i].DomainName, L);
      end;
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRBControl.SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
var
  i: Integer;

  procedure DomainDBs(const ADomainName: String; const ADBList: IisStringList);
  begin
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    try
      if AMaintenance then
        ctx_DBAccess.DisableDBs(ADBList)
      else
        ctx_DBAccess.EnableDBs(ADBList);
    finally
      Mainboard.ContextManager.DestroyThreadContext;
    end;
  end;

begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    for i := 0 to ADBListByDomains.Count - 1 do
      DomainDBs(ADBListByDomains[i].Name, IInterface(ADBListByDomains[i].Value) as IisStringList);
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;

  if not AMaintenance then
    mb_TaskQueue.Pulse; // Wake up queue!
end;

function TevRBControl.GetSystemStatus: IisParamsCollection;
var
  Item, LV: IisListOfValues;
  L, L2: IisList;
  i, j, ReceivingQueueSize, SendingQueueSize: Integer;
  Session: IevSession;
  s1, s2: String;
  RequestList: IevRPPendingRequestList;
  Request: IevRPPendingRequest;
  RPPool: IevRPPool;
  RPInPoolInfo: IevRPInPoolInfo;
  AppInfo: TisAppInfo;
begin
  Result := TisParamsCollection.Create;

  // collect Client transmitter queue status
  Item := Result.AddParams('CLTransmitterStatus');
  Mainboard.TCPServer.DatagramDispatcher.Transmitter.QueueStatus(ReceivingQueueSize, SendingQueueSize);
  Item.AddValue('ReceivingQueueSize', ReceivingQueueSize);
  Item.AddValue('SendingQueueSize', SendingQueueSize);

  // collect RP transmitter queue status
  Item := Result.AddParams('RPTransmitterStatus');
  Mainboard.TCPClient.DatagramDispatcher.Transmitter.QueueStatus(ReceivingQueueSize, SendingQueueSize);
  Item.AddValue('ReceivingQueueSize', ReceivingQueueSize);
  Item.AddValue('SendingQueueSize', SendingQueueSize);

  // collect client session info
  Item := Result.AddParams('ClientSessions');
  L := Mainboard.TCPServer.DatagramDispatcher.GetSessions;
  for i := 0 to L.Count - 1 do
  begin
    Session := L[i] as IevSession;
    LV := TisListOfValues.Create;

    AppInfo := AppInfoByID(Session.AppID);
    s1 := AppInfo.Abbr;
    if (Length(s1) > 3) and  StartsWith(s1, 'Evo') then
      Delete(s1, 1, 3);

    LV.AddValue('AppID', s1);
    LV.AddValue('Login', Session.Login);
    LV.AddValue('State', Session.StateDescr);
    LV.AddValue('Encryption', Session.EncryptionTypeDscr);
    LV.AddValue('Compression', Session.CompressionLevel);

    s1 := '';
    L2 := Session.Transmitter.TCPCommunicator.GetConnectionsBySessionID(Session.SessionID);
    for j := 0 to L2.Count - 1 do
    begin
      s2 := (L2[j] as IisTCPStreamAsyncConnection).GetRemoteIPAddress;
      if not Contains(s1, s2) then
        AddStrValue(s1, s2, ',');
    end;
    LV.AddValue('Connections', L2.Count);
    LV.AddValue('IPAddress', s1);

    Item.AddValue(Session.SessionID, LV);
  end;

  // collect RP session info
  Item := Result.AddParams('RPSessions');
  L := Mainboard.TCPClient.DatagramDispatcher.GetSessions;
  for i := 0 to L.Count - 1 do
  begin
    Session := L[i] as IevSession;
    LV := TisListOfValues.Create;
    LV.AddValue('State', Session.StateDescr);
    LV.AddValue('Encryption', Session.EncryptionTypeDscr);
    LV.AddValue('Compression', Session.CompressionLevel);

    L2 := Session.Transmitter.TCPCommunicator.GetConnectionsBySessionID(Session.SessionID);
    if L2.Count > 0 then
      s1 := SocketLib.LookupHostAddr((L2[0] as IisTCPStreamAsyncConnection).GetRemoteIPAddress)
    else
      s1 := '';
    LV.AddValue('Connections', L2.Count);
    LV.AddValue('Host', s1);

    Item.AddValue(Session.SessionID, LV);
  end;

  // collect RP pool status
  Item := Result.AddParams('RPPool');
  RPPool := (Mainboard.TCPClient as IevRPTCPClient).RPPool;
  L := RPPool.CopyItems(coAll);
  for i := 0 to L.Count - 1 do
  begin
    RPInPoolInfo := L[i] as IevRPInPoolInfo;
    RPInPoolInfo.UpdateCurrentState;
    LV := TisListOfValues.Create;

    if Assigned(RPInPoolInfo.Session) then
      s1 := RPInPoolInfo.Session.SessionID
    else
      s1 := '';
    LV.AddValue('Session', s1);

    LV.AddValue('Enabled', RPInPoolInfo.Enabled);
    LV.AddValue('MaxScore', RPInPoolInfo.MaxScore);
    LV.AddValue('CurrentScore', RPInPoolInfo.CurrentScore);
    LV.AddValue('MaxTaskRequests', RPInPoolInfo.MaxLongRequests);
    LV.AddValue('TaskRequests', RPInPoolInfo.LongRequests);
    LV.AddValue('NewTaskRequests', RPInPoolInfo.GetNewLongRequestCount);
    LV.AddValue('RTRequests', RPInPoolInfo.RequestCount);
    LV.AddValue('NewRTRequests', RPInPoolInfo.GetNewRequestCount);

    Item.AddValue(RPInPoolInfo.Host, LV);
  end;


  // collect pending requests status
  RequestList := (Mainboard.TCPClient as IevRPTCPClient).PendingRequestList;
  Item := Result.AddParams('Requests');
  L := RequestList.GetAllRequests;
  for i := 0 to RequestList.Count - 1 do
  begin
    Request := L[i] as IevRPPendingRequest;
    LV := TisListOfValues.Create;

    LV.AddValue('Method', Request.DatagramHeader.Method);

    if Assigned(Request.SourceSession) then
      s1 := Request.SourceSession.SessionID
    else
      s1 := '';
    LV.AddValue('SourceSession', s1);

    if Assigned(Request.DestinationSession) then
      s1 := Request.DestinationSession.SessionID
    else
      s1 := '';
    LV.AddValue('DestinationSession', s1);

    LV.AddValue('TaskRequest', Request.IsLongRequest);
    LV.AddValue('Priority', Request.GetRequestPriority);

    Item.AddValue(Request.ID, LV);
  end;
end;

function TevRBControl.GetAUS: String;
begin
  Result := mb_AppConfiguration.AsString['General\AlternativeVerUpdSource'];
end;

procedure TevRBControl.SetAUS(const AValue: String);
begin
  mb_AppConfiguration.AsString['General\AlternativeVerUpdSource'] := AValue;
end;

function TevRBControl.GetInternalRR: IisStringList;
begin
  Result := TisStringList.Create;
  Result.Text := mb_GlobalSettings.InternalRR.Text;
end;

procedure TevRBControl.SetInternalRR(const ARRList: IisStringList);
begin
  mb_GlobalSettings.InternalRR.Text := ARRList.Text;
  mb_GlobalSettings.Save;
  TevClientDatagramDispatcher((FemaleConnector.DatagramDispatcher as IisInterfacedObject).GetImplementation).PrepareInternalRRIP;
end;

function TevRBControl.GetInternalUserRR(const ADomainName: String): Boolean;
var
  DomainInfo: IevDomainInfo;
begin
  DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if Assigned(DomainInfo) then
    Result := DomainInfo.InternalUsersThroughRR
  else
    raise EevException.CreateFmt('Domain name %s not found',[ADomainName]);
end;

procedure TevRBControl.SetInternalUserRR(const ADomainName: String; const AValue: Boolean);
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    ctx_DomainInfo.InternalUsersThroughRR := AValue;
    mb_GlobalSettings.Save;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

function TevRBControl.GetBBDiagnosticCheck: String;
begin
  Result := Mainboard.BBClient.DiagnosticCheck;
end;

procedure TevRBControl.GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
begin
  APurgeRecThreshold := GlobalLogFile.PurgeRecThreshold;
  APurgeRecCount := GlobalLogFile.PurgeRecCount;
end;

procedure TevRBControl.SetLogFileThresholdInfo(const APurgeRecThreshold,
  APurgeRecCount: Integer);
begin
  mb_AppConfiguration.AsInteger['General\LogFilePurgeRecThreshold'] := APurgeRecThreshold;
  mb_AppConfiguration.AsInteger['General\LogFilePurgeRecCount'] := APurgeRecCount;

  GlobalLogFile.PurgeRecCount := APurgeRecCount;
  GlobalLogFile.PurgeRecThreshold := APurgeRecThreshold;
end;

function TevRBControl.GetStackInfo: String;
begin
  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      Result := GetStack(E);
  end;
end;

procedure TevRBControl.SetKnownADRServices(const AServices: IisListOfValues);
var
  i: Integer;
  bACInstalled: Boolean;
  Domain: IevDomainInfo;
  Changed: Boolean;
begin
  mb_AppConfiguration.Lock;
  try
    mb_AppConfiguration.ClearNode('RequestBroker\ADRClient');
    mb_AppConfiguration.ClearNode('RequestBroker\ADRServer');
    bACInstalled := False;

    for i := 0 to AServices.Count - 1 do
    begin
      if Contains(AServices[i].Value, 'AC') then
      begin
        mb_AppConfiguration.AsBoolean['RequestBroker\ADRClient\' + AServices[i].Name] := True;
        bACInstalled := True;
      end;
      if Contains(AServices[i].Value, 'AS') then
        mb_AppConfiguration.AsBoolean['RequestBroker\ADRServer\' + AServices[i].Name] := True;
    end;
  finally
    mb_AppConfiguration.Unlock;
  end;


  if bACInstalled then
    mb_GlobalSettings.DomainInfoList.Lock;
    try
      Changed := False;
      for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
      begin
        Domain := mb_GlobalSettings.DomainInfoList[i];
        if not Domain.DBChangesBroadcast then
        begin
          Domain.DBChangesBroadcast := True;
          Changed := True;
        end;
      end;

      if Changed then
      begin
        mb_GlobalSettings.Save;
        Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('DB.ChangesBroadcast');
      end;
    finally
      mb_GlobalSettings.DomainInfoList.Unlock;
    end;
end;

function TevRBControl.GetDatabasePasswords: IisListOfValues;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');

    Result := TisListOfValues.Create;
    Result.AddValue('DBAdminPassword', mb_GlobalSettings.DBAdminPassword);
    Result.AddValue('DBUserPassword', mb_GlobalSettings.DBUserPassword);
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRBControl.SetDatabasePasswords(const AParams: IisListOfValues);
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');

    mb_GlobalSettings.DBAdminPassword := AParams.TryGetValue('DBAdminPassword', mb_GlobalSettings.DBAdminPassword);
    mb_GlobalSettings.DBUserPassword := AParams.TryGetValue('DBUserPassword', mb_GlobalSettings.DBUserPassword);
    mb_GlobalSettings.Save;

    Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('DB.Passwords');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRBControl.ResetAdminPassword(const ADomainName, APassword: String);
var
  Domain: IevDomainInfo;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Domain := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
    if not Assigned(Domain) then
      Exit;

    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(Domain.SystemAccountInfo.UserName, Domain.DomainName), Domain.SystemAccountInfo.Password);
    ctx_Security.ResetPassword(ctx_DomainInfo.AdminAccount, nil, APassword);
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevRBControl.GetAdminAccount(const ADomainName: String; out AUser: String);
var
  DomainInfo: IevDomainInfo;
begin
  DomainInfo := mb_GlobalSettings.DomainInfoList.GetDomainInfo(ADomainName);
  if Assigned(DomainInfo) then
    AUser := DomainInfo.AdminAccount
  else
    raise EevException.CreateFmt('Domain name %s not found',[ADomainName]);
end;

procedure TevRBControl.SetAdminAccount(const ADomainName, AUser: String);
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, ADomainName), '');
    ctx_DomainInfo.AdminAccount := AUser;
    mb_GlobalSettings.Save;
    Mainboard.GlobalCallbacks.NotifyGlobalSettingsChange('AdminAccount');
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

{ TevClientDatagramDispatcher }

procedure TevClientDatagramDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
begin
  inherited;
  if not Handled and not IsTransportDatagram(ADatagramHeader) then
    if not ADatagramHeader.IsResponse and not HandlerExists(ADatagramHeader.Method) then
    begin
      // Client <-> RP pass through data
      (Mainboard.TCPClient as IevRPTCPClient).ForwardDatagram(AStream, ASession);
      Handled := True;
    end;
end;

function TevClientDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;

  function IsADRService: Boolean;
  var
    L: IisStringListRO;
    sIP, sSection, sValName: String;
    i: Integer;
  begin
    Result := False;

    if AnsiSameStr(AClientAppID, EvoADRClientAppInfo.AppID) then
      sSection := 'RequestBroker\ADRClient'
    else if AnsiSameStr(AClientAppID, EvoADRServerAppInfo.AppID) then
      sSection := 'RequestBroker\ADRServer'
    else
      Exit;

    sIP := ThisSession.GetRemoteIPAddress;
    L := mb_AppConfiguration.GetValueNames(sSection);
    for i := 0 to L.Count - 1 do
    begin
      sValName := sSection + '\' + L[i];
      if mb_AppConfiguration.AsBoolean[sValName] then
        if (SocketLib.LookupHostAddr(L[i]) = sIP) or SocketLib.IsLocalAddress(sIP) then
        begin
          Result := True;
          break;
        end;
    end;
  end;

begin
  Result := AnsiSameStr(AClientAppID, EvoClientAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoRemoteRelayAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoRWAdapterAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoAPIAdapterAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoDNetConnectorAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoDNetMapleConnectorAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoJavaESSAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoJavaWCAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoJavaMyHRAdminAppInfo.AppID);

  if not Result then
    Result := IsADRService;
end;

procedure TevClientDatagramDispatcher.dmGlobalSettingsGet(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  if AnsiSameStr(ThisSession.AppID, EvoADRClientAppInfo.AppID) or AnsiSameStr(ThisSession.AppID, EvoADRServerAppInfo.AppID) then
    inherited dmGlobalSettingsGet(ADatagram, AResult)
  else
    raise ESecurityViolation.Create('Wrong application type');
end;

procedure TevClientDatagramDispatcher.DoOnConstruction;
begin
  inherited;
  SetThreadPoolCapacity(100);
  ActivateProgressUpdater;

  FInternalRRIPs := TisStringList.CreateUnique(True);
  PrepareInternalRRIP;
end;

function TevClientDatagramDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
var
  sUser, sDomain: String;
  Session: IevSession;
begin
  // Check for maintenance mode
  DecodeUserAtDomain(ADatagram.Header.User, sUser, sDomain);
  Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDomain), '');
  try
    ErrorIf(IsMaintenanceMode, 'Evolution is in maintenance mode. Please try to log in later.');
  finally
    Mainboard.ContextManager.DestroyThreadContext;
  end;

  // Check user and password
  Mainboard.ContextManager.CreateThreadContext(ADatagram.Header.User, ADatagram.Header.Password,
    '', ADatagram.Header.ContextID); // raises an exception if login is invalid
  try
    if AnsiSameText(sUser, ctx_DomainInfo.SystemAccountInfo.UserName) then
      raise EevException.Create('Login has been rejected for this account');

    if Context.UserAccount.AccountType in [uatServiceBureau, uatSBAdmin] then
    begin
      Session := FindSessionByID(ADatagram.Header.SessionID);
      if AnsiSameStr(Session.AppID, EvoRemoteRelayAppInfo.AppID) then
        if not ctx_DomainInfo.InternalUsersThroughRR or (FInternalRRIPs.Count > 0) and (FInternalRRIPs.IndexOf(Session.GetRemoteIPAddress) = -1) then
          raise EevException.Create('Login has been rejected. Internal user is not allowed to login remotely');
    end;
  finally
    Mainboard.ContextManager.DestroyThreadContext;
  end;

  Result := True;
end;

procedure TevClientDatagramDispatcher.PrepareInternalRRIP;
var
  s, sHost: String;
begin
  FInternalRRIPs.Lock;
  try
    s := mb_GlobalSettings.InternalRR.CommaText;
    FInternalRRIPs.Clear;
    while s <> '' do
    begin
      sHost := GetNextStrValue(s, ',');
      if AnsiSameText(Mainboard.MachineInfo.Name, sHost) then
        FInternalRRIPs.Add('127.0.0.1');

      sHost := SocketLib.LookupHostAddr(sHost);
      FInternalRRIPs.Add(sHost);
    end;
  finally
    FInternalRRIPs.Unlock;
  end;
end;

procedure TevClientDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_GLOBALSETTINGS_GET, dmGlobalSettingsGet);

  AddHandler(METHOD_VERSION_UPDATE_REMOTE_GETVERSIONINFO, dmVersionUpdateRemoteGetVersionInfo);
  AddHandler(METHOD_VERSION_UPDATE_REMOTE_CHECK, dmVersionUpdateRemoteCheck);
  AddHandler(METHOD_VERSION_UPDATE_REMOTE_GETUPDATEPACK, dmVersionUpdateRemoteGetUpdatePack);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTCPServer, IevTCPServer, 'TCP Server');
  Mainboard.ModuleRegister.RegisterModule(@GetRPTCPClient, IevTCPClient, 'RP TCP Client');
end.

