unit EvRBController;

interface

uses Classes, SysUtils, isBaseClasses, EvTransportShared, evRemoteMethods,
    EvConsts, EvControllerInterfaces, EvTransportInterfaces, isAppIDs, isLogFile,
    EvMainboard, EvBasicUtils;

type
   TevRBController = class(TevCustomTCPServer, IevAppController)
   private
   protected
     procedure DoOnConstruction; override;   
     function  CreateDispatcher: IevDatagramDispatcher; override;
   end;

implementation

uses
  evRBServerMod;

type
  TevRBControllerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmGetMachineInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmHardware(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmModules(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetDomainList(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLicenseInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetLicenseInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFolders(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetFolders(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetEMailInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetEMailInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetDatabasePasswords(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetDatabasePasswords(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetDatabaseParams(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetDatabaseParams(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetSystemAccount(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetSystemAccount(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetAdminAccount(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetAdminAccount(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetRPPoolInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetRPPoolInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetRPPoolStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStackInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetUsersPoolStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetAppTempFolder(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetAppTempFolder(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetPrintersInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetPrintersInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSendMessage(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetMaintenanceDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetMaintenanceDB(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure GetSystemStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetAUS(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetAUS(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetInternalRR(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetInternalRR(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetInternalUserRR(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetInternalUserRR(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetBBDiagnosticCheck(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetLogFileThresholdInfo(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmSetKnownADRServices(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmResetAdminPassword(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure RegisterHandlers; override;
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;
  end;



{ TevRBControllerDatagramDispatcher }

procedure TevRBControllerDatagramDispatcher.dmHardware(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := RBControl.GetHardware;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmModules(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisStringList;
begin
  Res := RBControl.GetModules;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmGetLicenseInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RBControl.GetLicenseInfo(ADatagram.Params.Value['ADomainName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmSetLicenseInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetLicenseInfo(
    ADatagram.Params.Value['ADomainName'],
    ADatagram.Params.Value['ASerialNumber'],
    ADatagram.Params.Value['ARegistrationCode']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetFolders(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RBControl.GetFolders(ADatagram.Params.Value['ADomainName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmSetFolders(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetFolders(ADatagram.Params.Value['ADomainName'],
    IInterface(ADatagram.Params.Value['AFolders']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetRPPoolStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := RBControl.GetRPPoolStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmGetDatabaseParams(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RBControl.GetDatabaseParams(ADatagram.Params.Value['ADomainName']);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmSetDatabaseParams(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetDatabaseParams(
    ADatagram.Params.Value['ADomainName'],
    IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetEMailInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String;
begin
  RBControl.GetEMailInfo(AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('AHost', AHost);
  AResult.Params.AddValue('APort', APort);
  AResult.Params.AddValue('AUser', AUser);
  AResult.Params.AddValue('APassword', APassword);
  AResult.Params.AddValue('ASentFrom', ASentFrom);
  AResult.Params.AddValue('ASendAlertsTo', ASendAlertsTo);
end;

procedure TevRBControllerDatagramDispatcher.dmSetEMailInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetEmailInfo(ADatagram.Params.Value['AHost'],
               ADatagram.Params.Value['APort'],
               ADatagram.Params.Value['AUser'],
               ADatagram.Params.Value['APassword'],
               ADatagram.Params.Value['ASentFrom'],
               ADatagram.Params.Value['ASendAlertsTo']);
  AResult := CreateResponseFor(ADatagram.Header);
end;


procedure TevRBControllerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_RBCONTROLLER_GETMACHINEINFO, dmGetMachineInfo);
  AddHandler(METHOD_RBCONTROLLER_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_RBCONTROLLER_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_RBCONTROLLER_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_RBCONTROLLER_GETHARDWARE, dmHardware);
  AddHandler(METHOD_RBCONTROLLER_GETMODULES, dmModules);
  AddHandler(METHOD_RBCONTROLLER_GETDOMAINLIST, dmGetDomainList);
  AddHandler(METHOD_RBCONTROLLER_GETLICENSEINFO, dmGetLicenseInfo);
  AddHandler(METHOD_RBCONTROLLER_SETLICENSEINFO, dmSetLicenseInfo);
  AddHandler(METHOD_RBCONTROLLER_GETFOLDERS, dmGetFolders);
  AddHandler(METHOD_RBCONTROLLER_SETFOLDERS, dmSetFolders);
  AddHandler(METHOD_RBCONTROLLER_GETEMAILINFO, dmGetEMailInfo);
  AddHandler(METHOD_RBCONTROLLER_SETEMAILINFO, dmSetEMailInfo);
  AddHandler(METHOD_RBCONTROLLER_GETDATABASEPASSWORDS, dmGetDatabasePasswords);
  AddHandler(METHOD_RBCONTROLLER_SETDATABASEPASSWORDS, dmSetDatabasePasswords);
  AddHandler(METHOD_RBCONTROLLER_GETDATABASEPARAMS, dmGetDatabaseParams);
  AddHandler(METHOD_RBCONTROLLER_SETDATABASEPARAMS, dmSetDatabaseParams);
  AddHandler(METHOD_RBCONTROLLER_GETSYSTEMACCOUNT, dmGetSystemAccount);
  AddHandler(METHOD_RBCONTROLLER_SETSYSTEMACCOUNT, dmSetSystemAccount);
  AddHandler(METHOD_RBCONTROLLER_GETADMINACCOUNT, dmGetAdminAccount);
  AddHandler(METHOD_RBCONTROLLER_SETADMINACCOUNT, dmSetAdminAccount);
  AddHandler(METHOD_RBCONTROLLER_GETRPPOOLSTATUS, dmGetRPPoolStatus);
  AddHandler(METHOD_RBCONTROLLER_GETUSERSPOOLSTATUS, dmGetUsersPoolStatus);
  AddHandler(METHOD_RBCONTROLLER_GETRPPOOLINFO, dmGetRPPoolInfo);
  AddHandler(METHOD_RBCONTROLLER_SETRPPOOLINFO, dmSetRPPoolInfo);
  AddHandler(METHOD_RBCONTROLLER_GETSTACKINFO, dmGetStackInfo);
  AddHandler(METHOD_RBCONTROLLER_GETAPPTEMPFOLDER, dmGetAppTempFolder);
  AddHandler(METHOD_RBCONTROLLER_SETAPPTEMPFOLDER, dmSetAppTempFolder);
  AddHandler(METHOD_RBCONTROLLER_GETPRINTERSINFO, dmGetPrintersInfo);
  AddHandler(METHOD_RBCONTROLLER_SETPRINTERSINFO, dmSetPrintersInfo);
  AddHandler(METHOD_RBCONTROLLER_SENDMESSAGE, dmSendMessage);
  AddHandler(METHOD_RBCONTROLLER_SETMAINTENANCEDB, dmSetMaintenanceDB);
  AddHandler(METHOD_RBCONTROLLER_GETMAINTENANCEDB, dmGetMaintenanceDB);
  AddHandler(METHOD_RBCONTROLLER_GETSYSTEMSTATUS, GetSystemStatus);
  AddHandler(METHOD_RBCONTROLLER_GETAUS, dmGetAUS);
  AddHandler(METHOD_RBCONTROLLER_SETAUS, dmSetAUS);
  AddHandler(METHOD_RBCONTROLLER_GETINTERNALRR, dmGetInternalRR);
  AddHandler(METHOD_RBCONTROLLER_SETINTERNALRR, dmSetInternalRR);
  AddHandler(METHOD_RBCONTROLLER_GETINTERNALUSERRR, dmGetInternalUserRR);
  AddHandler(METHOD_RBCONTROLLER_SETINTERNALUSERRR, dmSetInternalUserRR);
  AddHandler(METHOD_RBCONTROLLER_GETBBDIAGNOSTICCHECK, dmGetBBDiagnosticCheck);
  AddHandler(METHOD_RBCONTROLLER_GETLOGFILETHRESHOLDINFO, dmGetLogFileThresholdInfo);
  AddHandler(METHOD_RBCONTROLLER_SETLOGFILETHRESHOLDINFO, dmSetLogFileThresholdInfo);
  AddHandler(METHOD_RBCONTROLLER_SETKNOWNADRSERVICES, dmSetKnownADRServices);
  AddHandler(METHOD_RBCONTROLLER_RESETADMINPASSWORD, dmResetAdminPassword);  
end;


function TevRBControllerDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoDeployMgrAppInfo.AppID);
end;

procedure TevRBControllerDatagramDispatcher.dmGetDomainList(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisListOfValues;
begin
  L := RBControl.GetDomainList;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);
end;

procedure TevRBControllerDatagramDispatcher.dmGetUsersPoolStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisParamsCollection;
begin
  L := RBControl.UsersPoolStatus;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);
end;

procedure TevRBControllerDatagramDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RBControl.GetLogEventDetails(
    ADatagram.Params.Value['EventNbr']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  PageCount: Integer;
begin
  Res := RBControl.GetLogEvents(
    ADatagram.Params.Value['PageNbr'],
    ADatagram.Params.Value['EventsPerPage'],
    PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevRBControllerDatagramDispatcher.dmGetMachineInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  L: IisListOfValues;
begin
  L := RBControl.GetMachineInfo;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, L);
end;

procedure TevRBControllerDatagramDispatcher.dmGetRPPoolInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := RBControl.GetRPPoolInfo;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmSetRPPoolInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetRPPoolInfo(IInterface(ADatagram.Params.Value['AInfo']) as IisParamsCollection);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetSystemAccount(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  User: String;
begin
  RBControl.GetSystemAccount(ADatagram.Params.Value['ADomainName'], User);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('AUser', User);
end;

procedure TevRBControllerDatagramDispatcher.dmSetSystemAccount(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetSystemAccount(
    ADatagram.Params.Value['ADomainName'],
    ADatagram.Params.Value['AUser']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

  Res := RBControl.GetFilteredLogEvents(Query);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmGetAppTempFolder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetAppTempFolder);
end;

procedure TevRBControllerDatagramDispatcher.dmSetAppTempFolder(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetAppTempFolder(ADatagram.Params.Value['ATempFolder']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

{ TevRDController }

function TevRBController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevRBControllerDatagramDispatcher.Create;
end;

procedure TevRBController.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(RBController_Port));
  SetIPAddress('127.0.0.1');
end;

procedure TevRBControllerDatagramDispatcher.dmSendMessage(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SendMessage(ADatagram.Params.Value['AText'], ADatagram.Params.Value['AToUsers']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetPrintersInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetPrintersInfo);
end;

procedure TevRBControllerDatagramDispatcher.dmSetPrintersInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetPrintersInfo(IInterface(ADatagram.Params.Value['APrintersInfo']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetMaintenanceDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetMaintenanceDB);
end;

procedure TevRBControllerDatagramDispatcher.dmSetMaintenanceDB(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetMaintenanceDB(IInterface(ADatagram.Params.Value['ADBListByDomains']) as IisListOfValues,
    ADatagram.Params.Value['AMaintenance']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.AfterDispatchDatagram(
  const ADatagramIn, ADatagramOut: IevDatagram);
begin
  inherited;
  Mainboard.ContextManager.RestoreThreadContext;
end;

procedure TevRBControllerDatagramDispatcher.BeforeDispatchDatagram(
  const ADatagramHeader: IevDatagramHeader);
var
  Session: IevSession;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;

  Session := FindSessionByID(ADatagramHeader.SessionID);
  if Session.State = ssActive then
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
end;

procedure TevRBControllerDatagramDispatcher.GetSystemStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetSystemStatus);
end;

procedure TevRBControllerDatagramDispatcher.dmGetAUS(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetAUS);
end;

procedure TevRBControllerDatagramDispatcher.dmSetAUS(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetAUS(ADatagram.Params.Value['AValue']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetInternalRR(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetInternalRR);
end;

procedure TevRBControllerDatagramDispatcher.dmGetInternalUserRR(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT,
    RBControl.GetInternalUserRR(ADatagram.Params.Value['ADomainName']));
end;

procedure TevRBControllerDatagramDispatcher.dmSetInternalRR(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetInternalRR(IInterface(ADatagram.Params.Value['ARRList']) as IisStringList);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmSetInternalUserRR(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetInternalUserRR(ADatagram.Params.Value['ADomainName'], ADatagram.Params.Value['AValue']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetBBDiagnosticCheck(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, RBControl.GetBBDiagnosticCheck);
end;

procedure TevRBControllerDatagramDispatcher.dmGetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  RBControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.Params.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TevRBControllerDatagramDispatcher.dmSetLogFileThresholdInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetLogFileThresholdInfo(ADatagram.Params.Value['APurgeRecThreshold'],
    ADatagram.Params.Value['APurgeRecCount']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetStackInfo(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: String;  
begin
  Res := RBControl.GetStackInfo;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmSetKnownADRServices(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetKnownADRServices(IInterface(ADatagram.Params.Value['AServices']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetDatabasePasswords(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
  Res := RBControl.GetDatabasePasswords;
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevRBControllerDatagramDispatcher.dmSetDatabasePasswords(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetDatabasePasswords(IInterface(ADatagram.Params.Value['AParams']) as IisListOfValues);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmResetAdminPassword(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.ResetAdminPassword(ADatagram.Params.Value['ADomainName'],
                               ADatagram.Params.Value['APassword']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

procedure TevRBControllerDatagramDispatcher.dmGetAdminAccount(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  User: String;
begin
  RBControl.GetAdminAccount(ADatagram.Params.Value['ADomainName'], User);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue('AUser', User);
end;

procedure TevRBControllerDatagramDispatcher.dmSetAdminAccount(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
begin
  RBControl.SetAdminAccount(
    ADatagram.Params.Value['ADomainName'],
    ADatagram.Params.Value['AUser']);
  AResult := CreateResponseFor(ADatagram.Header);
end;

end.
