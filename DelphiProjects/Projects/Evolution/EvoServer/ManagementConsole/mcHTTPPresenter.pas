unit mcHttpPresenter;

interface

uses Windows, SysUtils, WinSvc, IniFiles, Classes, mcTypes, EvTypes, isBaseClasses, isObjectPool,
     EvConsts, Math, isBasicUtils, EvStreamUtils, Sockets, DateUtils, isLogFile, isErrorUtils,
     Variants, EvControllerInterfaces, EvTransportInterfaces, isThreadManager, isSocket, isSSL,
     evRemoteMethods, isSchedule, IsTypes, isExceptions, zlib, AbUtils, isSettings;

type
  ImcSession = interface
  ['{438FE0CF-2D29-4553-BEFA-8529E212C0E1}']
    function  SessionID: TisGUID;
    function  UserAccount: ImcUserAccount;
    procedure Authorize(const AUser, APassword: String);
    function  Authorized: Boolean;
    function  GetMainMenuPosition: String;
    procedure SetMainMenuPosition(const AValue: String);
    function  GetRefreshTime: Integer;
    procedure SetRefreshTime(const AValue: Integer);
    function  GetEvDomain: String;
    procedure SetEvDomain(const AValue: String);
    function  GetRequestID: TTaskID;
    function  GetRequestResult: String;
    function  GetRequestResultMimeType: String;
    procedure SetRequestResultMimeType(const Value: String);
    procedure SetRequestID(const AValue: TTaskID);
    procedure SetRequestResult(const AValue: String);
    property  RefreshTime: Integer read GetRefreshTime write SetRefreshTime;
    property  EvDomain: String read GetEvDomain write SetEvDomain;
    property  MainMenuPosition: String read GetMainMenuPosition write SetMainMenuPosition;
    property  RequestID: TTaskID read GetRequestID write SetRequestID;
    property  RequestResult: String read GetRequestResult write SetRequestResult;
    property  RequestResultMimeType: String read GetRequestResultMimeType write SetRequestResultMimeType;
  end;

  TmcHttpRequestHandler = function(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String of object;

  TmcHttpPresenter = class(TisInterfacedObject, ImcHttpPresenter)
  private
    FSessions: IisObjectPool;
    FHttpServer: TTcpServer;
    FHttpRedirect: TTcpServer;
    FRequestHandlers: TStringList;
    FRBDS: ImcDMControl;
    FEvVersionFolder: String;
    FUseSSL: Boolean;
    FSSLContext: Pointer;
    FEvDomainList: IisListOfValues;
    procedure StopTCPServers;
    procedure SyncTCPSettings;
    procedure RegisterRequestHandlers;
    procedure RegisterRequestHandler(const AMethod: String; const AHandler: TmcHttpRequestHandler);
    procedure DoOnHttpRequest(Sender: TObject; ClientSocket: TCustomIpClient);
    procedure DoOnHttpRedirect(Sender: TObject; ClientSocket: TCustomIpClient);
    function  RunHttpRequest(const aParams: IisListOfValues): String;
    function  RenderRequest(const aParams: IisListOfValues; out AMIMEType: String): String;
    procedure ExecRequest(const Params: TTaskParamList);
    function  GetResult(const ASession: ImcSession; out AMIMEType: String; const AWaitTime: Integer): String;
    function  GetWaitScreen(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    procedure ParseParams(const ASrc: string; const AParams: IisListOfValues);
    function  GetResource(const AResourceName: String; out AMIMEType: String): String;
    function  GetPageTemplate(const ASession: ImcSession; out AMIMEType: String; const ALockedMode: Boolean = False): String;
    function  WrapText(const AText: String; const ALineSize: Integer): String;
//    function  GetUnderConstruction(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    procedure SelectEvDomain(const AParams: IisListOfValues; out ADomain, ADomainDescr, AHTML, AMIMEType: String);
    function  GetDomainContextPage(const AResourceName, ADomain, ADomainDescr: String): String;
    function  AddRefresh(const ASession: ImcSession; const ASource, AMethod: String): String;
    function  AddEvDomainSelection(const ASession: ImcSession; const ASource, AMethod: String): String;

    function  ParallelExec(const AMethod: String; const AParams: IisListOfValues): IisParamsCollection;
    procedure CheckParallelExecError(const AParallelExecResult: IisParamsCollection);
    procedure FilterParallelExecResult(const AParallelExecResult: IisParamsCollection);

    function  GetRBDM(const AErrorIfNotFound: Boolean = False): ImcDMControl;
    function  GetRBControl(const AErrorIfNotFound: Boolean = False): IevRBControl;
    function  GetEvDomainList: IisListOfValues;
    function  ACInstalled: Boolean;
    function  GetFirstACHost: String;
    function  GetFirstASHost: String;

    procedure UpdateRPPoolInfo;
    procedure UpdateADRServicesInfo;

    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
//    procedure EnsureEvDomain(const ASession: ImcSession);

    // Server Methods
    function  rhLogin(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhLogout(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhAuthorization(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhMain(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhSetRefresh(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhSetEvDomain(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhCurrentActivity(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhServerMonitor(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhPSActivity(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhActiveUsers(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhAllLogs(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhLog(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhLog_Details(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhMaintenanceServices(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhServiceAction(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhAppVersion(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeAppVersion(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhDownloadAppVersion(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigMain(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigEvServers(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigDSServices(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhDeleteDSService(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhAddDSServices(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhDiscoverDSServices(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigDBPasswords(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeDBPasswords(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigDBLocations(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeDBLocations(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigRRUserConnections(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeRRUserConnections(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigSystemUser(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeSystemUser(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigLicense(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeLicense(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigEMailNotification(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeEMailNotification(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhTestEMail(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigRBFolders(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeRBFolders(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigRBPrinters(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeRBPrinters(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigTempFolders(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeTempFolders(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhMCUsers(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeMCUsers(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhDBMaintenance(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhManualDBMaintenance(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeScheduledDBMaintenance(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhUserMessage(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhResetAdmin(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeAdminPassword(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhServiceState(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhServiceStacks(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhSendUserMessage(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigRR(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeRR(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigAA(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeAA(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigAC(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeAC(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigAS(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeAS(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigASDB(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeASDB(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhRBStatus(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhRRStatus(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhConfigLogFileThreshold(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhChangeLogFileThreshold(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhACStatus(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhACDomainStatus(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhASStatus(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhASDomainStatus(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhRunACOperation(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
    function  rhRunASOperation(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;

    procedure SetActive(const AValue: Boolean);
    function  GetActive: Boolean;
    procedure SetUseSSL(const AValue: Boolean);
  end;

implementation

{$R *.RES}

uses mcManagementConsole, StrUtils;

const
  // HTML macros
  M_REFRESH       = '$REFRESH$';
  M_REFRESH_TIME  = '$REFRESH_TIME$';
  M_SET_REFRESH   = '$SET_REFRESH$';
  M_SET_EVDOMAIN  = '$SET_EVDOMAIN$';
  M_TEXT          = '$TEXT$';
  M_SESSION       = '$SESSION$';
  M_CONTENT       = '$CONTENT$';
  M_MAIN_MENU     = '$MAIN_MENU$';
  M_METHOD        = '$METHOD$';
  M_PARAM         = '$PARAM$';
  M_NAME          = '$NAME$';
  M_VALUE         = '$VALUE$';
  M_CAPTION       = '$CAPTION$';
  M_ATTRIBUTES    = '$ATTR$';
  M_LINK          = '$LINK$';
  M_REDIRECT      = '$REDIRECT$';
  M_STYLE_CLASS   = '$STYLE_CLASS$';
  M_HOST          = '$HOST$';
  M_EVDOMAIN      = '$EVDOMAIN$';

  M_DM_HOST       = '$DM_HOST$';
  M_RP_HOST       = '$RP_HOST$';
  M_RR_HOST       = '$RR_HOST$';
  M_AA_HOST       = '$AA_HOST$';
  M_AC_HOST       = '$AC_HOST$';
  M_AS_HOST       = '$AS_HOST$';
    
  // HTML params
  P_Session      = 'session';
  P_RequestID    = 'requestid';
  P_MainMenuItem = 'mmitem';
  P_Redirect     = 'redirect';
  P_Page         = 'page';
  P_Host         = 'host';
  P_Type         = 'type';
  P_ID           = 'id';
  P_Operation    = 'oper';
  P_EvDomain     = 'evdomain';
  P_WaitText     = 'waittext';

  // Main menu  {Path} ^{method} ^{security level},
  sMainMenuStructure = '|Monitoring ^current_activity ^1,' +
                       '|Monitoring|Current Activity ^current_activity ^1,' +
                       '|Monitoring|Active Users ^active_users ^1,' +
                       '|Monitoring|Server Monitor ^server_monitor ^1,' +
                       '|Monitoring|Server Monitor|$RP_HOST$ ^ps_activity&host=$RP_HOST$ ^1,' +

                       '|Monitoring|System Logs ^all_logs ^1,' +
                       '|Monitoring|System Logs|RB Log ^log&type=RB ^1,' +
                       '|Monitoring|System Logs|RP Logs ^all_logs&type=RP ^1,' +
                       '|Monitoring|System Logs|RP Logs|$RP_HOST$ ^log&type=RP&host=$RP_HOST$ ^1,' +
                       '|Monitoring|System Logs|RR Logs ^all_logs&type=RR ^1,' +
                       '|Monitoring|System Logs|RR Logs|$RR_HOST$ ^log&type=RR&host=$RR_HOST$ ^1,' +
                       '|Monitoring|System Logs|API Logs ^all_logs&type=AA ^1,' +
                       '|Monitoring|System Logs|API Logs|$AA_HOST$ ^log&type=AA&host=$AA_HOST$ ^1,' +
                       '|Monitoring|System Logs|AC Logs ^all_logs&type=AC ^1,' +
                       '|Monitoring|System Logs|AC Logs|$AC_HOST$ ^log&type=AC&host=$AC_HOST$ ^1,' +
                       '|Monitoring|System Logs|AS Logs ^all_logs&type=AS ^1,' +
                       '|Monitoring|System Logs|AS Logs|$AS_HOST$ ^log&type=AS&host=$AS_HOST$ ^1,' +
                       '|Monitoring|System Logs|DM Logs ^all_logs&type=DM ^1,' +
                       '|Monitoring|System Logs|DM Logs|$DM_HOST$ ^log&type=DM&host=$DM_HOST$ ^1,' +
                       '|Monitoring|System Logs|MC Log ^log&type=MC ^1,' +

                       '|Monitoring|RR Status ^rr_status ^1,' +
                       '|Monitoring|RB Status ^rb_status ^1,' +
                       '|Monitoring|AC Status ^ac_status ^1,' +
                       '|Monitoring|AC Status|$AC_HOST$ ^ac_status&host=$AC_HOST$ ^1,' +
                       '|Monitoring|AS Status ^as_status ^1,' +
                       '|Monitoring|AS Status|$AS_HOST$ ^as_status&host=$AS_HOST$ ^1,' +

                       '|Maintenance ^maintenance_services ^2,' +
                       '|Maintenance|Service Control ^maintenance_services ^2,' +
                       '|Maintenance|Version Update ^appversion ^3,' +
                       '|Maintenance|Maintenance Mode ^maintenance_mode ^2,' +
                       '|Maintenance|Service Debug ^maintenance_service_state ^2,' +
                       '|Maintenance|Send Message ^user_message ^2,' +
                       '|Maintenance|Reset ADMIN ^reset_admin ^3,' +

                       '|Configuration ^config_main ^3,' +
                       '|Configuration|Deployment Managers ^config_dsservices ^3,' +
                       '|Configuration|Evolution Services ^config_evservers ^3,' +
                       '|Configuration|Database Passwords ^config_dbpasswords^3,' +
                       '|Configuration|Database Access ^config_dblocations&type=prod^3,' +
                       '|Configuration|EMail Notification ^config_email ^3,' +
                       '|Configuration|Admin and System User ^config_sysuser ^3,' +
                       '|Configuration|RB Folders ^config_rbfolders ^3,' +
                       '|Configuration|Temp Folders ^config_tmpfolders ^3,' +
                       '|Configuration|Log Files ^config_log_files_threshold ^3,' +
                       '|Configuration|License ^config_license ^3,' +
                       '|Configuration|RR Ports ^config_rr ^3,' +
                       '|Configuration|RR User Connections ^config_rr_user_connections ^3,' +
                       '|Configuration|API Adapters ^config_aa ^3,' +
                       '|Configuration|RB Printers ^config_rbprinters ^3,' +
                       '|Configuration|AC Config ^config_ac ^3,' +
                       '|Configuration|AC Config|$AC_HOST$ ^config_ac&host=$AC_HOST$ ^3,' +
                       '|Configuration|AS Config ^config_as ^3,' +                       
                       '|Configuration|AS Config|$AS_HOST$ ^config_as&host=$AS_HOST$ ^3,' +

                       '|Configuration|MC Users ^mc_users ^3,' +

                       '|Logout ^logout ^0';


  // HTML fragments
  hfSpace = '&nbsp;';
  hfBR = '<br>';
  hfParagraph = '<p>$CONTENT$</p>';
  hfText = '<a>$CONTENT$</a>';
  hfAlertText='<a class="alertText">$CONTENT$</a>';
  hfTextLink = '<a href="$LINK$">$CONTENT$</a>';
  hfImage = '<img src="$CONTENT$">';
  hfLable = '<lable>$CONTENT$</lable>';
  hfPostButton = '<form action="$METHOD$" method="post"><input type="submit" style="text-align:center" $ATTR$ value="$CAPTION$"/></form>';
  hfGetButton = '<form action="$METHOD$" method="get"><input type="submit" $ATTR$ value="$CAPTION$"/></form>';
  hfSubmitButton = '<input style="text-align:center" name="$$" type="submit" $ATTR$ value="$CAPTION$"/>';
  hfEditBox = '<input name="$PARAM$" type="text" value="$VALUE$" $ATTR$/>';
  hfEditPassword = '<input name="$PARAM$" type="password" value="$VALUE$" $ATTR$/>';
  hfRadioButton = '<input name="$PARAM$" type="radio" id="$PARAM$" value="$VALUE$" $ATTR$/>';
  hfCheckBox = '<input name="$PARAM$" type="checkbox" id="$PARAM$" value="$VALUE$" $ATTR$/>';
  hfList = '<select name="$CAPTION$" id="$CAPTION$">$CONTENT$</select>';
  hfHiddenField='<input name="$PARAM$" type="hidden" value="$VALUE$"/>';
  hfOnClickConfirmation='OnClick="return confirm(''$TEXT$'')"';

  hfMainMenuItem = '<tr id="navigation"><td><a $METHOD$ class="navText">$CONTENT$</a></td></tr>';
  hfMainMenuSelectedItem = '<tr><td><a $METHOD$ class="navigation_sel">$CONTENT$</a></td></tr>';

  hfSimpleTable = '<table width="136" border="0" cellspacing="0"><tr>$CONTENT$</tr></table>';
  hfTableOutline = '<table width="136" border="1" cellspacing="0" bordercolor="#999999">$CONTENT$</table>';
  hfTableRow = '<tr>$CONTENT$</tr>';
  hfTableCell = '<td $ATTR$>$CONTENT$</td>';
  hfTableHeaderCell = '<td $ATTR$ class="tabHeader">$CONTENT$</td>';
  hfTableDetailCell = '<td $ATTR$ class="tabDetails">$CONTENT$</td>';
  hfPageIndex = '<table width="200" border="0" class="subHeader"><tr>$CONTENT$</tr></table>';
  hfPageRef = '<th scope="col"><a href="$LINK$">$CONTENT$</a></th>';
  hfOKPict = '<img src="ok_pict.gif" alt="Unknown" border="0">';
  hfCancelPict = '<img src="cancel_pict.gif" alt="Unknown" border="0">';

  hfRefresh ='<meta http-equiv="Refresh" content="$REFRESH_TIME$; URL=$REDIRECT$" />';


  LogEventNames: array [Low(TLogEventType)..High(TLogEventType)] of string =
                        ('Unknown', 'Information', 'Warning', 'Error', 'FatalError');

  cDummyPassword = '[dummy_pwd]';

  TableMaxRowsPerPage = 16;

type
  // HREF builder
  ImcHREF = interface
  ['{5B3A0CBA-FB83-467C-AB2C-93D21D84CBE0}']
    procedure AddParam(const AParamName, AValue: String);
    function  GetHtml: String;
  end;

  TmcHREF = class(TisInterfacedObject, ImcHREF)
  private
    Fhtml: String;
    procedure AddParam(const AParamName, AValue: String);
    function  GetHtml: String;
  public
    constructor Create(const AMethod: String; const ASession: ImcSession); reintroduce;
  end;


  // Main Menu
  ImcMainMenuBuilder = interface
  ['{4A392FD6-347A-4F21-8D7F-AEA93632BB04}']
    function  GetHtml(const ALockedMode: Boolean): String;
  end;

  TmcMainMenuBuilder = class(TisInterfacedObject, ImcMainMenuBuilder)
  private
    FStructure: IisParamsCollection;
    FSession: ImcSession;
    procedure AddItem(const APath, AOwner, AName, AMethod, ASecurityLevel: String);
    procedure AddDMItems(const AOwner, AMethod: String);
    procedure AddRPItems(const AOwner, AMethod: String);
    procedure AddRRItems(const AOwner, AMethod: String);
    procedure AddAAItems(const AOwner, AMethod: String);
    procedure AddACItems(const AOwner, AMethod: String);
    procedure AddASItems(const AOwner, AMethod: String);
    procedure BuildStructure;
    function  GetHtml(const ALockedMode: Boolean): String;
  public
    constructor Create(const ASession: ImcSession); reintroduce;
  end;


  // Unified Table
  ImcHtmlTableBulder = interface
  ['{06C3180A-F184-4477-B1F1-F40759A00012}']
    function  AddColumn(const ATitle, AParamName: String): IisListOfValues;
    function  GetHtml: String; overload;
    function  GetHtml(const ACurrentMethodParams: IisListOfValues; const ASession: ImcSession;
                      const APageCount: Integer; const APageClickMethod: String;
                      const AMethodParams: TisCommaDilimitedString;
                      const ATableTemplate: String = ''): String; overload;
  end;

  TmcHtmlTableBulder = class(TisInterfacedObject, ImcHtmlTableBulder)
  private
    FColumns: IisParamsCollection;
    FTableData: IisParamsCollection;
    FTableOutline: String;
    function  AddColumn(const ATitle, AParamName: String): IisListOfValues;
    function  GetHtml: String; overload;
    function  GetHtml(const ACurrentMethodParams: IisListOfValues; const ASession: ImcSession;
                      const APageCount: Integer; const APageClickMethod: String;
                      const AMethodParams: TisCommaDilimitedString;
                      const ATableTemplate: String = ''): String; overload;
  public
    constructor Create(const ATableData: IisParamsCollection); reintroduce;
  end;


  // HREF builder
  ImcHtmlButtonBuilder = interface
  ['{7B4DF394-19BE-42C7-928C-E74D85A3AF52}']
    procedure AddMethodParam(const AParamName, AValue: String);
    function  GetConfirmation: String;
    procedure SetConfirmation(const AValue: String);
    function  GetHtml: String;
    property  Confirmation: String read GetConfirmation write SetConfirmation;
  end;

  TmcHtmlButtonBuilder = class(TisInterfacedObject, ImcHtmlButtonBuilder)
  private
    FHREF: ImcHREF;
    FCaption: String;
    FConfirmation: String;
    FPostButton: Boolean;
    procedure AddMethodParam(const AParamName, AValue: String);
    function  GetConfirmation: String;
    procedure SetConfirmation(const AValue: String);
    function  GetHtml: String;
  public
    constructor Create(const AMethod: String; const ACaption: String; const APostButton: Boolean; const ASession: ImcSession); reintroduce;
  end;


  // Drop down
  ImcHtmlListBuilder = interface
  ['{420A4550-0F8F-4A07-84CB-078FFF7FA230}']
    procedure AddValue(const AName, AValue: String);
    procedure AddValues(const AText: String);
    procedure SelectItem(const AValue: String);
    function  GetHtml: String;
  end;

  TmcHtmlListBuilder = class(TisInterfacedObject, ImcHtmlListBuilder)
  private
    FValues: IisParamsCollection;
    FParamName: String;
    procedure AddValue(const AName, AValue: String);
    procedure AddValues(const AText: String);
    procedure SelectItem(const AValue: String);
    function  GetHtml: String;
  public
    constructor Create(const AParamName: String); reintroduce;
  end;

  // Edit Box
  ImcHtmlEditBoxBuilder = interface
  ['{F07BF7DD-0497-4DA8-89B4-D2ADBDE17D77}']
    function  GetSecured: Boolean;
    procedure SetSecured(const AValue: Boolean);
    function  GetSize: Integer;
    procedure SetSize(const AValue: Integer);
    function  GetHtml: String;
    property  Secured: Boolean read GetSecured write SetSecured;
    property  Size: Integer read GetSize write SetSize;
  end;

  TmcHtmlEditBoxBuilder = class(TisInterfacedObject, ImcHtmlEditBoxBuilder)
  private
    FParamName: String;
    FValue: String;
    FSecured: Boolean;
    FSize: Integer;
    function  GetSecured: Boolean;
    procedure SetSecured(const AValue: Boolean);
    function  GetSize: Integer;
    procedure SetSize(const AValue: Integer);
    function  GetHtml: String;
  public
    constructor Create(const AParamName: String; const AValue: String); reintroduce;
  end;


  // Check Box
  TmcLocation = (lLeft, lRight, lTop, lBottom);

  ImcHtmlCheckBoxBuilder = interface
  ['{420A4550-0F8F-4A07-84CB-078FFF7FA230}']
    procedure TryCheck(const AValue: String);
    function  Checked: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetEnabled: Boolean;
    function  GetCaptionLocation: TmcLocation;
    procedure SetCaptionLocation(const AValue: TmcLocation);
    function  GetHtml: String;
    property  Enabled: Boolean read GetEnabled write SetEnabled;
    property  CaptionLocation: TmcLocation read GetCaptionLocation write SetCaptionLocation;
  end;


  TmcHtmlCheckBoxBuilder = class(TisInterfacedObject, ImcHtmlCheckBoxBuilder)
  private
    FParamName: String;
    FCaption: String;
    FValue: String;
    FEnabled: Boolean;
    FChecked: Boolean;
    FCaptionLocation: TmcLocation;
    procedure TryCheck(const AValue: String);
    function  Checked: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function  GetEnabled: Boolean;
    function  GetHtml: String;
    function  GetCaptionLocation: TmcLocation;
    procedure SetCaptionLocation(const AValue: TmcLocation);
  public
    constructor Create(const AParamName, AValue, ACaption: String); reintroduce;
  end;



  // Web session

  TmcSession = class(TisObjectFromPool, ImcSession)
  private
    FUserAccount: ImcUserAccount;
    FRefreshTime: Integer;
    FEvDomain: String;
    FMainMenuPosition: String;
    FRequestID: TTaskID;
    FRequestResult: String;
    FRequestResultMimeType: String;
    function  SessionID: TisGUID;
    function  UserAccount: ImcUserAccount;
    function  GetMainMenuPosition: String;
    procedure SetMainMenuPosition(const AValue: String);
    function  GetRefreshTime: Integer;
    procedure SetRefreshTime(const AValue: Integer);
    function  GetEvDomain: String;
    procedure SetEvDomain(const AValue: String);
    procedure Authorize(const AUser, APassword: String);
    function  Authorized: Boolean;
    function  GetRequestID: TTaskID;
    function  GetRequestResult: String;
    procedure SetRequestID(const AValue: TTaskID);
    procedure SetRequestResult(const AValue: String);
    function  GetRequestResultMimeType: String;
    procedure SetRequestResultMimeType(const Value: String);
  public
    constructor Create(const AName: String); override;
  end;


function HttpWrap(const Content, MIMEType: String; const Cookie: String = ''): String;

  function CT: String;
  begin
    if Length(MIMEType) > 0 then
      Result := 'Content-Type: ' + MIMEType + #13#10
    else
      Result := '';  
  end;

  function GetExpiration: String;
  // Wdy, DD-Mon-YY HH:MM:SS GMT
  const
    wdays: array[1..7] of string = ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
    monthnames: array[1..12] of string = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  var
    wDay,
    wMonth,
    wYear: Word;
    ANow: TDatetime;
  begin
    ANow := Now + 365;
    DecodeDate(ANow, wYear, wMonth, wDay);
    Result := Format('%s, %d-%s-%d %s GMT',
                   [wdays[DayOfWeek(ANow)], wDay, monthnames[wMonth],
                    wYear, FormatDateTime('HH":"NN":"SS', ANow)]);
  end;

  function GetCookie: String;
  begin
    if Length(Cookie) > 0 then
      Result := 'Set-Cookie:'+Cookie + '; expires=' + GetExpiration + #13#10
    else
      Result := '';
  end;

begin
  Result := 'HTTP/1.0 200 OK' + #13#10 +
              'Connection: Keep-Alive' + #13#10 +
              CT+
              GetCookie +
              'Content-Length: '+ IntToStr(Length(Content)) + #13#10#13#10 +
              Content;
end;

function HttpErrorNotFound: String;
begin
  Result := 'HTTP/1.1 400 Forbidden'#13#10;
end;

function DecodeURL(const ASrc: string): string;
var
  i: integer;
  ESC: string[2];
  CharCode: integer;
begin
  Result := '';
  i := 1;
  while i <= Length(ASrc) do
  begin
    if ASrc[i] = '+' then
      Result := Result + ' '

    else if ASrc[i] = '%' then
    begin
      Inc(i);
      ESC := Copy(ASrc, i, 2);
      Inc(i, 1);
      try
        CharCode := StrToInt('$' + ESC);
        if (CharCode > 0) and (CharCode < 256) then
          Result := Result + Char(CharCode);
      except
      end;
    end
    
    else
      Result := Result + ASrc[i];
    Inc(i);
  end;
end;

function EncodeURL(const ASrc: string; const AOnlySpecialSymbols: Boolean): string;
var
  i: integer;
begin
  Result := '';
  for i := 1 to Length(ASrc) do
    if (ASrc[i] in ['A'..'Z']) or (ASrc[i] in ['a'..'z']) or (ASrc[i] in ['0'..'9']) then
      Result := Result + ASrc[i]    
    else
      Result := Result + '%' + IntToHex(Byte(ASrc[i]), 2);
end;

function ReplaceMacros(const AText, AMacroName, AValue: string): String;
begin
  Result := StringReplace(AText, AMacroName, AValue, [rfReplaceAll]);
end;

function CleanupMacros(const AText: string): String;
begin
  Result := ReplaceMacros(AText,  M_REFRESH, '');
  Result := ReplaceMacros(Result, M_SET_REFRESH, '');
  Result := ReplaceMacros(Result, M_SET_EVDOMAIN, '');  
  Result := ReplaceMacros(Result, M_SESSION, '');
  Result := ReplaceMacros(Result, M_TEXT, '');
  Result := ReplaceMacros(Result, M_CONTENT, '');
  Result := ReplaceMacros(Result, M_METHOD, '');
  Result := ReplaceMacros(Result, M_ATTRIBUTES, '');
  Result := ReplaceMacros(Result, M_LINK, '');
  Result := ReplaceMacros(Result, M_VALUE, '');
  Result := ReplaceMacros(Result, M_PARAM, '');
  Result := ReplaceMacros(Result, M_NAME, '');
  Result := ReplaceMacros(Result, M_HOST, '');
end;

function MakeHtmlText(const AText: String): String;
begin
  if (AText <> '') and (AText[1] <> '<') and (AText[Length(AText)] <> '>') then
  begin
    Result := StringReplace(AText, #13, hfBR, [rfReplaceAll]);
    Result := StringReplace(Result, ' ', hfSpace, [rfReplaceAll]);
  end
  else
    Result := AText;
end;

function Spaces(const ACount: Integer): String;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to ACount do
    Result := Result + hfSpace;
end;


{ TmcHttpPresenter }

procedure TmcHttpPresenter.DoOnHttpRequest(Sender: TObject; ClientSocket: TCustomIpClient);

var
  SSL: Pointer;

  function SSL_read_ln: String;
  var
    c: Char;
    err: Integer;
  begin
    Result := '';
    repeat
      err := SSL_read(SSL, @c, 1);
      if (err > 0) and not (c in [#13,#10]) then Result := Result + c;
    until not ((c <> #10) and (err > 0));
  end;

  function SocketReadLn: String;
  begin
    if FUseSSL then
      Result := SSL_read_ln
    else
      Result := ClientSocket.Receiveln;
  end;

  procedure SocketReadBuf(var buf; len: Integer);
  begin
    if FUseSSL then
      SSL_read(SSL, @buf, len)
    else
      ClientSocket.ReceiveBuf(buf, len);
  end;

  procedure SocketWriteLn(const Line: String);
  begin
    if FUseSSL then
    begin
      SSL_write(SSL, PChar(Line), Length(Line));
      SSL_write(SSL, #13#10, 2);
    end
    else
      ClientSocket.SendLn(Line);
  end;

  procedure SocketWrite(Line: String);
  begin
    if FUseSSL then
    begin
      SSL_write(SSL, PChar(Line), Length(Line));
    end
    else
      ClientSocket.SendBuf(Line[1], Length(Line));
  end;

var
  sCmd, sInputLine, s, sFld, sVal: String;
  iContentLength: Integer;
  Params: IisListOfValues;
  sURL: String;
  HttpHeader: IisListOfValues;
  err: Integer;
begin
  if FUseSSL then
  begin
    SSL := SSL_new(FSSLContext);
    SSL_set_fd(SSL, ClientSocket.Handle);
    err := SSL_accept(SSL);
    if err <= 0 then
      raise EisException.Create('SSL_accept error');
  end;

  repeat
    try
      sInputLine := SocketReadLn;
      if sInputLine = '' then
        break;

      sCmd := AnsiUpperCase(GetNextStrValue(sInputLine, ' '));        // read command
      sURL := GetNextStrValue(sInputLine, ' HTTP/');                  // read URL

      Params := TisListOfValues.Create;

      // read header fields
      HttpHeader := TisListOfValues.Create;
      while True do
      begin
        s := SocketReadLn;
        if s = '' then
          break;
        sFld := GetNextStrValue(s, ':');
        HttpHeader.AddValue(sFld, TrimLeft(s));
        if AnsiSameText(sFld, 'Host') then
        begin
          Params.AddValue('local_host_url', Trim(s));
        end;
        if AnsiSameText(sFld, 'Cookie') then
        begin
          sVal := TrimLeft(GetPrevStrValue(s,'='));
          sFld := TrimLeft(GetNextStrValue(s,'='));
          Params.AddValue(sFld, sVal);
        end;
      end;

      if AnsiSameText(sCmd, 'POST') then
      begin
        Params.AddValue(P_Redirect, HttpHeader.Value['Referer']);
        iContentLength := HttpHeader.Value['Content-Length'];
        if iContentLength > 0 then
        begin
          SetLength(sInputLine, iContentLength);
          SocketReadBuf(sInputLine[1], iContentLength);
        end;
        ParseParams(sInputLine, Params);
      end

      else if (sCmd <> 'GET') and (sCmd <> 'HEAD') and (sCmd <> 'PROPFIND') and (sCmd <> 'OPTIONS') then
        raise EisException.CreateFmt('Unknown command %s', [sCmd]);

      if AnsiSameText(sCmd, 'HEAD') then
        sCmd := HttpWrap('', 'text/html')
      else if AnsiSameText(sCmd, 'PROPFIND') then
        sCmd := HttpWrap('', 'text/html')
      else if AnsiSameText(sCmd, 'OPTIONS') then
        sCmd := HttpWrap('', 'text/html')
      else
      begin
        s := sURL;
        ParseParams(s, Params);
        Params.AddValue('URL', sURL);
        sCmd := RunHttpRequest(Params);
      end;
    except
      on E: Exception do
      begin
        sCmd := HttpWrap(MakeHtmlText(E.Message), 'text/html');
        GlobalLogFile.AddEvent(etError, 'HTTP Server', E.Message, 'Client: ' + ClientSocket.RemoteHost + #13#13 + BuildStackedErrorStr(E));
      end;
    end;

    SocketWrite(sCmd);
  until not ClientSocket.Connected;
  if FUseSSL then
    SSL_free(SSL);
end;

procedure TmcHttpPresenter.DoOnConstruction;
begin
  inherited;
  InitLock;
  FEvVersionFolder := AppDir + 'AppVersions\Evolution\';

  FSessions := TisObjectPool.Create('Web Sessions', TmcSession);
  FSessions.ObjectExpirationTime := 10; // mins
  FSessions.MaxPoolSize := 100;

  FRequestHandlers := TStringList.Create;
  FRequestHandlers.Duplicates := dupError;
  FRequestHandlers.Sorted := True;
  RegisterRequestHandlers;

  FHttpServer := TTcpServer.Create(nil);
  FHttpServer.OnAccept := DoOnHttpRequest;

  FHttpRedirect := TTcpServer.Create(nil);
  FHttpRedirect.OnAccept := DoOnHttpRedirect;

  FUseSSL := Mainboard.AppSettings.AsBoolean['Settings\UseSSL'];
  SyncTCPSettings;
end;

function TmcHttpPresenter.GetActive: Boolean;
begin
  Result := FHttpServer.Active;
end;

procedure TmcHttpPresenter.SetActive(const AValue: Boolean);
begin
  FHttpServer.Active := AValue;
end;

procedure TmcHttpPresenter.ParseParams(const ASrc: string; const AParams: IisListOfValues);
var
  s, sPar, sParamName: String;
begin
  s := ASrc;
  while s <> '' do
  begin
    sPar := GetNextStrValue(s, '&');
    if sPar <> '' then
    begin
      sParamName := GetNextStrValue(sPar, '=');
      sPar := DecodeURL(sPar);
      AParams.AddValue(sParamName, sPar);
    end;
  end;
end;

function TmcHttpPresenter.RunHttpRequest(const aParams: IisListOfValues): String;
var
  sMIMEType, Cookie: String;
begin
  Result := RenderRequest(aParams, sMIMEType);
  Result := CleanupMacros(Result);

  Cookie := aParams.TryGetValue('refresh_time', '');
  if Length(Cookie) > 0 then
    Cookie := 'refresh_time='+Cookie;

  Result := HttpWrap(Result, sMIMEType, Cookie);

{  if Result <> '' then
    Result := HttpWrap(Result, sMIMEType)
  else
    Result := HttpErrorNotFound;}
end;

function TmcHttpPresenter.GetResource(const AResourceName: String; out AMIMEType: String): String;
var
  S: IisStream;
  sFile: String;
  HR: HRSRC;
  SR: Cardinal;
  PR: Pointer;
begin
  Result := '';
  AMIMEType := ExtractFileExt(AResourceName);

  if AMIMEType <> '' then
  begin
    Delete(AMIMEType, 1, 1);
    // try to load from resources
    sFile := ChangeFileExt(ExtractFileName(AResourceName), '');
    HR := FindResource(HINSTANCE, PAnsiChar(sFile), PAnsiChar(UpperCase(AMIMEType)));
    if HR <> 0 then
    begin
      SR := SizeofResource(HINSTANCE, HR);
      if SR > 0 then
      begin
        PR := Pointer(LoadResource(HINSTANCE, HR));
        SetLength(Result, SR);
        CopyMemory(@Result[1], PR, SR);
      end;
    end;

    if Result = '' then
    begin
      // try to load from file
      sFile := AppDir + '\HTML\' + ExtractFileName(AResourceName);
      if FileExists(sFile) then
      begin
        S := TisStream.CreateFromFile(sFile);
        Result := S.GetAsString;
      end;
    end;

    if AnsiSameText(AMIMEType, 'html') then
      AMIMEType := 'text/html'
    else if AnsiSameText(AMIMEType, 'css') then
      AMIMEType := 'text/css'
    else if AnsiSameText(AMIMEType, 'gif') then
      AMIMEType := 'image/gif'
    else if AnsiSameText(AMIMEType, 'bmp') then
      AMIMEType := 'image/bmp'
    else if AnsiSameText(AMIMEType, 'jpg') then
      AMIMEType := 'image/jpeg'
    else if AnsiSameText(AMIMEType, 'ico') then
      AMIMEType := 'image/x-icon'
    else
      raise EisException.CreateFmt('Unknown file extension %s', [AMIMEType]);
  end;
end;

procedure TmcHttpPresenter.RegisterRequestHandlers;
begin
  RegisterRequestHandler('LOGIN', rhLogin);
  RegisterRequestHandler('LOGOUT', rhLogout);
  RegisterRequestHandler('AUTHORIZATION', rhAuthorization);
  RegisterRequestHandler('MAIN', rhMain);
  RegisterRequestHandler('SET_REFRESH', rhSetRefresh);
  RegisterRequestHandler('SET_EVDOMAIN', rhSetEvDomain);  
  RegisterRequestHandler('CURRENT_ACTIVITY', rhCurrentActivity);
  RegisterRequestHandler('SERVER_MONITOR', rhServerMonitor);
  RegisterRequestHandler('PS_ACTIVITY', rhPSActivity);
  RegisterRequestHandler('ACTIVE_USERS', rhActiveUsers);
  RegisterRequestHandler('ALL_LOGS', rhAllLogs);
  RegisterRequestHandler('LOG', rhLog);
  RegisterRequestHandler('LOG_DETAILS', rhLog_Details);
  RegisterRequestHandler('MAINTENANCE_SERVICES', rhMaintenanceServices);
  RegisterRequestHandler('SERVICE_ACTION', rhServiceAction);
  RegisterRequestHandler('APPVERSION', rhAppVersion);
  RegisterRequestHandler('DOWNLOAD_APPVERSION', rhDownloadAppVersion);
  RegisterRequestHandler('CHANGE_APPVERSION', rhChangeAppVersion);
  RegisterRequestHandler('CONFIG_MAIN', rhConfigMain);
  RegisterRequestHandler('CONFIG_EVSERVERS', rhConfigEvServers);
  RegisterRequestHandler('CONFIG_DSSERVICES', rhConfigDSServices);
  RegisterRequestHandler('DELETE_DSSERVICE', rhDeleteDSService);
  RegisterRequestHandler('ADD_DSSERVICES', rhAddDSServices);
  RegisterRequestHandler('DISCOVER_DSSERVICES', rhDiscoverDSServices);
  RegisterRequestHandler('CONFIG_DBPASSWORDS', rhConfigDBPasswords);
  RegisterRequestHandler('CHANGE_DBPASSWORDS', rhChangeDBPasswords);  
  RegisterRequestHandler('CONFIG_DBLOCATIONS', rhConfigDBLocations);
  RegisterRequestHandler('CHANGE_DBLOCATIONS', rhChangeDBLocations);
  RegisterRequestHandler('CONFIG_SYSUSER', rhConfigSystemUser);
  RegisterRequestHandler('CHANGE_SYSUSER', rhChangeSystemUser);
  RegisterRequestHandler('CONFIG_LICENSE', rhConfigLicense);
  RegisterRequestHandler('CHANGE_LICENSE', rhChangeLicense);
  RegisterRequestHandler('CONFIG_EMAIL', rhConfigEMailNotification);
  RegisterRequestHandler('CHANGE_EMAIL', rhChangeEMailNotification);
  RegisterRequestHandler('TEST_EMAIL', rhTestEMail);
  RegisterRequestHandler('CONFIG_RBFOLDERS', rhConfigRBFolders);
  RegisterRequestHandler('CHANGE_RBFOLDERS', rhChangeRBFolders);
  RegisterRequestHandler('CONFIG_RBPRINTERS', rhConfigRBPrinters);
  RegisterRequestHandler('CHANGE_RBPRINTERS', rhChangeRBPrinters);
  RegisterRequestHandler('CONFIG_TMPFOLDERS', rhConfigTempFolders);
  RegisterRequestHandler('CHANGE_TMPFOLDERS', rhChangeTempFolders);
  RegisterRequestHandler('MC_USERS', rhMCUsers);
  RegisterRequestHandler('CHANGE_MC_USERS', rhChangeMCUsers);
  RegisterRequestHandler('MAINTENANCE_MODE', rhDBMaintenance);
  RegisterRequestHandler('MANUAL_DBMAINTENANCE', rhManualDBMaintenance);
  RegisterRequestHandler('CHANGE_SCHEDULED_DBMAINTENANCE', rhChangeScheduledDBMaintenance);
  RegisterRequestHandler('USER_MESSAGE', rhUserMessage);
  RegisterRequestHandler('RESET_ADMIN', rhResetAdmin);
  RegisterRequestHandler('CHANGE_ADMIN_PASSWORD', rhChangeAdminPassword);
  RegisterRequestHandler('MAINTENANCE_SERVICE_STATE', rhServiceState);
  RegisterRequestHandler('SERVICE_STACK', rhServiceStacks);
  RegisterRequestHandler('SEND_USER_MESSAGE', rhSendUserMessage);
  RegisterRequestHandler('CONFIG_RR', rhConfigRR);
  RegisterRequestHandler('CHANGE_RR', rhChangeRR);
  RegisterRequestHandler('CONFIG_AA', rhConfigAA);
  RegisterRequestHandler('CHANGE_AA', rhChangeAA);
  RegisterRequestHandler('CONFIG_AC', rhConfigAC);
  RegisterRequestHandler('CHANGE_AC', rhChangeAC);
  RegisterRequestHandler('CONFIG_AS', rhConfigAS);
  RegisterRequestHandler('CHANGE_AS', rhChangeAS);
  RegisterRequestHandler('CONFIG_ASDB', rhConfigASDB);
  RegisterRequestHandler('CHANGE_ASDB', rhChangeASDB);
  RegisterRequestHandler('RB_STATUS', rhRBStatus);
  RegisterRequestHandler('RR_STATUS', rhRRStatus);
  RegisterRequestHandler('AC_STATUS', rhACStatus);
  RegisterRequestHandler('AC_DOMAIN_STATUS', rhACDomainStatus);  
  RegisterRequestHandler('AS_STATUS', rhASStatus);
  RegisterRequestHandler('AS_DOMAIN_STATUS', rhASDomainStatus);
  RegisterRequestHandler('CONFIG_RR_USER_CONNECTIONS', rhConfigRRUserConnections);
  RegisterRequestHandler('CHANGE_RR_USER_CONNECTIONS', rhChangeRRUserConnections);
  RegisterRequestHandler('CONFIG_LOG_FILES_THRESHOLD', rhConfigLogFileThreshold);
  RegisterRequestHandler('CHANGE_LOG_FILES_THRESHOLD', rhChangeLogFileThreshold);
  RegisterRequestHandler('RUN_AC_OPERATION', rhRunACOperation);
  RegisterRequestHandler('RUN_AS_OPERATION', rhRunASOperation);  
end;

procedure TmcHttpPresenter.RegisterRequestHandler(const AMethod: String; const AHandler: TmcHttpRequestHandler);
begin
  FRequestHandlers.AddObject(AnsiUpperCase(AMethod), @AHandler);
end;

function TmcHttpPresenter.GetPageTemplate(const ASession: ImcSession; out AMIMEType: String; const ALockedMode: Boolean): String;
var
  sPageTemplate: String;
  MainMenuBuilder: ImcMainMenuBuilder;
begin
  sPageTemplate := GetResource('page_style.html', AMIMEType);
  sPageTemplate := ReplaceMacros(sPageTemplate, '$VERSION$', Format('v%s (%s)', [IntToStr(EvMajorVerNbr), EvVerName]));
  sPageTemplate := ReplaceMacros(sPageTemplate, '$MC_VERSION$', 'MC version:' + hfSpace + hfSpace + AppVersion);
  sPageTemplate := ReplaceMacros(sPageTemplate, '$COPYRIGHTYEAR$', IntToStr(YearOf(Now)));  
  MainMenuBuilder := TmcMainMenuBuilder.Create(ASession);
  Result := ReplaceMacros(sPageTemplate, M_MAIN_MENU, MainMenuBuilder.GetHTML(ALockedMode));
end;

function TmcHttpPresenter.rhMain(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RBDM: ImcDMControl;
  DBMStatus: IisListOfValues;
  sPage: String;
  TableBuilder: ImcHtmlTableBulder;
  DBMExecItems: IisParamsCollection;
  inProgress: Boolean;
begin
  inProgress := False;

  RBDM := GetRBDM();
  if Assigned(RBDM) then
  begin
    try
      DBMStatus := RBDM.EvolutionControl.DBMControl.GetStatus;
      DBMExecItems := RBDM.EvolutionControl.DBMControl.GetExecutingItems;
      if DBMExecItems.Count > 0 then
      begin
        sPage := GetResource('dbm_activity.html', AMIMEType);
        sPage := ReplaceMacros(sPage, '$TOTAL_PROGRESS$', String(DBMStatus.Value['TotalProgress']) + '%');
        sPage := ReplaceMacros(sPage, '$ELAPSED_TIME$', 'Elapsed Time:  ' + PeriodToString(DBMStatus.Value['ElapsedTimeSec'] * 1000));
{
        if DBMStatus.Value['EstimatedTimeLeftSec'] = 0 then
          sPage := ReplaceMacros(sPage, '$ETA$', '')
        else
          sPage := ReplaceMacros(sPage, '$ETA$', 'ETA:  ' + PeriodToString(DBMStatus.Value['EstimatedTimeLeftSec'] * 1000, 'm'));
}
        sPage := ReplaceMacros(sPage, '$ETA$', '');

        sPage := ReplaceMacros(sPage, '$QUEUED$', DBMStatus.Value['Queued']);
        sPage := ReplaceMacros(sPage, '$EXECUTING$', DBMStatus.Value['Executing']);
        sPage := ReplaceMacros(sPage, '$COMPLETED$', DBMStatus.Value['Finished']);
        sPage := ReplaceMacros(sPage, '$SKIPPED$', DBMStatus.Value['Skipped']);        
        sPage := ReplaceMacros(sPage, '$ERRORED$', DBMStatus.Value['Errored']);

        TableBuilder := TmcHtmlTableBulder.Create(DBMExecItems);
        TableBuilder.AddColumn('Database', 'DBName');
        TableBuilder.AddColumn('Host', 'DBHost');
        TableBuilder.AddColumn('Status', 'Status');
        sPage := ReplaceMacros(sPage, '$ACTIVIE_ITEMS$', TableBuilder.GetHtml);

        inProgress := True;
      end;
    except
    end;
  end;

  if not inProgress then
    sPage := #13#10'<p align="center" class="pageName">Welcome to Evolution administration service!</p></td>';

  Result := GetPageTemplate(ASession, AMIMEType, inProgress);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhServerMonitor(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  TableBuilder: ImcHtmlTableBulder;
  i: Integer;
  sPage, sScore, sLink_Activities, sVal1, sVal2: String;
  RPPoolStatus, PSStatus: IisParamsCollection;
  PS: IisListOfValues;
  HREF: ImcHREF;
  RB: IevRBControl;
begin
  PSStatus := ParallelExec('RP.GetLoadStatus', nil);

  // Prepare table data
  RB := GetRBControl;
  if Assigned(RB) then
  begin
    RPPoolStatus := RB.GetRPPoolStatus;
    RPPoolStatus.Sort;
  end
  else
    RPPoolStatus := TisParamsCollection.Create;

  for i := 0 to RPPoolStatus.Count - 1 do
  begin
    if RPPoolStatus[i].ValueExists('CurrentScore') then
      sScore := RPPoolStatus[i].Value['CurrentScore'] + '/' + RPPoolStatus[i].Value['MaxScore']
    else
      sScore := '';

    RPPoolStatus[i].AddValue('Score', sScore);
    RPPoolStatus[i].AddValue('Server', RPPoolStatus.ParamName(i));

    PS := PSStatus.ParamsByName(RPPoolStatus[i].Value['Server']);
    if Assigned(PS) and not PS.ValueExists('Error') then
    begin
      PS := IInterface(PS.Value[METHOD_RESULT]) as IisListOfValues;
      RPPoolStatus[i].AddValue('CPUTotal', VarToStr(PS.Value['CPUTotal']) + '%');
      RPPoolStatus[i].AddValue('ProcessCPU', VarToStr(PS.Value['ProcessCPUTotal']) + '%');
      RPPoolStatus[i].AddValue('MemoryLoad', VarToStr(PS.Value['MemoryLoad']) + '%');

      sVal1 := VarToStr(PS.Value['MemoryProcessLoad']);
      if StrToFloatDef(sVal1, 0) > 70 then
        sVal1 := '<font color="red">' + sVal1 + '%</font>'
      else
        sVal1 := sVal1 + '%';
      RPPoolStatus[i].AddValue('MemoryProcessLoad', sVal1);

      sVal1 := VarToStr(PS.Value['TransReceivingQueueSize']);
      sVal2 := VarToStr(PS.Value['TransSendingQueueSize']);
      sVal1 := sVal1 + '/' + sVal2;
      if (StrToFloatDef(sVal1, 0) > 2) or (StrToFloatDef(sVal2, 0) > 2) then
        sVal1 := '<font color="red">' + sVal1 + '%</font>';
      RPPoolStatus[i].AddValue('TransQueueSize', sVal1);

      HREF := TmcHREF.Create('ps_activity', ASession);
      HREF.AddParam(P_Host, RPPoolStatus[i].Value['Server']);
      HREF.AddParam(P_MainMenuItem, '|Monitoring|Server Monitor|' + RPPoolStatus[i].Value['Server']);

      sLink_Activities := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
      sLink_Activities := ReplaceMacros(sLink_Activities, M_CONTENT, 'Show');

      RPPoolStatus[i].AddValue('Activities', sLink_Activities);
    end;
    if RPPoolStatus[i].TryGetValue('Connected', '') = 'Y' then
      RPPoolStatus[i].AddValue('ConnectedPict', '<table width=100%><tr><td align="center">' + hfOKPict + '</td></tr></table>')
    else
      RPPoolStatus[i].AddValue('ConnectedPict', '<table width=100%><tr><td align="center">' + hfCancelPict + '</td></tr></table>');
  end;

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(RPPoolStatus);
  TableBuilder.AddColumn('Server', 'Server');
  TableBuilder.AddColumn('Connected', 'ConnectedPict');
  TableBuilder.AddColumn('Score', 'Score');
  TableBuilder.AddColumn('Requests', 'Requests');
  TableBuilder.AddColumn('CPU<br>Total Load', 'CPUTotal');
  TableBuilder.AddColumn('CPU<br>Service Load', 'ProcessCPU');
  TableBuilder.AddColumn('Memory<br>Total Load', 'MemoryLoad');
  TableBuilder.AddColumn('Memory<br>Service Load', 'MemoryProcessLoad');
  TableBuilder.AddColumn('Trans Queue<br>In/Out', 'TransQueueSize');
  TableBuilder.AddColumn('Activity', 'Activities');

  sPage := GetResource('server_monitor.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.GetRBControl(const AErrorIfNotFound: Boolean = False): IevRBControl;
var
  DS: ImcDMControl;
begin
  DS := GetRBDM(AErrorIfNotFound);
  if Assigned(DS) then
    Result := DS.EvolutionControl.GetRBControl
  else
    Result := nil;
end;

function TmcHttpPresenter.GetRBDM(const AErrorIfNotFound: Boolean = False): ImcDMControl;

  procedure FindRD;
  var
    i: Integer;
    Res: IisParamsCollection;
  begin
    FRBDS := nil;
    Res := ParallelExec('RB.Started', nil);
    for i := 0 to Res.Count - 1 do
      if not Res[i].ValueExists('Error') then
        if Res[i].Value[METHOD_RESULT] then
        begin
          FRBDS := Mainboard.DeploymentManagers.FindDeploymentManager(Res.ParamName(i));
          break;
        end;
  end;

begin
  Lock;
  try
    if Assigned(FRBDS) and ((FRBDS as IisInterfacedObject).GetOwner <> nil)  then
    begin
      if not FRBDS.EvolutionControl.GetRBStarted then
        FindRD;
    end
    else
      FindRD;

    Result := FRBDS;
  finally
    Unlock;
  end;

  if not Assigned(Result) and AErrorIfNotFound then
    raise EisException.Create('Request Broker is not found');
end;

function TmcHttpPresenter.rhActiveUsers(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  sPage, s: String;
  TableBuilder: ImcHtmlTableBulder;
  TableDetails: IisParamsCollection;
  i: Integer;
begin
  // Prepare table data
  RB := GetRBControl;

  if Assigned(RB) then
  begin
    TableDetails := RB.UsersPoolStatus;
    for i := 0 to TableDetails.Count - 1 do
      (TableDetails[i] as IisCollection).Name := FormatDateTime('yymmddhhnnss', TableDetails[i].Value['LastActivity']) + TableDetails[i].Value['Login'] + ' ' + (TableDetails[i] as IisCollection).Name;
    TableDetails.Sort('desc');
  end
  else
    TableDetails := TisParamsCollection.Create;

  for i := 0 to TableDetails.Count - 1 do
  begin
    if TableDetails[i].Value['AppType'] = 'API' then
      s := 'EvoAPI'
    else
    begin
      s := TableDetails[i].Value['Login'];
      s := StringReplace(s, '@' + sDefaultDomain, '', [rfReplaceAll]);
    end;
    TableDetails[i].Value['Login'] := s;
  end;

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('User ID', 'Login');
  TableBuilder.AddColumn('Connections', 'Connections');
  TableBuilder.AddColumn('Remote Address', 'Address');
  TableBuilder.AddColumn('Session State', 'SessionState');
  TableBuilder.AddColumn('App Type', 'AppType');
  TableBuilder.AddColumn('Requests', 'ExecutingRequests');
  TableBuilder.AddColumn('Last Activity', 'LastActivity');  

  sPage := GetResource('active_users.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.rhLog(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  s, sType, sLink_Details, sHost, sTitle, sPage: String;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  i, iPages, iPageNumber: Integer;
  DS: ImcDMControl;
  HREF: ImcHREF;
  EventType: TLogEventType;
begin
  // collect log data
  sType := AParams.Value[P_Type];
  iPageNumber := StrToIntDef(AParams.TryGetValue(P_Page, '1'),1);

  if AParams.ValueExists(P_Host) then
  begin
    sHost := AParams.Value[P_Host];
    DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost)
  end
  else if Mainboard.DeploymentManagers.Count > 0 then
  begin
    DS := Mainboard.DeploymentManagers[0];
    sHost := DS.Host;
  end
  else
  begin
    DS := nil;
    sHost := '';
  end;

  sTitle := '';

  if AnsiSameText(sType, 'RB') then
  begin
    DS := GetRBDM(True);
    sHost := DS.Host;
    TableDetails := DS.EvolutionControl.RBControl.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'Request Broker Log';
  end

  else if AnsiSameText(sType, 'RP') and Assigned(DS) then
  begin
    TableDetails := DS.EvolutionControl.RPControl.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'Request Processor Log';
  end

  else if AnsiSameText(sType, 'AA') and Assigned(DS) then
  begin
    TableDetails := DS.EvolutionControl.AAControl.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'API Adapter Log';
  end

  else if AnsiSameText(sType, 'AC') and Assigned(DS) then
  begin
    TableDetails := DS.EvolutionControl.ACControl.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'ADR Client Log';
  end

  else if AnsiSameText(sType, 'AS') and Assigned(DS) then
  begin
    TableDetails := DS.EvolutionControl.ASControl.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'ADR Server Log';
  end

  else if AnsiSameText(sType, 'RR') and Assigned(DS) then
  begin
    TableDetails := DS.EvolutionControl.RRControl.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'Remote Relay Log';
  end

  else if AnsiSameText(sType, 'DM') and Assigned(DS) then
  begin
    TableDetails := DS.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'Deployment Manager Log';
  end

  else if AnsiSameText(sType, 'MC') then
  begin
    TableDetails := Mainboard.GetLogEvents(iPageNumber, TableMaxRowsPerPage, iPages);
    sTitle := 'Management Console Log';
  end;

  if sTitle <> '' then
  begin
    // convert event type
    for i := 0 to TableDetails.Count - 1 do
    begin
      EventType := TLogEventType(TableDetails[i].Value['EventType']);
      s := LogEventNames[EventType];
      if EventType >= etFatalError then
        s := ReplaceMacros(hfAlertText, M_CONTENT, s);

      TableDetails[i].Value['EventType'] := s;

      s := TableDetails[i].Value['Text'];
      if Length(s) > 60 then
        s := Copy(s, 1, 60) + ' ...';
      TableDetails[i].Value['Text'] := s;
    end;

    // Add Details links
    for i := 0 to TableDetails.Count - 1 do
    begin
      HREF := TmcHREF.Create('log_details', ASession);
      HREF.AddParam(P_Type, sType);
      if sHost <> '' then
        HREF.AddParam(P_Host, sHost);
      HREF.AddParam(P_ID, TableDetails[i].Value['EventNbr']);
      sLink_Details := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
      sLink_Details := ReplaceMacros(sLink_Details, M_CONTENT, 'Details');
      TableDetails[i].AddValue('Link_Details', sLink_Details)
    end;

    // Build table
    TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
    TableBuilder.AddColumn('Time', 'TimeStamp');
    TableBuilder.AddColumn('Event Type', 'EventType');
    TableBuilder.AddColumn('Module', 'EventClass');
    TableBuilder.AddColumn('Message', 'Text');
    TableBuilder.AddColumn('Details', 'Link_Details');

    sPage := GetResource('log.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CAPTION, sTitle);
    sPage := ReplaceMacros(sPage, M_HOST, 'Host: ' + sHost);
    sPage := ReplaceMacros(sPage, M_CONTENT,
      TableBuilder.GetHtml(AParams, ASession, iPages, 'log', P_Type + ',' + P_Host, '<table width="900" border="1" cellspacing="0" bordercolor="#999999">$CONTENT$</table>'));
  end
  else
    sPage := '';

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhLog_Details(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sType, sDetails, sPage, sTitle: String;
  EventDetails: IisListOfValues;
  sID: Integer;
  DS: ImcDMControl;
  RB: IevRBControl;
begin
  // collecr log data
  sType := AParams.Value[P_Type];
  sID := AParams.Value[P_ID];

  if AParams.ValueExists(P_Host) then
    DS := Mainboard.DeploymentManagers.FindDeploymentManager(AParams.Value[P_Host])
  else if Mainboard.DeploymentManagers.Count > 0 then
    DS := Mainboard.DeploymentManagers[0]
  else
    DS := nil;

  if AnsiSameText(sType, 'RB') then
  begin
    RB := GetRBControl;
    if Assigned(RB) then
      EventDetails := RB.GetLogEventDetails(sID)
    else
      EventDetails := TisListOfValues.Create;

    sTitle := 'Request Broker Log Event Details';
  end

  else if AnsiSameText(sType, 'RP') and Assigned(DS) then
  begin
    EventDetails := DS.EvolutionControl.RPControl.GetLogEventDetails(sID);
    sTitle := 'Request Processor Log Event Details';
  end

  else if AnsiSameText(sType, 'RR') and Assigned(DS) then
  begin
    EventDetails := DS.EvolutionControl.RRControl.GetLogEventDetails(sID);
    sTitle := 'Remote Relay Log Event Details';
  end

  else if AnsiSameText(sType, 'AA') and Assigned(DS) then
  begin
    EventDetails := DS.EvolutionControl.AAControl.GetLogEventDetails(sID);
    sTitle := 'API Log Event Details';
  end

  else if AnsiSameText(sType, 'AC') and Assigned(DS) then
  begin
    EventDetails := DS.EvolutionControl.ACControl.GetLogEventDetails(sID);
    sTitle := 'ADR Client Log Event Details';
  end

  else if AnsiSameText(sType, 'AS') and Assigned(DS) then
  begin
    EventDetails := DS.EvolutionControl.ASControl.GetLogEventDetails(sID);
    sTitle := 'ADR Server Log Event Details';
  end

  else if AnsiSameText(sType, 'DM') and Assigned(DS) then
  begin
    EventDetails := DS.GetLogEventDetails(sID);
    sTitle := 'Deployment Server Log Event Details';
  end

  else if AnsiSameText(sType, 'MC') then
  begin
    EventDetails := Mainboard.GetLogEventDetails(sID);
    sTitle := 'Management Console Log Event Details';
  end

  else
    EventDetails := nil;

  if Assigned(EventDetails) then
    sDetails := EventDetails.Value['Text'] + hfBR + hfBR + EventDetails.Value['Details']
  else
    sDetails := '';

  sPage := GetResource('log_details.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CAPTION, sTitle);
  if Assigned(DS) then
    sPage := ReplaceMacros(sPage, M_HOST, 'Host: ' + DS.Host)
  else
    sPage := ReplaceMacros(sPage, M_HOST, '');  
  sPage := ReplaceMacros(sPage, M_CONTENT, MakeHtmlText(sDetails));

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhMaintenanceServices(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  InstalledRB, StartedRD, InstalledPS,
  StartedPS, InstalledRR, StartedRR,
  InstalledAA, StartedAA, InstalledAC, StartedAC,
  InstalledAS, StartedAS: IisParamsCollection;

  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage: String;

  procedure AddServiceInfo(const aServiceName, aServiceType: String; const InstalledSrv, StartedSrv: IisParamsCollection);
  var
    i: Integer;
    LV, LVStartedSrvc: IisListOfValues;
    bStarted: Boolean;
    sActionLink1, sActionLink2, sOnClick: String;
    HREF: ImcHREF;
  begin
    for i := 0 to InstalledSrv.Count - 1 do
      if InstalledSrv[i].ValueExists('Error') then
      begin
        LV := TableDetails.AddParams(aServiceType + ' on ' + InstalledSrv.ParamName(i));
        LV.AddValue('ServiceName', aServiceName);
        LV.AddValue('Host', InstalledSrv.ParamName(i));
        LV.AddValue('Status', '<table width=100%><tr><td align="center">' + hfCancelPict + '</td></tr></table>');
      end

      else if InstalledSrv[i].Value[METHOD_RESULT] then
      begin
        LV := TableDetails.AddParams(aServiceType + ' on ' + InstalledSrv.ParamName(i));
        LV.AddValue('ServiceName', aServiceName);
        LV.AddValue('Host', InstalledSrv.ParamName(i));

        LVStartedSrvc := StartedSrv.ParamsByName(InstalledSrv.ParamName(i));
        if Assigned(LVStartedSrvc) then
          bStarted := LVStartedSrvc.Value[METHOD_RESULT]
        else
          bStarted := False;

        if bStarted then
          LV.AddValue('Status', '<table><tr><td align="center">' + hfOKPict + '</td></tr></table>')
        else
          LV.AddValue('Status', '<table><tr><td align="center">' + hfCancelPict + '</td></tr></table>');

        if bStarted then
        begin
          HREF := TmcHREF.Create('service_action', ASession);
          HREF.AddParam(P_Host, LV.Value['Host']);
          HREF.AddParam(P_Operation, 'stop');
          HREF.AddParam(P_Type, aServiceType);
          HREF.AddParam(P_WaitText, Format('Stopping %s service on %s...', [aServiceType, LV.Value['Host']]));

          sActionLink1 := ReplaceMacros(hfPostButton, M_METHOD, HREF.GetHtml);
          sOnClick := ReplaceMacros(hfOnClickConfirmation, M_TEXT,
             Format('Please confirm %s service %s on %s', [aServiceName, 'STOP', LV.Value['Host']]));
          sActionLink1 := ReplaceMacros(sActionLink1, M_ATTRIBUTES, sOnClick );
          sActionLink1 := ReplaceMacros(sActionLink1, M_CAPTION, ' Stop  ');

          HREF := TmcHREF.Create('service_action', ASession);
          HREF.AddParam(P_Host, LV.Value['Host']);
          HREF.AddParam(P_Operation, 'restart');
          HREF.AddParam(P_Type, aServiceType);
          HREF.AddParam(P_WaitText, Format('Restarting %s service on %s...', [aServiceType, LV.Value['Host']]));
          sActionLink2 := ReplaceMacros(hfPostButton, M_METHOD, HREF.GetHtml);
          sOnClick := ReplaceMacros(hfOnClickConfirmation, M_TEXT,
             Format('Please confirm %s service %s on %s', [aServiceName, 'RESTART', LV.Value['Host']]));
          sActionLink2 := ReplaceMacros(sActionLink2, M_ATTRIBUTES, sOnClick );
          sActionLink2 := ReplaceMacros(sActionLink2, M_CAPTION, 'Restart');

          sActionLink1 := ReplaceMacros(hfTableCell, M_CONTENT, sActionLink1);
          sActionLink2 := ReplaceMacros(hfTableCell, M_CONTENT, sActionLink2);
          sActionLink1 := ReplaceMacros(hfSimpleTable, M_CONTENT, sActionLink1 + sActionLink2);
        end
        else
        begin
          HREF := TmcHREF.Create('service_action', ASession);
          HREF.AddParam(P_Host, LV.Value['Host']);
          HREF.AddParam(P_Operation, 'start');
          HREF.AddParam(P_Type, aServiceType);
          HREF.AddParam(P_WaitText, Format('Starting %s service on %s...', [aServiceType, LV.Value['Host']]));
          sActionLink1 := ReplaceMacros(hfPostButton, M_METHOD, HREF.GetHtml);
          sOnClick := ReplaceMacros(hfOnClickConfirmation, M_TEXT,
             Format('Please confirm %s service %s on %s', [aServiceName, 'START', LV.Value['Host']]));
          sActionLink1 := ReplaceMacros(sActionLink1, M_ATTRIBUTES, sOnClick );
          sActionLink1 := ReplaceMacros(sActionLink1, M_CAPTION, ' Start ');
          sActionLink1 := ReplaceMacros(hfTableCell, M_CONTENT, sActionLink1);
          sActionLink1 := ReplaceMacros(hfSimpleTable, M_CONTENT, sActionLink1);
        end;

        LV.AddValue('Action', sActionLink1);
      end;
  end;

begin
  // collect info from DSs
  InstalledRB := ParallelExec('RB.Installed', nil);
  StartedRD := ParallelExec('RB.Started', nil);

  InstalledPS := ParallelExec('RP.Installed', nil);
  StartedPS := ParallelExec('RP.Started', nil);

  InstalledRR := ParallelExec('RR.Installed', nil);
  StartedRR := ParallelExec('RR.Started', nil);

  InstalledAA := ParallelExec('AA.Installed', nil);
  StartedAA := ParallelExec('AA.Started', nil);

  InstalledAC := ParallelExec('AC.Installed', nil);
  StartedAC := ParallelExec('AC.Started', nil);

  InstalledAS := ParallelExec('AS.Installed', nil);
  StartedAS := ParallelExec('AS.Started', nil);

  TableDetails := TisParamsCollection.Create;
  AddServiceInfo('Request Broker', 'RB', InstalledRB, StartedRD);
  AddServiceInfo('Request Processor', 'RP', InstalledPS, StartedPS);
  AddServiceInfo('Remote Relay', 'RR', InstalledRR, StartedRR);
  AddServiceInfo('API Adapter', 'AA', InstalledAA, StartedAA);
  AddServiceInfo('ADR Client', 'AC', InstalledAC, StartedAC);
  AddServiceInfo('ADR Server', 'AS', InstalledAS, StartedAS);

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Service', 'ServiceName');
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Status', 'Status');
  TableBuilder.AddColumn('Action', 'Action');

  sPage := GetResource('maintenance_services.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhAppVersion(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sCurVerFile, sVerText, s: String;
  AvailableVersions: IisParamsCollection;
  Versions: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  Res: IisParamsCollection;
  i: Integer;

  procedure MarkCurrentVersion;
  var
    i: Integer;
    LV: IisListOfValues;
    sCurVer: String;
  begin
    sCurVer := ChangeFileExt(ExtractFileName(sCurVerFile), '');
    for i := 0 to Versions.Count - 1 do
    begin
      LV := Versions[i];
      if LV.Value['Version'] = sCurVer then
      begin
        s := LV.Value['Selector'];
        s := ReplaceMacros(s, M_ATTRIBUTES, 'checked="checked"');
        LV.Value['Selector'] := s;
      end;
    end;
  end;

  procedure MarkUnpublishedVersions;
  var
    i: Integer;
    Ver, AvlVer: IisListOfValues;
  begin
    if Assigned(AvailableVersions) then
      for i := 0 to Versions.Count - 1 do
      begin
        Ver := Versions[i];
        AvlVer := AvailableVersions.ParamsByName(Ver.Value['Version']);
        if Assigned(AvlVer) then
          if AvlVer.TryGetValue('QA', False) then
            Ver.Value['Version'] := Ver.Value['Version'] + ReplaceMacros(hfAlertText, M_CONTENT, ' (Unpublished)');
      end;
  end;

  procedure BuildRemoteVersionList;
  var
    sLastVer, sVer, s: String;
    i: Integer;
    LV: IisListOfValues;
  begin
    if Versions.Count > 0 then
      sLastVer := Versions[0].Value['Version']
    else
      sLastVer := IntToStr(EvMajorVerNbr);

    Versions := nil;
    if Assigned(AvailableVersions) then
    begin
      Versions := TisParamsCollection.Create;
      for i := 0 to AvailableVersions.Count - 1 do
      begin
        sVer := AvailableVersions.ParamName(i);
        if sVer > sLastVer then
        begin
          LV := Versions.AddParams(AvailableVersions[i].Value['File']);
          LV.AddValue('Version', sVer);

          s := ReplaceMacros(hfRadioButton, M_PARAM, 'version');
          s := ReplaceMacros(s, M_VALUE, sVer);
          LV.AddValue('Selector', s);
        end;
      end;

      MarkUnpublishedVersions;
    end;
  end;

  procedure BuildLocalVersionList;
  var
    SR: TSearchRec;
    sVer, s: String;
    LV: IisListOfValues;
    LocalVersions: IisParamsCollection;
    i: Integer;
  begin
    Versions := TisParamsCollection.Create;

    LocalVersions := Mainboard.GetLocalVersions(ASession.UserAccount.Application);
    for i := 0 to LocalVersions.Count - 1 do
    begin
      sVer := ChangeFileExt(ExtractFileName(SR.Name), LocalVersions.ParamName(i));

      s := sVer;

      LV := Versions.AddParams(LocalVersions[i].Value['File']);
      LV.AddValue('Version', sVer);

      s := ReplaceMacros(hfRadioButton, M_PARAM, 'version');
      s := ReplaceMacros(s, M_VALUE, sVer);
      LV.AddValue('Selector', s);
    end;

    MarkCurrentVersion;
    Versions.Sort('desc');
    MarkUnpublishedVersions;
  end;

begin
  // Request current version info from all DS
  Res := ParallelExec('DM.EvGetAppArchiveFileName', nil);

  sCurVerFile := '';
  for i := 0 to Res.Count - 1 do
    if Res[i].ValueExists('Error') then
    begin
      sCurVerFile := 'unknown';
      break;
    end
    else
    begin
      s := Res[i].Value[METHOD_RESULT];
      if s = '' then
      begin
        sCurVerFile := 'broken';
        break;
      end;

      if sCurVerFile = '' then
        sCurVerFile := s
      else if sCurVerFile <> s then
      begin
        sCurVerFile := 'broken';
        break;
      end;
    end;


  if sCurVerFile = '' then
    sVerText := 'Currently there is no version installed'
  else if sCurVerFile = 'unknown' then
    sVerText := 'One or more Deployment Managers have not responded on request. Please check service status.'
  else if sCurVerFile = 'broken' then
    sVerText := 'Version information does not match. Reinstallation is required.'
  else
    sVerText := 'Installed Version: ' + ChangeFileExt(ExtractFileName(sCurVerFile), '');


  // build result
  sPage := GetResource('version_update.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$VERSION_STATUS$', sVerText);

  AvailableVersions := Mainboard.GetRemoteVersions(ASession.UserAccount.Application);
  AvailableVersions.Sort('desc');

  BuildLocalVersionList;
  TableBuilder := TmcHtmlTableBulder.Create(Versions);
  TableBuilder.AddColumn('Current', 'Selector');
  TableBuilder.AddColumn('Version', 'Version');
  sPage := ReplaceMacros(sPage, '$LOCAL_VERSIONS$', TableBuilder.GetHtml);

  BuildRemoteVersionList;
  if Assigned(Versions) then
  begin
    TableBuilder := TmcHtmlTableBulder.Create(Versions);
    TableBuilder.AddColumn('Download', 'Selector');
    TableBuilder.AddColumn('Version', 'Version');
    sPage := ReplaceMacros(sPage, '$REMOTE_VERSIONS$', TableBuilder.GetHtml);
  end
  else
    sPage := ReplaceMacros(sPage, '$REMOTE_VERSIONS$', ReplaceMacros(hfAlertText, M_CONTENT, 'Cannot connect to iSystems website'));

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.rhServiceAction(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sType, sHost, sOper: String;
  RB: ImcDMControl;

  procedure InformServicesAboutRB(const AHost: String);
  var
    Settings: IisListOfValues;
    Params: IisListOfValues;
  begin
    Settings := TisListOfValues.Create;
    Settings.AddValue('RequestBroker', AHost);
    Params := TisListOfValues.Create;
    Params.AddValue('Settings', Settings);

    Mainboard.DeploymentManagers.ParallelExec(['RR.SetSettings', 'AA.SetSettings', 'AC.SetSettings', 'AS.SetSettings'], [Params, Params, Params, Params]);
  end;


  procedure SingleDSOper;
  var
    DC: ImcDMControl;
  begin
    DC := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);
    if Assigned(DC) then
    begin
      if AnsiSameText(sType, 'RB') then
      begin
        if AnsiSameText(sOper, 'start') then
          DC.EvolutionControl.StartRB
        else if AnsiSameText(sOper, 'stop') then
          DC.EvolutionControl.StopRB
        else if AnsiSameText(sOper, 'restart') then
          DC.EvolutionControl.RestartRB
        else if AnsiSameText(sOper, 'install') then
          DC.EvolutionControl.InstallRB
        else if AnsiSameText(sOper, 'uninstall') then
          DC.EvolutionControl.UnInstallRB;
       end

      else if AnsiSameText(sType, 'RP') then
      begin
        if AnsiSameText(sOper, 'start') then
          DC.EvolutionControl.StartRP
        else if AnsiSameText(sOper, 'stop') then
          DC.EvolutionControl.StopRP
        else if AnsiSameText(sOper, 'restart') then
          DC.EvolutionControl.RestartRP
        else if AnsiSameText(sOper, 'install') then
          DC.EvolutionControl.InstallRP
        else if AnsiSameText(sOper, 'uninstall') then
          DC.EvolutionControl.UnInstallRP;
      end

      else if AnsiSameText(sType, 'RR') then
      begin
        if AnsiSameText(sOper, 'start') then
          DC.EvolutionControl.StartRR
        else if AnsiSameText(sOper, 'stop') then
          DC.EvolutionControl.StopRR
        else if AnsiSameText(sOper, 'restart') then
          DC.EvolutionControl.RestartRR
        else if AnsiSameText(sOper, 'install') then
          DC.EvolutionControl.InstallRR
        else if AnsiSameText(sOper, 'uninstall') then
          DC.EvolutionControl.UninstallRR;
      end

      else if AnsiSameText(sType, 'AA') then
      begin
        if AnsiSameText(sOper, 'start') then
          DC.EvolutionControl.StartAA
        else if AnsiSameText(sOper, 'stop') then
          DC.EvolutionControl.StopAA
        else if AnsiSameText(sOper, 'restart') then
          DC.EvolutionControl.RestartAA
        else if AnsiSameText(sOper, 'install') then
          DC.EvolutionControl.InstallAA
        else if AnsiSameText(sOper, 'uninstall') then
          DC.EvolutionControl.UninstallAA;
      end

      else if AnsiSameText(sType, 'AC') then
      begin
        if AnsiSameText(sOper, 'start') then
          DC.EvolutionControl.StartAC
        else if AnsiSameText(sOper, 'stop') then
          DC.EvolutionControl.StopAC
        else if AnsiSameText(sOper, 'restart') then
          DC.EvolutionControl.RestartAC
        else if AnsiSameText(sOper, 'install') then
          DC.EvolutionControl.InstallAC
        else if AnsiSameText(sOper, 'uninstall') then
          DC.EvolutionControl.UninstallAC;
      end

      else if AnsiSameText(sType, 'AS') then
      begin
        if AnsiSameText(sOper, 'start') then
          DC.EvolutionControl.StartAS
        else if AnsiSameText(sOper, 'stop') then
          DC.EvolutionControl.StopAS
        else if AnsiSameText(sOper, 'restart') then
          DC.EvolutionControl.RestartAS
        else if AnsiSameText(sOper, 'install') then
          DC.EvolutionControl.InstallAS
        else if AnsiSameText(sOper, 'uninstall') then
          DC.EvolutionControl.UninstallAS;
      end;
    end;
  end;


  procedure MultipleDSOper;
  var
    Res: IisParamsCollection;
    SL: IisStringList;
    i: Integer;
  begin
    if AnsiSameText(sType, 'RP') then
    begin
      if AnsiSameText(sOper, 'start') then
        Res := ParallelExec('RP.Start', nil)
      else if AnsiSameText(sOper, 'stop') then
        ParallelExec('RP.Stop', nil)
      else if AnsiSameText(sOper, 'restart') then
        ParallelExec('RP.Restart', nil);
    end

    else if AnsiSameText(sType, 'RR') then
    begin
      if AnsiSameText(sOper, 'start') then
        ParallelExec('RR.Start', nil)
      else if AnsiSameText(sOper, 'stop') then
        ParallelExec('RR.Stop', nil)
      else if AnsiSameText(sOper, 'restart') then
        ParallelExec('RR.Restart', nil);
    end

    else if AnsiSameText(sType, 'AA') then
    begin
      if AnsiSameText(sOper, 'start') then
        ParallelExec('AA.Start', nil)
      else if AnsiSameText(sOper, 'stop') then
        ParallelExec('AA.Stop', nil)
      else if AnsiSameText(sOper, 'restart') then
        ParallelExec('AA.Restart', nil);
    end

    else if AnsiSameText(sType, 'AC') then
    begin
      if AnsiSameText(sOper, 'start') then
        ParallelExec('AC.Start', nil)
      else if AnsiSameText(sOper, 'stop') then
        ParallelExec('AC.Stop', nil)
      else if AnsiSameText(sOper, 'restart') then
        ParallelExec('AC.Restart', nil);
    end

    else if AnsiSameText(sType, 'AS') then
    begin
      if AnsiSameText(sOper, 'start') then
        ParallelExec('AS.Start', nil)
      else if AnsiSameText(sOper, 'stop') then
        ParallelExec('AS.Stop', nil)
      else if AnsiSameText(sOper, 'restart') then
        ParallelExec('AS.Restart', nil);
    end

    else if sType = '' then
    begin
      if AnsiSameText(sOper, 'start') then
        Mainboard.DeploymentManagers.ParallelExec(['RP.Start', 'RR.Start', 'AA.Start', 'AC.Start', 'AS.Start', 'RB.Start'], [])
      else if AnsiSameText(sOper, 'stop') then
        SL := Mainboard.DeploymentManagers.ParallelExec(['RP.Stop', 'RR.Stop', 'AA.Stop', 'AC.Stop', 'AS.Stop', 'RB.Stop'], [])
      else if AnsiSameText(sOper, 'restart') then
        Mainboard.DeploymentManagers.ParallelExec(['RP.Restart', 'RR.Restart', 'AA.Restart', 'AC.Restart', 'AS.Restart', 'RB.Restart'], [])
   end;

   if Assigned(Res) then
   begin
     try
       CheckParallelExecError(Res);
     except
       on E: Exception do
         GlobalLogFile.AddEvent(etError, 'HTTP Server', E.Message, '');
     end;
   end
   else
   if Assigned(SL) then
   begin
     for i := 0 to SL.Count - 1 do
     try
       Res := SL.Objects[i] as IisParamsCollection;
       CheckParallelExecError(Res);
     except
       on E: Exception do
         GlobalLogFile.AddEvent(etError, 'HTTP Server', E.Message, '');
     end;
   end;
  end;

begin
  if AParams.ValueExists(P_Host) then
    sHost := AParams.Value[P_Host]
  else
    sHost := '';

  if AParams.ValueExists(P_Type) then
    sType := AParams.Value[P_Type]
  else
    sType := '';

  sOper := AParams.Value[P_Operation];

  try
    if sHost <> '' then
      SingleDSOper
    else
      MultipleDSOper;

    UpdateRPPoolInfo;
    UpdateADRServicesInfo;

    RB := GetRBDM;
    if Assigned(RB) then
      InformServicesAboutRB(RB.Host); // update RB location
  except
    on E: Exception do
      GlobalLogFile.AddEvent(etError, 'HTTP Server', E.Message, '');
  end;

  Result := '';
end;

function TmcHttpPresenter.RenderRequest(const aParams: IisListOfValues; out AMIMEType: String): String;
var
  sMIMEType: String;
  Idx: Integer;
  s, sMethod, sSession: String;
  Session: ImcSession;
begin
  s := aParams.Value['URL'];
  sMethod := Trim(GetNextStrValue(s, '&'));
  if (sMethod <> '') and (sMethod[1] = '/') then
    Delete(sMethod, 1, 1);

  if sMethod = '' then
    sMethod := 'MAIN';

  // finding session
  Idx := FRequestHandlers.IndexOf(sMethod);
  if Idx >= 0 then
  begin
    if aParams.ValueExists(P_SESSION) then
      sSession := aParams.Value[P_SESSION]
    else
      sSession := '';

    Session := FSessions.AcquireObject(sSession) as ImcSession;
    FSessions.ReturnObject(Session as IisObjectFromPool);
    try
      if aParams.TryGetValue('refresh_time', '') <> '' then
        Session.RefreshTime := StrToIntDef(aParams.TryGetValue('refresh_time', ''),
                               Session.RefreshTime);

      if aParams.TryGetValue(P_EvDomain, '') <> '' then
        Session.EvDomain := aParams.Value[P_EvDomain];

      if not AnsiSameText(sMethod, 'AUTHORIZATION') and not Session.Authorized then
      begin
        sMethod := 'LOGIN';
        Idx := FRequestHandlers.IndexOf(sMethod);
      end;

      if AParams.ValueExists(P_MainMenuItem) then
        Session.MainMenuPosition := AParams.Value[P_MainMenuItem];

      if Session.RequestID <> 0 then
        Result := GetResult(Session, AMIMEType, 0)
      else
      begin
        Session.RequestID := GlobalThreadManager.RunTask(ExecRequest, Self, MakeTaskParams([Idx, Session, AParams]));
        if not Session.Authorized then
          Result := GetResult(Session, AMIMEType, 0)
        else
          Result := GetResult(Session, AMIMEType, 500);
       end;

      if Result = '' then
        Result := GetWaitScreen(Session, aParams, AMIMEType);

      Result := AddRefresh(Session, Result, aParams.Value['URL']);
      Result := AddEvDomainSelection(Session, Result, aParams.Value['URL']);      
      Result := ReplaceMacros(Result, M_SESSION, '&session=' + Session.SessionID);
    finally
      if not Session.Authorized then
        Session := FSessions.AcquireObject(sSession) as ImcSession;  // remove session from pool
    end;
  end
  else
    Result := GetResource(sMethod, sMIMEType);
end;

function TmcHttpPresenter.rhConfigEvServers(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  InstalledRB, InstalledPS, InstalledRR, InstalledAA, InstalledAC, InstalledAS: IisParamsCollection;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage: String;

  procedure AddServiceInfo(const aServiceName, aServiceType: String; const InstalledSrv: IisParamsCollection);
  var
    i: Integer;
    LV: IisListOfValues;
    sActionLink, sOnClick, sHost: String;
    HREF: ImcHREF;
  begin
    for i := 0 to InstalledSrv.Count - 1 do
    begin
      sHost := InstalledSrv.ParamName(i);

      if InstalledSrv[i].ValueExists('Error') then
        sActionLink := ''

      else
      begin
        HREF := TmcHREF.Create('service_action', ASession);
        HREF.AddParam(P_Host, sHost);
        HREF.AddParam(P_Type, aServiceType);

        if InstalledSrv[i].Value[METHOD_RESULT] then
        begin
          HREF.AddParam(P_Operation, 'uninstall');
          sActionLink := ReplaceMacros(hfPostButton, M_METHOD, HREF.GetHtml);
          sOnClick := ReplaceMacros(hfOnClickConfirmation, M_TEXT,
               Format('Please confirm %s service %s on %s', [aServiceName, 'UNINSTALL', sHost]));
          sActionLink := ReplaceMacros(sActionLink, M_ATTRIBUTES, sOnClick );
          sActionLink := ReplaceMacros(sActionLink, M_CAPTION, 'Uninstall');
        end
        else
        begin
          HREF.AddParam(P_Operation, 'install');
          sActionLink := ReplaceMacros(hfPostButton, M_METHOD, HREF.GetHtml);
          sOnClick := ReplaceMacros(hfOnClickConfirmation, M_TEXT,
               Format('Please confirm %s service %s on %s', [aServiceName, 'INSTALL', sHost]));
          sActionLink := ReplaceMacros(sActionLink, M_ATTRIBUTES, sOnClick );
          sActionLink := ReplaceMacros(sActionLink, M_CAPTION, 'Install');
        end;
      end;

      LV := TableDetails.ParamsByName(sHost);
      if not Assigned(LV) then
      begin
        LV := TableDetails.AddParams(sHost);
        LV.AddValue('Host', sHost);
      end;
      LV.AddValue(aServiceType + 'Action', sActionLink);
    end;
  end;

  procedure EnsureOneInstance(const AInstallInfo: IisParamsCollection);
  var
    sHost: String;
    i: Integer;
  begin
    sHost := '';
    for i := 0 to AInstallInfo.Count - 1 do
      if not InstalledRB[i].ValueExists('Error') then
        if AInstallInfo[i].Value[METHOD_RESULT] then
        begin
          sHost := AInstallInfo.ParamName(i);
          break
        end;

    if sHost <> '' then
      for i := AInstallInfo.Count - 1 downto 0 do
        if AInstallInfo.ParamName(i) <> sHost then
          AInstallInfo.DeleteParams(i);
  end;

begin
  // collect info from DSs
  InstalledRB := ParallelExec('RB.Installed', nil);
  InstalledPS := ParallelExec('RP.Installed', nil);
  InstalledRR := ParallelExec('RR.Installed', nil);
  InstalledAA := ParallelExec('AA.Installed', nil);
  InstalledAC := ParallelExec('AC.Installed', nil);
  InstalledAS := ParallelExec('AS.Installed', nil);

  TableDetails := TisParamsCollection.Create;
  AddServiceInfo('Request Processor', 'RP', InstalledPS);
  AddServiceInfo('Remote Relay', 'RR', InstalledRR);
  AddServiceInfo('API Adapter', 'AA', InstalledAA);
  AddServiceInfo('ADR Client', 'AC', InstalledAC);

  if not isISystemsDomain then
    EnsureOneInstance(InstalledAS);
  AddServiceInfo('ADR Server', 'AS', InstalledAS);

  EnsureOneInstance(InstalledRB);
  AddServiceInfo('Request Broker', 'RB', InstalledRB);

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Request Broker', 'RBAction');
  TableBuilder.AddColumn('Request Processor', 'RPAction');
  TableBuilder.AddColumn('Remote Relay', 'RRAction');
  TableBuilder.AddColumn('API Adapter', 'AAAction');
  TableBuilder.AddColumn('ADR Client', 'ACAction');
  TableBuilder.AddColumn('ADR Server', 'ASAction');

  sPage := GetResource('config_services.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhConfigMain(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage: String;
begin
  sPage := GetResource('config_main.html', AMIMEType);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

{
function TmcHttpPresenter.GetUnderConstruction(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  s: String;
begin
  s := ReplaceMacros(hfImage, M_CONTENT, 'under_construction.gif');
  s := ReplaceMacros(hfParagraph, M_CONTENT, s);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, s);
end;
}

function TmcHttpPresenter.rhConfigDSServices(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  LV: IisListOfValues;
  sPage, sActionLink, sOnClick: String;
  HREF: ImcHREF;
begin
  TableDetails := TisParamsCollection.Create;

  Mainboard.DeploymentManagers.Lock;
  try
    for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
    begin
      LV := TableDetails.AddParams(Mainboard.DeploymentManagers[i].Host);
      LV.AddValue('Host', Mainboard.DeploymentManagers[i].Host);

      HREF := TmcHREF.Create('delete_dsservice', ASession);
      HREF.AddParam('Host', LV.Value['Host']);
      sActionLink := ReplaceMacros(hfPostButton, M_METHOD, HREF.GetHtml);
      sOnClick := ReplaceMacros(hfOnClickConfirmation, M_TEXT,
         Format('All Evolution services on %s will be uninstalled. Do you want to continue?', [LV.Value['Host']]));
      sActionLink := ReplaceMacros(sActionLink, M_ATTRIBUTES, sOnClick );
      sActionLink := ReplaceMacros(sActionLink, M_CAPTION, 'Delete');

      LV.AddValue('Action', sActionLink);
    end;
  finally
    Mainboard.DeploymentManagers.Unlock;
  end;

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Action', 'Action');

  sPage := GetResource('config_dm_services.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhDeleteDSService(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sHost: String;
  DS: ImcDMControl;
begin
  sHost := AParams.Value['Host'];
  DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);
  if Assigned(DS) then
    try
      DS.EvolutionControl.Uninstall;
    except
      on E: Exception do
        GlobalLogFile.AddEvent(etError, 'HTTP Server', E.Message, BuildStackedErrorStr(E));
    end;
  Mainboard.DeploymentManagers.DelDeploymentManager(sHost);

  UpdateRPPoolInfo;
  UpdateADRServicesInfo;

  Result := '';
end;

function TmcHttpPresenter.rhAddDSServices(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  s: String;
  sCurVerFile: String;
  DS: ImcDMControl;
  Res: IisParamsCollection;
  NewServers: IisList;
  Par: IisListOfValues;
begin
  // get current version info
  Res := ParallelExec('DM.EvGetAppArchiveFileName', nil);
  sCurVerFile := '';
  for i := 0 to Res.Count - 1 do
    if Res[i].ValueExists('Error') then
    begin
      sCurVerFile := '';
      break;
    end
    else
    begin
      s := Res[i].Value[METHOD_RESULT];
      if s = '' then
      begin
        sCurVerFile := '';
        break;
      end;

      if sCurVerFile = '' then
        sCurVerFile := s
      else if sCurVerFile <> s then
      begin
        sCurVerFile := '';
        break;
      end;
    end;

  if (sCurVerFile <> '') then
  begin
    sCurVerFile := FEvVersionFolder + sCurVerFile;
    if not FileExists(sCurVerFile) then
      sCurVerFile := '';
  end;


  // add new host
  NewServers := TisList.Create;
  Mainboard.DeploymentManagers.Lock;
  try
    for i := 0 to AParams.Count - 1 do
    begin
      if StartsWith(AParams[i].Name, 'Host_') or AnsiSameText(AParams[i].Name, 'Host') then
      begin
        s := Trim(AParams[i].Value);
        if (s <> '') and (Mainboard.DeploymentManagers.FindDeploymentManager(s) = nil) then
        begin
          DS := Mainboard.DeploymentManagers.AddDeploymentManager(s, '');
          NewServers.Add(DS);
          DS := nil;
        end;
      end;
    end;
  finally
    Mainboard.DeploymentManagers.Unlock;
  end;

  // install current version on newly added servers
  if (NewServers.Count > 0) and (sCurVerFile <> '') then
  begin
    Par := TisListOfValues.Create;
    Par.AddValue('ArchiveFile', sCurVerFile);
    Mainboard.DeploymentManagers.ParallelExecFor(NewServers, ['DM.EvInstall'], [Par]);
  end;

  Result := '';
end;

function TmcHttpPresenter.rhConfigDBLocations(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr: String;
  defPath, sysPath, sbPath, tmpPath: String;
  RB: IevRBControl;
  Locations: IisParamsCollection;
  DBSettings: IisListOfValues;
  i: Integer;
  LV: IisListOfValues;
  ChBoxBuilder: ImcHtmlCheckBoxBuilder;
  TableBuilder: ImcHtmlTableBulder;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    sPage := GetDomainContextPage('config_db_locations.html', sDomain, sDomainDescr);

    RB := GetRBControl(True);
    DBSettings := RB.GetDatabaseParams(sDomain);
    Locations := IInterface(DBSettings.Value['DBLocation']) as IisParamsCollection;

    // Primary DBs
    defPath := '';
    sysPath := '';
    sbPath := '';
    tmpPath := '';
    for i := 0 to Locations.Count - 1 do
    begin
      LV := Locations[i];
      case TevDBType(Integer(LV.Value['DBType'])) of
        dbtUnspecified : defPath := LV.Value['Path'];
        dbtSystem      : sysPath := LV.Value['Path'];
        dbtBureau      : sbPath  := LV.Value['Path'];
        dbtTemporary   : tmpPath := LV.Value['Path'];
      end;
    end;

    // Misc settings
    ChBoxBuilder := TmcHtmlCheckBoxBuilder.Create('broadcast', '1', 'Broadcast DB changes (should be on for ADR)');
    ChBoxBuilder.CaptionLocation := lRight;
    if ACInstalled and AnsiSameText(sDomain, sDefaultDomain) then
    begin
      ChBoxBuilder.TryCheck('1');
      ChBoxBuilder.Enabled := False;
    end
    else if DBSettings.Value['DBChangesBroadcast'] then
      ChBoxBuilder.TryCheck('1');

    sPage := ReplaceMacros(sPage, '$BROADCAST_CHANGES$', ChBoxBuilder.GetHtml);
    sPage := ReplaceMacros(sPage, '$defaultDBpath$', defPath);
    sPage := ReplaceMacros(sPage, '$systemDBpath$', sysPath);
    sPage := ReplaceMacros(sPage, '$sbDBpath$', sbPath);
    sPage := ReplaceMacros(sPage, '$tmpDBpath$', tmpPath);

    // client DBs
    for i := Locations.Count - 1 downto 0 do  // filter CL paths
    begin
      LV := Locations[i];
      if TevDBType(Integer(LV.Value['DBType'])) <> dbtClient then
        Locations.DeleteParams(i);
    end;

    for i := 1 to 3 do    // add blank rows
    begin
      LV := Locations.AddParams('Blank' + IntToStr(i));
      LV.AddValue('Path', '');
      LV.AddValue('BeginRangeNbr', '');
      LV.AddValue('EndRangeNbr', '');
    end;

    TableBuilder := TmcHtmlTableBulder.Create(Locations);
    LV := TableBuilder.AddColumn('Client DB path override', 'Path');
    LV.Value['EditType'] := 'EditBox';
    LV.Value['EditParam'] := 'cl_path';
    LV.Value['Size'] := '60';
    LV := TableBuilder.AddColumn('Starting CL_#', 'BeginRangeNbr');
    LV.Value['EditType'] := 'EditBox';
    LV.Value['EditParam'] := 'cl_bnbr';
    LV.Value['Size'] := '10';
    LV := TableBuilder.AddColumn('Ending CL_#', 'EndRangeNbr');
    LV.Value['EditType'] := 'EditBox';
    LV.Value['EditParam'] := 'cl_enbr';
    LV.Value['Size'] := '10';

    sPage := ReplaceMacros(sPage, '$CLIENT_DB_PATHS$', TableBuilder.GetHtml);
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

procedure TmcHttpPresenter.SelectEvDomain(const AParams: IisListOfValues; out ADomain, ADomainDescr, AHTML, AMIMEType: String);
var
  Domains: IisListOfValues;

  function BuildSBLinks: String;
  var
    i: Integer;
    s: String;
    HREF: ImcHREF;
  begin
    Result := '';
    for i := 0 to Domains.Count - 1 do
      if not AnsiSameText(Domains[i].Name, sDefaultDomain) then
      begin
        HREF := TmcHREF.Create(AParams.Value['URL'], nil);
        HREF.AddParam(P_EvDomain, Domains[i].Name);
        s := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);

        if Domains[i].Value = '' then
          s := ReplaceMacros(s, M_CONTENT, Domains[i].Name)
        else
          s := ReplaceMacros(s, M_CONTENT, Domains[i].Value);

        Result := Result + s + hfBR;
      end;
  end;

begin
  AHTML := '';

  Domains := GetEvDomainList;

  ADomain := AParams.TryGetValue(P_EvDomain, '');
  ADomainDescr := Domains.TryGetValue(ADomain, ADomain);
  if ADomainDescr = '' then
    ADomainDescr := ADomain;

  if ADomain = '' then
  begin
    if Domains.Count = 1 then
    begin
      ADomain := Domains.Values[0].Name;
      ADomainDescr := Domains[0].Value;
    end
    else
    begin
      AHTML := GetResource('evdomains.html', AMIMEType);
      AHTML := ReplaceMacros(AHTML, M_CONTENT, BuildSBLinks);
    end;
  end;
end;

function TmcHttpPresenter.rhChangeDBLocations(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sDomain, sParNbr: String;
  RB: IevRBControl;
  Locations: IisParamsCollection;
  DBSettings: IisListOfValues;
  i: Integer;
  bDBChangeBroadcast: Boolean;

  procedure AddDBInfo(const AType: TevDBType; const APath: String; const ABeginRangeNbr, AEndRangeNbr: Integer);
  var
    Item: IisListOfValues;
  begin
    if APath <> '' then
    begin
      Item := Locations.AddParams(IntToStr(Locations.Count));
      Item.AddValue('DBType', Ord(AType));
      Item.AddValue('Path', APath);
      Item.AddValue('BeginRangeNbr', ABeginRangeNbr);
      Item.AddValue('EndRangeNbr', AEndRangeNbr);
    end;
  end;

begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  sDomain := AParams.Value[P_EvDomain];
  Locations := TisParamsCollection.Create;
  DBSettings := TisListOfValues.Create;
  DBSettings.AddValue('DBLocation', Locations);

  AddDBInfo(dbtUnspecified, AParams.Value['defaultDBpath'], 0, 0);
  AddDBInfo(dbtSystem, AParams.Value['systemDBpath'], 0, 0);
  AddDBInfo(dbtBureau, AParams.Value['sbDBPath'], 0, 0);
  AddDBInfo(dbtTemporary, AParams.Value['tmpDBPath'], 0, 0);

  for i := 0 to AParams.Count - 1 do
  begin
    if StartsWith(AParams[i].Name, 'cl_path') and (AParams[i].Value <> '') then
    begin
      sParNbr := AParams[i].Name;
      GetNextStrValue(sParNbr, 'cl_path');
      AddDBInfo(dbtClient, AParams.Value['cl_path' + sParNbr],
                StrToInt(Trim(AParams.Value['cl_bnbr' + sParNbr])),
                StrToInt(Trim(AParams.Value['cl_enbr' + sParNbr])));
    end;
  end;

  if ACInstalled then
    bDBChangeBroadcast := True
  else
    bDBChangeBroadcast := AParams.TryGetValue('broadcast', '0') = '1';
  DBSettings.AddValue('DBChangesBroadcast', bDBChangeBroadcast);

  RB.SetDatabaseParams(sDomain, DBSettings);
end;

function TmcHttpPresenter.rhChangeSystemUser(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  RB.SetAdminAccount(AParams.Value[P_EvDomain], AParams.Value['Admin_User']);
  RB.SetSystemAccount(AParams.Value[P_EvDomain], AParams.Value['System_User']);
end;

function TmcHttpPresenter.rhConfigSystemUser(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr, sUser: String;
  RB: IevRBControl;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    sPage := GetDomainContextPage('config_system_user.html', sDomain, sDomainDescr);

    RB := GetRBControl(True);
    RB.GetSystemAccount(sDomain, sUser);
    sPage := ReplaceMacros(sPage, '$SYSTEM_USER$', sUser);
    RB.GetAdminAccount(sDomain, sUser);
    sPage := ReplaceMacros(sPage, '$ADMIN_USER$', sUser);
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeLicense(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  s: String;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  s := StringReplace(AParams.Value['reg_code'], #13, '', [rfReplaceAll]);
  s := StringReplace(s, #10, '', [rfReplaceAll]);
  RB.SetLicenseInfo(AParams.Value[P_EvDomain], AParams.Value['serial_number'], Trim(s));
end;

function TmcHttpPresenter.rhConfigLicense(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr, sLicModDetails, sAPIKeyDetails, sDiag, s: String;
  RB: IevRBControl;
  MachineInfo, LicenseInfo, LicenseDetails, LV, APIKeys, KeyInfo: IisListOfValues;
  TableBuilder: ImcHtmlTableBulder;
  i: Integer;
  TblData: IisParamsCollection;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    sPage := GetDomainContextPage('config_sb_license.html', sDomain, sDomainDescr);

    RB := GetRBControl(True);
    MachineInfo := RB.GetMachineInfo;
    LicenseInfo := RB.GetLicenseInfo(sDomain);

    // build license info
    sAPIKeyDetails := '';
    if LicenseInfo.ValueExists('LicenseDetails') then
    begin
      LicenseDetails := IInterface(LicenseInfo.Value['LicenseDetails']) as IisListOfValues;
      LicenseDetails.DeleteValue('SerialNumber');
      TblData := TisParamsCollection.Create;
      for i := 0 to LicenseDetails.Count - 1 do
        if not AnsiSameText(LicenseDetails.Values[i].Name, 'APIKeys') then
        begin
          LV := TblData.AddParams(LicenseDetails.Values[i].Name);
          LV.AddValue('Module', LicenseDetails.Values[i].Name);
          if LicenseDetails.Values[i].Value then
            LV.AddValue('State', '1');
        end;
      TblData.Sort;

      TableBuilder := TmcHtmlTableBulder.Create(TblData);
      TableBuilder.AddColumn('Module', 'Module');
      LV := TableBuilder.AddColumn('State', 'State');
      LV.AddValue('EditType', 'Checkbox');
      LV.AddValue('Enabled', False);

      sLicModDetails := TableBuilder.GetHtml;

      // build API keys info
      if LicenseDetails.ValueExists('APIKeys') then
      begin
       // convert API keys structure to ParamCollection
        APIKeys := IInterface(LicenseDetails.Value['APIKeys']) as IisListOfValues;
        TblData := TisParamsCollection.Create;
        for i := 0 to APIKeys.Count - 1 do
        begin
          KeyInfo := IInterface(APIKeys[i].Value) as IisListOfValues;
          LV := TblData.AddParams(IntToStr(i));
          if Assigned(KeyInfo) then
          begin
            LV.AddValue('Key', APIKeys[i].Name);
            LV.AddValue('Vendor', KeyInfo.TryGetValue('APIKeyVendorName', ''));
            LV.AddValue('VendorNbr', KeyInfo.TryGetValue('APIKeyVendorCustomNbr', ''));
            LV.AddValue('AppName', KeyInfo.TryGetValue('APIKeyApplicationName', ''));
          end
          else
          begin
            s := ReplaceMacros(hfAlertText, M_CONTENT, 'Invalid');
            LV.AddValue('Key', s);
            LV.AddValue('Vendor', s);
            LV.AddValue('VendorNbr', s);
            LV.AddValue('AppName', s);
          end;
        end;
      end;

      TableBuilder := TmcHtmlTableBulder.Create(TblData);
      TableBuilder.AddColumn('Vendor', 'Vendor');
      TableBuilder.AddColumn('Vendor #', 'VendorNbr');
      TableBuilder.AddColumn('Application', 'AppName');
      TableBuilder.AddColumn('API Key', 'Key');

      sAPIKeyDetails := TableBuilder.GetHtml;
    end
    else
      sLicModDetails := ReplaceMacros(hfAlertText, M_CONTENT, LicenseInfo.TryGetValue('Error', ''));

    sPage := ReplaceMacros(sPage, M_HOST, MachineInfo.Value['Name']);
    sDiag  := AParams.TryGetValue('bbcheck','');
    if Length(sDiag) > 0 then
    begin
      sDiag := GetRBControl.GetBBDiagnosticCheck;
      if not AnsiSameText(copy(sDiag, 1, Length('Success')), 'Success') then
        raise EisException.Create(sDiag); 
    end;
    sPage := ReplaceMacros(sPage, '$BBDIAG$', sDiag);
    sPage := ReplaceMacros(sPage, '$COMPUTER_MODIFIER$', MachineInfo.Value['ID']);
    sPage := ReplaceMacros(sPage, '$SERIAL_NUMBER$', LicenseInfo.TryGetValue('SerialNumber', ''));
    sPage := ReplaceMacros(sPage, '$REG_CODE$', WrapText(LicenseInfo.TryGetValue('RegistrationCode', ''), 48));
    sPage := ReplaceMacros(sPage, '$LICMOD_DETAILS$', sLicModDetails);
    sPage := ReplaceMacros(sPage, '$APIKEYS_DETAILS$', sAPIKeyDetails);
    sPage := ReplaceMacros(sPage, '$LAST_CHANGE$', 'License updated on: ' + LicenseInfo.TryGetValue('LastChange', ''));
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeEMailNotification(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  s, sEmail, sSendTo: String;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  s := StringReplace(AParams.Value['sent_to'], #13#10, ';', [rfReplaceAll]);
  sSendTo := '';
  while s <> '' do
  begin
    sEmail := Trim(GetNextStrValue(s, ';'));
    if sEmail <> '' then
      AddStrValue(sSendTo, sEmail, ';');
  end;

  RB.SetEMailInfo(AParams.Value['smtp_host'],
                  AParams.Value['smtp_port'],
                  AParams.Value['smtp_user'],
                  AParams.Value['smtp_password'],
                  AParams.Value['sent_from'],
                  sSendTo);
end;

function TmcHttpPresenter.rhConfigEMailNotification(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sSMTPHost, sSMTPPort, sSMTPUser, sSMTPPassword,
  sSentFrom, sSentTo: String;
  RB: IevRBControl;
begin
  RB := GetRBControl(True);
  RB.GetEMailInfo(sSMTPHost, sSMTPPort, sSMTPUser, sSMTPPassword, sSentFrom, sSentTo);

  // We need to synchronize MC eMail setting in case if it was changed manually on RB side
  // GET is the best place to do that
  Mainboard.UpdateEMailConfig(sSMTPHost, sSMTPPort, sSMTPUser, sSMTPPassword, sSentFrom, sSentTo);

  sPage := GetResource('config_email.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$SMTP_HOST$', sSMTPHost);
  sPage := ReplaceMacros(sPage, '$SMTP_PORT$', sSMTPPort);
  sPage := ReplaceMacros(sPage, '$SMTP_USER$', sSMTPUser);
  sPage := ReplaceMacros(sPage, '$SMTP_PASSWORD$', sSMTPPassword);
  sPage := ReplaceMacros(sPage, '$SENT_FROM$', sSentFrom);

  sSentTo := StringReplace(sSentTo, ';', #13#10, [rfReplaceAll]);
  sPage := ReplaceMacros(sPage, '$SENT_TO$', sSentTo);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeRBFolders(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  Folders: IisListOfValues;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  Folders := TisListOfValues.Create;
  Folders.AddValue('TaskQueue', AParams.Value['task_queue']);
  Folders.AddValue('VMR', AParams.Value['vmr']);
  Folders.AddValue('ACH', AParams.Value['ach']);
  Folders.AddValue('ASCII', AParams.Value['ascii']);

  RB.SetFolders(AParams.Value[P_EvDomain], Folders);
end;

function TmcHttpPresenter.rhConfigRBFolders(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr: String;
  RB: IevRBControl;
  Folders: IisListOfValues;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    sPage := GetDomainContextPage('config_rb_folders.html', sDomain, sDomainDescr);

    RB := GetRBControl(True);
    Folders := RB.GetFolders(sDomain);
    sPage := ReplaceMacros(sPage, '$TASK_QUEUE$', Folders.Value['TaskQueue']);
    sPage := ReplaceMacros(sPage, '$VMR$', Folders.Value['VMR']);
    sPage := ReplaceMacros(sPage, '$ACH$', Folders.Value['ACH']);
    sPage := ReplaceMacros(sPage, '$ASCII$', Folders.Value['ASCII']);
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.GetDomainContextPage(const AResourceName, ADomain, ADomainDescr: String): String;
var
  sMIMEType: String;
begin
  Result := GetResource(AResourceName, sMIMEType);
  Result := ReplaceMacros(Result, M_EVDOMAIN, ADomain);
  if AnsiSameText(ADomain, sDefaultDomain) then
    Result := ReplaceMacros(Result, '$EVDOMAIN_NAME$', '')
  else
    Result := ReplaceMacros(Result, '$EVDOMAIN_NAME$', 'Service Bureau: ' + ADomainDescr + hfBR);
  Result := ReplaceMacros(Result, '$EVDOMAIN$', ADomain);
end;

function TmcHttpPresenter.rhChangeTempFolders(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  sHost, sParNbr, sKind: String;
  DM: ImcDMControl;
  RB: ImcDMControl;
begin
  RB := GetRBDM;

  for i := 0 to AParams.Count - 1 do
    if StartsWith(AParams[i].Name, 'rec') then
    begin
      sHost := AParams[i].Value;
      sKind := GetNextStrValue(sHost, '_');
      sParNbr := AParams[i].Name;
      GetNextStrValue(sParNbr, 'rec');

      if sKind = 'RP' then
      begin
        DM := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);
        if Assigned(DM) and (AParams.TryGetValue('Path' + sParNbr, '') <> '') then
        begin
          DM.EvolutionControl.RPControl.SetAppTempFolder(Trim(AParams.Value['Path' + sParNbr]));
          if RB = DM then
            RB.EvolutionControl.RBControl.SetAppTempFolder(Trim(AParams.Value['Path' + sParNbr]));          
        end;
      end

      else if sKind = 'RB' then
      begin
        if Assigned(RB) and (AParams.TryGetValue('Path' + sParNbr, '') <> '') then
          RB.EvolutionControl.RBControl.SetAppTempFolder(Trim(AParams.Value['Path' + sParNbr]));
      end;
    end;
end;

function TmcHttpPresenter.rhChangeRBPrinters(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  PrintersInfo: IisListOfValues;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  PrintersInfo := TisListOfValues.Create;
  PrintersInfo.AddValue('PrintingEnabled', AParams.TryGetValue('PrintingEnabled', '0') = '1');
  PrintersInfo.AddValue('DuplexMode', AParams.Value['DuplexMode']);
  PrintersInfo.AddValue('ChecksPrinter', AParams.Value['Checks_Printer']);
  PrintersInfo.AddValue('ChecksPrinter.VertOffset', AParams.Value['check_v_offset']);
  PrintersInfo.AddValue('ChecksPrinter.HorizOffset', AParams.Value['check_h_offset']);
  PrintersInfo.AddValue('ReportsPrinter', AParams.Value['Reports_Printer']);
  PrintersInfo.AddValue('ReportsPrinter.VertOffset', AParams.Value['report_v_offset']);
  PrintersInfo.AddValue('ReportsPrinter.HorizOffset', AParams.Value['report_h_offset']);

  RB.SetPrintersInfo(PrintersInfo);
end;

function TmcHttpPresenter.rhConfigRBPrinters(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage: String;
  Chb: ImcHtmlCheckBoxBuilder;
  Lb: ImcHtmlListBuilder;
  RB: IevRBControl;
  Settings: IisListOfValues;
  Printers: IisStringList;
  i: Integer;
begin
  RB := GetRBControl(True);
  Settings := RB.GetPrintersInfo;
  sPage := GetResource('config_rb_printers.html', AMIMEType);

  // Printing Enabled
  Chb := TmcHtmlCheckBoxBuilder.Create('PrintingEnabled', '1', 'Allow printing through Request Broker');
  if Settings.Value['PrintingEnabled'] then
    Chb.TryCheck('1');
  sPage := ReplaceMacros(sPage, '$PRINTING_ENABLED$', Chb.GetHtml);

  // Duplex Mode
  Lb := TmcHtmlListBuilder.Create('DuplexMode');
  Lb.AddValue('Only Reports with Duplexing', '0');
  Lb.AddValue('Always Duplex', '1');
  Lb.AddValue('Never', '2');
  Lb.SelectItem(Settings.Value['DuplexMode']);
  sPage := ReplaceMacros(sPage, '$DUPLEX_MODE$', Lb.GetHtml);

  // Checks Printer
  Lb := TmcHtmlListBuilder.Create('Checks_Printer');
  Printers := IInterface(Settings.Value['Printers']) as IisStringList;
  Lb.AddValue('Default Printer', '');
  for i := 0 to Printers.Count - 1 do
    Lb.AddValue(Printers[i], Printers[i]);
  Lb.SelectItem(Settings.Value['ChecksPrinter']);
  sPage := ReplaceMacros(sPage, '$CHECKS_PRINTER$', Lb.GetHtml);
  sPage := ReplaceMacros(sPage, '$CHECK_V_OFFSET$', Settings.Value['ChecksPrinter.VertOffset']);
  sPage := ReplaceMacros(sPage, '$CHECK_H_OFFSET$', Settings.Value['ChecksPrinter.HorizOffset']);

  // Reports Printer
  Lb := TmcHtmlListBuilder.Create('Reports_Printer');
  Printers := IInterface(Settings.Value['Printers']) as IisStringList;
  Lb.AddValue('Default Printer', '');
  for i := 0 to Printers.Count - 1 do
    Lb.AddValue(Printers[i], Printers[i]);
  Lb.SelectItem(Settings.Value['ReportsPrinter']);
  sPage := ReplaceMacros(sPage, '$REPORTS_PRINTER$', Lb.GetHtml);
  sPage := ReplaceMacros(sPage, '$REPORT_V_OFFSET$', Settings.Value['ReportsPrinter.VertOffset']);
  sPage := ReplaceMacros(sPage, '$REPORT_H_OFFSET$', Settings.Value['ReportsPrinter.HorizOffset']);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhConfigTempFolders(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  Paths: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage, sHost, s: String;
  LV: IisListOfValues;
  RPInfo: IisParamsCollection;
  i: Integer;
  RBDM: ImcDMControl;
begin
  Paths := TisParamsCollection.Create;

  RPInfo := ParallelExec('RP.GetAppTempFolder', nil);
  for i := 0 to RPInfo.Count - 1 do
    if not RPInfo[i].ValueExists('Error') then
    begin
      sHost := RPInfo.ParamName(i);
      LV := Paths.AddParams('RP_' + sHost);
      LV.AddValue('Host', sHost);
      LV.AddValue('Service', 'RP');

      s := RPInfo[i].Value[METHOD_RESULT];
      s := DenormalizePath(s);
      GetPrevStrValue(s, '\');

      LV.AddValue('Path', s);
    end;

  RBDM := GetRBDM;
  if Assigned(RBDM) then
  begin
    LV := Paths.ParamsByName('RP_' + RBDM.Host);
    if not Assigned(LV) then
    begin
      LV := Paths.AddParams('RB');
      LV.AddValue('Host', RBDM.Host);
      LV.AddValue('Service', 'RB');

      s := RBDM.EvolutionControl.RBControl.GetAppTempFolder;
      s := DenormalizePath(s);
      GetPrevStrValue(s, '\');

      LV.AddValue('Path', s);
    end
    else
      LV.AddValue('Service', 'RB,RP');
  end;

  Paths.Sort;

  TableBuilder := TmcHtmlTableBulder.Create(Paths);
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Service', 'Service');  
  LV := TableBuilder.AddColumn('Temporary Folder', 'Path');
  LV.Value['EditType'] := 'EditBox';
  LV.Value['EditParam'] := 'path';
  LV.Value['Size'] := '80';

  sPage := GetResource('config_tmp_folders.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhPSActivity(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  TableBuilder: ImcHtmlTableBulder;
  ExecStatus: IisParamsCollection;
  DS: ImcDMControl;
  sPage, s: String;
  i:Integer;
begin
  DS := Mainboard.DeploymentManagers.FindDeploymentManager(AParams.Value[P_Host]);
  ExecStatus := DS.EvolutionControl.RPControl.GetExecStatus;

  for i := 0 to ExecStatus.Count - 1 do
  begin
    s := ExecStatus[i].Value['User'];
    s := StringReplace(s, '@' + sDefaultDomain, '', [rfReplaceAll]);
    ExecStatus[i].Value['User'] := s;

    s := ExecStatus[i].Value['Method'];
    if StartsWith(s, 'AsyncFunctions.') then
      s := 'Task Request'
    else
      s := 'Real Time';
    ExecStatus[i].AddValue('MethodType', s);
  end;

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(ExecStatus);
  TableBuilder.AddColumn('Started at', 'StartTime');
  TableBuilder.AddColumn('Server Method', 'Method');
  TableBuilder.AddColumn('Type', 'MethodType');
  TableBuilder.AddColumn('User', 'User');

  sPage := GetResource('rp_activity.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_HOST, DS.Host);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.AddRefresh(const ASession: ImcSession; const ASource, AMethod: String): String;
var
  sPage: String;
  sMIME: String;

  procedure SelectValue(const ARefreshTime: Integer);
  var
    s: String;
  begin
    if ARefreshTime = ASession.RefreshTime then
      s := 'selected="selected"'
    else
      s := '';

    sPage := ReplaceMacros(sPage, '$SELECTED' + IntToStr(ARefreshTime) +'$', s);
  end;

begin
  if Pos(M_SET_REFRESH, ASource) <> 0 then
  begin
    sPage := GetResource('setrefresh.html', sMIME);
    SelectValue(0);
    SelectValue(5);
    SelectValue(30);
    SelectValue(60);

    Result := ReplaceMacros(ASource, M_SET_REFRESH, sPage);
    if ASession.RefreshTime > 0 then
    begin
      sPage := ReplaceMacros(hfRefresh, M_REFRESH_TIME, IntToStr(ASession.RefreshTime));
      sPage := ReplaceMacros(sPage, M_REDIRECT, AMethod);
      Result := ReplaceMacros(Result, M_REFRESH, sPage);
    end;
  end
  else
    Result := ASource;
end;

function TmcHttpPresenter.rhSetRefresh(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
begin
  ASession.RefreshTime := AParams.Value['refresh_time'];
end;

function TmcHttpPresenter.rhChangeAppVersion(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  Par: IisListOfValues;
  Res: IisParamsCollection;
  sFile: String;
begin
  sFile := AParams.TryGetValue('version', '');
  if sFile <> '' then
  begin
    sFile := FEvVersionFolder + sFile + '.zip';
    if not FileExists(sFile) then
      raise EisException.CreateFmt('Cannot find file %s', [sFile]);

    Par := TisListOfValues.Create;
    Par.AddValue('ArchiveFile', sFile);
    Res := ParallelExec('DM.EvInstall', Par);

    CheckParallelExecError(Res);
  end;  
end;

procedure TmcHttpPresenter.CheckParallelExecError(const AParallelExecResult: IisParamsCollection);
var
  i: Integer;
  Err, s: String;
begin
  Err := '';
  for i := 0 to AParallelExecResult.Count - 1 do
  begin
    s := AParallelExecResult[i].TryGetValue('Error', '');
    if s <> '' then
      AddStrValue(Err, 'Host ' + AParallelExecResult.ParamName(i) + ':   ' + s, #13);
  end;

  if Err <> '' then
    raise EisException.Create(Err);
end;

function TmcHttpPresenter.rhDiscoverDSServices(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  Res: IisParamsCollection;
  i: Integer;
  TableDetails: IisParamsCollection;
  LV: IisListOfValues;
  sPage, s: String;
  TableBuilder: ImcHtmlTableBulder;
begin
  Res := Mainboard.DeploymentManagers.DiscoverDeploymentManagers;
  Res.Sort;

  TableDetails := TisParamsCollection.Create;
  for i := 0 to Res.Count - 1 do
  begin
    LV := TableDetails.AddParams(IntToStr(i));
    LV.AddValue('Host', Res.ParamName(i));
    LV.AddValue('Version', Res[i].TryGetValue('Version', ''));

    s := ReplaceMacros(hfCheckBox, M_PARAM, 'Host_' + IntToStr(i));
    s := ReplaceMacros(s, M_VALUE, LV.Value['Host']);

    if Mainboard.DeploymentManagers.FindDeploymentManager(LV.Value['Host']) <> nil then
      s := ReplaceMacros(s, M_ATTRIBUTES, 'checked disabled');

    LV.AddValue('Selector', s);
  end;

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Select', 'Selector');
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Version', 'Version');  

  sPage := GetResource('discover_dm_services.html', AMIMEType);
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.rhCurrentActivity(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  TableBuilder: ImcHtmlTableBulder;
  Activity, PSExecStatus, TableDetails: IisParamsCollection;
  sPage, s: String;
  i, j:Integer;
  LV: IisListOfValues;
  StarTime: TDateTime;
  Delay, DelayCount: Integer;
begin
  Activity := ParallelExec('RP.GetExecStatus', nil);

  Delay := 0;
  DelayCount := 0;
  TableDetails := TisParamsCollection.Create;
  for j := 0 to Activity.Count - 1 do
    if not Activity[j].ValueExists('Error') then
    begin
      PSExecStatus := IInterface(Activity[j].Value[METHOD_RESULT]) as IisParamsCollection;
      for i := 0 to PSExecStatus.Count - 1 do
      begin
        LV := TableDetails.AddParams(FormatDateTime('yy/mm/dd hh:nn:ss ', PSExecStatus[i].Value['StartTime']) +
                                     FormatFloat('000', TableDetails.Count));
        LV.AddValue('Host', Activity.ParamName(j));
        LV.AddValue('StartTime', PSExecStatus[i].Value['StartTime']);
        LV.AddValue('Method', PSExecStatus[i].Value['Method']);

        s := PSExecStatus[i].Value['User'];
        s := StringReplace(s, '@' + sDefaultDomain, '', [rfReplaceAll]);
        LV.AddValue('User', s);

        s := PSExecStatus[i].Value['Method'];
        if StartsWith(s, 'AsyncFunctions.') then
          s := 'Task Request'
        else
          s := 'Real Time';

        LV.AddValue('MethodType', s);

        StarTime := StrToDateTimeDef(PSExecStatus[i].Value['StartTime'], 0);
        if (StarTime <> 0) and (StarTime < Now) then
        begin
          Delay := Delay + SecondsBetween(Now, StarTime);
          Inc(DelayCount);
        end;
      end;
    end;

  if DelayCount > 0 then
    Delay := Round(Delay/DelayCount);

  TableDetails.Sort;

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Started at', 'StartTime');
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Server Method', 'Method');
  TableBuilder.AddColumn('Type', 'MethodType');
  TableBuilder.AddColumn('User', 'User');

  sPage := GetResource('current_activity.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$EXEC_DELAY$', PeriodToString(Delay * 1000));
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhAllLogs(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
const PageSize = 50;
var
  LogData, RDLogResult, PSLogResult, RRLogResult,
  AALogResult, ACLogResult, ASLogResult, DSLogResult, ECLogResult, TableDetails: IisParamsCollection;
  LV: IisListOfValues;
  sType, sTitle, sPage: String;
  TableBuilder: ImcHtmlTableBulder;
  RDds: ImcDMControl;
  Params: IisListOfValues;
  LogQuery: TEventQuery;

  procedure AddLogToResult(const AType: String; const AServerData: IisParamsCollection);
  var
    i, j: Integer;
    LV: IisListOfValues;
    LogData: IisParamsCollection;
    sHost, sLink_Details, s: String;
    EventType: TLogEventType;
    HREF: ImcHREF;
  begin
    for i := 0 to AServerData.Count - 1 do
      if not AServerData[i].ValueExists('Error') then
      begin
        sHost := AServerData.ParamName(i);
        LogData := IInterface(AServerData[i].Value[METHOD_RESULT]) as IisParamsCollection;
        for j := 0 to LogData.Count - 1 do
        begin
          LV := TableDetails.AddParams(FormatDateTime('yy/mm/dd hh:nn:ss ', LogData[j].Value['TimeStamp']) +
                                       FormatFloat('000', TableDetails.Count));

          LV.AddValue('TimeStamp', LogData[j].Value['TimeStamp']);
          LV.AddValue('Host', sHost);

          if AType = 'AA' then
            LV.AddValue('Type', 'API')          
          else
            LV.AddValue('Type', AType);

          EventType := TLogEventType(LogData[j].Value['EventType']);
          s := LogEventNames[EventType];
          if EventType >= etFatalError then
            s := ReplaceMacros(hfAlertText, M_CONTENT, s);
          LV.AddValue('EventType', s);

          LV.AddValue('EventClass', LogData[j].Value['EventClass']);
          s := LogData[j].Value['Text'];
          if Length(s) > 40 then
            s := Copy(s, 1, 40) + ' ...';
          LV.AddValue('Text', s);

          // Add Details links
          HREF := TmcHREF.Create('log_details', ASession);
          HREF.AddParam(P_Type, AType);
          HREF.AddParam(P_Host, sHost);
          HREF.AddParam(P_ID, LogData[j].Value['EventNbr']);
          sLink_Details := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
          sLink_Details := ReplaceMacros(sLink_Details, M_CONTENT, 'Details');
          LV.AddValue('Link_Details', sLink_Details);

          // cut off if log is huge
          if j = 20 then
            break;
        end;
      end;
  end;

begin
  // collect log data
  sType := AParams.TryGetValue(P_Type, '');
  sTitle := '';

  RDLogResult := nil;
  PSLogResult := nil;
  RRLogResult := nil;
  AALogResult := nil;
  ACLogResult := nil;
  ASLogResult := nil;
  DSLogResult := nil;
  ECLogResult := nil;

  Params := TisListOfValues.Create;
  Params.AddValue('StartTime', 0);
  Params.AddValue('EndTime', IncHour(Now, -12));
  Params.AddValue('Types', LogEventTypesToByte([etWarning, etError, etFatalError]));
  Params.AddValue('EventClass', '');
  Params.AddValue('MaxCount', 20);  

  if (sType = '') or AnsiSameText(sType, 'RP') then
  begin
    PSLogResult := ParallelExec('RP.GetFilteredLogEvents', Params);
    sTitle := 'All Request Processors Log';
  end;

  if (sType = '') or AnsiSameText(sType, 'RR') then
  begin
    RRLogResult := ParallelExec('RR.GetFilteredLogEvents', Params);
    sTitle := 'All Remote Relays Log';
  end;

  if (sType = '') or AnsiSameText(sType, 'AA') then
  begin
    AALogResult := ParallelExec('AA.GetFilteredLogEvents', Params);
    sTitle := 'All API Log';
  end;

  if (sType = '') or AnsiSameText(sType, 'AC') then
  begin
    ACLogResult := ParallelExec('AC.GetFilteredLogEvents', Params);
    sTitle := 'All ADR Client Log';
  end;

  if (sType = '') or AnsiSameText(sType, 'AS') then
  begin
    ASLogResult := ParallelExec('AS.GetFilteredLogEvents', Params);
    sTitle := 'All ADR Server Log';
  end;

  if (sType = '') or AnsiSameText(sType, 'DM') then
  begin
    DSLogResult := ParallelExec('DM.GetFilteredLogEvents', Params);
    sTitle := 'All Deployment Servers Log';
  end;

  if sType = '' then
  begin
    LogQuery.StartTime := Params.Value['StartTime'];
    LogQuery.EndTime := Params.Value['EndTime'];
    LogQuery.Types := ByteToLogEventTypes(Params.Value['Types']);
    LogQuery.EventClass := Params.Value['EventClass'];
    LogQuery.MaxCount := Params.Value['MaxCount'];

    LogData := Mainboard.GetFilteredLogEvents(LogQuery);
    ECLogResult := TisParamsCollection.Create;
    LV := ECLogResult.AddParams(GetComputerNameString);
    LV.AddValue(METHOD_RESULT, LogData);

    RDds := GetRBDM;
    if Assigned(RDds) then
    begin
      try
        LogData := RDds.EvolutionControl.RBControl.GetFilteredLogEvents(LogQuery);
        RDLogResult := TisParamsCollection.Create;
        LV := RDLogResult.AddParams(RDds.Host);
        LV.AddValue(METHOD_RESULT, LogData);
      except
      end;
    end;

    sTitle := 'System Logs';
  end;

  TableDetails := TisParamsCollection.Create;

  if Assigned(RDLogResult) then
    AddLogToResult('RB', RDLogResult);
  if Assigned(PSLogResult) then
    AddLogToResult('RP', PSLogResult);
  if Assigned(RRLogResult) then
    AddLogToResult('RR', RRLogResult);
  if Assigned(AALogResult) then
    AddLogToResult('AA', AALogResult);
  if Assigned(ACLogResult) then
    AddLogToResult('AC', ACLogResult);
  if Assigned(ASLogResult) then
    AddLogToResult('AS', ASLogResult);
  if Assigned(DSLogResult) then
    AddLogToResult('DM', DSLogResult);
  if Assigned(ECLogResult) then
    AddLogToResult('MC', ECLogResult);

  TableDetails.Sort('desc');

  // Build table
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Time', 'TimeStamp');
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Service', 'Type');  
  TableBuilder.AddColumn('Event Type', 'EventType');
  TableBuilder.AddColumn('Module', 'EventClass');
  TableBuilder.AddColumn('Message', 'Text');
  TableBuilder.AddColumn('Details', 'Link_Details');

  sPage := GetResource('log.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$PAGES$', '');
  sPage := ReplaceMacros(sPage, M_CAPTION, sTitle);
  sPage := ReplaceMacros(sPage, M_HOST, '');
  sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhLogin(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
begin
  Result := GetResource('login.html', AMIMEType);
  Result := ReplaceMacros(Result, '$VERSION$', Format('v%s (%s)', [IntToStr(EvMajorVerNbr), EvVerName]));

  if AParams.ValueExists('Error') then
    Result := ReplaceMacros(Result, '$ERROR$', AParams.Value['Error'])
  else
    Result := ReplaceMacros(Result, '$ERROR$', '');
end;

function TmcHttpPresenter.rhAuthorization(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  HRef: ImcHREF;
begin
  ASession.Authorize(AParams.Value['user'], AParams.Value['password']);
  if ASession.Authorized then
  begin
    HRef := TmcHREF.Create('main', ASession);
    AParams.AddValue(P_Redirect, HRef.GetHtml);
    Result := '';
  end
  else
  begin
    AParams.AddValue('Error', 'User or password is incorrect');
    AParams.DeleteValue(P_Redirect);
    Result := rhLogin(ASession, AParams, AMIMEType);
  end;
end;

function TmcHttpPresenter.rhMCUsers(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  Users: IisParamsCollection;
  LV: IisListOfValues;
  i: Integer;
  sPage: String;
  TableBuilder: ImcHtmlTableBulder;
  Usr: ImcUserAccount;
  CheckBuilder: ImcHtmlCheckBoxBuilder;
begin
  Users := TisParamsCollection.Create;
  Mainboard.UserAccounts.Lock;
  try
    for i := 0 to Mainboard.UserAccounts.Count - 1 do
    begin
      Usr := Mainboard.UserAccounts[i];
      LV := Users.AddParams(Usr.UserName);
      LV.AddValue('User', Usr.UserName);
      LV.AddValue('Password', cDummyPassword);
      LV.AddValue('Type', Usr.AccountType);

      CheckBuilder := TmcHtmlCheckBoxBuilder.Create('delete' + IntToStr(i), Usr.UserName, '');
      LV.AddValue('Delete', CheckBuilder.GetHtml);
    end;
  finally
    Mainboard.UserAccounts.Unlock;
  end;

  // Blank line for new record
  LV := Users.AddParams('New');
  LV.AddValue('User', '');
  LV.AddValue('Password', '');
  LV.AddValue('Type', 0);

  TableBuilder := TmcHtmlTableBulder.Create(Users);
  LV := TableBuilder.AddColumn('User Name', 'User');
  LV.Value['EditType'] := 'EditBox';
  LV.Value['EditParam'] := 'user';
  LV.Value['Size'] := '16';

  LV := TableBuilder.AddColumn('Password', 'Password');
  LV.Value['EditType'] := 'EditPassword';
  LV.Value['EditParam'] := 'password';
  LV.Value['Size'] := '16';

  LV := TableBuilder.AddColumn('Type', 'Type');
  LV.Value['EditType'] := 'List';
  LV.Value['EditParam'] := 'type';
  LV.Value['Options'] := 'Unassigned=0'#13'Restricted User=1'#13'Power User=2'#13'Administrator=3';

  LV := TableBuilder.AddColumn('Delete', 'Delete');

  sPage := GetResource('edit_mc_users.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$MC_USERS$', TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeMCUsers(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sParNbr: String;
  i: Integer;
  NewRec: String;

  procedure UpdateUser(const AOldUserName, AUserName, APassword: String; const AAccountType: TmcUserAccountType);
  var
    Usr: ImcUserAccount;
  begin
    Usr := Mainboard.UserAccounts.FindUser(AOldUserName);
    if Assigned(Usr) then
    begin
      Usr.UserName := AUserName;
      if APassword <> cDummyPassword then
        Usr.Password := APassword;
      Usr.AccountType := AAccountType;      
    end;
  end;

begin
  Mainboard.UserAccounts.Lock;
  try
    // delete users
    for i := 0 to AParams.Count - 1 do
      if StartsWith(AParams[i].Name, 'delete') then
        Mainboard.UserAccounts.DeleteUser(AParams[i].Value);

    // update users
    NewRec := '';
    for i := 0 to AParams.Count - 1 do
      if StartsWith(AParams[i].Name, 'rec') then
      begin
        sParNbr := AParams[i].Name;
        GetNextStrValue(sParNbr, 'rec');
        if AParams[i].Value = 'New' then
          NewRec := sParNbr
        else
          UpdateUser(AParams[i].Value,
                     AParams.Value['User' + sParNbr],
                     AParams.Value['Password' + sParNbr],
                     TmcUserAccountType(AParams.Value['Type' + sParNbr]));
      end;


    // add new user
    if AParams.Value['User' + NewRec] <> '' then
    begin
      Mainboard.UserAccounts.AddUser(AParams.Value['User' + NewRec]);
      UpdateUser(AParams.Value['User' + NewRec],
                 AParams.Value['User' + NewRec],
                 AParams.Value['Password' + NewRec],
                 TmcUserAccountType(AParams.Value['Type' + NewRec]));
    end;


  finally
    Mainboard.UserAccounts.Unlock;
  end;

  Result := '';
end;

function TmcHttpPresenter.rhLogout(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
begin
  ASession.Authorize('', '');
  AParams.AddValue(P_Redirect, 'login');
  Result := '';
end;

function TmcHttpPresenter.rhDBMaintenance(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  sPage: String;
  RPParExecRes, StatusTableData: IisParamsCollection;
  SrvData, LV, DomainList: IisListOfValues;
  RB: IevRBControl;
  s, sStatus, sStartMaintenance, sScheduler: String;
  TableBuilder: ImcHtmlTableBulder;
  BtnBuilder: ImcHtmlButtonBuilder;
  LstBuilder: ImcHtmlListBuilder;
  EditBoxBuilder: ImcHtmlEditBoxBuilder;

  procedure AddServerDataToResult(const ADBData: IisListOfValues);
  var
    i: Integer;
    DomainData: IisListOfValues;
    DBList: IisStringList;
    s: String;
  begin
    // go Domain by Domain and add DB info
    for i := 0 to ADBData.Count - 1 do
    begin
      DBList := IInterface(ADBData[i].Value) as IisStringList;
      DomainData := StatusTableData.ParamsByName(ADBData[i].Name);
      if not Assigned(DomainData) then
      begin
        DomainData := StatusTableData.AddParams(ADBData[i].Name);
        s := ADBData[i].Name;
        if Assigned(DomainList) then
          s := DomainList.TryGetValue(s, s);
        DomainData.AddValue('SB', s);
      end;

      if DomainData.TryGetValue('DBList', DBList.CommaText) <> DBList.CommaText then
        DomainData.AddValue('DBList', 'Invalid status. Please click "Stop" to fix the problem.')  // it's just in case, should not happen
      else
        DomainData.AddValue('DBList', DBList.CommaText);
    end;
  end;


  function BuildScheduler: String;
  var
    i: Integer;
    s: String;
    LstBuilder: ImcHtmlListBuilder;
    EditBoxBuilder: ImcHtmlEditBoxBuilder;
    SchedEvent: IisScheduleEventWeekly;

    function BuildSchedDay(const ADay: TDayOfWeek): String;
    var
      CheckBoxBuilder: ImcHtmlCheckBoxBuilder;
    begin
      CheckBoxBuilder := TmcHtmlCheckBoxBuilder.Create('sched_' + DayShortNames[ADay], '1', DayLongNames[ADay]);
      CheckBoxBuilder.CaptionLocation := lRight;
      if Assigned(SchedEvent) and (ADay in SchedEvent.RunDays) then
       CheckBoxBuilder.TryCheck('1');
      Result := CheckBoxBuilder.GetHtml;
    end;
  begin
    Result := '';

    Mainboard.Schedule.Lock;
    try
      SchedEvent := nil;
      for i := 0 to Mainboard.Schedule.Count - 1 do
        if (Mainboard.Schedule.GetEventByIndex(i).TaskID = 'Evolution.StartDBMaintenance') then
        begin
          SchedEvent := Mainboard.Schedule.GetEventByIndex(i) as IisScheduleEventWeekly;
          break;
        end;
    finally
      Mainboard.Schedule.Unlock;
    end;
    LstBuilder := TmcHtmlListBuilder.Create('sched_type');
    LstBuilder.AddValue('Never', '0');    
    LstBuilder.AddValue('Weekly', '1');
    if Assigned(SchedEvent) then
      LstBuilder.SelectItem('1');

    Result := 'Schedule DB Maintenance ' + LstBuilder.GetHtml;

    LstBuilder := TmcHtmlListBuilder.Create('sched_time');
    LstBuilder.AddValue('12:00 AM', '12:00 AM');
    LstBuilder.AddValue('1:00 AM', '1:00 AM');
    LstBuilder.AddValue('2:00 AM', '2:00 AM');
    LstBuilder.AddValue('3:00 AM', '3:00 AM');
    LstBuilder.AddValue('4:00 AM', '4:00 AM');
    LstBuilder.AddValue('5:00 AM', '5:00 AM');
    LstBuilder.AddValue('6:00 AM', '6:00 AM');
    LstBuilder.AddValue('7:00 AM', '7:00 AM');
    LstBuilder.AddValue('8:00 AM', '8:00 AM');
    LstBuilder.AddValue('9:00 AM', '9:00 AM');
    LstBuilder.AddValue('10:00 AM', '10:00 AM');
    LstBuilder.AddValue('11:00 AM', '11:00 AM');
    LstBuilder.AddValue('12:00 PM', '12:00 PM');
    LstBuilder.AddValue('1:00 PM', '1:00 PM');
    LstBuilder.AddValue('2:00 PM', '2:00 PM');
    LstBuilder.AddValue('3:00 PM', '3:00 PM');
    LstBuilder.AddValue('4:00 PM', '4:00 PM');
    LstBuilder.AddValue('5:00 PM', '5:00 PM');
    LstBuilder.AddValue('6:00 PM', '6:00 PM');
    LstBuilder.AddValue('7:00 PM', '7:00 PM');
    LstBuilder.AddValue('8:00 PM', '8:00 PM');
    LstBuilder.AddValue('9:00 PM', '9:00 PM');
    LstBuilder.AddValue('10:00 PM', '10:00 PM');
    LstBuilder.AddValue('11:00 PM', '11:00 PM');
    if Assigned(SchedEvent) then
    begin
      s := FormatDateTime('h:nn AM/PM', SchedEvent.StartTime);
      LstBuilder.SelectItem(s);
    end;
    Result := Result + Spaces(5) + 'at ' + LstBuilder.GetHtml;

    if Assigned(SchedEvent) then
      s := IntToStr(SchedEvent.EveryNbrWeek)
    else
      s := '1';
    EditBoxBuilder := TmcHtmlEditBoxBuilder.Create('sched_nbrweeks', s);
    EditBoxBuilder.Size := 2;
    Result := Result + hfBR + hfBR + 'Every ' + EditBoxBuilder.GetHtml + ' week(s) on:';

    Result := Result + hfBR + hfBR + BuildSchedDay(dwMonday);
    Result := Result + Spaces(3) + BuildSchedDay(dwTuesday);
    Result := Result + Spaces(3) + BuildSchedDay(dwWednesday);
    Result := Result + Spaces(3) + BuildSchedDay(dwThursday);
    Result := Result + Spaces(3) + BuildSchedDay(dwFriday);
    Result := Result + Spaces(3) + BuildSchedDay(dwSaturday);
    Result := Result + Spaces(3) + BuildSchedDay(dwSunday);

    if Assigned(SchedEvent) then
      s := SchedEvent.TaskParams.Value['Duration']
    else
      s := '5';
    EditBoxBuilder := TmcHtmlEditBoxBuilder.Create('tsk_duration', s);
    EditBoxBuilder.Size := 2;
    Result := Result + hfBR + hfBR + 'Run for ' + EditBoxBuilder.GetHtml + ' hours';

    if Assigned(SchedEvent) and (SchedEvent.LastRunDate <> 0) then
      Result := Result + hfBR + hfBR + 'Last run date: ' + FormatDateTime('mm/dd/yyyy h:nn AM/PM', SchedEvent.LastRunDate);
  end;

begin
  // Add all DB in maintenance in StatusTableData
  RB := GetRBControl(True);
  DomainList := GetEvDomainList;
  RPParExecRes := ParallelExec('RP.GetMaintenanceDB', nil);

  StatusTableData := TisParamsCollection.Create;
  // go RP by RP
  for i := 0 to RPParExecRes.Count - 1 do
    if not RPParExecRes[i].ValueExists('Error') then
    begin
      SrvData := IInterface(RPParExecRes[i].Value[METHOD_RESULT]) as IisListOfValues;
      AddServerDataToResult(SrvData);
    end;

  // add RB info as well
  if Assigned(RB) then
    AddServerDataToResult(RB.GetMaintenanceDB);

  // Filter out empty domains
  for i := StatusTableData.Count - 1 downto 0 do
  begin
    s := StatusTableData[i].Value['DBList'];
    if s = '' then  // no DB maintenance for this domain
      StatusTableData.DeleteParams(i)
    else
    begin
      if s = '*' then
        StatusTableData[i].Value['DBList'] := 'All DBs';
      BtnBuilder := TmcHtmlButtonBuilder.Create('manual_dbmaintenance', 'Stop Maintenance', True, ASession);
      BtnBuilder.Confirmation := 'Are you sure you want to stop maintenance for selected database(s)?';
      BtnBuilder.AddMethodParam(P_Type, 'stop');
      BtnBuilder.AddMethodParam(P_EvDomain, StatusTableData.ParamName(i));
      StatusTableData[i].AddValue('Action', BtnBuilder.GetHtml);
    end;
  end;
  StatusTableData.Sort;

  // Build status table
  if StatusTableData.Count > 0 then
  begin
    TableBuilder := TmcHtmlTableBulder.Create(StatusTableData);
    LV := TableBuilder.AddColumn('Service Bureau', 'SB');
    LV := TableBuilder.AddColumn('Databases', 'DBList');
    LV := TableBuilder.AddColumn('Action', 'Action');
    sStatus := TableBuilder.GetHtml;
  end
  else
    sStatus := MakeHtmlText('Currently there are no databases in maintenance');

  // Build add DB frame
  sStartMaintenance := '';
  if Assigned(DomainList) then
  begin
    if DomainList.Count > 1 then
    begin
      LstBuilder := TmcHtmlListBuilder.Create(P_EvDomain);
      LstBuilder.AddValue('All', '');
      for i := 0 to DomainList.Count - 1 do
        LstBuilder.AddValue(DomainList[i].Value, DomainList[i].Name);
      sStartMaintenance := MakeHtmlText('Service Bureau') + hfSpace + LstBuilder.GetHtml + hfBR + hfBR;
    end
    else
      sStartMaintenance := '';

    EditBoxBuilder := TmcHtmlEditBoxBuilder.Create('DB', '');
    EditBoxBuilder.Size := 60;
    sStartMaintenance := sStartMaintenance + MakeHtmlText('Client database number') + hfSpace + EditBoxBuilder.GetHtml;
  end;

  // Build scheduler
  sScheduler := BuildScheduler;

  // Prepare the final result
  sPage := GetResource('db_maintenance.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$STATUS$', hfBR + sStatus);
  sPage := ReplaceMacros(sPage, '$START_MAINTENANCE$', hfBR + sStartMaintenance);
  sPage := ReplaceMacros(sPage, '$SCHEDULER$', hfBR + sScheduler);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhUserMessage(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  LV: IisListOfValues;
  i: Integer;
  SendTo: ImcHtmlListBuilder;
  Users: IisParamsCollection;
  s, sPage: String;
  sLastSentUserMessage: String;
begin
  RB := GetRBControl(True);

  // Build SendTo list
  LV := GetEvDomainList;
  LV.Sort;
  SendTo := TmcHtmlListBuilder.Create('SendTo');
  SendTo.AddValue('Everyone', 'Everyone');
  for i := 0 to LV.Count - 1 do
    SendTo.AddValue('Everyone at ' + LV[i].Value, 'Everyone@' + LV[i].Name);

  Users := RB.UsersPoolStatus;
  for i := 0 to Users.Count - 1 do
    (Users[i] as IisCollection).Name := Users[i].Value['Login'] + ' ' + (Users[i] as IisCollection).Name;
  Users.Sort;

  for i := 0 to Users.Count - 1 do
  begin
    s := Users[i].Value['Login'];
    s := StringReplace(s, '@' + sDefaultDomain, '', [rfReplaceAll]);
    SendTo.AddValue(s, Users[i].Value['Login']);
  end;

  sPage := GetResource('user_message.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$SEND_TO_LIST$', SendTo.GetHtml);
  sLastSentUserMessage := Mainboard.AppSettings.GetValue('Settings\LastSentUserMessage', '');
  sPage := ReplaceMacros(sPage, '$LAST_SENT_USER_MESSAGE$', sLastSentUserMessage);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

procedure TmcHttpPresenter.SetUseSSL(const AValue: Boolean);
begin
  if FUseSSL <> AValue then
  begin
    FUseSSL := AValue;
    SyncTCPSettings;
  end;
end;

function TmcHttpPresenter.rhChangeRR(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  sHost, sParNbr, sPorts: String;
  RB, DM: ImcDMControl;
  Settings: IisListOfValues;
begin
  RB := GetRBDM;
  for i := 0 to AParams.Count - 1 do
    if StartsWith(AParams[i].Name, 'rec') then
    begin
      sHost := AParams[i].Value;
      sParNbr := AParams[i].Name;
      GetNextStrValue(sParNbr, 'rec');

      DM := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);
      if Assigned(DM) then
      begin
        sPorts := '';
        if AParams.TryGetValue('Port' + IntToStr(TCPClient_PortSSL1) + '_' + sParNbr, '0') = '1' then
          AddStrValue(sPorts, IntToStr(TCPClient_PortSSL1));
        if AParams.TryGetValue('Port' + IntToStr(TCPClient_PortSSL2) + '_' + sParNbr, '0') = '1' then
          AddStrValue(sPorts, IntToStr(TCPClient_PortSSL2));
        if AParams.TryGetValue('Port' + IntToStr(TCPClient_PortSSL3) + '_' + sParNbr, '0') = '1' then
          AddStrValue(sPorts, IntToStr(TCPClient_PortSSL3));
        if AParams.TryGetValue('Custom_' + sParNbr, '') <> '' then
          AddStrValue(sPorts, String(AParams.Value['Custom_' + sParNbr]));

        Settings := TisListOfValues.Create;
        if Assigned(RB) then
          Settings.AddValue('RequestBroker', RB.Host);
        Settings.AddValue('ClientPorts', sPorts);

        if AParams.TryGetValue('MinCompression_' + sParNbr, '') <> '' then
          Settings.AddValue('MinCompression', String(AParams.Value['MinCompression_' + sParNbr]));

        DM.EvolutionControl.RRControl.SetSettings(Settings);
      end;
    end;

  Result := '';
end;

function TmcHttpPresenter.rhConfigRR(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  TableDetails, InstalledRR: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage: String;
  i, j: Integer;
  RRSettings, TblRow, TblCol: IisListOfValues;
  Ports: IisStringList;
  sHost: String;
begin
  InstalledRR := ParallelExec('RR.GetSettings', nil);
  FilterParallelExecResult(InstalledRR);

  if InstalledRR.Count > 0 then
  begin
    TableDetails := TisParamsCollection.Create;
    Ports := TisStringList.Create;
    for i := 0 to InstalledRR.Count - 1 do
      if not InstalledRR[i].ValueExists('Error') then
      begin
        sHost := InstalledRR.ParamName(i);
        RRSettings := IInterface(InstalledRR[i].Value[METHOD_RESULT]) as IisListOfValues;
        TblRow := TableDetails.AddParams(sHost);
        TblRow.AddValue('Host', sHost);
        Ports.CommaText := RRSettings.Value['ClientPorts'];

        j := Ports.IndexOf(IntToStr(TCPClient_PortSSL1));
        if j <> -1 then
        begin
          TblRow.AddValue('Port1', '1');
          Ports.Delete(j);
        end
        else
          TblRow.AddValue('Port1', '');

        j := Ports.IndexOf(IntToStr(TCPClient_PortSSL2));
        if j <> -1 then
        begin
          TblRow.AddValue('Port2', '1');
          Ports.Delete(j);
        end
        else
          TblRow.AddValue('Port2', '');

        j := Ports.IndexOf(IntToStr(TCPClient_PortSSL3));
        if j <> -1 then
        begin
          TblRow.AddValue('Port3', '1');
          Ports.Delete(j);
        end
        else
          TblRow.AddValue('Port3', '');

        TblRow.AddValue('Custom', Ports.CommaText);
        TblRow.AddValue('MinCompression', RRSettings.Value['MinCompression']);
      end;

    // Build table
    TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
    TableBuilder.AddColumn('Host', 'Host');
    TblCol := TableBuilder.AddColumn(IntToStr(TCPClient_PortSSL1), 'Port1');
    TblCol.AddValue('EditType', 'Checkbox');
    TblCol.AddValue('EditParam', 'Port' + IntToStr(TCPClient_PortSSL1) + '_');
    TblCol := TableBuilder.AddColumn(IntToStr(TCPClient_PortSSL2), 'Port2');
    TblCol.AddValue('EditType', 'Checkbox');
    TblCol.AddValue('EditParam', 'Port' + IntToStr(TCPClient_PortSSL2) + '_');
    TblCol := TableBuilder.AddColumn(IntToStr(TCPClient_PortSSL3),  'Port3');
    TblCol.AddValue('EditType', 'Checkbox');
    TblCol.AddValue('EditParam', 'Port' + IntToStr(TCPClient_PortSSL3) + '_');
    TblCol := TableBuilder.AddColumn('Custom', 'Custom');
    TblCol.AddValue('EditType', 'Editbox');
    TblCol.AddValue('EditParam', 'Custom_');
    TblCol.AddValue('Size', '12');
    TblCol := TableBuilder.AddColumn('Compression', 'MinCompression');
    TblCol.AddValue('EditType', 'List');
    TblCol.AddValue('EditParam', 'MinCompression_');
    TblCol.Value['Options'] := 'None=0'#13'Good=1'#13'Better=2'#13'Best=3';

    sPage := GetResource('config_rr.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhConfigAA(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  TableDetails, InstalledAA: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage: String;
  i: Integer;
  AASettings, TblRow, TblCol: IisListOfValues;
  sHost: String;
begin
  InstalledAA := ParallelExec('AA.GetSettings', nil);
  FilterParallelExecResult(InstalledAA);

  if InstalledAA.Count > 0 then
  begin
    TableDetails := TisParamsCollection.Create;
    for i := 0 to InstalledAA.Count - 1 do
      if not InstalledAA[i].ValueExists('Error') then
      begin
        sHost := InstalledAA.ParamName(i);
        AASettings := IInterface(InstalledAA[i].Value[METHOD_RESULT]) as IisListOfValues;
        TblRow := TableDetails.AddParams(sHost);
        TblRow.AddValue('Host', sHost);
        TblRow.AddValue('XmlRpcPort', AASettings.Value['XmlRpcPort']);
        if AASettings.Value['XmlRpcSSL'] then
          TblRow.AddValue('XmlRpcSSL', '1')
        else
          TblRow.AddValue('XmlRpcSSL', '');
      end;

    // Build table
    TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
    TableBuilder.AddColumn('Host', 'Host');

    TblCol := TableBuilder.AddColumn('XML RPC Port', 'XmlRpcPort');
    TblCol.AddValue('EditType', 'Editbox');
    TblCol.AddValue('EditParam', 'XmlRpcPort');
    TblCol.AddValue('Size', '12');

    TblCol := TableBuilder.AddColumn('SSL', 'XmlRpcSSL');
    TblCol.AddValue('EditType', 'Checkbox');
    TblCol.AddValue('EditParam', 'XmlRpcSSL');
    TblCol.AddValue('Enabled', False);

    sPage := GetResource('config_aa.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeAA(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  sHost, sParNbr: String;
  RB, DM: ImcDMControl;
  AAControl: IevAAControl;
  Settings: IisListOfValues;
begin
  RB := GetRBDM;
  for i := 0 to AParams.Count - 1 do
    if StartsWith(AParams[i].Name, 'rec') then
    begin
      sHost := AParams[i].Value;
      sParNbr := AParams[i].Name;
      GetNextStrValue(sParNbr, 'rec');

      DM := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);
      if Assigned(DM) then
      begin
        AAControl := DM.EvolutionControl.GetAAControl;
        if Assigned(AAControl) then
        begin
          Settings := TisListOfValues.Create;
          if Assigned(RB) then
            Settings.AddValue('RequestBroker', RB.Host);
          Settings.AddValue('XmlRpcPort', AParams.Value['XmlRpcPort' + sParNbr]);
          Settings.AddValue('XmlRpcSSL', AParams.TryGetValue('XmlRpcSSL' + sParNbr, '1') = '1');
          DM.EvolutionControl.AAControl.SetSettings(Settings);
        end;
      end;
    end;

  Result := '';
end;

function TmcHttpPresenter.rhSendUserMessage(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
begin
  RB := GetRBControl;
  if Assigned(RB) then
  begin
    RB.SendMessage(AParams.Value['message'], AParams.Value['SendTo']);
    Mainboard.AppSettings.SetValue('Settings\LastSentUserMessage', AParams.Value['message']);
  end;
  Result := '';
end;

function TmcHttpPresenter.rhManualDBMaintenance(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;

  procedure StartMaintenance;
  var
    sDomain: String;
    DomainList, DBListByDomains: IisListOfValues;
    DBList: IisStringList;
    i: Integer;
    sDBList, s: String;

    procedure AddDBsToList(const ADomainName: String; const ADBList: IisStringList);
    var
      L: IisStringList;
      i: Integer;
    begin
      // add only missing DBs
      if DBListByDomains.ValueExists(ADomainName) then
      begin
        L := IInterface(DBListByDomains.Value[ADomainName]) as IisStringList;
        if (ADBList.Count = 0) or (ADBList.Count = 1) and (ADBList[0] = '*') then
          L.Text := ADBList.Text
        else if not((L.Count = 1) and (L[0] = '*')) then
        begin
          L.Duplicates := dupIgnore;
          L.Sorted := True;          
          for i := 0 to ADBList.Count - 1 do
            L.Add(ADBList[i]);
        end;
      end
      else
        DBListByDomains.AddValue(ADomainName, ADBList);
    end;

  begin
    // Create DB list
    DBList := TisStringList.Create;
    if AParams.ValueExists('btn_dblist') then
    begin
      sDBList := Trim(AParams.Value['DB']);

      if sDBList <> '' then
        while sDBList <> '' do
        begin
          s := GetNextStrValue(sDBList, ',');
          DBList.Add('CL_' + Trim(s));
        end;
    end
    else
      DBList.Add('*'); // all DBs

    if DBList.Count > 0 then
    begin
      // Create method params
      DBListByDomains := RB.GetMaintenanceDB; // Ask RB for existing status
      sDomain := AParams.TryGetValue(P_EvDomain, '');
      if sDomain = '' then
      begin
        DomainList := GetEvDomainList;
        for i := 0 to DomainList.Count - 1 do
          AddDBsToList(DomainList[i].Name, DBList);
      end
      else
        AddDBsToList(sDomain, DBList);

      SetMaintenanceDB(DBListByDomains, True);
    end;  
  end;


  procedure StopMaintenance;
  var
    sDomain: String;
    DomainList, DBListByDomains: IisListOfValues;
    DBList: IisStringList;
    i: Integer;
  begin
    // Create empty DB list
    DBList := TisStringList.Create;
    DBList.Add('*');

    // Create method params
    DBListByDomains := TisListOfValues.Create;
    sDomain := AParams.TryGetValue(P_EvDomain, '');
    if sDomain = '' then
    begin
      DomainList := GetEvDomainList;
      for i := 0 to DomainList.Count - 1 do
        DBListByDomains.AddValue(DomainList[i].Name, DBList);
    end
    else
      DBListByDomains.AddValue(sDomain, DBList);

    SetMaintenanceDB(DBListByDomains, False);
  end;

begin
  RB := GetRBControl(True);
  if AnsiSameText(AParams.Value[P_Type], 'start') then
    StartMaintenance
  else if AnsiSameText(AParams.Value[P_Type], 'stop') then
    StopMaintenance;
end;

procedure TmcHttpPresenter.SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
var
  RB: IevRBControl;
  ParExeParams: IisListOfValues;
  Res: IisParamsCollection;
begin
 // call method for RB and RPs

  RB := GetRBControl;
  if Assigned(RB) then
    RB.SetMaintenanceDB(ADBListByDomains, AMaintenance);

  ParExeParams := TisListOfValues.Create;
  ParExeParams.AddValue('ADBListByDomains', ADBListByDomains);
  ParExeParams.AddValue('AMaintenance', AMaintenance);
  Res := ParallelExec('RP.SetMaintenanceDB', ParExeParams);
  FilterParallelExecResult(Res);
  CheckParallelExecError(Res);
end;

function TmcHttpPresenter.rhChangeScheduledDBMaintenance(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  NewEvent: IisScheduleEventWeekly;
  DomainList, DBListByDomains: IisListOfValues;
  DBList: IisStringList;
  WeekDays: TDaysOfWeek;
  RB: IevRBControl;
begin
  RB := GetRBControl(True);

  // Create method params
  DBList := TisStringList.Create;
  DBList.Add('*');  
  DBListByDomains := TisListOfValues.Create;
  DomainList := GetEvDomainList;
  for i := 0 to DomainList.Count - 1 do
    DBListByDomains.AddValue(DomainList[i].Name, DBList);

  if AnsiSameText(AParams.Value['sched_type'], '1') then
  begin
    NewEvent := TisScheduleEventWeekly.Create;
    NewEvent.Description := 'Evolution DB Maintenance';
    NewEvent.TaskID := 'Evolution.StartDBMaintenance';    
    NewEvent.StartTime := StrToTime(AParams.Value['sched_time']);
    NewEvent.EveryNbrWeek := AParams.Value['sched_nbrweeks'];
    NewEvent.TaskParams.AddValue('Domains', DBListByDomains.AsStream);
    NewEvent.TaskParams.AddValue('Duration', AParams.Value['tsk_duration']);

    WeekDays := [];
    if AParams.ValueExists('sched_mon') then
      WeekDays := WeekDays + [dwMonday];
    if AParams.ValueExists('sched_tue') then
      WeekDays := WeekDays + [dwTuesday];
    if AParams.ValueExists('sched_wed') then
      WeekDays := WeekDays + [dwWednesday];
    if AParams.ValueExists('sched_thu') then
      WeekDays := WeekDays + [dwThursday];
    if AParams.ValueExists('sched_fri') then
      WeekDays := WeekDays + [dwFriday];
    if AParams.ValueExists('sched_sat') then
      WeekDays := WeekDays + [dwSaturday];
    if AParams.ValueExists('sched_sun') then
      WeekDays := WeekDays + [dwSunday];
    NewEvent.RunDays := WeekDays;
  end
  else
    NewEvent := nil;

  Mainboard.Schedule.Lock;
  try
    // clean up old existing events
    for i := Mainboard.Schedule.Count - 1 downto 0 do
      if (Mainboard.Schedule.GetEventByIndex(i).TaskID = 'Evolution.StartDBMaintenance') or
         (Mainboard.Schedule.GetEventByIndex(i).TaskID = 'Evolution.StopDBMaintenance') then
        Mainboard.Schedule.DeleteEvent(Mainboard.Schedule.GetEventByIndex(i));

    if Assigned(NewEvent) then
      Mainboard.Schedule.AddEvent(NewEvent);

    Mainboard.Schedule.SaveToStorage;
  finally
    Mainboard.Schedule.Unlock;
  end;
end;

function TmcHttpPresenter.rhRBStatus(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  rbStatus: IisParamsCollection;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage: String;
  Item, Row, LV: IisListOfValues;
  i: Integer;
begin
  RB := GetRBControl(True);
  rbStatus := RB.GetSystemStatus;
  sPage := GetResource('rb_status.html', AMIMEType);

  // Transmitters Queue Status
  TableDetails := TisParamsCollection.Create;
  Item := rbStatus.ParamsByName('CLTransmitterStatus');
  Row := TableDetails.AddParams('Clients');
  Row.AddValue('Transmitter', 'RB <-> Clients');
  Row.AddValue('ReceivingQueueSize', Item.Value['ReceivingQueueSize']);
  Row.AddValue('SendingQueueSize', Item.Value['SendingQueueSize']);

  Item := rbStatus.ParamsByName('RPTransmitterStatus');
  Row := TableDetails.AddParams('RPs');
  Row.AddValue('Transmitter', 'RB <-> RPs');
  Row.AddValue('ReceivingQueueSize', Item.Value['ReceivingQueueSize']);
  Row.AddValue('SendingQueueSize', Item.Value['SendingQueueSize']);

  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Transmitter', 'Transmitter');
  TableBuilder.AddColumn('Inbound Queue Size', 'ReceivingQueueSize');
  TableBuilder.AddColumn('Outbound Queue Size', 'SendingQueueSize');
  sPage := ReplaceMacros(sPage, '$TRANSMITTER_QUEUE_STATUS$', TableBuilder.GetHtml);


  // RP Pool status
  TableDetails := TisParamsCollection.Create;
  Item := rbStatus.ParamsByName('RPPool');
  for i := 0 to Item.Count - 1 do
  begin
    LV := IInterface(Item[i].Value) as IisListOfValues;
    Row := TableDetails.AddParams(Item[i].Name);
    Row.AddValue('Host', Item[i].Name);
    Row.AddValue('Session', LV.Value['Session']);

    Row.AddValue('Score', String(LV.Value['CurrentScore']) + '/' + String(LV.Value['MaxScore']));
    Row.AddValue('RTRequests', 'New=' + String(LV.Value['NewRTRequests']) + ' Exec=' + String(LV.Value['RTRequests']));
    Row.AddValue('TaskRequests', 'New=' + String(LV.Value['NewTaskRequests']) + ' Exec=' + String(LV.Value['TaskRequests']) +
                 ' Max=' + String(LV.Value['MaxTaskRequests']));
  end;
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('RP Host', 'Host');
  TableBuilder.AddColumn('Session ID', 'Session');
  TableBuilder.AddColumn('Score', 'Score');
  TableBuilder.AddColumn('RT Requests', 'RTRequests');
  TableBuilder.AddColumn('Task Requests', 'TaskRequests');
  sPage := ReplaceMacros(sPage, '$RP_POOL_STATUS$', TableBuilder.GetHtml);

  // Pending Requests
  TableDetails := TisParamsCollection.Create;
  Item := rbStatus.ParamsByName('Requests');
  for i := 0 to Item.Count - 1 do
  begin
    LV := IInterface(Item[i].Value) as IisListOfValues;
    Row := TableDetails.AddParams(Item[i].Name);
    Row.AddValue('Method', LV.Value['Method']);
    Row.AddValue('SourceSession', LV.Value['SourceSession']);
    Row.AddValue('DestinationSession', LV.Value['DestinationSession']);
    Row.AddValue('TaskRequest', LV.Value['TaskRequest']);
    Row.AddValue('Priority', LV.Value['Priority']);
  end;
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Method', 'Method');
  TableBuilder.AddColumn('Source Session ID', 'SourceSession');
  TableBuilder.AddColumn('Destination Session ID', 'DestinationSession');
  TableBuilder.AddColumn('Priority', 'Priority');
  sPage := ReplaceMacros(sPage, '$PENDING_REQUESTS$', TableBuilder.GetHtml);


  // RP Sessions
  TableDetails := TisParamsCollection.Create;
  Item := rbStatus.ParamsByName('RPSessions');
  for i := 0 to Item.Count - 1 do
  begin
    LV := IInterface(Item[i].Value) as IisListOfValues;
    Row := TableDetails.AddParams(Item[i].Name);
    Row.AddValue('Session', Item[i].Name);
    Row.AddValue('Host', LV.Value['Host']);
    Row.AddValue('Connections', LV.Value['Connections']);
    Row.AddValue('State', LV.Value['State']);
    Row.AddValue('Encryption', LV.Value['Encryption']);
    Row.AddValue('Compression', LV.Value['Compression']);
  end;
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Session ID', 'Session');
  TableBuilder.AddColumn('Host', 'Host');
  TableBuilder.AddColumn('Con', 'Connections');
  TableBuilder.AddColumn('State', 'State');
  TableBuilder.AddColumn('Enc', 'Encryption');
  TableBuilder.AddColumn('Comp', 'Compression');
  sPage := ReplaceMacros(sPage, '$RP_SESSIONS$', TableBuilder.GetHtml);


  // Client Sessions
  TableDetails := TisParamsCollection.Create;
  Item := rbStatus.ParamsByName('ClientSessions');
  for i := 0 to Item.Count - 1 do
  begin
    LV := IInterface(Item[i].Value) as IisListOfValues;
    Row := TableDetails.AddParams(Item[i].Name);
    Row.AddValue('Session', Item[i].Name);
    Row.AddValue('Login', LV.Value['Login']);
    Row.AddValue('IPAddress', LV.Value['IPAddress']);
    Row.AddValue('Connections', LV.Value['Connections']);
    Row.AddValue('AppID', LV.Value['AppID']);
    Row.AddValue('State', LV.Value['State']);
    Row.AddValue('Encryption', LV.Value['Encryption']);
    Row.AddValue('Compression', LV.Value['Compression']);
  end;
  TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
  TableBuilder.AddColumn('Session ID', 'Session');
  TableBuilder.AddColumn('Login', 'Login');
  TableBuilder.AddColumn('IPAddress', 'IPAddress');
  TableBuilder.AddColumn('Con', 'Connections');
  TableBuilder.AddColumn('AppID', 'AppID');
  TableBuilder.AddColumn('State', 'State');
  TableBuilder.AddColumn('Enc', 'Encryption');
  TableBuilder.AddColumn('Comp', 'Compression');
  sPage := ReplaceMacros(sPage, '$CLIENT_SESSIONS$', TableBuilder.GetHtml);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

procedure TmcHttpPresenter.UpdateRPPoolInfo;
var
  RB: ImcDMControl;
  RPPoolInfo, RPPoolStatus: IisParamsCollection;
  LV: IisListOfValues;
  i: Integer;
begin
  // update RB pool info
  RB := GetRBDM;
  if Assigned(RB) then
  begin
    RPPoolStatus := ParallelExec('RP.Installed', nil);
    RPPoolInfo := TisParamsCollection.Create;
    for i := 0 to RPPoolStatus.Count - 1 do
      if not RPPoolStatus[i].ValueExists('Error') then
        if RPPoolStatus[i].Value[METHOD_RESULT] then
        begin
          LV := RPPoolInfo.AddParams(RPPoolStatus.ParamName(i));
          LV.AddValue('Host', RPPoolStatus.ParamName(i));
          LV.AddValue('Enabled', True);
        end;
    RB.EvolutionControl.RBControl.SetRPPoolInfo(RPPoolInfo);
  end;
end;

function TmcHttpPresenter.WrapText(const AText: String; const ALineSize: Integer): String;
var
  i: Integer;
  s: String;
begin
  Result := '';
  s := StringReplace(AText, #13, '', [rfReplaceAll]);
  s := StringReplace(s, #10, '', [rfReplaceAll]);
  i := 0;
  while i < Length(s) do
  begin
    if Result <> '' then
      Result := Result + #13;
    Result := Result + Copy(s, i + 1, ALineSize);
    Inc(i, ALineSize);
  end;
end;

procedure TmcHttpPresenter.DoOnHttpRedirect(Sender: TObject;
  ClientSocket: TCustomIpClient);
var
  s, v, MimeType: String;  
begin
  repeat
    s := ClientSocket.Receiveln;
    if AnsiSameText(copy(s,1,5), 'Host:') then
    begin
      GetNextStrValue(s, ':');
      v := Trim(s);
    end;
  until (Length(s) = 0);

  if Length(v) > 0 then
  begin
    s := GetResource('redirect.html', MimeType);
    s := ReplaceMacros(s, M_REDIRECT, 'https://'+v);
    s := HttpWrap(s, MimeType);
    ClientSocket.SendBuf(s[1], Length(s));
  end;
end;

function TmcHttpPresenter.rhConfigRRUserConnections(const ASession: ImcSession; const AParams: IisListOfValues;
  out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr, sContent: String;
  RB: IevRBControl;
  TableDetails, InstalledRR: IisParamsCollection;
  i: Integer;
  TableBuilder: ImcHtmlTableBulder;
  sHost: String;
  TblRow, TblCol: IisListOfValues;
  RRUserConn: IisStringList;
  bAllow: Boolean;
  ListBuilder: ImcHtmlListBuilder;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    sPage := GetDomainContextPage('config_rr_access.html', sDomain, sDomainDescr);

    RB := GetRBControl(True);
    bAllow := RB.GetInternalUserRR(sDomain);

    ListBuilder := TmcHtmlListBuilder.Create('allow');
    ListBuilder.AddValue('Not Allowed', '0');
    ListBuilder.AddValue('Allowed for selected RR', '1');
    if bAllow then
    begin
      ListBuilder.SelectItem('1');

      RRUserConn := RB.GetInternalRR;
      RRUserConn.CaseSensitive := False;
      InstalledRR := ParallelExec('RR.Installed', nil);

      TableDetails := TisParamsCollection.Create;
      for i := 0 to InstalledRR.Count - 1 do
        if not InstalledRR[i].ValueExists('Error') and
           InstalledRR[i].Value[METHOD_RESULT] then
        begin
          sHost := InstalledRR.ParamName(i);
          TblRow := TableDetails.AddParams(sHost);
          TblRow.AddValue('Host', sHost);
          if (RRUserConn.Count > 0) and (RRUserConn.IndexOf(sHost) = -1) then
            TblRow.AddValue('Allow', '0')
          else
            TblRow.AddValue('Allow', '1');
        end;

      // Build table
      TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
      TableBuilder.AddColumn('Remote Relay', 'Host');
      TblCol := TableBuilder.AddColumn('Allow', 'Allow');
      TblCol.AddValue('EditType', 'Checkbox');
      TblCol.AddValue('EditParam', 'allow_');

      sContent := ListBuilder.GetHtml + hfParagraph + TableBuilder.GetHtml;
    end
    else
    begin
      ListBuilder.SelectItem('0');
      sContent := ListBuilder.GetHtml;
    end;

    sPage := ReplaceMacros(sPage, M_CONTENT, sContent);
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeRRUserConnections(
  const ASession: ImcSession; const AParams: IisListOfValues;
  out AMIMEType: String): String;
var
  RB: IevRBControl;
  InternalRR: IisStringList;
  i: Integer;
  sParNbr, sRRHost: String;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  RB.SetInternalUserRR(AParams.Value[P_EvDomain], AParams.Value['allow'] = '1');

  if AParams.Value['allow'] = '1' then
  begin
    InternalRR := TisStringList.Create;
    for i := 0 to AParams.Count - 1 do
      if StartsWith(AParams[i].Name, 'allow_') and (AParams[i].Value = '1') then
      begin
        sParNbr := AParams[i].Name;
        GetNextStrValue(sParNbr, 'allow_');
        sRRHost := AParams.Value['rec' + sParNbr];
        InternalRR.Add(sRRHost);
      end;
      RB.SetInternalRR(InternalRR);
  end;
end;

function TmcHttpPresenter.ParallelExec(const AMethod: String;
  const AParams: IisListOfValues): IisParamsCollection;
var
  ParExecRes: IisStringList;
begin
  ParExecRes := Mainboard.DeploymentManagers.ParallelExec([AMethod], [AParams]);
  Result := ParExecRes.Objects[0] as IisParamsCollection
end;

function TmcHttpPresenter.rhChangeLogFileThreshold(
  const ASession: ImcSession; const AParams: IisListOfValues;
  out AMIMEType: String): String;
var
  Par: IisListOfValues;
begin
  Par := TisListOfValues.Create;
  Par.AddValue('APurgeRecThreshold', StrToIntDef(AParams.Value['max_size'], 0));
  Par.AddValue('APurgeRecCount', StrToIntDef(AParams.Value['purge'], 0));

  Mainboard.DeploymentManagers.ParallelExecFor(nil,
    ['RB.SetLogFileThresholdInfo',
     'RP.SetLogFileThresholdInfo',
     'RR.SetLogFileThresholdInfo',
     'AA.SetLogFileThresholdInfo',
     'AC.SetLogFileThresholdInfo',
     'AS.SetLogFileThresholdInfo'], [Par, Par, Par, Par, Par, Par]);

  Result := '';
end;

function TmcHttpPresenter.rhConfigLogFileThreshold(
  const ASession: ImcSession; const AParams: IisListOfValues;
  out AMIMEType: String): String;
var
  ParExecRes: IisStringList;
  Par: IisParamsCollection;
  i, j: Integer;
  PurgeRecThreshold, PurgeRecCount: Integer;
  sPage, sError: String;
  bDontMatch: Boolean;
begin
  ParExecRes := Mainboard.DeploymentManagers.ParallelExecFor(nil,
    ['RB.GetLogFileThresholdInfo',
     'RP.GetLogFileThresholdInfo',
     'RR.GetLogFileThresholdInfo',
     'AA.GetLogFileThresholdInfo',
     'AC.GetLogFileThresholdInfo',
     'AS.GetLogFileThresholdInfo'], []);

  PurgeRecThreshold := -1;
  PurgeRecCount := -1;
  bDontMatch := False;

  for i := 0 to ParExecRes.Count - 1 do
  begin
    Par := ParExecRes.Objects[i] as IisParamsCollection;
    FilterParallelExecResult(Par);
    for j := 0 to Par.Count - 1 do
      if not Par[j].ValueExists('Error') then
      begin
        if PurgeRecThreshold <> Par[j].Value['APurgeRecThreshold'] then
          if PurgeRecThreshold = -1 then
            PurgeRecThreshold := Par[j].Value['APurgeRecThreshold']
          else
            bDontMatch := True;

        if PurgeRecCount <> Par[j].Value['APurgeRecCount'] then
          if PurgeRecCount = -1 then
            PurgeRecCount := Par[j].Value['APurgeRecCount']
          else
            bDontMatch := True;
      end;
  end;

  sPage := GetResource('config_log_files.html', AMIMEType);
  sPage := ReplaceMacros(sPage, '$MAX_SIZE$', IntToStr(PurgeRecThreshold));
  sPage := ReplaceMacros(sPage, '$PURGE$', IntToStr(PurgeRecCount));
  Result := GetPageTemplate(ASession, AMIMEType);

  if bDontMatch then
    sError := ReplaceMacros(hfAlertText, M_CONTENT, 'Settings do not match for all servers. Please set correct values and save.')
  else
    sError := '';

  Result := ReplaceMacros(Result, M_CONTENT, sPage + sError);
end;

procedure TmcHttpPresenter.FilterParallelExecResult(const AParallelExecResult: IisParamsCollection);
var
  i: Integer;
  s: String;
begin
  for i := AParallelExecResult.Count - 1 downto 0 do
  begin
    s := AParallelExecResult[i].TryGetValue('Error', '');
    if s = sServiceUnavailable then
      AParallelExecResult.DeleteParams(i);
  end;
end;

procedure TmcHttpPresenter.ExecRequest(const Params: TTaskParamList);
var
  Handler: TMethod;
  Idx: Integer;
  sPage, s, sMIMEType: String;
  Session: ImcSession;
  RequestParams: IisListOfValues;
begin
  try
    Idx := Params[0];
    Session := IInterface(Params[1]) as ImcSession;
    RequestParams := IInterface(Params[2]) as IisListOfValues;

    Handler.Data := Self;
    Handler.Code := Pointer(FRequestHandlers.Objects[Idx]);
    sPage := TmcHttpRequestHandler(Handler)(Session, RequestParams, sMIMEType);
  except
    on E: Exception do
    begin
      GlobalLogFile.AddEvent(etError, 'HTTP Server',  E.Message, BuildStackedErrorStr(E));
      s := GetResource('error.html', sMIMEType);
      s := ReplaceMacros(s, M_CONTENT, MakeHtmlText(BuildStackedErrorStr(E)));
      sPage := GetPageTemplate(Session, sMIMEType);
      sPage := ReplaceMacros(sPage, M_CONTENT, s);
    end;
  end;

  Session.RequestResult := sPage;
  Session.RequestResultMimeType := sMIMEType;
end;

function TmcHttpPresenter.GetResult(const ASession: ImcSession; out AMIMEType: String; const AWaitTime: Integer): String;
begin
  Assert(ASession.RequestID <> 0);

  if GlobalThreadManager.WaitForTaskEnd(ASession.RequestID, AWaitTime) then
  begin
    Result := ASession.RequestResult;
    AMIMEType := ASession.RequestResultMimeType;
    ASession.RequestID := 0;
    ASession.RequestResult := '';
  end
  else
  begin
    Result := '';
    AMIMEType := '';
  end;
end;

function TmcHttpPresenter.GetWaitScreen(const ASession: ImcSession; const AParams: IisListOfValues;
  out AMIMEType: String): String;
var
  s, sURL, sWaitText: String;
begin
  s := ReplaceMacros(hfRefresh, M_REFRESH_TIME, '0');

  if aParams.ValueExists(P_Redirect) then
    sURL := aParams.Value[P_Redirect]
  else
    sURL := aParams.Value['URL'];

  s := ReplaceMacros(s, M_REDIRECT, sURL);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_REFRESH, s);

  sWaitText := AParams.TryGetValue(P_WaitText, 'Please wait');
  s := GetResource('wait.html', AMIMEType);
  s := ReplaceMacros(s, M_CAPTION, sWaitText);

  Result := ReplaceMacros(Result, M_CONTENT, s);
end;

function TmcHttpPresenter.rhTestEMail(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
begin
  Mainboard.SendEMailNotification('Test', 'This is an email settings test');
  AMIMEType := '';
  Result := '';
end;


function TmcHttpPresenter.rhRRStatus(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
const
  ComprDescr: array [0..3] of string = ('', 'Good', 'Better', 'Best');
var
  InstalledRR, rrStatus: IisParamsCollection;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sHost, sPage: String;
  Row, LV: IisListOfValues;
  i, j: Integer;
begin
  InstalledRR := ParallelExec('RR.GetSessionsStatus', nil);
  FilterParallelExecResult(InstalledRR);

  if InstalledRR.Count > 0 then
  begin
    TableDetails := TisParamsCollection.Create;
    for i := 0 to InstalledRR.Count - 1 do
      if not InstalledRR[i].ValueExists('Error') then
      begin
        sHost := InstalledRR.ParamName(i);
        rrStatus := IInterface(InstalledRR[i].Value[METHOD_RESULT]) as IisParamsCollection;

        for j := 0 to rrStatus.Count - 1 do
        begin
          Row := TableDetails.AddParams(rrStatus.ParamName(j));
          LV := rrStatus.Params[j];
          Row.AddValue('Host', sHost);
          Row.AddValue('Login', LV.Value['Login']);
          Row.AddValue('IPAddress', LV.Value['IPAddress']);
          Row.AddValue('Connections', LV.Value['Connections']);
          Row.AddValue('Encryption', LV.Value['Encryption']);
          Row.AddValue('Compression', ComprDescr[Integer(LV.Value['Compression'])]);
          Row.AddValue('AppID', LV.Value['AppID']);
          Row.AddValue('State', LV.Value['State']);
        end;
      end;

    TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
    TableBuilder.AddColumn('RR Host', 'Host');
    TableBuilder.AddColumn('Login', 'Login');
    TableBuilder.AddColumn('IPAddress', 'IPAddress');
    TableBuilder.AddColumn('Con', 'Connections');
    TableBuilder.AddColumn('Enc', 'Encryption');
    TableBuilder.AddColumn('Comp', 'Compression');
    TableBuilder.AddColumn('AppID', 'AppID');
    TableBuilder.AddColumn('State', 'State');

    sPage := GetResource('rr_status.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhServiceState(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  sPage: String;
begin
  RB := GetRBControl(True);
  sPage := GetResource('maintenance_service_state.html', AMIMEType);
  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
  Result := ReplaceMacros(Result, '$SESSION_ID$', ASession.SessionID);
end;

function ZipString(Info, FileName: String): String;
var
  Compressor: TCompressionStream;
  CompressedStream: TStringStream;
  M: TMemoryStream;
  CDPOS, CDSIZE: DWORD;
  ZipData, ZipFile: String;

  procedure WriteWord(Value: Word);
  begin
    M.Write(Value, 2);
  end;

  procedure WriteDWord(Value: DWord);
  begin
    M.Write(Value, 4);
  end;

  function ZipCRC32(const Data: AnsiString): LongWord;
  var
    i: integer;
  begin
    Result := $FFFFFFFF;
    for i := 0 to length(Data) - 1 do
      Result := (Result shr 8) xor (AbCrc32Table[byte(Result) xor Ord(Data[i + 1])]);
    Result := Result xor $FFFFFFFF;
  end;

begin
  CompressedStream := TStringStream.Create('');
  try
    Compressor := TCompressionStream.Create(clDefault, CompressedStream);
    try
      Compressor.Write(Info[1], Length(Info));
    finally
      Compressor.Free;
    end;
    ZipData := copy(CompressedStream.DataString, 3, Length(CompressedStream.DataString) - 6);
  finally
    CompressedStream.Free;
  end;

  M := TMemoryStream.Create;
  try
    WriteDWord($04034B50);
    WriteWord(20);
    WriteWord(0);
    WriteWord(8);
    WriteDWord(DateTimeToFileDate(Now));
    WriteDWord(ZipCRC32(Info));
    WriteDWord(Length(ZipData)); // compressed size
    WriteDWord(Length(Info)); // uncompressed size
    WriteWord(Length(FileName)); // filename length
    WriteWord(0);
    M.Write(FileName[1], Length(FileName));
    M.Write(ZipData[1], Length(ZipData));
    CDPOS := M.Position;
    WriteDWord($02014b50);
    WriteWord(20); // version created by
    WriteWord(20); // version to extract
    WriteWord(0);
    WriteWord(8);
    WriteDWord(DateTimeToFileDate(Now));
    WriteDWord(ZipCRC32(Info));
    WriteDWord(Length(ZipData)); // compressed size
    WriteDWord(Length(Info)); // uncompressed size
    WriteWord(Length(FileName)); // filename length
    WriteWord(0);

    WriteWord(0);
    WriteWord(0);
    WriteWord(0);
    WriteDWord(0);
    WriteDWord(0); // offset of local header

    M.Write(FileName[1], Length(FileName));
    CDSIZE := M.Position - CDPOS;
    // end of central dir
    WriteDWord($06054B50);
    WriteWord(0);
    WriteWord(0);
    WriteWord(1);
    WriteWord(1);
    WriteDWord(CDSIZE);
    WriteDWord(CDPOS);
    WriteWord(0);
    M.Position := 0;

    SetLength(ZipFile, M.Size);
    M.Read(ZipFile[1], M.Size);
  finally
    M.Free;
  end;
  Result := ZipFile;
end;

function TmcHttpPresenter.rhServiceStacks(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  RPPoolInfo: IisParamsCollection;
  LV: IisListOfValues;
  i: Integer;
  StackInfo: String;
  RBDM, DM: ImcDMControl;
const
  LN = '===========================================================================================';
begin
  RB := GetRBControl;
  RBDM := GetRBDM;
  StackInfo := LN + #13#10 + 'REQUEST BROKER [' + RBDM.Host + ']' + #13#10#13#10 + RB.GetStackInfo + #13#10;

  RPPoolInfo := RB.GetRPPoolInfo;

  for i := 0 to RPPoolInfo.Count - 1 do
  begin
    LV := RPPoolInfo.Params[i];
    DM := Mainboard.DeploymentManagers.FindDeploymentManager(LV.Value['host']);
    if Assigned(DM) then
    begin
      try
        StackInfo := StackInfo + #13#10 + LN + #13#10 + 'REQUEST PROCESSOR [' + LV.Value['host']+']'+#13#10#13#10 +
          DM.EvolutionControl.RPControl.GetStackInfo;
      except
      end;
    end;
  end;

  for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
  begin
    DM := Mainboard.DeploymentManagers[i];
    if DM.EvolutionControl.GetRRInstalled and DM.EvolutionControl.GetRRStarted then
    try
      StackInfo := StackInfo + #13#10 + LN + #13#10 + 'REMOTE RELAY [' + DM.Host + ']'+#13#10#13#10 +
        DM.EvolutionControl.RRControl.GetStackInfo;
    except
    end;
  end;

  for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
  begin
    DM := Mainboard.DeploymentManagers[i];
    if DM.EvolutionControl.GetAAInstalled and DM.EvolutionControl.GetAAStarted then
    try
      StackInfo := StackInfo + #13#10 + LN + #13#10 + 'API ADAPTER [' + DM.Host + ']'+#13#10#13#10 +
        DM.EvolutionControl.AAControl.GetStackInfo;
    except
    end;
  end;

  for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
  begin
    DM := Mainboard.DeploymentManagers[i];
    if DM.EvolutionControl.GetACInstalled and DM.EvolutionControl.GetACStarted then
    try
      StackInfo := StackInfo + #13#10 + LN + #13#10 + 'ADR CLIENT [' + DM.Host + ']'+#13#10#13#10 +
        DM.EvolutionControl.ACControl.GetStackInfo;
    except
    end;
  end;

  for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
  begin
    DM := Mainboard.DeploymentManagers[i];
    if DM.EvolutionControl.GetASInstalled and DM.EvolutionControl.GetASStarted then
    try
      StackInfo := StackInfo + #13#10 + LN + #13#10 + 'ADR SERVER [' + DM.Host + ']'+#13#10#13#10 +
        DM.EvolutionControl.ASControl.GetStackInfo;
    except
    end;
  end;

  for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
  begin
    try
      DM := Mainboard.DeploymentManagers[i];
      StackInfo := StackInfo + #13#10 + LN + #13#10 + 'DEPLOYMENT MANAGER [' + DM.Host + ']'+#13#10#13#10 +
        DM.GetStackInfo;
    except
    end;
  end;

  try
    raise EisDumpAllThreads.Create('');
  except
    on E: Exception do
      StackInfo := StackInfo + #13#10 + LN + #13#10 + 'MANAGEMENT CONSOLE ['+GetComputerNameString+']'+#13#10#13#10 + GetStack(E);
  end;

  AMIMEType := 'application/zip;'#13#10'Content-Disposition: attachment; filename="service_stack.zip"';
  Result := ZipString(StackInfo, 'service_stack.txt');
end;

function TmcHttpPresenter.rhDownloadAppVersion(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sVer: String;
begin
  sVer := AParams.TryGetValue('version', '');
  if sVer <> '' then
    Mainboard.DownloadAppVersion(ASession.UserAccount.Application, sVer);
end;

function TmcHttpPresenter.rhACStatus(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  DS: ImcDMControl;
  sHost: String;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage, sLink_Domain: String;
  i: Integer;
  HREF: ImcHREF;
  TotalQueued, TotalExecuting, TotalUploading, TotalUplQueued, MaxLatency: Integer;
  sMaxLatency: String;  
  Totals: IisListOfValues;
  DomainList: IisListOfValues;
begin
  DomainList := GetEvDomainList;
  if DomainList.Count = 1 then
  begin
    AParams.AddValue(P_EvDomain, DomainList[0].Name);
    Result := rhACDomainStatus(ASession, AParams, AMIMEType);
    exit;
  end;

  if AParams.ValueExists(P_Host) then
    sHost := AParams.Value[P_Host]
  else
    sHost := GetFirstACHost;
  DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);

  if Assigned(DS) then
  begin
    TableDetails := DS.EvolutionControl.ACControl.GetStatus;

    TotalQueued := 0;
    TotalExecuting := 0;
    TotalUploading := 0;
    TotalUplQueued := 0;
    MaxLatency := 0;
    for i := 0 to TableDetails.Count - 1 do
    begin
      HREF := TmcHREF.Create('ac_domain_status', ASession);
      HREF.AddParam(P_EvDomain, TableDetails.ParamName(i));
      sLink_Domain := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
      sLink_Domain := ReplaceMacros(sLink_Domain, M_CONTENT, TableDetails[i].Value['DomainName']);
      TableDetails[i].Value['DomainName'] := sLink_Domain;

      if TableDetails[i].Value['Connected'] then
        TableDetails[i].Value['Connected'] := hfOKPict
      else
        TableDetails[i].Value['Connected'] := hfCancelPict;

      TotalQueued := TotalQueued + TableDetails[i].Value['RepQueued'];
      TotalExecuting := TotalExecuting + TableDetails[i].Value['RepExecuting'];
      TotalUplQueued := TotalUplQueued + TableDetails[i].Value['UplQueued'];
      TotalUploading := TotalUploading + TableDetails[i].Value['UplExecuting'];

      if MaxLatency < TableDetails[i].Value['Latency'] then
        MaxLatency := TableDetails[i].Value['Latency'];

      TableDetails[i].Value['Latency'] := PeriodToString(TableDetails[i].Value['Latency'] * 1000);
    end;

    if MaxLatency = 0 then
      sMaxLatency := ''
    else
      sMaxLatency := PeriodToString(MaxLatency * 1000);

    // Add total record
    Totals := TableDetails.AddParams('Summary');
    Totals.AddValue('DomainName', '');
    Totals.AddValue('Connected', '');
    Totals.AddValue('RepQueued', TotalQueued);
    Totals.AddValue('RepExecuting', TotalExecuting);
    Totals.AddValue('UplQueued', TotalUplQueued);
    Totals.AddValue('UplExecuting', TotalUploading);
    Totals.AddValue('Latency', sMaxLatency);
    Totals.AddValue('LastActivity', '');

    // Build table
    TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
    TableBuilder.AddColumn('Service Bureau', 'DomainName');
    TableBuilder.AddColumn('Connected', 'Connected');
    TableBuilder.AddColumn('Rep Queue', 'RepQueued');
    TableBuilder.AddColumn('Executing', 'RepExecuting');
    TableBuilder.AddColumn('Upl Queue', 'UplQueued');
    TableBuilder.AddColumn('Uploading', 'UplExecuting');
    TableBuilder.AddColumn('Latency',   'Latency');
    TableBuilder.AddColumn('Last Activity', 'LastActivity');

    sPage := GetResource('adr_status.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
    sPage := ReplaceMacros(sPage, M_CAPTION, 'ADR Client Status');
    sPage := ReplaceMacros(sPage, M_HOST, sHost);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.rhASStatus(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  ADRS: IevASControl;
  DS: ImcDMControl;
  sHost: String;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage, sLink_Domain: String;
  i: Integer;
  HREF: ImcHREF;
  TotalQueued, TotalExecuting, TotalUploading, TotalQueueing, MaxLatency: Integer;
  sMaxLatency: String;
  Totals: IisListOfValues;
  DomainList: IisListOfValues;
begin
  DomainList := GetEvDomainList;
  if DomainList.Count = 1 then
  begin
    AParams.AddValue(P_EvDomain, DomainList[0].Name);
    Result := rhASDomainStatus(ASession, AParams, AMIMEType);
    exit;
  end;

  if AParams.ValueExists(P_Host) then
    sHost := AParams.Value[P_Host]
  else
    sHost := GetFirstASHost;
  DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);

  if Assigned(DS) then
  begin
    ADRS := DS.EvolutionControl.ASControl;

    TableDetails := ADRS.GetStatus;

    TotalQueued := 0;
    TotalExecuting := 0;
    TotalUploading := 0;
    TotalQueueing := 0;
    MaxLatency := 0;
    for i := 0 to TableDetails.Count - 1 do
    begin
      HREF := TmcHREF.Create('as_domain_status', ASession);
      HREF.AddParam(P_EvDomain, TableDetails.ParamName(i));
      HREF.AddParam(P_Host, sHost);
      sLink_Domain := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
      sLink_Domain := ReplaceMacros(sLink_Domain, M_CONTENT, TableDetails[i].Value['DomainName']);
      TableDetails[i].Value['DomainName'] := sLink_Domain;

      if TableDetails[i].Value['Connected'] then
        TableDetails[i].Value['Connected'] := hfOKPict
      else
        TableDetails[i].Value['Connected'] := hfCancelPict;

      TotalQueued := TotalQueued + TableDetails[i].Value['RepQueued'];
      TotalExecuting := TotalExecuting + TableDetails[i].Value['RepExecuting'];
      TotalUploading := TotalUploading + TableDetails[i].Value['UplQueued'];
      TotalQueueing := TotalQueueing + TableDetails[i].Value['UplExecuting'];

      if MaxLatency < TableDetails[i].Value['Latency'] then
        MaxLatency := TableDetails[i].Value['Latency'];

      TableDetails[i].Value['Latency'] := PeriodToString(TableDetails[i].Value['Latency'] * 1000);
    end;

    if MaxLatency = 0 then
      sMaxLatency := ''
    else
      sMaxLatency := PeriodToString(MaxLatency * 1000);

    // Add total record
    Totals := TableDetails.AddParams('Summary');
    Totals.AddValue('DomainName', '');
    Totals.AddValue('Connected', '');
    Totals.AddValue('UplQueued', TotalUploading);
    Totals.AddValue('UplExecuting', TotalQueueing);
    Totals.AddValue('RepQueued', TotalQueued);
    Totals.AddValue('RepExecuting', TotalExecuting);
    Totals.AddValue('Latency', sMaxLatency);
    Totals.AddValue('LastActivity', '');

    // Build table
    TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
    TableBuilder.AddColumn('Service Bureau', 'DomainName');
    TableBuilder.AddColumn('Connected', 'Connected');
    TableBuilder.AddColumn('Uploading', 'UplQueued');
    TableBuilder.AddColumn('Upl Disp',  'UplExecuting');
    TableBuilder.AddColumn('Rep Queue', 'RepQueued');
    TableBuilder.AddColumn('Executing', 'RepExecuting');
    TableBuilder.AddColumn('Latency',   'Latency');
    TableBuilder.AddColumn('Last Server Activity', 'LastActivity');

    sPage := GetResource('adr_status.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, TableBuilder.GetHtml);
    sPage := ReplaceMacros(sPage, M_CAPTION, 'ADR Server Status');
    sPage := ReplaceMacros(sPage, M_HOST, DS.Host);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhRunACOperation(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  AC: IevACControl;
  DS: ImcDMControl;
  sDomain: String;
begin
  if AParams.ValueExists(P_Host) then
    DS := Mainboard.DeploymentManagers.FindDeploymentManager(AParams.Value[P_Host])
  else
    Exit;

  AC := DS.EvolutionControl.ACControl;

  sDomain := AParams.Value[P_EvDomain];
  if AParams.ValueExists('Sync_DB_List') then
    AC.SyncDBList(sDomain)
  else if AParams.ValueExists('Check') then
    AC.CheckDB(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Full_Copy') then
    AC.FullDBCopy(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Check_Data') then
    AC.CheckDBData(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Check_Trans') then
    AC.CheckDBTransactions(sDomain, AParams.Value['DBName']);

  Result := '';
end;

function TmcHttpPresenter.AddEvDomainSelection(const ASession: ImcSession; const ASource, AMethod: String): String;
var
  sPage: String;
  sMIME: String;
  RB: IevRBControl;
  DomainList: IisListOfValues;
  i: Integer;
  sItems, sSel: String;
begin
  Result := ASource;
  if Pos(M_SET_EVDOMAIN, ASource) <> 0 then
  begin
    RB := GetRBControl;
    if not Assigned(RB) then
      Exit;

    sItems := '';
    DomainList := GetEvDomainList;
    if DomainList.Count <= 1 then
      Exit;

    DomainList.Sort;
    for i := 0 to DomainList.Count - 1 do
    begin
      if AnsiSameText(DomainList[i].Name, ASession.EvDomain) then
        sSel := 'selected="selected"'
      else
        sSel := '';
      sItems := sItems + Format('<option value="%s" %s>%s</option>', [DomainList[i].Name, sSel, DomainList[i].Value]);
    end;

    sPage := GetResource('setevdomain.html', sMIME);
    sPage := ReplaceMacros(sPage, '$DOMAINLIST$', sItems);
    sPage := ReplaceMacros(hfParagraph, M_CONTENT, sPage);

    Result := ReplaceMacros(ASource, M_SET_EVDOMAIN, sPage);
  end;
end;

function TmcHttpPresenter.rhSetEvDomain(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
begin
  ASession.EvDomain := AParams.Value[P_EvDomain];
end;

{
procedure TmcHttpPresenter.EnsureEvDomain(const ASession: ImcSession);
var
  RB: IevRBControl;
begin
  if ASession.EvDomain = '' then
  begin
    RB := GetRBControl;
    if Assigned(RB) then
      ASession.EvDomain := GetEvDomainList[0].Name;
  end;
end;
}

function TmcHttpPresenter.rhASDomainStatus(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  ADRS: IevASControl;
  Status: IisListOfValues;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage, sManualOperFrame: String;
  sHost: String;
  DS: ImcDMControl;
  sDomain, sDomainDescr: String;
  Lb: ImcHtmlListBuilder;
  DBList: IisStringList;
  i: Integer;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    if AParams.ValueExists(P_Host) then
      sHost := AParams.Value[P_Host]
    else
      sHost := GetFirstASHost;
    DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);

    if Assigned(DS) then
    begin
      ADRS := DS.EvolutionControl.ASControl;

      Status := ADRS.GetDomainStatus(AParams.Value[P_EvDomain]);
      TableDetails := IInterface(Status.Value['ActiveRequests']) as IisParamsCollection;

      // Build table
      TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
      TableBuilder.AddColumn('Queued', 'QueuedAt');
      TableBuilder.AddColumn('Started', 'StartedAt');
      TableBuilder.AddColumn('Request', 'Type');
      TableBuilder.AddColumn('Description', 'Caption');
      TableBuilder.AddColumn('Status', 'Status');
      TableBuilder.AddColumn('Progress', 'Progress');

      sPage := GetDomainContextPage('adr_domain_status.html', sDomain, sDomainDescr);
      sPage := ReplaceMacros(sPage, M_CAPTION, 'ADR Server Status');
      sPage := ReplaceMacros(sPage, M_HOST, DS.Host);
      sPage := ReplaceMacros(sPage, '$ACTIVE_REQUESTS$', TableBuilder.GetHtml);

      sPage := ReplaceMacros(sPage, '$REP_QUEUE$', 'Queued:' + hfSpace + VarToStr(Status.Value['RepQueued']) +
        hfSpace + hfSpace + hfSpace + 'Executing:' + hfSpace + VarToStr(Status.Value['RepExecuting']));

      sPage := ReplaceMacros(sPage, '$UPL_QUEUE$', 'Uploading:' + hfSpace +
        VarToStr(Status.Value['UplQueued']) + hfSpace + hfSpace + hfSpace + 'Dispatching:' + hfSpace + VarToStr(Status.Value['UplExecuting']));

      sPage := ReplaceMacros(sPage, '$CL_LAST_ACT$', Status.Value['ClLastActivity']);

      if Status.Value['Connected'] then
        sPage := ReplaceMacros(sPage, '$CONNECTED$', hfOKPict)
      else
        sPage := ReplaceMacros(sPage, '$CONNECTED$', hfCancelPict);
      sPage := ReplaceMacros(sPage, '$SRV_LAST_ACT$', Status.Value['SrvLastActivity']);

      sPage := ReplaceMacros(sPage, '$LATENCY$', PeriodToString(Status.TryGetValue('Latency', 0) * 1000));

      // manual operations
      sManualOperFrame := GetDomainContextPage('as_manual_local_oper.html', sDomain, sDomainDescr);

      if Status.Value['Connected'] and (ASession.UserAccount.AccountType >= uatPowerUser) then
      begin
        sManualOperFrame := sManualOperFrame + hfBR + GetDomainContextPage('as_manual_oper.html', sDomain, sDomainDescr);

        Lb := TmcHtmlListBuilder.Create('DBName');
        Lb.AddValue('All Databases', '*');
        Lb.AddValue('All Client Databases', 'CL*');

        DBList := ADRS.GetClientDBList(sDomain);
        for i := 0 to DBList.Count - 1 do
          Lb.AddValue(DBList[i], DBList[i]);

        Lb.SelectItem('*');

        sManualOperFrame := ReplaceMacros(sManualOperFrame, '$DB_LIST$', Lb.GetHtml);
      end;

      sManualOperFrame := ReplaceMacros(sManualOperFrame, M_HOST, sHost);

      sPage := ReplaceMacros(sPage, '$MANUAL_OPERATION$', hfBR + sManualOperFrame);
    end
    else
    begin
      sPage := GetResource('no_info.html', AMIMEType);
      sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
    end;
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeAC(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  sParNbr, sPort, pwd: String;
  RB, DM: ImcDMControl;
  Settings: IisListOfValues;
  Domains: IisParamsCollection;
  D: IisListOfValues;
begin
  DM := Mainboard.DeploymentManagers.FindDeploymentManager(AParams.TryGetValue(P_Host, ''));

  if AParams.ValueExists('Max_Requests') then
  begin
    Settings := TisListOfValues.Create;
    Settings.AddValue('MaxRequests', StrToInt(Trim(AParams.Value['Max_Requests'])));
  end

  else
  begin
    Settings := DM.EvolutionControl.ACControl.GetSettings;
    RB := GetRBDM;
    if Assigned(RB) then
      Settings.Value['RequestBroker'] := RB.Host;

    Settings.Value['StagingFolder'] := Trim(AParams.TryGetValue('Staging_Folder', ''));

    Domains := IInterface(Settings.Value['Domains']) as IisParamsCollection;
    for i := 0 to AParams.Count - 1 do
      if StartsWith(AParams[i].Name, 'rec') then
      begin
        D := Domains.ParamsByName(AParams[i].Value);
        if Assigned(D) then
        begin
          sParNbr := AParams[i].Name;
          GetNextStrValue(sParNbr, 'rec');

          D.Value['Active'] := AParams.TryGetValue('Active_' + sParNbr, '0') = '1';
          D.Value['Host'] := AParams.TryGetValue('Host_' + sParNbr, '');

          sPort := Trim(AParams.TryGetValue('Port_' + sParNbr, ''));
          if (sPort <> '') and (AParams.TryGetValue('SSL_' + sParNbr, '0') = '1') then
            sPort := sPort + ':SSL';
          D.Value['Port'] := sPort;

          D.Value['User'] := AParams.TryGetValue('User_' + sParNbr, '');

          pwd := AParams.TryGetValue('Password_' + sParNbr, '');
          if pwd <> cDummyPassword then
            D.Value['Password'] := pwd;
        end;
      end;
  end;

  DM.EvolutionControl.ACControl.SetSettings(Settings);

  Result := '';
end;

function TmcHttpPresenter.rhChangeASDB(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sDomain, sParNbr: String;
  RB: IevRBControl;
  Locations: IisParamsCollection;
  DBSettings: IisListOfValues;
  i: Integer;

  procedure AddDBInfo(const AType: TevDBType; const APath: String; const ABeginRangeNbr, AEndRangeNbr: Integer);
  var
    Item: IisListOfValues;
  begin
    if APath <> '' then
    begin
      Item := Locations.AddParams(IntToStr(Locations.Count));
      Item.AddValue('DBType', Ord(AType));
      Item.AddValue('Path', APath);
      Item.AddValue('BeginRangeNbr', ABeginRangeNbr);
      Item.AddValue('EndRangeNbr', AEndRangeNbr);
    end;
  end;

begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  sDomain := AParams.Value[P_EvDomain];
  Locations := TisParamsCollection.Create;
  DBSettings := TisListOfValues.Create;

  DBSettings.AddValue('ADRDBLocation', Locations);

  AddDBInfo(dbtUnspecified, AParams.Value['defaultDBpath'], 0, 0);
  AddDBInfo(dbtSystem, AParams.Value['systemDBpath'], 0, 0);
  AddDBInfo(dbtBureau, AParams.Value['sbDBPath'], 0, 0);
  AddDBInfo(dbtTemporary, AParams.Value['tmpDBPath'], 0, 0);

  for i := 0 to AParams.Count - 1 do
  begin
    if StartsWith(AParams[i].Name, 'cl_path') and (AParams[i].Value <> '') then
    begin
      sParNbr := AParams[i].Name;
      GetNextStrValue(sParNbr, 'cl_path');
      AddDBInfo(dbtClient, AParams.Value['cl_path' + sParNbr],
                StrToInt(Trim(AParams.Value['cl_bnbr' + sParNbr])),
                StrToInt(Trim(AParams.Value['cl_enbr' + sParNbr])));
    end;
  end;
  RB.SetDatabaseParams(sDomain, DBSettings);
end;

function TmcHttpPresenter.rhConfigAC(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  DS: ImcDMControl;
  TableBuilder: ImcHtmlTableBulder;
  Settings, TblCol: IisListOfValues;
  sPage, sHost, s: String;
  i: Integer;
  Domains: IisParamsCollection;
begin
  if AParams.ValueExists(P_Host) then
    sHost := AParams.Value[P_Host]
  else
    sHost := GetFirstACHost;
  DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);

  if Assigned(DS) then
  begin
    Settings := DS.EvolutionControl.ACControl.GetSettings;

    sPage := GetResource('config_ac.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_HOST, sHost);
    sPage := ReplaceMacros(sPage, '$MAX_REQUESTS$', Settings.Value['MaxRequests']);
    sPage := ReplaceMacros(sPage, '$STAGING_FOLDER$', Settings.Value['StagingFolder']);

    // Domain list
    Domains := IInterface(Settings.Value['Domains']) as IisParamsCollection;
    for i := 0 to Domains.Count - 1 do
    begin
      if Domains[i].Value['Password'] <> '' then
        Domains[i].Value['Password'] := cDummyPassword;

      s := Domains[i].Value['Port'];
      Domains[i].Value['Port'] := GetNextStrValue(s, ':');
      Domains[i].AddValue('SSL', AnsiSameText(s, 'SSL'));
    end;

    TableBuilder := TmcHtmlTableBulder.Create(Domains);

    TblCol := TableBuilder.AddColumn('Service Bureau', 'Name');

    TblCol := TableBuilder.AddColumn('Active', 'Active');
    TblCol.AddValue('EditType', 'Checkbox');
    TblCol.AddValue('EditParam', 'Active_');

    TblCol := TableBuilder.AddColumn('ADR Server', 'Host');
    TblCol.AddValue('EditType', 'EditBox');
    TblCol.AddValue('EditParam', 'Host_');
    TblCol.Value['Size'] := '40';

    TblCol := TableBuilder.AddColumn('Port', 'Port');
    TblCol.AddValue('EditType', 'EditBox');
    TblCol.AddValue('EditParam', 'Port_');
    TblCol.Value['Size'] := '6';

    TblCol := TableBuilder.AddColumn('SSL', 'SSL');
    TblCol.AddValue('EditType', 'CheckBox');
    TblCol.AddValue('EditParam', 'SSL_');

    TblCol := TableBuilder.AddColumn('Login', 'User');
    TblCol.AddValue('EditType', 'EditBox');
    TblCol.AddValue('EditParam', 'User_');
    TblCol.Value['Size'] := '10';

    TblCol := TableBuilder.AddColumn('Password', 'Password');
    TblCol.AddValue('EditType', 'EditPassword');
    TblCol.AddValue('EditParam', 'Password_');
    TblCol.Value['Size'] := '10';

    sPage := ReplaceMacros(sPage, '$DOMAINS_SETUP$', TableBuilder.GetHtml);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhConfigASDB(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr: String;
  defPath, sysPath, sbPath, tmpPath: String;
  RB: IevRBControl;
  Locations: IisParamsCollection;
  DBSettings: IisListOfValues;
  i: Integer;
  LV: IisListOfValues;
  TableBuilder: ImcHtmlTableBulder;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    sPage := GetDomainContextPage('config_as_db.html', sDomain, sDomainDescr);

    RB := GetRBControl(True);
    DBSettings := RB.GetDatabaseParams(sDomain);
    Locations := IInterface(DBSettings.Value['ADRDBLocation']) as IisParamsCollection;

    // Primary DBs
    defPath := '';
    sysPath := '';
    sbPath := '';
    tmpPath := '';
    for i := 0 to Locations.Count - 1 do
    begin
      LV := Locations[i];
      case TevDBType(Integer(LV.Value['DBType'])) of
        dbtUnspecified : defPath := LV.Value['Path'];
        dbtSystem      : sysPath := LV.Value['Path'];
        dbtBureau      : sbPath  := LV.Value['Path'];
        dbtTemporary   : tmpPath := LV.Value['Path'];
      end;
    end;

    sPage := ReplaceMacros(sPage, '$defaultDBpath$', defPath);
    sPage := ReplaceMacros(sPage, '$systemDBpath$', sysPath);
    sPage := ReplaceMacros(sPage, '$sbDBpath$', sbPath);
    sPage := ReplaceMacros(sPage, '$tmpDBpath$', tmpPath);

    // client DBs
    for i := Locations.Count - 1 downto 0 do  // filter CL paths
    begin
      LV := Locations[i];
      if TevDBType(Integer(LV.Value['DBType'])) <> dbtClient then
        Locations.DeleteParams(i);
    end;

    for i := 1 to 3 do    // add blank rows
    begin
      LV := Locations.AddParams('Blank' + IntToStr(i));
      LV.AddValue('Path', '');
      LV.AddValue('BeginRangeNbr', '');
      LV.AddValue('EndRangeNbr', '');
    end;

    TableBuilder := TmcHtmlTableBulder.Create(Locations);
    LV := TableBuilder.AddColumn('Client DB path override', 'Path');
    LV.Value['EditType'] := 'EditBox';
    LV.Value['EditParam'] := 'cl_path';
    LV.Value['Size'] := '60';
    LV := TableBuilder.AddColumn('Starting CL_#', 'BeginRangeNbr');
    LV.Value['EditType'] := 'EditBox';
    LV.Value['EditParam'] := 'cl_bnbr';
    LV.Value['Size'] := '10';
    LV := TableBuilder.AddColumn('Ending CL_#', 'EndRangeNbr');
    LV.Value['EditType'] := 'EditBox';
    LV.Value['EditParam'] := 'cl_enbr';
    LV.Value['Size'] := '10';

    sPage := ReplaceMacros(sPage, '$CLIENT_DB_PATHS$', TableBuilder.GetHtml);
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.rhACDomainStatus(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  ADRC: IevACControl;
  Status: IisListOfValues;
  TableDetails: IisParamsCollection;
  TableBuilder: ImcHtmlTableBulder;
  sPage, sManualOperFrame: String;
  sHost: String;
  DS: ImcDMControl;
  sDomain, sDomainDescr: String;
  Lb: ImcHtmlListBuilder;
  DBList: IisStringList;
  i: Integer;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
  begin
    if AParams.ValueExists(P_Host) then
      sHost := AParams.Value[P_Host]
    else
      sHost := GetFirstACHost;
    DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);

    if Assigned(DS) then
    begin
      ADRC := DS.EvolutionControl.ACControl;

      Status := ADRC.GetDomainStatus(AParams.Value[P_EvDomain]);
      TableDetails := IInterface(Status.Value['ActiveRequests']) as IisParamsCollection;

      // Build table
      TableBuilder := TmcHtmlTableBulder.Create(TableDetails);
      TableBuilder.AddColumn('Queued', 'QueuedAt');
      TableBuilder.AddColumn('Started', 'StartedAt');
      TableBuilder.AddColumn('Request', 'Type');
      TableBuilder.AddColumn('Description', 'Caption');
      TableBuilder.AddColumn('Status', 'Status');
      TableBuilder.AddColumn('Progress', 'Progress');

      sPage := GetDomainContextPage('adr_domain_status.html', sDomain, sDomainDescr);
      sPage := ReplaceMacros(sPage, M_CAPTION, 'ADR Client Status');
      sPage := ReplaceMacros(sPage, M_HOST, DS.Host);
      sPage := ReplaceMacros(sPage, '$ACTIVE_REQUESTS$', TableBuilder.GetHtml);

      sPage := ReplaceMacros(sPage, '$REP_QUEUE$', 'Queued:' + hfSpace + VarToStr(Status.Value['RepQueued']) +
        hfSpace + hfSpace + hfSpace + 'Executing:' + hfSpace + VarToStr(Status.Value['RepExecuting']));

      sPage := ReplaceMacros(sPage, '$UPL_QUEUE$', 'Queued:' + hfSpace +
        VarToStr(Status.Value['UplQueued']) + hfSpace + hfSpace + hfSpace + 'Uploading:' + hfSpace + VarToStr(Status.Value['UplExecuting']));

      sPage := ReplaceMacros(sPage, '$CL_LAST_ACT$', Status.Value['ClLastActivity']);

      if Status.Value['Connected'] then
        sPage := ReplaceMacros(sPage, '$CONNECTED$', hfOKPict)
      else
        sPage := ReplaceMacros(sPage, '$CONNECTED$', hfCancelPict);
      sPage := ReplaceMacros(sPage, '$SRV_LAST_ACT$', Status.Value['SrvLastActivity']);

      sPage := ReplaceMacros(sPage, '$LATENCY$', PeriodToString(Status.Value['Latency'] * 1000));

      // manual operations
      if ASession.UserAccount.AccountType >= uatPowerUser then
      begin
        sManualOperFrame := GetDomainContextPage('ac_manual_oper.html', sDomain, sDomainDescr);

        Lb := TmcHtmlListBuilder.Create('DBName');
        Lb.AddValue('All Databases', '*');
        Lb.AddValue('All Client Databases', 'CL*');

        DBList := ADRC.GetDBList(sDomain);
        for i := 0 to DBList.Count - 1 do
          Lb.AddValue(DBList[i], DBList[i]);

        Lb.SelectItem('*');

        sManualOperFrame := ReplaceMacros(sManualOperFrame, '$DB_LIST$', Lb.GetHtml);
        sManualOperFrame := ReplaceMacros(sManualOperFrame, M_HOST, sHost);
      end
      else
        sManualOperFrame := '';

      sPage := ReplaceMacros(sPage, '$MANUAL_OPERATION$', hfBR + sManualOperFrame);
    end
    else
    begin
      sPage := GetResource('no_info.html', AMIMEType);
      sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
    end;
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.GetEvDomainList: IisListOfValues;
begin
  if not Assigned(FEvDomainList) then
    FEvDomainList := GetRBControl(True).GetDomainList;
  Result := FEvDomainList.GetClone;
end;

procedure TmcHttpPresenter.UpdateADRServicesInfo;
var
  RB: IevRBControl;
  RunRes: IisStringList;
  ServicesInfo: IisListOfValues;
  Res: IisParamsCollection;
  i, j: Integer;
  s: String;
begin
  RB := GetRBControl;
  if Assigned(RB) then
  begin
    RunRes := Mainboard.DeploymentManagers.ParallelExec(['AC.Installed', 'AS.Installed'], []);
    if not Assigned(RunRes) then
      Exit;

    ServicesInfo := TisListOfValues.Create;

    for i := 0 to RunRes.Count - 1 do
    begin
      Res := RunRes.Objects[i] as IisParamsCollection;
      FilterParallelExecResult(Res);

      s := RunRes[i];
      s := GetNextStrValue(s, '.');

      for j := 0 to Res.Count - 1 do
        if Res[j].Value[METHOD_RESULT] then
        begin
          if ServicesInfo.ValueExists(Res.ParamName(j)) then
            ServicesInfo.Value[Res.ParamName(j)] := ServicesInfo.Value[Res.ParamName(j)] + ',' + s
          else
            ServicesInfo.AddValue(Res.ParamName(j), s);
        end;
    end;

    RB.SetKnownADRServices(ServicesInfo);
  end;
end;

function TmcHttpPresenter.rhConfigAS(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  DS: ImcDMControl;
  TableBuilder: ImcHtmlTableBulder;
  ASSettings, TblCol: IisListOfValues;
  sPage, sHost, s: String;
  i: Integer;
  Domains: IisParamsCollection;
  HREF: ImcHREF;
begin
  if AParams.ValueExists(P_Host) then
    sHost := AParams.Value[P_Host]
  else
    sHost := GetFirstASHost;
  DS := Mainboard.DeploymentManagers.FindDeploymentManager(sHost);

  if Assigned(DS) then
  begin
    ASSettings := DS.EvolutionControl.ASControl.GetSettings;

    sPage := GetResource('config_as.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_HOST, sHost);

    s := ASSettings.Value['Port'];
    sPage := ReplaceMacros(sPage, '$PORT$', GetNextStrValue(s, ':'));
    if AnsiSameText(s, 'SSL') then
      sPage := ReplaceMacros(sPage, '$SSL$', 'checked="checked"')
    else
      sPage := ReplaceMacros(sPage, '$SSL$', '');

    sPage := ReplaceMacros(sPage, '$MAX_REQUESTS$', ASSettings.Value['MaxRequests']);
    sPage := ReplaceMacros(sPage, '$STAGING_FOLDER$', ASSettings.Value['StagingFolder']);

    // Domain list
    Domains := IInterface(ASSettings.Value['Domains']) as IisParamsCollection;
    for i := 0 to Domains.Count - 1 do
    begin
      if Domains[i].Value['Password'] <> '' then
        Domains[i].Value['Password'] := cDummyPassword;

      HREF := TmcHREF.Create('config_asdb', ASession);
      HREF.AddParam(P_EvDomain, Domains.ParamName(i));
  //    HREF.AddParam(P_MainMenuItem, '|Monitoring|Server Monitor|' + RPPoolStatus[i].Value['Server']);
      Domains[i].AddValue('DBConfig', ReplaceMacros(ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml), M_CONTENT, 'DB Config'));
    end;

    TableBuilder := TmcHtmlTableBulder.Create(Domains);

    TblCol := TableBuilder.AddColumn('Service Bureau', 'Name');

    TblCol := TableBuilder.AddColumn('Active', 'Active');
    TblCol.AddValue('EditType', 'Checkbox');
    TblCol.AddValue('EditParam', 'Active_');

    TblCol := TableBuilder.AddColumn('Login', 'User');

    TblCol := TableBuilder.AddColumn('Password', 'Password');
    TblCol.AddValue('EditType', 'EditPassword');
    TblCol.AddValue('EditParam', 'Password_');

    TblCol := TableBuilder.AddColumn('', 'DBConfig');

    sPage := ReplaceMacros(sPage, '$DOMAINS_SETUP$', TableBuilder.GetHtml);
  end
  else
  begin
    sPage := GetResource('no_info.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Service is not installed');
  end;

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeAS(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  i: Integer;
  sParNbr, sPort, pwd: String;
  RB, DM: ImcDMControl;
  Settings: IisListOfValues;
  Domains: IisParamsCollection;
  D: IisListOfValues;
begin
  DM := Mainboard.DeploymentManagers.FindDeploymentManager(AParams.TryGetValue(P_Host, ''));

  if AParams.ValueExists('Max_Requests') then
  begin
    Settings := TisListOfValues.Create;
    Settings.AddValue('MaxRequests', StrToInt(Trim(AParams.Value['Max_Requests'])));
  end

  else
  begin
    Settings := DM.EvolutionControl.ASControl.GetSettings;
    RB := GetRBDM;
    if Assigned(RB) then
      Settings.Value['RequestBroker'] := RB.Host;

    sPort := Trim(AParams.TryGetValue('Port', ''));
    if (sPort <> '') and (AParams.TryGetValue('SSL', '0') = '1') then
      sPort := sPort + ':SSL';
    Settings.Value['Port'] := sPort;
    Settings.Value['StagingFolder'] := Trim(AParams.TryGetValue('Staging_Folder', ''));

    Domains := IInterface(Settings.Value['Domains']) as IisParamsCollection;
    for i := 0 to AParams.Count - 1 do
      if StartsWith(AParams[i].Name, 'rec') then
      begin
        D := Domains.ParamsByName(AParams[i].Value);
        if Assigned(D) then
        begin
          sParNbr := AParams[i].Name;
          GetNextStrValue(sParNbr, 'rec');

          D.Value['Active'] := AParams.TryGetValue('Active_' + sParNbr, '0') = '1';

          pwd := AParams.TryGetValue('Password_' + sParNbr, '');
          if pwd <> cDummyPassword then
            D.Value['Password'] := pwd;
        end;
      end;
  end;

  DM.EvolutionControl.ASControl.SetSettings(Settings);

  Result := '';
end;

function TmcHttpPresenter.rhRunASOperation(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  ADRS: IevASControl;
  DS: ImcDMControl;
  sDomain: String;
begin
  if AParams.ValueExists(P_Host) then
    DS := Mainboard.DeploymentManagers.FindDeploymentManager(AParams.Value[P_Host])
  else
    Exit;

  ADRS := DS.EvolutionControl.ASControl;

  sDomain := AParams.Value[P_EvDomain];
  if AParams.ValueExists('Check') then
    ADRS.CheckDB(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Full_Copy') then
    ADRS.FullDBCopy(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Check_Data') then
    ADRS.CheckDBData(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Check_Trans') then
    ADRS.CheckDBTransactions(sDomain, AParams.Value['DBName'])
  else if AParams.ValueExists('Rebuild_All_TT') then
    ADRS.RebuildTT(sDomain, '');

  Result := '';
end;

function TmcHttpPresenter.ACInstalled: Boolean;
begin
  Result := GetFirstACHost <> '';
end;

function TmcHttpPresenter.GetFirstACHost: String;
var
  DMs: IisParamsCollection;
  i: Integer;
begin
  DMs := ParallelExec('AC.Installed', nil);
  FilterParallelExecResult(DMs);

  Result := '';
  for i := 0 to DMs.Count - 1 do
  if DMs[i].Value[METHOD_RESULT] then
  begin
    Result := DMs.ParamName(i);
    break;
  end;
end;

function TmcHttpPresenter.GetFirstASHost: String;
var
  DMs: IisParamsCollection;
  i: Integer;  
begin
  DMs := ParallelExec('AS.Installed', nil);
  FilterParallelExecResult(DMs);

  Result := '';
  for i := 0 to DMs.Count - 1 do
  if DMs[i].Value[METHOD_RESULT] then
  begin
    Result := DMs.ParamName(i);
    break;
  end;
end;

procedure TmcHttpPresenter.SyncTCPSettings;
var
  HTTPPort: String;
begin
  StopTCPServers;

  HTTPPort := Mainboard.AppSettings.GetValue('Settings\HTTPPort', '80');
  if FUseSSL then
  begin
    FHttpServer.LocalPort := Mainboard.AppSettings.GetValue('Settings\HTTPSPort', '443');
    FSSLContext := OpenSSLLib.NewServerContext(AppDir + 'root.pem', AppDir + 'cert.pem', AppDir + 'key.pem');
    FHttpRedirect.LocalPort := HTTPPort;
    FHttpRedirect.Active := True;
  end
  else
    FHttpServer.LocalPort := HTTPPort;

  FHttpServer.Active := True;
end;

procedure TmcHttpPresenter.StopTCPServers;
begin
  FHttpServer.Active := False;
  FHttpRedirect.Active := False;

  if FSSLContext <> nil then
  begin
    SSL_free(FSSLContext);
    FSSLContext := nil;
  end;
end;

procedure TmcHttpPresenter.DoOnDestruction;
begin
  StopTCPServers;

  FHttpServer.Free;
  FHttpRedirect.Free;
  FRequestHandlers.Free;

  inherited;
end;

function TmcHttpPresenter.rhConfigDBPasswords(const ASession: ImcSession; const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage: String;
  RB: IevRBControl;
  DBPasswords: IisListOfValues;
begin
  sPage := GetResource('config_db_passwords.html', AMIMEType);

  RB := GetRBControl(True);
  DBPasswords := RB.GetDatabasePasswords;

  sPage := ReplaceMacros(sPage, '$DBSysdbaPassword$', IfThen(DBPasswords.Value['DBAdminPassword'] = '', '',  cDummyPassword));
  sPage := ReplaceMacros(sPage, '$DBEuserPassword$', IfThen(DBPasswords.Value['DBUserPassword'] = '', '',  cDummyPassword));

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;


function TmcHttpPresenter.rhChangeDBPasswords(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  s, sPage: String;
  RB: IevRBControl;
  DBPasswords: IisListOfValues;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  DBPasswords := TisListOfValues.Create;
  s := AParams.Value['dbSysdbaPassword'];
  if (s <> '') and (s <> cDummyPassword) then
    DBPasswords.AddValue('DBAdminPassword', s);

  s := AParams.Value['dbEuserPassword'];
  if (s <> '') and (s <> cDummyPassword) then
    DBPasswords.AddValue('DBUserPassword', s);

  if DBPasswords.Count > 0 then
  begin
    RB.SetDatabasePasswords(DBPasswords);

    sPage := GetResource('oper_message.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'Database passwords have been successfully changed.');

    Result := GetPageTemplate(ASession, AMIMEType);
    Result := ReplaceMacros(Result, M_CONTENT, sPage);
  end;
end;

function TmcHttpPresenter.rhResetAdmin(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  sPage, sDomain, sDomainDescr: String;
begin
  SelectEvDomain(AParams, sDomain, sDomainDescr, sPage, AMIMEType);
  if sPage = '' then
    sPage := GetDomainContextPage('reset_admin.html', sDomain, sDomainDescr);

  Result := GetPageTemplate(ASession, AMIMEType);
  Result := ReplaceMacros(Result, M_CONTENT, sPage);
end;

function TmcHttpPresenter.rhChangeAdminPassword(const ASession: ImcSession;
  const AParams: IisListOfValues; out AMIMEType: String): String;
var
  RB: IevRBControl;
  sPage: String;
begin
  RB := GetRBControl;
  if not Assigned(RB) then
    Exit;

  if AParams.Value['dbPassword'] <> '' then
  begin
    RB.ResetAdminPassword(AParams.Value[P_EvDomain], AParams.Value['dbPassword']);

    sPage := GetResource('oper_message.html', AMIMEType);
    sPage := ReplaceMacros(sPage, M_CONTENT, 'ADMIN account has been successfully reset.');

    Result := GetPageTemplate(ASession, AMIMEType);
    Result := ReplaceMacros(Result, M_CONTENT, sPage);
  end;  
end;

{ TmcHtmlTableBulder }

function TmcHtmlTableBulder.AddColumn(const ATitle, AParamName: String): IisListOfValues;
begin
  Result := FColumns.AddParams(ATitle);
  Result.AddValue('Param', AParamName);
  Result.AddValue('EditType', '');
  Result.AddValue('EditParam', AParamName);
  Result.AddValue('Width', '');
  Result.AddValue('Size', '');
  Result.AddValue('Options', '');
end;

constructor TmcHtmlTableBulder.Create(const ATableData: IisParamsCollection);
begin
  inherited Create;
  FColumns := TisParamsCollection.Create;
  FTableData := ATableData;
  FTableOutline := hfTableOutline;
end;

function TmcHtmlTableBulder.GetHtml: String;
var
  RowNbr, i: Integer;
  sHeader, sDetail, sRowId: String;

  function BuildCell(const AColInfo: IisListOfValues; const ACellData: IisNamedValue): String;
  var
    s, sCellVal, sCell, sColType: String;
    ListBuilder: ImcHtmlListBuilder;
    ChBoxBuilder: ImcHtmlCheckBoxBuilder;
  begin
    if Assigned(ACellData) then
    begin
      if VarIsType(ACellData.Value, varDate) and (ACellData.Value = 0) then
        sCellVal := ''
      else
        sCellVal := ACellData.Value;
    end
    else
      sCellVal := hfSpace;

    sColType := AColInfo.TryGetValue('EditType', '');
    if AnsiSameText(sColType, 'EditBox') then
    begin
      sCell := ReplaceMacros(hfEditBox, M_PARAM, AColInfo.Value['EditParam'] + IntToStr(RowNbr));
      sCell := ReplaceMacros(sCell, M_VALUE, sCellVal);
      s := AColInfo.TryGetValue('Size', '');
      if s <> '' then
        sCell := ReplaceMacros(sCell, M_ATTRIBUTES, 'size="' + s + '"');
    end

    else if AnsiSameText(sColType, 'EditPassword') then
    begin
      sCell := ReplaceMacros(hfEditPassword, M_PARAM, AColInfo.Value['EditParam'] + IntToStr(RowNbr));
      sCell := ReplaceMacros(sCell, M_VALUE, sCellVal);
      s := AColInfo.TryGetValue('Size', '');
      if s <> '' then
        sCell := ReplaceMacros(sCell, M_ATTRIBUTES, 'size="' + s + '"');
    end

    else if AnsiSameText(sColType, 'List') then
    begin
      ListBuilder := TmcHtmlListBuilder.Create(AColInfo.Value['EditParam'] + IntToStr(RowNbr));
      ListBuilder.AddValues(AColInfo.TryGetValue('Options', ''));
      ListBuilder.SelectItem(sCellVal);
      sCell := ListBuilder.GetHtml;
    end

    else if AnsiSameText(sColType, 'Checkbox') then
    begin
      ChBoxBuilder := TmcHtmlCheckBoxBuilder.Create(AColInfo.Value['EditParam'] + IntToStr(RowNbr), '1', '');
      if sCellVal = VarToStr(True) then
        sCellVal := '1'
      else if sCellVal = VarToStr(False) then
        sCellVal := '0';

      ChBoxBuilder.TryCheck(sCellVal);
      ChBoxBuilder.Enabled := AColInfo.TryGetValue('Enabled', True);
      sCell := ChBoxBuilder.GetHtml;
    end

    else
      sCell := sCellVal;

    if sCell = '' then
      sCell := hfSpace;

    s := '';
    if AColInfo.Value['Width'] <> '' then
      s := s + ' width="' + AColInfo.Value['Width'] + '"';

    Result := ReplaceMacros(hfTableDetailCell, M_ATTRIBUTES, s);
    Result := ReplaceMacros(Result, M_CONTENT, sCell);
  end;

  function BuildRow(const ARowData: IisListOfValues): String;
  var
    i: Integer;
    Val: IisNamedValue;
    Col: IisListOfValues;
    sRecId: String;
  begin
    Result := '';
    for i := 0 to FColumns.Count - 1 do
    begin
      Col := FColumns[i];
      if Assigned(ARowData) then
        Val := ARowData.FindValue(Col.Value['Param']);

      Result := Result + BuildCell(Col, Val);
    end;

    sRecId := ReplaceMacros(hfHiddenField, M_PARAM, 'rec' + IntToStr(RowNbr));
    sRecId := ReplaceMacros(sRecId, M_VALUE, sRowId);
    Result := ReplaceMacros(hfTableRow, M_CONTENT, sRecId + Result);
  end;

begin
  Result := '';

  sHeader := '';
  for i := 0 to FColumns.Count - 1 do
    sHeader := sHeader + ReplaceMacros(hfTableHeaderCell, M_CONTENT, FColumns.ParamName(i));

  if FTableData.Count > 0 then
  begin
    sDetail := '';
    for RowNbr := 0 to FTableData.Count - 1 do
    begin
      sRowId := FTableData.ParamName(RowNbr);
      sDetail := sDetail + BuildRow(FTableData[RowNbr]);
    end;
  end
  else
    sDetail := BuildRow(nil);

  Result := ReplaceMacros(FTableOutline, M_CONTENT, sHeader + sDetail);
end;

function TmcHtmlTableBulder.GetHtml(const ACurrentMethodParams: IisListOfValues; const ASession: ImcSession;
 const APageCount: Integer; const APageClickMethod: String; const AMethodParams: TisCommaDilimitedString;
 const ATableTemplate: String = ''): String;
var
  CurrentPage: Integer;
  MethodParams: IisListOfValues;
  s, p: String;
  V: IisNamedValue;

  function CreatePageHREF(const APageNbr: Integer): ImcHREF;
  var
    i: Integer;
  begin
    Result := TmcHREF.Create(APageClickMethod, ASession);
    Result.AddParam(P_Page, IntToStr(APageNbr));
    for i := 0 to MethodParams.Count - 1 do
      Result.AddParam(MethodParams[i].Name, MethodParams[i].Value);
  end;

  function GetPageLinks: String;
  var
    StartPage, EndPage, i: Integer;
    HREF: ImcHREF;
    sC, FirstHtm, PrevHtm, NextHtm, LastHtm: String;
  const
    LinksCount = 10;
  begin
    Result := '';
    if APageCount > 1 then
    begin
      StartPage := ((CurrentPage - 1) div LinksCount) * LinksCount + 1;
      EndPage := StartPage + LinksCount - 1;
      if EndPage > APageCount then
        EndPage := APageCount;

      for i := StartPage to EndPage do
      begin
        if i = CurrentPage then
          Result := Result + hfSpace + IntToStr(i)
        else
        begin
          HREF := CreatePageHREF(i);
          sC := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
          sC := ReplaceMacros(sC, M_CONTENT, IntToStr(i));
          Result := Result + hfSpace + sC;
        end;
      end;

      if CurrentPage > 1 then
      begin
        HREF := CreatePageHREF(CurrentPage-1);
        sC := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
        sC := ReplaceMacros(sC, M_CONTENT, 'Prev');
        PrevHtm := sC;
        HREF :=  CreatePageHREF(1);
        sC := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
        sC := ReplaceMacros(sC, M_CONTENT, 'First');
        FirstHtm := sC;
      end
      else
      begin
        FirstHtm := 'First';
        PrevHtm := 'Prev';
      end;

      if CurrentPage < APageCount then
      begin
        HREF := CreatePageHREF(CurrentPage + 1);
        sC := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
        sC := ReplaceMacros(sC, M_CONTENT, 'Next');
        NextHtm := sC;
        HREF := CreatePageHREF(APageCount);
        sC := ReplaceMacros(hfTextLink, M_LINK, HREF.GetHtml);
        sC := ReplaceMacros(sC, M_CONTENT, hfSpace + 'Last');
        LastHtm := sC;
      end
      else
      begin
        NextHtm := 'Next';
        LastHtm := 'Last';
      end;

      Result := '<table border=0 width=100%><tr><td align="right"  colspan="2"><b>'+
      'Page '+IntToStr(CurrentPage)+' of '+IntToStr(APageCount)+'<br></b></td></tr>' +
      '<tr><td align="left"><b>' + PrevHtm + '</b></td><td align="right"><b>' + FirstHtm + '</b></td></tr>'+
      '<tr><td align="center" colspan="2"><b>' + Result + '</b></td></tr>'+
      '<tr><td align="left"><b>' + NextHtm + '</b></td><td align="right"><b>' + LastHtm + '</b></td></tr>'+
      '</table>';
    end;
  end;

begin
  if ATableTemplate <> '' then
    FTableOutline := ATableTemplate;
  CurrentPage := StrToIntDef(ACurrentMethodParams.TryGetValue(P_Page, '1'), 1);

  MethodParams := TisListOfValues.Create;
  s := AMethodParams;
  while s <> '' do
  begin
    p := GetNextStrValue(s, ',');
    V := ACurrentMethodParams.FindValue(p);
    if Assigned(V) then
      MethodParams.AddValue(V.Name, V.Value);
  end;

  Result :='<table width="335" border="0"><tr><td height="16" valign="top" colspan="7" nowrap="nowrap">$PAGES$</td></tr><tr><td height="16" valign="top" colspan="7" nowrap="nowrap">$TABLE$</td></tr></table>';
  Result := ReplaceMacros(Result, '$PAGES$', GetPageLinks);
  Result := ReplaceMacros(Result, '$TABLE$', GetHTML);
end;



procedure TmcMainMenuBuilder.AddAAItems(const AOwner, AMethod: String);
var
  i: Integer;
  sFullPath, sName, sMethod: String;
  Res: IisParamsCollection;
begin
  Res := Mainboard.DeploymentManagers.ParallelExec(['AA.Installed'], []).Objects[0] as IisParamsCollection;
  Res.Sort;

  for i := 0 to Res.Count - 1 do
  begin
    if not Res[i].ValueExists('Error') then
      if Res[i].Value[METHOD_RESULT] then
      begin
        sName := Res.ParamName(i);
        sFullPath := AOwner + '|' + sName;
        sMethod := ReplaceMacros(AMethod, M_AA_HOST, sName);
        AddItem(sFullPath, AOwner, sName, sMethod, '0');
      end;
  end;
end;

procedure TmcMainMenuBuilder.AddACItems(const AOwner, AMethod: String);
var
  i: Integer;
  sFullPath, sName, sMethod: String;
  Res: IisParamsCollection;
begin
  Res := Mainboard.DeploymentManagers.ParallelExec(['AC.Installed'], []).Objects[0] as IisParamsCollection;
  Res.Sort;

  for i := 0 to Res.Count - 1 do
  begin
    if not Res[i].ValueExists('Error') then
      if Res[i].Value[METHOD_RESULT] then
      begin
        sName := Res.ParamName(i);
        sFullPath := AOwner + '|' + sName;
        sMethod := ReplaceMacros(AMethod, M_AC_HOST, sName);
        AddItem(sFullPath, AOwner, sName, sMethod, '0');
      end;
  end;
end;

procedure TmcMainMenuBuilder.AddASItems(const AOwner, AMethod: String);
var
  i: Integer;
  sFullPath, sName, sMethod: String;
  Res: IisParamsCollection;
begin
  Res := Mainboard.DeploymentManagers.ParallelExec(['AS.Installed'], []).Objects[0] as IisParamsCollection;
  Res.Sort;

  for i := 0 to Res.Count - 1 do
  begin
    if not Res[i].ValueExists('Error') then
      if Res[i].Value[METHOD_RESULT] then
      begin
        sName := Res.ParamName(i);
        sFullPath := AOwner + '|' + sName;
        sMethod := ReplaceMacros(AMethod, M_AS_HOST, sName);
        AddItem(sFullPath, AOwner, sName, sMethod, '0');
      end;
  end;
end;

procedure TmcMainMenuBuilder.AddDMItems(const AOwner, AMethod: String);
var
  i: Integer;
  DS: ImcDMControl;
  sFullPath, sMethod: String;
begin
  Mainboard.DeploymentManagers.Lock;
  try
    for i := 0 to Mainboard.DeploymentManagers.Count - 1 do
    begin
      DS := Mainboard.DeploymentManagers[i];
      sFullPath := AOwner + '|' + DS.Host;
      sMethod := ReplaceMacros(AMethod, M_DM_HOST, DS.Host);
      AddItem(sFullPath, AOwner, DS.Host, sMethod, '0');
    end;
  finally
    Mainboard.DeploymentManagers.Unlock;
  end;
end;

procedure TmcMainMenuBuilder.AddItem(const APath, AOwner, AName, AMethod, ASecurityLevel: String);
var
  LV: IisListOfValues;
begin
  LV := FStructure.AddParams(APath);
  LV.AddValue('Owner', AOwner);
  LV.AddValue('Name', AName);
  LV.AddValue('Method', AMethod);
  LV.AddValue('SecLevel', TmcUserAccountType(StrToIntDef(ASecurityLevel, 0)));
end;

procedure TmcMainMenuBuilder.AddRPItems(const AOwner, AMethod: String);
var
  i: Integer;
  sFullPath, sName, sMethod: String;
  Res: IisParamsCollection;
begin
  Res := Mainboard.DeploymentManagers.ParallelExec(['RP.Installed'], []).Objects[0] as IisParamsCollection;
  Res.Sort;

  for i := 0 to Res.Count - 1 do
  begin
    if not Res[i].ValueExists('Error') then
      if Res[i].Value[METHOD_RESULT] then
      begin
        sName := Res.ParamName(i);
        sFullPath := AOwner + '|' + sName;
        sMethod := ReplaceMacros(AMethod, M_RP_HOST, sName);
        AddItem(sFullPath, AOwner, sName, sMethod, '0');
      end;
  end;
end;

procedure TmcMainMenuBuilder.AddRRItems(const AOwner, AMethod: String);
var
  i: Integer;
  sFullPath, sName, sMethod: String;
  Res: IisParamsCollection;
begin
  Res := Mainboard.DeploymentManagers.ParallelExec(['RR.Installed'], []).Objects[0] as IisParamsCollection;
  Res.Sort;

  for i := 0 to Res.Count - 1 do
  begin
    if not Res[i].ValueExists('Error') then
      if Res[i].Value[METHOD_RESULT] then
      begin
        sName := Res.ParamName(i);
        sFullPath := AOwner + '|' + sName;
        sMethod := ReplaceMacros(AMethod, M_RR_HOST, sName);
        AddItem(sFullPath, AOwner, sName, sMethod, '0');
      end;
  end;
end;

procedure TmcMainMenuBuilder.BuildStructure;
var
  s, sFullPath, sOwnerItem, sMenuItem, sMethod, sSecurityLevel: String;
begin
  FStructure := TisParamsCollection.Create;

  s := sMainMenuStructure;
  while s <> '' do
  begin
    sSecurityLevel := GetNextStrValue(s, ',');
    sFullPath := Trim(GetNextStrValue(sSecurityLevel, '^'));
    sOwnerItem := sFullPath;
    sMenuItem := GetPrevStrValue(sOwnerItem, '|');
    sMethod := Trim(GetNextStrValue(sSecurityLevel, '^'));

    AddItem(sFullPath, sOwnerItem, sMenuItem, sMethod, sSecurityLevel);
  end;
end;

constructor TmcMainMenuBuilder.Create(const ASession: ImcSession);
begin
  inherited Create;
  FSession := ASession;
  BuildStructure;
end;

function TmcMainMenuBuilder.GetHtml(const ALockedMode: Boolean): String;

  procedure BuildDynamicItems(const AName, AOwner, AMethod: String);
  begin
    if AnsiSameText(AName, M_DM_HOST) then
      AddDMItems(AOwner, AMethod)
    else if AnsiSameText(AName, M_RP_HOST) then
      AddRPItems(AOwner, AMethod)
    else if AnsiSameText(AName, M_RR_HOST) then
      AddRRItems(AOwner, AMethod)
    else if AnsiSameText(AName, M_AA_HOST) then
      AddAAItems(AOwner, AMethod)
    else if AnsiSameText(AName, M_AC_HOST) then
      AddACItems(AOwner, AMethod)
    else if AnsiSameText(AName, M_AS_HOST) then
      AddASItems(AOwner, AMethod);
  end;

  function BuildMenu(const AOwnerMenuItem: String; const ALevel: Integer): String;
  var
    i: Integer;
    sIndent, s, sItem: String;
    HREF: ImcHREF;
  begin
    Result := '';
    sIndent := '';
    for i := 0 to ALevel - 1 do
      sIndent := sIndent + hfSpace + hfSpace + hfSpace + hfSpace + hfSpace;

    i := 0;
    while i < FStructure.Count do
    begin
      if (not ALockedMode or (FStructure[i].Value['SecLevel'] = 0)) and
         Assigned(FSession.UserAccount) and (FStructure[i].Value['SecLevel'] <= FSession.UserAccount.AccountType) and 
         AnsiSameText(AOwnerMenuItem, FStructure[i].Value['Owner']) then
      begin
        if StartsWith(FStructure[i].Value['Name'], '$') then
          BuildDynamicItems(FStructure[i].Value['Name'], FStructure[i].Value['Owner'], FStructure[i].Value['Method'])
        else
        begin
          HREF := TmcHREF.Create(FStructure[i].Value['Method'], FSession);
          HREF.AddParam(P_MainMenuItem, FStructure.ParamName(i));
          if StartsWith(FSession.MainMenuPosition, FStructure.ParamName(i)) then
            s := BuildMenu(FStructure.ParamName(i), ALevel + 1)
          else
            s := '';

          if AnsiSameText(FStructure.ParamName(i), FSession.MainMenuPosition) then
            sItem := hfMainMenuSelectedItem
          else
            sItem := hfMainMenuItem;

          sItem := ReplaceMacros(sItem, M_CONTENT, sIndent + MakeHtmlText(FStructure[i].Value['Name']));
          sItem := ReplaceMacros(sItem, M_METHOD, 'href="' + HREF.GetHtml + '"');

          Result := Result + sItem + s;
        end;
      end;

      Inc(i);
    end;
  end;

begin
  Result := BuildMenu('', 0);

  Result := '<table border="0" cellspacing="0" cellpadding="0" width="100%">' +
             Result + '</table>';
end;


{ TmcSession }

procedure TmcSession.Authorize(const AUser, APassword: String);
var
  Acc: ImcUserAccount;
begin
  Acc := Mainboard.UserAccounts.FindUser(AUser);
  if Assigned(Acc) and (Acc.Password = APassword) then
    FUserAccount := Acc
  else
    FUserAccount := nil;
end;

function TmcSession.Authorized: Boolean;
begin
  Result := Assigned(FUserAccount);
end;

constructor TmcSession.Create(const AName: String);
begin
  inherited;
  SetName(GetUniqueID);
end;

function TmcSession.GetEvDomain: String;
begin
  Result := FEvDomain;
end;

function TmcSession.GetMainMenuPosition: String;
begin
  Result := FMainMenuPosition;
end;

function TmcSession.GetRefreshTime: Integer;
begin
  Result := FRefreshTime;
end;

function TmcSession.GetRequestID: TTaskID;
begin
  Result := FRequestID;
end;

function TmcSession.GetRequestResult: String;
begin
  Result := FRequestResult;
end;

function TmcSession.GetRequestResultMimeType: String;
begin
  Result := FRequestResultMimeType;
end;

function TmcSession.SessionID: TisGUID;
begin
  Result := GetName;
end;

procedure TmcSession.SetEvDomain(const AValue: String);
begin
  FEvDomain := AValue;
end;

procedure TmcSession.SetMainMenuPosition(const AValue: String);
begin
  FMainMenuPosition := AValue;
end;

procedure TmcSession.SetRefreshTime(const AValue: Integer);
begin
  FRefreshTime := AValue;
end;

procedure TmcSession.SetRequestID(const AValue: TTaskID);
begin
  FRequestID := AValue;
end;

procedure TmcSession.SetRequestResult(const AValue: String);
begin
  FRequestResult := AValue;
end;

procedure TmcSession.SetRequestResultMimeType(const Value: String);
begin
  FRequestResultMimeType := Value;
end;

function TmcSession.UserAccount: ImcUserAccount;
begin
  Result := FUserAccount;
end;

{ TmcHREF }

procedure TmcHREF.AddParam(const AParamName, AValue: String);
begin
  Fhtml := Fhtml + '&' + AParamName + '=' + EncodeURL(AValue, True);
end;

constructor TmcHREF.Create(const AMethod: String; const ASession: ImcSession);
begin
  inherited Create;
  Fhtml := AMethod;
  if Assigned(ASession) then
    AddParam(P_Session, ASession.SessionID);
end;

function TmcHREF.GetHtml: String;
begin
  Result := Fhtml;
end;

{ TmcHtmlListBuilder }

procedure TmcHtmlListBuilder.AddValue(const AName, AValue: String);
var
  Par: IisListOfValues;
begin
  Par := FValues.ParamsByName(AValue);
  if not Assigned(Par) then
    Par := FValues.AddParams(AValue);

  Par.AddValue('Name', AName);
  Par.AddValue('Value', AValue);
  Par.AddValue('Selected', False);  
end;

procedure TmcHtmlListBuilder.AddValues(const AText: String);
var
  s, sVal, sName: String;
begin
  s := AText;
  while s <> '' do
  begin
    sVal := GetNextStrValue(s, #13);
    sName := GetNextStrValue(sVal, '=');
    AddValue(sName, sVal);
  end;
end;

constructor TmcHtmlListBuilder.Create(const AParamName: String);
begin
  inherited Create;
  FValues := TisParamsCollection.Create;
  FParamName := AParamName;
end;

function TmcHtmlListBuilder.GetHtml: String;
var
  i: Integer;
  Val: IisListOfValues;
  s: String;
begin
  Result := '';
  for i := 0 to FValues.Count - 1 do
  begin
    Val := FValues[i];
    s := '<option value="$VALUE$" $SELECTED$>$TEXT$</option>';
    s := ReplaceMacros(s, '$VALUE$', Val.Value['Value']);
    s := ReplaceMacros(s, '$TEXT$', Val.Value['Name']);
    if Val.Value['Selected'] then
      s := ReplaceMacros(s, '$SELECTED$', 'selected="selected"')
    else
      s := ReplaceMacros(s, '$SELECTED$', '');
    Result := Result + s;
  end;
  Result := ReplaceMacros(hfList, M_CONTENT, Result);
  Result := ReplaceMacros(Result, M_CAPTION, FParamName);
end;

procedure TmcHtmlListBuilder.SelectItem(const AValue: String);
var
  Par: IisListOfValues;
begin
  Par := FValues.ParamsByName(AValue);
  if Assigned(Par) then
    Par.Value['Selected'] := True;  
end;

{ TmcHtmlCheckBoxBuilder }

function TmcHtmlCheckBoxBuilder.Checked: Boolean;
begin
  Result := FChecked;
end;

constructor TmcHtmlCheckBoxBuilder.Create(const AParamName, AValue, ACaption: String);
begin
  inherited Create;
  FEnabled := True;
  FParamName := AParamName;
  FCaption := ACaption;
  FValue := AValue; 
end;

function TmcHtmlCheckBoxBuilder.GetCaptionLocation: TmcLocation;
begin
  Result := FCaptionLocation;
end;

function TmcHtmlCheckBoxBuilder.GetEnabled: Boolean;
begin
  Result := FEnabled;
end;

function TmcHtmlCheckBoxBuilder.GetHtml: String;
var
  s, sAttr: String;
begin
  s := ReplaceMacros(hfCheckBox, M_PARAM, FParamName);
  s := ReplaceMacros(s, M_VALUE, FValue);

  sAttr := '';
  if FChecked then
    AddStrValue(sAttr, 'checked', ' ');
  if not FEnabled then
    AddStrValue(sAttr, 'disabled', ' ');

  Result := ReplaceMacros(s, M_ATTRIBUTES, sAttr);

  if FCaption <> '' then
  begin
   case FCaptionLocation of
     lLeft:   Result := FCaption + Result;
     lRight:  Result := Result + FCaption;
     lTop:    Result := FCaption + hfBR + Result;
     lBottom: Result := Result + hfBR + FCaption;
   end;
   Result := ReplaceMacros(hfLable, M_CONTENT, Result);
 end;
end;

procedure TmcHtmlCheckBoxBuilder.SetCaptionLocation(const AValue: TmcLocation);
begin
  FCaptionLocation := AValue;
end;

procedure TmcHtmlCheckBoxBuilder.SetEnabled(const AValue: Boolean);
begin
  FEnabled := AValue;
end;

procedure TmcHtmlCheckBoxBuilder.TryCheck(const AValue: String);
begin
  FChecked := AValue = FValue;
end;

{ TmcHtmlButtonBuilder }

procedure TmcHtmlButtonBuilder.AddMethodParam(const AParamName, AValue: String);
begin
  FHREF.AddParam(AParamName, AValue);
end;

constructor TmcHtmlButtonBuilder.Create(const AMethod, ACaption: String;
  const APostButton: Boolean; const ASession: ImcSession);
begin
  inherited Create;
  FHREF := TmcHREF.Create(AMethod, ASession);
  FCaption := ACaption;
  FPostButton := APostButton;
end;

function TmcHtmlButtonBuilder.GetConfirmation: String;
begin
  Result := FConfirmation;
end;

function TmcHtmlButtonBuilder.GetHtml: String;
var
  sAttr: String;
begin
  if FPostButton then
    Result := hfPostButton
  else
    Result := hfGetButton;

  Result := ReplaceMacros(Result, M_METHOD, FHREF.GetHtml);

  sAttr := '';
  if FConfirmation <> '' then
    sAttr := ReplaceMacros(hfOnClickConfirmation, M_TEXT, FConfirmation);
  Result := ReplaceMacros(Result, M_ATTRIBUTES, sAttr);

  Result := ReplaceMacros(Result, M_CAPTION, FCaption);
end;

procedure TmcHtmlButtonBuilder.SetConfirmation(const AValue: String);
begin
  FConfirmation := AValue;
end;

{ TmcHtmlEditBoxBuilder }

constructor TmcHtmlEditBoxBuilder.Create(const AParamName, AValue: String);
begin
  inherited Create;
  FParamName := AParamName;
  FValue := AValue;
end;

function TmcHtmlEditBoxBuilder.GetHtml: String;
begin
  if FSecured then
    Result := hfEditPassword
  else
    Result := hfEditBox;

  Result := ReplaceMacros(Result, M_PARAM, FParamName);
  Result := ReplaceMacros(Result, M_VALUE, FValue);

  if FSize > 0 then
    Result := ReplaceMacros(Result, M_ATTRIBUTES, 'size="' + IntToStr(FSize) + '"');
end;

function TmcHtmlEditBoxBuilder.GetSecured: Boolean;
begin
  Result := FSecured;
end;

function TmcHtmlEditBoxBuilder.GetSize: Integer;
begin
  Result := FSize;
end;

procedure TmcHtmlEditBoxBuilder.SetSecured(const AValue: Boolean);
begin
  FSecured := AValue;
end;

procedure TmcHtmlEditBoxBuilder.SetSize(const AValue: Integer);
begin
  FSize := AValue;
end;

end.
