unit mcEvolutionControl;

interface

uses Classes, SysUtils, isBaseClasses, evRemoteMethods, EvTransportDatagrams, EvDMInterfaces,
     EvConsts, EvControllerInterfaces, EvTransportInterfaces, mcTypes, isLogFile;

type
  TmcEvolutionControl = class(TisCollection, ImcEvolutionControl)
  private
    function  GetDMControl: IevDMControl;
  protected
    procedure DoOnConstruction; override;
    procedure Install(const AArchiveFile: String);
    procedure Uninstall;
    function  GetArchiveFileName: String;

    function  GetRPInstalled: Boolean;
    function  GetRPStarted: Boolean;
    function  GetRPControl: IevRPControl;
    procedure InstallRP;
    procedure UnInstallRP;
    procedure StartRP;
    procedure StopRP;
    procedure RestartRP;

    function  GetRBInstalled: Boolean;
    function  GetRBStarted: Boolean;
    function  GetRBControl: IevRBControl;
    procedure InstallRB;
    procedure UnInstallRB;
    procedure StartRB;
    procedure StopRB;
    procedure RestartRB;

    function  GetRRControl: IevRRControl;
    function  GetRRInstalled: Boolean;
    function  GetRRStarted: Boolean;
    procedure InstallRR;
    procedure UninstallRR;
    procedure StartRR;
    procedure StopRR;
    procedure RestartRR;

    function  GetAAControl: IevAAControl;
    function  GetAAInstalled: Boolean;
    function  GetAAStarted: Boolean;
    procedure InstallAA;
    procedure UninstallAA;
    procedure StartAA;
    procedure StopAA;
    procedure RestartAA;

    function  GetACControl: IevACControl;
    function  GetACInstalled: Boolean;
    function  GetACStarted: Boolean;
    procedure InstallAC;
    procedure UninstallAC;
    procedure StartAC;
    procedure StopAC;
    procedure RestartAC;

    function  GetASControl: IevASControl;
    function  GetASInstalled: Boolean;
    function  GetASStarted: Boolean;
    procedure InstallAS;
    procedure UninstallAS;
    procedure StartAS;
    procedure StopAS;
    procedure RestartAS;

    function  GetDBMControl: IevDBMControl;
  public
  end;


implementation

const AppEvolution = 'Evolution';
      SrvcEvRequestBroker = 'EvolutionRequestBroker';
      SrvcEvRequestProcessor = 'EvolutionRequestProcessor';
      SrvcEvRemoteRelay = 'EvolutionRemoteRelay';
      SrvcEvApiAdapter = 'EvolutionAPIAdapter';
      SrvcEvADRClient = 'EvolutionADRClient';
      SrvcEvADRServer = 'EvolutionADRServer';      

type
  TevControlProxy = class(TisInterfacedObject)
  private
    function  GetDMControl: ImcDMControl;
  end;


  TevRPControlProxy = class(TevControlProxy, IevRPControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetHardware: IisStringList;
    function  GetModules: IisStringList;
    function  GetLoadStatus: IisListOfValues;
    function  GetExecStatus: IisParamsCollection;
    function  GetAppTempFolder: String;
    procedure SetAppTempFolder(const ATempFolder: String);
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    function  GetMaintenanceDB: IisListOfValues;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
  end;


  TevRBControlProxy = class(TevControlProxy, IevRBControl)
  protected
    function  GetMachineInfo: IisListOfValues;
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetHardware: IisStringList;
    function  GetModules: IisStringList;
    function  GetDomainList: IisListOfValues;
    function  GetLicenseInfo(const ADomainName: String): IisListOfValues;
    procedure SetLicenseInfo(const ADomainName: String; const ASerialNumber, ARegistrationCode: String);
    function  GetFolders(const ADomainName: String): IisListOfValues;
    procedure SetFolders(const ADomainName: String; const AFolders: IisListOfValues);
    procedure GetEMailInfo(var AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String);
    procedure SetEMailInfo(const AHost, APort, AUser, APassword, ASentFrom, ASendAlertsTo: String);
    procedure SetDatabasePasswords(const AParams: IisListOfValues);
    function  GetDatabasePasswords: IisListOfValues;
    procedure SetDatabaseParams(const ADomainName: String; const AParams: IisListOfValues);
    function  GetDatabaseParams(const ADomainName: String): IisListOfValues;
    function  GetRPPoolInfo: IisParamsCollection;
    procedure SetRPPoolInfo(const AInfo: IisParamsCollection);
    function  GetStackInfo: String;
    procedure GetSystemAccount(const ADomainName: String; out AUser: String);
    procedure SetSystemAccount(const ADomainName, AUser: String);
    procedure GetAdminAccount(const ADomainName: String; out AUser: String);
    procedure SetAdminAccount(const ADomainName, AUser: String);
    function  GetRPPoolStatus: IisParamsCollection;
    function  UsersPoolStatus: IisParamsCollection;
    function  GetAppTempFolder: String;
    procedure SetAppTempFolder(const ATempFolder: String);
    function  GetPrintersInfo: IisListOfValues;
    procedure SetPrintersInfo(const APrintersInfo: IisListOfValues);
    procedure SendMessage(const aText: String; const aToUsers: String = '');
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    function  GetMaintenanceDB: IisListOfValues;
    function  GetSystemStatus: IisParamsCollection;
    function  GetAUS: String;
    procedure SetAUS(const AValue: String);
    function  GetInternalRR: IisStringList;
    procedure SetInternalRR(const ARRList: IisStringList);
    function  GetInternalUserRR(const ADomainName: String): Boolean;
    procedure SetInternalUserRR(const ADomainName: String; const AValue: Boolean);
    function  GetBBDiagnosticCheck: String;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetKnownADRServices(const AServices: IisListOfValues);
    procedure ResetAdminPassword(const ADomainName: String; const APassword: String);
  end;


  TevRRControlProxy = class(TevControlProxy, IevRRControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    function  GetConnectionStatus: IisParamsCollection;
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetSessionsStatus: IisParamsCollection;
    function  GetStackInfo: String;
  end;

  TevAAControlProxy = class(TevControlProxy, IevAAControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
  end;

  TevACControlProxy = class(TevControlProxy, IevACControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
    function  GetStatus: IisParamsCollection;
    function  GetDomainStatus(const ADomainName: String): IisListOfValues;
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetTransRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetDBList(const ADomainName: String): IisStringList;
    procedure SyncDBList(const ADomainName: String);
    procedure CheckDB(const ADomainName: String; const ADataBase: String);
    procedure FullDBCopy(const ADomainName: String; const ADataBase: String);
    procedure CheckDBTransactions(const ADomainName: String; const ADataBase: String);
    procedure CheckDBData(const ADomainName: String; const ADataBase: String);
  end;

  TevASControlProxy = class(TevControlProxy, IevASControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetSettings: IisListOfValues;
    procedure SetSettings(const ASettings: IisListOfValues);
    procedure GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
    procedure SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
    function  GetStackInfo: String;
    function  GetStatus: IisParamsCollection;
    function  GetDomainStatus(const ADomainName: String): IisListOfValues;
    function  GetRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetTransRequestList(const APageNbr, AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
    function  GetClientDBList(const ADomainName: String): IisStringList;
    procedure CheckDB(const ADomainName: String; const ADataBase: String);
    procedure FullDBCopy(const ADomainName: String; const ADataBase: String);
    procedure CheckDBData(const ADomainName: String; const ADataBase: String);
    procedure CheckDBTransactions(const ADomainName: String; const ADataBase: String);
    procedure RebuildTT(const ADomainName: String; const ADataBase: String);
  end;

  TevDBMControlProxy = class(TevControlProxy, IevDBMControl)
  protected
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  GetStatus: IisListOfValues;
    function  GetExecutingItems: IisParamsCollection;
  end;


{ TevRPControlProxy }

function TevRPControlProxy.GetAppTempFolder: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETAPPTEMPFOLDER);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevRPControlProxy.GetExecStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETEXECSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRPControlProxy.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);

  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRPControlProxy.GetHardware: IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETHARDWARE);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevRPControlProxy.GetLoadStatus: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETLOADSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRPControlProxy.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRPControlProxy.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

procedure TevRPControlProxy.GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETLOGFILETHRESHOLDINFO);
  D := GetDMControl.SendRequest(D);
  APurgeRecThreshold := D.Params.Value['APurgeRecThreshold'];
  APurgeRecCount := D.Params.Value['APurgeRecCount'];
end;

function TevRPControlProxy.GetMaintenanceDB: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETMAINTENANCEDB);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRPControlProxy.GetModules: IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETMODULES);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;


function TevRPControlProxy.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_GETSTACKINFO);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRPControlProxy.SetAppTempFolder(const ATempFolder: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_SETAPPTEMPFOLDER);
  D.Params.AddValue('ATempFolder', ATempFolder);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRPControlProxy.SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_SETLOGFILETHRESHOLDINFO);
  D.Params.AddValue('APurgeRecThreshold', APurgeRecThreshold);
  D.Params.AddValue('APurgeRecCount', APurgeRecCount);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRPControlProxy.SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RPCONTROLLER_SETMAINTENANCEDB);
  D.Params.AddValue('ADBListByDomains', ADBListByDomains);
  D.Params.AddValue('AMaintenance', AMaintenance);
  D := GetDMControl.SendRequest(D);
end;

{ TevRBControlProxy }

function TevRBControlProxy.GetHardware: IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETHARDWARE);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevRBControlProxy.GetModules: IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETMODULES);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevRBControlProxy.GetFolders(const ADomainName: String): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETFOLDERS);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevRBControlProxy.SetFolders(const ADOmainName: String; const AFolders: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETFOLDERS);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('AFolders', AFolders);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetLicenseInfo(const ADomainName: String): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETLICENSEINFO);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevRBControlProxy.SetLicenseInfo(const ADomainName,
  ASerialNumber, ARegistrationCode: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETLICENSEINFO);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ASerialNumber', ASerialNumber);
  D.Params.AddValue('ARegistrationCode', ARegistrationCode);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRBControlProxy.GetEMailInfo(var AHost, APort, AUser,
  APassword, ASentFrom, ASendAlertsTo: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETEMAILINFO);
  D := GetDMControl.SendRequest(D);
  AHost := D.Params.Value['AHost'];
  APort := D.Params.Value['APort'];
  AUser := D.Params.Value['AUser'];
  APassword := D.Params.Value['APassword'];
  ASentFrom := D.Params.Value['ASentFrom'];
  ASendAlertsTo := D.Params.Value['ASendAlertsTo'];
end;

procedure TevRBControlProxy.SetEMailInfo(const AHost, APort, AUser,
  APassword, ASentFrom, ASendAlertsTo: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETEMAILINFO);
  D.Params.AddValue('AHost', AHost);
  D.Params.AddValue('APort', APort);
  D.Params.AddValue('AUser', AUser);
  D.Params.AddValue('APassword', APassword);
  D.Params.AddValue('ASentFrom', ASentFrom);
  D.Params.AddValue('ASendAlertsTo', ASendAlertsTo);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetDatabaseParams(const ADomainName: String): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETDATABASEPARAMS);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevRBControlProxy.SetDatabaseParams(
  const ADomainName: String; const AParams: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETDATABASEPARAMS);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('AParams', AParams);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetRPPoolStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETRPPoolSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRBControlProxy.GetDomainList: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETDOMAINLIST);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRBControlProxy.UsersPoolStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETUSERSPOOLSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRBControlProxy.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRBControlProxy.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

function TevRBControlProxy.GetMachineInfo: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETMACHINEINFO);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRBControlProxy.GetRPPoolInfo: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETRPPOOLINFO);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

procedure TevRBControlProxy.SetRPPoolInfo(const AInfo: IisParamsCollection);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETRPPOOLINFO);
  D.Params.AddValue('AInfo', AInfo);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETSTACKINFO);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRBControlProxy.GetSystemAccount(const ADomainName: String; out AUser: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETSYSTEMACCOUNT);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  AUser := D.Params.Value['AUser'];
end;

procedure TevRBControlProxy.SetSystemAccount(const ADomainName, AUser: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETSYSTEMACCOUNT);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('AUser', AUser);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);

  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRBControlProxy.GetAppTempFolder: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETAPPTEMPFOLDER);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRBControlProxy.SetAppTempFolder(const ATempFolder: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETAPPTEMPFOLDER);
  D.Params.AddValue('ATempFolder', ATempFolder);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRBControlProxy.SendMessage(const aText, aToUsers: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SENDMESSAGE);
  D.Params.AddValue('AText', aText);
  D.Params.AddValue('AToUsers', aToUsers);
  GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetPrintersInfo: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETPRINTERSINFO);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevRBControlProxy.SetPrintersInfo(const APrintersInfo: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETPRINTERSINFO);
  D.Params.AddValue('APrintersInfo', APrintersInfo);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetMaintenanceDB: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETMAINTENANCEDB);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevRBControlProxy.SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETMAINTENANCEDB);
  D.Params.AddValue('ADBListByDomains', ADBListByDomains);
  D.Params.AddValue('AMaintenance', AMaintenance);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetSystemStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETSYSTEMSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRBControlProxy.GetAUS: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETAUS);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRBControlProxy.SetAUS(const AValue: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETAUS);
  D.Params.AddValue('AValue', AValue);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetInternalRR: IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETINTERNALRR);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

procedure TevRBControlProxy.SetInternalRR(const ARRList: IisStringList);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETINTERNALRR);
  D.Params.AddValue('ARRList', ARRList);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetInternalUserRR(const ADomainName: String): Boolean;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETINTERNALUSERRR);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRBControlProxy.SetInternalUserRR(const ADomainName: String;
  const AValue: Boolean);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETINTERNALUSERRR);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('AValue', AValue);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetBBDiagnosticCheck: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETBBDIAGNOSTICCHECK);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRBControlProxy.GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETLOGFILETHRESHOLDINFO);
  D := GetDMControl.SendRequest(D);
  APurgeRecThreshold := D.Params.Value['APurgeRecThreshold'];
  APurgeRecCount := D.Params.Value['APurgeRecCount'];
end;

procedure TevRBControlProxy.SetLogFileThresholdInfo(const APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETLOGFILETHRESHOLDINFO);
  D.Params.AddValue('APurgeRecThreshold', APurgeRecThreshold);
  D.Params.AddValue('APurgeRecCount', APurgeRecCount);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRBControlProxy.SetKnownADRServices(const AServices: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETKNOWNADRSERVICES);
  D.Params.AddValue('AServices', AServices);
  D := GetDMControl.SendRequest(D);
end;

function TevRBControlProxy.GetDatabasePasswords: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETDATABASEPASSWORDS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

procedure TevRBControlProxy.SetDatabasePasswords(const AParams: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETDATABASEPASSWORDS);
  D.Params.AddValue('AParams', AParams);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRBControlProxy.ResetAdminPassword(const ADomainName: String; const APassword: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_RESETADMINPASSWORD);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('APassword', APassword);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRBControlProxy.GetAdminAccount(const ADomainName: String; out AUser: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_GETADMINACCOUNT);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  AUser := D.Params.Value['AUser'];
end;

procedure TevRBControlProxy.SetAdminAccount(const ADomainName, AUser: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RBCONTROLLER_SETADMINACCOUNT);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('AUser', AUser);
  D := GetDMControl.SendRequest(D);
end;

{ TmcEvolutionControl }

procedure TmcEvolutionControl.InstallRP;
begin
  GetDMControl.InstallService(AppEvolution, SrvcEvRequestProcessor);
end;

procedure TmcEvolutionControl.RestartRP;
begin
  GetDMControl.RestartService(AppEvolution, SrvcEvRequestProcessor);
end;

procedure TmcEvolutionControl.StartRP;
begin
  GetDMControl.StartService(AppEvolution, SrvcEvRequestProcessor);
end;

procedure TmcEvolutionControl.StopRP;
begin
  GetDMControl.StopService(AppEvolution, SrvcEvRequestProcessor);
end;

procedure TmcEvolutionControl.UnInstallRP;
begin
  GetDMControl.UninstallService(AppEvolution, SrvcEvRequestProcessor);
end;

procedure TmcEvolutionControl.InstallRB;
begin
  GetDMControl.InstallService(AppEvolution, SrvcEvRequestBroker);
end;

procedure TmcEvolutionControl.RestartRB;
begin
  GetDMControl.RestartService(AppEvolution, SrvcEvRequestBroker);
end;

procedure TmcEvolutionControl.StartRB;
begin
  GetDMControl.StartService(AppEvolution, SrvcEvRequestBroker);
end;

procedure TmcEvolutionControl.StopRB;
begin
  GetDMControl.StopService(AppEvolution, SrvcEvRequestBroker);
end;

procedure TmcEvolutionControl.UnInstallRB;
begin
  GetDMControl.UninstallService(AppEvolution, SrvcEvRequestBroker);
end;

function TmcEvolutionControl.GetRBControl: IevRBControl;
begin
  Result := GetChild(0) as IevRBControl;
end;

function TmcEvolutionControl.GetRPInstalled: Boolean;
begin
  Result := GetDMControl.IsServiceInstalled(AppEvolution, SrvcEvRequestProcessor);
end;

function TmcEvolutionControl.GetRPStarted: Boolean;
begin
  Result := GetDMControl.IsServiceStarted(AppEvolution, SrvcEvRequestProcessor);
end;

function TmcEvolutionControl.GetRBInstalled: Boolean;
begin
  Result := GetDMControl.IsServiceInstalled(AppEvolution, SrvcEvRequestBroker);
end;

function TmcEvolutionControl.GetRBStarted: Boolean;
begin
  Result := GetDMControl.IsServiceStarted(AppEvolution, SrvcEvRequestBroker);
end;

procedure TmcEvolutionControl.Install(const AArchiveFile: String);
begin
  GetDMControl.InstallApp(AppEvolution, AArchiveFile);
end;

function TmcEvolutionControl.GetRRControl: IevRRControl;
begin
  Result := GetChild(2) as IevRRControl;
end;

function TmcEvolutionControl.GetRRInstalled: Boolean;
begin
  Result := GetDMControl.IsServiceInstalled(AppEvolution, SrvcEvRemoteRelay);
end;

function TmcEvolutionControl.GetRRStarted: Boolean;
begin
  Result := GetDMControl.IsServiceStarted(AppEvolution, SrvcEvRemoteRelay);
end;

procedure TmcEvolutionControl.InstallRR;
begin
  GetDMControl.InstallService(AppEvolution, SrvcEvRemoteRelay);
end;

procedure TmcEvolutionControl.RestartRR;
begin
  GetDMControl.RestartService(AppEvolution, SrvcEvRemoteRelay);
end;

procedure TmcEvolutionControl.StartRR;
begin
  GetDMControl.StartService(AppEvolution, SrvcEvRemoteRelay);
end;

procedure TmcEvolutionControl.StopRR;
begin
  GetDMControl.StopService(AppEvolution, SrvcEvRemoteRelay);
end;

procedure TmcEvolutionControl.UninstallRR;
begin
  GetDMControl.UninstallService(AppEvolution, SrvcEvRemoteRelay);
end;

procedure TmcEvolutionControl.Uninstall;
begin
  GetDMControl.UninstallApp(AppEvolution);
end;

function TmcEvolutionControl.GetArchiveFileName: String;
begin
  Result := GetDMControl.GetAppArchiveFileName(AppEvolution);
end;

function TmcEvolutionControl.GetRPControl: IevRPControl;
begin
  Result := GetChild(1) as IevRPControl;
end;

procedure TmcEvolutionControl.DoOnConstruction;
var
  O: IInterface;
begin
  inherited;
  O := TevRBControlProxy.Create(Self);
  O := TevRPControlProxy.Create(Self);
  O := TevRRControlProxy.Create(Self);
  O := TevAAControlProxy.Create(Self);
  O := TevACControlProxy.Create(Self);
  O := TevASControlProxy.Create(Self);
  O := TevDBMControlProxy.Create(Self);      
end;

function TmcEvolutionControl.GetDMControl: IevDMControl;
begin
  Result := GetOwner as IevDMControl;
end;

function TmcEvolutionControl.GetAAControl: IevAAControl;
begin
  Result := GetChild(3) as IevAAControl;
end;

function TmcEvolutionControl.GetAAInstalled: Boolean;
begin
  Result := GetDMControl.IsServiceInstalled(AppEvolution, SrvcEvApiAdapter);
end;

function TmcEvolutionControl.GetAAStarted: Boolean;
begin
  Result := GetDMControl.IsServiceStarted(AppEvolution, SrvcEvApiAdapter);
end;

procedure TmcEvolutionControl.InstallAA;
begin
  GetDMControl.InstallService(AppEvolution, SrvcEvApiAdapter);
end;

procedure TmcEvolutionControl.RestartAA;
begin
  GetDMControl.RestartService(AppEvolution, SrvcEvApiAdapter);
end;

procedure TmcEvolutionControl.StartAA;
begin
  GetDMControl.StartService(AppEvolution, SrvcEvApiAdapter);
end;

procedure TmcEvolutionControl.StopAA;
begin
  GetDMControl.StopService(AppEvolution, SrvcEvApiAdapter);
end;

procedure TmcEvolutionControl.UninstallAA;
begin
  GetDMControl.UninstallService(AppEvolution, SrvcEvApiAdapter);
end;

function TmcEvolutionControl.GetACControl: IevACControl;
begin
  Result := GetChild(4) as IevACControl;
end;

function TmcEvolutionControl.GetACInstalled: Boolean;
begin
  Result := GetDMControl.IsServiceInstalled(AppEvolution, SrvcEvADRClient);
end;

function TmcEvolutionControl.GetACStarted: Boolean;
begin
  Result := GetDMControl.IsServiceStarted(AppEvolution, SrvcEvADRClient);
end;

procedure TmcEvolutionControl.InstallAC;
begin
  GetDMControl.InstallService(AppEvolution, SrvcEvADRClient);
end;

procedure TmcEvolutionControl.RestartAC;
begin
  GetDMControl.RestartService(AppEvolution, SrvcEvADRClient);
end;

procedure TmcEvolutionControl.StartAC;
begin
  GetDMControl.StartService(AppEvolution, SrvcEvADRClient);
end;

procedure TmcEvolutionControl.StopAC;
begin
  GetDMControl.StopService(AppEvolution, SrvcEvADRClient);
end;

procedure TmcEvolutionControl.UninstallAC;
begin
  GetDMControl.UninstallService(AppEvolution, SrvcEvADRClient);
end;

function TmcEvolutionControl.GetASControl: IevASControl;
begin
  Result := GetChild(5) as IevASControl;
end;

function TmcEvolutionControl.GetASInstalled: Boolean;
begin
  Result := GetDMControl.IsServiceInstalled(AppEvolution, SrvcEvADRServer);
end;

function TmcEvolutionControl.GetASStarted: Boolean;
begin
  Result := GetDMControl.IsServiceStarted(AppEvolution, SrvcEvADRServer);
end;

procedure TmcEvolutionControl.InstallAS;
begin
  GetDMControl.InstallService(AppEvolution, SrvcEvADRServer);
end;

procedure TmcEvolutionControl.RestartAS;
begin
  GetDMControl.RestartService(AppEvolution, SrvcEvADRServer);
end;

procedure TmcEvolutionControl.StartAS;
begin
  GetDMControl.StartService(AppEvolution, SrvcEvADRServer);
end;

procedure TmcEvolutionControl.StopAS;
begin
  GetDMControl.StopService(AppEvolution, SrvcEvADRServer);
end;

procedure TmcEvolutionControl.UninstallAS;
begin
  GetDMControl.UninstallService(AppEvolution, SrvcEvADRServer);
end;

function TmcEvolutionControl.GetDBMControl: IevDBMControl;
begin
  Result := GetChild(6) as IevDBMControl;
end;

{ TevRRControlProxy }

function TevRRControlProxy.GetConnectionStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETCONNECTIONSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRRControlProxy.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);

  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRRControlProxy.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRRControlProxy.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

procedure TevRRControlProxy.GetLogFileThresholdInfo(out APurgeRecThreshold,
  APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETLOGFILETHRESHOLDINFO);
  D := GetDMControl.SendRequest(D);
  APurgeRecThreshold := D.Params.Value['APurgeRecThreshold'];
  APurgeRecCount := D.Params.Value['APurgeRecCount'];
end;

function TevRRControlProxy.GetSessionsStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETSESSIONSSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevRRControlProxy.GetSettings: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETSETTINGS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevRRControlProxy.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_GETSTACKINFO);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevRRControlProxy.SetLogFileThresholdInfo(
  const APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_SETLOGFILETHRESHOLDINFO);
  D.Params.AddValue('APurgeRecThreshold', APurgeRecThreshold);
  D.Params.AddValue('APurgeRecCount', APurgeRecCount);
  D := GetDMControl.SendRequest(D);
end;

procedure TevRRControlProxy.SetSettings(const ASettings: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_RRCONTROLLER_SETSETTINGS);
  D.Params.AddValue('ASettings', ASettings);
  D := GetDMControl.SendRequest(D);
end;

{ TevAAControlProxy }

function TevAAControlProxy.GetFilteredLogEvents(
  const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevAAControlProxy.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevAAControlProxy.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

procedure TevAAControlProxy.GetLogFileThresholdInfo(out APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_GETLOGFILETHRESHOLDINFO);
  D := GetDMControl.SendRequest(D);
  APurgeRecThreshold := D.Params.Value['APurgeRecThreshold'];
  APurgeRecCount := D.Params.Value['APurgeRecCount'];
end;

function TevAAControlProxy.GetSettings: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_GETSETTINGS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevAAControlProxy.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_GETSTACKINFO);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TevAAControlProxy.SetLogFileThresholdInfo(
  const APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_SETLOGFILETHRESHOLDINFO);
  D.Params.AddValue('APurgeRecThreshold', APurgeRecThreshold);
  D.Params.AddValue('APurgeRecCount', APurgeRecCount);
  D := GetDMControl.SendRequest(D);
end;

procedure TevAAControlProxy.SetSettings(const ASettings: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_AACONTROLLER_SETSETTINGS);
  D.Params.AddValue('ASettings', ASettings);
  D := GetDMControl.SendRequest(D);
end;

{ TevACControlProxy }

procedure TevACControlProxy.CheckDB(const ADomainName: String; const ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_CHECKDB);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevACControlProxy.CheckDBData(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_CHECKDBDATA);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevACControlProxy.CheckDBTransactions(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_CHECKDBTRANSACTIONS);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevACControlProxy.FullDBCopy(const ADomainName: String; const ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_FULLDBCOPY);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

function TevACControlProxy.GetDBList(const ADomainName: String): IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETDBLIST);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevACControlProxy.GetDomainStatus(const ADomainName: String): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETDOMAINSTATUS);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevACControlProxy.GetFilteredLogEvents(
  const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevACControlProxy.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevACControlProxy.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

procedure TevACControlProxy.GetLogFileThresholdInfo(out APurgeRecThreshold,
  APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETLOGFILETHRESHOLDINFO);
  D := GetDMControl.SendRequest(D);
  APurgeRecThreshold := D.Params.Value['APurgeRecThreshold'];
  APurgeRecCount := D.Params.Value['APurgeRecCount'];
end;

function TevACControlProxy.GetRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETREQUESTLIST);
  D.Params.AddValue('APageNbr', APageNbr);
  D.Params.AddValue('AItemsPerPage', AItemsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  APageCount := D.Params.Value['APageCount'];
end;

function TevACControlProxy.GetSettings: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETSETTINGS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevACControlProxy.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETSTACKINFO);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevACControlProxy.GetStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevACControlProxy.GetTransRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_GETTRANSREQUESTLIST);
  D.Params.AddValue('APageNbr', APageNbr);
  D.Params.AddValue('AItemsPerPage', AItemsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  APageCount := D.Params.Value['APageCount'];
end;

procedure TevACControlProxy.SetLogFileThresholdInfo(
  const APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_SETLOGFILETHRESHOLDINFO);
  D.Params.AddValue('APurgeRecThreshold', APurgeRecThreshold);
  D.Params.AddValue('APurgeRecCount', APurgeRecCount);
  D := GetDMControl.SendRequest(D);
end;

procedure TevACControlProxy.SetSettings(const ASettings: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_SETSETTINGS);
  D.Params.AddValue('ASettings', ASettings);
  D := GetDMControl.SendRequest(D);
end;

procedure TevACControlProxy.SyncDBList(const ADomainName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ACCONTROLLER_SYNCDBLIST);
  D.Params.AddValue('ADomainName', ADomainName);  
  D := GetDMControl.SendRequest(D);
end;

{ TevASControlProxy }

procedure TevASControlProxy.CheckDB(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_CHECKDB);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevASControlProxy.CheckDBData(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_CHECKDBDATA);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevASControlProxy.CheckDBTransactions(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_CHECKDBTRANSACTIONS);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevASControlProxy.FullDBCopy(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_FULLDBCOPY);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

function TevASControlProxy.GetClientDBList(const ADomainName: String): IisStringList;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETCLIENTDBLIST);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisStringList;
end;

function TevASControlProxy.GetDomainStatus(const ADomainName: String): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETDOMAINSTATUS);
  D.Params.AddValue('ADomainName', ADomainName);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevASControlProxy.GetFilteredLogEvents(
  const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevASControlProxy.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevASControlProxy.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

procedure TevASControlProxy.GetLogFileThresholdInfo(out APurgeRecThreshold,
  APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETLOGFILETHRESHOLDINFO);
  D := GetDMControl.SendRequest(D);
  APurgeRecThreshold := D.Params.Value['APurgeRecThreshold'];
  APurgeRecCount := D.Params.Value['APurgeRecCount'];
end;

function TevASControlProxy.GetRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETREQUESTLIST);
  D.Params.AddValue('APageNbr', APageNbr);
  D.Params.AddValue('AItemsPerPage', AItemsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  APageCount := D.Params.Value['APageCount'];
end;

function TevASControlProxy.GetSettings: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETSETTINGS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevASControlProxy.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETSTACKINFO);
  D := GetDMControl.SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TevASControlProxy.GetStatus: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevASControlProxy.GetTransRequestList(const APageNbr,
  AItemsPerPage: Integer; out APageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_GETTRANSREQUESTLIST);
  D.Params.AddValue('APageNbr', APageNbr);
  D.Params.AddValue('AItemsPerPage', AItemsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  APageCount := D.Params.Value['APageCount'];
end;

procedure TevASControlProxy.RebuildTT(const ADomainName, ADataBase: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_REBUILDTT);
  D.Params.AddValue('ADomainName', ADomainName);
  D.Params.AddValue('ADataBase', ADataBase);
  D := GetDMControl.SendRequest(D);
end;

procedure TevASControlProxy.SetLogFileThresholdInfo(
  const APurgeRecThreshold, APurgeRecCount: Integer);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_SETLOGFILETHRESHOLDINFO);
  D.Params.AddValue('APurgeRecThreshold', APurgeRecThreshold);
  D.Params.AddValue('APurgeRecCount', APurgeRecCount);
  D := GetDMControl.SendRequest(D);
end;

procedure TevASControlProxy.SetSettings(const ASettings: IisListOfValues);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_ASCONTROLLER_SETSETTINGS);
  D.Params.AddValue('ASettings', ASettings);
  D := GetDMControl.SendRequest(D);
end;


{ TevControlProxy }

function TevControlProxy.GetDMControl: ImcDMControl;
begin
  Result := GetOwner.GetOwner as ImcDMControl;
end;

{ TevDBMControlProxy }

function TevDBMControlProxy.GetExecutingItems: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DBMCONTROLLER_GETEXECUTINGITEMS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevDBMControlProxy.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DBMCONTROLLER_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TevDBMControlProxy.GetLogEventDetails(
  const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DBMCONTROLLER_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TevDBMControlProxy.GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DBMCONTROLLER_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

function TevDBMControlProxy.GetStatus: IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DBMCONTROLLER_GETSTATUS);
  D := GetDMControl.SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

end.
