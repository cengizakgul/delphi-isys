unit mcTypes;

interface

uses isBaseClasses, EvControllerInterfaces, evTransportInterfaces, isThreadManager, isLogFile, isSettings,
     EvDMInterfaces, isSchedule;

type
  ImcEvolutionControl = interface
  ['{DBE8FDF6-5E10-46C1-A1B5-508842402E6A}']
    procedure Install(const AArchiveFile: String);
    procedure Uninstall;
    function  GetArchiveFileName: String;

    function  GetRPInstalled: Boolean;
    function  GetRPStarted: Boolean;
    function  GetRPControl: IevRPControl;
    procedure InstallRP;
    procedure UnInstallRP;
    procedure StartRP;
    procedure StopRP;
    procedure RestartRP;

    function  GetRBInstalled: Boolean;
    function  GetRBStarted: Boolean;
    function  GetRBControl: IevRBControl;
    procedure InstallRB;
    procedure UnInstallRB;
    procedure StartRB;
    procedure StopRB;
    procedure RestartRB;

    function  GetRRControl: IevRRControl;
    function  GetRRInstalled: Boolean;
    function  GetRRStarted: Boolean;
    procedure InstallRR;
    procedure UninstallRR;
    procedure StartRR;
    procedure StopRR;
    procedure RestartRR;

    function  GetAAControl: IevAAControl;
    function  GetAAInstalled: Boolean;
    function  GetAAStarted: Boolean;
    procedure InstallAA;
    procedure UninstallAA;
    procedure StartAA;
    procedure StopAA;
    procedure RestartAA;

    function  GetACControl: IevACControl;
    function  GetACInstalled: Boolean;
    function  GetACStarted: Boolean;
    procedure InstallAC;
    procedure UninstallAC;
    procedure StartAC;
    procedure StopAC;
    procedure RestartAC;

    function  GetASControl: IevASControl;
    function  GetASInstalled: Boolean;
    function  GetASStarted: Boolean;
    procedure InstallAS;
    procedure UninstallAS;
    procedure StartAS;
    procedure StopAS;
    procedure RestartAS;

    function  GetDBMControl: IevDBMControl;

    property  RPControl: IevRPControl read GetRPControl;
    property  RBControl: IevRBControl read GetRBControl;
    property  RRControl: IevRRControl read GetRRControl;
    property  AAControl: IevAAControl read GetAAControl;
    property  ACControl: IevACControl read GetACControl;
    property  ASControl: IevASControl read GetASControl;
    property  DBMControl:IevDBMControl read GetDBMControl;
  end;


  ImcDMControl = interface(IevDMControl)
  ['{F398B12C-12E2-4519-BE8B-EA33EA645F89}']
    function  Host: String;
    function  Description: String;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  EvolutionControl: ImcEvolutionControl;
  end;


  ImcDeploymentManagers = interface
  ['{9239E9A9-89DA-4547-BAE7-814D4870ABD4}']
    procedure Lock;
    procedure Unlock;
    function  Count: Integer;
    function  GetItem(Index: Integer): ImcDMControl;
    function  AddDeploymentManager(const AHost, ADescription: String): ImcDMControl;
    procedure DelDeploymentManager(const AHost: String);
    function  FindDeploymentManager(const AHost: String): ImcDMControl;
    function  DiscoverDeploymentManagers: IisParamsCollection;
    function  ParallelExec(const AMethodNames: array of String; const AParams: array of IisListOfValues): IisStringList;
    function  ParallelExecFor(const ADMList: IisList; const AMethodNames: array of String;
                              const AParams: array of IisListOfValues): IisStringList;
    property  Items[Index: Integer]: ImcDMControl read GetItem; default;
  end;


  ImcHttpPresenter = interface
  ['{4085EB4B-A830-418E-B983-D620099BC477}']
    procedure SetActive(const AValue: Boolean);
    function  GetActive: Boolean;
    procedure SetUseSSL(const AValue: Boolean);
    procedure SetMaintenanceDB(const ADBListByDomains: IisListOfValues; const AMaintenance: Boolean);
    property  Active: Boolean read GetActive write SetActive;
  end;


  TmcUserAccountType = (uatUnknown, uatRestrictedUser, uatPowerUser, uatAdministrator);


  ImcUserAccount = interface
  ['{FECCDC73-FF7F-4CC5-8382-33AFD518300E}']
    function  GetAccountType: TmcUserAccountType;
    function  GetPassword: String;
    function  GetUserName: String;
    function  GetApplication: String;
    procedure SetAccountType(const AValue: TmcUserAccountType);
    procedure SetPassword(const AValue: String);
    procedure SetUserName(const AValue: String);
    procedure SetApplication(const AValue: String);
    property  Application: String read GetApplication write SetApplication;
    property  UserName: String read GetUserName write SetUserName;
    property  Password: String read GetPassword write SetPassword;
    property  AccountType: TmcUserAccountType read GetAccountType write SetAccountType;
  end;


  ImcUserAccounts = interface
  ['{2941A3A1-0413-4F0F-A3C7-4490A688969F}']
    procedure Lock;
    procedure Unlock;
    function  Count: Integer;
    function  AddUser(const AUserName: String): ImcUserAccount;
    procedure DeleteUser(const AUserName: String);
    function  GetItem(AIndex: Integer): ImcUserAccount;
    function  FindUser(const AUserName: String): ImcUserAccount;
    property  Items[Index: Integer]: ImcUserAccount read GetItem; default;
  end;


  ImcSchedule = interface(IisSchedule)
  ['{A0EDD165-4276-4FB1-A0C5-8467115B163C}']
    procedure LoadFromStorage;
    procedure SaveToStorage;
  end;

  ImcMainboard = interface
  ['{BCA6A9F8-6ABD-4BCE-B420-897C0128126D}']
    procedure Initialize;
    function  AppSettings: IisSettings;
    function  UserAccounts: ImcUserAccounts;
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  DeploymentManagers: ImcDeploymentManagers;
    function  HttpPresenter: ImcHttpPresenter;
    function  Schedule: ImcSchedule;
    procedure UpdateEMailConfig(const ASMTPHost, ASMTPPort, AUser, APassword, ASendFrom, ASendTo: String);
    procedure SendEMailNotification(const ASubject, AMessage: String);
    function  GetLocalVersions(const AApplication: String): IisParamsCollection;    
    function  GetRemoteVersions(const AApplication: String): IisParamsCollection;
    procedure DownloadAppVersion(const AApplication: String; const AVersion: String);
  end;


implementation

end.