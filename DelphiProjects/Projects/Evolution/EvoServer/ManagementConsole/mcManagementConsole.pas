unit mcManagementConsole;

interface

uses SysUtils, Classes, isBaseClasses, EvTransportShared, EvTransportDatagrams, isSocket,
     evStreamUtils, isBasicUtils, isThreadManager, evRemoteMethods, EvConsts, mcTypes,
     evTransportInterfaces, EvControllerInterfaces, isSettings, isLogFile,
     mcHttpPresenter, mcEvolutionControl, isAppIDs, SEncryptionRoutines, EvDMInterfaces,
     isSchedule, isErrorUtils, isExceptions, IsSMTPClient, isHttp, mcDSClient;

const
  sServiceUnavailable = 'Service unavailable';

type
  TmcMainboard = class(TisInterfacedObject, ImcMainboard)
  private
    FDeploymentManagers: ImcDeploymentManagers;
    FHttpPresenter: ImcHttpPresenter;
    FAppSettings: IisSettings;
    FUserAccounts: ImcUserAccounts;
    FSchedule: ImcSchedule;
    function  GetVersions(const APath: String): IisParamsCollection;
    function  DoGetRemoteVersions(const AURL: String): IisParamsCollection;
    procedure FilterIncompatibleVersions(const AVersionList: IisParamsCollection);
  protected
    procedure DoOnConstruction; override;
    procedure Initialize;
    function  AppSettings: IisSettings;
    function  UserAccounts: ImcUserAccounts;
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
    function  DeploymentManagers: ImcDeploymentManagers;
    function  HttpPresenter: ImcHttpPresenter;
    function  Schedule: ImcSchedule;
    procedure UpdateEMailConfig(const ASMTPHost, ASMTPPort, AUser, APassword, ASendFrom, ASendTo: String);
    procedure SendEMailNotification(const ASubject, AMessage: String);
    function  GetLocalVersions(const AApplication: String): IisParamsCollection;
    function  GetRemoteVersions(const AApplication: String): IisParamsCollection;
    procedure DownloadAppVersion(const AApplication: String; const AVersion: String);
  public
    destructor Destroy; override;
  end;

var
  Mainboard: ImcMainboard;

implementation

uses DateUtils;

const ResponseTimeout = 300000;  // 5 mins

type
  TmcUserAccount = class(TisNamedObject, ImcUserAccount)
  private
    FApplication: String;
    FPassword: String;
    FAccountType: TmcUserAccountType;
    procedure Load;
    procedure Save;
  protected
    function  GetAccountType: TmcUserAccountType;
    function  GetPassword: String;
    function  GetUserName: String;
    function  GetApplication: String;
    procedure SetAccountType(const AValue: TmcUserAccountType);
    procedure SetPassword(const AValue: String);
    procedure SetUserName(const AValue: String);
    procedure SetApplication(const AValue: String);
  public
    constructor Create(const AUserName: String); reintroduce;   
  end;

  TmcUserAccounts = class(TisCollection, ImcUserAccounts)
  private
    function  IntAddUser(const AUserName: String): ImcUserAccount;
  protected
    procedure DoOnConstruction; override;
    function  Count: Integer;
    function  AddUser(const AUserName: String): ImcUserAccount;
    procedure DeleteUser(const AUserName: String);
    function  GetItem(AIndex: Integer): ImcUserAccount;
    function  FindUser(const AUserName: String): ImcUserAccount;
  end;

  TevParallelExecProc = procedure(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues) of object;

  TmcDeploymentManagers = class(TisCollection, ImcDeploymentManagers)
  private
    FDSConnector: ImcDSConnector;
    FParExecProcList: TStringList;
    procedure RegisterParExecProc;
    procedure AddParExecProc(const AMethod: String; AProc: TevParallelExecProc);
    procedure DoInThread(const AParams: TTaskParamList);
    function  GetAllDMList: IisList;

    procedure peDMEvGetAppArchiveFileName(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peDMEvInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peDMGetLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peDMGetFilteredLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBInstalled(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBStart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBStop(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBRestart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBUninstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBGetRPPoolStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBGetAppTempFolder(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBGetMaintenanceDB(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBSetMaintenanceDB(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBGetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBSetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRBSetKnownADRServices(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);

    procedure peRPInstalled(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPStart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPStop(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPRestart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPUninstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetLoadStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetExecStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetFilteredLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetAppTempFolder(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetMaintenanceDB(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPSetMaintenanceDB(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPGetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRPSetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);

    procedure peAAGetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAASetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAInstalled(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAStart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAStop(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAARestart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAUninstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAGetLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAGetFilteredLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAAGetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peAASetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);

    procedure peACGetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACSetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACInstalled(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACStart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACStop(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACRestart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACUninstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetFilteredLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACSetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetRequestList(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetTransRequestList(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peACGetDomainStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);

    procedure peASGetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASSetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASInstalled(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASStart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASStop(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASRestart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASUninstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetFilteredLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASSetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetDomainStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetRequestList(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetTransRequestList(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASGetClientDBList(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peASRebuildTT(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);

    procedure peRRInstalled(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRStart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRStop(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRRestart(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRInstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRUninstall(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRGetLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRGetFilteredLogEvents(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRGetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRSetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRGetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRSetLogFileThresholdInfo(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
    procedure peRRGetSessionsStatus(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);

    procedure Refresh;
  protected
    procedure DoOnConstruction; override;
    function  Count: Integer;
    function  GetItem(Index: Integer): ImcDMControl;
    function  AddDeploymentManager(const AHost, ADescription: String): ImcDMControl;
    procedure DelDeploymentManager(const AHost: String);
    function  FindDeploymentManager(const AHost: String): ImcDMControl;
    function  ParallelExec(const AMethodNames: array of String; const AParamList: array of IisListOfValues): IisStringList;
    function  ParallelExecFor(const ADMList: IisList; const AMethodNames: array of String;
                              const AParamList: array of IisListOfValues): IisStringList;
    function  DiscoverDeploymentManagers: IisParamsCollection;
  public
    destructor Destroy; override;
  end;


  TmcSchedule = class(TisSchedule, ImcSchedule)
  private
    function  GetStorageFile: String;
  protected
    procedure RunTaskFor(const AEvent: IisScheduleEvent); override;
    procedure LoadFromStorage; override;
    procedure SaveToStorage; override;
    procedure RemoveFromStorage; override;
    procedure DoOnConstruction; override;
  end;


{ TmcDeploymentManagers }

procedure TmcDeploymentManagers.DoOnConstruction;
begin
  inherited;
  InitLock;
  FDSConnector := TecDSConnector.Create;

  FParExecProcList := TStringList.Create;
  RegisterParExecProc;
  FParExecProcList.Sorted := True;

  Refresh;
end;

procedure TmcDeploymentManagers.Refresh;
var
  DS: ImcDMControl;
  i: Integer;
  sl: IisStringListRO;
begin
  Lock;
  try
    RemoveAllChildren;
    sl := Mainboard.AppSettings.GetValueNames('DeploymentManager');
    for i := 0 to sl.Count - 1 do
    begin
      DS := TmcDMControl.Create(sl[i], Mainboard.AppSettings.AsString['DeploymentManager\' + sl[i]], FDSConnector);
      AddChild(DS as IisInterfacedObject);
    end;

    Sort;
  finally
    Unlock;
  end;
end;

function TmcDeploymentManagers.AddDeploymentManager(const AHost, ADescription: String): ImcDMControl;
begin
  Lock;
  try
    if FindDeploymentManager(AHost) <> nil then
      raise EisException.CreateFmt('Deployment Manager on [%s] is already registered', [AHost]);

    Mainboard.AppSettings.AsString['DeploymentManager\' + AHost] := ADescription;
    Result := TmcDMControl.Create(AHost, ADescription, FDSConnector);
    AddChild(Result as IisInterfacedObject);
    Sort;
  finally
    Unlock;
  end;
end;

procedure TmcDeploymentManagers.DelDeploymentManager(const AHost: String);
var
  DS: ImcDMControl;
begin
  Lock;
  try
    DS := FindDeploymentManager(AHost);
    if Assigned(DS) then
    begin
      RemoveChild(DS as IisInterfacedObject);
      Mainboard.AppSettings.DeleteValue('DeploymentManager\' + AHost);
    end;
  finally
    Unlock;
  end;
end;

function TmcDeploymentManagers.FindDeploymentManager(const AHost: String): ImcDMControl;
begin
  Result := FindChildByName(AHost) as ImcDMControl;
end;

function TmcDeploymentManagers.Count: Integer;
begin
  Result := ChildCount;
end;

function TmcDeploymentManagers.GetItem(Index: Integer): ImcDMControl;
begin
  Result := GetChild(Index) as ImcDMControl;
end;

function TmcDeploymentManagers.ParallelExec(const AMethodNames: array of String;
  const AParamList: array of IisListOfValues): IisStringList;
begin
  Result := ParallelExecFor(nil, AMethodNames, AParamList);
end;

procedure TmcDeploymentManagers.DoInThread(const AParams: TTaskParamList);
var
  DM: ImcDMControl;
  sMethod: String;
  Params: IisListOfValues;
  Res: IisListOfValues;
  Proc: TMethod;
  Idx: Integer;
begin
  DM := IInterface(AParams[0]) as ImcDMControl;
  sMethod := AParams[1];
  Params := IInterface(AParams[2]) as IisListOfValues;
  Res := IInterface(AParams[3]) as IisListOfValues;

  try
    Idx := FParExecProcList.IndexOf(sMethod);
    CheckCondition(Idx >= 0, 'Unknown method ' + sMethod);
    Proc.Data := Self;
    Proc.Code := Pointer(FParExecProcList.Objects[Idx]);

    TevParallelExecProc(Proc)(DM, Params, Res);
  except
    on E: Exception do
      Res.AddValue('Error', E.Message);
  end;
end;


function TmcDeploymentManagers.DiscoverDeploymentManagers: IisParamsCollection;
var
  Socket: IisUDPSocket;
  Tsk: TTaskID;

  procedure DoWork(const Params: TTaskParamList);
  var
    Socket: IisUDPSocket;
    D: IevDatagram;
    S: IisStream;
    Res: IisParamsCollection;
    LV: IisListOfValues;
    IPAddress: String;    
  begin
    Socket := IInterface(Params[0]) as IisUDPSocket;
    Res := IInterface(Params[1]) as IisParamsCollection;

    D := TevDatagram.Create(METHOD_SERVICEDISCOVERY_REPORTLOCATION);
    D.Params.AddValue('AppID', AppID);
    D.Params.AddValue('Host', GetComputerNameString);
    D.Params.AddValue('Port', MCDiscovery_Port);

    S := TisStream.Create;
    (D as IisInterfacedObject).WriteToStream(S);
    Socket.SendStream(S, BROADCAST_IP, DMDiscovery_Port);

    while not CurrentThreadTaskTerminated do
    begin
      try
        S := Socket.ReceiveStream;
        IPAddress := Socket.GetLastRemoteAddress.Host;
      except
        IPAddress := '';
        S := nil;
      end;

      try
        if Assigned(S) and (S.Size > 0) then
        begin
          try
            D := TevDatagram.CreateFromStream(S);
          except
            on E: Exception do
              GlobalLogFile.AddEvent(etInformation, 'Network Discovery', 'Response is not recognized',
                                     'IP Address: ' + IPAddress + #13 + BuildStackedErrorStr(E));
          end;

          if D.Params.TryGetValue('AppID', '') = EvoDeployMgrAppInfo.AppID then
            if Res.ParamsByName(D.Params.Value['Host']) = nil then
            begin
              LV := Res.AddParams(D.Params.Value['Host']);
              LV.AddValue('Version', (D.Params.TryGetValue('Version', '')));
            end;
        end;
      except
      end;
    end;
  end;

begin
  Lock;
  try
    Result := TisParamsCollection.Create;
    Socket := TisUDPSocket.Create('', MCDiscovery_Port);
    Tsk := GlobalThreadManager.RunTask(@DoWork, MakeTaskParams([Socket, Result]), 'Service Discovery');
    try
      Snooze(1000);
    finally
      GlobalThreadManager.TerminateTask(Tsk);
      Socket.Close;
      GlobalThreadManager.WaitForTaskEnd(Tsk);
    end;  
  finally
    Unlock;
  end;
end;


function TmcDeploymentManagers.ParallelExecFor(const ADMList: IisList; const AMethodNames: array of String;
  const AParamList: array of IisListOfValues): IisStringList;
var
  i, j, TskIndex: Integer;
  TaskList: array of TTaskID;
  Res: IisListOfValues;
  MethodResults: IisParamsCollection;
  RunParam: IisListOfValues;
  DMList: IisList;
begin
  Result := TisStringList.CreateUnique;

  for i := Low(AMethodNames) to High(AMethodNames) do
  begin
    MethodResults := TisParamsCollection.Create;
    Result.AddObject(AMethodNames[i], MethodResults);
  end;

  if not Assigned(ADMList) then
    DMList := GetAllDMList
  else
    DMList := ADMList;

  SetLength(TaskList, DMList.Count * Length(AMethodNames));
  TskIndex := 0;
  for i := 0 to DMList.Count - 1 do
  begin
    for j := 0 to Result.Count - 1 do
    begin
      if Length(AParamList) > 0 then
        RunParam := AParamList[j]
      else
        RunParam := nil;

      MethodResults := Result.Objects[j] as IisParamsCollection;
      Res := MethodResults.AddParams((DMList[i] as ImcDMControl).Host);
      TaskList[TskIndex] := GlobalThreadManager.RunTask(DoInThread, Self,
                              MakeTaskParams([DMList[i], Result[j], RunParam, Res]),
                             'Deployment Manager request');
      Inc(TskIndex);
    end;
  end;

  for i := Low(TaskList) to High(TaskList) do
    GlobalThreadManager.WaitForTaskEnd(TaskList[i]);
end;

procedure TmcDeploymentManagers.RegisterParExecProc;
begin
  AddParExecProc('DM.EvGetAppArchiveFileName', peDMEvGetAppArchiveFileName);
  AddParExecProc('DM.EvInstall', peDMEvInstall);
  AddParExecProc('DM.GetLogEvents', peDMGetLogEvents);
  AddParExecProc('DM.GetFilteredLogEvents', peDMGetFilteredLogEvents);

  AddParExecProc('RB.Installed', peRBInstalled);
  AddParExecProc('RB.Started', peRBStarted);
  AddParExecProc('RB.Start', peRBStart);
  AddParExecProc('RB.Stop', peRBStop);
  AddParExecProc('RB.Restart', peRBRestart);
  AddParExecProc('RB.Install', peRBInstall);
  AddParExecProc('RB.Uninstall', peRBUninstall);
  AddParExecProc('RB.GetRPPoolStatus', peRBGetRPPoolStatus);
  AddParExecProc('RB.GetAppTempFolder', peRBGetAppTempFolder);
  AddParExecProc('RB.GetMaintenanceDB', peRBGetMaintenanceDB);
  AddParExecProc('RB.SetMaintenanceDB', peRBSetMaintenanceDB);
  AddParExecProc('RB.GetLogFileThresholdInfo', peRBGetLogFileThresholdInfo);
  AddParExecProc('RB.SetLogFileThresholdInfo', peRBSetLogFileThresholdInfo);
  AddParExecProc('RB.SetKnownADRServices', peRBSetKnownADRServices);

  AddParExecProc('RP.Installed', peRPInstalled);
  AddParExecProc('RP.Started', peRPStarted);
  AddParExecProc('RP.Start', peRPStart);
  AddParExecProc('RP.Stop', peRPStop);
  AddParExecProc('RP.Restart', peRPRestart);
  AddParExecProc('RP.Install', peRPInstall);
  AddParExecProc('RP.Uninstall', peRPUninstall);
  AddParExecProc('RP.GetLoadStatus', peRPGetLoadStatus);
  AddParExecProc('RP.GetExecStatus', peRPGetExecStatus);
  AddParExecProc('RP.GetLogEvents', peRPGetLogEvents);
  AddParExecProc('RP.GetFilteredLogEvents', peRPGetFilteredLogEvents);
  AddParExecProc('RP.GetAppTempFolder', peRPGetAppTempFolder);
  AddParExecProc('RP.GetMaintenanceDB', peRPGetMaintenanceDB);
  AddParExecProc('RP.SetMaintenanceDB', peRPSetMaintenanceDB);
  AddParExecProc('RP.GetLogFileThresholdInfo', peRPGetLogFileThresholdInfo);
  AddParExecProc('RP.SetLogFileThresholdInfo', peRPSetLogFileThresholdInfo);

  AddParExecProc('AA.GetSettings', peAAGetSettings);
  AddParExecProc('AA.SetSettings', peAASetSettings);
  AddParExecProc('AA.Installed', peAAInstalled);
  AddParExecProc('AA.Started', peAAStarted);  
  AddParExecProc('AA.Start', peAAStart);
  AddParExecProc('AA.Stop', peAAStop);
  AddParExecProc('AA.Restart', peAARestart);
  AddParExecProc('AA.Install', peAAInstall);
  AddParExecProc('AA.Uninstall', peAAUninstall);
  AddParExecProc('AA.GetLogEvents', peAAGetLogEvents);
  AddParExecProc('AA.GetFilteredLogEvents', peAAGetFilteredLogEvents);
  AddParExecProc('AA.GetLogFileThresholdInfo', peAAGetLogFileThresholdInfo);
  AddParExecProc('AA.SetLogFileThresholdInfo', peAASetLogFileThresholdInfo);

  AddParExecProc('AC.GetSettings', peACGetSettings);
  AddParExecProc('AC.SetSettings', peACSetSettings);
  AddParExecProc('AC.Installed', peACInstalled);
  AddParExecProc('AC.Started', peACStarted);
  AddParExecProc('AC.Start', peACStart);
  AddParExecProc('AC.Stop', peACStop);
  AddParExecProc('AC.Restart', peACRestart);
  AddParExecProc('AC.Install', peACInstall);
  AddParExecProc('AC.Uninstall', peACUninstall);
  AddParExecProc('AC.GetLogEvents', peACGetLogEvents);
  AddParExecProc('AC.GetFilteredLogEvents', peACGetFilteredLogEvents);
  AddParExecProc('AC.GetLogFileThresholdInfo', peACGetLogFileThresholdInfo);
  AddParExecProc('AC.SetLogFileThresholdInfo', peACSetLogFileThresholdInfo);
  AddParExecProc('AC.GetRequestList', peACGetRequestList);
  AddParExecProc('AC.GetTransRequestList', peACGetTransRequestList);  
  AddParExecProc('AC.GetStatus', peACGetStatus);
  AddParExecProc('AC.GetDomainStatus', peACGetDomainStatus);

  AddParExecProc('AS.GetSettings', peASGetSettings);
  AddParExecProc('AS.SetSettings', peASSetSettings);
  AddParExecProc('AS.Installed', peASInstalled);
  AddParExecProc('AS.Started', peASStarted);
  AddParExecProc('AS.Start', peASStart);
  AddParExecProc('AS.Stop', peASStop);
  AddParExecProc('AS.Restart', peASRestart);
  AddParExecProc('AS.Install', peASInstall);
  AddParExecProc('AS.Uninstall', peASUninstall);
  AddParExecProc('AS.GetLogEvents', peASGetLogEvents);
  AddParExecProc('AS.GetFilteredLogEvents', peASGetFilteredLogEvents);
  AddParExecProc('AS.GetLogFileThresholdInfo', peASGetLogFileThresholdInfo);
  AddParExecProc('AS.SetLogFileThresholdInfo', peASSetLogFileThresholdInfo);
  AddParExecProc('AS.GetStatus', peASGetStatus);
  AddParExecProc('AS.GetDomainStatus', peASGetDomainStatus);
  AddParExecProc('AS.GetRequestList', peASGetRequestList);
  AddParExecProc('AS.GetTransRequestList', peASGetTransRequestList);
  AddParExecProc('AS.GetClientDBList', peASGetClientDBList);
  AddParExecProc('AS.RebuildTT', peASRebuildTT);

  AddParExecProc('RR.Installed', peRRInstalled);
  AddParExecProc('RR.Started', peRRStarted);
  AddParExecProc('RR.Start', peRRStart);
  AddParExecProc('RR.Stop', peRRStop);
  AddParExecProc('RR.Restart', peRRRestart);
  AddParExecProc('RR.Install', peRRInstall);
  AddParExecProc('RR.Uninstall', peRRUninstall);
  AddParExecProc('RR.GetLogEvents', peRRGetLogEvents);
  AddParExecProc('RR.GetFilteredLogEvents', peRRGetFilteredLogEvents);
  AddParExecProc('RR.GetSettings', peRRGetSettings);
  AddParExecProc('RR.SetSettings', peRRSetSettings);
  AddParExecProc('RR.GetLogFileThresholdInfo', peRRGetLogFileThresholdInfo);
  AddParExecProc('RR.SetLogFileThresholdInfo', peRRSetLogFileThresholdInfo);
  AddParExecProc('RR.GetSessionsStatus', peRRGetSessionsStatus);
end;

destructor TmcDeploymentManagers.Destroy;
begin
  FParExecProcList.Free;
  inherited;
end;

procedure TmcDeploymentManagers.AddParExecProc(const AMethod: String; AProc: TevParallelExecProc);
begin
  FParExecProcList.AddObject(AMethod, @AProc);
end;

procedure TmcDeploymentManagers.peAAGetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetAAStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.AAControl.GetSettings);
end;

procedure TmcDeploymentManagers.peAAInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.InstallAA;
end;

procedure TmcDeploymentManagers.peAAInstalled(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetAAInstalled);
end;

procedure TmcDeploymentManagers.peAARestart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.RestartAA;
end;

procedure TmcDeploymentManagers.peAAStart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StartAA;
end;

procedure TmcDeploymentManagers.peAAStop(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StopAA;
end;

procedure TmcDeploymentManagers.peAAUninstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.UninstallAA;
end;

procedure TmcDeploymentManagers.peDMEvGetAppArchiveFileName(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetArchiveFileName);
end;

procedure TmcDeploymentManagers.peDMEvInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.Install(AParams[0].Value);
end;

procedure TmcDeploymentManagers.peDMGetFilteredLogEvents(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  LogQuery: TEventQuery;
begin
  LogQuery.StartTime := AParams.Value['StartTime'];
  LogQuery.EndTime := AParams.Value['EndTime'];
  LogQuery.Types := ByteToLogEventTypes(AParams.Value['Types']);
  LogQuery.EventClass := AParams.Value['EventClass'];
  LogQuery.MaxCount := AParams.Value['MaxCount'];

  AResult.AddValue(METHOD_RESULT, ADM.GetFilteredLogEvents(LogQuery));
end;

procedure TmcDeploymentManagers.peDMGetLogEvents(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  AResult.AddValue(METHOD_RESULT, ADM.GetLogEvents(AParams.Value['PageNbr'],
                   AParams.Value['EventsPerPage'], iOutParam));
  AResult.AddValue('PageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peRBGetRPPoolStatus(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RBControl.GetRPPoolStatus);
end;

procedure TmcDeploymentManagers.peRBInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.InstallRB;
end;

procedure TmcDeploymentManagers.peRBInstalled(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetRBInstalled);
end;

procedure TmcDeploymentManagers.peRBRestart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.RestartRB;
end;

procedure TmcDeploymentManagers.peRBStart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StartRB;
end;

procedure TmcDeploymentManagers.peRBStarted(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetRBStarted);
end;

procedure TmcDeploymentManagers.peRBStop(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StopRB;
end;

procedure TmcDeploymentManagers.peRBUninstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.UnInstallRB;
end;

procedure TmcDeploymentManagers.peRPGetExecStatus(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RPControl.GetExecStatus);
end;

procedure TmcDeploymentManagers.peRPGetFilteredLogEvents(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  LogQuery: TEventQuery;
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  
  LogQuery.StartTime := AParams.Value['StartTime'];
  LogQuery.EndTime := AParams.Value['EndTime'];
  LogQuery.Types := ByteToLogEventTypes(AParams.Value['Types']);
  LogQuery.EventClass := AParams.Value['EventClass'];
  LogQuery.MaxCount := AParams.Value['MaxCount'];
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RPControl.GetFilteredLogEvents(LogQuery));
end;

procedure TmcDeploymentManagers.peRPGetLoadStatus(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RPControl.GetLoadStatus);
end;

procedure TmcDeploymentManagers.peRPGetLogEvents(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RPControl.
    GetLogEvents(AParams.Value['PageNbr'], AParams.Value['EventsPerPage'], iOutParam));
  AResult.AddValue('PageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peRPInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.InstallRP;
end;

procedure TmcDeploymentManagers.peRPInstalled(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetRPInstalled);
end;

procedure TmcDeploymentManagers.peRPRestart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.RestartRP;
end;

procedure TmcDeploymentManagers.peRPStart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StartRP;
end;

procedure TmcDeploymentManagers.peRPStarted(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetRPStarted);
end;

procedure TmcDeploymentManagers.peRPStop(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StopRP;
end;

procedure TmcDeploymentManagers.peRPUninstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.UnInstallRP;
end;

procedure TmcDeploymentManagers.peRRGetFilteredLogEvents(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  LogQuery: TEventQuery;
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);

  LogQuery.StartTime := AParams.Value['StartTime'];
  LogQuery.EndTime := AParams.Value['EndTime'];
  LogQuery.Types := ByteToLogEventTypes(AParams.Value['Types']);
  LogQuery.EventClass := AParams.Value['EventClass'];
  LogQuery.MaxCount := AParams.Value['MaxCount'];
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RRControl.GetFilteredLogEvents(LogQuery));
end;

procedure TmcDeploymentManagers.peRRGetLogEvents(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RRControl.
        GetLogEvents(AParams.Value['PageNbr'], AParams.Value['EventsPerPage'], iOutParam));
  AResult.AddValue('PageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peRRGetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RRControl.GetSettings);
end;

procedure TmcDeploymentManagers.peRRInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.InstallRR;
end;

procedure TmcDeploymentManagers.peRRInstalled(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetRRInstalled);
end;

procedure TmcDeploymentManagers.peRRRestart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.RestartRR;
end;

procedure TmcDeploymentManagers.peRRSetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);
  ADM.EvolutionControl.RRControl.SetSettings(IInterface(AParams.Value['Settings']) as IisListOfValues);
end;

procedure TmcDeploymentManagers.peRRStart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StartRR;
end;

procedure TmcDeploymentManagers.peRRStarted(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetRRStarted);
end;

procedure TmcDeploymentManagers.peRRStop(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StopRR;
end;

procedure TmcDeploymentManagers.peRRUninstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.UninstallRR;
end;

procedure TmcDeploymentManagers.peRPGetAppTempFolder(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RPControl.GetAppTempFolder);
end;

procedure TmcDeploymentManagers.peRBGetAppTempFolder(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RBControl.GetAppTempFolder);
end;

procedure TmcDeploymentManagers.peAASetSettings(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetAAStarted, sServiceUnavailable);
  ADM.EvolutionControl.AAControl.SetSettings(IInterface(AParams.Value['Settings']) as IisListOfValues);
end;

procedure TmcDeploymentManagers.peAAStarted(const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetAAStarted);
end;

procedure TmcDeploymentManagers.peRPGetMaintenanceDB(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RPControl.GetMaintenanceDB);
end;

procedure TmcDeploymentManagers.peRPSetMaintenanceDB(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  ADM.EvolutionControl.RPControl.SetMaintenanceDB(IInterface(AParams.Value['ADBListByDomains']) as IisListOfValues,
    AParams.Value['AMaintenance']);
end;

procedure TmcDeploymentManagers.peAAGetFilteredLogEvents(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  LogQuery: TEventQuery;
begin
  CheckCondition(ADM.EvolutionControl.GetAAStarted, sServiceUnavailable);

  LogQuery.StartTime := AParams.Value['StartTime'];
  LogQuery.EndTime := AParams.Value['EndTime'];
  LogQuery.Types := ByteToLogEventTypes(AParams.Value['Types']);
  LogQuery.EventClass := AParams.Value['EventClass'];
  LogQuery.MaxCount := AParams.Value['MaxCount'];
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.AAControl.GetFilteredLogEvents(LogQuery));
end;

procedure TmcDeploymentManagers.peAAGetLogEvents(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetAAStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.AAControl.
        GetLogEvents(AParams.Value['PageNbr'], AParams.Value['EventsPerPage'], iOutParam));
  AResult.AddValue('PageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peRBGetMaintenanceDB(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RBControl.GetMaintenanceDB);
end;

procedure TmcDeploymentManagers.peRBSetMaintenanceDB(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);
  ADM.EvolutionControl.RBControl.SetMaintenanceDB(IInterface(AParams.Value['ADBListByDomains']) as IisListOfValues,
    AParams.Value['AMaintenance']);
end;

procedure TmcDeploymentManagers.peRBGetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);

  ADM.EvolutionControl.RBControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TmcDeploymentManagers.peRBSetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);
  ADM.EvolutionControl.RBControl.SetLogFileThresholdInfo(AParams.Value['APurgeRecThreshold'], AParams.Value['APurgeRecCount']);
end;

procedure TmcDeploymentManagers.peRPGetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);

  ADM.EvolutionControl.RPControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TmcDeploymentManagers.peRPSetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRPStarted, sServiceUnavailable);
  ADM.EvolutionControl.RPControl.SetLogFileThresholdInfo(AParams.Value['APurgeRecThreshold'], AParams.Value['APurgeRecCount']);
end;

procedure TmcDeploymentManagers.peAAGetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetAAStarted, sServiceUnavailable);

  ADM.EvolutionControl.AAControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TmcDeploymentManagers.peAASetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetAAStarted, sServiceUnavailable);
  ADM.EvolutionControl.AAControl.SetLogFileThresholdInfo(AParams.Value['APurgeRecThreshold'], AParams.Value['APurgeRecCount']);
end;

procedure TmcDeploymentManagers.peRRGetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);

  ADM.EvolutionControl.RRControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TmcDeploymentManagers.peRRSetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);
  ADM.EvolutionControl.RRControl.SetLogFileThresholdInfo(AParams.Value['APurgeRecThreshold'], AParams.Value['APurgeRecCount']);
end;

function TmcDeploymentManagers.GetAllDMList: IisList;
var
  i: Integer;
begin
  Result := TisList.Create;
  Lock;
  try
    Result.Capacity := Count;
    for i := 0 to Count - 1 do
      Result.Add(GetItem(i));
  finally
    Unlock;
  end;
end;

procedure TmcDeploymentManagers.peRRGetSessionsStatus(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRRStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.RRControl.GetSessionsStatus);
end;

procedure TmcDeploymentManagers.peACGetFilteredLogEvents(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  LogQuery: TEventQuery;
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);

  LogQuery.StartTime := AParams.Value['StartTime'];
  LogQuery.EndTime := AParams.Value['EndTime'];
  LogQuery.Types := ByteToLogEventTypes(AParams.Value['Types']);
  LogQuery.EventClass := AParams.Value['EventClass'];
  LogQuery.MaxCount := AParams.Value['MaxCount'];
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.GetFilteredLogEvents(LogQuery));
end;

procedure TmcDeploymentManagers.peACGetLogEvents(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.
        GetLogEvents(AParams.Value['PageNbr'], AParams.Value['EventsPerPage'], iOutParam));
  AResult.AddValue('PageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peACGetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);

  ADM.EvolutionControl.ACControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TmcDeploymentManagers.peACGetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.GetSettings);
end;

procedure TmcDeploymentManagers.peACInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.InstallAC;
end;

procedure TmcDeploymentManagers.peACInstalled(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetACInstalled);
end;

procedure TmcDeploymentManagers.peACRestart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.RestartAC;
end;

procedure TmcDeploymentManagers.peACSetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);
  ADM.EvolutionControl.ACControl.SetLogFileThresholdInfo(AParams.Value['APurgeRecThreshold'], AParams.Value['APurgeRecCount']);
end;

procedure TmcDeploymentManagers.peACSetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);
  ADM.EvolutionControl.ACControl.SetSettings(IInterface(AParams.Value['Settings']) as IisListOfValues);
end;

procedure TmcDeploymentManagers.peACStart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StartAC;
end;

procedure TmcDeploymentManagers.peACStarted(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetACStarted);
end;

procedure TmcDeploymentManagers.peACStop(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StopAC;
end;

procedure TmcDeploymentManagers.peACUninstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.UninstallAC;
end;

procedure TmcDeploymentManagers.peACGetRequestList(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.
        GetRequestList(AParams.Value['APageNbr'], AParams.Value['AItemsPerPage'], iOutParam));
  AResult.AddValue('APageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peASGetFilteredLogEvents(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  LogQuery: TEventQuery;
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);

  LogQuery.StartTime := AParams.Value['StartTime'];
  LogQuery.EndTime := AParams.Value['EndTime'];
  LogQuery.Types := ByteToLogEventTypes(AParams.Value['Types']);
  LogQuery.EventClass := AParams.Value['EventClass'];
  LogQuery.MaxCount := AParams.Value['MaxCount'];
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.GetFilteredLogEvents(LogQuery));
end;

procedure TmcDeploymentManagers.peASGetLogEvents(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.
        GetLogEvents(AParams.Value['PageNbr'], AParams.Value['EventsPerPage'], iOutParam));
  AResult.AddValue('PageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peASGetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  PurgeRecThreshold, PurgeRecCount: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);

  ADM.EvolutionControl.ASControl.GetLogFileThresholdInfo(PurgeRecThreshold, PurgeRecCount);
  AResult.AddValue('APurgeRecThreshold', PurgeRecThreshold);
  AResult.AddValue('APurgeRecCount', PurgeRecCount);
end;

procedure TmcDeploymentManagers.peASGetRequestList(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.
        GetRequestList(AParams.Value['APageNbr'], AParams.Value['AItemsPerPage'], iOutParam));
  AResult.AddValue('APageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peASGetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.GetSettings);
end;

procedure TmcDeploymentManagers.peASInstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.InstallAS;
end;

procedure TmcDeploymentManagers.peASInstalled(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetASInstalled);
end;

procedure TmcDeploymentManagers.peASRestart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.RestartAS;
end;

procedure TmcDeploymentManagers.peASSetLogFileThresholdInfo(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);
  ADM.EvolutionControl.ASControl.SetLogFileThresholdInfo(AParams.Value['APurgeRecThreshold'], AParams.Value['APurgeRecCount']);
end;

procedure TmcDeploymentManagers.peASSetSettings(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);
  ADM.EvolutionControl.ASControl.SetSettings(IInterface(AParams.Value['Settings']) as IisListOfValues);
end;

procedure TmcDeploymentManagers.peASStart(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StartAS;
end;

procedure TmcDeploymentManagers.peASStarted(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.GetASStarted);
end;

procedure TmcDeploymentManagers.peASStop(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.StopAS;
end;

procedure TmcDeploymentManagers.peASUninstall(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  ADM.EvolutionControl.UninstallAS;
end;

procedure TmcDeploymentManagers.peASGetStatus(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.GetStatus);
end;

procedure TmcDeploymentManagers.peACGetTransRequestList(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.
        GetTransRequestList(AParams.Value['APageNbr'], AParams.Value['AItemsPerPage'], iOutParam));
  AResult.AddValue('APageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peASGetTransRequestList(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
var
  iOutParam: Integer;
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);

  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.
        GetTransRequestList(AParams.Value['APageNbr'], AParams.Value['AItemsPerPage'], iOutParam));
  AResult.AddValue('APageCount', iOutParam);
end;

procedure TmcDeploymentManagers.peASGetDomainStatus(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.GetDomainStatus(AParams.Value['ADomainName']));
end;

procedure TmcDeploymentManagers.peACGetDomainStatus(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.GetDomainStatus(AParams.Value['ADomainName']));
end;

procedure TmcDeploymentManagers.peACGetStatus(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetACStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ACControl.GetStatus);
end;

procedure TmcDeploymentManagers.peRBSetKnownADRServices(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetRBStarted, sServiceUnavailable);
  ADM.EvolutionControl.RBControl.SetKnownADRServices(IInterface(AParams.Value['AServices']) as IisListOfValues);
end;

procedure TmcDeploymentManagers.peASGetClientDBList(
  const ADM: ImcDMControl; const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);
  AResult.AddValue(METHOD_RESULT, ADM.EvolutionControl.ASControl.GetClientDBList(AParams.Value['ADomain']));
end;

procedure TmcDeploymentManagers.peASRebuildTT(const ADM: ImcDMControl;
  const AParams, AResult: IisListOfValues);
begin
  CheckCondition(ADM.EvolutionControl.GetASStarted, sServiceUnavailable);

  ADM.EvolutionControl.ASControl.RebuildTT(
        AParams.Value['ADomainName'], AParams.Value['ADataBase']);
end;

{ TmcMainboard }

destructor TmcMainboard.Destroy;
begin
  FSchedule := nil;
  FHttpPresenter := nil;
  FDeploymentManagers := nil;
  FUserAccounts := nil;

  GlobalLogFile.AddEvent(etInformation, 'Management Console', 'Service stopped', '');
  SetGlobalLogFile(nil);
  ClearDir(AppTempFolder, True);  
  inherited;
end;

procedure TmcMainboard.DoOnConstruction;
begin
  inherited;
  ClearDir(AppTempFolder);
  SetGlobalLogFile(TLogFile.Create(ChangeFileExt(AppFileName, '.log')));
  GlobalLogFile.PurgeRecThreshold := DefaultPurgeRecThreshold;
  GlobalLogFile.PurgeRecCount := DefaultPurgeRecCount;
end;

function TmcMainboard.DeploymentManagers: ImcDeploymentManagers;
begin
  Result := FDeploymentManagers;
end;

function TmcMainboard.HttpPresenter: ImcHttpPresenter;
begin
  Result := FHttpPresenter;
end;

function TmcMainboard.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  Event: ILogEvent;
begin
  GlobalLogFile.Lock;
  try
    CheckCondition((EventNbr >= 0) and (EventNbr < GlobalLogFile.Count), 'Event number is incorrect');
    Event := GlobalLogFile[EventNbr];
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisListOfValues.Create;
  Result.AddValue('TimeStamp', Event.TimeStamp);
  Result.AddValue('EventClass', Event.EventClass);
  Result.AddValue('EventType', Ord(Event.EventType));
  Result.AddValue('Text', Event.Text);
  Result.AddValue('Details', Event.Details);
end;

function TmcMainboard.GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  GlobalLogFile.Lock;
  try
    L := GlobalLogFile.GetPage(PageNbr, EventsPerPage);
    PageCount := GlobalLogFile.Count div EventsPerPage;
    if GlobalLogFile.Count mod EventsPerPage > 0 then
      Inc(PageCount);
  finally
    GlobalLogFile.UnLock;
  end;

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;


function TmcMainboard.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  i: Integer;
  Event: ILogEvent;
  EventInfo: IisListOfValues;
  L: IisList;
begin
  L := GlobalLogFile.GetEvents(AQuery);

  Result := TisParamsCollection.Create;
  for i := 0 to L.Count - 1 do
  begin
    Event := L[i] as ILogEvent;
    EventInfo := Result.AddParams(IntToStr(i + 1));
    EventInfo.AddValue('EventNbr', Event.Nbr);
    EventInfo.AddValue('TimeStamp', Event.TimeStamp);
    EventInfo.AddValue('EventClass', Event.EventClass);
    EventInfo.AddValue('EventType', Ord(Event.EventType));
    EventInfo.AddValue('Text', Event.Text);
  end;
end;

function TmcMainboard.AppSettings: IisSettings;
begin
  Result := FAppSettings;
end;

function TmcMainboard.UserAccounts: ImcUserAccounts;
begin
  Result := FUserAccounts;
end;

procedure TmcMainboard.Initialize;
begin
  FAppSettings := TisSettingsINI.Create(ChangeFileExt(AppFileName, '.cfg'));
  FUserAccounts := TmcUserAccounts.Create(nil, True);
  FDeploymentManagers := TmcDeploymentManagers.Create;
  FHttpPresenter := TmcHttpPresenter.Create;
  FSchedule := TmcSchedule.Create('MM Scheduler');
  FSchedule.Active := True;
  GlobalLogFile.AddEvent(etInformation, 'Management Console', 'Service started', '');
end;

function TmcMainboard.Schedule: ImcSchedule;
begin
  Result := FSchedule;
end;

procedure TmcMainboard.SendEMailNotification(const ASubject, AMessage: String);
var
  SMTPClient: TisSMTPClient;
begin
  try
    SMTPClient := TisSMTPClient.Create(
      AppSettings.AsString['eMail\SMTPServer'],
      AppSettings.GetValue('eMail\SMTPPort', 25),
      DecryptStringLocaly(BinToStr(AppSettings.AsString['eMail\UserName']), ''),
      DecryptStringLocaly(BinToStr(AppSettings.AsString['eMail\Password']), ''),
      atAutoSelect);
    try
      SMTPClient.SendQuickMessage(
        'ManagementConsole@' + AppSettings.AsString['eMail\NotificationDomain'],
        AppSettings.AsString['eMail\SendAlertsTo'],
        ASubject,AMessage);
    finally
      SMTPClient.Free;
    end;
  except
    on E: Exception do
      GlobalLogFile.AddEvent(etError, 'EMail Alerter', E.Message, GetStack);
  end;
end;

procedure TmcMainboard.UpdateEMailConfig(const ASMTPHost, ASMTPPort, AUser, APassword, ASendFrom, ASendTo: String);
begin
  AppSettings.AsString['eMail\SMTPServer'] := ASMTPHost;
  AppSettings.AsString['eMail\SMTPPort'] := ASMTPPort;
  AppSettings.AsString['eMail\UserName'] := StrToBin(EncryptStringLocaly(AUser));
  AppSettings.AsString['eMail\Password'] := StrToBin(EncryptStringLocaly(APassword));
  AppSettings.AsString['eMail\NotificationDomain'] := ASendFrom;
  AppSettings.AsString['eMail\SendAlertsTo'] := ASendTo;
end;

function TmcMainboard.GetRemoteVersions(const AApplication: String): IisParamsCollection;
const
  DefaultVersionListURL = 'http://www.isystemsllc.com/update-a2Gttf1/';
  QAVersionListURL = 'http://isys-evobuild/';
var
  i: Integer;
  VersionListURL: String;
  QAVerList: IisParamsCollection;
  LV: IisListOfValues;
begin
  VersionListURL := AppSettings.AsString[AApplication + '\UpdateURL'];
  if VersionListURL = '' then
    VersionListURL := DefaultVersionListURL;

  Result := DoGetRemoteVersions(VersionListURL);

  if isISystemsDomain then
  begin
    QAVerList := DoGetRemoteVersions(QAVersionListURL);
    for i := 0 to QAVerList.Count - 1 do
      if Result.ParamsByName(QAVerList.ParamName(i)) = nil then
      begin
        LV := Result.AddParams(QAVerList.ParamName(i));
        LV.Assign(QAVerList[i]);
        LV.AddValue('QA', True);
      end;
  end;

  // Keep only EvMajorVerNbr versions
  FilterIncompatibleVersions(Result);

  Result.Sort;
end;

procedure TmcMainboard.DownloadAppVersion(const AApplication: String; const AVersion: String);
var
  VerArch: IisStream;
  sSrcFile, sDestFile: String;
  LV: IisListOfValues;
  Versions: IisParamsCollection;
begin
  Versions := GetRemoteVersions(AApplication);
  LV := Versions.ParamsByName(AVersion);
  if Assigned(LV) then
  begin
    sSrcFile := LV.Value['File'];
    sDestFile := StringReplace(sSrcFile, '/', '\', [rfReplaceAll]);
    sDestFile := ChangeFilePath(sDestFile, AppDir + 'AppVersions\' + AApplication );
    ForceDirectories(ExtractFilePath(sDestFile));

    if StartsWith(sSrcFile, 'http://') then
    begin
      VerArch := TisHttpClient.Get(sSrcFile);
      VerArch.SaveToFile(sDestFile);
    end
    else
      CopyFile(sSrcFile, sDestFile, False);
  end;
end;

function TmcMainboard.GetLocalVersions(const AApplication: String): IisParamsCollection;
var
  i: Integer;
  s: String;
begin
  Result := GetVersions(AppDir + 'AppVersions\' + AApplication);

  // Keep only EvMajorVerNbr versions
  for i := Result.Count - 1 downto 0 do
  begin
    s := Result.ParamName(i);
    s := GetNextStrValue(s, '.');

    if StrToIntDef(s, 0) <> EvMajorVerNbr then
    begin
      DeleteFile(Result[i].Value['File']);
      Result.DeleteParams(i);
    end;
  end;

  // Keep only 5 versions and cleanup old ones
  Result.Sort('desc');
  for i := Result.Count - 1 downto 5 do
    DeleteFile(Result[i].Value['File']);

  Result.Sort;
end;

function TmcMainboard.GetVersions(const APath: String): IisParamsCollection;
var
  SR: TSearchRec;
  Found: Boolean;
  LV: IisListOfValues;
  sVer: String;
begin
  Result := TisParamsCollection.Create;

  Found := FindFirst(NormalizePath(APath) + '??.??.??.??.zip', faAnyFile and not faDirectory, SR) = 0;
  try
    while Found do
    begin
      sVer := ChangeFileExt(ExtractFileName(SR.Name), '');
      LV := Result.AddParams(sVer);
      LV.AddValue('File', NormalizePath(APath) + SR.Name);
      Found := FindNext(SR) = 0;
    end;
  finally
    FindClose(SR);
  end;
end;

function TmcMainboard.DoGetRemoteVersions(const AURL: String): IisParamsCollection;
var
  Page: IisStream;
  sPage, s, sFile: String;
  LV: IisListOfValues;
begin
  if StartsWith(AURL, 'http://') then
  begin
    Result := TisParamsCollection.Create;

    try
      Page := TisHttpClient.Get(AURL);
      sPage := Page.AsString;
      Page := nil;
      while sPage <> '' do
      begin
        s := GetNextStrValue(sPage, '.zip">');
        if sPage = '' then
          break;

        s := GetPrevStrValue(s, '"');
        sFile := NormalizePath(AURL) + s + '.zip';
        s := ChangeFileExt(ExtractFileName(StringReplace(sFile, '/', '\', [rfReplaceAll])), '');

        LV := Result.AddParams(s);
        LV.AddValue('File', sFile);
      end;
    except
      on E: Exception do
        GlobalLogFile.AddEvent(etError, 'HTTP Client', AURL + #13 + E.Message, BuildStackedErrorStr(E));
    end;
  end

  else
    Result := GetVersions(AURL);
end;

procedure TmcMainboard.FilterIncompatibleVersions(const AVersionList: IisParamsCollection);
var
  i: Integer;
  s: String;
begin
  for i := AVersionList.Count - 1 downto 0 do
  begin
    s := AVersionList.ParamName(i);
    s := GetNextStrValue(s, '.');

    if StrToIntDef(s, 0) <> EvMajorVerNbr then
      AVersionList.DeleteParams(i);
  end;
end;

{ TmcUserAccounts }

function TmcUserAccounts.AddUser(const AUserName: String): ImcUserAccount;
begin
  Result := IntAddUser(AUserName);
  Mainboard.AppSettings.AsString['UserAccount\' + Result.UserName] := '';
end;

function TmcUserAccounts.Count: Integer;
begin
  Result := ChildCount;
end;

procedure TmcUserAccounts.DeleteUser(const AUserName: String);
var
  Usr: ImcUserAccount;
begin
  Usr := FindUser(AUserName);
  if Assigned(Usr) then
    RemoveChild(Usr as IisInterfacedObject);
  Mainboard.AppSettings.DeleteValue('UserAccount\' + AUserName);
end;

procedure TmcUserAccounts.DoOnConstruction;
var
  L: IisStringListRO;
  i: Integer;
  Usr: ImcUserAccount;
begin
  inherited;

  L := Mainboard.AppSettings.GetValueNames('UserAccount');
  for i := 0 to L.Count - 1 do
  begin
    Usr := IntAddUser(L[i]);
    TmcUserAccount((Usr as IisInterfacedObject).GetImplementation).Load;
  end;

  if Count = 0 then
  begin
    // add default admin account
    Usr := AddUser('admin');
    Usr.Password := 'evolution';
    Usr.AccountType := uatAdministrator;
  end;
end;

function TmcUserAccounts.FindUser(const AUserName: String): ImcUserAccount;
begin
  Result := FindChildByName(AUserName) as ImcUserAccount;
end;

function TmcUserAccounts.GetItem(AIndex: Integer): ImcUserAccount;
begin
  Result := GetChild(AIndex) as ImcUserAccount;
end;

function TmcUserAccounts.IntAddUser(const AUserName: String): ImcUserAccount;
begin
  Result := TmcUserAccount.Create(AUserName);
  Result.Application := 'Evolution';
  AddChild(Result as IisInterfacedObject);
end;

{ TmcUserAccount }

constructor TmcUserAccount.Create(const AUserName: String);
begin
  inherited Create;
  SetName(AUserName);
end;

function TmcUserAccount.GetAccountType: TmcUserAccountType;
begin
  Result := FAccountType;
end;

function TmcUserAccount.GetApplication: String;
begin
  Result := FApplication;
end;

function TmcUserAccount.GetPassword: String;
begin
  Result := FPassword;
end;

function TmcUserAccount.GetUserName: String;
begin
  Result := GetName;
end;

procedure TmcUserAccount.Load;
var
  s: String;
begin
  s := Mainboard.AppSettings.AsString['UserAccount\' + GetUserName];

  FPassword := GetNextStrValue(s, ',');
  FPassword := DecryptStringLocaly(BinToStr(FPassword), GetUniqueID);

  FAccountType := TmcUserAccountType(StrToIntDef(s, 0));
end;

procedure TmcUserAccount.Save;
var
  s: String;
begin
  s := StrToBin(EncryptStringLocaly(FPassword)) + ',' + IntToStr(Ord(FAccountType));
  Mainboard.AppSettings.AsString['UserAccount\' + GetUserName] := s;
end;

procedure TmcUserAccount.SetAccountType(const AValue: TmcUserAccountType);
begin
  if FAccountType <> AValue then
  begin
    FAccountType := AValue;
    Save;
  end;
end;

procedure TmcUserAccount.SetApplication(const AValue: String);
begin
  FApplication := AValue;
end;

procedure TmcUserAccount.SetPassword(const AValue: String);
begin
  if FPassword <> AValue then
  begin
    FPassword := AValue;
    Save;
  end;
end;

procedure TmcUserAccount.SetUserName(const AValue: String);
begin
  if GetUserName <> AValue then
  begin
    SetName(AValue);
    Save;
  end;
end;


{ TmcSchedule }
procedure TmcSchedule.DoOnConstruction;
begin
  inherited;
  try
    LoadFromStorage;
  except
    Clear;
  end;  
end;

function TmcSchedule.GetStorageFile: String;
begin
  Result := AppDir + 'Scheduler.dat';
end;

procedure TmcSchedule.LoadFromStorage;
var
  F: IisStream;
begin
  inherited;
  Lock;
  try
    Clear;
    if FileExists(GetStorageFile) then
    begin
      F := TisStream.CreateFromFile(GetStorageFile);
      try
        ReadFromStream(F);
      except
        on E: Exception do
        begin
          GlobalLogFile.AddEvent(etError, GetName, 'File "' + GetStorageFile + '" is corrupted', BuildStackedErrorStr(E));
          Clear;
        end;
      end;
    end;
  finally
    Unlock;
  end;
end;

procedure TmcSchedule.RemoveFromStorage;
begin
  inherited;
  DeleteFile(GetStorageFile);
end;

procedure TmcSchedule.RunTaskFor(const AEvent: IisScheduleEvent);
var
  StopEvent: IisScheduleEventOnce;
  DBListByDomains: IisListOfValues;
  d: TDateTime;

  procedure ChangeMM(const Activate: Boolean);
  var
    sOper1, sOper2: String;
    bSuccess: Boolean;
    i: Integer;
  begin
    if Activate then
    begin
      sOper1 := 'activation';
      sOper2 := 'activated';
    end
    else
    begin
      sOper1 := 'deactivation';
      sOper2 := 'deactivated';
    end;

    bSuccess := False;
    for i := 1 to 3 do
    begin
      try
        Mainboard.HttpPresenter.SetMaintenanceDB(DBListByDomains, Activate);
        bSuccess := True;
        GlobalLogFile.AddEvent(etInformation, GetName, 'Maintenance mode ' + sOper2, '');
      except
        on E: Exception do
          GlobalLogFile.AddEvent(etWarning, GetName, 'Maintenance mode ' + sOper1 + ' failed', BuildStackedErrorStr(E));
      end;

      if not bSuccess and not CurrentThreadTaskTerminated then
      begin
        Mainboard.DeploymentManagers.ParallelExec(['RB.Restart', 'RP.Restart'], []);
        GlobalLogFile.AddEvent(etWarning, GetName, 'All RPs and RB restarted', '');
        Snooze(60000); // just in case to make sure all services have restarted and passed initialization
      end
      else
        break;
    end;

    if not bSuccess then
      Mainboard.SendEMailNotification('Maintenance Mode Alert', 'Maintenance mode ' + sOper1 + ' failed');
  end;

begin
  try
    if AEvent.TaskID = 'Evolution.StartDBMaintenance' then
    begin
      // add stop event
      StopEvent := TisScheduleEventOnce.Create;
      StopEvent.TaskID := 'Evolution.StopDBMaintenance';
      StopEvent.Description := 'Stop Evolution DB Maintenance';
      d := IncHour(Now, AEvent.TaskParams.Value['Duration']);
      StopEvent.StartDate := DateOf(d);
      StopEvent.StartTime := TimeOf(d);
      StopEvent.RunIfMissed := True;

      StopEvent.TaskParams.AddValue('Domains', AEvent.TaskParams.Value['Domains']);
      AddEvent(StopEvent);
      SaveToStorage;

      DBListByDomains := TisListOfValues.Create;
      DBListByDomains.AsStream := IInterface(AEvent.TaskParams.Value['Domains']) as IisStream;
      ChangeMM(True);

      AEvent.LastRunDate := Now;
    end

    else if AEvent.TaskID = 'Evolution.StopDBMaintenance' then
    begin
      DBListByDomains := TisListOfValues.Create;
      DBListByDomains.AsStream := IInterface(AEvent.TaskParams.Value['Domains']) as IisStream;
      ChangeMM(False);
      AEvent.LastRunDate := Now;
    end;

    SaveToStorage;
  except
    on E: Exception do
      GlobalLogFile.AddEvent(etError, GetName, E.Message, GetStack);
  end;
end;


procedure TmcSchedule.SaveToStorage;
var
  F: IisStream;
begin
  inherited;
  Lock;
  try
    F := TisStream.CreateFromNewFile(GetStorageFile);
    WriteToStream(F);
  finally
    Unlock;
  end;
end;

end.
