unit mcDSClient;

interface

uses SysUtils,
     isBaseClasses, isBasicUtils, isLogFile, EvStreamUtils,
     evTransportInterfaces, EvTransportShared, EvTransportDatagrams, evRemoteMethods,
     EvConsts, EvDMInterfaces, mcTypes, mcEvolutionControl;

type
  ImcDSConnector = interface
  ['{6035E10E-4921-44B2-BF85-F587BF601DF4}']
    function  SendRequest(const ADatagram: IevDatagram; var ASessionID: String; const AHost: String): IevDatagram;
  end;


  TecDSConnector = class(TevCustomTCPClient, ImcDSConnector)
  private
    FAppsInfo: IisParamsCollection;
  protected
    procedure DoOnConstruction; override;
    function  SendRequest(const ADatagram: IevDatagram; var ASessionID: String; const AHost: String): IevDatagram;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  end;


  TmcDMControl = class(TisCollection, IevDMControl, ImcDMControl)
  private
    FDSConnector: ImcDSConnector;
    FSessionID: String;
    FDescription: String;
  protected
    procedure SetAppsInfo(const AppsInfo: IisParamsCollection);
    function  GetAppsInfo: IisParamsCollection;
    procedure InstallApp(const AppName: String; const ArchiveFile: String);
    procedure UninstallApp(const AppName: String);
    function  GetAppArchiveFileName(const AppName: String): String;
    function  IsServiceInstalled(const AppName: String; const ServiceName: String): Boolean;
    procedure InstallService(const AppName: String; const ServiceName: String);
    procedure UninstallService(const AppName: String; const ServiceName: String);
    function  IsServiceStarted(const AppName: String; const ServiceName: String): Boolean;
    procedure StartService(const AppName: String; const ServiceName: String);
    procedure StopService(const AppName: String; const ServiceName: String);
    procedure RestartService(const AppName: String; const ServiceName: String);
    function  GetLogEvents(const PageNbr, EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
    function  GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
    function  GetLogEventDetails(const EventNbr: Integer): IisListOfValues;

    function  Host: String;
    function  Description: String;
    function  SendRequest(const ADatagram: IevDatagram): IevDatagram;
    function  EvolutionControl: ImcEvolutionControl;
    function  GetStackInfo: String;
  public
    constructor Create(const AHost: String; const ADescription: String = ''; const ADSConnector: ImcDSConnector = nil); reintroduce;
  end;



implementation

{ TecDSConnector }

function TecDSConnector.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevDatagramDispatcher.Create;
end;

procedure TecDSConnector.DoOnConstruction;

  procedure AddAppInfo(const AppName, MethodPrefix, Port, User, Password: String);
  var
    App: IisListOfValues;
  begin
    App := FAppsInfo.AddParams(AppName);
    App.AddValue('MethodPrefix', MethodPrefix);
    App.AddValue('Port', Port);
    App.AddValue('User', User);
    App.AddValue('Password', Password);
  end;

begin
  inherited;
  FAppsInfo := TisParamsCollection.Create;
  AddAppInfo('EvRequestBroker', METHOD_RBCONTROLLER, IntToStr(RBController_Port), '', '');
  AddAppInfo('EvRequestProc', METHOD_RPCONTROLLER, IntToStr(RPController_Port), '', '');
  AddAppInfo('EvRemoteRelay', METHOD_RRCONTROLLER, IntToStr(RRController_Port), '', '');
  AddAppInfo('EvAPIAdapter', METHOD_AACONTROLLER, IntToStr(AAController_Port), '', '');
  AddAppInfo('EvADRClient', METHOD_ACCONTROLLER, IntToStr(ACController_Port), '', '');
  AddAppInfo('EvADRServer', METHOD_ASCONTROLLER, IntToStr(ASController_Port), '', '');
  AddAppInfo('EvDBMaintenance', METHOD_DBMCONTROLLER, IntToStr(DBMController_Port), '', '');
end;

function TecDSConnector.SendRequest(const ADatagram: IevDatagram; var ASessionID: String; const AHost: String): IevDatagram;
var
  Session: IevSession;

  procedure SetAppsInfo;
  var
    D: IevDatagram;
  begin
    // DM should know what applications to control
    D := TevDatagram.Create(METHOD_DM_SETAPPSINFO);
    D.Params.AddValue('AppsInfo', FAppsInfo);
    D.Header.SessionID := Session.SessionID;
    D := Session.SendRequest(D);
  end;

begin
  Lock;
  try
    Session := FindSession(ASessionID);
    if not Assigned(Session) then
    begin
      ASessionID := CreateConnection(AHost, IntToStr(DMController_Port),
                                     '', '', '', '');
      Session := FindSession(ASessionID);
      SetAppsInfo;
    end;
  finally
    UnLock;
  end;

  ADatagram.Header.SessionID := Session.SessionID;
  Result := Session.SendRequest(ADatagram, 300000);  // 5 mins timeout
  CheckCondition(Assigned(Result), 'Response was not received within timeout');
end;


{ TmcDMControl }

constructor TmcDMControl.Create(const AHost: String; const ADescription: String = ''; const ADSConnector: ImcDSConnector = nil);
var
  Ev: ImcEvolutionControl;
begin
  inherited Create;

  if Assigned(ADSConnector) then
    FDSConnector := ADSConnector
  else
    FDSConnector := TecDSConnector.Create;

  SetName(AHost);
  FDescription := ADescription;
  Ev := TmcEvolutionControl.Create(Self, False);
end;

function TmcDMControl.Host: String;
begin
  Result := GetName;
end;

function TmcDMControl.SendRequest(const ADatagram: IevDatagram): IevDatagram;
begin
  Result := FDSConnector.SendRequest(ADatagram, FSessionID, Host);
end;

function TmcDMControl.Description: String;
begin
  Result := FDescription;
end;

function TmcDMControl.EvolutionControl: ImcEvolutionControl;
begin
  Result := GetChild(0) as ImcEvolutionControl;
end;

procedure TmcDMControl.InstallApp(const AppName, ArchiveFile: String);
var
  F: IisStream;
  D: IevDatagram;
begin
  F := TisStream.CreateFromFile(ArchiveFile);
  D := TevDatagram.Create(METHOD_DM_INSTALLAPP);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ArchiveFile', ArchiveFile);
  D.Params.AddValue('FileContent', F);
  D := SendRequest(D);
end;

procedure TmcDMControl.InstallService(const AppName, ServiceName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_INSTALLSERVICE);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
end;

function TmcDMControl.IsServiceInstalled(const AppName, ServiceName: String): Boolean;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_ISSERVICEINSTALLED);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

function TmcDMControl.IsServiceStarted(const AppName, ServiceName: String): Boolean;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_ISSERVICESTARTED);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

procedure TmcDMControl.RestartService(const AppName, ServiceName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_RESTARTSERVICE);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
end;

procedure TmcDMControl.StartService(const AppName, ServiceName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_STARTSERVICE);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
end;

procedure TmcDMControl.StopService(const AppName, ServiceName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_STOPSERVICE);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
end;

procedure TmcDMControl.UninstallService(const AppName, ServiceName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_UNINSTALLSERVICE);
  D.Params.AddValue('AppName', AppName);
  D.Params.AddValue('ServiceName', ServiceName);
  D := SendRequest(D);
end;

function TmcDMControl.GetAppArchiveFileName(const AppName: String): String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_GETAPPARCHIVEFILENAME);
  D.Params.AddValue('AppName', AppName);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];  
end;

procedure TmcDMControl.UninstallApp(const AppName: String);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_UNINSTALLAPP);
  D.Params.AddValue('AppName', AppName);
  D := SendRequest(D);
end;

function TmcDMControl.GetLogEventDetails(const EventNbr: Integer): IisListOfValues;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_GETLOGEVENTDETAILS);
  D.Params.AddValue('EventNbr', EventNbr);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisListOfValues;
end;

function TmcDMControl.GetLogEvents(const PageNbr,
  EventsPerPage: Integer; out PageCount: Integer): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_GETLOGEVENTS);
  D.Params.AddValue('PageNbr', PageNbr);
  D.Params.AddValue('EventsPerPage', EventsPerPage);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
  PageCount := D.Params.Value['PageCount'];
end;

function TmcDMControl.GetFilteredLogEvents(const AQuery: TEventQuery): IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_GETFILTEREDLOGEVENTS);
  D.Params.AddValue('StartTime', AQuery.StartTime);
  D.Params.AddValue('EndTime', AQuery.EndTime);
  D.Params.AddValue('Types', LogEventTypesToByte(AQuery.Types));
  D.Params.AddValue('EventClass', AQuery.EventClass);
  D.Params.AddValue('MaxCount', AQuery.MaxCount);

  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

function TmcDMControl.GetAppsInfo: IisParamsCollection;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_GETAPPSINFO);
  D := SendRequest(D);
  Result := IInterface(D.Params.Value[METHOD_RESULT]) as IisParamsCollection;
end;

procedure TmcDMControl.SetAppsInfo(const AppsInfo: IisParamsCollection);
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_SETAPPSINFO);
  D.Params.AddValue('AppsInfo', AppsInfo);
  D := SendRequest(D);
end;

function TmcDMControl.GetStackInfo: String;
var
  D: IevDatagram;
begin
  D := TevDatagram.Create(METHOD_DM_GETSTACKINFO);
  D := SendRequest(D);
  Result := D.Params.Value[METHOD_RESULT];
end;

end.
