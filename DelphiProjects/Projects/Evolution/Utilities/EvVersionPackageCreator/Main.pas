unit Main;

interface

uses SysUtils, IsBasicUtils, isBaseClasses, isSettings, isThreadManager, is7Zip,
     EvStreamUtils, isZippingRoutines;

procedure Run;

implementation

type
  TevServerPackageCreator = class
  private
    FProcessingTM: IThreadManager;
    FLastVersionsFolder: String;
    FBinFolder: String;
    FTargetFolder: String;
    FWorkFolder: String;
    FPackageInfo: IisParamsCollection;
    FPackageVersion: String;
    FLock: IisLock;
    FThreadError: String;
    procedure WriteLnOutput(const AText: String = '');
    procedure ProcessItemInThread(const Params: TTaskParamList);
    procedure ProcessItem(const APackageItem: IisListOfValues);
    procedure ProcessFile(const APackageItem: IisListOfValues);
    procedure ProcessArchive(const APackageItem: IisListOfValues);
    procedure ProcessDiff(const APackageItem: IisListOfValues);

    procedure PrepareMCPackageInfo;
    procedure PrepareMCPackageContent;
    procedure CreateFinalResult;
  public
    constructor Create(const ABinFolder, ATargetFolder, ALastVersionsFolder: String);
    destructor Destroy; override;
    procedure Build;
  end;

procedure Run;
var
  C: TevServerPackageCreator;
begin
  if ParamStr(1) = '/?' then
  begin
    Writeln('Parameters:');
    Writeln('   1. Binaries folder (run folder by default)');
    Writeln('   2. Destination folder (run folder by default)');
    Writeln('   3. Released versions folder (if empty, no diffs will be created)');
    Exit;
  end;

  C := TevServerPackageCreator.Create(ParamStr(1), ParamStr(2), ParamStr(3));
  try
    C.Build;
  finally
    C.Free;
  end;
end;

{ TevServerPackageCreator }

procedure TevServerPackageCreator.Build;
begin
  PrepareMCPackageInfo;
  PrepareMCPackageContent;
  CreateFinalResult;
end;

constructor TevServerPackageCreator.Create(const ABinFolder, ATargetFolder, ALastVersionsFolder: String);
begin
  FProcessingTM := TThreadManager.Create(ClassName);

  FLock := TisLock.Create;

  FBinFolder := NormalizePath(ExpandFileName(ABinFolder));
  if FBinFolder = '' then
    FBinFolder :=  AppDir;
  FPackageVersion := GetFileVersion(FBinFolder + 'Evolution.exe');

  FLastVersionsFolder := NormalizePath(ExpandFileName(ALastVersionsFolder));
  if FLastVersionsFolder = '' then
    FLastVersionsFolder := AppDir;

  FTargetFolder := NormalizePath(ExpandFileName(ATargetFolder));
  if FTargetFolder = '' then
    FTargetFolder :=  AppDir;

  FWorkFolder := AppTempFolder + 'Data\';
  ForceDirectories(FWorkFolder);
  ClearDir(FWorkFolder);
end;

procedure TevServerPackageCreator.PrepareMCPackageInfo;
var
  AppList: IisStringListRO;
  FileList, ClientSideBinaries: IisStringList;
  i, j: Integer;
  Item: IisListOfValues;
  DeploymentCFG: IisSettings;
  arcName, s, arctype: String;
  PackageMajorVer, ReleaseFolder: String;

  function StrVersion(AVersion: String): String;
  begin
    Result := '';
    while AVersion <> '' do
      Result := Result + FormatFloat('0000', StrToInt(GetNextStrValue(AVersion, '.')));
  end;

begin
  CheckCondition(FileExists(FBinFolder + 'Deployment.cfg'), 'Deployment.cfg is not found');
  DeploymentCFG := TisSettingsINI.Create(FBinFolder + 'Deployment.cfg');

  FPackageInfo := TisParamsCollection.Create;

  Item := FPackageInfo.AddParams('Deployment.cfg');
  Item.AddValue('Type', 'file');
  Item.AddValue('Source', FBinFolder + 'Deployment.cfg');

  Item := FPackageInfo.AddParams('xDelta.exe');
  Item.AddValue('Type', 'file');
  Item.AddValue('Source', FBinFolder + 'xDelta.exe');

  AppList := DeploymentCFG.GetChildNodes('');
  FileList := TisStringList.Create;
  ClientSideBinaries := TisStringList.CreateUnique;
  for i := 0 to AppList.Count - 1 do
  begin
    // Add application files
    FileList.CommaText := DeploymentCFG.AsString[AppList[i] + '\Content'];
    for j := 0 to FileList.Count - 1 do
      if FPackageInfo.ParamsByName(FileList[j]) = nil then
      begin
        Item := FPackageInfo.AddParams(FileList[j]);
        Item.AddValue('Type', 'file');
        Item.AddValue('Source', FBinFolder + FileList[j]);
      end;

    // Add application archive files
    arcName := DeploymentCFG.AsString[AppList[i] + '\Archive'];
    if (arcName <> '') and (FPackageInfo.ParamsByName(arcName) = nil) then
    begin
      arctype := ExtractFileExt(arcName);
      Delete(arctype, 1, 1);
      Item := FPackageInfo.AddParams(arcName);
      Item.AddValue('Type', 'archive');
      Item.AddValue('ArchiveName', arcName);
      Item.AddValue('ArchiveType', arctype);

      s := '';
      for j := 0 to FileList.Count - 1 do
      begin
        if not AnsiSameText(FileList[j], '7-zip32.dll') then
          AddStrValue(s, FBinFolder + FileList[j], ',');
        if Contains('.exe .dll .bpl', ExtractFileExt(FileList[j])) then
          ClientSideBinaries.Add(FileList[j]);
      end;
      Item.AddValue('Content', s);

      // Add each archive file too
      for j := 0 to FileList.Count - 1 do
        if not AnsiSameText(FileList[j], '7-zip32.dll') then
        begin
          arcName := FileList[j] + '.' + arctype;
          if FPackageInfo.ParamsByName(arcName) = nil then
          begin
            Item := FPackageInfo.AddParams(arcName);
            Item.AddValue('Type', 'archive');
            Item.AddValue('ArchiveName', arcName);            
            Item.AddValue('ArchiveType', arctype);
            Item.AddValue('Content', FBinFolder + FileList[j]);
          end;
        end;
    end;
  end;

  // Add diff folders
  if FLastVersionsFolder <> '' then
  begin
    PackageMajorVer := FPackageVersion;
    PackageMajorVer := GetNextStrValue(PackageMajorVer, '.');

    FileList := GetSubDirs(FLastVersionsFolder);
    for i := FileList.Count - 1 downto 0 do
    begin
      ReleaseFolder := ExtractFileName(FileList[i]);
      if not StartsWith(ReleaseFolder, PackageMajorVer) or
         (StrVersion(FPackageVersion) <= StrVersion(ReleaseFolder)) then
        FileList.Delete(i);
    end;

    for i := 0 to FileList.Count - 1 do
    begin
      ReleaseFolder := FileList[i];

      s := ExtractFileName(ReleaseFolder);
      Item := FPackageInfo.AddParams(s);
      Item.AddValue('Type', 'diff');
      Item.AddValue('DiffName', s);
      Item.AddValue('BaseVersion', ExtractFileName(DenormalizePath(ReleaseFolder)));
      Item.AddValue('BaseFolder', ReleaseFolder);

      s := '';
      for j := 0 to ClientSideBinaries.Count - 1 do
        if FileExists(NormalizePath(ReleaseFolder) + ClientSideBinaries[j]) then
          AddStrValue(s, FBinFolder + ClientSideBinaries[j], ',');

      Item.AddValue('Content', s);
    end;
  end;  
end;

procedure TevServerPackageCreator.PrepareMCPackageContent;
var
  i: Integer;
begin
  WriteLnOutput('Preparation');

  ClearDir(FWorkFolder);

  // Copy files first
  for i := 0 to FPackageInfo.Count - 1 do
    if FPackageInfo[i].Value['Type'] = 'file' then
      ProcessItem(FPackageInfo[i]);

  // Run 7-zip archives (since it is NOT thread-safe!)
  for i := 0 to FPackageInfo.Count - 1 do
    if FPackageInfo[i].Value['Type'] = 'archive' then
      ProcessItem(FPackageInfo[i]);

 // Process the rest of items
  FThreadError := '';
  for i := 0 to FPackageInfo.Count - 1 do
  begin
    if (FPackageInfo[i].Value['Type'] <> 'file') and (FPackageInfo[i].Value['Type'] <> 'archive') then
    begin
      FProcessingTM.RunTask(ProcessItemInThread, Self,
        MakeTaskParams([FPackageInfo[i]]), 'Processing ' + FPackageInfo.ParamName(i));

      FLock.Lock;
      try
        if FThreadError <> '' then
        begin
          FProcessingTM.TerminateTask;
          break;
        end;
      finally
        FLock.Unlock;
      end;
    end;
  end;

  FProcessingTM.WaitForTaskEnd;

  if FThreadError <> '' then
    raise Exception.Create(FThreadError);  
end;

procedure TevServerPackageCreator.CreateFinalResult;
var
  FileName: String;
  s: String;
begin
  s := FPackageVersion;
  FileName := '';
  while s <> '' do
   AddStrValue(FileName, FormatFloat('00', StrToInt(GetNextStrValue(s, '.'))), '.');
  FileName := FileName + '.zip';

  WriteLnOutput('Creating MC archive: ' + FileName);
  DeleteFile(FTargetFolder + FileName);
  ISZippingRoutines.AddToFile(FTargetFolder + FileName, FWorkFolder, '*.*', '', True);

  DeleteFile(FTargetFolder + FileName);
  ISZippingRoutines.AddToFile(FTargetFolder + FileName, FWorkFolder, '*', '', True);
end;

destructor TevServerPackageCreator.Destroy;
begin
  FProcessingTM.TerminateTask;
  FProcessingTM.WaitForTaskEnd;
  ClearDir(FWorkFolder, True);
  inherited;
end;

procedure TevServerPackageCreator.ProcessItem(const APackageItem: IisListOfValues);
var
  s: String;
begin
  s := APackageItem.Value['Type'];
  if s = 'file' then
    ProcessFile(APackageItem)
  else if s = 'archive' then
    ProcessArchive(APackageItem)
  else if s = 'diff' then
    ProcessDiff(APackageItem);
end;

procedure TevServerPackageCreator.ProcessArchive(const APackageItem: IisListOfValues);
var
  ArchiveType: String;
  Files: IisStringList;
  ArchiveData: IisStream;
  SourceFolder, SourceFiles: String;
  i: Integer;
begin
  ArchiveType := APackageItem.Value['ArchiveType'];
  Files := TisStringList.Create;
  Files.CommaText := APackageItem.Value['Content'];

  if Files.Count = 0 then
    Exit;

  WriteLnOutput('Creating archive: ' + APackageItem.Value['ArchiveName']);
  if ArchiveType = '7z' then
  begin
    if Files.Count = 1 then
      ArchiveData := CompressFile7z(Files[0], 9)
    else
    begin
      SourceFolder := ExtractFilePath(Files[0]);
      SourceFiles := '';
      for i := 0 to Files.Count - 1 do
      begin
        Assert(AnsiSameText(SourceFolder, ExtractFilePath(Files[i])), 'Archiving files from different folders is not supported');
        AddStrValue(SourceFiles, ExtractFileName(Files[i]), ',');
      end;

      ArchiveData := CompressFiles7z(SourceFolder, SourceFiles, 9);
    end;
    ArchiveData.SaveToFile(FWorkFolder + APackageItem.Value['ArchiveName']);
  end
  else
    Assert(False, 'Archiving method ' + ArchiveType + ' is not implemented');
end;

procedure TevServerPackageCreator.ProcessDiff(const APackageItem: IisListOfValues);
var
  DestFolder, BaseFolder, BaseVersion: String;
  OldFile, NewFile, DeltaFile: String;
  Files: IisStringList;
  i: Integer;
  NewFileInfo, DeltaFileInfo: TisFileInfo;
begin
  DestFolder := NormalizePath(FWorkFolder + APackageItem.Value['DiffName']);
  BaseVersion := APackageItem.Value['BaseVersion'];
  BaseFolder := NormalizePath(APackageItem.Value['BaseFolder']);
  Files := TisStringList.Create;
  Files.CommaText := APackageItem.Value['Content'];

  WriteLnOutput('Creating diff: ' + BaseVersion + ' --> ' + FPackageVersion);

  ForceDirectories(DestFolder);
  for i := 0 to Files.Count - 1 do
  begin
    NewFile := Files[i];
    OldFile := BaseFolder + ExtractFileName(NewFile);
    DeltaFile := DestFolder + ExtractFileName(NewFile);

    RunProcess(FBinFolder + 'xDelta.exe',
      Format('-e -9 -S djw -f -I 0 -q -s "%s" "%s" "%s"', [OldFile, NewFile, DeltaFile]),
      [roHideWindow, roWaitForClose]);

    // delta file should be less than 2/3 of the new file
    // otherwise there is no benefit in usage of such big delta file
    NewFileInfo := GetFileInfo(NewFile);
    DeltaFileInfo := GetFileInfo(DeltaFile);
    if (NewFileInfo.Size - DeltaFileInfo.Size) < NewFileInfo.Size div 3 then
      DeleteFile(DeltaFile);
  end;
end;

procedure TevServerPackageCreator.ProcessFile(const APackageItem: IisListOfValues);
var
  s: String;
begin
  s := APackageItem.Value['Source'];
  WriteLnOutput('Copying file: ' + ExtractFileName(s));
  CopyFile(s, FWorkFolder + ExtractFileName(s), False);
end;

procedure TevServerPackageCreator.ProcessItemInThread(const Params: TTaskParamList);
begin
  try
    ProcessItem(IInterface(Params[0]) as IisListOfValues);
  except
    on E: Exception do
    begin
      FLock.Lock;
      try
        if FThreadError = '' then
          FThreadError := E.Message;
      finally
        FLock.Unlock;
      end;
    end;
  end;
end;

procedure TevServerPackageCreator.WriteLnOutput(const AText: String);
begin
  FLock.Lock;
  try
    Writeln(Output, AText);
    Flush(Output);
  finally
    FLock.Unlock;
  end;
end;

end.
