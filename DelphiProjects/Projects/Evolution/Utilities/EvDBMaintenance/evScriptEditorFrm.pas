// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit evScriptEditorFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, isUtils;

type
  TevScriptEditor = class(TForm)
    Memo1: TMemo;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  evScriptEditor: TevScriptEditor;

implementation

{$R *.DFM}

procedure TevScriptEditor .FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=True;
  ModalResult:=mrNo;
  if Memo1.Modified then
  begin
    ModalResult:=isMessage('The script was changed. Would you like to save it?', mtConfirmation, mbYesNoCancel);
    if ModalResult=mrCancel then
    begin
      CanClose:=False;
    end;
  end;
end;

end.
