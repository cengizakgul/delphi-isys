unit evWizBackupDBSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvContext, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, EvConsts,
  wwdblook, isUIwwDBLookupCombo, isUIEdit, LMDBaseControl, PBFolderDialog,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, evWizBeforeProcessFrm,
  EvCommonInterfaces, Menus, EvBasicUtils;

type
  TevWizBackupDBSelect = class(TevWizardPage)
    pnlDBList: TisUIFashionPanel;
    lvDBList: TISListView;
    isUIFashionPanel1: TisUIFashionPanel;
    edDestFolder: TISEdit;
    ISSpeedButton1: TISSpeedButton;
    lFreeSpace: TISLabel;
    lSelectedSize: TISLabel;
    chbCompress: TISCheckBox;
    dlgDestFolder: TPBFolderDialog;
    pmSelect: TISPopupMenu;
    miSelectAll: TMenuItem;
    miUnselectAll: TMenuItem;
    miReverseSelection: TMenuItem;
    lCaption: TISLabel;
    chbScramble: TISCheckBox;
    procedure rbUpgradeDBClick(Sender: TObject);
    procedure ISSpeedButton1Click(Sender: TObject);
    procedure edDestFolderChange(Sender: TObject);
    procedure lvDBListChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure miSelectAllClick(Sender: TObject);
    procedure miUnselectAllClick(Sender: TObject);
    procedure miReverseSelectionClick(Sender: TObject);
    procedure chbCompressClick(Sender: TObject);
    procedure chbScrambleClick(Sender: TObject);
  private
    FSelectedSizeMB: Int64;
    procedure BuildDBList;
    procedure UpdateSizeInfoLabel;
  protected
    procedure InitParams; override;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

{$R *.dfm}

type
  TevFileListItem = class(TListItem)
  private
    function GetSizeMB: Cardinal;
  public
    constructor Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
    property SizeMB: Cardinal read GetSizeMB;
  end;

{ TevWizBackupDBSelect}

function TevWizBackupDBSelect.GetNextPage: TevWizardPage;
var
  dbL: IisParamsCollection;
  s, compr, dbs, db_info: String;
  i: Integer;
  bScramble: Boolean;
begin
  s := 'You are going to backup%s the databases listed below to the folder'#13'"%s"'#13#13'Databases:'#13'%s';

  if FParams.Value['Compression'] then
    compr := ' and compress'
  else
    compr := '';

  bScramble := FParams.Value['Scramble'];

  dbs := '';
  dbL := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  for i := 0 to dbL.Count - 1 do
  begin
    db_info := dbL[i].Value['DB'];
    if bScramble and StartsWith(db_info, DB_Cl) and not StartsWith(db_info, DB_Cl_Base) then
    begin
      dbL[i].Value['Scramble'] := True;
      db_info := db_info + #9#9'(Scramble sensitive data)';
    end;

    dbs := dbs + db_info + #13;
  end;

  s := Format(s, [compr, FParams.Value['DestFolder'], dbs]);

  FParams.Value['Description'] := s;

  Result := TevWizBeforeProcess.Create(Owner, FParams);

  mb_AppSettings.SetValue('LastBackupFolder', FParams.Value['DestFolder'])
end;

procedure TevWizBackupDBSelect.Init;
begin
  inherited;

  if not AnsiSameText(FParams.Value['Domain'], sDefaultDomain) then
    pnlDBList.Title := pnlDBList.Title + ' (Domain: ' + FParams.Value['Domain'] + ')';

  BuildDBList;
  edDestFolder.Text := FParams.Value['DestFolder'];
  chbCompress.Checked := FParams.Value['Compression'];
  chbScramble.Checked := FParams.Value['Scramble'];
end;

function TevWizBackupDBSelect.CanMoveForward: Boolean;
begin
  Result := ((IInterface(FParams.Value['Databases']) as IisParamsCollection).Count > 0) and DirectoryExists(FParams.Value['DestFolder']);
end;

procedure TevWizBackupDBSelect.rbUpgradeDBClick(Sender: TObject);
begin
  NotifyChange;
end;

procedure TevWizBackupDBSelect.BuildDBList;
var
  Items: IisStringList;

  procedure BuildForPath(const APath: String);
  var
    s, host: String;
    DS: IevDataSet;
  begin
    s := APath;
    host := GetNextStrValue(s, ':');
    if s = '' then
      Exit;

    DS := ctx_DBAccess.GetDBServerFolderContent(APath, FB_Admin, mb_GlobalSettings.DBAdminPassword);

    DS.IndexFieldNames := 'file_name';
    DS.First;
    while not DS.Eof do
    begin
      s := DS.FieldByName('file_name').AsString;
      if not StartsWith(s, DB_Service) and FinishesWith(s, '.gdb', True) and (Items.IndexOf(s) = -1) then
      begin
        TevFileListItem.Create(lvDBList.Items, s, DS.FieldByName('file_size').Value div (1024 * 1024));
        Items.Add(s);
      end;

      DS.Next;
    end;
  end;


var
  i: Integer;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, FParams.Value['Domain']), '');

    Items := TisStringList.CreateAsIndex;

    lvDBList.BeginUpdate;
    try
      for i := 0 to ctx_DomainInfo.DBLocationList.Count - 1 do
        BuildForPath(ctx_DomainInfo.DBLocationList[i].Path);
    finally
      lvDBList.EndUpdate;
    end;

    lvDBList.SortType := stText;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevWizBackupDBSelect.InitParams;
var
  P: IisParamsCollection;
begin
  inherited;
  FParams.AddValue('Domain', sDefaultDomain);
  FParams.AddValue('Method', 'Backup Databases');
  P := TisParamsCollection.Create;  
  FParams.AddValue('Databases', P);
  FParams.AddValue('DestFolder', mb_AppSettings.GetValue('LastBackupFolder', 'C:\'));
  FParams.AddValue('Compression', False);
  FParams.AddValue('Scramble', False);
  FParams.AddValue('Description', '');  
end;

procedure TevWizBackupDBSelect.UpdateSizeInfoLabel;
var
  estsz: Int64;
begin
  if FSelectedSizeMB > 0 then
  begin
    if FParams.Value['Compression'] then
      estsz := FSelectedSizeMB div 40 // average backup and compression ratio
    else
      estsz := FSelectedSizeMB div 10; // average backup ratio
    lSelectedSize.Caption := Format('Total selection %s (estimated backup %s)', [SizeToString(FSelectedSizeMB, 'M'), SizeToString(estsz, 'M')]);
  end
  else
    lSelectedSize.Caption := '';
end;


{ TevFileListItem }

constructor TevFileListItem.Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
begin
  inherited Create(AOwner);

  AOwner.AddItem(Self);
  Caption := AFileName;
  Data := Pointer(ASizeMB);
  SubItems.Add(SizeToString(ASizeMB, 'M'));
end;

procedure TevWizBackupDBSelect.ISSpeedButton1Click(Sender: TObject);
begin
  dlgDestFolder.Folder := edDestFolder.Text;
  if dlgDestFolder.Execute then
    edDestFolder.Text := dlgDestFolder.Folder;
end;

procedure TevWizBackupDBSelect.edDestFolderChange(Sender: TObject);
var
  s: String;
  total: Int64;
begin
  FParams.Value['DestFolder'] := DenormalizePath(Trim(edDestFolder.Text));

  s := ExtractFileDrive(FParams.Value['DestFolder']);
  if Length(s) <> 2 then
    lFreeSpace.Caption := ''
  else
  begin
    total := DiskSize(Ord(s[1]) - $40);
    if total <> -1 then
      lFreeSpace.Caption := SizeToString(DiskFree(Ord(s[1]) - $40)) + ' free of ' + SizeToString(total)
    else
      lFreeSpace.Caption := '';
  end;

  NotifyChange;
end;

procedure TevWizBackupDBSelect.lvDBListChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
var
  i: Integer;
  dbList: IisParamsCollection;
  dbInfo: IisListOfValues;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;

  dbList.Clear;
  FSelectedSizeMB := 0;

  for i := 0 to lvDBList.Items.Count - 1 do
    if lvDBList.Items[i].Checked then
    begin
      dbInfo := dbList.AddParams(lvDBList.Items[i].Caption);
      dbInfo.AddValue('DB', lvDBList.Items[i].Caption);
      dbInfo.AddValue('SizeMB', TevFileListItem(lvDBList.Items[i]).SizeMB);
      dbInfo.AddValue('Scramble', False);
      FSelectedSizeMB := FSelectedSizeMB + TevFileListItem(lvDBList.Items[i]).SizeMB;
    end;

  UpdateSizeInfoLabel;

  NotifyChange;
end;

procedure TevWizBackupDBSelect.miSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := True;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizBackupDBSelect.miUnselectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := False;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizBackupDBSelect.miReverseSelectionClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := not lvDBList.Items[i].Checked;
  finally
    lvDBList.EndUpdate;  
  end;
end;

function TevFileListItem.GetSizeMB: Cardinal;
begin
  Result := Cardinal(Data);
end;

procedure TevWizBackupDBSelect.chbCompressClick(Sender: TObject);
begin
  FParams.Value['Compression'] := chbCompress.Checked;
  UpdateSizeInfoLabel;
  NotifyChange;
end;

procedure TevWizBackupDBSelect.chbScrambleClick(Sender: TObject);
begin
  FParams.Value['Scramble'] := chbScramble.Checked;
  NotifyChange;
end;

end.
