inherited evWizImportDBSelect: TevWizImportDBSelect
  Width = 563
  Height = 441
  object pnlDBList: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 563
    Height = 441
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Importing databases'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      563
      441)
    object lSelectedSize: TISLabel
      Left = 180
      Top = 50
      Width = 140
      Height = 16
      Caption = 'Selected database size:'
    end
    object lvDBList: TISListView
      Left = 30
      Top = 88
      Width = 502
      Height = 268
      Anchors = [akLeft, akTop, akRight, akBottom]
      Columns = <
        item
          AutoSize = True
          Caption = 'Source Database'
        end
        item
          AutoSize = True
          Caption = 'Destination Folder'
        end
        item
          AutoSize = True
          Caption = 'Size'
          MaxWidth = 100
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = pmSelect
      TabOrder = 0
      ViewStyle = vsReport
      OnCreateItemClass = lvDBListCreateItemClass
    end
    object btnSelDB: TISButton
      Left = 30
      Top = 46
      Width = 130
      Height = 25
      Caption = 'Select Databases...'
      TabOrder = 1
      OnClick = btnSelDBClick
      Color = clBlack
      Margin = 0
    end
  end
  object pmSelect: TISPopupMenu
    Left = 136
    Top = 120
    object miRemove: TMenuItem
      Caption = 'Remove Selected'
      OnClick = miRemoveClick
    end
    object miRemoveAll: TMenuItem
      Caption = 'Remove All'
      OnClick = miRemoveAllClick
    end
  end
end
