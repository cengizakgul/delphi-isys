unit EvDBMaintController;

interface

uses Classes, SysUtils, isBaseClasses, EvTransportShared, evRemoteMethods,
    EvConsts, EvControllerInterfaces, EvTransportInterfaces, isAppIDs, isLogFile,
    EvMainboard, EvBasicUtils, evContext, EvCommonInterfaces;

type
   TevDBMController = class(TevCustomTCPServer, IevAppController)
   protected
     procedure DoOnConstruction; override;
     function  CreateDispatcher: IevDatagramDispatcher; override;
   end;

implementation

type
  TevDBMControllerDatagramDispatcher = class(TevDatagramDispatcher)
  private
    procedure dmGetLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetFilteredLogEvents(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetLogEventDetails(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetStatus(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmGetExecutingItems(const ADatagram: IevDatagram; out AResult: IevDatagram);
  protected
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure RegisterHandlers; override;
    procedure BeforeDispatchDatagram(const ADatagramHeader: IevDatagramHeader); override;
    procedure AfterDispatchDatagram(const ADatagramIn, ADatagramOut: IevDatagram); override;
  end;


{ TevRDController }

function TevDBMController.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevDBMControllerDatagramDispatcher.Create;
end;

procedure TevDBMController.DoOnConstruction;
begin
  inherited;
  SetPort(IntToStr(DBMController_Port));
  SetIPAddress('127.0.0.1');
end;


{ TevDBMControllerDatagramDispatcher }

procedure TevDBMControllerDatagramDispatcher.RegisterHandlers;
begin
  inherited;
  AddHandler(METHOD_DBMCONTROLLER_GETLOGEVENTS, dmGetLogEvents);
  AddHandler(METHOD_DBMCONTROLLER_GETFILTEREDLOGEVENTS, dmGetFilteredLogEvents);
  AddHandler(METHOD_DBMCONTROLLER_GETLOGEVENTDETAILS, dmGetLogEventDetails);
  AddHandler(METHOD_DBMCONTROLLER_GETSTATUS, dmGetStatus);
  AddHandler(METHOD_DBMCONTROLLER_GETEXECUTINGITEMS, dmGetExecutingItems);
end;


function TevDBMControllerDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoDeployMgrAppInfo.AppID);
end;

procedure TevDBMControllerDatagramDispatcher.dmGetLogEventDetails(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisListOfValues;
begin
//  Res := RBControl.GetLogEventDetails(
//    ADatagram.Params.Value['EventNbr']);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDBMControllerDatagramDispatcher.dmGetLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
//  PageCount: Integer;
begin
//  Res := RBControl.GetLogEvents(
//    ADatagram.Params.Value['PageNbr'],
//    ADatagram.Params.Value['EventsPerPage'],
//    PageCount);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
//  AResult.Params.AddValue('PageCount', PageCount);
end;

procedure TevDBMControllerDatagramDispatcher.dmGetFilteredLogEvents(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
  Query: TEventQuery;
begin
  Query.StartTime := ADatagram.Params.Value['StartTime'];
  Query.EndTime := ADatagram.Params.Value['EndTime'];
  Query.Types := ByteToLogEventTypes(ADatagram.Params.Value['Types']);
  Query.EventClass := ADatagram.Params.Value['EventClass'];
  Query.MaxCount := ADatagram.Params.Value['MaxCount'];

//  Res := RBControl.GetFilteredLogEvents(Query);
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDBMControllerDatagramDispatcher.AfterDispatchDatagram(
  const ADatagramIn, ADatagramOut: IevDatagram);
begin
  inherited;
  Mainboard.ContextManager.RestoreThreadContext;
end;

procedure TevDBMControllerDatagramDispatcher.BeforeDispatchDatagram(
  const ADatagramHeader: IevDatagramHeader);
var
  Session: IevSession;
begin
  inherited;
  Mainboard.ContextManager.StoreThreadContext;

  Session := FindSessionByID(ADatagramHeader.SessionID);
  if Session.State = ssActive then
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
end;

procedure TevDBMControllerDatagramDispatcher.dmGetStatus(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  status: TevQueueStatus;
  Res: IisListOfValues;
begin
  status := Mainboard.DBMaintenance.GetStatus;

  Res := TisListOfValues.Create;
  Res.AddValue('Executing', status.Executing);
  Res.AddValue('Queued', status.Queued);
  Res.AddValue('Finished', status.Finished);
  Res.AddValue('Skipped', status.Skipped);  
  Res.AddValue('Errored', status.Errored);
  Res.AddValue('TotalProgress', status.TotalProgress);
  Res.AddValue('StartTime', status.StartTime);
  Res.AddValue('ElapsedTimeSec', status.ElapsedTimeSec);
  Res.AddValue('EstimatedTimeLeftSec', status.EstimatedTimeLeftSec);

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

procedure TevDBMControllerDatagramDispatcher.dmGetExecutingItems(
  const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  Res: IisParamsCollection;
begin
  Res := Mainboard.DBMaintenance.GetExecutingItems;
  
  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(METHOD_RESULT, Res);
end;

end.
