unit evWizProcessFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, isUtils,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, GIFImage,
  LMDCustomButton, LMDButton, isUILMDButton, Mask, wwdbedit, Wwdbspin,
  evConsts, evMainboard, evCommonInterfaces, isThreadManager, Spin,
  EvUIComponents, Menus, Clipbrd, isErrorUtils, isLogFile, EvBasicUtils;

const
  WM_QUEUE_CHANGED = WM_USER + 1;
  
type
  TDBItem = class(TListItem)
  private
    FQueueItem: IevDBItem;
  public
    procedure Assign(Source: TPersistent); override;
    function  GetItemDetails: String;
  end;

  
  TevWizProcess = class(TevWizardPage)
    pnlStatus: TisUIFashionPanel;
    pbProgress: TProgressBar;
    lTotalProgress: TISLabel;
    timUpdate: TTimer;
    imGears: TImage;
    lTimeInfo: TISLabel;
    btnDetails: TISButton;
    lQueueStatus: TISLabel;
    pnlWork: TPanel;
    pnlQueue: TisUIFashionPanel;
    lvQueue: TISListView;
    lThreadPerServ: TISLabel;
    meDetails: TISMemo;
    pmQueue: TevPopupMenu;
    miCopyErrors: TMenuItem;
    procedure timUpdateTimer(Sender: TObject);
    procedure btnDetailsClick(Sender: TObject);
    procedure lvQueueChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure miCopyErrorsClick(Sender: TObject);
  private
    FCurrentQueueStatus: TevQueueStatus;
    FError: Boolean;
    FSettingUpQueue: Boolean;
    procedure DoBackupDatabases;
    procedure DoRestoreDatabases;
    procedure DoImportDatabases;
    procedure DoPatchDatabases;
    procedure DoSweepDatabases;
    procedure DoCustomScriptDatabases;    
    procedure UpdateStatus;
    procedure BuildThreadsCounters;
    procedure AsyncCallback(const AError: String);
    procedure WMOnQueueChanged(var Message: TMessage); message WM_QUEUE_CHANGED;
    procedure OnQueueChanged(const AItem: IevDBItem; const AOperation: TevQCEOperation);
    function  FindItemInList(const AListView: TISListView; const AItem: IevDBItem): TDBItem;
    procedure OnThreadsChanged(Sender: TObject);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
    function  NextButtonName: String; override;
  end;

implementation

{$R *.dfm}

uses evWizardFrm, DateUtils;

{ TDBItem }

procedure TDBItem.Assign(Source: TPersistent);
begin
  inherited;
  FQueueItem := TDBItem(Source).FQueueItem;
end;

function TDBItem.GetItemDetails: String;
var
  s, s2: String;
begin
  s :=
  'Domain:   %s'#13#10 +
  'Database: %s'#13#10 +
  'Location: %s'#13#10 +
  'Status:   %s'#13#10;

  s2 := FQueueItem.GetDBName;
  if FQueueItem.GetADRContext then
    s2 := s2 + ' (ADR)';

  s := Format(s, [FQueueItem.GetDomain, s2, FQueueItem.GetDBLocation, FQueueItem.GetStatus]);

  if FQueueItem.GetStartedAt <> 0 then
    s := s + 'Started at: ' + FormatDateTime('dd/mm/yyyy hh:nn:ss a/p', FQueueItem.GetStartedAt) + #13#10;

  if FQueueItem.GetFinishedAt <> 0 then
    s := s + 'Finished at: ' + FormatDateTime('dd/mm/yyyy hh:nn:ss a/p', FQueueItem.GetFinishedAt) + #13#10;

  s := s + #13#10 + FQueueItem.GetDetails;

  Result := s;
end;


{ TevWizProcess }

function TevWizProcess.GetNextPage: TevWizardPage;
begin
  Result := nil;
end;

procedure TevWizProcess.Init;
begin
  inherited;
  TevWizard(Owner).ClearHistory;
  Mainboard.DBMaintenance.ClearQueue;
  BuildThreadsCounters;
  FSettingUpQueue := True;
  UpdateStatus;
  FSettingUpQueue := False;
  pnlStatus.Title := '        Processing: ' + FParams.Value['Method'];
  timUpdate.Enabled := True;
  (imGears.Picture.Graphic as TGIFImage).Animate := True;

  if FParams.Value['Method'] = 'Patch Databases' then
    DoPatchDatabases
  else
  begin
    Mainboard.ContextManager.StoreThreadContext;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, FParams.Value['Domain']), '');

      if FParams.Value['Method'] = 'Backup Databases' then
        DoBackupDatabases
      else if FParams.Value['Method'] = 'Restore Databases' then
        DoRestoreDatabases
      else if FParams.Value['Method'] = 'Import Databases' then
        DoImportDatabases
      else if FParams.Value['Method'] = 'Sweep Databases' then
        DoSweepDatabases
      else if FParams.Value['Method'] = 'Custom Script Databases' then
        DoCustomScriptDatabases;
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;

  Sleep(250); // yield time slice for other threads so they start changing the queue
end;

function TevWizProcess.CanMoveForward: Boolean;
begin
  Result := FError or (pbProgress.Position = 100) and (FCurrentQueueStatus.Queued = 0) and (FCurrentQueueStatus.Executing = 0);
  timUpdate.Enabled := not Result;
  (imGears.Picture.Graphic as TGIFImage).Animate := not Result;

  if Result and (FCurrentQueueStatus.Errored > 0) and not pnlQueue.Visible then
    btnDetails.Click;
end;

function TevWizProcess.NextButtonName: String;
begin
  Result := 'Done';
end;

procedure TevWizProcess.DoBackupDatabases;
var
  dbList: IisParamsCollection;
  dbParams: array of TevBackupDBParam;
  i: Integer;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  SetLength(dbParams, dbList.Count);

  for i := 0 to dbList.Count - 1 do
  begin
    dbParams[i].Name := dbList[i].Value['DB'];
    dbParams[i].SizeMB := dbList[i].Value['SizeMB'];
    dbParams[i].Scramble := dbList[i].Value['Scramble'];
  end;

  Mainboard.DBMaintenance.RunBackup(dbParams,
    FParams.Value['DestFolder'],
    FParams.Value['Compression']);
end;

procedure TevWizProcess.timUpdateTimer(Sender: TObject);
begin
  UpdateStatus;
end;

procedure TevWizProcess.AfterConstruction;
begin
  inherited;
  Mainboard.DBMaintenance.OnQueueChanged := OnQueueChanged;
end;

procedure TevWizProcess.UpdateStatus;
var
  s: String;
begin
  try
    FCurrentQueueStatus := Mainboard.DBMaintenance.GetStatus;

    if not FSettingUpQueue and not FCurrentQueueStatus.Active then
      pbProgress.Position := 100
    else
      pbProgress.Position := FCurrentQueueStatus.TotalProgress;

    lTotalProgress.Caption := 'Total progress ' + IntToStr(pbProgress.Position) + '%';

    s := 'Elapsed time: ' + PeriodToString(FCurrentQueueStatus.ElapsedTimeSec * 1000);

{ This is highly inaccurate, so I comment it out for now. I may improve the algorithm later.    -Aleksey
    if FCurrentQueueStatus.EstimatedTimeLeftSec > 0 then
      s := s + '     Estimated time left: ' + PeriodToString(FCurrentQueueStatus.EstimatedTimeLeftSec * 1000, 'm');
}
    lTimeInfo.Caption := s;

    lQueueStatus.Caption := Format('Executing: %d     Queued: %d     Processed: %d     Skipped: %d     Errors: %d',
      [FCurrentQueueStatus.Executing, FCurrentQueueStatus.Queued, FCurrentQueueStatus.Finished, FCurrentQueueStatus.Skipped, FCurrentQueueStatus.Errored]);

    if pbProgress.Position = 100 then
      NotifyChange;
  except
    // Suppress any progress update errors just in case 
    on E: Exception do
      mb_LogFile.AddEvent(etError, 'Internal', 'Progress update error', GetStack(E));
  end;
end;

procedure TevWizProcess.btnDetailsClick(Sender: TObject);
begin
  pnlQueue.Visible := not pnlQueue.Visible;
  
  if pnlQueue.Visible then
    btnDetails.Caption := 'Hide Details'
  else
    btnDetails.Caption := 'Show Details'; 
end;

function TevWizProcess.FindItemInList(const AListView: TISListView; const AItem: IevDBItem): TDBItem;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to AListView.Items.Count - 1 do
  begin
    if TDBItem(AListView.Items[i]).FQueueItem = AItem then
    begin
      Result := TDBItem(AListView.Items[i]);
      break;
    end;
  end;
end;

procedure TevWizProcess.OnQueueChanged(const AItem: IevDBItem;  const AOperation: TevQCEOperation);
begin
  if Assigned(AItem) then
    AItem._AddRef;
    
  PostMessage(Handle, WM_QUEUE_CHANGED, Integer(Pointer(AItem)), Ord(AOperation));
end;

procedure TevWizProcess.WMOnQueueChanged(var Message: TMessage);
var
  Item: IevDBItem;
  Operation: TevQCEOperation;
  LstItem, LstItem2: TDBItem;
  s: String;
  i: Integer;
begin
  Item :=  IevDBItem(Pointer(Message.wParam));
  Operation := TevQCEOperation(Message.lParam);

  try
    case Operation of
      qceAdd:
      begin
        LstItem := TDBItem.Create(lvQueue.Items);
        LstItem.FQueueItem := Item;

        if (Item.GetState = qisExecuting) then
          i := 0
        else
          i := -1;
        lvQueue.Items.AddItem(LstItem, i);

        s := Item.GetDBName;
        if Item.GetDomain <> sDefaultDomain then
          s := Item.GetDomain + ': ' + s;
        if Item.GetADRContext then
          s := s + ' (ADR)';
        LstItem.Caption := s;

        LstItem.SubItems.Add(Item.GetDBHost);
        LstItem.SubItems.Add(Item.GetStatus);
      end;

      qceUpdate:
      begin
        LstItem := FindItemInList(lvQueue, Item);
        if Assigned(LstItem) then
        begin
          LstItem.SubItems[0] := Item.GetDBHost;
          LstItem.SubItems[1] := Item.GetStatus;
        end;
      end;

      qceDelete:
      begin
        LstItem := FindItemInList(lvQueue, Item);
        if Assigned(LstItem) then
          LstItem.Free;
      end;

      qceMoveToTop,
      qceMoveToBottom:
      begin
        LstItem := FindItemInList(lvQueue, Item);
        if Assigned(LstItem) then
        begin
          LstItem2 := TDBItem.Create(lvQueue.Items);
          if Operation = qceMoveToBottom then
            lvQueue.Items.AddItem(LstItem2)
          else
            lvQueue.Items.AddItem(LstItem2, 0);

          LstItem2.Assign(LstItem);
          LstItem.Free;
        end;
      end;
    end;

  finally
    if Assigned(Item) then
      Item._Release;
  end;
end;

procedure TevWizProcess.lvQueueChange(Sender: TObject; Item: TListItem;  Change: TItemChange);
begin
  if lvQueue.Selected <> nil then
    meDetails.Text := TDBItem(lvQueue.Selected).GetItemDetails
  else
    meDetails.Text := '';
end;

procedure TevWizProcess.DoImportDatabases;
var
  dbList: IisParamsCollection;
  Params: array of TevImportDBParam;
  i: Integer;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  SetLength(Params, dbList.Count);

  for i := 0 to dbList.Count - 1 do
  begin
    Params[i].SourceDB := dbList[i].Value['SrcDB'];
    Params[i].DestinationDB := dbList[i].Value['DstDB'];
    Params[i].SourcePassword := dbList[i].Value['Password'];
    Params[i].DBSizeMB := dbList[i].Value['SizeMB'];
  end;

  Mainboard.DBMaintenance.RunImport(Params);
end;

procedure TevWizProcess.DoPatchDatabases;
begin
  FSettingUpQueue := True;
  Mainboard.DBMaintenance.RunPatchingAsync(FParams.Value['Sweep'], FParams.Value['AppVersion'], FParams.Value['AllowDowngrade'], AsyncCallback);
end;

procedure TevWizProcess.DoSweepDatabases;
var
  dbList: IisParamsCollection;
  Params: array of TevDBParam;
  i: Integer;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  SetLength(Params, dbList.Count);

  for i := 0 to dbList.Count - 1 do
  begin
    Params[i].Name := dbList[i].Value['DB'];
    Params[i].SizeMB := dbList[i].Value['SizeMB'];
  end;

  Mainboard.DBMaintenance.RunSweep(Params);
end;

procedure TevWizProcess.BeforeDestruction;
begin
  Mainboard.DBMaintenance.OnQueueChanged := nil;
  inherited;
end;

procedure TevWizProcess.DoRestoreDatabases;
var
  dbList: IisParamsCollection;
  dbParams: array of TevRestoreDBParam;
  i: Integer;
  s: String;
begin
  dbList := IInterface(FParams.Value['Backups']) as IisParamsCollection;
  SetLength(dbParams, dbList.Count);

  for i := 0 to dbList.Count - 1 do
  begin
    s := dbList[i].Value['File'];
    s := ExtractFileName(s);
    s := GetNextStrValue(s, '.');

    dbParams[i].Name := s;
    dbParams[i].FileName := dbList[i].Value['File'];
    dbParams[i].SizeMB := dbList[i].Value['SizeMB'];
  end;

  Mainboard.DBMaintenance.RunRestore(dbParams);
end;

procedure TevWizProcess.DoCustomScriptDatabases;
var
  dbList: IisParamsCollection;
  dbParams: array of TevBackupDBParam;
  i: Integer;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  SetLength(dbParams, dbList.Count);

  for i := 0 to dbList.Count - 1 do
  begin
    dbParams[i].Name := dbList[i].Value['DB'];
    dbParams[i].SizeMB := dbList[i].Value['SizeMB'];
    dbParams[i].Scramble := dbList[i].Value['Scramble'];
  end;

  Mainboard.DBMaintenance.RunCustomScript(dbParams,
    FParams.Value['script'],
    FParams.Value['Sweep']);

end;

procedure TevWizProcess.AsyncCallback(const AError: String);

  procedure ProcessError(const AParams: TTaskParamList);
  begin
    TevWizProcess(Pointer(Integer(AParams[0]))).FError := True;
    TevWizProcess(Pointer(Integer(AParams[0]))).NotifyChange;
    raise Exception.Create(AParams[1]);
  end;

begin
  if AError = '' then
    Self.FSettingUpQueue := False
  else
    GlobalThreadManager.RunInMainThread(@ProcessError, MakeTaskParams([Self, AError]));
end;

procedure TevWizProcess.BuildThreadsCounters;
var
  i: Integer;
  T: IisListOfValues;
  E: TISDBSpinEdit;
  L: TISLabel;
  x: Integer;
begin
  T := Mainboard.DBMaintenance.ThreadsPerServer;

  x := 24;
  for i := 0 to T.Count - 1 do
  begin
    L := TISLabel.Create(Self);
    L.Caption := T[i].Name;
    L.Left := x;
    L.Top := lThreadPerServ.Top + 25;
    L.Height := 24;
    L.Anchors := [akLeft, akBottom];
    L.Font.Charset := DEFAULT_CHARSET;
    L.Font.Color := clWindowText;
    L.Font.Height := -11;
    L.Font.Name := 'Arial';
    L.Font.Style := [];
    L.ParentFont := False;
    L.Parent := pnlQueue;

    E := TISDBSpinEdit.Create(Self);
    E.Name := 'E' + IntToStr(i);
    E.Left := x;
    E.Top := lThreadPerServ.Top + 45;
    E.Width := 50;
    E.Height := 24;
    E.Anchors := [akLeft, akBottom];
    E.Increment := 1;
    E.MaxValue := 256;
    E.MinValue := 1;
    E.Value := T[i].Value;
    E.Parent := pnlQueue;
    E.OnChange := OnThreadsChanged;

    x := x + 80;    
  end;
end;

procedure TevWizProcess.OnThreadsChanged(Sender: TObject);
var
  s: String;
begin
  s := (Sender as TISDBSpinEdit).Name;
  Delete(s, 1, 1);
  Mainboard.DBMaintenance.UpdateThreadsPerServer(Mainboard.DBMaintenance.ThreadsPerServer[StrToInt(s)].Name, Round((Sender as TISDBSpinEdit).Value));
end;

procedure TevWizProcess.miCopyErrorsClick(Sender: TObject);
var
  Content: String;
  i: Integer;
begin
  Content := '';
  for i := 0 to lvQueue.Items.Count - 1 do
  begin
    AddStrValue(Content, TDBItem(lvQueue.Items[i]).GetItemDetails, #13#10#13#10#13#10'------------------------------------------------------------------------------------'#13#10);
  end;

  if Content <> '' then
  begin
    Clipboard.Open;
    try
      Clipboard.SetTextBuf(@Content[1]);
    finally
      Clipboard.Close;
    end;

    isMessage('All errors have been copied into clipboard', mtInformation, [mbOK]);
  end;
end;


end.
