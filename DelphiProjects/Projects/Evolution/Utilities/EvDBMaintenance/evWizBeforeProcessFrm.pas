unit evWizBeforeProcessFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvConsts,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, evWizProcessFrm, EvMainboard;

type
  TevWizBeforeProcess = class(TevWizardPage)
    isUIFashionPanel2: TisUIFashionPanel;
    memDescription: TISMemo;
    chbCorrect: TISCheckBox;
    lThreadPerServ: TISLabel;
    Panel1: TPanel;
    procedure chbCorrectClick(Sender: TObject);
  private
    procedure BuildThreadsCounters;
    procedure OnThreadsChanged(Sender: TObject);
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
    function  NextButtonName: String; override;
  end;

implementation

{$R *.dfm}

{ TevWizBeforeProcess }

function TevWizBeforeProcess.GetNextPage: TevWizardPage;
begin
  Result := TevWizProcess.Create(Owner, FParams);
end;

procedure TevWizBeforeProcess.Init;
var
  s: String;
begin
  inherited;

  Mainboard.DBMaintenance.InitThreadsPerServer;
  BuildThreadsCounters;
  
  if not AnsiSameText(FParams.TryGetValue('Domain', sDefaultDomain), sDefaultDomain) then
    s := 'Evolution Domain: ' + FParams.Value['Domain'] + #13#13
  else
    s := '';

  memDescription.Lines.Text := s + FParams.Value['Description'];
end;

function TevWizBeforeProcess.CanMoveForward: Boolean;
begin
  Result := chbCorrect.Checked;
end;

function TevWizBeforeProcess.NextButtonName: String;
begin
  Result := 'Run';
end;

procedure TevWizBeforeProcess.chbCorrectClick(Sender: TObject);
begin
  NotifyChange;
end;

procedure TevWizBeforeProcess.BuildThreadsCounters;
var
  i: Integer;
  T: IisListOfValues;
  E: TISDBSpinEdit;
  L: TISLabel;
  x: Integer;
begin
  T := Mainboard.DBMaintenance.ThreadsPerServer;

  x := 30;
  for i := 0 to T.Count - 1 do
  begin
    L := TISLabel.Create(Self);
    L.Caption := T[i].Name;
    L.Left := x;
    L.Top := lThreadPerServ.Top + 25;
    L.Height := 24;
    L.Anchors := [akLeft, akBottom];
    L.Font.Charset := DEFAULT_CHARSET;
    L.Font.Color := clWindowText;
    L.Font.Height := -11;
    L.Font.Name := 'Arial';
    L.Font.Style := [];
    L.ParentFont := False;
    L.Parent := isUIFashionPanel2;

    E := TISDBSpinEdit.Create(Self);
    E.Name := 'E' + IntToStr(i);
    E.Left := x;
    E.Top := lThreadPerServ.Top + 45;
    E.Width := 50;
    E.Height := 24;
    E.Anchors := [akLeft, akBottom];
    E.Increment := 1;
    E.MaxValue := 256;
    E.MinValue := 1;
    E.Value := T[i].Value;
    E.Parent := isUIFashionPanel2;
    E.OnChange := OnThreadsChanged;

    x := x + 80;
  end;
end;

procedure TevWizBeforeProcess.OnThreadsChanged(Sender: TObject);
var
  s: String;
begin
  s := (Sender as TISDBSpinEdit).Name;
  Delete(s, 1, 1);
  Mainboard.DBMaintenance.UpdateThreadsPerServer(Mainboard.DBMaintenance.ThreadsPerServer[StrToInt(s)].Name, Round((Sender as TISDBSpinEdit).Value));
end;

end.
