inherited evWizRestoreDBSelect: TevWizRestoreDBSelect
  Width = 499
  Height = 578
  object pnlDBList: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 499
    Height = 578
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Restoring Databases'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      499
      578)
    object lvDBList: TISListView
      Left = 30
      Top = 94
      Width = 437
      Height = 398
      Anchors = [akLeft, akTop, akRight, akBottom]
      Columns = <
        item
          Caption = 'File'
          Width = 340
        end
        item
          Caption = 'Size'
          Width = 70
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      MultiSelect = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = pmSelect
      TabOrder = 0
      ViewStyle = vsReport
    end
    object btnSelBackup: TISButton
      Left = 30
      Top = 51
      Width = 161
      Height = 25
      Caption = 'Select backup files...'
      TabOrder = 1
      OnClick = btnSelBackupClick
      Color = clBlack
      Margin = 0
    end
  end
  object odBackup: TOpenDialog
    Filter = 
      'Evolution database backup files (*.gbk.bz2)|*.gbk.bz2|Evolution ' +
      'database backup files (*.zip)|*.zip|Firebird backup files (*.gbk' +
      ')|*.gbk'
    FilterIndex = 0
    InitialDir = 'C:\'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Title = 'Backup files selection'
    Left = 208
    Top = 48
  end
  object pmSelect: TISPopupMenu
    Left = 136
    Top = 120
    object miRemove: TMenuItem
      Caption = 'Remove Selected'
      OnClick = miRemoveClick
    end
    object miRemoveAll: TMenuItem
      Caption = 'Remove All'
      OnClick = miRemoveAllClick
    end
  end
end
