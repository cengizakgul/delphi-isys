unit evAutonomousMode;

interface

uses SysUtils, isBasicUtils, EvMainboard, isErrorUtils, isLogFile, isThreadManager,
  isBaseClasses, EvStreamUtils, evCommonInterfaces, isSettings, SimpleXML, EvContext,
  EvConsts, EvBasicUtils, EvUIUtils;

procedure ExecAutonomousMode;

implementation

const
  sHelpUsage =  'EvDBMaintenance.exe -password <password> -mode <mode>' +
       #10#13 +
       #10#13 +
       'Password: SYSDBA password' +
       #10#13 +
       #10#13 +
       'Mode:' +
       #10#13 +
       #10#13;
  sHelpBackUp = 'backup <path> [compress]' +
       #10#13 +
       '  Backup all the databases to <path> folder. Compress option indicates that backup files should be compressed.';
  sHelpRestore = 'restore <path>' +
       #10#13 +
       '  Restore all the backup files (*.gbk.bz2 and *.gbk) located in <path> folder.';
  sHelpSweep = 'sweep' +
       #10#13 +
       '  Sweep all the databases.';

type
  THelpContent = (hAll, hBackUp, hRestore, hSweep);

  TevAutonomousWorker = class
  private
    FPassword: String;
    FErrFileName: String;
    procedure ProcessCommand(const AParams: IisStringList);
    procedure ProcessScript(const AParams: IisStringList);
    procedure ProcessUpgrade(const AParams: IisStringList);
    procedure ProcessPatch(const AParams: IisStringList);
    procedure ProcessImport(const AParams: IisStringList);
    procedure ProcessBackUp(const AParams: IisStringList);
    procedure ProcessRestore(const AParams: IisStringList);
    procedure ProcessSweep(const AParams: IisStringList);

    procedure ShowHelp(aContent: THelpContent);
  public
    procedure Execute;
  end;


procedure ExecAutonomousMode;
var
  W: TevAutonomousWorker;
begin
  W := TevAutonomousWorker.Create;
  try
    W.Execute;
  finally
    W.Free;
  end;
end;


{ TevAutonomousWorker }

procedure TevAutonomousWorker.Execute;
var
  modeParams: IisStringList;
  ErrorFile: IisStream;
begin
  FErrFileName := ChangeFileExt(AppFileName, '.err');
  DeleteFile(FErrFileName);

  modeParams := TisStringList.Create;
  modeParams.Text := AppSwitches.TryGetValue('mode', '');

  if AppSwitches.ValueExists('help') then
  begin
    if modeParams.Count >= 1 then
    begin
      if AnsiSameText(modeParams[0], 'backup') then
        ShowHelp(hBackUp)
      else if AnsiSameText(modeParams[0], 'restore') then
        ShowHelp(hRestore)
      else if AnsiSameText(modeParams[0], 'sweep') then
        ShowHelp(hSweep)
      else
        ShowHelp(hAll);
    end
    else
      ShowHelp(hAll);
  end
  else
    try
      FPassword := AppSwitches.TryGetValue('password', 'placeholder');
      CheckCondition(FPassword = mb_GlobalSettings.DBAdminPassword, 'Password is incorrect');

      if modeParams.Count = 0 then
        Exit;

      ProcessCommand(modeParams);
    except
      on E: Exception do
      begin
        mb_LogFile.AddEvent(etFatalError, 'Main', E.Message, GetStack(E));

        ErrorFile := TisStream.CreateFromNewFile(FErrFileName);
        ErrorFile.Writeln(E.Message);
      end;
    end;
end;

procedure TevAutonomousWorker.ProcessCommand(const AParams: IisStringList);
var
  Status: TevQueueStatus;
  mode: String;
  i: Integer;
  ErrorFile: IisStream;
  ErrItems: IisParamsCollection;
begin
  mode := AParams[0];
  AParams.Delete(0);

  if AnsiSameText(mode, 'upgrade') then
    ProcessUpgrade(AParams)
  else if AnsiSameText(mode, 'patch') then
    ProcessPatch(AParams)
  else if AnsiSameText(mode, 'import') then
    ProcessImport(AParams)
  else if AnsiSameText(mode, 'script') then
    ProcessScript(AParams)
  else if AnsiSameText(mode, 'backup') then
    ProcessBackUp(AParams)
  else if AnsiSameText(mode, 'restore') then
    ProcessRestore(AParams)
  else if AnsiSameText(mode, 'sweep') then
    ProcessSweep(AParams);

  // Waiting for the last item get processed
  Status := Mainboard.DBMaintenance.GetStatus;
  while (Status.Executing > 0) or (Status.Queued > 0) do
  begin
    if (Status.Executing + Status.Queued) > 2 then
      Snooze(5000)
    else
      Snooze(500);

    Status := Mainboard.DBMaintenance.GetStatus;
  end;

  // Checking for errors
  ErrItems := Mainboard.DBMaintenance.GetErroredItems;
  if ErrItems.Count > 0 then
  begin
    ErrorFile := TisStream.CreateFromNewFile(FErrFileName);
    for i := 0 to ErrItems.Count - 1 do
    begin
      ErrorFile.Writeln('Database: ' + ErrItems[i].Value['DBName']);
      ErrorFile.Writeln('Host: ' + ErrItems[i].Value['DBHost']);
      ErrorFile.Writeln(ErrItems[i].Value['Status']);
      ErrorFile.Writeln(ErrItems[i].Value['Details']);
      ErrorFile.Writeln('');
      ErrorFile.Writeln('');
    end;
  end;
end;

procedure TevAutonomousWorker.ProcessImport(const AParams: IisStringList);
var
  ImportParams: array of TevImportDBParam;
begin
  if AParams.Count < 3 then
    exit
  else
  begin
    SetLength(ImportParams, 1);
    ImportParams[0].SourcePassword := AParams[0];
    ImportParams[0].SourceDB := AParams[1];
    ImportParams[0].DestinationDB := AParams[2];
    if AParams.Count > 3 then
      ImportParams[0].DBSizeMB := StrToIntDef(AParams[3], 0)
    else
      ImportParams[0].DBSizeMB := 0;
      
    Mainboard.DBMaintenance.RunImport(ImportParams);
  end;
end;

procedure TevAutonomousWorker.ProcessUpgrade(const AParams: IisStringList);
var
  sweep: Boolean;
begin
  if AParams.Count > 0 then
    sweep := AnsiSameText(AParams[0], 'sweep')
  else
    sweep := False;

  Mainboard.DBMaintenance.RunPatching(sweep, '', False);
end;

procedure TevAutonomousWorker.ProcessScript(const AParams: IisStringList);
var
  ImportParams: array of TevImportDBParam;
  Script: IisSettings;
  Items: IisStringListRO;
  Item: IXmlElement;
  i, n: Integer;
  s: String;
begin
  if AParams.Count < 1 then
    exit
  else
  begin
    s := AParams[0];
    if ExtractFilePath(s) = '' then
      s := AppDir + s;

    Script := TisSettingsXML.Create(s, '', 'script');

    Items := Script.GetChildNodes('import');
    SetLength(ImportParams, Items.Count);
    n := 0;
    for i := 0 to Items.Count - 1 do
    begin
      if AnsiSameText(Items[i], 'item') then
      begin
        Item := Items.Objects[i] as IXmlElement;

        ImportParams[i].SourcePassword := Item.GetAttr('sourcePassword', FPassword);
        ImportParams[i].SourceDB := Item.GetAttr('dbSource', '');
        ImportParams[i].DestinationDB := Item.GetAttr('dbDestination', '');
        ImportParams[i].DBSizeMB := StrToIntDef(Item.GetAttr('dbSize', '0'), 0);

        if (ImportParams[0].SourceDB <> '') and (ImportParams[0].DestinationDB <> '') then
          Inc(n);
      end;
    end;
    SetLength(ImportParams, n);

    if Length(ImportParams) > 0 then
      Mainboard.DBMaintenance.RunImport(ImportParams);
  end;
end;

procedure TevAutonomousWorker.ProcessPatch(const AParams: IisStringList);
var
  ver: string;
  sweep: Boolean;
begin
  ver := '';
  sweep := False;

  if AParams.Count > 0 then
  begin
    if not AnsiSameText(AParams[0], 'current') then
      ver := AParams[0];

    if AParams.Count > 1 then
      sweep := AnsiSameText(AParams[1], 'sweep');
  end;

  Mainboard.DBMaintenance.RunPatching(sweep, ver, False);
end;

procedure TevAutonomousWorker.ProcessBackUp(const AParams: IisStringList);
var
  BackupParams: array of TevBackupDBParam;
  Compress: boolean;
  i: integer;
  Items: IisStringList;

  procedure BuildForPath(const APath: String);
  var
    s, host: String;
    DS: IevDataSet;
  begin
    s := APath;
    host := GetNextStrValue(s, ':');
    if s = '' then
      Exit;

    DS := ctx_DBAccess.GetDBServerFolderContent(APath, FB_Admin, mb_GlobalSettings.DBAdminPassword);

    DS.IndexFieldNames := 'file_name';
    DS.First;
    while not DS.Eof do
    begin
      s := DS.FieldByName('file_name').AsString;
      if not StartsWith(s, DB_Service) and FinishesWith(s, '.gdb', True) and (Items.IndexOf(s) = -1) then
      begin
        SetLength(BackupParams, Length(BackupParams) + 1);
        with BackupParams[Length(BackupParams)-1] do
        begin
          Name := s;
          SizeMB := DS.FieldByName('file_size').Value div (1024 * 1024);
          Scramble := False;
        end;
        Items.Add(s);

      end;
      DS.Next;
    end;
  end;

begin
  if AParams.Count < 1 then
    exit
  else
  begin  // parameters: 1. Backup Folder, 2. Compress?
    SetLength(BackupParams, 0);

    Mainboard.ContextManager.StoreThreadContext;
    Items := TisStringList.CreateAsIndex;
    try
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
      for i := 0 to ctx_DomainInfo.DBLocationList.Count - 1 do
        BuildForPath(ctx_DomainInfo.DBLocationList[i].Path);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;

    Compress := False;
    if AParams.Count > 1 then
      Compress := UpperCase(AParams[1]) = 'COMPRESS';

    Mainboard.DBMaintenance.RunBackup(BackupParams, AParams[0], Compress);
  end;
end;

procedure TevAutonomousWorker.ProcessRestore(const AParams: IisStringList);
var
  RestoreParams: array of TevRestoreDBParam;

  function NormalDir(const DirName: string): string;
  begin
    Result := DirName;
    if (Result <> '') and not (AnsiLastChar(Result)^ in [':', '\']) then
    begin
      if (Length(Result) = 1) and (UpCase(Result[1]) in ['A'..'Z']) then
        Result := Result + ':\'
      else
        Result := Result + '\';
    end;
  end;

  procedure FindFilesByMask(const APath, AMask: String);
  var
    SR: TSearchRec;
  begin
    if SysUtils.FindFirst(NormalDir(APath) + AMask, faAnyFile, SR) = 0 then
    begin
      repeat
        if ((SR.Attr and faDirectory) <> faDirectory) then
        begin
          SetLength(RestoreParams, Length(RestoreParams) + 1);
          with RestoreParams[Length(RestoreParams)-1] do
          begin
            Name := SR.Name;
            Name := GetNextStrValue(Name, '.');
            FileName := NormalDir(APath) + SR.Name;
            SizeMB := SR.Size div (1024 * 1024);
          end;
        end
        else if (SR.Name <> '.') and (SR.Name <> '..') then
          FindFilesByMask(NormalDir(APath) + SR.Name, AMask);
      until SysUtils.FindNext(SR) <> 0;
      SysUtils.FindClose(SR);
    end;
  end;

begin
  if AParams.Count < 1 then
    exit
  else
  begin  // parameters: 1. Folder that contains BackUp files: *.gbk
    SetLength(RestoreParams, 0);

    FindFilesByMask(AParams[0], '*.gbk.bz2');
    FindFilesByMask(AParams[0], '*.gbk');

    Mainboard.DBMaintenance.RunRestore(RestoreParams);
  end;
end;

procedure TevAutonomousWorker.ProcessSweep(const AParams: IisStringList);
var
  SweepParams: array of TevDBParam;
  i: integer;
  Items: IisStringList;

  procedure BuildForPath(const APath: String);
  var
    s, host: String;
    DS: IevDataSet;
  begin
    s := APath;
    host := GetNextStrValue(s, ':');
    if s = '' then
      Exit;

    DS := ctx_DBAccess.GetDBServerFolderContent(APath, FB_Admin, mb_GlobalSettings.DBAdminPassword);

    DS.IndexFieldNames := 'file_name';
    DS.First;
    while not DS.Eof do
    begin
      s := DS.FieldByName('file_name').AsString;
      if not StartsWith(s, DB_Service) and FinishesWith(s, '.gdb', True) and (Items.IndexOf(s) = -1) then
      begin
        SetLength(SweepParams, Length(SweepParams) + 1);
        with SweepParams[Length(SweepParams)-1] do
        begin
          Name := s;
          SizeMB := DS.FieldByName('file_size').Value div (1024 * 1024);
        end;
        Items.Add(s);

      end;
      DS.Next;
    end;
  end;

begin
  SetLength(SweepParams, 0);

  Mainboard.ContextManager.StoreThreadContext;
  Items := TisStringList.CreateAsIndex;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
    for i := 0 to ctx_DomainInfo.DBLocationList.Count - 1 do
      BuildForPath(ctx_DomainInfo.DBLocationList[i].Path);
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;

  Mainboard.DBMaintenance.RunSweep(SweepParams);
end;

procedure TevAutonomousWorker.ShowHelp(aContent: THelpContent);
var
  s: string;
begin
  s := sHelpUsage;

  case aContent of
  hAll:
    s := s + '1. ' + sHelpBackUp +
      #10#13 +
      #10#13 +
      '2. ' + sHelpRestore +
      #10#13 +
      #10#13 +
      '3. ' + sHelpSweep;
  hBackUp:
    s := s + sHelpBackUp;
  hRestore:
    s := s + sHelpRestore;
  hSweep:
    s := s + sHelpSweep;
  end;

  EvMessage(s);
end;

end.
