inherited evWizDomainSelection: TevWizDomainSelection
  Width = 465
  Height = 395
  object isUIFashionPanel2: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 465
    Height = 395
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Please select Evolution domain'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      465
      395)
    object lvDomainList: TISListView
      Left = 30
      Top = 44
      Width = 407
      Height = 267
      Anchors = [akLeft, akTop, akRight, akBottom]
      Checkboxes = True
      Columns = <
        item
          Caption = 'Domain ID'
          Width = 100
        end
        item
          AutoSize = True
          Caption = 'Descripton'
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = lvDomainListChange
    end
  end
end
