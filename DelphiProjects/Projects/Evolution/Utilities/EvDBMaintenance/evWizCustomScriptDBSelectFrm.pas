unit evWizCustomScriptDBSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvContext, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, EvConsts,
  wwdblook, isUIwwDBLookupCombo, isUIEdit, LMDBaseControl, PBFolderDialog,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, evWizBeforeProcessFrm,
  EvCommonInterfaces, Menus, EvBasicUtils, evScriptEditorFrm, Buttons;

type
  TevWizCustomScriptDBSelect = class(TevWizardPage)
    pnlDBList: TisUIFashionPanel;
    lvDBList: TISListView;
    isUIFashionPanel1: TisUIFashionPanel;
    lSelectedSize: TISLabel;
    pmSelect: TISPopupMenu;
    miSelectAll: TMenuItem;
    miUnselectAll: TMenuItem;
    miReverseSelection: TMenuItem;
    lCaption: TISLabel;
    btnScriptEditor: TSpeedButton;
    OpenScript: TOpenDialog;
    btnScriptSelect: TSpeedButton;
    edtScript: TISEdit;
    lDetails: TISLabel;
    chbSweep: TISCheckBox;
    procedure lvDBListChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure miSelectAllClick(Sender: TObject);
    procedure miUnselectAllClick(Sender: TObject);
    procedure miReverseSelectionClick(Sender: TObject);
    procedure btnScriptEditorClick(Sender: TObject);
    procedure btnScriptSelectClick(Sender: TObject);
    procedure chbSweepClick(Sender: TObject);
  private
    FScriptPath: string;
    FSelectedSizeMB: Int64;
    procedure BuildDBList;
    procedure UpdateSizeInfoLabel;
    procedure LaunchEditor;
  protected
    procedure InitParams; override;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

{$R *.dfm}

type
  TevFileListItem = class(TListItem)
  private
    function GetSizeMB: Cardinal;
  public
    constructor Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
    property SizeMB: Cardinal read GetSizeMB;
  end;

{ TevWizBackupDBSelect}

function TevWizCustomScriptDBSelect.GetNextPage: TevWizardPage;
var
  dbL: IisParamsCollection;
  dbs, db_info: String;
  i: Integer;
//  bScramble: Boolean;
begin
  dbs := '';
  dbL := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  for i := 0 to dbL.Count - 1 do
  begin
    db_info := dbL[i].Value['DB'];
    dbs := dbs + db_info + #13;
  end;

  FParams.Value['Description'] :=
    Format( 'Script: '#13#13 +  '%s'#13#13 +
            'The utility is going to run script ONLY those databases. '#13#13 +
            'Sweep after  update: %s'#13#13+
            'Databases: '#13 +  dbs,
              [FParams.Value['script'], IIF(FParams.Value['Sweep'], 'yes', 'no')]);

  Result := TevWizBeforeProcess.Create(Owner, FParams);
end;

procedure TevWizCustomScriptDBSelect.Init;
begin
  inherited;

  if not AnsiSameText(FParams.Value['Domain'], sDefaultDomain) then
    pnlDBList.Title := pnlDBList.Title + ' (Domain: ' + FParams.Value['Domain'] + ')';

  chbSweep.Checked := FParams.Value['Sweep'];
  edtScript.Text := '';
end;

function TevWizCustomScriptDBSelect.CanMoveForward: Boolean;
begin
  Result := ((IInterface(FParams.Value['Databases']) as IisParamsCollection).Count > 0);
end;

procedure TevWizCustomScriptDBSelect.BuildDBList;
var
  Items: IisStringList;

  procedure BuildForPath(const APath: String);
  var
    s, host: String;
    DS: IevDataSet;

    procedure AddItems(const sFilter: string);
    begin
      DS.IndexFieldNames := 'file_name';
      DS.First;
      while not DS.Eof do
      begin
        s := DS.FieldByName('file_name').AsString;

        if not StartsWith(s, DB_Service) and FinishesWith(s, '.gdb', True) and (Items.IndexOf(s) = -1) and
           StartsWith(s, sFilter) then
        begin
          TevFileListItem.Create(lvDBList.Items, s, DS.FieldByName('file_size').Value div (1024 * 1024));
          Items.Add(s);
        end;
        DS.Next;
      end;
    end;

  begin
    s := APath;
    host := GetNextStrValue(s, ':');
    if s = '' then
      Exit;

    DS := ctx_DBAccess.GetDBServerFolderContent(APath, FB_Admin, mb_GlobalSettings.DBAdminPassword);

    if StartsWith(ExtractFileName(FScriptPath), DB_S_Bureau) then
        AddItems(DB_S_Bureau)
    else if StartsWith(ExtractFileName(FScriptPath), DB_TempTables) then
        AddItems(DB_TempTables)
    else if StartsWith(ExtractFileName(FScriptPath), DB_Cl) then
        AddItems(DB_Cl);
  end;

var
  i: Integer;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, FParams.Value['Domain']), '');

    Items := TisStringList.CreateAsIndex;

    lvDBList.BeginUpdate;
    lvDBList.Clear;
    try
      for i := 0 to ctx_DomainInfo.DBLocationList.Count - 1 do
        BuildForPath(ctx_DomainInfo.DBLocationList[i].Path);
    finally
      lvDBList.EndUpdate;
    end;

    lvDBList.SortType := stText;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevWizCustomScriptDBSelect.InitParams;
var
  P: IisParamsCollection;
begin
  inherited;
  FParams.AddValue('Domain', sDefaultDomain);
  FParams.AddValue('Method', 'Custom Script Databases');
  P := TisParamsCollection.Create;  
  FParams.AddValue('Databases', P);
  FParams.AddValue('script', '');
  FParams.AddValue('Sweep', false);
  FParams.AddValue('Description', '');
end;

procedure TevWizCustomScriptDBSelect.UpdateSizeInfoLabel;
var
  estsz: Int64;
begin
  if FSelectedSizeMB > 0 then
  begin
    estsz := FSelectedSizeMB div 10; // average backup ratio
    lSelectedSize.Caption := Format('Total selection %s (estimated backup %s)', [SizeToString(FSelectedSizeMB, 'M'), SizeToString(estsz, 'M')]);
  end
  else
    lSelectedSize.Caption := '';
end;

{ TevFileListItem }

constructor TevFileListItem.Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
begin
  inherited Create(AOwner);
  AOwner.AddItem(Self);
  Caption := AFileName;
  Data := Pointer(ASizeMB);
  SubItems.Add(SizeToString(ASizeMB, 'M'));
end;


procedure TevWizCustomScriptDBSelect.lvDBListChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
var
  i: Integer;
  dbList: IisParamsCollection;
  dbInfo: IisListOfValues;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;

  dbList.Clear;
  FSelectedSizeMB := 0;

  for i := 0 to lvDBList.Items.Count - 1 do
    if lvDBList.Items[i].Checked then
    begin
      dbInfo := dbList.AddParams(lvDBList.Items[i].Caption);
      dbInfo.AddValue('DB', lvDBList.Items[i].Caption);
      dbInfo.AddValue('SizeMB', TevFileListItem(lvDBList.Items[i]).SizeMB);
      dbInfo.AddValue('Scramble', False);
      FSelectedSizeMB := FSelectedSizeMB + TevFileListItem(lvDBList.Items[i]).SizeMB;
    end;

  UpdateSizeInfoLabel;

  NotifyChange;
end;

procedure TevWizCustomScriptDBSelect.miSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := True;
  finally
    lvDBList.EndUpdate;
  end;
end;

procedure TevWizCustomScriptDBSelect.miUnselectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := False;
  finally
    lvDBList.EndUpdate;
  end;
end;

procedure TevWizCustomScriptDBSelect.miReverseSelectionClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := not lvDBList.Items[i].Checked;
  finally
    lvDBList.EndUpdate;
  end;
end;

function TevFileListItem.GetSizeMB: Cardinal;
begin
  Result := Cardinal(Data);
end;

procedure TevWizCustomScriptDBSelect.btnScriptEditorClick(Sender: TObject);
begin
  inherited;
  if FScriptPath <> '' then
     LaunchEditor;
end;

procedure TevWizCustomScriptDBSelect.LaunchEditor;
begin
  if FScriptPath <> '' then
  begin
    evScriptEditor:=TevScriptEditor.Create(Application);
    try
      evScriptEditor.Memo1.Lines.LoadFromFile(FScriptPath);
      if evScriptEditor.ShowModal=mrYes then
      begin
        evScriptEditor.Memo1.Lines.SaveToFile(FScriptPath);
      end;
    finally
      evScriptEditor.Free;
    end;
  end;
end;

procedure TevWizCustomScriptDBSelect.btnScriptSelectClick(Sender: TObject);
begin
  inherited;
  OpenScript.FileName := edtScript.Text;
  if OpenScript.Execute then
  begin
    edtScript.Text:=OpenScript.FileName;
    FScriptPath:=Trim(edtScript.Text);
    FParams.Value['script']:= FScriptPath;
    BuildDBList;
  end;
end;

procedure TevWizCustomScriptDBSelect.chbSweepClick(Sender: TObject);
begin
  inherited;
  FParams.Value['Sweep'] := chbSweep.Checked;
end;

end.
