inherited evWizCustomScriptDBSelect: TevWizCustomScriptDBSelect
  Width = 706
  Height = 647
  object pnlDBList: TisUIFashionPanel
    Left = 0
    Top = 201
    Width = 706
    Height = 446
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Databases'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      706
      446)
    object lSelectedSize: TISLabel
      Left = 30
      Top = 349
      Width = 140
      Height = 16
      Anchors = [akLeft, akBottom]
      Caption = 'Selected database size:'
    end
    object lCaption: TISLabel
      Left = 30
      Top = 45
      Width = 299
      Height = 16
      Caption = 'Select databases you want to run custom script on '
    end
    object lvDBList: TISListView
      Left = 30
      Top = 72
      Width = 644
      Height = 264
      Anchors = [akLeft, akTop, akRight, akBottom]
      Checkboxes = True
      Columns = <
        item
          Caption = 'Database'
          Width = 150
        end
        item
          Caption = 'Size'
          Width = 70
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = pmSelect
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = lvDBListChange
    end
  end
  object isUIFashionPanel1: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 706
    Height = 201
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 1
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Script Info '
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    DesignSize = (
      706
      201)
    object btnScriptEditor: TSpeedButton
      Left = 24
      Top = 49
      Width = 25
      Height = 25
      Hint = 'Edit Patch'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnScriptEditorClick
    end
    object btnScriptSelect: TSpeedButton
      Left = 652
      Top = 49
      Width = 25
      Height = 25
      Anchors = [akTop, akRight]
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = btnScriptSelectClick
    end
    object lDetails: TISLabel
      Left = 22
      Top = 110
      Width = 560
      Height = 48
      Anchors = [akLeft, akTop, akRight]
      Caption = 
        'If this option is off script process completes faster but may re' +
        'quire more free disk space (double  size of database files plus ' +
        '10%). It is strongly recommended to perform database sweeps manu' +
        'ally at later time.'
      WordWrap = True
    end
    object edtScript: TISEdit
      Left = 60
      Top = 50
      Width = 583
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 0
    end
    object chbSweep: TISCheckBox
      Left = 22
      Top = 84
      Width = 291
      Height = 17
      Caption = 'Sweep databases after script is complete'
      TabOrder = 1
      OnClick = chbSweepClick
    end
  end
  object pmSelect: TISPopupMenu
    Left = 648
    Top = 88
    object miSelectAll: TMenuItem
      Caption = 'Select All'
      ShortCut = 16449
      OnClick = miSelectAllClick
    end
    object miUnselectAll: TMenuItem
      Caption = 'Unselect All'
      OnClick = miUnselectAllClick
    end
    object miReverseSelection: TMenuItem
      Caption = 'Reverse Selection'
      OnClick = miReverseSelectionClick
    end
  end
  object OpenScript: TOpenDialog
    Filter = 'SQL script (*.sql)|*.sql'
    Options = [ofReadOnly, ofHideReadOnly]
    Left = 264
    Top = 520
  end
end
