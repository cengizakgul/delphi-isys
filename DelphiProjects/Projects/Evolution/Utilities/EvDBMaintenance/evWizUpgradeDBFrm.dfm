inherited evWizUpgradeDB: TevWizUpgradeDB
  Width = 538
  Height = 459
  object isUIFashionPanel2: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 538
    Height = 433
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Database patching options'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    DesignSize = (
      538
      433)
    object lDetails: TISLabel
      Left = 30
      Top = 158
      Width = 427
      Height = 75
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 
        'If this option is off patching process completes faster but may ' +
        'require more free disk space (double  size of database files plu' +
        's 10%). It is strongly recommended to perform database sweeps ma' +
        'nually at later time.'
      WordWrap = True
    end
    object Label1: TLabel
      Left = 30
      Top = 58
      Width = 198
      Height = 16
      Caption = 'Evolution application version range'
    end
    object Label2: TLabel
      Left = 32
      Top = 272
      Width = 182
      Height = 16
      Caption = 'DB downgrade license modifier:'
    end
    object Label3: TLabel
      Left = 32
      Top = 328
      Width = 404
      Height = 16
      Caption = 
        'DB downgrade license key (only valid on the day the key was issu' +
        'ed):'
    end
    object chbSweep: TISCheckBox
      Left = 30
      Top = 132
      Width = 291
      Height = 17
      Caption = 'Sweep databases after patching is complete'
      TabOrder = 1
      OnClick = chbSweepClick
    end
    object cbVersions: TevComboBox
      Left = 30
      Top = 80
      Width = 283
      Height = 24
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 16
      TabOrder = 0
      OnChange = cbVersionsChange
    end
    object edMachineID: TEdit
      Left = 30
      Top = 296
      Width = 123
      Height = 24
      Enabled = False
      ReadOnly = True
      TabOrder = 2
    end
    object CopyButton: TButton
      Left = 160
      Top = 296
      Width = 121
      Height = 25
      Caption = 'Copy to Clipboard'
      Enabled = False
      TabOrder = 3
      OnClick = CopyButtonClick
    end
    object edDowngradeKey: TEdit
      Left = 32
      Top = 352
      Width = 121
      Height = 24
      Enabled = False
      TabOrder = 4
    end
    object btnVerifyKey: TButton
      Left = 160
      Top = 352
      Width = 121
      Height = 25
      Caption = 'Verify License Key'
      Enabled = False
      TabOrder = 5
      OnClick = btnVerifyKeyClick
    end
    object chbDowngrade: TISCheckBox
      Left = 30
      Top = 244
      Width = 259
      Height = 17
      Caption = 'Downgrade databases to a prior version'
      TabOrder = 6
      OnClick = chbDowngradeClick
    end
  end
end
