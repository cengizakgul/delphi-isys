unit evWizModeSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvMainboard,
  isBaseClasses, isUIFashionPanel, evWizDomainSelectionFrm, evWizBackupDBSelectFrm,
  evWizImportDBSelectFrm, evWizUpgradeDBFrm, evWizSweepDBSelectFrm, evWizRestoreDBSelectFrm,
  evDBVersion, evWizCustomScriptDBSelectFrm;

type
  TevWizModeSelect = class(TevWizardPage)
    isUIFashionPanel2: TisUIFashionPanel;
    rbUpgradeDB: TRadioButton;
    rbSweepDB: TRadioButton;
    rbImportDB: TRadioButton;
    rbBackupDB: TRadioButton;
    rbRestoreDB: TRadioButton;
    rbCustomScriptDB: TRadioButton;
    procedure rbUpgradeDBClick(Sender: TObject);
  private
    function GetNextPageClass: TevWizardPageClass;
  public
    procedure AfterConstruction; override;
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

uses evWizardFrm;

{$R *.dfm}

{ TevWizModeSelect }

function TevWizModeSelect.GetNextPage: TevWizardPage;
begin
  Result := GetNextPageClass.Create(Owner, FParams);
end;

procedure TevWizModeSelect.Init;
begin
  inherited;
//  rbUpgradeDB.Caption := Format('Check and upgrade (Evo application version %s)', [AppVersion]);

  TevWizard(Owner).ClearHistory;
end;

function TevWizModeSelect.CanMoveForward: Boolean;
begin
  Result := GetNextPageClass <> nil;
end;

function TevWizModeSelect.GetNextPageClass: TevWizardPageClass;
var
  Cl: TevWizardPageClass;
begin
  Result := nil;
  FParams.DeleteValue('NextPageClass');

  if rbUpgradeDB.Checked then
    Result := TevWizUpgradeDB
  else
  begin
    if rbBackupDB.Checked then
      Cl := TevWizBackupDBSelect
    else if rbRestoreDB.Checked then
      Cl := TevWizRestoreDBSelect
    else if rbImportDB.Checked then
      Cl := TevWizImportDBSelect
    else if rbSweepDB.Checked then
      Cl := TevWizSweepDBSelect
    else if rbCustomScriptDB.Checked then
      Cl := TevWizCustomScriptDBSelect
    else
      Cl := nil;

    if Assigned(Cl) then
    begin
       if mb_GlobalSettings.DomainInfoList.Count > 1 then
       begin
         Result := TevWizDomainSelection;
         FParams.AddValue('NextPageClass', Integer(Pointer(Cl)));
       end
       else
         Result := Cl;
    end;
  end;
end;

procedure TevWizModeSelect.rbUpgradeDBClick(Sender: TObject);
begin
  NotifyChange;
end;

procedure TevWizModeSelect.AfterConstruction;
begin
  inherited;
  FParams := TisListOfValues.Create;
end;

end.
