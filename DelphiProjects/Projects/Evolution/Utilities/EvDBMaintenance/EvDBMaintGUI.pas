unit EvDBMaintGUI;

interface

uses Classes, SysUtils, Forms, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvTypes, Controls, sSecurityClassDefs, SSecurityInterface, EvStreamUtils,
     SWaitingObjectForm, evWizardFrm, IsUtils, EvUtils, EvContext, EvTransportInterfaces,
     EvConsts, EvUIUtils, isBasicUtils;

implementation

type
  TevDBMaintGUI = class(TisInterfacedObject, IevEvolutionGUI, IevContextCallbacks)
  private
    FWaitDialog: TAdvancedWait;
    FMainForm: TevWizard;
    FGUIContext: IevContext;
  private
    procedure OnException(Sender: TObject; E: Exception);
    procedure OnShowModalDialog(Sender: TObject);
    procedure OnHideModalDialog(Sender: TObject);
  protected
    procedure DoOnConstruction; override;
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
    procedure CreateMainForm;
    procedure DestroyMainForm;
    procedure ApplySecurity(const Component: TComponent);
    function  GUIContext: IevContext;
    procedure AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure CheckTaskQueueStatus;
    procedure PrintTask(const ATaskID: TisGUID);
    procedure OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure OnWaitingServerResponse;
    procedure OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
    procedure PasswordChangeClose;
  public
    destructor  Destroy; override;
  end;

var
  vEvolutionGUI: IevEvolutionGUI;

function GetEvoDBMaintGUI: IevEvolutionGUI;
begin
  if not Assigned(vEvolutionGUI) then
    vEvolutionGUI := TevDBMaintGUI.Create as IevEvolutionGUI;
  Result := vEvolutionGUI;
end;


function GetContextCallbacks: IevContextCallbacks;
begin
  Result := GetEvoDBMaintGUI as IevContextCallbacks;
end;


{ TevDBMaintGUI }

procedure TevDBMaintGUI.ApplySecurity(const Component: TComponent);
var
  SecurityElement: ISecurityElement;
  i: Integer;
  State: TevSecElementState;
begin
  Component.GetInterface(ISecurityElement, SecurityElement);
  if Assigned(SecurityElement) and (SecurityElement.SGetTag <> '') then
  begin
    State := ctx_AccountRights.GetElementState(SecurityElement.SGetType[1], SecurityElement.SGetTag);
    SecurityElement.SSetCurrentSecurityState(State);
    SecurityElement := nil;
  end;

  for i := 0 to Component.ComponentCount-1 do
    ApplySecurity(Component.Components[i]);
end;

procedure TevDBMaintGUI.AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
begin
  // plug
end;

function TevDBMaintGUI.GUIContext: IevContext;
begin
  Result := FGUIContext;
end;

procedure TevDBMaintGUI.CreateMainForm;
var
  Frm: TForm;
  Password: String;
begin
  if not Assigned(FMainForm) then
  begin
    FGUIContext := Mainboard.ContextManager.GetThreadContext;

    CheckCondition(mb_GlobalSettings.DBUserPassword <> '', 'This utility should run from Request Broker folder with valid Evolution.cfg; check EUSER/SYSDBA passwords in Evo MC.');
    CheckCondition(mb_GlobalSettings.DBAdminPassword <> '', 'SYSDBA password is not set. Please run Management Console and check Evolution configuration.');    

    Password := '';
    if ISDialog(Application.Title, 'Please type SYSDBA password' , Password, True) then
    begin
      CheckCondition(Password = mb_GlobalSettings.DBAdminPassword, 'Password is incorrect');

      Application.ShowMainForm := False;
      Application.CreateForm(TevWizard, Frm);
      FMainForm := TevWizard(Frm);
      FMainForm.Show;
    end;
  end;
end;

destructor TevDBMaintGUI.Destroy;
begin
  Application.OnModalBegin := nil;
  Application.OnModalEnd := nil;
  Application.OnException := nil;

  FreeandNil(FWaitDialog);
  inherited;
end;

procedure TevDBMaintGUI.DestroyMainForm;
begin
  FreeAndNil(FMainForm);
  FGUIContext := nil;
end;

procedure TevDBMaintGUI.DoOnConstruction;
begin
  inherited;

  Application.OnModalBegin := OnShowModalDialog;
  Application.OnModalEnd := OnHideModalDialog;
  Application.OnException := OnException;

  FWaitDialog := TAdvancedWait.Create;
end;

procedure TevDBMaintGUI.EndWait;
begin
  if not UnAttended then
    FWaitDialog.EndWait;
end;

procedure TevDBMaintGUI.StartWait(const AText: string; AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TevDBMaintGUI.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

procedure TevDBMaintGUI.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TevDBMaintGUI.OnException(Sender: TObject; E: Exception);
begin
  FWaitDialog.KillWait;
  EvExceptMessage(E);
end;

procedure TevDBMaintGUI.OnHideModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.ShowIfHidden;
end;

procedure TevDBMaintGUI.OnShowModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.Hide;
end;

procedure TevDBMaintGUI.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TevDBMaintGUI.OnGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TevDBMaintGUI.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;

procedure TevDBMaintGUI.OnWaitingServerResponse;
begin
// a plug
end;

procedure TevDBMaintGUI.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TevDBMaintGUI.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
//a plug
end;

procedure TevDBMaintGUI.AddTaskQueue(const ATaskList: IisList;
  const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TevDBMaintGUI.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

procedure TevDBMaintGUI.PasswordChangeClose;
begin
// a plug
end;

initialization
  DisableProcessWindowsGhosting;
  Mainboard.ModuleRegister.RegisterModule(@GetEvoDBMaintGUI, IevEvolutionGUI, 'Evolution GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetContextCallbacks, IevContextCallbacks, 'Context Callbacks');

end.


