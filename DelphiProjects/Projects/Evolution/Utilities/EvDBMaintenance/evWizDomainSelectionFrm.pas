unit evWizDomainSelectionFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, evWizProcessFrm,
  ComCtrls;

type
  TevWizDomainSelection = class(TevWizardPage)
    isUIFashionPanel2: TisUIFashionPanel;
    lvDomainList: TISListView;
    procedure lvDomainListChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
  private
    FNextClass: TevWizardPageClass;
    FSelectedItem: TListItem;
    procedure BuildDomainList;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

{$R *.dfm}

{ TevWizDomainSelection }

function TevWizDomainSelection.GetNextPage: TevWizardPage;
begin
  FParams.AddValue('Domain', FSelectedItem.Caption);
  Result := FNextClass.Create(Owner, FParams);
end;

procedure TevWizDomainSelection.Init;
begin
  inherited;
  FNextClass := TevWizardPageClass(Pointer(Integer(FParams.Value['NextPageClass'])));
  FParams := TisListOfValues.Create;

  BuildDomainList;
end;

function TevWizDomainSelection.CanMoveForward: Boolean;
begin
  Result := Assigned(FSelectedItem);
end;

procedure TevWizDomainSelection.BuildDomainList;
var
  i: Integer;
  Item: TListItem;
begin
  lvDomainList.BeginUpdate;
  try
    for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
    begin
      Item := TListItem.Create(lvDomainList.Items);
      lvDomainList.Items.AddItem(Item);
      Item.Caption := mb_GlobalSettings.DomainInfoList[i].DomainName;
      Item.SubItems.Add(mb_GlobalSettings.DomainInfoList[i].DomainDescription);      
    end;
  finally
    lvDomainList.EndUpdate;
  end;

  lvDomainList.SortType := stText;
end;

procedure TevWizDomainSelection.lvDomainListChange(Sender: TObject; Item: TListItem; Change: TItemChange);
var
  i: Integer;
begin
  if Assigned(FSelectedItem) then
    FSelectedItem.Checked := False;

  FSelectedItem := nil;

  for i := 0 to lvDomainList.Items.Count - 1 do
    if lvDomainList.Items[i].Checked then
    begin
      FSelectedItem := lvDomainList.Items[i];
      break;
    end;

  NotifyChange;
end;

end.
