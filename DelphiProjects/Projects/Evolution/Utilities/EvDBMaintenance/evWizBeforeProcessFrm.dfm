inherited evWizBeforeProcess: TevWizBeforeProcess
  Width = 671
  Height = 540
  object isUIFashionPanel2: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 671
    Height = 540
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Please carefully check your selection'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      671
      540)
    object lThreadPerServ: TISLabel
      Left = 30
      Top = 356
      Width = 134
      Height = 16
      Anchors = [akLeft, akBottom]
      Caption = 'Threads per DB Server:'
    end
    object memDescription: TISMemo
      Left = 30
      Top = 56
      Width = 610
      Height = 289
      Anchors = [akLeft, akTop, akRight, akBottom]
      Ctl3D = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Courier New'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
      WantReturns = False
    end
    object chbCorrect: TISCheckBox
      Left = 30
      Top = 442
      Width = 227
      Height = 17
      Anchors = [akLeft, akBottom]
      Caption = 'This is what I want to do'
      TabOrder = 1
      OnClick = chbCorrectClick
    end
    object Panel1: TPanel
      Left = 24
      Top = 433
      Width = 600
      Height = 3
      Anchors = [akLeft, akBottom]
      TabOrder = 2
    end
  end
end
