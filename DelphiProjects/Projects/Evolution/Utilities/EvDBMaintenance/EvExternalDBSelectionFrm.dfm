inherited EvExternalDBSelection: TEvExternalDBSelection
  Left = 670
  Top = 247
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Database Selection'
  ClientHeight = 516
  ClientWidth = 437
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object isUIFashionPanel2: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 437
    Height = 472
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Available databases'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 6
    DesignSize = (
      437
      472)
    object ISLabel1: TISLabel
      Left = 26
      Top = 49
      Width = 47
      Height = 13
      Caption = 'DB Folder'
    end
    object btnRefresh: TISSpeedButton
      Left = 388
      Top = 45
      Width = 23
      Height = 22
      HideHint = True
      AutoSize = False
      OnClick = btnRefreshClick
      Anchors = [akTop, akRight]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFCFCFCF9F9F9F9F9F9FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFED4C9C3623720623720F9F9F9FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFCFCFCF9F9F97E472ABDA192FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCC7A796945431E0D0
        C7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF9F9F9FCFCFCFFFFFFFF
        FFFFFFFFFFFDFDFDD4B6A5A66137D0AE99FCFCFCFFFFFFFFFFFFFFFFFFFFFFFF
        FCFCFCF9F9F9B26B39F9F9F9FCFCFCFCFCFCF9F9F9F9F9F9D0A78AB26B39B26B
        39F9F9F9F9F9F9FCFCFCFFFFFFFCFCFCF9F9F9B87139B87139B87139F9F9F9F9
        F9F9B87139B87139B87139B87139B87139B87139B87139F9F9F9FCFCFCF9F9F9
        BD7839BD7839BD7839BD7839BD7839F9F9F9F9F9F9BD7839BD7839BD7839BD78
        39BD7839F9F9F9FCFCFCF9F9F9C27F39C27F39C27F39C27F39C27F39C27F39C2
        7F39F9F9F9F9F9F9C27F39C27F39C27F39F9F9F9FCFCFCFFFFFFFCFCFCF9F9F9
        F9F9F9C68739C68739DBB78AF9F9F9F9F9F9FCFCFCFCFCFCF9F9F9C68739F9F9
        F9FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCE2C49ACA8E39E4CBA5FDFDFDFF
        FFFFFFFFFFFFFFFFFCFCFCF9F9F9FCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FEFEFEEFE0C9CE9738E4C89AFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCE8D198D5A835F9F9F9FC
        FCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF9F9F9DAB533DAB533F1E8C8FEFEFEFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF9F9F9F9F9F9FC
        FCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentColor = False
      ShortCut = 0
    end
    object edFolder: TISEdit
      Left = 82
      Top = 46
      Width = 303
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      Text = 'minerva:/db/peru'
      OnChange = edFolderChange
    end
    object lvDBList: TISListView
      Left = 26
      Top = 79
      Width = 385
      Height = 365
      Anchors = [akLeft, akTop, akRight, akBottom]
      Checkboxes = True
      Columns = <
        item
          Caption = 'Database'
          Width = 300
        end
        item
          AutoSize = True
          Caption = 'Size'
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = pmSelect
      TabOrder = 1
      ViewStyle = vsReport
      OnChange = lvDBListChange
    end
  end
  object ISPanel1: TISPanel
    Left = 0
    Top = 472
    Width = 437
    Height = 44
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      437
      44)
    object btnCancel: TISButton
      Left = 348
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
      Color = clBlack
      ParentColor = False
      Margin = 0
    end
    object btnOk: TISButton
      Left = 259
      Top = 7
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 1
      Color = clBlack
      ParentColor = False
      Margin = 0
    end
  end
  object pmSelect: TISPopupMenu
    Left = 128
    Top = 144
    object miSelectAll: TMenuItem
      Caption = 'Select All'
      ShortCut = 16449
      OnClick = miSelectAllClick
    end
    object miUnselectAll: TMenuItem
      Caption = 'Unselect All'
      OnClick = miUnselectAllClick
    end
    object miReverseSelection: TMenuItem
      Caption = 'Reverse Selection'
      OnClick = miReverseSelectionClick
    end
  end
end
