unit evWizardPageFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isBaseClasses;

type
  TevWizardPage = class(TFrame)
  protected
    FParams: IisListOfValues;
    procedure InitParams; virtual;
  public
    procedure Init; virtual;
    function  GetNextPage: TevWizardPage; virtual;
    function  CanMoveForward: Boolean; virtual;
    procedure NotifyChange; virtual;
    function  NextButtonName: String; virtual;

    constructor Create(const AWizard: TComponent; const AParams: IisListOfValues); reintroduce;
  end;

  TevWizardPageClass = class of TevWizardPage;

implementation

{$R *.dfm}

uses evWizardFrm;

{ TevWizardPage }

function TevWizardPage.CanMoveForward: Boolean;
begin
  Result := True;
end;

function TevWizardPage.NextButtonName: String;
begin
  Result := 'Next';
end;

function TevWizardPage.GetNextPage: TevWizardPage;
begin
  Result := nil;
end;

procedure TevWizardPage.Init;
begin
  Update;
end;

procedure TevWizardPage.NotifyChange;
begin
  TevWizard(Owner).DoOnChange;
end;

constructor TevWizardPage.Create(const AWizard: TComponent; const AParams: IisListOfValues);
var
  i: Integer;
begin
  inherited Create(AWizard);
  Name := '';

  FParams := TisListOfValues.Create;
  InitParams;
  
  if Assigned(AParams) then
  begin
    for i := 0 to AParams.Count - 1 do
      FParams.AddValue(AParams[i].Name, AParams[i].Value);
  end;
end;

procedure TevWizardPage.InitParams;
begin
end;

end.
