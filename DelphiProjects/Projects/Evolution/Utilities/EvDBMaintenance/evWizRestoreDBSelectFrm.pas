unit evWizRestoreDBSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvContext, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, EvConsts,
  wwdblook, isUIwwDBLookupCombo, isUIEdit, LMDBaseControl, PBFolderDialog,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, evWizBeforeProcessFrm,
  EvCommonInterfaces, Menus, LMDCustomButton, LMDButton, isUILMDButton;

type
  TevWizRestoreDBSelect = class(TevWizardPage)
    pnlDBList: TisUIFashionPanel;
    lvDBList: TISListView;
    btnSelBackup: TISButton;
    odBackup: TOpenDialog;
    pmSelect: TISPopupMenu;
    miRemove: TMenuItem;
    miRemoveAll: TMenuItem;
    procedure rbUpgradeDBClick(Sender: TObject);
    procedure miSelectAllClick(Sender: TObject);
    procedure miUnselectAllClick(Sender: TObject);
    procedure miReverseSelectionClick(Sender: TObject);
    procedure btnSelBackupClick(Sender: TObject);
    procedure miRemoveClick(Sender: TObject);
    procedure miRemoveAllClick(Sender: TObject);
  private
    procedure UpdateSelection;
  protected
    procedure InitParams; override;
  public
    procedure Init; override;  
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

{$R *.dfm}

type
  TevFileListItem = class(TListItem)
  private
    function GetSizeMB: Cardinal;
  public
    constructor Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
    property SizeMB: Cardinal read GetSizeMB;
  end;

{ TevWizBackupDBSelect}

function TevWizRestoreDBSelect.GetNextPage: TevWizardPage;
var
  dbL: IisParamsCollection;
  s, dbs, db_info: String;
  i: Integer;
begin
  s := 'You are going to restore the backup files listed below to the database server(s)'#13#13'Backup files:'#13'%s';

  dbs := '';
  dbL := IInterface(FParams.Value['Backups']) as IisParamsCollection;
  for i := 0 to dbL.Count - 1 do
  begin
    db_info := dbL[i].Value['File'];
    dbs := dbs + db_info + #13;
  end;

  s := Format(s, [dbs]);

  FParams.Value['Description'] := s;

  Result := TevWizBeforeProcess.Create(Owner, FParams);
end;

function TevWizRestoreDBSelect.CanMoveForward: Boolean;
begin
  Result := (IInterface(FParams.Value['Backups']) as IisParamsCollection).Count > 0;
end;

procedure TevWizRestoreDBSelect.rbUpgradeDBClick(Sender: TObject);
begin
  NotifyChange;
end;

procedure TevWizRestoreDBSelect.InitParams;
var
  P: IisParamsCollection;
begin
  inherited;
  FParams.AddValue('Domain', sDefaultDomain);
  FParams.AddValue('Method', 'Restore Databases');
  P := TisParamsCollection.Create;
  FParams.AddValue('Backups', P);
  FParams.AddValue('Description', '');
end;

{ TevFileListItem }

constructor TevFileListItem.Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
begin
  inherited Create(AOwner);

  AOwner.AddItem(Self);
  Caption := AFileName;
  Data := Pointer(ASizeMB);
  SubItems.Add(SizeToString(ASizeMB, 'M'));
end;

procedure TevWizRestoreDBSelect.miSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := True;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizRestoreDBSelect.miUnselectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := False;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizRestoreDBSelect.miReverseSelectionClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := not lvDBList.Items[i].Checked;
  finally
    lvDBList.EndUpdate;  
  end;
end;

function TevFileListItem.GetSizeMB: Cardinal;
begin
  Result := Cardinal(Data);
end;

procedure TevWizRestoreDBSelect.btnSelBackupClick(Sender: TObject);
var
  i: Integer;
begin
  odBackup.Files.Clear;
  if odBackup.Execute then
  begin
    lvDBList.BeginUpdate;
    try
      for i := 0 to odBackup.Files.Count - 1 do
        TevFileListItem.Create(lvDBList.Items, odBackup.Files[i], Round(GetFileInfo(odBackup.Files[i]).Size /(1024 * 1024)));
    finally
      lvDBList.EndUpdate;
    end;

    UpdateSelection;
  end;
end;

procedure TevWizRestoreDBSelect.miRemoveClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := lvDBList.Items.Count - 1 downto 0 do
      if lvDBList.Items[i].Selected then
        lvDBList.Items.Delete(i);
  finally
    lvDBList.EndUpdate;
  end;
  UpdateSelection;
end;

procedure TevWizRestoreDBSelect.miRemoveAllClick(Sender: TObject);
begin
  lvDBList.Clear;
  UpdateSelection;
end;

procedure TevWizRestoreDBSelect.UpdateSelection;
var
  i: Integer;
  dbList: IisParamsCollection;
  dbInfo: IisListOfValues;
begin
  dbList := IInterface(FParams.Value['Backups']) as IisParamsCollection;
  dbList.Clear;

  for i := 0 to lvDBList.Items.Count - 1 do
  begin
    dbInfo := dbList.AddParams(lvDBList.Items[i].Caption);
    dbInfo.AddValue('File', lvDBList.Items[i].Caption);
    dbInfo.AddValue('SizeMB', TevFileListItem(lvDBList.Items[i]).GetSizeMB);
  end;
  NotifyChange;
end;

procedure TevWizRestoreDBSelect.Init;
begin
  inherited;

  if not AnsiSameText(FParams.Value['Domain'], sDefaultDomain) then
    pnlDBList.Title := pnlDBList.Title + ' (Domain: ' + FParams.Value['Domain'] + ')';
end;

end.
