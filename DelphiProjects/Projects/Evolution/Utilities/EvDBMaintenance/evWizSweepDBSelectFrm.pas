unit evWizSweepDBSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvContext, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, EvConsts,
  wwdblook, isUIwwDBLookupCombo, isUIEdit, LMDBaseControl, PBFolderDialog,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, evWizBeforeProcessFrm,
  EvCommonInterfaces, Menus, EvBasicUtils;

type
  TevWizSweepDBSelect = class(TevWizardPage)
    pnlDBList: TisUIFashionPanel;
    lvDBList: TISListView;
    pmSelect: TISPopupMenu;
    miSelectAll: TMenuItem;
    miUnselectAll: TMenuItem;
    miReverseSelection: TMenuItem;
    lCaption: TISLabel;
    procedure rbUpgradeDBClick(Sender: TObject);
    procedure lvDBListChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure miSelectAllClick(Sender: TObject);
    procedure miUnselectAllClick(Sender: TObject);
    procedure miReverseSelectionClick(Sender: TObject);
    procedure lvDBListCreateItemClass(Sender: TCustomListView;
      var ItemClass: TListItemClass);
  private
    procedure BuildDBList;
  protected
    procedure InitParams; override;
  public
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

{$R *.dfm}

type
  TevDBListItem = class(TListItem)
  private
    function GetSizeMB: Cardinal;
  public
    constructor Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
    property SizeMB: Cardinal read GetSizeMB;
  end;

{ TevWizBackupDBSelect}

function TevWizSweepDBSelect.GetNextPage: TevWizardPage;
var
  dbList: IisParamsCollection;
  s, dbs: String;
  i: Integer;
begin
  s := 'You are going to sweep the databases listed below:'#13#13'%s';

  dbs := '';
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  for i := 0 to dbList.Count - 1 do
    dbs := dbs + dbList[i].Value['DB'] + #13;

  FParams.Value['Description'] := Format(s, [dbs]);

  Result := TevWizBeforeProcess.Create(Owner, FParams);
end;

procedure TevWizSweepDBSelect.Init;
begin
  inherited;

  if not AnsiSameText(FParams.Value['Domain'], sDefaultDomain) then
    pnlDBList.Title := pnlDBList.Title + ' (Domain: ' + FParams.Value['Domain'] + ')';

  BuildDBList;
end;

function TevWizSweepDBSelect.CanMoveForward: Boolean;
begin
  Result := (IInterface(FParams.Value['Databases']) as IisParamsCollection).Count > 0;
end;

procedure TevWizSweepDBSelect.rbUpgradeDBClick(Sender: TObject);
begin
  NotifyChange;
end;

procedure TevWizSweepDBSelect.BuildDBList;
var
  Items: IisStringList;

  procedure BuildForPath(const APath: String);
  var
    s, host: String;
    DS: IevDataSet;
  begin
    s := APath;
    host := GetNextStrValue(s, ':');
    if s = '' then
      Exit;

    DS := ctx_DBAccess.GetDBServerFolderContent(APath, FB_Admin, mb_GlobalSettings.DBAdminPassword);

    DS.IndexFieldNames := 'file_name';
    DS.First;
    while not DS.Eof do
    begin
      s := DS.FieldByName('file_name').AsString;
      if not StartsWith(s, DB_Service) and not StartsWith(s, DB_Cl_Base) and FinishesWith(s, '.gdb', True) and (Items.IndexOf(s) = -1) then
      begin
        TevDBListItem.Create(lvDBList.Items, s, DS.FieldByName('file_size').Value div (1024 * 1024));
        Items.Add(s);
      end;

      DS.Next;
    end;
  end;


var
  i: Integer;
begin
  Mainboard.ContextManager.StoreThreadContext;
  try
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, FParams.Value['Domain']), '');

    Items := TisStringList.CreateAsIndex;

    lvDBList.BeginUpdate;
    try
      for i := 0 to ctx_DomainInfo.DBLocationList.Count - 1 do
        BuildForPath(ctx_DomainInfo.DBLocationList[i].Path);
    finally
      lvDBList.EndUpdate;
    end;

    lvDBList.SortType := stText;
  finally
    Mainboard.ContextManager.RestoreThreadContext;
  end;
end;

procedure TevWizSweepDBSelect.InitParams;
begin
  inherited;
  FParams.AddValue('Domain', sDefaultDomain);
  FParams.AddValue('Method', 'Sweep Databases');
  FParams.AddValue('Databases', TisParamsCollection.Create as IisParamsCollection);
  FParams.AddValue('Description', '');
end;


{ TevDBListItem }

constructor TevDBListItem.Create(const AOwner: TListItems; const AFileName: String; const ASizeMB: Cardinal);
begin
  inherited Create(AOwner);

  AOwner.AddItem(Self);
  Caption := AFileName;
  Data := Pointer(ASizeMB);
  SubItems.Add(SizeToString(ASizeMB, 'M'));
end;

procedure TevWizSweepDBSelect.lvDBListChange(Sender: TObject;
  Item: TListItem; Change: TItemChange);
var
  i: Integer;
  dbList: IisParamsCollection;
  dbInfo: IisListOfValues;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;

  dbList.Clear;

  for i := 0 to lvDBList.Items.Count - 1 do
    if lvDBList.Items[i].Checked then
    begin
      dbInfo := dbList.AddParams(lvDBList.Items[i].Caption);
      dbInfo.AddValue('DB', lvDBList.Items[i].Caption);
      dbInfo.AddValue('SizeMB', TevDBListItem(lvDBList.Items[i]).SizeMB);
    end;

  NotifyChange;
end;

procedure TevWizSweepDBSelect.miSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := True;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizSweepDBSelect.miUnselectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := False;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizSweepDBSelect.miReverseSelectionClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := not lvDBList.Items[i].Checked;
  finally
    lvDBList.EndUpdate;  
  end;
end;

procedure TevWizSweepDBSelect.lvDBListCreateItemClass(
  Sender: TCustomListView; var ItemClass: TListItemClass);
begin
  ItemClass := TevDBListItem;
end;

function TevDBListItem.GetSizeMB: Cardinal;
begin
  Result := Cardinal(Data);
end;

end.
