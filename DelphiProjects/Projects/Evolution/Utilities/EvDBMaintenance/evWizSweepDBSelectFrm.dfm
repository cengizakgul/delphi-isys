inherited evWizSweepDBSelect: TevWizSweepDBSelect
  Width = 457
  Height = 501
  object pnlDBList: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 457
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Databases'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      457
      501)
    object lCaption: TISLabel
      Left = 30
      Top = 45
      Width = 227
      Height = 16
      Caption = 'Selected databases you want to sweep'
    end
    object lvDBList: TISListView
      Left = 30
      Top = 72
      Width = 395
      Height = 343
      Anchors = [akLeft, akTop, akRight, akBottom]
      Checkboxes = True
      Columns = <
        item
          Caption = 'Database'
          Width = 150
        end
        item
          Caption = 'Size'
          Width = 70
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = pmSelect
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = lvDBListChange
      OnCreateItemClass = lvDBListCreateItemClass
    end
  end
  object pmSelect: TISPopupMenu
    Left = 120
    Top = 104
    object miSelectAll: TMenuItem
      Caption = 'Select All'
      ShortCut = 16449
      OnClick = miSelectAllClick
    end
    object miUnselectAll: TMenuItem
      Caption = 'Unselect All'
      OnClick = miUnselectAllClick
    end
    object miReverseSelection: TMenuItem
      Caption = 'Reverse Selection'
      OnClick = miReverseSelectionClick
    end
  end
end
