// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvExternalDBSelectionFrm;

interface

uses Windows, Messages, Forms, Controls, ExtCtrls, ISBasicClasses, isUIFashionPanel, Classes,
     ComCtrls, isBasicUtils, evConsts, StdCtrls, Graphics, IsUtils, Dialogs,
     Mask, wwdbedit, Wwdbspin, LMDCustomButton, LMDButton, isUILMDButton, EvCommonInterfaces,
     LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton, EvContext, isBaseClasses,
     LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, isUIEdit, EvFormFrm,
     SysUtils, DB, Menus;

type
  TEvExternalDBSelection = class(TevForm)
    isUIFashionPanel2: TisUIFashionPanel;
    ISPanel1: TISPanel;
    ISLabel1: TISLabel;
    edFolder: TISEdit;
    btnRefresh: TISSpeedButton;
    lvDBList: TISListView;
    btnOk: TISButton;
    btnCancel: TISButton;
    pmSelect: TISPopupMenu;
    miSelectAll: TMenuItem;
    miUnselectAll: TMenuItem;
    miReverseSelection: TMenuItem;
    procedure btnRefreshClick(Sender: TObject);
    procedure edFolderChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miSelectAllClick(Sender: TObject);
    procedure miUnselectAllClick(Sender: TObject);
    procedure miReverseSelectionClick(Sender: TObject);
    procedure lvDBListChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
  private
    FCurrHostPass: String;
    FCurrPath: String;
    procedure Refresh;
    procedure UpdateOKButton;
  public
    function GetSelection: IisParamsCollection;
  end;

implementation

{$R *.DFM}

type
  TevDBListItem = class(TListItem)
  private
    function GetFileSizeMB: Cardinal;
  public
    constructor Create(const AOwner: TListItems; const ADBName: String; const ASizeMB: Cardinal);
    property FileSizeMB: Cardinal read GetFileSizeMB;
  end;

var
  FHostPasswords: IisListOfValues;

  { TevDBListItem }

constructor TevDBListItem.Create(const AOwner: TListItems; const ADBName: String; const ASizeMB: Cardinal);
begin
  inherited Create(AOwner);

  AOwner.AddItem(Self);
  Caption := ADBName;
  Data := Pointer(ASizeMB);
  SubItems.Add(SizeToString(ASizeMB, 'M'));
end;

function TevDBListItem.GetFileSizeMB: Cardinal;
begin
  Result := Cardinal(Data);
end;


{ TEvDBImportParamsForm }

procedure TEvExternalDBSelection.Refresh;
var
  s, host: String;
  DS: IevDataSet;
begin
  s := edFolder.Text;
  host := GetNextStrValue(s, ':');
  if s = '' then
    Exit;

  if FHostPasswords.ValueExists(host) then
    FCurrHostPass := FHostPasswords.Value[host]
  else
    FCurrHostPass := '';

  if FCurrHostPass = '' then
    ISDialog('Password', 'Please type SYSDBA password for server ' + host, FCurrHostPass, True);

  if FCurrHostPass = '' then
    Exit;

  FCurrPath := Trim(edFolder.Text);
  DS := ctx_DBAccess.GetDBServerFolderContent(FCurrPath, FB_Admin, FCurrHostPass);

  FHostPasswords.AddValue(host, FCurrHostPass);  

  DS.IndexFieldNames := 'file_name';
  DS.First;

  lvDBList.BeginUpdate;
  try
    while not DS.Eof do
    begin
      s := DS.FieldByName('file_name').AsString;
      if not StartsWith(s, DB_Service) and FinishesWith(s, '.gdb', True) then
        TevDBListItem.Create(lvDBList.Items, s, DS.FieldByName('file_size').Value div (1024 * 1024));

      DS.Next;
    end;
  finally
    lvDBList.EndUpdate;
  end;
end;

procedure TEvExternalDBSelection.btnRefreshClick(Sender: TObject);
begin
  Refresh;
end;

procedure TEvExternalDBSelection.edFolderChange(Sender: TObject);
begin
  lvDBList.Clear;
end;

procedure TEvExternalDBSelection.FormCreate(Sender: TObject);
begin
  btnOk.Enabled := False;
end;

function TEvExternalDBSelection.GetSelection: IisParamsCollection;
var
  i: Integer;
  Par: IisListOfValues;
begin
  Result := TisParamsCollection.Create;

  for i := 0 to lvDBList.Items.Count - 1 do
    if lvDBList.Items[i].Checked then
    begin
      Par := Result.AddParams(IntToStr(i));
      Par.AddValue('DB', NormalizePath(FCurrPath) + lvDBList.Items[i].Caption);
      Par.AddValue('SizeMB', TevDBListItem(lvDBList.Items[i]).GetFileSizeMB);
      Par.AddValue('Password', FCurrHostPass);
    end;
end;

procedure TEvExternalDBSelection.miSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := True;
  finally
    lvDBList.EndUpdate;
  end;
end;

procedure TEvExternalDBSelection.miUnselectAllClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := False;
  finally
    lvDBList.EndUpdate;
  end;
end;

procedure TEvExternalDBSelection.miReverseSelectionClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := 0 to lvDBList.Items.Count - 1 do
      lvDBList.Items[i].Checked := not lvDBList.Items[i].Checked;
  finally
    lvDBList.EndUpdate;
  end;
end;

procedure TEvExternalDBSelection.lvDBListChange(Sender: TObject; Item: TListItem; Change: TItemChange);
begin
  UpdateOKButton;
end;

procedure TEvExternalDBSelection.UpdateOKButton;
var
  i: Integer;
begin
  btnOk.Enabled := False;
  for i := 0 to lvDBList.Items.Count - 1 do
    if lvDBList.Items[i].Checked then
    begin
      btnOk.Enabled := True;
      break;
    end;
end;

initialization
  FHostPasswords := TisListOfValues.Create;

end.
