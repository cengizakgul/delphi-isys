unit evWizUpgradeDBFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Types,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvContext, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, EvConsts,
  wwdblook, isUIwwDBLookupCombo, isUIEdit, LMDBaseControl, PBFolderDialog,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, evWizBeforeProcessFrm, isUtils,
  EvCommonInterfaces, Menus, LMDCustomButton, LMDButton, isUILMDButton,
  EvUtils, EvTypes, EvDBVersion, EvUIComponents, isTypes, OnGuard, SEncryptionRoutines;

type
  TevWizUpgradeDB = class(TevWizardPage)
    isUIFashionPanel2: TisUIFashionPanel;
    chbSweep: TISCheckBox;
    lDetails: TISLabel;
    Label1: TLabel;
    cbVersions: TevComboBox;
    Label2: TLabel;
    Label3: TLabel;
    edMachineID: TEdit;
    CopyButton: TButton;
    edDowngradeKey: TEdit;
    btnVerifyKey: TButton;
    chbDowngrade: TISCheckBox;
    procedure chbSweepClick(Sender: TObject);
    procedure cbVersionsChange(Sender: TObject);
    procedure CopyButtonClick(Sender: TObject);
    procedure btnVerifyKeyClick(Sender: TObject);
    procedure chbDowngradeClick(Sender: TObject);
  private
    FVerList: TevAppDBVerInfos;
  protected
    procedure InitParams; override;
  public
    procedure Init; override;
    function  GetNextPage: TevWizardPage; override;
    function  CanMoveForward: Boolean; override;
  end;

implementation

{$R *.dfm}

{ TevWizImportDBSelect}

procedure TevWizUpgradeDB.InitParams;
begin
  inherited;
  FParams.AddValue('Method', 'Patch Databases');
  FParams.AddValue('Sweep', True);
  FParams.AddValue('AllowDowngrade', False);
  FParams.AddValue('AppVersion', '');
  FParams.AddValue('AppVersionRange', '');
  FParams.AddValue('Description', '');
end;

function TevWizUpgradeDB.GetNextPage: TevWizardPage;
var
  s, DBList: String;
  i: Integer;
  DBPathList: IisListOfValues;

  procedure AddLoc(const ADBLocList: IevDBLocationList; const ADomainName: String);

    procedure AddDB(APath, ADBName: String);
    var
      V: IisNamedValue;
    begin
      if APath = '' then Exit;

      APath := DenormalizePath(NormalizePath(APath));

      V := DBPathList.FindValue(APath);
      if Assigned(V) then
        V.Value := V.Value + '  ' + ADBName
      else
        DBPathList.AddValue(APath, ADBName);
    end;

  var
    i: Integer;
    max_cl_nbr: Cardinal;
  begin
    if ADBLocList.Count = 0 then exit;

    DBPathList.Clear;

    max_cl_nbr := 0;
    for i := 0 to ADBLocList.Count - 1 do
    begin
      case ADBLocList[i].DBType of
        dbtClient:
        begin
          AddDB(ADBLocList[i].Path, Format('CL_%d...CL_%d  CL_BASE', [ADBLocList[i].BeginRangeNbr, ADBLocList[i].EndRangeNbr]));
          if max_cl_nbr < ADBLocList[i].EndRangeNbr then
            max_cl_nbr := ADBLocList[i].EndRangeNbr;
        end;
      end;
    end;

    for i := 0 to ADBLocList.Count - 1 do
    begin
      case ADBLocList[i].DBType of
        dbtUnspecified:
          AddDB(ADBLocList[i].Path, Format('CL_%d...  CL_BASE', [max_cl_nbr + 1]));
      end;
    end;

    if IsDebugMode then
      AddDB(ExtractFilePathUD(ADBLocList.GetDBPath(DB_System)), 'SYSTEM');

    AddDB(ExtractFilePathUD(ADBLocList.GetDBPath(DB_S_Bureau)), 'S_BUREAU');
    AddDB(ExtractFilePathUD(ADBLocList.GetDBPath(DB_TempTables)), 'TMP_TBLS');

    if DBPathList.Count > 0 then
    begin
      DBList := DBList + #13#13 + 'Evo Domain: ' + ADomainName;
      for i := 0 to DBPathList.Count - 1 do
        DBList := DBList + #13 +  DBPathList[i].Name + '    ' + DBPathList[i].Value;
    end;
  end;

var
  reqVer: TevAppDBVerInfo;

begin
  DBList := '';
  DBPathList := TisListOfValues.Create;

  for i := 0 to mb_GlobalSettings.DomainInfoList.Count - 1 do
  begin
    AddLoc(mb_GlobalSettings.DomainInfoList[i].DBLocationList, mb_GlobalSettings.DomainInfoList[i].DomainName);
    AddLoc(mb_GlobalSettings.DomainInfoList[i].ADRDBLocationList, mb_GlobalSettings.DomainInfoList[i].DomainName + ' (ADR)');
  end;

  if FParams.Value['AppVersion'] = '' then
    reqVer := AppDBVersions.Current
  else
    reqVer := AppDBVersions.GetCompatibilityInfo(FParams.Value['AppVersion']);

  if IsDebugMode then
    s := 'SYSTEM.gdb     ' +  VersionRecToStr(reqVer.SystemDBVersion) + #13;

  FParams.Value['Description'] :=
    'The utility is going to check the version of all databases in this environment ' +
    'and patch ONLY those databases, that have a different version than Evolution requires. ' +
    'This utility cannot patch databases which version is out of scope.'#13#13 +
    'Required version information: '#13#13 +
    Format(
      'Application    %s'#13 +
      s +
      'S_BUREAU.gdb   %s'#13 +
      'CL_*.gdb       %s'#13 +
      'TMP_TBLS.gdb   %s'#13 +
      DBList + #13#13 +
      'Sweep after  update: %s',
        [FParams.Value['AppVersionRange'],
         VersionRecToStr(reqVer.BureauDBVersion),
         VersionRecToStr(reqVer.ClientDBVersion),
         VersionRecToStr(reqVer.TempDBVersion),
         IIF(FParams.Value['Sweep'], 'yes', 'no')]);

  if FParams.Value['AllowDowngrade'] then
    FParams.Value['Description'] := FParams.Value['Description'] + #13#13 +
      '!!!WARNING!!! You selected an option to downgrade databases to a prior version. ' +
      'Please note that iSystems does not recommend rolling back database changes except in very limited situations.';

  Result := TevWizBeforeProcess.Create(Owner, FParams);
end;

procedure TevWizUpgradeDB.Init;
var
  PatchList: IisStringList;

  function DecVersion(const AVer: TisVersionInfo): TisVersionInfo;
  begin
    Result := AVer;
    if Result.Build > 1 then
      Result.Build := Result.Build - 1
    else if Result.Patch > 1 then
    begin
      Result.Build := -1;
      Result.Patch := Result.Patch - 1
    end
    else if Result.Minor > 1 then
    begin
      Result.Build := -1;
      Result.Patch := -1;
      Result.Minor := Result.Minor - 1
    end
    else
    begin
      Result.Build := -1;
      Result.Patch := -1;
      Result.Major := -1;
      Result.Major := Result.Major - 1;
    end
  end;

  function VerToStr(const AVer: TisVersionInfo): String;
  begin
    Result := StringReplace(VersionRecToStr(AVer), '-1', 'X', [rfReplaceAll]);
  end;

  procedure BuildPatchList(const ADBType: String);
  var
    L: IisStringList;
    i: Integer;
    s: String;
  begin
    L := GetResourceList('D_' + ADBType + '_DB');
    for i := 0 to L.Count - 1 do
    begin
      s := VersionRecToStr(StrToVersionRec(StringReplace(L[i], '_', '.', [rfReplaceAll])));
      PatchList.Add(ADBType + s);
    end;
  end;

  function PatchExists(const ADBType: String; const AVersion: TisVersionInfo): Boolean;
  begin
    Result := PatchList.IndexOf(ADBType + VerToStr(AVersion)) <> -1;
  end;

var
  tmpList: TevAppDBVerInfos;
  i: Integer;
  s: String;
  MaxVer: TisVersionInfo;
  bPatchExists: Boolean;
  N: Cardinal;
begin
  inherited;

  N := Cardinal(CreateMachineID([midUser, midSystem, midNetwork, midDrives]));
  edMachineID.Text := BufferToHex(N, SizeOf(N));

  chbSweep.Checked := FParams.Value['Sweep'];
  chbDowngrade.Checked := FParams.Value['AllowDowngrade'];

  PatchList :=  TisStringList.CreateUnique;
  BuildPatchList('SYSTEM');
  BuildPatchList('BUREAU');
  BuildPatchList('CLIENT');
  BuildPatchList('TEMP');

  cbVersions.Clear;

  // System DB also contains data which cannot be downgraded.
  // So let's filter out System DB changes
  FVerList := AppDBVersions.GetAllInfo;
  SetLength(tmpList, 1);
  tmpList[0] := FVerList[0];
  for i := 1 to High(FVerList) do
  begin
    if (CompareVersions(FVerList[i].BureauDBVersion, tmpList[High(tmpList)].BureauDBVersion) <> EqualsValue) or
       (CompareVersions(FVerList[i].ClientDBVersion, tmpList[High(tmpList)].ClientDBVersion) <> EqualsValue) or
       (CompareVersions(FVerList[i].TempDBVersion, tmpList[High(tmpList)].TempDBVersion) <> EqualsValue) then
    begin
      SetLength(tmpList, Length(tmpList) + 1);
      tmpList[High(tmpList)] := FVerList[i];
    end;
  end;
  FVerList := tmpList;

  for i := High(FVerList) downto Low(FVerList) do
  begin
    if i = High(FVerList) then
      MaxVer := StrToVersionRec(AppVersion)
    else
    begin
      if IsQAMode then
        bPatchExists := PatchExists('SYSTEM', FVerList[i].SystemDBVersion)
      else
        bPatchExists := False;

      bPatchExists := bPatchExists or PatchExists('BUREAU', FVerList[i].BureauDBVersion) or
        PatchExists('CLIENT', FVerList[i].ClientDBVersion) or PatchExists('TEMP', FVerList[i].TempDBVersion);

      if not bPatchExists then
        break;

      MaxVer := DecVersion(FVerList[i + 1].AppMinVersion);
    end;

    s := VerToStr(FVerList[i].AppMinVersion);
    if (i < High(FVerList)) or (CompareVersions(FVerList[i].AppMinVersion, MaxVer) = LessThanValue) then
      s := s + ' - ' + VerToStr(MaxVer);

    if i = High(FVerList) then
      s := s + ' (Current)';

    cbVersions.AddItem(s, Pointer(i));
  end;
  cbVersions.ItemIndex := 0;
  cbVersionsChange(nil);
end;

procedure TevWizUpgradeDB.chbSweepClick(Sender: TObject);
begin
  FParams.Value['Sweep'] := chbSweep.Checked;
end;

procedure TevWizUpgradeDB.cbVersionsChange(Sender: TObject);
var
  i: Integer;
begin
  i := Integer(cbVersions.Items.Objects[cbVersions.ItemIndex]);
  if i < High(FVerList) then
    FParams.Value['AppVersion'] := VersionRecToStr(FVerList[i].AppMinVersion)
  else
    FParams.Value['AppVersion'] :=  '';

  FParams.Value['AppVersionRange'] :=  cbVersions.Text;
end;

procedure TevWizUpgradeDB.CopyButtonClick(Sender: TObject);
begin
  edMachineID.SelectAll;
  edMachineID.CopyToClipboard;
end;

procedure TevWizUpgradeDB.btnVerifyKeyClick(Sender: TObject);
var
  S: String;
begin
  inherited;
  FParams.Value['AllowDowngrade'] := False;
  if edDowngradeKey.Text = '' then
    Exit;
  S := DecryptHexString(edDowngradeKey.Text);
  if Copy(S, 1, Length(edMachineID.Text)) = edMachineID.Text then
  begin
    S := Copy(S, Length(edMachineID.Text) + 1, Length(S));
    if StrToDate(S) = Date then
    begin
      ShowMessage('License Key valid. You can proceed with DB downgrade.');
      FParams.Value['AllowDowngrade'] := True;
    end
    else
      ShowMessage('License Key has expired!');
  end
  else
    ShowMessage('License Key is invalid as it corresponds to a different modifier!');
    
  NotifyChange;
end;

procedure TevWizUpgradeDB.chbDowngradeClick(Sender: TObject);
begin
  edMachineID.Enabled := chbDowngrade.Checked;
  CopyButton.Enabled := chbDowngrade.Checked;
  edDowngradeKey.Enabled := chbDowngrade.Checked;
  btnVerifyKey.Enabled := chbDowngrade.Checked;
  NotifyChange;
end;

function TevWizUpgradeDB.CanMoveForward: Boolean;
begin
  Result := not chbDowngrade.Checked or (chbDowngrade.Checked and FParams.Value['AllowDowngrade']);
end;

end.
