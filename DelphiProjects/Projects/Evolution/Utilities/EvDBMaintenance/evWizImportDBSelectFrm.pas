unit evWizImportDBSelectFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evWizardPageFrm, StdCtrls, ExtCtrls, isBasicUtils, EvContext, EvMainboard,
  isBaseClasses, isUIFashionPanel, ISBasicClasses, ComCtrls, EvConsts,
  wwdblook, isUIwwDBLookupCombo, isUIEdit, LMDBaseControl, PBFolderDialog,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, evWizBeforeProcessFrm,
  EvCommonInterfaces, Menus, LMDCustomButton, LMDButton, isUILMDButton,
  EvExternalDBSelectionFrm, EvBasicUtils;

type
  TevWizImportDBSelect = class(TevWizardPage)
    pnlDBList: TisUIFashionPanel;
    lvDBList: TISListView;
    lSelectedSize: TISLabel;
    btnSelDB: TISButton;
    pmSelect: TISPopupMenu;
    miRemove: TMenuItem;
    miRemoveAll: TMenuItem;
    procedure lvDBListCreateItemClass(Sender: TCustomListView;
      var ItemClass: TListItemClass);
    procedure btnSelDBClick(Sender: TObject);
    procedure miRemoveClick(Sender: TObject);
    procedure miRemoveAllClick(Sender: TObject);
  private
    FDBSelForm: TEvExternalDBSelection;
    procedure UpdateSelection;
  protected
    procedure InitParams; override;
  public
    procedure AfterConstruction; override;
    procedure Init; override;
    function  CanMoveForward: Boolean; override;
    function  GetNextPage: TevWizardPage; override;
  end;

implementation

{$R *.dfm}

type
  TevDBItemData = record
    DBSizeMB: Cardinal;
    Password: String;
  end;

  PTevDBItemData = ^TevDBItemData;

  TevDBListItem = class(TListItem)
  private
    function GetExtData: TevDBItemData;
  public
    constructor Create(const AOwner: TListItems; const ASrcDBName: String; const APassword: String; const ASizeMB: Cardinal);
    destructor Destroy; override;
    property ExtData: TevDBItemData read GetExtData;
  end;


{ TevDBListItem }

constructor TevDBListItem.Create(const AOwner: TListItems; const ASrcDBName: String; const APassword: String; const ASizeMB: Cardinal);
var
  R: PTevDBItemData;
begin
  inherited Create(AOwner);


  AOwner.AddItem(Self);
  Caption := ASrcDBName;

  New(R);
  R.DBSizeMB := ASizeMB;
  R.Password := APassword;
  Data := R;

  SubItems.Add(ctx_DomainInfo.DBLocationList.GetDBPath(ExtractFileNameUD(ASrcDBName)));
  SubItems.Add(SizeToString(ASizeMB, 'M'));
end;


destructor TevDBListItem.Destroy;
begin
  Dispose(PTevDBItemData(Data));
  inherited;
end;

function TevDBListItem.GetExtData: TevDBItemData;
begin
  Result := PTevDBItemData(Data)^;
end;


{ TevWizImportDBSelect}

function TevWizImportDBSelect.GetNextPage: TevWizardPage;
var
  dbL: IisParamsCollection;
  s, dbs: String;
  i: Integer;
begin
  s := 'You are going to import the databases listed below:'#13#13'%s';
  dbs := '';
  dbL := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  for i := 0 to dbL.Count - 1 do
    dbs := dbs + dbL[i].Value['SrcDB'] + '   --->   ' + dbL[i].Value['DstDB'] + #13;

  FParams.Value['Description'] := Format(s, [dbs]);

  Result := TevWizBeforeProcess.Create(Owner, FParams);
end;

procedure TevWizImportDBSelect.Init;
begin
  inherited;

  if not AnsiSameText(FParams.Value['Domain'], sDefaultDomain) then
    pnlDBList.Title := pnlDBList.Title + ' (Domain: ' + FParams.Value['Domain'] + ')';

  UpdateSelection;
end;

function TevWizImportDBSelect.CanMoveForward: Boolean;
begin
  Result := (IInterface(FParams.Value['Databases']) as IisParamsCollection).Count > 0;
end;

procedure TevWizImportDBSelect.AfterConstruction;
begin
  inherited;
  FDBSelForm := TEvExternalDBSelection.Create(Self);
end;

procedure TevWizImportDBSelect.UpdateSelection;
var
  i: Integer;
  dbList: IisParamsCollection;
  SelectedSizeMB: Int64;
  dbInfo: IisListOfValues;
begin
  dbList := IInterface(FParams.Value['Databases']) as IisParamsCollection;
  dbList.Clear;
  SelectedSizeMB := 0;

  for i := 0 to lvDBList.Items.Count - 1 do
  begin
    dbInfo := dbList.AddParams(lvDBList.Items[i].Caption);
    dbInfo.AddValue('SrcDB', lvDBList.Items[i].Caption);
    dbInfo.AddValue('DstDB', lvDBList.Items[i].SubItems[0]);
    dbInfo.AddValue('Password', TevDBListItem(lvDBList.Items[i]).ExtData.Password);
    dbInfo.AddValue('SizeMB', TevDBListItem(lvDBList.Items[i]).ExtData.DBSizeMB);

    SelectedSizeMB := SelectedSizeMB + TevDBListItem(lvDBList.Items[i]).ExtData.DBSizeMB;
  end;

  if SelectedSizeMB > 0 then
    lSelectedSize.Caption := Format('Selected %d databases of total size %s', [dbList.Count, SizeToString(SelectedSizeMB, 'M')])
  else
    lSelectedSize.Caption := '';

  NotifyChange;    
end;

procedure TevWizImportDBSelect.lvDBListCreateItemClass(
  Sender: TCustomListView; var ItemClass: TListItemClass);
begin
  ItemClass := TevDBListItem;
end;

procedure TevWizImportDBSelect.btnSelDBClick(Sender: TObject);
var
  DBList: IisParamsCollection;
  i: Integer;
  dbL: IisParamsCollection;
begin
  if FDBSelForm.ShowModal = mrOk then
  begin
    DBList := FDBSelForm.GetSelection;
    dbL := IInterface(FParams.Value['Databases']) as IisParamsCollection;

    lvDBList.BeginUpdate;
    try
      Mainboard.ContextManager.StoreThreadContext;
      try
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, FParams.Value['Domain']), '');

        for i := 0 to DBList.Count - 1 do
        begin
          if dbL.IndexOf(DBList[i].Value['DB']) = -1 then
            TevDBListItem.Create(lvDBList.Items, DBList[i].Value['DB'], DBList[i].Value['Password'], DBList[i].Value['SizeMB']);
        end;
      finally
        Mainboard.ContextManager.RestoreThreadContext;
      end;
    finally
      lvDBList.EndUpdate;
    end;

    UpdateSelection;
  end;
end;

procedure TevWizImportDBSelect.miRemoveClick(Sender: TObject);
var
  i: Integer;
begin
  lvDBList.BeginUpdate;
  try
    for i := lvDBList.Items.Count - 1 downto 0 do
      if lvDBList.Items[i].Selected then
        lvDBList.Items.Delete(i);
  finally
    lvDBList.EndUpdate;
  end;

  UpdateSelection;
end;

procedure TevWizImportDBSelect.miRemoveAllClick(Sender: TObject);
begin
  lvDBList.Clear;
  UpdateSelection;
end;

procedure TevWizImportDBSelect.InitParams;
begin
  inherited;
  FParams.AddValue('Domain', sDefaultDomain);
  FParams.AddValue('Method', 'Import Databases');
  FParams.AddValue('Databases', TisParamsCollection.Create as IisParamsCollection);
  FParams.AddValue('Description', '');
end;

end.
