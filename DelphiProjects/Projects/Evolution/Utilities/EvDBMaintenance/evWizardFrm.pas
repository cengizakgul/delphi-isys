unit evWizardFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, evFormFrm, ExtCtrls, StdCtrls, jpeg, isBasicUtils, isBaseClasses,
  evWizardPageFrm, isUtils, ComCtrls, LMDCustomButton, LMDButton,
  isUILMDButton, ISBasicClasses;

type
  TevWizard = class(TevForm)
    imPict: TImage;
    pnlPages: TPanel;
    ISPanel1: TISPanel;
    btnBack: TISButton;
    btnNext: TISButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
  private
    FHist: TList;
    FCurrentPage: TevWizardPage;
    procedure ShowPage(const APage: TevWizardPage);
    procedure GoToNextPage;
    procedure GoToPreviousPage;
    procedure SyncButtons;
    procedure Init;
  public
    procedure DoOnChange;
    procedure ClearHistory;
  end;

implementation

{$R *.dfm}

uses evWizModeSelectFrm;

{ TevWizard }

procedure TevWizard.Init;
begin
  FCurrentPage := TevWizModeSelect.Create(Self, nil);
  try
    FCurrentPage.Init;
  finally
    ShowPage(FCurrentPage);
    SyncButtons;
  end;
end;

procedure TevWizard.ShowPage(const APage: TevWizardPage);
begin
  APage.Align := alClient;
  APage.Parent := pnlPages;
  APage.Show;
end;

procedure TevWizard.FormCreate(Sender: TObject);
begin
  inherited;
  Caption := Application.Title + ' (v '+ AppVersion + ')';
  FHist := TList.Create;
  Init;
end;

procedure TevWizard.FormDestroy(Sender: TObject);
begin
  FHist.Free;
  inherited;
end;

procedure TevWizard.GoToNextPage;
var
  Page: TevWizardPage;
begin
  if Assigned(FCurrentPage) then
  begin
    Page := FCurrentPage.GetNextPage;
    if Assigned(Page) then
    begin
      FCurrentPage.Hide;
      FHist.Add(FCurrentPage);
      FCurrentPage := Page;
      ShowPage(FCurrentPage);
      SyncButtons;
      try
        FCurrentPage.Init;
      except
        on E: Exception do
        begin
          IsExceptMessage(E, False);
          GoToPreviousPage;
        end;
      end;
    end
    else
      Init;
  end;
end;

procedure TevWizard.SyncButtons;
begin
  btnBack.Visible := FHist.Count > 0;
  btnNext.Enabled := Assigned(FCurrentPage) and FCurrentPage.CanMoveForward;
  if Assigned(FCurrentPage) then
    btnNext.Caption := FCurrentPage.NextButtonName;
end;

procedure TevWizard.DoOnChange;
begin
  SyncButtons;
end;

procedure TevWizard.btnNextClick(Sender: TObject);
begin
  GoToNextPage;
end;

procedure TevWizard.btnBackClick(Sender: TObject);
begin
  GoToPreviousPage;
end;

procedure TevWizard.GoToPreviousPage;
var
  Page: TevWizardPage;
begin
  if FHist.Count > 0 then
  begin
    Page := TevWizardPage(FHist[FHist.Count - 1]);
    FHist.Delete(FHist.Count - 1);
    ShowPage(Page);
  end
  else
    Page := nil;

  if Assigned(FCurrentPage) then
    FCurrentPage.Free;

  FCurrentPage := Page;
  if Assigned(FCurrentPage) then
    SyncButtons
  else
    Init;    
end;

procedure TevWizard.ClearHistory;
var
  i: Integer;
begin
  for i := 0 to FHist.Count - 1 do
    TevWizardPage(FHist[i]).Free;
  FHist.Clear;
  SyncButtons;  
end;

end.
