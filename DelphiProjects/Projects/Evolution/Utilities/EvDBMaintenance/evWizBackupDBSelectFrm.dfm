inherited evWizBackupDBSelect: TevWizBackupDBSelect
  Width = 543
  Height = 569
  object pnlDBList: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 543
    Height = 341
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Databases'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    DesignSize = (
      543
      341)
    object lSelectedSize: TISLabel
      Left = 30
      Top = 308
      Width = 140
      Height = 16
      Anchors = [akLeft, akBottom]
      Caption = 'Selected database size:'
    end
    object lCaption: TISLabel
      Left = 30
      Top = 45
      Width = 222
      Height = 16
      Caption = 'Select databases you want to back up'
    end
    object lvDBList: TISListView
      Left = 30
      Top = 72
      Width = 481
      Height = 203
      Anchors = [akLeft, akTop, akRight, akBottom]
      Checkboxes = True
      Columns = <
        item
          Caption = 'Database'
          Width = 150
        end
        item
          Caption = 'Size'
          Width = 70
        end>
      ColumnClick = False
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FlatScrollBars = True
      GridLines = True
      ReadOnly = True
      RowSelect = True
      ParentFont = False
      PopupMenu = pmSelect
      TabOrder = 0
      ViewStyle = vsReport
      OnChange = lvDBListChange
    end
    object chbScramble: TISCheckBox
      Left = 30
      Top = 281
      Width = 171
      Height = 17
      Anchors = [akLeft, akBottom]
      Caption = 'Scramble sensitive data'
      TabOrder = 1
      OnClick = chbScrambleClick
    end
  end
  object isUIFashionPanel1: TisUIFashionPanel
    Left = 0
    Top = 341
    Width = 543
    Height = 228
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 1
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Backup Folder'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 12
    Margins.Left = 12
    Margins.Right = 6
    Margins.Bottom = 60
    DesignSize = (
      543
      228)
    object ISSpeedButton1: TISSpeedButton
      Left = 489
      Top = 87
      Width = 25
      Height = 24
      HideHint = True
      AutoSize = False
      OnClick = ISSpeedButton1Click
      Anchors = [akTop, akRight]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDB66CC79B3FC79B3FDEBA77FFFFFFFF
        FFFFFFFFFFFFFFFFDAB268C2922DC2922DD8AF62FFFFFFFFFFFFAAD3DA60B8D1
        CF9D39E3BF74E3BF74BC973B73AAA154B1CC53B1CC53B1CCCD972EE0B864E0B8
        64C18C2267ACB18DD6E667C0D99CE2EDD2A84DE6C178DDB768BE99418DC1B370
        CEE469CBE163C8DFCFA343DEAE48DBA738C0943466B3B954B2CC69C2DAB3E9F0
        D7BC74EFDAA9EACE90BE994190C2B48ABFB384BCB180BAAFD0A84DDEAE48DBA7
        38C0943466B3B954B2CC6DC5DCBEECF1DCC587F5EBD2F2E4C1E3CB8EBF9D45BF
        9B43BF9A43BF9C45DBB356E1B250E1B250C1973868B4BA55B3CE6EC7DEC1EFF4
        DBCF99FAF9EEFAF9EEF2E6C4EEDAA5EAD496E5C881E2C275E2C275E3C172E3C1
        72BD973B6BB6BB56B4D070C9DFC4F3F8DAD8ABFAF9EEFAF9EEFFFFF7FFFFE9FF
        FDD5FFEFB7FFE6A5FFE6A5FEE29BE7CE8DBD973B6CB8BD56B4D074CCE2C4F3F9
        ABEEF5D2C992D2C992DCC57FDAC176D2BB70CAB365C4B060C1AB5CACA664ACA6
        6484BCB060C5DE58B6D175CEE3C7F4F9ABEDF6ABEEF6AAEDF5A6EAF49FE8F299
        E6F193E3F08EE0EF88DDED83D9EA7FD6E975D2E66BCDE259B8D275CEE3D1F6FA
        ACEFF6ABEEF6ABEEF5ABEEF6A6ECF5A1E9F49DE7F399E5F194E3F090E0EF8CDE
        EC83DCED77D5E85AB9D277D0E4DCF5F9D9F4F9CDF0F7CEF0F7CDF0F7C9EFF6BB
        F0F7A4EBF5A1E9F49EE8F29BE6F199E5F190E3F186DEEE5AB9D377D0E464C0D9
        5AB9D35AB9D35AB9D35AB9D35AB9D35AB9D38AD1E2BFEDF4BBECF3B5EAF2A8E6
        EF9CE2ED98E1EC5ABAD375CFE381CDDF26EB5C26C45CFDFEFEF6F6F6F6F6F6F6
        F6F65ABAD35ABAD35ABAD35ABAD35ABAD35ABAD35ABAD3AEDAE2AEDAE26AC5DC
        66C3DA5DBCD65DBCD65DBCD65DBCD67BC6DAAEDAE2FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentColor = False
      ShortCut = 0
    end
    object lFreeSpace: TISLabel
      Left = 30
      Top = 126
      Width = 114
      Height = 16
      Caption = 'Free space:  45 GB'
    end
    object edDestFolder: TISEdit
      Left = 30
      Top = 88
      Width = 456
      Height = 24
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnChange = edDestFolderChange
    end
    object chbCompress: TISCheckBox
      Left = 30
      Top = 56
      Width = 179
      Height = 17
      Caption = 'Compress backup files'
      TabOrder = 1
      OnClick = chbCompressClick
    end
  end
  object dlgDestFolder: TPBFolderDialog
    LabelCaptions.Strings = (
      'Default=Current Folder:')
    NewFolderCaptions.Strings = (
      'Default=New folder')
    Titles.Strings = (
      'Database backup folder')
    Left = 488
    Top = 384
  end
  object pmSelect: TISPopupMenu
    Left = 120
    Top = 104
    object miSelectAll: TMenuItem
      Caption = 'Select All'
      ShortCut = 16449
      OnClick = miSelectAllClick
    end
    object miUnselectAll: TMenuItem
      Caption = 'Unselect All'
      OnClick = miUnselectAllClick
    end
    object miReverseSelection: TMenuItem
      Caption = 'Reverse Selection'
      OnClick = miReverseSelectionClick
    end
  end
end
