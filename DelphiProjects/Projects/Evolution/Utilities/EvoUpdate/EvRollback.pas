unit EvRollback;

interface

uses Windows, SysUtils, isBaseClasses, evUpdate, ISZippingRoutines, Dialogs, isBasicUtils,
     SWaitingObjectForm;

type
  TevRollback = class(TisInterfacedObject, IevUpdate)
  private
    FUpdateTmpFolder: String;
    FWaitObj: TAdvancedWait;
  protected
     procedure DoOnConstruction; override;
     procedure Run;
  public
    destructor Destroy; override;
  end;

implementation

{ TevRollback }

destructor TevRollback.Destroy;
begin
  FWaitObj.Free;
  inherited;
end;

procedure TevRollback.DoOnConstruction;
begin
  inherited;
  FUpdateTmpFolder := AppTempFolder + 'Update\';
  FWaitObj := TAdvancedWait.Create;
end;

procedure TevRollback.Run;
var
  sNewVersion, UpdAppFolder, BkpFolder: String;
  sBackupFile, sAppFile, sEvoUpdate: String;
  L: IisStringList;
  i: Integer;

  function CopyRequiredFile(const AFileName: String): String;
  var
    sFile: String;
  begin
    sFile := ExtractFilePath(AppFileName) + AFileName;
    if FileExists(sFile) then
    begin
      Result := FUpdateTmpFolder + AFileName;
      CopyFile(sFile, Result, False);
    end
    else
      Result := '';  
  end;

begin
  FWaitObj.StartWait('');
  try
    sAppFile := AppSwitches.TryGetValue('AppFile', AppFileName);
    sBackupFile := AppSwitches.TryGetValue('BackupFile', '');

    if sBackupFile = '' then
    begin
      sBackupFile := AppDir + 'PrevVersion\';
      L := GetFilesByMask(sBackupFile + '*.zip');
      if L.Count = 0 then
      begin
        MessageDlg('Cannot find a backup file in ' + sBackupFile, mtError, [mbOK], 0);
        Exit;
      end;
      sBackupFile := L[L.Count - 1];

      sNewVersion := sBackupFile;
      GetPrevStrValue(sNewVersion, '.');
      sNewVersion := GetPrevStrValue(sNewVersion, '_');

      if MessageDlg('Evolution autoupdate wants to downgrade to version ' + sNewVersion + '. Continue?',
                 mtConfirmation , [mbYes, mbNo], 0) <> idYes then
        Exit;           


      FWaitObj.UpdateWait('Preparing for downgrade');

      sEvoUpdate := CopyRequiredFile(ExtractFileName(AppFileName));
      RunProcess(sEvoUpdate,
        Format('/BackupFile "%s" /AppFile "%s"', [sBackupFile, sAppFile]), []);
      Exit; // exit for update
    end

    else if not FileExists(sBackupFile) then
    begin
      MessageDlg('Cannot open file ' + sBackupFile, mtError, [mbOK], 0);
      Exit;
    end;

    // Rollback process

    sNewVersion := sBackupFile;
    GetPrevStrValue(sNewVersion, '.');
    sNewVersion := GetPrevStrValue(sNewVersion, '_');
    UpdAppFolder := ExtractFilePath(sAppFile);
    BkpFolder := UpdAppFolder + 'Backup\';

    FWaitObj.UpdateWait('Downgrading to version ' + sNewVersion + #13#13 + 'Please wait...');
    try
      // backup current version
      ClearDir(BkpFolder);
      ForceDirectories(BkpFolder);

      L := GetFilesByMask(UpdAppFolder + '*.*');
      for i := 0 to L.Count - 1 do
        MoveFile(L[i], BkpFolder + ExtractFileName(L[i]));

      ExtractFromFile(sBackupFile, UpdAppFolder, '*.*');

      SetFileAttributes(PAnsiChar(sBackupFile), FILE_ATTRIBUTE_NORMAL);
      DeleteFile(sBackupFile);
    except
      // rollback
      on E: Exception do
      begin
        FWaitObj.UpdateWait(E.Message + #13 + 'Rolling back...');
        L := GetFilesByMask(BkpFolder + '*.*');
        for i := 0 to L.Count - 1 do
          try
            DeleteFile(UpdAppFolder + ExtractFileName(L[i]));
            MoveFile(L[i], UpdAppFolder + ExtractFileName(L[i]));
          except
            // in case if file is locked
          end;
        MessageDlg('Version downgradate has failed. All changes have been rolled back.', mtError, [mbOK], 0);
        raise;
      end;
    end;

  finally
    FWaitObj.EndWait;
  end;

  MessageDlg('Downgrade process has finished successfully!'#13'Current Version: ' + sNewVersion, mtInformation, [mbOK], 0);
  RunProcess(AppSwitches.Value['AppFile'], '', []);  // restart app
end;

end.