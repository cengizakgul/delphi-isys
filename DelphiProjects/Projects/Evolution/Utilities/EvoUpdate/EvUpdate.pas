unit EvUpdate;

interface

uses Sysutils, Dialogs;

type
  IevUpdate = interface
  ['{01469DED-D979-4447-90C7-C733A0F773F0}']
     procedure Run;
  end;

procedure RunUpdate;

implementation

{$IFDEF EVO9}
uses EvUpdate10;
{$ENDIF}

{$IFDEF ROLLBACK}
uses EvRollback;
{$ENDIF}


procedure RunUpdate;
var
  Upd: IevUpdate;
begin
  try
  {$IFDEF EVO9}
    Upd := TevUpdate10.Create;
  {$ENDIF}

  {$IFDEF ROLLBACK}
    Upd := TevRollback.Create;
  {$ENDIF}
    Upd.Run;
  except
    on E: Exception do
      MessageDlg(E.Message, mtError, [mbOK], 0);
  end;
end;


end.