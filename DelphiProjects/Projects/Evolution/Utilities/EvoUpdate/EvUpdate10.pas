// Lincoln update
unit EvUpdate10;

interface

uses SysUtils, Classes, isBaseClasses, EvUpdate, EvInitApp, isAppIDs, ISZippingRoutines,
     EvMainboard, EvCommonInterfaces, SWaitingObjectForm, EvTransportInterfaces, isBasicUtils;

type
  TevUpdate10 = class(TisInterfacedObject, IevUpdate, IevEvolutionGUI, IevContextCallbacks)
  private
    FAppFile: String;
    FAppID: TisGUID;
    FHost: String;
    FPort: String;
    FWaitDialog: TAdvancedWait;
  protected
    procedure DoOnConstruction; override;

    procedure  Run;

    function   GUIContext: IevContext;
    procedure  CreateMainForm;
    procedure  DestroyMainForm;
    procedure  StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure  UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure  EndWait;
    procedure  AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
    procedure  OnWaitingServerResponse;    

    procedure  ApplySecurity(const Component: TComponent);
    procedure  AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True);
    procedure  OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure  CheckTaskQueueStatus;
    procedure  PrintTask(const ATaskID: TisGUID);
    procedure  OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure  OnPopupMessage(const aText, aFromUser, aToUsers: String);    
  public
    destructor  Destroy; override;
  end;

implementation

var
  FUpdater: TevUpdate10;


function GetEvoUpdateGUI: IevEvolutionGUI;
begin
  Result := FUpdater as IevEvolutionGUI;
end;

function GetEvoUpdateContextCallbacks: IevContextCallbacks;
begin
  Result := FUpdater as IevContextCallbacks;
end;


{ TevUpdate10 }

procedure TevUpdate10.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TevUpdate10.ApplySecurity(const Component: TComponent);
begin
// a plug
end;

procedure TevUpdate10.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
begin
  // a plug
end;

procedure TevUpdate10.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TevUpdate10.CreateMainForm;
begin
// a plug
end;

destructor TevUpdate10.Destroy;
begin
  FreeAndNil(FWaitDialog);
  inherited;
  FUpdater := nil;
end;

procedure TevUpdate10.DestroyMainForm;
begin
// a plug
end;

procedure TevUpdate10.DoOnConstruction;
begin
  inherited;
  FUpdater := Self;

  FAppFile := AppSwitches.TryGetValue('AppFile', '');
  FAppID := AppSwitches.TryGetValue('AppID', EvoClientAppInfo.AppID);
  FHost := AppSwitches.TryGetValue('Host', '');
  FPort := AppSwitches.TryGetValue('Port', '');

  FWaitDialog := TAdvancedWait.Create;
end;

procedure TevUpdate10.EndWait;
begin
  FWaitDialog.EndWait;
end;

function TevUpdate10.GUIContext: IevContext;
begin
  Result := Mainboard.ContextManager.GetThreadContext;
end;

procedure TevUpdate10.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
// a plug
end;

procedure TevUpdate10.OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TevUpdate10.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TevUpdate10.OnWaitingServerResponse;
begin
// a plug
end;

procedure TevUpdate10.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;

procedure TevUpdate10.Run;
begin
  inherited;
  InitializeEvoApp(FAppID);
  try
    if Mainboard.VersionUpdateLocal.PerformUpdateIfNeeds then
      Exit;  // update is finished

    Mainboard.TCPClient.Host := FHost;
    Mainboard.TCPClient.EncryptionType := etNone; // just in case
    Mainboard.TCPClient.Port := FPort;  // will setup SSL encryption types for non 9500 ports automatically
    Mainboard.TCPClient.CompressionLevel := clBestCompression;

    Mainboard.VersionUpdateLocal.PrepareUpdateIfNeeds(False);

  finally
    if Assigned(Mainboard.TCPClient.Session) then
      Mainboard.TCPClient.Logoff;  // in order to get rid of Guest connection

    UninitializeEvoApp;
  end;
end;


procedure TevUpdate10.StartWait(const AText: string; AMaxProgress: Integer);
begin
  FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TevUpdate10.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEvoUpdateGUI, IevEvolutionGUI, 'Evo Update GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetEvoUpdateContextCallbacks, IevContextCallbacks, 'Context Callbacks');

end.
