object frmSyncReport: TfrmSyncReport
  Left = 0
  Top = 0
  Width = 677
  Height = 469
  TabOrder = 0
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 677
    Height = 469
    ActivePage = tsReports
    Align = alClient
    TabOrder = 0
    object tsReports: TTabSheet
      Caption = 'Reports'
      object Label1: TLabel
        Left = 4
        Top = 6
        Width = 149
        Height = 13
        Caption = 'Development System Database'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 56
        Width = 103
        Height = 13
        Caption = 'Development Reports'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TLabel
        Left = 362
        Top = 6
        Width = 114
        Height = 13
        Caption = 'Patch System Database'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 363
        Top = 56
        Width = 68
        Height = 13
        Caption = 'Patch Reports'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object sbToPatch: TSpeedButton
        Left = 336
        Top = 207
        Width = 25
        Height = 27
        Caption = '>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Layout = blGlyphBottom
        ParentFont = False
        Spacing = 0
        Visible = False
        OnClick = sbToDevClick
      end
      object sbToDev: TSpeedButton
        Left = 336
        Top = 207
        Width = 25
        Height = 27
        Caption = '<'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Layout = blGlyphBottom
        ParentFont = False
        Spacing = 0
        Visible = False
        OnClick = sbToDevClick
      end
      object Label6: TLabel
        Left = 4
        Top = 400
        Width = 13
        Height = 13
        AutoSize = False
        Color = clGreen
        ParentColor = False
      end
      object Label7: TLabel
        Left = 23
        Top = 400
        Width = 78
        Height = 13
        Caption = 'Changed Report'
      end
      object Label8: TLabel
        Left = 4
        Top = 418
        Width = 13
        Height = 13
        AutoSize = False
        Color = clRed
        ParentColor = False
      end
      object Label9: TLabel
        Left = 23
        Top = 418
        Width = 57
        Height = 13
        Caption = 'New Report'
      end
      object Label14: TLabel
        Left = 324
        Top = 395
        Width = 47
        Height = 13
        Caption = 'Fork Date'
      end
      object edDevDB: TEdit
        Left = 4
        Top = 22
        Width = 330
        Height = 21
        TabOrder = 0
        Text = '192.168.2.224:/db/dorset/SYSTEM.gdb'
      end
      object grDev: TDBGrid
        Left = 4
        Top = 72
        Width = 330
        Height = 323
        DataSource = dsDevRep
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawDataCell = grPathDrawDataCell
        OnEnter = grDevEnter
        OnExit = grDevExit
      end
      object edPatchDB: TEdit
        Left = 362
        Top = 22
        Width = 330
        Height = 21
        TabOrder = 2
        Text = '192.168.2.224:/db/system/system.gdb'
      end
      object grPath: TDBGrid
        Left = 363
        Top = 72
        Width = 330
        Height = 323
        DataSource = dsPatchRep
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawDataCell = grPathDrawDataCell
        OnEnter = grPathEnter
        OnExit = grDevExit
      end
      object btnGetInfo: TButton
        Left = 436
        Top = 403
        Width = 123
        Height = 25
        Caption = 'Get Sync Info'
        TabOrder = 4
        OnClick = btnGetInfoClick
      end
      object btnAutoSync: TButton
        Left = 570
        Top = 403
        Width = 123
        Height = 25
        Caption = 'Auto Sync'
        TabOrder = 5
        OnClick = btnAutoSyncClick
      end
      object dedForkDate: TDateTimePicker
        Left = 308
        Top = 408
        Width = 89
        Height = 21
        Date = 37712.000000000000000000
        Time = 37712.000000000000000000
        TabOrder = 6
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Change Log'
      ImageIndex = 1
      object Label4: TLabel
        Left = 4
        Top = 6
        Width = 108
        Height = 13
        Caption = 'Development Changes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 351
        Top = 6
        Width = 73
        Height = 13
        Caption = 'Patch Changes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 4
        Top = 400
        Width = 13
        Height = 13
        AutoSize = False
        Color = clGreen
        ParentColor = False
      end
      object Label11: TLabel
        Left = 23
        Top = 400
        Width = 103
        Height = 13
        Caption = 'Report to be Updated'
      end
      object Label12: TLabel
        Left = 4
        Top = 418
        Width = 13
        Height = 13
        AutoSize = False
        Color = clRed
        ParentColor = False
      end
      object Label13: TLabel
        Left = 23
        Top = 418
        Width = 93
        Height = 13
        Caption = 'Report to be Added'
      end
      object btnCommit: TButton
        Left = 570
        Top = 403
        Width = 123
        Height = 25
        Caption = 'Commit Changes'
        TabOrder = 0
        OnClick = btnCommitClick
      end
      object grDevLog: TDBGrid
        Left = 4
        Top = 22
        Width = 342
        Height = 373
        DataSource = dsDevLog
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawDataCell = grPathDrawDataCell
      end
      object DBGrid4: TDBGrid
        Left = 351
        Top = 22
        Width = 342
        Height = 373
        DataSource = dsPatchLog
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawDataCell = grPathDrawDataCell
      end
    end
    object tsErrLog: TTabSheet
      Caption = 'Errors Log'
      ImageIndex = 2
      TabVisible = False
      object memErrLog: TMemo
        Left = 0
        Top = 0
        Width = 696
        Height = 433
        Align = alClient
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
        OnChange = memErrLogChange
      end
    end
  end
  object dbDevelopment: TIBDatabase
    Params.Strings = (
      'isc_dpb_user_name=SYSDBA'
      'isc_dpb_password=pps97')
    LoginPrompt = False
    AllowStreamedConnected = False
    Left = 184
    Top = 32
  end
  object dbPatch: TIBDatabase
    Params.Strings = (
      'isc_dpb_user_name=SYSDBA'
      'isc_dpb_password=pps97')
    LoginPrompt = False
    SQLDialect = 1
    AllowStreamedConnected = False
    Left = 520
  end
  object trDevelopment: TIBTransaction
    DefaultDatabase = dbDevelopment
    DefaultAction = TARollback
    AutoStopAction = saCommitRetaining
    Left = 216
    Top = 32
  end
  object trPatch: TIBTransaction
    DefaultDatabase = dbPatch
    DefaultAction = TARollback
    AutoStopAction = saCommitRetaining
    Left = 552
  end
  object qDevReports: TIBQuery
    Database = dbDevelopment
    Transaction = trDevelopment
    SQL.Strings = (
      'select * from SY_REPORT_WRITER_REPORTS_HIST('#39'now'#39')'
      ' ')
    Left = 248
    Top = 32
  end
  object qPatchReports: TIBQuery
    Database = dbPatch
    Transaction = trPatch
    SQL.Strings = (
      'select * from SY_REPORT_WRITER_REPORTS_HIST('#39'now'#39')'
      ' ')
    Left = 590
  end
  object clDevReport: TEvBasicClientDataSet
    IndexFieldNames = 'nbr'
    IndexDefs = <
      item
        Name = 'clDevReportIndex1'
        Fields = 'nbr'
      end
      item
        Name = 'clDevReportIndex2'
        Fields = 'name'
      end>
    MasterFields = 'nbr'
    Left = 188
    Top = 104
    object clDevReportname: TStringField
      DisplayLabel = 'Report'
      DisplayWidth = 32
      FieldName = 'name'
      Size = 40
    end
    object clDevReporttype: TStringField
      DisplayLabel = 'Type'
      FieldName = 'type'
      Size = 1
    end
    object clDevReportlast_upd: TDateTimeField
      DisplayLabel = 'Last Update'
      DisplayWidth = 20
      FieldName = 'last_upd'
    end
    object clDevReportstatus: TStringField
      FieldName = 'status'
      Visible = False
      Size = 1
    end
    object clDevReportnbr: TIntegerField
      DisplayLabel = 'NBR'
      DisplayWidth = 5
      FieldName = 'nbr'
    end
  end
  object dsDevRep: TDataSource
    AutoEdit = False
    DataSet = clDevReport
    Left = 188
    Top = 136
  end
  object clPatchReport: TEvBasicClientDataSet
    IndexFieldNames = 'nbr'
    IndexDefs = <
      item
        Name = 'clPatchReportIndex1'
        Fields = 'nbr'
      end
      item
        Name = 'clPatchReportIndex2'
        Fields = 'name'
      end>
    MasterFields = 'nbr'
    Left = 516
    Top = 112
    object StringField1: TStringField
      DisplayLabel = 'Report'
      DisplayWidth = 32
      FieldName = 'name'
      Size = 40
    end
    object clPatchReporttype: TStringField
      DisplayLabel = 'Type'
      FieldName = 'type'
      Size = 1
    end
    object clPatchReportlast_upd: TDateTimeField
      DisplayLabel = 'Last Update'
      DisplayWidth = 20
      FieldName = 'last_upd'
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'NBR'
      DisplayWidth = 5
      FieldName = 'nbr'
    end
    object StringField2: TStringField
      FieldName = 'status'
      Visible = False
      Size = 1
    end
  end
  object dsPatchRep: TDataSource
    AutoEdit = False
    DataSet = clPatchReport
    Left = 516
    Top = 144
  end
  object clDevLog: TEvBasicClientDataSet
    IndexFieldNames = 'name'
    IndexDefs = <
      item
        Name = 'clDevLogIndex1'
        Fields = 'name'
      end>
    BeforeDelete = clDevLogBeforeDelete
    Left = 188
    Top = 192
    object clDevLogname: TStringField
      DisplayLabel = 'Report'
      DisplayWidth = 32
      FieldName = 'name'
      Size = 40
    end
    object clDevLogtype: TStringField
      DisplayLabel = 'Type'
      FieldName = 'type'
      Size = 1
    end
    object clDevLognbr: TIntegerField
      DisplayLabel = 'NBR'
      DisplayWidth = 5
      FieldName = 'nbr'
    end
    object clDevLoglast_upd: TDateTimeField
      DisplayLabel = 'Patch Version'
      DisplayWidth = 20
      FieldName = 'last_upd_src'
    end
    object clDevLoglast_upd_dest: TDateTimeField
      DisplayLabel = 'Dev Version'
      DisplayWidth = 20
      FieldName = 'last_upd_dest'
    end
    object clDevLogstatus: TStringField
      FieldName = 'status'
      Visible = False
      Size = 1
    end
  end
  object dsDevLog: TDataSource
    AutoEdit = False
    DataSet = clDevLog
    Left = 188
    Top = 224
  end
  object clPatchLog: TEvBasicClientDataSet
    IndexFieldNames = 'name'
    IndexDefs = <
      item
        Name = 'clPatchLogIndex1'
        Fields = 'name'
      end>
    BeforeDelete = clPatchLogBeforeDelete
    Left = 516
    Top = 192
    object StringField3: TStringField
      DisplayLabel = 'Report'
      DisplayWidth = 32
      FieldName = 'name'
      Size = 40
    end
    object clPatchLogtype: TStringField
      DisplayLabel = 'Type'
      FieldName = 'type'
      Size = 1
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'NBR'
      FieldName = 'nbr'
    end
    object clPatchLoglast_upd: TDateTimeField
      DisplayLabel = 'Dev Version'
      DisplayWidth = 20
      FieldName = 'last_upd_src'
    end
    object clPatchLoglast_upd_dest: TDateTimeField
      DisplayLabel = 'Patch Version'
      DisplayWidth = 20
      FieldName = 'last_upd_dest'
    end
    object clPatchLogstatus: TStringField
      FieldName = 'status'
      Visible = False
      Size = 1
    end
  end
  object dsPatchLog: TDataSource
    AutoEdit = False
    DataSet = clPatchLog
    Left = 516
    Top = 224
  end
  object qInsDev: TIBQuery
    Database = dbDevelopment
    Transaction = trDevelopment
    SQL.Strings = (
      'insert into SY_REPORT_WRITER_REPORTS '
      ' (report_type, '
      '  report_status,'
      '  report_description, '
      '  report_file, '
      '  notes, '
      '  media_type,'
      '  ancestor_class_name,'
      '  changed_by, '
      '  sy_report_writer_reports_nbr,'
      '  active_record,'
      '  creation_date,'
      '  effective_date)'
      ''
      'values '
      ' (:report_type, '
      '  :report_status,'
      '  :report_description, '
      '  :report_file, '
      '  :notes, '
      '  :media_type,'
      '  :ancestor_class_name,'
      '  0, '
      '  :nbr,'
      '  '#39'C'#39','
      '  '#39'now'#39','
      '  '#39'now'#39')')
    Left = 341
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'report_type'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'report_status'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'report_description'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'report_file'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'notes'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'media_type'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'ancestor_class_name'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'nbr'
        ParamType = ptUnknown
      end>
  end
  object qGen: TIBQuery
    Database = dbDevelopment
    Transaction = trDevelopment
    SQL.Strings = (
      'select Gen_Id(sy_report_writer_reports_gen, 1) nbr'
      'from rdb$database')
    Left = 341
    Top = 152
  end
  object qInsPatch: TIBQuery
    Database = dbDevelopment
    Transaction = trDevelopment
    SQL.Strings = (
      'insert into SY_REPORT_WRITER_REPORTS '
      ' (report_type, '
      '  report_status,'
      '  report_description, '
      '  report_file, '
      '  notes, '
      '  media_type,'
      '  ancestor_class_name,'
      '  changed_by, '
      '  sy_report_writer_reports_nbr,'
      '  active_record,'
      '  creation_date,'
      '  effective_date)'
      ''
      'values '
      ' (:report_type, '
      '  :report_status,'
      '  :report_description, '
      '  :report_file, '
      '  :notes, '
      '  :media_type,'
      '  :ancestor_class_name,'
      '  0, '
      '  :nbr,'
      '  '#39'C'#39','
      '  '#39'now'#39','
      '  '#39'now'#39')')
    Left = 373
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'report_type'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'report_status'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'report_description'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'report_file'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'notes'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'media_type'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'ancestor_class_name'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'nbr'
        ParamType = ptUnknown
      end>
  end
  object stpAfterStartTransaction: TIBStoredProc
    Database = dbDevelopment
    Transaction = trDevelopment
    StoredProcName = 'AFTER_START_TRANSACTION'
    Left = 344
    Top = 192
  end
  object stpBeforeCommitTransaction: TIBStoredProc
    Database = dbDevelopment
    Transaction = trDevelopment
    StoredProcName = 'BEFORE_COMMIT_TRANSACTION'
    Left = 344
    Top = 224
  end
end
