unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, DBGrids, ComCtrls, Buttons, Db,
  IBDatabase, IBCustomDataSet, IBQuery, Variants, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses,  EvDataAccessComponents,
  ISDataAccessComponents, IBStoredProc, EvUIComponents;

type
  TfrmSyncReport = class(TFrame)
    PageControl1: TPageControl;
    tsReports: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    edDevDB: TEdit;
    Label2: TLabel;
    grDev: TDBGrid;
    evLabel1: TLabel;
    edPatchDB: TEdit;
    Label3: TLabel;
    grPath: TDBGrid;
    btnGetInfo: TButton;
    btnAutoSync: TButton;
    btnCommit: TButton;
    Label4: TLabel;
    grDevLog: TDBGrid;
    Label5: TLabel;
    DBGrid4: TDBGrid;
    sbToPatch: TSpeedButton;
    sbToDev: TSpeedButton;
    dbDevelopment: TIBDatabase;
    dbPatch: TIBDatabase;
    trDevelopment: TIBTransaction;
    trPatch: TIBTransaction;
    qDevReports: TIBQuery;
    qPatchReports: TIBQuery;
    clDevReport: TEvBasicClientDataSet;
    clDevReportnbr: TIntegerField;
    clDevReportname: TStringField;
    clDevReportstatus: TStringField;
    dsDevRep: TDataSource;
    clPatchReport: TEvBasicClientDataSet;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    dsPatchRep: TDataSource;
    clDevReportlast_upd: TDateTimeField;
    clPatchReportlast_upd: TDateTimeField;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    clDevLog: TEvBasicClientDataSet;
    dsDevLog: TDataSource;
    clDevLognbr: TIntegerField;
    clDevLogname: TStringField;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    clPatchLog: TEvBasicClientDataSet;
    IntegerField2: TIntegerField;
    StringField3: TStringField;
    clPatchLogstatus: TStringField;
    clDevLogstatus: TStringField;
    dsPatchLog: TDataSource;
    Label14: TLabel;
    dedForkDate: TDateTimePicker;
    clDevReporttype: TStringField;
    clPatchReporttype: TStringField;
    clDevLogtype: TStringField;
    clPatchLogtype: TStringField;
    qInsDev: TIBQuery;
    qGen: TIBQuery;
    tsErrLog: TTabSheet;
    memErrLog: TMemo;
    clDevLoglast_upd: TDateTimeField;
    clPatchLoglast_upd: TDateTimeField;
    clDevLoglast_upd_dest: TDateTimeField;
    clPatchLoglast_upd_dest: TDateTimeField;
    qInsPatch: TIBQuery;
    stpAfterStartTransaction: TIBStoredProc;
    stpBeforeCommitTransaction: TIBStoredProc;
    procedure btnGetInfoClick(Sender: TObject);
    procedure grDevEnter(Sender: TObject);
    procedure grPathEnter(Sender: TObject);
    procedure grPathDrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure sbToDevClick(Sender: TObject);
    procedure grDevExit(Sender: TObject);
    procedure btnCommitClick(Sender: TObject);
    procedure memErrLogChange(Sender: TObject);
    procedure btnAutoSyncClick(Sender: TObject);
    procedure clDevLogBeforeDelete(DataSet: TDataSet);
    procedure clPatchLogBeforeDelete(DataSet: TDataSet);

  private
    FForkDate: TDateTime;
    FCommiting: Boolean;
    procedure Connect;
    procedure Disconnect;
    procedure FillClDataSet(AQuery: TIBQuery; ADataSet: TevBasicClientDataset);
    procedure CompareDataSets;
    procedure Busy;
    procedure Ready;
    procedure CommitChanges(ADest: Char);
    procedure AutoSync(ADest: Char);
    procedure AddLogRecord(ADest: Char);
    procedure RollbackRecord(ADest: Char);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.DFM}

procedure TfrmSyncReport.Connect;
begin
  try
    dbDevelopment.DatabaseName := edDevDB.Text;
    dbDevelopment.Connected := True;
    trDevelopment.Active := True;

    dbPatch.DatabaseName := edPatchDB.Text;
    dbPatch.Connected := True;
    trPatch.Active := True;

    edDevDB.Enabled := False;
    edPatchDB.Enabled := False;
    btnGetInfo.Enabled := False;
    dedForkDate.Enabled := False;
    btnAutoSync.Enabled := True;
    btnCommit.Enabled := True;


    FForkDate := dedForkDate.Date + StrToTime('11:59:59 PM');

    clPatchReport.Filter := 'last_upd is not null and last_upd > ''' + DateToStr(FForkDate) + '''';
    clDevReport.Filter := clPatchReport.Filter;

    clDevReport.CreateDataSet;
    clPatchReport.CreateDataSet;
    clDevLog.CreateDataSet;
    clPatchLog.CreateDataSet;

    FCommiting := False;
  except
    Disconnect;
    raise;
  end;
end;

procedure TfrmSyncReport.Disconnect;
begin
  qDevReports.Close;
  qPatchReports.Close;
  clDevReport.Close;
  clPatchReport.Close;
  clDevLog.Close;
  clPatchLog.Close;


  trDevelopment.Active := False;
  dbDevelopment.Connected := False;

  trPatch.Active := False;
  dbPatch.Connected := False;

  edDevDB.Enabled := True;
  edPatchDB.Enabled := True;
  btnGetInfo.Enabled := True;
  dedForkDate.Enabled := True;
  btnAutoSync.Enabled := False;
  btnCommit.Enabled := False;
end;

procedure TfrmSyncReport.btnGetInfoClick(Sender: TObject);
begin
  Busy;
  try
    Disconnect;
    Connect;
    Update;

    qDevReports.Open;
    qPatchReports.Open;

    FillClDataSet(qDevReports, clDevReport);
    FillClDataSet(qPatchReports, clPatchReport);

    CompareDataSets;

    clDevLog.EmptyDataSet;
    clPatchLog.EmptyDataSet;

    grDev.OnExit(nil);
  finally
    Ready;
  end;
end;

procedure TfrmSyncReport.FillClDataSet(AQuery: TIBQuery; ADataSet: TevBasicClientDataset);
var
  h: String;
  i, j: Integer;
begin
  ADataSet.DisableControls;
  ADataSet.Filtered := False;
  ADataSet.MasterSource := nil;
  ADataSet.EmptyDataSet;

  AQuery.First;
  while not AQuery.Eof do
  begin
    ADataSet.Append;
    ADataSet.FieldByName('nbr').AsInteger := AQuery.FieldByName('sy_report_writer_reports_nbr').AsInteger;
    ADataSet.FieldByName('name').AsString := AQuery.FieldByName('report_description').AsString;
    ADataSet.FieldByName('type').AsString := AQuery.FieldByName('report_type').AsString;

    h := AQuery.FieldByName('notes').AsString;
    j := 0;
    for i := Length(h) downto 1 do
      if h[i] = #13 then
      begin
        j := i;
        break;
      end;

    h := AnsiUpperCase(Copy(h, j+1, Length(h) - j));
    if Copy(h, 1, 12) = 'LAST UPDATE:' then
    begin
      h := Trim(Copy(h, 13, Length(h) - 12));
      ADataSet.FieldByName('last_upd').AsDateTime := StrToDateTime(h);
    end;

    ADataSet.Post;
    AQuery.Next;
  end;

  ADataSet.First;
  ADataSet.EnableControls
end;

procedure TfrmSyncReport.Busy;
begin
  Screen.Cursor := crHourGlass;
end;

procedure TfrmSyncReport.Ready;
begin
  Screen.Cursor := crArrow;
end;

procedure TfrmSyncReport.grDevEnter(Sender: TObject);
begin
  clDevReport.MasterSource := nil;
  clDevReport.IndexFieldNames := 'name';
  clPatchReport.IndexFieldNames := 'nbr';
  clPatchReport.Filtered := False;
  clDevReport.Filtered := True;
  clPatchReport.MasterSource := dsDevRep;
  sbToPatch.Visible := True;
  sbToDev.Visible := False;
end;

procedure TfrmSyncReport.grPathEnter(Sender: TObject);
begin
  clPatchReport.MasterSource := nil;
  clPatchReport.IndexFieldNames := 'name';
  clDevReport.IndexFieldNames := 'nbr';
  clDevReport.Filtered := False;
  clPatchReport.Filtered := True;
  clDevReport.MasterSource := dsPatchRep;
  sbToDev.Visible := True;
  sbToPatch.Visible := False;
end;

procedure TfrmSyncReport.CompareDataSets;
  procedure Comp(AMaster, AChild: TevBasicClientDataSet);
  begin
    AMaster.First;
    AChild.IndexFieldNames := 'nbr';
    while not AMaster.Eof do
    begin
      if not AChild.FindKey([AMaster.FieldByName('nbr').AsInteger]) then
      begin
        AMaster.Edit;
        AMaster.FieldByName('status').AsString := 'N';
        AMaster.Post;
        AMaster.Next;
      end

      else if (AMaster.FieldByName('last_upd').IsNull and AChild.FieldByName('last_upd').IsNull) or
        (AMaster.FieldByName('last_upd').AsDateTime = AChild.FieldByName('last_upd').AsDateTime) then
      begin
        AChild.Delete;
        AMaster.Delete;
      end
      else
        AMaster.Next;
    end;
  end;

begin
  clDevReport.DisableControls;
  clPatchReport.DisableControls;
  try
    Comp(clPatchReport, clDevReport);
    Comp(clDevReport, clPatchReport);
  finally
    clDevReport.First;
    clDevReport.EnableControls;
    clPatchReport.First;
    clPatchReport.EnableControls;
  end;
end;

procedure TfrmSyncReport.grPathDrawDataCell(Sender: TObject; const Rect: TRect;
  Field: TField; State: TGridDrawState);
var c: TColor;
begin
  if (Field.FieldName = 'name') then
  begin
    if not TDBGrid(Sender).DataSource.DataSet.FieldByName('status').IsNull then
      c := clRed
    else
      c := clGreen;

    if gdFocused in State then
      TDBGrid(Sender).Canvas.Font.Color := clYellow
    else
      TDBGrid(Sender).Canvas.Font.Color := c;
    TDBGrid(Sender).Canvas.TextRect(Rect, Rect.Left + 2 , Rect.Top + 2, Field.AsString);
  end;
end;

procedure TfrmSyncReport.sbToDevClick(Sender: TObject);
begin
  if Sender = sbToDev then
    AddLogRecord('D')
  else if Sender = sbToPatch then
    AddLogRecord('P')
  else
    Exit;
end;

procedure TfrmSyncReport.grDevExit(Sender: TObject);
begin
  clDevReport.MasterSource := nil;
  clPatchReport.MasterSource := nil;
  clPatchReport.Filtered := False;
  clDevReport.Filtered := False;
  clDevReport.IndexFieldNames := 'name';
  clPatchReport.IndexFieldNames := 'name';
end;

procedure TfrmSyncReport.btnCommitClick(Sender: TObject);
begin
  FCommiting :=True;
  Busy;
  try
    memErrLog.Clear;
    memErrLog.OnChange(nil);
    CommitChanges('D');
    qDevReports.Open;
    CommitChanges('P');
    qPatchReports.Open;
  finally
    FCommiting :=False;
    Ready;
  end;    
end;


procedure TfrmSyncReport.CommitChanges(ADest: Char);
var
  clLog: TevBasicClientDataSet;
  qSource: TIBQuery;
  Err: Boolean;
  fl_first_err: Boolean;
  DestName: String;
  qIns: TIBQuery;
begin
  fl_first_err := True;

  if ADest = 'D' then
  begin
    qIns := qInsDev;
    qIns.Database := dbDevelopment;
    qIns.Transaction := trDevelopment;
    qGen.Database := dbDevelopment;
    qGen.Transaction := trDevelopment;
    stpAfterStartTransaction.Database := dbDevelopment;
    stpAfterStartTransaction.Transaction := trDevelopment;
    stpBeforeCommitTransaction.Database := dbDevelopment;
    stpBeforeCommitTransaction.Transaction := trDevelopment;
    clLog := clDevLog;
    qSource := qPatchReports;
    DestName := 'DEVELOPMENT';
  end

  else if ADest = 'P' then
  begin
    qIns := qInsPatch;
    qIns.Database := dbPatch;
    qIns.Transaction := trPatch;
    qGen.Database := dbPatch;
    qGen.Transaction := trPatch;
    stpAfterStartTransaction.Database := dbPatch;
    stpAfterStartTransaction.Transaction := trPatch;
    stpBeforeCommitTransaction.Database := dbPatch;
    stpBeforeCommitTransaction.Transaction := trPatch;
    clLog := clPatchLog;
    qSource := qDevReports;
    DestName := 'PATCH';
  end

  else
    Exit;

  if clLog.RecordCount = 0 then
    Exit;

  if qIns.Transaction.InTransaction then
    qIns.Transaction.Commit;

  clLog.DisableControls;
  try
    clLog.First;

    while not clLog.Eof do
    begin
      qIns.Transaction.StartTransaction;
      stpAfterStartTransaction.ExecProc;

      Err := True;
      try
        try
          if qSource.Locate('sy_report_writer_reports_nbr', VarArrayOf([clLog.FieldByName('nbr').AsInteger]), []) then
          begin
            qIns.ParamByName('report_type').AsString := qSource.FieldByName('report_type').AsString;
            qIns.ParamByName('report_status').AsString := qSource.FieldByName('report_status').AsString;
            qIns.ParamByName('report_description').AsString := qSource.FieldByName('report_description').AsString;
            qIns.ParamByName('report_file').Value := qSource.FieldByName('report_file').Value;
            qIns.ParamByName('notes').Value := qSource.FieldByName('notes').Value;
            qIns.ParamByName('media_type').Value := qSource.FieldByName('media_type').Value;
            qIns.ParamByName('ancestor_class_name').Value := qSource.FieldByName('ancestor_class_name').Value;

            if clLog.FieldByName('status').IsNull then
              qIns.ParamByName('nbr').AsInteger := qSource.FieldByName('sy_report_writer_reports_nbr').AsInteger

            else
              if ADest = 'D' then
                qIns.ParamByName('nbr').AsInteger := qSource.FieldByName('sy_report_writer_reports_nbr').AsInteger
              else
              begin
                qGen.Open;
                qIns.ParamByName('nbr').AsInteger := qGen.Fields[0].AsInteger;
                qGen.Close;
              end;

            qIns.ExecSQL;
          end;

          Err := False;
        except
          on E: Exception do
          begin
            if fl_first_err then
              memErrLog.Lines.Add('---------- ' + DestName + ' ----------');
            fl_first_err := False;
            memErrLog.Lines.Add(clLog.FieldByName('name').AsString + '   ' + E.Message);
            Update;
          end;
        end;

       finally
         if Err then
           qIns.Transaction.Rollback
         else
         begin
           stpBeforeCommitTransaction.ExecProc;
           qIns.Transaction.Commit;
         end;
       end;

      if not Err then
        clLog.Delete
      else
        clLog.Next;
    end;

  finally
    clLog.First;
    clLog.EnableControls;
    Update;
  end;
end;

procedure TfrmSyncReport.memErrLogChange(Sender: TObject);
begin
  tsErrLog.TabVisible := (memErrLog.Lines.Count > 0);
end;

procedure TfrmSyncReport.btnAutoSyncClick(Sender: TObject);
begin
  Busy;
  try
    AutoSync('D');
    AutoSync('P');
  finally
    Ready;
  end;
end;

procedure TfrmSyncReport.AutoSync(ADest: Char);
var
  clMaster, clChild: TevBasicClientDataSet;
begin
  if ADest = 'D' then
  begin
    clMaster := clPatchReport;
    clChild := clDevReport;
  end

  else if ADest = 'P' then
  begin
    clMaster := clDevReport;
    clChild := clPatchReport;
  end

  else
    Exit;

  clMaster.DisableControls;
  try
    clMaster.First;

    clChild.IndexFieldNames := 'nbr';
    while not clMaster.Eof do
    begin
      if not clChild.FindKey([clMaster.FieldByName('nbr').AsInteger]) then
        AddLogRecord(ADest)

      else if clMaster.FieldByName('last_upd').IsNull or
             (clMaster.FieldByName('last_upd').AsDateTime <= FForkDate) or
             (not clChild.FieldByName('last_upd').IsNull and
               (clChild.FieldByName('last_upd').AsDateTime > FForkDate)) then
        clMaster.Next
      else
        AddLogRecord(ADest);
    end;

  finally
    clMaster.First;
    clMaster.EnableControls;
  end;
end;

procedure TfrmSyncReport.AddLogRecord(ADest: Char);
var
  Master: TevBasicClientDataSet;
  Child: TevBasicClientDataSet;
  Log: TevBasicClientDataSet;
  DS: TDataSource;
begin
  if ADest = 'D' then
  begin
    Master := clPatchReport;
    Child := clDevReport;
    Log := clDevLog;
  end
  else if ADest = 'P' then
  begin
    Master := clDevReport;
    Child := clPatchReport;
    Log := clPatchLog;
  end

  else
    Exit;

  Master.DisableControls;
  Child.DisableControls;
  Log.DisableControls;

  try
    Log.Append;
    Log.FieldByName('nbr').AsInteger := Master.FieldByName('nbr').AsInteger;
    Log.FieldByName('type').AsString := Master.FieldByName('type').AsString;
    Log.FieldByName('last_upd_src').Value := Master.FieldByName('last_upd').Value;
    Log.FieldByName('last_upd_dest').Value := Child.FieldByName('last_upd').Value;
    if not Master.FieldByName('status').IsNull then
    begin
      Log.FieldByName('name').AsString := Master.FieldByName('name').AsString;
      Log.FieldByName('status').AsString := 'N';
    end
    else
    begin
      Log.FieldByName('name').AsString := Child.FieldByName('name').AsString;
      Child.Delete;
    end;
    Log.Post;

    DS := Child.MasterSource;
    Child.MasterSource := nil;
    Master.Delete;
    Child.MasterSource := DS;

  finally
    Log.EnableControls;
    Master.EnableControls;
    Child.EnableControls;
  end;
end;

procedure TfrmSyncReport.clDevLogBeforeDelete(DataSet: TDataSet);
begin
  RollbackRecord('D');
end;

procedure TfrmSyncReport.clPatchLogBeforeDelete(DataSet: TDataSet);
begin
  RollbackRecord('P');
end;

procedure TfrmSyncReport.RollbackRecord(ADest: Char);
var
  Master: TevBasicClientDataSet;
  Child: TevBasicClientDataSet;
  Log: TevBasicClientDataSet;
begin
  if FCommiting then
    Exit;

  if ADest = 'D' then
  begin
    Master := clPatchReport;
    Child := clDevReport;
    Log := clDevLog;
  end
  else if ADest = 'P' then
  begin
    Master := clDevReport;
    Child := clPatchReport;
    Log := clPatchLog;
  end

  else
    Exit;

  Master.Append;
  Master.FieldByName('nbr').AsInteger := Log.FieldByName('nbr').AsInteger;
  Master.FieldByName('name').AsString := Log.FieldByName('name').AsString;
  Master.FieldByName('type').AsString := Log.FieldByName('type').AsString;
  Master.FieldByName('status').Value := Log.FieldByName('status').Value;
  Master.FieldByName('last_upd').Value := Log.FieldByName('last_upd_src').Value;
  Master.Post;

  if Log.FieldByName('status').IsNull then
  begin
    Child.Append;
    Child.FieldByName('nbr').AsInteger := Log.FieldByName('nbr').AsInteger;
    Child.FieldByName('name').AsString := Log.FieldByName('name').AsString;
    Child.FieldByName('type').AsString := Log.FieldByName('type').AsString;
    Child.FieldByName('status').Value := Log.FieldByName('status').Value;
    Child.FieldByName('last_upd').Value := Log.FieldByName('last_upd_dest').Value;
    Child.Post;
  end;
end;

constructor TfrmSyncReport.Create(AOwner: TComponent);
begin
  inherited;
  Disconnect;
end;

destructor TfrmSyncReport.Destroy;
begin
  Disconnect;
  inherited;
end;

end.
