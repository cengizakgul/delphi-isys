unit sfldeMainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SDataStructure, Db, Wwdatsrc,  DBClient, wwclient, StdCtrls,
  Grids, Wwdbigrd, Wwdbgrid, Buttons, Mask, wwdbedit, Wwdotdot, Wwdbcomb,
  wwdblook, SDDClasses, ISBasicClasses, kbmMemTable, EvUtils, EvMainboard,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvBasicUtils,
  SDataDictsystem, EvStreamUtils, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, LMDCustomButton, LMDButton,
  isUILMDButton, evCommonInterfaces;

type
  TflseMainForm = class(TFrame)
    cdMain: TevClientDataSet;
    cdDetail: TevClientDataSet;
    dsMain: TevDataSource;
    dsDetail: TevDataSource;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    gMain: TevDBGrid;
    evLabel1: TevLabel;
    gDetail: TevDBGrid;
    cdMainNBR: TIntegerField;
    cdMainNAME: TStringField;
    cdDetailMASTER_NBR: TIntegerField;
    cdDetailTABLE_NAME: TStringField;
    cdDetailFIELD_NAME: TStringField;
    evLabel2: TevLabel;
    bSave: TevBitBtn;
    bCancel: TevBitBtn;
    cdMainTAG: TStringField;
    cdDetailFIELD_TYPE: TStringField;
    FieldMetaTypes: TevDBComboBox;
    cdDetailFIELD_LENGTH: TIntegerField;
    luMove: TevDBLookupCombo;
    bMove: TevBitBtn;
    bCopy: TevBitBtn;
    procedure bSaveClick(Sender: TObject);
    procedure cdMainAfterInsert(DataSet: TDataSet);
    procedure cdDetailAfterInsert(DataSet: TDataSet);
    procedure cdMainAfterScroll(DataSet: TDataSet);
    procedure bCancelClick(Sender: TObject);
    procedure gMainExit(Sender: TObject);
    procedure cdDetailBeforeInsert(DataSet: TDataSet);
    procedure bMoveClick(Sender: TObject);
    procedure bCopyClick(Sender: TObject);
  private
    { Private declarations }
    FNbrCounter: Integer;
    FRecord: Variant;
    bInit: boolean;
    procedure PopulateScreenControls(const m: IisStream);
    procedure SaveScreenControls(const m: IisStream);
  public
    procedure Init;
    procedure CloseQuery(var CanClose: Boolean);
  end;

var
  flseMainForm: TflseMainForm;

implementation

uses EvConsts, RegistryFunctions, SFieldCodeValues, Variants,
  IniFiles;

{$R *.DFM}

procedure TflseMainForm.Init;
var
  m: IisStream;
  sPwd: String;
begin
  if not bInit then
  begin
    if Context.UserAccount.AccountType <> uatSBAdmin then
    begin
      sPwd := '';
      if EvDialog('Password', 'Please provide ADMIN password', sPwd, True) then
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(ctx_DomainInfo.AdminAccount, sDefaultDomain), HashPassword(sPwd))
      else
        Exit;
    end;

    FieldMetaTypes.Items.Text := FieldMetaType_ComboChoices;
    ctx_DataAccess.OpenDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
    cdDetail.CreateDataSet;
    cdMain.CreateDataSet;
    if DM_SYSTEM_MISC.SY_STORAGE.Locate('TAG', FieldLevelStructureStorageTag, []) then
    begin
      m := DM_SYSTEM_MISC.SY_STORAGE.GetBlobData('STORAGE_DATA');
      PopulateScreenControls(m);
    end;
    bInit := True;
  end;
end;

procedure TflseMainForm.bSaveClick(Sender: TObject);
var
  m: IisStream;
begin
  if not DM_SYSTEM_MISC.SY_STORAGE.Locate('TAG', FieldLevelStructureStorageTag, []) then
  begin
    DM_SYSTEM_MISC.SY_STORAGE.Append;
    DM_SYSTEM_MISC.SY_STORAGE['TAG'] := 'HIDDEN_FIELDS';
  end
  else
    DM_SYSTEM_MISC.SY_STORAGE.Edit;

  m := TisStream.Create;
  SaveScreenControls(m);
  m.Position := 0;
  (DM_SYSTEM_MISC.SY_STORAGE.FieldByName('STORAGE_DATA') as TBlobField).LoadFromStream(m.RealStream);

  DM_SYSTEM_MISC.SY_STORAGE.Post;
  ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
  cdDetail.MergeChangeLog;
  cdMain.MergeChangeLog;
end;

procedure TflseMainForm.PopulateScreenControls(const m: IisStream);
var
  lcdMain, lcdDetail: TevClientDataSet;
begin
  DecodeFieldLevelSecurityStructure(m, lcdMain, lcdDetail);
  try
    cdMain.Data := lcdMain.Data;
    cdDetail.Data := lcdDetail.Data;
  finally
    lcdMain.Free;
    lcdDetail.Free;
  end;
end;

procedure TflseMainForm.SaveScreenControls(const m: IisStream);
begin
  m.Position := 0;
  cdMain.CheckBrowseMode;
  cdDetail.CheckBrowseMode;
  m.WriteInteger(cdMain.RecordCount);
  cdMain.First;
  while not cdMain.Eof do
  begin
    m.WriteString(cdMain['TAG']);
    m.WriteString(cdMain['NAME']);
    m.WriteInteger(cdDetail.RecordCount);
    cdDetail.First;
    while not cdDetail.Eof do
    begin
      m.WriteString(cdDetail['TABLE_NAME']);
      m.WriteString(cdDetail['FIELD_NAME']);
      m.WriteString(cdDetail['FIELD_TYPE']);
      m.WriteInteger(cdDetail['FIELD_LENGTH']);
      cdDetail.Next;
    end;
    cdMain.Next;
  end;
end;

procedure TflseMainForm.cdMainAfterInsert(DataSet: TDataSet);
begin
  Dec(FNbrCounter);
  DataSet['MAIN_NBR'] := FNbrCounter;
end;

procedure TflseMainForm.cdDetailAfterInsert(DataSet: TDataSet);
begin
  DataSet['MAIN_NBR'] := cdMain['MAIN_NBR'];
  Dataset['TABLE_NAME;FIELD_NAME;FIELD_TYPE;FIELD_LENGTH'] := FRecord;
end;

procedure TflseMainForm.cdMainAfterScroll(DataSet: TDataSet);
begin
  if VarIsNull(DataSet['MAIN_NBR']) then Exit;
  cdDetail.Filter := 'MAIN_NBR = '+ IntToStr(DataSet['MAIN_NBR']);
  cdDetail.Filtered := True;
end;

procedure TflseMainForm.CloseQuery(var CanClose: Boolean);
begin
  CanClose := (cdMain.ChangeCount = 0) and (cdDetail.ChangeCount = 0);
  if not CanClose then
    ShowMessage('You have to save or cancel changes first');
end;

procedure TflseMainForm.bCancelClick(Sender: TObject);
begin
  cdDetail.CancelUpdates;
  cdMain.CancelUpdates;
end;

procedure TflseMainForm.gMainExit(Sender: TObject);
begin
  if TevDBGrid(Sender).DataSource.DataSet.Active then
   TevDBGrid(Sender).DataSource.DataSet.CheckBrowseMode;
end;

procedure TflseMainForm.cdDetailBeforeInsert(DataSet: TDataSet);
begin
  FRecord := Dataset['TABLE_NAME;FIELD_NAME;FIELD_TYPE;FIELD_LENGTH'];
end;

procedure TflseMainForm.bMoveClick(Sender: TObject);
begin
  if cdDetail.RecordCount > 0 then
  begin
    dsDetail.Edit;
    cdDetail['MAIN_NBR'] := StrToInt(luMove.LookupValue);
    cdDetail.Post;
  end;
end;

procedure TflseMainForm.bCopyClick(Sender: TObject);
begin
  if cdDetail.RecordCount > 0 then
  begin
    with TevClientDataSet.Create(nil) do
    try
      CloneCursor(cdDetail, True);
      Insert;
      FieldValues['MAIN_NBR'] := StrToInt(luMove.LookupValue);
      FieldValues['TABLE_NAME;FIELD_NAME;FIELD_TYPE;FIELD_LENGTH'] := cdDetail['TABLE_NAME;FIELD_NAME;FIELD_TYPE;FIELD_LENGTH'];
      Post;
    finally
      Free;
    end;
  end;
end;

end.
