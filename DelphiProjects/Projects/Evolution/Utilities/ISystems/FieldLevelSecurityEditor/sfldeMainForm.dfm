object flseMainForm: TflseMainForm
  Left = 0
  Top = 0
  Width = 575
  Height = 414
  TabOrder = 0
  DesignSize = (
    575
    414)
  object evLabel1: TevLabel
    Left = 8
    Top = 8
    Width = 68
    Height = 13
    Caption = 'Name all items'
  end
  object evLabel2: TevLabel
    Left = 264
    Top = 8
    Width = 108
    Height = 13
    Caption = 'List all tables and fields'
  end
  object gMain: TevDBGrid
    Left = 8
    Top = 24
    Width = 241
    Height = 321
    DisableThemesInTitle = False
    Selected.Strings = (
      'TAG'#9'15'#9'Tag'#9'F'
      'NAME'#9'32'#9'Name'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TflseMainForm\gMain'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Anchors = [akLeft, akTop, akBottom]
    DataSource = dsMain
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgTabExitsOnLastCol, dgDblClickColSizing]
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnExit = gMainExit
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object gDetail: TevDBGrid
    Left = 264
    Top = 24
    Width = 299
    Height = 321
    DisableThemesInTitle = False
    ControlType.Strings = (
      'FIELD_TYPE;CustomEdit;FieldMetaTypes')
    Selected.Strings = (
      'TABLE_NAME'#9'28'#9'Table name'#9'F'
      'FIELD_NAME'#9'30'#9'Field name'#9'F'
      'FIELD_TYPE'#9'10'#9'Type'#9'F'
      'FIELD_LENGTH'#9'5'#9'Length'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TflseMainForm\gDetail'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsDetail
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgTabExitsOnLastCol, dgDblClickColSizing]
    ReadOnly = False
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnExit = gMainExit
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object bSave: TevBitBtn
    Left = 482
    Top = 352
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save'
    TabOrder = 6
    OnClick = bSaveClick
    Color = clBlack
    Margin = 0
  end
  object bCancel: TevBitBtn
    Left = 386
    Top = 352
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 5
    OnClick = bCancelClick
    Color = clBlack
    Margin = 0
  end
  object FieldMetaTypes: TevDBComboBox
    Left = 384
    Top = 96
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataSource = dsDetail
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 7
    UnboundDataType = wwDefault
  end
  object luMove: TevDBLookupCombo
    Left = 8
    Top = 352
    Width = 185
    Height = 21
    Anchors = [akLeft, akBottom]
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'NAME'#9'40'#9'NAME'#9'F')
    LookupTable = cdMain
    LookupField = 'MAIN_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object bMove: TevBitBtn
    Left = 202
    Top = 352
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Move'
    TabOrder = 3
    OnClick = bMoveClick
    Color = clBlack
    Margin = 0
  end
  object bCopy: TevBitBtn
    Left = 283
    Top = 352
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Copy'
    TabOrder = 4
    OnClick = bCopyClick
    Color = clBlack
    Margin = 0
  end
  object cdMain: TevClientDataSet
    AfterInsert = cdMainAfterInsert
    AfterScroll = cdMainAfterScroll
    Left = 24
    Top = 16
    object cdMainNBR: TIntegerField
      FieldName = 'MAIN_NBR'
      Required = True
    end
    object cdMainTAG: TStringField
      DisplayLabel = 'Tag'
      FieldName = 'TAG'
      Size = 15
    end
    object cdMainNAME: TStringField
      FieldName = 'NAME'
      Required = True
      Size = 40
    end
  end
  object cdDetail: TevClientDataSet
    FieldDefs = <
      item
        Name = 'MAIN_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'TABLE_NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'FIELD_NAME'
        Attributes = [faRequired]
        DataType = ftString
        Size = 40
      end
      item
        Name = 'FIELD_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FIELD_LENGTH'
        DataType = ftInteger
      end>
    BeforeInsert = cdDetailBeforeInsert
    AfterInsert = cdDetailAfterInsert
    Left = 24
    Top = 56
    object cdDetailMASTER_NBR: TIntegerField
      FieldName = 'MAIN_NBR'
      Required = True
    end
    object cdDetailTABLE_NAME: TStringField
      FieldName = 'TABLE_NAME'
      Required = True
      Size = 40
    end
    object cdDetailFIELD_NAME: TStringField
      FieldName = 'FIELD_NAME'
      Required = True
      Size = 40
    end
    object cdDetailFIELD_TYPE: TStringField
      DisplayLabel = 'Type'
      FieldName = 'FIELD_TYPE'
      Required = True
      Size = 1
    end
    object cdDetailFIELD_LENGTH: TIntegerField
      DisplayLabel = 'Length'
      FieldName = 'FIELD_LENGTH'
      Required = True
    end
  end
  object dsMain: TevDataSource
    DataSet = cdMain
    Left = 64
    Top = 16
  end
  object dsDetail: TevDataSource
    DataSet = cdDetail
    Left = 64
    Top = 56
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 104
    Top = 16
  end
end
