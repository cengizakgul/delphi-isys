object EvSystemDBUtils: TEvSystemDBUtils
  Left = 0
  Top = 0
  Width = 605
  Height = 261
  TabOrder = 0
  object evPageControl1: TevPageControl
    Left = 0
    Top = 0
    Width = 605
    Height = 261
    ActivePage = tsDBVersion
    Align = alClient
    Style = tsFlatButtons
    TabOrder = 0
    object tsDBVersion: TTabSheet
      Caption = 'DB Version'
      object evLabel1: TevLabel
        Left = 16
        Top = 13
        Width = 96
        Height = 13
        Caption = 'System DB Path:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 16
        Top = 48
        Width = 88
        Height = 13
        Caption = 'Current Version'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lDBPath: TevLabel
        Left = 128
        Top = 13
        Width = 33
        Height = 13
        Caption = '           '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edVersion: TevDBEdit
        Left = 128
        Top = 46
        Width = 106
        Height = 21
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '#[#].#[#].#[#].#[#]'
        Picture.AllowInvalidExit = True
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object btnUpdateVersion: TevButton
        Left = 240
        Top = 44
        Width = 65
        Height = 25
        Caption = 'Update'
        TabOrder = 1
        OnClick = btnUpdateVersionClick
        Color = clBlack
        Margin = 0
      end
    end
    object tsKeyRemap: TTabSheet
      Caption = 'NBR Remap'
      ImageIndex = 1
      object Table: TevLabel
        Left = 16
        Top = 24
        Width = 27
        Height = 13
        Caption = 'Table'
      end
      object evLabel3: TevLabel
        Left = 17
        Top = 76
        Width = 60
        Height = 13
        Caption = 'Current NBR'
      end
      object evLabel4: TevLabel
        Left = 159
        Top = 76
        Width = 48
        Height = 13
        Caption = 'New NBR'
      end
      object evLabel5: TevLabel
        Left = 123
        Top = 97
        Width = 12
        Height = 13
        Caption = '-->'
      end
      object cbTables: TevDBComboBox
        Left = 16
        Top = 42
        Width = 224
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 20
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
        OnChange = edOldNbrChange
      end
      object edOldNbr: TevDBEdit
        Left = 17
        Top = 93
        Width = 81
        Height = 21
        Picture.PictureMask = '*#'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = edOldNbrChange
        Glowing = False
      end
      object edNewNbr: TevDBEdit
        Left = 159
        Top = 93
        Width = 81
        Height = 21
        Picture.PictureMask = '*#'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        OnChange = edOldNbrChange
        Glowing = False
      end
      object btnUpdateNbrs: TevButton
        Left = 262
        Top = 92
        Width = 75
        Height = 25
        Caption = 'Update'
        TabOrder = 3
        OnClick = btnUpdateNbrsClick
        Color = clBlack
        Margin = 0
      end
    end
    object tsSyStorage: TTabSheet
      Caption = 'SY_STORAGE Update'
      ImageIndex = 2
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 19
        Height = 13
        Caption = 'Tag'
      end
      object cbSyStorageTag: TevComboBox
        Left = 16
        Top = 32
        Width = 239
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items.Strings = (
          'EVOX_PACKAGE1'
          'EVOX_PACKAGE2'
          'EVOX_PACKAGE3'
          'EVOX_PACKAGE4'
          'EVOX_PACKAGE5'
          'EVOX_PACKAGE6')
      end
      object btnSyStorageUpdate: TButton
        Left = 263
        Top = 30
        Width = 75
        Height = 25
        Caption = 'Updates'
        TabOrder = 1
        OnClick = btnSyStorageUpdateClick
      end
      object btnSyStorageInsert: TButton
        Left = 263
        Top = 65
        Width = 121
        Height = 25
        Caption = 'Add with New Tag'
        TabOrder = 2
        OnClick = btnSyStorageInsertClick
      end
    end
  end
  object OpenDialog_xml: TOpenDialog
    Filter = '*.xml'
    InitialDir = 'c:\Evolution'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 344
    Top = 56
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 488
    Top = 64
  end
end
