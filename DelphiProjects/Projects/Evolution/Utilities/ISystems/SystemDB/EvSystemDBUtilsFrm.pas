unit EvSystemDBUtilsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, wwdbedit, isUIwwDBEdit, ISBasicClasses,
  EvUIComponents, ComCtrls, LMDCustomButton, LMDButton, isUILMDButton,
  EvMainboard, EvContext, EvTypes, EvConsts, evDataSet, EvCommonInterfaces,
  EvUIUtils, EvBasicUtils, EvStreamUtils, SDDClasses, SDataDictsystem,
  SDataStructure, Wwdotdot, Wwdbcomb, isUIwwDBComboBox, EvUtils,
  isBasicUtils, isUtils, isBaseClasses;

type
  TEvSystemDBUtils = class(TFrame)
    evPageControl1: TevPageControl;
    tsDBVersion: TTabSheet;
    tsKeyRemap: TTabSheet;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    edVersion: TevDBEdit;
    btnUpdateVersion: TevButton;
    lDBPath: TevLabel;
    tsSyStorage: TTabSheet;
    Label1: TLabel;
    cbSyStorageTag: TevComboBox;
    btnSyStorageUpdate: TButton;
    btnSyStorageInsert: TButton;
    OpenDialog_xml: TOpenDialog;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    cbTables: TevDBComboBox;
    Table: TevLabel;
    evLabel3: TevLabel;
    edOldNbr: TevDBEdit;
    evLabel4: TevLabel;
    edNewNbr: TevDBEdit;
    evLabel5: TevLabel;
    btnUpdateNbrs: TevButton;
    procedure btnUpdateVersionClick(Sender: TObject);
    procedure btnSyStorageUpdateClick(Sender: TObject);
    procedure btnSyStorageInsertClick(Sender: TObject);
    procedure edOldNbrChange(Sender: TObject);
    procedure btnUpdateNbrsClick(Sender: TObject);
  private
    FData: IevTable;
  public
    procedure Init;
  end;

implementation

{$R *.dfm}

procedure TEvSystemDBUtils.btnUpdateVersionClick(Sender: TObject);
begin
  FData.Edit;
  FData.FieldByName('tag').AsString := 'VER ' + edVersion.Text;
  FData.Post;
  FData.SaveChanges;
  EvMessage('System Database version has been successfully updated', mtInformation, [mbOk]);
end;

procedure TEvSystemDBUtils.btnSyStorageUpdateClick(Sender: TObject);
var
  m: IisStream;
begin
  if Trim(cbSyStorageTag.Text) = '' then
  begin
    EvMessage('Please select Tag', mtInformation, [mbOk]);
    Exit;
  end;

  m := TisStream.Create();
  if OpenDialog_xml.Execute then
  begin
    m.LoadFromFile(OpenDialog_xml.FileName);
    m.Position := 0;
    ctx_DataAccess.OpenDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
    if DM_SYSTEM_MISC.SY_STORAGE.Locate('TAG', Trim(cbSyStorageTag.Text), []) then
    begin
      DM_SYSTEM_MISC.SY_STORAGE.Edit;
      DM_SYSTEM_MISC.SY_STORAGE.UpdateBlobData('STORAGE_DATA', m);
      DM_SYSTEM_MISC.SY_STORAGE.Post;
      try
        ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
      finally
        EvMessage('Done', mtInformation, [mbOk]);
      end;
    end;
  end;
end;

procedure TEvSystemDBUtils.btnSyStorageInsertClick(Sender: TObject);
var
  m: IisStream;
  InputTag: string;
begin
  EvDialog('Tag', 'Please enter new tag', InputTag, False);
  if Trim(InputTag) = '' then Exit;

  InputTag := Uppercase(Trim(InputTag));

  m := TisStream.Create();
  if OpenDialog_xml.Execute then
  begin
    m.LoadFromFile(OpenDialog_xml.FileName);
    m.Position := 0;
    ctx_DataAccess.OpenDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
    if DM_SYSTEM_MISC.SY_STORAGE.Locate('TAG', InputTag, []) then
    begin
      if EvMessage('found same Tag. Do you want to update the existing Tag ?', mtConfirmation, [mbYes,mbNo]) = mrNo
      then Exit;
      DM_SYSTEM_MISC.SY_STORAGE.Edit;
    end
    else begin
      if EvMessage('Do you want to insert "' +InputTag+'" Tag ?', mtConfirmation, [mbYes,mbNo]) = mrNo
      then Exit;
      DM_SYSTEM_MISC.SY_STORAGE.Append;
      DM_SYSTEM_MISC.SY_STORAGE.FieldByName('Tag').asString := InputTag;
    end;
    DM_SYSTEM_MISC.SY_STORAGE.UpdateBlobData('STORAGE_DATA', m);
    DM_SYSTEM_MISC.SY_STORAGE.Post;
    try
      ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
    finally
      EvMessage('Done', mtInformation, [mbOk]);
    end;
  end;
end;

procedure TEvSystemDBUtils.Init;
var
  s: String;
  bAvail: Boolean;
  Lst: TddTableList;
  i: Integer;
begin
  lDBPath.Caption := ctx_DomainInfo.DBLocationList.GetDBPath(DB_System);

  bAvail := Context.UserAccount.AccountType = uatSBAdmin;
  btnUpdateVersion.Enabled := bAvail;
  edVersion.Enabled := bAvail;
  tsKeyRemap.TabVisible := bAvail;
  tsSyStorage.TabVisible := bAvail;

  if not bAvail then
    Exit;

  FData := TevTable.CreateAsQuery('SY_STORAGE', 'SELECT t.sy_storage_nbr, t.tag FROM sy_storage t WHERE {AsOfNow<t>} AND t.tag LIKE ''VER %''');
  FData.Open;
  s := FData.FieldByName('tag').AsString;
  Delete(s, 1, 4);
  if s = '' then
    s := ctx_DBAccess.GetDBVersion(dbtSystem);
  edVersion.Text := s;

  cbTables.Items.Clear;
  Lst := TDM_SYSTEM.GetTableList;
  for i := 0 to High(Lst) do
    cbTables.Items.Add(Lst[i]);
  cbTables.Sorted := True;

  edOldNbr.Text := '';
  edNewNbr.Text := '';
  cbTables.ItemIndex := -1;

  edOldNbrChange(nil);
end;

procedure TEvSystemDBUtils.edOldNbrChange(Sender: TObject);
begin
  btnUpdateNbrs.Enabled := (edOldNbr.EditText <> '') and (edNewNbr.EditText <> '') and (cbTables.ItemIndex <> -1);
end;

procedure TEvSystemDBUtils.btnUpdateNbrsClick(Sender: TObject);
var
  TotalRecsUpd: String;

  procedure UpdateTable(const ATable, AField: String);
  var
    Q: IevQuery;
  begin
    Q := TevQuery.CreateFmt(
      '{RemapKey<%s,%s,%s,%d,%d>}',
      [GetDBNameByTable(ATable), AnsiUpperCase(ATable), AnsiUpperCase(AField),
       StrToInt(edOldNbr.Text), StrToInt(edNewNbr.Text)]);

    Q.Execute;
    if Q.Result.Fields[0].AsInteger > 0 then
      AddStrValue(TotalRecsUpd, Format('%s.%s updated %d records', [ATable, AField, Q.Result.Fields[0].AsInteger]), #13);
  end;

var
  TblCl: TddTableClass;
  ChldList: TddReferencesDesc;
  i: Integer;
  L: IisStringList;
  Q: IevQuery;
begin
  TblCl := GetddTableClassByName(cbTables.Text);
  Assert(Assigned(TblCl));

  Q := TevQuery.CreateFmt('SELECT FIRST 1 rec_version FROM %s WHERE %s_nbr = %s', [TblCl.GetTableName, TblCl.GetTableName, edNewNbr.Text]);
  if Q.Result.RecordCount > 0 then
  begin
    EvErrMessage(Format('Record %s.%s_NBR = %s already exists', [TblCl.GetTableName, TblCl.GetTableName, edNewNbr.Text]));
    edNewNbr.SetFocus;
    Exit;
  end;
  Q := nil;

  ChldList := TblCl.GetChildrenDesc;
  L := TisStringList.CreateUnique;
  L.Add(TblCl.GetTableName);
  for i := 0 to High(ChldList) do
    if ChldList[i].Database.GetDBType = dbtSystem then
      L.Add(ChldList[i].Table.GetTableName);

  if EvMessage('Please confirm remaping key values for the selected table and all referencing tables.'#13#13'Table to be updated:'#13 + L.Text,
       mtConfirmation, mbOKCancel, mbCancel) <> mrOk then
    Exit;

  TotalRecsUpd := '';

  ctx_DBAccess.StartTransaction([dbtSystem]);
  try
    UpdateTable(TblCl.GetTableName, TblCl.GetTableName + '_NBR');

    for i := 0 to High(ChldList) do
      if ChldList[i].Database.GetDBType = dbtSystem then
        UpdateTable(ChldList[i].Table.GetTableName, ChldList[i].DestFields[0]);

    if TotalRecsUpd <> '' then
    begin
      if EvMessage('Please inspect the operation result. Do you want to save it?'#13#13 + TotalRecsUpd, mtConfirmation, [mbYes, mbNo], mbNo) = mrYes then
      begin
        ctx_DBAccess.CommitTransaction;

        edOldNbr.Text := '';
        edNewNbr.Text := '';
        edOldNbrChange(nil);        
      end
      else
        ctx_DBAccess.RollbackTransaction;
    end
    else
      ctx_DBAccess.RollbackTransaction;

  except
    ctx_DBAccess.RollbackTransaction;
  end;

  if TotalRecsUpd = '' then
  begin
    EvErrMessage('No records have been updated. Please check the current NBR value.');
    edOldNbr.SetFocus;
  end;
end;

end.
