// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvISystemsGUIPackage;

interface

uses Classes, SysUtils, Forms, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvTypes, Controls, sSecurityClassDefs, SSecurityInterface, EvStreamUtils,
     SWaitingObjectForm, ISMain, IsUtils, EvUtils, EvContext, EvTransportInterfaces,
     EvConsts, EvUIUtils;

implementation

type
  TiSystemsGUI = class(TisInterfacedObject, IevEvolutionGUI, IevContextCallbacks)
  private
    FWaitDialog: TAdvancedWait;
    FMainForm: TISMainForm;
    FGUIContext: IevContext;
  private
    procedure OnException(Sender: TObject; E: Exception);
    procedure OnShowModalDialog(Sender: TObject);
    procedure OnHideModalDialog(Sender: TObject);
  protected
    procedure DoOnConstruction; override;
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
    procedure CreateMainForm;
    procedure DestroyMainForm;
    procedure ApplySecurity(const Component: TComponent);
    function  GUIContext: IevContext;
    procedure AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure CheckTaskQueueStatus;
    procedure PrintTask(const ATaskID: TisGUID);
    procedure OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure OnWaitingServerResponse;
    procedure OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
    procedure PasswordChangeClose;
  public
    destructor  Destroy; override;
  end;

var
  vEvolutionGUI: IevEvolutionGUI;

function GetISystemsGUI: IevEvolutionGUI;
begin
  if not Assigned(vEvolutionGUI) then
    vEvolutionGUI := TiSystemsGUI.Create as IevEvolutionGUI;
  Result := vEvolutionGUI;
end;


function GetContextCallbacks: IevContextCallbacks;
begin
  Result := GeTiSystemsGUI as IevContextCallbacks;
end;


{ TiSystemsGUI }

procedure TiSystemsGUI.ApplySecurity(const Component: TComponent);
var
  SecurityElement: ISecurityElement;
  i: Integer;
  State: TevSecElementState;
begin
  Component.GetInterface(ISecurityElement, SecurityElement);
  if Assigned(SecurityElement) and (SecurityElement.SGetTag <> '') then
  begin
    State := ctx_AccountRights.GetElementState(SecurityElement.SGetType[1], SecurityElement.SGetTag);
    SecurityElement.SSetCurrentSecurityState(State);
    SecurityElement := nil;
  end;

  for i := 0 to Component.ComponentCount-1 do
    ApplySecurity(Component.Components[i]);
end;

procedure TiSystemsGUI.AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
begin
  // plug
end;

function TiSystemsGUI.GUIContext: IevContext;
begin
  Result := FGUIContext;
end;

procedure TiSystemsGUI.CreateMainForm;
var
  Frm: TForm;
begin
  if not Assigned(FMainForm) then
  begin
    FGUIContext := Mainboard.ContextManager.GetThreadContext;
    Application.ShowMainForm := False;
    Application.CreateForm(TISMainForm, Frm);
    FMainForm := TISMainForm(Frm);
    FMainForm.Show;
  end;
end;

destructor TiSystemsGUI.Destroy;
begin
  Application.OnModalBegin := nil;
  Application.OnModalEnd := nil;
  Application.OnException := nil;

  FreeandNil(FWaitDialog);
  inherited;
end;

procedure TiSystemsGUI.DestroyMainForm;
begin
  FreeAndNil(FMainForm);
  FGUIContext := nil;
end;

procedure TiSystemsGUI.DoOnConstruction;
begin
  inherited;

  Application.OnModalBegin := OnShowModalDialog;
  Application.OnModalEnd := OnHideModalDialog;
  Application.OnException := OnException;

  FWaitDialog := TAdvancedWait.Create;
end;

procedure TiSystemsGUI.EndWait;
begin
  if not UnAttended then
    FWaitDialog.EndWait;
end;

procedure TiSystemsGUI.StartWait(const AText: string; AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TiSystemsGUI.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

procedure TiSystemsGUI.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TiSystemsGUI.OnException(Sender: TObject; E: Exception);
begin
  FWaitDialog.KillWait;
  EvExceptMessage(E);
end;

procedure TiSystemsGUI.OnHideModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.ShowIfHidden;
end;

procedure TiSystemsGUI.OnShowModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.Hide;
end;

procedure TiSystemsGUI.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TiSystemsGUI.OnGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TiSystemsGUI.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;

procedure TiSystemsGUI.OnWaitingServerResponse;
begin
// a plug
end;

procedure TiSystemsGUI.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TiSystemsGUI.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
//a plug
end;

procedure TiSystemsGUI.AddTaskQueue(const ATaskList: IisList;
  const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TiSystemsGUI.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

procedure TiSystemsGUI.PasswordChangeClose;
begin
// a plug
end;

initialization
  DisableProcessWindowsGhosting;
  Mainboard.ModuleRegister.RegisterModule(@GetISystemsGUI, IevEvolutionGUI, 'Evolution GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetContextCallbacks, IevContextCallbacks, 'Context Callbacks');

end.


