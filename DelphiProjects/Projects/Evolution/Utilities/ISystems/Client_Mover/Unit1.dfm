object FormA1: TFormA1
  Left = 0
  Top = 0
  Width = 702
  Height = 390
  TabOrder = 0
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 63
    Height = 13
    Caption = 'From IB path:'
  end
  object Label2: TLabel
    Left = 8
    Top = 48
    Width = 53
    Height = 13
    Caption = 'To IB path:'
  end
  object Label3: TLabel
    Left = 8
    Top = 80
    Width = 43
    Height = 13
    Caption = 'Client ID:'
  end
  object Label4: TLabel
    Left = 8
    Top = 360
    Width = 54
    Height = 13
    Caption = 'Script Files:'
    Visible = False
  end
  object Label5: TLabel
    Left = 8
    Top = 104
    Width = 56
    Height = 13
    Caption = 'GBAK.EXE:'
  end
  object Label6: TLabel
    Left = 8
    Top = 136
    Width = 51
    Height = 13
    Caption = 'ISQL.EXE:'
  end
  object lStatus: TLabel
    Left = 8
    Top = 342
    Width = 5
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 232
    Top = 80
    Width = 112
    Height = 13
    Caption = 'Alternative To Client ID:'
  end
  object btnisql: TevSpeedButton
    Left = 479
    Top = 135
    Width = 23
    Height = 22
    HideHint = True
    AutoSize = False
    OnClick = btnisqlClick
    NumGlyphs = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      5555555555555555555555555555555555555555555555555555555555555555
      555555555555555555555555555555555555555FFFFFFFFFF555550000000000
      55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
      B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
      000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
      555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
      55555575FFF75555555555700007555555555557777555555555555555555555
      5555555555555555555555555555555555555555555555555555}
    ParentColor = False
    ShortCut = 0
  end
  object btngbak: TevSpeedButton
    Left = 479
    Top = 103
    Width = 23
    Height = 22
    HideHint = True
    AutoSize = False
    OnClick = btngbakClick
    NumGlyphs = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      5555555555555555555555555555555555555555555555555555555555555555
      555555555555555555555555555555555555555FFFFFFFFFF555550000000000
      55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
      B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
      000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
      555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
      55555575FFF75555555555700007555555555557777555555555555555555555
      5555555555555555555555555555555555555555555555555555}
    ParentColor = False
    ShortCut = 0
  end
  object edClNbr: TEdit
    Left = 104
    Top = 80
    Width = 97
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object edScripts: TEdit
    Left = 104
    Top = 360
    Width = 369
    Height = 21
    TabOrder = 2
    Text = 'C:\evolution\SQL\CopyCLtoCL.sql'
    Visible = False
  end
  object edGBak: TEdit
    Left = 104
    Top = 104
    Width = 369
    Height = 21
    TabOrder = 3
    Text = 'C:\Program Files\Firebird\Firebird_2_0\bin\gbak.exe'
  end
  object bGo: TButton
    Left = 488
    Top = 356
    Width = 75
    Height = 25
    Caption = 'Go'
    Default = True
    TabOrder = 5
    OnClick = bGoClick
  end
  object edISQL: TEdit
    Left = 104
    Top = 136
    Width = 369
    Height = 21
    TabOrder = 4
    Text = 'C:\Program Files\Firebird\Firebird_2_0\bin\isql.exe'
  end
  object edToClNbr: TEdit
    Left = 376
    Top = 80
    Width = 97
    Height = 21
    TabOrder = 1
  end
  object evMemo1: TevMemo
    Left = 8
    Top = 168
    Width = 553
    Height = 81
    Enabled = False
    Lines.Strings = (
      'CONNECT "data:/db/programmer/cengiz/CL_%CL.gdb"'
      'USER "SYSDBA"'
      'PASSWORD "pps97";'
      
        'update cl set name= '#39'%CP'#39'||name, custom_client_number='#39'%CP'#39'||cus' +
        'tom_client_number,CL_NBR=%CL;'
      
        'update co set custom_company_number='#39'%CP'#39'||custom_company_number' +
        ',name='#39'%CP'#39'||name;')
    TabOrder = 6
  end
  object evMemo2: TevMemo
    Left = 8
    Top = 256
    Width = 553
    Height = 89
    TabOrder = 7
  end
  object evcbMoverFrom: TevComboBox
    Left = 103
    Top = 14
    Width = 370
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
    Items.Strings = (
      'SY_GL_AGENCY_REPORT'
      'SY_REPORTS_GROUP'
      'SY_REPORTS'
      'SY_REPORT_WRITER_REPORTS'
      'SY_GLOBAL_AGENCY'
      'SY_GL_AGENCY_FIELD_OFFICE'
      'SY_GL_AGENCY_HOLIDAY_EXPS'
      'SY_GL_AGENCY_HOLIDAYS'
      'SY_STATES'
      'SY_STATE_DEPOSIT_FREQ'
      'SY_STATE_EXEMPTIONS'
      'SY_STATE_MARITAL_STATUS'
      'SY_STATE_TAX_CHART'
      'SY_SUI'
      'SY_LOCALS'
      'SY_LOCAL_DEPOSIT_FREQ'
      'SY_LOCAL_EXEMPTIONS'
      'SY_LOCAL_MARITAL_STATUS'
      'SY_LOCAL_TAX_CHART'
      'SY_FED_EXEMPTIONS'
      'SY_FED_REPORTING_AGENCY'
      'SY_FED_TAX_PAYMENT_AGENCY'
      'SY_FED_TAX_TABLE'
      'SY_FED_TAX_TABLE_BRACKETS'
      'SY_REPORT_GROUPS'
      'SY_REPORT_GROUP_MEMBERS'
      'SY_COUNTY'
      'SY_DELIVERY_METHOD'
      'SY_DELIVERY_SERVICE'
      'SY_EXCEPTIONS'
      'SY_HR_EEO'
      'SY_HR_ETHNICITY'
      'SY_HR_HANDICAPS'
      'SY_HR_INJURY_CODES'
      'SY_HR_OSHA_ANATOMIC_CODES'
      'SY_MEDIA_TYPE'
      'SY_QUEUE_PRIORITY'
      'SY_RECIPROCATED_STATES')
  end
  object evcbMoverTo: TevComboBox
    Left = 103
    Top = 46
    Width = 370
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 9
    Items.Strings = (
      'SY_GL_AGENCY_REPORT'
      'SY_REPORTS_GROUP'
      'SY_REPORTS'
      'SY_REPORT_WRITER_REPORTS'
      'SY_GLOBAL_AGENCY'
      'SY_GL_AGENCY_FIELD_OFFICE'
      'SY_GL_AGENCY_HOLIDAY_EXPS'
      'SY_GL_AGENCY_HOLIDAYS'
      'SY_STATES'
      'SY_STATE_DEPOSIT_FREQ'
      'SY_STATE_EXEMPTIONS'
      'SY_STATE_MARITAL_STATUS'
      'SY_STATE_TAX_CHART'
      'SY_SUI'
      'SY_LOCALS'
      'SY_LOCAL_DEPOSIT_FREQ'
      'SY_LOCAL_EXEMPTIONS'
      'SY_LOCAL_MARITAL_STATUS'
      'SY_LOCAL_TAX_CHART'
      'SY_FED_EXEMPTIONS'
      'SY_FED_REPORTING_AGENCY'
      'SY_FED_TAX_PAYMENT_AGENCY'
      'SY_FED_TAX_TABLE'
      'SY_FED_TAX_TABLE_BRACKETS'
      'SY_REPORT_GROUPS'
      'SY_REPORT_GROUP_MEMBERS'
      'SY_COUNTY'
      'SY_DELIVERY_METHOD'
      'SY_DELIVERY_SERVICE'
      'SY_EXCEPTIONS'
      'SY_HR_EEO'
      'SY_HR_ETHNICITY'
      'SY_HR_HANDICAPS'
      'SY_HR_INJURY_CODES'
      'SY_HR_OSHA_ANATOMIC_CODES'
      'SY_MEDIA_TYPE'
      'SY_QUEUE_PRIORITY'
      'SY_RECIPROCATED_STATES')
  end
  object gbakfileDialog: TOpenDialog
    Filter = '*.Exe|*.exe'
    InitialDir = 'c:\'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 528
    Top = 104
  end
  object isqlfileDialog: TOpenDialog
    Filter = '*.Exe|*.exe'
    InitialDir = 'c:\'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 528
    Top = 136
  end
end
