unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ISBasicClasses,  Buttons, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TFormA1 = class(TFrame)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edClNbr: TEdit;
    Label4: TLabel;
    edScripts: TEdit;
    Label5: TLabel;
    edGBak: TEdit;
    bGo: TButton;
    Label6: TLabel;
    edISQL: TEdit;
    lStatus: TLabel;
    Label7: TLabel;
    edToClNbr: TEdit;
    evMemo1: TevMemo;    
    evMemo2: TevMemo;
    btnisql: TevSpeedButton;
    btngbak: TevSpeedButton;
    gbakfileDialog: TOpenDialog;
    isqlfileDialog: TOpenDialog;
    evcbMoverFrom: TevComboBox;
    evcbMoverTo: TevComboBox;
    procedure bGoClick(Sender: TObject);
    function ParseFile(const FileName: string; const p: array of string): TStringList;
    procedure btngbakClick(Sender: TObject);
    procedure btnisqlClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFormA1.bGoClick(Sender: TObject);
  procedure RunProcess(const ExeFileName, ParamLine, ErrorMessage, StatusMessage: string);
  var
    pi: TProcessInformation;
    si: TStartupInfo;
    dwStatus: DWORD;
  begin
    lStatus.Caption := StatusMessage;
    GetStartupInfo(si);
    si.lpReserved := nil;
    si.lpTitle := PChar(StatusMessage);
    si.wShowWindow := SW_MINIMIZE;
    ZeroMemory(@pi, SizeOf(pi));
    if not CreateProcess(nil, PChar('cmd /c "'+ ExeFileName+ '" '+ ParamLine),
      nil, nil, False, CREATE_NEW_CONSOLE, nil, nil, si, pi) then
      RaiseLastOSError
    else
      with pi do
      begin
        repeat
          Update;
          Sleep(1000);
          Assert(GetExitCodeProcess(hProcess, dwStatus));
        until (dwStatus <> STILL_ACTIVE);
        CloseHandle(hThread);
        CloseHandle(hProcess);
        if dwStatus <> 0 then
          raise Exception.CreateFmt('%s'#13'%s'#13'%s (%u)', [ExeFileName, ParamLine, ErrorMessage, dwStatus]);
      end;
  end;
  function ToClNbr: string;
  begin
    if edToClNbr.Text <> '' then
      Result := edToClNbr.Text
    else
      Result := edClNbr.Text;
  end;
  function ToClNbrWithDash: string;
  begin
    if (edToClNbr.Text <> '') and (edToClNbr.Text <> edClNbr.Text) then
      Result := edToClNbr.Text+'_'
    else
      Result := '';
  end;

  Function GetPathName(sPath : String):String;
  begin
   Result := sPath;
   if pos(']', Result) > 0 then
     Result := Trim(Copy(Result, Succ(Pos(']', Result)), Length(Result)));
  end;

var
  l, lScript: TStringList;
  i: Integer;
begin
  bGo.Enabled := False;
  l := TStringList.Create;
  lScript := TStringList.Create;
  try
    try
      l.Sorted := False;

      if StrToIntDef(edClNbr.Text, 0) <= 0 then
        raise Exception.Create('Wrong Client Id');
      if (edToClNbr.Text <> '') and (StrToIntDef(edToClNbr.Text, 0) <= 0) then
        raise Exception.Create('Wrong Alternative To Client Id');
      if not FileExists(edGBak.Text) then
        raise Exception.Create('Wrong GBAK.exe file name');
      if not FileExists(edISQL.Text) then
        raise Exception.Create('Wrong ISQL.exe file name');
      if (evcbMoverFrom.Text = evcbMoverTo.Text) and
         ((StrToInt(edClNbr.Text) = StrToIntDef(edToClNbr.Text, 0)) or (StrToIntDef(edToClNbr.Text, 0) = 0)) then
        raise Exception.Create('Wrong Set up');

      l.Append('sql.sql');

      RunProcess(edGBak.Text, '-b -t -user SYSDBA -pass pps97 '+ GetPathName(evcbMoverFrom.Text) + 'CL_'+ edClNbr.Text+ '.gdb c:\t.gbk',
        'Backup failed', 'Backing up...');
      RunProcess(edGBak.Text, '-user SYSDBA -pass pps97 -p 8192 -rep c:\t.gbk '+ GetPathName(evcbMoverTo.Text) + 'CL_'+ ToClNbr+ '.gdb',
        'Restore failed', 'Restoring...');

      for i := 0 to l.Count-1 do
      begin
        lScript := ParseFile(l[i], ['%CP', ToClNbrWithDash,'%CL', ToClNbr]);
        lScript[0] := 'CONNECT "'+ GetPathName(evcbMoverTo.Text)+ 'CL_'+ ToClNbr+ '.gdb"';
        evMemo2.Lines.CommaText := lScript.CommaText;

        lScript.SaveToFile('c:\t.sql');
        RunProcess(edISQL.Text, '-i c:\t.sql',
         'Patch script '+ l[i]+ ' failed', 'Applying '+ l[i]+ '...');
      end;
      lStatus.Caption := 'Client '+ edClNbr.Text+ ' moved. Please Rebuild Temp tables';
    except
      lStatus.Caption := '';
      raise;
    end;
  finally
    l.Free;
    lScript.Free;
    edClNbr.Text := '';
    bGo.Enabled := True;
  end;
end;

function TFormA1.ParseFile(const FileName: string;
  const p: array of string): TStringList;
  procedure Subst(var s: string; const Key, Value: string);
  var
    i: Integer;
  begin
    repeat
      i := Pos(Key, s);
      if i > 0 then
      begin
        Delete(s, i, Length(Key));
        Insert(Value, s, i);
      end;
    until i = 0;
  end;
var
  l: TStringList;
  i, j: Integer;
  s: string;
begin
  Assert(Length(p) mod 2 = 0);
  Result := TStringList.Create;
  l := TStringList.Create;
  l.CommaText := evMemo1.Lines.CommaText;
  try

    for i := 0 to l.Count-1 do
    begin
      s := l[i];
      if s <> '' then
      begin
        j := 0;
        while j <= High(p) do
        begin
          Subst(s, p[j], p[j+1]);
          Inc(j, 2);
        end;
        Result.Append(s);
      end;
    end;
  finally
    l.Free;
  end;
end;


procedure TFormA1.btngbakClick(Sender: TObject);
begin
    if gbakfileDialog.Execute then
      edGBak.Text := gbakfileDialog.FileName;
end;

procedure TFormA1.btnisqlClick(Sender: TObject);
begin
    if isqlfileDialog.Execute then
      edISQL.Text := isqlfileDialog.FileName;
end;

end.
