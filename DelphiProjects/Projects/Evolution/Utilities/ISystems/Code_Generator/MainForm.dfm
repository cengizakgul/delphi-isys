object MainFormFrm: TMainFormFrm
  Left = 0
  Top = 0
  Width = 1016
  Height = 617
  ParentShowHint = False
  ShowHint = True
  TabOrder = 0
  object Label2: TLabel
    Left = 8
    Top = 206
    Width = 65
    Height = 13
    Caption = 'License Code'
  end
  object CopySb: TSpeedButton
    Left = 80
    Top = 202
    Width = 23
    Height = 22
    Hint = 'Copy code to clipboard'
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888800000008888888888888888800000008888888444444444800000008888
      8884FFFFFFF48000000088888884F00000F48000000080000004FFFFFFF48000
      000080FFFFF4F00000F48000000080F00004FFFFFFF48000000080FFFFF4F00F
      44448000000080F00004FFFF4F488000000080FFFFF4FFFF44888000000080F0
      0F04444448888000000080FFFF0F088888888000000080FFFF00888888888000
      0000800000088888888880000000888888888888888880000000888888888888
      888880000000}
    OnClick = CopySbClick
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 66
    Height = 13
    Caption = 'Serial Number'
  end
  object Label3: TLabel
    Left = 8
    Top = 55
    Width = 72
    Height = 13
    Caption = 'Expiration Date'
  end
  object Label4: TLabel
    Left = 8
    Top = 104
    Width = 85
    Height = 13
    Caption = 'Computer Modifier'
  end
  object Label5: TLabel
    Left = 144
    Top = 8
    Width = 65
    Height = 13
    Caption = 'License Items'
  end
  object BtnGenerate: TButton
    Left = 8
    Top = 163
    Width = 449
    Height = 25
    Hint = 'Display code generation dialog'
    Caption = '&Generate License'
    Default = True
    Enabled = False
    TabOrder = 4
    OnClick = BtnGenerateClick
  end
  object edSerialNumber: TEdit
    Left = 8
    Top = 24
    Width = 129
    Height = 21
    TabOrder = 0
    Text = '0'
    OnChange = edSerialNumberChange
  end
  object dpExpDate: TDateTimePicker
    Left = 8
    Top = 71
    Width = 129
    Height = 21
    Date = 37266.751407870400000000
    Time = 37266.751407870400000000
    TabOrder = 1
  end
  object edModifier: TEdit
    Left = 8
    Top = 120
    Width = 129
    Height = 21
    TabOrder = 2
    OnChange = edSerialNumberChange
  end
  object cbLicense: TCheckListBox
    Left = 144
    Top = 24
    Width = 314
    Height = 117
    Columns = 2
    ItemHeight = 13
    Style = lbOwnerDrawFixed
    TabOrder = 3
    OnDrawItem = cbLicenseDrawItem
  end
  object memLicenseCode: TMemo
    Left = 8
    Top = 227
    Width = 448
    Height = 177
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 5
    WantReturns = False
  end
  object GroupBox1: TGroupBox
    Left = 488
    Top = 16
    Width = 273
    Height = 145
    Caption = 'DB Downgrade'
    TabOrder = 6
    object Label6: TLabel
      Left = 16
      Top = 24
      Width = 85
      Height = 13
      Caption = 'Computer Modifier'
    end
    object Label7: TLabel
      Left = 16
      Top = 80
      Width = 58
      Height = 13
      Caption = 'License Key'
    end
    object btnCopyToClipboard: TSpeedButton
      Left = 80
      Top = 75
      Width = 23
      Height = 22
      Hint = 'Copy key to clipboard'
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888800000008888888888888888800000008888888444444444800000008888
        8884FFFFFFF48000000088888884F00000F48000000080000004FFFFFFF48000
        000080FFFFF4F00000F48000000080F00004FFFFFFF48000000080FFFFF4F00F
        44448000000080F00004FFFF4F488000000080FFFFF4FFFF44888000000080F0
        0F04444448888000000080FFFF0F088888888000000080FFFF00888888888000
        0000800000088888888880000000888888888888888880000000888888888888
        888880000000}
      OnClick = btnCopyToClipboardClick
    end
    object edCompMod: TEdit
      Left = 16
      Top = 48
      Width = 129
      Height = 21
      TabOrder = 0
      OnChange = edCompModChange
    end
    object edLicenseKey: TEdit
      Left = 16
      Top = 104
      Width = 241
      Height = 21
      TabOrder = 1
    end
    object btnGenerateKey: TButton
      Left = 160
      Top = 46
      Width = 97
      Height = 25
      Caption = 'Generate Key'
      Enabled = False
      TabOrder = 2
      OnClick = btnGenerateKeyClick
    end
  end
end
