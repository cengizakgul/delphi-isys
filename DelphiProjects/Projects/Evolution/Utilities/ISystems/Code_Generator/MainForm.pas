unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, CheckLst, isBaseClasses, EvTypes,
  EvLicenseInfo, Math, evLicenseCodeGenerator, SEncryptionRoutines;

type
  TMainFormFrm = class(TFrame)
    BtnGenerate: TButton;
    Label2: TLabel;
    CopySb: TSpeedButton;
    Label1: TLabel;
    edSerialNumber: TEdit;
    Label3: TLabel;
    dpExpDate: TDateTimePicker;
    Label4: TLabel;
    edModifier: TEdit;
    Label5: TLabel;
    cbLicense: TCheckListBox;
    memLicenseCode: TMemo;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    edCompMod: TEdit;
    Label7: TLabel;
    edLicenseKey: TEdit;
    btnGenerateKey: TButton;
    btnCopyToClipboard: TSpeedButton;
    procedure CopySbClick(Sender: TObject);
    procedure BtnGenerateClick(Sender: TObject);
    procedure edSerialNumberChange(Sender: TObject);
    procedure cbLicenseDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure edCompModChange(Sender: TObject);
    procedure btnGenerateKeyClick(Sender: TObject);
    procedure btnCopyToClipboardClick(Sender: TObject);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation


{$R *.DFM}

procedure TMainFormFrm.CopySbClick(Sender: TObject);
var
  OldSelStart : Integer;
begin
  OldSelStart := memLicenseCode.SelStart;
  memLicenseCode.SelStart := 0;
  memLicenseCode.SelLength := MaxInt;
  memLicenseCode.CopyToClipboard;
  memLicenseCode.SelStart := OldSelStart;
  memLicenseCode.SelLength := 0;
end;

procedure TMainFormFrm.BtnGenerateClick(Sender: TObject);
var
  i: Integer;
  Modules: array [0..LIC_TOP] of Boolean;
  APIKeysList, Details: IisListOfValues;
  k: TevVendorKeyType;
begin
  APIKeysList := TisListOfValues.Create;

  for i := 0 to cbLicense.Items.Count - 1 do
    if i <= LIC_TOP then
    begin
      Modules[i] := cbLicense.Checked[i];
    end
    else
    begin
      if cbLicense.Checked[i] then
      begin
        k := TevVendorKeyType(Integer(Pointer(cbLicense.Items.Objects[i])));
        Details := TisListOfValues.Create;
        Details.AddValue('APIKeyApplicationName', APIVendorKeys[k].AppName);
        Details.AddValue('APIKeyVendorCustomNbr', APIVendorKeys[k].VendorID);
        Details.AddValue('APIKeyVendorName', APIVendorKeys[k].VendorName);
        Details.AddValue('APIKey', APIVendorKeys[k].AppKey);
        APIKeysList.AddValue(APIVendorKeys[k].AppKey, Details);
      end;
    end;
      
  memLicenseCode.Text := GenerateEvLicense(EdModifier.Text, StrToInt(EdSerialNumber.Text), dpExpDate.Date,
  Modules, APIKeysList);
end;

procedure TMainFormFrm.edSerialNumberChange(Sender: TObject);
begin
  BtnGenerate.Enabled := (edSerialNumber.Text <> '') and (edModifier.Text <> '');
end;

constructor TMainFormFrm.Create(AOwner: TComponent);
var
  i: Integer;
  v: TevVendorKeyType;
begin
  inherited;
  dpExpDate.DateTime := Date + 1;
  for i := 0 to LIC_TOP do
  begin
    cbLicense.AddItem(lDescr[i].Description, nil);
    cbLicense.Checked[i] := lDefaults[i];
  end;

  for v := Low(APIVendorKeys) to High(APIVendorKeys) do
    cbLicense.AddItem(APIVendorKeys[v].AppName, Pointer(Integer(Ord(v))));
end;

procedure TMainFormFrm.cbLicenseDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  if odSelected in State then
    cbLicense.Canvas.Font.Color := clWhite
  else
  begin
    if Index > LIC_TOP then
      cbLicense.Canvas.Font.Color := clGreen
    else
      cbLicense.Canvas.Font.Color := clBlack;
  end;

  cbLicense.Canvas.TextRect(Rect, Rect.Left, Rect.Top, cbLicense.Items[Index]);
end;

procedure TMainFormFrm.edCompModChange(Sender: TObject);
begin
  BtnGenerateKey.Enabled := edCompMod.Text <> '';
end;

procedure TMainFormFrm.btnGenerateKeyClick(Sender: TObject);
begin
  edLicenseKey.Text := EncryptHexString(edCompMod.Text + DateToStr(Date));
end;

procedure TMainFormFrm.btnCopyToClipboardClick(Sender: TObject);
begin
  edLicenseKey.SelectAll;
  edLicenseKey.CopyToClipboard;
end;

end.
