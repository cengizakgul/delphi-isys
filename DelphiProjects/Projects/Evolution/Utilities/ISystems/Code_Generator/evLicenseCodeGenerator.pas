unit EvLicenseCodeGenerator;

interface

uses isBaseClasses;

function GenerateEvLicense(const AModifier: String; const ASerialNbr: Cardinal;
  const AExpirationDate: TDateTime;
  const AModules: array of Boolean;
  const AAPIKeysList : IisListOfValues): String;

implementation

uses EvLicenseInfo, OnGuard, EvStreamUtils, OgUtil, Math, SEncryptionRoutines, Classes;

function GenerateEvLicense(const AModifier: String; const ASerialNbr: Cardinal;
  const AExpirationDate: TDateTime;
  const AModules: array of Boolean;
  const AAPIKeysList : IisListOfValues): String;
var
  Work: TCode;
  K: TKey;
  Modifier: LongInt;
  SN: Cardinal;
  Code: Word;
  i: Integer;
  LicenseCode: String;
  sAPIKeys, sEncryptedAPIKeys : String;
  APIKeysStream : IisStream;
begin
  // Modules part
  Assert(High(AModules) <= LIC_TOP);

  HexToBuffer(AModifier, Modifier, SizeOf(Modifier));
  K := cKey;

  ApplyModifierToKeyPrim(Modifier, K, SizeOf(K));

  SN := ASerialNbr;
  Code := 0;
  for i := Low(AModules) to High(AModules) do
    if AModules[i] then
      Code := Code or Trunc(IntPower(2, i));
  SN := (SN shl LicenseOptionMaxCount) or Code;

  InitSerialNumberCode(K, SN, AExpirationDate, Work);

  LicenseCode := BufferToHex(Work, SizeOf(Work));


  // API Keys part
  if Assigned(AAPIKeysList) and (AAPIKeysList.Count > 0) then
  begin
    APIKeysStream := TisStream.Create;
    AAPIKeysList.WriteToStream(APIKeysStream);

    sAPIKeys :=  APIKeysStream.AsString;

    EnsureKeyLength(LicenseCode);
    sEncryptedAPIKeys := EncryptString(sAPIKeys, LicenseCode);
    Result := StringOfChar(' ', Length(sEncryptedAPIKeys) * 2);

    BinToHex(PChar(sEncryptedAPIKeys), PChar(Result), Length(sEncryptedAPIKeys));
    
    Result := LicenseCode + Result;
  end
  
  else
    Result := LicenseCode;
end;


end.