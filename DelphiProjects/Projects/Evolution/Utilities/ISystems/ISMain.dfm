object ISMainForm: TISMainForm
  Left = 537
  Top = 226
  Width = 862
  Height = 526
  Caption = 'ISystems Utilities'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 846
    Height = 488
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChanging = PageControl1Changing
    object TabSheet2: TTabSheet
      Caption = 'License Generator'
      ImageIndex = 1
      inline MainFormFrm1: TMainFormFrm
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Build Security'
      ImageIndex = 2
      inline frm1: Tfrm
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        TabOrder = 0
        inherited btnRebuildSec: TButton
          Caption = 'Rebuild Security'
        end
      end
    end
    object tsFieldLevelEditor: TTabSheet
      Caption = 'Field Level Editor'
      ImageIndex = 7
      OnShow = tsFieldLevelEditorShow
      inline flseMainForm1: TflseMainForm
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        TabOrder = 0
        inherited gMain: TevDBGrid
          Height = 367
        end
        inherited gDetail: TevDBGrid
          Width = 562
          Height = 367
        end
        inherited bSave: TevBitBtn
          Left = 745
          Top = 398
        end
        inherited bCancel: TevBitBtn
          Left = 649
          Top = 398
        end
        inherited luMove: TevDBLookupCombo
          Top = 402
        end
        inherited bMove: TevBitBtn
          Top = 402
        end
        inherited bCopy: TevBitBtn
          Top = 402
        end
      end
    end
    object tsSysUtils: TTabSheet
      Caption = 'System DB Utils'
      ImageIndex = 9
      OnShow = tsSysUtilsShow
      inline EvSystemDBUtils: TEvSystemDBUtils
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        TabOrder = 0
        inherited evPageControl1: TevPageControl
          Width = 838
          Height = 460
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Client Mover'
      ImageIndex = 4
      inline Form11: TFormA1
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        TabOrder = 0
        inherited bGo: TButton
          OnClick = Form11bGoClick
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Sync Reports'
      ImageIndex = 3
      inline frmSyncReport1: TfrmSyncReport
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        TabOrder = 0
        inherited PageControl1: TPageControl
          Width = 838
          Height = 460
        end
      end
    end
    object tsDevUtils: TTabSheet
      Caption = 'Dev Utils'
      ImageIndex = 9
      object evLabel4: TevLabel
        Left = 8
        Top = 37
        Width = 82
        Height = 13
        Caption = 'Search Directory '
      end
      object evLabel5: TevLabel
        Left = 8
        Top = 93
        Width = 57
        Height = 13
        Caption = 'Field List file'
      end
      object evLabel2: TevLabel
        Left = 8
        Top = 69
        Width = 51
        Height = 13
        Caption = 'Output File'
      end
      object edDirectory: TevEdit
        Left = 120
        Top = 29
        Width = 321
        Height = 21
        TabOrder = 0
        Text = 'C:\accurev\DelphiProjects'
      end
      object edFieldList: TevEdit
        Left = 120
        Top = 85
        Width = 321
        Height = 21
        Enabled = False
        TabOrder = 1
      end
      object btnStart: TevBitBtn
        Left = 120
        Top = 128
        Width = 75
        Height = 25
        Caption = 'Start'
        TabOrder = 2
        OnClick = btnStartClick
        Color = clBlack
        Margin = 0
      end
      object btnLoad: TevBitBtn
        Left = 456
        Top = 80
        Width = 153
        Height = 25
        Caption = 'Field Load'
        TabOrder = 3
        OnClick = btnLoadClick
        Color = clBlack
        Margin = 0
      end
      object edResult: TevEdit
        Left = 120
        Top = 61
        Width = 321
        Height = 21
        TabOrder = 4
        Text = 'C:\temp\FieldSearchResult.csv'
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'DAT Creator'
      inline MainCreator1: TMainCreator
        Left = 0
        Top = 0
        Width = 838
        Height = 460
        Align = alClient
        TabOrder = 0
      end
    end
    object tsBranding: TTabSheet
      Caption = 'Branding'
      ImageIndex = 7
      object Label2: TLabel
        Left = 13
        Top = 14
        Width = 58
        Height = 13
        Caption = 'Login Image'
      end
      object SpeedButton1: TSpeedButton
        Left = 365
        Top = 11
        Width = 23
        Height = 22
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDD8888888888DDDDDDFFFFFFFFFFDDDDD000000000008
          DDDD8D888888888FDDDD0B03333333308DDD88D888888888FDDD0BB033333333
          08DD888D888888888FDD0BBB03333333308D8888D888888888FD0BBBB0333333
          330888888D888888888F0BBBBB0000000000888888DDDDDDDDDD0BBBBBBBBB08
          DDDD88888888888FDDDD0BBBB000000DDDDD88888888888DDDDDD0000DDDDDDD
          DDDDD8888DDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDDD888DDDDDDDDDD0DDD
          00DDDDDDDDDD8DDD88DDDDDDDDDDD000D0DDDDDDDDDDD888D8DDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        OnClick = SpeedButton1Click
      end
      object Label3: TLabel
        Left = 13
        Top = 46
        Width = 52
        Height = 13
        Caption = 'Title Image'
      end
      object SpeedButton2: TSpeedButton
        Left = 365
        Top = 43
        Width = 23
        Height = 22
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDD8888888888DDDDDDFFFFFFFFFFDDDDD000000000008
          DDDD8D888888888FDDDD0B03333333308DDD88D888888888FDDD0BB033333333
          08DD888D888888888FDD0BBB03333333308D8888D888888888FD0BBBB0333333
          330888888D888888888F0BBBBB0000000000888888DDDDDDDDDD0BBBBBBBBB08
          DDDD88888888888FDDDD0BBBB000000DDDDD88888888888DDDDDD0000DDDDDDD
          DDDDD8888DDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDDD888DDDDDDDDDD0DDD
          00DDDDDDDDDD8DDD88DDDDDDDDDDD000D0DDDDDDDDDDD888D8DDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        OnClick = SpeedButton2Click
      end
      object Edit1: TEdit
        Left = 87
        Top = 11
        Width = 276
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
      end
      object Edit2: TEdit
        Left = 87
        Top = 43
        Width = 276
        Height = 21
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 1
      end
      object Button2: TButton
        Left = 87
        Top = 75
        Width = 75
        Height = 25
        Caption = 'Create File'
        TabOrder = 2
        OnClick = Button2Click
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Images (*.bmp;*.jpg)|*.bmp;*.jpg'
    Left = 500
    Top = 32
  end
  object dIniFileDialog: TOpenDialog
    Filter = '*.cfg|*.cfg'
    InitialDir = 'c:\Evolution'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 464
    Top = 32
  end
end
