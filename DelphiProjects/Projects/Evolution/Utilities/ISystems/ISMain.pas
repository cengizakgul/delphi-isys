// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit ISMain;

interface

{$WARN UNIT_PLATFORM OFF}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, SMainForm, MainForm, SSMainForm, EvContext,
  MainFrm, Unit1, StdCtrls, ShellCtrls, ISBasicClasses, EvMainboard,
   Buttons, IBDatabase, IBQuery, IniFiles, EvTypes, evCommonInterfaces,
  StrUtils, SEncryptionRoutines, EvUtils, sfldeMainForm, CheckLst, EvBasicUtils, EvUIUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, EvStreamUtils, SDDClasses,
  SDataDictsystem, SDataStructure, ExtCtrls, EvConsts, isUIEdit, isBaseClasses, ISBasicUtils,
  EvSystemDBUtilsFrm;

type
  TISMainForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    MainCreator1: TMainCreator;
    TabSheet2: TTabSheet;
    MainFormFrm1: TMainFormFrm;
    TabSheet3: TTabSheet;
    frm1: Tfrm;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Form11: TFormA1;
    frmSyncReport1: TfrmSyncReport;
    tsBranding: TTabSheet;
    Label2: TLabel;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    OpenDialog1: TOpenDialog;
    Label3: TLabel;
    Edit2: TEdit;
    SpeedButton2: TSpeedButton;
    Button2: TButton;
    tsFieldLevelEditor: TTabSheet;
    flseMainForm1: TflseMainForm;
    dIniFileDialog: TOpenDialog;
    tsDevUtils: TTabSheet;
    evLabel4: TevLabel;
    edDirectory: TevEdit;
    edFieldList: TevEdit;
    evLabel5: TevLabel;
    btnStart: TevBitBtn;
    btnLoad: TevBitBtn;
    evLabel2: TevLabel;
    edResult: TevEdit;
    tsSysUtils: TTabSheet;
    EvSystemDBUtils: TEvSystemDBUtils;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Form11bGoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure tsFieldLevelEditorShow(Sender: TObject);
    procedure tsSysUtilsShow(Sender: TObject);
  public
    { Public declarations }
     Settings           : TIniFile;
  end;

const
  pUser = FB_Admin;
  pPassword = 'pps97';

implementation

{$R *.dfm}

procedure TISMainForm.SpeedButton1Click(Sender: TObject);
begin
  OpenDialog1.FileName := Edit1.Text;
  if OpenDialog1.Execute then
    Edit1.Text := OpenDialog1.FileName;
end;

procedure TISMainForm.SpeedButton2Click(Sender: TObject);
begin
  OpenDialog1.FileName := Edit2.Text;
  if OpenDialog1.Execute then
    Edit2.Text := OpenDialog1.FileName;
end;

procedure TISMainForm.Button2Click(Sender: TObject);
var
  ms: TMemoryStream;
  brd, fs: TFileStream;
  i: Integer;
  OutName: string;
  s: string;

  procedure PutFile(const FileName: string);
  begin
    ms.Clear;
    i := 0;
    if FileExists(FileName) then
    begin
      fs := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
      try
        EncryptMemoryStream(fs, ms);
        i := ms.Size;
      finally
        fs.Free;
      end;
    end;
    brd.WriteBuffer(i, SizeOf(i));
    if i > 0 then
    begin
      s := RightStr(ExtractFileExt(FileName), 3);
      brd.WriteBuffer(s[1], 3);
      brd.CopyFrom(ms, 0);
    end;
  end;
begin
  ms := TMemoryStream.Create;
  OutName := ExtractFilePath(Application.ExeName) + 'Evolution.brd';
  brd := TFileStream.Create(OutName, fmCreate);
  try
    PutFile(Edit1.Text);
    PutFile(Edit2.Text);
  finally
    brd.Free;
    ms.Free;
  end;
  EvMessage('File ' + OutName + ' created');
end;

procedure TISMainForm.PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
begin
  if PageControl1.ActivePage = tsFieldLevelEditor then
    flseMainForm1.CloseQuery(AllowChange)
end;

procedure TISMainForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  flseMainForm1.CloseQuery(CanClose);
end;

procedure TISMainForm.Form11bGoClick(Sender: TObject);
begin
  Form11.bGoClick(Sender);

end;

procedure TISMainForm.FormCreate(Sender: TObject);
var
 I : integer;
 S : String;
begin
    Settings  := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\isystems.Ini');

    Form11.evcbMoverFrom.Clear;
    Settings.ReadSectionValues('ClientMoverFrom', Form11.evcbMoverFrom.Items);
    for I := 0 to Form11.evcbMoverFrom.Items.Count - 1 do
    begin
      S := Form11.evcbMoverFrom.Items[I];
      Delete(S, 1, Pos('=', S));
      Form11.evcbMoverFrom.Items[I] := S;
    end;
    Form11.evcbMoverFrom.ItemIndex := 0;

    Form11.evcbMoverTo.Clear;
    Settings.ReadSectionValues('ClientMoverTo', Form11.evcbMoverTo.Items);
    for I := 0 to Form11.evcbMoverTo.Items.Count - 1 do
    begin
      S := Form11.evcbMoverTo.Items[I];
      Delete(S, 1, Pos('=', S));
      Form11.evcbMoverTo.Items[I] := S;
    end;
    Form11.evcbMoverTo.ItemIndex := 0;

end;

procedure TISMainForm.FormDestroy(Sender: TObject);
begin
    if Assigned(Settings) then
     Settings.Free;
end;

procedure TISMainForm.btnLoadClick(Sender: TObject);
var
  openDialog : TOpenDialog;
begin
  edDirectory.Text := ExpandFileName(edDirectory.Text);
  openDialog := TOpenDialog.Create(self);
  try
     openDialog.InitialDir := 'C:\';
     openDialog.Filter := '*.txt';

     openDialog.Options := [ofFileMustExist];

     if not openDialog.Execute then
        ShowMessage('Open file was cancelled')
     else
       edFieldList.Text:= ExpandFileName(openDialog.FileName);
  finally
     openDialog.Free;
  end;

end;

procedure TISMainForm.btnStartClick(Sender: TObject);
var
  lList: IisStringList;
  outputFile: IisListOfValues;
  i: integer;
  fileName : String;
  vValue: variant;
  rf: TextFile;


  procedure PosInFile(FileName:string; const idx: integer);
  var
    FileData: IisStream;
    fName, sFields, sTableField, sFile, s, fList, tName: String;
    i, countTotalDFM, countTotalPAS, countScreenDfm, countScreenPas,
    countDataModul, countDataDict, countFieldValue: integer;
    sScreenDFM, sScreenPAS, sDataModul, sDataDict, sFieldValue: string;
    bFileList:boolean;

  begin
    fName:= ExtractFileName(FileName);

        FileData := TisStream.CreateFromFile(FileName);
        sFile := AnsiUpperCase(FileData.AsString);
        ctx_UpdateWait('Processing ' + fName);
        for i := 0 to lList.Count-1 do
        begin
            s := lList[i];
            sFields:= GetNextStrValue(s, ',');
            sTableField:= GetNextStrValue(s, ',');
            tName := sTableField;
            tName:= GetNextStrValue(tName, '.');

            if (sFields <> tName +'_NBR') and  Contains(sFile, sFields, True) then
            begin
                countTotalPAS:=0;
                countTotalDFM:=0;
                countScreenPas:= 0;
                countScreenDfm:= 0;
                countDataModul:=0;
                countDataDict:=0;
                countFieldValue:=0;
                sScreenPAS:='N';
                sScreenDFM:='N';
                sDataModul :='N';
                sDataDict:='N';
                sFieldValue:='N';
                fList := '';
                bFileList:=true;
                if outputFile.ValueExists(sTableField)  then
                begin
                    vValue := outputFile.Value[sTableField];

                    countTotalPAS := vValue[0];
                    countTotalDFM := vValue[1];
                    countScreenPas := vValue[2];
                    countScreenDfm := vValue[3];
                    countDataModul:= vValue[4];
                    countDataDict:= vValue[5];
                    countFieldValue:= vValue[6];
                    sScreenPAS:= vValue[7];
                    sScreenDFM:= vValue[8];
                    sDataModul:= vValue[9];
                    sDataDict:= vValue[10];
                    sFieldValue:= vValue[11];
                    fList :=vValue[12];
                end;

                if Contains(FileName,'EvoClient',True) then
                begin
                    if idx = 0 then
                    begin
                       inc(countScreenPas);
                       sScreenPAS:='Y'
                    end
                    else
                    begin
                       inc(countScreenDfm);
                       sScreenDFM:='Y';
                    end;
                end;

                if Contains(FileName,'Data_Access',True) then
                begin
                    inc(countDataModul);
                    sDataModul:='Y';
                end;

                if SameText(fName, 'sdatadictclient.pas') or
                   SameText(fName, 'sdatadictbureau.pas') or
                   SameText(fName, 'sdatadictsystem.pas') or
                   SameText(fName, 'sdatadicttemp.pas') then
                begin
                  inc(countDataDict);
                  sDataDict:='Y';
                  bFileList:=false;
                end;

                if SameText(fName, 'SFieldCodeValues.pas') then
                begin
                  inc(countFieldValue);
                  sFieldValue:='Y';
                  bFileList:=false;
                end;

                if idx = 0 then
                  inc(countTotalPAS)
                else
                  inc(countTotalDFM);

                if ((countTotalDFM + countTotalPAS) <10)  and bFileList then

                   fList := fList + ' - ' +  fName;
                vValue := VarArrayOf([countTotalPAS, countTotalDFM, countScreenPas, countScreenDfm,
                                      countDataModul, countDataDict, countFieldValue, sScreenPAS, sScreenDFM, sDataModul,
                                      sDataDict, sFieldValue, fList]);

                outputFile.AddValue(sTableField, vValue);
              end;
        end;


  end;

  procedure AddAllFilesInDir(const Dir: string);
  var
    ext, fName:String;
    i,idx: integer;
    FileList: IisStringList;
  begin
    FileList := GetFilesByMask(NormalizePath(Dir) + '*.*');

    for i := 0 to FileList.Count - 1 do
    begin
      fName:= ExtractFileName(FileList[i]);
      ext:= ExtractFileExt(fName);
      idx := 0;
      if SameText(ext, '.dfm') then
        idx := 1;

      if (SameText(ext, '.pas') or SameText(ext, '.dfm')) then
         PosInFile(FileList[i], idx);
    end;

    FileList := GetSubDirs(Dir);
    for i := 0 to FileList.Count - 1 do
      AddAllFilesInDir(FileList[i]);
  end;

begin
  lList := TisStringList.Create;
  lList.Clear;
  lList.LoadFromFile(edFieldList.Text);

  outputFile := TislistOfValues.Create;

  ctx_StartWait;
  try
     AddAllFilesInDir(edDirectory.Text);

     if outputFile.Count > 0 then
     begin
        fileName := edResult.Text;
        AssignFile(rf, fileName);
        try
          try
            if not FileExists(fileName) then
              Rewrite(rf)
            else
              Append(rf);

          except
            on E: Exception do
            begin
              E.Message := 'Can''t open report file'#13 + E.Message;
              raise;
            end;
          end;

          Writeln(rf, Format('%-70s,  %s,  %s,   %s,   %s,  %s,  %s,  %s , %s , %s , %s , %s , %s , %s , %s,  %s ',
                ['Table and Field Name',
                 'Screen PAS Y/N', 'Screen DFM Y/N', 'Screen Total', 'Screen PAS File', 'Screen DFM file',
                 'DataModul Y/N', 'DataModul Total',
                 'DataDict Y/N', 'DataDict Total',
                 'FieldValue Y/N', 'FieldValue Total',
                 'TOTAL ALL',
                 'TOTAL PAS', 'TOTAL DFM',
                 'Files']));
          for i := 0 to outputFile.Count-1 do
          begin
             vValue := outputFile[i].Value;
             if vValue[0]+vValue[1] < 11 then
                  Writeln(rf, Format('%-70s,  %s,  %s,   %s,   %s,  %s,  %s,  %s , %s , %s , %s , %s , %s , %s , %s, %s  ',
                                          [outputFile[i].Name, vValue[7], vValue[8],
                                           vValue[2]+vValue[3], vValue[2], vValue[3],
                                           vValue[9], vValue[4],
                                           vValue[10],vValue[5],
                                           vValue[11],vValue[6],
                                           vValue[0]+vValue[1],
                                           vValue[0],vValue[1],
                                           vValue[12] ]));
          end;
        finally
          CloseFile(rf);
        end;
     end;

  finally
    ctx_EndWait;
  end;

end;

procedure TISMainForm.tsFieldLevelEditorShow(Sender: TObject);
begin
  flseMainForm1.Init;
end;

procedure TISMainForm.tsSysUtilsShow(Sender: TObject);
var
  sPwd: String;
begin
  try
    if Context.UserAccount.AccountType <> uatSBAdmin then
    begin
      sPwd := '';
      if EvDialog('Password', 'Please provide ADMIN password', sPwd, True) then
        Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(ctx_DomainInfo.AdminAccount, sDefaultDomain), HashPassword(sPwd))
    end;
  finally
    EvSystemDBUtils.Init;
  end;
end;

end.
