unit SSMainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, EvSecElement, SSecurityInterface, EvStreamUtils, EvTypes,
  EvSecurityUtils, ISZippingRoutines, IsBasicUtils, EvContext,
  EvMainboard, EvBasicUtils, EvUIUtils;
type
  Tfrm = class(TFrame)
    btnRebuildSec: TButton;
    procedure btnRebuildSecClick(Sender: TObject);
  private
  end;

implementation

uses
  sSecurityBuilderClassDefs, 
  SDataStructure, EvUtils, EvConsts, Db, CustomSecurityAtoms,
  EvCommonInterfaces;

{$R *.DFM}

procedure Tfrm.btnRebuildSecClick(Sender: TObject);
var
  fs, ms: IevDualStream;
  SI: STARTUPINFO;
  PI: PROCESS_INFORMATION;
  CmdLineStr: string;
  sPwd: String;
begin
  sPwd := '';
  if EvDialog('Password', 'Please provide ADMIN password', sPwd, True) then
    Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(ctx_DomainInfo.AdminAccount, sDefaultDomain), HashPassword(sPwd))
  else
    Exit;

  DeleteFile(AppDir + 'EvSecuritySchema.dat');

  ZeroMemory(@SI, SizeOf(SI));
  ZeroMemory(@PI, SizeOf(PI));

  CmdLineStr := '"' + AppDir + 'EvolutionSA.exe" -GETSEC' +
    ' -USER "' + Context.UserAccount.User + '" -PASSWORD "' + sPwd + '" -SB "' + Context.UserAccount.Domain + '"';

  if CreateProcess(nil, PChar(CmdLineStr), nil, nil, False, 0, nil, PChar(AppDir), SI, PI) then
  begin
    WaitForSingleObject(PI.hProcess, INFINITE);
    CloseHandle(PI.hThread);
    CloseHandle(PI.hProcess);
  end;

  ms := TevDualStreamHolder.CreateFromFile(AppDir + 'EvSecuritySchema.dat');
  fs := TevDualStreamHolder.Create;

  ms.Position := 0;
  DeflateStream(ms.RealStream, fs.RealStream);
  fs.Position := 0;

  ctx_DataAccess.StartNestedTransaction([dbtSystem]);
  try
    DM_SYSTEM_MISC.SY_STORAGE.Open;
    if DM_SYSTEM_MISC.SY_STORAGE.Locate('TAG', SecurityStructureStorageTag, []) then
      DM_SYSTEM_MISC.SY_STORAGE.Edit
    else
    begin
      DM_SYSTEM_MISC.SY_STORAGE.Append;
      DM_SYSTEM_MISC.SY_STORAGE['TAG'] := SecurityStructureStorageTag;
    end;

    (DM_SYSTEM_MISC.SY_STORAGE.FieldByName('STORAGE_DATA') as TBlobField).LoadFromStream(fs.RealStream);
    DM_SYSTEM_MISC.SY_STORAGE.Post;

    ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    raise;
  end;

  ShowMessage('Security has been rebuilt');
end;

end.
