unit SMainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, IniFiles, SEncryptionRoutines;

type
  TMainCreator = class(TFrame)
    edtIniName: TEdit;
    lblIniFile: TLabel;
    btnOpenIniFile: TSpeedButton;
    lblDBPath: TLabel;
    edtDBPath: TEdit;
    lblList: TLabel;
    memList: TMemo;
    btnCreate: TButton;
    dlgOpenIniFile: TOpenDialog;
    lblDatPath: TLabel;
    edtDatPath: TEdit;
    procedure btnOpenIniFileClick(Sender: TObject);
    procedure edtIniNameChange(Sender: TObject);
    procedure btnCreateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TMainCreator.btnOpenIniFileClick(Sender: TObject);
begin
  dlgOpenIniFile.FileName := edtIniName.Text;
  if dlgOpenIniFile.Execute then
    edtIniName.Text := dlgOpenIniFile.FileName;
end;

procedure TMainCreator.edtIniNameChange(Sender: TObject);
const
  Prefix = 'ConnectIni_';
var
  i: Integer;
begin
  if FileExists(edtIniName.Text) then
    with TIniFile.Create(edtIniName.Text) do
    try
      ReadSections(memList.Lines);
      for i := Pred(memList.Lines.Count) downto 0 do
        if (Copy(memList.Lines[i], 1, Length(Prefix)) <> Prefix) then
          memList.Lines.Delete(i)
        else
          memList.Lines[i] := Copy(memList.Lines[i], Length(Prefix)+ 1, MaxInt);
    finally
      Free;
    end
  else
    memList.Lines.Clear;
end;

procedure TMainCreator.btnCreateClick(Sender: TObject);
var
  PathChar: Char;
  s, sSection: string;
  i: Integer;
  sDat: string;
  F: TextFile;
begin
  if Pos('/', edtDBPath.Text) > 0 then
    PathChar := '/'
  else
    PathChar := '\';
  s := edtDBPath.Text;
  if (Length(s) > 0) and (s[Length(s)] <> PathChar) then
    s := s + PathChar;
  sDat := edtDATPath.Text;
  if (Length(sDat) > 0) and (sDat[Length(sDat)] <> '\') then
    sDat := sDat + '\';
  if not DirectoryExists(sDat) then
    raise Exception.Create('Path ' + sDat + ' doesn''t exist');

  with TIniFile.Create(edtIniName.Text) do
  try
    for i := 0 to Pred(memList.Lines.Count) do
      if not AnsiSameText(memList.Lines[i], 'Aliases') then
      begin
        sSection := 'ConnectIni_'+ memList.Lines[i];
        WriteString(sSection, 'DefaultDB', s + memList.Lines[i] + PathChar);
        WriteString(sSection, 'SysDBOverrd', s);

        CreateDir(sDat + memList.Lines[i]);
        AssignFile(F, sDat + memList.Lines[i] + '\SB.dat');
        try
          Rewrite(F);
          Write(F, EncryptHexString(memList.Lines[i]));
        finally
          CloseFile(F);
        end;
      end;
  finally
    Free;
  end;
  ShowMessage('Done');
end;

end.
