object MainCreator: TMainCreator
  Left = 0
  Top = 0
  Width = 464
  Height = 340
  TabOrder = 0
  object lblIniFile: TLabel
    Left = 12
    Top = 15
    Width = 64
    Height = 13
    Caption = 'INI File Name'
  end
  object btnOpenIniFile: TSpeedButton
    Left = 423
    Top = 12
    Width = 23
    Height = 22
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      5555555555555555555555555555555555555555555555555555555555555555
      555555555555555555555555555555555555555FFFFFFFFFF555550000000000
      55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
      B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
      000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
      555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
      55555575FFF75555555555700007555555555557777555555555555555555555
      5555555555555555555555555555555555555555555555555555}
    NumGlyphs = 2
    OnClick = btnOpenIniFileClick
  end
  object lblDBPath: TLabel
    Left = 12
    Top = 45
    Width = 71
    Height = 13
    Caption = 'Database Path'
  end
  object lblList: TLabel
    Left = 12
    Top = 102
    Width = 53
    Height = 13
    Caption = 'Bureau List'
  end
  object lblDatPath: TLabel
    Left = 12
    Top = 75
    Width = 71
    Height = 13
    Caption = 'DAT Files Path'
  end
  object edtIniName: TEdit
    Left = 93
    Top = 12
    Width = 319
    Height = 21
    TabOrder = 0
    OnChange = edtIniNameChange
  end
  object edtDBPath: TEdit
    Left = 93
    Top = 42
    Width = 319
    Height = 21
    TabOrder = 1
  end
  object memList: TMemo
    Left = 93
    Top = 102
    Width = 319
    Height = 160
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object btnCreate: TButton
    Left = 219
    Top = 276
    Width = 75
    Height = 25
    Caption = 'Create'
    TabOrder = 4
    OnClick = btnCreateClick
  end
  object edtDatPath: TEdit
    Left = 93
    Top = 72
    Width = 319
    Height = 21
    TabOrder = 2
  end
  object dlgOpenIniFile: TOpenDialog
    Filter = 'INI Files|*.ini'
    Left = 27
    Top = 120
  end
end
