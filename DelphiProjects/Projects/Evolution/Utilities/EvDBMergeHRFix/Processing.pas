unit Processing;

interface

uses SysUtils, Variants,
     IBDatabase, IBQuery,
     isBaseClasses, isThreadManager, isDataSet, isBasicUtils;

type
  IevProcessingCallback = interface
  ['{FC3265DD-A338-4DD6-95DD-9E80ECF262D9}']
    procedure OnStart;
    procedure OnError(const AError: String);
    procedure OnFinish;
    procedure LogMessage(const AText: String);
  end;

  IevDBDataRecover = interface
  ['{92016C28-D9F1-4FE6-90AD-96C5AE1789F6}']
    procedure Execute(const ASourceDB, ADestinationDB, EUserPassword: String; ProcessType: integer; const ACallback: IevProcessingCallback);
  end;

  TevDBDataRecover = class(TisInterfacedObject, IevDBDataRecover)
  private
    FCallback: IevProcessingCallback;
    FSrcDB: TIBDatabase;
    FDestDB: TIBDatabase;
    FSrcTran: TIBTransaction;
    FDestTran: TIBTransaction;

    procedure DoInThread(const Params: TTaskParamList);
    procedure MergeDB;
    procedure MergeDBDependent;
  protected
    procedure Execute(const ASourceDB, ADestinationDB, EUserPassword: String; ProcessType: integer; const ACallback: IevProcessingCallback);

  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

implementation

uses DB;


{ TevDBDataRecover }

procedure TevDBDataRecover.AfterConstruction;
begin
  inherited;
  FSrcDB := TIBDatabase.Create(nil);
  FSrcDB.LoginPrompt := False;
  FSrcTran := TIBTransaction.Create(nil);
  FSrcDB.DefaultTransaction := FSrcTran;

  FDestDB := TIBDatabase.Create(nil);
  FDestDB.LoginPrompt := False;
  FDestTran := TIBTransaction.Create(nil);
  FDestDB.DefaultTransaction := FDestTran;
end;

procedure TevDBDataRecover.BeforeDestruction;
begin
  FreeAndNil(FSrcDB);
  FreeAndNil(FDestDB);
  inherited;
end;

procedure TevDBDataRecover.DoInThread(const Params: TTaskParamList);
begin
//  try
//    FCallback.OnStart;
    try
      if (Params[0] = 0) then
        MergeDB
      else
        MergeDBDependent;
    except
      on E: Exception do
        FCallback.OnError(E.Message);
    end;
//  finally
//    FCallback.OnFinish;
//  end;
end;

procedure TevDBDataRecover.Execute(const ASourceDB, ADestinationDB,
  EUserPassword: String; ProcessType :integer; const ACallback: IevProcessingCallback);
begin
  FCallback := ACallback;

  FSrcDB.Close;
  FDestDB.Close;

  FSrcDB.Params.Clear;
  FSrcDB.DatabaseName := ASourceDB;
  FSrcDB.Params.Add('USER_NAME=EUSER');
  FSrcDB.Params.Add('PASSWORD=' + EUserPassword);

  FDestDB.Params.Clear;
  FDestDB.DatabaseName := ADestinationDB;
  FDestDB.Params.Add('USER_NAME=EUSER');
  FDestDB.Params.Add('PASSWORD=' + EUserPassword);

  GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([ProcessType]), 'Processing');
end;

procedure TevDBDataRecover.MergeDB;
var
  SourceQ, DestQ, DestQEE : TIBQuery;
begin
   FSrcDB.Open;
   FDestDB.Open;
   try
      SourceQ := TIBQuery.Create(nil);
      DestQ := TIBQuery.Create(nil);
      DestQEE := TIBQuery.Create(nil);
      try
         DestQ.Database := FDestDB;
         DestQEE.Database := FDestDB;
         SourceQ.Database := FSrcDB;

         if not FDestTran.Active then
            FDestTran.StartTransaction;
         try
              SourceQ.Close;
              SourceQ.SQL.Text :=
               ' select a.EE_BENEFITS_NBR, a.CL_BENEFITS_NBR, c.BENEFIT_NAME, b.CO_NBR ' +
                  ' from EE_BENEFITS a, EE b, CL_BENEFITS c ' +
                    ' where a.EE_NBR = b.EE_NBR and  a.CL_BENEFITS_NBR = c.CL_BENEFITS_NBR and ' +
                       ' a.ACTIVE_RECORD = ''C'' and b.ACTIVE_RECORD = ''C'' and c.ACTIVE_RECORD = ''C'' and a.CL_BENEFIT_RATES_NBR is null ';
              SourceQ.Open;

              DestQ.Close;
              DestQ.SQL.Text :=
                 ' select CO_BENEFITS_NBR, BENEFIT_NAME, CO_NBR ' +
                        ' from CO_BENEFITS order by BENEFIT_NAME,CO_NBR';
              DestQ.Open;

              SourceQ.First;
              while not SourceQ.Eof do
              begin

                if DestQ.Locate('BENEFIT_NAME;CO_NBR', VarArrayOf([SourceQ.Fields[2].Value,SourceQ.Fields[3].Value]),[]) then
                begin

                     DestQEE.SQL.Text :=
                          ' update EE_BENEFITS set CO_BENEFITS_NBR = :pCO_BENEFITS where EE_BENEFITS_NBR = :pEE_BENEFITS_NBR and CO_BENEFITS_NBR = :pCL_BENEFITS_NBR ';
                     DestQEE.ParamByName('pCO_BENEFITS').AsInteger :=  DestQ.Fields[0].AsInteger;
                     DestQEE.ParamByName('pEE_BENEFITS_NBR').AsInteger :=  SourceQ.Fields[0].AsInteger;
                     DestQEE.ParamByName('pCL_BENEFITS_NBR').AsInteger :=  SourceQ.Fields[1].AsInteger;

                     DestQEE.ExecSQL;
                end;
               SourceQ.Next;
              end;

            SourceQ.Close;
            DestQ.Close;
            FDestTran.Commit;
         except
           on E: Exception do
             begin
              FDestTran.Rollback;
              FCallback.OnError(E.Message);
            end;
         end;
      finally
        DestQ.Free;
        DestQEE.Free;
        SourceQ.Free;
      end;
   finally
    FSrcDB.Close;
    FDestDB.Close;
   end;
end;

procedure TevDBDataRecover.MergeDBDependent;
    function GetGeneratorValue(const AGeneratorName: String; const AIncrement: Integer): Int64;
    var
      Q: TIBQuery;
    begin
        Q := TIBQuery.Create(nil);
        try
          Q.SQL.Text := 'SELECT Gen_Id(' + AGeneratorName + ', ' + IntToStr(AIncrement) + ') FROM ev_database';
          Q.Database := FDestDB;
          Q.Open;
          Result := Q.Fields[0].AsInteger;
        finally
          Q.Free;
        end;
    end;

var
  SourceQ, DestQ, DestQEE : TIBQuery;
begin
   FSrcDB.Open;
   FDestDB.Open;
   try
      SourceQ := TIBQuery.Create(nil);
      DestQ := TIBQuery.Create(nil);
      DestQEE := TIBQuery.Create(nil);
      try
         DestQ.Database := FDestDB;
         DestQEE.Database := FDestDB;
         SourceQ.Database := FSrcDB;

         if not FDestTran.Active then
            FDestTran.StartTransaction;

         try
            SourceQ.Close;
            SourceQ.SQL.Text :=
                     ' select a.EE_BENEFITS_NBR, a.CL_PERSON_DEPENDENTS_NBR from EE_BENEFITS a  ' +
                         ' where a.ACTIVE_RECORD = ''C'' and a.CL_PERSON_DEPENDENTS_NBR is not null ' +
                         ' order by EE_BENEFITS_NBR, a.CL_PERSON_DEPENDENTS_NBR ';
            SourceQ.Open;

            DestQ.Close;
            DestQ.SQL.Text :=
                'select a.EE_BENEFITS_NBR,a.EFFECTIVE_DATE, a.CREATION_DATE, a.CHANGED_BY ' +
                     ' from EE_BENEFITS a  where a.ACTIVE_RECORD = ''C'' and ' +
                       ' a.EE_BENEFITS_NBR not in ' +
                           ' ( select b.EE_BENEFITS_NBR from EE_BENEFICIARY b where b.ACTIVE_RECORD = ''C'' ) ' +
                     ' order by a.EE_BENEFITS_NBR ';
            DestQ.Open;


            DestQEE.SQL.Text := 'EXECUTE PROCEDURE AFTER_START_TRANSACTION';
            DestQEE.ExecSQL;


            SourceQ.First;
            while not SourceQ.Eof do
            begin
              if DestQ.Locate('EE_BENEFITS_NBR', VarArrayOf([SourceQ.Fields[0].Value]),[]) then
              begin
               DestQEE.SQL.Text :=
                 'INSERT INTO EE_BENEFICIARY ' +
                    '(EE_BENEFICIARY_NBR, EE_BENEFITS_NBR, EFFECTIVE_DATE, CREATION_DATE, CHANGED_BY, ACTIVE_RECORD, ' +
                       ' CL_PERSON_DEPENDENTS_NBR, PRIMARY_BENEFICIARY) ' +
                       ' VALUES ( ' +
                         ' :pEE_BENEFICIARY_NBR, :pEE_BENEFITS_NBR, :pEFFECTIVE_DATE, :pCREATION_DATE, :pCHANGED_BY, ' +
                         ' :pACTIVE_RECORD, :pCL_PERSON_DEPENDENTS_NBR, :pPRIMARY_BENEFICIARY )';

                DestQEE.ParamByName('pEE_BENEFICIARY_NBR').AsInteger :=  GetGeneratorValue('G_EE_BENEFICIARY', 1);;
                DestQEE.ParamByName('pEE_BENEFITS_NBR').AsInteger :=  DestQ.Fields[0].AsInteger;
                DestQEE.ParamByName('pEFFECTIVE_DATE').AsDateTime :=  DestQ.Fields[1].AsDateTime;
                DestQEE.ParamByName('pCREATION_DATE').AsDateTime :=  DestQ.Fields[2].AsDateTime;
                DestQEE.ParamByName('pCHANGED_BY').AsInteger :=  DestQ.Fields[3].AsInteger;
                DestQEE.ParamByName('pACTIVE_RECORD').AsString :=  'C';
                DestQEE.ParamByName('pCL_PERSON_DEPENDENTS_NBR').AsInteger :=  SourceQ.Fields[1].AsInteger;
                DestQEE.ParamByName('pPRIMARY_BENEFICIARY').AsString :=  'N';
                DestQEE.ExecSQL;
              end;
              SourceQ.Next;
            end;

            DestQEE.SQL.Text := 'EXECUTE PROCEDURE BEFORE_COMMIT_TRANSACTION';
            DestQEE.ExecSQL;

            SourceQ.Close;
            DestQ.Close;
            FDestTran.Commit;
         except
           on E: Exception do
             begin
              FDestTran.Rollback;
              FCallback.OnError(E.Message);
            end;
         end;
      finally
        DestQ.Free;
        DestQEE.Free;
        SourceQ.Free;
      end;
   finally
    FSrcDB.Close;
    FDestDB.Close;
   end;
end;

end.
