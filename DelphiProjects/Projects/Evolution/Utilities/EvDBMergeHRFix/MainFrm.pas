unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, Processing, isThreadManager, isBaseClasses,
  isBasicUtils, ExtCtrls, ISBasicClasses, Buttons, IBDatabase, IBQuery;

type
  TMain = class(TForm, IevProcessingCallback)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edSourceDB: TEdit;
    Label2: TLabel;
    edDestinationDB: TEdit;
    Label3: TLabel;
    edDBPassword: TEdit;
    btnRun: TButton;
    memLog: TMemo;
    Label4: TLabel;
    lblClient: TLabel;
    edtClients: TEdit;
    edTempDatabase: TEdit;
    lblTemp: TLabel;
    rgProcessType: TRadioGroup;
    procedure btnRunClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure rgProcessTypeClick(Sender: TObject);
  private
    FInProgress: Boolean;
    FMCThreadManager: IThreadManager;
    FTempDB: TIBDatabase;
    FTempTran: TIBTransaction;
    procedure DoLogMessage(const AText: String);
    procedure MulticlientProcess(const Params: TTaskParamList);
  protected
    procedure OnStart;
    procedure OnError(const AError: String);
    procedure OnFinish;
    procedure LogMessage(const AText: String);
  public
  end;

var
  Main: TMain;

implementation

{$R *.dfm}

procedure TMain.btnRunClick(Sender: TObject);
begin
  FMCThreadManager.RunTask(MulticlientProcess, Self,
    MakeTaskParams([Trim(edSourceDB.Text), Trim(edDestinationDB.Text), edDBPassword.Text, edtClients.Text, Trim(edTempDatabase.Text), rgProcessType.ItemIndex]),
    'Multiclient loop');
end;

procedure TMain.LogMessage(const AText: String);

   procedure DoLogMessage(const Params: TTaskParamList);
   begin
     Main.DoLogMessage(Params[0]);
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoLogMessage, MakeTaskParams([AText]), False);
end;

procedure TMain.OnError(const AError: String);

   procedure DoOnError(const Params: TTaskParamList);
   begin
     Main.DoLogMessage('ERROR: ' + Params[0]);
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnError, MakeTaskParams([AError]), False);
end;

procedure TMain.OnFinish;

   procedure DoOnFinish(const Params: TTaskParamList);
   begin
     Main.DoLogMessage('Finished');

     Main.btnRun.Enabled := True;
     Main.edSourceDB.Enabled := True;
     Main.edDestinationDB.Enabled := True;
     Main.edDBPassword.Enabled := True;
     Main.edtClients.Enabled := True;
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnFinish, MakeTaskParams([]), False);
end;

procedure TMain.OnStart;

   procedure DoOnStart(const Params: TTaskParamList);
   begin
     Main.btnRun.Enabled := False;
     Main.edSourceDB.Enabled := False;
     Main.edDestinationDB.Enabled := False;
     Main.edDBPassword.Enabled := False;
     Main.edtClients.Enabled := False;     

     Main.memLog.Clear;
     Main.DoLogMessage('Started');
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnStart, MakeTaskParams([]), False);
end;

procedure TMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not FInProgress;
end;

procedure TMain.DoLogMessage(const AText: String);
begin
  memLog.Lines.Add(FormatDateTime('hh:nn:ss', Now) + '     ' +
    StringReplace(StringReplace(AText, #13, ' ', [rfReplaceAll]), #13, ' ', [rfReplaceAll]));
end;

procedure TMain.MulticlientProcess(const Params: TTaskParamList);
var
  iClient : IisStringList;
  DBDataRecover: IevDBDataRecover;
  i: Integer;
  sDB1, sDB2: String;
  TempQ: TIBQuery;
begin
  FInProgress := True;
  OnStart;
  try
    if (Params[5] <> 0) then
    begin
        FTempDB := TIBDatabase.Create(nil);
        FTempDB.LoginPrompt := False;
        FTempTran := TIBTransaction.Create(nil);
        FTempDB.DefaultTransaction := FTempTran;
        FTempDB.Params.Clear;
        FTempDB.DatabaseName := NormalizePath(Trim(Params[4])) + 'TMP_TBLS.gdb';
        FTempDB.Params.Add('USER_NAME=EUSER');
        FTempDB.Params.Add('PASSWORD=' + Params[2]);
        FTempDB.Open;
        Try
            TempQ := TIBQuery.Create(nil);
            TempQ.Database := FTempDB;
            try
                TempQ.Close;
                TempQ.SQL.Text :=  'select  CL_NBR from  TMP_CL order by CL_NBR';
                TempQ.Open;
                TempQ.First;

                while not TempQ.Eof do
                begin
                  if (TempQ.Fields[0].AsInteger > 0) then
                  begin
                     sDB1 := NormalizePath(Params[0]) + 'CL_'+ Trim(TempQ.Fields[0].AsString) + '.gdb';
                     sDB2 := NormalizePath(Params[1]) + 'CL_'+ Trim(TempQ.Fields[0].AsString) + '.gdb';

                     try
                       DBDataRecover := TevDBDataRecover.Create;
                       DBDataRecover.Execute(sDB1, sDB2, Params[2], Params[5], Self);
                       LogMessage('Started CL_' + Trim(TempQ.Fields[0].AsString));
                     except
                       on E: Exception do
                         OnError(E.Message);
                     end;
                   end;
                   TempQ.Next;
                end;

            finally
              TempQ.Close;
            end;
        Finally
          FTempDB.close;
          FreeAndNil(FTempDB);
        end;
    end
    else
    begin
        iClient := TisStringList.Create;
        iClient.CommaText := Params[3];

        for i := 0 to iClient.Count - 1 do
        begin
          sDB1 := NormalizePath(Params[0]) + 'CL_'+ Trim(iClient[i]) + '.gdb';
          sDB2 := NormalizePath(Params[1]) + 'CL_'+ Trim(iClient[i]) + '.gdb';

          try
            DBDataRecover := TevDBDataRecover.Create;
            DBDataRecover.Execute(sDB1, sDB2, Params[2], Params[5], Self);
            LogMessage('Started CL_' + Trim(iClient[i]));
          except
            on E: Exception do
              OnError(E.Message);
          end;
        end;
    end;

    GlobalThreadManager.WaitForTaskEnd;

  finally
    FInProgress := False;
    OnFinish;
  end;
end;

procedure TMain.FormCreate(Sender: TObject);
begin
  FMCThreadManager := TThreadManager.Create('MC TM');
  GlobalThreadManager.Capacity := HowManyProcessors;
  rgProcessType.ItemIndex := 0;
  edTempDatabase.Visible := false;

end;

procedure TMain.rgProcessTypeClick(Sender: TObject);
begin
  edTempDatabase.Visible := rgProcessType.ItemIndex = 1;
  lblTemp.Visible := edTempDatabase.Visible;
  edtClients.Visible := rgProcessType.ItemIndex = 0;
  lblClient.Visible := edtClients.Visible;
end;

end.
