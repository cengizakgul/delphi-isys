object Main: TMain
  Left = 359
  Top = 164
  Width = 669
  Height = 594
  Caption = 
    'Evolution Database Merging Utility for Orange HR conversion issu' +
    'e'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  DesignSize = (
    653
    556)
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 16
    Top = 299
    Width = 22
    Height = 13
    Caption = 'Log'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 16
    Width = 616
    Height = 239
    Caption = 'Parameters'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 99
      Width = 126
      Height = 13
      Caption = 'Original DB Path (Norwich)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 132
      Width = 121
      Height = 13
      Caption = 'Current DB Path (Orange)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 204
      Width = 86
      Height = 13
      Caption = 'EUSER Password'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblClient: TLabel
      Left = 16
      Top = 171
      Width = 133
      Height = 13
      Caption = 'Clients      CL1,CL2,CL3, .... '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblTemp: TLabel
      Left = 16
      Top = 67
      Width = 145
      Height = 13
      Caption = 'Temp Database Path (Orange)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edSourceDB: TEdit
      Left = 176
      Top = 97
      Width = 407
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Text = 'maia:/db/norwich/'
    end
    object edDestinationDB: TEdit
      Left = 176
      Top = 130
      Width = 407
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'maia:/db/orange/'
    end
    object edDBPassword: TEdit
      Left = 177
      Top = 201
      Width = 141
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 3
    end
    object edtClients: TEdit
      Left = 176
      Top = 163
      Width = 407
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object edTempDatabase: TEdit
      Left = 176
      Top = 65
      Width = 407
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      Text = 'maia:/db/orange/'
    end
    object rgProcessType: TRadioGroup
      Left = 176
      Top = 16
      Width = 409
      Height = 41
      Caption = 'Process Type '
      Columns = 2
      Items.Strings = (
        'Fix Company Benefits'
        'Fix Dependent Benefits')
      TabOrder = 5
      OnClick = rgProcessTypeClick
    end
  end
  object btnRun: TButton
    Left = 392
    Top = 210
    Width = 111
    Height = 25
    Caption = 'Run'
    TabOrder = 2
    OnClick = btnRunClick
  end
  object memLog: TMemo
    Left = 16
    Top = 323
    Width = 619
    Height = 221
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
  end
end
