unit ViewerMainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, unxcrypt,
  StdCtrls, IBQuery, IBDatabase, Buttons,  CheckLst,
  db, StrUtils, Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, wwdblook, Wwdbdlg, DBGrids, Mask, ComCtrls, inifiles,
  Menus,
  IBSQL,IBTable, IB,IBUtils, IBHeader,  IBCustomDataSet, Variants, ExtCtrls,
  Registry, //Datamodule,
  ISBasicClasses,  EvUIUtils, EvUIComponents, EvConsts,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit
  ,rwLocalReportMod
  ,SReportSettings
  ,EvStreamUtils
  ,  EvCommonInterfaces
  ,  iskbmMemDataSet
    ,isBaseClasses
    ,EvClientDataset, DBClient
    ,EvContext
    ,evModuleRegister, EvClasses, EvUtils
    ,EvMainboard
    ,SDataAccessMod
    ,EvDataAccessComponents
    ,ISDataAccessComponents
    ,kbmMemTable
    ,XMLRWParams
  ;

type
  TForm2 = class(TForm)
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    Panel1: TPanel;
    Button1: TButton;
    TabSheet1: TTabSheet;
    StringGrid1: TStringGrid;
    evDBGrid1: TevDBGrid;
    DataSource1: TDataSource;
    ClientDataSet1: TClientDataSet;
    PopupMenu1: TPopupMenu;
    C1: TMenuItem;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    PopupMenuArray: TPopupMenu;
    Change1: TMenuItem;
    Button3: TButton;
    OpenDialog2: TOpenDialog;
    SaveDialog2: TSaveDialog;
    Button4: TButton;
    ClientDataSet1Idx: TIntegerField;
    procedure Button1Click(Sender: TObject);
    procedure C1Click(Sender: TObject);
    procedure StringGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Change1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    aFileRepParams: TrwReportParams;
    procedure DisplayParametersContentFromStream(aStream: TStream);
    procedure ProcessArrayParam(V: Variant; Name:string; parIndex:integer);
    procedure PlaceDataset(Memo: Tmemo; Name:string; IDS: IevDataSetHolder);
    function ShowBasicVariantType(varVar: Variant): string;
    procedure PlaceArrayMemo(Memo: TMemo; U: Variant);
    function StrToISystemVar(const Value, sVarType: string): Variant;
    procedure DisplayParametersContentFromFile(FileName: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.PlaceDataset(Memo: Tmemo; Name:string; IDS:IevDataSetHolder);
var
  PageC: TPageControl;
  n: integer;
  TabSheet: TTabSheet;
//  dsource : TevDataSource;
  dGrid: //TDBGrid; //
        TStringGrid;//TDBGrid;
  jCol, jRow :integer;
  xds: //TISBasicClientDataSet;
        TISKbmMemDataSet;
//  memoStream:TMemoryStream;
begin
  if Memo.ComponentCount = 0 then
  begin
    PageC := TPageControl.Create(Memo);
    PageC.Parent := Memo;
    PageC.Align := alClient;
  end
  else
    PageC := Memo.Controls[0] as TPageControl;

  n := PageC.PageCount;

  TabSheet := TTabSheet.Create(PageC);
  TabSheet.Caption := Name+'.'+Format('Table[%.3d]',[n]);
  TabSheet.PageControl := PageC;

  ids.DataSet.Open;
  Application.Processmessages;

  dGrid := //TDBGrid.Create(TabSheet);//
            TStringGrid.Create(TabSheet);
  dGrid.Parent := TabSheet;
  dGrid.Align := alClient;
  IDS.Dataset.ShadowDataset.Refresh;
  IDS.Dataset.ShadowDataset.Last;
  IDS.Dataset.ShadowDataset.First;


  dGrid.RowCount := IDS.Dataset.RecordCount+1;
  dGrid.ColCount := IDS.Dataset.FieldCount;
  dGrid.FixedCols := 0;
  dGrid.Options :=  dGrid.Options+[goColSizing];


  xds:= TISKbmMemDataSet.Create(Tabsheet);
  xds.CommaTextFormat := TkbmStreamFormat.Create(xds);
  xds.CloneCursor(IDS.Dataset, True, False);
//  xds := IDS.Dataset;
  xds.Open;

// ShowMessage(xds.CommaText);

  Xds.First;
  Application.Processmessages;
  Xds.Last;
  Application.Processmessages;
  Xds.First;
  Application.Processmessages;

//  Xds.Next;
  jRow := 1;
  while NOT Xds.Eof do begin
    for jCol := 0 to Xds.FieldCount-1 do
    begin
      dGrid.Cells[jCol, 0] := Xds.Fields[jCol].FieldName;
      dGrid.Cells[jCol, jRow] := VarToStr(Xds.Fields[jCol].Value);
    end;

    Xds.Next;
    jRow := jRow + 1;
    Application.ProcessMessages;
  end;

  Application.ProcessMessages;



end;

procedure TForm2.PlaceArrayMemo(Memo:TMemo; U:Variant);
var
    i: integer;
    s: string;
begin
  s := 'Array[' + IntToStr(VarArrayLowBound(U, 1))
        +'..'
        + IntToStr(VarArrayHighBound(U, 1))+']: ';
  for i := VarArrayLowBound(U, 1) to VarArrayHighBound(U, 1) do
  begin
      s:=s + '<'+VarToStr(u[i])+'>;';
  end;
  if Length(s) > 0 then
    s := System.Copy(s,1, Length(s)-1);

  Memo.Lines.Add(s);
end;

procedure TForm2.ProcessArrayParam(V : Variant; Name:string;  parIndex:integer);
var
   TabSheet: TTabSheet;
   U: Variant;
   i:integer;
   Memo: TMemo;
   iStream: IIsStream;
   iDs: IevDataSetHolder ;

begin
  Assert(VarIsArray(V));

  TabSheet := TTabSheet.Create(PageControl1);
  TabSheet.Caption := Name;
  TabSheet.PageControl := PageControl1;
  Memo := Tmemo.Create(TabSheet);
  Memo.Parent :=TabSheet;
  Memo.Align := alClient;
  Memo.PopupMenu := PopupMenuArray;
  Memo.Tag := parIndex;


  for i := VarArrayLowBound(V, 1) to VarArrayHighBound(V, 1) do
  begin
    U := V[i];
    if VarIsType(U, varUnknown) then
    begin
      SysUtils.Supports(U, IIsStream, iStream);
      iDS := TevDataSetHolder.Create;
      iDS.DataSet.LoadFromUniversalStream(iStream);
      PlaceDataset(Memo, Name, IDS);
    end
    else
    begin
       if VarIsArray(U) then
          PlaceArrayMemo(Memo, U)
       else
       begin
         iStream := nil;
         if (i = 0) then
          Memo.Lines.Text := VarToStr(U)
         else
           Memo.Lines.Add( VarToStr(U) );
       end;
    end;
  end;

end;

procedure TForm2.DisplayParametersContentFromStream(aStream: TStream);//FileName:string);
var
  i: integer;
  parInst: TrwReportParam;
begin

  Button2.Visible := False;

  PageControl1.Pages[0].Caption := 'Parameter list';

  for i := PageControl1.PageCount-1 downto 1 do
  begin
    PageControl1.Pages[i].Free;
  end;

  Application.Processmessages;

  aFileRepParams.Free;

  aFileRepParams :=  TrwReportParams.Create();

    //aFileRepParams.LoadFromFile(FileName);
    aFileRepParams.LoadFromStream(aStream);
    StringGrid1.RowCount := aFileRepParams.Count + 1;
    StringGrid1.ColWidths[0] := 200;     StringGrid1.ColWidths[2] := 300;
    StringGrid1.Cells[0,0] := 'Name';
    StringGrid1.Cells[1,0] := 'VarType (Int)';
    StringGrid1.Cells[2,0] := 'Value';
    StringGrid1.Cells[3,0] := 'Store';

    for i := 0 to aFileRepParams.Count-1 do
    begin
      parInst := aFileRepParams[i];
      StringGrid1.Cells[0, i+1] := parInst.Name;
      StringGrid1.Cells[1, i+1] := //IntToStr(parInst.VarType);
                                      ShowBasicVariantType(parInst.Value);
      if VarIsArray(parInst.Value) then
      begin
          StringGrid1.Cells[2, i+1] := '<Array>' ;
          ProcessArrayParam(parInst.Value, Parinst.Name, i);
      end
      else
          StringGrid1.Cells[2, i+1] := VarToStr(parInst.Value);

      StringGrid1.Cells[3, i+1] :=  VarToStr(parInst.Store);

    end;

  if StringGrid1.RowCount > 1 then
    StringGrid1.FixedRows := 1;
  PageControl1.Pages[0].Caption := 'Parameter list (' + IntToStr(StringGrid1.RowCount-1) + ')';
end;

procedure TForm2.Button1Click(Sender: TObject);
var
  MainB : IEvMainboard;
begin
  MainB := EvMainboard.MainBoard;

  if OpenDialog1.Execute then
  begin
     DisplayParametersContentFromFile(OpenDialog1.FileName);
  end;
end;

procedure TForm2.DisplayParametersContentFromFile(FileName: string);
var
    iiS: IisStream;
begin
     iIs := TIsStream.CreateFromFile(FileName, False, True, True);
     Self.Caption := 'ReportParameterViewer ' + FileName;
     DisplayParametersContentFromStream(iIs.RealStream);
end;

procedure TForm2.C1Click(Sender: TObject);
var
   sOld: string;
   sNew: string;
   jRow: integer;
   sVarType: string;
begin
   if ((Sender as TMenuItem).GetParentMenu as TPopupMenu).PopupComponent  = StringGrid1 then
   begin
    jRow := StringGrid1.Row;
    sOld := StringGrid1.Cells[2, jRow];
    sVarType := StringGrid1.Cells[1, jRow];
    if CompareText( sOld, '<Array>') <> 0 then
    begin
      sNew :=
         InputBox('Parameter change', 'Alter parameter value', StringGrid1.Cells[2, StringGrid1.Row]);

      if CompareStr(sNew, sOld) <> 0 then
      begin
//        aFileRepParams.Items[StringGrid1.Row-1].Value := sNew;
        aFileRepParams.Items[StringGrid1.Row-1].Value := StrToISystemVar(sNew,sVarType);
        StringGrid1.Cells[2, StringGrid1.Row] := sNew;
        Button2.Visible := true;
      end;
    end;
   end;
end;

function TForm2.StrToISystemVar( const Value, sVarType:string):Variant;
var
  d: double;
  sngl: single;
  b:Byte;
begin
  if CompareText(sVarType, 'varString')=0 then
  begin
     Result := Value; // transform to Var
     Exit;
  end;
  if CompareText(sVarType, 'varInteger')=0 then
  begin
     Result := integer(StrToInt(Value)); // transform to Var
     Exit;
  end;

  if CompareText(sVarType, 'varSmallInt')=0 then
  begin
     Result := smallint(StrToInt(Value)); // transform to Var
     Exit;
  end;

  if CompareText(sVarType, 'varDouble')=0 then
  begin
     d := StrToFloat(Value);
     Result := d;// transform to Var
     Exit;
  end;

  if CompareText(sVarType, 'varSingle')=0 then
  begin
     sngl := StrToFloat(Value);
     Result := sngl; // transform to Var
     Exit;
  end;

  if CompareText(sVarType, 'varCurrency')=0 then
  begin
     Result := Currency(StrToCurr(Value)); // transform to Var
     Exit;
  end;

  if CompareText(sVarType, 'varDate')=0 then
  begin
     Result := TDateTime(StrToDate(Value)); // transform to Var
     Exit;
  end;

  if CompareText(sVarType, 'varBoolean')=0 then
  begin
     if AnsiStartsText('Y', Value) or AnsiStartsText('1', Value) or AnsiStartsText('T', Value) then
     begin
        Result := True; Exit;
     end
     else
     if AnsiStartsText('N', Value) or AnsiStartsText('0', Value) or AnsiStartsText('F', Value) then
     begin
       Result := False;  Exit;
     end
     else
       Raise Exception.Create('Cannot convert "'+Value+'" to Boolean');
  end;

  if CompareText(sVarType, 'varByte')=0 then
  begin
    b := Byte(StrToint(Value));
    Result := b;
    exit;
  end;

  Raise Exception.Create('Conversion to '+ sVarType + ' not implemented');
end;

procedure TForm2.StringGrid1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   if Button = mbRight then
    StringGrid1.Perform(WM_LBUTTONDOWN, 0, MakeLParam(Word(X), Word(Y)));
end;


function TForm2.ShowBasicVariantType(varVar: Variant):string;
var
  typeString : string;
  basicType  : Integer;

begin
  // Get the Variant basic type :
  // this means excluding array or indirection modifiers
  basicType := VarType(varVar) and VarTypeMask;

  // Set a string to match the type
  case basicType of
    varEmpty     : typeString := 'varEmpty';
    varNull      : typeString := 'varNull';
    varSmallInt  : typeString := 'varSmallInt';
    varInteger   : typeString := 'varInteger';
    varSingle    : typeString := 'varSingle';
    varDouble    : typeString := 'varDouble';
    varCurrency  : typeString := 'varCurrency';
    varDate      : typeString := 'varDate';
    varOleStr    : typeString := 'varOleStr';
    varDispatch  : typeString := 'varDispatch';
    varError     : typeString := 'varError';
    varBoolean   : typeString := 'varBoolean';
    varVariant   : typeString := 'varVariant';
    varUnknown   : typeString := 'varUnknown';
    varByte      : typeString := 'varByte';
    varWord      : typeString := 'varWord';
    varLongWord  : typeString := 'varLongWord';
    varInt64     : typeString := 'varInt64';
    varStrArg    : typeString := 'varStrArg';
    varString    : typeString := 'varString';
    varAny       : typeString := 'varAny';
    varTypeMask  : typeString := 'varTypeMask';
  end;

  Result :=  typeString;
end;


procedure TForm2.PopupMenu1Popup(Sender: TObject);
var
   sOld:string;
begin
    sOld := StringGrid1.Cells[2, StringGrid1.Row];
    if (StringGrid1.RowCount = 1)
        or
        (CompareStr(sOld, '<Array>')=0)
        or
        (aFileRepParams = nil)
    then
      Abort;
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  aFileRepParams.Free;
end;

procedure TForm2.Button2Click(Sender: TObject);
var
    SaveFileName: string;
begin
  if SaveDialog1.Execute then
  begin
     SaveFileName :=  SaveDialog1.FileName;
     if Length(Trim(ExtractFileExt(SaveFileName))) = 0 then
        SaveFileName := SavefileName +'.rwp';
     aFileRepParams.SaveToFile(SaveFileName);
  end;
end;

procedure TForm2.FormShow(Sender: TObject);
begin

 if (ParamCount > 0) and FileExists(ParamStr(1)) then
     DisplayParametersContentFromFile(ParamStr(1));

end;

procedure TForm2.Change1Click(Sender: TObject);
var
  M: TMemo;
  V: Variant;
  jLowBound: integer;
  jLineToChange: integer;
  sToChange: string;
  sNew: string;
  U:Variant;
  UNew:Variant;
  X:Variant;
begin
  M:= TMemo(TPopupMenu(TMenuItem(Sender).GetParentMenu).PopupComponent);
  V := aFileRepParams[ M.Tag].Value; //  Variant Array;
  if VarIsArray(V) then
  begin
    jLowBound := VarArrayLowBound(V, 1);
    jLineToChange := M.Perform(EM_LINEFROMCHAR,M.SelStart, 0) ; // get line number of cursor
    sToChange := M.Lines[jLineToChange];
    sNew := InputBox('Parameter change', 'Alter parameter value', sToChange);
    if CompareStr(sNew, sToChange) <> 0 then
    begin
      U := V[jLowBound + jLineToChange];
      UNew := VarAsType(sNew, VarType(U));
      X := aFileRepParams[ M.Tag].Value;
      X[jLowBound + jLineToChange] := UNew;
      aFileRepParams[ M.Tag].Value := X;
      M.Lines[jLineToChange] := sNew;
      Button2.Visible := True;
    end;
  end;
end;

procedure TForm2.Button3Click(Sender: TObject);
var
  ASlist: IisStringList;
  Xstring: string;
  RWInstance:TrwReportParams;
  iIS: IisStream;
begin
   if OpenDialog2.Execute then
   begin
       ASlist := TIsStringList.Create;
       ASList.LoadFromFile(OpenDialog2.FileName);
       XString := ASlist.Text;
       if Length(Trim(XString))= 0 then Exit;
       RWInstance := TrwReportParams.Create;
       try
          TXMLRWConvertor.ReportParametersFromXML(XString, RWInstance);
          iiS := TisStream.CreateInMemory();
          RWInstance.SaveToStream(iis.RealStream);
          iiS.Position := 0;
          DisplayParametersContentFromStream(iiS.RealStream);
       finally
          RWInstance.Free;
       end;
       Button2.Visible := True;
   end; // of Dialog Execute
end;

procedure TForm2.Button4Click(Sender: TObject);
var
  SaveFileName: string;
  outStream: iIsStream;
begin
  if NOT Assigned( aFileRepParams) then
     Exit;

  if SaveDialog2.Execute then
  begin
     SaveFileName := SaveDialog2.FileName;
     if FileExists(SaveDialog2.FileName) and NOT ( AnsiEndsStr('.xml.xml', SaveDialog2.FileName)) then
       SaveFileName := SaveDialog2.FileName  + '.xml'
     else
        SaveFileName := SaveDialog2.FileName;
  end;

  outStream := TisStream.CreateInMemory();
  TXMLRWConvertor.SaveReportParametersToXML(  aFileRepParams, outStream);
  Assert(Assigned(outStream));
  outStream.SaveToFile(SaveFileName);
end;

end.
