object Form2: TForm2
  Left = 516
  Top = 209
  Width = 637
  Height = 417
  Caption = 'ReportParameterViewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 621
    Height = 337
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Parameter list'
      object StringGrid1: TStringGrid
        Left = 0
        Top = 0
        Width = 613
        Height = 309
        Align = alClient
        ColCount = 4
        RowCount = 1
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = PopupMenu1
        TabOrder = 0
        OnMouseDown = StringGrid1MouseDown
      end
      object evDBGrid1: TevDBGrid
        Left = 280
        Top = 16
        Width = 320
        Height = 120
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TForm2\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        Visible = False
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 621
    Height = 41
    Align = alTop
    TabOrder = 1
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 121
      Height = 25
      Caption = 'Open parameter file'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 152
      Top = 8
      Width = 137
      Height = 25
      Caption = 'Save .rwp file'
      TabOrder = 1
      Visible = False
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 344
      Top = 10
      Width = 137
      Height = 25
      Caption = 'Open XML'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 496
      Top = 10
      Width = 108
      Height = 25
      Caption = 'Save as XML'
      TabOrder = 3
      OnClick = Button4Click
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Report Parameters (*.rwp)|*.rwp'
    Left = 104
    Top = 8
  end
  object DataSource1: TDataSource
    Left = 200
    Top = 32
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ClientDataSet1Idx'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 232
    Top = 32
    object ClientDataSet1Idx: TIntegerField
      FieldName = 'Idx'
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 236
    Top = 161
    object C1: TMenuItem
      Caption = 'Change'
      OnClick = C1Click
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = 'Report Parameters (*.rwp)|*.rwp'
    Left = 280
    Top = 8
  end
  object PopupMenuArray: TPopupMenu
    Left = 380
    Top = 225
    object Change1: TMenuItem
      Caption = 'Change'
      OnClick = Change1Click
    end
  end
  object OpenDialog2: TOpenDialog
    Filter = 'XML,txt|*.xml;*.txt'
    Left = 424
    Top = 24
  end
  object SaveDialog2: TSaveDialog
    Filter = 'XML|*.xml'
    Left = 520
    Top = 24
  end
end
