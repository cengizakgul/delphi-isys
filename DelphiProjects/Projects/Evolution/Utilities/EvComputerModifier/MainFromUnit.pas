unit MainFromUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OnGuard, StdCtrls, OgUtil;

type
  TMainForm = class(TForm)
    edMachineID: TEdit;
    Label4: TLabel;
    CopyButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure CopyButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
var
  N: Cardinal;
begin
  N := Cardinal(CreateMachineID([midUser, midSystem, midNetwork, midDrives]));
  edMachineID.Text := BufferToHex(N, SizeOf(N));
end;

procedure TMainForm.CopyButtonClick(Sender: TObject);
begin
  edMachineID.SelectAll;
  edMachineID.CopyToClipboard;
end;

end.
