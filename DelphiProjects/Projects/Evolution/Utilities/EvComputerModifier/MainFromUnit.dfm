object MainForm: TMainForm
  Left = 349
  Top = 134
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Evolution Computer Modifier Utility'
  ClientHeight = 74
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 8
    Top = 12
    Width = 85
    Height = 13
    Caption = 'Computer Modifier'
  end
  object edMachineID: TEdit
    Left = 102
    Top = 10
    Width = 203
    Height = 21
    Color = clInactiveBorder
    ReadOnly = True
    TabOrder = 1
  end
  object CopyButton: TButton
    Left = 112
    Top = 40
    Width = 107
    Height = 25
    Caption = 'Copy to Clipboard'
    TabOrder = 0
    OnClick = CopyButtonClick
  end
end
