object EvPerfMain: TEvPerfMain
  Left = 360
  Top = 190
  Width = 765
  Height = 430
  Caption = 'Evo DB performance monitor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 757
    Height = 396
    ActivePage = tsMonitor
    Align = alClient
    TabOrder = 0
    object tsSettings: TTabSheet
      Caption = 'Settings'
      TabVisible = False
    end
    object tsMonitor: TTabSheet
      Caption = 'Performance Monitor'
      ImageIndex = 1
      object Chart: TChart
        Left = 0
        Top = 0
        Width = 749
        Height = 368
        AllowPanning = pmNone
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginTop = 5
        Title.Text.Strings = (
          'SB Database')
        BottomAxis.AxisValuesFormat = 'hh:mm:ss'
        BottomAxis.DateTimeFormat = 'hh:nn:ss'
        BottomAxis.Title.Caption = 'Time stamp'
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.AxisValuesFormat = '#,##0.##'
        LeftAxis.Maximum = 753.000000000000000000
        LeftAxis.Title.Caption = 'Time (ms)'
        Legend.LegendStyle = lsSeries
        Legend.TextStyle = ltsPlain
        RightAxis.Visible = False
        ScaleLastPage = False
        TopAxis.Visible = False
        View3D = False
        Align = alClient
        TabOrder = 0
        object PatternLine: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'Pattern'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Style = psCircle
          Pointer.VertSize = 2
          Pointer.Visible = False
          XValues.DateTime = True
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
    end
  end
end
