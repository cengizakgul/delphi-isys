unit EvPerfMainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart,
  isThreadManager, isBaseClasses, EvContext, EvMainboard, isStatistics,
  evDataSet, EvCommonInterfaces, EvStreamUtils;

const CM_UPDATE_CHART = WM_USER + 1;

type
  IevCountValue = interface
  ['{221DC093-43F7-4661-958E-8B15C9D3DA1A}']
    function  TimeStamp: TDateTime;
    function  ExecTime: Cardinal;
  end;

  TevCountValues = array of IevCountValue;

  IevMonitorCounter = interface
  ['{A6DDFDE0-ECC6-44C1-A3A9-0228EDA2BC33}']
    function  GetName: String;
    function  GetParams: IisListOfValues;
    function  GetInterval: Cardinal;
    procedure SetInterval(const AValue: Cardinal);
    procedure Start;
    procedure Stop;
    function  GetValues: TevCountValues;
    property  Name: String read GetName;
    property  Interval: Cardinal read GetInterval write SetInterval;
    property  Params: IisListOfValues read GetParams;
  end;


  IevMonitorCallback = interface
  ['{ACF1FF0C-40E3-41E9-B940-D44EF7227810}']
    procedure OnNewValue(const ACounter: IevMonitorCounter);
  end;


  IevMonitor = interface
  ['{3EDC9E64-2F6B-4EE0-AA57-8C653EF59BFD}']
    procedure AddCounter(const ACounter: IevMonitorCounter);
    function  GetCounter(const AIndex: Integer): IevMonitorCounter;
    function  Count: Integer;
    procedure SetCallback(const ACallback: IevMonitorCallback);
    function  GetCallback: IevMonitorCallback;    
  end;

  TEvPerfMain = class(TForm, IevMonitorCallback)
    PageControl1: TPageControl;
    tsSettings: TTabSheet;
    tsMonitor: TTabSheet;
    Chart: TChart;
    PatternLine: TLineSeries;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    FMonitor: IevMonitor;
    procedure CMUpdateChart(var Message: TMessage); message CM_UPDATE_CHART;
    procedure Start;
    procedure Stop;
    function  AddSeries(const ATitle: String): TLineSeries;
//    procedure RemoveSeries(const ATitle: String);

    procedure OnNewValue(const ACounter: IevMonitorCounter);
  public
  end;

implementation

uses SysConst, Math;

{$R *.dfm}

const
  cStatFrameSize = 600;

type
  TevMonitor =  class(TisCollection, IevMonitor)
  private
    FCallback: IevMonitorCallback;
    procedure AddCounter(const ACounter: IevMonitorCounter);
    function  GetCounter(const AIndex: Integer): IevMonitorCounter;
    function  Count: Integer;
    procedure SetCallback(const ACallback: IevMonitorCallback);
    function  GetCallback: IevMonitorCallback;
  end;

  TevMonitorCounter =  class(TisNamedObject, IevMonitorCounter)
  private
    FParams: IisListOfValues;
    FTaskID: TTaskID;
    FInterval: Cardinal;
    FValues:  IisList;
    function  GetParams: IisListOfValues;
    function  GetInterval: Cardinal;
    procedure SetInterval(const AValue: Cardinal);
    function  GetValues: TevCountValues;
    procedure MonitorThread(const Params: TTaskParamList);
    function  Monitor: IevMonitor;
  protected
    procedure DoOnConstruction; override;
    procedure DoOnDestruction; override;
    procedure BeforeStart; virtual;
    procedure ExecCounter; virtual; abstract;
    procedure AfterFinish; virtual;
    procedure Start;
    procedure Stop;
  public
    constructor Create(const AName: String); reintroduce;
  end;

  TevCountValue = class(TisInterfacedObject, IevCountValue)
  private
    FTimeStamp: TDateTime;
    FExecTime: Cardinal;
  protected
    function  TimeStamp: TDateTime;
    function  ExecTime: Cardinal;
  public
    constructor Create(const ATimeStamp: TDateTime; const AValue: Cardinal);
  end;

  TevQueryCounter = class(TevMonitorCounter)
  protected
    procedure BeforeStart; override;  
    procedure ExecCounter; override;
  end;


{ TEvPerfMain }

procedure TEvPerfMain.Start;
var
  i: Integer;
begin
  for i := 0 to FMonitor.Count - 1 do
    FMonitor.GetCounter(i).Start;
end;

procedure TEvPerfMain.Stop;
var
  i: Integer;
begin
  for i := 0 to FMonitor.Count - 1 do
    FMonitor.GetCounter(i).Stop;
end;

procedure TEvPerfMain.OnNewValue(const ACounter: IevMonitorCounter);
begin
  if HandleAllocated then
  begin
    ACounter._AddRef;
    PostMessage(Handle, CM_UPDATE_CHART, Integer(Pointer(ACounter)), 0);
  end;
end;

procedure TEvPerfMain.CMUpdateChart(var Message: TMessage);
var
  Counter: IevMonitorCounter;
  Series: TChartSeries;
  Vals: TevCountValues;
  i: Integer;
  MaxVal, MinVal: Double;
begin
  Counter := IInterface(Pointer(Message.wParam)) as IevMonitorCounter;
  try
    if HandleAllocated then
    begin
      Vals := Counter.GetValues;

      Series := nil;
      for i := 0 to Chart.SeriesCount - 1 do
        if Chart.Series[i].Title = Counter.Name then
        begin
          Series := Chart.Series[i];
          break;
        end;

      if not Assigned(Series) then
        Series := AddSeries(Counter.Name);

      Series.Clear;
      for i := 0 to High(Vals) do
        Series.AddXY(Vals[i].TimeStamp, Vals[i].ExecTime);

      // Define Min and Max values
      MinVal := MaxInt;
      MaxVal := 0;
      for i := 0 to Chart.SeriesCount - 1 do
      begin
        if MinVal > Chart.Series[i].MinYValue then
          MinVal := Chart.Series[i].MinYValue;
        if MaxVal < Chart.Series[i].MaxYValue then
          MaxVal := Chart.Series[i].MaxYValue;
      end;

      Chart.LeftAxis.Minimum := 0;
      Chart.LeftAxis.Maximum := MaxVal + RoundTo((MaxVal - MinVal) * 0.1, -2);
      if MinVal - RoundTo((MaxVal - MinVal) * 0.1, -2) > 0 then
        Chart.LeftAxis.Minimum := MinVal - RoundTo((MaxVal - MinVal) * 0.1, -2);
    end;
  finally
    Counter._Release;
  end;
end;

function TEvPerfMain.AddSeries(const ATitle: String): TLineSeries;
const
  Colors: array [0..9] of TColor = (clRed, clBlue, clGreen, clLime, clMaroon, clOlive, clPurple, clAqua, clFuchsia, clYellow);
var
  S: IisStream;
  i: Integer;
begin
  S := TisStream.Create;
  S.RealStream.WriteComponent(PatternLine);
  S.Position := 0;
  Result := S.RealStream.ReadComponent(nil) as TLineSeries;
  InsertComponent(Result);
  Result.Title := ATitle;
  Chart.AddSeries(Result);

  for i := 0 to Chart.SeriesCount - 1 do
    Chart.Series[i].SeriesColor := Colors[i];
end;
{
procedure TEvPerfMain.RemoveSeries(const ATitle: String);
var
  Series: TChartSeries;
  i: Integer;
begin
  Series := nil;
  for i := 0 to Chart.SeriesCount - 1 do
    if Chart.Series[i].Title = ATitle then
    begin
      Series := Chart.Series[i];
      break;
    end;

  if Assigned(Series) then
  begin
    Chart.RemoveSeries(Series);
    Series.Free;
  end;
end;
}
{ TevMonitorCounter }

procedure TevMonitorCounter.AfterFinish;
begin
end;

procedure TevMonitorCounter.BeforeStart;
begin
end;

constructor TevMonitorCounter.Create(const AName: String);
begin
  inherited Create;
  SetName(AName);
end;

procedure TevMonitorCounter.DoOnConstruction;
begin
  inherited;
  FParams := TisListOfValues.Create;
  FValues := TisList.Create;
  FInterval := 5000;
end;

procedure TevMonitorCounter.DoOnDestruction;
begin
  Stop;
  inherited;
end;

function TevMonitorCounter.GetInterval: Cardinal;
begin
  Result := FInterval;
end;

function TevMonitorCounter.GetParams: IisListOfValues;
begin
  Result := FParams;
end;

function TevMonitorCounter.GetValues: TevCountValues;
var
  i: Integer;
begin
  FValues.Lock;
  try
    SetLength(Result, FValues.Count);
    for i := 0 to FValues.Count - 1 do
      Result[i] := FValues[i] as IevCountValue;
  finally
    FValues.Unlock;
  end;
end;

function TevMonitorCounter.Monitor: IevMonitor;
begin
  Result := GetOwner as IevMonitor;
end;

procedure TevMonitorCounter.MonitorThread(const Params: TTaskParamList);
var
  Stat: IisSimpleStatEvent;

  procedure LoopStep;
  var
    Res: Boolean;
    Val: IevCountValue;
  begin
    Stat.StartWatch;
    try
      try
        ExecCounter;
        Res := True;
      except
        Res := False;
      end;
    finally
      Stat.StopWatch;
    end;

    if Res then
    begin
      Val := TevCountValue.Create(Stat.BeginTime, Stat.Duration);

      FValues.Lock;
      try
        if FValues.Count >= cStatFrameSize then
          FValues.Delete(0);
        FValues.Add(Val);
      finally
        FValues.Unlock;
      end;

      if Monitor.GetCallback <> nil then
        Monitor.GetCallback.OnNewValue(Self);
    end;
  end;

begin
  FValues.Clear;

  BeforeStart;
  try
    Stat := CreateSimpleStatEvent;
    while not CurrentThreadTaskTerminated do
    begin
      LoopStep;
      Snooze(FInterval);
    end;
  finally
    AfterFinish;
  end;
end;

procedure TevMonitorCounter.SetInterval(const AValue: Cardinal);
begin
  FInterval := AValue;
end;

procedure TevMonitorCounter.Start;
begin
  if FTaskID = 0 then
    FTaskID := Context.RunParallelTask(MonitorThread, Self, MakeTaskParams([]), 'Monitor: ' + GetName, tpLower);
end;

procedure TevMonitorCounter.Stop;
begin
  if FTaskID <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FTaskID);
    GlobalThreadManager.WaitForTaskEnd(FTaskID);
    FTaskID := 0;
  end;
end;

procedure TEvPerfMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Stop;
  FMonitor := nil;
end;

procedure TEvPerfMain.FormShow(Sender: TObject);
var
  Counter: IevMonitorCounter;
  s: String;
begin
  Chart.RemoveAllSeries;

  FMonitor := TevMonitor.Create;
  FMonitor.SetCallback(Self);

  Counter := TevQueryCounter.Create('Sb_User');
  Counter.Params.AddValue('Query', 'SELECT * from sb_user WHERE active_record = ''C'' and Upper(user_id) = ''' + Context.UserAccount.User + '''');
  Counter.Interval := 10000;
  FMonitor.AddCounter(Counter);

  Counter := TevQueryCounter.Create('Sb_Sec_Clients');
  s := 'SELECT c.Cl_Nbr '+
      'FROM  Sb_Sec_Clients c,  Sb_Sec_Group_Members gm '+
      'WHERE c.ACTIVE_RECORD = ''C'' AND ' +
      '      gm.ACTIVE_RECORD = ''C'' AND ' +
      '      c.Sb_Sec_Groups_Nbr = gm.Sb_Sec_Groups_Nbr AND'+
      '      gm.Sb_User_Nbr = ' + IntToStr(Context.UserAccount.InternalNbr) +
      ' UNION ' +
      'SELECT c.Cl_Nbr ' +
      'FROM Sb_Sec_Clients c ' +
      'WHERE c.ACTIVE_RECORD = ''C'' AND ' +
      '      c.Sb_User_Nbr = ' + IntToStr(Context.UserAccount.InternalNbr);
  Counter.Params.AddValue('Query', s);
  Counter.Interval := 10000;
  FMonitor.AddCounter(Counter);

  Counter := TevQueryCounter.Create('Sb_Sec_Rights');
  s := 'SELECT ''G'' src, Cast(Trim(strcopy(tag, 2, 128)) as VarChar(128)) tag, context ' +
                       'FROM   SB_SEC_RIGHTS r, SB_SEC_GROUP_MEMBERS gm ' +
                       'WHERE  r.ACTIVE_RECORD = ''C'' AND gm.ACTIVE_RECORD = ''C'' AND ' +
                       '       r.SB_SEC_GROUPS_NBR = gm.SB_SEC_GROUPS_NBR AND ' +
                       '       gm.SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr) + ' AND' +
                       '       r.tag STARTS ''M'' ' +
                       'UNION ' +
                       'SELECT ''U'' src, Cast(Trim(strcopy(tag, 2, 128)) as VarChar(128)) tag, context ' +
                       'FROM   SB_SEC_RIGHTS r ' +
                       'WHERE  r.ACTIVE_RECORD = ''C'' AND ' +
                       '       r.SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr) + ' AND ' +
                       '       r.tag STARTS ''M'' ' +
                       'ORDER BY 1, 2';
  Counter.Params.AddValue('Query', s);
  Counter.Interval := 10000;
  FMonitor.AddCounter(Counter);

  Counter := TevQueryCounter.Create('Sb_Sec_Records');
  s := 'SELECT s.Cl_Nbr, s.Filter_Type, s.Custom_Expr '+
                        'FROM  Sb_Sec_Row_Filters s,  Sb_Sec_Group_Members gm '+
                        'WHERE s.ACTIVE_RECORD = ''C'' AND ' +
                        '      gm.ACTIVE_RECORD = ''C'' AND ' +
                        '      s.Sb_Sec_Groups_Nbr = gm.Sb_Sec_Groups_Nbr AND'+
                        '      gm.Sb_User_Nbr = ' + IntToStr(Context.UserAccount.InternalNbr) +
                        ' UNION ' +
                        'SELECT s.Cl_Nbr, s.Filter_Type, s.Custom_Expr ' +
                        'FROM Sb_Sec_Row_Filters s ' +
                        'WHERE s.ACTIVE_RECORD = ''C'' AND ' +
                        '      s.Sb_User_Nbr = ' + IntToStr(Context.UserAccount.InternalNbr);

  Counter.Params.AddValue('Query', s);
  Counter.Interval := 10000;
  FMonitor.AddCounter(Counter);


  Counter.Params.AddValue('Query', s);
  Counter.Interval := 10000;
  FMonitor.AddCounter(Counter);

  Start;
end;

{ TevMonitor }

procedure TevMonitor.AddCounter(const ACounter: IevMonitorCounter);
begin
  AddChild(ACounter as IisInterfacedObject);
end;

function TevMonitor.Count: Integer;
begin
  Result := ChildCount;
end;

function TevMonitor.GetCallback: IevMonitorCallback;
begin
  Result := FCallback;
end;

function TevMonitor.GetCounter(const AIndex: Integer): IevMonitorCounter;
begin
  Result := GetChild(AIndex) as IevMonitorCounter;
end;

procedure TevMonitor.SetCallback(const ACallback: IevMonitorCallback);
begin
  FCallback := ACallback;
end;

{ TevQueryCounter }

procedure TevQueryCounter.BeforeStart;
begin
  inherited;
  // In order to establish connection 
  ExecCounter;
end;

procedure TevQueryCounter.ExecCounter;
var
  Q: IevQuery;
begin
  ctx_DBAccess.CurrentClientNbr := FParams.TryGetValue('ClientID', 0);
  Q := TevQuery.Create(FParams.Value['Query']);
  Q.Execute;
end;

{ TevCountValue }

constructor TevCountValue.Create(const ATimeStamp: TDateTime; const AValue: Cardinal);
begin
  inherited Create;
  FTimeStamp := ATimeStamp;
  FExecTime := AValue;
end;

function TevCountValue.ExecTime: Cardinal;
begin
  Result := FExecTime;
end;

function TevCountValue.TimeStamp: TDateTime;
begin
  Result := FTimeStamp;
end;


end.
