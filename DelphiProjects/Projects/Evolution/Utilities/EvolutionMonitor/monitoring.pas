unit monitoring;

interface

uses Windows, SysUtils, Classes, isBaseClasses, EvTransportShared, EvTransportDatagrams,
     isSocket, evStreamUtils, isBasicUtils, isThreadManager, evRemoteMethods, EvConsts,
     evTransportInterfaces, EvControllerInterfaces, isAppIDs, ISZippingRoutines, isStreamTCP,
     mmSystem, EvBasicUtils, mcDSClient, mcTypes, isConsoleApp, Variants, isLogFile;

procedure Run;

implementation

uses DateUtils;

type
  ICommand = interface
  ['{0E6C32E7-DA83-4AEA-9F33-33F8225508BE}']
    procedure Execute;
  end;

  TCommandClass = class of TCommand;

  TCommand = class(TisInterfacedObject, ICommand)
  protected
    class function  Description: String; virtual;
    procedure ShowTitleHelp; virtual;
    procedure ShowContextHelp; virtual;
    procedure Run; virtual; abstract;
    class function Command: String; virtual;
  public
    class function GetTypeID: String; override;
    procedure Execute;
  end;


  TCmdHELP = class(TCommand)
  protected
    class function  Description: String; override;
    procedure ShowContextHelp; override;
    procedure Run; override;
  end;


  TCacheableCommand = class(TCommand)
  private
    function  GetDataFromCache: IisInterfacedObject;
    procedure PutDataToCache(const AData: IisInterfacedObject);
  protected
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; virtual; abstract;
    function  CacheID: String; virtual;
    function  GetData: IisInterfacedObject;
  end;


  TCmdLOGIN = class(TCommand)
  protected
    class function  Description: String; override;
    procedure ShowContextHelp; override;
    procedure Run; override;
  end;


  TDSCommand = class(TCacheableCommand)
  private
    FDS: ImcDMControl;
    function DSHost: String;
  protected
    function GetDS: ImcDMControl;
    function CacheID: String; override;
  end;


  TDSListOfValuesCommand = class(TDSCommand)
  protected
    procedure PostProcessData(const AData: IisListOfValues); virtual;
    procedure Run; override;
  end;


  TCmdASSTAT = class(TDSListOfValuesCommand)
  protected
    class function  Description: String; override;
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; override;
  end;


  TCmdASERR = class(TDSCommand)
  protected
    class function  Description: String; override;
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; override;
    procedure Run; override;
  end;


  TCmdASSTATDOMAIN = class(TDSListOfValuesCommand)
  private
    function DomainID: String;
  protected
    class function  Description: String; override;
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; override;
    function  CacheID: String; override;
  end;


  TCmdRBSTAT = class(TDSListOfValuesCommand)
  protected
    class function Description: String; override;
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; override;
    procedure PostProcessData(const AData: IisListOfValues); override;
  end;


  TCmdRPSTAT = class(TDSListOfValuesCommand)
  protected
    class function Description: String; override;
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; override;
  end;


  TCmdRRSTAT = class(TDSListOfValuesCommand)
  protected
    class function Description: String; override;
    procedure ShowContextHelp; override;
    function  PrepareData: IisInterfacedObject; override;
  end;



// Internal stuff

  IevClient = interface
  ['{AD70A610-D30D-4814-A4F7-D31FA16CAAF0}']
    function  CreateConnection(const AHost, APort, AUser, APassword: String; const ASessionID, AContextID: TisGUID;
                               const ALocalIPAddr: String = ''; const AAppID: String = ''): TisGUID;
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
    function  GetCommunicator: IisAsyncTCPClient;
    function  GetSession(const ASessionID: TisGUID): IevSession;
  end;

  TevClientDispatcher = class(TevDatagramDispatcher)
  protected
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
  end;

  TevTCPClient = class(TevCustomTCPClient, IevClient)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
    function  GetCommunicator: IisAsyncTCPClient;
  end;


const
  cCacheFile = 'EvoMonitor.tmp';

procedure Run;
var
  cmd, ErrFileName: String;
  CmdObj: ICommand;
  S: IisStream;
begin
  try
    if AppSwitches.Count = 0 then
      cmd := 'HELP'
    else
      cmd := AppSwitches[0].Value;

    CheckCondition(ObjectFactory.IsRegistered('CMD_' + cmd), Format('Command %s is not recognized', [cmd]));

    CmdObj := ObjectFactory.CreateInstanceOf('CMD_' + cmd) as ICommand;
    CmdObj.Execute;
  except
    on E: Exception do
    begin
      Console.WriteErrLn('ERROR: ' + E.Message);
      ExitCode := 1;

      if AppSwitches.ValueExists('errlog') then
      begin
        ErrFileName := AppSwitches.Value['errlog'];
        if ErrFileName = '' then
          ErrFileName := ChangeFileExt(AppFileName, '_Errors.txt');
        S := TisStream.CreateFromFile(ErrFileName, False, False, False, True, 10);
        S.Position := S.Size;
        S.WriteLn(FormatDateTime('mm/dd/yyyy hh:nn:ss', Now) + '   ' + StringReplace(StringReplace(E.Message, #10, '', [rfReplaceAll]), #13, '', [rfReplaceAll]));
        S := nil;
      end;
    end;
  end;
end;

{ TevTCPClient }

function TevTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevClientDispatcher.Create;
end;

function TevTCPClient.GetCommunicator: IisAsyncTCPClient;
begin
  Result := GetCommunicator;
end;

{ TevClientDispatcher }

procedure TevClientDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
begin
  inherited;
  Handled := False;
end;

function TevClientDispatcher.CheckClientAppID(
  const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoClientAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoRWAdapterAppInfo.AppID);
end;

procedure TevClientDispatcher.DoOnSessionStateChange(
  const ASession: IevSession; const APreviousState: TevSessionState);
begin
  inherited;
  if (ASession.State = ssInvalid) and (APreviousState = ssActive) then
  begin
  end;
end;

{ TCommand }

class function TCommand.Command: String;
begin
  Result := Copy(ClassName, 5, Length(ClassName));
end;

class function TCommand.Description: String;
begin
  Result := '';
end;

procedure TCommand.Execute;
begin
  if AppSwitches.ValueExists('?') then
    ShowContextHelp
  else
    Run;
end;

class function TCommand.GetTypeID: String;
begin
  Result := 'CMD_' + Command;
end;

procedure TCommand.ShowContextHelp;
begin
  ShowTitleHelp;
  
  Console.Write('Command: ', clYellow); Console.Writeln(Command, clWhite);
  Console.Writeln;
  Console.Write('Description: ', clYellow); Console.Writeln(Description);
end;

procedure TCommand.ShowTitleHelp;
begin
  Console.WriteLn;
  Console.WriteLn('                     Evolution Monitoring Utility ', clLime);
  Console.WriteLn('                             v' + AppVersion, clLime);
  Console.WriteLn('                   Copyright (C) 2011 iSystems, LLC', clLime);
  Console.WriteLn;
end;

{ TCmdHelp }

class function TCmdHELP.Description: String;
begin
  Result := 'Show help';
end;

procedure TCmdHELP.Run;
var
  RegClasses: IisPointerList;
  i: Integer;
  Cl: TisInterfacedObjectClass;
begin
  ShowTitleHelp;

  Console.Write('Usage: ', clYellow);
  Console.Writeln('EvoMonitor [command] [options]');
  Console.Writeln;
  Console.Writeln('Commands:', clYellow);

  RegClasses := ObjectFactory.GetRegisteredClasses;
  for i := 0 to RegClasses.Count - 1 do
  begin
    Cl := TisInterfacedObjectClass(RegClasses[i]);
    if Cl.InheritsFrom(TCommand) then
    begin
      Console.Write('   ');
      Console.Write(Copy(TCommandClass(Cl).Command + '               ', 1, 15), clWhite);
      Console.Writeln(TCommandClass(Cl).Description);
    end;
  end;

  Console.Writeln;
  Console.Writeln('For detailed information run:  EvoMonitor <command> -?');
end;

procedure TCmdHELP.ShowContextHelp;
begin
end;

{ TCmdLOGIN }

class function TCmdLOGIN.Description: String;
begin
  Result := 'Measure login/connection time in milliseconds';
end;

procedure TCmdLOGIN.Run;
var
  s: String;
  StartTime, EndTime: Cardinal;
  Port, RBAddress, UserName, Password: String;
  SessionID: TisGUID;
  Client: TevTCPClient;
begin
  SetAppID(EvoClientAppInfo.AppID);

  Client := TevTCPClient.Create;

  s := AppSwitches.TryGetValue('host', 'localhost');
  RBAddress := GetNextStrValue(s, ':');
  Port := s;
  if Port = '' then
    Port := IntToStr(TCPClient_Port);
  UserName := AppSwitches.TryGetValue('user', 'guest');
  Password := AppSwitches.TryGetValue('password', '');

  StartTime := timeGetTime;
  SessionID := Client.CreateConnection(RBAddress, Port, UserName, HashPassword(Password), '', '');
  EndTime := timeGetTime;

  Client.Disconnect(SessionID);

  Console.Writeln(IntToStr(EndTime - StartTime));
end;


procedure TCmdLOGIN.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('Usage: ', clYellow);
  Console.Writeln('EvoMonitor ' + Command + ' [options]');
  Console.Writeln;
  Console.Writeln('Options:', clYellow);
  Console.Write('  -host <host>           ', clWhite); Console.Writeln('Evolution server (LOCALHOST if omitted)');
  Console.Write('  -host <host:port>      ', clWhite); Console.Writeln('Evolution server''s port (9500 if omitted)');
  Console.Write('  -host <host:port:SSL>  ', clWhite); Console.Writeln('Use SSL encryption');
  Console.Writeln;
  Console.Write('  -user <user>           ', clWhite); Console.Writeln('User name (GUEST if omitted)');
  Console.Write('  -password <password>   ', clWhite); Console.Writeln('User''s password');
  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Check if RB/AS/DM is alive (tests only propriatery communication)');
  Console.Writeln('      EvoMonitor ' + Command + ' -host evo.mysb.com');
  Console.Writeln;
  Console.Writeln('   Test RB/AS login (involves security module)');
  Console.Writeln('      EvoMonitor ' + Command + ' -host evo.mysb.com -user john -password 12345');
  Console.Writeln;
  Console.Writeln('   Check if RR is alive (tests only propriatery communication)');
  Console.Writeln('      EvoMonitor ' + Command + ' -host evo.mysb.com:9901:SSL');
  Console.Writeln;
  Console.Writeln('   Test RR login');
  Console.Writeln('      EvoMonitor ' + Command + ' -host evo.mysb.com:9901:SSL -user john -password 12345');
end;

{ TCmdASSTAT }

class function TCmdASSTAT.Description: String;
begin
  Result := 'Show ADR Server summary statistics';
end;

function TCmdASSTAT.PrepareData: IisInterfacedObject;
var
  s, ServerActivity: String;
  SrvActivityTime: TDateTime;
  Status: IisParamsCollection;
  Res: IisListOfValues;
  i: Integer;
  TotalQueued, TotalExecuting, TotalUploading, TotalConnected: Integer;
begin
  TotalQueued := 0;
  TotalExecuting := 0;
  TotalUploading := 0;
  TotalConnected := 0;
  ServerActivity := '';
  SrvActivityTime := 0;

  Status := GetDS.EvolutionControl.ASControl.GetStatus;

  for i := 0 to Status.Count - 1 do
  begin
    TotalQueued := TotalQueued + Status[i].Value['RepQueued'] + Status[i].Value['UplExecuting'];
    TotalExecuting := TotalExecuting + Status[i].Value['RepExecuting'];
    TotalUploading := TotalUploading + Status[i].Value['UplQueued'];

    if Status[i].Value['Connected'] then
      Inc(TotalConnected);

    s := Status[i].Value['LastActivity'];
    s := GetNextStrValue(s, ' ') + ' ' + GetNextStrValue(s, ' ') + ' ' +GetNextStrValue(s, ' ');
    if SrvActivityTime < StrToDateTimeDef(s, 0) then
    begin
      SrvActivityTime := StrToDateTime(s);
      ServerActivity := Status.ParamName(i) + ': ' + Status[i].Value['LastActivity'];
    end;
  end;

  Res := TisListOfValues.Create;
  Res.AddValue('upload', TotalUploading);
  Res.AddValue('queue', TotalQueued);
  Res.AddValue('exec', TotalExecuting);
  Res.AddValue('conn_clt', TotalConnected);
  Res.AddValue('last_act', ServerActivity);
  Res.AddValue('last_act_time', FormatDateTime('mm/dd/yyyy hh:nn:ss AM/PM', SrvActivityTime));
  Res.AddValue('domain_count', Status.Count);

  Result := Res as IisInterfacedObject;
end;

procedure TCmdASSTAT.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('  -host <host>       ', clWhite); Console.Writeln('ADR Server host name (LOCALHOST if omitted)');
  Console.Writeln;
  Console.Write('  -value exec            ', clWhite); Console.Writeln('Executing requests');
  Console.Write('  -value queue           ', clWhite); Console.Writeln('Queued requests');
  Console.Write('  -value upload          ', clWhite); Console.Writeln('Uploading requests');
  Console.Write('  -value conn_clt        ', clWhite); Console.Writeln('Connected ADR clients');
  Console.Write('  -value last_act        ', clWhite); Console.Writeln('Last ADR server activity');
  Console.Write('  -value last_act_time   ', clWhite); Console.Writeln('Last ADR server activity time');
  Console.Write('  -value domain_count    ', clWhite); Console.Writeln('How many Evo domains are registered');
  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Show ADR server executing requests');
  Console.Writeln('      EvoMonitor ' + Command + ' -host adr1 -value exec');
  Console.Writeln;
  Console.Writeln('   Show ADR server last activity');
  Console.Writeln('      EvoMonitor ' + Command + ' -host adr1 -value last_act');
end;

{ TCmdASSTATDOMAIN }

function TCmdASSTATDOMAIN.CacheID: String;
begin
  Result := inherited CacheID + ';' + DomainID;
end;

class function TCmdASSTATDOMAIN.Description: String;
begin
  Result := 'Show ADR Server domain statistics';
end;

function TCmdASSTATDOMAIN.DomainID: String;
begin
  Result := AppSwitches.TryGetValue('domain', '');
  if Result = '' then
    Result := sDefaultDomain;
end;

function TCmdASSTATDOMAIN.PrepareData: IisInterfacedObject;
var
  s, ServerActivity, ClientActivity, ValID, Connected: String;
  SrvActivityTime, CltActivityTime: TDateTime;
  DomainStatus: IisListOfValues;
  TotalQueued, TotalExecuting, TotalUploading, Latency: Integer;
  Res: IisListOfValues;
begin
  DomainStatus := GetDS.EvolutionControl.ASControl.GetDomainStatus(DomainID);

  TotalQueued := DomainStatus.Value['RepQueued'] + DomainStatus.Value['UplExecuting'];
  TotalExecuting := DomainStatus.Value['RepExecuting'];
  TotalUploading := DomainStatus.Value['UplQueued'];
  Latency := DomainStatus.Value['Latency'];

  if DomainStatus.Value['Connected'] then
    Connected := 'Y'
  else
    Connected := 'N';

  ServerActivity := DomainStatus.Value['SrvLastActivity'];
  ClientActivity := DomainStatus.Value['ClLastActivity'];

  s := ClientActivity;
  s := GetNextStrValue(s, ' ') + ' ' + GetNextStrValue(s, ' ') + ' ' +GetNextStrValue(s, ' ');
  CltActivityTime := StrToDateTimeDef(s, 0);

  s := ServerActivity;
  s := GetNextStrValue(s, ' ') + ' ' + GetNextStrValue(s, ' ') + ' ' +GetNextStrValue(s, ' ');
  SrvActivityTime := StrToDateTimeDef(s, 0);

  ValID := AppSwitches.TryGetValue('value', '');

  Res := TisListOfValues.Create;
  Res.AddValue('upload', TotalUploading);
  Res.AddValue('queue', TotalQueued);
  Res.AddValue('exec', TotalExecuting);
  Res.AddValue('connected', Connected);
  Res.AddValue('last_act', ServerActivity);
  Res.AddValue('cl_last_act', ClientActivity);
  Res.AddValue('last_act_time', FormatDateTime('mm/dd/yyyy hh:nn:ss AM/PM', SrvActivityTime));
  Res.AddValue('cl_last_act_time', FormatDateTime('mm/dd/yyyy hh:nn:ss AM/PM', CltActivityTime));
  Res.AddValue('oper_latency', Latency);  

  Result := Res as IisInterfacedObject;
end;

procedure TCmdASSTATDOMAIN.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('  -host <host>   ', clWhite); Console.Writeln('ADR Server host name (LOCALHOST if omitted)');
  Console.Writeln;
  Console.Write('  -domain <SB domain ID>   ', clWhite); Console.Writeln('SB domain name (DEFAULT if omitted)');
  Console.Writeln;
  Console.Write('  -value exec               ', clWhite); Console.Writeln('Executing requests');
  Console.Write('  -value queue              ', clWhite); Console.Writeln('Queued requests');
  Console.Write('  -value upload             ', clWhite); Console.Writeln('Uploading requests');
  Console.Write('  -value connected          ', clWhite); Console.Writeln('Connected ADR clients');
  Console.Write('  -value last_act           ', clWhite); Console.Writeln('Last ADR server activity');
  Console.Write('  -value last_act_time      ', clWhite); Console.Writeln('Last ADR server activity time');
  Console.Write('  -value cl_last_act        ', clWhite); Console.Writeln('Last ADR client activity');
  Console.Write('  -value cl_last_act_time   ', clWhite); Console.Writeln('Last ADR client activity time');
  Console.Write('  -value oper_latency       ', clWhite); Console.Writeln('ADR operations latency (sec)');
  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Show ADR server executing requests');
  Console.Writeln('      EvoMonitor ' + Command + ' -host adr1 -domain prtax -value exec');
  Console.Writeln;
  Console.Writeln('   Show ADR server last activity time');
  Console.Writeln('      EvoMonitor ' + Command + ' -host adr1 -domain prtax -value last_act_time');
  Console.Writeln;
  Console.Writeln('   Show last activity on ADR client side');
  Console.Writeln('      EvoMonitor ' + Command + ' -host adr1 -domain prtax -value cl_last_act');
end;

{ TDSCommand }

function TDSCommand.CacheID: String;
begin
  Result := inherited CacheID + ';' + DSHost;
end;

function TDSCommand.DSHost: String;
var
  s: String;
begin
  s := AppSwitches.TryGetValue('host', 'localhost');
  Result := GetNextStrValue(s, ':');
end;

function TDSCommand.GetDS: ImcDMControl;
begin
  if not Assigned(FDS) then
  begin
    SetAppID(EvoMgmtConsoleAppInfo.AppID);
    FDS := TmcDMControl.Create(DSHost, '');
  end;

  Result := FDS;
end;

{ TCacheableCommand }

function TCacheableCommand.CacheID: String;
begin
  Result := Command;
end;

function TCacheableCommand.GetData: IisInterfacedObject;
begin
  Result := GetDataFromCache;
  if not Assigned(Result) then
  begin
    Result := PrepareData;
    PutDataToCache(Result);
  end;
end;

function TCacheableCommand.GetDataFromCache: IisInterfacedObject;
var
  FileName: String;
  LifeTime: Integer;
  S: IisStream;
  CacheData: IisParamsCollection;
  Item: IisListOfValues;
begin
  Result := nil;

  Lifetime := StrToIntDef(AppSwitches.TryGetValue('cache', '0'), 60);

  if Lifetime = 0 then
    Exit;

  FileName := AppDir + cCacheFile;
  if not FileExists(FileName) then
    Exit;

  S := TisStream.CreateFromFile(FileName, False, False, False, False, 10);
  if not Assigned(S) then
    Exit;

  try
    CacheData := ObjectFactory.CreateInstanceFromStream(S) as IisParamsCollection;

    Item := CacheData.ParamsByName(CacheID);
    if not Assigned(Item) then
      Exit;

    if SecondsBetween(Item.Value['Timestamp'], Now) < Lifetime then
      Result := IInterface(Item.Value['Data']) as IisInterfacedObject;
  except
    S := nil;
    DeleteFile(FileName);
  end;
end;

procedure TCacheableCommand.PutDataToCache(const AData: IisInterfacedObject);
var
  FileName: String;
  S: IisStream;
  CacheData: IisParamsCollection;
  Item: IisListOfValues;
begin
  FileName := AppDir + cCacheFile;

  S := TisStream.CreateFromFile(FileName, False, False, False, True, 10);
  if not Assigned(S) then
    Exit;

  try
    if S.Size > 0 then
      CacheData := ObjectFactory.CreateInstanceFromStream(S) as IisParamsCollection
    else
      CacheData := TisParamsCollection.Create;

    Item := CacheData.ParamsByName(CacheID);
    if not Assigned(Item) then
      Item := CacheData.AddParams(CacheID);

    Item.AddValue('Timestamp', Now);
    Item.AddValue('Data', AData);

    S.Size := 0;
    ObjectFactory.SaveInstanceToStream(CacheData as IisInterfacedObject, S);
  except
  end;
end;

procedure TCacheableCommand.ShowContextHelp;
begin
  inherited;
  Console.WriteLn;
  Console.Write('Usage: ', clYellow);
  Console.Writeln('EvoMonitor ' + Command + ' [options]');
  Console.Writeln;
  Console.Writeln('Options:', clYellow);
  Console.Write('  -errlog <file>     ', clWhite); Console.Writeln('Log errors in a text file (EvoMonitor_Errors.txt if <File> ommited)');
  Console.Writeln;  
  Console.Write('  -cache <seconds>   ', clWhite); Console.Writeln('Cache received data. It activates data sharing for consecutive calls.');
  Console.Writeln('                     <seconds> defines cache lifetime (60 sec if ommitted)');
end;

{ TCmdASERR }

class function TCmdASERR.Description: String;
begin
  Result := 'Show ADR Server critical errors';
end;

function TCmdASERR.PrepareData: IisInterfacedObject;
var
  s: String;
  LogItems: IisParamsCollection;
  Res: IisStringList;
  Par: IisListOfValues;
  GroupRes: IisParamsCollection;
  Q: TEventQuery;
  i, n: Integer;
begin
  Q.StartTime := 0;
  Q.EndTime := IncHour(Now, -2);
  Q.Types := [etError, etFatalError];
  Q.EventClass := '';
  Q.MaxCount := 100;

  LogItems := GetDS.EvolutionControl.ASControl.GetFilteredLogEvents(Q);

  GroupRes := TisParamsCollection.Create;

  for i := 0 to LogItems.Count - 1 do
  begin
    s := LogItems[i].Value['Text'];
    Par := GroupRes.ParamsByName(s);
    if not Assigned(Par) then
    begin
      if MinutesBetween(LogItems[i].Value['TimeStamp'] , Now) <= 15 then
      begin
        Par := GroupRes.AddParams(s);
        Par.AddValue('Text', s);
        Par.AddValue('Count', 1);
      end
      else
        Par := nil;
    end;

    if Assigned(Par) then
      Par.Value['Count'] := Par.Value['Count'] + 1;
  end;

  Res := TisStringList.Create;
  for i := 0 to GroupRes.Count - 1 do
  begin
    n := GroupRes[i].Value['Count'];
    s := GroupRes[i].Value['Text'];
    if (Contains(s, ' rebuild TT failed') and (n > 0)) or
       (Contains(s, ' could not be restored. Trying again...') and (n > 5)) or
       (Contains(s, ' data synchronization failed') and (n > 5)) then
    begin
       Res.Add(s);
    end;
  end;

  Result := Res as IisInterfacedObject;
end;

procedure TCmdASERR.Run;
var
  Data: IisStringList;
  i: Integer;
begin
  Data := GetData as IisStringList;

  for i := 0 to Data.Count - 1 do
    Console.Writeln(Data[i]);
end;

procedure TCmdASERR.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('  -host <host>  ', clWhite); Console.Writeln('ADR Server host name (LOCALHOST if omitted)');
  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Show ADR server critical errors');
  Console.Writeln('      EvoMonitor ' + Command + ' -host adr1');
end;

{ TCmdRBSTAT }

class function TCmdRBSTAT.Description: String;
begin
  Result := 'Show Request Broker summary statistics';
end;

procedure TCmdRBSTAT.PostProcessData(const AData: IisListOfValues);
var
  rp: String;
  i: Integer;
  RPStatus: IisParamsCollection;
  LV: IisListOfValues;
  Exec, QueueSlots, QueueExecSlots, ScoreTotal, ScoreCurr, RPConn: Integer;
begin
  inherited;

  rp := AppSwitches.TryGetValue('value', '');
  GetNextStrValue(rp, #13);

  RPStatus := IInterface(AData.Value['RPStatus']) as IisParamsCollection;

  Exec := 0;
  QueueSlots := 0;
  QueueExecSlots := 0;
  ScoreTotal := 0;
  ScoreCurr := 0;
  RPConn := 0;
  for i := 0 to RPStatus.Count - 1 do
    if (rp = '') or (rp <> '') and AnsiSameText(RPStatus.ParamName(i), rp) then
    begin
      LV := RPStatus[i];
      Exec := Exec + LV.Value['exec'];
      QueueSlots := QueueSlots + LV.Value['queue_slots'];
      QueueExecSlots := QueueExecSlots + LV.Value['queue_exec_slots'];
      ScoreTotal := ScoreTotal + LV.Value['score_total'];
      ScoreCurr := ScoreCurr + LV.Value['score_cur'];
      RPConn := RPConn + LV.Value['rp_connected'];

      if rp <> '' then
        break;
    end;

  AData.AddValue('exec', Exec);
  AData.AddValue('queue_slots', QueueSlots);
  AData.AddValue('queue_exec_slots', QueueExecSlots);
  AData.AddValue('score_total', ScoreTotal);
  AData.AddValue('score_cur', ScoreCurr);
  AData.AddValue('rp_connected', RPConn);
end;

function TCmdRBSTAT.PrepareData: IisInterfacedObject;
var
  s: String;
  act, LastActivity: TDateTime;
  Status, RPStatus: IisParamsCollection;
  Res: IisListOfValues;
  i: Integer;
  Users, Connections, Exec, QueueSlots, QueueExecSlots, ScoreTotal, ScoreCurr: Integer;
  Item, LV, RPInfo: IisListOfValues;
begin
  Users := 0;
  LastActivity := 0;
  Connections := 0;

  Status := GetDS.EvolutionControl.RBControl.UsersPoolStatus;

  for i := 0 to Status.Count - 1 do
  begin
    s := Status[i].Value['AppType'];
    if AnsiSameText(Status[i].Value['SessionState'], 'Active') and ((s = 'Evo') or (s = 'RR') or (s = 'RW')) then
      Inc(Users);

    Inc(Connections, Integer(Status[i].Value['Connections']));

    act := Status[i].Value['LastActivity'];
    if LastActivity < act then
      LastActivity := act;
  end;

  Res := TisListOfValues.Create;
  Res.AddValue('users', Users);

  if LastActivity = 0 then
    s := ''
  else
    s := FormatDateTime('mm/dd/yyyy hh:nn:ss AM/PM', LastActivity);
  Res.AddValue('users_last_act_time', s);
  Res.AddValue('connections', Connections);

  RPStatus := TisParamsCollection.Create;
  Res.AddValue('RPStatus', RPStatus);

  Status := GetDS.EvolutionControl.RBControl.GetSystemStatus;
  Item := Status.ParamsByName('RPPool');
  for i := 0 to Item.Count - 1 do
  begin
    LV := IInterface(Item[i].Value) as IisListOfValues;
    RPInfo := RPStatus.AddParams(Item[i].Name);

    if LV.Value['Session'] <> '' then
      RPInfo.AddValue('rp_connected', 1)
    else
      RPInfo.AddValue('rp_connected', 0);

    Exec := Integer(LV.TryGetValue('NewRTRequests', 0)) +
            Integer(LV.TryGetValue('NewTaskRequests', 0)) +
            Integer(LV.TryGetValue('TaskRequests', 0));
    QueueSlots := Integer(LV.TryGetValue('MaxTaskRequests', 0));
    QueueExecSlots := Integer(LV.TryGetValue('NewTaskRequests', 0)) + Integer(LV.TryGetValue('TaskRequests', 0));;
    ScoreTotal := LV.TryGetValue('MaxScore', 0);
    ScoreCurr := LV.TryGetValue('CurrentScore', 0);

    RPInfo.AddValue('exec', Exec);
    RPInfo.AddValue('queue_slots', QueueSlots);
    RPInfo.AddValue('queue_exec_slots', QueueExecSlots);
    RPInfo.AddValue('score_total', ScoreTotal);
    RPInfo.AddValue('score_cur', ScoreCurr);
  end;
  Res.AddValue('rp_available', RPStatus.Count);

  Result := Res as IisInterfacedObject;
end;

procedure TCmdRBSTAT.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('  -host <host>       ', clWhite); Console.Writeln('Request Broker host name (LOCALHOST if omitted)');
  Console.Writeln;
  Console.Write('  -value connections                 ', clWhite); Console.Writeln('Connections count');
  Console.Write('  -value users                       ', clWhite); Console.Writeln('Active users count');
  Console.Write('  -value users_last_act_time         ', clWhite); Console.Writeln('Users'' last activity time');
  Console.Write('  -value rp_available                ', clWhite); Console.Writeln('Available Request Processors count');
  Console.Write('  -value rp_connected [rp_host]      ', clWhite); Console.Writeln('Connected Request Processors count');
  Console.Write('  -value exec [rp_host]              ', clWhite); Console.Writeln('Executing requests');
  Console.Write('  -value queue_slots [rp_host]       ', clWhite); Console.Writeln('Task queue execution capacity');
  Console.Write('  -value queue_exec_slots [rp_host]  ', clWhite); Console.Writeln('Task queue curently used slots');
  Console.Write('  -value score_total [rp_host]       ', clWhite); Console.Writeln('Maximum score');
  Console.Write('  -value score_cur [rp_host]         ', clWhite); Console.Writeln('Current score');
  Console.Writeln;
  Console.Writeln('If preffix [rp_host] is ommited data is summarized for all RPs');
  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Show Request Broker active user count');
  Console.Writeln('      EvoMonitor ' + Command + ' -host rb1 -value users');
  Console.Writeln;
  Console.Writeln('   Show used slots on Request Processor rp01');
  Console.Writeln('      EvoMonitor ' + Command + ' -host rb1 -value queue_slots rp01');

end;

{ TCmdRPSTAT }

class function TCmdRPSTAT.Description: String;
begin
  Result := 'Show Request Processor summary statistics';
end;

function TCmdRPSTAT.PrepareData: IisInterfacedObject;
var
  LoadStatus: IisListOfValues;
  Res: IisListOfValues;
begin
  Res := TisListOfValues.Create;

  LoadStatus := GetDS.EvolutionControl.RPControl.GetLoadStatus;
  Res.AddValue('cpu_total', LoadStatus.Value['CPUTotal']);
  Res.AddValue('cpu_service', LoadStatus.Value['ProcessCPUTotal']);
  Res.AddValue('mem_total', LoadStatus.Value['MemoryLoad']);
  Res.AddValue('mem_service', LoadStatus.Value['MemoryProcessLoad']);

  Result := Res as IisInterfacedObject;
end;

procedure TCmdRPSTAT.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('  -host <host>       ', clWhite); Console.Writeln('Request Processor host name (LOCALHOST if omitted)');
  Console.Writeln;
  Console.Write('  -value cpu_total            ', clWhite); Console.Writeln('CPU total load %');
  Console.Write('  -value cpu_service          ', clWhite); Console.Writeln('CPU service load %');
  Console.Write('  -value mem_total            ', clWhite); Console.Writeln('Memory total load %');
  Console.Write('  -value mem_service          ', clWhite); Console.Writeln('Memory service load %');

  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Show Request Processor''s service CPU load');
  Console.Writeln('      EvoMonitor ' + Command + ' -host rp01 -value cpu_service');
end;

{ TDSListOfValuesCommand }

procedure TDSListOfValuesCommand.PostProcessData(const AData: IisListOfValues);
begin
end;

procedure TDSListOfValuesCommand.Run;
var
  s, ValID: String;
  Data: IisListOfValues;
  v: Variant;
begin
  Data := GetData as IisListOfValues;

  PostProcessData(Data);

  s := AppSwitches.TryGetValue('value', '');
  ValID := GetNextStrValue(s, #13);
  v := Data.TryGetValue(ValID, Null);
  if v <> Null then
    Console.Writeln(VarToStr(v));
end;

{ TCmdRRSTAT }

class function TCmdRRSTAT.Description: String;
begin
  Result := 'Show Remote Relay summary statistics';
end;

function TCmdRRSTAT.PrepareData: IisInterfacedObject;
var
  Status: IisParamsCollection;
  i: Integer;
  LV, Res: IisListOfValues;
  Users: Integer;
  s: String;
begin
  Res := TisListOfValues.Create;

  Status := GetDS.EvolutionControl.RRControl.GetSessionsStatus;
  Users := 0;
  for i := 0 to Status.Count - 1 do
  begin
    LV := Status.Params[i];
    s := LV.TryGetValue('AppID', '');
    if AnsiSameText(LV.TryGetValue('State', ''), 'Active') and
       (AnsiSameText(s, 'Evo') or AnsiSameText(s, 'RW')) then
      Inc(Users);
  end;

  Res.AddValue('connections', Status.Count);
  Res.AddValue('users', Users);

  Result := Res as IisInterfacedObject;
end;

procedure TCmdRRSTAT.ShowContextHelp;
begin
  inherited;
  Console.Writeln;
  Console.Write('  -host <host>       ', clWhite); Console.Writeln('Remote Relay host name (LOCALHOST if omitted)');
  Console.Writeln;
  Console.Write('  -value connections      ', clWhite); Console.Writeln('Connections count');
  Console.Write('  -value users            ', clWhite); Console.Writeln('Active users count');

  Console.Writeln;
  Console.Writeln;
  Console.Writeln('Examples:', clYellow);
  Console.Writeln('   Show Remote Relay active users');
  Console.Writeln('      EvoMonitor ' + Command + ' -host rr01 -value users');
end;

initialization
  ObjectFactory.Register([TCmdHELP, TCmdLOGIN, TCmdASSTAT, TCmdASERR, TCmdASSTATDOMAIN,
                          TCmdRBSTAT, TCmdRPSTAT, TCmdRRSTAT]);

end.
