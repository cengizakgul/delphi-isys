unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, Processing, isThreadManager;

type
  TMain = class(TForm, IevProcessingCallback)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edSourceDB: TEdit;
    Label2: TLabel;
    edDestinationDB: TEdit;
    Label3: TLabel;
    edDBPassword: TEdit;
    btnRun: TButton;
    memLog: TMemo;
    Label4: TLabel;
    procedure btnRunClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FDBDataRecover: IevDBDataRecover;
    procedure DoLogMessage(const AText: String);
  protected
    procedure OnStart;
    procedure OnError(const AError: String);
    procedure OnFinish;
    procedure LogMessage(const AText: String);
  public
  end;

var
  Main: TMain;

implementation

{$R *.dfm}


procedure TMain.btnRunClick(Sender: TObject);
begin
  FDBDataRecover := TevDBDataRecover.Create;
  try
    FDBDataRecover.Execute(edSourceDB.Text, edDestinationDB.Text, edDBPassword.Text, Self);
  except
    FDBDataRecover := nil;
    raise;
  end;
end;

procedure TMain.LogMessage(const AText: String);

   procedure DoLogMessage(const Params: TTaskParamList);
   begin
     Main.DoLogMessage(Params[0]);
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoLogMessage, MakeTaskParams([AText]), False);
end;

procedure TMain.OnError(const AError: String);

   procedure DoOnError(const Params: TTaskParamList);
   begin
     Main.DoLogMessage('ERROR: ' + Params[0]);
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnError, MakeTaskParams([AError]), False);
end;

procedure TMain.OnFinish;

   procedure DoOnFinish(const Params: TTaskParamList);
   begin
     Main.DoLogMessage('Finished');

     Main.btnRun.Enabled := True;
     Main.edSourceDB.Enabled := True;
     Main.edDestinationDB.Enabled := True;
     Main.edDBPassword.Enabled := True;

     Main.FDBDataRecover := nil;     
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnFinish, MakeTaskParams([]), False);
end;

procedure TMain.OnStart;

   procedure DoOnStart(const Params: TTaskParamList);
   begin
     Main.btnRun.Enabled := False;
     Main.edSourceDB.Enabled := False;
     Main.edDestinationDB.Enabled := False;
     Main.edDBPassword.Enabled := False;

     Main.memLog.Clear;
     Main.DoLogMessage('Started');
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnStart, MakeTaskParams([]), False);
end;

procedure TMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not Assigned(FDBDataRecover);
end;

procedure TMain.DoLogMessage(const AText: String);
begin
  memLog.Lines.Add(FormatDateTime('hh:nn:ss', Now) + '     ' +
    StringReplace(StringReplace(AText, #13, ' ', [rfReplaceAll]), #13, ' ', [rfReplaceAll]));
end;

end.
