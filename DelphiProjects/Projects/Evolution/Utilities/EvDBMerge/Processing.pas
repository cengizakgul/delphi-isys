unit Processing;

interface

uses SysUtils,
     IBDatabase, IBQuery,
     isBaseClasses, isThreadManager, isDataSet, isBasicUtils;

type
  IevProcessingCallback = interface
  ['{FC3265DD-A338-4DD6-95DD-9E80ECF262D9}']
    procedure OnStart;
    procedure OnError(const AError: String);
    procedure OnFinish;
    procedure LogMessage(const AText: String);    
  end;


  IevDBDataRecover = interface
  ['{92016C28-D9F1-4FE6-90AD-96C5AE1789F6}']
    procedure Execute(const ASourceDB, ADestinationDB, EUserPassword: String; const ACallback: IevProcessingCallback);
  end;


  TevDBDataRecover = class(TisInterfacedObject, IevDBDataRecover)
  private
    FCallback: IevProcessingCallback;
    FSrcDB: TIBDatabase;
    FDestDB: TIBDatabase;
    FSrcTran: TIBTransaction;
    FDestTran: TIBTransaction;

    procedure DoInThread(const Params: TTaskParamList);
    function  GetMissingTransactions: IDataSet;
    procedure RecoverMissingData(const ATransactions: IDataSet);
    procedure SetGenIDs;
  protected
    procedure Execute(const ASourceDB, ADestinationDB, EUserPassword: String; const ACallback: IevProcessingCallback);
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

implementation

uses DB;


{ TevDBDataRecover }

procedure TevDBDataRecover.AfterConstruction;
begin
  inherited;
  FSrcDB := TIBDatabase.Create(nil);
  FSrcDB.LoginPrompt := False;
  FSrcTran := TIBTransaction.Create(nil);
  FSrcDB.DefaultTransaction := FSrcTran;

  FDestDB := TIBDatabase.Create(nil);
  FDestDB.LoginPrompt := False;
  FDestTran := TIBTransaction.Create(nil);
  FDestDB.DefaultTransaction := FDestTran;
end;

procedure TevDBDataRecover.BeforeDestruction;
begin
  FreeAndNil(FSrcDB);
  FreeAndNil(FSrcDB);
  FreeAndNil(FDestDB);  
  FreeAndNil(FDestDB);

  inherited;
end;

procedure TevDBDataRecover.DoInThread(const Params: TTaskParamList);
var
  DS: IDataSet;
begin
  try
    FCallback.OnStart;
    try
      try
        FSrcDB.Open;
        FDestDB.Open;

        DS := GetMissingTransactions;
        if Assigned(DS) then
        begin
          FCallback.LogMessage(Format('Found %d missing transactions', [DS.RecordCount]));
          RecoverMissingData(DS);
        end
        else
          FCallback.LogMessage('No missing transactions found in current database');

      finally
        FSrcDB.Close;
        FDestDB.Close;
      end;
    except
      on E: Exception do
        FCallback.OnError(E.Message);
    end;
  finally
    FCallback.OnFinish;
  end;
end;

procedure TevDBDataRecover.Execute(const ASourceDB, ADestinationDB,
  EUserPassword: String; const ACallback: IevProcessingCallback);
begin
  CheckCondition(ExtractFileName(StringReplace(ASourceDB, '/', '\', [rfReplaceAll])) =  ExtractFileName(StringReplace(ADestinationDB, '/', '\', [rfReplaceAll])), 'Database names do not match!');

  FCallback := ACallback;

  FSrcDB.Close;
  FDestDB.Close;

  FSrcDB.DatabaseName := ASourceDB;
  FSrcDB.Params.Clear;
  FSrcDB.Params.Add('USER_NAME=EUSER');
  FSrcDB.Params.Add('PASSWORD=' + EUserPassword);

  FDestDB.DatabaseName := ADestinationDB;
  FDestDB.Params.Clear;
  FDestDB.Params.Add('USER_NAME=EUSER');
  FDestDB.Params.Add('PASSWORD=' + EUserPassword);

  GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([]), 'Processing');
end;

function TevDBDataRecover.GetMissingTransactions: IDataSet;
var
  SrcQ, DestQ: TIBQuery;
begin
  FCallback.LogMessage('Analyzing TRANSACTION_INFO tables...');

  SrcQ := TIBQuery.Create(nil);
  DestQ := TIBQuery.Create(nil);
  try
    SrcQ.Database := FSrcDB;
    SrcQ.SQL.Text := 'SELECT transaction_nbr, commit_nbr FROM transaction_info ORDER BY 1';

    DestQ.Database := FDestDB;
    DestQ.SQL.Text := 'SELECT transaction_nbr, commit_nbr FROM transaction_info ORDER BY 1';

    SrcQ.Open;
    DestQ.Open;

    Result := TisDataSet.Create(SrcQ.Fields);

    while not SrcQ.Eof do
    begin
      if DestQ.Eof or (SrcQ.Fields[0].AsInteger < DestQ.Fields[0].AsInteger) then
        Result.AppendRecord([SrcQ.Fields[0].AsInteger, SrcQ.Fields[1].AsInteger])
      else
        DestQ.Next;
      SrcQ.Next;
    end;

  finally
    SrcQ.Free;
    DestQ.Free;
  end;

  if Result.RecordCount = 0 then
    Result := nil;
end;

procedure TevDBDataRecover.RecoverMissingData(const ATransactions: IDataSet);
var
  SrcChLog, SrcTblData, DestCheck, DestQ: TIBQuery;
  flds, params: String;
  i: Integer;
  v: Variant;
begin
  FCallback.LogMessage('Recovering missing data...');

  SrcChLog := TIBQuery.Create(nil);
  SrcTblData := TIBQuery.Create(nil);
  DestQ := TIBQuery.Create(nil);
  DestCheck := TIBQuery.Create(nil);
  try
    SrcChLog.Database := FSrcDB;
    SrcChLog.SQL.Text := 'SELECT change_log_nbr, table_name, record_nbr, effective_date, creation_date FROM change_log WHERE transaction_nbr = :transaction_nbr ORDER BY 1';
    SrcChLog.Prepare;

    SrcTblData.Database := FSrcDB;

    DestQ.Database := FDestDB;
    DestCheck.Database := FDestDB;

    ATransactions.IndexFieldNames := 'COMMIT_NBR';
    ATransactions.First;

    while not ATransactions.Eof do
    begin
      FCallback.LogMessage(Format('Recovering transaction #%d...', [ATransactions.Fields[0].AsInteger]));

      if not FDestTran.Active then
        FDestTran.StartTransaction;
      try
        SrcChLog.Params[0].AsInteger := ATransactions.Fields[0].AsInteger;
        SrcChLog.Open;

        DestQ.SQL.Text := 'SET GENERATOR TEMP_TRANSACTION_GEN TO ' + IntToStr(ATransactions.Fields[0].AsInteger - 1);
        DestQ.ExecSQL;

        DestQ.SQL.Text := 'SET GENERATOR CHANGE_LOG_TRANSACTION_GEN TO ' + IntToStr(ATransactions.Fields[1].AsInteger - 1);
        DestQ.ExecSQL;

        DestQ.SQL.Text := 'EXECUTE PROCEDURE AFTER_START_TRANSACTION';
        DestQ.ExecSQL;

        while not SrcChLog.Eof do
        begin
          FCallback.LogMessage(Format('Restoring %s NBR=%s ED=%s CD=%s', [SrcChLog.Fields[1].AsString, SrcChLog.Fields[2].AsString, SrcChLog.Fields[3].AsString, SrcChLog.Fields[4].AsString]));

          SrcTblData.SQL.Text := 'SELECT * FROM ' + SrcChLog.Fields[1].AsString + ' WHERE ' + SrcChLog.Fields[1].AsString + '_nbr = ' +  SrcChLog.Fields[2].AsString + ' AND effective_date = :effective_date AND creation_date = :creation_date';
          SrcTblData.ParamByName('effective_date').AsDateTime := SrcChLog.Fields[3].AsDateTime;
          SrcTblData.ParamByName('creation_date').AsDateTime := SrcChLog.Fields[4].AsDateTime;
          SrcTblData.Open;

          if SrcTblData.RecordCount = 1 then
          begin
            DestQ.SQL.Text := 'SET GENERATOR CHANGE_LOG_GEN TO ' + IntToStr(SrcChLog.Fields[0].AsInteger - 1);
            DestQ.ExecSQL;

            flds := '';
            params := '';
            for i := 0 to SrcTblData.Fields.Count - 1 do
            begin
              AddStrValue(flds, SrcTblData.Fields[i].FieldName, ',');
              AddStrValue(params, ':' + SrcTblData.Fields[i].FieldName, ',');
            end;

            DestQ.SQL.Text := 'INSERT INTO ' + SrcChLog.Fields[1].AsString + ' (' + flds + ') VALUES (' + params + ')';

            for i := 0 to DestQ.ParamCount - 1 do
            begin
              v := SrcTblData.Fields[i].Value;

              if AnsiSameText(SrcTblData.Fields[i].FieldName, 'ACTIVE_RECORD') and (v = 'C') then
              begin
                DestCheck.SQL.Text := 'SELECT creation_date FROM ' + SrcChLog.Fields[1].AsString + ' WHERE ACTIVE_RECORD = ''C'' AND ' + SrcChLog.Fields[1].AsString + '_nbr = ' + SrcChLog.Fields[2].AsString;
                DestCheck.Open;
                if (DestCheck.RecordCount > 0) and (DestCheck.Fields[0].AsDateTime > SrcChLog.Fields[4].AsDateTime) then
                  v := 'P';
                DestCheck.Close;
              end;

              DestQ.Params[i].Value := v;
            end;

            DestQ.ExecSQL;
          end;

          SrcTblData.Close;

          SrcChLog.Next;
        end;
        SrcChLog.Close;

        DestQ.SQL.Text := 'EXECUTE PROCEDURE BEFORE_COMMIT_TRANSACTION';
        DestQ.ExecSQL;

        FDestTran.Commit;
      except
        on E: Exception do
        begin
          FDestTran.Rollback;
          FCallback.OnError(E.Message);
        end;
      end;

      ATransactions.Next;
    end;

  finally
    SrcChLog.Free;
    SrcTblData.Free;
    DestQ.Free;
    DestCheck.Free;

    SetGenIDs;    
  end;
end;

procedure TevDBDataRecover.SetGenIDs;
var
  DestQ: TIBQuery;
  v: Integer;
begin
  DestQ := TIBQuery.Create(nil);
  try
    DestQ.Database := FDestDB;

    DestQ.SQL.Text := 'SELECT MAX(transaction_nbr) FROM transaction_info';
    DestQ.Open;
    v := DestQ.Fields[0].AsInteger;
    DestQ.Close;

    DestQ.SQL.Text := 'SET GENERATOR TEMP_TRANSACTION_GEN TO ' + IntToStr(v);
    DestQ.ExecSQL;

    DestQ.SQL.Text := 'SELECT MAX(commit_nbr) FROM transaction_info';
    DestQ.Open;
    v := DestQ.Fields[0].AsInteger;
    DestQ.Close;

    DestQ.SQL.Text := 'SET GENERATOR CHANGE_LOG_TRANSACTION_GEN TO ' + IntToStr(v);
    DestQ.ExecSQL;

    DestQ.SQL.Text := 'SELECT MAX(change_log_nbr) FROM change_log';
    DestQ.Open;
    v := DestQ.Fields[0].AsInteger;
    DestQ.Close;

    DestQ.SQL.Text := 'SET GENERATOR CHANGE_LOG_GEN TO ' + IntToStr(v);
    DestQ.ExecSQL;
  finally
    DestQ.Free;
  end;
end;

end.
