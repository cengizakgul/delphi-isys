// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, IBCustomDataSet, IBQuery, IBDatabase,
  IBStoredProc;

type
  TDM = class(TDataModule)
    qGetUserNbr: TIBQuery;
    FIBTransactionSbureau: TIBTransaction;
    FIBDbSbureau: TIBDatabase;
    qAddNewRight: TIBQuery;
    qGetNewRightNbr: TIBStoredProc;
    qGetAllGroups: TIBQuery;
    spAfterStartTransaction: TIBStoredProc;
    spBeforeCommitTransaction: TIBStoredProc;
    qGetRightsUser: TIBQuery;
    spRemoveRight: TIBStoredProc;
    qGetUserNotINGroups: TIBQuery;
    qGetRightsGroups: TIBQuery;
    qRollBackRights: TIBQuery;
    qRollBackDelete: TIBQuery;
    UpdVersion_info: TIBQuery;
    qGetVersion_Info: TIBQuery;
    UpdBankAccountFunction: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{$R *.DFM}

end.
