object DM: TDM
  OldCreateOrder = False
  Left = 33
  Top = 149
  Height = 532
  Width = 958
  object qGetUserNbr: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      
        'SELECT * FROM SB_USER WHERE USER_ID = '#39'ADMIN'#39' and ACTIVE_RECORD ' +
        '= '#39'C'#39
      ' ')
    Left = 152
    Top = 232
  end
  object FIBTransactionSbureau: TIBTransaction
    Active = True
    DefaultDatabase = FIBDbSbureau
    DefaultAction = TARollback
    Left = 136
    Top = 16
  end
  object FIBDbSbureau: TIBDatabase
    Connected = True
    DatabaseName = 'thor:/db/killington/S_BUREAU.gdb'
    Params.Strings = (
      'isc_dpb_user_name=EUSER'
      'isc_dpb_password=pps97')
    LoginPrompt = False
    DefaultTransaction = FIBTransactionSbureau
    AllowStreamedConnected = False
    Left = 24
    Top = 16
  end
  object qAddNewRight: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'insert into sb_sec_rights ('
      'SB_SEC_RIGHTS_NBR,'
      'SB_SEC_GROUPS_NBR,'
      'SB_USER_NBR,'
      'TAG,'
      'CONTEXT,'
      'active_record,'
      'CHANGED_BY,'
      'CREATION_DATE, effective_date'
      ')'
      'values ('
      ':SB_SEC_RIGHTS_NBR,'
      ':SB_SEC_GROUPS_NBR,'
      ':SB_USER_NBR,'
      ':TAG,'
      ':CONTEXT,'
      ':active_record, '
      ':CHANGED_BY,'
      ':CREATION_DATE, :effective_date'
      ') ')
    Left = 328
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SB_SEC_RIGHTS_NBR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SB_SEC_GROUPS_NBR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SB_USER_NBR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'TAG'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CONTEXT'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'active_record'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CHANGED_BY'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CREATION_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'effective_date'
        ParamType = ptUnknown
      end>
  end
  object qGetNewRightNbr: TIBStoredProc
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    StoredProcName = 'SB_SEC_RIGHTS_NBR'
    Left = 224
    Top = 120
  end
  object qGetAllGroups: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      
        'SELECT SB_SEC_GROUPS_NBR FROM SB_SEC_GROUPS  WHERE ACTIVE_RECORD' +
        ' = '#39'C'#39'   AND NAME  <> '#39'System Default Group'#39
      ' ')
    Left = 312
    Top = 224
  end
  object spAfterStartTransaction: TIBStoredProc
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    StoredProcName = 'AFTER_START_TRANSACTION'
    Left = 408
    Top = 24
  end
  object spBeforeCommitTransaction: TIBStoredProc
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    StoredProcName = 'BEFORE_COMMIT_TRANSACTION'
    Left = 264
    Top = 24
  end
  object qGetRightsUser: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'SELECT SB_SEC_RIGHTS_NBR FROM SB_SEC_RIGHTS'
      'WHERE SB_USER_NBR = :SB_USER_NBR AND ACTIVE_RECORD = '#39'C'#39'  AND '
      'TAG IN '
      '(  '#39'F;FIELDS_SSN'#39', '#39'F;FIELDS_CL_BANK_'#39', '#39'F;FIELDS_CK_BANK_'#39','
      #39'F;FIELDS_EE_BANK_'#39',  '#39'F;FIELDS_MC_BANK_'#39', '#39'F;FIELDS_SB_BANK_'#39')'
      ' ')
    UniDirectional = True
    Left = 456
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SB_USER_NBR'
        ParamType = ptUnknown
      end>
  end
  object spRemoveRight: TIBStoredProc
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    StoredProcName = 'SB_SEC_RIGHTS_DEL'
    Left = 680
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'USER_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DELETE_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CHANGE_DATE'
        ParamType = ptInput
      end>
  end
  object qGetUserNotINGroups: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'SELECT SB_USER_NBR FROM SB_USER  b'
      'WHERE'
      ' b.SB_USER_NBR NOT IN ( SELECT'
      '  DISTINCT a.SB_USER_NBR'
      'FROM'
      '  SB_SEC_GROUP_MEMBERS a'
      '  WHERE a.ACTIVE_RECORD = '#39'C'#39' and'
      
        '  SB_SEC_GROUPS_NBR <> ( SELECT  c.SB_SEC_GROUPS_NBR FROM SB_SEC' +
        '_GROUPS c WHERE'
      '  c.NAME = '#39'System Default Group'#39' and c.ACTIVE_RECORD = '#39'C'#39')'
      '  )'
      '  AND b.ACTIVE_RECORD = '#39'C'#39
      'and'
      ' USER_ID <> '#39'ADMIN'#39)
    Left = 416
    Top = 224
  end
  object qGetRightsGroups: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'SELECT SB_SEC_RIGHTS_NBR FROM SB_SEC_RIGHTS'
      
        'WHERE SB_SEC_GROUPS_NBR = :SB_SEC_GROUPS_NBR AND ACTIVE_RECORD =' +
        ' '#39'C'#39' and  '
      'TAG IN '
      '(  '#39'F;FIELDS_SSN'#39', '#39'F;FIELDS_CL_BANK_'#39', '#39'F;FIELDS_CK_BANK_'#39','
      #39'F;FIELDS_EE_BANK_'#39',  '#39'F;FIELDS_MC_BANK_'#39', '#39'F;FIELDS_SB_BANK_'#39')'
      ' ')
    UniDirectional = True
    Left = 552
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SB_SEC_GROUPS_NBR'
        ParamType = ptUnknown
      end>
  end
  object qRollBackRights: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'SELECT a.SB_SEC_RIGHTS_NBR, a.SB_USER_NBR FROM SB_SEC_RIGHTS a'
      
        'WHERE a.SB_USER_NBR not in ( Select SB_USER_NBR FROM SB_USER WHE' +
        'RE USER_ID = '#39'ADMIN'#39' and ACTIVE_RECORD = '#39'C'#39')'
      'and a.TAG IN'
      ' (  '#39'F;FIELDS_SSN'#39', '#39'F;FIELDS_CL_BANK_'#39', '#39'F;FIELDS_CK_BANK_'#39','
      
        '   '#39'F;FIELDS_EE_BANK_'#39',  '#39'F;FIELDS_MC_BANK_'#39', '#39'F;FIELDS_SB_BANK_' +
        #39')'
      ' ')
    UniDirectional = True
    Left = 696
    Top = 208
  end
  object qRollBackDelete: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'DELETE FROM SB_SEC_RIGHTS'
      'WHERE SB_SEC_RIGHTS_NBR = :SB_SEC_RIGHTS_NBR'
      ' ')
    UniDirectional = True
    Left = 696
    Top = 272
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SB_SEC_RIGHTS_NBR'
        ParamType = ptInput
      end>
  end
  object UpdVersion_info: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'UPDATE VERSION_INFO'
      'SET patch_version = :patch_version'
      'WHERE major_version = 9')
    Left = 304
    Top = 336
    ParamData = <
      item
        DataType = ftInteger
        Name = 'patch_version'
        ParamType = ptInput
        Value = 7
      end>
  end
  object qGetVersion_Info: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'select '
      '  PATCH_VERSION'
      'from'
      '  VERSION_INFO'
      'where  '
      '  MAJOR_VERSION= 9 '
      ' ')
    Left = 304
    Top = 392
  end
  object UpdBankAccountFunction: TIBQuery
    Database = FIBDbSbureau
    Transaction = FIBTransactionSbureau
    SQL.Strings = (
      'Update SB_SEC_RIGHTS a set  '
      ' a.TAG = '#39'F;USER_CAN_EDIT_CO_BANK_ACCOUNTS'#39' '
      '  where a.TAG = '#39'F;'#39' and a.ACTIVE_RECORD = '#39'C'#39' ')
    Left = 416
    Top = 336
  end
end
