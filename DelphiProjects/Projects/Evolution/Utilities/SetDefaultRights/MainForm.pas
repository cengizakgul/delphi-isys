// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, 
  StdCtrls, IBQuery, IBDatabase, Buttons,  CheckLst, EvTypes, isBasicUtils,
  EvUtils, db, StrUtils, ISBasicClasses, EvGlobalSettingsMod, EvCommonInterfaces,
  EvMainboard, isVCLBugFix,  EvUIUtils, EvUIComponents, isUIEdit,
  LMDCustomButton, LMDButton, isUILMDButton, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

const
  WM_START = WM_USER+ 33;

type
  TForm1 = class(TForm)
    btnOk: TevButton;
    Label1: TevLabel;
    edIniFile: TevEdit;
    dIniFileDialog: TOpenDialog;
    btnCancel: TevButton;
    btnOpenIniFile: TevSpeedButton;
    lbSections: TevCheckListBox;
    evLabel1: TevLabel;
    btnSelectAll: TevButton;
    btnUnSelectAll: TevButton;
    cbRollback: TCheckBox;
    btnRollBack: TevButton;
    Memo1: TMemo;
    evLabel2: TevLabel;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edIniFileChange(Sender: TObject);
    procedure btnOpenIniFileClick(Sender: TObject);
    procedure btnSelectAllClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnRollBackClick(Sender: TObject);
    Function  ProcessRights(ProcessType : char): Boolean;
    Function  CheckDomain: Boolean;
  private
    FCommandLineMode: Boolean;
    FDbPassword: string;
    FGlobalSettings: IevSettings;
    procedure StartMessage(var Message: TMessage); message WM_START;

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Registry, inifiles, evConsts, Datamodule;

{$R *.DFM}


Function TForm1.ProcessRights(ProcessType : char): Boolean;
const
 NewFunction : array[1..6] of string =
  (
   'F;FIELDS_SSN',
   'F;FIELDS_SB_BANK_',
   'F;FIELDS_CL_BANK_',
   'F;FIELDS_EE_BANK_',
   'F;FIELDS_CK_BANK_',
   'F;FIELDS_MC_BANK_'
  );
 NewFunctionCount  = 6;
 NewPATCH_VERSION  = 7;



  procedure AddRights;
  var
    i :  Integer;
  begin
    with DM do
    begin

  //   Group Update Process...

        qGetAllGroups.Close;
        qGetAllGroups.Open;
        try
         while not qGetAllGroups.Eof do
         begin

          qGetRightsGroups.Close;
          qGetRightsGroups.ParamByName('SB_SEC_GROUPS_NBR').AsInteger := qGetAllGroups.FieldByName('SB_SEC_GROUPS_NBR').AsInteger;
          qGetRightsGroups.Open;
          while not qGetRightsGroups.Eof do
          begin
           spRemoveRight.ParamByName('NUMBER').AsInteger := qGetRightsGroups.Fields.FieldByName('SB_SEC_RIGHTS_NBR').AsInteger;
           spRemoveRight.ParamByName('USER_ID').AsInteger := 0;
           spRemoveRight.ParamByName('DELETE_DATE').AsDateTime := Now;
           spRemoveRight.ParamByName('CHANGE_DATE').AsDateTime := Now;
           spRemoveRight.ExecProc;
           qGetRightsGroups.Next;
          end;
          qGetRightsGroups.Close;

          for i := 1 to NewFunctionCount do
          begin
           qAddNewRight.Prepare;
           qGetNewRightNbr.ExecProc;
           qAddNewRight.ParamByName('SB_SEC_RIGHTS_NBR').AsInteger := qGetNewRightNbr.ParamByName('NUMBER').AsInteger;
           qAddNewRight.ParamByName('SB_SEC_GROUPS_NBR').AsInteger := qGetAllGroups.FieldByName('SB_SEC_GROUPS_NBR').AsInteger;
           qAddNewRight.ParamByName('SB_USER_NBR').Clear;
           qAddNewRight.ParamByName('TAG').AsString := NewFunction[i];
           qAddNewRight.ParamByName('CONTEXT').AsString := 'Y';
           qAddNewRight.ParamByName('CHANGED_BY').AsInteger := 0;
           qAddNewRight.ParamByName('CREATION_DATE').AsDateTime := Now;
           qAddNewRight.ParamByName('ACTIVE_RECORD').AsString := 'C';
           qAddNewRight.ParamByName('EFFECTIVE_DATE').AsDateTime := Now;
           qAddNewRight.ExecSQL;
           qAddNewRight.Unprepare;
          end;

           qGetAllGroups.Next;
         end;

        finally
         qGetAllGroups.Close;
        end;

//   User Update Process...

       qGetUserNotINGroups.Close;
       qGetUserNotINGroups.Open;
       try
        while not qGetUserNotINGroups.Eof do
        begin

          qGetRightsUser.Close;
          qGetRightsUser.ParamByName('SB_USER_NBR').AsInteger := qGetUserNotINGroups.FieldByName('SB_USER_NBR').AsInteger;
          qGetRightsUser.Open;
          while not qGetRightsUser.Eof do
          begin
            spRemoveRight.ParamByName('NUMBER').AsInteger := qGetRightsUser.Fields.FieldByName('SB_SEC_RIGHTS_NBR').AsInteger;
            spRemoveRight.ParamByName('USER_ID').AsInteger := 0;
            spRemoveRight.ParamByName('DELETE_DATE').AsDateTime := Now;
            spRemoveRight.ParamByName('CHANGE_DATE').AsDateTime := Now;
            spRemoveRight.ExecProc;
            qGetRightsUser.Next;
          end;
          qGetRightsUser.Close;

          for i := 1 to NewFunctionCount do
          begin
           qAddNewRight.Prepare;
           qGetNewRightNbr.ExecProc;
           qAddNewRight.ParamByName('SB_SEC_RIGHTS_NBR').AsInteger := qGetNewRightNbr.ParamByName('NUMBER').AsInteger;
           qAddNewRight.ParamByName('SB_SEC_GROUPS_NBR').Clear;
           qAddNewRight.ParamByName('SB_USER_NBR').AsInteger := qGetUserNotINGroups.FieldByName('SB_USER_NBR').AsInteger;
           qAddNewRight.ParamByName('TAG').AsString := NewFunction[i];
           qAddNewRight.ParamByName('CONTEXT').AsString := 'Y';
           qAddNewRight.ParamByName('CHANGED_BY').AsInteger := 0;
           qAddNewRight.ParamByName('CREATION_DATE').AsDateTime := Now;
           qAddNewRight.ParamByName('ACTIVE_RECORD').AsString := 'C';
           qAddNewRight.ParamByName('EFFECTIVE_DATE').AsDateTime := Now;
           qAddNewRight.ExecSQL;
           qAddNewRight.Unprepare;
          end;

          qGetUserNotINGroups.Next;
         end;
        Finally
         qGetUserNotINGroups.Close;
        end;

        UpdVersion_info.Prepare;
        UpdVersion_info.ParamByName('patch_version').AsInteger := NewPATCH_VERSION;
        UpdVersion_info.ExecSQL;
        UpdVersion_info.Unprepare;

        UpdBankAccountFunction.Prepare;
        UpdBankAccountFunction.ExecSQL;
        UpdBankAccountFunction.Unprepare;

    end;

  end;

  procedure RollBackRights;
  begin
    with DM do
    begin
  //   RollBack Process...
      qRollBackRights.Close;
      qRollBackRights.Open;
      while not qRollBackRights.Eof do
      begin

        qRollBackDelete.ParamByName('SB_SEC_RIGHTS_NBR').AsInteger := qRollBackRights.Fields.FieldByName('SB_SEC_RIGHTS_NBR').AsInteger;
        qRollBackDelete.ExecSQL;

        qRollBackRights.Next;
      end;

      UpdVersion_info.Prepare;
      UpdVersion_info.ParamByName('patch_version').AsInteger := NewPATCH_VERSION-1;
      UpdVersion_info.ExecSQL;
      UpdVersion_info.Unprepare;
    end;

  end;

var
  i: Integer;
  s: string;
  DomainInfo: IevDomainInfo;
Begin
  Result := False;
  s := '';
  if FCommandLineMode then
    s := FDbPassword
  else
    if not EvDialog('Please enter EUSER password for S_BUREAU.gdb', 'Password:', s, True) then
      AbortEx;

  with DM do
    for i := 0 to lbSections.Items.Count-1 do
      if lbSections.Checked[i] then
      begin
        DomainInfo := FGlobalSettings.DomainInfoList[i];

        CheckCondition(s = mb_GlobalSettings.DBUserPassword, 'Database password is incorrect');

        FIBDbSbureau.DatabaseName := DomainInfo.DBLocationList.GetDBPath(DB_S_Bureau);
        DM.FIBDbSbureau.Params.Values['isc_dpb_user_name'] := FB_User;
        DM.FIBDbSbureau.Params.Values['isc_dpb_password'] := s;

        Screen.Cursor := crSQLWait;
        try
          FIBDbSbureau.Connected := True;
          FIBTransactionSbureau.Active := True;
          try
           qGetVersion_Info.Open;
           if (qGetVersion_Info.FieldByName('PATCH_VERSION').AsInteger >= NewPATCH_VERSION) and
              (ProcessType = 'U')  then
             EvMessage('This utility has run before. It cannot be run a second time.', mtInformation, [mbYes])
           else
            begin
            qGetUserNbr.Open;
            if qGetUserNbr.RecordCount = 0 then
              EvMessage('There is no ADMIN user...', mtInformation, [mbYes])
            else
             begin
              spAfterStartTransaction.ExecProc;
              case ProcessType of
              'U' : AddRights;
              'R' : RollBackRights;
              end;
              spBeforeCommitTransaction.ExecProc;
              FIBTransactionSbureau.Commit;
              Result := True;
             end;
            end;

          finally
            if FIBTransactionSbureau.InTransaction then
              FIBTransactionSbureau.Rollback;

            FIBDbSbureau.Connected := False;
          end;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
end;

Function TForm1.CheckDomain: Boolean;
var
 i : integer;
begin
 result := false;
 for i := 0 to lbSections.Items.Count-1 do
    if lbSections.Checked[i] then Result := True;

end;

procedure TForm1.btnOkClick(Sender: TObject);
begin

  if CheckDomain then
  begin
   if not FCommandLineMode
   and (EvMessage('This will create new Rights for all selected Evolution domains.'#13+
    'Are you sure you want to proceed?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
    AbortEx;

   if ProcessRights('U') and (not FCommandLineMode) then
     EvMessage('Rights have been updated', mtInformation, [mbOk]);
   Application.Terminate;
  end
  else
   EvMessage('Please Select Evolution Domain.', mtInformation, [mbOk]);

end;

procedure TForm1.btnRollBackClick(Sender: TObject);
begin
  if CheckDomain then
  begin
   if not cbRollback.Checked then exit;
   if not FCommandLineMode
    and (EvMessage('This will RollBack all changes to Killington SB for all selected Evolution domains.'#13+
     'Are you sure you want to proceed?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
     AbortEx;

   ProcessRights('R');
   if not FCommandLineMode then
      EvMessage('Rolled back all new rights.', mtInformation, [mbOk]);

  end
  else
   EvMessage('Please Select Evolution Domain.', mtInformation, [mbOk]);

end;


procedure TForm1.btnCancelClick(Sender: TObject);
begin
  Application.Terminate;
end;


procedure TForm1.edIniFileChange(Sender: TObject);
var
  i: Integer;
begin
  if FileExists(edIniFile.Text) then
  begin
    lbSections.Clear;

    FGlobalSettings :=     Mainboard.ModuleRegister.CreateModuleInstance(IevSettings) as IevSettings;

    for i := 0 to FGlobalSettings.DomainInfoList.Count - 1 do
      lbSections.AddItem(FGlobalSettings.DomainInfoList[i].DomainDescription, Pointer(i));

    lbSections.Enabled := lbSections.Items.Count > 0;

    if lbSections.Enabled then
    begin
      btnSelectAll.Click;
      btnOk.SetFocus;
    end;
  end

  else
    lbSections.Enabled := False;
end;

procedure TForm1.btnOpenIniFileClick(Sender: TObject);
begin
  with dIniFileDialog do
  begin
    dIniFileDialog.InitialDir := ExtractFilePath(edIniFile.Text);
    if Execute then
      edIniFile.Text := FileName;
  end;
end;

procedure TForm1.btnSelectAllClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbSections.Items.Count-1 do
    lbSections.Checked[i] := Sender = btnSelectAll;
end;

procedure TForm1.FormShow(Sender: TObject);
var
  i: Integer;
begin

  Caption := Application.Title;
  FCommandLineMode := False;
  i := 1;
  while i <= ParamCount do
  begin
    if (ParamStr(i) = '-connect')
    and (i+1 <= ParamCount) then
    begin
      edIniFile.Text := ParamStr(i+1);
      FCommandLineMode := True;
      PostMessage(Handle, WM_START, 0, 0);
      Inc(i);
    end;
    Inc(i);
  end;

  if(Now > EncodeDate( 2009, 1, 1 ))then
  begin
    EvMessage('This program has expired.', mtInformation, [mbOk]);
    Application.Terminate;
  end;

  if not FCommandLineMode then
    edIniFile.Text := 'C:\Program Files\Evolution\DeploymentManager\Applications\Evolution\' + sConfigFileName;
end;

procedure TForm1.StartMessage(var Message: TMessage);
begin
  btnOk.Click;
end;

end.
