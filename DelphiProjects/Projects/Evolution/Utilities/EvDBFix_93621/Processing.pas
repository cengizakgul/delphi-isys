unit Processing;

interface

uses SysUtils, Variants,
     IBDatabase, IBQuery,
     isBaseClasses, isThreadManager, isDataSet, isBasicUtils;

type
  IevProcessingCallback = interface
  ['{FC3265DD-A338-4DD6-95DD-9E80ECF262D9}']
    procedure OnStart;
    procedure OnError(const AError: String);
    procedure OnFinish;
    procedure LogMessage(const AText: String);
  end;

  IevDBDataRecover = interface
  ['{92016C28-D9F1-4FE6-90AD-96C5AE1789F6}']
    procedure Execute(const ASourceDB, EUserPassword: String; const ACallback: IevProcessingCallback);
  end;

  TevDBDataRecover = class(TisInterfacedObject, IevDBDataRecover)
  private
    FCallback: IevProcessingCallback;
    FSrcDB: TIBDatabase;
    FSrcTran: TIBTransaction;

    procedure DoInThread(const Params: TTaskParamList);
    procedure FixIt;
  protected
    procedure Execute(const ASourceDB, EUserPassword: String; const ACallback: IevProcessingCallback);

  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

implementation

uses DB;


{ TevDBDataRecover }

procedure TevDBDataRecover.AfterConstruction;
begin
  inherited;
  FSrcDB := TIBDatabase.Create(nil);
  FSrcDB.LoginPrompt := False;
  FSrcTran := TIBTransaction.Create(nil);
  FSrcDB.DefaultTransaction := FSrcTran;
end;

procedure TevDBDataRecover.BeforeDestruction;
begin
  FreeAndNil(FSrcDB);
  inherited;
end;

procedure TevDBDataRecover.DoInThread(const Params: TTaskParamList);
begin
//  try
//    FCallback.OnStart;
    try
      FixIt;
    except
      on E: Exception do
        FCallback.OnError(E.Message);
    end;
//  finally
//    FCallback.OnFinish;
//  end;
end;

procedure TevDBDataRecover.Execute(const ASourceDB,
  EUserPassword: String; const ACallback: IevProcessingCallback);
begin
  FCallback := ACallback;

  FSrcDB.Close;

  FSrcDB.Params.Clear;
  FSrcDB.DatabaseName := ASourceDB;
  FSrcDB.Params.Add('USER_NAME=EUSER');
  FSrcDB.Params.Add('PASSWORD=' + EUserPassword);


  GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([]), 'Processing');
end;

procedure TevDBDataRecover.FixIt;
var
  SourceQ, DestQEE : TIBQuery;
begin
   FSrcDB.Open;
   try
      SourceQ := TIBQuery.Create(nil);
      DestQEE := TIBQuery.Create(nil);
      try
        SourceQ.Database := FSrcDB;
        DestQEE.Database := FSrcDB;
        if not FSrcTran.Active then
           FSrcTran.StartTransaction;
        try
          SourceQ.Close;
          SourceQ.SQL.Text :=
              ' select s.PR_CHECK_STATES_NBR,' +
              ' s.STATE_TAX, s.EE_SDI_TAX, s.ER_SDI_TAX,' +
              ' y.PR_CHECK_STATES_NBR CORRECT_CHECK_STATES_NBR' +
              ' from PR_CHECK c' +
              ' join PR r on r.PR_NBR=c.PR_NBR' +
              ' join EE e on e.EE_NBR=c.EE_NBR' +
              ' join PR_CHECK_STATES s on s.PR_CHECK_NBR=c.PR_CHECK_NBR' +
              ' join EE_STATES t on t.EE_STATES_NBR=s.EE_STATES_NBR' +
              ' join EE_STATES k on k.EE_NBR=c.EE_NBR and k.CO_STATES_NBR=t.CO_STATES_NBR' +
              ' join PR_CHECK_STATES y on y.PR_CHECK_NBR=c.PR_CHECK_NBR and y.EE_STATES_NBR=k.EE_STATES_NBR' +
              ' where c.ACTIVE_RECORD=''C''' +
              ' and e.ACTIVE_RECORD=''C''' +
              ' and s.ACTIVE_RECORD=''C''' +
              ' and t.ACTIVE_RECORD=''C''' +
              ' and r.ACTIVE_RECORD=''C''' +
              ' and k.ACTIVE_RECORD=''C''' +
              ' and y.ACTIVE_RECORD=''C''' +
              ' and t.EE_NBR <> c.EE_NBR' ;
          SourceQ.Open;

          SourceQ.First;
          while not SourceQ.Eof do
          begin
            DestQEE.SQL.Text := 'UPDATE PR_CHECK_STATES '+
              'SET STATE_TAX = STATE_TAX + ' + SourceQ.FieldByName('STATE_TAX').AsString;

            if Abs(SourceQ.FieldByName('EE_SDI_TAX').AsFloat) > 0.005 then
              DestQEE.SQL.Text := DestQEE.SQL.Text +
                  ',EE_SDI_TAX = EE_SDI_TAX + ' + SourceQ.FieldByName('EE_SDI_TAX').AsString;

            if Abs(SourceQ.FieldByName('ER_SDI_TAX').AsFloat) > 0.005 then
              DestQEE.SQL.Text := DestQEE.SQL.Text +
                  ',ER_SDI_TAX = ER_SDI_TAX + ' + SourceQ.FieldByName('ER_SDI_TAX').AsString;

            DestQEE.SQL.Text := DestQEE.SQL.Text + ' WHERE PR_CHECK_STATES_NBR = ' +
              SourceQ.FieldByName('CORRECT_CHECK_STATES_NBR').AsString + ' AND ACTIVE_RECORD=''C''';

            DestQEE.ExecSQL;

            DestQEE.SQL.Text := 'DELETE FROM PR_CHECK_STATES WHERE PR_CHECK_STATES_NBR = ' +
              SourceQ.FieldByName('PR_CHECK_STATES_NBR').AsString;

            DestQEE.ExecSQL;  

            SourceQ.Next;
          end;

          SourceQ.Close;

          SourceQ.SQL.Text :=
            ' select l.PR_CHECK_LINES_NBR, k.EE_STATES_NBR' +
            ' from PR_CHECK_LINES l' +
            ' join PR_CHECK c on c.PR_CHECK_NBR=l.PR_CHECK_NBR' +
            ' join PR p on p.PR_NBR=c.PR_NBR' +
            ' join EE_STATES e on e.EE_STATES_NBR=l.EE_STATES_NBR' +
            ' join EE_STATES k on k.EE_NBR=c.EE_NBR and k.CO_STATES_NBR=e.CO_STATES_NBR' +
            ' where l.ACTIVE_RECORD=''C''' +
            ' and c.ACTIVE_RECORD=''C''' +
            ' and e.ACTIVE_RECORD=''C''' +
            ' and p.CHECK_DATE > ''09/01/2011'' ' +
            ' and e.EE_NBR <> c.EE_NBR';

          SourceQ.Open;

          SourceQ.First;
          while not SourceQ.Eof do
          begin
            DestQEE.SQL.Text := 'UPDATE PR_CHECK_LINES '+
              'SET EE_STATES_NBR = ' + SourceQ.FieldByName('EE_STATES_NBR').AsString +
             ' WHERE PR_CHECK_LINES_NBR = ' + SourceQ.FieldByName('PR_CHECK_LINES_NBR').AsString +
             ' AND ACTIVE_RECORD=''C''';

            DestQEE.ExecSQL;

            SourceQ.Next;
          end;

          SourceQ.Close;

          FSrcTran.Commit;
        except
          on E: Exception do
          begin
            FSrcTran.Rollback;
            FCallback.OnError(E.Message);
          end;
        end;
      finally
        DestQEE.Free;
        SourceQ.Free;
      end;
   finally
     FSrcDB.Close;
   end;
end;

end.
