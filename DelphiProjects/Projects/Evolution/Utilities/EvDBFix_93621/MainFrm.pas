unit MainFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, Processing, isThreadManager, isBaseClasses,
  isBasicUtils, ExtCtrls, ISBasicClasses, Buttons, IBDatabase, IBQuery;

type
  TMain = class(TForm, IevProcessingCallback)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    edDBPassword: TEdit;
    memLog: TMemo;
    Label4: TLabel;
    lblClient: TLabel;
    edtClients: TEdit;
    edDBPath: TEdit;
    lblTemp: TLabel;
    btnRun: TBitBtn;
    procedure btnRunClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    FInProgress: Boolean;
    FMCThreadManager: IThreadManager;
    procedure DoLogMessage(const AText: String);
    procedure MulticlientProcess(const Params: TTaskParamList);
  protected
    procedure OnStart;
    procedure OnError(const AError: String);
    procedure OnFinish;
    procedure LogMessage(const AText: String);
  public
  end;

var
  Main: TMain;

implementation

{$R *.dfm}

procedure TMain.btnRunClick(Sender: TObject);
begin
  FMCThreadManager.RunTask(MulticlientProcess, Self,
    MakeTaskParams([Trim(edDBPath.Text), edDBPassword.Text, edtClients.Text]),
    'Multiclient loop');
end;

procedure TMain.LogMessage(const AText: String);

   procedure DoLogMessage(const Params: TTaskParamList);
   begin
     Main.DoLogMessage(Params[0]);
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoLogMessage, MakeTaskParams([AText]), False);
end;

procedure TMain.OnError(const AError: String);

   procedure DoOnError(const Params: TTaskParamList);
   begin
     Main.DoLogMessage('ERROR: ' + Params[0]);
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnError, MakeTaskParams([AError]), False);
end;

procedure TMain.OnFinish;

   procedure DoOnFinish(const Params: TTaskParamList);
   begin
     Main.DoLogMessage('Finished');

     Main.btnRun.Enabled := True;

     Main.edDBPassword.Enabled := True;
     Main.edtClients.Enabled := True;
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnFinish, MakeTaskParams([]), False);
end;

procedure TMain.OnStart;

   procedure DoOnStart(const Params: TTaskParamList);
   begin
     Main.btnRun.Enabled := False;

     Main.edDBPassword.Enabled := False;
     Main.edtClients.Enabled := False;     

     Main.memLog.Clear;
     Main.DoLogMessage('Started');
   end;

begin
  GlobalThreadManager.RunInMainThread(@DoOnStart, MakeTaskParams([]), False);
end;

procedure TMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not FInProgress;
end;

procedure TMain.DoLogMessage(const AText: String);
begin
  memLog.Lines.Add(FormatDateTime('hh:nn:ss', Now) + '     ' +
    StringReplace(StringReplace(AText, #13, ' ', [rfReplaceAll]), #13, ' ', [rfReplaceAll]));
end;

procedure TMain.MulticlientProcess(const Params: TTaskParamList);
var
  iClient : IisStringList;
  DBDataRecover: IevDBDataRecover;
  i: Integer;
  sDB1: String;
begin
  FInProgress := True;
  OnStart;
  try
    iClient := TisStringList.Create;
    iClient.CommaText := Params[2];

    for i := 0 to iClient.Count - 1 do
    begin
      sDB1 := NormalizePath(Params[0]) + 'CL_'+ Trim(iClient[i]) + '.gdb';

      try
        DBDataRecover := TevDBDataRecover.Create;
        DBDataRecover.Execute(sDB1, Params[1], Self);
        LogMessage('Started CL_' + Trim(iClient[i]));
      except
        on E: Exception do
          OnError(E.Message);
      end;
    end;

    GlobalThreadManager.WaitForTaskEnd;

  finally
    FInProgress := False;
    OnFinish;
  end;
end;

procedure TMain.FormCreate(Sender: TObject);
begin
  FMCThreadManager := TThreadManager.Create('MC TM');
  GlobalThreadManager.Capacity := HowManyProcessors;
end;

end.
