object DM: TDM
  OldCreateOrder = False
  Left = 66
  Top = 166
  Height = 532
  Width = 958
  object FIBDbSystem1: TIBDatabase
    Params.Strings = (
      'isc_dpb_user_name=EUSER'
      'isc_dpb_password=pps97'
      '')
    LoginPrompt = False
    DefaultTransaction = FIBTransactionSystem1
    AllowStreamedConnected = False
    Left = 24
    Top = 16
  end
  object FIBDbSystem2: TIBDatabase
    Params.Strings = (
      'isc_dpb_user_name=EUSER'
      'isc_dpb_password=pps97')
    LoginPrompt = False
    DefaultTransaction = FIBTransactionSystem2
    AllowStreamedConnected = False
    Left = 24
    Top = 80
  end
  object FIBTransactionSystem2: TIBTransaction
    DefaultDatabase = FIBDbSystem2
    DefaultAction = TARollback
    Left = 160
    Top = 80
  end
  object spBeforeCommitTransaction2: TIBStoredProc
    Database = FIBDbSystem2
    Transaction = FIBTransactionSystem2
    StoredProcName = 'BEFORE_COMMIT_TRANSACTION'
    Left = 328
    Top = 80
  end
  object spAfterStartTransaction2: TIBStoredProc
    Database = FIBDbSystem2
    Transaction = FIBTransactionSystem2
    StoredProcName = 'AFTER_START_TRANSACTION'
    Left = 736
    Top = 104
  end
  object FIBDbS_BUREAU: TIBDatabase
    Params.Strings = (
      'isc_dpb_user_name=EUSER'
      'isc_dpb_password=pps97')
    LoginPrompt = False
    DefaultTransaction = FIBTransactionS_Bureau
    AllowStreamedConnected = False
    Left = 32
    Top = 168
  end
  object evTableUser: TIBTable
    Database = FIBDbS_BUREAU
    Transaction = FIBTransactionS_Bureau
    StoreDefs = True
    TableName = 'SB_USER'
    Left = 520
    Top = 168
  end
  object evLTable1: TIBTable
    Database = FIBDbSystem1
    Transaction = FIBTransactionSystem1
    StoreDefs = True
    Left = 600
    Top = 24
  end
  object evLTable2: TIBTable
    Database = FIBDbSystem2
    Transaction = FIBTransactionSystem2
    StoreDefs = True
    Left = 600
    Top = 96
  end
  object FIBTransactionSystem1: TIBTransaction
    DefaultDatabase = FIBDbSystem1
    DefaultAction = TARollback
    Left = 160
    Top = 16
  end
  object FIBTransactionS_Bureau: TIBTransaction
    DefaultDatabase = FIBDbS_BUREAU
    DefaultAction = TARollback
    Left = 160
    Top = 168
  end
  object spBeforeCommitTransactionS_Bureau: TIBStoredProc
    Database = FIBDbS_BUREAU
    Transaction = FIBTransactionS_Bureau
    StoredProcName = 'BEFORE_COMMIT_TRANSACTION'
    Left = 328
    Top = 168
  end
  object spBeforeCommitTransaction1: TIBStoredProc
    Database = FIBDbSystem1
    Transaction = FIBTransactionSystem1
    StoredProcName = 'BEFORE_COMMIT_TRANSACTION'
    Left = 328
    Top = 16
  end
  object spAfterStartTransaction1: TIBStoredProc
    Database = FIBDbSystem1
    Transaction = FIBTransactionSystem1
    StoredProcName = 'AFTER_START_TRANSACTION'
    Left = 728
    Top = 24
  end
  object spAfterStartTransactionS_Bureau: TIBStoredProc
    Database = FIBDbS_BUREAU
    Transaction = FIBTransactionS_Bureau
    StoredProcName = 'AFTER_START_TRANSACTION'
    Left = 736
    Top = 168
  end
end
