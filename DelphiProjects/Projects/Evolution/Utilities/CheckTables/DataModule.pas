// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, IBCustomDataSet, IBQuery, IBDatabase,
  IBStoredProc, Menus, IBTable, DBClient, Provider, IBSQL, SDEngine,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents;

type
  TDM = class(TDataModule)
    FIBDbSystem1: TIBDatabase;
    FIBDbSystem2: TIBDatabase;
    FIBTransactionSystem2: TIBTransaction;
    spBeforeCommitTransaction2: TIBStoredProc;
    spAfterStartTransaction2: TIBStoredProc;
    FIBDbS_BUREAU: TIBDatabase;
    evTableUser: TIBTable;
    evLTable1: TIBTable;
    evLTable2: TIBTable;
    FIBTransactionSystem1: TIBTransaction;
    FIBTransactionS_Bureau: TIBTransaction;
    spBeforeCommitTransactionS_Bureau: TIBStoredProc;
    spBeforeCommitTransaction1: TIBStoredProc;
    spAfterStartTransaction1: TIBStoredProc;
    spAfterStartTransactionS_Bureau: TIBStoredProc;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{$R *.DFM}

end.
