// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, unxcrypt,
  StdCtrls, IBQuery, IBDatabase, Buttons,  CheckLst,
  db, StrUtils, Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, wwdblook, Wwdbdlg, DBGrids, Mask, ComCtrls, inifiles,
  Menus,
  IBSQL,IBTable, IB,IBUtils, IBHeader,  IBCustomDataSet, Variants, ExtCtrls,
  Registry, Datamodule,
  evUtils, ISBasicClasses,  EvUIUtils, EvUIComponents, EvConsts;


type
  TevIBTable = class(TIBTable)
  private
  protected
    procedure CreateFields; override;
  public
  published
  end;

  TForm1 = class(TForm)
    evDs1: TevDataSource;
    evDs2: TevDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel4: TPanel;
    evLabel3: TevLabel;
    evCbDbType: TevComboBox;
    evLabel4: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel6: TevLabel;
    evActiveRecord: TevCheckBox;
    evT2Nbr1: TevEdit;
    evT1Nbr1: TevEdit;
    evT1Nbr2: TevEdit;
    evT2Nbr2: TevEdit;
    evcbTableList: TevComboBox;
    evCbDb2: TevComboBox;
    evCbDb1: TevComboBox;
    evCbSb1: TevComboBox;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evOrderBy: TevEdit;
    evExtFilter: TevEdit;
    evbtnTables: TevButton;
    evBtnDisconnect: TevButton;
    evBtnConnect: TevButton;
    evBtnInsert: TevButton;
    evBtnEdit: TevButton;
    evbtnCompare: TevButton;
    evFieldList: TevCheckListBox;
    evcbLookup: TevCheckBox;
    evlblSelect1: TevLabel;
    evlblSelect2: TevLabel;
    btnCancel: TevButton;
    Panel5: TPanel;
    evLblResult1: TevLabel;
    evLblResult2: TevLabel;
    evList1: TevListView;
    evList2: TevListView;
    Panel6: TPanel;
    Splitter2: TSplitter;
    Grd1: TwwDBGrid;
    Grd2: TwwDBGrid;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    evLabel5: TevLabel;
    procedure evbtnCompareClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure evbtnTablesClick(Sender: TObject);
    procedure evT1Nbr1KeyPress(Sender: TObject; var Key: Char);
    procedure evcbTableListChange(Sender: TObject);
    procedure evBtnConnectClick(Sender: TObject);
    procedure evBtnDisconnectClick(Sender: TObject);
    procedure CloseDatabase;
    procedure CloseTables;
    procedure SetButtons;
    procedure SetSelectedLabel1;
    procedure SetSelectedLabel2;
    procedure evList1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure evBtnInsertClick(Sender: TObject);
    procedure evDs1DataChange(Sender: TObject; Field: TField);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure evBtnEditClick(Sender: TObject);
    procedure TableFilterSetup(const bFilter : Boolean);
    function  GetTableKeyNbr:String;
    function  FindUserName(tb: TIBTable):String;
    function  GetTableName:String;
    function  GetTableKeys:String;
    function  GetTableKeysValue(tb: TIBTable):Variant;
    function  GetTableKeysValueString(tb: TIBTable):String;
    procedure SetComboBox;
    procedure evCbDbTypeChange(Sender: TObject);
  private
    InsertRecordNbr : String;
    EditRecordNbr : String;
    evSource : TevIBTable;
    evTarget : TevIBTable;
  public
      { Public declarations }
     Settings           : TIniFile;
     FieldMap           : TStringList;

  end;

const
  pUser = FB_Admin;
  pPassword = 'pps97';

var
  Form1: TForm1;

implementation

uses  SFieldCodeValues;

{$R *.DFM}

procedure TForm1.FormShow(Sender: TObject);
begin
  Caption := Application.Title;
end;

procedure TForm1.evBtnConnectClick(Sender: TObject);
begin
  with DM do
  begin
    if not FIBDbS_BUREAU.Connected then
    begin
     FIBDbS_BUREAU.DatabaseName := evCbSb1.Text;
     DM.FIBDbS_BUREAU.Params.Values['isc_dpb_user_name'] := pUser;
     DM.FIBDbS_BUREAU.Params.Values['isc_dpb_password'] := pPassword;
     FIBDbS_BUREAU.Connected := True;
    end;

    if not FIBDbSystem1.Connected then
    begin
     FIBDbSystem1.DatabaseName := evCbDb1.Text;
     DM.FIBDbSystem1.Params.Values['isc_dpb_user_name'] := pUser;
     DM.FIBDbSystem1.Params.Values['isc_dpb_password'] := pPassword;
     FIBDbSystem1.Connected := True;
    end;

    if not FIBDbSystem2.Connected then
    Begin
     FIBDbSystem2.DatabaseName := evCbDb2.Text;
     DM.FIBDbSystem2.Params.Values['isc_dpb_user_name'] := pUser;
     DM.FIBDbSystem2.Params.Values['isc_dpb_password'] := pPassword;
     FIBDbSystem2.Connected := True;
    end;

    if FIBDbS_BUREAU.Connected and FIBDbSystem1.Connected and FIBDbSystem2.Connected then
      evBtnConnect.Enabled := False;
  end;
  evbtnTables.Enabled := not evBtnConnect.Enabled;
  evBtnDisconnect.Enabled := not evBtnConnect.Enabled;
end;

procedure TForm1.evBtnDisconnectClick(Sender: TObject);
begin
      CloseTables;
      CloseDatabase;

      evBtnConnect.Enabled := True;
      evBtnDisconnect.Enabled := not evBtnConnect.Enabled;

      SetButtons;
end;

procedure TForm1.CloseDatabase;
Begin
      DM.FIBDbSystem1.Connected := False;
      DM.FIBDbSystem2.Connected := False;
      DM.FIBDbS_BUREAU.Connected := False;
end;

procedure TForm1.CloseTables;
Begin

  with DM do
  begin
    evSource.IndexFieldNames := '';
    evTarget.IndexFieldNames := '';
    evSource.Filter := '';
    evSource.Filtered := false;

    evSource.Close;

    if evTarget.Active then
    begin
      evTarget.Filter := '';
      evTarget.Filtered := false;
      evTarget.Close;
    end;

    evTableUser.Close;
  end;
  evBtnEdit.Visible := False;
  evBtnInsert.Visible := False;

end;

procedure TForm1.SetButtons;
Begin
  evbtnTables.Enabled := not evBtnConnect.Enabled;
  evbtnCompare.Enabled := False;
  evList1.Items.Clear;
  evList2.Items.Clear;
  evFieldList.Clear;
end;

procedure TForm1.evcbTableListChange(Sender: TObject);
begin
  CloseTables;
  SetButtons;
  evOrderBy.Text := GetTableKeys + ' ';
end;

procedure TForm1.btnCancelClick(Sender: TObject);
begin
  CloseTables;
  CloseDatabase;
  Application.Terminate;
end;

procedure TForm1.TableFilterSetup(const bFilter : Boolean);
var
 sFilter1, sFilter2 : String;
begin
  with DM do
  begin
   sFilter1 := '';
   sFilter2 := '';

   evTableUser.Filter := 'ACTIVE_RECORD = ''C''';
   evTableUser.Filtered := True;

   if  evCbDbType.Items[evCbDbType.ItemIndex] <> 'temp' then
   begin
      if evActiveRecord.Checked  and   (evCbDbType.Items[evCbDbType.ItemIndex] <> 'temp') then
      begin
          sFilter1 := 'ACTIVE_RECORD = ''C''';
          sFilter2 := 'ACTIVE_RECORD = ''C''';
      end;

      if ((Length(Trim(evT1Nbr1.Text)) > 0) and (Length(Trim(evT1Nbr2.Text)) > 0)) then
      begin
         if length(sFilter1)> 0 then  sFilter1 :=sFilter1 + ' and ';

         sFilter1 := sFilter1 + GetTableKeyNbr + ' >= ' +  evT1Nbr1.Text + ' and '  +
                                GetTableKeyNbr + ' <= ' +  evT1Nbr2.Text;
      end;

      if ((Length(Trim(evT2Nbr1.Text)) > 0) and (Length(Trim(evT2Nbr2.Text)) > 0)) then
      begin
         if length(sFilter2)> 0 then  sFilter2 :=sFilter2 + ' and ';

         sFilter2 := sFilter2 + GetTableKeyNbr + ' >= ' + evT2Nbr1.Text + ' and '  +
                                GetTableKeyNbr + ' <= ' +  evT2Nbr2.Text;
      end;
   end;
   if (Length(Trim(evExtFilter.Text)) > 0)  then
   begin

     if length(sFilter1)> 0 then  sFilter1 :=sFilter1 + ' and ';
     sFilter1 := sFilter1 + evExtFilter.Text;

     if length(sFilter2)> 0 then  sFilter2 :=sFilter2 + ' and ';
     sFilter2 := sFilter2 + evExtFilter.Text;
   end;

   if bFilter then
   begin
    evSource.Filter := sFilter1;
    evSource.Filtered := True;
    evTarget.Filter := sFilter2;
    evTarget.Filtered := True;
   end;
  end;
end;


procedure TForm1.evbtnTablesClick(Sender: TObject);
var
 i : Integer;
begin
 if evcbTableList.ItemIndex > -1 then
 begin

     InsertRecordNbr := '';
     EditRecordNbr := '';
     SetSelectedLabel1;
     SetSelectedLabel2;

     CloseTables;
     with DM do
     begin
         evTableUser.TableName := 'SB_USER';
         evTableUser.Open;

         evSource.Database := DM.FIBDbSystem1;
         evSource.Transaction := DM.FIBTransactionSystem1;
         evTarget.Database := DM.FIBDbSystem2;
         evTarget.Transaction := DM.FIBTransactionSystem2;

         evDs1.DataSet:= evSource;
         evDs2.DataSet:= evTarget;

         evSource.TableName := GetTableName;
         evTarget.TableName := GetTableName;

         evSource.Open;
         evTarget.Open;
         TableFilterSetup(True);

         evSource.IndexFieldNames := GetTableKeys;
         evTarget.IndexFieldNames := GetTableKeys;


         if evSource.Active and evTarget.Active then
         begin
           evbtnCompare.Enabled := True;
         //  evSource.Last;     evSource.First;
         //  evTarget.Last;     evTarget.First;
         //  edTotalRec1.Caption := IntToStr(evSource.RecordCount);
         //  edTotalRec2.Caption := IntToStr(evTarget.RecordCount);
         end;


          evFieldList.Clear;
          for i := 0 to evSource.Fields.Count-1 do
            evFieldList.Items.Add(evSource.Fields[i].DisplayName);
      end;

 {
     DM.evTableUser.TableName := 'SB_USER';
     DM.evTableUser.Open;

      s :='';
      if evActiveRecord.Checked or
         ((Length(Trim(evT1Nbr1.Text)) > 0) and (Length(Trim(evT1Nbr2.Text)) > 0)) or
         (Length(Trim(evOrderBy.Text)) > 0)  then
          s :=' Where ';

      if evActiveRecord.Checked then
         s :=s + ' ACTIVE_RECORD = ''C'' ';
      if ((Length(Trim(evT1Nbr1.Text)) > 0) and (Length(Trim(evT1Nbr2.Text)) > 0)) then
      begin

       if length(s)> 0 then  s :=s + ' and ';

        s := s +
           evcbTableList.Items[evcbTableList.ItemIndex]+'_NBR >= ' +
          evT1Nbr1.Text + ' and '  +
          evcbTableList.Items[evcbTableList.ItemIndex]+'_NBR <= ' +
          evT1Nbr2.Text;
      end;

      s :=' Select * from ' + evcbTableList.Items[evcbTableList.ItemIndex] + s;
      if Length(Trim(evOrderBy.Text)) > 0 then
       s := s +  ' order by ' +  evOrderBy.Text;

     try

       DM.evSource.Close;
       DM.evSource.SQL.Clear;
       DM.evSource.SQL.Add(s);
       DM.evSource.Open;

     except
      raise Exception.Create('Error getting Table 1 open.');
     end;
     try
      DM.evTarget.Close;
      DM.evTarget.SQL.Clear;
      DM.evTarget.SQL.Add(s);
      DM.evTarget.Open;
     except
      raise Exception.Create('Error getting Table 2 open.');
     end;
     if DM.evSource.Active and DM.evTarget.Active then
       evbtnCompare.Enabled := True;

      evFieldList.Clear;
      for i := 0 to DM.evSource.Fields.Count-1 do
        evFieldList.Items.Add(DM.evSource.Fields[i].DisplayName);
}

     Grd1.DataSource := evDs1;
     Grd2.DataSource := evDs2;
 end
 else
    EvMessage('Please Select Table...', mtInformation, [mbYes]);
end;

procedure TForm1.evbtnCompareClick(Sender: TObject);
var
  sUser, SFieldList : String;

//  procedure Process1(tb1, tb2: TIBQuery; List : TevListView; const CompareType : integer);
  procedure Process1(tb1, tb2: TIBTable; List : TevListView; const CompareType : integer);
  var
    i : integer;
    bRecordCheck : Boolean;
    LookupTable : String;
  begin
     bRecordCheck := False;
     for i := 0 to tb1.Fields.Count-1 do
      if Pos(tb1.Fields[i].DisplayName, SFieldList) =  0 then
       if tb1.Fields[i].Visible and
           (tb1.Fields[i].DataType in [ftString, ftCurrency, ftFloat, ftInteger, ftDateTime, ftDate, ftFMTBcd])  then
       begin
          if Assigned(tb2.FindField(tb1.Fields[i].DisplayName)) then
           if tb1.Fields[i].Value <> tb2.FindField(tb1.Fields[i].DisplayName).Value then
           begin
             bRecordCheck := True;
             if CompareType = 1 then
             begin
               with List.Items.Add do
               begin
                  Caption := GetTableKeysValueString(tb1);
                  SubItems.Add( sUser);
                  SubItems.Add( tb1.Fields[i].DisplayName);
                  SubItems.Add( tb1.Fields[i].asString);
                  SubItems.Add( tb2.FindField(tb1.Fields[i].DisplayName).asString);

                  if evcbLookup.Checked then
                  begin
                      if (Copy(tb1.Fields[i].DisplayName, Length(tb1.Fields[i].DisplayName)-3, 4) = '_NBR') and
                          (Copy(tb1.Fields[i].DisplayName, 0, 3) = 'SY_') and
                          (GetTableKeyNbr <>
                             Copy(tb1.Fields[i].DisplayName, 0, Pos('_NBR', tb1.Fields[i].DisplayName)-1))
                           then
                      begin
                        with DM do
                        begin
                          LookupTable := Copy(tb1.Fields[i].DisplayName, 0, Pos('_NBR', tb1.Fields[i].DisplayName)-1);
                          evLTable1.TableName := LookupTable;
                          evLTable2.TableName := LookupTable;
                          evLTable1.Open;
                          evLTable2.Open;
                          try
                            evLTable1.Filter := 'ACTIVE_RECORD = ''C''';
                            evLTable1.Filtered := True;
                            evLTable2.Filter := 'ACTIVE_RECORD = ''C''';
                            evLTable2.Filtered := True;
                            evLTable1.IndexFieldNames := LookupTable + '_NBR';
                            evLTable2.IndexFieldNames := LookupTable + '_NBR';

                            if evLTable1.Locate(LookupTable+'_NBR', tb1.Fields.FieldByName(LookupTable+'_NBR').AsInteger,[]) then
                                  SubItems.Add(evLTable1.Fields.FieldByName(FieldMap.Values[LookupTable+'_NBR']).asString)
                             else SubItems.Add( 'N/A');

                            if evLTable2.Locate(LookupTable+'_NBR', tb2.Fields.FieldByName(LookupTable+'_NBR').AsInteger,[]) then
                                  SubItems.Add(evLTable2.Fields.FieldByName(FieldMap.Values[LookupTable+'_NBR']).asString)
                             else SubItems.Add( 'N/A');
                          finally
                           evLTable1.Close;
                           evLTable2.Close;
                          end;
                        end;
                      end;
                  end;
               end;
             end;
           end;
       end;
     if (not bRecordCheck) and (CompareType = 2) then
       with List.Items.Add do
        begin
          Caption := GetTableKeysValueString(tb1);
          SubItems.Add( sUser);
          SubItems.Add( 'Same Record ');
        end;
  end;
//  procedure Process2(tb1: TIBQuery; List : TevListView);
  procedure Process2(tb1: TIBTable; List : TevListView);
  begin
        with List.Items.Add do
        begin
          Caption := GetTableKeysValueString(tb1);
          SubItems.Add( sUser);
          SubItems.Add( 'New Record');
        end;
  end;

var
  i : Integer;
  CompareType : integer;
begin
  Screen.Cursor := crSQLWait;
  try

    SetSelectedLabel1;
    SetSelectedLabel2;
    evList1.Items.Clear;
    evList2.Items.Clear;
    SFieldList := '';
    for i := 0 to evFieldList.Items.Count-1 do
      if evFieldList.Checked[i] then
       SFieldList :=SFieldList + ';' + evFieldList.Items[i];
    with DM do
    begin
      evSource.DisableControls;
      evTarget.DisableControls;

      try
       if (Grd1.SelectedList.Count = 1) and (Grd2.SelectedList.Count = 1) then
       begin
          evLblResult1.Caption := 'Listed different Fields ';
          evLblResult2.Caption := '';
          CompareType := 1;

          Grd1.DataSource.DataSet.GotoBookmark(Grd1.SelectedList[0]);
          Grd2.DataSource.DataSet.GotoBookmark(Grd2.SelectedList[0]);
          sUser := '';

//          sUser := FindUserName(evSource);
          Process1(evSource,evTarget, evList1, CompareType);
       end
       else
       if (Grd1.SelectedList.Count = 1) and
          (Grd2.SelectedList.Count = 0) then
       begin
          evLblResult1.Caption := '';
          evLblResult2.Caption := 'Listed Same Records ';
          CompareType := 2;
          Grd1.DataSource.DataSet.GotoBookmark(Grd1.SelectedList[0]);
          evTarget.First;
          while not evTarget.Eof do
          begin
//             sUser := FindUserName(evTarget);
          sUser := '';
             Process1(evTarget,evSource, evList2, CompareType);
            evTarget.Next;
          end;
       end
       else
       if (Grd1.SelectedList.Count = 0) and
          (Grd2.SelectedList.Count = 1) then
       begin
          evLblResult1.Caption := 'Listed Same Records ';
          evLblResult2.Caption := '';
          CompareType := 2;
          Grd2.DataSource.DataSet.GotoBookmark(Grd2.SelectedList[0]);
          evSource.First;
          while not evSource.Eof do
          begin
                    sUser := '';
//             sUser := FindUserName(evSource);
             Process1(evSource,evTarget, evList1, CompareType);
            evSource.Next;
          end;
       end
       else
       begin
          evLblResult1.Caption := 'Listed different Fields ';
          evLblResult2.Caption := 'Listed different Record ';
          CompareType := 1;

          evSource.First;
          while not evSource.Eof do
          begin
                    sUser := '';
//            sUser := FindUserName(evSource);
            if evTarget.Locate(GetTableKeys,GetTableKeysValue(evSource), [])
               then
                  Process1(evSource,evTarget, evList1, CompareType)
            else  Process2(evSource, evList1);
            evSource.Next;
          end;

          evTarget.First;
          while not evTarget.Eof do
          begin
                    sUser := '';
//            sUser := FindUserName(evTarget);
            if not evSource.Locate(GetTableKeys,GetTableKeysValue(evTarget), []) then
                 Process2(evTarget, evList2);
            evTarget.Next;
          end;
       end;
      finally
       evSource.EnableControls;
       evTarget.EnableControls;
      end;
    end;

  finally
    Screen.Cursor := crDefault;
    EvMessage('Compare Process Done...', mtInformation, [mbYes]);
  end;

end;


procedure TForm1.evT1Nbr1KeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9']) then begin
    ShowMessage('Invalid key');
    Key := #0;
  end;
end;

procedure TForm1.SetSelectedLabel1;
Begin
  if  Grd1.SelectedList.Count > 0 then
       evlblSelect1.Caption := '~Selected    '
  else evlblSelect1.Caption := '~UnSelected    ';
End;

procedure TForm1.SetSelectedLabel2;
Begin
  if  Grd2.SelectedList.Count > 0 then
       evlblSelect2.Caption := '~Selected    '
  else evlblSelect2.Caption := '~UnSelected    ';
End;

procedure TForm1.evList1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
 if evCbDbType.Items[evCbDbType.ItemIndex] = 'temp' then  exit;
 if Selected and (Item.SubItems[1] = 'New Record') then
 begin
   Grd1.UnselectAll;
   Grd2.UnselectAll;
   if Assigned(evDs1.DataSet) then
   begin
    InsertRecordNbr := Item.Caption;
    evBtnInsert.Visible := True;
    evDs1.DataSet.Locate(GetTableKeyNbr, StrToInt(InsertRecordNbr),[]);
   end;
 end
 else
 if Selected  then
 begin
   Grd1.UnselectAll;
   Grd2.UnselectAll;
   if Assigned(evDs1.DataSet) and
      Assigned(evDs2.DataSet) then
   begin
    EditRecordNbr := Item.Caption;
    evBtnEdit.Visible := True;
    evDs1.DataSet.Locate(GetTableKeyNbr, StrToInt(EditRecordNbr),[]);
    evDs2.DataSet.Locate(GetTableKeyNbr, StrToInt(EditRecordNbr),[]);
   end;
 end;
end;

procedure TForm1.evBtnInsertClick(Sender: TObject);
var
 i,l  : integer;
 SFieldList : String;
begin
    if (EvMessage('This process will create new Record in Table 2.'#13+
         'Are you sure you want to proceed?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
      exit;

    if not evDs1.DataSet.Locate(GetTableKeyNbr, StrToInt(InsertRecordNbr),[]) then
    begin
      EvMessage('Table 1 can not locate...', mtInformation, [mbYes]);
      exit;
    end;

    Screen.Cursor := crSQLWait;
    try
      with DM do
      begin
          FIBTransactionSystem2.Active := True;
          try
            spAfterStartTransaction2.ExecProc;
            evTarget.Insert;

            for i := 0 to evSource.Fields.Count-1 do
              if Assigned(evTarget.FindField(evSource.Fields[i].DisplayName)) then
                evTarget.FindField(evSource.Fields[i].DisplayName).Value :=  evSource.Fields[i].Value;

            SFieldList := '';
            for i := 0 to evTarget.Fields.Count-1 do
              if evTarget.Fields[i].Visible and
                 (evTarget.Fields[i].DataType in [ftString]) and
                  (evTarget.Fields[i].DataSize = 2) and
                   (evTarget.Fields[i].IsNull) then
                     SFieldList :=SFieldList + ';' + evTarget.Fields[i].DisplayName;

            l := Low(FieldsToPopulate);
            for i := l to High(FieldsToPopulate) do
             if (AnsiCompareText(FieldsToPopulate[i].T.GetTableName, GetTableName) = 0) and
                 (Pos(FieldsToPopulate[i].F, SFieldList) > 0) and
                  (Assigned(evTarget.FindField(FieldsToPopulate[i].F))) and
                   (Length(FieldsToPopulate[i].D) > 0)  then
                    evTarget.FindField(FieldsToPopulate[i].F).Value := FieldsToPopulate[i].D;

            for i := 0 to evTarget.Fields.Count-1 do
              if evTarget.Fields[i].Visible and
                 (evTarget.Fields[i].DataType in [ftString]) and
                  (evTarget.Fields[i].DataSize = 2) and
                   (evTarget.Fields[i].IsNull) then
                     evTarget.Fields[i].Value := ' ';

            evTarget.FieldByName('CREATION_DATE').AsDateTime := Now;
            evTarget.post;
            spBeforeCommitTransaction2.ExecProc;
            FIBTransactionSystem2.Commit;
          finally
            if FIBTransactionSystem2.InTransaction then
              FIBTransactionSystem2.Rollback;
          end;
      end;
    finally
      Screen.Cursor := crDefault;
      CloseTables;
      evbtnTablesClick(sender);
    end;

    evBtnInsert.Visible := False;
end;

procedure TForm1.evBtnEditClick(Sender: TObject);
var
 i  : integer;
begin
    if (EvMessage('This process will Update current Record in Table 2.'#13+
         'Are you sure you want to proceed?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
      exit;
    if (EditRecordNbr = '') then
    begin
      EvMessage('Edit Record Nbr is null...', mtInformation, [mbYes]);
      exit;
    end;

    if not evDs1.DataSet.Locate(GetTableKeyNbr, StrToInt(EditRecordNbr),[]) then
    begin
      EvMessage('Table 1 can not locate...', mtInformation, [mbYes]);
      exit;
    end;
    if not evDs2.DataSet.Locate(GetTableKeyNbr, StrToInt(EditRecordNbr),[]) then
    begin
      EvMessage('Table 2 can not locate...', mtInformation, [mbYes]);
      exit;
    end;

    Screen.Cursor := crSQLWait;
    try
      with DM do
      begin
          FIBTransactionSystem2.Active := True;
          try
            spAfterStartTransaction2.ExecProc;
            evTarget.Edit;

            for i := 0 to evSource.Fields.Count-1 do
              if Assigned(evTarget.FindField(evSource.Fields[i].DisplayName)) then
                evTarget.FindField(evSource.Fields[i].DisplayName).Value :=  evSource.Fields[i].Value;

            evTarget.FieldByName('CREATION_DATE').AsDateTime := Now;
            evTarget.post;
            spBeforeCommitTransaction2.ExecProc;
            FIBTransactionSystem2.Commit;
          finally
            if FIBTransactionSystem2.InTransaction then
              FIBTransactionSystem2.Rollback;
          end;
      end;
    finally
      Screen.Cursor := crDefault;
      CloseTables;
      evbtnTablesClick(sender);
    end;

    evBtnEdit.Visible := False;

end;

procedure TForm1.evDs1DataChange(Sender: TObject; Field: TField);
begin
   if Assigned(evBtnInsert) and Assigned(evDs1.DataSet) and
      evBtnInsert.Visible   and
      (InsertRecordNbr <> evDs1.DataSet.FieldByName(GetTableKeyNbr).AsString) then
   begin
      evBtnInsert.Visible := False;
      InsertRecordNbr := '';
   end;
   if Assigned(evBtnEdit) and Assigned(evDs1.DataSet) and
      evBtnEdit.Visible   and
      (EditRecordNbr <> evDs1.DataSet.FieldByName(GetTableKeyNbr).AsString) then
   begin
      evBtnEdit.Visible := False;
      EditRecordNbr := '';
   end;

end;

function TForm1.GetTableName:String;
begin
  result :=evcbTableList.Items[evcbTableList.ItemIndex];
end;

function TForm1.GetTableKeyNbr:String;
begin
  result :=evcbTableList.Items[evcbTableList.ItemIndex]+'_NBR';
  if evCbDbType.Items[evCbDbType.ItemIndex] = 'temp' then
   Delete(result, 1, 4);
end;

function TForm1.GetTableKeys:String;
begin
  result := GetTableKeyNbr;
  if evCbDbType.Items[evCbDbType.ItemIndex] = 'temp' then
  begin
   if (Pos('_CO', evcbTableList.Items[evcbTableList.ItemIndex]) = 0) and
      (Pos('_CL', evcbTableList.Items[evcbTableList.ItemIndex]) = 0) then
        result := 'CL_NBR;CO_NBR;' + result
   else
   begin
      if Pos('_CL', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
      begin
         if Pos('_CL_', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
           result := 'CL_NBR;' + result
      end
      else
      if Pos('_CO', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
      begin
         if Pos('_CO_', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
           result := 'CL_NBR;CO_NBR;' + result
         else
          result := 'CL_NBR;' + result;
      end;
   end;
  end;
end;

function TForm1.GetTableKeysValue(tb: TIBTable):Variant;
begin
  result := VarArrayOf([tb.Fields.FieldByName(GetTableKeyNbr).AsInteger]);
  if evCbDbType.Items[evCbDbType.ItemIndex] = 'temp' then
  begin
   if (Pos('_CO', evcbTableList.Items[evcbTableList.ItemIndex]) = 0) and
      (Pos('_CL', evcbTableList.Items[evcbTableList.ItemIndex]) = 0) then
        result := VarArrayOf([tb.Fields.FieldByName('CL_NBR').AsInteger,
                               tb.Fields.FieldByName('CO_NBR').AsInteger,
                                tb.Fields.FieldByName(GetTableKeyNbr).AsInteger])
   else
   begin
      if Pos('_CL', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
      begin
         if Pos('_CL_', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
            result := VarArrayOf([tb.Fields.FieldByName('CL_NBR').AsInteger,
                                    tb.Fields.FieldByName(GetTableKeyNbr).AsInteger]);

      end
      else
      if Pos('_CO', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
      begin
         if Pos('_CO_', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
            result := VarArrayOf([tb.Fields.FieldByName('CL_NBR').AsInteger,
                               tb.Fields.FieldByName('CO_NBR').AsInteger,
                                tb.Fields.FieldByName(GetTableKeyNbr).AsInteger])
         else
            result := VarArrayOf([tb.Fields.FieldByName('CL_NBR').AsInteger,
                                tb.Fields.FieldByName(GetTableKeyNbr).AsInteger]);
      end;
   end;
  end;
end;

function TForm1.GetTableKeysValueString(tb: TIBTable):String;
begin
  result := tb.Fields.FieldByName(GetTableKeyNbr).AsString;
  if evCbDbType.Items[evCbDbType.ItemIndex] = 'temp' then
  begin
   if (Pos('_CO', evcbTableList.Items[evcbTableList.ItemIndex]) = 0) and
      (Pos('_CL', evcbTableList.Items[evcbTableList.ItemIndex]) = 0) then
        result := tb.Fields.FieldByName('CL_NBR').AsString + ';' +
                               tb.Fields.FieldByName('CO_NBR').AsString + ';' +
                                tb.Fields.FieldByName(GetTableKeyNbr).AsString
   else
   begin
      if Pos('_CL', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
      begin
         if Pos('_CL_', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
           result := tb.Fields.FieldByName('CL_NBR').AsString + ';' +
                                tb.Fields.FieldByName(GetTableKeyNbr).AsString;

      end
      else
      if Pos('_CO', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
      begin
         if Pos('_CO_', evcbTableList.Items[evcbTableList.ItemIndex]) > 0 then
           result := tb.Fields.FieldByName('CL_NBR').AsString + ';' +
                               tb.Fields.FieldByName('CO_NBR').AsString + ';' +
                                tb.Fields.FieldByName(GetTableKeyNbr).AsString
         else
           result := tb.Fields.FieldByName('CL_NBR').AsString + ';' +
                                tb.Fields.FieldByName(GetTableKeyNbr).AsString;

      end;
   end;
  end;

end;


function TForm1.FindUserName(tb: TIBTable):String;
begin
  Result := 'N/A';
  if evCbDbType.Items[evCbDbType.ItemIndex] <> 'temp' then
    if DM.evTableUser.Locate('SB_USER_NBR',tb.Fields.FieldByName('CHANGED_BY').Value,[]) then
       Result := DM.evTableUser.Fields.FieldByName('USER_ID').AsString;
end;


procedure TForm1.SetComboBox;
var
 I : integer;
 S : String;
begin
    evCbDb1.Clear;
    Settings.ReadSectionValues(evCbDbType.Items[evCbDbType.ItemIndex] +'.connect', evCbDb1.Items);
    for I := 0 to evCbDb1.Items.Count - 1 do
    begin
      S := evCbDb1.Items[I];
      Delete(S, 1, Pos('=', S));
      evCbDb1.Items[I] := S;
    end;

    evCbDb2.Clear;
    Settings.ReadSectionValues(evCbDbType.Items[evCbDbType.ItemIndex] +'.connect', evCbDb2.Items);
    for I := 0 to evCbDb2.Items.Count - 1 do
    begin
      S := evCbDb2.Items[I];
      Delete(S, 1, Pos('=', S));
      evCbDb2.Items[I] := S;
    end;

    evcbTableList.Clear;
    Settings.ReadSectionValues(evCbDbType.Items[evCbDbType.ItemIndex] + '.tables', evcbTableList.Items);
    for I := 0 to evcbTableList.Items.Count - 1 do
    begin
      S := evcbTableList.Items[I];
      Delete(S, 1, Pos('=', S));
      evcbTableList.Items[I] := S;
    end;

    FieldMap.Clear;
    Settings.ReadSectionValues(evCbDbType.Items[evCbDbType.ItemIndex] + '.FieldMap', FieldMap);

end;

procedure TForm1.FormCreate(Sender: TObject);
var
 I : integer;
 S : String;
begin
    Settings  := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\CheckTables.Ini');

    evCbDbType.Clear;
    Settings.ReadSectionValues('dbtype', evCbDbType.Items);
    for I := 0 to evCbDbType.Items.Count - 1 do
    begin
      S := evCbDbType.Items[I];
      Delete(S, 1, Pos('=', S));
      evCbDbType.Items[I] := S;
    end;
    evCbDbType.ItemIndex := 0;

    evCbSb1.Clear;
    Settings.ReadSectionValues('Sb', evCbSb1.Items);
    for I := 0 to evCbSb1.Items.Count - 1 do
    begin
      S := evCbSb1.Items[I];
      Delete(S, 1, Pos('=', S));
      evCbSb1.Items[I] := S;
    end;

    FieldMap := TStringList.Create;

    SetComboBox;

    evSource :=  TevIBTable.Create(nil);
    evTarget :=  TevIBTable.Create(nil);

end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  if Assigned(Settings) then
     Settings.Free;
  if Assigned(FieldMap) then
     FieldMap.Free;

  evSource.Free;
  evTarget.Free;

end;


{ TevIBTable }

procedure TevIBTable.CreateFields;
var
   i : integer;
begin
  try
    FieldDefs.BeginUpdate;
    for i := 0 to FieldDefs.Count - 1 do
    begin
       if FieldDefs[i].DataType = ftFMTBCD then
       begin
         FieldDefs[i].DataType := ftFloat;
         FieldDefs[i].Size := 0;
         FieldDefs[i].Precision := 0;
       end;
    end;
  finally
    FieldDefs.EndUpdate;
  end;
  inherited CreateFields;
end;

procedure TForm1.evCbDbTypeChange(Sender: TObject);
begin
  evBtnDisconnectClick(Sender);
  SetComboBox;
end;

end.
