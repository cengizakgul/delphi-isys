unit FixNegative;

interface

uses Windows, Dialogs, EvContext, EvTypes,  EvConsts, EvDataset, EvCommonInterfaces,
  EvUtils, IsBaseClasses, SDDStreamClasses, rwCustomDataDictionary, SysUtils, isBasicUtils,
  EvStreamUtils, SDDClasses, SDataDictClient, SDataStructure, Controls, isThreadManager;

function FixNegativeKeys(const AClientNbr: Integer): IisStringList;
function FixNegativeKeysSB: IisStringList;

implementation

type
  TddsDataDictionaryExt = class(TddsDataDictionary)
  private
    procedure FixForeignKeys;
  public
    procedure AfterConstruction; override;
  end;

function FixNegativeKeys(const AClientNbr: Integer): IisStringList;
var
  SBCH : IevQuery;
  SBMAP: IisStringList;
  Idx, NewValue, NegValue: Integer;
  KeyMap: IisValueMap;
  TableName, RefTableName, KeyTableName, SQL, ForeignKey, ChangedKey: String;
  Table: TDataDictTable;
  i, j, n: Integer;
  DD: TddsDataDictionaryExt;
begin
  Result := TIsStringList.Create;
  DD := TddsDataDictionaryExt.Create(nil);
  ctx_StartWait('Please wait...');
  try
    ctx_DataAccess.OpenClient(AClientNbr);
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    try
      SBCH := TEvQuery.Create('SELECT DISTINCT TABLE_NAME, RECORD_NBR FROM CL_CHANGE_LOG WHERE RECORD_NBR < 0');
      SBCH.Execute;
      SBMAP := TisStringList.Create;
      while not SBCH.Result.Eof do
      begin
        TableName := SBCH.Result.FieldByName('TABLE_NAME').AsString;
        Idx := SBMAP.IndexOf(TableName);
        if Idx < 0 then
        begin
          KeyMap := TisValueMap.Create;
          SBMAP.AddObject(TableName, KeyMap);
        end
        else
        begin
          KeyMap := SBMAP.Objects[Idx] as IisValueMap;
        end;
        NewValue := ctx_DBAccess.GetGeneratorValue('G_'+TableName, dbtClient, 1);
        NegValue := SBCH.Result.FieldByName('RECORD_NBR').AsInteger;
        KeyMap.EncodeValue(NegValue, NewValue);

        SQL := Format('UPDATE CL_CHANGE_LOG SET RECORD_NBR = %d WHERE TABLE_NAME = ''%s'' AND RECORD_NBR = %d;',
                      [NewValue, TableName, NegValue]);
        Result.Add(SQL);
        ctx_DBAccess.ExecQuery(SQL, nil, nil);

        ChangedKey := TableName + '_NBR';
        SQL := Format('UPDATE %s SET %s = %d WHERE %s = %d;',
                      [TableName, ChangedKey, NewValue, ChangedKey, NegValue]);
        Result.Add(SQL);
        ctx_DBAccess.ExecQuery(SQL, nil, nil);


        for i := 0 to DD.Tables.Count - 1 do
        if (DD.Tables[i].Name <> 'PR_CHECK_LINES_DISTRIBUTED')
        and (copy(DD.Tables[i].Name,1,Length('USER_BLOCK_')) <> 'USER_BLOCK_') then
        begin
          RefTableName := DD.Tables[i].Name;
          if copy(RefTableName,1,3) <> 'SY_' then
          if copy(RefTableName,1,3) <> 'SB_' then
          if copy(RefTableName,1,4) <> 'TMP_' then
          begin
            Table := DD.Tables[i];
            n := Table.ForeignKeys.Count;
            for j := 0 to n - 1 do
            begin
              KeyTableName := DD.Tables[i].ForeignKeys[j].Table.Name;
              ForeignKey := DD.Tables[i].ForeignKeys[j].Fields[0].Field.Name;
              if KeyTableName = TableName then
              begin
                SQL := Format('UPDATE %s SET %s = %d WHERE %s = %d;',
                              [RefTableName, ForeignKey, NewValue, ForeignKey, NegValue]);
                Result.Add(SQL);
                ctx_DBAccess.ExecQuery(SQL, nil, nil);
              end;
            end;
          end;
        end;
        SBCH.Result.Next;
      end;

      if Result.Count > 0 then
        ctx_DataAccess.CommitNestedTransaction
      else
        ctx_DataAccess.RollbackNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;

  finally
    DD.Free;
    ctx_EndWait;
  end;
end;


function FixNegativeKeysSB: IisStringList;
var
  SBCH : IevQuery;
  SBMAP: IisStringList;
  Idx, NewValue, NegValue: Integer;
  KeyMap: IisValueMap;
  TableName, RefTableName, KeyTableName, SQL, ForeignKey, ChangedKey: String;
  Table: TDataDictTable;
  i, j, n: Integer;
  DD: TddsDataDictionaryExt;
begin
  Result := TIsStringList.Create;
  DD := TddsDataDictionaryExt.Create(nil);
  ctx_StartWait('Please wait...');
  try
    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      SBCH := TEvQuery.Create('SELECT DISTINCT TABLE_NAME, RECORD_NBR FROM SB_CHANGE_LOG WHERE RECORD_NBR < 0');
      SBCH.Execute;
      SBMAP := TisStringList.Create;
      while not SBCH.Result.Eof do
      begin
        TableName := SBCH.Result.FieldByName('TABLE_NAME').AsString;
        Idx := SBMAP.IndexOf(TableName);
        if Idx < 0 then
        begin
          KeyMap := TisValueMap.Create;
          SBMAP.AddObject(TableName, KeyMap);
        end
        else
        begin
          KeyMap := SBMAP.Objects[Idx] as IisValueMap;
        end;
        NewValue := ctx_DBAccess.GetGeneratorValue('G_'+ TableName, dbtBureau, 1);
        NegValue := SBCH.Result.FieldByName('RECORD_NBR').AsInteger;
        KeyMap.EncodeValue(NegValue, NewValue);

        SQL := Format('UPDATE SB_CHANGE_LOG SET RECORD_NBR = %d WHERE TABLE_NAME = ''%s'' AND RECORD_NBR = %d;',
                      [NewValue, TableName, NegValue]);
        Result.Add(SQL);
        ctx_DBAccess.ExecQuery(SQL, nil, nil);


        ChangedKey := TableName + '_NBR';
        SQL := Format('UPDATE %s SET %s = %d WHERE %s = %d;',
                      [TableName, ChangedKey, NewValue, ChangedKey, NegValue]);
        Result.Add(SQL);
        ctx_DBAccess.ExecQuery(SQL, nil, nil);


        for i := 0 to DD.Tables.Count - 1 do
        begin
          RefTableName := DD.Tables[i].Name;
          if copy(RefTableName,1,3) <> 'SY_' then
          begin
            Table := DD.Tables[i];
            n := Table.ForeignKeys.Count;
            for j := 0 to n - 1 do
            begin
              KeyTableName := DD.Tables[i].ForeignKeys[j].Table.Name;
              ForeignKey := DD.Tables[i].ForeignKeys[j].Fields[0].Field.Name;
              if KeyTableName = TableName then
              begin
                SQL := Format('UPDATE %s SET %s = %d WHERE %s = %d;',
                              [RefTableName, ForeignKey, NewValue, ForeignKey, NegValue]);
                Result.Add(SQL);
                ctx_DBAccess.ExecQuery(SQL, nil, nil);
              end;
            end;
          end;
        end;
        SBCH.Result.Next;
      end;

      if Result.Count > 0 then
        ctx_DataAccess.CommitNestedTransaction
      else
        ctx_DataAccess.RollbackNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;

  finally
    DD.Free;
    ctx_EndWait;
  end;
end;



{ TddsDataDictionaryExt }

procedure TddsDataDictionaryExt.AfterConstruction;
var
  Stream: IevDualStream;
begin
  Stream := TevDualStreamHolder.Create;
  EvoDataDictionary.SaveToStream(Stream.RealStream);
  Stream.Position := 0;
  LoadFromStream(Stream.RealStream);
  FixForeignKeys;
end;

procedure TddsDataDictionaryExt.FixForeignKeys;
var
  Table: TDataDictTable;

  procedure FixOneTable;

    procedure AddSingleFK(const ATblName, AFldName: String);
    var
      FKTbl: TDataDictTable;
      FK: TDataDictForeignKey;
      Fld: TDataDictField;
    begin
      FKTbl := Tables.TableByName(ATblName);
      Assert(Assigned(FKTbl));
      FK :=Table.ForeignKeys.AddForeignKey(FKTbl);
      Fld := Table.Fields.FieldByName(AFldName);
      Assert(Assigned(Fld));
      FK.Fields.AddKeyField(Fld);
    end;

    procedure AddFK(const EvoFKInfo: TddReferenceDesc);
    var
      i: Integer;
      FKTbl: TDataDictTable;
      FK: TDataDictForeignKey;
      Fld: TDataDictField;
    begin
      FKTbl := Tables.TableByName(EvoFKInfo.Table.GetTableName);
      Assert(Assigned(FKTbl));
      FK :=Table.ForeignKeys.AddForeignKey(FKTbl);
      for i := Low(EvoFKInfo.SrcFields) to High(EvoFKInfo.SrcFields) do
      begin
        Fld := Table.Fields.FieldByName(EvoFKInfo.SrcFields[i]);
        Assert(Assigned(Fld));
        FK.Fields.AddKeyField(Fld);
      end;
    end;

  var
    i: Integer;
    T: TddTableClass;
    RL: TddReferencesDesc;
  begin
    T := GetddTableClassByName(Table.Name);

    RL := T.GetParentsDesc;
    for i := Low(RL) to High(RL) do
      AddFK(RL[i]);

    if T = TCO then
    begin
      AddSingleFK('CO_STATES', 'HOME_CO_STATES_NBR');
      AddSingleFK('CO_STATES', 'HOME_SDI_CO_STATES_NBR');
      AddSingleFK('CO_STATES', 'HOME_SUI_CO_STATES_NBR');
    end
    else if T = TEE then
    begin
      AddSingleFK('EE_STATES', 'HOME_TAX_EE_STATES_NBR');
    end;

    if T.GetIsHistorical then
      AddSingleFK('SB_USER', 'CHANGED_BY');
  end;

var
  i: Integer;
begin
  for i := 0 to Tables.Count - 1 do
  begin
    Table := Tables[i];
    FixOneTable;
  end;
end;

end.