unit TrimHistoryFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DB, Wwdatsrc, ISBasicClasses,
   Grids, Wwdbigrd, Wwdbgrid, EvContext, isBaseClasses,
  isThreadManager, EvDataSet, EvCommonInterfaces, EvUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, Mask,
  wwdbedit, Wwdbspin, Spin, EvUIComponents, EvClientDataSet;

type
  TTrimHistoryForm = class(TForm)
    dsrDB: TevDataSource;
    dsDB: TevClientDataSet;
    dsDBCl_Nbr: TIntegerField;
    dsDBRecCount: TStringField;
    dsDBCustomNbr: TStringField;
    dsDBDB: TStringField;
    Label2: TLabel;
    lDBList: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    grDB: TevDBCheckGrid;
    btnAllDB: TButton;
    memLog: TMemo;
    edThreads: TSpinEdit;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnAllDBClick(Sender: TObject);
  private
    FThreadManager: IThreadManager;
    FFixedDB: IisStringList;
//    procedure DoUpdateDBDS(const Params: TTaskParamList);
//    procedure DoAddLogInfo(const Params: TTaskParamList);
  public
  end;

implementation

{$R *.dfm}

procedure TTrimHistoryForm.FormCreate(Sender: TObject);
begin
  FThreadManager := TThreadManager.Create('TrimHistory');
  dsDB.CreateDataSet;
  FFixedDB := TisStringList.CreateUnique(True);
end;
{
procedure TTrimHistoryForm.DoUpdateDBDS(const Params: TTaskParamList);
begin
  if Params[3]<> '' then
  begin
    dsDB.DisableControls;
    try
      dsDB.Append;
      dsDB.FieldByName('DB').AsString := Params[0];
      dsDB.FieldByName('Cl_Nbr').Value := Params[1];
      dsDB.FieldByName('CustomNbr').Value := Params[2];
      dsDB.FieldByName('RecCount').Value := Params[3];
      dsDB.Post;
    finally
      dsDB.EnableControls;
    end;
  end;
end;

procedure TTrimHistoryForm.DoAddLogInfo(const Params: TTaskParamList);
begin
  memLog.Lines.Add(Params[0]);
end;
}
procedure TTrimHistoryForm.btnAllDBClick(Sender: TObject);
var
  Q: IevQuery;
begin
  lDBList.Caption :=  'All available databases';

  dsDB.EmptyDataSet;
  ctx_StartWait('Getting client DB list');
  dsDB.DisableControls;
  try
    dsDB.Append;
    dsDB.FieldByName('DB').AsString := 'B';
    dsDB.Post;

    Q := TevQuery.Create('SELECT Cl_Nbr, Custom_Client_Number FROM Tmp_Cl');
    Q.Execute;

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      dsDB.Append;
      dsDB.FieldByName('DB').AsString := 'C';        
      dsDB.FieldByName('Cl_Nbr').AsInteger := Q.Result.Fields[0].AsInteger;
      dsDB.FieldByName('CustomNbr').AsString := Q.Result.Fields[1].AsString;
      dsDB.Post;

      Q.Result.Next;
    end;
    dsDB.First;
  finally
    ctx_EndWait;
    dsDB.EnableControls;
  end;
end;

end.
