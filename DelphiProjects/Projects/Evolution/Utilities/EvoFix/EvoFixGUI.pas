// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvoFixGUI;

interface

uses Classes, SysUtils, Forms, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvTypes, Controls, sSecurityClassDefs, SSecurityInterface, EvStreamUtils,
     SWaitingObjectForm, IsUtils, EvUtils, EvContext, EvTransportInterfaces,
     EvConsts, FixNegativeFrm, TrimHistoryFrm;

implementation

type
  TEvoFixGUI = class(TisInterfacedObject, IevEvolutionGUI, IevContextCallbacks)
  private
    FWaitDialog: TAdvancedWait;
    FMainForm: TForm;
    FGUIContext: IevContext;
  private
    procedure OnException(Sender: TObject; E: Exception);
    procedure OnShowModalDialog(Sender: TObject);
    procedure OnHideModalDialog(Sender: TObject);
  protected
    procedure DoOnConstruction; override;
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
    procedure CreateMainForm;
    procedure DestroyMainForm;
    procedure ApplySecurity(const Component: TComponent);
    function  GUIContext: IevContext;
    procedure AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure CheckTaskQueueStatus;
    procedure PrintTask(const ATaskID: TisGUID);
    procedure OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure OnWaitingServerResponse;
    procedure OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
    procedure PasswordChangeClose;
  public
    destructor  Destroy; override;
  end;

var
  vEvolutionGUI: IevEvolutionGUI;

function GetEvoFixGUI: IevEvolutionGUI;
begin
  if not Assigned(vEvolutionGUI) then
    vEvolutionGUI := TEvoFixGUI.Create as IevEvolutionGUI;
  Result := vEvolutionGUI;
end;


function GetContextCallbacks: IevContextCallbacks;
begin
  Result := GeTEvoFixGUI as IevContextCallbacks;
end;


{ TEvoFixGUI }

procedure TEvoFixGUI.ApplySecurity(const Component: TComponent);
var
  SecurityElement: ISecurityElement;
  i: Integer;
  State: TevSecElementState;
begin
  Component.GetInterface(ISecurityElement, SecurityElement);
  if Assigned(SecurityElement) and (SecurityElement.SGetTag <> '') then
  begin
    State := ctx_AccountRights.GetElementState(SecurityElement.SGetType[1], SecurityElement.SGetTag);
    SecurityElement.SSetCurrentSecurityState(State);
    SecurityElement := nil;
  end;

  for i := 0 to Component.ComponentCount-1 do
    ApplySecurity(Component.Components[i]);
end;

procedure TEvoFixGUI.AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
begin
  // plug
end;

function TEvoFixGUI.GUIContext: IevContext;
begin
  Result := FGUIContext;
end;

procedure TEvoFixGUI.CreateMainForm;
begin
  if not Assigned(FMainForm) then
  begin
    FGUIContext := Mainboard.ContextManager.GetThreadContext;
    Application.ShowMainForm := False;
    Application.CreateForm(TFixNegativeForm, FMainForm);
//    Application.CreateForm(TTrimHistoryForm, FMainForm);
    FMainForm.Show;
  end;
end;

destructor TEvoFixGUI.Destroy;
begin
  Application.OnModalBegin := nil;
  Application.OnModalEnd := nil;
  Application.OnException := nil;

  FreeandNil(FWaitDialog);
  inherited;
end;

procedure TEvoFixGUI.DestroyMainForm;
begin
  FreeAndNil(FMainForm);
  FGUIContext := nil;
end;

procedure TEvoFixGUI.DoOnConstruction;
begin
  inherited;

  Application.OnModalBegin := OnShowModalDialog;
  Application.OnModalEnd := OnHideModalDialog;
  Application.OnException := OnException;

  FWaitDialog := TAdvancedWait.Create;
end;

procedure TEvoFixGUI.EndWait;
begin
  if not UnAttended then
    FWaitDialog.EndWait;
end;

procedure TEvoFixGUI.StartWait(const AText: string; AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TEvoFixGUI.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

procedure TEvoFixGUI.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TEvoFixGUI.OnException(Sender: TObject; E: Exception);
begin
  FWaitDialog.KillWait;
end;

procedure TEvoFixGUI.OnHideModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.ShowIfHidden;
end;

procedure TEvoFixGUI.OnShowModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.Hide;
end;

procedure TEvoFixGUI.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TEvoFixGUI.OnGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TEvoFixGUI.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;

procedure TEvoFixGUI.OnWaitingServerResponse;
begin
// a plug
end;

procedure TEvoFixGUI.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TEvoFixGUI.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
//a plug
end;

procedure TEvoFixGUI.AddTaskQueue(const ATaskList: IisList;
  const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TEvoFixGUI.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

procedure TEvoFixGUI.PasswordChangeClose;
begin
// a plug
end;

initialization
  DisableProcessWindowsGhosting;
  Mainboard.ModuleRegister.RegisterModule(@GetEvoFixGUI, IevEvolutionGUI, 'Evolution GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetContextCallbacks, IevContextCallbacks, 'Context Callbacks');

end.


