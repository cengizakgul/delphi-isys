unit FixNegativeFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, FixNegative, DB, Wwdatsrc, ISBasicClasses,
   Grids, Wwdbigrd, Wwdbgrid, EvContext, isBaseClasses,
  isThreadManager, EvDataSet, EvCommonInterfaces, EvUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, Mask,
  wwdbedit, Wwdbspin, Spin, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TFixNegativeForm = class(TForm)
    dsrDB: TevDataSource;
    dsDB: TevClientDataSet;
    dsDBCl_Nbr: TIntegerField;
    dsDBRecCount: TStringField;
    dsDBCustomNbr: TStringField;
    dsDBDB: TStringField;
    Label2: TLabel;
    lDBList: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    btnFixDB: TButton;
    btnProblemDB: TButton;
    grDB: TevDBCheckGrid;
    btnAllDB: TButton;
    memLog: TMemo;
    edThreads: TSpinEdit;
    chbRebuildTT: TCheckBox;
    procedure btnFixDBClick(Sender: TObject);
    procedure btnProblemDBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAllDBClick(Sender: TObject);
  private
    FThreadManager: IThreadManager;
    FFixedDB: IisStringList;
    procedure DoCheckClientInThread(const Params: TTaskParamList);
    procedure DoCheckSBInThread(const Params: TTaskParamList);
    procedure DoFixClientInThread(const Params: TTaskParamList);
    procedure DoFixSBInThread(const Params: TTaskParamList);
    procedure DoRebuildTTInThread(const Params: TTaskParamList);
    procedure DoUpdateDBDS(const Params: TTaskParamList);
    procedure DoAddLogInfo(const Params: TTaskParamList);
    function  CheckClient(const AClNbr: Integer; const ACustNbr: String): Boolean;
    function  CheckSB: Boolean;
    function  FixClient(const AClNbr: Integer): Boolean;
    function  FixSB: Boolean;
    function  RebuildTT(const AClNbr: Integer): Boolean;
  public
  end;

implementation

{$R *.dfm}

procedure TFixNegativeForm.btnFixDBClick(Sender: TObject);

  procedure RunFix;
  var
    i: Integer;
    bRes: Boolean;
  begin
    Enabled := False;
    dsDB.DisableControls;
    ctx_StartWait('');
    try
      i := 0;
      while i < grDB.SelectedList.Count do
      begin
        ctx_UpdateWait('Fixing Databases (' + IntToStr(FThreadManager.Capacity) +' in parallel)', i, grDB.SelectedList.Count);

        dsDB.GotoBookmark(grDB.SelectedList[i]);
        if dsDB.FieldByName('DB').AsString = 'C' then
          bRes := FixClient(dsDB.FieldByName('Cl_Nbr').AsInteger)
        else
          bRes := FixSB;        

        if bRes then
          Inc(i)
        else
          Sleep(200);

        Application.ProcessMessages;
      end;

      while not FThreadManager.AllThreadsAreReady do
      begin
        Sleep(200);
        Application.ProcessMessages;
      end;

    finally
      ctx_EndWait;
      Enabled := True;
      dsDB.EnableControls;
    end;
  end;


  procedure RebuildTempTables;
  var
    i: Integer;
  begin
    Enabled := False;
    ctx_StartWait('');
    try
      i := 0;
      while i < FFixedDB.Count do
      begin
        ctx_UpdateWait('Rebuilding Temp Tables (' + IntToStr(FThreadManager.Capacity) +' in parallel)', i, FFixedDB.Count);

        if RebuildTT(StrToInt(FFixedDB[i])) then
          Inc(i)
        else
          Sleep(200);
        Application.ProcessMessages;
      end;

      while not FThreadManager.AllThreadsAreReady do
      begin
        Sleep(200);
        Application.ProcessMessages;
      end;

    finally
      ctx_EndWait;
      Enabled := True;
    end;
  end;

begin
  if grDB.SelectedList.Count = 0 then
    Exit;

  if EvMessage('We recommend to backup Databases before fixing the issue. Click OK to fix selected Databases.', mtWarning, [mbOK, mbCancel], mbCancel) <> mrOK then
    Exit;

  FThreadManager.Capacity := Round(edThreads.Value);
  memLog.Clear;
  FFixedDB.Clear;

  RunFix;

  if FFixedDB.Count > 0 then
    if chbRebuildTT.Checked then
      RebuildTempTables
    else
      EvMessage('Please do not forget rebuild temp tables for fixed databases.', mtInformation, [mbOK]);
end;


procedure TFixNegativeForm.btnProblemDBClick(Sender: TObject);
var
  Q: IevQuery;
begin
  FThreadManager.Capacity := Round(edThreads.Value);
  lDBList.Caption :=  'Problematic databases';

  dsDB.EmptyDataSet;
  Enabled := False;
  ctx_StartWait('Getting client DB list');
  try
    Q := TevQuery.Create('SELECT Cl_Nbr, Custom_Client_Number FROM Tmp_Cl');
    Q.Execute;

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      if CheckClient(Q.Result.Fields[0].AsInteger, Q.Result.Fields[1].AsString) then
      begin
        Q.Result.Next;
        ctx_UpdateWait('Analyzing Databases (' + IntToStr(FThreadManager.Capacity) +' in parallel)', Q.Result.RecNo, Q.Result.RecordCount + 1);
      end
      else
        Sleep(200);
      Application.ProcessMessages;
    end;

    while not CheckSB do
    begin
      Sleep(200);
      Application.ProcessMessages;
    end;
    ctx_UpdateWait('Analyzing Databases (' + IntToStr(FThreadManager.Capacity) +' in parallel)', Q.Result.RecNo + 1, Q.Result.RecordCount + 1);

    while not FThreadManager.AllThreadsAreReady do
    begin
      Sleep(200);
      Application.ProcessMessages;
    end;

    dsDB.First;
  finally
    ctx_EndWait;
    Enabled := True;
  end;
end;

function TFixNegativeForm.CheckClient(const AClNbr: Integer; const ACustNbr: String): Boolean;
begin
  if FThreadManager.AllThreadsAreBusy then
    Result := False
  else
  begin
    Context.RunParallelTask(DoCheckClientInThread, Self, MakeTaskParams([AClNbr, ACustNbr]),
      'Checking CL_' + IntToStr(AClNbr), tpNormal, FThreadManager);
    Result := True;
  end;
end;

procedure TFixNegativeForm.DoCheckClientInThread(const Params: TTaskParamList);
var
  Q: IevQuery;
  ClNbr: Integer;
  sRecNbr: String;
  CustNbr: String;
begin
  ClNbr := Params[0];
  CustNbr := Params[1];
  try
    ctx_DataAccess.OpenClient(ClNbr);
    Q := TevQuery.Create('SELECT Count(*) FROM Cl_Change_Log where Record_Nbr < 0');
    Q.Execute;
    if Q.Result.Fields[0].AsInteger > 0 then
      sRecNbr := Q.Result.Fields[0].AsString
    else
      sRecNbr := '';
  except
    on E: Exception do
      sRecNbr := E.Message;
  end;

  FThreadManager.RunInMainThread(Self, DoUpdateDBDS, MakeTaskParams(['C', ClNbr, CustNbr, sRecNbr]));
end;

procedure TFixNegativeForm.FormCreate(Sender: TObject);
begin
  FThreadManager := TThreadManager.Create('EvoFix');
  dsDB.CreateDataSet;
  FFixedDB := TisStringList.CreateUnique(True);
end;

procedure TFixNegativeForm.DoUpdateDBDS(const Params: TTaskParamList);
begin
  if Params[3]<> '' then
  begin
    dsDB.DisableControls;
    try
      dsDB.Append;
      dsDB.FieldByName('DB').AsString := Params[0];
      dsDB.FieldByName('Cl_Nbr').Value := Params[1];
      dsDB.FieldByName('CustomNbr').Value := Params[2];
      dsDB.FieldByName('RecCount').Value := Params[3];
      dsDB.Post;
    finally
      dsDB.EnableControls;
    end;
  end;
end;

function TFixNegativeForm.FixClient(const AClNbr: Integer): Boolean;
begin
  if FThreadManager.AllThreadsAreBusy then
    Result := False
  else
  begin
    Context.RunParallelTask(DoFixClientInThread, Self, MakeTaskParams([AClNbr]),
      'Fixing CL_' + IntToStr(AClNbr), tpNormal, FThreadManager);
    Result := True;
  end;
end;

procedure TFixNegativeForm.DoFixClientInThread(const Params: TTaskParamList);
var
  ClNbr: Integer;
  sLog: String;
  Res: IisStringList;
begin
  ClNbr := Params[0];
  try
    Res := FixNegativeKeys(ClNbr);
    if Res.Count > 0 then
    begin
      sLog := 'Fixed OK';
      FFixedDB.Lock;
      try
        FFixedDB.Add(IntToStr(ClNbr));
      finally
        FFixedDB.Unlock;
      end;
    end
    else
      sLog := 'Seems to be valid. Does not need fix.';
  except
    on E: Exception do
      sLog := E.Message;
  end;
  sLog := 'CL_' + IntToStr(ClNbr) + ':   ' + sLog;

  FThreadManager.RunInMainThread(Self, DoAddLogInfo, MakeTaskParams([sLog]));
end;

procedure TFixNegativeForm.DoAddLogInfo(const Params: TTaskParamList);
begin
  memLog.Lines.Add(Params[0]);
end;

procedure TFixNegativeForm.btnAllDBClick(Sender: TObject);
var
  Q: IevQuery;
begin
  lDBList.Caption :=  'All available databases';

  dsDB.EmptyDataSet;
  ctx_StartWait('Getting client DB list');
  dsDB.DisableControls;
  try
    dsDB.Append;
    dsDB.FieldByName('DB').AsString := 'B';
    dsDB.Post;

    Q := TevQuery.Create('SELECT Cl_Nbr, Custom_Client_Number FROM Tmp_Cl');
    Q.Execute;

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      dsDB.Append;
      dsDB.FieldByName('DB').AsString := 'C';        
      dsDB.FieldByName('Cl_Nbr').AsInteger := Q.Result.Fields[0].AsInteger;
      dsDB.FieldByName('CustomNbr').AsString := Q.Result.Fields[1].AsString;
      dsDB.Post;

      Q.Result.Next;
    end;
    dsDB.First;
  finally
    ctx_EndWait;
    dsDB.EnableControls;
  end;
end;

function TFixNegativeForm.RebuildTT(const AClNbr: Integer): Boolean;
begin
  if FThreadManager.AllThreadsAreBusy then
    Result := False
  else
  begin
    Context.RunParallelTask(DoRebuildTTInThread, Self, MakeTaskParams([AClNbr]),
      'Rebuild TT CL_' + IntToStr(AClNbr), tpNormal, FThreadManager);
    Result := True;
  end;
end;

procedure TFixNegativeForm.DoRebuildTTInThread(const Params: TTaskParamList);
var
  sLog: String;
  ClNbr: Integer;
begin
  ClNbr := Params[0];
  try
    ctx_DBAccess.RebuildTemporaryTables(ClNbr, False);
    sLog := 'Temp tables rebuilt OK';
  except
    on E: Exception do
      sLog := E.Message;
  end;
  sLog := 'CL_' + IntToStr(ClNbr) + ':   ' + sLog;

  FThreadManager.RunInMainThread(Self, DoAddLogInfo, MakeTaskParams([sLog]));
end;

function TFixNegativeForm.CheckSB: Boolean;
begin
  if FThreadManager.AllThreadsAreBusy then
    Result := False
  else
  begin
    Context.RunParallelTask(DoCheckSBInThread, Self, MakeTaskParams([0, '']),
      'Checking SB', tpNormal, FThreadManager);
    Result := True;
  end;
end;

procedure TFixNegativeForm.DoCheckSBInThread(const Params: TTaskParamList);
var
  Q: IevQuery;
  sRecNbr: String;
begin
  try
    Q := TevQuery.Create('SELECT Count(*) FROM Sb_Change_Log where Record_Nbr < 0');
    Q.Execute;
    if Q.Result.Fields[0].AsInteger > 0 then
      sRecNbr := Q.Result.Fields[0].AsString
    else
      sRecNbr := '';
  except
    on E: Exception do
      sRecNbr := E.Message;
  end;

  FThreadManager.RunInMainThread(Self, DoUpdateDBDS, MakeTaskParams(['B', Null, Null, sRecNbr]));
end;

function TFixNegativeForm.FixSB: Boolean;
begin
  if FThreadManager.AllThreadsAreBusy then
    Result := False
  else
  begin
    Context.RunParallelTask(DoFixSBInThread, Self, MakeTaskParams([]), 'Fixing SB', tpNormal, FThreadManager);
    Result := True;
  end;
end;

procedure TFixNegativeForm.DoFixSBInThread(const Params: TTaskParamList);
var
  sLog: String;
  Res: IisStringList;
begin
  try
    Res := FixNegativeKeysSB;
    if Res.Count > 0 then
      sLog := 'Fixed OK'
    else
      sLog := 'Seems to be valid. Does not need fix.';
  except
    on E: Exception do
      sLog := E.Message;
  end;
  sLog := 'SB:   ' + sLog;

  FThreadManager.RunInMainThread(Self, DoAddLogInfo, MakeTaskParams([sLog]));
end;

end.
