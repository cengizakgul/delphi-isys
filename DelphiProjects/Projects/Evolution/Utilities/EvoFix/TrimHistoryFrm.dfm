object TrimHistoryForm: TTrimHistoryForm
  Left = 421
  Top = 206
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Trim history records'
  ClientHeight = 573
  ClientWidth = 573
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 10
    Top = 18
    Width = 352
    Height = 13
    Caption = 'This utility deletes historical records from Evolution databases'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lDBList: TLabel
    Left = 10
    Top = 96
    Width = 51
    Height = 13
    Caption = 'Databases'
  end
  object Label3: TLabel
    Left = 10
    Top = 408
    Width = 73
    Height = 13
    Caption = 'Processing Log'
  end
  object Label1: TLabel
    Left = 419
    Top = 399
    Width = 72
    Height = 13
    Caption = 'Parallel threads'
  end
  object grDB: TevDBCheckGrid
    Left = 10
    Top = 115
    Width = 545
    Height = 270
    DisableThemesInTitle = False
    Selected.Strings = (
      'DB'#9'5'#9'Db'#9'F'
      'CustomNbr'#9'20'#9'Custom #'#9'F'
      'Cl_Nbr'#9'15'#9'Cl Nbr'#9'F'
      'RecCount'#9'39'#9'Additional Info'#9'F')
    IniAttributes.Enabled = False
    IniAttributes.SectionName = 'TMainForm\grDB'
    IniAttributes.Delimiter = ';;'
    IniAttributes.CheckNewFields = False
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsrDB
    MultiSelectOptions = [msoShiftSelect]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    ReadOnly = True
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    UseTFields = False
    PaintOptions.AlternatingRowColor = clCream
    DefaultSort = 'DB;CustomNbr'
  end
  object btnAllDB: TButton
    Left = 10
    Top = 59
    Width = 153
    Height = 25
    Caption = 'Show All Databases'
    TabOrder = 1
    OnClick = btnAllDBClick
  end
  object memLog: TMemo
    Left = 10
    Top = 427
    Width = 545
    Height = 110
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 2
    WantReturns = False
  end
  object edThreads: TSpinEdit
    Left = 500
    Top = 394
    Width = 55
    Height = 22
    MaxValue = 100
    MinValue = 1
    TabOrder = 3
    Value = 10
  end
  object Button1: TButton
    Left = 178
    Top = 59
    Width = 153
    Height = 25
    Caption = 'Trim History'
    TabOrder = 4
    OnClick = btnAllDBClick
  end
  object dsrDB: TevDataSource
    DataSet = dsDB
    Left = 76
    Top = 192
  end
  object dsDB: TevClientDataSet
    Left = 76
    Top = 232
    object dsDBDB: TStringField
      FieldName = 'DB'
      Size = 1
    end
    object dsDBCustomNbr: TStringField
      DisplayWidth = 40
      FieldName = 'CustomNbr'
      Size = 40
    end
    object dsDBCl_Nbr: TIntegerField
      DisplayLabel = 'CL_NBR'
      DisplayWidth = 20
      FieldName = 'Cl_Nbr'
    end
    object dsDBRecCount: TStringField
      DisplayLabel = 'Additional Info'
      DisplayWidth = 50
      FieldName = 'RecCount'
      Size = 128
    end
  end
end
