unit ImportTaxesFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, FixNegative, DB, Wwdatsrc, ISBasicClasses,
   Grids, Wwdbigrd, Wwdbgrid, EvContext, isBaseClasses,
  isThreadManager, EvDataSet, EvCommonInterfaces, EvUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, Mask,
  wwdbedit, Wwdbspin, Spin, ExtCtrls, DateUtils, EvCSVReader, EvStreamUtils, EvUIComponents, EvClientDataSet;

type
  TImportTaxesForm = class(TForm)
    evPageControl1: TevPageControl;
    evPanel1: TevPanel;
    Label2: TLabel;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    evPanel2: TevPanel;
    Label1: TLabel;
    LoadBtn: TevButton;
    FileNameEdit: TevEdit;
    SelectFileBtn: TevButton;
    evPanel3: TevPanel;
    SelectFileDlg: TOpenDialog;
    DataSource1: TDataSource;
    ExampleGrid: TevDBGrid;
    LoadedCountLabel: TLabel;
    evPanel4: TevPanel;
    ImportBtn: TevButton;
    evLabel1: TevLabel;
    AsOfDateEdit: TevDateTimePicker;
    evPanel5: TevPanel;
    LogGrid: TevDBGrid;
    DataSource2: TDataSource;
    CurrentRecordLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FileNameEditChange(Sender: TObject);
    procedure SelectFileBtnClick(Sender: TObject);
    procedure LoadBtnClick(Sender: TObject);
    procedure ImportBtnClick(Sender: TObject);
    procedure ExampleGridRowChanged(Sender: TObject);
  private
    FLoadSuccess: boolean;
    FData: IevDataSet;
    FLog: IevDataSet;
    function EnableLoadBtn: boolean;
    function LoadDataToPreview: boolean;
    procedure EnableImportElements;
    function ImportData: IisStringList;
  public
  end;

  IEvExchangeSimpleDataReader = interface(IEvExchangeDataReader)
  ['{3739AD24-201F-438A-857A-CEA41E98F53F}']
    function ReadDataToDataset(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog): IEvDataset;
  end;

  TSimpleCsvReader = class(TevCSVReader, IEvExchangeSimpleDataReader)
  private
  protected
  public
    function ReadDataToDataset(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog): IEvDataset;
  end;

implementation

{$R *.dfm}

uses EvExchangeResultLog, EvExchangeConsts, isBasicutils, SDDClasses, EvTypes, IsErrorUtils;

procedure TImportTaxesForm.FormCreate(Sender: TObject);
begin
  AsOfDateEdit.Enabled := false;
  ImportBtn.Enabled := false;
  LoadBtn.Enabled := false;
  AsOfDateEdit.Time := 0;
  AsOfDateEdit.Date := EncodeDate(YearOf(Now), 1, 1);
  evPageControl1.TabIndex := 0;
  LogGrid.Enabled := false;
  CurrentRecordLabel.Caption := '';
end;

procedure TImportTaxesForm.FileNameEditChange(Sender: TObject);
begin
  LoadBtn.Enabled := EnableLoadBtn;
end;

function TImportTaxesForm.EnableLoadBtn: boolean;
begin
  Result := (Length(FileNameEdit.text) > 0) and FileExists(FileNameEdit.Text);
end;

procedure TImportTaxesForm.SelectFileBtnClick(Sender: TObject);
begin
  if Length(FileNameEdit.text) > 0 then
    SelectFileDlg.FileName := FileNameEdit.Text;
  if SelectFileDlg.Execute then
    FileNameEdit.Text := SelectFileDlg.FileName;
end;

procedure TImportTaxesForm.LoadBtnClick(Sender: TObject);
begin
  FLoadSuccess := LoadDataToPreview;
  EnableImportElements;
end;

function TImportTaxesForm.LoadDataToPreview: boolean;
var
  InputStream : IevDualStream;
  Reader: IEvExchangeSimpleDataReader;
  Log: IEvExchangeResultLog;
  Options: IisStringList;
  i: integer;
begin
  Result := false;
  DataSource1.DataSet := nil;

  Reader := TSimpleCsvReader.Create;
  Log := TEvExchangeResultLog.Create;
  Options := TisStringList.Create;
  Options.Add(fieldNamesInFirstRow + '=1');
  Reader.SetDataReaderOptions(Options);

  InputStream := TevDualStreamHolder.CreateInMemory;
  InputStream.LoadFromFile(FileNameEdit.Text);
  FData := Reader.ReadDataToDataset(InputStream, Log);

  if Log.Count = 0 then
  begin
    DataSource1.DataSet := FData.vclDataSet;
    FData.First;
    ShowMessage('Data loaded successfully');
    Result := true;
  end
  else
  begin
    FData := TevDataSet.Create;
    FData.vclDataSet.FieldDefs.Add('Error message', ftString, 1024, false);
    FData.vclDataSet.Active := true;
    for i := 0 to Log.Count - 1 do
    begin
      FData.Append;
      FData.vclDataSet.Fields[0].Value := Copy(log.Items[i].GetText, 1, 1000);
      FData.Post;
    end;
    FData.First;
    DataSource1.DataSet := FData.vclDataSet;
    ShowMessage('Data loaded with errors. See errors in grid instead of data');
  end;
end;

procedure TImportTaxesForm.EnableImportElements;
begin
  AsOfDateEdit.Enabled := FLoadSuccess;
  ImportBtn.Enabled := FLoadSuccess;
  if FLoadSuccess then
  begin
    LoadedCountLabel.Visible := true;
    LoadedCountLabel.Caption := 'Loaded ' + IntToStr(FData.RecordCount) + ' record(s)';
  end
  else
  begin
    LoadedCountLabel.Visible := false;
  end;
end;

{ TSimpleCsvReader }

function TSimpleCsvReader.ReadDataToDataset(const AStream: IevDualStream; const AErrorsList: IEvExchangeResultLog): IEvDataset;
begin
  Result := InternalReadData(AStream, AErrorsList);
end;

procedure TImportTaxesForm.ImportBtnClick(Sender: TObject);
var
  Log: IisStringList;
  i: integer;
begin
  DataSource2.DataSet := nil;
  ctx_StartWait('Importing...');
  try
//PLYMOUTH:TODO    SetUpdateAsOfDate(AsOfDateEdit.Date);
    Log := ImportData;
  finally
    ctx_EndWait;
  end;
  if log.Count = 0 then
    ShowMessage('Data imported successfully')
  else
  begin
    Flog := TevDataSet.Create;
    Flog.vclDataSet.FieldDefs.Add('Error message', ftString, 1024, false);
    Flog.vclDataSet.Active := true;
    for i := 0 to Log.Count - 1 do
    begin
      Flog.Append;
      Flog.vclDataSet.Fields[0].Value := Copy(log[i], 1, 1024);
      Flog.Post;
    end;
    DataSource2.DataSet := Flog.vclDataSet;
    LogGrid.Enabled := true;
    ShowMessage('Data loaded with errors. See errors in grid');
  end;
end;

function TImportTaxesForm.ImportData: IisStringList;
var
  LocalsDS, FreqDS: TevClientDataSet;
  Tbl: TClass;
  i: integer;
  sFieldName: String;

  procedure CheckData;
  var
    CountriesDS, StatesDS, GlobalAgenciesDS: TevClientDataSet;
  begin
    try
      Tbl := GetClass('TSY_COUNTY');
      CheckCondition(Assigned(Tbl), 'Cannot get class: TSY_COUNTY');
      CountriesDS := TddTableClass(Tbl).Create(nil);
      CountriesDS.SkipFieldCheck := False;
      CountriesDS.ProviderName := 'SY_COUNTY_PROV';
      CountriesDS.DataRequired();

      Tbl := GetClass('TSY_STATES');
      CheckCondition(Assigned(Tbl), 'Cannot get class: TSY_STATES');
      StatesDS := TddTableClass(Tbl).Create(nil);
      StatesDS.SkipFieldCheck := False;
      StatesDS.ProviderName := 'SY_STATES_PROV';
      StatesDS.DataRequired();

      Tbl := GetClass('TSY_GLOBAL_AGENCY');
      CheckCondition(Assigned(Tbl), 'Cannot get class: TSY_GLOBAL_AGENCY');
      GlobalAgenciesDS := TddTableClass(Tbl).Create(nil);
      GlobalAgenciesDS.SkipFieldCheck := False;
      GlobalAgenciesDS.ProviderName := 'SY_GLOBAL_AGENCY_PROV';
      GlobalAgenciesDS.DataRequired();


      FData.First;
      while not FData.eof do
      begin
        if not CountriesDS.Locate('SY_COUNTY_NBR', Fdata['SY_COUNTY_NBR'], []) then
          Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Parent not found in SY_COUNTRY. SY_COUNTY_NBR=' + Fdata.FieldbyName('SY_COUNTY_NBR').AsString);

        if not GlobalAgenciesDS.Locate('SY_GLOBAL_AGENCY_NBR', Fdata['SY_LOCAL_REPORTING_AGENCY_NBR'], []) then
          Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Parent not found in SY_GLOBAL_AGENCY. SY_LOCAL_REPORTING_AGENCY_NBR =' + Fdata.FieldbyName('SY_LOCAL_REPORTING_AGENCY_NBR').AsString);

        if not GlobalAgenciesDS.Locate('SY_GLOBAL_AGENCY_NBR', Fdata['SY_LOCAL_TAX_PMT_AGENCY_NBR'], []) then
          Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Parent not found in SY_GLOBAL_AGENCY. SY_LOCAL_TAX_PMT_AGENCY_NBR =' + Fdata.FieldbyName('SY_LOCAL_TAX_PMT_AGENCY_NBR').AsString);

        if not StatesDS.Locate('SY_STATES_NBR', Fdata['SY_STATES_NBR'], []) then
          Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Parent not found in SY_STATES. SY_STATES=' + Fdata.FieldbyName('SY_STATES').AsString);

        FData.Next;
      end;
    finally
      FreeAndNil(CountriesDS);
      FreeAndNil(StatesDS);
      FreeAndNil(GlobalAgenciesDS);
    end;
  end;
begin
  Result := TisStringList.Create;

  Tbl := GetClass('TSY_LOCALS');
  CheckCondition(Assigned(Tbl), 'Cannot get class: TSY_LOCALS');
  LocalsDS := TddTableClass(Tbl).Create(nil);
  try
    LocalsDS.SkipFieldCheck := False;
    LocalsDS.ProviderName := 'SY_LOCALS_PROV';
    LocalsDS.DataRequired('1=2');


    Tbl := GetClass('TSY_LOCAL_DEPOSIT_FREQ');
    CheckCondition(Assigned(Tbl), 'Cannot get class: TSY_LOCAL_DEPOSIT_FREQ');
    FreqDS := TddTableClass(Tbl).Create(nil);
    try
      FreqDS.SkipFieldCheck := False;
      FreqDS.ProviderName := 'SY_LOCAL_DEPOSIT_FREQ_PROV';
      FreqDS.DataRequired('1=2');

      if Assigned(FData) and FData.vclDataset.Active and (FData.RecordCount > 0) then
      begin
        FData.vclDataSet.DisableControls;
        try
          CheckData;
          if Result.Count = 0 then
          begin
            FData.First;
            while not FData.eof do
            begin
              LocalsDS.Append;

              for i := 0 to FData.vclDataSet.FieldDefs.Count - 1 do
              begin
                sFieldName := AnsiUpperCase(FData.vclDataSet.FieldDefs.Items[i].Name);
                try
                  LocalsDS[sFieldName] := Fdata[sFieldName];
                except
                  on E: Exception do
                  begin
                    Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Cannot assign value. Message: ' + E.Message);
                  end;
                end;
              end;  // for

              try
                LocalsDS.Post;
                // adding freq
                try
                  FreqDS.Append;
                  FreqDS['SY_LOCALS_NBR'] := LocalsDS['SY_LOCALS_NBR'];
                  FreqDS['Change_Freq_On_Threshold'] := 'N';
                  FreqDS['Description'] := 'Quarterly';
                  FreqDS['First_Threshold_Period'] := 'N';
                  FreqDS['Frequency_Type'] := 'Q';
                  FreqDS['Hide_From_User'] := 'N';
                  FreqDS['Inc_Tax_Payment_Code'] := 'N';
                  FreqDS['Number_Of_Days_For_When_Due'] := 0;
                  FreqDS['Pay_And_Shiftback'] := 'N';
                  FreqDS['Second_Threshold_Period'] := 'N';
                  FreqDS['When_Due_Type'] := 'N';
                  FreqDS.Post;
                except
                  on EE: Exception do
                  begin
                    Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Cannot add frequence. Message: ' + EE.Message);
                    FreqDS.Cancel;
                  end;
                end;
              except
                on E: Exception do
                begin
                  Result.Add('Error in record# ' + IntToStr(FData.RecNo) + '. Cannot post data. Message: ' + E.Message);
                  LocalsDS.Cancel;
                end;
              end;

              FData.Next;
            end; // while
          end;
        finally
          FData.vclDataSet.EnableControls;
        end;

        if Result.Count = 0 then
        begin
          ctx_DataAccess.StartNestedTransaction([dbtSystem]);
          try
            ctx_DataAccess.PostDataSets([LocalsDS, FreqDS]);
            ctx_DataAccess.CommitNestedTransaction;
          except
            on E : Exception do
            begin
              Result.Add('Cannot commit transaction. Error: ' + StringReplace(E.Message, #13#10, '; ', [rfReplaceAll]));
              ctx_DataAccess.RollbackNestedTransaction;
            end;
          end;
        end;
      end
      else
        Result.Add('No data to import');
    finally
      FreeAndNil(FreqDS);
    end;
  finally
    FreeAndNil(LocalsDS);
  end;
end;

procedure TImportTaxesForm.ExampleGridRowChanged(Sender: TObject);
begin
  if FLoadSuccess and self.DataSource1.DataSet.Active then
    CurrentRecordLabel.Caption := 'Current record #' + IntToStr(self.DataSource1.DataSet.RecNo)
  else
    CurrentRecordLabel.Caption := '';
end;

end.
