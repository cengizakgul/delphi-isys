// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvoImportTaxesGUI;

interface

uses Classes, SysUtils, Forms, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvTypes, Controls, sSecurityClassDefs, SSecurityInterface, EvStreamUtils,
     SWaitingObjectForm, IsUtils, EvUtils, EvContext, EvTransportInterfaces,
     EvConsts, ImportTaxesFrm;

implementation

type
  TEvoImportTaxesGUI = class(TisInterfacedObject, IevEvolutionGUI, IevContextCallbacks)
  private
    FWaitDialog: TAdvancedWait;
    FMainForm: TForm;
    FGUIContext: IevContext;
  private
    procedure OnException(Sender: TObject; E: Exception);
    procedure OnShowModalDialog(Sender: TObject);
    procedure OnHideModalDialog(Sender: TObject);
  protected
    procedure DoOnConstruction; override;
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
    procedure CreateMainForm;
    procedure DestroyMainForm;
    procedure ApplySecurity(const Component: TComponent);
    function  GUIContext: IevContext;
    procedure AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure CheckTaskQueueStatus;
    procedure PrintTask(const ATaskID: TisGUID);
    procedure OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure OnWaitingServerResponse;
    procedure OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
    procedure PasswordChangeClose;
  public
    destructor  Destroy; override;
  end;

var
  vEvolutionGUI: IevEvolutionGUI;

function GeTEvoImportTaxesGUI: IevEvolutionGUI;
begin
  if not Assigned(vEvolutionGUI) then
    vEvolutionGUI := TEvoImportTaxesGUI.Create as IevEvolutionGUI;
  Result := vEvolutionGUI;
end;


function GetContextCallbacks: IevContextCallbacks;
begin
  Result := GeTEvoImportTaxesGUI as IevContextCallbacks;
end;


{ TEvoImportTaxesGUI }

procedure TEvoImportTaxesGUI.ApplySecurity(const Component: TComponent);
var
  SecurityElement: ISecurityElement;
  i: Integer;
  State: TevSecElementState;
begin
  Component.GetInterface(ISecurityElement, SecurityElement);
  if Assigned(SecurityElement) and (SecurityElement.SGetTag <> '') then
  begin
    State := ctx_AccountRights.GetElementState(SecurityElement.SGetType[1], SecurityElement.SGetTag);
    SecurityElement.SSetCurrentSecurityState(State);
    SecurityElement := nil;
  end;

  for i := 0 to Component.ComponentCount-1 do
    ApplySecurity(Component.Components[i]);
end;

procedure TEvoImportTaxesGUI.AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
begin
  // plug
end;

function TEvoImportTaxesGUI.GUIContext: IevContext;
begin
  Result := FGUIContext;
end;

procedure TEvoImportTaxesGUI.CreateMainForm;
begin
  if not Assigned(FMainForm) then
  begin
    FGUIContext := Mainboard.ContextManager.GetThreadContext;
    Application.ShowMainForm := False;
    Application.CreateForm(TImportTaxesForm, FMainForm);
    FMainForm.Show;
  end;
end;

destructor TEvoImportTaxesGUI.Destroy;
begin
  Application.OnModalBegin := nil;
  Application.OnModalEnd := nil;
  Application.OnException := nil;

  FreeandNil(FWaitDialog);
  inherited;
end;

procedure TEvoImportTaxesGUI.DestroyMainForm;
begin
  FreeAndNil(FMainForm);
  FGUIContext := nil;
end;

procedure TEvoImportTaxesGUI.DoOnConstruction;
begin
  inherited;

  Application.OnModalBegin := OnShowModalDialog;
  Application.OnModalEnd := OnHideModalDialog;
  Application.OnException := OnException;

  FWaitDialog := TAdvancedWait.Create;
end;

procedure TEvoImportTaxesGUI.EndWait;
begin
  if not UnAttended then
    FWaitDialog.EndWait;
end;

procedure TEvoImportTaxesGUI.StartWait(const AText: string; AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TEvoImportTaxesGUI.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  if not UnAttended then
    FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

procedure TEvoImportTaxesGUI.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TEvoImportTaxesGUI.OnException(Sender: TObject; E: Exception);
begin
  FWaitDialog.KillWait;
end;

procedure TEvoImportTaxesGUI.OnHideModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.ShowIfHidden;
end;

procedure TEvoImportTaxesGUI.OnShowModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.Hide;
end;

procedure TEvoImportTaxesGUI.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TEvoImportTaxesGUI.OnGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TEvoImportTaxesGUI.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;

procedure TEvoImportTaxesGUI.OnWaitingServerResponse;
begin
// a plug
end;

procedure TEvoImportTaxesGUI.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TEvoImportTaxesGUI.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
//a plug
end;

procedure TEvoImportTaxesGUI.AddTaskQueue(const ATaskList: IisList;
  const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TEvoImportTaxesGUI.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

procedure TEvoImportTaxesGUI.PasswordChangeClose;
begin
// a plug
end;

initialization
  DisableProcessWindowsGhosting;
  Mainboard.ModuleRegister.RegisterModule(@GeTEvoImportTaxesGUI, IevEvolutionGUI, 'Evolution GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetContextCallbacks, IevContextCallbacks, 'Context Callbacks');

end.


