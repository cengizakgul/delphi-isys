object ImportTaxesForm: TImportTaxesForm
  Left = 349
  Top = 188
  Width = 865
  Height = 717
  Caption = 'Import Local Taxes to SY LOCALS'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evPageControl1: TevPageControl
    Left = 0
    Top = 33
    Width = 849
    Height = 646
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Preview'
      object evPanel2: TevPanel
        Left = 0
        Top = 0
        Width = 849
        Height = 72
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 12
          Width = 48
          Height = 13
          Caption = 'File name:'
        end
        object LoadedCountLabel: TLabel
          Left = 184
          Top = 46
          Width = 100
          Height = 13
          Caption = 'Loaded 0 records'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object CurrentRecordLabel: TLabel
          Left = 576
          Top = 46
          Width = 54
          Height = 13
          Caption = 'Record #'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LoadBtn: TevButton
          Left = 8
          Top = 40
          Width = 137
          Height = 25
          Caption = 'Load File to Preview'
          TabOrder = 0
          OnClick = LoadBtnClick
        end
        object FileNameEdit: TevEdit
          Left = 64
          Top = 12
          Width = 425
          Height = 21
          TabOrder = 1
          OnChange = FileNameEditChange
        end
        object SelectFileBtn: TevButton
          Left = 494
          Top = 10
          Width = 20
          Height = 25
          Caption = '...'
          TabOrder = 2
          OnClick = SelectFileBtnClick
        end
      end
      object evPanel3: TevPanel
        Left = 0
        Top = 72
        Width = 849
        Height = 550
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object ExampleGrid: TevDBGrid
          Left = 0
          Top = 0
          Width = 849
          Height = 550
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TImportTaxesForm\ExampleGrid'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          OnRowChanged = ExampleGridRowChanged
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
          ParentFont = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Import'
      ImageIndex = 1
      object evPanel4: TevPanel
        Left = 0
        Top = 0
        Width = 841
        Height = 73
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 16
          Top = 16
          Width = 79
          Height = 13
          Caption = 'Import as of date'
        end
        object ImportBtn: TevButton
          Left = 16
          Top = 40
          Width = 201
          Height = 25
          Caption = 'Import taxes to system database'
          TabOrder = 0
          OnClick = ImportBtnClick
        end
        object AsOfDateEdit: TevDateTimePicker
          Left = 104
          Top = 13
          Width = 113
          Height = 21
          Date = 40638.000000000000000000
          Time = 40638.000000000000000000
          TabOrder = 1
        end
      end
      object evPanel5: TevPanel
        Left = 0
        Top = 73
        Width = 841
        Height = 545
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object LogGrid: TevDBGrid
          Left = 0
          Top = 0
          Width = 841
          Height = 545
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TImportTaxesForm\LogGrid'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DataSource2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
          ParentFont = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
  end
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 849
    Height = 33
    Align = alTop
    TabOrder = 1
    object Label2: TLabel
      Left = 10
      Top = 10
      Width = 605
      Height = 13
      Caption = 
        'This utility imports new local taxes. Import file must be comma ' +
        'delimited file with field names in the first row'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object SelectFileDlg: TOpenDialog
    Filter = 'Comma delimited files (*.csv)|*.csv|Text files (*.txt)|*.txt'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 532
    Top = 65
  end
  object DataSource1: TDataSource
    Left = 644
    Top = 81
  end
  object DataSource2: TDataSource
    Left = 684
    Top = 81
  end
end
