unit EmfParserUnitTwoPilot;

interface
uses
    Windows
  , Messages
  , SysUtils
  , Variants
  , Classes
  , Dialogs
  , StdCtrls
  , isBaseClasses
  , Graphics
  , EvStreamUtils
  , StrUtils
  , IsBasicUtils
  , emfParserUnit
  ;

type
  TEmfParserTwoPilot = class(TEmfParser)
  protected
    procedure Init; override;
  end;

implementation

function EnumEnhFuncTwoPilot(DC: HDC; HTable: PHandleTable; EMFR: PEnhMetaRecord; nObj: Integer; Sender: TObject): BOOL; stdcall;

  function GetTempDir: string;
  var
    Buffer: array[0..MAX_PATH] of Char;
  begin
    GetTempPath(SizeOf(Buffer) - 1, Buffer);
    Result := IncludeTrailingPathDelimiter(StrPas(Buffer));
  end;

var
  pFont : PEMRExtCreateFontIndirect;
  tmpFontFile, tmpFontName : String;
  FontData : IisStream;
begin
  case EMFR^.iType of
    EMR_EXTCREATEFONTINDIRECTW:
      begin
        // look for *.tmp file in user's tmp folder
        // adapted for use with Two Pilots demo Printer
        pFont := PEMRExtCreateFontIndirect(EMFR);
        tmpFontName := WideCharToString(pFont^.elfw.elfLogFont.lfFaceName);

        if AnsiEndsText('.tmp', tmpFontName) then
        begin
          tmpFontFile := GetTempDir + tmpFontName;
         // add font's name to list
          if not ((Sender as TEmfParser).FindFont(tmpFontName)) then
          begin
            ErrorIf(not FileExists(tmpFontFile), 'Cannot find file ' + tmpFontFile);
            FontData := TisStream.CreateFromFile(tmpFontFile);
           (Sender as TEmfParser).AddFont(tmpFontName, FontData);
          end;
        end;
      end;
  end;
  Result := true;
end;

{ TEmfParserTwoPilot }

procedure TEmfParserTwoPilot.Init;
begin
  inherited;
  FEnumCallBack := @EnumEnhFuncTwoPilot;
end;

end.
