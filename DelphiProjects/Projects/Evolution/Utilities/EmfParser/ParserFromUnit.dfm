object ParserForm: TParserForm
  Left = 336
  Top = 147
  BorderStyle = bsDialog
  Caption = 'Tool for creation "ismf" files'
  ClientHeight = 216
  ClientWidth = 600
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 600
    Height = 216
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Creating a new "ismf" file'
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 503
        Height = 13
        Caption = 
          'Caution: Send the ONE page of source PDF file to virtual printer' +
          ' before start processing!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 8
        Top = 24
        Width = 242
        Height = 13
        Caption = 'Start processing immediately after printing!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 8
        Top = 52
        Width = 135
        Height = 13
        Caption = 'Filename of source "emf" file'
      end
      object Label5: TLabel
        Left = 8
        Top = 84
        Width = 139
        Height = 13
        Caption = 'Filename of source "PDF" file'
      end
      object Label4: TLabel
        Left = 8
        Top = 116
        Width = 132
        Height = 13
        Caption = 'Folder for output "ismf" files:'
      end
      object PreviewCheckBox: TCheckBox
        Left = 8
        Top = 151
        Width = 185
        Height = 17
        Caption = 'Show "ismf" file after processing'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object ProcessButton: TButton
        Left = 264
        Top = 148
        Width = 75
        Height = 25
        Caption = 'Process'
        Default = True
        TabOrder = 1
        OnClick = ProcessButtonClick
      end
      object OutputPathEdit: TEdit
        Left = 152
        Top = 113
        Width = 361
        Height = 21
        TabOrder = 2
      end
      object SourcePDFPathEdit: TEdit
        Left = 152
        Top = 81
        Width = 361
        Height = 21
        TabOrder = 3
      end
      object SourcePathEdit: TEdit
        Left = 152
        Top = 49
        Width = 361
        Height = 21
        TabOrder = 4
      end
      object SearchSourceFileButton: TButton
        Left = 520
        Top = 47
        Width = 25
        Height = 25
        Caption = '...'
        TabOrder = 5
        OnClick = SearchSourceFileButtonClick
      end
      object SearchSourcePDFFileButton: TButton
        Left = 520
        Top = 79
        Width = 25
        Height = 25
        Caption = '...'
        TabOrder = 6
        OnClick = SearchSourcePDFFileButtonClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Editing existing "ismf" file'
      ImageIndex = 1
      object Label6: TLabel
        Left = 0
        Top = 12
        Width = 136
        Height = 13
        Caption = 'Filename of source "ismf" file'
      end
      object SourceISMFPathEdit: TEdit
        Left = 144
        Top = 9
        Width = 361
        Height = 21
        TabOrder = 0
      end
      object Button1: TButton
        Left = 512
        Top = 7
        Width = 25
        Height = 25
        Caption = '...'
        TabOrder = 1
        OnClick = Button1Click
      end
      object RadioGroup1: TRadioGroup
        Left = 0
        Top = 48
        Width = 545
        Height = 105
        TabOrder = 2
      end
      object RadioButton1: TRadioButton
        Left = 16
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Insert PDF'
        Checked = True
        TabOrder = 3
        TabStop = True
        OnClick = RadioButton1Click
      end
      object RadioButton2: TRadioButton
        Left = 16
        Top = 104
        Width = 385
        Height = 17
        Caption = 'View PDF which is stored inside "ismf" file'
        TabOrder = 4
        OnClick = RadioButton1Click
      end
      object PDFToInsert: TEdit
        Left = 64
        Top = 73
        Width = 361
        Height = 21
        TabOrder = 5
      end
      object PDFToInsertButton: TButton
        Left = 432
        Top = 71
        Width = 25
        Height = 25
        Caption = '...'
        TabOrder = 6
        OnClick = PDFToInsertButtonClick
      end
      object Button3: TButton
        Left = 240
        Top = 160
        Width = 75
        Height = 25
        Caption = 'Process'
        TabOrder = 7
        OnClick = Button3Click
      end
      object RadioButton3: TRadioButton
        Left = 16
        Top = 128
        Width = 233
        Height = 17
        Caption = 'Delete PDF from "ismf" file'
        TabOrder = 8
        OnClick = RadioButton1Click
      end
    end
  end
  object OpenSourceFile: TOpenDialog
    Filter = 'EMF - enchanted metafiles|*.emf'
    Left = 376
    Top = 160
  end
  object OpenSourcePDFFile: TOpenDialog
    Filter = 'PDF files|*.pdf'
    Left = 408
    Top = 160
  end
  object EditISMFDialog: TOpenDialog
    Filter = 'ISMF - iSystems enchanted metafiles|*.ismf'
    Left = 448
    Top = 160
  end
  object AddPDFDialog: TOpenDialog
    Filter = 'PDF files|*.pdf'
    Left = 488
    Top = 160
  end
end
