unit ParserFromUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IniFiles, ISBasicUtils,
  emfParserUnit,
  EmfParserUnitTwoPilot,

  EvStreamUtils,
  ComCtrls, ExtCtrls, SHellAPI, isSysUtils;

const
  ViewerFileName = 'ismfViewer.exe';

type
  TParserForm = class(TForm)
    OpenSourceFile: TOpenDialog;
    OpenSourcePDFFile: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    PreviewCheckBox: TCheckBox;
    ProcessButton: TButton;
    OutputPathEdit: TEdit;
    SourcePDFPathEdit: TEdit;
    SourcePathEdit: TEdit;
    SearchSourceFileButton: TButton;
    SearchSourcePDFFileButton: TButton;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    SourceISMFPathEdit: TEdit;
    Button1: TButton;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    PDFToInsert: TEdit;
    PDFToInsertButton: TButton;
    Button3: TButton;
    EditISMFDialog: TOpenDialog;
    AddPDFDialog: TOpenDialog;
    RadioButton3: TRadioButton;
    procedure SearchSourceFileButtonClick(Sender: TObject);
    procedure ProcessButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SearchSourcePDFFileButtonClick(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PDFToInsertButtonClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    FViewerPath: String;
    procedure SetViewerPath(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    property ViewerPath : String read FViewerPath write SetViewerPath;
  end;

var
  ParserForm: TParserForm;

implementation

{$R *.dfm}

procedure TParserForm.SearchSourceFileButtonClick(Sender: TObject);
begin
  OpenSourceFile.FileName := '';
  if DirectoryExists(SourcePathEdit.Text) then
    OpenSourceFile.InitialDir := SourcePathEdit.Text
  else
    OpenSourceFile.InitialDir := ExtractFilePath(SourcePathEdit.Text);
  if OpenSourceFile.Execute then
  begin
    SourcePathEdit.Text := OpenSourceFile.Filename;
  end;
end;

procedure TParserForm.ProcessButtonClick(Sender: TObject);
var
  tmpParser : IEmfParser;
  tmp : String;
  tmpStream : IEvDualStream;
  tmpFileName : String;
  tmpPDFStream : IEvDualStream;
begin
  if not FileExists(SourcePathEdit.Text) then
  begin
    ShowMessage('Source file "' + SourcePathEdit.Text + '" doesn''t exists!');
    AbortEx;
  end;
  if not DirectoryExists(OutputPathEdit.Text) then
  begin
    ShowMessage('Output directory "' + OutputPathEdit.Text + '" doesn''t exists!');
    AbortEx;
  end;
//    tmpParser := TEmfParser.CreateFromEmfFile(SourcePathEdit.Text);
    tmpParser := TEmfParserTwoPilot.CreateFromEmfFile(SourcePathEdit.Text);

    if Trim(SourcePDFPathEdit.Text) <> '' then
    begin
      tmpPDFStream := TEvDualStreamHolder.CreateFromFile(SourcePDFPathEdit.Text);
      tmpParser.AddPDFBackground(tmpPDFStream.RealStream);
    end;

    tmpFileName := ChangeFileExt(NormalizePath(OutputPathEdit.text) + ExtractFileName(SourcePathEdit.Text), '.' + tmpParser.GetDefaultFileExtention);
    tmpStream := TEvDualStreamHolder.CreateFromNewFile(tmpFileName);


    tmpParser.SaveToStream(tmpStream);
    tmpParser := nil;
    tmpStream := nil;
  if not PreviewCheckBox.Checked then
    ShowMessage('Done')
  else
  begin
    if (ViewerPath <> '') and FileExists(Viewerpath) then
      tmp := ViewerPath
    else
      if FileExists(NormalizePath(ExtractFilePath(Application.ExeName)) + ViewerFileName) then
        tmp := NormalizePath(ExtractFilePath(Application.ExeName)) + ViewerFileName
      else
      begin
        ShowMessage('Done!' +#13#10 + 'Cannot find viewer! Please provide path to it using "ViewerPath" parameter in program''s ini-file');
        AbortEx;
      end;
    Sleep(2000);
    WinExec(PChar(tmp + ' ' + tmpFileName), 0);
  end;
end;

procedure TParserForm.FormCreate(Sender: TObject);
var
  tmpIni : TIniFile;
begin
  PageControl1.TabIndex := 0;
  // read settings
  tmpIni := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    SourcePathEdit.Text := tmpIni.ReadString('General', 'SourcePath', '');
    OpenSourceFile.InitialDir := SourcePathEdit.Text;
    SourcePDFPathEdit.Text := tmpIni.ReadString('General', 'SourcePDFPath', '');
    OpenSourcePDFFile.InitialDir := SourcePDFPathEdit.Text;
    OutputPathEdit.Text := tmpIni.ReadString('General', 'OutputPath', '');
    ViewerPath := tmpIni.ReadString('General', 'ViewerPath', '');
  finally
    tmpIni.Free;
  end;
end;

procedure TParserForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  tmpIni : TIniFile;
begin
  try
    tmpIni := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
    try
      if DirectoryExists(SourcePathEdit.Text) then
        tmpIni.WriteString('General', 'SourcePath', SourcePathEdit.Text)
      else
        tmpIni.WriteString('General', 'SourcePath', ExtractFilePath(SourcePathEdit.Text));
      if DirectoryExists(SourcePDFPathEdit.Text) then
        tmpIni.WriteString('General', 'SourcePDFPath', SourcePDFPathEdit.Text)
      else
        tmpIni.WriteString('General', 'SourcePDFPath', ExtractFilePath(SourcePDFPathEdit.Text));
      tmpIni.WriteString('General', 'OutputPath', OutputPathEdit.Text);
    finally
      tmpIni.Free;
    end;
  except
    on E:Exception do
    begin
      ShowMessage('Error: ' + E.Message);
    end;
  end;
end;

procedure TParserForm.SetViewerPath(const Value: String);
begin
  FViewerPath := Value;
end;

procedure TParserForm.FormActivate(Sender: TObject);
begin
  if SourcePathEdit.text <> '' then
    SearchSourceFileButton.SetFocus;
end;

procedure TParserForm.SearchSourcePDFFileButtonClick(Sender: TObject);
begin
  OpenSourcePDFFile.FileName := '';
  if DirectoryExists(SourcePDFPathEdit.Text) then
    OpenSourcePDFFile.InitialDir := SourcePDFPathEdit.Text
  else
    OpenSourcePDFFile.InitialDir := ExtractFilePath(SourcePDFPathEdit.Text);
  if OpenSourcePDFFile.Execute then
  begin
    SourcePDFPathEdit.Text := OpenSourcePDFFile.Filename;
  end;
end;

procedure TParserForm.RadioButton1Click(Sender: TObject);
begin
  PDFToInsertButton.Enabled := RadioButton1.Checked;
  PDFToInsert.Enabled := RadioButton1.Checked;
end;

procedure TParserForm.Button1Click(Sender: TObject);
begin
  EditISMFDialog.FileName := '';
  if DirectoryExists(SourceISMFPathEdit.Text) then
    EditISMFDialog.InitialDir := SourceISMFPathEdit.Text
  else
    EditISMFDialog.InitialDir := ExtractFilePath(SourceISMFPathEdit.Text);
  if EditISMFDialog.Execute then
  begin
    SourceISMFPathEdit.Text := EditISMFDialog.Filename;
  end;
end;

procedure TParserForm.PDFToInsertButtonClick(Sender: TObject);
begin
  AddPDFDialog.FileName := '';
  if DirectoryExists(PDFToInsert.Text) then
    AddPDFDialog.InitialDir := PDFToInsert.Text
  else
    AddPDFDialog.InitialDir := ExtractFilePath(PDFToInsert.Text);
  if AddPDFDialog.Execute then
  begin
    PDFToInsert.Text := AddPDFDialog.Filename;
  end;
end;

procedure TParserForm.Button3Click(Sender: TObject);
var
  tmpParser : IEmfParser;
  tmpStream : IEvDualStream;
  tmpPDFStream : IEvDualStream;
  tmpFileName : String;
begin
  if not FileExists(SourceISMFPathEdit.text) then
  begin
    Application.MessageBox('Source "ismf" file doesn''t exists!', 'Error', MB_OK);
    AbortEx;
  end;

  if RadioButton1.Checked then
  begin
    if not FileExists(PDFToInsert.text) then
      Application.MessageBox('Source PDF file doesn''t exists!', 'Error', MB_OK)
    else
    begin
      tmpParser := TEmfParserTwoPilot.CreateFromFile(SourceISMFPathEdit.text);
      tmpPDFStream := TEvDualStreamHolder.CreateFromFile(PDFToInsert.text);
      tmpParser.AddPDFBackground(tmpPDFStream.RealStream);
      tmpStream := TEvDualStreamHolder.CreateFromNewFile(SourceISMFPathEdit.text);
      tmpParser.SaveToStream(tmpStream);
      Application.MessageBox('Done', 'Information', MB_OK);
    end;
    Exit;
  end;

  if RadioButton2.Checked then
  begin
    tmpParser := TEmfParserTwoPilot.CreateFromFile(SourceISMFPathEdit.text);
    tmpFileName := ExtractFilePath(ParamStr(0)) + 'tmpEmpParser.pdf';
    tmpStream := TEvDualStreamHolder.CreateFromNewFile(tmpFileName);
    if not tmpParser.PDFExists then
      Application.MessageBox('This ismf file contains no PDF', 'Information', MB_OK)
    else
    begin
      tmpParser.GetPDFBackground(tmpStream.RealStream);
      tmpStream := nil;
      ShellExecute(Handle, PChar('open'), PChar(tmpFIleName), nil, nil, SW_SHOWMAXIMIZED);
    end;
    Exit;
  end;

  if RadioButton3.Checked then
  begin
    tmpParser := TEmfParserTwoPilot.CreateFromFile(SourceISMFPathEdit.text);
    tmpParser.DeletePDFBackground;
    tmpStream := TEvDualStreamHolder.CreateFromNewFile(SourceISMFPathEdit.text);
    tmpParser.SaveToStream(tmpStream);
    Application.MessageBox('Done', 'Information', MB_OK);
    Exit;
  end;
end;

end.
