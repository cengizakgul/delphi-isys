unit Postprocess;

interface

//uses Classes, SysUtils, MSXML2_TLB;
uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, MSXML2_TLB, EvStreamUtils
  ,ExtCtrls
  ,XMLDoc
  ;

type
  TCustomPostprocess = class(TObject)
  private
    FFileName: String;
  protected
    procedure DoRun; virtual; abstract;
  public
    constructor Create(const AFileName: String); virtual;
    procedure Run; virtual; abstract;
    class function FormName: String; virtual; abstract;
    class function DialogFilter: String; virtual; abstract;
  end;

  TCustomPostprocessClass = class of TCustomPostprocess;

  TCustomFileStreamPostprocess = class(TCustomPostprocess)
  private
    FFileStream : TFileStream;
  protected
    property FileStream : TFileStream read FFileStream;
    procedure DoRun; override; abstract;
  public
    constructor Create(const AFileName: String); override;
    procedure Run; override;
    class function FormName: String; override; abstract;
    class function DialogFilter: String; override; abstract;
  end;

  TCustomXMLPostprocess = class(TCustomPostprocess)
  private
    FXMLDoc: IXMLDOMDocument2;
  protected
    property  XMLDoc: IXMLDOMDocument2 read FXMLDoc;
    procedure DoRun; override; abstract;
    procedure XMLSave; virtual;
  public
    constructor Create(const AFileName: String); override;
    procedure Run; override;
    class function FormName: String; override; abstract;
    class function DialogFilter: String; override;
  end;

//  TCustomXMLPostprocessClass = class of TCustomXMLPostprocess;

  // New Mexico

  TppNM_Custom = class(TCustomXMLPostprocess)
  protected
    FReportNameSpace: String;
    procedure DoRun; override;
  end;


  TppNM_ES903 = class(TppNM_Custom)
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
  end;


  TppNM_CRS1 = class(TppNM_Custom)
  private
  protected
    procedure RemoveUpToSingleNamespace(ns1: string); virtual;
    procedure DoRun; override;
    procedure XMLSave; override;
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
  end;

  // The Hartford XML Report

  TppHartfordXMLReport = class(TCustomXMLPostprocess)
  private
  protected
    procedure DoRun; override;
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
  end;

  // HI Quarterly Wage Magmedia

  TppHIQuarterlyWageMagmedia = class(TCustomFileStreamPostprocess)
  private
  protected
    procedure DoRun; override;
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
    class function DialogFilter: String; override;
  end;

  TppHISuiContributionMagmedia = class(TCustomFileStreamPostprocess)
  private
  protected
    procedure DoRun; override;
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
    class function DialogFilter: String; override;
  end;

  TppS421ICESAMagmedia = class(TCustomFileStreamPostprocess)
  private
  protected
    procedure DoRun; override;
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
    class function DialogFilter: String; override;
  end;

  // TRD_31109_XML

//  TppTrd31109XMLReport = class(TCustomXMLPostprocess)
  TppTrd31109XMLReport = class(TppNM_CRS1)
  private
  //protected
  //  procedure DoRun; override;
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
  end;

  TppNM_WC1_XML_Report = class(TppNM_CRS1)
  private
  public
    class function FormName: String; override;
    constructor Create(const AFileName: String); override;
  end;


 var
  // Reports list;
  ReportsList: TStringList;

  // filling the Reports list
  procedure FillReportList;

  // command line processing
  procedure MainBackgroundLoop;

  procedure RemoveAllCarriageReturns(const AFileStream : TStream);

implementation

{ TCustomXMLPostprocess }

constructor TCustomXMLPostprocess.Create(const AFileName: String);
begin
  FFileName := AFileName;
end;

class function TCustomXMLPostprocess.DialogFilter: String;
begin
  result := 'XML Files (*.xml)|*.xml';
end;

procedure TCustomXMLPostprocess.Run;
begin
  try
    try
      FXMLDoc := CoDOMDocument60.Create;
    except
      on E: Exception do
      begin
        E.Message := 'Library MSXML 6.0 or higher is required' + #13#13 + E.Message;
        raise;
      end;
    end;

    FXMLDoc.validateOnParse := False;
    FXMLDoc.Load(FFileName);

    DoRun;

    //xmlDoc.save(FFileName);
    XMLSave;
  finally
    FXMLDoc := nil;
  end;
end;

procedure TCustomXMLPostprocess.XMLSave;
begin
   xmlDoc.save(FFileName);
end;

{ TppNM_ES903 }


constructor TppNM_ES903.Create(const AFileName: String);
begin
  inherited;
  FReportNameSpace := 'https://ec3.state.nm.us/NMWebFile/Schemas/DOL_903_1.0';
end;

class function TppNM_ES903.FormName: String;
begin
  Result := 'NM ES903';
end;


{ TppNM_CRS1 }

constructor TppNM_CRS1.Create(const AFileName: String);
begin
  inherited;
  //FReportNameSpace := 'https://ec3.state.nm.us/NMWebFile/Schemas/CRS_Return_1.1';
  FReportNameSpace := 'https://tap.state.nm.us/WebFiles/CombinedReportingSystem_2.0';
end;

procedure TppNM_CRS1.RemoveUpToSingleNamespace(ns1:string);
var
  i: Integer;
  s: String;
  aStr: TStringList;
  j: Integer;
  jStart: Integer;
  jLength: Integer;
begin

  // remove all namespaces except    ns1 = 'https://tap.state.nm.us/WebFiles/CombinedReportingSystem_2.0';

  aStr := TStringList.Create;
  try
    for i := 0 to XMLDoc.namespaces.length-1 do
    begin
       aStr.Add(( XMLDoc.namespaces[i]));//.namespaceURI[i]));
    end;


    if (aStr.Count = 1)
       and
       (CompareText(aStr[0], ns1)=0)
    then
       raise Exception.Create('The file "' + FFileName + '" is already patched');


    j := aStr.IndexOf(ns1);

    if j >= 0 then
    begin
      s := XMLDoc.xml;
      for i:= 0 to aStr.Count - 1 do
      begin
         if i <> j then
         begin
            jStart := Pos(  Astr[i]+'"',  s);
            if jStart > 0 then
            begin
              jLength := Length(Astr[i])+1;
              while s[jStart] <> ' ' do
              begin
                 jStart := jStart - 1;
                 jLength := jLength + 1;
              end;
              Delete(s,jStart, jLength);
            end;
         end;
      end;
      XMLDoc.loadXML(s);
    end;
  finally
    aStr.Free;
  end;
end;


procedure TppNM_CRS1.DoRun;
begin
  RemoveUpToSingleNamespace(FReportNameSpace);
end;

class function TppNM_CRS1.FormName: String;
begin
  Result := 'NM CRS1';
end;


procedure TppNM_CRS1.XMLSave;
var
 s:string;
 p:TPanel;
 x: TXmlDocument;
begin
  //inherited;
  s := xmlDoc.xml;
  p := TPanel.Create(nil);
  try
    x:=TXMLDocument.Create(p);
    x.xml.Text := s;
    x.Active := true;
    x.Encoding := 'utf-8';
    x.SaveToFile(FFileName);
  finally
    p.Free;
  end;
end;

{ TppNM_Custom }

procedure TppNM_Custom.DoRun;
const ns1 = 'https://ec3.state.nm.us/NMWebFile/Schemas/CompleteFile';
      ns2 = 'https://ec3.state.nm.us/NMWebFile/Schemas/FilerInfo';
      MACRO_xmlns = 'MACRO_xmlns';
var
  nList: IXMLDOMNodeList;
  N: IXMLDOMNode;
  i: Integer;
  s: String;
begin
  if not (AnsiSameText(XMLDoc.namespaces[0], ns1) or AnsiSameText(XMLDoc.namespaces[1], ns1)) then
    raise Exception.Create('This file is not "' + FormName + '" compatible');

  xmlDoc.setProperty('SelectionLanguage', 'XPath');

  XMLDoc.setProperty('SelectionNamespaces', 'xmlns:na="' + ns2 + '"');
  N := XMLDoc.documentElement.selectSingleNode('./na:Filer_Information');
  if Assigned(N) then
    raise Exception.Create('The file "' + FFileName + '" is already patched');

  XMLDoc.setProperty('SelectionNamespaces', 'xmlns:na="' + ns1 + '"');

  N := XMLDoc.documentElement.selectSingleNode('./na:Filer_Information');
  if not Assigned(N) then
    raise Exception.Create('Element "Filer_Information" is not found');

  (N as IXMLDOMElement).setAttribute(MACRO_xmlns, ns2);

  N := XMLDoc.documentElement.selectSingleNode('./na:Reports');
  if not Assigned(N) then
    raise Exception.Create('Element "Reports" is not found');
  nList := (N as IXMLDOMElement).selectNodes('./na:Report');

  if not Assigned(nList) or (nList.Length = 0) then
    raise Exception.Create('Element "Report" is not found');

  for i := 0  to nList.Length - 1 do
    (nList[i] as IXMLDOMElement).setAttribute(MACRO_xmlns, FReportNameSpace);

  s := XMLDoc.xml;
  s := StringReplace(s, MACRO_xmlns, 'xmlns', [rfReplaceAll]);
  XMLDoc.loadXML(s);
end;


{ TppHartfordXMLReport }

constructor TppHartfordXMLReport.Create(const AFileName: String);
begin
  inherited;
end;

procedure TppHartfordXMLReport.DoRun;
const
  text1 = 'xmlns="PACSelfServiceDataSet"';
  text2 = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"';
  text3 = '</PayrollData>';
  text4 = '<PayrollData>';
var
  S : String;
  tmpList : TStringList;
  i : integer;
begin
  s := XMLDoc.xml;
  if (Pos(text1, s) = 0) or (Pos(text2, s) = 0) then
    raise Exception.Create('This file is not "' + FormName + '" compatible');
  s := StringReplace(s, text1, ' ', [rfReplaceAll]);
  s := StringReplace(s, text2, ' ', [rfReplaceAll]);
  s := StringReplace(s, 'xmlns:', ' ', [rfReplaceAll]);

  // deleting </PayrollData><PayrollData>
  tmpList := TStringList.Create;
  tmpList.CaseSensitive := false;
  try
    tmpList.Text := S;
    i := tmpList.Count - 1;
    while i >= 0 do
    begin
      if Trim(tmpList[i]) = text4 then
        if i > 0 then
          if Trim(tmpList[i - 1]) = text3 then
          begin
            tmpList.Delete(i);
            tmpList.Delete(i - 1);
            Dec(i);
          end;
      Dec(i);
    end;
    S := tmpList.Text;
  finally
    tmpList.Free;
  end;

  XMLDoc.loadXML(s);
end;

class function TppHartfordXMLReport.FormName: String;
begin
  Result := 'Hartford XML Report';
end;

function ParamExists(MyParam: String; var ParamNumber : integer): Boolean;
var
  I: Integer;
begin
  ParamNumber := -1;
  Result := False;
  for I := 1 to ParamCount do
    if UpperCase(ParamStr(I)) = UpperCase(MyParam) then
    begin
      Result := True;
      ParamNumber := i;
      Break;
    end;
end;

// command line processing
procedure MainBackgroundLoop;
const
  CommandLineHelp = 'Wrong number of format of command line parameters!' + #13#10 +
    'First parameter should be number of report, for example: 1' + #13#10 +
    'The second parameter should be mask or filename for input files' + #13#10+
    'The third parameter is switch not to show messages: /mute, it''s not required' + #13#10+
    'Example with messages: CorrectXML.exe 1 file1.xml' + #13#10 +
    'or example without messages: CorrectXML.exe 1 file1.xml /mute';
var
  ReportNumber : integer;
  ReportNumberString : String;
  FileName : String;

  ParamNumber : integer;

  Sr: TSearchRec;
  FilesCount : integer;
  FilesPath : String;
  Processor: TCustomPostprocess;
  FlagNoMessages : boolean;

  procedure WeHaveError (MyMessage : String; MyErrorCode : integer);
  begin
    if not FlagNoMessages then
       ShowMessage(MyMessage);
    Halt(MyErrorCode);
  end;

begin
  FlagNoMessages := false;
  ReportNumber := -1;
  ReportNumberString := '-1';
  FileName := '';



  if ParamExists('/Mute', ParamNumber) then  // don't show any error messages
  begin
    FlagNoMessages := true;
    if ParamCount <> 3 then  // user must provide 3 parameters: the first - '/Mute', the second - number of report (for example: 3), the third: name of mask for input files
      WeHaveError(CommandLineHelp, 1);
    Case ParamNumber of
      1 : begin ReportNumberString := ParamStr(2); FileName := ParamStr(3); end;
      2 : begin ReportNumberString := ParamStr(1); FileName := ParamStr(3); end;
      3 : begin ReportNumberString := ParamStr(1); FileName := ParamStr(2); end;
    end;
  end
  else  // show error messages to user
  begin
    if ParamCount <> 2 then  // user must provide 2 parameters: the first - number of report (for example: 3), the second: name of mask for input files
      WeHaveError(CommandLineHelp, 1)
    else
    begin
      ReportNumberString := ParamStr(1);
      FileName := ParamStr(2);
    end;
  end;


  try
    ReportNumber := StrToInt(ReportNumberString);
    if ReportNumber <= 0 then raise Exception.Create('');
  except
    on E:Exception do
      WeHaveError(CommandLineHelp, 1);
  end;

   // checking for report in list
  if ReportNumber > ReportsList.Count then
    WeHaveError('Report #' + IntToStr(ReportNumber) + ' not found in the reports list!', 1);

  // filenames loop
  FilesCount := 0;
  FilesPath := ExtractFilePath(FileName);
  if FindFirst(FileName, 0, sr) = 0 then
  begin
    repeat
      Inc(FilesCount);
      Processor := (TCustomPostprocessClass(ReportsList.Objects[ReportNumber-1])).Create(FilesPath + Sr.Name);
      try
        try
           Processor.Run;
        except
          on E:Exception do
            WeHaveError('Error! ' + E.Message + #13#10 + 'Processing will be stopped.', 1);
        end;
      finally
        Processor.Free;
      end;
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
  if FilesCount = 0 then
    WeHaveError('No files were processed!', 1);
end;

// filling the Reports list
procedure FillReportList;
begin
  ReportsList := TStringList.Create;
  ReportsList.AddObject('1 - ' + TppNM_ES903.FormName, Pointer(TppNM_ES903));
  ReportsList.AddObject('2 - ' + TppNM_CRS1.FormName, Pointer(TppNM_CRS1));
  ReportsList.AddObject('3 - ' + TppHartfordXMLReport.FormName, Pointer(TppHartfordXMLReport));
  ReportsList.AddObject('4 - ' + TppHIQuarterlyWageMagmedia.FormName, Pointer(TppHIQuarterlyWageMagmedia));
  ReportsList.AddObject('5 - ' + TppHISuiContributionMagmedia.FormName, Pointer(TppHISuiContributionMagmedia));
  ReportsList.AddObject('6 - ' + TppS421ICESAMagmedia.FormName, Pointer(TppS421ICESAMagmedia));
  ReportsList.AddObject('7 - ' + TppTrd31109XMLReport.FormName, Pointer(TppTrd31109XMLReport));
  ReportsList.AddObject('8 - ' + TppNM_WC1_XML_Report.FormName, Pointer(TppNM_WC1_XML_Report));

end;

{ TppHIQuarterlyWageMagmedia }

constructor TppHIQuarterlyWageMagmedia.Create(const AFileName: String);
begin
  inherited;
end;

class function TppHIQuarterlyWageMagmedia.DialogFilter: String;
begin
  result := 'All Files (*.*)|*.*';
end;

procedure TppHIQuarterlyWageMagmedia.DoRun;
var
  Buffer : array [1..2] of char;
  Size : integer;
begin
  Size := FFileStream.Size;
  FFileStream.Position := Size - 2;
  FFileStream.Read(Buffer, 2);
  if (Size < 2) or (Buffer[1] <> #13) or (Buffer[2] <> #10) then
    raise Exception.Create('This file is not "' + FormName + '" compatible');
  FFileStream.Size := Size - 2;
end;

class function TppHIQuarterlyWageMagmedia.FormName: String;
begin
  Result := 'HI Quarterly Wage Magmedia';
end;

{ TCustomBinaryPostprocess }

constructor TCustomFileStreamPostprocess.Create(const AFileName: String);
begin
  FFileName := AFileName;
end;

procedure TCustomFileStreamPostprocess.Run;
begin
  FFileStream := TFileStream.Create(FFileName, fmOpenReadWrite);
  try
    DoRun;
  finally
    FFileStream.Free;
  end;
end;

{ TCustomPostprocess }

constructor TCustomPostprocess.Create(const AFileName: String);
begin
  FFileName := AFileName;
end;

{ TppHISuiContributionMagmedia }

constructor TppHISuiContributionMagmedia.Create(const AFileName: String);
begin
  inherited;

end;

class function TppHISuiContributionMagmedia.DialogFilter: String;
begin
  result := 'All Files (*.*)|*.*';
end;

procedure TppHISuiContributionMagmedia.DoRun;
begin
  if (FFileStream.Size mod 176) > 0 then
    raise Exception.Create('This file is not "' + FormName + '" compatible');

  RemoveAllCarriageReturns(FFileStream);
end;

class function TppHISuiContributionMagmedia.FormName: String;
begin
  Result := 'S1625 - HI SUI Contribution Magmedia';
end;

procedure RemoveAllCarriageReturns(const AFileStream : TStream);
var
  Buffer : array [1..2] of char;
  amount : integer;
  tmpStream : IisStream;
  BreakFlag : boolean;
begin
  Buffer[2] := #13;
  tmpStream := TevDualStreamHolder.Create;
  AFileStream.Position := 0;
  BreakFlag := false;
  amount := AFileStream.Read(Buffer, 1);
  while amount > 0 do
  begin
    if not BreakFlag then
      if Buffer[1] = #13 then
        BreakFlag := true
      else
        tmpStream.WriteBuffer(Buffer, 1)
    else
      if Buffer[1] = #10 then
        BreakFlag := false
      else
      begin
        BreakFlag := false;
        tmpStream.WriteBuffer(Buffer[2], 1);
        tmpStream.WriteBuffer(Buffer, 1);
      end;
    amount := AFileStream.Read(Buffer, 1);
  end;

  AFileStream.Size := 0;
  tmpStream.Position := 0;
  AFileStream.CopyFrom(tmpStream.RealStream, tmpStream.Size);
end;

{ TppS421ICESAMagmedia }

constructor TppS421ICESAMagmedia.Create(const AFileName: String);
begin
  inherited;

end;

class function TppS421ICESAMagmedia.DialogFilter: String;
begin
  result := 'All Files (*.*)|*.*';
end;

procedure TppS421ICESAMagmedia.DoRun;
begin
  if (FFileStream.Size mod 278) > 0 then
    raise Exception.Create('This file is not "' + FormName + '" compatible');

  RemoveAllCarriageReturns(FFileStream);
end;

class function TppS421ICESAMagmedia.FormName: String;
begin
  Result := 'S421 ICESA Magmedia';
end;

{ TppTrd31109XMLReport }

constructor TppTrd31109XMLReport.Create(const AFileName: String);
begin
  inherited;
  FReportNameSpace := 'https://tap.state.nm.us/WebFiles/CombinedReporting31109_2.0';
end;

{
procedure TppTrd31109XMLReport.DoRun;
const
  Template1From = '<Filer_Information>';
  Template1To   = '<Filer_Information xmlns="https://ec3.state.nm.us/NMWebFile/Schemas/FilerInfo">';
  Template2From = '<Report ReportName="TRD_31109_XML" Version="1.0">';
  Template2To   = '<Report ReportName="TRD_31109_XML" Version="1.0" xmlns="https://ec3.state.nm.us/NMWebFile/Schemas/TRD_31109_1.0">';
var
  s: String;
begin
  s := XMLDoc.xml;
  if (Pos(Template2From, s) = 0) or (Pos(Template2From, s) = 0) then
    raise Exception.Create('This file is not "' + FormName + '" compatible');
  s := StringReplace(s, Template1From, Template1To, [rfReplaceAll]);
  s := StringReplace(s, Template2From, Template2To, [rfReplaceAll]);

  XMLDoc.loadXML(s);
end;
}

class function TppTrd31109XMLReport.FormName: String;
begin
  Result := 'TRD 31109 XML';
end;

{ TppNM_WC1_XML_Report }

constructor TppNM_WC1_XML_Report.Create(const AFileName: String);
begin
  inherited;
  FReportNameSpace := 'https://tap.state.nm.us/WebFiles/WorkersComp_2.0';
end;

class function TppNM_WC1_XML_Report.FormName: String;
begin
   Result := 'NM WC1 XML';
end;

end.
