object CorrectNMXML: TCorrectNMXML
  Left = 458
  Top = 242
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'XML Postprocess'
  ClientHeight = 98
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 16
    Width = 59
    Height = 13
    Caption = 'Report Type'
  end
  object Button1: TButton
    Left = 103
    Top = 52
    Width = 137
    Height = 25
    Caption = 'Run Postprocess'
    TabOrder = 0
    OnClick = Button1Click
  end
  object cbReportType: TComboBox
    Left = 91
    Top = 13
    Width = 241
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object OpenDialog: TOpenDialog
    Filter = 'XML Files (*.xml)|*.xml'
    Title = 'Select File'
    Left = 48
    Top = 40
  end
end
