unit CorrectNMXMLFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TCorrectNMXML = class(TForm)
    Button1: TButton;
    cbReportType: TComboBox;
    Label1: TLabel;
    OpenDialog: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  CorrectNMXML: TCorrectNMXML;

implementation

{$R *.dfm}

uses Postprocess;

procedure TCorrectNMXML.Button1Click(Sender: TObject);
var
  Processor: TCustomPostprocess;
begin
  // have to analize file extention because there's not only xml reports now :(
  OpenDialog.Filter := TCustomPostprocessClass(cbReportType.Items.Objects[cbReportType.ItemIndex]).DialogFilter;
  if OpenDialog.Execute then
  begin
    Processor := (TCustomPostprocessClass(cbReportType.Items.Objects[cbReportType.ItemIndex])).Create(OpenDialog.FileName);
    Screen.Cursor := crHourGlass;
    try
      Update;
      Processor.Run;
      ShowMessage('File has been postprocessed successfuly!');
    finally
      Screen.Cursor := crDefault;
      Processor.Free;
    end;
  end;
end;

procedure TCorrectNMXML.FormCreate(Sender: TObject);
var
   i : integer;
begin
  cbReportType.Clear;

  for i := 0 to ReportsList.Count-1 do
    cbReportType.Items.AddObject(ReportsList.Strings[i], ReportsList.Objects[i]);

  cbReportType.ItemIndex := 0;
end;

end.
