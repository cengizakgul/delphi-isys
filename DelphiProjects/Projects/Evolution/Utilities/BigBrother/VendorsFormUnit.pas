unit VendorsFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, wwSpeedButton, wwDBNavigator, wwclearpanel,
  Grids, Wwdbigrd, Wwdbgrid, DBCtrls, SBLicenseWriterUnit, DBGrids;

type
  TVendorsForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    SaveButton: TButton;
    CancelButton: TButton;
    Panel3: TPanel;
    DBNavigator1: TDBNavigator;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
  private
    { Private declarations }
    Writer : TSBLicenseWriter;
  public
    { Public declarations }
    procedure ShowVendors;
  end;

var
  VendorsForm: TVendorsForm;

implementation

{$R *.dfm}

uses BBConfigDataUnit;

{ TVendorsForm }

procedure TVendorsForm.ShowVendors;
begin
  if BBConfigData.EditIsDisabled then
  begin
    DBGrid1.ReadOnly := true;
    SaveButton.Enabled := false;
    BBConfigData.IBVendor.UpdateObject := nil;
  end
  else
  begin
    DBGrid1.ReadOnly := false;
    SaveButton.Enabled := true;
    BBConfigData.IBVendor.UpdateObject := BBConfigData.IBVendorUpdate;
  end;
  BBConfigData.IBManualTransactions.StartTransaction;
  BBConfigData.IBVendor.Active := false;
  BBConfigData.IBVendor.Active := true;
  try
    ShowModal;
    if Modalresult <> mrOk then
      try
        BBConfigData.IBVendor.CancelUpdates;
        BBConfigData.IBManualTransactions.Rollback;
      except
        on E:Exception do
        begin
          BBConfigData.IBVendor.CancelUpdates;
          BBConfigData.IBManualTransactions.Rollback;
        end;
      end;
    BBConfigData.IBVendor.Active := false;
  finally
    if BBConfigData.IBManualTransactions.Active then
       BBConfigData.IBManualTransactions.Rollback;
  end;
end;

procedure TVendorsForm.FormCreate(Sender: TObject);
begin
  Writer := TSBLicenseWriter.Create(BBConfigData.IBDatabase, nil);
end;

procedure TVendorsForm.FormDestroy(Sender: TObject);
begin
  Writer.Free;
end;

procedure TVendorsForm.SaveButtonClick(Sender: TObject);
begin
  try
    BBConfigData.IBVendor.ApplyUpdates;
    BBConfigData.IBManualTransactions.Commit;
    Writer.PostDatabaseEvent(CL_LIC_CHANGED);
  except
    on E:Exception do
    begin
      ModalResult := mrNone;
      ShowMessage('Cannot save changes. Error: ' + E.Message);
    end;
  end;
end;

end.



