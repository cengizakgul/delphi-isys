unit BBUtilsUnit;

interface

uses
  SysUtils, Classes, SBLicenseUnit, OgUtil, OnGuard, Math, EvLicenseInfo, isBaseClasses, EvStreamUtils,
  ISBasicUtils, SEncryptionRoutines;

function GenerateCode(SN, MD: Integer; const ALic: TSBLicense): string;
function GenerateAPICode(ALicenseCode: String; AAPIKeysList : IisListOfValues) : String;

implementation

function GenerateCode(SN, MD: Integer; const ALic: TSBLicense): string;
var
  Work: TCode;
  K: TKey;
  D1: TDateTime;
  Code: Word;
  i: Integer;
  tmpModuleInfo : TModuleInfo;
begin
  K := CKey;
  ApplyModifierToKeyPrim(MD, K, SizeOf(K));

  D1 := Date + Pred(ALic.LicenseExpiration);
  Code := 0;
  for i := 0 to ALic.ModuleInfos.Count - 1 do
  begin
    tmpModuleInfo := TModuleInfo(ALic.ModuleInfos[i]);
    if tmpModuleInfo.Value = 'Y' then
      Code := Code or Trunc(IntPower(2, tmpModuleInfo.PackageTypeNbr));
  end;

  SN := (SN shl LicenseOptionMaxCount) or Code;
  InitSerialNumberCode(K, SN, D1, Work);
  Result := BufferToHex(Work, SizeOf(Work));
end;

function GenerateAPICode(ALicenseCode: String; AAPIKeysList : IisListOfValues) : String;
var
  sAPIKeys, sEncryptedAPIKeys : String;
  sEncryptKey : String;
  APIKeysList : IisListOfValues;
  APIKeysStream : IevDualStream;
begin
  Result := '';

  CheckCondition(Length(Trim(ALicenseCode)) > 0, 'License code has zero length!');
  APIKeysList := AAPIKeysList;
  if APIKeysList.Count > 0 then
  begin
    APIKeysStream := TisStream.Create;
    APIKeysList.WriteToStream(APIKeysStream);
    APIKeysStream.Position := 0;
    sAPIKeys := StringOfChar(' ', APIKeysStream.Size);
    APIKeysStream.ReadBuffer(sAPIKeys[1], APIKeysStream.Size);

    sEncryptKey := ALicenseCode;
    EnsureKeyLength(sEncryptKey);
    sEncryptedAPIKeys := EncryptString(sAPIKeys, sEncryptKey);
    Result := StringOfChar(' ', Length(sEncryptedAPIKeys) * 2);

    BinToHex(PChar(sEncryptedAPIKeys), PChar(Result), Length(sEncryptedAPIKeys));
  end;
end;

end.
