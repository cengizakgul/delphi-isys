object TasksListForm: TTasksListForm
  Left = 370
  Top = 173
  Width = 339
  Height = 367
  Caption = 'Tasks list'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 292
    Width = 331
    Height = 41
    Align = alBottom
    TabOrder = 0
    object OKButton: TButton
      Left = 120
      Top = 8
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 33
    Width = 331
    Height = 259
    Align = alClient
    TabOrder = 1
    object TaskGrid: TwwDBGrid
      Left = 1
      Top = 1
      Width = 329
      Height = 257
      DisableThemesInTitle = False
      Selected.Strings = (
        'RWR_LIST_NBR'#9'10'#9'Task number'#9'F'#9
        'REPORT_NUMBER'#9'10'#9'Report number'#9'F'#9
        'StatusText'#9'15'#9'Status'#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      BorderStyle = bsNone
      DataSource = BBConfigData.IBRWRListDataSource
      ReadOnly = True
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      TitleButtons = False
      UseTFields = False
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 331
    Height = 33
    Align = alTop
    TabOrder = 2
    object ShowAllCheck: TCheckBox
      Left = 104
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Show all tasks'
      TabOrder = 0
      OnClick = ShowAllCheckClick
    end
  end
end
