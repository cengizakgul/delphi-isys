object BBMainForm: TBBMainForm
  Left = 244
  Top = 116
  BorderStyle = bsSingle
  Caption = 'BigBrother (Orange) server'
  ClientHeight = 54
  ClientWidth = 279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ThrowExceptionButton: TButton
    Left = 32
    Top = 16
    Width = 113
    Height = 25
    Caption = 'Throw an exception'
    Enabled = False
    TabOrder = 0
    Visible = False
    OnClick = ThrowExceptionButtonClick
  end
end
