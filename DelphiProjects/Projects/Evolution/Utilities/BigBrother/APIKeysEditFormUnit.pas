unit APIKeysEditFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SBLicenseWriterUnit, Grids, DBGrids, DBCtrls, StdCtrls, ExtCtrls;

type
  TAPIKeysEditForm = class(TForm)
    Panel1: TPanel;
    SaveButton: TButton;
    CancelButton: TButton;
    Panel2: TPanel;
    Panel3: TPanel;
    DBNavigator1: TDBNavigator;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
  private
    { Private declarations }
    Writer : TSBLicenseWriter;
  public
    { Public declarations }
    procedure ShowAPIkeys;
  end;

var
  APIKeysEditForm: TAPIKeysEditForm;

implementation

uses BBConfigDataUnit;

{$R *.dfm}

procedure TAPIKeysEditForm.FormCreate(Sender: TObject);
begin
  Writer := TSBLicenseWriter.Create(BBConfigData.IBDatabase, nil);
end;

procedure TAPIKeysEditForm.FormDestroy(Sender: TObject);
begin
  Writer.Free;
end;

procedure TAPIKeysEditForm.ShowAPIkeys;
begin
  if BBConfigData.EditIsDisabled then
  begin
    DBGrid1.ReadOnly := true;
    SaveButton.Enabled := false;
    BBConfigData.IBAPIKeys.UpdateObject := nil;
  end
  else
  begin
    DBGrid1.ReadOnly := false;
    SaveButton.Enabled := true;
    BBConfigData.IBAPIKeys.UpdateObject := BBConfigData.IBAPIKeysUpdateSQL;
  end;

  BBConfigData.IBManualTransactions.StartTransaction;
  BBConfigData.IBVendor.Active := false;
  BBConfigData.IBVendor.Active := true;
  BBConfigData.IBAPIKeys.Active := false;
  BBConfigData.IBAPIKeys.Active := true;
  try
    ShowModal;
    if Modalresult <> mrOk then
      try
        BBConfigData.IBAPIKeys.CancelUpdates;
        BBConfigData.IBManualTransactions.Rollback;
      except
        on E:Exception do
        begin
          BBConfigData.IBAPIKeys.CancelUpdates;
          BBConfigData.IBManualTransactions.Rollback;
        end;
      end;
    BBConfigData.IBVendor.Active := false;
    BBConfigData.IBAPIKeys.Active := false;
  finally
    if BBConfigData.IBManualTransactions.Active then
       BBConfigData.IBManualTransactions.Rollback;
  end;
end;

procedure TAPIKeysEditForm.SaveButtonClick(Sender: TObject);
begin
  try
    BBConfigData.IBAPIKeys.ApplyUpdates;
    BBConfigData.IBManualTransactions.Commit;
    Writer.PostDatabaseEvent(CL_LIC_CHANGED);
  except
    on E:Exception do
    begin
      ModalResult := mrNone;
      ShowMessage('Cannot save changes. Error: ' + E.Message);
    end;
  end;
end;

end.
