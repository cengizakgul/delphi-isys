unit GlovalObjectsUnit;

interface

uses ConnectionsPoolUnit, SBLicenseReaderUnit, SBLicenseUnit, isBaseClasses,
  SBLicenseWriterUnit, SysUtils, IniFiles, SyncObjs, LazyEmailNotifierUnit, MyTCPServerUnit, isLogFile;

const
  LogName = 'BBService.log';
  BBEventClass = 'BigBrother';
  WatchDogsDefault = 30;

type
  IGlobalObject = interface
    ['{A82EE4DA-6D49-4981-B0F9-CA7DE42B632A}']
    function GetEMail : ILazyEmailNotifier;
    function GetConnectionPool : IBBConnectionPool;
    function GetGlobalCS : IisLock;
    function GetIniFile : String;
  end;

  TGlobalObjects = class(TInterfacedObject, IGlobalObject)
  private
    FIniFileName: String;

    // e-mail settings
    FTimeout : cardinal;
    FHost : String;
    FAddressees : String;
    FSubj : String;
    FFrom : String;
    FRowsLimit : cardinal;
    FEMailEnabled : boolean;

    procedure SetIniFileName(const Value: String);
  protected
  public
    EMail : ILazyEmailNotifier;
    ConnectionPool : IBBConnectionPool;
    CS : IisLock; // global critical section;
    property IniFileName : String read FIniFileName write SetIniFileName;
    constructor Create;
    destructor Destroy; override;
    procedure ReadSettings;

    function GetEMail : ILazyEmailNotifier;
    function GetConnectionPool : IBBConnectionPool;
    function GetGlobalCS : IisLock;
    function GetIniFile : String;
  end;

  function WorkDir: string;

implementation

{ TGlobalObjects }

constructor TGlobalObjects.Create;
begin
  IniFileName := ChangeFileExt(ParamStr(0), '.ini');
  ReadSettings;
  CS := TisLock.Create;
  OpenGlobalLogFile(ExtractFilePath(ParamStr(0)) + LogName);

  EMail := TLazyEmailNotifier.Create(FHost, FFrom, FAddressees, FSubj, FTimeout, FRowsLimit, FEMailEnabled);
  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Application started.', '');
  EMail.Write('Application started.');
  EMail.Flush;
  if FEMailEnabled then
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'EMail notification is enabled', '')
  else
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'EMail notification is disabled', '');
  ConnectionPool := InitializeConnectionPool(IniFileName, EMail);
end;

destructor TGlobalObjects.Destroy;
begin
  EMail.Write('Application stopped.');
  EMail := nil;
  ConnectionPool := nil; // important! before free log!
  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Application stopped.', '');
  sleep(100);
  SetGlobalLogFile(nil);
  CS := nil;
  inherited;
end;

function TGlobalObjects.GetConnectionPool: IBBConnectionPool;
begin
  result := ConnectionPool;
end;

function TGlobalObjects.GetEMail: ILazyEmailNotifier;
begin
  result := EMail;
end;

function TGlobalObjects.GetGlobalCS: IisLock;
begin
  result := CS;
end;

function TGlobalObjects.GetIniFile: String;
begin
  result := FIniFileName;
end;

procedure TGlobalObjects.ReadSettings;
var
  tmpIni : TIniFile;
begin
  tmpIni := TIniFile.Create(IniFileName);
  try
    FTimeout := tmpIni.ReadInteger('Email', 'Timeout', 0);
    FRowsLimit := tmpIni.ReadInteger('Email', 'RowsLimit', 0);
    FHost := tmpIni.readString('EMail', 'Host', '');
    FAddressees := tmpIni.readString('EMail', 'To', '');
    FSubj := tmpIni.ReadString('EMail', 'Subject', '');
    FFrom := tmpIni.ReadString('EMail', 'From', '');
    FEMailEnabled := tmpIni.ReadBool('EMail', 'Enabled', true);
  finally
    tmpIni.Free;
  end;
end;

procedure TGlobalObjects.SetIniFileName(const Value: String);
begin
  FIniFileName := Value;
end;

function WorkDir: string;
begin
  Result := ExtractFilePath(ParamStr(0));
end;

end.
