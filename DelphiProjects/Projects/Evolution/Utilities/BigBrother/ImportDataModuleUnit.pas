unit ImportDataModuleUnit;

interface

uses
  SysUtils, Classes, IBDatabaseINI, IBDatabase, DB, IBSQL, IBScript,
  IBCustomDataSet, IBQuery, IBTable;

type
  TImportDataModule = class(TDataModule)
    IBDatabase: TIBDatabase;
    IBTransaction: TIBTransaction;
    IBClearLogs: TIBScript;
    IBDeletePreviousInfo: TIBScript;
    IBLicInfo: TIBQuery;
    IBStatusInfo: TIBQuery;
    IBLicPackage: TIBQuery;
    IBDefaultInfo: TIBTable;
    IBPackageType: TIBTable;
    IBRWRList: TIBQuery;
    UpdateQuery: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportDataModule: TImportDataModule;

implementation

{$R *.dfm}

end.
