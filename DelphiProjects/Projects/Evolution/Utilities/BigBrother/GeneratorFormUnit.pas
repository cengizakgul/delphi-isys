unit GeneratorFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, CheckLst, ComCtrls, OnGuard, OgUtil, EvLicenseInfo, Math,
  ExtCtrls, SBLicenseReaderUnit, SBLicenseUnit, BBUtilsUnit, DateUtils, isBaseClasses,
  EvStreamUtils;

type
  TGeneratorForm = class(TForm)
    edSerialNumber: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    dpExpDate: TDateTimePicker;
    Label4: TLabel;
    edModifier: TEdit;
    cbLicense: TCheckListBox;
    Label5: TLabel;
    BtnGenerate: TButton;
    Bevel1: TBevel;
    Label6: TLabel;
    CodeMemo: TMemo;
    SpeedButton1: TSpeedButton;
    procedure edSerialNumberChange(Sender: TObject);
//    procedure CopySbClick(Sender: TObject);
    procedure BtnGenerateClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    Lic : TSBLicense;
    CurrentSerialNumber : integer;
  public
    { Public declarations }
    procedure ShowGenerator;
  end;

var
  GeneratorForm: TGeneratorForm;

implementation

uses BBConfigDataUnit, EditFormUnit;

{$R *.dfm}

procedure TGeneratorForm.edSerialNumberChange(Sender: TObject);
begin
  BtnGenerate.Enabled := (edSerialNumber.Text <> '') and (edModifier.Text <> '');
end;

{procedure TGeneratorForm.CopySbClick(Sender: TObject);
var
  OldSelStart : Integer;
begin
  OldSelStart := EdCode.SelStart;
  EdCode.SelStart := 0;
  EdCode.SelLength := MaxInt;
  EdCode.CopyToClipboard;
  EdCode.SelStart := OldSelStart;
  EdCode.SelLength := 0;
end;
}

procedure TGeneratorForm.BtnGenerateClick(Sender: TObject);
var
  Modifier: LongInt;
  sLicCode : String;
begin
  CodeMemo.Clear;

  // license
  HexToBuffer(EdModifier.Text, Modifier, SizeOf(LongInt));
  if dpExpDate.Date < Date then
    raise Exception.Create('You cannot create expired licenses!');
  Lic.LicenseExpiration := DaysBetween(Date, dpExpDate.Date) + 1;
  sLicCode := GenerateCode(Lic.SerialNumber, Modifier, Lic) ;

  CodeMemo.Lines.Add(sLicCode + GenerateAPICode(sLicCode, Lic.APIKeys));
  CodeMemo.SelLength := 0;
end;

procedure TGeneratorForm.ShowGenerator;
var
  i: Integer;
  tmp : TModuleInfo;
begin
  if BBConfigData.IBLicInfo['SERIAL_NUMBER'] <> null then
  begin
    dpExpDate.DateTime := Date + 1;
    CurrentSerialNumber := BBConfigData.IBLicInfo['SERIAL_NUMBER'];
    edSerialNumber.Text := InttOStr(CurrentSerialNumber);

    Lic := EditForm.Reader.Read(CurrentSerialNumber);
    try
      cbLicense.Clear;
      for i := 0 to Lic.ModuleInfos.Count - 1 do
      begin
        tmp := TModuleInfo(Lic.ModuleInfos[i]);
        cbLicense.AddItem(tmp.ShortDescription, nil);
        cbLicense.Checked[i] := tmp.Value = 'Y';
      end;  // for i

      CodeMemo.Clear;
      edModifier.Text := '';

      ShowModal;
    finally
      FreeAndNil(Lic);
    end;
  end;
end;

procedure TGeneratorForm.FormActivate(Sender: TObject);
begin
  edModifier.SetFocus;
end;

procedure TGeneratorForm.SpeedButton1Click(Sender: TObject);
begin
  CodeMemo.SelectAll;
  CodeMemo.CopyToClipboard;
  CodeMemo.SelLength := 0;
end;

end.
