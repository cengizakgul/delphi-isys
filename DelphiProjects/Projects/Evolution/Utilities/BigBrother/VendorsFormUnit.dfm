object VendorsForm: TVendorsForm
  Left = 383
  Top = 226
  BorderStyle = bsSingle
  Caption = 'API applications vendors'
  ClientHeight = 524
  ClientWidth = 611
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 483
    Width = 611
    Height = 41
    Align = alBottom
    TabOrder = 0
    object SaveButton: TButton
      Left = 168
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 0
      OnClick = SaveButtonClick
    end
    object CancelButton: TButton
      Left = 368
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 611
    Height = 483
    Align = alClient
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 448
      Width = 609
      Height = 34
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object DBNavigator1: TDBNavigator
        Left = 184
        Top = 6
        Width = 240
        Height = 25
        DataSource = BBConfigData.IBVendorDataSource
        TabOrder = 0
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 609
      Height = 447
      Align = alClient
      DataSource = BBConfigData.IBVendorDataSource
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'VENDOR_CUSTOM_NBR'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VENDOR_NAME'
          Width = 505
          Visible = True
        end>
    end
  end
end
