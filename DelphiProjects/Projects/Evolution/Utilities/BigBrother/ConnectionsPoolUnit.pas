unit ConnectionsPoolUnit;

interface

uses DB,SysUtils, Controls, Classes, SBLicenseUnit, IBDatabase, IBQuery, SyncObjs, Variants, IBHeader,
  SBLicenseReaderUnit, SBLicenseWriterUnit, IniFiles, DBLogDlg, isThreadManager,
  MyTCPServerUnit, LazyEmailNotifierUnit, isLogFile;

const
  DefaultBadThreshold = 10;
  DefaultConnectionCheckManagerSleep = 100;
  DefaultConnectionRecreationManagerSleep = 1000;
  DefaultSleepWaitingForFree = 100;
  DefaultSleepSliceForFree = 10;
  LogSeverityImportant = 0;
  LogSeverityAll = 1;
  LogSeverityDebug = 2;

type
  TBBConnectionException = class(Exception);

  TBBConnection = class;

  IBBConnection = interface
    ['{51B2A0AE-113A-40D9-A9B0-87C3DD39467C}']
    function GetReader : TSBLicenseReader;
    function GetWriter : TSBLicenseWriter;
    procedure IncErrorCount;
    function GetErrorCount : cardinal;
  end;

  IBBConnectionExt = interface
    ['{4F6A8702-C96F-414A-9A3B-620D6A1DEEC5}']
    function GetImplementation : TBBConnection;
 end;

  IBBConnectionPool = interface
    ['{27C4DC96-5DFF-4800-891C-7282ADB86576}']
    function FindAndTakeSlotWait : IBBConnection;
    function FindAndTakeSlotWaitSomeTime(ATimeout : cardinal; ARaiseException : boolean = true) : IBBConnection;
    function FindAndTakeSlotNoWait : IBBConnection;
    procedure ReleaseSlot (AConnection : IBBConnection);
    function GetLogSeverity : integer; // LogSeverityAll or LogSeverityImportant
  end;

  // one connection object
  TBBConnection = class(TInterfacedObject, IBBConnection, IBBConnectionExt)
  private
    // configuration parameters
    FDatabase : TIBDatabase;
    FUserName: String;
    FPassword: String;
    FBadThreshold : cardinal;
    //
    FBusyCriticalSection : TCriticalSection;
    FDatabaseCriticalSection : TCriticalSection;
    FLogSeverity : integer; // ALL or IMPORTANT
    // flags
    FConnected: boolean;
    FBusy: boolean;
    FBusySince: TDateTime;
    FIsBad: boolean;
    FErrorCount: cardinal;
    procedure SetPassword(const Value: String);
    procedure SetUserName(const Value: String);
    procedure SetConnected(const Value: boolean);
    function ReadConnected : boolean;
    procedure SetBusy(const Value: boolean);
    procedure SetBusySince(const Value: TDateTime);
    procedure SetIsBad(const Value: boolean);
  protected
    function GetReader : TSBLicenseReader;
    function GetWriter : TSBLicenseWriter;
    function GetErrorCount : cardinal;
    procedure IncErrorCount;
    procedure SetErrorCount(const AValue: cardinal);
    function GetImplementation : TBBConnection;
    procedure IBDatabaseLogin(Database: TIBDatabase; LoginParams: TStrings);
  public
    Reader : TSBLicenseReader;
    Writer : TSBLicenseWriter;
    property IsBad : boolean read FIsBad write SetIsBad;
    property Busy : boolean read FBusy write SetBusy;
    property BusySince : TDateTime read FBusySince write SetBusySince;
    property Connected : boolean read ReadConnected write SetConnected;
    property UserName : String read FUserName write SetUserName;
    property Password : String read FPassword write SetPassword;
    property ErrorCount : cardinal read GetErrorCount write SetErrorCount;
    constructor Create(pDatabasePath : String; ALogSeverity : integer);
    destructor Destroy; override;
    procedure EnterCriticalSection;
    procedure LeaveCriticalSection;
    function GetDatabase : TIBDatabase;
  end;

  TBBConnectionPool = class(TInterfacedObject, IBBConnectionPool)
  private
    FList : IInterfaceList;
     // configuration parameters
    FSleepWaitingForFree : cardinal; // time in milliseconds, how long the thread will sleep before next check for free slot
    FConnectionCheckManagerSleep : cardinal; // time in milliseconds, how long the connection check manager thread will sleep between slot checks
    FConnectionRecreationManagerSleep : cardinal; // time in milliseconds, how long the connection recreation manager thread will sleep between slot checks
    FPoolSize : cardinal;
    FDatabasePath : String;
    FUserName: String;
    FPassword: String;
    FBadThreshold : cardinal;
    FLogSeverity : integer; // ALL or IMPORTANT
    //
    FConnectionCheckManagerId : TTaskId;
    FConnectionRecreationManagerId : TTaskId;
    EMail : ILazyEmailNotifier;
    procedure CreateAndOpenConnection;
    procedure ConnectionCheckManager(const Params: TTaskParamList);
    procedure ConnectionRecreationManager(const Params: TTaskParamList);
  protected
    procedure ReadConfigFromIni(AConfigFileName : String);
  public
    constructor Create(pConfigFileName : String; pEMail : ILazyEmailNotifier);
    destructor Destroy; override;
    function FindAndTakeSlotWait : IBBConnection;
    function FindAndTakeSlotWaitSomeTime(ATimeout : cardinal; ARaiseException : boolean = true) : IBBConnection;
    function FindAndTakeSlotNoWait : IBBConnection;
    procedure ReleaseSlot (AConnection : IBBConnection);
    function GetLogSeverity : integer; // LogSeverityAll or LogSeverityImportant
  end;

  // call it to create and initialize connections pool
  function InitializeConnectionPool(AConfigFileName : String; pEMail : ILazyEmailNotifier) : IBBConnectionPool;

implementation

uses GlovalObjectsUnit;

{ TBBConnection }
constructor TBBConnection.Create(pDatabasePath : String; ALogSeverity : integer);
begin
  FDatabase := TIBDatabase.Create(nil);
  FDatabase.DatabaseName := pDatabasePath;
  FBusyCriticalSection := TCriticalSection.Create;
  FDatabaseCriticalSection := TCriticalSection.Create;
  FBusy := false;
  FBusySince := 0;
  FBadThreshold := DefaultBadThreshold;
  FLogSeverity := ALogSeverity;
  Reader := TSBLicenseReader.Create(FDatabase, FDatabaseCriticalSection);
  Writer := TSBLicenseWriter.Create(FDatabase, FDatabaseCriticalSection);
end;

destructor TBBConnection.Destroy;
begin
  try
    if Assigned(Reader) then
      FreeAndNil(Reader);
  except
    ;
  end;
  try
    if Assigned(Writer) then
      FreeAndNil(Writer);
  except
    ;
  end;
  if Assigned(FBusyCriticalSection) then
    FBusyCriticalSection.Free; FBusyCriticalSection := nil;
  if Assigned(FDatabaseCriticalSection) then
    FreeAndNil(FDatabaseCriticalSection);
  try
    try
      FDatabase.Connected := false;
    except
      ;
    end;
  finally
    FDatabase.Free;
  end;
  inherited;
end;

procedure TBBConnection.SetConnected(const Value: boolean);
begin
  if Value <> FConnected then
    if Value then  // connect
    begin
      FDatabase.Connected := false;
      FDatabase.OnLogin := IBDatabaseLogin;
      FDatabase.Connected := true;
    end
    else  // disconnect
      try
        FDatabase.Connected := false;
      except
        ;
      end;
  FConnected := Value;
end;

procedure TBBConnection.SetPassword(const Value: String);
begin
  FPassword := Value;
end;

procedure TBBConnection.SetUserName(const Value: String);
begin
  FUserName := Value;
end;

function TBBConnection.ReadConnected : boolean;
begin
  try
    FConnected := FDatabase.Connected;
  except
    on E:Exception do
    begin
      isBad := true;
      FConnected := false;
      if FLogSeverity >= LogSeverityAll then // ALL
        GlobalLogFile.AddEvent(etError, BBEventClass, 'Connection. Error reading "Connected" property of database', 'Connection. Error reading "Connected" property of database:' + E.Message);
    end;
  end;
  result := FConnected;
end;

procedure TBBConnection.EnterCriticalSection;
begin
  FBusyCriticalSection.Enter;
end;

procedure TBBConnection.LeaveCriticalSection;
begin
  FBusyCriticalSection.Leave;
end;

procedure TBBConnection.SetBusy(const Value: boolean);
begin
  FBusy := Value;
end;

procedure TBBConnection.SetBusySince(const Value: TDateTime);
begin
  FBusySince := Value;
end;

procedure TBBConnection.SetIsBad(const Value: boolean);
begin
  FIsBad := Value;
end;

function TBBConnection.GetReader: TSBLicenseReader;
begin
  result := Reader;
end;

function TBBConnection.GetWriter: TSBLicenseWriter;
begin
  result := Writer;
end;

procedure TBBConnection.SetErrorCount(const AValue: cardinal);
begin
  EnterCriticalSection;
  try
    FErrorCount := AValue;
    if AValue >= FBadThreshold then IsBad := true;
  finally
    LeaveCriticalSection;
  end;
end;

function TBBConnection.GetErrorCount: cardinal;
begin
  result := FErrorCount;
end;

function TBBConnection.GetImplementation: TBBConnection;
begin
  result := self;
end;

procedure TBBConnection.IBDatabaseLogin(Database: TIBDatabase;
  LoginParams: TStrings);
begin
   // add username and password
   LoginParams.Clear;
   LoginParams.Add('user_name='+Username);
   LoginParams.Add('password='+Password);
end;

{ TBBConnectionPool }

// Logic of ConnectionManager:
// 1. If Slot is bad and free, manager makes it busy and trys to reconnect
// 2. on success, manager makes it free and not bad
// 3. if not success, manager close connection and delete slot
// 4. another thread (see CreationManager) checks pools size and tries to recreate slot periodically
procedure TBBConnectionPool.ConnectionCheckManager(const Params: TTaskParamList);
var
  ItsBadSlot : boolean;

  function RepairCurrentSlot(const AConnection : IBBConnectionExt) : boolean;
  begin
    result := false;
    try
      AConnection.GetImplementation.Connected := false;
      AConnection.GetImplementation.Connected := true;
      result := true;
    except
      on E:Exception do
       ;
    end;
  end;

  procedure Dummy;
  var
    i : integer;
    tmpConnection : IBBConnectionExt;
  begin
    try
      // search for the first bad slot
      FList.Lock;
      try
        for i := 0 to FList.Count - 1 do
        begin
          ItsBadSlot := false;
          tmpConnection := FList[i] as IBBConnectionExt;
          tmpConnection.GetImplementation.EnterCriticalSection;
          // check if slot is bad and not busy
          try
            if (not tmpConnection.GetImplementation.Busy) and
              ((tmpConnection.GetImplementation.isBad) or (not tmpConnection.GetImplementation.FDatabase.Connected)) then
            begin
              tmpConnection.GetImplementation.Busy := true;
              tmpConnection.GetImplementation.BusySince := now; // if slot is bad, this thread takes this slot
              ItsBadSlot := true;
              break;
            end;
          finally
            tmpConnection.GetImplementation.LeaveCriticalSection; // don't try to repair in critical section
          end;
        end; // for
      finally
        FList.Unlock;
      end;

      // now we can try to repair bad slot
      if not ItsBadSlot then
        tmpConnection := nil
      else
      begin
        if not RepairCurrentSlot(tmpConnection) then
        begin
          // cannot repair - deleting slot
          try
            tmpConnection.GetImplementation.Connected := false; // close connection
          except
            ;
          end;
          FList.Lock;
          try
            Flist.Delete(Flist.IndexOf(tmpConnection));
            tmpConnection := nil;
          finally
            FList.Unlock
          end;
          GlobalLogFile.AddEvent(etError, BBEventClass, 'ConnectionPool. Cannot reconnect slot', 'ConnectionPool. Cannot reconnect slot. It''ll be deleted');
        end
        else
        begin
          // succes, so set bad off
          tmpConnection.GetImplementation.EnterCriticalSection;
          try
            tmpConnection.GetImplementation.isBad := false;
            tmpConnection.GetImplementation.Busy := false;
            tmpConnection.GetImplementation.BusySince := 0;
          finally
            tmpConnection.GetImplementation.LeaveCriticalSection;
          end;
          tmpConnection := nil;
        end;
      end; // else
    except
      on E:Exception do
        GlobalLogFile.AddEvent(etError, BBEventClass, 'ConnectionPool. Exception in CheckManager', 'ConnectionPool. Exception in CheckManager: ' + E.Message);
    end;

    sleep(FConnectionCheckManagerSleep);
  end;  // dummy
begin
  ItsBadSlot := false;
  while not CurrentThreadTaskTerminated do
    Dummy;
end;

procedure TBBConnectionPool.ConnectionRecreationManager(
  const Params: TTaskParamList);
var
  i : integer;
begin
  while not CurrentThreadTaskTerminated do
  begin
    FList.Lock;
    try
      if Cardinal(Flist.Count) < FPoolSize then
      begin
        i := FPoolSize - Cardinal(FList.Count);
        try
          while i > 0 do
          begin
            CreateAndOpenConnection;
            Dec(i);
            if (FLogSeverity >= LogSeverityAll) then // ALL
              GlobalLogFile.AddEvent(etInformation, BBEventClass, 'ConnectionPool. Recreated a new connection', '');
          end;
        except
          on E:Exception do
          begin
            GlobalLogFile.AddEvent(etError, BBEventClass, 'ConnectionPool. Exception in RecreationManager', 'ConnectionPool. Exception in RecreationManager: ' + E.Message);
            if Assigned(EMail) then
              EMail.Write('ConnectionPool. Exception in RecreationManager: ' + E.Message);
          end;  
        end;
      end;
    finally
      FList.Unlock;
    end;
    sleep(FConnectionRecreationManagerSleep);
  end;  // while
end;

constructor TBBConnectionPool.Create(pConfigFileName: String; pEMail : ILazyEmailNotifier);
var
  i : integer;
begin
  EMail := PEMail;
  ReadConfigFromIni(pConfigFileName);
  FList := TInterfaceList.Create;
  for i := 1 to FPoolSize  do
    CreateAndOpenConnection;
  FConnectionCheckManagerId := GlobalThreadManager.RunTask(ConnectionCheckManager, Self, MakeTaskParams([]), 'Connection Check');
  FConnectionRecreationManagerId := GlobalThreadManager.RunTask(ConnectionRecreationManager, Self, MakeTaskParams([]), 'Connection Recreation');
  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Connection pool created', 'Connection pool created. LogSeverity=' + IntToStr(FLogSeverity) + ' PoolSize=' + IntToStr(FPoolSize));
end;

procedure TBBConnectionPool.CreateAndOpenConnection;
var
  tmpConnection : IBBConnectionExt;
begin
  tmpConnection := TBBConnection.Create(FDatabasePath, FLogSeverity);
  tmpConnection.GetImplementation.UserName := FUserName;
  tmpConnection.GetImplementation.Password := FPassword;
  tmpConnection.GetImplementation.Connected := true;
  tmpConnection.GetImplementation.FBadThreshold := FBadThreshold;
  Flist.Add(tmpConnection);
end;

destructor TBBConnectionPool.Destroy;
begin
  EMail := nil;
  GlobalThreadManager.TerminateTask(FConnectionCheckManagerId);
  GlobalThreadManager.WaitForTaskEnd(FConnectionCheckManagerId);

  GlobalThreadManager.TerminateTask(FConnectionRecreationManagerId);
  GlobalThreadManager.WaitForTaskEnd(FConnectionRecreationManagerId);

  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Connection pool destroyed', '');
  inherited;
end;

function TBBConnectionPool.FindAndTakeSlotWait: IBBConnection;
begin
  result := nil;
  while true do
  begin
    result := FindAndTakeSlotNoWait;
    if result <> nil then exit;
    sleep(FSleepWaitingForFree);
  end;  // while
end;

function TBBConnectionPool.FindAndTakeSlotNoWait: IBBConnection;
var
  i : integer;
  tmpConnection : IBBConnectionExt;
begin
  result := nil;
  FList.Lock;
  try
    for i := 0 to FList.Count - 1 do
    begin
      tmpConnection := FList[i] as IBBConnectionExt;
      tmpConnection.GetImplementation.EnterCriticalSection;
      try
        if not tmpConnection.GetImplementation.Busy then
        begin
          if (not tmpConnection.GetImplementation.IsBad) and (not tmpConnection.GetImplementation.Connected) then
          begin
            tmpConnection.GetImplementation.IsBad := true; // if not connected, set up that it's bad
            continue;
          end;
          if not tmpConnection.GetImplementation.IsBad then
          begin
            tmpConnection.GetImplementation.Busy := true;
            tmpConnection.GetImplementation.BusySince := now;
            result := tmpConnection as IBBConnection;  // no errors, take it and leave the loop
            exit;
          end;
        end;
      finally
        tmpConnection.GetImplementation.LeaveCriticalSection;
      end;
    end; // for
  finally
    FList.Unlock;
  end;
end;

function TBBConnectionPool.GetLogSeverity: integer;
begin
  result := FLogSeverity;
end;

procedure TBBConnectionPool.ReadConfigFromIni(AConfigFileName : String);
var
  tmpIni : TIniFile;
begin
  tmpIni := TIniFile.Create(AConfigFileName);
  try
    FUserName := tmpIni.ReadString('Database', 'UserName', '');
    if FUserName = '' then raise TBBConnectionException.Create('Error! You must provide "UserName" in configuration file!');

    FDatabasePath := tmpIni.ReadString('Database', 'Path', '');
    if FDatabasePath = '' then raise TBBConnectionException.Create('Error! You must provide "Path" in configuration file!');

    FPassword := tmpIni.ReadString('Database', 'Password', '');
    if FPassword = '' then raise TBBConnectionException.Create('Error! You must provide "Password" in configuration file!');

    FPoolSize := tmpIni.ReadInteger('Database', 'PoolSize', 0);
    if FPoolSize = 0 then raise TBBConnectionException.Create('Error! You must provide "PoolSize" in configuration file!');

    FSleepWaitingForFree := tmpIni.ReadInteger('Database', 'SleepWaitingForFree', DefaultSleepWaitingForFree);
    FBadThreshold := tmpIni.ReadInteger('Database', 'BadThreshold', DefaultBadThreshold);
    FConnectionCheckManagerSleep := tmpIni.ReadInteger('Database', 'ConnectionCheckManagerSleep', DefaultConnectionCheckManagerSleep);
    FConnectionRecreationManagerSleep := tmpIni.ReadInteger('Database', 'ConnectionRecreationManagerSleep', DefaultConnectionRecreationManagerSleep);

    FLogSeverity := tmpIni.ReadInteger('Database', 'LogSeverity', LogSeverityImportant);
    if (FLogSeverity <> LogSeverityImportant) and (FLogSeverity <> LogSeverityAll) and (FLogSeverity <> LogSeverityDebug) then raise TBBConnectionException.Create('Error! Wrong value for "LogSeverity"! It can be ' + IntToStr(LogSeverityAll) + ' or ' + IntToStr(LogSeverityImportant));
  finally
    tmpIni.Free;
  end;
end;

procedure TBBConnectionPool.ReleaseSlot(AConnection: IBBConnection);
var
  tmp : IBBConnectionExt;
begin
  tmp := (AConnection as IBBConnectionExt);
  tmp.GetImplementation.EnterCriticalSection;
  try
    tmp.GetImplementation.Busy := false;
    tmp.GetImplementation.BusySince := 0;
  finally
    tmp.GetImplementation.LeaveCriticalSection;
  end;
end;

function InitializeConnectionPool(AConfigFileName : String; pEMail : ILazyEmailNotifier) : IBBConnectionPool;
begin
  result := TBBConnectionPool.Create(AConfigFileName, pEMail);
end;

procedure TBBConnection.IncErrorCount;
begin
  EnterCriticalSection;
  try
    FErrorCount := FErrorCount + 1;
    if FErrorCount >= FBadThreshold then IsBad := true
    else
      if not Connected then IsBad := true;  // important: not FConnected but Connected
  finally
    LeaveCriticalSection;
  end;
end;

function TBBConnection.GetDatabase: TIBDatabase;
begin
  result := FDatabase;
end;

function TBBConnectionPool.FindAndTakeSlotWaitSomeTime(ATimeout: cardinal;
  ARaiseException: boolean): IBBConnection;
var
  cnt : cardinal;
begin
  result := nil;
  cnt := 0;
  repeat
    result := FindAndTakeSlotNoWait;
    if result <> nil then exit;
    sleep(DefaultSleepSliceForFree);
    cnt := cnt + DefaultSleepSliceForFree;
  until cnt > ATimeout;
  if ARaiseException then
    raise TBBConnectionException.Create('Connection pool manager. Cannot find and take slot: timeout!');
end;

end.
