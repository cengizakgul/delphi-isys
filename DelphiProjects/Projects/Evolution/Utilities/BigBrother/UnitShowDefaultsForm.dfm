object ShowDefaultForm: TShowDefaultForm
  Left = 242
  Top = 120
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'View default settings'
  ClientHeight = 286
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 85
    Height = 13
    Caption = 'License expiration'
  end
  object Label2: TLabel
    Left = 16
    Top = 32
    Width = 87
    Height = 13
    Caption = 'License frequency'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 94
    Height = 13
    Caption = 'Report grace period'
  end
  object DBEdit1: TDBEdit
    Left = 120
    Top = 8
    Width = 40
    Height = 21
    DataField = 'LICENSE_EXPIRATION'
    DataSource = BBConfigData.IBDefaultInfoDataSource
    ReadOnly = True
    TabOrder = 1
  end
  object DBEdit2: TDBEdit
    Left = 120
    Top = 32
    Width = 40
    Height = 21
    DataField = 'LICENSE_FREQUENCY'
    DataSource = BBConfigData.IBDefaultInfoDataSource
    ReadOnly = True
    TabOrder = 2
  end
  object DBEdit3: TDBEdit
    Left = 120
    Top = 56
    Width = 40
    Height = 21
    DataField = 'REPORT_GRACE_PERIOD'
    DataSource = BBConfigData.IBDefaultInfoDataSource
    ReadOnly = True
    TabOrder = 3
  end
  object wwDBGrid1: TwwDBGrid
    Left = 16
    Top = 88
    Width = 225
    Height = 158
    DisableThemesInTitle = False
    ControlType.Strings = (
      'DefaultValueIndicator;ImageIndex;Original Size')
    Selected.Strings = (
      'PACKAGE_TYPE_NBR'#9'5'#9'Code'#9'F'
      'SHORT_DESCRIPTION'#9'5'#9'Abbr'#9'F'
      'LONG_DESCRIPTION'#9'15'#9'Description'#9'F'
      'DefaultValueIndicator'#9'5'#9'Active'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = BBConfigData.IBPackageTypeDataSource
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgPerfectRowFit]
    ReadOnly = True
    TabOrder = 4
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    ImageList = BBConfigMainForm.ImageList1
  end
  object OKButton: TButton
    Left = 88
    Top = 256
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKButtonClick
  end
end
