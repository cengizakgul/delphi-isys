{ Created by Mykhaylo Makarkin
  Augest 2007
  Allows to write TSBLicense to database
}

unit SBLicenseWriterUnit;

interface

uses DB,SysUtils, Controls, Classes, SBLicenseUnit, IBDatabase, IBQuery, SyncObjs, Variants, IBHeader;

const
  // server side events
  SRV_STATUS_CHANGED = 'SRV_STATUS_CHANGED';
  SRV_RWR_CHANGED = 'SRV_RWR_CHANGED';
  // client side events
  CL_LIC_CHANGED = 'CL_LIC_CHANGED';
  CL_STATUS_CHANGED = 'CL_STATUS_CHANGED';
  CL_RWR_CHANGED = 'CL_RWR_CHANGED';
  CL_LIC_UPD_CHANGED = 'CL_LIC_UPD_CHANGED';

type

  SBLicenseWriterRefreshType = Set of (AllInfo, LicInfo, StatusInfo, RWRTasks);
  SBLicenseWriterException = class(Exception);

  // Writer class
  TSBLicenseWriter = class (TObject)
  private
  protected
    FCriticalSection : TCriticalSection;
    FDatabase: TIBDatabase;
    FTransaction: TIBTransaction;
    FLicInfo : TIBQuery;
    FStatusInfo : TIBQuery;
    FLicPackage : TIBQuery;
    FUpdateQuery : TIBQuery;
    FRWRList : TIBQuery;
    function InternalCheckInDatabase (PSerialNumber : integer) : boolean;
    procedure InternalWrite(const PSBLicense : TSBLicense; PTypeOfWrite : integer); // PTypeOfWrite: 0 - all, 1 - LicInfo and LicPackage, 2 - StatusInfo, 3 - RWR list
  public
    // Database should be provided and opened
    // if critical section is not nil it will be used
    constructor Create(const PDatabase : TIBDatabase; const PCriticalSection : TCriticalSection);
    destructor Destroy; override;
    function CheckInDatabase (PSerialNumber : integer) : boolean;
    procedure Write(const PSBLicense : TSBLicense; pRefreshType : SBLicenseWriterRefreshType);
    procedure WriteAll (const PSBLicense : TSBLicense); // internally call WriteLicInfo, WriteStatusInfo, WriteRWRTasks
    procedure WriteLicInfo (const PSBLicense : TSBLicense);
    procedure WriteStatusInfo (const PSBLicense : TSBLicense);
    procedure WriteRWRTasks (const PSBLicense : TSBLicense); // this procedure NEVER CHANGE STATUS FROM "1" TO "0" and NEVER DELETE ROWS.
                                                             // it can only add a new rows or change status from "0" to "1"
    procedure WriteRWRReport (PRWRListNBR : integer; PStream : TStream); // saves stream to field REPORT_FILE of RWR_LIST table
    procedure WriteStatusReport (PSerialNumber : integer; PStream : TStream); // saves stream to field REPORT_FILE of STATUS_INFO table
    procedure Delete (PSerialNumber : Integer);
    procedure WriteOnlyReportStatus(PSerialNumber : integer; PReportStatus : String); // insert record if there's no appropriate record in STATUS_INFO for such serial_number
    procedure WriteOnlyLicenseUpdateStatus(PSerialNumber : integer; PLicenseUpdateStatus : String); // insert record if there's no appropriate record in STATUS_INFO for such serial_number
    procedure PostDatabaseEvent(AEventName : STring);  // to send database event
    procedure MarkAllAsDisconnected; // mark records as disconnected for all SB - called during startup BB server
    procedure FixSpoiledStatus(ASerialNumber : integer; ATime : TDateTime); // set status to '0' if ATime is more than record's last connected time is
  end;

implementation

{ TSBLicenseWriter }

function TSBLicenseWriter.CheckInDatabase(PSerialNumber: integer): boolean;
begin
  result := false;
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      result := InternalCheckInDatabase(PSerialNumber);
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

constructor TSBLicenseWriter.Create(const PDatabase: TIBDatabase;
  const PCriticalSection: TCriticalSection);
begin
  if (not Assigned(PDatabase)) {or (not pDataBase.Connected)} then
     raise SBLicenseWriterException.Create('Error! You should provide connected database!');

  inherited Create;

  if Assigned(PCriticalSection) then FCriticalSection := PCriticalSection;

  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FDatabase := PDatabase;
    FTransaction := TIBTransaction.Create(nil);
    FTransaction.Params.Clear;
    FTransaction.Params.Add('write');
    FTransaction.Params.Add('read_committed');
    FTransaction.Params.Add('rec_version');
    FTransaction.Params.Add('nowait');
    FTransaction.AllowAutoStart := false;
    FTransaction.DefaultDatabase := FDatabase;

    FLicInfo := TIBQuery.Create(nil);
    FLicInfo.Database := FDatabase;
    FLicInfo.Transaction := FTransaction;
    FLicInfo.SQL.text := 'select SERIAL_NUMBER, SERVICE_BUREAU_NAME, REPORT_GRACE_PERIOD, LICENSE_FREQUENCY, LICENSE_EXPIRATION from LIC_INFO where serial_number = :p1';

    FStatusInfo := TIBQuery.Create(nil);
    FStatusInfo.Database := FDatabase;
    FStatusInfo.Transaction := FTransaction;
    FStatusInfo.Sql.Text := 'select CONNECTED, DATABASE_VERSION, DATE_TIME_CONNECTED, ' +
      'LAST_ADRESS, LICENSE_UPDATED, REPORT_RAN, REPORT_STATUS, REPORT_UPDATED, SERIAL_NUMBER, SERVER_VERSION, SERVER_VERSION_SENT, DATABASE_VERSION_SENT, LICENSE_UPDATE_STATUS from STATUS_INFO where serial_number = :p1';

    FLicPackage := TIBQuery.Create(nil);
    FLicPackage.Database := FDatabase;
    FLicPackage.Transaction := FTransaction;
    FlicPackage.Sql.Text := 'select PACKAGE_TYPE_NBR , SERIAL_NUMBER from LIC_PACKAGE where serial_number=:p1';

    FUpdateQuery := TIBQuery.Create(nil);
    FUpdateQuery.Database := FDatabase;
    FUpdateQuery.Transaction := FTransaction;

    FRWRList := TIBQuery.Create(nil);
    FRWRList.Database := FDatabase;
    FRWRList.Transaction := FTransaction;
    FRWRList.SQL.text := 'select RWR_LIST_NBR, SERIAL_NUMBER, REPORT_NUMBER, TASK_STATUS from RWR_LIST WHERE SERIAL_NUMBER=:P1 ORDER BY RWR_LIST_NBR';
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseWriter.Delete(PSerialNumber : integer);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.Text := 'DELETE FROM rwr_list WHERE serial_number=:p1';
      FUpdateQuery.ParamByName('P1').AsInteger := PSerialNumber;  FUpdateQuery.ExecSQL;

      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.Text := 'DELETE FROM status_info WHERE serial_number=:p1';
      FUpdateQuery.ParamByName('P1').AsInteger := PSerialNumber;  FUpdateQuery.ExecSQL;

      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.Text := 'DELETE FROM lic_package WHERE serial_number=:p1';
      FUpdateQuery.ParamByName('P1').AsInteger := PSerialNumber;  FUpdateQuery.ExecSQL;

      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.Text := 'DELETE FROM LIC_API_KEY WHERE serial_number=:p1';
      FUpdateQuery.ParamByName('P1').AsInteger := PSerialNumber;  FUpdateQuery.ExecSQL;

      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.Text := 'DELETE FROM lic_info WHERE serial_number=:p1';
      FUpdateQuery.ParamByName('P1').AsInteger := PSerialNumber;  FUpdateQuery.ExecSQL;

      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

destructor TSBLicenseWriter.Destroy;
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    if FLicInfo.Active then FLicInfo.Close;
    if FStatusInfo.Active then FStatusInfo.Close;
    if FLicPackage.Active then FLicPackage.Close;
    if FUpdateQuery.Active then FUpdateQuery.Close;

    FLicInfo.Transaction := nil;
    FLicInfo.Database := nil;
    FLicInfo.Free;

    FStatusInfo.Transaction := nil;
    FStatusInfo.Database := nil;
    FStatusInfo.Free;

    FLicPackage.Transaction := nil;
    FLicPackage.Database := nil;
    FLicPackage.Free;

    FUpdateQuery.Transaction := nil;
    FUpdateQuery.Database := nil;
    FUpdateQuery.Free;

    FRWRList.Transaction := nil;
    FRWRList.Database := nil;
    FRWRList.Free;

    if FTransaction.Active then FTransaction.Rollback;
    FTransaction.Free;
    FTransaction := nil;

    FDatabase := nil;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
  FCriticalSection := nil;

  inherited;
end;

procedure TSBLicenseWriter.FixSpoiledStatus(ASerialNumber: integer;
  ATime: TDateTime);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.text := 'UPDATE STATUS_INFO SET Connected=''0'' where SERIAL_NUMBER=:p1 and DATE_TIME_CONNECTED < :p2';
      FUpdateQuery.ParamByName('P1').AsInteger := ASerialNumber;
      FUpdateQuery.ParamByName('P2').AsDateTime := ATime;
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

function TSBLicenseWriter.InternalCheckInDatabase(
  PSerialNumber: integer): boolean;
begin
  result := false;
  FLicInfo.ParamByName('P1').AsInteger := PSerialNumber;
  FLicInfo.Open;
  if not FLicInfo.eof then result := true;
  FLicInfo.Close;
end;

procedure TSBLicenseWriter.InternalWrite(const PSBLicense: TSBLicense;
  PTypeOfWrite: integer);
label m1;
var
  tmp : TModuleInfo;
  tmp1 : TRWRTask;
  i : integer;
  cnt : integer;
begin
  if (PTypeOfWrite = 1) or (PTypeOfWrite = 0) then   // order is important!
  begin
    // LIC_INFO
    FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
    if InternalCheckInDatabase(PSBLicense.SerialNumber) then
      FUpdateQuery.SQL.Text := 'UPDATE LIC_INFO SET SERVICE_BUREAU_NAME=:P2, REPORT_GRACE_PERIOD=:P3, LICENSE_FREQUENCY=:P4, LICENSE_EXPIRATION=:P5 where SERIAL_NUMBER=:P1'
    else
      FUpdateQuery.SQL.Text := 'INSERT INTO LIC_INFO (SERIAL_NUMBER, SERVICE_BUREAU_NAME, REPORT_GRACE_PERIOD, LICENSE_FREQUENCY, LICENSE_EXPIRATION) VALUES (:P1, :P2, :P3, :P4, :P5)';
    FUpdateQuery.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FUpdateQuery.ParamByName('P2').AsString := PSBLicense.ServiceBureauName;
    FUpdateQuery.ParamByName('P3').AsInteger := PSBLicense.ReportGraCePeriod;
    FUpdateQuery.ParamByName('P4').AsInteger := PSBLicense.LicenseFrequency;
    FUpdateQuery.ParamByName('P5').AsInteger := PSBLicense.LicenseExpiration;
    FUpdateQuery.ExecSQL;
    // LIC_PACKAGE
    FLicPackage.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FLicPackage.Open;
    try
      // scan table and delete unnecessary rows
      FLicPackage.FetchAll;
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.text := 'DELETE from LIC_PACKAGE WHERE SERIAL_NUMBER=:P1 and PACKAGE_TYPE_NBR=:P2';
      while not FLicPackage.eof do
      begin
        // check in list, if there no item with 'Y' - delete
        for i := 0 to PSBLicense.ModuleInfos.Count-1 do
        begin
          if Assigned(PSBLicense.ModuleInfos.Items[i]) then
          begin
            tmp := TModuleInfo(PSBLicense.ModuleInfos.Items[i]);
            if (tmp.PackageTypeNbr = FLicPackage.FieldByName('PACKAGE_TYPE_NBR').AsInteger) and (tmp.Value = 'Y') then goto m1;
          end;
        end;
        FUpdateQuery.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
        FUpdateQuery.ParamByName('P2').AsInteger := FLicPackage.FieldByName('PACKAGE_TYPE_NBR').AsInteger;
        FUpdateQuery.ExecSQL;
m1:     FLicPackage.Next;
      end;
      // add new rows
      FLicPackage.Close; FLicPackage.Open; FLicPackage.FetchAll; // it's refresh
      for i := 0 to PSBLicense.ModuleInfos.Count-1 do
      begin
        if Assigned(PSBLicense.ModuleInfos.Items[i]) then
        begin
          tmp := TModuleInfo(PSBLicense.ModuleInfos.Items[i]);
          if (tmp.Value = 'Y') then
            if not FLicPackage.Locate('PACKAGE_TYPE_NBR', tmp.PackageTypeNbr, []) then
            begin
              // insert
              FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
              FUpdateQuery.SQL.text := 'INSERT INTO LIC_PACKAGE (SERIAL_NUMBER, PACKAGE_TYPE_NBR) VALUES(:P1, :P2)';
              FUpdateQuery.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
              FUpdateQuery.ParamByName('P2').AsInteger := tmp.PackageTypeNbr;
              FUpdateQuery.ExecSQL;
            end;
          end;
      end;  // for

    finally
      FlicPackage.Close;
    end;
  end;

  if (PTypeOfWrite = 2) or (PTypeOfWrite = 0) then   // order is important!
  begin
    // STATUS_INFO
    if not InternalCheckInDatabase(PSBLicense.SerialNumber) then
      raise SBLicenseWriterException.Create('Error! Cannot save STATUS_INFO because there''s no record in LIC_INFO! SerialNumber# ' + IntToStr(PSBLicense.SerialNumber));
    FStatusInfo.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FStatusInfo.Open;
    try
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      if FStatusInfo.eof then
      begin
        if PSBLicense.IsStatusInfoEmpty then exit; // nothing to insert
        FUpdateQuery.SQL.Text := 'INSERT INTO STATUS_INFO (SERIAL_NUMBER, CONNECTED, ' +
          'DATABASE_VERSION, DATE_TIME_CONNECTED, LAST_ADRESS, ' +
          'LICENSE_UPDATED, REPORT_RAN, REPORT_STATUS, REPORT_UPDATED, '+
          'SERVER_VERSION, SERVER_VERSION_SENT, DATABASE_VERSION_SENT, LICENSE_UPDATE_STATUS) ' +
          'VALUES (:P_SERIAL_NUMBER, :P_CONNECTED, :P_DATABASE_VERSION, ' +
          ':P_DATE_TIME_CONNECTED, :P_LAST_ADRESS, :P_LICENSE_UPDATED, :P_REPORT_RAN, ' +
          ':P_REPORT_STATUS, :P_REPORT_UPDATED, :P_SERVER_VERSION, :P_SERVER_VERSION_SENT, ' +
          ':P_DATABASE_VERSION_SENT, :P_LICENSE_UPDATE_STATUS)';
      end
      else
        FUpdateQuery.SQL.Text := 'UPDATE STATUS_INFO SET CONNECTED=:P_CONNECTED, DATABASE_VERSION=:P_DATABASE_VERSION,  ' +
          'DATE_TIME_CONNECTED = :P_DATE_TIME_CONNECTED, LAST_ADRESS=:P_LAST_ADRESS, ' +
          'LICENSE_UPDATED=:P_LICENSE_UPDATED, REPORT_RAN=:P_REPORT_RAN, REPORT_STATUS=:P_REPORT_STATUS, ' +
          'REPORT_UPDATED=:P_REPORT_UPDATED, SERVER_VERSION=:P_SERVER_VERSION, ' +
          'SERVER_VERSION_SENT=:P_SERVER_VERSION_SENT, DATABASE_VERSION_SENT=:P_DATABASE_VERSION_SENT, LICENSE_UPDATE_STATUS=:P_LICENSE_UPDATE_STATUS ' +
          'WHERE SERIAL_NUMBER=:P_SERIAL_NUMBER';
      FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := PSBLicense.SerialNumber;
      FUpdateQuery.ParamByName('P_CONNECTED').Value := PSBLicense.Connected;
      FUpdateQuery.ParamByName('P_DATABASE_VERSION').Value := PSBLicense.DatabaseVersion;
      FUpdateQuery.ParamByName('P_DATE_TIME_CONNECTED').Value := PSBLicense.DateTimeConnected;
      FUpdateQuery.ParamByName('P_LAST_ADRESS').Value := PSBLicense.LastAddress;
      FUpdateQuery.ParamByName('P_LICENSE_UPDATED').Value := PSBLicense.LicenseUpdated;
      FUpdateQuery.ParamByName('P_REPORT_RAN').Value := PSBLicense.ReportRan;
      FUpdateQuery.ParamByName('P_REPORT_STATUS').Value := PSBLicense.ReportStatus;
      FUpdateQuery.ParamByName('P_REPORT_UPDATED').Value := PSBLicense.ReportUpdated;
      FUpdateQuery.ParamByName('P_SERVER_VERSION').Value := PSBLicense.ServerVersion;
      FUpdateQuery.ParamByName('P_SERVER_VERSION_SENT').Value := PSBLicense.ServerVersionSent;
      FUpdateQuery.ParamByName('P_DATABASE_VERSION_SENT').Value := PSBLicense.DatabaseVersionSent;
      FUpdateQuery.ParamByName('P_LICENSE_UPDATE_STATUS').Value := PSBLicense.LicenseUpdateStatus;
      FUpdateQuery.ExecSQL;
    finally
      FStatusInfo.Close;
    end;
  end;

  // this code NEVER CHANGE STATUS FROM "1" TO "0" and NEVER DELETE ROWS.
  // it can only add a new rows or change status from "0" to "1"
  // and there's cannot be 2 unfinished tasks
  if (PTypeOfWrite = 3) or (PTypeOfWrite = 0) then // order is important
  begin
    // checking list
    cnt := 0;
    for i := 0 to PSBLicense.RWRTasks.Count-1 do
      if Assigned(PSBLicense.RWRTasks[i]) then
        if TRWRTask(PSBLicense.RWRTasks[i]).TaskStatus = '0' then Inc(cnt);
    if cnt > 1 then
      raise SBLicenseWriterException.Create('Error! Cannot save more then 1 unfinished report upload task! SerialNumber# ' + IntToStr(PSBLicense.SerialNumber));

    // saving
    FRWRList.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FRWRList.Open;
    try
      FRWRList.FetchAll;
      with PSBLicense do
      begin
        for i := 0 to RWRTasks.Count-1 do
          if Assigned(RWRTasks[i]) then
          begin
            tmp1 := TRWRTask(RWRTasks[i]);
            FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
            if FRWRList.Locate('RWR_LIST_NBR', tmp1.RWRListNbr, []) then
            begin
              // update
              if FRWRList['TASK_STATUS'] = '1' then continue; // don't change '1' status
              FUpdateQuery.SQL.Text := 'UPDATE RWR_LIST SET ' +
                'SERIAL_NUMBER=:P_SERIAL_NUMBER, REPORT_NUMBER=:P_REPORT_NUMBER, TASK_STATUS=:P_TASK_STATUS '+
                'WHERE RWR_LIST_NBR=:P_RWR_LIST_NBR';
            end
            else
            begin
              // insert
              FUpdateQuery.SQL.Text := 'INSERT INTO RWR_LIST (RWR_LIST_NBR, SERIAL_NUMBER, REPORT_NUMBER, TASK_STATUS) ' +
                'VALUES(:P_RWR_LIST_NBR, :P_SERIAL_NUMBER, :P_REPORT_NUMBER, :P_TASK_STATUS)';
            end;
            FUpdateQuery.ParamByName('P_RWR_LIST_NBR').AsInteger := tmp1.RWRListNbr;
            FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := tmp1.SerialNumber;
            FUpdateQuery.ParamByName('P_REPORT_NUMBER').AsInteger := tmp1.ReportNumber;
            FUpdateQuery.ParamByName('P_TASK_STATUS').AsString := tmp1.TaskStatus;
            FUpdateQuery.ExecSQL;
          end;
      end;  // with
    finally
      FRWRList.Close;
    end;
  end;
end;

procedure TSBLicenseWriter.MarkAllAsDisconnected;
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.text := 'UPDATE STATUS_INFO SET Connected=''0''';
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseWriter.PostDatabaseEvent(AEventName: String);
begin
  if (AEventName <> SRV_STATUS_CHANGED) and (AEventName <> SRV_RWR_CHANGED)
    and (AEventName <> CL_LIC_CHANGED) and (AEventName <> CL_STATUS_CHANGED)
    and (AEventName <> CL_RWR_CHANGED) and (AEventName <> CL_LIC_UPD_CHANGED) then
      raise SBLicenseWriterException.Create('Error! Unknown event name!');

  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FUpdateQuery.Active := false;
      FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.Text := 'execute procedure P_POST_EVENT(:P1);';
      FUpdateQuery.ParamByName('P1').AsString := AEventName;
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseWriter.Write(const PSBLicense: TSBLicense;
  pRefreshType: SBLicenseWriterRefreshType);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      if AllInfo in pRefreshType then
      begin
        InternalWrite(PSBLicense, 0);
        FTransaction.Commit;
        exit;
      end;
      if LicInfo in pRefreshType then
        InternalWrite(PSBLicense, 1);
      if StatusInfo in pRefreshType then
        InternalWrite(PSBLicense, 2);
      if RWRTasks in pRefreshType then
        InternalWrite(PSBLicense, 3);
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseWriter.WriteAll(const PSBLicense: TSBLicense);
begin
  Write(PSBLicense, [AllInfo]);
end;

procedure TSBLicenseWriter.WriteLicInfo(const PSBLicense: TSBLicense);
begin
  Write(PSBLicense, [LicInfo]);
end;

procedure TSBLicenseWriter.WriteOnlyReportStatus(PSerialNumber : integer; PReportStatus : String);
var
  RecordExists : boolean;
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      try
        FStatusInfo.ParamByName('P1').AsInteger := PSerialNumber;
        FStatusInfo.Open;
        RecordExists := not FStatusInfo.eof;
      finally
        FStatusInfo.Close;
      end;
      FUpdateQuery.Close;
      if RecordExists then
      begin
        FUpdateQuery.SQL.Text := 'UPDATE STATUS_INFO SET REPORT_STATUS=:P_REPORT_STATUS WHERE SERIAL_NUMBER=:P_SERIAL_NUMBER';
        FUpdateQuery.ParamByName('P_REPORT_STATUS').ASString := PReportStatus;
        FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := PSerialNumber;
      end
      else
      begin
        FUpdateQuery.SQL.Text := 'INSERT INTO STATUS_INFO (SERIAL_NUMBER, REPORT_STATUS, CONNECTED) VALUES(:P_SERIAL_NUMBER, :P_REPORT_STATUS, ''0'')';
        FUpdateQuery.ParamByName('P_REPORT_STATUS').ASString := PReportStatus;
        FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := PSerialNumber;
      end;
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseWriter.WriteOnlyLicenseUpdateStatus(PSerialNumber: integer;
  PLicenseUpdateStatus: String);
var
  RecordExists : boolean;
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FStatusInfo.ParamByName('P1').AsInteger := PSerialNumber;
      FStatusInfo.Open;
      try
        if (not FStatusInfo.eof) and (FStatusInfo['LICENSE_UPDATE_STATUS'] <> null) then
          if (PLicenseUpdateStatus = '2') and (FStatusInfo['LICENSE_UPDATE_STATUS'] = '1') then
            // cannot force again if it's already being updated
            raise SBLicenseWriterException.Create('You cannot force license update process again if license''s already being updated!');
      finally
        FStatusInfo.Close;
      end;
      try
        FStatusInfo.ParamByName('P1').AsInteger := PSerialNumber;
        FStatusInfo.Open;
        RecordExists := not FStatusInfo.eof;
      finally
        FStatusInfo.Close;
      end;
      FUpdateQuery.Close;
      if RecordExists then
      begin
        FUpdateQuery.SQL.Text := 'UPDATE STATUS_INFO SET LICENSE_UPDATE_STATUS=:P_LICENSE_UPDATE_STATUS WHERE SERIAL_NUMBER=:P_SERIAL_NUMBER';
        FUpdateQuery.ParamByName('P_LICENSE_UPDATE_STATUS').ASString := PLicenseUpdateStatus;
        FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := PSerialNumber;
      end
      else
      begin
        FUpdateQuery.SQL.Text := 'INSERT INTO STATUS_INFO (SERIAL_NUMBER, LICENSE_UPDATED_STATUS, CONNECTED) VALUES(:P_SERIAL_NUMBER, :P_LICENSE_UPDATE_STATUS, ''0'')';
        FUpdateQuery.ParamByName('P_REPORT_STATUS').ASString := PLicenseUpdateStatus;
        FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := PSerialNumber;
      end;
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseWriter.WriteRWRReport(PRWRListNBR: integer;
  PStream: TStream);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  if not Assigned(PStream) then
    raise SBLicenseWriterException.Create('Error! BLOB stream not assigned!');
  try
    FTransaction.StartTransaction;
    try
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.text := 'UPDATE rwr_list SET REPORT_FILE = :P_REPORT_FILE WHERE RWR_LIST_NBR=:P_RWR_LIST_NBR';
      FUpdateQuery.ParamByName('P_RWR_LIST_NBR').AsInteger := PRWRListNBR;
      FUpdateQuery.ParamByName('P_REPORT_FILE').LoadFromStream(PStream, ftBlob);
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

// this procedure NEVER CHANGE STATUS FROM "1" TO "0" and NEVER DELETE ROWS.
// it can only add a new rows or change status from "0" to "1"
procedure TSBLicenseWriter.WriteRWRTasks(const PSBLicense: TSBLicense);
begin
  Write(PSBLicense, [RWRTasks]);
end;

procedure TSBLicenseWriter.WriteStatusInfo(const PSBLicense: TSBLicense);
begin
  Write(PSBLicense, [StatusInfo]);
end;

procedure TSBLicenseWriter.WriteStatusReport(PSerialNumber: integer;
  PStream: TStream);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  if not Assigned(PStream) then
    raise SBLicenseWriterException.Create('Error! BLOB stream not assigned!');
  try
    FTransaction.StartTransaction;
    try
      FUpdateQuery.Close; FUpdateQuery.SQL.Clear;
      FUpdateQuery.SQL.text := 'UPDATE status_info SET REPORT_FILE = :P_REPORT_FILE WHERE SERIAL_NUMBER=:P_SERIAL_NUMBER';
      FUpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := PSerialNumber;
      FUpdateQuery.ParamByName('P_REPORT_FILE').LoadFromStream(PStream, ftBlob);
      FUpdateQuery.ExecSQL;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

end.
