object GeneratorForm: TGeneratorForm
  Left = 348
  Top = 133
  BorderStyle = bsDialog
  Caption = 'License generator'
  ClientHeight = 507
  ClientWidth = 588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 66
    Height = 13
    Caption = 'Serial Number'
  end
  object Label3: TLabel
    Left = 8
    Top = 48
    Width = 72
    Height = 13
    Caption = 'Expiration Date'
  end
  object Label4: TLabel
    Left = 8
    Top = 88
    Width = 85
    Height = 13
    Caption = 'Computer Modifier'
  end
  object Label5: TLabel
    Left = 144
    Top = 8
    Width = 37
    Height = 13
    Caption = 'License'
  end
  object Bevel1: TBevel
    Left = 10
    Top = 144
    Width = 495
    Height = 4
    Shape = bsTopLine
  end
  object Label6: TLabel
    Left = 10
    Top = 150
    Width = 64
    Height = 13
    Caption = 'License code'
  end
  object SpeedButton1: TSpeedButton
    Left = 558
    Top = 175
    Width = 23
    Height = 22
    Hint = 'Copy code to clipboard'
    Glyph.Data = {
      42010000424D4201000000000000760000002800000011000000110000000100
      040000000000CC00000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888800000008888888888888888800000008888888444444444800000008888
      8884FFFFFFF48000000088888884F00000F48000000080000004FFFFFFF48000
      000080FFFFF4F00000F48000000080F00004FFFFFFF48000000080FFFFF4F00F
      44448000000080F00004FFFF4F488000000080FFFFF4FFFF44888000000080F0
      0F04444448888000000080FFFF0F088888888000000080FFFF00888888888000
      0000800000088888888880000000888888888888888880000000888888888888
      888880000000}
    OnClick = SpeedButton1Click
  end
  object edSerialNumber: TEdit
    Left = 8
    Top = 24
    Width = 129
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 0
    Text = '0'
    OnChange = edSerialNumberChange
  end
  object dpExpDate: TDateTimePicker
    Left = 8
    Top = 64
    Width = 129
    Height = 21
    Date = 37266.751407870400000000
    Time = 37266.751407870400000000
    TabOrder = 1
  end
  object edModifier: TEdit
    Left = 8
    Top = 104
    Width = 129
    Height = 21
    TabOrder = 2
    OnChange = edSerialNumberChange
  end
  object cbLicense: TCheckListBox
    Left = 144
    Top = 24
    Width = 121
    Height = 103
    Color = clBtnFace
    Enabled = False
    ItemHeight = 13
    TabOrder = 3
  end
  object BtnGenerate: TButton
    Left = 232
    Top = 475
    Width = 137
    Height = 25
    Hint = 'Display code generation dialog'
    Caption = '&Generate Code'
    Default = True
    Enabled = False
    TabOrder = 4
    OnClick = BtnGenerateClick
  end
  object CodeMemo: TMemo
    Left = 8
    Top = 168
    Width = 545
    Height = 297
    Color = clBtnFace
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 5
    WantReturns = False
  end
end
