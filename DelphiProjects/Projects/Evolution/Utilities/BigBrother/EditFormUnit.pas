unit EditFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, SBLicenseReaderUnit, SBLicenseUnit,
  SBLicenseWriterUnit, isVCLBugFix, Grids, DBGrids, DBCtrls;

type
  TEditForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    SaveButton: TButton;
    CancelButton: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    SNEdit: TEdit;
    SBNEdit: TEdit;
    RGPEdit: TEdit;
    LEEdit: TEdit;
    LFEdit: TEdit;
    GroupBox1: TGroupBox;
    LicList: TListBox;
    AllowedList: TListBox;
    AddButton: TButton;
    RemoveButton: TButton;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    APIKeysGrid: TDBGrid;
    DBNavigator1: TDBNavigator;
    procedure SaveButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure AddButtonClick(Sender: TObject);
    procedure RemoveButtonClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentSerialNumber: integer;
    FAddingFlag: boolean;
    Lic : TSBLicense;
    procedure SetCurrentSerialNumber(const Value: integer);
    procedure SetAddingFlag(const Value: boolean);
  public
    { Public declarations }
    Reader : TSBLicenseReader;
    Writer : TSBLicenseWriter;
    property CurrentSerialNumber : integer read FCurrentSerialNumber write SetCurrentSerialNumber;
    procedure ShowForEditorView;
    property AddingFlag : boolean read FAddingFlag write SetAddingFlag;
    procedure ShowForAdd;
    function CheckEditRights : boolean; // true if editing is enabled
    procedure MoveFromLicToEdits;
    procedure MoveFromEditToLic;
    procedure DeleteLic(PSerialNumber : integer);
    procedure FillLicList;
    function GetPackageTypeNbr(pName : String) : integer; // extract nbr from string like "0 - ...."
    procedure OpenAPIKeys(ASerialNumber : integer);
  end;

var
  EditForm: TEditForm;

implementation

uses BBConfigDataUnit;

{$R *.dfm}

{ TEditForm }

// true if editing is enabled
function TEditForm.CheckEditRights : boolean;
begin
  result := not BBConfigData.EditIsDisabled;
  if result then begin
    Caption := 'Editing the license #' + IntToStr(CurrentSerialNumber);
    SaveButton.Enabled := true;
    APIKeysGrid.ReadOnly := false;
  end
  else
  begin
    Caption := 'Viewing the license #' + IntToStr(CurrentSerialNumber);
    SaveButton.Enabled := false;
    APIKeysGrid.ReadOnly := true;
  end;
end;

procedure TEditForm.SetAddingFlag(const Value: boolean);
begin
  FAddingFlag := Value;
end;

procedure TEditForm.SetCurrentSerialNumber(const Value: integer);
begin
  FCurrentSerialNumber := Value;
end;

procedure TEditForm.ShowForAdd;
begin
  AddingFlag := true;
  SNEdit.Enabled := true;
  if CheckEditRights then
  begin
    CurrentSerialNumber := -1;
    OpenAPIKeys(CurrentSerialNumber);
    Caption := 'Adding a new license';
    if Assigned(Lic) then
      Lic.Free;
    Lic := Reader.NewEmptySBLicense(-1);
    MoveFromLicToEdits;
    try
      ShowModal;
      if Modalresult <> mrOk then
        try
          BBConfigData.IBLicAPIKeys.CancelUpdates;
          BBConfigData.IBManualTransactions.Rollback;
        except
          on E:Exception do
          begin
            BBConfigData.IBLicAPIKeys.CancelUpdates;
            BBConfigData.IBManualTransactions.Rollback;
          end;
        end;
      BBConfigData.IBAPIKeys.Active := false;
      BBConfigData.IBLicAPIKeys.Active := false;
    finally
      if BBConfigData.IBManualTransactions.Active then
         BBConfigData.IBManualTransactions.Rollback;
    end;
  end
  else
    ShowMessage('You have no rights to add new licenses!');
end;

procedure TEditForm.ShowForEditorView;
begin
  AddingFlag := false;
  SNEdit.Enabled := false;
  if BBConfigData.IBLicInfo['SERIAL_NUMBER'] <> null then
  begin
    CurrentSerialNumber := BBConfigData.IBLicInfo['SERIAL_NUMBER'];
    OpenAPIKeys(CurrentSerialNumber);
    CheckEditRights;
    if Assigned(Lic) then
      Lic.Free;
    Lic := Reader.Read(CurrentSerialNumber);
    MoveFromLicToEdits;
    try
      ShowModal;
      if Modalresult <> mrOk then
        try
          BBConfigData.IBLicAPIKeys.CancelUpdates;
          BBConfigData.IBManualTransactions.Rollback;
        except
          on E:Exception do
          begin
            BBConfigData.IBLicAPIKeys.CancelUpdates;
            BBConfigData.IBManualTransactions.Rollback;
          end;
        end;
      BBConfigData.IBAPIKeys.Active := false;
      BBConfigData.IBLicAPIKeys.Active := false;
    finally
      if BBConfigData.IBManualTransactions.Active then
         BBConfigData.IBManualTransactions.Rollback;
    end;
  end;
end;

procedure TEditForm.SaveButtonClick(Sender: TObject);
var
 i : integer;
begin
  // checking for right numbers
  if not TryStrToInt(SNEdit.Text, i) then
  begin
    ShowMessage('Error! Serial number should be a number!');
    Modalresult := mrNone;
    AbortEx;
  end;
  if not TryStrToInt(RGPEdit.Text, i) then
  begin
    ShowMessage('Error! ReportGrace period should be a number!');
    Modalresult := mrNone;
    AbortEx;
  end;
  if not TryStrToInt(LEEdit.Text, i) then
  begin
    ShowMessage('Error! License expiration should be a number!');
    Modalresult := mrNone;
    AbortEx;
  end;
  if not TryStrToInt(LFEdit.Text, i) then
  begin
    ShowMessage('Error! License frequency should be a number!');
    Modalresult := mrNone;
    AbortEx;
  end;

  // more checking if adding mode
  if AddingFlag then
  begin
    if Reader.CheckInDatabase(StrToInt(SNEdit.Text)) then
    begin
      ShowMessage('Error! You should type a new unique serial number!');
      Modalresult := mrNone;
      AbortEx;
    end;
  end;

  MoveFromEditToLic;
  Writer.WriteLicInfo(Lic);

  if not AddingFlag then
    try
      BBConfigData.IBLicAPIKeys.ApplyUpdates;
      BBConfigData.IBManualTransactions.Commit;
    except
      on E:Exception do
      begin
        ModalResult := mrNone;
        ShowMessage('Cannot save changes of allowed API keys. Error: ' + E.Message);
      end;
    end;

  Writer.PostDatabaseEvent(CL_LIC_CHANGED);
  CurrentSerialNumber := Lic.SerialNumber;
end;

procedure TEditForm.FormCreate(Sender: TObject);
begin
  Reader := TSBLicenseReader.Create(BBConfigData.IBDatabase, nil);
  Writer := TSBLicenseWriter.Create(BBConfigData.IBDatabase, nil);
end;

procedure TEditForm.FormDestroy(Sender: TObject);
begin
  Reader.Free;
  Writer.Free;
  if Assigned(Lic) then Lic.Free;
end;

procedure TEditForm.MoveFromLicToEdits;
var
  tmp : TModuleInfo;
  i , j : integer;
begin
  if not AddingFlag then
    SNEdit.text := IntToStr(Lic.SerialNumber)
  else
    SNEdit.text := '';
  SBNEdit.text := Lic.ServiceBureauName;
  RGPEdit.text := IntToStr(Lic.ReportGraCePeriod);
  LEEdit.text := IntToStr(Lic.LicenseExpiration);
  LFEdit.Text := IntToStr(Lic.LicenseFrequency);

  // licenses
  LicList.Items.Clear;
  AllowedList.Items.Clear;
  FillLicList;
  for i := 0 to Lic.ModuleInfos.Count - 1 do
  begin
    tmp := TModuleInfo(Lic.ModuleInfos[i]);
    if tmp.Value = 'Y' then
      for j := LicList.Count - 1 downto 0 do
      begin
        if tmp.PackageTypeNbr = GetpackageTypeNbr(LicList.Items[j]) then
        begin
          LicList.ItemIndex := j;
          Addbutton.Click;
          break;
        end;
      end;  // for j
  end;  // for i
end;

procedure TEditForm.FormActivate(Sender: TObject);
begin
  if AddingFlag then
    SNEdit.SetFocus
  else
    SBNEdit.SetFocus;
end;

procedure TEditForm.MoveFromEditToLic;
var
  tmp : TModuleInfo;
  i , j : integer;
begin
   Lic.SerialNumber := StrToInt(SNEdit.Text);
   Lic.ServiceBureauName := SBNEdit.Text;
   Lic.ReportGraCePeriod := StrToInt(RGPEdit.Text);
   Lic.LicenseFrequency := StrToInt(LFEdit.Text);
   Lic.LicenseExpiration := StrToInt(LEEdit.Text);

   // licenses
  for i := 0 to Lic.ModuleInfos.Count - 1 do
  begin
    tmp := TModuleInfo(Lic.ModuleInfos[i]);
    tmp.Value := 'N';
    for j := 0  to AllowedList.Count - 1 do
    begin
      if tmp.PackageTypeNbr = GetpackageTypeNbr(AllowedList.Items[j]) then
      begin
        tmp.Value := 'Y';
        break;
      end;
    end;  // for j
  end;  // for i
end;

procedure TEditForm.DeleteLic(PSerialNumber: integer);
begin
  Writer.Delete(PSerialNumber);
end;

procedure TEditForm.AddButtonClick(Sender: TObject);
begin
  if LicList.Items.Count > 0 then
    if LicList.ItemIndex <> -1 then
    begin
      AllowedList.Items.Add(LicList.Items[LicList.ItemIndex]);
      LicList.Items.Delete(LicList.ItemIndex);
    end
    else
    begin
      ShowMessage('Please, select item to add!');
    end;
end;

procedure TEditForm.RemoveButtonClick(Sender: TObject);
begin
  if AllowedList.Items.Count > 0 then
    if AllowedList.ItemIndex <> -1 then
    begin
      LicList.Items.Add(AllowedList.Items[AllowedList.ItemIndex]);
      AllowedList.Items.Delete(AllowedList.ItemIndex);
    end
    else
    begin
      ShowMessage('Please, select item to remove!');
    end;
end;

procedure TEditForm.FillLicList;
begin
  with BBConfigData do
  begin
    IBPackageType.First;
    LicList.Items.Clear;
    while not IBPackageType.eof do
    begin
      LicList.Items.Add(IBPackageType.FieldByName('Package_type_nbr').AsString + ' - ' + IBPackageType.FieldByName('Long_description').AsString);
      IBPackageType.Next;
    end;
  end;
end;

function TEditForm.GetPackageTypeNbr(pName: String): integer;
var
  i : integer;
  tmp : String;
begin
  i := Pos('-', pName);
  if i = 0 then
    raise Exception.Create('String contains no package_type_nbr');
  tmp := Trim(Copy(pName, 1, i-1));
  if not TryStrToInt( tmp, i) then
    raise Exception.Create('String contains no package_type_nbr');
  result := StrToInt(tmp);
end;

procedure TEditForm.OpenAPIKeys(ASerialNumber: integer);
begin
  if BBConfigData.EditIsDisabled then
    BBConfigData.IBLicAPIKeys.UpdateObject := nil
  else
    BBConfigData.IBLicAPIKeys.UpdateObject := BBConfigData.IBLicAPIKeysUpdate;
  if ASerialNumber <> - 1 then
  begin
    BBConfigData.IBManualTransactions.StartTransaction;
    BBConfigData.IBAPIKeys.Active := false;
    BBConfigData.IBAPIKeys.Active := true;
    BBConfigData.IBLicAPIKeys.ParamByName('P1').AsInteger := ASerialNumber;
    BBConfigData.IBLicAPIKeys.Active := false;
    BBConfigData.IBLicAPIKeys.Active := true;
  end;  
end;

end.

