object UpdateReportForm: TUpdateReportForm
  Left = 288
  Top = 196
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Upload system report'
  ClientHeight = 150
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 64
    Height = 13
    Caption = 'Serial number'
  end
  object Label2: TLabel
    Left = 16
    Top = 43
    Width = 70
    Height = 13
    Caption = 'Report number'
  end
  object Label3: TLabel
    Left = 16
    Top = 76
    Width = 61
    Height = 13
    Caption = 'Attach report'
  end
  object SerialNumberLabel: TLabel
    Left = 96
    Top = 16
    Width = 63
    Height = 13
    Caption = 'SerialNumber'
  end
  object ReportNumberEdit: TEdit
    Left = 96
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 0
  end
  object ReportFileNameEdit: TEdit
    Left = 96
    Top = 72
    Width = 369
    Height = 21
    TabOrder = 1
  end
  object BrowseButton: TButton
    Left = 472
    Top = 71
    Width = 75
    Height = 25
    Caption = 'Browse'
    TabOrder = 2
    OnClick = BrowseButtonClick
  end
  object PostButton: TButton
    Left = 184
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Post'
    ModalResult = 1
    TabOrder = 3
    OnClick = PostButtonClick
  end
  object CancelButton: TButton
    Left = 288
    Top = 112
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object TasksButton: TButton
    Left = 432
    Top = 112
    Width = 105
    Height = 25
    Caption = 'Show tasks list...'
    TabOrder = 5
    OnClick = TasksButtonClick
  end
  object OpenFile: TOpenDialog
    Filter = 'Reports (*.rwr)|*.rwr'
    Left = 120
    Top = 104
  end
end
