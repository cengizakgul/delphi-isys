object VersionChoiseForm: TVersionChoiseForm
  Left = 243
  Top = 114
  BorderStyle = bsDialog
  Caption = 'Please, select version'
  ClientHeight = 65
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Rb1: TRadioButton
    Left = 16
    Top = 8
    Width = 113
    Height = 17
    Caption = 'Killington'
    TabOrder = 0
  end
  object Rb2: TRadioButton
    Left = 16
    Top = 32
    Width = 113
    Height = 17
    Caption = 'Lincoln'
    Checked = True
    TabOrder = 1
    TabStop = True
  end
  object Button1: TButton
    Left = 160
    Top = 16
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
end
