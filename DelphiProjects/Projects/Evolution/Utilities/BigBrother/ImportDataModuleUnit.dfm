object ImportDataModule: TImportDataModule
  OldCreateOrder = False
  Left = 324
  Top = 215
  Height = 223
  Width = 559
  object IBDatabase: TIBDatabase
    DatabaseName = 'data:/db/programmer/michael/BB.gdb'
    Params.Strings = (
      'user_name=EUSER'
      'password=pps97')
    LoginPrompt = False
    DefaultTransaction = IBTransaction
    Left = 40
    Top = 8
  end
  object IBTransaction: TIBTransaction
    AllowAutoStart = False
    DefaultDatabase = IBDatabase
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 40
    Top = 72
  end
  object IBClearLogs: TIBScript
    Database = IBDatabase
    Transaction = IBTransaction
    Terminator = ';'
    Script.Strings = (
      'delete from log_lic_info;'
      'delete from log_lic_package')
    Left = 120
    Top = 8
  end
  object IBDeletePreviousInfo: TIBScript
    Database = IBDatabase
    Transaction = IBTransaction
    Terminator = ';'
    Script.Strings = (
      'delete from status_info;'
      'delete from lic_package;'
      'delete from rwr_list;'
      'delete from lic_info;')
    Left = 216
    Top = 8
  end
  object IBLicInfo: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      
        'insert into LIC_INFO (LICENSE_EXPIRATION, LICENSE_FREQUENCY, REP' +
        'ORT_GRACE_PERIOD, SERIAL_NUMBER, SERVICE_BUREAU_NAME)'
      
        'VALUES (:P_LICENSE_EXPIRATION, :P_LICENSE_FREQUENCY, :P_REPORT_G' +
        'RACE_PERIOD, :P_SERIAL_NUMBER, :P_SERVICE_BUREAU_NAME)')
    Left = 120
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P_LICENSE_EXPIRATION'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P_LICENSE_FREQUENCY'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P_REPORT_GRACE_PERIOD'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P_SERIAL_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'P_SERVICE_BUREAU_NAME'
        ParamType = ptInput
      end>
  end
  object IBStatusInfo: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      
        'insert into STATUS_INFO (CONNECTED, DATABASE_VERSION, DATE_TIME_' +
        'CONNECTED, LAST_ADRESS, LICENSE_UPDATED, REPORT_RAN, REPORT_STAT' +
        'US, REPORT_UPDATED, SERIAL_NUMBER, SERVER_VERSION, SERVER_VERSIO' +
        'N_SENT, DATABASE_VERSION_SENT)'
      
        'VALUES (:P_CONNECTED, :P_DATABASE_VERSION, :P_DATE_TIME_CONNECTE' +
        'D, :P_LAST_ADRESS, :P_LICENSE_UPDATED, :P_REPORT_RAN, :P_REPORT_' +
        'STATUS, :P_REPORT_UPDATED, :P_SERIAL_NUMBER, :P_SERVER_VERSION, ' +
        ':P_SERVER_VERSION_SENT, :P_DATABASE_VERSION_SENT)')
    Left = 200
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'P_CONNECTED'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'P_DATABASE_VERSION'
        ParamType = ptInput
      end
      item
        DataType = ftTimeStamp
        Name = 'P_DATE_TIME_CONNECTED'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'P_LAST_ADRESS'
        ParamType = ptInput
      end
      item
        DataType = ftTimeStamp
        Name = 'P_LICENSE_UPDATED'
        ParamType = ptInput
      end
      item
        DataType = ftTimeStamp
        Name = 'P_REPORT_RAN'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'P_REPORT_STATUS'
        ParamType = ptInput
      end
      item
        DataType = ftTimeStamp
        Name = 'P_REPORT_UPDATED'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P_SERIAL_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'P_SERVER_VERSION'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'P_SERVER_VERSION_SENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P_DATABASE_VERSION_SENT'
        ParamType = ptUnknown
      end>
  end
  object IBLicPackage: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      'insert into LIC_PACKAGE (PACKAGE_TYPE_NBR, SERIAL_NUMBER)'
      'VALUES(:P1, :P2)')
    Left = 288
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptInput
      end>
  end
  object IBDefaultInfo: TIBTable
    Database = IBDatabase
    Transaction = IBTransaction
    ReadOnly = True
    TableName = 'DEFAULT_INFO'
    Left = 352
    Top = 8
  end
  object IBPackageType: TIBTable
    Database = IBDatabase
    Transaction = IBTransaction
    FieldDefs = <
      item
        Name = 'DEFAULT_VALUE'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LONG_DESCRIPTION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 50
      end
      item
        Name = 'PACKAGE_TYPE_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'SHORT_DESCRIPTION'
        Attributes = [faRequired]
        DataType = ftString
        Size = 5
      end>
    IndexDefs = <
      item
        Name = 'IDX_PACKAGE_TYPE_NBR'
        Fields = 'PACKAGE_TYPE_NBR'
        Options = [ixUnique]
      end
      item
        Name = 'UQ_PACKAGE_TYPE_SHORT_DESCR'
        Fields = 'SHORT_DESCRIPTION'
        Options = [ixUnique]
      end
      item
        Name = 'PK_PACKAGE_TYPE'
        Fields = 'PACKAGE_TYPE_NBR'
        Options = [ixUnique]
      end>
    ReadOnly = True
    StoreDefs = True
    TableName = 'PACKAGE_TYPE'
    Left = 432
    Top = 8
  end
  object IBRWRList: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      
        'insert into RWR_LIST(RWR_LIST_NBR, REPORT_NUMBER, SERIAL_NUMBER,' +
        ' TASK_STATUS)'
      'values(GEN_ID(GEN_RWR_LIST_NBR,1),  :P_REPORT_NUMBER,'
      ':P_SERIAL_NUMBER, :P_TASK_STATUS);')
    Left = 360
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P_REPORT_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'P_SERIAL_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'P_TASK_STATUS'
        ParamType = ptInput
      end>
  end
  object UpdateQuery: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    Left = 440
    Top = 72
  end
end
