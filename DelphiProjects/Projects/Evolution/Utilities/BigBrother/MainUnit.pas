unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  IBEvents, NewBBUnit, ConnectionsPoolUnit, StdCtrls, ExtCtrls,
  ISErrorUtils, Clipbrd, {IDTCPServer,} EventReceiverUnit;

type
  TEvBigBrotherService10 = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceShutdown(Sender: TService);
  private
    { Private declarations }
    EventReceiver : IEventReceiver;
    procedure DoInit;
    procedure DoDeinit;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  EvBigBrotherService10: TEvBigBrotherService10;

implementation

uses MyTCPServerUnit, EvConsts, SBLicenseReaderUnit, SBLicenseUnit,
  SBLicenseWriterUnit;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  EvBigBrotherService10.Controller(CtrlCode);
end;

function TEvBigBrotherService10.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TEvBigBrotherService10.ServiceStart(Sender: TService; var Started: Boolean);
begin
  ServiceThread.Synchronize(ServiceThread, DoInit);
end;

procedure TEvBigBrotherService10.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  ServiceThread.Synchronize(ServiceThread, DoDeinit);
end;

procedure TEvBigBrotherService10.DoDeinit;
begin
  EventReceiver := nil;
  StopNewBB;
end;

procedure TEvBigBrotherService10.DoInit;
begin
  try
    RunNewBB;
    EventReceiver := TEventReceiver.Create;
  except
    on E:Exception do
    begin
      MessageBox(0, PChar('Cannot start BB server. Exception: ' + E.Message), 'Error', 0);
      ServiceThread.Terminate;
      raise;
    end;
  end;
end;

procedure TEvBigBrotherService10.ServiceExecute(Sender: TService);
begin
  while not Terminated do
    ServiceThread.ProcessRequests(True);
end;

procedure TEvBigBrotherService10.ServiceShutdown(Sender: TService);
begin
  ServiceThread.Synchronize(ServiceThread, DoDeinit);
end;

end.
