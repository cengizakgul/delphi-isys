unit LazyEmailNotifierUnit;

interface

uses
  DB,SysUtils, Controls, Classes, SyncObjs, Variants, isThreadManager, IsSMTPClient,
  MyTCPServerUnit, isLogFile;

const
  SleepConst = 500;

type

  ILazyEmailNotifier = interface
    ['{CD1A9190-51B6-47F6-8CCD-328E8199FDF7}']
    procedure Write(AMessage : String);
    procedure Flush;
  end;


  { this class sends e-mail notification periodically
    it collects messages in list, then sends all of them in one email message
    timestamp is added to message automatically on call Write method.
    Class can send to several addressees: just type theirs e-mails using semicolon
  }
  TLazyEmailNotifier = class(TInterfacedObject, ILazyEmailNotifier)
  private
    FTimeout : cardinal;
    FHost : String;
    FAddressees : String;
    FMessages : TStringList;
    FSubj : String;
    FFrom : String;
    FCritSection : TCriticalSection;
    FSendEMailId : TTaskId;
    FRowsLimit : cardinal;
    FEnabled : boolean;
    FFlush : boolean;
  protected
    procedure SenEMail(const Params: TTaskParamList);
  public
    procedure Write(AMessage : String);
    procedure Flush;
    constructor Create(AHost : String; AFrom: String; AAddressees : String; ASubj : String; ATimeout : cardinal; ARowsLimit : cardinal = 0; AEnableEMail : boolean = true);
    destructor Destroy; override;
  end;

implementation

uses NewBBUnit, GlovalObjectsUnit;

{ TLazyEmailNotifier }

constructor TLazyEmailNotifier.Create(AHost, AFrom, AAddressees: String; ASubj : String;
  ATimeout: cardinal; ARowsLimit : cardinal = 0; AEnableEMail : boolean = true);
begin
  FFlush := false;
  Fhost := AHost;
  FAddressees := AAddressees;
  FSubj := ASubj;
  FFrom := AFrom;
  FTimeout := ATimeout;
  FRowsLimit := ArowsLimit;
  FEnabled := AEnableEMail;
  FMessages := TStringList.Create;
  FCritSection := TCriticalSection.Create;
  if AHost = '' then
    raise Exception.Create('Cannot create EMailNotifier object. Host cannot be empty');
  if AAddressees = '' then
    raise Exception.Create('Cannot create EMailNotifier object. Addressees cannot be empty');
  if ATimeout = 0 then
    raise Exception.Create('Cannot create EMailNotifier object. Timeout cannot be zero');
  if FEnabled then
    FSendEmailId := GlobalThreadManager.RunTask(SenEMail, Self, MakeTaskParams([]), 'Email notifier');
end;

destructor TLazyEmailNotifier.Destroy;
begin
  GlobalThreadManager.TerminateTask(FSendEMailId);
  GlobalThreadManager.WaitForTaskEnd(FSendEmailId);

  FMessages.Clear;
  FMessages.Free;
  FCritSection.Free;
  inherited;
end;

procedure TLazyEmailNotifier.Flush;
begin
  FFlush := true;
end;

procedure TLazyEmailNotifier.SenEMail(const Params: TTaskParamList);
var
  tmpBody : String;
  i, StartMsg : integer;
  msecs : cardinal;
  Client: TIsSMTPClient;
begin
  msecs := 0;
  while not CurrentThreadTaskTerminated do
  begin
    sleep(SleepConst);
    if (msecs >= FTimeout) or (CurrentThreadTaskTerminated) or FFlush then
    begin
      FFlush := false;
      msecs := 0;
      FCritSection.Enter;
      try
        try
          tmpBody := '';
          if (FRowsLimit = 0) or (FrowsLimit >= Cardinal(FMessages.Count)) then
            StartMsg := 0
          else
            StartMsg := Cardinal(FMessages.Count) - FRowsLimit;

          for i := StartMsg to FMessages.Count - 1 do begin
            tmpBody := tmpBody + FMessages.Strings[i] + #13#10;
          end;
          if StartMsg <> 0 then
            tmpBody := tmpBody + #13#10 + 'Attention! Only ' + intToStr(FRowsLimit) + ' last messages has been sent!';
          if FMessages.Count > 0 then
          begin
            Client := TIsSMTPClient.Create(FHost);
            try
              Client.SendQuickMessage(FFrom, FAddressees, FSubj, tmpBody);
            finally
              Client.Free;
            end;
            FMessages.Clear;
          end;
        except
          on E:Exception do
            GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in E-Mail notifier', 'Exception in E-Mail notifier: ' + E.Message);
        end;
      finally
        FCritSection.Leave;
      end;
    end
    else
      msecs := msecs +  SleepConst;
  end;  // while
end;

procedure TLazyEmailNotifier.Write(AMessage: String);
begin
  if FEnabled then
  begin
    FCritSection.Enter;
    try
      FMessages.Add(FormatDateTime('YYYY/MM/DD-HH:NN:SS:ZZZ', Now) + ' ' + AMessage);
    finally
      FCritSection.Leave;
    end;
  end;
end;

end.
