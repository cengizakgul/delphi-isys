unit ImportCfgFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IBDatabaseINI, IniFiles, DB;

type
  TImportCFGForm = class(TForm)
    FilePathEdit: TEdit;
    Label1: TLabel;
    ChooseFileButton: TButton;
    StartButton: TButton;
    OpenDialog: TOpenDialog;
    ClearLogsCB: TCheckBox;
    UploadReportsButton: TButton;
    UploadReportDialog: TOpenDialog;
    ClearDatabaseCB: TCheckBox;
    procedure ChooseFileButtonClick(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure UploadReportsButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportCFGForm: TImportCFGForm;

implementation

uses ImportDataModuleUnit;

{$R *.dfm}

procedure TImportCFGForm.ChooseFileButtonClick(Sender: TObject);
begin
   if OpenDialog.Execute then
     FilePathEdit.Text := OpenDialog.Filename;
end;

procedure TImportCFGForm.StartButtonClick(Sender: TObject);
var
  CFGFile : TIniFile;

  procedure ImportData;
  var
    Sections : TStringList;
    i, j, Code : integer;
    SectionNumber : integer;
    count : integer;
    tmpDate : TDate;
    tmpDateTime : TDateTime;
    tmpString : String;
    SBSection : TStringList;
    Nm : String;
    ReportNumber : integer;
  begin
    Sections := TStringList.Create;
    SBSection := TStringList.Create;
    try
      CFGFile.ReadSections(Sections);
      count := 0;
      for i := 0 to Sections.Count - 1 do
      begin
        Val(Sections[i], SectionNumber, Code);
        if  Code <> 0 then
        begin
           ShowMessage('Section ' + Sections[i] + ' will be skipped');
           continue;
        end; // if

        Inc(Count);
        with ImportDataModule do
        begin
          // LIC_INFO Table
          IBLicInfo.ParamByName('P_LICENSE_EXPIRATION').AsInteger := CFGFile.ReadInteger(Sections[i], 'LicenseExpiration', IBDefaultInfo['LICENSE_EXPIRATION']);
          IBLicInfo.ParamByName('P_LICENSE_FREQUENCY').AsInteger := CFGFile.ReadInteger(Sections[i], 'LicenseFrequency', IBDefaultInfo['LICENSE_FREQUENCY']);
          IBLicInfo.ParamByName('P_REPORT_GRACE_PERIOD').AsInteger := CFGFile.ReadInteger(Sections[i], 'ReportGracePeriod', IBDefaultInfo['REPORT_GRACE_PERIOD']);
          IBLicInfo.ParamByName('P_Service_Bureau_Name').asString := CFGFile.ReadString(Sections[i], 'ServiceBureauName', 'Name is missed, SN# ' + Sections[i]);
          IBLicInfo.ParamByName('P_SERIAL_NUMBER').asInteger := SectionNumber;
          try
            IBLicInfo.ExecSQL;
          except
            on E:Exception do
              raise Exception.Create('Error during insert into table LIC_INFO. Section #' + Sections[i] + #13#10 + E.Message);
          end;

          // STATUS_INFO Table
          try
//            IBStatusInfo.ParamByName('P_CONNECTED').AsString := CFGFile.ReadString(Sections[i], 'Connected', '0');
            IBStatusInfo.ParamByName('P_CONNECTED').AsString := '0'; // alway disconnected, because it's just import

            tmpString := CFGFile.ReadString(Sections[i], 'DVersion', '');
            if (tmpString <> '') then
              IBStatusInfo.ParamByName('P_DATABASE_VERSION').AsString := tmpString
            else
              IBStatusInfo.ParamByName('P_DATABASE_VERSION').Value := null;

            tmpString := CFGFile.ReadString(Sections[i], 'DVersionSent', '');
            if (tmpString <> '') then
              IBStatusInfo.ParamByName('P_DATABASE_VERSION_SENT').AsString := tmpString
            else
              IBStatusInfo.ParamByName('P_DATABASE_VERSION_SENT').Value := null;

            tmpString := CFGFile.ReadString(Sections[i], 'SVersion', '');
            if tmpString <> '' then
              IBStatusInfo.ParamByName('P_SERVER_VERSION').AsString := tmpString
            else
              IBStatusInfo.ParamByName('P_SERVER_VERSION').Value := null;

            tmpString := CFGFile.ReadString(Sections[i], 'SVersionSent', '');
            if tmpString <> '' then
              IBStatusInfo.ParamByName('P_SERVER_VERSION_SENT').AsString := tmpString
            else
              IBStatusInfo.ParamByName('P_SERVER_VERSION_SENT').Value := null;

            tmpDateTime := CFGFile.ReadDateTime(Sections[i], 'DateTimeConnected', 0);
            if tmpDateTime <> 0 then
              IBStatusInfo.ParamByName('P_DATE_TIME_CONNECTED').AsDateTime := tmpDateTime
            else
              IBStatusInfo.ParamByName('P_DATE_TIME_CONNECTED').AsDateTime := null;

            tmpString := CFGFile.ReadString(Sections[i], 'LastAddress', '');
            if tmpString <> '' then
              IBStatusInfo.ParamByName('P_LAST_ADRESS').AsString := tmpString
            else
              IBStatusInfo.ParamByName('P_LAST_ADRESS').Value := null;

            tmpDate := CFGFile.ReadDate(Sections[i], 'LicenseUpdated', 0);
            if tmpDate <> 0 then
              IBStatusInfo.ParamByName('P_LICENSE_UPDATED').AsDate := tmpDate
            else
              IBStatusInfo.ParamByName('P_LICENSE_UPDATED').Value := null;

            tmpDate := CFGFile.ReadDate(Sections[i], 'ReportRan', 0);
            if tmpDate <> 0 then
              IBStatusInfo.ParamByName('P_REPORT_RAN').AsDate := tmpDate
            else
              IBStatusInfo.ParamByName('P_REPORT_RAN').Value := null;

            tmpString := CFGFile.ReadString(Sections[i], 'ReportStatus', '');
            if tmpString <> '' then
              IBStatusInfo.ParamByName('P_REPORT_STATUS').AsString := tmpString
            else
              IBStatusInfo.ParamByName('P_REPORT_STATUS').Value := null;

            tmpDate := CFGFile.ReadDate(Sections[i], 'ReportUpdated', 0);
            if tmpDate <> 0 then
              IBStatusInfo.ParamByName('P_REPORT_UPDATED').AsDate := tmpDate
            else
              IBStatusInfo.ParamByName('P_REPORT_UPDATED').Value := null;

            IBStatusInfo.ParamByName('P_SERIAL_NUMBER').AsInteger := SectionNumber;
          except
            on E:Exception do
              raise Exception.Create('Error during parsing info for table STATUS_INFO. Section #' + Sections[i] + #13#10 + E.Message);
          end;
          try
            IBStatusInfo.ExecSQL;
          except
            on E:Exception do
              raise Exception.Create('Error during insert into table STATUS_INFO. Section #' + Sections[i] + #13#10 + E.Message);
          end;

          // LIC_PACKAGE_INFO Table
          for j := 0 to 6 do
          begin
            tmpString := CFGFile.ReadString(Sections[i], 'L' + IntToStr(j), '');
            if tmpString = '' then
            begin
              // looking in PACKAGE_TYPE for default value
              if IBPackageType.Locate('PACKAGE_TYPE_NBR', j, []) then
                if IBPackageType['DEFAULT_VALUE'] = 'Y' then
                  tmpString := '1'
                else
                  tmpString := '0'
              else
                tmpString := '0';
            end;
            if tmpString = '1' then
            begin
              try
                IBLicPackage.ParamByName('P1').AsInteger := j;
                IBLicPackage.ParamByName('P2').AsInteger := SectionNumber;
                IBLicPackage.ExecSQL;
              except
                on E:Exception do
                  raise Exception.Create('Error during insert into table LIC_PACKAGE. Section #' + Sections[i] + ' L'+ IntToStr(j) + #13#10 + E.Message);
              end;
            end;
          end;

          // RWR_LIST Table
          try
            // FetchSBSection
            CFGFile.ReadSectionValues(IntToStr(SectionNumber), SBSection);

            for j := 0 to SBSection.Count-1 do
            begin
              nm := trim(SBSection.Names[j]);
              if UpperCase(copy(nm,1,4)) = 'RWR_' then
              begin
                if UpperCase(trim(SBSection.ValueFromIndex[j])) = 'NEW' then
                begin
                  ReportNumber := StrToInt( Copy(Nm,5,999) );

                  IBRWRList.ParamByName('P_REPORT_NUMBER').AsInteger := ReportNumber;
                  IBRWRList.ParamByName('P_TASK_STATUS').asString := '0';
                  IBRWRList.ParamByName('P_SERIAL_NUMBER').asInteger := SectionNumber;

                  try
                    IBRWRList.ExecSQL;
                  except
                    on E:Exception do
                      raise Exception.Create('Error during insert into table RWR_LIST. Section #' + Sections[i] + #13#10 + E.Message);
                  end;
                end
              end
            end;  // for
          except
            on E:Exception do
              raise Exception.Create('Error processing info for table RWR_LIST. Section #' + Sections[i] + #13#10 + E.Message);
          end;


        end;  // with
      end;  // for
      ShowMessage('Processed ' + IntToStr(Count) + ' sections');
    finally
      SBSection.Free;
      Sections.Free;
    end;
  end;

  procedure DeletePreviousInfo;
  begin
    with ImportDataModule do
      IBDeletePreviousInfo.ExecuteScript;
  end;

  procedure ClearLogs;
  begin
    with ImportDataModule do
      IBClearLogs.ExecuteScript;
  end;

begin
  // some checks
  if trim(FilePathEdit.Text) = '' then
  begin
     ShowMessage('Select the file!');
     ChooseFileButton.SetFocus;
     AbortEx;
  end;

  // ini - file preparation
  CfgFile := TIniFile.Create(FilePathEdit.Text);
  try
    with ImportDataModule do
    begin
      IBDatabase.Connected := true;
      try  // start transaction
        IBTransaction.StartTransaction;
        try
          IBDefaultInfo.Active := false;
          IBDefaultInfo.Active := true;
          if IBDefaultInfo.eof then raise Exception.Create('Error! Table "Default_info" has no rows!');
          IBPackageType.Active := false;
          IBPackageType.Active := true;
//          if IBPackageType.RecordCount < 7 then raise Exception.Create('Error! Table "Package_type" has less then 7 rows!');
          if ClearDatabaseCB.Checked then
          begin
            if Application.MessageBox('All information in database will be deleted! Are you sure?', 'Warning', MB_OKCANCEL) <> IDOK then AbortEx
            else
              DeletePreviousInfo;
          end;    
          ImportData;
          if ClearLogsCB.Checked then
            ClearLogs;
          IBTransaction.Commit;
          ShowMessage('Succesfully completed.');
        except
          on E:Exception do
          begin
            IBTransaction.Rollback; raise;
          end;
        end; // try except
      finally
        IBDatabase.Connected := false;
      end; // try - start transaction
    end;  // with
  finally
    CfgFile.Free;
  end;
end;

procedure TImportCFGForm.UploadReportsButtonClick(Sender: TObject);
var
  sn : integer;
  tmp : String;
  f : TFileStream;
begin
   if UploadReportDialog.Execute then
   begin
     try
       tmp := ExtractFileName(UploadReportDialog.FileName);
       sn := Pos('.', tmp);
       if sn > 0 then
         tmp := Copy(tmp, 1, sn-1);
       sn := StrToInt(tmp);
     except
       on E:Exception do
       begin
         raise Exception.Create('Wrong filename. Filename must be a serial number!');
       end;
     end;
     f := TFileStream.Create(UploadReportDialog.FileName, fmOpenRead);
     try
        with ImportDataModule do
        begin
          IBDatabase.Connected := true;
          try  // start transaction
            IBTransaction.StartTransaction;
            try
              UpdateQuery.Close; UpdateQuery.SQL.Clear;
              UpdateQuery.SQL.text := 'UPDATE status_info SET REPORT_FILE = :P_REPORT_FILE WHERE SERIAL_NUMBER=:P_SERIAL_NUMBER';
              UpdateQuery.ParamByName('P_SERIAL_NUMBER').AsInteger := sn;
              UpdateQuery.ParamByName('P_REPORT_FILE').LoadFromStream(f, ftBlob);
              UpdateQuery.ExecSQL;
              if UpdateQuery.RowsAffected = 0 then
              begin
                IBTransaction.Rollback;
                ShowMessage('Report hasn''t been uploaded. Download status information before. Serial number is ' + IntTostr(sn));
              end
              else
              begin
                IBTransaction.Commit;
                ShowMessage('Report''s been uploaded succesfully. Serial number is ' + IntTostr(sn));
              end;
            except
              on E:Exception do
              begin
                IBTransaction.Rollback; raise;
              end;
            end; // try except
          finally
            IBDatabase.Connected := false;
          end; // try - start transaction
        end;  // with

     finally
       f.Free;
     end;
   end;
end;

end.
