unit UnitShowDefaultsForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, wwdbedit, Grids, Wwdbigrd, Wwdbgrid;

type
  TShowDefaultForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    wwDBGrid1: TwwDBGrid;
    OKButton: TButton;
    procedure OKButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ShowDefaultForm: TShowDefaultForm;

implementation

uses BBConfigDataUnit, BBConfigMainFormUnit;

{$R *.dfm}

procedure TShowDefaultForm.OKButtonClick(Sender: TObject);
begin
  Close;
end;

end.
