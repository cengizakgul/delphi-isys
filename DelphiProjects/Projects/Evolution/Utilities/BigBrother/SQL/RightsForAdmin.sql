grant REFERENCES on TABLE DEFAULT_INFO TO PUBLIC;
grant REFERENCES on TABLE LIC_INFO to PUBLIC;
grant REFERENCES on TABLE PACKAGE_TYPE to PUBLIC;

grant ALL on TABLE DEFAULT_INFO to bigbrother;
grant ALL on TABLE LIC_INFO to bigbrother;
grant ALL on TABLE LIC_PACKAGE to bigbrother;
grant ALL on TABLE LOG_LIC_INFO to bigbrother;
grant ALL on TABLE LOG_LIC_PACKAGE to bigbrother;
grant ALL on TABLE PACKAGE_TYPE to bigbrother;
grant ALL on TABLE RWR_LIST to bigbrother;
grant ALL on TABLE STATUS_INFO to bigbrother;

grant ALL on TABLE VENDOR to bigbrother;
grant ALL on TABLE API_KEY to bigbrother;
grant ALL on TABLE LIC_API_KEY to bigbrother;

grant execute on procedure P_POST_EVENT to bigbrother;