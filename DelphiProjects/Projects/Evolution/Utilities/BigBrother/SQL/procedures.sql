SET TERM ^^ ;
CREATE PROCEDURE P_POST_EVENT (
  EVENT_NAME VarChar(50))
AS
/*
  Author   : Makarkin,
  Date     : 08/30/2007
  Purpose  : to post event
  Params
  ------
  EVENT_NAME  : string with event name
*/
begin
  /* code */
  POST_EVENT :EVENT_NAME;
end
^^
SET TERM ; ^^
