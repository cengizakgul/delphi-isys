CREATE EXCEPTION RWR_LIST_NO_UPDATE_EXC 'You can update only "TASK_STATUS" column';

CREATE EXCEPTION LIC_PACKAGE_NO_UPDATE_EXC 'No updates allowed for LIC_PACKAGE table';

CREATE EXCEPTION RWR_LIST_ONLY_1_ACTIVE_TASK 'You can have only one active report upload task!';

CREATE EXCEPTION SB_DO_NOT_HAVE_S_LICENSE 'SB do not have Self Serve license';

CREATE EXCEPTION SB_DO_NOT_HAVE_W_LICENSE 'SB do not have webClient license';