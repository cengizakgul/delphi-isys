/************ LIC_INFO **************/

SET TERM ^^ ;
CREATE TRIGGER TR_LIC_INFO_01 FOR LIC_INFO ACTIVE AFTER UPDATE POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : July 24 2007
  Purpose  : Trigger for logging all changes: for updates creates 2 rows (with old values and with new values)
*/
declare variable  log_nbr integer;
declare variable change_nbr integer;
begin
   change_nbr =  GEN_ID(LOG_CHANGE_NBR,1);

   /* old values */
   log_nbr =  GEN_ID(LOG_REC_NBR, 1);
   insert into LOG_LIC_INFO (LOG_LIC_INFO_NBR, LOG_LIC_INFO_CHANGE_NBR, CHANGED_BY, CHANGED_DATE,
     CHANGED_OPERATION, SERIAL_NUMBER, LICENSE_EXPIRATION, LICENSE_FREQUENCY, REPORT_GRACE_PERIOD, SERVICE_BUREAU_NAME )
     VALUES (:log_nbr, :change_nbr, current_user, current_timestamp, 'U', old.SERIAL_NUMBER, old.LICENSE_EXPIRATION, old.LICENSE_FREQUENCY,
     old.REPORT_GRACE_PERIOD, old.SERVICE_BUREAU_NAME);

   /* new values */
   log_nbr =  GEN_ID(LOG_REC_NBR, 1);
   insert into LOG_LIC_INFO (LOG_LIC_INFO_NBR, LOG_LIC_INFO_CHANGE_NBR, CHANGED_BY, CHANGED_DATE,
     CHANGED_OPERATION, SERIAL_NUMBER, LICENSE_EXPIRATION, LICENSE_FREQUENCY, REPORT_GRACE_PERIOD, SERVICE_BUREAU_NAME )
     VALUES (:log_nbr, :change_nbr, current_user, current_timestamp, 'U', new.SERIAL_NUMBER, new.LICENSE_EXPIRATION, new.LICENSE_FREQUENCY,
     new.REPORT_GRACE_PERIOD, new.SERVICE_BUREAU_NAME);
end
^^
SET TERM ; ^^

/* ---------------------------------------- */


SET TERM ^^ ;
CREATE TRIGGER TR_LIC_INFO_02 FOR LIC_INFO ACTIVE AFTER INSERT POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : July 24 2007
  Purpose  : Trigger for logging inserts changes
*/
declare variable  log_nbr integer;
declare variable change_nbr integer;
begin
   log_nbr =  GEN_ID(LOG_REC_NBR, 1);
   change_nbr =  GEN_ID(LOG_CHANGE_NBR,1);

   /* new values */
   insert into LOG_LIC_INFO (LOG_LIC_INFO_NBR, LOG_LIC_INFO_CHANGE_NBR, CHANGED_BY, CHANGED_DATE,
     CHANGED_OPERATION, SERIAL_NUMBER, LICENSE_EXPIRATION, LICENSE_FREQUENCY, REPORT_GRACE_PERIOD, SERVICE_BUREAU_NAME )
     VALUES (:log_nbr, :change_nbr, current_user, current_timestamp, 'I', new.SERIAL_NUMBER, new.LICENSE_EXPIRATION, new.LICENSE_FREQUENCY,
     new.REPORT_GRACE_PERIOD, new.SERVICE_BUREAU_NAME);
end
^^
SET TERM ; ^^

/* ---------------------------------------- */

SET TERM ^^ ;
CREATE TRIGGER TR_LIC_INFO_03 FOR LIC_INFO ACTIVE AFTER DELETE POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : July 24 2007
  Purpose  : Trigger for logging all deletes
*/
declare variable  log_nbr integer;
declare variable change_nbr integer;
begin
   change_nbr =  GEN_ID(LOG_CHANGE_NBR,1);

   /* old values */
   log_nbr =  GEN_ID(LOG_REC_NBR, 1);
   insert into LOG_LIC_INFO (LOG_LIC_INFO_NBR, LOG_LIC_INFO_CHANGE_NBR, CHANGED_BY, CHANGED_DATE,
     CHANGED_OPERATION, SERIAL_NUMBER, LICENSE_EXPIRATION, LICENSE_FREQUENCY, REPORT_GRACE_PERIOD, SERVICE_BUREAU_NAME )
     VALUES (:log_nbr, :change_nbr, current_user, current_timestamp, 'D', old.SERIAL_NUMBER, old.LICENSE_EXPIRATION, old.LICENSE_FREQUENCY,
     old.REPORT_GRACE_PERIOD, old.SERVICE_BUREAU_NAME);
end
^^
SET TERM ; ^^

/************ LIC_PACKAGE **************/


SET TERM ^^ ;
CREATE TRIGGER TR_LIC_PACKAGE_01 FOR LIC_PACKAGE ACTIVE BEFORE UPDATE POSITION 0 AS
/*
  Author   :  M.Makarkin
  Date     : July 24 2007
  Purpose  : doesn't allow any changes, you should delete and insert again
*/
begin
  exception LIC_PACKAGE_NO_UPDATE_EXC;
end
^^
SET TERM ; ^^

/* ---------------------------------------- */

SET TERM ^^ ;
CREATE TRIGGER TR_LIC_PACKAGE_02 FOR LIC_PACKAGE ACTIVE AFTER INSERT POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : July 26 2007
  Purpose  : Trigger for logging inserts
*/
declare variable  log_nbr integer;
declare variable change_nbr integer;
begin
   change_nbr =  GEN_ID(LOG_CHANGE_NBR,1);

   /* new values */
   log_nbr =  GEN_ID(LOG_REC_NBR, 1);
   insert into LOG_LIC_PACKAGE (LOG_LIC_PACKAGE_NBR, LOG_LIC_PACKAGE_CHANGE_NBR, CHANGED_BY, CHANGED_DATE,
     CHANGED_OPERATION, SERIAL_NUMBER, PACKAGE_TYPE_NBR)
     VALUES (:log_nbr, :change_nbr, current_user, current_timestamp, 'I', new.SERIAL_NUMBER, new.PACKAGE_TYPE_NBR);
end
^^
SET TERM ; ^^

/* ---------------------------------------- */


SET TERM ^^ ;
CREATE TRIGGER TR_LIC_PACKAGE_03 FOR LIC_PACKAGE ACTIVE AFTER DELETE POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : July 26 2007
  Purpose  : Trigger for logging inserts
*/
declare variable  log_nbr integer;
declare variable change_nbr integer;
begin
   change_nbr =  GEN_ID(LOG_CHANGE_NBR,1);

   /* old values */
   log_nbr =  GEN_ID(LOG_REC_NBR, 1);
   insert into LOG_LIC_PACKAGE (LOG_LIC_PACKAGE_NBR, LOG_LIC_PACKAGE_CHANGE_NBR, CHANGED_BY, CHANGED_DATE,
     CHANGED_OPERATION, SERIAL_NUMBER, PACKAGE_TYPE_NBR)
     VALUES (:log_nbr, :change_nbr, current_user, current_timestamp, 'D', OLD.SERIAL_NUMBER, OLD.PACKAGE_TYPE_NBR);
end
^^
SET TERM ; ^^


/************ RWR_LIST **************/


SET TERM ^^ ;
CREATE TRIGGER TR_RWR_LIST_01 FOR RWR_LIST ACTIVE BEFORE UPDATE POSITION 0 AS
/*
  Author   :  M.Makarkin
  Date     : Aug 03 2007
  Purpose  : doesn't allow any changes, you should delete and insert again
*/
begin
  if ((new.REPORT_NUMBER<>old.REPORT_NUMBER) or (new.SERIAL_NUMBER<>old.SERIAL_NUMBER) or (new.RWR_LIST_NBR<>old.RWR_LIST_NBR)) then
    exception RWR_LIST_NO_UPDATE_EXC;
end
^^
SET TERM ; ^^

/************ PACKAGE_INFO **************/

SET TERM ^^ ;
CREATE TRIGGER TR_PACKAGE_TYPE_01 FOR PACKAGE_TYPE ACTIVE AFTER INSERT POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : Aug 29 2007
  Purpose  : amount of modules should not be more than LicenseOptionMaxCount (now it's 16), see LicenseInfo unit in  \iSystems\Evolution\Common
*/
declare variable cnt integer;
begin
  select count(*) from PACKAGE_TYPE into :cnt;
  if (cnt > 16) then
    exception PACKAGE_TYPE_TOO_MANY_MODULES;
end
^^
SET TERM ; ^^

/************ LIC_API_KEY **************/


SET TERM ^^ ;
CREATE TRIGGER TR_LIC_API_KEY_01 FOR LIC_API_KEY ACTIVE BEFORE INSERT POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : Sept 04 2008
  Purpose  : SB must have license (webClient or Self serve) in order to have API key of 'W' or 'S' types
*/
declare variable cnt integer;
begin
  /* Web Client check */
  select count(*) from API_KEY where (API_KEY = new.API_KEY) and (APP_KEY_TYPE = 'W') into :cnt;
  if (cnt > 0) then
  begin
    select count(*) from LIC_PACKAGE where (SERIAL_NUMBER=new.SERIAL_NUMBER) and (package_type_nbr = 1) into :cnt;
    if (cnt = 0) then
      exception SB_DO_NOT_HAVE_W_LICENSE;
  end
  /* Selfe serve check */
  select count(*) from API_KEY a where (a.API_KEY = new.API_KEY) and (a.APP_KEY_TYPE = 'S') into :cnt;
  if (cnt > 0) then
  begin
    select count(*) from LIC_PACKAGE where (SERIAL_NUMBER=new.SERIAL_NUMBER) and (package_type_nbr = 6) into :cnt;
    if (cnt = 0) then
      exception SB_DO_NOT_HAVE_S_LICENSE;
  end
end
^^
SET TERM ; ^^



SET TERM ^^ ;
CREATE TRIGGER TR_LIC_API_KEY_02 FOR LIC_API_KEY ACTIVE BEFORE UPDATE POSITION 0 AS
/*
  Author   M.Makarkin: ,
  Date     : Sept 04 2008
  Purpose  : SB must have license (webClient or Self serve) in order to have API key of 'W' or 'S' types
*/
declare variable cnt integer;
begin
  /* Web Client check */
  select count(*) from API_KEY where (API_KEY = new.API_KEY) and (APP_KEY_TYPE = 'W') into :cnt;
  if (cnt > 0) then
  begin
    select count(*) from LIC_PACKAGE where (SERIAL_NUMBER=new.SERIAL_NUMBER) and (package_type_nbr = 1) into :cnt;
    if (cnt = 0) then
      exception SB_DO_NOT_HAVE_W_LICENSE;
  end
  /* Selfe serve check */
  select count(*) from API_KEY a where (a.API_KEY = new.API_KEY) and (a.APP_KEY_TYPE = 'S') into :cnt;
  if (cnt > 0) then
  begin
    select count(*) from LIC_PACKAGE where (SERIAL_NUMBER=new.SERIAL_NUMBER) and (package_type_nbr = 6) into :cnt;
    if (cnt = 0) then
      exception SB_DO_NOT_HAVE_S_LICENSE;
  end
end
^^
SET TERM ; ^^


