grant REFERENCES on TABLE DEFAULT_INFO TO PUBLIC;
grant REFERENCES on TABLE LIC_INFO to PUBLIC;
grant REFERENCES on TABLE PACKAGE_TYPE to PUBLIC;

grant select, references on TABLE DEFAULT_INFO to reado;
grant select, references on TABLE LIC_INFO to reado;
grant select, references on TABLE LIC_PACKAGE to reado;
grant select, references on TABLE LOG_LIC_INFO to reado;
grant select, references on TABLE LOG_LIC_PACKAGE to reado;
grant select, references on TABLE PACKAGE_TYPE to reado;
grant ALL on TABLE RWR_LIST to reado;
grant select, update, references on TABLE STATUS_INFO to reado;

grant select, references on TABLE VENDOR to reado;
grant select, references on TABLE API_KEY to reado;
grant select, references on TABLE LIC_API_KEY to reado;

grant execute on procedure P_POST_EVENT to reado;