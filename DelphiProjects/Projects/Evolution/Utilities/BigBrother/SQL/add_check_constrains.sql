ALTER TABLE PACKAGE_TYPE
        ADD CONSTRAINT CH_PACKAGE_TYPE_01 CHECK (DEFAULT_VALUE in ('N', 'Y'));
ALTER TABLE STATUS_INFO
        ADD CONSTRAINT CH_STATUS_INFO_01 CHECK (REPORT_STATUS  in ('0', '1', '2', '3', '4'));
ALTER TABLE STATUS_INFO
        ADD CONSTRAINT CH_STATUS_INFO_02 CHECK (CONNECTED  in ('0', '1'));
ALTER TABLE STATUS_INFO
        ADD CONSTRAINT CH_STATUS_INFO_03 CHECK (LICENSE_UPDATE_STATUS  in ('0', '1', '2', '3'));
ALTER TABLE RWR_LIST
        ADD CONSTRAINT CH_RWR_LIST_01 CHECK (TASK_STATUS  in ('0', '1', '2'));
