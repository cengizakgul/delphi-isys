unit TasksListFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid;

type
  TTasksListForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    OKButton: TButton;
    TaskGrid: TwwDBGrid;
    Panel3: TPanel;
    ShowAllCheck: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ShowAllCheckClick(Sender: TObject);
  private
    FCurrentSerialNumber: integer;
    procedure SetCurrentSerialNumber(const Value: integer);
    { Private declarations }
  public
    { Public declarations }
    property CurrentSerialNumber : integer read FCurrentSerialNumber write SetCurrentSerialNumber;
    procedure ShowTasksList;
    procedure OpenWithFilters;
  end;

var
  TasksListForm: TTasksListForm;

implementation

uses BBConfigDataUnit;

{$R *.dfm}

procedure TTasksListForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  BBConfigData.IBRWRList.Close;
end;

procedure TTasksListForm.SetCurrentSerialNumber(const Value: integer);
begin
  FCurrentSerialNumber := Value;
end;

procedure TTasksListForm.OpenWithFilters;
begin
  with BBConfigData do
  begin
    IBRWRList.Active := false;
    if ShowAllCheck.Checked then
      IBRWRList.SQL.Text := 'select * from rwr_list where serial_number=:p1 order by rwr_list_nbr'
    else
      IBRWRList.SQL.Text := 'select * from rwr_list where serial_number=:p1 and task_status=''0'' order by rwr_list_nbr';
    IBRWRList.Active := true;
  end;  
end;

procedure TTasksListForm.ShowTasksList;
begin
  with BBConfigData do
  begin
    if BBConfigData.IBLicInfo['SERIAL_NUMBER'] <> null then
    begin
      CurrentSerialNumber := BBConfigData.IBLicInfo['SERIAL_NUMBER'];
      IBRWRList.Active := false;
      IBRWRList.ParamByName('P1').AsInteger := CurrentSerialNumber;
      OpenWithFilters;
      IBRWRList.FetchAll;
      Caption := 'Tasks list for serial# ' + IntToStr(CurrentSerialNumber);
      ShowModal;
    end;
  end;
end;

procedure TTasksListForm.ShowAllCheckClick(Sender: TObject);
begin
  OpenWithFilters;
end;

end.
