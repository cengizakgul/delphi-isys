unit BBConfigDataUnit;

interface

uses
  SysUtils, Classes, IBDatabase, DB, Dialogs, DBLogDlg, Forms,
  IBDatabaseINI, IBCustomDataSet, IBTable, IBQuery, IniFiles, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, Provider, Variants,
  IBUpdateSQL, IBEvents, IsBasicUtils;

const
  DatabaseIniSection = 'Database';

type
  TBBConfigData = class(TDataModule)
    IBDatabase: TIBDatabase;
    IBTransaction: TIBTransaction;
    IBLicInfo: TIBQuery;
    IBLicInfoDataSource: TDataSource;
    IBLicInfoSERIAL_NUMBER: TIntegerField;
    IBLicInfoSERVICE_BUREAU_NAME: TIBStringField;
    IBLicInfoLICENSE_EXPIRATION: TIntegerField;
    IBLicInfoLICENSE_FREQUENCY: TIntegerField;
    IBLicInfoREPORT_GRACE_PERIOD: TIntegerField;
    IBLicInfoCONNECTED: TIBStringField;
    IBLicInfoDATABASE_VERSION: TIBStringField;
    IBLicInfoDATABASE_VERSION_SENT: TIBStringField;
    IBLicInfoDATE_TIME_CONNECTED: TDateTimeField;
    IBLicInfoLAST_ADRESS: TIBStringField;
    IBLicInfoLICENSE_UPDATED: TDateTimeField;
    IBLicInfoREPORT_RAN: TDateTimeField;
    IBLicInfoREPORT_STATUS: TIBStringField;
    IBLicInfoREPORT_UPDATED: TDateTimeField;
    IBLicInfoSERVER_VERSION: TIBStringField;
    IBLicInfoSERVER_VERSION_SENT: TIBStringField;
    IBLicInfoCONNECTION_STATUS: TIntegerField;
    IBPackageType: TIBQuery;
    IBLicInfoOpenReport: TStringField;
    IBLicInfoRunReportNow: TStringField;
    IBLicInfoReportStatus: TStringField;
    IBLicInfoL0: TIntegerField;
    IBLicInfoL1: TIntegerField;
    IBLicInfoL2: TIntegerField;
    IBLicInfoL3: TIntegerField;
    IBLicInfoL4: TIntegerField;
    IBLicInfoL5: TIntegerField;
    IBLicInfoL6: TIntegerField;
    IBLicPackage: TIBQuery;
    IBTotals: TIBQuery;
    IBDefaultInfo: TIBQuery;
    IBDefaultInfoDataSource: TDataSource;
    IBPackageTypeDataSource: TDataSource;
    IBPackageTypeDefaultValueIndicator: TIntegerField;
    IBPackageTypeDEFAULT_VALUE: TIBStringField;
    IBPackageTypeLONG_DESCRIPTION: TIBStringField;
    IBPackageTypePACKAGE_TYPE_NBR: TIntegerField;
    IBPackageTypeSHORT_DESCRIPTION: TIBStringField;
    IBRWRList: TIBQuery;
    IBRWRListDataSource: TDataSource;
    IBRWRListREPORT_FILE: TBlobField;
    IBRWRListREPORT_NUMBER: TIntegerField;
    IBRWRListRWR_LIST_NBR: TIntegerField;
    IBRWRListSERIAL_NUMBER: TIntegerField;
    IBRWRListTASK_STATUS: TIBStringField;
    IBRWRListStatusText: TStringField;
    IBServerEvents: TIBEvents;
    IBLicInfoREPORT_FILE: TBlobField;
    IBLicInfoLICENSE_UPDATE_STATUS: TIBStringField;
    IBLicInfoLicenseUpdateStatus: TStringField;
    IBLicInfoUpdateLicenseNow: TStringField;
    IBVendor: TIBQuery;
    IBVendorDataSource: TDataSource;
    IBVendorUpdate: TIBUpdateSQL;
    IBVendorVENDOR_CUSTOM_NBR: TIntegerField;
    IBVendorVENDOR_NAME: TIBStringField;
    IBManualTransactions: TIBTransaction;
    IBAPIKeys: TIBQuery;
    IBAPIKeysDataSource: TDataSource;
    IBAPIKeysAPI_KEY: TIBStringField;
    IBAPIKeysAPP_KEY_TYPE: TIBStringField;
    IBAPIKeysAPPLICATION_NAME: TIBStringField;
    IBAPIKeysVENDOR_CUSTOM_NBR: TIntegerField;
    IBAPIKeysUpdateSQL: TIBUpdateSQL;
    IBAPIKeysVENDOR_NAME: TStringField;
    IBLicAPIKeys: TIBQuery;
    IBLicAPIKeysDataSource: TDataSource;
    IBLicAPIKeysAPI_KEY: TIBStringField;
    IBLicAPIKeysSERIAL_NUMBER: TIntegerField;
    IBLicAPIKeysUpdate: TIBUpdateSQL;
    IBLicAPIKeysAPPLICATION_NAME: TStringField;
    IBLicInfoL7: TIntegerField;
    IBLicInfoL8: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure IBLicInfoCalcFields(DataSet: TDataSet);
    procedure IBPackageTypeCalcFields(DataSet: TDataSet);
    procedure IBRWRListCalcFields(DataSet: TDataSet);
    procedure IBServerEventsEventAlert(Sender: TObject; EventName: String;
      EventCount: Integer; var CancelAlerts: Boolean);
    procedure IBAPIKeysNewRecord(DataSet: TDataSet);
    procedure IBLicAPIKeysNewRecord(DataSet: TDataSet);
  private
    FDatabasePath: String;
    FLastUserName: String;
    FEditIsDisabled: boolean;
    FRefreshType: integer;
    FGotSrvStatusChanged: boolean;
    FGotSRVRWRChanged: boolean;
    FDatabasePathV10: String;
    FConnectedToV10: boolean;
    procedure SetDatabasePath(const Value: String);
    procedure SetLastUserName(const Value: String);
    procedure SetEditIsDisabled(const Value: boolean);
    procedure SetRefreshType(const Value: integer);
    procedure SetGotSrvStatusChanged(const Value: boolean);
    procedure SetGotSRVRWRChanged(const Value: boolean);
    procedure SetDatabasePathV10(const Value: String);
    procedure SetConnectedToV10(const Value: boolean);
    { Private declarations }
  public
    { Public declarations }
    property DatabasePath : String read FDatabasePath write SetDatabasePath;
    property DatabasePathV10 : String read FDatabasePathV10 write SetDatabasePathV10;
    property ConnectedToV10 : boolean read FConnectedToV10 write SetConnectedToV10;
    property LastUserName : String read FLastUserName write SetLastUserName;
    property EditIsDisabled : boolean read FEditIsDisabled write SetEditIsDisabled; // main security flag
    property RefreshType : integer read FRefreshType write SetRefreshType;
    property GotSrvStatusChanged : boolean read FGotSrvStatusChanged write SetGotSrvStatusChanged;
    property GotSRVRWRChanged : boolean read FGotSRVRWRChanged write SetGotSRVRWRChanged;
    procedure LoadSettings;
    procedure SaveSettings;
    procedure FullRefreshDataSets;
    procedure CloseDataSets;
    procedure ReconnectToDatabase;
  end;

var
  BBConfigData: TBBConfigData;
  ConfigFileName : String;

implementation

uses BBConfigMainFormUnit, SBLicenseWriterUnit, VersionChoiseFormUnit;

{$R *.dfm}

procedure TBBConfigData.DataModuleCreate(Sender: TObject);
var
  CFGFile : TIniFile;
  UserName, Password : String;
  i : integer;
  tmp1, tmp2, tmp3 : String;
begin
  if not Assigned(VersionChoiseForm) then
    VersionChoiseForm := TVersionChoiseForm.Create(self.Owner);

  LoadSettings;

  EditIsDisabled := true;

  IBDatabase.Connected := false;

  if VersionChoiseForm.ShowVersionChoise(ConnectedToV10) = 0 then
  begin
    ConnectedToV10 := false;
    IBDatabase.DatabaseName := DatabasePath;
  end
  else
  begin
    ConnectedToV10 := true;
    IBDatabase.DatabaseName := DatabasePathV10
  end;

  UserName := LastUserName;
  for i := 1 to 3 do
  begin
    if LoginDialog(IBDatabase.DatabaseName, UserName, Password) then
    begin
      // don't allow login for EUSER and SYSDBA
      tmp1 := 'USE'; tmp2 := 'E'; tmp3 := 'R'; tmp1 := tmp2 + tmp1 + tmp3;
      if UpperCase(Username) = tmp1 then
      begin
        ShowMessage('User: '+ Username + ' has no rights for this application!');
        continue;
      end;

      tmp1 := 'S'; tmp2 := 'YS'; tmp3 := 'BA'; tmp1 := tmp1 + tmp2 + 'D' + tmp3;
      if UpperCase(Username) = tmp1 then
      begin
        ShowMessage('User: '+ Username + ' has no rights for this application!');
        continue;
      end;

      IBDatabase.Params.Clear;
      IBDatabase.Params.Add('user_name='+Username);
      IBDatabase.Params.Add('password='+Password);
      LastUserName := UserName;

      CFGFile := TIniFile.Create(ConfigFileName);
      try
        CFGFile.WriteString(DatabaseIniSection, 'LastUserName', UserName);
        CFGFile.WriteBool(DatabaseIniSection, 'ConnectedToV10', ConnectedToV10);
      finally
        CFGFile.Free;
      end;
    end
    else
    begin
      Application.Terminate;
      Exit;
    end;

    try
      ReconnectToDatabase;
      Exit;
    except
      on E:Exception do
      begin
        ShowMessage('Error: '+ E.Message);
      end;
    end;  // try except
  end; // for
  ShowMessage('You are allowed only three login attempts at a time.');
  Application.Terminate;
end;

procedure TBBConfigData.LoadSettings;
var
  CFGFile : TIniFile;
  tmpRefresh : integer;
begin
  CFGFile := TIniFile.Create(ConfigFileName);
  try
     DatabasePath := CFGFile.ReadString(DatabaseIniSection, 'Path', '');
     if DatabasePath = '' then
     begin
       ShowMessage('You should create "Path" key in "Database" section of ' + ExtractFileName(ConfigFileName) + ' file');
       Application.Terminate;
     end;

     DatabasePathV10 := CFGFile.ReadString(DatabaseIniSection, 'PathV10', '');
     if DatabasePathV10 = '' then
     begin
       ShowMessage('You should create "PathV10" key in "Database" section of ' + ExtractFileName(ConfigFileName) + ' file');
       Application.Terminate;
     end;
     ConnectedToV10 := CFGFile.ReadBool(DatabaseIniSection, 'ConnectedToV10', false);

     LastUserName := CFGFile.ReadString(DatabaseIniSection, 'LastUserName', '');
     tmpRefresh := CFGFile.ReadInteger(DatabaseIniSection, 'RefreshType', 0);
     with BBConfigMainForm do
       case  tmpRefresh of
         0: RefreshType := 0;
         1: RefreshType := 1;
         2: RefreshType := 2;
         else
         begin
           ShowMessage('Wrong refresh type in config file. It''ll be changed to default');
           RefreshType := 0;
         end;
       end;
  finally
    CFGFile.Free;
  end;
end;

procedure TBBConfigData.SetDatabasePath(const Value: String);
begin
  FDatabasePath := Value;
end;

procedure TBBConfigData.SetLastUserName(const Value: String);
begin
  FLastUserName := Value;
end;

procedure TBBConfigData.IBLicInfoCalcFields(DataSet: TDataSet);
begin
  if DataSet['CONNECTED'] <> null then
    DataSet['Connection_STATUS'] := DataSet.FieldByName('CONNECTED').ASInteger;

  // URL to open report
  if DataSet['SERIAL_NUMBER'] <> null then
    if TBlobField(DataSet.FieldByname('REPORT_FILE')).BlobSize > 0 then
      DataSet['OpenReport'] := 'Open#'+ DataSet.FieldByName('SERIAL_NUMBER').AsString
    else
      DataSet['OpenReport'] := null;
{    if FileExists(DataSet.FieldByName('SERIAL_NUMBER').AsString + '.rwa') then
      DataSet['OpenReport'] := 'Open#'+ ExtractFilePath(ParamStr(0)) + DataSet.FieldByName('SERIAL_NUMBER').AsString + '.rwa'
    else
      DataSet['OpenReport'] := null; }

  // URL to run report
  DataSet['RunReportNow'] := 'Run now';

  // URL to update license
  DataSet['UpdateLicenseNow'] := 'Update now';

  // report status from integer to string
  if DataSet['Report_Status'] <> null then
  begin
    if DataSet['Report_Status'] = '0' then
      DataSet['ReportStatus'] := 'OK';
    if DataSet['Report_Status'] = '1' then
      DataSet['ReportStatus'] := 'Running';
    if DataSet['Report_Status'] = '2' then
      DataSet['ReportStatus'] := 'Forced';
    if DataSet['Report_Status'] = '3' then
      DataSet['ReportStatus'] := 'Error';
    if DataSet['Report_Status'] = '4' then
      DataSet['ReportStatus'] := 'Already running';
  end;

  // Licenses
  DataSet['L0'] := 0; DataSet['L1'] := 0; DataSet['L2'] := 0; DataSet['L3'] := 0;
  DataSet['L4'] := 0; DataSet['L5'] := 0; DataSet['L6'] := 0;  DataSet['L7'] := 0;
  DataSet['L8'] := 0;
  IBLicPackage.Active := false;
  IBLicPackage.ParamByName('P_SERIAL_NUMBER').Value := DataSet['SERIAL_NUMBER'];
  IBLicPackage.Active := true;
  try
    while not IBLicPackage.eof do
    begin
      DataSet.FieldByName('L'+IBLicPackage.FieldByName('PACKAGE_TYPE_NBR').AsString).AsInteger := 1;
      IBLicPackage.Next;
    end;
  finally
    IBLicPackage.Active := false;
  end;

  // license update status from integer to string
  if DataSet['License_Update_Status'] <> null then
  begin
    if DataSet['License_Update_Status'] = '0' then
      DataSet['LicenseUpdateStatus'] := 'OK';
    if DataSet['License_Update_Status'] = '1' then
      DataSet['LicenseUpdateStatus'] := 'Updating';
    if DataSet['License_Update_Status'] = '2' then
      DataSet['LicenseUpdateStatus'] := 'Forced';
    if DataSet['License_Update_Status'] = '3' then
      DataSet['LicenseUpdateStatus'] := 'Refused by RD';
  end;
end;

procedure TBBConfigData.SetEditIsDisabled(const Value: boolean);
begin
  FEditIsDisabled := Value;
end;

procedure TBBConfigData.FullRefreshDataSets;
var
  SerialNumber : Variant;
  PackageTypeNbr : Variant;
begin
  // LIC_INFO
  IBLicInfo.DisableControls;
  try
    SerialNumber := IBLicInfo['Serial_Number'];
    IBLicInfo.Active := false;
    IBLicInfo.Active := true;
    if SerialNumber <> null then
      IBLicInfo.Locate('Serial_number', SerialNumber, []);
  finally
    IBLicInfo.EnableControls;
  end;

  // DEFAULT_INFO
  IBDefaultInfo.DisableControls;
  try
    IBDefaultInfo.Active := false;
    IBDefaultInfo.Active := true;
  finally
    IBDefaultInfo.EnableControls;
  end;

  // PACKAGE_TYPE
  PackageTypeNbr := IBPackageType['PACKAGE_TYPE_NBR'];
  IBPackageType.DisableControls;
  try
    IBPackageType.Active := false;
    IBPackageType.Active := true;
    IBPackageType.FetchAll;
    if PackageTypeNbr <> null then
      IBPackageType.Locate('PACKAGE_TYPE_NBR', PackageTypeNbr, []);
  finally
    IBPackageType.EnableControls;
  end;
end;

procedure TBBConfigData.ClosedataSets;
var
  i : integer;
begin
  for i := 0 to ComponentCount - 1 do
  begin
    if Components[i] is TDataSet then
      TDataSet(Components[i]).Active := false;
  end;
  if IBServerEvents.Registered then
    IBServerEvents.UnRegisterEvents;
  IBDatabase.Connected := false;
end;

procedure TBBConfigData.IBPackageTypeCalcFields(DataSet: TDataSet);
begin
  if DataSet['DEFAULT_VALUE'] = 'Y' then
    DataSet['DefaultValueIndicator'] := 1
  else
    DataSet['DefaultValueIndicator'] := 0;
end;

procedure TBBConfigData.IBRWRListCalcFields(DataSet: TDataSet);
begin
  DataSet['StatusText'] := 'Unknown';
  if  DataSet['Task_status'] = '0' then
    DataSet['StatusText'] := 'In queue';
  if  DataSet['Task_status'] = '1' then
    DataSet['StatusText'] := 'Done';
  if  DataSet['Task_status'] = '2' then
    DataSet['StatusText'] := 'Error';
end;

procedure TBBConfigData.SaveSettings;
var
  CFGFile : TIniFile;
begin
  CFGFile := TIniFile.Create(ConfigFileName);
  try
     CFGFile.WriteInteger(DatabaseIniSection, 'RefreshType', RefreshType);
  finally
    CFGFile.Free;
  end;
end;

procedure TBBConfigData.SetRefreshType(const Value: integer);
begin
  FRefreshType := Value;
end;

procedure TBBConfigData.IBServerEventsEventAlert(Sender: TObject;
  EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
begin
  if UpperCase(EventName) = SRV_RWR_CHANGED then
    GotSRVRWRChanged := true;
  if UpperCase(EventName) = SRV_STATUS_CHANGED then
    GotSRVStatusChanged := true;
  if not BBConfigMainForm.ServerEventTimer.Enabled then
    BBConfigMainForm.ServerEventTimer.Enabled := true;
end;

procedure TBBConfigData.SetGotSrvStatusChanged(const Value: boolean);
begin
  FGotSrvStatusChanged := Value;
end;

procedure TBBConfigData.SetGotSRVRWRChanged(const Value: boolean);
begin
  FGotSRVRWRChanged := Value;
end;

procedure TBBConfigData.SetDatabasePathV10(const Value: String);
begin
  FDatabasePathV10 := Value;
end;

procedure TBBConfigData.SetConnectedToV10(const Value: boolean);
begin
  FConnectedToV10 := Value;
end;

procedure TBBConfigData.ReconnectToDatabase;
var
 tmp1, tmp2, tmp3 : STring;
begin
  IBDatabase.Connected := false;

  if ConnectedToV10 then
    IBDatabase.DatabaseName := DatabasePathV10
  else
    IBDatabase.DatabaseName := DatabasePath;

  // connecting
//  IBDatabase.Connected := false;
  IBDatabase.Connected := true;

  // opening tables
  IBLicInfo.Active := false;
  IBLicInfo.Active := true;
  IBLicInfo.FetchAll;

  IBPackageType.Active := false;
  IBPackageType.Active := true;
  IBPackageType.FetchAll;

  IBDefaultInfo.Active := false;
  IBDefaultInfo.Active := true;

{      tmp1 := 'USE'; tmp2 := 'E'; tmp3 := 'R'; tmp1 := tmp2 + tmp1 + tmp3;
  if UpperCase(Username) = tmp1 then
    EditIsDisabled := false; }
  tmp1 := 'BROTH'; tmp2 := 'BIG'; tmp3 := 'ER'; tmp1 := tmp2 + tmp1 + tmp3;
  if UpperCase(LastUsername) = tmp1 then
    EditIsDisabled := false
  else
    EditIsDisabled := true;
end;

procedure TBBConfigData.IBAPIKeysNewRecord(DataSet: TDataSet);
begin
 DataSet['API_KEY'] := GetUniqueID;
end;

procedure TBBConfigData.IBLicAPIKeysNewRecord(DataSet: TDataSet);
begin
  IBLicAPIKeys['SERIAL_NUMBER'] := IBLicAPIKeys.Params[0].Value;
end;

initialization
  ConfigFileName := ChangeFileExt(ParamStr(0),'.ini');
end.
