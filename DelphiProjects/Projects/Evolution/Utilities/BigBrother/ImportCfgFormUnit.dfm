object ImportCFGForm: TImportCFGForm
  Left = 230
  Top = 119
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Import BigBrother.cgf to database'
  ClientHeight = 127
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 19
    Width = 19
    Height = 13
    Caption = 'File:'
  end
  object FilePathEdit: TEdit
    Left = 48
    Top = 16
    Width = 369
    Height = 21
    TabOrder = 2
  end
  object ChooseFileButton: TButton
    Left = 424
    Top = 15
    Width = 23
    Height = 25
    Caption = '...'
    TabOrder = 0
    OnClick = ChooseFileButtonClick
  end
  object StartButton: TButton
    Left = 200
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Run import'
    TabOrder = 1
    OnClick = StartButtonClick
  end
  object ClearLogsCB: TCheckBox
    Left = 48
    Top = 48
    Width = 177
    Height = 17
    Caption = 'Clear logs after import'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object UploadReportsButton: TButton
    Left = 368
    Top = 96
    Width = 89
    Height = 25
    Caption = 'Upload report'
    TabOrder = 4
    OnClick = UploadReportsButtonClick
  end
  object ClearDatabaseCB: TCheckBox
    Left = 48
    Top = 72
    Width = 209
    Height = 17
    Caption = 'Clear exising record in database'
    TabOrder = 5
  end
  object OpenDialog: TOpenDialog
    Filter = '*.cfg|*.cfg'
    Title = 'Select File'
    Left = 8
    Top = 104
  end
  object UploadReportDialog: TOpenDialog
    Filter = '*.rwa|*.rwa'
    Title = 'Select File'
    Left = 48
    Top = 104
  end
end
