unit MyTCPServerUnit;

interface

uses SysUtils, Classes, SBLicenseUnit, SyncObjs, ISErrorUtils, EvTransportShared,
  EvTransportInterfaces, EvTransportDatagrams, BBExternalInterfaces, isBaseClasses, BButilsUnit;

type
  // new interface and class
  IClientThreadData = interface
    ['{A676D5C9-7EA4-487E-A55E-8D6F06E4CE28}']
    function  GetRefreshLicInfo : boolean;
    procedure SetRefreshLicInfo(const Value: boolean);
    function  GetRefreshAll : boolean;
    procedure SetRefreshAll(const Value: boolean);
    function  GetRefreshStatusInfo : boolean;
    procedure SetRefreshStatusInfo(const Value: boolean);
    function  GetRefreshRWRList : boolean;
    procedure SetRefreshRWRList(const Value: boolean);
    function  GetRefreshLicUpdStatusInfo : boolean;
    procedure SetRefreshLicUpsStatusInfo(const Value: boolean);
    function  GetSessionId : TisGUID;
    procedure SetSessionId(const ASessionID: TisGUID);

    function GetSerialNumber : String;

    function GetReportIsready : boolean;
    procedure SetReportIsReady(AValue : boolean);

    function GetJustCreated : boolean;
    procedure SetJustCreated(AValue : boolean);

    function GetModifier : integer;
    procedure SetModifier(AModifier : integer);

    function GetLock : IisLock;
  end;

  IClientThreadDataExt = interface
    ['{97975184-1690-4C14-917A-454C6D339C0F}']
    function  GetRefreshLicInfo : boolean;
    procedure SetRefreshLicInfo(const Value: boolean);
    function  GetRefreshAll : boolean;
    procedure SetRefreshAll(const Value: boolean);
    function  GetRefreshStatusInfo : boolean;
    procedure SetRefreshStatusInfo(const Value: boolean);
    function  GetRefreshRWRList : boolean;
    procedure SetRefreshRWRList(const Value: boolean);
    function  GetRefreshLicUpdStatusInfo : boolean;
    procedure SetRefreshLicUpsStatusInfo(const Value: boolean);

    function  GetReportIsready : boolean;
    procedure SetReportIsReady(AValue : boolean);

    function  GetJustCreated : boolean;
    procedure SetJustCreated(AValue : boolean);

    function  GetLock : IisLock;

    function  GetSerialNumber : String;

    function  GetLic : TSBLicense;
    procedure SetLic (ALic : TSBLicense);

    function  GetAcceptRWUpdate : boolean;
    procedure SetAcceptRWUpdate (AValue : boolean);

    function  GetAcceptLicenseUpdate : boolean;
    procedure SetAcceptLicenseUpdate(AValue : boolean);

    function  GetSessionId : TisGUID;
    procedure SetSessionId(const ASessionID: TisGUID);

    function  GetRBForBB : IevBBController;

    function  GetModifier : integer;
    procedure SetModifier(AModifier : integer);
  end;

  TClientThreadData = class(TInterfacedObject, IClientThreadData, IClientThreadDataExt, IBigBrother)
  private
    FRefreshLicInfo: boolean;
    FRefreshAll: boolean;
    FRefreshStatusInfo: boolean;
    FRefreshRWRList: boolean;
    FRefreshLicUpdStatusInfo: boolean;
    FReportIsReady : boolean;
    FJustCreated : boolean;

    FAcceptRWUpdate : boolean;
    FAcceptLicenseUpdate : boolean;
    FLock : IisLock;

    FSerialNumber: String;
    FModifier : integer;
  protected
    Lic : TSBLicense;
    FSessionId: TisGUID;
    FRBForBB : IevBBController;
    function GetRefreshLicInfo : boolean;
    procedure SetRefreshLicInfo(const Value: boolean);
    function GetRefreshAll : boolean;
    procedure SetRefreshAll(const Value: boolean);
    function GetRefreshStatusInfo : boolean;
    procedure SetRefreshStatusInfo(const Value: boolean);
    function GetRefreshRWRList : boolean;
    procedure SetRefreshRWRList(const Value: boolean);
    function GetRefreshLicUpdStatusInfo : boolean;
    procedure SetRefreshLicUpsStatusInfo(const Value: boolean);

    function GetReportIsready : boolean;
    procedure SetReportIsReady(AValue : boolean);

    function GetJustCreated : boolean;
    procedure SetJustCreated(AValue : boolean);

    function GetSerialNumber : String;

    function GetLic : TSBLicense;
    procedure SetLic (ALic : TSBLicense);

    function GetAcceptRWUpdate : boolean;
    procedure SetAcceptRWUpdate (AValue : boolean);

    function GetAcceptLicenseUpdate : boolean;
    procedure SetAcceptLicenseUpdate(AValue : boolean);
  public
    property RefreshAll : boolean read GetRefreshAll write SetRefreshAll;
    property RefreshLicInfo : boolean read GetRefreshLicInfo write SetRefreshLicInfo;
    property RefreshStatusInfo : boolean read GetRefreshStatusInfo write SetRefreshStatusInfo;
    property RefreshRWRList : boolean read GetRefreshRWRList write SetRefreshRWRList;
    property RefreshLicUpdStatusInfo : boolean read GetRefreshLicUpdStatusInfo write SetRefreshLicUpsStatusInfo;

    function GetLock : IisLock;

    function GenerateLicense(AModifier : integer) : String;

    function GetSessionId : TisGUID;
    procedure SetSessionId(const ASessionID: TisGUID);

    function GetRBForBB : IevBBController;

    function GetModifier : integer;
    procedure SetModifier(AModifier : integer);

    procedure ReportReady(const ASerialNumber: String; const AReportNumber : string);

    constructor Create(const ASessionId : TisGUID; const ASerialNumber: String);
    destructor Destroy; override;
  end;

implementation

uses RDProxyUnit, NewBBunit, GlovalObjectsUnit, ConnectionsPoolUnit, isLogFile,
  SBLicenseWriterUnit;

{ TClientThreadData }

constructor TClientThreadData.Create(const ASessionId : TisGUID; const ASerialNumber: String);
begin
  inherited Create;
  FLock := TisLock.Create;
  FAcceptRWUpdate := True;
  FAcceptLicenseUpdate := True;
  FSerialNumber := ASerialNumber;
  Lic := TSBLicense.Create;
  FSessionId := ASessionId;
  FRBForBB := TRBForBB.Create(FSessionId);
  FModifier := 0;
  FJustCreated := true;
end;

destructor TClientThreadData.Destroy;
begin
  Lic.Free;
  inherited;
end;

function TClientThreadData.GetAcceptLicenseUpdate: boolean;
begin
  result := FAcceptLicenseUpdate;
end;

function TClientThreadData.GetAcceptRWUpdate: boolean;
begin
  result := FAcceptRWUpdate;
end;

function TClientThreadData.GetLic: TSBLicense;
begin
  result := Lic;
end;

function TClientThreadData.GenerateLicense(AModifier: integer): String;
var
  tmpConnection : IBBConnection;
begin
  if not FAcceptLicenseUpdate then
  begin
    // RD don't want to update
    Lic.LicenseUpdateStatus := '3';
    NewBB.SaveLicUpdStatus(Lic.SerialNumber, '3');
    if (NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug) then  // Debug
       GlobalLogFile.AddEvent(etWarning, BBEventClass, 'License not updated', 'License not updated. Section ' + IntToStr(Lic.SerialNumber));
  end
  else
  begin
    result := GenerateCode(Lic.SerialNumber, AModifier, Lic);
    // get connection
    tmpConnection := NewBB.Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
    try
      Lic.LicenseUpdated := Date;
      Lic.LicenseUpdateStatus := '0'; // update's been done
      try
        tmpConnection.GetWriter.WriteStatusInfo(Lic);
        tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
      except
        on E:Exception do
        begin
          tmpConnection.IncErrorCount;
          if NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
            GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call WriteStatusInfo. (statement 4)', 'Error during call WriteStatusInfo. (statement 4)' + E.Message + ' Section ' + IntToStr(Lic.SerialNumber));
          raise;
        end;
      end;
    finally
      // free connection interface
      NewBB.Global.GetConnectionPool.ReleaseSlot(tmpConnection);
      tmpConnection := nil;
    end;

    if (NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug) then  // Debug
       GlobalLogFile.AddEvent(etInformation, BBEventClass, 'License updated', 'License updated. Section ' + IntToStr(Lic.SerialNumber));
  end;
end;

function TClientThreadData.GetRBForBB: IevBBController;
begin
  result := FRBForBB;
end;

function TClientThreadData.GetRefreshAll: boolean;
begin
  result := FRefreshAll;
end;

function TClientThreadData.GetRefreshlicInfo: boolean;
begin
  result := FRefreshLicInfo;
end;

function TClientThreadData.GetRefreshLicUpdStatusInfo: boolean;
begin
  result := FRefreshLicUpdStatusInfo;
end;

function TClientThreadData.GetRefreshRWRList: boolean;
begin
  result := FRefreshRWRList;
end;

function TClientThreadData.GetRefreshStatusInfo: boolean;
begin
  result := FRefreshStatusInfo;
end;

function TClientThreadData.GetSessionId: TisGUID;
begin
  result := FSessionId;
end;

procedure TClientThreadData.SetAcceptLicenseUpdate(AValue: boolean);
begin
  FAcceptLicenseUpdate := AValue;
end;

procedure TClientThreadData.SetAcceptRWUpdate(AValue: boolean);
begin
  FAcceptRWUpdate := AValue;
end;

procedure TClientThreadData.SetLic(ALic: TSBLicense);
begin
  if not Assigned(ALic) then
    raise Exception.Create('You cannot set TSBLicense to nil!');
  Lic := ALic;
end;

procedure TClientThreadData.SetRefreshAll(const Value: boolean);
begin
  FRefreshAll := Value;
end;

procedure TClientThreadData.SetRefreshLicInfo(const Value: boolean);
begin
  FRefreshLicInfo := Value;
end;

procedure TClientThreadData.SetRefreshLicUpsStatusInfo(
  const Value: boolean);
begin
  FRefreshLicUpdStatusInfo := Value;
end;

procedure TClientThreadData.SetRefreshRWRList(const Value: boolean);
begin
  FRefreshRWRList := Value;
end;

procedure TClientThreadData.SetRefreshStatusInfo(const Value: boolean);
begin
  FRefreshStatusInfo := Value;
end;

function TClientThreadData.GetLock: IisLock;
begin
  result := FLock;
end;

function TClientThreadData.GetModifier: integer;
begin
  if FModifier = 0 then
    raise Exception.Create('Modifier isn''t set!')
  else
    result := FModifier;
end;

procedure TClientThreadData.SetModifier(AModifier: integer);
begin
  FModifier := AModifier;
end;

procedure TClientThreadData.ReportReady(const ASerialNumber: String; const AReportNumber: String);
begin
  if AReportNumber = RunReport_Billing then
    SetReportIsReady(true)
  else
    GlobalLogFile.AddEvent(etWarning, BBEventClass, 'ReportReady: unsupported report number', 'ReportReady: unsupported report number ' + AReportNumber);
end;

function TClientThreadData.GetReportIsready: boolean;
begin
  result := FReportIsReady;
end;

procedure TClientThreadData.SetReportIsReady(AValue: boolean);
begin
  FReportIsReady := AValue;
end;

function TClientThreadData.GetJustCreated: boolean;
begin
  result := FJustCreated;
end;

procedure TClientThreadData.SetJustCreated(AValue: boolean);
begin
  FJustCreated := AValue;
end;

function TClientThreadData.GetSerialNumber: String;
begin
  result := FSerialNumber;
end;

procedure TClientThreadData.SetSessionId(const ASessionID: TisGUID);
var
  oldSession: IEvSession;
begin
  oldSession := NewBB.TCPServer.DatagramDispatcher.FindSessionByID(FSessionId);
  FSessionId := ASessionId;
  FRBForBB := TRBForBB.Create(FSessionId);

  try
    if Assigned(oldSession) then
      NewBB.TCPServer.DatagramDispatcher.DeleteSession(oldSession);
  except
  end;

end;

end.


