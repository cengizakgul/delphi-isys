object EditForm: TEditForm
  Left = 341
  Top = 121
  Width = 569
  Height = 570
  Caption = 'EditForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 561
    Height = 504
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 5
      Width = 64
      Height = 13
      Caption = 'Serial number'
    end
    object Label2: TLabel
      Left = 16
      Top = 29
      Width = 101
      Height = 13
      Caption = 'Service bureau name'
    end
    object Label3: TLabel
      Left = 16
      Top = 54
      Width = 94
      Height = 13
      Caption = 'Report grace period'
    end
    object Label4: TLabel
      Left = 197
      Top = 56
      Width = 85
      Height = 13
      Caption = 'License expiration'
    end
    object Label5: TLabel
      Left = 370
      Top = 57
      Width = 87
      Height = 13
      Caption = 'License frequency'
    end
    object SNEdit: TEdit
      Left = 128
      Top = 5
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object SBNEdit: TEdit
      Left = 128
      Top = 29
      Width = 417
      Height = 21
      MaxLength = 80
      TabOrder = 1
    end
    object RGPEdit: TEdit
      Left = 128
      Top = 52
      Width = 35
      Height = 21
      MaxLength = 3
      TabOrder = 2
    end
    object LEEdit: TEdit
      Left = 296
      Top = 53
      Width = 35
      Height = 21
      MaxLength = 3
      TabOrder = 3
    end
    object LFEdit: TEdit
      Left = 473
      Top = 53
      Width = 35
      Height = 21
      MaxLength = 3
      TabOrder = 4
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 81
      Width = 529
      Height = 185
      Caption = ' Licenses '
      TabOrder = 5
      object Label6: TLabel
        Left = 88
        Top = 12
        Width = 57
        Height = 13
        Caption = 'Licenses list'
      end
      object Label7: TLabel
        Left = 376
        Top = 12
        Width = 78
        Height = 13
        Caption = 'Allowed licenses'
      end
      object LicList: TListBox
        Left = 16
        Top = 28
        Width = 209
        Height = 145
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
      end
      object AllowedList: TListBox
        Left = 312
        Top = 28
        Width = 201
        Height = 145
        ItemHeight = 13
        Sorted = True
        TabOrder = 1
      end
      object AddButton: TButton
        Left = 232
        Top = 69
        Width = 75
        Height = 25
        Caption = 'Add >'
        TabOrder = 2
        OnClick = AddButtonClick
      end
      object RemoveButton: TButton
        Left = 232
        Top = 109
        Width = 75
        Height = 25
        Caption = '< Remove'
        TabOrder = 3
        OnClick = RemoveButtonClick
      end
    end
    object GroupBox2: TGroupBox
      Left = 16
      Top = 272
      Width = 529
      Height = 225
      Caption = ' Allowed API Keys'
      TabOrder = 6
      object APIKeysGrid: TDBGrid
        Left = 16
        Top = 16
        Width = 499
        Height = 171
        DataSource = BBConfigData.IBLicAPIKeysDataSource
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'APPLICATION_NAME'
            Title.Caption = 'Application Name'
            Width = 220
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'API_KEY'
            ReadOnly = True
            Title.Caption = 'API Key'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SERIAL_NUMBER'
            Title.Caption = 'Serial Number'
            Visible = False
          end>
      end
      object DBNavigator1: TDBNavigator
        Left = 151
        Top = 194
        Width = 240
        Height = 25
        DataSource = BBConfigData.IBLicAPIKeysDataSource
        TabOrder = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 504
    Width = 561
    Height = 32
    Align = alBottom
    TabOrder = 1
    object SaveButton: TButton
      Left = 128
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 0
      OnClick = SaveButtonClick
    end
    object CancelButton: TButton
      Left = 360
      Top = 4
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
