program ImportCGF;

uses
  Forms,
  ImportCfgFormUnit in 'ImportCfgFormUnit.pas' {ImportCFGForm},
  ImportDataModuleUnit in 'ImportDataModuleUnit.pas' {ImportDataModule: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TImportCFGForm, ImportCFGForm);
  Application.CreateForm(TImportDataModule, ImportDataModule);
  Application.Run;
end.
