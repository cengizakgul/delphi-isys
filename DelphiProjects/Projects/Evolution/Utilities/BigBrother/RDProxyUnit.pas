unit RDProxyUnit;

interface

uses SysUtils, Classes, BBExternalInterfaces, isBaseClasses, evTransportInterfaces,
     EvTransportDatagrams;

type
  TRBForBB = class (TInterfacedObject, IevBBController)
  private
    FSessionID: TisGUID;
  protected
    function CheckSession(const ASerialNumber: String; const ASessionID: TisGUID) : IevSession;
  public
    function GetSbInfo(const ASerialNumber: String) : IisListofValues;

    function RunReport(const ASerialNumber: String; const AReportNumber : string; const AReportParams : IisListofValues) : IisListOfValues;
    function GetReportResults(const ASerialNumber: String; const AReportNumber : string)  : IisListOfValues;

    procedure UpdateSystemReport(const ASerialNumber: String; const AReportNumber : string; const AReportData : IisListOfValues);

    procedure SetNewLicense(const ASerialNumber: String; const ALicense : String);

    constructor Create(ASessionID: TisGUID);
    destructor Destroy; override;
  end;

implementation

uses NewBBUnit;

{ TRDForBB }

function TRBForBB.CheckSession(const ASerialNumber: String; const ASessionID: TisGUID): IevSession;
var
  NewSessionId: TisGUID;
begin
  result := NewBB.TCPServer.DatagramDispatcher.FindSessionByID(ASessionID);
  if not Assigned(result) then
  begin
    NewBB.DeleteRB(ASessionID);

    NewSessionId := NewBB.TryToFindAnotherSession(ASerialNumber);
    if NewSessionId <> '' then
    begin
      // we found a new session
      FSessionID := NewSessionId;
      result := NewBB.TCPServer.DatagramDispatcher.FindSessionByID(NewSessionId);
      if not Assigned(result) then
      begin
        NewBB.DeleteRB(NewSessionId);
        raise Exception.Create('RBForBB proxy: Session doesn''t exist. SessionId: ' + NewSessionId);
      end;
    end
    else
      raise Exception.Create('RBForBB proxy: Session doesn''t exist. SessionId: ' + ASessionID);
  end;
end;

constructor TRBForBB.Create(ASessionID: TisGUID);
begin
  inherited Create;
  FSessionId := ASessionId;
end;

destructor TRBForBB.Destroy;
begin
  inherited;
end;

function TRBForBB.GetReportResults(const ASerialNumber: String; const AReportNumber: String): IisListOfValues;
var
  Session: IevSession;
  D: IevDatagram;
begin
  Session := CheckSession(ASerialNumber, FSessionID);

  D := TevDatagram.Create(Method_RB_GetReportResults);
  D.Header.SessionID := FSessionID;  
  D.Params.AddValue('SerialNumber', ASerialNumber);
  D.Params.AddValue('AReportNumber', AReportNumber);

  D := Session.SendRequest(D);
  Result := IInterface(D.Params.Value[Method_Result]) as IisListOfValues;
end;

function TRBForBB.GetSbInfo(const ASerialNumber: String) : IisListofValues;
var
  Session: IevSession;
  D: IevDatagram;
begin
  Session := CheckSession(ASerialNumber, FSessionID);

  D := TevDatagram.Create(Method_RB_GetSbInfo);
  D.Header.SessionID := FSessionID;
  D.Params.AddValue('SerialNumber', ASerialNumber);

  D := Session.SendRequest(D);
  Result := IInterface(D.Params.Value[Method_Result]) as IisListOfValues;
end;

procedure TRBForBB.SetNewLicense(const ASerialNumber: String; const ALicense: String);
var
  Session: IevSession;
  D: IevDatagram;
begin
  Session := CheckSession(ASerialNumber, FSessionID);

  D := TevDatagram.Create(Method_RB_SetNewLicense);
  D.Header.SessionID := FSessionID;
  D.Params.AddValue('SerialNumber', ASerialNumber);
  D.Params.AddValue('ALicense', ALicense);

  Session.SendRequest(D);
end;

function TRBForBB.RunReport(const ASerialNumber: String; const AReportNumber: string;  const AReportParams: IisListofValues): IisListOfValues;
var
  Session: IevSession;
  D: IevDatagram;
begin
  Session := CheckSession(ASerialNumber, FSessionID);

  D := TevDatagram.Create(Method_RB_RunReport);
  D.Header.SessionID := FSessionID;
  D.Params.AddValue('SerialNumber', ASerialNumber);
  D.Params.AddValue('AReportNumber', AReportNumber);
  D.Params.AddValue('AReportParams', AReportParams);

  D := Session.SendRequest(D);

  Result := IInterface(D.Params.Value[Method_Result]) as IisListOfValues;
end;

procedure TRBForBB.UpdateSystemReport(const ASerialNumber: String; const AReportNumber: string; const AReportData: IisListOfValues);
var
  Session: IevSession;
  D: IevDatagram;
begin
  Session := CheckSession(ASerialNumber, FSessionID);

  D := TevDatagram.Create(Method_RB_UpdateSystemReport);
  D.Header.SessionID := FSessionID;
  D.Params.AddValue('SerialNumber', ASerialNumber);
  D.Params.AddValue('AReportNumber', AReportNumber);
  D.Params.AddValue('AReportData', AReportData);

  Session.SendRequest(D);
end;

end.
