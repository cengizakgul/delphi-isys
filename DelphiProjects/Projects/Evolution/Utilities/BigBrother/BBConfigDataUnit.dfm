object BBConfigData: TBBConfigData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 332
  Top = 294
  Height = 360
  Width = 603
  object IBDatabase: TIBDatabase
    Params.Strings = (
      'user_name=BigBrother')
    LoginPrompt = False
    Left = 32
    Top = 24
  end
  object IBTransaction: TIBTransaction
    DefaultDatabase = IBDatabase
    Params.Strings = (
      'read'
      'read_committed'
      'rec_version')
    Left = 112
    Top = 24
  end
  object IBLicInfo: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    OnCalcFields = IBLicInfoCalcFields
    BufferChunks = 2000
    SQL.Strings = (
      
        'select a.serial_number, service_bureau_name, license_expiration,' +
        ' license_frequency, report_grace_period,'
      
        'connected, database_version, database_version_sent, date_time_co' +
        'nnected, last_adress, license_updated,'
      
        'report_ran, report_status, report_updated, server_version, serve' +
        'r_version_sent, report_file, license_update_status'
      
        'from lic_info a left join status_info b on a.serial_number=b.ser' +
        'ial_number'
      'order by'
      '/*SORT1*/'
      'a.serial_number'
      '/*SORT2*/')
    Left = 184
    Top = 8
    object IBLicInfoSERIAL_NUMBER: TIntegerField
      DisplayLabel = 'Serial Number'
      DisplayWidth = 11
      FieldName = 'SERIAL_NUMBER'
      Origin = '"LIC_INFO"."SERIAL_NUMBER"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IBLicInfoSERVICE_BUREAU_NAME: TIBStringField
      DisplayLabel = 'Name'
      DisplayWidth = 31
      FieldName = 'SERVICE_BUREAU_NAME'
      Origin = '"LIC_INFO"."SERVICE_BUREAU_NAME"'
      Required = True
      Size = 80
    end
    object IBLicInfoLAST_ADRESS: TIBStringField
      DisplayLabel = 'IP Adress'
      DisplayWidth = 16
      FieldName = 'LAST_ADRESS'
      Origin = '"STATUS_INFO"."LAST_ADRESS"'
      Visible = False
    end
    object IBLicInfoDATE_TIME_CONNECTED: TDateTimeField
      DisplayLabel = 'Last connected'
      DisplayWidth = 20
      FieldName = 'DATE_TIME_CONNECTED'
      Origin = '"STATUS_INFO"."DATE_TIME_CONNECTED"'
      Visible = False
    end
    object IBLicInfoLICENSE_FREQUENCY: TIntegerField
      DisplayWidth = 19
      FieldName = 'LICENSE_FREQUENCY'
      Origin = '"LIC_INFO"."LICENSE_FREQUENCY"'
      Visible = False
    end
    object IBLicInfoREPORT_GRACE_PERIOD: TIntegerField
      DisplayWidth = 22
      FieldName = 'REPORT_GRACE_PERIOD'
      Origin = '"LIC_INFO"."REPORT_GRACE_PERIOD"'
      Visible = False
    end
    object IBLicInfoCONNECTED: TIBStringField
      DisplayWidth = 11
      FieldName = 'CONNECTED'
      Origin = '"STATUS_INFO"."CONNECTED"'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object IBLicInfoDATABASE_VERSION: TIBStringField
      DisplayWidth = 30
      FieldName = 'DATABASE_VERSION'
      Origin = '"STATUS_INFO"."DATABASE_VERSION"'
      Visible = False
      Size = 30
    end
    object IBLicInfoLICENSE_EXPIRATION: TIntegerField
      DisplayWidth = 19
      FieldName = 'LICENSE_EXPIRATION'
      Origin = '"LIC_INFO"."LICENSE_EXPIRATION"'
      Visible = False
    end
    object IBLicInfoDATABASE_VERSION_SENT: TIBStringField
      DisplayWidth = 30
      FieldName = 'DATABASE_VERSION_SENT'
      Origin = '"STATUS_INFO"."DATABASE_VERSION_SENT"'
      Visible = False
      Size = 30
    end
    object IBLicInfoLICENSE_UPDATED: TDateTimeField
      DisplayWidth = 18
      FieldName = 'LICENSE_UPDATED'
      Origin = '"STATUS_INFO"."LICENSE_UPDATED"'
      Visible = False
    end
    object IBLicInfoREPORT_RAN: TDateTimeField
      DisplayWidth = 18
      FieldName = 'REPORT_RAN'
      Origin = '"STATUS_INFO"."REPORT_RAN"'
      Visible = False
    end
    object IBLicInfoREPORT_STATUS: TIBStringField
      DisplayWidth = 15
      FieldName = 'REPORT_STATUS'
      Origin = '"STATUS_INFO"."REPORT_STATUS"'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object IBLicInfoREPORT_UPDATED: TDateTimeField
      DisplayWidth = 18
      FieldName = 'REPORT_UPDATED'
      Origin = '"STATUS_INFO"."REPORT_UPDATED"'
      Visible = False
    end
    object IBLicInfoSERVER_VERSION: TIBStringField
      DisplayLabel = 'Server'
      DisplayWidth = 30
      FieldName = 'SERVER_VERSION'
      Origin = '"STATUS_INFO"."SERVER_VERSION"'
      Visible = False
      Size = 30
    end
    object IBLicInfoSERVER_VERSION_SENT: TIBStringField
      DisplayWidth = 30
      FieldName = 'SERVER_VERSION_SENT'
      Origin = '"STATUS_INFO"."SERVER_VERSION_SENT"'
      Visible = False
      Size = 30
    end
    object IBLicInfoCONNECTION_STATUS: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONNECTION_STATUS'
      Visible = False
      Calculated = True
    end
    object IBLicInfoOpenReport: TStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'OpenReport'
      Visible = False
      Size = 1000
      Calculated = True
    end
    object IBLicInfoRunReportNow: TStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'RunReportNow'
      Visible = False
      Size = 10
      Calculated = True
    end
    object IBLicInfoReportStatus: TStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'ReportStatus'
      Visible = False
      Size = 30
      Calculated = True
    end
    object IBLicInfoL0: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L0'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L1'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L2'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L3'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL4: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L4'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL5: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L5'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL6: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L6'
      Visible = False
      Calculated = True
    end
    object IBLicInfoL7: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L7'
      Calculated = True
    end
    object IBLicInfoL8: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'L8'
      Calculated = True
    end
    object IBLicInfoREPORT_FILE: TBlobField
      FieldName = 'REPORT_FILE'
      Origin = '"STATUS_INFO"."REPORT_FILE"'
      ProviderFlags = [pfInUpdate]
      Size = 8
    end
    object IBLicInfoLICENSE_UPDATE_STATUS: TIBStringField
      FieldName = 'LICENSE_UPDATE_STATUS'
      Origin = '"STATUS_INFO"."LICENSE_UPDATE_STATUS"'
      FixedChar = True
      Size = 1
    end
    object IBLicInfoLicenseUpdateStatus: TStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'LicenseUpdateStatus'
      Calculated = True
    end
    object IBLicInfoUpdateLicenseNow: TStringField
      FieldKind = fkCalculated
      FieldName = 'UpdateLicenseNow'
      Size = 15
      Calculated = True
    end
  end
  object IBLicInfoDataSource: TDataSource
    DataSet = IBLicInfo
    Left = 232
    Top = 24
  end
  object IBPackageType: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    OnCalcFields = IBPackageTypeCalcFields
    SQL.Strings = (
      'SELECT * FROM PACKAGE_TYPE order by PACKAGE_TYPE_NBR')
    Left = 408
    Top = 8
    object IBPackageTypeDefaultValueIndicator: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DefaultValueIndicator'
      Calculated = True
    end
    object IBPackageTypeDEFAULT_VALUE: TIBStringField
      FieldName = 'DEFAULT_VALUE'
      Origin = '"PACKAGE_TYPE"."DEFAULT_VALUE"'
      FixedChar = True
      Size = 1
    end
    object IBPackageTypeLONG_DESCRIPTION: TIBStringField
      FieldName = 'LONG_DESCRIPTION'
      Origin = '"PACKAGE_TYPE"."LONG_DESCRIPTION"'
      Required = True
      Size = 50
    end
    object IBPackageTypePACKAGE_TYPE_NBR: TIntegerField
      FieldName = 'PACKAGE_TYPE_NBR'
      Origin = '"PACKAGE_TYPE"."PACKAGE_TYPE_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IBPackageTypeSHORT_DESCRIPTION: TIBStringField
      FieldName = 'SHORT_DESCRIPTION'
      Origin = '"PACKAGE_TYPE"."SHORT_DESCRIPTION"'
      Required = True
      Size = 5
    end
  end
  object IBLicPackage: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      'select  SERIAL_NUMBER, PACKAGE_TYPE_NBR from LIC_PACKAGE'
      'where SERIAL_NUMBER=:P_SERIAL_NUMBER'
      'order by PACKAGE_TYPE_NBR')
    Left = 32
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P_SERIAL_NUMBER'
        ParamType = ptInput
      end>
  end
  object IBTotals: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      'select '#39'Total rows'#39' name, count(*) amount from lic_info'
      'union all'
      'select '#39'Inactive rows'#39', count(*) from lic_info a'
      'where not exists (select 1 from lic_package b '
      'where b.serial_number=a.serial_number)')
    Left = 104
    Top = 96
  end
  object IBDefaultInfo: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    SQL.Strings = (
      'select * from default_info')
    Left = 296
    Top = 8
  end
  object IBDefaultInfoDataSource: TDataSource
    DataSet = IBDefaultInfo
    Left = 344
    Top = 40
  end
  object IBPackageTypeDataSource: TDataSource
    DataSet = IBPackageType
    Left = 464
    Top = 24
  end
  object IBRWRList: TIBQuery
    Database = IBDatabase
    Transaction = IBTransaction
    OnCalcFields = IBRWRListCalcFields
    SQL.Strings = (
      'select * from rwr_list where serial_number=:p1'
      'order by rwr_list_nbr')
    Left = 184
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p1'
        ParamType = ptUnknown
      end>
    object IBRWRListRWR_LIST_NBR: TIntegerField
      DisplayLabel = 'Task number'
      DisplayWidth = 10
      FieldName = 'RWR_LIST_NBR'
      Origin = '"RWR_LIST"."RWR_LIST_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object IBRWRListREPORT_NUMBER: TIntegerField
      DisplayLabel = 'Report number'
      DisplayWidth = 10
      FieldName = 'REPORT_NUMBER'
      Origin = '"RWR_LIST"."REPORT_NUMBER"'
      Required = True
    end
    object IBRWRListStatusText: TStringField
      DisplayLabel = 'Status'
      DisplayWidth = 60
      FieldKind = fkCalculated
      FieldName = 'StatusText'
      Size = 60
      Calculated = True
    end
    object IBRWRListREPORT_FILE: TBlobField
      DisplayWidth = 10
      FieldName = 'REPORT_FILE'
      Origin = '"RWR_LIST"."REPORT_FILE"'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 8
    end
    object IBRWRListSERIAL_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'SERIAL_NUMBER'
      Origin = '"RWR_LIST"."SERIAL_NUMBER"'
      Required = True
      Visible = False
    end
    object IBRWRListTASK_STATUS: TIBStringField
      DisplayWidth = 1
      FieldName = 'TASK_STATUS'
      Origin = '"RWR_LIST"."TASK_STATUS"'
      Required = True
      Visible = False
      FixedChar = True
      Size = 1
    end
  end
  object IBRWRListDataSource: TDataSource
    DataSet = IBRWRList
    Left = 232
    Top = 112
  end
  object IBServerEvents: TIBEvents
    AutoRegister = False
    Database = IBDatabase
    Events.Strings = (
      'SRV_RWR_CHANGED'
      'SRV_STATUS_CHANGED')
    Registered = False
    OnEventAlert = IBServerEventsEventAlert
    Left = 328
    Top = 104
  end
  object IBVendor: TIBQuery
    Database = IBDatabase
    Transaction = IBManualTransactions
    BufferChunks = 2000
    CachedUpdates = True
    SQL.Strings = (
      'select vendor_custom_nbr, vendor_name from vendor'
      'order by vendor_custom_nbr')
    UpdateObject = IBVendorUpdate
    Left = 32
    Top = 160
    object IBVendorVENDOR_CUSTOM_NBR: TIntegerField
      DisplayLabel = 'Custom Nbr'
      DisplayWidth = 10
      FieldName = 'VENDOR_CUSTOM_NBR'
      Origin = '"VENDOR"."VENDOR_CUSTOM_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      MaxValue = 999999999
      MinValue = 1
    end
    object IBVendorVENDOR_NAME: TIBStringField
      DisplayLabel = 'Name'
      DisplayWidth = 50
      FieldName = 'VENDOR_NAME'
      KeyFields = 'VENDOR_CUSTOM_NBR'
      Origin = '"VENDOR"."VENDOR_NAME"'
      Required = True
      Size = 50
    end
  end
  object IBVendorDataSource: TDataSource
    DataSet = IBVendor
    Left = 88
    Top = 176
  end
  object IBVendorUpdate: TIBUpdateSQL
    RefreshSQL.Strings = (
      'Select *'
      'from vendor '
      'where'
      '  VENDOR_CUSTOM_NBR = :VENDOR_CUSTOM_NBR')
    ModifySQL.Strings = (
      'update vendor'
      'set'
      '  VENDOR_CUSTOM_NBR = :VENDOR_CUSTOM_NBR,'
      '  VENDOR_NAME = :VENDOR_NAME'
      'where'
      '  VENDOR_CUSTOM_NBR = :OLD_VENDOR_CUSTOM_NBR')
    InsertSQL.Strings = (
      'insert into vendor'
      '  (VENDOR_CUSTOM_NBR, VENDOR_NAME)'
      'values'
      '  (:VENDOR_CUSTOM_NBR, :VENDOR_NAME)')
    DeleteSQL.Strings = (
      'delete from vendor'
      'where'
      '  VENDOR_CUSTOM_NBR = :OLD_VENDOR_CUSTOM_NBR')
    Left = 168
    Top = 192
  end
  object IBManualTransactions: TIBTransaction
    AllowAutoStart = False
    DefaultDatabase = IBDatabase
    Params.Strings = (
      'write'
      'read_committed'
      'rec_version'
      'nowait')
    Left = 520
    Top = 264
  end
  object IBAPIKeys: TIBQuery
    Database = IBDatabase
    Transaction = IBManualTransactions
    OnNewRecord = IBAPIKeysNewRecord
    BufferChunks = 2000
    CachedUpdates = True
    SQL.Strings = (
      'select'
      '  API_KEY, APP_KEY_TYPE, APPLICATION_NAME, VENDOR_CUSTOM_NBR'
      'from'
      '  API_KEY'
      'order by'
      '  APPLICATION_NAME')
    UpdateObject = IBAPIKeysUpdateSQL
    Left = 32
    Top = 232
    object IBAPIKeysAPI_KEY: TIBStringField
      FieldName = 'API_KEY'
      Origin = '"API_KEY"."API_KEY"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 40
    end
    object IBAPIKeysAPP_KEY_TYPE: TIBStringField
      DefaultExpression = 'V'
      FieldName = 'APP_KEY_TYPE'
      Origin = '"API_KEY"."APP_KEY_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object IBAPIKeysAPPLICATION_NAME: TIBStringField
      FieldName = 'APPLICATION_NAME'
      Origin = '"API_KEY"."APPLICATION_NAME"'
      Required = True
      Size = 50
    end
    object IBAPIKeysVENDOR_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'VENDOR_NAME'
      LookupDataSet = IBVendor
      LookupKeyFields = 'VENDOR_CUSTOM_NBR'
      LookupResultField = 'VENDOR_NAME'
      KeyFields = 'VENDOR_CUSTOM_NBR'
      Required = True
      Size = 50
      Lookup = True
    end
    object IBAPIKeysVENDOR_CUSTOM_NBR: TIntegerField
      FieldName = 'VENDOR_CUSTOM_NBR'
      LookupDataSet = IBVendor
      LookupKeyFields = 'VENDOR_CUSTOM_NBR'
      LookupResultField = 'VENDOR_CUSTOM_NBR'
      Origin = '"API_KEY"."VENDOR_CUSTOM_NBR"'
      Required = True
    end
  end
  object IBAPIKeysDataSource: TDataSource
    DataSet = IBAPIKeys
    Left = 88
    Top = 248
  end
  object IBAPIKeysUpdateSQL: TIBUpdateSQL
    RefreshSQL.Strings = (
      'Select '
      '  API_KEY,'
      '  APP_KEY_TYPE,'
      '  APPLICATION_NAME,'
      '  VENDOR_CUSTOM_NBR'
      'from API_KEY '
      'where'
      '  API_KEY = :API_KEY')
    ModifySQL.Strings = (
      'update API_KEY'
      'set'
      '  API_KEY = :API_KEY,'
      '  APP_KEY_TYPE = :APP_KEY_TYPE,'
      '  APPLICATION_NAME = :APPLICATION_NAME,'
      '  VENDOR_CUSTOM_NBR = :VENDOR_CUSTOM_NBR'
      'where'
      '  API_KEY = :OLD_API_KEY')
    InsertSQL.Strings = (
      'insert into API_KEY'
      '  (API_KEY, APP_KEY_TYPE, APPLICATION_NAME, VENDOR_CUSTOM_NBR)'
      'values'
      
        '  (:API_KEY, :APP_KEY_TYPE, :APPLICATION_NAME, :VENDOR_CUSTOM_NB' +
        'R)')
    DeleteSQL.Strings = (
      'delete from API_KEY'
      'where'
      '  API_KEY = :OLD_API_KEY')
    Left = 176
    Top = 264
  end
  object IBLicAPIKeys: TIBQuery
    Database = IBDatabase
    Transaction = IBManualTransactions
    OnNewRecord = IBLicAPIKeysNewRecord
    BufferChunks = 2000
    CachedUpdates = True
    SQL.Strings = (
      'select'
      '  API_KEY, SERIAL_NUMBER'
      'from  LIC_API_KEY'
      'where SERIAL_NUMBER = :p1'
      'order by   API_KEY')
    UpdateObject = IBLicAPIKeysUpdate
    Left = 264
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'p1'
        ParamType = ptInput
      end>
    object IBLicAPIKeysAPPLICATION_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'APPLICATION_NAME'
      LookupDataSet = IBAPIKeys
      LookupKeyFields = 'API_KEY'
      LookupResultField = 'APPLICATION_NAME'
      KeyFields = 'API_KEY'
      Size = 50
      Lookup = True
    end
    object IBLicAPIKeysAPI_KEY: TIBStringField
      FieldName = 'API_KEY'
      Origin = '"LIC_API_KEY"."API_KEY"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 40
    end
    object IBLicAPIKeysSERIAL_NUMBER: TIntegerField
      FieldName = 'SERIAL_NUMBER'
      Origin = '"LIC_API_KEY"."SERIAL_NUMBER"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Visible = False
    end
  end
  object IBLicAPIKeysDataSource: TDataSource
    DataSet = IBLicAPIKeys
    Left = 320
    Top = 216
  end
  object IBLicAPIKeysUpdate: TIBUpdateSQL
    RefreshSQL.Strings = (
      'Select '
      '  API_KEY,'
      '  SERIAL_NUMBER'
      'from LIC_API_KEY '
      'where'
      '  API_KEY = :API_KEY and'
      '  SERIAL_NUMBER = :SERIAL_NUMBER')
    ModifySQL.Strings = (
      'update LIC_API_KEY'
      'set'
      '  API_KEY = :API_KEY,'
      '  SERIAL_NUMBER = :SERIAL_NUMBER'
      'where'
      '  API_KEY = :OLD_API_KEY and'
      '  SERIAL_NUMBER = :OLD_SERIAL_NUMBER')
    InsertSQL.Strings = (
      'insert into LIC_API_KEY'
      '  (API_KEY, SERIAL_NUMBER)'
      'values'
      '  (:API_KEY, :SERIAL_NUMBER)')
    DeleteSQL.Strings = (
      'delete from LIC_API_KEY'
      'where'
      '  API_KEY = :OLD_API_KEY and'
      '  SERIAL_NUMBER = :OLD_SERIAL_NUMBER')
    Left = 368
    Top = 264
  end
end
