object APIKeysEditForm: TAPIKeysEditForm
  Left = 312
  Top = 114
  BorderStyle = bsSingle
  Caption = 'API keys'
  ClientHeight = 619
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 578
    Width = 814
    Height = 41
    Align = alBottom
    TabOrder = 0
    object SaveButton: TButton
      Left = 248
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 0
      OnClick = SaveButtonClick
    end
    object CancelButton: TButton
      Left = 448
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 578
    Align = alClient
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 543
      Width = 812
      Height = 34
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object DBNavigator1: TDBNavigator
        Left = 264
        Top = 6
        Width = 240
        Height = 25
        DataSource = BBConfigData.IBAPIKeysDataSource
        TabOrder = 0
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 812
      Height = 542
      Align = alClient
      DataSource = BBConfigData.IBAPIKeysDataSource
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'APPLICATION_NAME'
          Title.Caption = 'Application Name'
          Width = 233
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VENDOR_CUSTOM_NBR'
          Title.Caption = 'Vendor Custom Nbr'
          Width = -1
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'VENDOR_NAME'
          Title.Caption = 'Vendor Name'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'API_KEY'
          ReadOnly = True
          Title.Caption = 'API Key'
          Width = 215
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APP_KEY_TYPE'
          Title.Caption = 'API Key Type'
          Visible = False
        end>
    end
  end
end
