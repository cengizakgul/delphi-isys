unit NewBBUnit;

interface

uses
  SysUtils, Classes, {IdTCPServer,} SReportSettings, EvStreamUtils, DateUtils,
  IdGlobal, syncobjs, Math, ISZippingRoutines,
  OgUtil, OnGuard, Windows, ISErrorUtils,
  IdFTP, IdComponent, IdBaseComponent, SBLicenseUnit, SBLicenseReaderUnit, SBLicenseWriterUnit, MyTCPServerUnit,
  GlovalObjectsUnit, ConnectionsPoolUnit, Variants, isThreadManager, IniFiles,
  IBEvents, BBExternalInterfaces, isBaseClasses, BBConnection, BBUtilsUnit;

const
  DefaultLicenseFullRefreshTime = 60000 * 5; // 5 minuts
  DefaultSpoiledStatusRefreshTime = 15000;  // 15 sec
  DefaultSleepinCircle = 10; // time in millisec
  FindSlotWaitTime = 5000; // time to wait for free slot
  MaxRefreshSkipped = 2000; // times, considert its value in connection with DefaultSleepCircle

type

  TIntegerArray = array of integer;

  PSpoiledStatus = ^TSpoiledStatus;
  TSpoiledStatus = record
    TimeOfDisconnect : TDateTime;
    SerialNumber : integer;
  end;

  IBB = interface
    ['{98608716-116B-4817-A319-96C7ABC70739}']
    function Global : IGlobalObject;
    function TCPServer: IbbTCPServer;
    function LicenseFullRefreshTime : cardinal; // time in milliseconds
    function SpoiledStatusRefreshTime : cardinal; // time in milliseconds
    procedure SaveLicUpdStatus(ASerialNumber : integer; ANewStatus : String);

    function FindRB(ASerialNumber: String) : IClientThreadDataExt;
    function AddRB(ASessionId : TisGUID; ASerialNumber: String; AIPAddress : String) : IClientThreadDataExt;
    function TryToFindAnotherSession(ASerialNumber: String): TisGUID;
    function GetRBList : IInterfaceList;
    procedure DeleteRB(ASessionId : TisGUID);
  end;

  TBB = class(TInterfacedObject, IBB)
  private
    FConnectionForEvent : IBBConnectionExt;
    FConnectionTimeRefreshManagerId : TTaskId;
    FSpoiledStatusRefreshManagerId : TTaskId;
    FThreadsList : array of TTaskId;
    FBBPort : String;

    procedure SaveRWRTaskStatus(const ALic :TSBLicense; ARWRListNBR : integer; ANewState : String); // can change from '0' to '1' or '2'
    function TimeToRunReport(const ALic :TSBLicense): Boolean;
    function TimeToUpdateLicense(const ALic :TSBLicense): Boolean;
    function GetRWRUpdate(const ALic :TSBLicense): TIntegerArray;
    procedure SendRWRUpdate(const ARB: IClientThreadDataExt;  ARecordNbr: integer);
    procedure AddToSpoiledList(ASerialNumber : integer; ATime : TDateTime);
    function RunReport(const ARB : IClientThreadDataExt): String;
    procedure SaveStatusToDatabase(AClientThreadData : IClientThreadDataExt);
  protected
    FRBList: IInterfaceList;
    FGlobal : IGlobalObject;
    FTCPServer: IbbTCPServer;
    FLicenseFullRefreshTime : cardinal; // time in milliseconds
    FSpoiledStatusRefreshTime : cardinal; // time in milliseconds
    SpoiledStatuses : TThreadList;

    FRefreshLoopsAmount : integer;
    FSetNewLicenseLoopsAmount : integer;
    FRunSystemReportLoopsAmount : integer;
    FGetReportResultsLoopsAmount : integer;
    FUpdateSystemReportLoopsAmount : integer;

    // it's just refresh by schedule
    procedure ConnectionTimeRefreshManager(const Params : TTaskParamList);
    procedure SpoiledStatusRefreshManager(const Params : TTaskParamList);

    procedure RefreshLoop(const Params : TTaskParamList);
    procedure SetNewLicenseLoop(const Params : TTaskParamList);
    procedure RunSystemReportLoop(const Params : TTaskParamList);
    procedure GetReportResultsLoop(const Params : TTaskParamList);
    procedure UpdateSystemReportLoop(const Params : TTaskParamList);
  public
    function Global : IGlobalObject;
    function TCPServer: IbbTCPServer;
    function GetRBList : IInterfaceList;
    function LicenseFullRefreshTime : cardinal; // time in milliseconds
    function SpoiledStatusRefreshTime : cardinal; // time in milliseconds
    function FindRB(ASerialNumber: String) : IClientThreadDataExt;
    function AddRB(ASessionId : TisGUID; ASerialNumber: String; AIPAddress : String) : IClientThreadDataExt;
    function TryToFindAnotherSession(ASerialNumber: String): TisGUID;
    procedure DeleteRB(ASessionId : TisGUID);
    procedure DeleteAllRB;
    procedure SaveLicUpdStatus(ASerialNumber : integer; ANewStatus : String);

    constructor Create;
    destructor Destroy; override;
  end;

procedure RunNewBB;
procedure StopNewBB;

function NullToStr(AParam : Variant; AIfNull : String = '') : String; // returns AIfNull string if AParam is null

var
  NewBB: IBB;

implementation

uses EvLicenseInfo, isLogFile, RDProxyUnit;

procedure RunNewBB;
begin
  NewBB := TBB.Create;
end;

procedure StopNewBB;
begin
  NewBB := nil;
end;

function TBB.RunReport(const ARB : IClientThreadDataExt): String;
var
  Params, Results: IisListofValues;
  d: TDateTime;
  sErrorText : String;
begin
  Params := TisListOfValues.Create;

  d := Date;
  if MonthOf(d) = 1 then
    d := EncodeDate(YearOf(d) - 1, 12, 1)
  else
    d := EncodeDate(YearOf(d), MonthOf(d) - 1, 1);
//  Params.AddValue('AllComp', True);
  Params.AddValue('dat_b', StartOfTheMonth(d));
  Params.AddValue('dat_e', EndOfTheMonth(d));

  Results := ARB.GetRBForBB.RunReport(IntToStr(ARB.GetLic.SerialNumber), RunReport_Billing, Params);
  Result := Results.Value[Method_Result];

  if (Result = RunReport_RunError) and (Results.ValueExists(RunReport_RunErrorException)) then
    sErrorText := Results.Value[RunReport_RunErrorException]
  else
    sErrorText := '';

  if Result = RunReport_RunOK then  // Debug
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Report ran', 'Report ran. Section ' + IntToStr(ARB.GetLic.SerialNumber))
  else
    if Result = RunReport_RunError then
      GlobalLogFile.AddEvent(etError, BBEventClass, 'Report didn''t run with Exception', 'Report didn''t run with Exception. Section ' + IntToStr(ARB.GetLic.SerialNumber) + #13#10 + sErrorText)
    else
      GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Report didn''t run', 'Report didn''t run. Section ' + IntToStr(ARB.GetLic.SerialNumber) + '. Reason: ' + Result);
end;


{ TBB }

procedure TBB.SaveLicUpdStatus(ASerialNumber : integer; ANewStatus : String);
var
  tmpConnection : IBBConnection;
begin
  tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
  try
    try
      tmpConnection.GetWriter.WriteOnlyLicenseUpdateStatus(ASerialNumber, ANewStatus);
      tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
    except
      on E:Exception do
      begin
        tmpConnection.IncErrorCount;
        if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call WriteOnlyLicenseUpdateStatus',
            'Error during call WriteOnlyLicenseUpdateStatus' + E.Message + ' Section '
            + IntToStr(ASerialNumber)+ #13#10 + GetStack);
        raise;
      end;
    end;
  finally
    // free connection interface
    Global.GetConnectionPool.ReleaseSlot(tmpConnection);
    tmpConnection := nil;
  end;
end;

procedure TBB.ConnectionTimeRefreshManager(
  const Params: TTaskParamList);
var
  cnt : cardinal;
  i : integer;
begin
  cnt := 0;
  // loop
  while not CurrentThreadTaskTerminated do
  begin
    try
      // sleep for 0.2 sec
      sleep(200);
      cnt := cnt + 200;
      if cnt >= FLicenseFullRefreshTime then
      begin
        // time for full reshresh
        cnt := 0;
        if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
          GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Time for full refresh', '');
        FRBList.Lock;
        try
          for i := 0 to FRBList.Count - 1 do
            (FRBList[i] as IClientThreadData).SetRefreshAll(True);
        finally
          FRBList.Unlock;
        end;
      end;
    except
      on E:Exception do
      begin
        try
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in ConnectionTimeRefreshManager',
            'Exception in ConnectionTimeRefreshManager' + E.Message+ #13#10 + GetStack);
          Global.GetEMail.Write('Exception in ConnectionTimeRefreshManager' + E.Message);
        except
        end;
      end;
    end;
  end;  // while
end;

constructor TBB.Create;
var
  tmp : TIniFile;
  tmpConnection : IBBConnection;
  i, j : integer;
begin
  SpoiledStatuses := TThreadList.Create;
  FGlobal := TGlobalObjects.Create;
  FRBList := TInterfaceList.Create;

  FConnectionForEvent := (Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true) as IBBConnectionExt);

  // mark all records as disconnected
  tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
  try
    tmpConnection.GetWriter.MarkAllAsDisconnected;
  finally
    Global.GetConnectionPool.ReleaseSlot(tmpConnection);
    tmpConnection := nil;
  end;

  tmp := TIniFile.Create(Global.GetIniFile);
  try
    FLicenseFullRefreshTime := tmp.ReadInteger('Database', 'LicenseFullRefreshTime', DefaultLicenseFullRefreshTime);
    FSpoiledStatusRefreshTime := tmp.ReadInteger('Database', 'SpoiledStatusRefreshTime', DefaultSpoiledStatusRefreshTime);

    FRefreshLoopsAmount := tmp.ReadInteger('General', 'RefreshLoopsAmount', 10);
    FSetNewLicenseLoopsAmount := tmp.ReadInteger('General', 'SetNewLicenseLoopsAmount', 5);
    FRunSystemReportLoopsAmount := tmp.ReadInteger('General', 'RunSystemReportLoopsAmount', 10);
    FGetReportResultsLoopsAmount := tmp.ReadInteger('General', 'GetReportResultsLoopsAmount', 10);
    FUpdateSystemReportLoopsAmount := tmp.ReadInteger('General', 'UpdateSystemReportLoopsAmount', 5);

    FBBPort := tmp.ReadString('General', 'BBPort', BBPort);

  finally
    tmp.free;
  end;

  FTCPServer := TbbTCPServer.Create;
  FTCPServer.SetPort(FBBPort);

  // must be only one such processes
  FConnectionTimeRefreshManagerId := GlobalThreadManager.RunTask(ConnectionTimeRefreshManager, Self, MakeTaskParams([]), 'Time refresh manager');
  FSpoiledStatusRefreshManagerId := GlobalThreadManager.RunTask(SpoiledStatusRefreshManager, Self, MakeTaskParams([]), 'Spoiled status refresh manager');

  // running threads for serving requests
  SetLength(FThreadsList, FRefreshLoopsAmount + FSetNewLicenseLoopsAmount +
    FRunSystemReportLoopsAmount + FGetReportResultsLoopsAmount + FUpdateSystemReportLoopsAmount);
  j := 0;
  for i := 1 to FRefreshLoopsAmount do
  begin
    FThreadsList[j] := GlobalThreadManager.RunTask(RefreshLoop, Self, MakeTaskParams([]), 'RefreshLoop thread #' + IntToStr(i));
    Inc(j);
  end;
  for i := 1 to FSetNewLicenseLoopsAmount do
  begin
    FThreadsList[j] := GlobalThreadManager.RunTask(SetNewLicenseLoop, Self, MakeTaskParams([]), 'SetNewLicenseLoop thread #' + IntToStr(i));
    Inc(j);
  end;
  for i := 1 to FRunSystemReportLoopsAmount do
  begin
    FThreadsList[j] := GlobalThreadManager.RunTask(RunSystemReportLoop, Self, MakeTaskParams([]), 'RunSystemReportLoop thread #' + IntToStr(i));
    Inc(j);
  end;
  for i := 1 to FRunSystemReportLoopsAmount do
  begin
    FThreadsList[j] := GlobalThreadManager.RunTask(GetReportResultsLoop, Self, MakeTaskParams([]), 'GetReportResultsLoop thread #' + IntToStr(i));
    Inc(j);
  end;
  for i := 1 to FUpdateSystemReportLoopsAmount do
  begin
    FThreadsList[j] := GlobalThreadManager.RunTask(UpdateSystemReportLoop, Self, MakeTaskParams([]), 'UpdateSystemReportLoop thread #' + IntToStr(i));
    Inc(j);
  end;

  try
    FTCPServer.Active := True;
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'TCPServer is started.', '');
  except
    on E:Exception do
    begin
      GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in NewBB constructor',
        'Exception in NewBB constructor. Error message:' + E.Message+ #13#10 + GetStack);
      raise;
    end;
  end;
end;

destructor TBB.Destroy;
var
  i : integer;
begin
  if Assigned(FTCPServer) then
    FTCPServer.Active := False;

  DeleteAllRB;

  for i := low(FThreadsList) to High(FThreadsList) do
    try
      if FThreadsList[i] <> 0 then
      begin
        GlobalThreadManager.TerminateTask(FThreadsList[i]);
        GlobalThreadManager.WaitForTaskEnd(FThreadsList[i]);
      end;
    except
    end;

  if FConnectionTimeRefreshManagerId <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FConnectionTimeRefreshManagerId);
    GlobalThreadManager.WaitForTaskEnd(FConnectionTimeRefreshManagerId);
  end;
  if FSpoiledStatusRefreshManagerId <> 0 then
  begin
    GlobalThreadManager.TerminateTask(FSpoiledStatusRefreshManagerId);
    GlobalThreadManager.WaitForTaskEnd(FSpoiledStatusRefreshManagerId);
  end;

  SpoiledStatuses.Clear;
  SpoiledStatuses.Free;

  inherited;
end;

function TBB.GetRWRUpdate(const ALic: TSBLicense): TIntegerArray;
var
  i: integer;
  cnt: integer;
  tmpRWRTask : TRWRTask;
begin
  cnt := 0;
  try
    // count the length
    for i := 0 to ALic.RWRTasks.Count - 1 do
    begin
      tmpRWRTask := TRWRTask(ALic.RWRTasks[i]);
      if tmpRWRTask.TaskStatus = '0' then
        Inc(cnt);
    end;
    SetLength( Result, cnt);
    // add values
    cnt := 0;
    for i := 0 to ALic.RWRTasks.Count - 1 do
    begin
      tmpRWRTask := TRWRTask(ALic.RWRTasks[i]);
      if tmpRWRTask.TaskStatus = '0' then
      begin
        Result[cnt] := tmpRWRTask.RWRListNbr;
        Inc(cnt);
      end;
    end;
    SetLength( Result, cnt);
  except
    SetLength( Result, cnt);
    //don't raise
  end;
end;

procedure TBB.SaveRWRTaskStatus(const ALic: TSBLicense;
  ARWRListNBR: integer; ANewState: String);
var
  tmpConnection : IBBConnection;
  i : integer;
begin
  tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWait;
  try
    for i := 0 to ALic.RWRTasks.Count - 1 do
    begin
      if (TRWRTask(Alic.RWRTasks[i]).RWRListNbr = ARWRListNBR) then
      begin
        TRWRTask(Alic.RWRTasks[i]).TaskStatus := ANewState;
        try
          tmpConnection.GetWriter.WriteRWRTasks(ALic);
          tmpConnection.GetWriter.PostDatabaseEvent(SRV_RWR_CHANGED);
        except
          on E:Exception do
          begin
            tmpConnection.IncErrorCount;
            if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
              GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call WriteRWRTasks',
                'Error during call WriteRWRTasks. ' + E.Message + ' Section ' + IntToStr(ALic.SerialNumber)
                + #13#10 + GetStack);
            raise;
          end;
        end;  // try
        break;
      end;  // if
    end;  // for
  finally
    // free connection interface
    Global.GetConnectionPool.ReleaseSlot(tmpConnection);
    tmpConnection := nil;
  end;
end;


procedure TBB.SendRWRUpdate(const ARB: IClientThreadDataExt;  ARecordNbr: integer);
var
  ms : IEvDualStream;
  tmpLic : TSBLicense;
  tmpReportNumber : integer;
  tmpConnection : IBBConnection;
  tmpLov : IisListOfValues;
  i : integer;
begin
  tmpLic := ARB.GetLic;

  //send one file at a time
  ms := TisStream.Create;
  // getting connection
  tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
  try
    try
      tmpConnection.GetReader.ReadRWRReport(ARecordNbr, ms.RealSTream);
    except
      on E:Exception do
      begin
        tmpConnection.IncErrorCount;
        if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call ReadRWRReport',
            'Error during call ReadRWRReport ' + E.Message + ' Section ' + IntToStr(ARB.GetLic.SerialNumber) +
             #13#10 + GetStack);
        raise;
      end;
    end;
  finally
    // free connection interface
    Global.GetConnectionPool.ReleaseSlot(tmpConnection);
    tmpConnection := nil;
  end;
  if ms.size = 0 then
    Exit;
  ms.Position := 0;

  if not ARB.GetAcceptRWUpdate then
    SaveRWRTaskStatus(tmpLic, ARecordNbr,  '2')
  else
  begin
    // looking for report number by record number
    tmpReportNumber := -999;
    for i := 0 to tmpLic.RWRTasks.Count - 1 do
      if (TRWRTask(tmpLic.RWRTasks[i]).RWRListNbr = ARecordNbr) then
      begin
        tmpReportNumber := TRWRTask(tmpLic.RWRTasks[i]).ReportNumber;
        break;
      end;
    if tmpReportNumber = -999 then
      raise Exception.Create('Cannot find report number by record number in RWRList. Record number is ' + IntToStr(ARecordNbr));
    tmpLov := TisListOfValues.Create;
    tmpLov.AddValue(UpdateSystemReport_ReportData, ms);
    try
      ARB.GetRBForBB.UpdateSystemReport(IntToStr(tmpLic.SerialNumber), IntToStr(tmpReportNumber), tmpLOV);
      SaveRWRTaskStatus(tmpLic, ARecordNbr, '1' );
    except
      on E: Exception do
      begin
        try
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error in SendRWRUpdate',
            'Error in SendRWRUpdate. Section ' + IntToStr(tmpLic.SerialNumber) + '. Report# ' +
            IntToStr(tmpReportNumber) + '. Exception: ' + E.Message + #13#10 + GetStack);
        except
        end;
        SaveRWRTaskStatus(tmpLic, ARecordNbr, '2' );
      end;
    end;    
  end;
end;


function TBB.TimeToRunReport(const ALic: TSBLicense): Boolean;
var
  tmpReportStatus: Integer;
  tmpReportRan : TDatetime;
  tmpReportUpdated : TDatetime;

  function CheckTimeBetweenErrors : boolean;
  begin
    if ALic.LastRunReportError = 0 then
      result := true
    else
      if Abs(MinutesBetween(now, ALic.LastRunReportError)) > 60*24 then
      begin
        result := true;
        ALic.LastRunReportError := 0;
      end
      else
        result := false;
  end;
begin
  if ALic.ReportStatus <> null then
    tmpReportStatus := ALic.ReportStatus
  else
    tmpReportStatus := 0;
  if ALic.ReportRan <> null then
    tmpReportRan := ALic.ReportRan
  else
    tmpReportRan := Date;
  if ALic.ReportUpdated <> null then
    tmpReportUpdated := ALic.ReportUpdated
  else
    tmpReportUpdated := 0;

  Result := (tmpReportStatus = 2) or  //  need report
            ((((tmpReportStatus = 1) or (tmpReportStatus = 3) or (tmpReportStatus = 4)) and CheckTimeBetweenErrors) and (tmpReportRan < Date)) or
            ((tmpReportStatus = 0) and (MonthOf(tmpReportUpdated) <> MonthOf(Date)));
  if Result and (Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug) then  // Debug
     GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Time to run report', 'Time to run report. Section ' + IntToStr(ALic.SerialNumber));
end;

function TBB.TimeToUpdateLicense(const ALic: TSBLicense): Boolean;
var
  tmpLicenseUpdated : TDatetime;
  tmpReportUpdated : TDatetime;
begin
  if (ALic.LicenseUpdateStatus = null) or (ALic.LicenseUpdateStatus = '0') or (ALic.LicenseUpdateStatus = '3') then
  begin
    // update by time
    if ALic.LicenseUpdated <> null then
      tmpLicenseUpdated := ALic.LicenseUpdated
    else
      tmpLicenseUpdated := 0;
    if ALic.ReportUpdated <> null then
      tmpReportUpdated := ALic.ReportUpdated
    else
      tmpReportUpdated := Date;
    Result := (ALic.LicenseFrequency <> 0) and
              ((tmpLicenseUpdated + ALic.LicenseFrequency) <= Date) and
              ((tmpReportUpdated + ALic.ReportGracePeriod) >= Date);

    // do not update between 12am and 5am
    if HourOf(Now) < 5 then
      Result := false;

    if Result then
    begin
      SaveLicUpdStatus(ALic.SerialNumber, '1');
      ALic.LicenseUpdateStatus := '1';
    end;
  end
  else
  begin
    // update by user command
    if Alic.LicenseUpdateStatus = '1' then
      Result := true
    else
    begin
      // command is '2'
      Result := true;
      SaveLicUpdStatus(ALic.SerialNumber, '1');
      ALic.LicenseUpdateStatus := '1';
    end;
  end;

  if Result and (Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug) then  // Debug
     GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Time to update license', 'Time to update license. Section ' + IntToStr(ALic.SerialNumber));
end;

procedure TBB.SpoiledStatusRefreshManager(const Params: TTaskParamList);
var
  cnt : cardinal;

  procedure Dummy;
  var
    tmpList : TList;
    i : integer;
    tmpSpoiledStatus : PSpoiledStatus;
    tmpConnection : IBBConnection;
    tmpSerial : integer;
  begin
    try
      // sleep for 0.2 sec
      sleep(200);
      cnt := cnt + 200;
      if cnt >= FSpoiledStatusRefreshTime then
      begin
        // time for find and fix spoiled status: 1 status per circle
        cnt := 0;

        tmpSpoiledStatus := nil;
        tmpList := SpoiledStatuses.LockList;
        try
          for I := 0 to tmpList.Count - 1 do
          begin
            tmpSpoiledStatus := PSpoiledStatus(tmpList[i]);
            tmpList.Delete(i); // temporary remove from list
          end;  // for
        finally
          SpoiledStatuses.UnLockList;
        end;
        if tmpSpoiledStatus <> nil then
        begin
          // trying to fix status
          try
            tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
            try
              try
                tmpSerial := tmpSpoiledStatus.SerialNumber;
                tmpConnection.GetWriter.FixSpoiledStatus(tmpSpoiledStatus.SerialNumber, tmpSpoiledStatus.TimeOfDisconnect);
                Dispose(tmpSpoiledStatus);
                tmpSpoiledStatus := nil;
                tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
                if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
                  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Fixed spoiled status', 'Fixed spoiled status. Section ' + IntToStr(tmpSerial));
              except
                on E:Exception do
                begin
                  tmpConnection.IncErrorCount;
                  if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
                    GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call FixSpoiledStatus',
                      'Error during call FixSpoiledStatus. ' + E.Message + #13#10 + GetStack);
                end;
              end;
            finally
              // free connection interface
              Global.GetConnectionPool.ReleaseSlot(tmpConnection);
              tmpConnection := nil;
            end;
          except
            on E:Exception do
              if tmpSpoiledStatus <> nil then
                AddToSpoiledList(tmpSpoiledStatus.SerialNumber, tmpSpoiledStatus.TimeOfDisconnect);
          end;
        end;
      end;
    except
      on E:Exception do
      begin
        try
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in SpoiledStatusRefreshManager',
            'Exception in SpoiledStatusRefreshManager' + E.Message + #13#10 + GetStack);
          Global.GetEMail.Write('Exception in SpoiledStatusRefreshManager' + E.Message);
        except
        end;
      end;
    end;
  end;

begin
  cnt := 0;
  // circle
  while not CurrentThreadTaskTerminated do
    Dummy;
end;

procedure TBB.AddToSpoiledList(ASerialNumber: integer;
  ATime: TDateTime);
var
  tmpSpoiledStatus : PSpoiledStatus;
  tmpList : TList;
  i : integer;
  tmpFound : boolean;
begin
  tmpList := SpoiledStatuses.LockList;
  try
    tmpFound := false;
    for i := 0 to tmpList.Count - 1 do
    begin
      tmpSpoiledStatus := PSpoiledStatus(tmpList[i]);
      if (tmpSpoiledStatus.SerialNumber = ASerialNumber) then
        if (tmpSpoiledStatus.TimeOfDisconnect <= ATime) then
        begin
          tmpSpoiledStatus.TimeOfDisconnect := ATime;
          tmpFound := true;
          break;
        end
        else
        begin
          tmpFound := true; // do nothing because we already have a newest record in list
          break;
        end;
    end;
    // didn't find, create new
    if not tmpFound then
    begin
      New(tmpSpoiledStatus);
      tmpSpoiledStatus.TimeOfDisconnect := ATime;
      tmpSpoiledStatus.SerialNumber := ASerialNumber;
      tmpList.Add(tmpSpoiledStatus);
    end;
  finally
    SpoiledStatuses.UnlockList;
  end;
end;

function NullToStr(AParam : Variant; AIfNull : String = '') : String; // returns AIfNull string if AParam is null
begin
  Result := '';
  if AParam = null then exit
  else  Result := AIfNull;
end;

function TBB.GetRBList: IInterfaceList;
begin
  Result := FRBList;
end;

function TBB.Global: IGlobalObject;
begin
  Result := FGlobal;
end;

function TBB.LicenseFullRefreshTime: cardinal;
begin
  Result := FLicenseFullRefreshTime;
end;

function TBB.SpoiledStatusRefreshTime: cardinal;
begin
  Result := FSpoiledStatusRefreshTime;
end;

function TBB.FindRB(ASerialNumber: String): IClientThreadDataExt;
var
  i : integer;
begin
  FRBList.Lock;
  try
    for i := 0 to FRBList.Count - 1 do
    begin
      if ((FRBList[i] as IClientThreadDataExt).GetSerialNumber = ASerialNumber) then
      begin
        Result := (FRBList[i] as IClientThreadDataExt);
        exit;
      end;
    end;
    Result := nil;
  finally
    FRBList.Unlock;
  end;
end;

function TBB.AddRB(ASessionId: TisGUID; ASerialNumber: String; AIpAddress : String): IClientThreadDataExt;
var
  Section : integer;
  VerSys, VerDB : String;
  tmpConnection : IBBConnection;
  tmpBoolean : boolean;
  tmpLogSeverity : integer;
  tmpRB, oldRB : IClientThreadDataExt;
  SbData : IisListOfValues;

  function CheckIfExist(const ACheckNumber: integer; const AModifier: integer): IClientThreadDataExt;
  var
    i: integer;
    tmpRb: IClientThreadDataExt;
  begin
    Result := nil;
    for i := 0 to FRBList.Count - 1 do
    begin
      tmpRb := FRBList[i] as IClientThreadDataExt;
      if (tmpRb.GetSerialNumber = ASerialNumber) and (tmpRb.GetModifier = AModifier) then
      begin
        Result := tmpRb;
        exit;
      end;
      if (tmpRb.GetSerialNumber = ASerialNumber) and (tmpRb.GetModifier <> AModifier) then
        raise EDuplicateSerialNumber.Create('Check# ' + IntToStr(ACheckNumber) + '. Error: attempt to connect with the duplicate serial number: ' + ASerialNumber
          + ' and different modifier. IP address is ' + AIpAddress + '. SessionId: ' + ASessionId);
    end;
  end;

begin
  tmpLogSeverity := NewBB.Global.GetConnectionPool.GetLogSeverity;

  if tmpLogSeverity >= LogSeverityDebug then  // Debug
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Got request to add a new session',
     'Got request to add a new session. SessionId: ' + ASessionId + '. Section: ' + ASerialNumber);

  tmpRB := TClientThreadData.Create(ASessionId, ASerialNumber);
  SbData := tmpRB.GetRBForBB.GetSbInfo(ASerialNumber);

  FRBList.Lock;
  try
    OldRB := CheckIfExist(1, Integer(SbData.Value[SbInfo_Modifier]));
    if Assigned(oldRB) then
    begin
      oldRB.GetLock.Lock;
      try
        oldRB.SetSessionId(ASessionId);
        if tmpLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etWarning, BBEventClass,
            'Assigned new session to existing connection (step 1)',
            Format('Assigned new session to existing connection (step 1). Serial is %d. IP address is %s. SessionId is %s',
            [ASerialNumber, AIPAddress, ASessionId]));
        exit;
      finally
        oldRB.GetLock.Unlock;
      end;
    end;
  finally
    FRBList.Unlock;
  end;

  Section := SbData.Value[SbInfo_Serial];
  VerSys := SbData.Value[SbInfo_SysVer];
  VerDb := SbData.Value[SbInfo_DBVer];
  tmpRB.SetAcceptRWUpdate(SbData.Value[SbInfo_RWUpdateAllowed]);
  tmpRB.SetAcceptLicenseUpdate(SbData.Value[SbInfo_LicenseUpdateAllowed]);
  tmpRB.SetModifier(Integer(SbData.Value[SbInfo_Modifier]));

  tmpRB.GetLic.SerialNumber := Section;
  tmpRB.GetLic.LastAddress := AIPAddress;

  // get connection
  tmpConnection := NewBB.Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
  try
    // checking for serial_number in database
    try
      tmpBoolean := tmpConnection.GetReader.CheckInDatabase(Section);
    except
      on E:Exception do
      begin
        tmpConnection.IncErrorCount;
        if tmpLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call CheckInDatabase.',
            'Error during call CheckInDatabase. ' + E.Message + 'Section ' + IntToStr(Section)
             + #13#10 + GetStack);
        raise;
      end;
    end;

    if not tmpBoolean then
    begin
      // Disconnect; implement disconnect
      if tmpLogSeverity >= LogSeverityAll then  // ALL
        GlobalLogFile.AddEvent(etWarning, BBEventClass, 'Refused connection', Format('Refused connection for %d because this serial number doesn''t exist. IP address is %s',[Section, AIPAddress]));
      raise Exception.Create(Format('Refused connection for %d because this serial number doesn''t exist. IP address is %s',[Section, AIPAddress]));
    end;

    // logging
    if tmpLogSeverity >= LogSeverityAll then  // ALL
      GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Got connection', Format('Got connection from %d. IP adress is: %s. SessionId is:%s',[Section, AIPAddress, ASessionId]));
    try
      tmpConnection.GetReader.RefreshAll(tmpRB.GetLic); // filling the structure
    except
      on E:Exception do
      begin
        tmpConnection.IncErrorCount;
        if tmpLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call RefreshAll',
            'Error during call RefreshAll. ' + E.Message + 'Section ' + IntToStr(Section) + #13#10 + GetStack);
        raise;
      end;
    end;

    // store information
    tmpRB.GetLic.SerialNumber := Section;
    tmpRB.GetLic.LastAddress := AIPAddress; // this row is repeated because this operator cleared information: tmpConnection.GetReader.RefreshAll(tmpLic);
    tmpRB.GetLic.Connected := '1';
    tmpRB.GetLic.DateTimeConnected := now;
    tmpRB.GetLic.ServerVersion := VerSys;
    tmpRB.GetLic.DatabaseVersion := VerDB;
    try
      tmpConnection.GetWriter.WriteStatusInfo(tmpRB.GetLic);
      tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
    except
      on E:Exception do
      begin
        tmpConnection.IncErrorCount;
        if tmpLogSeverity >= LogSeverityAll then   // ALL
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call WriteStatusInfo (statement 1)',
            'Error during call WriteStatusInfo (statement 1). ' + E.Message + ' Section ' +
             IntToStr(Section) + #13#10 + GetStack);
        raise;
      end;
    end;
  finally
    // free connection interface
    NewBB.Global.GetConnectionPool.ReleaseSlot(tmpConnection);
    tmpConnection := nil;
  end;

  FRBList.Lock;
  try
    OldRB := CheckIfExist(2, tmpRB.GetModifier);
    if Assigned(oldRB) then
    begin
      oldRB.GetLock.Lock;
      try
        oldRB.SetSessionId(ASessionId);
        if tmpLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etWarning, BBEventClass, 'Assigned new session to existing connection (step 2)', Format('Assigned new session to existing connection (step 2). Serial is %d. IP address is %s. SessionId is %s',[Section, AIPAddress, ASessionId]));
        exit;
      finally
        oldRB.GetLock.Unlock;
      end;
    end;

    FRBList.Add(tmpRb);
  finally
    FRBList.Unlock;
  end;
  Result := tmpRb;
end;

procedure TBB.SaveStatusToDatabase(AClientThreadData : IClientThreadDataExt);
var
  SBLicense : TSBLicense;
  Connection : IBBConnection;
begin
  // save status to database
  SBLicense := AClientThreadData.GetLic;
  try
    if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
      GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Client disconnected', Format('Disconnected from %d',[SBLicense.SerialNumber]));
  except
    ;
  end;
  SBLicense.Connected := '0';
  Connection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
  try
    try
      if Connection.GetReader.CheckInDatabase(SBLicense.SerialNumber) then
      begin
        Connection.GetWriter.WriteStatusInfo(SBLicense);
        Connection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
      end;
    except
      on E:Exception do
      begin
        Connection.IncErrorCount;

        // add this serial to spoiled list;
        AddToSpoiledList(SBLicense.SerialNumber, now);

        GlobalLogFile.AddEvent(etError, BBEventClass, 'TCPServerDisconnect. Exception during saving state',
         'TCPServerDisconnect. Exception during saving state: ' + E.Message + #13#10 + GetStack);
      end;
    end;
  finally
    Global.GetConnectionPool.ReleaseSlot(Connection);
  end;
end;

procedure TBB.DeleteRB(ASessionId: TisGUID);
var
  i : integer;
  tmpI : IClientThreadDataExt;
  bFound: boolean;
  tmpLock: IisLock;
begin
  FRBList.Lock;
  try
    bFound := false;
    for i := 0 to FRBList.Count - 1 do
    begin
      if (FRBList[i] as IClientThreadDataExt).GetSessionId = ASessionId then
      begin
        bFound := true;
        tmpI := FRBList[i] as IClientThreadDataExt;
        FRBList.Delete(i);

        break;
      end;
    end;  // for
  finally
    FRBList.Unlock;
  end;

  if bFound then
  begin
    if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
      GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Deleting session',
       'Deleted session. SessionId: ' + ASessionId + ' Section: ' + tmpI.GetSerialNumber);

    tmpLock := tmpI.GetLock;
    if Assigned(tmpLock) then
    begin
      tmpLock.Lock;
      try
        SaveStatusToDatabase(tmpI);
      finally
        tmpLock.Unlock;
      end;
    end
    else
    begin
      SaveStatusToDatabase(tmpI);
      if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
        GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Lock object not found',
         'Lock object not found. SessionId: ' + ASessionId + ' Section: ' + tmpI.GetSerialNumber);
    end;
  end;

  if (not bFound) and (Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug) then
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Did not find session to be deleted', 'Did not find session to be deleted. SessionId: ' + ASessionId);
end;

function TBB.TCPServer: IbbTCPServer;
begin
  Result := FTCPServer;
end;

procedure TBB.RefreshLoop(const Params: TTaskParamList);

  // work with tmpI.GetLic (new value) and oldTmpLic (old value)
  // doesn't allow change status in cash by status from database, new tasks will be allowed of course
  procedure CheckRWRStatusChanges(const AClientThreadData : IClientThreadDataExt; const AOldLic : TSBLicense);
  var
    i, j : integer;
    tmpNew, tmpOld : TRWRTask;
  begin
    // all new tasks
    for i := 0 to AClientThreadData.GetLic.RWRTasks.Count - 1 do
    begin
      tmpNew := TRWRTask(AClientThreadData.GetLic.RWRTasks[i]);
      for j := 0 to AOldLic.RWRTasks.Count - 1 do
      begin
        tmpOld := TRWRTask(AOldLic.RWRTasks[j]);
        if (tmpNew.RWRListNbr = tmpOld.RWRListNbr) then  // found
        begin
          if (tmpNew.TaskStatus <> tmpOld.TaskStatus) then  // restore task status from cash
            tmpNew.TaskStatus := tmpOld.TaskStatus;
          break; // leave j loop
        end;  // if
      end;  // for j
      // not found new task in old list? not a problem it's just a new task
    end;  // for i
  end;

  procedure Dummy;
  var
    i, j : integer;
    tmpI : IClientThreadDataExt;
    tmpConnection : IBBConnection;
    tmpOldReportStatus : Variant;
    tmpOldLicUpdStatus : Variant;
    oldTmpLic : TSBLicense;
  begin
    try
      // looking for object
      FRBList.Lock;
      try
        j := -1;
        for i := 0 to FRBList.Count - 1 do
        begin
          tmpI := FRBList[i] as IClientThreadDataExt;
          if tmpI.GetLock.TryLock then
          begin
            // checking object status
            try
              if (tmpI.GetRefreshLicInfo or tmpI.GetRefreshAll or tmpI.GetRefreshStatusInfo or tmpI.GetRefreshRWRList
                or tmpI.GetRefreshLicUpdStatusInfo) and (tmpI.GetLic.Connected = '1') then
              begin
                j := i;
                // movig to the end of list
                try
                  FRBList.Delete(j);
                finally
                  FRBList.Add(tmpI);
                end;
                break;
              end
              else
                tmpI.GetLock.Unlock;
            except
              on E:Exception do
              begin
                tmpI.GetLock.Unlock;
                raise;
              end;
            end; // try
          end;  // if tmpI.GetLock.TryLock
        end;  // for
      finally
        FRBList.Unlock;
      end;

      // working with object
      if j <> - 1 then
        try
          // check flags and refresh data
          tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
          if Assigned(tmpConnection) then
          begin
            try
              try
                if tmpI.GetRefreshAll then
                begin
                  tmpOldReportStatus := tmpI.GetLic.ReportStatus;
                  tmpOldLicUpdStatus := tmpI.GetLic.LicenseUpdateStatus;
                  oldTmpLic :=  TSBLicense.CopyObject(tmpI.GetLic);
                  try
                    tmpConnection.GetReader.RefreshLicInfo(tmpI.getLic);

                    tmpI.GetLic.ReportStatus := tmpConnection.GetReader.ReadReportStatus(tmpI.getLic.SerialNumber);
                    // change status back if it's 'running
                    if tmpOldReportStatus = '1' then
                      tmpI.GetLic.ReportStatus := '1';

                    tmpI.getLic.LicenseUpdateStatus := tmpConnection.GetReader.ReadLicenseUpdateStatus(tmpI.GetLic.SerialNumber);
                    // change status back if it's 'running
                    if tmpOldLicUpdStatus = '1' then
                      tmpI.GetLic.LicenseUpdateStatus := '1';

                    tmpConnection.GetReader.RefreshRWRList(tmpI.GetLic);
                    tmpI.SetRefreshAll(false);
                    tmpI.SetRefreshLicInfo(false);
                    tmpI.SetRefreshStatusInfo(false);
                    tmpI.SetRefreshRWRList(false);
                    tmpI.SetRefreshLicUpsStatusInfo(false);
                    CheckRWRStatusChanges(tmpI, oldTmpLic);

                    // save status to database if it's a full refresh
                    tmpConnection.GetWriter.WriteStatusInfo(tmpI.GetLic);
                    tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
                  finally
                    OldTmpLic.Free;
                  end;
                end
                else
                begin
                  if tmpI.GetRefreshLicInfo then
                  begin
                    tmpConnection.GetReader.RefreshLicInfo(tmpI.GetLic);
                    tmpI.SetRefreshLicInfo(false);
                  end;
                  if tmpI.GetRefreshStatusInfo then
                  begin
                    tmpOldReportStatus := tmpI.GetLic.ReportStatus;
                    tmpI.GetLic.ReportStatus := tmpConnection.GetReader.ReadReportStatus(tmpI.GetLic.SerialNumber);
                    tmpI.SetRefreshStatusInfo(false);
                  end;
                  if tmpI.GetRefreshLicUpdStatusInfo then
                  begin
                    tmpOldLicUpdStatus := tmpI.GetLic.LicenseUpdateStatus;
                    tmpI.GetLic.LicenseUpdateStatus := tmpConnection.GetReader.ReadLicenseUpdateStatus(tmpI.GetLic.SerialNumber);
                    // change status back if it's 'running
                    if tmpOldLicUpdStatus = '1' then
                      tmpI.GetLic.LicenseUpdateStatus := '1';
                    tmpI.SetRefreshLicUpsStatusInfo(false);
                  end;
                  if tmpI.GetRefreshRWRList then
                  begin
                    oldTmpLic :=  TSBLicense.CopyObject(tmpI.GetLic);
                    try
                      tmpConnection.GetReader.RefreshRWRList(tmpI.GetLic);
                      tmpI.SetRefreshRWRList(false);
                      CheckRWRStatusChanges(tmpI, oldTmpLic);
                    finally
                      OldTmpLic.Free;
                    end;
                  end;
                end;
                // it's debug output
                if (tmpI.GetLic.Connected = '0') and (Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug) then
                  GlobalLogFile.AddEvent(etWarning, BBEventClass, 'Logic error', 'Logic error. After refresh connected=''0''. Section ' + IntToStr(tmpI.GetLic.SerialNumber));
              except
                on E:Exception do
                begin
                  tmpConnection.IncErrorCount;
                  if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
                    GlobalLogFile.AddEvent(etError, BBEventClass,
                      'Error during Refresh information for refresh flag.',
                      'Error during Refresh information for refresh flag.' + E.Message +
                      ' Section ' + IntToStr(tmpI.GetLic.SerialNumber) + #13#10 + GetStack);
                end;
              end;
            finally
              // free connection interface
              Global.GetConnectionPool.ReleaseSlot(tmpConnection);
              tmpConnection := nil;
            end;
          end; // if tmpConnection <> nil
        finally
          tmpI.GetLock.Unlock;
        end;
    except
      on E:Exception do
      begin
        if E is EAbort then
          raise;
        try
          tmpI := nil; // because we never out of while
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in RefreshLoop',
            'Exception in RefreshLoop' + E.Message + #13#10 + GetStack);
          Global.GetEMail.Write('Exception in RefreshLoop' + E.Message);
        except
        end;
      end;
    end;
  end;

begin
  // circle
  while not CurrentThreadTaskTerminated do
  begin
    Dummy;
    snooze(Random(200) + 50);
  end;
end;

procedure TBB.SetNewLicenseLoop(const Params: TTaskParamList);
const
  MaxErrors = 100;
var
  iErrorsCount : integer;
  bFlag : boolean;

  procedure Dummy;
  var
    i, j : integer;
    tmpI : IClientThreadDataExt;
    tmpConnection : IBBConnection;
    LicCode, Serial : String;
  begin
    try
      // looking for object
      FRBList.Lock;
      try
        j := -1;
        for i := 0 to FRBList.Count - 1 do
        begin
          tmpI := FRBList[i] as IClientThreadDataExt;
          if tmpI.GetLock.TryLock then
          begin
            // checking object status
            try
              if TimeToUpdateLicense(tmpI.GetLic) and tmpI.GetAcceptLicenseUpdate and (tmpI.GetLic.Connected = '1') then
              begin
                j := i;
                // movig to the end of list
                try
                  FRBList.Delete(j);
                finally
                  FRBList.Add(tmpI);
                end;
                break;
              end
              else
                tmpI.GetLock.Unlock;
            except
              on E:Exception do
              begin
                tmpI.GetLock.Unlock;
                raise;
              end;
            end; // try
          end;  // if tmpI.GetLock.TryLock
        end;  // for
      finally
        FRBList.Unlock;
      end;

      // working with object
      if j <> - 1 then
        try
          // updating license
          LicCode := GenerateCode(tmpI.GetLic.SerialNumber, tmpI.GetModifier, tmpI.GetLic);
          tmpI.GetRBForBB.SetNewLicense(IntToStr(tmpI.GetLic.SerialNumber), LicCode + GenerateAPICode(LicCode, tmpI.GetLic.APIKeys));

          // getting connection and saving status
          tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
          if Assigned(tmpConnection) then
            try
              tmpI.GetLic.LicenseUpdated := Date;
              tmpI.GetLic.LicenseUpdateStatus := '0'; // update's been done
              try
                tmpConnection.GetWriter.WriteStatusInfo(tmpI.GetLic);
                tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
              except
                on E:Exception do
                begin
                  tmpConnection.IncErrorCount;
                  if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
                    GlobalLogFile.AddEvent(etError, BBEventClass,
                      'Error during call WriteStatusInfo. (statement 4)',
                      'Error during call WriteStatusInfo. (statement 4)' + E.Message +
                      ' Section ' + IntToStr(tmpI.GetLic.SerialNumber) + #13#10 + GetStack);
                end;
              end;
            finally
              // free connection interface
              Global.GetConnectionPool.ReleaseSlot(tmpConnection);
              tmpConnection := nil;
            end;
        finally
          tmpI.GetLock.Unlock;
        end;

    except
      on E:Exception do
      begin
        if E is EAbort then
          raise;
        Inc(iErrorsCount);
        if iErrorsCount <= MaxErrors then
          try
            if Assigned(tmpI) and Assigned(tmpI.GetLic) then
              Serial := IntToStr(tmpI.GetLic.SerialNumber)
            else
              Serial := '';
            GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in SetNewLicenseLoop. SN#: ' +
              Serial, 'Exception in SetNewLicenseLoop: ' + E.Message + #13#10 + GetStack);
            Global.GetEMail.Write('Exception in SetNewLicenseLoop. SN#: ' + Serial + ' Error: ' + E.Message);
          except
          end
        else
          if not bFlag then
            try
              bFlag := true;
              GlobalLogFile.AddEvent(etError, BBEventClass, 'Too many errors in one of SetNewLicenseLoop threads', 'Logging of exception in one of SetNewLicenseLoop threads is stopped. Reason: too many errors.');
              Global.GetEMail.Write('Logging of exception in one of SetNewLicenseLoop threads is stopped. Reason: too many errors.');
            except
            end;
      end;
    end;
  end;
begin
  iErrorsCount := 0;
  bFlag := false;
  while not CurrentThreadTaskTerminated do
  begin
    Dummy;
    snooze(Random(200) + 50);
  end;
end;

procedure TBB.RunSystemReportLoop(const Params: TTaskParamList);

  procedure Dummy;
  var
    i, j : integer;
    tmpI : IClientThreadDataExt;
    tmpConnection : IBBConnection;
    tmpNewStatus : String;
    tmp : String;
  begin
    try
      // looking for object
      FRBList.Lock;
      try
        j := -1;
        for i := 0 to FRBList.Count - 1 do
        begin
          tmpI := FRBList[i] as IClientThreadDataExt;
          if tmpI.GetLock.TryLock then
          begin
            // checking object status
            try
              if TimeToRunReport(tmpI.GetLic) and (tmpI.GetLic.Connected = '1') then
              begin
                j := i;
                // movig to the end of list
                try
                  FRBList.Delete(j);
                finally
                  FRBList.Add(tmpI);
                end;
                break;
              end
              else
                tmpI.GetLock.Unlock;
            except
              on E:Exception do
              begin
                tmpI.GetLock.Unlock;
                raise;
              end;
            end; // try
          end;  // if tmpI.GetLock.TryLock
        end;  // for
      finally
        FRBList.Unlock;
      end;

      // working with object
      if j <> - 1 then
        try
          try
            tmpI.SetJustCreated(false);
            tmp := RunReport(tmpI);
          except
            on E:Exception do
            begin
              tmp := RunReport_RunError;
            end;
          end;

          if tmp = RunReport_RunOK then
          begin
            tmpNewStatus := '1';
            tmpI.GetLic.LastRunReportError := 0;
          end
          else
          begin
            tmpI.GetLic.LastRunReportError := now;
            if tmp = RunReport_RunError then
              tmpNewStatus := '3'  // error
            else
              tmpNewStatus := '4'; // refused because alreadu running
          end;

          // get connection and save status
          tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWait;
          try
            tmpI.GetLic.ReportStatus := tmpNewStatus;
            if tmpNewStatus = '1' then
              tmpI.GetLic.ReportRan := Date;
            try
              tmpConnection.GetWriter.WriteStatusInfo(tmpI.GetLic);
              tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
            except
              on E:Exception do
              begin
                tmpConnection.IncErrorCount;
                if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
                  GlobalLogFile.AddEvent(etError, BBEventClass,
                    'Error during call WriteStatusInfo. (statement 3)',
                    'Error during call WriteStatusInfo. (statement 3)' + E.Message +
                    ' Section ' + IntToStr(tmpI.GetLic.SerialNumber) + #13#10 + GetStack);
              end;
            end;
          finally
            // free connection interface
            Global.GetConnectionPool.ReleaseSlot(tmpConnection);
            tmpConnection := nil;
          end;
        finally
          tmpI.GetLock.Unlock;
        end;

    except
      on E:Exception do
      begin
        if E is EAbort then
          raise;
        try
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in RunSystemReportLoop',
            'Exception in RunSystemReportLoop' + E.Message + #13#10 + GetStack);
          Global.GetEMail.Write('Exception in RunSystemReportLoop' + E.Message);
        except
        end;
      end;
    end;
  end;
begin
  // circle
  while not CurrentThreadTaskTerminated do
  begin
    Dummy;
    snooze(Random(200) + 100);
  end;
end;

procedure TBB.GetReportResultsLoop(const Params: TTaskParamList);
const
  MaxErrors = 100;
var
  iErrorsCount : integer;
  bFlag : boolean;

  procedure Dummy;
  var
    i, j : integer;
    tmpI : IClientThreadDataExt;
    tmpConnection : IBBConnection;
    tmpStream : IEvDualStream;
    tmpLOV : IisListOfValues;
    tmpNewStatus : String;
    bNoResults : boolean;

    procedure SaveReportStatus(ANewStatus : String);
    begin
      // get connection and save new status
      tmpConnection := Global.GetConnectionPool.FindAndTakeSlotWait;
      try
        tmpI.GetLic.ReportStatus := ANewStatus;
        tmpI.GetLic.ReportUpdated := Date;
        try
          tmpConnection.GetWriter.WriteStatusInfo(tmpI.GetLic);
          tmpConnection.GetWriter.PostDatabaseEvent(SRV_STATUS_CHANGED);
          if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
            GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Saved new report status to database', 'Saved new report status to database. SN#' + IntToStr(tmpI.GetLic.SerialNumber) + ' Status: ' + ANewStatus);
        except
          on E:Exception do
          begin
            tmpConnection.IncErrorCount;
            if Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
              GlobalLogFile.AddEvent(etError, BBEventClass,
                'Error during call WriteStatusInfo (statement 2).',
                'Error during call WriteStatusInfo (statement 2). ' + E.Message +
                ' Section ' + IntToStr(tmpI.GetLic.SerialNumber) + #13#10 + GetStack);
          end;
        end;
      finally
        // free connection interface
        Global.GetConnectionPool.ReleaseSlot(tmpConnection);
        tmpConnection := nil;
      end;
    end;

    begin
    try
      // looking for object
      FRBList.Lock;
      try
        j := -1;
        for i := 0 to FRBList.Count - 1 do
        begin
          tmpI := FRBList[i] as IClientThreadDataExt;
          if tmpI.GetLock.TryLock then
          begin
            // checking object status
            try
              if (tmpI.GetLic.Connected = '1') and (tmpI.GetReportIsReady or (tmpI.GetJustCreated and
                ((tmpI.GetLic.ReportStatus = '1') or (tmpI.GetLic.ReportStatus = '4')))) then
              begin
                j := i;
                // moving to the end of list
                try
                  FRBList.Delete(j);
                finally
                  FRBList.Add(tmpI);
                end;
                break;
              end
              else
                tmpI.GetLock.Unlock;
            except
              on E:Exception do
              begin
                tmpI.GetLock.Unlock;
                raise;
              end;
            end; // try
          end;  // if tmpI.GetLock.TryLock
        end;  // for
      finally
        FRBList.Unlock;
      end;

      // working with object
      if j <> - 1 then
        try
          bNoResults := false;
          if tmpI.GetReportIsReady then
          begin
            tmpI.SetJustCreated(false);
            try
              tmpLOV := tmpI.GetRBForBB.GetReportResults(IntToStr(tmpI.GetLic.SerialNumber), RunReport_Billing);
              tmpI.SetReportIsReady(false);
              if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Received ReportResults (not first run)', 'Received ReportResults (not first run). SN#' + IntToStr(tmpI.GetLic.SerialNumber));
            except
              on E : ENoReadyReports do
              begin
                bNoResults := true;
                tmpI.SetReportIsReady(false);
                SaveReportStatus('3');  // error
                if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'BB was not able to get report results: NoReadyReports', 'BB was not able to get report results: NoReadyReports. SN#' + IntToStr(tmpI.GetLic.SerialNumber));
              end;
              on E:Exception do
              begin
                bNoResults := true;
                tmpI.SetReportIsReady(false);
                GlobalLogFile.AddEvent(etError, BBEventClass, 'BB was not able to get report results', 'BB was not able to get report results. SN#' + IntToStr(tmpI.GetLic.SerialNumber) + '. Exception: ' + E.Message);
                SaveReportStatus('3');  // error
              end;
            end;
          end
          else  // tmpI.GetJustCreated = true, trying to get report result because RB is just connected in order to pick up old results
            try
              tmpLOV := tmpI.GetRBForBB.GetReportResults(IntToStr(tmpI.GetLic.SerialNumber), RunReport_Billing);
              tmpI.SetJustCreated(false);
              tmpI.SetReportIsReady(false);
              if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Received ReportResults (first run)', 'Received ReportResults (first run). SN#' + IntToStr(tmpI.GetLic.SerialNumber));
            except
              on E : ENoReadyReports do
              begin
                bNoResults := true;
                tmpI.SetJustCreated(false);
                tmpI.SetReportIsReady(false);
                if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'BB was not able to get report results: NoReadyReports (first run)', 'BB was not able to get report results: NoReadyReports (first run). SN#' + IntToStr(tmpI.GetLic.SerialNumber));
              end;
              on E:Exception do
              begin
                bNoResults := true;
                tmpI.SetJustCreated(false);
                tmpI.SetReportIsReady(false);
                GlobalLogFile.AddEvent(etError, BBEventClass, 'BB was not able to get report results (attempt at startup)', 'BB was not able to get report results (attempt at startup). SN#' + IntToStr(tmpI.GetLic.SerialNumber) + '. Exception: ' + E.Message);
                SaveReportStatus('3');  // error
              end;
            end;

          if not bNoResults then
          begin
            try
              tmpStream := IInterface(tmpLov.Value[Method_result]) as IevDualStream;
            except
              on EE:Exception do  // added to handle errors on RB side related with "Item "Result" does not exist"
              begin
                tmpI.SetJustCreated(false);
                tmpI.SetReportIsReady(false);
                SaveReportStatus('3');  // error
                raise Exception.Create('Serial number: ' + IntToStr(tmpI.GetLic.SerialNumber) + ' ' + EE.Message);
              end;
            end;

            if Assigned(tmpStream) then
            begin
              tmpStream.Position := 0;
              tmpNewStatus := '0';

              // save to file
              tmpStream.SaveToFile(WorkDir + IntToStr(tmpI.GetLic.SerialNumber) + '.rwa');
              if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Saved report results to file', 'Saved report results to file. SN#' + IntToStr(tmpI.GetLic.SerialNumber) + ' File: ' +WorkDir + IntToStr(tmpI.GetLic.SerialNumber) + '.rwa');

              // now save to database in order to report will be avaliable for configuration utility
              tmpConnection := NewBB.Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true);
              try
                tmpStream.Position := 0;
                tmpConnection.GetWriter.WriteStatusReport(tmpI.GetLic.SerialNumber, tmpStream.RealStream);
                if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                  GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Saved report results to database', 'Saved report results to database. SN#' + IntToStr(tmpI.GetLic.SerialNumber));
              finally
                NewBB.Global.GetConnectionPool.ReleaseSlot(tmpConnection);
                tmpConnection := nil;
              end;
            end  // Assigned(tmpStream)
            else
            begin
              tmpNewStatus := '3';
              if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then
                GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Stream with Report results is nil', 'Stream with Report results is nil. SN#' + IntToStr(tmpI.GetLic.SerialNumber));
            end;

            SaveReportStatus(tmpNewStatus);

          end;  // if not bNoResults
        finally
          tmpI.GetLock.Unlock;
        end;

    except
      on E:Exception do
      begin
        if E is EAbort then
          raise;
        Inc(iErrorsCount);
        if iErrorsCount <= MaxErrors then
          try
            GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in GetReportResultsLoop',
              'Exception in GetReportResultsLoop. ' + E.Message + #13#10 + GetStack);
            Global.GetEMail.Write('Exception in GetReportResultsLoop. ' + E.Message);
          except
          end
        else
          if not bFlag then
            try
              bFlag := true;
              GlobalLogFile.AddEvent(etError, BBEventClass, 'Too many errors in one of GetReportResultsLoop threads', 'Logging of exception in one of GetReportResultsLoop threads is stopped. Reason: too many errors.');
              Global.GetEMail.Write('Logging of exception in one of GetReportResultsLoop threads is stopped. Reason: too many errors.');
            except
            end;
      end;
    end;
  end;
begin
  iErrorsCount := 0;
  bFlag := false;
  while not CurrentThreadTaskTerminated do
  begin
    Dummy;
    snooze(Random(200) + 50);
  end;
end;

procedure TBB.UpdateSystemReportLoop(const Params: TTaskParamList);

  procedure Dummy;
  var
    tmpI : IClientThreadDataExt;
    i, j : integer;
    RecordNbrs: TIntegerArray;
  begin
    try
      // looking for object
      FRBList.Lock;
      try
        j := -1;
        for i := 0 to FRBList.Count - 1 do
        begin
          tmpI := FRBList[i] as IClientThreadDataExt;
          if tmpI.GetLock.TryLock then
          begin
            // checking object status
            try
              RecordNbrs := GetRWRUpdate(tmpI.GetLic);
              if (tmpI.GetLic.Connected = '1') and (Length(RecordNbrs)>0) then
              begin
                j := i;
                // movig to the end of list
                try
                  FRBList.Delete(j);
                finally
                  FRBList.Add(tmpI);
                end;
                break;
              end
              else
                tmpI.GetLock.Unlock;
            except
              on E:Exception do
              begin
                tmpI.GetLock.Unlock;
                raise;
              end;
            end; // try
          end;  // if tmpI.GetLock.TryLock
        end;  // for
      finally
        FRBList.Unlock;
      end;

      // working with object
      if j <> - 1 then
        try
          if Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
            GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Sending RWRUpdate', 'Sending RWRUpdate. Section ' + IntToStr(tmpI.GetLic.SerialNumber));
          SendRWRUpdate(tmpI, RecordNbrs[0]);
        finally
          tmpI.GetLock.Unlock;
        end;
    except
      on E:Exception do
      begin
        if E is EAbort then
          raise;
        try
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in UpdateSystemReportLoop',
            'Exception in UpdateSystemReportLoop' + E.Message + #13#10 + GetStack);
          Global.GetEMail.Write('Exception in UpdateSystemReportLoop' + E.Message);
        except
        end;
      end;
    end;
  end;
begin
  // circle
  while not CurrentThreadTaskTerminated do
  begin
    Dummy;
    snooze(Random(200) + 50);
  end;
end;

procedure TBB.DeleteAllRB;
var
  i : integer;
  tmpI : IClientThreadDataExt;
begin
  FRBList.Lock;
  try
    for i := FRBList.Count - 1 downto 0 do
    begin
      tmpI := FRBList[i] as IClientThreadDataExt;
      tmpI.GetLic.Connected := '0';
      SaveStatusToDatabase(tmpI);
      FRBList.Delete(i);
    end;
  finally
    FRBList.Unlock;
  end;
end;

function TBB.TryToFindAnotherSession(ASerialNumber: String): TisGUID;
var
  i: integer;
  tmpRB : IClientThreadDataExt;
begin
  Result := '';
  FRBList.Lock;
  try
    for i := 0 to FRBList.Count - 1 do
    begin
      tmpRb := FRBList[i] as IClientThreadDataExt;
      if tmpRb.GetSerialNumber = ASerialNumber then
      begin
        Result := tmpRb.GetSessionId;
        exit;
      end;
    end;
  finally
    FRBList.Unlock;
  end;
end;

end.
