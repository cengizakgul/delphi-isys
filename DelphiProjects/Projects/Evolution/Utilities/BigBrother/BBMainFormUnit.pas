unit BBMainFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IBEvents, NewBBUnit, ConnectionsPoolUnit, StdCtrls, ExtCtrls,
  ISErrorUtils, Clipbrd, {IDTCPServer,} EventReceiverUnit;

type
  TBBMainForm = class(TForm)
    ThrowExceptionButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ThrowExceptionButtonClick(Sender: TObject);
  private
    { Private declarations }
    EventReceiver : IEventReceiver;
  public
    { Public declarations }
  end;

var
  BBMainForm: TBBMainForm;

implementation

uses GlovalObjectsUnit, SBLicenseReaderUnit, SBLicenseUnit,
  SBLicenseWriterUnit, MyTCPServerUnit;

{$R *.dfm}

procedure TBBMainForm.FormCreate(Sender: TObject);
begin
  try
    RunNewBB;
    EventReceiver := TEventReceiver.Create;
  except
    on E:Exception do
    begin
      Application.MessageBox(PChar('Cannot start BB server. Exception: ' + E.Message), 'Error',  MB_OK);
      Application.Terminate;
    end;
  end;
end;

procedure TBBMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  EventReceiver := nil;
  StopNewBB;
end;

procedure TBBMainForm.ThrowExceptionButtonClick(Sender: TObject);
//var
//  StackTrace: string;
begin
{  try
    raise EisDumpAllThreads.Create('Dummy Exception');
  except
    StackTrace := GetStack;
    ClipBoard.SetTextBuf(PChar(StackTrace));
    ShowMessage('Thread trace has been copied to clipboard');
  end; }
end;

end.
