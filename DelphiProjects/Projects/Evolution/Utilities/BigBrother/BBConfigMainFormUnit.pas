unit BBConfigMainFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DB, Grids, Wwdbigrd, Wwdbgrid,
  ISBasicClasses, ImgList, DBGrids, wwSpeedButton, EvUIUtils, EvUtils,
  wwDBNavigator, wwclearpanel, wwDialog, wwidlg, Wwlocate, Menus, ShellApi;

type
  TBBConfigMainForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    MainGrid: TwwDBGrid;
    ImageList1: TImageList;
    EditButton: TButton;
    DeleteButton: TButton;
    AddButton: TButton;
    UpdateReportButton: TButton;
    DefaultsButton: TButton;
    RefreshButton: TButton;
    wwDBNavigator1: TwwDBNavigator;
    wwDBNavigator1First: TwwNavButton;
    wwDBNavigator1PriorPage: TwwNavButton;
    wwDBNavigator1Prior: TwwNavButton;
    wwDBNavigator1Next: TwwNavButton;
    wwDBNavigator1NextPage: TwwNavButton;
    wwDBNavigator1Last: TwwNavButton;
    wwDBNavigator1SaveBookmark: TwwNavButton;
    wwDBNavigator1RestoreBookmark: TwwNavButton;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    TotalRowsLabel: TLabel;
    InactiveRowsLabel: TLabel;
    Button1: TButton;
    ExportDialog: TOpenDialog;
    wwLocateDialog1: TwwLocateDialog;
    wwDBNavigator1SearchButton: TwwNavButton;
    MainMenu: TMainMenu;
    License1: TMenuItem;
    Edit1: TMenuItem;
    Add1: TMenuItem;
    Delete1: TMenuItem;
    Exit1: TMenuItem;
    Actions1: TMenuItem;
    ExporttoExcel1: TMenuItem;
    Refresf1: TMenuItem;
    Showdefaults1: TMenuItem;
    Uploadsystemreport1: TMenuItem;
    Options1: TMenuItem;
    RefreshmanuallyItem: TMenuItem;
    RefreshbytimerItem: TMenuItem;
    RefreshbyeventItem: TMenuItem;
    AutoRefreshTimer: TTimer;
    ServerEventTimer: TTimer;
    N1: TMenuItem;
    N2: TMenuItem;
    ReconnecttoKillingtonItem: TMenuItem;
    ReconnecttoLincolnItem: TMenuItem;
    Vendors1: TMenuItem;
    Vendors2: TMenuItem;
    APIkeys1: TMenuItem;
    Generatelicensecodes1: TMenuItem;
    N3: TMenuItem;
    procedure MainGridURLOpen(Sender: TObject; var URLLink: String;
      Field: TField; var UseDefault: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure RefreshButtonClick(Sender: TObject);
    procedure MainGridUpdateFooter(Sender: TObject);
    procedure DefaultsButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EditButtonClick(Sender: TObject);
    procedure AddButtonClick(Sender: TObject);
    procedure UpdateReportButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure MainGridTitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure FormCreate(Sender: TObject);
    procedure MainGridCalcTitleAttributes(Sender: TObject;
      AFieldName: String; AFont: TFont; ABrush: TBrush;
      var ATitleAlignment: TAlignment);
    procedure Exit1Click(Sender: TObject);
    procedure RefreshbytimerItemClick(Sender: TObject);
    procedure RefreshmanuallyItemClick(Sender: TObject);
    procedure RefreshbyeventItemClick(Sender: TObject);
    procedure AutoRefreshTimerTimer(Sender: TObject);
    procedure ServerEventTimerTimer(Sender: TObject);
    procedure ReconnecttoKillingtonItemClick(Sender: TObject);
    procedure ReconnecttoLincolnItemClick(Sender: TObject);
    procedure Vendors2Click(Sender: TObject);
    procedure APIkeys1Click(Sender: TObject);
    procedure Generatelicensecodes1Click(Sender: TObject);
  private
    FSortFieldName: String;
    FSortOrder: String;
    procedure SetSortFieldName(const Value: String);
    procedure SetSortOrder(const Value: String);
    procedure RefreshFormCaption;
    { Private declarations }
  public
    { Public declarations }
    procedure RefreshTotals;
    function CheckEditRights : boolean; // true if editing is enabled
    property SortFieldName : String read FSortFieldName write SetSortFieldName;
    property SortOrder : String read FSortOrder write SetSortOrder;
  end;

var
  BBConfigMainForm: TBBConfigMainForm;

implementation

uses BBConfigDataUnit, SBLicenseUnit, SBLicenseReaderUnit,
  SBLicenseWriterUnit, UnitShowDefaultsForm, EditFormUnit,
  UpdateReportFormUnit, TasksListFormUnit, VendorsFormUnit,
  APIKeysEditFormUnit, GeneratorFormUnit;

{$R *.dfm}

procedure TBBConfigMainForm.MainGridURLOpen(Sender: TObject;
  var URLLink: String; Field: TField; var UseDefault: Boolean);
var
  Reader : TSBLicenseReader;
  Writer : TSBLicenseWriter;
  tmpStatus : Variant;
  f : TFileStream;
  tmpSerialNumber : integer;
begin
  Reader := nil;
  Writer := nil;

  if UpperCase(Field.FieldName) = 'OPENREPORT' then
  begin
    UseDefault := false;
    if URLLink <> '' then
    begin
      f := TFileStream.Create(ExtractFilePath(ParamStr(0)) + URLLink + '.rwa', fmCreate);
      try
        Reader := TSBLicenseReader.Create(BBConfigData.IBDatabase, nil);
        try
          tmpSerialNumber := StrToInt(URLLink);
          Reader.ReadStatusReport(tmpSerialNumber, f);
        finally
          Reader.Free;
        end;
      finally
        f.Free;
      end;
//      ShellExecute(0, 'open', PAnsiChar(ExtractFilePath(ParamStr(0)) + IntToStr(tmpSerialNumber) + '.rwa'), nil, nil, SW_SHOWNORMAL);
      RunShellViewer(ExtractFilePath(ParamStr(0)) + IntToStr(tmpSerialNumber) + '.rwa', true);
    end
  end;


  if UpperCase(Field.FieldName) = 'RUNREPORTNOW' then
  begin
    UseDefault := false;
    with BBConfigData do
      if IBLicInfo['SERIAL_NUMBER'] <> null then
      begin
        try
          Reader := TSBLicenseReader.Create(BBConfigData.IBDatabase, nil);
          Writer := TSBLicenseWriter.Create(BBConfigData.IBDatabase, nil);
          tmpStatus := Reader.ReadReportStatus(IBLicInfo.FieldByName('SERIAL_NUMBER').AsInteger);
          if not ((tmpStatus <> null) and (tmpStatus = '2')) then
          begin
            Writer.WriteOnlyReportStatus(IBLicInfo.FieldByName('SERIAL_NUMBER').AsInteger, '2');
            Writer.PostDatabaseEvent(CL_STATUS_CHANGED);
          end;
        finally
          Reader.Free;
          Writer.Free;
        end;
        RefreshButton.Click;
      end;
  end;

  if UpperCase(Field.FieldName) = 'UPDATELICENSENOW' then
  begin
    if BBConfigData.EditIsDisabled then
      Application.MessageBox(PChar('You have no right to update license!'), 'Warning!', MB_OK)
    else
    begin
      UseDefault := false;
      if Application.MessageBox(PChar('Serial#' + BBConfigData.IBLicInfo.FieldByName('SERIAL_NUMBER').AsString + '. Are you sure you want to update the license now?'), 'Warning!', MB_OKCANCEL) = IDOK then
      begin
        with BBConfigData do
        begin
          if IBLicInfo['SERIAL_NUMBER'] <> null then
          begin
            try
              Writer := TSBLicenseWriter.Create(BBConfigData.IBDatabase, nil);
              Writer.WriteOnlyLicenseUpdateStatus(IBLicInfo.FieldByName('SERIAL_NUMBER').AsInteger, '2');
              Writer.PostDatabaseEvent(CL_LIC_UPD_CHANGED);
            finally
              Reader.Free;
              Writer.Free;
            end;
            RefreshButton.Click;
          end;  // if
        end;  // with
      end;  // if
    end; // else
  end;
end;

procedure TBBConfigMainForm.FormActivate(Sender: TObject);
begin
  RefreshFormCaption;
  CheckEditRights;
  MainGrid.SetFocus;
end;

procedure TBBConfigMainForm.RefreshButtonClick(Sender: TObject);
var
  oldCursor : TCursor;
begin
   oldCursor := Screen.Cursor;
   try
     Screen.Cursor := crHourGlass;
     with BBConfigData do
     begin
       FullRefreshDataSets;
     end;
   finally
     Screen.Cursor := oldCursor;
   end;
end;

procedure TBBConfigMainForm.RefreshTotals;
begin
  with BBConfigData do
  begin
    IBTotals.Active := false;
    IBTotals.Active := true;
    try
      while not IBTotals.eof do
      begin
        if (Trim(IBTotals['NAME']) = 'Total rows') then
           TotalRowsLabel.Caption := IntToStr(VarToInt(IBTotals['AMOUNT']));
        if (Trim(IBTotals['NAME']) = 'Inactive rows') then
           InactiveRowsLabel.Caption := IntToStr(VarToInt(IBTotals['AMOUNT']));
        IBTotals.Next;
      end;
    finally
      IBTotals.Active := false;
    end;
  end;
end;

procedure TBBConfigMainForm.MainGridUpdateFooter(Sender: TObject);
begin
  RefreshTotals;
end;

procedure TBBConfigMainForm.DefaultsButtonClick(Sender: TObject);
begin
  ShowDefaultForm.Show;
end;

procedure TBBConfigMainForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  BBConfigData.SaveSettings;
  BBConfigData.CloseDatasets;
end;

procedure TBBConfigMainForm.EditButtonClick(Sender: TObject);
begin
  EditForm.ShowForEditorView;
  if EditForm.ModalResult = mrOk then
    RefreshButton.Click;
end;

procedure TBBConfigMainForm.AddButtonClick(Sender: TObject);
begin
  EditForm.ShowForAdd;
  if EditForm.ModalResult = mrOk then
  begin
    RefreshButton.Click;
    BBConfigData.IBLicInfo.Locate('Serial_number', EditForm.CurrentSerialNumber, []);
  end;
end;

// true if editing is enabled
function TBBConfigMainForm.CheckEditRights : boolean;
begin
  result := not BBConfigData.EditIsDisabled;
  if not BBConfigData.EditIsDisabled then
  begin
    EditButton.Caption := 'Edit...';
    EditButton.Enabled := true;
    Edit1.Caption := 'Edit...';
    Edit1.Enabled := true;
    DeleteButton.Enabled := true;
    Delete1.Enabled := true;
    AddButton.Enabled := true;
    Add1.Enabled := true;
    Generatelicensecodes1.Enabled := true;
  end
  else
  begin
    EditButton.Caption := 'View...';
    EditButton.Enabled := true;
    Edit1.Caption := 'View...';
    Edit1.Enabled := true;
    DeleteButton.Enabled := false;
    Delete1.Enabled := false;
    AddButton.Enabled := false;
    Add1.Enabled := false;
    Generatelicensecodes1.Enabled := false;
  end;
end;

procedure TBBConfigMainForm.UpdateReportButtonClick(Sender: TObject);
begin
  UpdateReportForm.ShowForm;
end;

procedure TBBConfigMainForm.DeleteButtonClick(Sender: TObject);
begin
  with BBConfigData do
  begin
    if IBLicInfo['SERIAL_NUMBER'] <> null then
      if Application.MessageBox(PChar('License# ' + IBLicInfo.FieldByName('SERIAL_NUMBER').AsString + ' will be deleted! Are you sure?'), 'Warning!', MB_OKCANCEL) = IDOK then
      begin
        EditForm.DeleteLic(IBLicInfo.FieldByName('SERIAL_NUMBER').AsInteger);
        RefreshButton.Click;
      end;
  end;
end;

procedure TBBConfigMainForm.Button1Click(Sender: TObject);
var
  tmp : String;
begin
  if ExportDialog.Execute then
  begin
    MainGrid.ExportOptions.FileName := ExportDialog.FileName;
    tmp := ExtractFileExt(MainGrid.ExportOptions.FileName);
    if tmp = '' then MainGrid.ExportOptions.FileName := MainGrid.ExportOptions.FileName + '.slk';
    MainGrid.ExportOptions.Save;
    ShowMessage('Export is finished');
  end;
end;

procedure TBBConfigMainForm.Button2Click(Sender: TObject);
begin
  wwLocateDialog1.Execute;
end;

procedure TBBConfigMainForm.MainGridTitleButtonClick(Sender: TObject;
  AFieldName: String);
const
  SortStart = '/*SORT1*/';
  SortEnd = '/*SORT2*/';
var
  i : integer;
  pos1, pos2 : integer;
  tmp : String;
begin
  with BBConfigData do
  for i := 0 to IBLicInfo.FieldCount - 1 do
  begin
    if (UpperCase(AFieldName) = IBLicInfo.Fields[i].FieldName) and (IBLicInfo.Fields[i].FieldKind <> fkCalculated) then
    begin
      IBLicInfo.DisableControls;
      try
        IBLicInfo.Active := false;
        tmp := IBLicInfo.SQL.Text;
        pos1 := Pos(SortStart, tmp); pos2 := Pos(SortEnd, tmp);
        if (pos1 <> 0) and (pos2 <> 0) then
        begin
            if SortOrder = 'ASC' then
              SortOrder := 'DESC'
            else
              SortOrder := 'ASC';
           SortFieldName := AFieldName;
           Delete(tmp, pos1, pos2 - pos1);
           if UpperCase(AFieldName) = 'SERIAL_NUMBER' then
             Insert(SortStart + ' a.' + AFieldName + ' ' + SortOrder + ' ', tmp, pos1)
           else
             Insert(SortStart + ' ' + AFieldName + ' ' + SortOrder + ' ', tmp, pos1);
        end;
        IBLicInfo.SQL.Clear;
        IBLicInfo.SQL.text := tmp;
        IBLicInfo.Active := true;
      finally
        IBLicInfo.EnableControls;
      end;
    end;
  end;
end;

procedure TBBConfigMainForm.SetSortFieldName(const Value: String);
begin
  FSortFieldName := Value;
end;

procedure TBBConfigMainForm.FormCreate(Sender: TObject);
begin
  SortFieldName := 'SERIAL_NUMBER';
  SortOrder := 'ASC';
  with BBConfigData do
  begin
    case RefreshType of
    0: RefreshManuallyItem.Click;
    1: RefreshbytimerItem.Click;
    2: RefreshbyeventItem.Click;
    else
      RefreshManuallyItem.Click;
    end;
  end;
end;

procedure TBBConfigMainForm.MainGridCalcTitleAttributes(Sender: TObject;
  AFieldName: String; AFont: TFont; ABrush: TBrush;
  var ATitleAlignment: TAlignment);
begin
  if UpperCase(AFieldName) = UpperCase(SortFieldName) then
    ABrush.Color := clYellow;
end;

procedure TBBConfigMainForm.SetSortOrder(const Value: String);
begin
  FSortOrder := Value;
end;

procedure TBBConfigMainForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TBBConfigMainForm.RefreshbytimerItemClick(Sender: TObject);
begin
  TMenuItem(sender).Checked := true;
  ServerEventTimer.Enabled := false;
  AutorefreshTimer.Enabled := true;
  BBConfigData.RefreshType := 1;
end;

procedure TBBConfigMainForm.RefreshmanuallyItemClick(Sender: TObject);
begin
  TMenuItem(sender).Checked := true;
  ServerEventTimer.Enabled := false;
  AutorefreshTimer.Enabled := false;
  BBConfigData.RefreshType := 0;
end;

procedure TBBConfigMainForm.RefreshbyeventItemClick(Sender: TObject);
begin
  TMenuItem(sender).Checked := true;
  BBConfigData.RefreshType := 2;
  if BBConfigData.IBServerEvents.Registered then
    BBConfigData.IBServerEvents.UnRegisterEvents;
  BBConfigData.IBServerEvents.RegisterEvents;
end;

procedure TBBConfigMainForm.AutoRefreshTimerTimer(Sender: TObject);
begin
  TTimer(Sender).Enabled := false;
  try
    if Screen.ActiveForm = BBConfigMainForm then
      RefreshButton.Click;
  finally
    TTimer(Sender).Enabled := true;
  end;
end;

procedure TBBConfigMainForm.ServerEventTimerTimer(Sender: TObject);
begin
  TTimer(Sender).Enabled := false;
  if BBConfigData.GotSRVStatusChanged then
  begin
    if Screen.ActiveForm = BBConfigMainForm then  // refresh if form is active, otherwise it'll be refreshed automatically
    begin
      RefreshButton.Click;
      BBConfigData.GotSRVStatusChanged := false;
    end;
  end;
  if BBConfigData.GotSRVRWRChanged then  // refresh if form is active, otherwise it'll be refreshed automatically
  begin
    if Screen.ActiveForm = TasksListForm then
    begin
      BBConfigData.IBRWRList.Active := false;
      BBConfigData.IBRWRList.Active := true;
      BBConfigData.GotSRVRWRChanged := false;  // clear event
    end;
  end;
end;

procedure TBBConfigMainForm.RefreshFormCaption;
begin
  if BBConfigData.ConnectedToV10 then
  begin
    Caption := 'BigBrother Configurator (Lincoln)';
    ReconnecttoLincolnItem.Enabled := false;
    ReconnecttoKillingtonItem.Enabled := true;
  end
  else
  begin
    Caption := 'BigBrother Configurator (Killington)';
    ReconnecttoLincolnItem.Enabled := true;
    ReconnecttoKillingtonItem.Enabled := false;
  end;
end;

procedure TBBConfigMainForm.ReconnecttoKillingtonItemClick(
  Sender: TObject);
begin
  ReconnecttoLincolnItem.Enabled := true;
  ReconnecttoKillingtonItem.Enabled := false;
  BBConfigData.ConnectedToV10 := false;
  try
    BBConfigData.ReconnectToDatabase;
    RefreshFormCaption;
    ShowMessage('Reconnected to Killington!');
  except
    on E:Exception do
    begin
      ShowMessage('Cannot reconnect to Killington database. Error: ' + E.Message);
      Application.Terminate;
    end;
  end;
end;

procedure TBBConfigMainForm.ReconnecttoLincolnItemClick(Sender: TObject);
begin
  ReconnecttoLincolnItem.Enabled := false;
  ReconnecttoKillingtonItem.Enabled := true;
  BBConfigData.ConnectedToV10 := true;
  try
    BBConfigData.ReconnectToDatabase;
    RefreshFormCaption;
    ShowMessage('Reconnected to Lincoln!');
  except
    on E:Exception do
    begin
      ShowMessage('Cannot reconnect to Killington database. Error: ' + E.Message);
      Application.Terminate;
    end;
  end;
end;

procedure TBBConfigMainForm.Vendors2Click(Sender: TObject);
begin
  VendorsForm.ShowVendors;
end;

procedure TBBConfigMainForm.APIkeys1Click(Sender: TObject);
begin
  APIKeysEditForm.ShowAPIkeys;
end;

procedure TBBConfigMainForm.Generatelicensecodes1Click(Sender: TObject);
begin
  GeneratorForm.ShowGenerator;
end;

end.

