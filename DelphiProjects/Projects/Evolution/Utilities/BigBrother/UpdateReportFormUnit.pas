unit UpdateReportFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, isVCLBugFix;

type
  TUpdateReportForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SerialNumberLabel: TLabel;
    ReportNumberEdit: TEdit;
    ReportFileNameEdit: TEdit;
    BrowseButton: TButton;
    PostButton: TButton;
    CancelButton: TButton;
    OpenFile: TOpenDialog;
    TasksButton: TButton;
    procedure PostButtonClick(Sender: TObject);
    procedure BrowseButtonClick(Sender: TObject);
    procedure TasksButtonClick(Sender: TObject);
  private
    FCurrentSerialNumber: integer;
    procedure SetCurrentSerialNumber(const Value: integer);
    { Private declarations }
  public
    { Public declarations }
    property CurrentSerialNumber : integer read FCurrentSerialNumber write SetCurrentSerialNumber;
    function CheckEditRights : boolean;  // true if editing is enabled
    procedure ShowForm;
  end;

var
  UpdateReportForm: TUpdateReportForm;

implementation

uses BBConfigDataUnit, SBLicenseReaderUnit, SBLicenseUnit,
  SBLicenseWriterUnit, TasksListFormUnit;

{$R *.dfm}

{ TUpdateReportForm }

// true if editing is enabled
function TUpdateReportForm.CheckEditRights : boolean;
begin
  result := true; //not BBConfigData.EditIsDisabled;
end;

procedure TUpdateReportForm.SetCurrentSerialNumber(const Value: integer);
begin
  FCurrentSerialNumber := Value;
end;

procedure TUpdateReportForm.ShowForm;
begin
  if CheckEditRights then
  begin
    if BBConfigData.IBLicInfo['SERIAL_NUMBER'] <> null then
    begin
      CurrentSerialNumber := BBConfigData.IBLicInfo['SERIAL_NUMBER'];
      SerialNumberLabel.Caption := IntToStr(CurrentSerialNumber);
      ReportNumberEdit.Text := '';
      ReportFileNameEdit.Text := '';
      ShowModal;
    end;
  end
  else
    ShowMessage('You have no rights to upload reports!');
end;

procedure TUpdateReportForm.PostButtonClick(Sender: TObject);
var
  Lic : TSBLicense;
  RWRListNbr : integer;
  Reader : TSBLicenseReader;
  Writer : TSBLicenseWriter;
  NewTask : TRWRTask;
  RepFile : TFileStream;
begin
  Lic := nil;
  if not FileExists(ReportFileNameEdit.Text) then
  begin
    ShowMessage('Provided report file doesn''t exist!');
    ModalResult := mrNone;
    AbortEx;
  end;
  if Trim(ReportNumberEdit.Text) = '' then
  begin
    ShowMessage('Please, type report number!');
    ModalResult := mrNone;
    AbortEx;
  end;
  Reader := TSBLicenseReader.Create(BBConfigData.IBDatabase, nil);
  Writer := TSBLicenseWriter.Create(BBConfigData.IBDatabase, nil);
  try
    Lic := Reader.Read(CurrentSerialNumber);
    RWRListNbr := Reader.GetNextVal([RWRListGen], 1); // it's important! 
    NewTask := TRWRTask.Create;
    NewTask.RWRListNbr := RWRListNbr;
    NewTask.SerialNumber := CurrentSerialNumber;
    NewTask.TaskStatus := '0';
    try
      NewTask.ReportNumber := StrToInt(ReportNumberEdit.Text);
    except
      on E:Exception do
      begin
        NewTask.Free;
        raise;
      end;
    end;
    Lic.RWRTasks.Add(NewTask);

    // order is important
    RepFile := TFileStream.Create(ReportFileNameEdit.Text, fmShareExclusive);
    RepFile.Seek(0, soFromEnd); // reading all file to check that it's OK. Stupid but save programmers time :)
    RepFile.Seek(0, soFromBeginning);
    Writer.WriteRWRTasks(Lic);
    Writer.PostDatabaseEvent(CL_RWR_CHANGED);  // sending event
    try
      Writer.WriteRWRReport(RWRListNbr, RepFile);
    finally
      RepFile.Free
    end;
  finally
    reader.free;
    writer.free;
    if Assigned(Lic) then Lic.Free;
  end;
  ShowMessage('New task created succesfully');
end;

procedure TUpdateReportForm.BrowseButtonClick(Sender: TObject);
begin
  OpenFile.Execute;
  ReportFileNameEdit.Text := OpenFile.FileName;
end;

procedure TUpdateReportForm.TasksButtonClick(Sender: TObject);
begin
  TasksListForm.ShowTasksList;
end;

end.
