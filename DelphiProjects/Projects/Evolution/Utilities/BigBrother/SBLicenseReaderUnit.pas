{ Created by Mykhaylo Makarkin
  Augest 2007
  Allows to read TSBLicense from database
}

unit SBLicenseReaderUnit;

interface

uses DB,SysUtils, Controls, Classes, SBLicenseUnit, IBDatabase, IBQuery, SyncObjs, Variants,
  IBHeader, bbExternalInterfaces, isBaseClasses, EvLicenseInfo;

const
  DefaultInfoEmptyMsg = 'Error! Table DEFAULT_INFO is empty!';
  NotAssignedStringListMsg = 'Error! String list is nil';
  CannotGenIdMsg = 'Error! Cannot generate new ID. Query returned no rows!';

type

  SBLicenseReaderRefreshType = Set of (AllInfo, LicInfo, StatusInfo, RWRList);
  SBLicenseReaderGenNames = Set of (RWRListGen);

  SBLicenseReaderException = class(Exception);

  // Reader class
  TSBLicenseReader = class (TObject)
  private
  protected
    FCriticalSection : TCriticalSection;
    FDatabase: TIBDatabase;
    FTransaction: TIBTransaction; // read-only transaction
    FLicInfo : TIBQuery;
    FStatusInfo : TIBQuery;
    FPackageType : TIBQuery;
    FLicPackage : TIBQuery;
    FDefaults : TIBQuery;
    FLicenseList : TIBQuery;
    FRWRList : TIBQUery;
    FRWRReport : TIBQUery;
    FStatusReport : TIBQUery;
    FGenId : TIBQuery;
    FAPIKeys : TIBQuery;
    function InternalCheckInDatabase (PSerialNumber : integer) : boolean;
    procedure InternalRefresh(const PSBLicense : TSBLicense; PTypeOfRefresh : integer); // PTypeOfRefresh: 0 -all, 1 - LicInfo and LicPackage, 2 - StatusInfo, 3 - RWRList
  public
    // Database should be provided and opened
    // if critical section is not nil it will be used
    constructor Create(const PDatabase : TIBDatabase; const PCriticalSection : TCriticalSection);
    destructor Destroy; override;
    function NewEmptySBLicense (PSerialNumber : integer) : TSBLicense; // don't add record to database, just create and fill structure
    function CheckInDatabase (PSerialNumber : integer) : boolean;
    // all functions don't load RWRList "DONE" tasks
    function Read (PSerialNumber : integer) : TSBLicense;
    procedure Refresh(const PSBLicense : TSBLicense; pRefreshType : SBLicenseReaderRefreshType);
    procedure RefreshAll (const PSBLicense : TSBLicense); // internally calls RefreshLicInfo, RefreshStatusInfo, RefreshRWRList
    procedure RefreshLicInfo (const PSBLicense : TSBLicense);
    procedure RefreshStatusInfo (const PSBLicense : TSBLicense);
    procedure RefreshRWRList (const PSBLicense : TSBLicense);
    // service
    procedure ReadLicenseList (const PStrings : TStrings);  // reads sorted by asc list of all clients' serial numbers
    function GetNextVal(PGenName : SBLicenseReaderGenNames; PStep : cardinal) : integer; // get new generator value
    procedure ReadRWRReport(const PRWRListNBR: integer; const  PStream: TStream);
    procedure ReadStatusReport(const PSerialNumber: integer; const  PStream: TStream);
    function ReadReportStatus (PSerialNumber : integer) : Variant;  // can return null if there's not record in status_info for such serial number
    function ReadLicenseUpdateStatus (PSerialNumber : integer) : Variant;  // can return null if there's not record in status_info for such serial number
  end;

implementation

{ TSBLicenseReader }

// Database should be provided and opened
// if critical section is not nil it will be used
constructor TSBLicenseReader.Create(const PDatabase: TIBDatabase; const PCriticalSection : TCriticalSection);
begin
  if (not Assigned(PDatabase)){ or (not pDataBase.Connected) } then
     raise SBLicenseReaderException.Create('Error! You should provide connected database!');

  inherited Create;

  if Assigned(PCriticalSection) then FCriticalSection := PCriticalSection;

  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FDatabase := PDatabase;
    FTransaction := TIBTransaction.Create(nil);
    FTransaction.Params.Clear;
    FTransaction.Params.Add('read');
    FTransaction.Params.Add('read_committed');
    FTransaction.Params.Add('no_rec_version');
    FTransaction.Params.Add('nowait');
    FTransaction.AllowAutoStart := false;
    FTransaction.DefaultDatabase := FDatabase;

    FLicInfo := TIBQuery.Create(nil);
    FLicInfo.Database := FDatabase;
    FLicInfo.Transaction := FTransaction;
    FLicInfo.SQL.text := 'select SERIAL_NUMBER, SERVICE_BUREAU_NAME, REPORT_GRACE_PERIOD, LICENSE_FREQUENCY, LICENSE_EXPIRATION from LIC_INFO where serial_number = :p1';

    FAPIKeys := TIBQuery.Create(nil);
    FAPIKeys.Database := FDatabase;
    FAPIKeys.Transaction := FTransaction;
    FAPIKeys.SQL.text := 'select  a.api_key, a.APP_KEY_TYPE, a.APPLICATION_NAME, b.VENDOR_CUSTOM_NBR, b.VENDOR_NAME from API_KEY a, VENDOR b, LIC_API_KEY c where c.serial_number = :p1 and c.api_key=a.api_key and a.vendor_custom_nbr=b.vendor_custom_nbr';

    FStatusInfo := TIBQuery.Create(nil);
    FStatusInfo.Database := FDatabase;
    FStatusInfo.Transaction := FTransaction;
    FStatusInfo.Sql.Text := 'select CONNECTED, DATABASE_VERSION, DATE_TIME_CONNECTED, ' +
      'LAST_ADRESS, LICENSE_UPDATED, REPORT_RAN, REPORT_STATUS, REPORT_UPDATED, SERIAL_NUMBER, SERVER_VERSION, SERVER_VERSION_SENT, DATABASE_VERSION_SENT, LICENSE_UPDATE_STATUS from STATUS_INFO where serial_number = :p1';

    FPackageType := TIBQuery.Create(nil);
    FPackageType.Database := FDatabase;
    FPackageType.Transaction := FTransaction;
    FPackageType.SQL.text := 'select  DEFAULT_VALUE, LONG_DESCRIPTION, PACKAGE_TYPE_NBR, SHORT_DESCRIPTION  from package_type order by package_type_nbr';

    FLicPackage := TIBQuery.Create(nil);
    FLicPackage.Database := FDatabase;
    FLicPackage.Transaction := FTransaction;
    FlicPackage.Sql.Text := 'select PACKAGE_TYPE_NBR , SERIAL_NUMBER from LIC_PACKAGE where serial_number=:p1';

    FDefaults := TIBQuery.Create(nil);
    FDefaults.Database := FDatabase;
    FDefaults.Transaction := FTransaction;
    FDefaults.SQL.Text := 'select  LICENSE_EXPIRATION,  LICENSE_FREQUENCY,  REPORT_GRACE_PERIOD  from DEFAULT_INFO';

    FLicenseList := TIBQuery.Create(nil);
    FLicenseList.Database := FDatabase;
    FLicenseList.Transaction := FTransaction;
    FLicenseList.SQL.text := 'select SERIAL_NUMBER from LIC_INFO ORDER BY SERIAL_NUMBER';

    FRWRList := TIBQuery.Create(nil);
    FRWRList.Database := FDatabase;
    FRWRList.Transaction := FTransaction;
    FRWRList.SQL.text := 'select RWR_LIST_NBR, SERIAL_NUMBER, REPORT_NUMBER, TASK_STATUS from RWR_LIST WHERE SERIAL_NUMBER=:P1 ORDER BY RWR_LIST_NBR';

    FRWRReport := TIBQuery.Create(nil);
    FRWRReport.Database := FDatabase;
    FRWRReport.Transaction := FTransaction;
    FRWRReport.SQL.text := 'select REPORT_FILE from RWR_LIST WHERE RWR_LIST_NBR=:P_RWR_LIST_NBR';

    FStatusReport := TIBQuery.Create(nil);
    FStatusReport.Database := FDatabase;
    FStatusReport.Transaction := FTransaction;
    FStatusReport.SQL.text := 'select REPORT_FILE from status_info WHERE serial_number=:P_serial_number';

    FGenId := TIBQuery.Create(nil);
    FGenId.Database := FDatabase;
    FGenId.Transaction := FTransaction;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

destructor TSBLicenseReader.Destroy;
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    if FLicInfo.Active then FLicInfo.Close;
    if FAPIKeys.Active then FAPIKeys.Close;
    if FStatusInfo.Active then FStatusInfo.Close;
    if FPackageType.Active then FPackageType.Close;
    if FLicPackage.Active then FLicPackage.Close;
    if FDefaults.Active then FDefaults.Close;
    if FLicenseList.Active then FLicenseList.Close;
    if FRWRList.Active then FRWRList.Close;
    if FRWRReport.Active then FRWRReport.Close;
    if FStatusReport.Active then FStatusReport.Close;
    if FGenId.Active then FGenId.Close;

    FLicInfo.Transaction := nil;
    FLicInfo.Database := nil;
    FLicInfo.Free;

    FAPIKeys.Transaction := nil;
    FAPIKeys.Database := nil;
    FAPIKeys.Free;

    FStatusInfo.Transaction := nil;
    FStatusInfo.Database := nil;
    FStatusInfo.Free;

    FPackageType.Transaction := nil;
    FPackageType.Database := nil;
    FPackageType.Free;

    FLicPackage.Transaction := nil;
    FLicPackage.Database := nil;
    FLicPackage.Free;

    FDefaults.Transaction := nil;
    FDefaults.Database := nil;
    FDefaults.Free;

    FLicenseList.Transaction := nil;
    FLicenseList.Database := nil;
    FLicenseList.Free;

    FRWRList.Transaction := nil;
    FRWRList.Database := nil;
    FRWRList.Free;

    FRWRReport.Transaction := nil;
    FRWRReport.Database := nil;
    FRWRReport.Free;

    FStatusReport.Transaction := nil;
    FStatusReport.Database := nil;
    FStatusReport.Free;

    FGenId.Transaction := nil;
    FGenId.Database := nil;
    FGenId.Free;

    if FTransaction.Active then FTransaction.Rollback;  // read-only transaction
    FTransaction.Free;
    FTransaction := nil;

    FDatabase := nil;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
  FCriticalSection := nil;

  inherited;
end;

// don't add record to database, just create and fill structure
function TSBLicenseReader.NewEmptySBLicense (PSerialNumber : integer) : TSBLicense;
var
  tmp : TModuleInfo;
begin
  result := nil;
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      if InternalCheckIndatabase(pSerialNumber) then
        raise SBLicenseReaderException.Create('Error! Serial number ' + IntToStr(PSerialNumber) + ' already exists in database!')
      else
      begin
        FDefaults.Open;
        try
          if FDefaults.Eof then
            raise SBLicenseReaderException.Create(DefaultInfoEmptyMsg);
          FPackageType.Open;
          try
            FPackageType.FetchAll;
            FPackageType.First;

            result := TSBLicense.Create;
            result.SerialNumber := PSerialNumber;
            try
              result.ServiceBureauName := 'Please, fill this field. Lic# ' + IntToStr(PSerialNumber);
              result.ReportGraCePeriod := FDefaults['REPORT_GRACE_PERIOD'];
              result.LicenseExpiration := FDefaults['LICENSE_EXPIRATION'];
              result.LicenseFrequency := FDefaults['LICENSE_FREQUENCY'];
              while not FPackageType.eof do
              begin
                tmp := TModuleInfo.Create;
                try
                  tmp.PackageTypeNbr := FPackageType['PACKAGE_TYPE_NBR'];
                  tmp.ShortDescription := FPackageType['SHORT_DESCRIPTION'];
                  tmp.LongDescription := FPackageType['LONG_DESCRIPTION'];
                  tmp.Value := FPackageType['DEFAULT_VALUE'];
                except
                  on E: Exception do
                  begin
                    tmp.Free; raise;
                  end;
                end;
                result.ModuleInfos.Add(tmp);
                FPackageType.Next;
              end;
            except
              on E:Exception do
              begin
                Result.Free; Result := nil; raise;
              end;
            end;
          finally  // PackageType
            FPackageType.Close;
          end;
        finally  // Defaults
          FDefaults.Close;
        end;
      end;  // else
    finally
      FTransaction.Rollback;  // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

function TSBLicenseReader.Read (PSerialNumber : integer): TSBLicense;
begin
  result := nil;
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      if InternalCheckIndatabase(pSerialNumber) then
      begin
        result := TSBLicense.Create;
        result.SerialNumber := PSerialNumber;
        InternalRefresh(result, 0);
      end;
    finally
      FTransaction.Rollback; // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

function TSBLicenseReader.CheckInDatabase(PSerialNumber: integer): boolean;
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      result := InternalCheckInDatabase(PSerialNumber);
    finally
      FTransaction.Rollback; // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseReader.RefreshAll(const PSBLicense: TSBLicense);
begin
  Refresh(PSBLicense, [AllInfo]);
end;

procedure TSBLicenseReader.RefreshLicInfo(const PSBLicense: TSBLicense);
begin
  Refresh(PSBLicense, [LicInfo]);
end;

procedure TSBLicenseReader.RefreshStatusInfo(const PSBLicense: TSBLicense);
begin
  Refresh(PSBLicense, [StatusInfo]);
end;

// PTypeOfRefresh: 0 -all, 1 - LicInfo and LicPackage, 2 - StatusInfo, 3 - RWRList
procedure TSBLicenseReader.InternalRefresh(const PSBLicense: TSBLicense;
  PTypeOfRefresh : integer);
var
  tmp : TModuleInfo;
  tmp1 : TRWRTask;

  procedure RefreshAPIKeys(const PSBLicense : TSBLicense);
  var
    NewAPIKey : IisListOfValues;
  begin
    PSBLicense.APIKeys.Clear;
    FAPIKeys.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FAPIKeys.Open;
    try
      while not FAPIKeys.eof do
      begin
        NewAPIKey := TisListOfValues.Create;
        NewAPIKey.AddValue(APIKey_AppName, FAPIKeys['APPLICATION_NAME']);
        NewAPIKey.AddValue(APIKey_VendorCustomNbr, FAPIKeys['VENDOR_CUSTOM_NBR']);
        NewAPIKey.AddValue(APIKey_VendorName, FAPIKeys['VENDOR_NAME']);
        PSBLicense.APIKeys.AddValue(FAPIKeys['API_KEY'], NewAPIKey);
        FAPIKeys.Next;
      end;
    finally
      FAPIKeys.Close;
    end;
  end;

begin
  if (PTypeOfRefresh = 2) or (PTypeOfRefresh = 0) then
  begin
    // reading status info
    FStatusInfo.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FStatusInfo.Open;
    try
      if FStatusInfo.eof then
        PSBLicense.ClearStatusInfo
      else
        with PSBLicense do
        begin
          Connected := FStatusInfo['CONNECTED'];
          DatabaseVersion := FStatusInfo['DATABASE_VERSION'];
          ServerVersion := FStatusInfo['SERVER_VERSION'];
          DatabaseVersionSent := FStatusInfo['DATABASE_VERSION_SENT'];
          ServerVersionSent := FStatusInfo['SERVER_VERSION_SENT'];
          DateTimeConnected  := FStatusInfo['DATE_TIME_CONNECTED'];
          LastAddress := FStatusInfo['LAST_ADRESS'];
          LicenseUpdated := FStatusInfo['LICENSE_UPDATED'];
          ReportRan := FStatusInfo['REPORT_RAN'];
          ReportStatus := FStatusInfo['REPORT_STATUS'];
          ReportUpdated := FStatusInfo['REPORT_UPDATED'];
          LicenseUpdateStatus := FStatusInfo['LICENSE_UPDATE_STATUS'];
        end;
    finally
      FStatusInfo.Close;
    end;
  end;

  if (PTypeOfRefresh = 3) or (PTypeOfRefresh = 0) then
  begin
    // reading rwr_list
    FRWRList.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
    FRWRList.Open;
    try
      FRWRList.FetchAll;
      FRWRList.First;
      PSBLicense.ClearRWRTasks;
      while not FRWRList.eof do
      begin
        tmp1 := TRWRTask.Create;
        try
          tmp1.RWRListNbr := FRWRList['RWR_LIST_NBR'];
          tmp1.SerialNumber := FRWRList['SERIAL_NUMBER'];
          tmp1.ReportNumber := FRWRList['REPORT_NUMBER'];
          tmp1.TaskStatus := FRWRList['TASK_STATUS'];
          PSBLicense.RWRTasks.Add(tmp1);
        except
          on E: Exception do
          begin
            tmp1.Free; raise;
          end;
        end;
        FRWRList.Next;
      end;
    finally
      FRWRList.Close;
    end;
  end;

  if (PTypeOfRefresh = 1) or (PTypeOfRefresh = 0) then
  begin
    // reading lic_info
    FDefaults.Open;
    try
      if FDefaults.Eof then
        raise SBLicenseReaderException.Create(DefaultInfoEmptyMsg);
      FLicInfo.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
      FLicInfo.Open;
      try
        if FLicInfo.eof then
          raise SBLicenseReaderException.Create('Error! Serial number ' + IntToStr(PSBLicense.SerialNumber) + ' doesn''t exist in database!');
        with PSBLicense do
        begin
          ServiceBureauName := FLicInfo['SERVICE_BUREAU_NAME'];
          ReportGraCePeriod := FDefaults['REPORT_GRACE_PERIOD'];
          LicenseExpiration := FDefaults['LICENSE_EXPIRATION'];
          LicenseFrequency := FDefaults['LICENSE_FREQUENCY'];
          if FLicInfo['LICENSE_EXPIRATION'] <> null then
            PSBLicense.LicenseExpiration := FLicInfo['LICENSE_EXPIRATION'];
          if FLicInfo['REPORT_GRACE_PERIOD'] <> null then
            PSBLicense.ReportGracePeriod := FLicInfo['REPORT_GRACE_PERIOD'];
          if FLicInfo['LICENSE_FREQUENCY'] <> null then
            PSBLicense.LicenseFrequency := FLicInfo['LICENSE_FREQUENCY'];
        end;  // with
      finally
        FLicInfo.Close;
      end;
    finally
      FDefaults.Close;
    end;
    // reading lic_package
    FPackageType.Open;
    try
      FPackageType.FetchAll;
      FPackageType.First;

      FLicPackage.ParamByName('P1').AsInteger := PSBLicense.SerialNumber;
      FLicPackage.Open;
      try
        FLicPackage.FetchAll;
        // deleting the existing information
        PSBLicense.ClearModuleInfos;
        // filing with defaults values
        while not FPackageType.eof do
        begin
          tmp := TModuleInfo.Create;
          try
            tmp.PackageTypeNbr := FPackageType['PACKAGE_TYPE_NBR'];
            tmp.ShortDescription := FPackageType['SHORT_DESCRIPTION'];
            tmp.LongDescription := FPackageType['LONG_DESCRIPTION'];
            tmp.Value := FPackageType['DEFAULT_VALUE'];
            // now look for real value
            if FLicPackage.Locate('SERIAL_NUMBER; PACKAGE_TYPE_NBR', VarArrayOf([PSBLicense.SerialNumber, tmp.PackageTypeNbr]), []) then
              tmp.Value := 'Y'
            else
              tmp.Value := 'N';
            PSBLicense.ModuleInfos.Add(tmp);
          except
            on E: Exception do
            begin
              tmp.Free; raise;
            end;
          end;
          FPackageType.Next;
        end;  // while
      finally
        FLicPackage.Close;
      end;
      RefreshAPIKeys(PSBLicense);
    finally
      FPackageType.Close;
    end;
  end;
end;

function TSBLicenseReader.InternalCheckInDatabase(
  PSerialNumber: integer): boolean;
begin
  result := false;
  FLicInfo.ParamByName('P1').AsInteger := PSerialNumber;
  FLicInfo.Open;
  if not FLicInfo.eof then result := true;
  FLicInfo.Close;
end;

procedure TSBLicenseReader.ReadLicenseList(const PStrings : TStrings);
begin
  if Assigned(PStrings) then
  begin
    if Assigned(FCriticalSection) then FCriticalSection.Enter;
    try
      FTransaction.StartTransaction;
      try
        FLicenseList.Open;
        try
          PStrings.Clear;
          while not FLicenseList.eof do begin
            PStrings.Add(FlicenseList.FieldByName('SERIAL_NUMBER').AsString);
            FLicenseList.Next;
          end;
        finally
          FLicenseList.Close;
        end;
      finally
        FTransaction.Rollback;  // read-only transaction
      end;
    finally
      if Assigned(FCriticalSection) then FCriticalSection.Leave;
    end;
  end
  else
    raise SBLicenseReaderException.Create(NotAssignedStringListMsg);
end;

procedure TSBLicenseReader.RefreshRWRList(const PSBLicense: TSBLicense);
begin
  Refresh(PSBLicense, [RWRList]);
end;

function TSBLicenseReader.GetNextVal(PGenName : SBLicenseReaderGenNames; PStep : cardinal) : integer; // get new generator value
var
  GenName : String;
begin
  GenName := '';
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      result := 0;
      FGenId.Close;
      FGenId.SQL.Clear;
      if RWRListGen in PGenName then GenName := 'GEN_RWR_LIST_NBR';
      FGenId.SQL.Text := 'select GEN_ID(' + GenName + ', ' + IntToStr(PStep) + ') as VALUE1 from rdb$database';
      FGenId.Open;
      try
        if not FGenId.eof then
          result := FGenId['VALUE1']
        else
          raise SBLicenseReaderException.Create(CannotGenIdMsg);
      finally
        FGenId.Close;
      end;
    finally
      FTransaction.Rollback;  // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseReader.Refresh(const PSBLicense : TSBLicense;
  pRefreshType: SBLicenseReaderRefreshType);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      if AllInfo in pRefreshType then
      begin
        InternalRefresh(PSBLicense, 0);
        exit;
      end;
      if LicInfo in PRefreshType then
        InternalRefresh(PSBLicense, 1);
      if StatusInfo in PRefreshType then
        InternalRefresh(PSBLicense, 2);
      if RWRList in PRefreshType then
        InternalRefresh(PSBLicense, 3);
    finally
      FTransaction.Rollback;  // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseReader.ReadRWRReport(const PRWRListNBR: integer;
const  PStream: TStream);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  if not Assigned(PStream) then
    raise SBLicenseReaderException.Create('Error! BLOB stream not assigned!');
  try
    FTransaction.StartTransaction;
    try
      FRWRreport.Active := false;
      try
        FRWRReport.ParamByName('P_RWR_LIST_NBR').AsInteger := PRWRListNbr;
        FRWRReport.Active := true;
        TBlobField(FRWRReport.FieldByName('REPORT_FILE')).SaveToStream(PStream);
      finally
        FRWRreport.Active := false;
      end;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

function TSBLicenseReader.ReadReportStatus(PSerialNumber: integer): Variant;
begin
  result := null;
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FStatusInfo.Active := false;
      FStatusInfo.ParamByName('P1').AsInteger := PSerialNumber;
      FStatusInfo.Open;
      try
        if not FStatusInfo.eof then
          result := FStatusInfo['REPORT_STATUS'];
      finally
        FStatusInfo.Active := false;
      end;
    finally
      FTransaction.Rollback; // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

procedure TSBLicenseReader.ReadStatusReport(const PSerialNumber: integer;
  const PStream: TStream);
begin
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  if not Assigned(PStream) then
    raise SBLicenseReaderException.Create('Error! BLOB stream not assigned!');
  try
    FTransaction.StartTransaction;
    try
      FStatusReport.Active := false;
      try
        FStatusReport.ParamByName('P_SERIAL_NUMBER').AsInteger := PSerialNumber;
        FStatusReport.Active := true;
        TBlobField(FStatusReport.FieldByName('REPORT_FILE')).SaveToStream(PStream);
      finally
        FStatusReport.Active := false;
      end;
      FTransaction.Commit;
    except
      on E:Exception do
      begin
        FTransaction.Rollback; raise;
      end;
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

function TSBLicenseReader.ReadLicenseUpdateStatus(
  PSerialNumber: integer): Variant;
begin
  result := null;
  if Assigned(FCriticalSection) then FCriticalSection.Enter;
  try
    FTransaction.StartTransaction;
    try
      FStatusInfo.Active := false;
      FStatusInfo.ParamByName('P1').AsInteger := PSerialNumber;
      FStatusInfo.Open;
      try
        if not FStatusInfo.eof then
          result := FStatusInfo['LICENSE_UPDATE_STATUS'];
      finally
        FStatusInfo.Active := false;
      end;
    finally
      FTransaction.Rollback; // read-only transaction
    end;
  finally
    if Assigned(FCriticalSection) then FCriticalSection.Leave;
  end;
end;

end.
