unit EventReceiverUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SvcMgr, Dialogs,
  IBEvents, NewBBUnit, ConnectionsPoolUnit, StdCtrls, ExtCtrls,
  ISErrorUtils, Clipbrd{, IDTCPServer};

const
  SpecialErrorCode = -999999;

type

  TEventReceiver = class;

  IEventReceiver = interface
  ['{B3719094-1E8D-4065-B2A9-EA9F4863BACA}']
    function GetImplementation : TEventReceiver;
  end;

  TEventReceiver = class(TInterfacedObject, IEventReceiver)
  private
    Connection : IBBConnectionExt;
  protected
    EventReceiver: TIBEvents;
    EventReceiverReconnectTimer: TTimer;
    procedure EventReceiverEventAlert(Sender: TObject; EventName: String;
      EventCount: Integer; var CancelAlerts: Boolean);
    procedure EventReceiverError(Sender: TObject; ErrorCode: Integer);
    procedure EventReceiverReconnectTimerTimer(Sender: TObject);
    procedure FindAndTakeConnectionAndEvent;
  public
    constructor Create;
    destructor Destroy; override;
    function GetImplementation : TEventReceiver;
  end;

implementation

uses MyTCPServerUnit, EvConsts, SBLicenseReaderUnit, SBLicenseUnit,
  SBLicenseWriterUnit, isLogFile, GlovalObjectsUnit;

{ TEventReceiver }

constructor TEventReceiver.Create;
begin
  EventReceiver := TIBEvents.Create(nil);
  EventReceiver.AutoRegister := false;
  EventReceiver.Registered := false;
  EventReceiver.OnEventAlert := EventReceiverEventAlert;
  EventReceiver.OnError := EventReceiverError;

  EventReceiverReconnectTimer := TTimer.Create(nil);
  EventReceiverReconnectTimer.OnTimer := EventReceiverReconnectTimerTimer;

  FindAndTakeConnectionAndEvent;
  EventReceiverReconnectTimer.Interval := NewBB.LicenseFullRefreshTime;
  EventReceiverReconnectTimer.Enabled := true;
end;

destructor TEventReceiver.Destroy;
begin
  EventReceiverReconnectTimer.Enabled := false;
  try
    EventReceiver.UnRegisterEvents;
  except
    ;
  end;
  try
    EventReceiver.Database := nil;
  except
    ;
  end;
  try
    NewBB.Global.GetConnectionPool.ReleaseSlot((Connection as IBBConnection));
  except
    ;
  end;
  Connection := nil;
  EventReceiver.Free;
  EventReceiverReconnectTimer.Free;
  inherited;
end;

procedure TEventReceiver.EventReceiverError(Sender: TObject;
  ErrorCode: Integer);
begin
  if ErrorCode <> SpecialErrorCode then
  begin
    if (NewBB.Global.GetConnectionPool.GetLogSeverity <= LogSeverityImportant) then // IMPORTANT
      GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in EventReceiver', 'Exception in EventReceiver. Error code:' + intToStr(ErrorCode));
  end;
  try
    EventReceiver.UnRegisterEvents;
  except
    ;
  end;
  try
    EventReceiver.Database := nil;
  except
    ;
  end;
  try
    NewBB.Global.GetConnectionPool.ReleaseSlot((Connection as IBBConnection));
    Connection := nil;
    FindAndTakeConnectionAndEvent;
  except
    on E:Exception do
    begin
      GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception during recreation EventReceiver''s  connection', 'Exception during recreation EventReceiver''s  connection. ' + E.Message);
      NewBB.Global.GetEMail.Write('Exception during recreation EventReceiver''s  connection. ' + E.Message);
    end;
  end;
end;

procedure TEventReceiver.EventReceiverEventAlert(Sender: TObject;
  EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
var
  i : integer;
  tmpI : IClientThreadData;
  tmpList : IInterfaceList;
begin
  if NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
    GlobalLogFile.AddEvent(etInformation, BBEventClass, 'Received refresh event', '');
  // algoritm is very free, it just sends the same signal for all ClientThreads!
  try
    tmpList := NewBB.GetRBList;
    tmpList.Lock;
    try
      for i := 0 to tmpList.Count - 1 do
      begin
        tmpI := tmpList[i] as IClientThreadData;
        if EventName = CL_LIC_CHANGED then
          tmpI.SetRefreshLicInfo(true);
        if EventName = CL_STATUS_CHANGED then
          tmpI.SetRefreshStatusInfo(true);
        if EventName = CL_RWR_CHANGED then
          tmpI.SetRefreshRWRList(true);
        if EventName = CL_LIC_UPD_CHANGED then
          tmpI.SetRefreshLicUpsStatusInfo(true);
      end;
    finally
      tmpList.Unlock;
    end;
  except
    on E:Exception do
    begin
      if (NewBB.Global.GetConnectionPool.GetLogSeverity > LogSeverityImportant) then // ALL
      begin
        GlobalLogFile.AddEvent(etError, BBEventClass, 'Exception in EventReceiverEventAlert', 'Exception in EventReceiverEventAlert:' + E.Message);
        NewBB.Global.GetEMail.Write('Exception in EventReceiverEventAlert:' + E.Message);
      end;
    end;
  end;
end;

procedure TEventReceiver.EventReceiverReconnectTimerTimer(Sender: TObject);
begin
  EventReceiverReconnectTimer.Enabled := false;
  // here we imitate error to recreate EventReceiver
  // we do it by timer because to simplify code
  try
    EventReceiverError(self, SpecialErrorCode);
  except
    ;
  end;
  EventReceiverReconnectTimer.Enabled := true;    
end;

procedure TEventReceiver.FindAndTakeConnectionAndEvent;
begin
  Connection := (NewBB.Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, true) as IBBConnectionExt);
  EventReceiver.Database := Connection.GetImplementation.GetDatabase;
  EventReceiver.Events.Add(CL_LIC_CHANGED);
  EventReceiver.Events.Add(CL_STATUS_CHANGED);
  EventReceiver.Events.Add(CL_RWR_CHANGED);
  EventReceiver.Events.Add(CL_LIC_UPD_CHANGED);
  EventReceiver.RegisterEvents;
end;

function TEventReceiver.GetImplementation: TEventReceiver;
begin
  result := self;
end;

end.
