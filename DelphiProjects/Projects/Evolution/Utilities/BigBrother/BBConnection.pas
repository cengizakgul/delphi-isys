unit BBConnection;

interface

uses EvTransportShared, EvTransportInterfaces, EvTransportDatagrams, BBExternalInterfaces, SysUtils, isAppIDs,
     isSocket, isThreadManager;

type
  IbbTCPServer = interface
  ['{389FCB47-C8F2-43FE-85FA-C8769F56AF4B}']
    function  GetPort: String;
    procedure SetPort(const AValue: String);
    function  GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  Active: Boolean read GetActive write SetActive;
    property  Port: String read GetPort;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
  end;

  TbbTCPServer = class(TevCustomTCPServer, IbbTCPServer)
  private
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
  public
    destructor Destroy; override;
  end;


  TbbDatagramDispatcher = class(TevDatagramDispatcher)
  private
  protected
    function  DoOnLogin(const ADatagram: IevDatagram): Boolean; override;
//    procedure dmGetNewLicense(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmReportReady(const ADatagram: IevDatagram; out AResult: IevDatagram);
    procedure dmRegister(const ADatagram: IevDatagram; out AResult: IevDatagram);

    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    procedure RegisterHandlers; override;
  end;


implementation

uses isBaseClasses, NewBBUnit, MyTCPServerUnit, ConnectionsPoolUnit, isLogFile, GlovalObjectsUnit;

{ TevTCPServer }

function TbbTCPServer.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TbbDatagramDispatcher.Create;
end;

destructor TbbTCPServer.Destroy;
begin

  inherited;
end;

procedure TbbTCPServer.DoOnConstruction;
begin
  inherited;
  SetPort(BBPort);
end;

{ TbbDatagramDispatcher }

function TbbDatagramDispatcher.CheckClientAppID(const AClientAppID: String): Boolean;
begin
  Result := inherited CheckClientAppID(AClientAppID);
  { todo : implement CheckClientAppID }
end;

function TbbDatagramDispatcher.DoOnLogin(const ADatagram: IevDatagram): Boolean;
var
  tmpConnection : IBBConnection;
  tmpInt : Integer;
  User : String;
  IPAddress : STring;
  Session: IevSession;
  SessionId : String;
begin
  Result := False;
  SessionID := ADatagram.Header.SessionID;
  IPAddress := '';
  Session := FindSessionByID(SessionID);
  IPAddress := (Session.Transmitter.TCPCommunicator.GetConnectionsBySessionID(SessionID)[0] as IisTCPSocket).GetRemoteIPAddress;
  try
    User := ADatagram.Header.User;
    tmpInt := StrToInt(User);
  except
    on E:Exception do
    begin
      if NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
        GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call DoOnLogin. Connection refused. IP address: ' + IpAddress, 'Error during call DoonLogin. Connection refused. IP address: ' + IpAddress + '. SerialNumber is not integer type: ' + User);
      exit;
    end;
  end;
  // get connection
  tmpConnection := NewBB.Global.GetConnectionPool.FindAndTakeSlotWaitSomeTime(FindSlotWaitTime, True);
  try
    // checking for serial_number in database
    try
      {Result := True;}
      Result := tmpConnection.GetReader.CheckInDatabase(StrToInt(User));
      if not Result then
        if NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etWarning, BBEventClass, 'Connection refused. IP address: ' + IpAddress, 'Refused connection for ' + User + ' because this serial number doesn''t exist.',);
    except
      on E:Exception do
      begin
        tmpConnection.IncErrorCount;
        if NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityAll then  // ALL
          GlobalLogFile.AddEvent(etError, BBEventClass, 'Error during call CheckInDatabase.', 'Error during call CheckInDatabase. ' + E.Message + 'Section ' + IntToStr(tmpInt));
        Exit;
      end;
    end;
  finally
    // free connection interface
    NewBB.Global.GetConnectionPool.ReleaseSlot(tmpConnection);
    tmpConnection := nil;
  end;
end;

procedure TbbDatagramDispatcher.dmReportReady(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  tmpI : IClientThreadDataExt;
begin
  tmpI := NewBB.FindRB(ADatagram.Params.Value['ASerialNumber']) as IClientThreadDataExt;
  if Assigned(tmpI) then
  begin
    tmpI.GetLock.Lock;
    try
      (tmpI as IBigBrother).ReportReady(ADatagram.Params.Value['ASerialNumber'], ADatagram.Params.Value['AReportNumber']);
    finally
      tmpI.GetLock.Unlock;
    end;
    AResult := CreateResponseFor(ADatagram.Header);
  end
  else
    raise Exception.Create('Error: cannot find BigBrother object for this session!');
end;

{
procedure TbbDatagramDispatcher.dmGetNewLicense(const ADatagram: IevDatagram; out AResult: IevDatagram);
var
  tmpI : IClientThreadDataExt;
  sLicense: String;
begin
  tmpI := FBB.FindRB(ADatagram.Header.SessionID) as IClientThreadDataExt;
  if Assigned(tmpI) then
  begin
    tmpI.EnterCriticalSection;
    try
      sLicense := (tmpI as IBigBrother).GetNewLicense(ADatagram.Params.Value['AModifier']);
    finally
      tmpI.LeaveCriticalSection;
    end;
  end
  else
    raise Exception.Create('Error: cannot find BigBrother object for this session!');

  AResult := CreateResponseFor(ADatagram.Header);
  AResult.Params.AddValue(Method_Result, sLicense);
end;
}
procedure TbbDatagramDispatcher.DoOnSessionStateChange(const ASession: IevSession;
  const APreviousState: TevSessionState);

  procedure DoInThread(const Params: TTaskParamList);
  begin
    NewBB.DeleteRB(Params[0]);
  end;

begin
  inherited;

  // disconnect
  if ASession.State = ssInvalid then
    if Assigned(NewBB) then
    begin
      GlobalThreadManager.RunTask(@DoInThread, MakeTaskParams([ASession.SessionID]), 'Deleting BB session in separate thread')
//      NewBB.DeleteRB(ASession.SessionID)
    end
    else
      Assert(false, 'BB'' object does not exist');

end;

procedure TbbDatagramDispatcher.RegisterHandlers;
begin
  inherited;
//  AddHandler(Method_BB_GetNewLicense, dmGetNewLicense);
  AddHandler(Method_BB_ReportReady, dmReportReady);
  AddHandler(Method_BB_Register, dmRegister);
end;

procedure TbbDatagramDispatcher.dmRegister(const ADatagram: IevDatagram;
  out AResult: IevDatagram);
var
  S: IisStringList;
  i: Integer;
  SessionID, IPAddress: String;
  Session: IevSession;
  SerialNumber: String;
begin
  SessionID := ADatagram.Header.SessionID;
  IPAddress := '';
  Session := FindSessionByID(SessionID);
  IPAddress := (Session.Transmitter.TCPCommunicator.GetConnectionsBySessionID(SessionID)[0] as IisTCPSocket).GetRemoteIPAddress;
  S := (IInterface(ADatagram.Params.Value['SerialNumbers']) as IIsStringList);
  for i := 0 to S.Count - 1 do
  begin
    SerialNumber := S[i];
    try
      NewBB.AddRB(SessionID, SerialNumber, IPAddress);
    except
      on E:EDuplicateSerialNumber do
      begin
        GlobalLogFile.AddEvent(etError, BBEventClass, 'Rejected attempt to connect with duplicate serial number (dmRegister).', 'Rejected attempt to connect with duplicate serial number (dmRegister). SN#' +
          SerialNumber + ' IP: ' + IPAddress + ' ' + #13#10  + E.Message);
        if S.Count = 1 then
          raise  // never allow duplicate serial numbers but inform Rb if it has one domain only
        else
        begin
          if NewBB.Global.GetConnectionPool.GetLogSeverity >= LogSeverityDebug then  // Debug
            GlobalLogFile.AddEvent(etError, BBEventClass, 'Rejected attempt to connect with duplicate serial number (dmRegister) for one of multiple domains.', 'Rejected attempt to connect with duplicate serial number (dmRegister) for one of multiple domains. SN#' +
              SerialNumber + ' IP: ' + IPAddress + ' ' + #13#10  + E.Message);
        end;
      end;
      on E:Exception do
        raise;
    end;
  end;

  AResult := CreateResponseFor(ADatagram.Header);
end;

end.
