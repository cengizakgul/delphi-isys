{ Created by Mykhaylo Makarkin
  Augest 2007, September 2008
}

unit SBLicenseUnit;

interface

uses SysUtils, Controls, Classes, Variants, isBaseClasses, bbExternalInterfaces;

const
  CannotCopyExceptionMsg = 'Error! Cannot copy an unassigned object!';
  EmptyServiceBureauMsg = 'Error! ServiceBureauName cannot be empty!';
  BadModuleValueMsg = 'Error! Field Value can only have values "Y" or "N"!';
  BadReportStatusMsg = 'Error! ReportStatus can be only "0", "1", "2", "3", "4". Your value is ';
  BadLicenseUpdateStatusMsg = 'Error! LicenseUpdateStatus can be only "0", "1", "2". Your value is ';
  BadTaskStatusMsg = 'Error! TaskStatus can be only "0", "1". Your value is ';

type

  ModuleInfoException = class (Exception);
  SBLicenseException = class (Exception);
  RWRTaskException = class (Exception);

  // info about a enabled/disabled module
  TModuleInfo = class (TObject)
  private
    FPackageTypeNbr: integer;
    FLongDescription: String;
    FShortDescription: String;
    FValue: String;  // 'Y' or 'N'
    procedure SetPackageTypeNbr(const Value: integer);
    procedure SetLongDescription(const Value: String);
    procedure SetShortDescription(const Value: String);
    procedure SetValue(const Value: String);
  public
    property PackageTypeNbr : integer read FPackageTypeNbr write SetPackageTypeNbr;
    property ShortDescription : String read FShortDescription write SetShortDescription;
    property LongDescription : String read FLongDescription write SetLongDescription;
    property Value : String read FValue write SetValue;
    constructor Create;
    destructor Destroy; override;
  end;

  // RWR task
  TRWRTask = class (TObject)
  private
    FRWRListNbr: integer;
    FReportNumber: integer;
    FSerialNumber: integer;
    FTaskStatus: String;
    procedure SetReportNumber(const Value: integer);
    procedure SetRWRListNbr(const Value: integer);
    procedure SetSerialNumber(const Value: integer);
    procedure SetTaskStatus(const Value: String);
  public
    property RWRListNbr : integer read FRWRListNbr write SetRWRListNbr; // USE DATABASE GENERATOR for new values (see TSBLicenseReader.GetNextVal)
    property SerialNumber : integer read FSerialNumber write SetSerialNumber;
    property ReportNumber : integer read FReportNumber write SetReportNumber;
    property TaskStatus : String read FTaskStatus write SetTaskStatus; // '0' or '1' or '2' , 0 - start task, 1 - task done, 2 - error
    constructor Create;
    destructor Destroy; override;
  end;


  // info about a license for service bureau
  TSBLicense = class (TObject)
  private
    FSerialNumber: integer;
    FServiceBureauName: String;
    FReportGraCePeriod: cardinal;
    FLicenseFrequency: cardinal;
    FLicenseExpiration: cardinal;
    FConnected: Variant;
    FDatabaseVersion: Variant;
    FServerVersion: Variant;
    FDateTimeConnected: Variant;
    FLastAddress: Variant;
    FLicenseUpdated: Variant;
    FReportRan: Variant;
    FReportStatus: Variant;
    FReportUpdated: Variant;
    FModuleInfos: Tlist;
    FDatabaseVersionSent: Variant;
    FServerVersionSent: Variant;
    FRWRTasks: Tlist;
    FLicenseUpdateStatus: Variant;
    FLastRunReportError: TDateTime;
    FAPIKeys : IisListOfValues;
    procedure SetSerialNumber(const Value: integer);
    procedure SetServiceBureauName(const Value: String);
    procedure SetReportGraCePeriod(const Value: cardinal);
    procedure SetLicenseFrequency(const Value: cardinal);
    procedure SetLicenseExpiration(const Value: cardinal);
    procedure SetConnected(const Value: Variant);
    procedure SetDatabaseversion(const Value: Variant);
    procedure SetServerVersion(const Value: Variant);
    procedure SetDateTimeConnected(const Value: Variant);
    procedure SetLastAddress(const Value: Variant);
    procedure SetLicenseUpdated(const Value: Variant);
    procedure SetReportRan(const Value: Variant);
    procedure SetReportStatus(const Value: Variant);
    procedure SetReportUpdated(const Value: Variant);
    procedure SetDatabaseVersionSent(const Value: Variant);
    procedure SetServerVersionSent(const Value: Variant);
    procedure SetLicenseUpdateStatus(const Value: Variant);
    procedure SetLastRunReportError(const Value: TDateTime);
  public
    // service property - not stored in database
    property LastRunReportError : TDateTime read FLastRunReportError write SetLastRunReportError;
    // info about license (table LIC_INFO)
    property SerialNumber : integer read FSerialNumber write SetSerialNumber;
    property ServiceBureauName : String read FServiceBureauName write SetServiceBureauName;
    property ReportGraCePeriod : cardinal read FReportGraCePeriod write SetReportGraCePeriod;
    property LicenseFrequency : cardinal read FLicenseFrequency write SetLicenseFrequency;
    property LicenseExpiration : cardinal read FLicenseExpiration write SetLicenseExpiration;
    // info about enabled and disabled modules (info from table LIC_PACKAGE (enabled modules) and info about disabled modules
    property ModuleInfos : Tlist read FModuleInfos;
    // info about RWRTasks
    property RWRTasks : Tlist read FRWRTasks;
    // info about license status (table STATUS_INFO)
    property Connected : Variant read FConnected write SetConnected; // '0', '1'
    property DatabaseVersion : Variant read FDatabaseversion write SetDatabaseversion; // string
    property ServerVersion : Variant read FServerVersion write SetServerVersion;  // string
    property DateTimeConnected : Variant read FDateTimeConnected write SetDateTimeConnected; // DateTime
    property LastAddress : Variant read FLastAddress write SetLastAddress;  // string
    property LicenseUpdated : Variant read FLicenseUpdated write SetLicenseUpdated; // Date only
    property ReportRan : Variant read FReportRan write SetReportRan;  // Date Only
    property ReportStatus : Variant read FReportStatus write SetReportStatus;  // '0' - OK, '1' - Running, '2' - Forced, '3' - Error, '4' - already running
    property ReportUpdated : Variant read FReportUpdated write SetReportUpdated;  // date only
    property ServerVersionSent : Variant read FServerVersionSent write SetServerVersionSent;  // string
    property DatabaseVersionSent : Variant read FDatabaseVersionSent write SetDatabaseVersionSent;  // string
    property LicenseUpdateStatus : Variant read FLicenseUpdateStatus write SetLicenseUpdateStatus; // '0' - OK, '1' - Updating, '2' - Forced, '3' - RD refused
    property APIKeys : IisListOfValues read FAPIKeys;
    //Constructor and destructor
    constructor Create;  // all variants will be null, ModuleInfos List will be empty
    destructor Destroy; override;
    // service procedures
    procedure ClearStatusInfo;  // sets all appropriate fields to null
    procedure ClearModuleInfos;  // deletes all list's items
    procedure ClearRWRTasks; // delete all list's items
    function IsStatusInfoEmpty : boolean; // check if all appropriate fields are nulls
    class function CopyObject(const PSBLicense : TSBLicense) : TSBLicense;  // create a new object and copy all values
  end;

implementation

{ TSBLicense }

procedure TSBLicense.ClearModuleInfos;
var
  tmp : TModuleInfo;
  i : integer;
begin
  for i := 0 to FModuleInfos.Count-1 do
  begin
    if Assigned(FModuleInfos.Items[i]) then
    begin
      tmp := TModuleInfo(FModuleInfos.Items[i]);
      FModuleInfos.Items[i] := nil;
      tmp.Free;
    end;
  end;
  FModuleInfos.Clear;
end;

procedure TSBLicense.ClearRWRTasks;
var
  tmp : TRWRTask;
  i : integer;
begin
  for i := 0 to FRWRTasks.Count-1 do
  begin
    if Assigned(RWRTasks.Items[i]) then
    begin
      tmp := TRWRTask(FRWRTasks.Items[i]);
      FRWRTasks.Items[i] := nil;
      tmp.Free;
    end;
  end;
  FRWRTasks.Clear;
end;

procedure TSBLicense.ClearStatusInfo;
begin
  FConnected := null;
  FDatabaseVersion := null;
  FServerVersion := null;
  FDatabaseVersionSent := null;
  FServerVersionSent := null;
  FDateTimeConnected  := null;
  FLastAddress := null;
  FLicenseUpdated := null;
  FReportRan := null;
  FReportStatus := null;
  FReportUpdated := null;
  FLicenseUpdateStatus := null;
  FLastRunReportError := 0;
end;

class function TSBLicense.CopyObject(const PSBLicense: TSBLicense): TSBLicense;
var
  tmp1, tmp2 : TModuleInfo;
  tmp3, tmp4 : TRWRTask;
  i : integer;
  NewAPIKey : IisListOfValues;
begin
  if Assigned(PSBLicense) then
  begin
    result := TSBLicense.Create;
    with PSBLicense do
    begin
      result.SerialNumber := SerialNumber;
      result.ServiceBureauName := ServiceBureauName;
      result.ReportGraCePeriod := ReportGraCePeriod;
      result.LicenseFrequency := LicenseFrequency;
      result.LicenseExpiration := LicenseExpiration;
      result.Connected := Connected;
      result.DatabaseVersion := DatabaseVersion;
      result.ServerVersion := ServerVersion;
      result.DatabaseVersionSent := DatabaseVersionSent;
      result.ServerVersionSent := ServerVersionSent;
      result.DateTimeConnected := DateTimeConnected;
      result.LastAddress := LastAddress;
      result.LicenseUpdated := LicenseUpdated;
      result.ReportRan := ReportRan;
      result.ReportStatus := ReportStatus;
      result.ReportUpdated := ReportUpdated;
      result.LicenseUpdateStatus := LicenseUpdateStatus;
      result.LastRunReportError := LastRunReportError;
      for i := 0 to FAPIKeys.Count - 1 do
      begin
        NewAPIKey := (IInterface(APIKeys.Values[i].Value) as IisListOfValues).GetClone;
        result.FAPIKeys.AddValue(FAPIKeys[i].Name, NewAPIKey);
      end;

      result.ClearModuleInfos;
      for i := 0 to ModuleInfos.Count-1 do begin
        if Assigned(ModuleInfos.Items[i]) then
        begin
          tmp2 := TModuleInfo(ModuleInfos.Items[i]);
          tmp1 := TModuleInfo.Create;
          tmp1.PackageTypeNbr := tmp2.PackageTypeNbr;
          tmp1.LongDescription := tmp2.LongDescription;
          tmp1.ShortDescription := tmp2.ShortDescription;
          tmp1.Value := tmp2.Value;
          result.ModuleInfos.Add(tmp1);
        end;
      end; // for;

      result.ClearRWRTasks;
      for i := 0 to RWRTasks.Count-1 do begin
        if Assigned(RWRTasks.Items[i]) then
        begin
          tmp4 := TRWRTask(RWRTasks.Items[i]);
          tmp3 := TRWRTask.Create;
          tmp3.RWRListNbr := tmp4.RWRListNbr;
          tmp3.ReportNumber := tmp4.ReportNumber;
          tmp3.SerialNumber := tmp4.SerialNumber;
          tmp3.TaskStatus := tmp4.TaskStatus;
          result.RWRTasks.Add(tmp3);
        end;
      end; // for;
    end;
  end
  else
    raise SBLicenseException.Create(CannotCopyExceptionMsg);
end;

constructor TSBLicense.Create;
begin
  inherited;
  FAPIKeys := TisListOfValues.Create;
  ClearStatusInfo;
  if not Assigned(FModuleInfos) then
    FModuleInfos := TList.Create;
  if not Assigned(FRWRTasks) then
    FRWRTasks := TList.Create;
end;

destructor TSBLicense.Destroy;
begin
  if Assigned(FModuleInfos) then
  begin
    ClearModuleInfos;
    FModuleInfos.Free;
  end;
  if Assigned(FRWRTasks) then
  begin
    ClearRWRTasks;
    FRWRTasks.Free;
  end;
  ClearStatusInfo;
  inherited;
end;

function TSBLicense.IsStatusInfoEmpty: boolean;
begin
  result := (Connected = null) and (FDatabaseVersion = null) and (FDatabaseVersionSent = null)
    and (FServerVersion = null) and (FServerVersionSent = null) and (FDateTimeConnected = null)
    and (FLastAddress = null) and (FLicenseUpdated = null)
    and (FReportRan = null) and (FReportStatus = null) and (FLicenseUpdateStatus = null)
    and (FReportUpdated=null);
end;

procedure TSBLicense.SetConnected(const Value: Variant);
begin
  FConnected := Value;
end;

procedure TSBLicense.SetDatabaseversion(const Value: Variant);
begin
  FDatabaseversion := Value;
end;

procedure TSBLicense.SetDatabaseVersionSent(const Value: Variant);
begin
  FDatabaseVersionSent := Value;
end;

procedure TSBLicense.SetDateTimeConnected(const Value: Variant);
begin
  FDateTimeConnected := Value;
end;

procedure TSBLicense.SetLastAddress(const Value: Variant);
begin
  FLastAddress := Value;
end;

procedure TSBLicense.SetLastRunReportError(const Value: TDateTime);
begin
  FLastRunReportError := Value;
end;

procedure TSBLicense.SetLicenseExpiration(const Value: cardinal);
begin
  FLicenseExpiration := Value;
end;

procedure TSBLicense.SetLicenseFrequency(const Value: cardinal);
begin
  FLicenseFrequency := Value;
end;

procedure TSBLicense.SetLicenseUpdated(const Value: Variant);
begin
  FLicenseUpdated := Value;
end;

procedure TSBLicense.SetLicenseUpdateStatus(const Value: Variant);
begin
  if Value <> null then
    if (Value <> '0') and (Value <> '1') and (Value <> '2') and (Value <> '3') then
      raise SBLicenseException.Create(BadLicenseUpdateStatusMsg + VarToStr(Value));
  FLicenseUpdateStatus := Value;
end;

procedure TSBLicense.SetReportGraCePeriod(const Value: cardinal);
begin
  FReportGraCePeriod := Value;
end;

procedure TSBLicense.SetReportRan(const Value: Variant);
begin
  FReportRan := Value;
end;

procedure TSBLicense.SetReportStatus(const Value: Variant);
begin
  if Value <> null then
    if (Value <> '0') and (Value <> '1') and (Value <> '2') and (Value <> '3') and (Value <> '4') then
      raise SBLicenseException.Create(BadReportStatusMsg + VarToStr(Value));
  FReportStatus := Value;
end;

procedure TSBLicense.SetReportUpdated(const Value: Variant);
begin
  FReportUpdated := Value;
end;

procedure TSBLicense.SetSerialNumber(const Value: integer);
begin
  FSerialNumber := Value;
end;

procedure TSBLicense.SetServerVersion(const Value: Variant);
begin
  FServerVersion := Value;
end;

procedure TSBLicense.SetServerVersionSent(const Value: Variant);
begin
  FServerVersionSent := Value;
end;

procedure TSBLicense.SetServiceBureauName(const Value: String);
begin
  if trim(Value) = '' then
    raise SBLicenseException.Create(EmptyServiceBureauMsg);
  FServiceBureauName := Value;
end;

{ TModuleInfo }

constructor TModuleInfo.Create;
begin
  FValue := 'N';
end;

destructor TModuleInfo.Destroy;
begin
  FLongDescription := '';
  FShortDescription := '';
  FValue := '';
  inherited;
end;

procedure TModuleInfo.SetLongDescription(const Value: String);
begin
  FLongDescription := Value;
end;

procedure TModuleInfo.SetPackageTypeNbr(const Value: integer);
begin
  FPackageTypeNbr := Value;
end;

procedure TModuleInfo.SetShortDescription(const Value: String);
begin
  FShortDescription := Value;
end;

procedure TModuleInfo.SetValue(const Value: String);
begin
  if (Value <> 'Y') and (Value <> 'N') then
    raise ModuleInfoException.Create(BadModuleValueMsg);
  FValue := Value;
end;

{ TRWRTask }

constructor TRWRTask.Create;
begin
  TaskStatus := '0';
end;

destructor TRWRTask.Destroy;
begin
  FTaskStatus := '';
  inherited;
end;

procedure TRWRTask.SetReportNumber(const Value: integer);
begin
  FReportNumber := Value;
end;

procedure TRWRTask.SetRWRListNbr(const Value: integer);
begin
  FRWRListNbr := Value;
end;

procedure TRWRTask.SetSerialNumber(const Value: integer);
begin
  FSerialNumber := Value;
end;

procedure TRWRTask.SetTaskStatus(const Value: String);
begin
  if (Value <> '0') and (Value <> '1') and (Value <> '2') then
    raise RWRTaskException.Create(BadTaskStatusMsg + Value);
  FTaskStatus := Value;
end;

end.
