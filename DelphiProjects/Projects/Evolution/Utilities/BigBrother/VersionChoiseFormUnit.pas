unit VersionChoiseFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TVersionChoiseForm = class(TForm)
    Rb1: TRadioButton;
    Rb2: TRadioButton;
    Button1: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
    function ShowVersionChoise(ADefaultLincoln : boolean) : integer; // returns 0 - Killington, 1 - Lincoln;
  end;

var
  VersionChoiseForm: TVersionChoiseForm;

implementation

{$R *.dfm}

{ TVersionChoiseForm }

function TVersionChoiseForm.ShowVersionChoise(
  ADefaultLincoln: boolean): integer;
begin
  if ADefaultLincoln then
    RB2.Checked := true
  else
    RB1.Checked := true;
  try
    ShowModal;
  finally
    if RB1.Checked then
      result := 0
    else
      result := 1;
  end;    
end;

end.
