unit ismfViewerFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, isMetaFileUnit, EvStreamUtils
  , isVCLBugFix
  ;

type
  TismfViewerForm = class(TForm)
    OpenFile: TOpenDialog;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    FontsList: TListBox;
    StretchCheckBox: TCheckBox;
    ProportionalCheckBox: TCheckBox;
    Image1: TImage;
    IncreaseButton: TButton;
    DecreaseButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StretchCheckBoxClick(Sender: TObject);
    procedure ProportionalCheckBoxClick(Sender: TObject);
    procedure IncreaseButtonClick(Sender: TObject);
    procedure DecreaseButtonClick(Sender: TObject);
  private
    FFileName: String;
    tmpMetafile : TIsMetafile;
    procedure SetFileName(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    procedure ShowViewerbyInterfacedObject;
    property FileName : String read FFileName write SetFileName;
  end;

var
  ismfViewerForm: TismfViewerForm;

implementation

uses emfParserUnit;

{$R *.dfm}
const IMARATIO = 1.2;

procedure TismfViewerForm.FormCreate(Sender: TObject);
begin
  StretchCheckBox.Checked := Image1.Stretch;
  ProportionalCheckBox.Checked := Image1.Proportional;
  FileName := '';
  OpenFile.Filter := DefaultFileDescription + ' (*.' + DefaultParsedFileExtention + ') | *.' + DefaultParsedFileExtention;
  if ParamCount = 0 then
    if not OpenFile.Execute then
      Application.Terminate
    else
      FileName := OpenFile.FileName
  else
  begin
    FileName := ParamStr(1);
    if UpperCase(ExtractFileExt(FileName)) <> '.' + UpperCase(DefaultParsedFileExtention) then
    begin
      ShowMessage('This viewer supports files with "' + DefaultParsedFileExtention + '" extention only.');
      Application.Terminate;
    end;
  end;
  ShowViewerbyInterfacedObject;
end;

procedure TismfViewerForm.SetFileName(const Value: String);
begin
  FFileName := Value;
end;

procedure TismfViewerForm.ShowViewerbyInterfacedObject;
var
  tmpStream: IEvDualStream;
  tmpList: TStringList;
  i: integer;
  iParser: IemfParser;
  sFontName: string;
  jFontSize: integer;
  iMf : IisStream;

begin
  Caption := 'ismfViewer. File: ' + FileName;
  tmpStream := TEvDualStreamHolder.CreateFromFile(FileName);
  tmpMetafile := TIsMetafile.Create;


  iParser := TEmfParser.CreateFromStream(tmpStream);
  iMF:=TisStream.Create;
  iParser.GetMetafile(iMf);
  try
    tmpMetafile.MakeFontsEnumerable := true;
    tmpMetafile.LoadFromStream(tmpStream.RealStream);
    tmpList := TStringList.Create;
    try
      tmpMetafile.GetFontsList(tmpList);
      FontsList.Clear;
      for i := 0 to tmpList.Count - 1 do
      begin
        sFontName := tmpList[i];
        jFontSize:=iParser.GetFontSize(sFontName);
        Fontslist.Items.Add(sFontName +'     ('+Format('%4.1f',[jFontsize / 1024.0])+' Kb)');
      end;
       Fontslist.Items.Add('________');
      Fontslist.Items.Add('EMF : '+' ('+Format('%4.1f',[iMf.Size / 1024.0])+' Kb)');
    finally
      tmpList.Free;
    end;
    Image1.Picture.Metafile := tmpMetafile;
  finally
    tmpMetafile.Free;
  end;  
end;

procedure TismfViewerForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  tmpMetafile := nil;
end;

procedure TismfViewerForm.StretchCheckBoxClick(Sender: TObject);
begin
  Image1.Stretch := StretchCheckBox.Checked;
end;

procedure TismfViewerForm.ProportionalCheckBoxClick(Sender: TObject);
begin
  Image1.Proportional := ProportionalCheckBox.Checked;
end;

procedure TismfViewerForm.IncreaseButtonClick(Sender: TObject);
begin
  Image1.Width := Round(Image1.Width * IMARATIO);
  Image1.Height := Round(Image1.Height * IMARATIO);
end;

procedure TismfViewerForm.DecreaseButtonClick(Sender: TObject);
var
  tmp1 : integer;
begin
  tmp1 := Round(Image1.Width / 2);
  if tmp1 > 100 then
  begin
    Image1.Width := Round(Image1.Width / IMARATIO);
    Image1.Height := Round(Image1.Height / IMARATIO);
  end;
end;

end.
