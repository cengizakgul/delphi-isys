object ismfViewerForm: TismfViewerForm
  Left = 311
  Top = 122
  Width = 1022
  Height = 842
  Caption = 'ismf viewer'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 104
    Top = 0
    Width = 910
    Height = 808
    Proportional = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 160
    Height = 808
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 160
      Height = 808
      Align = alClient
      TabOrder = 0
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 158
        Height = 112
        Align = alTop
        TabOrder = 0
        object StretchCheckBox: TCheckBox
          Left = 14
          Top = 8
          Width = 57
          Height = 17
          Caption = 'Stretch'
          TabOrder = 0
          OnClick = StretchCheckBoxClick
        end
        object ProportionalCheckBox: TCheckBox
          Left = 14
          Top = 26
          Width = 81
          Height = 17
          Caption = 'Proportional'
          TabOrder = 1
          OnClick = ProportionalCheckBoxClick
        end
        object IncreaseButton: TButton
          Left = 11
          Top = 48
          Width = 134
          Height = 25
          Caption = 'Increase'
          TabOrder = 2
          OnClick = IncreaseButtonClick
        end
        object DecreaseButton: TButton
          Left = 11
          Top = 80
          Width = 134
          Height = 25
          Caption = 'Decrease'
          TabOrder = 3
          OnClick = DecreaseButtonClick
        end
      end
      object FontsList: TListBox
        Left = 1
        Top = 113
        Width = 158
        Height = 694
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
  object OpenFile: TOpenDialog
    Filter = 'iSystems metafiles|*.ismf'
    Left = 8
    Top = 120
  end
end
