object ViewerForm: TViewerForm
  Left = 241
  Top = 112
  Width = 870
  Height = 640
  Caption = 'Viewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 606
    Align = alLeft
    TabOrder = 0
    object FontsList: TListBox
      Left = 1
      Top = 1
      Width = 183
      Height = 604
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object DrawPanel: TPanel
    Left = 185
    Top = 0
    Width = 677
    Height = 606
    Align = alClient
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 675
      Height = 604
      Align = alClient
      Stretch = True
    end
  end
  object OpenFile: TOpenDialog
    Filter = 'Evo enchanted metafiles|*.ismf'
    Left = 192
    Top = 8
  end
end
