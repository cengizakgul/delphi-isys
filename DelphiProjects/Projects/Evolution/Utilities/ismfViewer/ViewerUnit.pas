unit ViewerUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, isMetaFileUnit;

type
  TViewerForm = class(TForm)
    Panel1: TPanel;
    DrawPanel: TPanel;
    OpenFile: TOpenDialog;
    FontsList: TListBox;
    Image1: TImage;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    FFileName: String;
    tmpMetafile : IIsMetafile;
    procedure SetFileName(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    procedure ShowViewerbyInterfacedObject;
    procedure ShowViewerbyGraphicsObject;
    property FileName : String read FFileName write SetFileName;
  end;

var
  ViewerForm: TViewerForm;

implementation

uses EvStreamUtils;

{$R *.dfm}

{ TViewerForm }

procedure TViewerForm.SetFileName(const Value: String);
begin
  FFileName := Value;
end;

procedure TViewerForm.ShowViewerbyGraphicsObject;
begin
  if OpenFile.Execute then
  begin
    FileName := OpenFile.Filename;
    Caption := 'Viewer. File: ' + FileName;
    Image1.Picture.LoadFromFile(Filename);
    ShowModal;
  end;
end;

procedure TViewerForm.ShowViewerbyInterfacedObject;
var
  tmpStream : IEvDualStream;
  tmpList : TStringList;
  i : integer;
begin
  if OpenFile.Execute then
  begin
    FileName := OpenFile.Filename;
    Caption := 'Viewer. File: ' + FileName;
    tmpStream := TEvDualStreamHolder.CreateFromFile(FileName);
    tmpMetafile := TIsInterfacedMetafile.Create;
    tmpMetafile.LoadFromStream(tmpStream);
    tmpList := TStringList.Create;
    try
      tmpMetafile.GetFontsList(tmpList);
      FontsList.Clear;
      for i := 0 to tmpList.Count - 1 do
        Fontslist.Items.Add(tmpList[i]);
    finally
      tmpList.Free;
    end;
    Image1.Picture.Metafile := tmpMetafile.GetMetafile;
    ShowModal;
    tmpStream := nil;
    tmpMetafile := nil;
  end;
end;

procedure TViewerForm.Button1Click(Sender: TObject);
var
  tmpWriter : TWriter;
  tmpStream : IEvDualStream;
begin
  tmpStream := TEvDualStreamHolder.CreateFromNewFile('c:\ffff');
  tmpWriter := TWriter.Create(tmpStream.realStream, 1024);
  try
    tmpWriter.WriteRootComponent(Image1);
  finally
    tmpWriter.Free;
  end;
end;

procedure TViewerForm.Button2Click(Sender: TObject);
var
  tmpReader : TReader;
  tmpStream : IEvDualStream;
begin
  tmpStream := TEvDualStreamHolder.CreateFromFile('c:\ffff');
  tmpReader := TReader.Create(tmpStream.realStream, 1024);
  try
    tmpReader.ReadRootComponent(Image1);
  finally
    tmpReader.Free;
  end;
end;

procedure TViewerForm.Button3Click(Sender: TObject);
begin
  Image1.Picture := nil;
end;

procedure TViewerForm.Button4Click(Sender: TObject);
var
  M: TIsMetafile;
begin
  M := TIsMetafile.Create;
  M.Assign(Image1.Picture.Metafile);
  M.Free;
end;

end.
