object MainForm: TMainForm
  Left = 503
  Top = 195
  Width = 476
  Height = 468
  Caption = 'Evoluton Load Test Tool'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 257
    Width = 468
    Height = 177
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object Memo: TMemo
      Left = 1
      Top = 1
      Width = 466
      Height = 175
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
      WantReturns = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 468
    Height = 257
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 32
      Top = 120
      Width = 57
      Height = 13
      Caption = 'Users count'
    end
    object RBAddressLabel: TLabel
      Left = 32
      Top = 24
      Width = 121
      Height = 13
      Caption = 'Request Brocker Address'
    end
    object Label2: TLabel
      Left = 296
      Top = 88
      Width = 53
      Height = 13
      Caption = 'User Name'
    end
    object Label3: TLabel
      Left = 296
      Top = 120
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object UsersCountEdit: TEdit
      Left = 168
      Top = 120
      Width = 49
      Height = 21
      TabOrder = 0
      Text = '1'
    end
    object StartBtn: TBitBtn
      Left = 32
      Top = 152
      Width = 75
      Height = 25
      Caption = 'Start test'
      TabOrder = 1
      OnClick = StartBtnClick
    end
    object StopBtn: TBitBtn
      Left = 32
      Top = 184
      Width = 75
      Height = 25
      Caption = 'Stop test'
      Enabled = False
      TabOrder = 2
      OnClick = StopBtnClick
    end
    object DumpFileNameEdit: TEdit
      Left = 168
      Top = 56
      Width = 267
      Height = 21
      TabOrder = 3
      Text = 'EvTraffic.dat'
    end
    object RBAddressEdit: TEdit
      Left = 168
      Top = 24
      Width = 289
      Height = 21
      TabOrder = 4
      Text = 'localhost'
    end
    object SSLBox: TCheckBox
      Left = 16
      Top = 224
      Width = 49
      Height = 17
      Caption = 'SSL'
      TabOrder = 5
    end
    object FindFileBtn: TButton
      Left = 435
      Top = 56
      Width = 22
      Height = 22
      Caption = '...'
      TabOrder = 6
      OnClick = FindFileBtnClick
    end
    object Radio1: TRadioButton
      Left = 32
      Top = 56
      Width = 113
      Height = 17
      Caption = 'Use dump file name'
      TabOrder = 7
      OnClick = Radio1Click
    end
    object Radio2: TRadioButton
      Left = 32
      Top = 88
      Width = 129
      Height = 17
      Caption = 'Login/Logout - number'
      Checked = True
      TabOrder = 8
      TabStop = True
      OnClick = Radio2Click
    end
    object LoginCountEdit: TEdit
      Left = 168
      Top = 88
      Width = 49
      Height = 21
      TabOrder = 9
      Text = '1'
    end
    object UserNameEdit: TEdit
      Left = 360
      Top = 88
      Width = 97
      Height = 21
      TabOrder = 10
    end
    object PasswordEdit: TEdit
      Left = 360
      Top = 120
      Width = 97
      Height = 21
      PasswordChar = '*'
      TabOrder = 11
    end
    object StatsMemo: TMemo
      Left = 272
      Top = 152
      Width = 185
      Height = 89
      TabOrder = 12
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 200
    OnTimer = Timer1Timer
    Left = 168
    Top = 152
  end
end
