unit main;

interface

uses Windows, Forms, SysUtils, Classes, isBaseClasses, EvTransportShared, EvTransportDatagrams,
     isSocket, evStreamUtils, isBasicUtils, isThreadManager, evRemoteMethods, EvConsts,
     evTransportInterfaces, EvControllerInterfaces, isSettings, isLogFile,
     isAppIDs, ISZippingRoutines, isStreamTCP, StdCtrls, Buttons, Controls, Messages, Dialogs,
     ExtCtrls, OleCtrls, EvBasicUtils, isStatistics;

type

  IevClient = interface
  ['{AD70A610-D30D-4814-A4F7-D31FA16CAAF0}']
    function  CreateConnection(const AHost, APort, AUser, APassword: String;
                               const ASessionID, AContextID: TisGUID; const ALocalIPAddr: String = '';
                               const AAppID: String = ''): TisGUID;
    function  GetDatagramDispatcher: IevDatagramDispatcher;
    property  DatagramDispatcher: IevDatagramDispatcher read GetDatagramDispatcher;
    function  GetCommunicator: IisAsyncTCPClient;
    function  GetSession(const ASessionID: TisGUID): IevSession;
  end;

  TevClientDispatcher = class(TevDatagramDispatcher)
  protected
    procedure DoOnSessionStateChange(const ASession: IevSession; const APreviousState: TevSessionState); override;
    function  CheckClientAppID(const AClientAppID: String): Boolean; override;
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
    procedure RegisterHandlers; override;
  end;

  // RAS Service -> RB
  TevTCPClient = class(TevCustomTCPClient, IevClient)
  protected
    function  CreateDispatcher: IevDatagramDispatcher; override;
    function  GetCommunicator: IisAsyncTCPClient;
  end;

const
  WM_LOG = WM_USER + 1;

type
  TMainForm = class(TForm)
    Panel1: TPanel;
    Memo: TMemo;
    Panel2: TPanel;
    Label1: TLabel;
    RBAddressLabel: TLabel;
    UsersCountEdit: TEdit;
    StartBtn: TBitBtn;
    StopBtn: TBitBtn;
    DumpFileNameEdit: TEdit;
    RBAddressEdit: TEdit;
    SSLBox: TCheckBox;
    FindFileBtn: TButton;
    Radio1: TRadioButton;
    Radio2: TRadioButton;
    LoginCountEdit: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    UserNameEdit: TEdit;
    PasswordEdit: TEdit;
    StatsMemo: TMemo;
    Timer1: TTimer;
    procedure StartBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
    procedure FindFileBtnClick(Sender: TObject);
    procedure Radio1Click(Sender: TObject);
    procedure Radio2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
  protected
    procedure Log(var Message: TMessage); message WM_LOG;
  public
    TM: IThreadManager;
    procedure RunDatagrams;
    procedure SendLog(const ALog: String);
  end;

var
  MainForm: TMainForm;
  Client: TevTCPClient;

implementation

uses SyncObjs;

{$R *.dfm}

type
  PevRunResult = ^TevRunResult;
  TevRunResult = record
    StartTime: TDateTime;
    RunTime: Int64;
  end;

  TevRunList = class(TList)
  private
    FCS: TCriticalSection;
    FFinished: Boolean;
    function GetResults(Index: Integer): PevRunResult;
  public
    ST: Int64;
    ET: Int64;
    Exception: String;
    property Results[Index: Integer]: PevRunResult read GetResults;
    property Finished: Boolean read FFinished write FFinished;
    function AverageRunTime: Int64;
    function MinRunTime: Int64;
    function MaxRunTime: Int64;
    procedure AddResults(const ARunResult: PevRunResult);
    procedure DeleteResults(Index: Integer);
    procedure Lock;
    procedure Unlock;
    constructor Create;
    destructor Destroy; override;
  end;

  function TevRunList.GetResults(Index: Integer): PevRunResult;
  begin
    Result := Items[Index];
  end;

  procedure TevRunList.AddResults(const ARunResult: PevRunResult);
  var
    P: PevRunResult;
  begin
    New(P);
    System.Move(ARunResult^, P^, SizeOf(TevRunResult));
    Add(P);
  end;

  function TevRunList.AverageRunTime: Int64;
  var
    i, n: Integer;
  begin
    if FFinished then
      n := Count
    else
      n := Count - 1;  
    Result := 0;
    if n > 0 then
    begin
      for i := 0 to n - 1 do
        Result := Result + Results[i].RunTime;
      Result := Result div n;
    end;
  end;

  procedure TevRunList.DeleteResults(Index: Integer);
  var
    P: PevRunResult;
  begin
    P := Items[Index];
    Delete(Index);
    Dispose(P);
  end;

var
  Stats: TevRunList;

{ TevTCPClient }

function TevTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevClientDispatcher.Create;
end;

function TevTCPClient.GetCommunicator: IisAsyncTCPClient;
begin
  Result := GetCommunicator;
end;

procedure TMainForm.StartBtnClick(Sender: TObject);

  procedure DoRunTask(const Params: TTaskParamList);
  var
    Dump: IevDualStream;

    procedure DoRun;
    var
      Stm: IevDualStream;
      Datagram: IevDatagram;
      UserName, Password: String;
      Session: IevSession;
      SessionID, ContextID: TisGUID;

      function RunRequest: Boolean;
      var
        LocalDatagram: IevDatagram;
        LocalStm: IevDualStream;
      begin
        if Dump.Position < Dump.Size then
        begin
          LocalStm := Dump.ReadStream(nil, False);
          LocalStm.Position := 0;
          LocalDatagram := TevDatagram.CreateFromStream(LocalStm);
        end
        else
          LocalDatagram := nil;

        Result := LocalDatagram <> nil;
        if Result then
        begin
          LocalDatagram.Header.SessionID := SessionID;
          LocalDatagram.Header.ContextID := ContextID;
          LocalDatagram := Session.SendRequest(LocalDatagram);
        end;
      end;

    begin
      try
        Dump.Position := 0;
        Stm := Dump.ReadStream(nil, False);
        Stm.Position := 0;
        Datagram := TevDatagram.CreateFromStream(Stm);
        UserName := Datagram.Header.User;
        Password := Datagram.Header.Password;
        SessionID := '';
        ContextID := GetUniqueID;

        if Client.GetEncryptionType = etSSL then
          SessionID := Client.CreateConnection(MainForm.RBAddressEdit.Text, IntToStr(TCPClient_PortSSL1),
            UserName, Password, SessionID, ContextID)
        else
          SessionID := Client.CreateConnection(MainForm.RBAddressEdit.Text, IntToStr(TCPClient_Port),
            UserName, Password, SessionID, ContextID);

        Session := Client.GetSession(SessionID);

        repeat
          Dump.Position := 0;
          while (not CurrentThreadTaskTerminated) and RunRequest do;
        until CurrentThreadTaskTerminated;

      finally
        Client.Disconnect(SessionID);
      end;

    end;


  begin
    try
      SetAppID(EvoClientAppInfo.AppID);
      Dump := TevDualStreamHolder.CreateFromFile(MainForm.DumpFileNameEdit.Text);
      MainForm.SendLog(TimeToStr(Now) + ' started');
      DoRun;
      MainForm.SendLog(TimeToStr(Now) + ' stopped');

    except
      on E: Exception do
      begin
        MainForm.SendLog(E.Message);
        raise;
      end;
    end;
  end;

  {1}

  procedure DoRunTaskLoginLogout(const Params: TTaskParamList);

    procedure DoRun;
    var
      UserName, Password: String;
      Session: IevSession;
      SessionID, ContextID: TisGUID;
      Port: Integer;

      function RunRequest: Boolean;
      var
        RR: TevRunResult;
      begin
        Result := True;
        try
          if AppSwitches.ValueExists('SCRIPT') then
            if AppSwitches.ValueExists('ENCRYPTION') then
              Client.SetEncryptionType(etSSL);

          if Client.GetEncryptionType = etSSL then
            Port := TCPClient_PortSSL1
          else
            Port := TCPClient_Port;

          if AppSwitches.ValueExists('SCRIPT') then
          begin
            if AppSwitches.ValueExists('PORT') then
              Port := AppSwitches.Value['PORT'];
            if AppSwitches.ValueExists('COMPRESSION') then
              Client.SetCompressionLevel(TisCompressionLevel(Integer(AppSwitches.Value['COMPRESSION'])));
          end;

          Stats.Lock;
          try
            RR.StartTime := GetTime;
            RR.RunTime := GetTickCount;
            Stats.AddResults(@RR);
          finally
            Stats.Unlock;
          end;

          try
            SessionID := Client.CreateConnection(MainForm.RBAddressEdit.Text, IntToStr(Port), UserName, Password, SessionID, ContextID);
          except
            on E: Exception do
            begin
              Stats.Lock;
              try
                Stats.Exception := E.Message
              finally
                Stats.Unlock;
              end;
              raise;
            end;
          end;
        finally

          Stats.Lock;
          try
            Stats.ET := GetTickCount;
            Stats.Results[Stats.Count - 1].RunTime := GetTickCount - Stats.Results[Stats.Count - 1].RunTime;
          finally
            Stats.Unlock;
          end;

          Client.Disconnect(SessionID);
        end;
      end;

    var
      Counter: Integer;
      sl: TStringList;
    begin
      UserName := MainForm.UserNameEdit.Text;
      Password := HashPassword(MainForm.PasswordEdit.Text);
      SessionID := '';
      ContextID := GetUniqueID;

      Session := Client.GetSession(SessionID);

      Counter := 0;

      try

        while (not CurrentThreadTaskTerminated) and
        (Counter < StrToIntDef(MainForm.LoginCountEdit.Text, 0)) do
        begin
          Inc(Counter);
          RunRequest;
        end;
      finally
        Stats.Lock;
        try
          Stats.ET := GetTickCount;
          Stats.Finished := True;
          if AppSwitches.ValueExists('SCRIPT') then
          begin
            sl := TStringList.Create;
            try
               sl.Add(Format('COUNT=%d',[Stats.Count]));
              sl.Add(Format('AVG_LOGIN_TIME=%d',[Stats.AverageRunTime]));
              sl.Add(Format('MIN_LOGIN_TIME=%d',[Stats.MinRunTime]));
              sl.Add(Format('MAX_LOGIN_TIME=%d',[Stats.MaxRunTime]));
              sl.Add(Format('TOTAL_TIME=%d',[Stats.ET - Stats.ST]));
              if Length(Stats.Exception) > 0 then
                sl.Add('EXCEPTION='+Stats.Exception);
              
              if Length(Stats.Exception) > 0 then
                
              sl.SaveToFile(AppSwitches.Value['OUTPUT']);
            finally
              sl.Free;
              PostMessage(MainForm.Handle, WM_QUIT, 0, 0);
            end;

          end;
        finally
          Stats.Unlock;
        end;
      end;
    end;


  begin
    try
      SetAppID(EvoClientAppInfo.AppID);
      MainForm.SendLog(TimeToStr(Now) + ' Login/Logout test started');
      DoRun;
      MainForm.SendLog(TimeToStr(Now) + ' Login/Logout test stopped');

    except
      on E: Exception do
      begin
        MainForm.SendLog(E.Message);
        raise;
      end;
    end;
  end;

  {1}

var
  P: TTaskParamList;
  i: Integer;
begin
  while Stats.Count > 0 do
    Stats.DeleteResults(0);

  Stats.Lock;
  try
    Stats.Finished := False;
    Stats.ST := GetTickCount;
    Stats.ET := Stats.ST;
  finally
    Stats.Unlock;
  end;

  Timer1.Enabled := True;
  StartBtn.Enabled := False;
  StopBtn.Enabled := True;
  Client := TevTCPClient.Create;
  if MainForm.SSLBox.Checked then
    Client.SetEncryptionType(etSSL);
  Memo.Clear;
  SetLength(P, 1);
  if Radio1.Checked then
    for i := 1 to StrToInt(UsersCountEdit.Text) do
      TM.RunTask(@DoRunTask, P, Format('Playing file %s',[DumpFileNameEdit.Text]))
  else if Radio2.Checked then
    for i := 1 to StrToInt(UsersCountEdit.Text) do
      TM.RunTask(@DoRunTaskLoginLogout, P, 'Simple Login/Logout');
end;

procedure TMainForm.RunDatagrams;
begin
end;

{ TevClientDispatcher }

procedure TevClientDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
begin
  inherited;
  Handled := False;
end;

function TevClientDispatcher.CheckClientAppID(
  const AClientAppID: String): Boolean;
begin
  Result := AnsiSameStr(AClientAppID, EvoClientAppInfo.AppID) or
            AnsiSameStr(AClientAppID, EvoRWAdapterAppInfo.AppID);
end;

procedure TevClientDispatcher.DoOnSessionStateChange(
  const ASession: IevSession; const APreviousState: TevSessionState);
begin
  inherited;
  if (ASession.State = ssInvalid) and (APreviousState = ssActive) then
  begin
  end;
end;

procedure TevClientDispatcher.RegisterHandlers;
begin
  inherited;

end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  Stats := TevRunList.Create;
  TM := TThreadManager.Create('LoadTest');
  TM.Capacity := 100;
  if AppSwitches.ValueExists('?') then
  begin
    MessageBox(Handle, 'Example: LoadTestTool.exe /SCRIPT login /HOST zeus /COMPRESSION 3 /ENCRYPTION /USER admin /PASSWORD parol /REPEAT 1 /OUTPUT c:\load.txt', 'Application parameters', MB_OK + MB_ICONASTERISK);
    PostMessage(MainForm.Handle, WM_QUIT, 0, 0);
  end
  else
  if AppSwitches.ValueExists('SCRIPT') then
  begin
    if AnsiSameText(AppSwitches.Value['SCRIPT'], 'LOGIN') then
    begin
      Radio1.Checked := False;
      Radio2.Checked := True;
    end;
    if AppSwitches.ValueExists('HOST') then
      RBAddressEdit.Text := AppSwitches.Value['HOST'];
    if AppSwitches.ValueExists('USER') then
      UserNameEdit.Text := AppSwitches.Value['USER'];
    if AppSwitches.ValueExists('PASSWORD') then
      PasswordEdit.Text := AppSwitches.Value['PASSWORD'];
    if AppSwitches.ValueExists('REPEAT') then
      LoginCountEdit.Text := AppSwitches.Value['REPEAT'];

    ShowWindow(Handle, SW_MINIMIZE);
    ShowWindow(Handle, SW_HIDE);

    StartBtnClick(Self);

  end;
end;

procedure TMainForm.StopBtnClick(Sender: TObject);
begin
  StartBtn.Enabled := True;
  StopBtn.Enabled := False;
  TM.TerminateTask;
  TM.WaitForTaskEnd;
end;

procedure TMainForm.Log(var Message: TMessage);
var
  s: String;
begin
  SetLength(s, Message.wParam);
  Move(Pointer(Message.lParam)^, s[1], Message.wParam);
  FreeMem(Pointer(Message.lParam));
  Memo.Lines.Add(s);
end;

procedure TMainForm.SendLog(const ALog: String);
var
  P: Pointer;
begin
  GetMem(P, Length(ALog));
  Move(ALog[1], P^, Length(ALog));
  PostMessage(Handle, WM_LOG, Length(ALog), lParam(P));
end;

procedure TMainForm.FindFileBtnClick(Sender: TObject);
var
  Dlg: TOpenDialog;
begin
  Dlg := TOpenDialog.Create(nil);
  try
    Dlg.Title := 'Open Client Requests file';
    Dlg.Filter := 'DAT files (*.dat)|*.dat';
    if Dlg.Execute then
      DumpFileNameEdit.Text := Dlg.FileName;
  finally
    Dlg.Free;
  end;
end;

procedure TMainForm.Radio1Click(Sender: TObject);
begin
  DumpFileNameEdit.Enabled := Radio1.Checked;
  FindFileBtn.Enabled := Radio1.Checked;
  LoginCountEdit.Enabled := Radio2.Checked;
end;

procedure TMainForm.Radio2Click(Sender: TObject);
begin
  DumpFileNameEdit.Enabled := Radio1.Checked;
  FindFileBtn.Enabled := Radio1.Checked;
  LoginCountEdit.Enabled := Radio2.Checked;
end;

constructor TevRunList.Create;
begin
  inherited Create;
  Exception := '';
  FCS := TCriticalSection.Create;
end;

destructor TevRunList.Destroy;
begin
  FCS.Free;
  inherited;
end;

procedure TevRunList.Lock;
begin
  FCS.Enter;
end;

procedure TevRunList.Unlock;
begin
  FCS.Leave;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
begin
  Stats.Lock;
  try
    StatsMemo.Clear;
    StatsMemo.Lines.Add(Format('Iterations count : %d',[Stats.Count]));
    StatsMemo.Lines.Add(Format('Average login time (ms) : %d',[Stats.AverageRunTime]));
    StatsMemo.Lines.Add(Format('Min time (ms) : %d',[Stats.MinRunTime]));
    StatsMemo.Lines.Add(Format('Max time (ms) : %d',[Stats.MaxRunTime]));
    StatsMemo.Lines.Add(Format('Full time (ms) : %d',[Stats.ET - Stats.ST]));
    if Stats.Finished then
      Timer1.Enabled := False;
  finally
    Stats.Unlock;
  end;
end;

function TevRunList.MaxRunTime: Int64;
var
  i, n: Integer;
begin
  if FFinished then
    n := Count
  else
    n := Count - 1;
    
  Result := 0;
  for i := 0 to n - 1 do
    if Results[i].RunTime > Result then
         Result := Results[i].RunTime;
end;

function TevRunList.MinRunTime: Int64;
var
  i, n: Integer;
begin

  if FFinished then
    n := Count
  else
    n := Count - 1;


  Result := MaxRunTime;

  for i := 0 to n - 1 do
    if Results[i].RunTime < Result then
       Result := Results[i].RunTime;
end;

end.
