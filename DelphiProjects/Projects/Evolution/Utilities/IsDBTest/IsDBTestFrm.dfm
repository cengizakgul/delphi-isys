object Form5: TForm5
  Left = 420
  Top = 191
  Width = 442
  Height = 433
  Caption = 'Firebird DB Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 434
    Height = 399
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Settings'
      DesignSize = (
        426
        371)
      object Label1: TLabel
        Left = 12
        Top = 18
        Width = 64
        Height = 13
        Caption = 'Test DB Path'
      end
      object Label2: TLabel
        Left = 12
        Top = 74
        Width = 40
        Height = 13
        Caption = 'DB User'
      end
      object Label3: TLabel
        Left = 12
        Top = 108
        Width = 64
        Height = 13
        Caption = 'DB Password'
      end
      object edTestDBPath: TEdit
        Left = 115
        Top = 15
        Width = 257
        Height = 21
        TabOrder = 0
        Text = 'data:/db/programmer/cengiz'
      end
      object edDBUser: TEdit
        Left = 115
        Top = 70
        Width = 106
        Height = 21
        TabOrder = 1
        Text = 'EUSER'
      end
      object edDBPassword: TEdit
        Left = 114
        Top = 104
        Width = 106
        Height = 21
        PasswordChar = '*'
        TabOrder = 2
        Text = 'pps97'
      end
      object btnRun: TButton
        Left = 12
        Top = 343
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Run Test'
        TabOrder = 3
        OnClick = btnRunClick
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 147
        Width = 361
        Height = 145
        Caption = 'Tests'
        TabOrder = 4
        object Label4: TLabel
          Left = 172
          Top = 81
          Width = 87
          Height = 13
          Caption = 'Server CPU Count'
        end
        object chbSingleConTest: TCheckBox
          Left = 14
          Top = 24
          Width = 131
          Height = 17
          Caption = 'Single Connection'
          TabOrder = 0
        end
        object chbMultiConTest: TCheckBox
          Left = 14
          Top = 52
          Width = 131
          Height = 17
          Caption = 'Multiple Connections'
          TabOrder = 1
        end
        object chbCPUPerfTest: TCheckBox
          Left = 14
          Top = 81
          Width = 131
          Height = 17
          Caption = 'CPU Performance'
          TabOrder = 2
        end
        object seCPUs: TSpinEdit
          Left = 269
          Top = 77
          Width = 59
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 3
          Value = 1
        end
        object chbDataLoadTest: TCheckBox
          Left = 14
          Top = 112
          Width = 131
          Height = 17
          Caption = 'Data Load'
          Enabled = False
          TabOrder = 4
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Test Results'
      ImageIndex = 1
      object tvRes: TTreeView
        Left = 0
        Top = 0
        Width = 426
        Height = 371
        Align = alClient
        Indent = 19
        TabOrder = 0
      end
    end
  end
end
