unit IsDBTestFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, dtTest, dtFullTest, ComCtrls, isThreadManager, Spin,
  dtSettings;

const
  CM_TESTDONE = WM_USER + 1;

type
  TForm5 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    tvRes: TTreeView;
    Label1: TLabel;
    edTestDBPath: TEdit;
    Label2: TLabel;
    edDBUser: TEdit;
    Label3: TLabel;
    edDBPassword: TEdit;
    btnRun: TButton;
    GroupBox1: TGroupBox;
    chbSingleConTest: TCheckBox;
    chbMultiConTest: TCheckBox;
    chbCPUPerfTest: TCheckBox;
    Label4: TLabel;
    seCPUs: TSpinEdit;
    chbDataLoadTest: TCheckBox;
    procedure btnRunClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FFullTest: IdtFullTest;
    procedure DoInThread(const Params: TTaskParamList);
    procedure ShowResults(var Message: TMessage); message CM_TESTDONE;
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

uses isBaseClasses;

{$R *.dfm}

procedure TForm5.btnRunClick(Sender: TObject);
begin
  gvDBPath := edTestDBPath.Text;
  gvDBUser := edDBUser.Text;
  gvDBPassword := edDBPassword.Text;
  gvServerCPUCount := seCPUs.Value;

  GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([]), 'FullTest');
  btnRun.Enabled := False;
end;

procedure TForm5.DoInThread(const Params: TTaskParamList);
begin
  try
    FFullTest := TdtFullTest.Create;
    FFullTest.Params.Value['SingleConnectionTest'] := chbSingleConTest.Checked;
    FFullTest.Params.Value['MultipleConnectionTest'] := chbMultiConTest.Checked;
    FFullTest.Params.Value['CPUPerformanceTest'] := chbCPUPerfTest.Checked;
    FFullTest.Params.Value['DataLoadTest'] := chbDataLoadTest.Checked;
    FFullTest.Run;
  finally
    PostMessage(Handle, CM_TESTDONE, 0, 0);
  end;  
end;

procedure TForm5.ShowResults;
var
  TstRes: IisListOfValues;
  i, j: Integer;
  N: TTreeNode;
begin
  try
    tvRes.Items.Clear;
    for i := 0 to FFullTest.Results.Count - 1 do
    begin
      if VarIsType(FFullTest.Results[i].Value, varUnknown) then
      begin
        N := tvRes.Items.AddChild(nil, FFullTest.Results[i].Name);
        TstRes := IInterface(FFullTest.Results[i].Value) as IisListOfValues;
        if Assigned(TstRes) then
          for j := 0 to TstRes.Count - 1 do
            tvRes.Items.AddChild(N, TstRes[j].Name + ' = ' + VarToStr(TstRes[j].Value));
      end
      else
        tvRes.Items.AddChild(nil, FFullTest.Results[i].Name + ' = ' + VarToStr(FFullTest.Results[i].Value));
    end;
  finally
    btnRun.Enabled := True;
  end;  
end;

procedure TForm5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  GlobalThreadManager.TerminateTask;
  GlobalThreadManager.WaitForTaskEnd;
end;

end.
