unit dtDBAccess;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses SysUtils, Windows, DB, Variants, isBasicUtils, isBaseClasses, IBDatabase,
     IB, IBSQL, IBHeader, isSettings, isThreadManager, isDataSet, isVCLBugFix;

type
  IdtDBAccess = interface
  ['{1D8C7DE2-B412-4114-A79E-7C690F6A9174}']
    procedure RestoreDB(const ABackupFile: String);
    procedure DropDB(const ADBName: String);
    procedure OpenDB(const ADBName: String);
    function  Active: Boolean;
    procedure CloseDB;
    procedure StartTransaction(const AReadOnly: Boolean);
    procedure CommitTransaction;
    procedure RollbackTransaction;
    function  ExecQuery(const ASQL: String; const AParams: IisListOfValues): IDataSet;
  end;


  TdtDBAccess = class(TisInterfacedObject, IdtDBAccess)
  private
    FDBPatch: String;
    FUser: String;
    FPassword: String;
    FDB: TIBDatabase;
    FTransaction: TIBTransaction;
    FQuery: TIBSQL;
    function  GetIBPath: String;
    procedure SetTransParameters(const AReadOnly: Boolean);
    procedure RestoreDB(const ABackupFile: String);
    procedure DropDB(const ADBName: String);
    procedure OpenDB(const ADBName: String);
    function  Active: Boolean;
    procedure CloseDB;
    function  ExecQuery(const ASQL: String; const AParams: IisListOfValues): IDataSet;
    procedure StartTransaction(const AReadOnly: Boolean);
    procedure CommitTransaction;
    procedure RollbackTransaction;
  protected
    procedure DoOnConstruction; override;
  public
    constructor Create(const ADBPatch, AUser, APassword: String); reintroduce;
    destructor  Destroy; override;
  end;



implementation

type
  TdtFieldDefs = class(TFieldDefs)
  public
    procedure Add(const Name: string; DataType: TFieldType; Size: Integer = 0); reintroduce;
    procedure BeginUpdate; override;
    procedure EndUpdate; override;
  end;

  TdtIBSQL = class(TIBSQL)
  private
    FFieldDefs: TdtFieldDefs;
  public
    function    GetFieldDefs: TFieldDefs;
    procedure   SetParams(const AParams: IisListOfValues; const AClearUndefined: Boolean = True);
    function    AsDataSet: IDataSet;
    destructor  Destroy; override;
  end;


{ TdtDBAccess }

function TdtDBAccess.Active: Boolean;
begin
  Result := FDB.Connected;
end;

procedure TdtDBAccess.CloseDB;
begin
  FQuery.Close;
  FDB.Close;
end;

procedure TdtDBAccess.CommitTransaction;
begin
  FTransaction.Commit;
end;

constructor TdtDBAccess.Create(const ADBPatch, AUser, APassword: String);
begin
  inherited Create;
  FDBPatch := NormalizePath(ADBPatch);
  FUser := AUser;
  FPassword := APassword;
end;

destructor TdtDBAccess.Destroy;
begin
  FQuery.Free;
  FTransaction.Free;
  FDB.Free;
  inherited;
end;

procedure TdtDBAccess.DoOnConstruction;
begin
  inherited;
  FDB := TIBDatabase.Create(nil);
  FDB.LoginPrompt := False;
  FTransaction := TIBTransaction.Create(nil);
  FDB.DefaultTransaction := FTransaction;
  FQuery := TIBSQL.Create(nil);
  FQuery.Database := FDB;
  FQuery.Transaction := FTransaction;
end;

procedure TdtDBAccess.DropDB(const ADBName: String);
begin
  OpenDB(ADBName);
  FDB.DropDatabase;
end;

function TdtDBAccess.ExecQuery(const ASQL: String; const AParams: IisListOfValues): IDataSet;
var
  Q: TdtIBSQL;
begin
  Q := TdtIBSQL.Create(nil);
  try
    Q.Database := FDB;
    Q.Transaction := FTransaction;
    Q.SQL.Text := ASQL;
    Q.SetParams(AParams);
    Q.ExecQuery;
    Result := Q.AsDataSet;
  finally
    Q.Free;
  end;
end;

function TdtDBAccess.GetIBPath: String;
var
  Reg: IisSettings;
begin
  Result := AppDir;

  if not FileExists(Result + 'gds32.dll') then
  begin
    Reg := TisSettingsRegistry.Create(HKEY_LOCAL_MACHINE, '');

    Result := Reg.AsString['Software\Firebird Project\Firebird Server\Instances\DefaultInstance'];
    if Result = '' then
    begin
      Result := Reg.AsString['Software\FirebirdSQL\Firebird\CurrentVersion\RootDirectory'];
      if Result = '' then
      begin
        Result := Reg.AsString['Software\Borland\Interbase\CurrentVersion\RootDirectory'];
        if Result = '' then
          raise Exception.Create('Firebird/Inrebase client installation is not found');
      end;
    end;

    Result := NormalizePath(Result) + 'bin\';
  end;
end;

procedure TdtDBAccess.OpenDB(const ADBName: String);
begin
  CloseDB;

  FDB.DatabaseName := FDBPatch + ADBName;
  FDB.Params.Clear;
  FDB.Params.Add('USER_NAME=' + FUser);
  FDB.Params.Add('PASSWORD=' + FPassword);

  try
    FDB.Open;
  except
    on E: EIBError do
      raise
    else
    begin
      // if AV in gds32.dll then try again... :(
      Sleep(5);
      FDB.Open;
    end;
  end;  
end;

procedure TdtDBAccess.RestoreDB(const ABackupFile: String);
var
  Cmd: String;
  StartupInfo: TStartupInfo;
  ProcessInfo: TProcessInformation;
  SA: TSecurityAttributes;
  errorPipeRead, errorPipeWrite: THandle;
  exitCode: Cardinal;
  pError: array[0..1024] of char;
  errorDesc: string;
  bytesRead: DWord;
begin
  // Run GBAK.exe
  Cmd := GetIBPath + 'gbak.exe';
  CheckCondition(FileExists(Cmd), 'gbak.exe utility is not found');
  Cmd := '"' + Cmd + '" -c -rep -p 8192 -user ' + FUser + ' -password ' + FPassword +
    ' "'+ ABackupFile + '" "' + FDBPatch + ChangeFileExt(ExtractFileName(ABackupFile), '.gdb') + '"';

  ZeroMemory(@SA, SizeOf(SA));
  SA.nLength := SizeOf(SA);
  SA.lpSecurityDescriptor := nil;
  SA.bInheritHandle := True;

  ZeroMemory(@StartupInfo, SizeOf(StartupInfo));
  StartupInfo.cb := SizeOf(TStartupInfo);
  StartupInfo.lpDesktop := nil;
  StartupInfo.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
  StartupInfo.wshowWindow := SW_HIDE;

  Win32Check(CreatePipe(errorPipeRead, errorPipeWrite, @SA, 0));
  try
    try
      StartupInfo.hStdError := errorPipeWrite;
      Win32Check(CreateProcess(nil, PChar(Cmd), nil, @SA, True, NORMAL_PRIORITY_CLASS , nil, nil, StartupInfo, ProcessInfo));

      while WaitForSingleObject(ProcessInfo.hProcess, 1000) = WAIT_TIMEOUT do
      begin
        if CurrentThreadTaskTerminated then
        begin
          TerminateProcess(ProcessInfo.hProcess, 0);
          break;
        end;
      end;

      GetExitCodeProcess(ProcessInfo.hProcess, exitCode);
    finally
      CloseHandle(ProcessInfo.hProcess);
      CloseHandle(ProcessInfo.hThread);
      CloseHandle(errorPipeWrite);
    end;

    bytesRead := 0;
    errorDesc := '';
    if exitCode <> 0 then
    begin
      FillChar(pError, SizeOf(pError), 0);
      while ReadFile(errorPipeRead, pError, 1024, bytesRead, nil) and (bytesRead > 0) do
      begin
        errorDesc := errorDesc + string(pError);
        FillChar(pError, SizeOf(pError), 0);
      end;

      raise Exception.Create(errorDesc);
    end;

  finally
    CloseHandle(errorPipeRead);
  end;
end;

procedure TdtDBAccess.RollbackTransaction;
begin
  FTransaction.Rollback;
end;

procedure TdtDBAccess.SetTransParameters(const AReadOnly: Boolean);
begin
  FTransaction.Params.Clear;

  if AReadOnly then
  begin
    FTransaction.Params.Add('isc_tpb_read_committed');
    FTransaction.Params.Add('isc_tpb_rec_version');
    FTransaction.Params.Add('isc_tpb_wait');
    FTransaction.Params.Add('isc_tpb_read');
  end
  else
  begin
    FTransaction.Params.Add('isc_tpb_read_committed');
    FTransaction.Params.Add('isc_tpb_no_rec_version');
    FTransaction.Params.Add('isc_tpb_wait');
    FTransaction.Params.Add('isc_tpb_write');
  end;
end;

procedure TdtDBAccess.StartTransaction(const AReadOnly: Boolean);
begin
  SetTransParameters(AReadOnly);
  FTransaction.StartTransaction;
end;

{ TdtIBSQL }

function TdtIBSQL.AsDataSet: IDataSet;
var
  i: Integer;
begin
  try
    Result := TisDataSet.Create(GetFieldDefs);
    while not Eof do
    begin
      if CurrentThreadTaskTerminated then
        AbortEx;
      Result.Append;
      for i := 0 to FieldCount - 1 do
        if Fields[i].IsNull then
          Result.Fields[i].Clear
        else if Fields[i].SqlVar.SqlDef = SQL_BLOB then
          Result.Fields[i].AsString := Fields[i].AsString
        else
          Result.Fields[i].Value := Fields[i].Value;

      Result.Post;
      Next;
    end;
  finally
    Close;
  end;
  
  Result.First;
end;

destructor TdtIBSQL.Destroy;
begin
  FreeAndNil(FFieldDefs);
  inherited;
end;

function TdtIBSQL.GetFieldDefs: TFieldDefs;
var
  FieldType: TFieldType;
  FieldSize: Word;
  i: Integer;
  Fld: TFieldDef;
begin
  if not Assigned(FFieldDefs) then
    FFieldDefs := TdtFieldDefs.Create(nil);

  FFieldDefs.Clear;
  for i := 0 to FieldCount - 1 do
  begin
    FieldSize := 0;

    case Fields[i].Data.SqlDef of
      SQL_VARYING,
      SQL_TEXT:
        begin
          FieldSize := Fields[i].Data.sqllen;
          FieldType := ftString;
        end;

      SQL_INT64,
      SQL_FLOAT,
      SQL_DOUBLE:
        FieldType := ftFloat;

      SQL_LONG,
      SQL_SHORT:
        FieldType := ftInteger;

      SQL_TIMESTAMP:
        FieldType := ftDateTime;

      SQL_TYPE_TIME:
        FieldType := ftTime;

      SQL_TYPE_DATE:
        FieldType := ftDate;

      SQL_BLOB:
        begin
          FieldSize := sizeof (TISC_QUAD);
            if (Fields[i].Data.sqlsubtype = 1) then
              FieldType := ftmemo
            else
              FieldType := ftBlob;
        end;

      SQL_BOOLEAN:
        FieldType := ftBoolean;
    else
      FieldType := ftUnknown;
      Assert(False);
    end;

    Fld := TFieldDef.Create(nil);
    Fld.Name := Fields[i].Name;
    Fld.DataType := FieldType;
    Fld.Size := FieldSize;
    Fld.Collection := FFieldDefs;
  end;

  Result := FFieldDefs;
end;

procedure TdtIBSQL.SetParams(const AParams: IisListOfValues;
  const AClearUndefined: Boolean);
var
  Param: IisNamedValue;
  i: Integer;
  V: Variant;
begin
  for i := 0 to Params.Count-1 do
  begin
    if Assigned(AParams) then
      Param := AParams.FindValue(Params[i].Name)
    else
      Param := nil;

    if Assigned(Param) then
    begin
      V := Param.Value;
      if VarType(V) = varString then
        if AnsiSameText(Param.Value, 'now') then
          V := Now
        else if AnsiSameText(V, 'today') then
          V := Date;

      if not VarIsNull(V) and
         ((Params[i].SQLType = SQL_TIMESTAMP) or (Params[i].SQLType = SQL_TYPE_DATE) or (Params[i].SQLType = SQL_TYPE_TIME)) then
        Params[i].Value := VarAsType(V, varDate)
      else
        Params[i].Value := V;
    end

    else if AClearUndefined then
      Params[i].Clear;
  end;
end;

{ TdtFieldDefs }

procedure TdtFieldDefs.Add(const Name: string; DataType: TFieldType; Size: Integer);
var
  Fld: TFieldDef;
begin
  Fld := TFieldDef.Create(nil);
  Fld.Name := Name;
  Fld.DataType := DataType;
  Fld.Size := Size;
  Fld.Collection := Self;
end;

procedure TdtFieldDefs.BeginUpdate;
begin
end;

procedure TdtFieldDefs.EndUpdate;
begin
end;

end.
