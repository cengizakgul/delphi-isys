unit dtFullTest;

interface

uses Windows, SysUtils, isBaseClasses, dtTest, dtConnectionTest, dtStoredProcTest, isBasicUtils,
     EvStreamUtils, dtDataLoadTest, dtSettings;

type

  IdtFullTest = interface(IdtTest)
  ['{05446325-6FB6-4C2B-AAFD-34931D2EB39A}']
  end;

  TdtFullTest = class(TdtTest, IdtFullTest)
  private
    FConnectionTest: IdtTest;
    FMultiConnectionTest: IdtTest;
    FDBServerCPUTest: IdtTest;
    FDataLoadTest: IdtTest;
    FRestoreDBTime: Cardinal;
    procedure PrepareTestDB;
  protected
    procedure DoOnConstruction; override;
    procedure DoBeforeRun; override;
    procedure DoTest; override;
    procedure DoAfterRun; override;
    procedure ProcessResults(const AResults: IisListOfValues); override;
  end;

{$R dtTestDB.RES}

implementation

{ TdtFullTest }

procedure TdtFullTest.DoAfterRun;
begin
  inherited;
  DBAccess.DropDB('ISTEST.gdb');
end;

procedure TdtFullTest.DoBeforeRun;
begin
  inherited;
  FRestoreDBTime := GetTickCount;
  PrepareTestDB;
  FRestoreDBTime := GetTickCount - FRestoreDBTime;
end;

procedure TdtFullTest.DoOnConstruction;
begin
  inherited;
  FConnectionTest := TdtConnectionTest.Create;

  FMultiConnectionTest := TdtMultiConnectionTest.Create;

  FDBServerCPUTest := TdtMultiStoredProcTest.Create;
  FDBServerCPUTest.Params.Value['Threads'] := gvServerCPUCount;

  FDataLoadTest := TdtDataLoadTest.Create;

  Params.AddValue('SingleConnectionTest', True);
  Params.AddValue('MultipleConnectionTest', True);
  Params.AddValue('CPUPerformanceTest', True);
  Params.AddValue('DataLoadTest', True);
end;

procedure TdtFullTest.DoTest;
begin
  inherited;

  if Params.Value['SingleConnectionTest'] then
    FConnectionTest.Run;

  if Params.Value['MultipleConnectionTest'] then
    FMultiConnectionTest.Run;

  if Params.Value['CPUPerformanceTest'] then
    FDBServerCPUTest.Run;

  if Params.Value['DataLoadTest'] then
    FDataLoadTest.Run;
end;

procedure TdtFullTest.PrepareTestDB;
var
  BackupFile: IevDualStream;
  HR: HRSRC;
  SR: Cardinal;
  PR: Pointer;
  S, sFile: String;
begin
  HR := FindResource(HINSTANCE, 'test_db', 'DATA');
  if HR <> 0 then
  begin
    SR := SizeofResource(HINSTANCE, HR);
    if SR > 0 then
    begin
      PR := Pointer(LoadResource(HINSTANCE, HR));
      SetLength(S, SR);
      CopyMemory(@S[1], PR, SR);
      sFile := AppDir + 'ISTEST.gbk';
      BackupFile := TevDualStreamHolder.CreateFromNewFile(sFile);
      BackupFile.WriteBuffer(S[1], SR);
      BackupFile := nil;

      try
        DBAccess.RestoreDB(sFile);
      finally
        DeleteFile(sFile);
      end;  
    end;
  end;
end;

procedure TdtFullTest.ProcessResults(const AResults: IisListOfValues);
begin
  inherited;

  AResults.AddValue('Restore DB Time', FRestoreDBTime);

  if Params.Value['SingleConnectionTest'] then
    AResults.AddValue('Single Connection Test', FConnectionTest.Results);

  if Params.Value['MultipleConnectionTest'] then
    AResults.AddValue('Multi Connection Test', FMultiConnectionTest.Results);

  if Params.Value['CPUPerformanceTest'] then
    AResults.AddValue('Server CPU Test', FDBServerCPUTest.Results);

  if Params.Value['DataLoadTest'] then
    AResults.AddValue('Data Load Test', FDataLoadTest.Results);
end;

end.
