unit dtTest;

interface

uses SysUtils, Windows, isBaseClasses, dtDBAccess, dtSettings;

type
  IdtTest = interface
  ['{B8A429D5-1FA2-43DE-B28E-354073E48B5A}']
    function  Params: IisListOfValues;
    procedure Run;
    function  Results: IisListOfValues;
  end;


  TdtTest = class(TisInterfacedObject, IdtTest)
  private
    FDBAccess: IdtDBAccess;
    FTestTime: Cardinal;
    FStartTime: Cardinal;
    FParams: IisListOfValues;
    FResults: IisListOfValues;
  protected
    procedure DoOnConstruction; override;
    function  DBAccess: IdtDBAccess;
    procedure StartTickTime;
    procedure EndTickTime;
    function  TestTime: Cardinal;
    procedure DoBeforeRun; virtual;
    procedure DoTest; virtual; abstract;
    procedure DoAfterRun; virtual;
    procedure ProcessResults(const AResults: IisListOfValues); virtual;

    function  Params: IisListOfValues;
    procedure Run;
    function  Results: IisListOfValues;
  end;

implementation

{ TdtTest }

function TdtTest.DBAccess: IdtDBAccess;
begin
  Result := FDBAccess;
end;

procedure TdtTest.DoAfterRun;
begin
end;

procedure TdtTest.DoBeforeRun;
begin
end;

procedure TdtTest.DoOnConstruction;
begin
  inherited;
  FDBAccess := TdtDBAccess.Create(gvDBPath, gvDBUser, gvDBPassword);
  FParams := TisListOfValues.Create;
end;

procedure TdtTest.EndTickTime;
begin
  if FStartTime > 0 then
  begin
    Inc(FTestTime, GetTickCount - FStartTime);
    FStartTime := 0;
  end;
end;

function TdtTest.Params: IisListOfValues;
begin
  Result := FParams;
end;

procedure TdtTest.ProcessResults(const AResults: IisListOfValues);
begin
  AResults.AddValue('TestTime', TestTime);
end;

function TdtTest.Results: IisListOfValues;
begin
  Result := FResults;
end;

procedure TdtTest.Run;
begin
  FTestTime := 0;
  FResults := TisListOfValues.Create;

  try
    DoBeforeRun;
    StartTickTime;
    try
      DoTest;
    finally
      EndTickTime;
    end;
    DoAfterRun;
  except
    on E: Exception do
      FResults.AddValue('Error', E.Message);
  end;

  ProcessResults(FResults);
end;

procedure TdtTest.StartTickTime;
begin
  FStartTime := GetTickCount;
end;

function TdtTest.TestTime: Cardinal;
begin
  Result := FTestTime;
end;

end.
