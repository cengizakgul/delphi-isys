unit dtDataLoadTest;

interface

uses Windows, SysUtils, isBaseClasses, dtTest, isThreadManager, isBasicUtils;

type
  TdtDataLoadTest = class(TdtTest)
  private
  protected
    procedure DoOnConstruction; override;
    procedure DoBeforeRun; override;
    procedure DoTest; override;
    procedure DoAfterRun; override;
    procedure ProcessResults(const AResults: IisListOfValues); override;
  end;


implementation

{ TdtDataLoadTest }

procedure TdtDataLoadTest.DoAfterRun;
begin
  inherited;
  DBAccess.CloseDB;
end;

procedure TdtDataLoadTest.DoBeforeRun;
begin
  inherited;
  if not DBAccess.Active then
    DBAccess.OpenDB('ISTEST.gdb');
end;

procedure TdtDataLoadTest.DoOnConstruction;
begin
  inherited;
end;

procedure TdtDataLoadTest.DoTest;
var
  i, j: Integer;
  Par: IisListOfValues;
  Key: Int64;
begin
  inherited;

  Par := TisListOfValues.Create;
  Par.AddValue('T1_NBR', 0);
  Par.AddValue('F_VARCHAR', '');
  Par.AddValue('F_DATE', Now);
  Par.AddValue('F_BLOB', '');
  Par.AddValue('F_NUMERIC', 0);
  Par.AddValue('F_DPRECISION', 0);

  Key := 0;
  for i := 1 to 100 do
  begin
    DBAccess.StartTransaction(False);
    try
      for j := 1 to 500 do
      begin
        Inc(Key);
        Par.Value['T1_NBR'] := Key;
        DBAccess.ExecQuery('INSERT INTO T1 (T1_NBR, F_VARCHAR, F_DATE, F_BLOB, F_NUMERIC, F_DPRECISION) VALUES(:T1_NBR, :F_VARCHAR, :F_DATE, :F_BLOB, :F_NUMERIC, :F_DPRECISION)', Par);
      end;
    except
      DBAccess.RollbackTransaction;
      raise;
    end;
  end;
end;

procedure TdtDataLoadTest.ProcessResults(const AResults: IisListOfValues);
begin
  inherited;

end;

end.
