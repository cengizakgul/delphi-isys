unit dtConnectionTest;

interface

uses Windows, isBaseClasses, dtTest, isThreadManager, isBasicUtils;

type
  TdtConnectionTest = class(TdtTest)
  private
    FCount: Cardinal;
    FError: String;
    procedure DoInThread(const Params: TTaskParamList);
  protected
    procedure DoOnConstruction; override;
    procedure DoTest; override;
    procedure ProcessResults(const AResults: IisListOfValues); override;
  end;


  TdtMultiConnectionTest = class(TdtTest)
  private
    FTestList: IisList;
    procedure DoInThread(const Params: TTaskParamList);
  protected
    procedure DoOnConstruction; override;
    procedure DoTest; override;
    procedure ProcessResults(const AResults: IisListOfValues); override;
  end;


implementation

uses SysUtils;


{ TdtConnectionTest }

procedure TdtConnectionTest.DoInThread(const Params: TTaskParamList);
begin
  StartTickTime;
  try
    try
      while not CurrentThreadTaskTerminated do
      begin
        DBAccess.OpenDB('ISTEST.gdb');
        DBAccess.CloseDB;
        Inc(FCount);
      end;
    except
      on E: Exception do
        FError := E.Message;
    end;
  finally
    EndTickTime;
  end;
end;

procedure TdtConnectionTest.DoOnConstruction;
begin
  inherited;
  Params.AddValue('TestTime', 10000);
end;

procedure TdtConnectionTest.DoTest;
var
  TaskID: TTaskID;
begin
  FCount := 0;
  FError := '';
  TaskID := GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([]), ClassName);
  Snooze(Params.Value['TestTime']);
  GlobalThreadManager.TerminateTask(TaskID);
  GlobalThreadManager.WaitForTaskEnd(TaskID);
end;

procedure TdtConnectionTest.ProcessResults(const AResults: IisListOfValues);
begin
  inherited;
  AResults.AddValue('Index', Round(FCount * (Params.Value['TestTime'] / AResults.Value['TestTime'])));
  if FError <> '' then
    AResults.AddValue('Error', FError);
end;


{ TdtMultiConnectionTest }

procedure TdtMultiConnectionTest.DoInThread(const Params: TTaskParamList);
begin
 (FTestList[Params[0]] as IdtTest).Run; 
end;

procedure TdtMultiConnectionTest.DoOnConstruction;
begin
  inherited;
  FTestList := TisList.Create;
  Params.AddValue('TestTime', 10000);
  Params.AddValue('Threads', 20);  
end;

procedure TdtMultiConnectionTest.DoTest;
var
  Tasks: array of TTaskID;
  i: Integer;
  T: IdtTest;
begin
  FTestList.Clear;
  SetLength(Tasks, Integer(Params.Value['Threads']));

  for i := Low(Tasks) to High(Tasks) do
  begin
    T := TdtConnectionTest.Create;
    T.Params.Value['TestTime'] := Params.Value['TestTime'];
    FTestList.Add(T);
  end;

  for i := Low(Tasks) to High(Tasks) do
    Tasks[i] := GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([i]), ClassName);

  for i := Low(Tasks) to High(Tasks) do
    GlobalThreadManager.WaitForTaskEnd(Tasks[i]);
end;

procedure TdtMultiConnectionTest.ProcessResults(const AResults: IisListOfValues);
var
  i: Integer;
  CountSum: Cardinal;
  Errors: String;
  T: IdtTest;
begin
  inherited;

  Errors := '';
  CountSum := 0;
  for i := 0 to FTestList.Count - 1 do
  begin
    T := FTestList[i] as IdtTest;
    CountSum := CountSum + Cardinal(T.Results.Value['Index']);
    if T.Results.ValueExists('Error') then
      AddStrValue(Errors, T.Results.Value['Error'], #13);
  end;

  AResults.AddValue('Index', Round(CountSum * (Params.Value['TestTime'] / AResults.Value['TestTime'])));

  if Errors <> '' then
    AResults.AddValue('Error', Errors);
end;

end.
