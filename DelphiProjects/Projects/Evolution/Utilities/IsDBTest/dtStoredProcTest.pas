unit dtStoredProcTest;

interface

uses Windows, isBaseClasses, dtTest, isThreadManager, isBasicUtils, isDataSet;

type
  TdtStoredProcTest = class(TdtTest)
  private
    FIndex: Integer;
  protected
    procedure DoOnConstruction; override;
    procedure DoBeforeRun; override;
    procedure DoTest; override;
    procedure DoAfterRun; override;
    procedure ProcessResults(const AResults: IisListOfValues); override;
  end;


  TdtMultiStoredProcTest = class(TdtTest)
  private
    FTestList: IisList;
    FStartEvent: THandle;
    procedure DoInThread(const Params: TTaskParamList);
  protected
    procedure DoOnConstruction; override;
    procedure DoTest; override;
    procedure ProcessResults(const AResults: IisListOfValues); override;
  public
    destructor Destroy; override;  
  end;


implementation

{ TdtStoredProcTest }

procedure TdtStoredProcTest.DoAfterRun;
begin
  inherited;
  DBAccess.CloseDB;
end;

procedure TdtStoredProcTest.DoBeforeRun;
begin
  inherited;
  if not DBAccess.Active then
    DBAccess.OpenDB('ISTEST.gdb');
end;

procedure TdtStoredProcTest.DoOnConstruction;
begin
  inherited;
  Params.AddValue('TestTime', 10);
end;

procedure TdtStoredProcTest.DoTest;
var
  DS: IDataSet;
begin
  inherited;
  DBAccess.StartTransaction(True);
  try
    DS := DBAccess.ExecQuery('SELECT * FROM P_CPU_TEST(:TestTime)', Params);
    FIndex := DS.FieldByName('PERF_COUNT').AsInteger;
  finally
    DBAccess.RollbackTransaction;
  end;
end;

procedure TdtStoredProcTest.ProcessResults(
  const AResults: IisListOfValues);
begin
  inherited;
  AResults.AddValue('Index', FIndex);
end;

{ TdtMultiStoredProcTest }

destructor TdtMultiStoredProcTest.Destroy;
begin
  CloseHandle(FStartEvent);
  inherited;
end;

procedure TdtMultiStoredProcTest.DoInThread(const Params: TTaskParamList);
begin
  WaitForSingleObject(FStartEvent, INFINITE);
  (FTestList[Params[0]] as IdtTest).Run;
end;

procedure TdtMultiStoredProcTest.DoOnConstruction;
begin
  inherited;
  FTestList := TisList.Create;
  FStartEvent := CreateEvent(nil, True, False, nil);

  Params.AddValue('TestTime', 10);
  Params.AddValue('Threads', 8);
end;

procedure TdtMultiStoredProcTest.DoTest;
var
  Tasks: array of TTaskID;
  i: Integer;
  T: IdtTest;
begin
  FTestList.Clear;
  SetLength(Tasks, Integer(Params.Value['Threads']));

  for i := Low(Tasks) to High(Tasks) do
  begin
    T := TdtStoredProcTest.Create;
    T.Params.Value['TestTime'] := Params.Value['TestTime'];
    FTestList.Add(T);

    TdtStoredProcTest((T as IisInterfacedObject).GetImplementation).DBAccess.OpenDB('ISTEST.gdb');
  end;

  for i := Low(Tasks) to High(Tasks) do
    Tasks[i] := GlobalThreadManager.RunTask(DoInThread, Self, MakeTaskParams([i]), ClassName);

  Sleep(100);  
  SetEvent(FStartEvent);

  for i := Low(Tasks) to High(Tasks) do
    GlobalThreadManager.WaitForTaskEnd(Tasks[i]);
end;

procedure TdtMultiStoredProcTest.ProcessResults(
  const AResults: IisListOfValues);
var
  i: Integer;
  CountSum: Cardinal;
  Errors: String;
  T: IdtTest;
  MaxTime: Integer;
begin
  inherited;

  Errors := '';
  CountSum := 0;
  MaxTime := 0;
  for i := 0 to FTestList.Count - 1 do
  begin
    T := FTestList[i] as IdtTest;
    CountSum := CountSum + Cardinal(T.Results.Value['Index']);
    if T.Results.ValueExists('Error') then
      AddStrValue(Errors, T.Results.Value['Error'], #13);

    if Integer(T.Results.Value['TestTime']) > MaxTime then
      MaxTime := T.Results.Value['TestTime'];
  end;

  AResults.AddValue('Index', Round(CountSum * (Params.Value['TestTime'] * 1000 / MaxTime)));

  if Errors <> '' then
    AResults.AddValue('Error', Errors);
end;

end.
