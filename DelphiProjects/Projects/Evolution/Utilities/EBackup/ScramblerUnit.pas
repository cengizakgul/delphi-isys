unit ScramblerUnit;

interface

uses DB, SysUtils, Controls, Classes, IBDatabase, IBQuery, SSNScramblerUnit, Variants, Windows;

const
  minScrabledNbr = 6000000;

  procedure Scramble(ADatabasePath, ATempDatabasePath, AUserName, APassword : String; ANewClientNumber : integer);
  function GetClNbr(AName : String) : integer;
  function CheckName(AName : String) : boolean;

implementation

type
  TMyIBDatabase = class (TIBDatabase)
  private
    FUserName: String;
    FPassword: String;
    procedure SetPassword(const Value: String);
    procedure SetUserName(const Value: String);
  public
    procedure IBDatabaseLogin(Database: TIBDatabase; LoginParams: TStrings);
    property UserName : String read FUserName write SetUserName;
    property Password : String read FPassword write SetPassword;
  end;

  procedure Scramble(ADatabasePath, ATempDatabasePath, AUserName, APassword : String; ANewClientNumber : integer);
  var
    DB : TMyIBDatabase;
    Tr : TIBTransaction;
    UpdateQuery, SelectQuery : TIBQuery;
    TakenList : TStringList;
    SSNList : ISSNList;
    BankAccountNbr : integer;
  begin
    SSNList := TSSNList.Create;
    DB := TMyIBDatabase.Create(nil);
    try
      DB.UserName := AUserName;
      DB.Password := APassword;
      Db.OnLogin := DB.IBDatabaseLogin;

      // reading list of taken numbers
      DB.Connected := false;
      DB.DatabaseName := ATempDatabasePath + 'TMP_TBLS.gdb';
      DB.Connected := true;
      Tr := TIBTransaction.Create(nil);
      try
        Tr.Params.Clear;
        Tr.Params.Add('read');
        Tr.Params.Add('read_committed');
        Tr.Params.Add('no_rec_version');
        Tr.Params.Add('nowait');
        Tr.AllowAutoStart := false;
        Tr.DefaultDatabase := DB;
        SelectQuery := TIBQuery.Create(nil);
        try
          SelectQuery.Database := Db;
          SelectQuery.Transaction := Tr;
          Tr.StartTransaction;
          try
            SelectQuery.Close;
            SelectQuery.SQL.Clear;
            SelectQuery.SQL.Text := 'select distinct social_security_number from TMP_CL_PERSON where StrCopy(social_security_number,0,3) between ''730'' and ''999''';
            SelectQuery.Open;
            TakenList := TSTringList.Create;
            try
              while not SelectQuery.eof do
              begin
                if SelectQuery['social_security_number'] <> null then
                  TakenList.Add(SelectQuery['social_security_number']);
                SelectQuery.Next;
              end;
              SelectQuery.Close;
              SSNList.FillFromList(TakenList);
            finally
              TakenList.Free;
            end;
          finally
            Tr.Rollback;
          end;
        finally
          SelectQuery.Free;
        end;
      finally
        Tr.Free;
      end;

      // updating client number and scrambling
      DB.Connected := false;
      DB.DatabaseName := ADatabasePath;
      DB.Connected := true;

      Tr := TIBTransaction.Create(nil);
      try
        Tr.Params.Clear;
        Tr.Params.Add('write');
        Tr.Params.Add('read_committed');
        Tr.Params.Add('rec_version');
        Tr.Params.Add('nowait');
        Tr.AllowAutoStart := false;
        Tr.DefaultDatabase := DB;

        UpdateQuery := TIBQuery.Create(nil);
        SelectQuery := TIBQuery.Create(nil);
        try
          UpdateQuery.Database := Db;
          UpdateQuery.Transaction := Tr;
          SelectQuery.Database := Db;
          SelectQuery.Transaction := Tr;

          Tr.StartTransaction;
          try
            SSNList.GetFirst;
            // updating client number
            UpdateQuery.Close; UpdateQuery.SQL.Clear;
            UpdateQuery.SQL.Text := 'update cl set name=StrCopy(''SCRAM ''||name, 0, 40), custom_client_number=custom_client_number||''S'',CL_NBR='+IntToStr(ANewClientNumber);
            UpdateQuery.ExecSQL;

            UpdateQuery.Close; UpdateQuery.SQL.Clear;
            UpdateQuery.SQL.Text := 'update co set custom_company_number=StrCopy(custom_company_number,0,19)||''S'',name=StrCopy(''SCRAM ''||name, 0, 40)';
            UpdateQuery.ExecSQL;

            // scrambling SSNs
            UpdateQuery.Close; UpdateQuery.SQL.Clear;
            UpdateQuery.SQL.Text := 'update cl_person set social_security_number=:p1 where social_security_number=:p2';
            SelectQuery.Close; SelectQuery.SQL.Clear;
            SelectQuery.SQL.Text := 'select distinct social_security_number from cl_person';
            SelectQuery.Open;
            while (not SelectQuery.eof) do
            begin
              if SelectQuery['social_security_number'] <> null then
              begin
                UpdateQuery.Close;
                UpdateQuery.ParamByName('P1').AsString := SSNList.GetNext;
                UpdateQuery.ParamByName('P2').AsString := SelectQuery['social_security_number'];
                UpdateQuery.ExecSQL;
              end;
              SelectQuery.Next;
            end;

            // scrambling bank account ee_bank_account_number field of EE_DIRECT_DEPOSIT
            BankAccountNbr := 10000000;
            UpdateQuery.Close; UpdateQuery.SQL.Clear;
            UpdateQuery.SQL.Text := 'update ee_direct_deposit set ee_bank_account_number=:p1 where ee_bank_account_number=:p2';
            SelectQuery.Close; SelectQuery.SQL.Clear;
            SelectQuery.SQL.Text := 'select distinct ee_bank_account_number from ee_direct_deposit';
            SelectQuery.Open;
            while (not SelectQuery.eof) do
            begin
              if SelectQuery['ee_bank_account_number'] <> null then
              begin
                UpdateQuery.Close;
                UpdateQuery.ParamByName('P1').AsString := IntToStr(BankAccountNbr);
                UpdateQuery.ParamByName('P2').AsString := SelectQuery['ee_bank_account_number'];
                UpdateQuery.ExecSQL;
                Inc(BankAccountNbr);
              end;
              SelectQuery.Next;
            end;

            // scrambling bank account custom_bank_account_number field of cl_bank_account
            UpdateQuery.Close; UpdateQuery.SQL.Clear;
            UpdateQuery.SQL.Text := 'update cl_bank_account set custom_bank_account_number=:p1 where custom_bank_account_number=:p2';
            SelectQuery.Close; SelectQuery.SQL.Clear;
            SelectQuery.SQL.Text := 'select distinct custom_bank_account_number from cl_bank_account';
            SelectQuery.Open;
            while (not SelectQuery.eof) do
            begin
              if SelectQuery['custom_bank_account_number'] <> null then
              begin
                UpdateQuery.Close;
                UpdateQuery.ParamByName('P1').AsString := IntToStr(BankAccountNbr);
                UpdateQuery.ParamByName('P2').AsString := SelectQuery['custom_bank_account_number'];
                UpdateQuery.ExecSQL;
                Inc(BankAccountNbr);
              end;
              SelectQuery.Next;
            end;

            UpdateQuery.Close; SelectQuery.Close;
            Tr.Commit;
          except
            on E:Exception do
            begin
              Tr.Rollback;
              UpdateQuery.Close;
              SelectQuery.Close;
              raise;
            end;
          end;

        finally
          SelectQuery.Free;
          UpdateQuery.Free;
        end;
      finally
        Tr.Free;
      end;
    finally
      try
        if DB.Connected then
          DB.Connected := false;
      except
      end;
      DB.Free;
    end;
  end;

  function CheckName(AName : String) : boolean;
  var
    tmp : String;
  begin
    result := false;
    tmp := UpperCase(Trim(AName));
    if Copy(tmp, 1, 3) <> 'CL_' then exit;
    result := true;
  end;

  function GetClNbr(AName : String) : integer;
  begin
    result := StrToInt(Copy(AName, 4, Pos('.', AName) - 4));
  end;

{ MyIBDatabase }

procedure TMyIBDatabase.SetPassword(const Value: String);
begin
  FPassword := Value;
end;

procedure TMyIBDatabase.SetUserName(const Value: String);
begin
  FUserName := Value;
end;

procedure TMyIBDatabase.IBDatabaseLogin(Database: TIBDatabase; LoginParams: TStrings);
begin
   // add username and password
   LoginParams.Clear;
   LoginParams.Add('user_name='+FUsername);
   LoginParams.Add('password='+FPassword);
end;

end.
