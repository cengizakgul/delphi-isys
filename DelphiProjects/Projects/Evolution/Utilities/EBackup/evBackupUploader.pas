unit evBackupUploader;

interface

uses Classes, IsFtp, SysUtils;

type
  IevBackupUploader = interface
  ['{BF71ECEC-9F73-4FC2-850A-6C01704EEAF3}']
    function  GetHost: String;
    procedure SetHost(const AValue: String);
    function  GetPort: Integer;
    procedure SetPort(const AValue: Integer);
    function  GetUser: String;
    procedure SetUser(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    procedure Connect;
    procedure Disconnect;
    procedure ReadDirectory;
    function  GetDirectoryListing: TStringList;
    procedure Delete(const AFileName: String);
    procedure Put(const AFileName: String); overload;
//    procedure Get(const ANewFile, ARemoteFile: String; AResume: Boolean = False); overload;
  end;

type

  TevFtpBackupUploader = class(TisFtp, IevBackupUploader)
  protected
    function  GetHost: String;
    procedure SetHost(const AValue: String);
    function  GetPort: Integer;
    procedure SetPort(const AValue: Integer);
    function  GetUser: String;
    procedure SetUser(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    function  GetDirectoryListing: TStringList;
  end;

implementation

{ TevFtpBackupUploader }

function TevFtpBackupUploader.GetDirectoryListing: TStringList;
begin
  Result := DirectoryListing;
end;

function TevFtpBackupUploader.GetHost: String;
begin
  Result := Host;
end;

function TevFtpBackupUploader.GetPassword: String;
begin
  Result := Password;
end;

function TevFtpBackupUploader.GetPort: Integer;
begin
  Result := Port;
end;

function TevFtpBackupUploader.GetUser: String;
begin
  Result := User;
end;

procedure TevFtpBackupUploader.SetHost(const AValue: String);
begin
  Host := AValue;
end;

procedure TevFtpBackupUploader.SetPassword(const AValue: String);
begin
  Password := AValue;
end;

procedure TevFtpBackupUploader.SetPort(const AValue: Integer);
begin
  Port := AValue;
end;

procedure TevFtpBackupUploader.SetUser(const AValue: String);
begin
  User := AValue;
end;

end.
