// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, EvTypes,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, DBTables, Db, RegistryFunctions,
  IsFTP, IsSMTPClient, isUtils, SBSimpleSftp, SBSftpCommon, evBackupUploader,
  evSFtpBackupUploader, EvExceptions;

const
  WM_THREAD_EXCEPTION = WM_USER+ $1000;

type
  TBackup = class(TForm)
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Panel2: TPanel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    Edit2: TEdit;
    Panel3: TPanel;
    Label3: TLabel;
    SpeedButton2: TSpeedButton;
    Edit3: TEdit;
    Panel4: TPanel;
    DoBackupNowButton: TButton;
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    SmtpHostEdit: TEdit;
    Label5: TLabel;
    SmtpUserEdit: TEdit;
    Label6: TLabel;
    EMailToEdit: TEdit;
    TestEmailButton: TButton;
    Label7: TLabel;
    FtpHostEdit: TEdit;
    Label8: TLabel;
    FtpLoginEdit: TEdit;
    Label9: TLabel;
    FtpPasswordEdit: TEdit;
    TestFtpButton: TButton;
    CheckBox1: TCheckBox;
    Label10: TLabel;
    Edit10: TEdit;
    UpDown1: TUpDown;
    ProgressBar1: TProgressBar;
    Panel5: TPanel;
    Label11: TLabel;
    Edit11: TEdit;
    Panel6: TPanel;
    Label12: TLabel;
    Edit12: TEdit;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    BackupToFtpZippedChkBox: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    ComboBox1: TComboBox;
    Label13: TLabel;
    SpeedButton3: TSpeedButton;
    Label14: TLabel;
    Edit13: TEdit;
    CheckBox9: TCheckBox;
    Label15: TLabel;
    EMailFromEdit: TEdit;
    SecureChkBox: TCheckBox;
    FtpPortEdit: TEdit;
    Label116: TLabel;
    SFtpPortEdit: TEdit;
    Label17: TLabel;
    ScrambleCheck: TCheckBox;
    Label16: TLabel;
    TmpDbPath: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure DoBackupNowButtonClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure TestEmailButtonClick(Sender: TObject);
    procedure TestFtpButtonClick(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton3Click(Sender: TObject);
    procedure ScrambleCheckClick(Sender: TObject);
  private
    { Private declarations }
    FServer, FSource, FDestination, FSMTPServer, FSMTPUserID, FEmailFrom, FEmailTo, FFTPHostName, FFTPLoginName, FFTPPassword, FFTPGlobalDBs, FFTPSecured,
    FFtpPort, FSFtpPort, FTMPDatabasePath: String;
    FISystems : boolean;
    procedure LaunchViewer;
  public
    { Public declarations }
    procedure HANDLE_WM_THREAD_EXCEPTION(var M: TMessage); message WM_THREAD_EXCEPTION;
  end;

var
  Backup: TBackup;

implementation

uses
  Unit2, IniFiles, Unit3, ScramblerUnit, IsBasicUtils;

{$R *.DFM}

procedure TBackup.FormCreate(Sender: TObject);
begin
  ReadRegistry(FServer, FSource, FDestination, FSMTPServer, FSMTPUserID,
               FEmailFrom, FEmailTo, FFTPHostName, FFTPLoginName, FFTPPassword,
               FFTPGlobalDBs, FFTPSecured, FFtpPort, FSFtpPort, FTMPDatabasePath);
  FISystems := isISystemsDomain;
  if FISystems then
  begin
    ScrambleCheck.Enabled := true;
    TmpDbPath.Enabled := true;
    Label16.Enabled := true;
  end
  else
  begin
    ScrambleCheck.Enabled := false;
    TmpDbPath.Enabled := false;
    Label16.Enabled := false;
  end;
end;

procedure TBackup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  WriteToRegistry('', 'Server',FServer);
  WriteToRegistry('', 'Source',FSource);
  WriteToRegistry('', 'Destination',FDestination);
  WriteToRegistry('', 'SMTP Server',SmtpHostEdit.Text);
  WriteToRegistry('', 'SMTP User ID',SmtpUserEdit.Text);
  WriteToRegistry('', 'Email From',EMailFromEdit.Text);
  WriteToRegistry('', 'Email To',EMailToEdit.Text);
  WriteToRegistry('', 'FTP Host Name',FtpHostEdit.Text);
  WriteToRegistry('', 'FTP Login Name',FtpLoginEdit.Text);
  WriteToRegistry('', 'FTP Password',FtpPasswordEdit.Text);
  WriteToRegistry('', 'TMP database path',TmpDbPath.Text);
  if CheckBox1.Checked then
    WriteToRegistry('', 'FTP Global DBs','Y')
  else
    WriteToRegistry('', 'FTP Global DBs','N');

  if SecureChkBox.Checked then
    WriteToRegistry('', 'FTP SSH','Y')
  else
    WriteToRegistry('', 'FTP SSH','N');
  WriteToRegistry('', 'FTP port', FtpPortEdit.Text);
  WriteToRegistry('', 'SFTP port', SFtpPortEdit.Text);  
end;

procedure TBackup.FormShow(Sender: TObject);
var
  MyIniFile: TIniFile;
begin
  Edit1.Text:=FServer;
  Edit2.Text:=FSource;
  Edit3.Text:=FDestination;
  SmtpHostEdit.Text:=FSMTPServer;
  SmtpUserEdit.Text:=FSMTPUserID;
  EMailToEdit.Text:=FEmailTo;
  EMailFromEdit.Text := FEmailFrom;
  FtpHostEdit.Text:=FFTPHostName;
  FtpLoginEdit.Text:=FFTPLoginName;
  FtpPasswordEdit.Text:=FFTPPassword;
  TmpDbPath.text := FTMPDatabasePath;
  CheckBox1.Checked:=(UpperCase(FFTPGlobalDBs)='Y');
  SecureChkBox.Checked := (UpperCase(FFTPSecured) = 'Y');
  FtpPortEdit.Text := FFtpPort;
  SFtpPortEdit.Text := FSFtpPort;

  if FileExists(ExtractFilePath(Application.ExeName) + 'ftp.ini') then
  begin
    MyIniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'ftp.ini');
    try
      MyIniFile.ReadSections(ComboBox1.Items);
    finally
      MyIniFile.Free;
    end;
  end;
  RadioGroup1Click(Self);
end;

procedure TBackup.SpeedButton1Click(Sender: TObject);
begin
  OpenDialog1.InitialDir := Edit2.Text;
  if OpenDialog1.Execute then
  begin
    Edit2.Text:=ExtractFilePath(OpenDialog1.FileName);
  end;
end;

procedure TBackup.SpeedButton2Click(Sender: TObject);
begin
  OpenDialog1.InitialDir := Edit3.Text;
  if OpenDialog1.Execute then
  begin
    Edit3.Text:=ExtractFilePath(OpenDialog1.FileName);
  end;
end;

procedure TBackup.Edit1Change(Sender: TObject);
begin
  DoBackupNowButton.Enabled:=(Edit2.Text<>'') and (Edit3.Text<>'');
  FServer:=Edit1.Text;
end;

procedure TBackup.Edit2Change(Sender: TObject);
begin
  DoBackupNowButton.Enabled:=(Edit2.Text<>'') and (Edit3.Text<>'');
  FSource:=Edit2.Text;
end;

procedure TBackup.Edit3Change(Sender: TObject);
begin
  DoBackupNowButton.Enabled:=(Edit2.Text<>'') and (Edit3.Text<>'');
  FDestination:=Edit3.Text;
end;

procedure TBackup.DoBackupNowButtonClick(Sender: TObject);
var
  MyList: TStringList;
  SearchRec1: TSearchRec;
  MyResult, ErrorCount, ActualPort: Integer;
  TempStr: String;
begin

  if (CheckBox8.Checked) and ((EMailFromEdit.Text = '') or (Pos('@', EMailFromEdit.Text) = 0)) then
  begin
    isMessage('You must Enter a reply address! On the Email/FTP Config tab!');
    Exit;
  end;
  CorrectPath(FSource);
  CorrectPath(FDestination);

  if Edit11.Text = '' then
    Password := 'isys99'
  else
    Password := Edit11.Text;
  if RadioGroup1.ItemIndex = 0 then
  begin
    if FDestination[Length(FDestination)]='/' then
      raise Exception.CreateHelp('Can not backup to UNIX server.', IDH_UNIXServer);

    if isMessage('All files in destination directory will be deleted.', mtConfirmation, [mbOK, mbCancel]) <> mrOk then
      Exit;

    MyList := TStringList.Create;
    try
      if CheckBox5.Checked then
      begin
        if (Edit12.Text = '') and (Edit13.Text = '') then
        begin
          GetClientDBList(FServer, FSource, MyList);
          MyList.Add('CL_BASE.gdb');
        end
        else
        begin
          if Edit12.Text <> '' then
            GetClientNbrs(FServer, FSource, Edit12.Text, MyList);
          if Edit13.Text <> '' then
          begin
            TempStr := Edit13.Text + ',';
            while Pos(',', TempStr) <> 0 do
            begin
              MyList.Add('CL_' + Copy(TempStr, 1, Pos(',', TempStr) - 1) + '.gdb');
              Delete(TempStr, 1, Pos(',', TempStr));
            end;
          end;
        end;
      end;
      if CheckBox2.Checked then
        MyList.Add('SYSTEM.gdb');
      if CheckBox3.Checked then
        MyList.Add('S_BUREAU.gdb');
      if CheckBox4.Checked then
        MyList.Add('TMP_TBLS.gdb');
      if CheckBox9.Checked then
        MyList.Add('RESOLUTION.gdb');
      MyResult := FindFirst(FDestination + '*.*', faAnyFile, SearchRec1);
      while MyResult = 0 do
      begin
        DeleteFile(FDestination + ExtractFileName(SearchRec1.Name));
        MyResult := FindNext(SearchRec1);
      end;
      ErrorCount := BackupLoop(FServer, FSource, FDestination, StrToInt(Edit10.Text), ProgressBar1, MyList, '');
    finally
      MyList.Free;
    end;

    if BackupToFtpZippedChkBox.Checked then
    begin

      if SecureChkBox.Checked then
        ActualPort := StrToInt(SFtpPortEdit.Text)
      else
        ActualPort := StrTOInt(FtpPortEdit.Text);  

      ZipBackup(FDestination);
      PutToFTP(FDestination, FtpHostEdit.Text, FtpLoginEdit.Text,
        FtpPasswordEdit.Text, CheckBox1.Checked, SecureChkBox.Checked, ActualPort);
    end;
  end
  else
  begin
    if ScrambleCheck.Checked then
      if isMessage('You are about to scramble restored data. Is it OK? ', mtConfirmation, [mbOK, mbCancel]) <> mrOk then
        ScrambleCheck.Checked := false;

    if ScrambleCheck.Checked then
    begin
      FTMPDatabasePath := TmpDbPath.text;
      if Trim(FTMPDatabasePath) = '' then
      begin
        isMessage('You must Enter a correct path to temporary database (TMP_TBLS.GDB)!');
        Exit;
      end;
      CorrectPath(FTMPDatabasePath);
    end;

    if CheckBox7.Checked then
    begin
{      MyResult := FindFirst(FDestination+'*.*', faAnyFile, SearchRec1);
      while MyResult = 0 do
      begin
        DeleteFile(FDestination+ExtractFileName(SearchRec1.Name));
        MyResult := FindNext(SearchRec1);
      end;}
      GetFromFTP(FDestination, FtpHostEdit.Text, FtpLoginEdit.Text, FtpPasswordEdit.Text);
      UnzipBackup(FDestination);
    end;

    if FDestination[Length(FDestination)]='/' then
      raise Exception.CreateHelp('Can not restore from UNIX server.', IDH_UNIXServer);
    ErrorCount := 0;
    MyResult := FindFirst(FDestination + '*.gbk', faAnyFile, SearchRec1);

    // checking if any file *.gbk exists
    if MyResult <> 0 then begin
      raise Exception.Create('Source directory not exists or contains no files to restore.');
    end;

    while MyResult = 0 do
    begin
      if UpperCase(ExtractFileName(SearchRec1.Name)) = 'SYSTEM.GBK' then
        TempStr := 'System, '
      else
      if UpperCase(ExtractFileName(SearchRec1.Name)) = 'S_BUREAU.GBK' then
        TempStr := 'Bureau, '
      else
      if UpperCase(ExtractFileName(SearchRec1.Name)) = 'TMP_TBLS.GBK' then
        TempStr := 'Temporary Tables, '
      else
        Inc(ErrorCount);
      MyResult := FindNext(SearchRec1);
    end;
    if TempStr <> '' then
      TempStr := TempStr + 'and ';
    if isMessage('You are about to restore ' + TempStr + IntToStr(ErrorCount) + ' Client database files.'
    + #13 + 'If these databases exist on the server they will be overwritten. Press OK to overwrite them.', mtConfirmation, [mbOK, mbCancel]) <> mrOk then
      Exit;
    ErrorCount := RestoreLoop(FServer, FDestination, FSource, False, StrToInt(Edit10.Text), ProgressBar1, ScrambleCheck.Checked, FTMPDatabasePath);
  end;

  if CheckBox8.Checked then
    EmailLog(SmtpHostEdit.Text, SmtpUserEdit.Text, EMailFromEdit.Text, EMailToEdit.Text);

  Beep;
  if ErrorCount = 0 then
  begin
    if RadioGroup1.ItemIndex=0 then
      isMessage('Backup completed successfully!')
    else
      isMessage('Restore completed successfully!');
  end
  else
  begin
    if RadioGroup1.ItemIndex=0 then
      isMessage('Backup completed with ' + IntToStr(ErrorCount) + ' errors!')
    else
      isMessage('Restore completed with ' + IntToStr(ErrorCount) + ' errors!');
    LaunchViewer;
  end;
  ProgressBar1.Position := 0;
end;

procedure TBackup.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex=0 then
  begin
    Backup.Caption:='Evolution Backup';
    DoBackupNowButton.Caption:='Do Backup Now';
    Label1.Caption:='Backup Server (blank for local):';
    Label2.Caption:='Backup Source Directory:';
    Label3.Caption:='Backup Destination Directory:';
    OpenDialog1.Filter:='Interbase Database (*.gdb)|*.gdb';
    CheckBox2.Enabled := True;
    CheckBox3.Enabled := True;
    CheckBox4.Enabled := True;
    CheckBox5.Enabled := True;
    CheckBox9.Enabled := True;
    Label12.Enabled := True;
    Edit12.Enabled := True;
    Label14.Enabled := True;
    Edit13.Enabled := True;
    if FIsystems then
    begin
      ScrambleCheck.Enabled := false;
      TmpDbPath.Enabled := false;
    end;
  end
  else
  begin
    Backup.Caption:='Evolution Restore';
    DoBackupNowButton.Caption:='Do Restore Now';
    Label1.Caption:='Restore Server (blank for local):';
    Label2.Caption:='Restore Destination Directory:';
    Label3.Caption:='Restore Source Directory:';
    OpenDialog1.Filter:='Interbase DB Backup (*.gbk)|*.gbk';
    CheckBox2.Enabled := False;
    CheckBox3.Enabled := False;
    CheckBox4.Enabled := False;
    CheckBox5.Enabled := False;
    CheckBox9.Enabled := False;
    Label12.Enabled := False;
    Edit12.Enabled := False;
    Label14.Enabled := False;
    Edit13.Enabled := False;
    if FIsystems then
    begin
      ScrambleCheck.Enabled := true;
      if ScrambleCheck.Checked then
      begin
        TmpDbPath.Enabled := true;
      end
      else
      begin
        TmpDbPath.Enabled := false;
      end;
    end;
  end;

  DoBackupNowButton.Enabled:=(Edit2.Text<>'') and (Edit3.Text<>'');
end;

procedure TBackup.TestEmailButtonClick(Sender: TObject);
var
  SMTP: TIsSMTPClient;
begin
  SMTP := TIsSMTPClient.Create(SmtpHostEdit.Text, 25, SmtpUserEdit.Text);
  try
    SMTP.SendQuickMessage(EMailFromEdit.Text, EMailToEdit.Text, 'Test message', 'EBackup email test successful!');
  finally
    SMTP.Free;
  end;
end;

procedure TBackup.TestFtpButtonClick(Sender: TObject);
var
  Uploader: IevBackupUploader;
begin
  if SecureChkBox.Checked then
  begin
    Uploader := TevSFtpBackupUploader.Create;
    Uploader.SetHost(FtpHostEdit.Text);
    Uploader.SetPort(StrToInt(SFtpPortEdit.Text));
    Uploader.SetUser(FtpLoginEdit.Text);
    Uploader.SetPassword(FtpPasswordEdit.Text);
    Uploader.Connect;
    Uploader.Disconnect;
    isMessage('Success!');
  end
  else
  begin
    if FtpTest(FtpHostEdit.Text, FtpLoginEdit.Text, FtpPasswordEdit.Text)
      then isMessage('Success!');
  end;
end;

procedure TBackup.HANDLE_WM_THREAD_EXCEPTION(var M: TMessage);
begin
  raise Exception(M.WParam);
end;

procedure TBackup.ComboBox1Change(Sender: TObject);
var
  MyIniFile: TIniFile;
  MyList: TStringList;
begin
  if ComboBox1.Text <> '' then
  begin
    MyIniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'ftp.ini');
    MyList := TStringList.Create;
    try
      MyIniFile.ReadSectionValues(ComboBox1.Text, MyList);
      FtpHostEdit.Text := '';
      FtpLoginEdit.Text := '';
      FtpPasswordEdit.Text := '';
      if MyList.IndexOfName('HostName') <> -1 then
        FtpHostEdit.Text := MyList.Values['HostName'];
      if MyList.IndexOfName('LoginName') <> -1 then
        FtpLoginEdit.Text := MyList.Values['LoginName'];
      if MyList.IndexOfName('Password') <> -1 then
        FtpPasswordEdit.Text := MyList.Values['Password'];
    finally
      MyList.Free;
      MyIniFile.Free;
    end;
  end;
end;

procedure TBackup.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F1 then
  begin
    isMessage('Command line switches:' + #13 +
    '/hidden � required to run in command line mode, it runs eBackup without the GUI screen' + #13 +
    '/password <password> � required password for databases' + #13 +
    '/threads <# of threads> � this number should be equal to the number of CPUs on your database server' + #13 +
    '/zipped � saves backup databases in .zip compressed format' + #13 +
    '/restore <optional path> � restores what was backed up (use " for names with spaces)' + #13 +
    '/erase � erases backup files after restore is completed' + #13 +
    '/email � emails the eBackup log file to a predefined address' + #13 +
    '/ftp � put backed up databases on a predefined ftp site' + #13 +
    '/overwrite � to overwrite backup.log file (data is appended by default)' + #13 +
    '/server <name> � to overwrite default backup server' + #13 +
    '/source <path> � to overwrite default backup source directory (use " for names with spaces)' + #13 +
    '/destination <path> � to overwrite default backup destination directory (use " for names with spaces)' + #13 +
    '/use_ini_file <optional path> - to use Connect.ini file for backup database locations (use " for names with spaces)' + #13 +
    '/split <count> � <count> is a number of smaller backups over which you want to spread your big backup' + #13 +
    '/resolution � backup RESOLUTION.gdb database as well');
  end;
end;

procedure TBackup.LaunchViewer;
begin
  LogViewer:=TLogViewer.Create(Application);
  try
    LogViewer.RichEdit1.Lines.LoadFromFile(ExtractFilePath(Application.ExeName)+'backup.log');
    LogViewer.ShowModal;
  finally
    LogViewer.Free;
  end;
end;

procedure TBackup.SpeedButton3Click(Sender: TObject);
begin
  LaunchViewer;
end;

procedure TBackup.ScrambleCheckClick(Sender: TObject);
begin
  if ScrambleCheck.Checked then
  begin
    TmpDbPath.Enabled := true;
  end
  else
  begin
    TmpDbPath.Enabled := false;
  end;
end;

end.
