unit evSFtpBackupUploader;

interface

uses evBackupUploader, SBSimpleSftp, SBSftpCommon, isBaseClasses, Classes,
  SysUtils;

type
  TevSFtpBackupUploader = class(TisInterfacedObject, IevBackupUploader)
  private
    FSFTP: TElSimpleSFTPClient;
    FDirectoryListing: TStringList;
  protected
    function  GetHost: String;
    procedure SetHost(const AValue: String);
    function  GetPort: Integer;
    procedure SetPort(const AValue: Integer);
    function  GetUser: String;
    procedure SetUser(const AValue: String);
    function  GetPassword: String;
    procedure SetPassword(const AValue: String);
    procedure Connect;
    procedure Disconnect;
    procedure ReadDirectory;
    function  GetDirectoryListing: TStringList;
    procedure Delete(const AFileName: String);
    procedure Put(const AFileName: String); overload;
    procedure DoOnConstruction; override;
  public  
    destructor  Destroy; override;
  end;


implementation

{ TevSFtpBackupUploader }

procedure TevSFtpBackupUploader.Connect;
begin
  FSFTP.Open;
end;

procedure TevSFtpBackupUploader.Delete(const AFileName: String);
begin
  FSFTP.RemoveFile(AFileName);
end;

destructor TevSFtpBackupUploader.Destroy;
begin
  FSFTP.Free;
  FDirectoryListing.Free;
  inherited;
end;

procedure TevSFtpBackupUploader.Disconnect;
begin
  FSFTP.Close;
end;

procedure TevSFtpBackupUploader.DoOnConstruction;
begin
  inherited;
  FDirectoryListing := TStringList.Create;
  FSFTP := TElSimpleSFTPClient.Create(nil);
end;

function TevSFtpBackupUploader.GetDirectoryListing: TStringList;
begin
  Result := FDirectoryListing;
end;

function TevSFtpBackupUploader.GetHost: String;
begin
  Result := FSFTP.Address;
end;

function TevSFtpBackupUploader.GetPassword: String;
begin
  Result := FSFTP.Password;
end;

function TevSFtpBackupUploader.GetPort: Integer;
begin
  Result := FSFTP.Port;
end;

function TevSFtpBackupUploader.GetUser: String;
begin
  Result := FSFTP.Username;
end;

procedure TevSFtpBackupUploader.Put(const AFileName: String);
begin
  FSFTP.UploadFile(AFileName, ExtractFileName(AFileName));
end;

procedure TevSFtpBackupUploader.ReadDirectory;
var
  Dir: String;
  List: TList;
  i: Integer;
begin
  Dir := FSFTP.OpenDirectory(FSFTP.RequestAbsolutePath('.'));
  List := TList.Create;
  try
    FSFTP.ReadDirectory(Dir, List);
    for i := 0 to List.Count - 1 do
      if (TElSftpFileInfo(List[i]).Name <> '.')
      and (TElSftpFileInfo(List[i]).Name <> '..') then
        FDirectoryListing.Add(TElSftpFileInfo(List[i]).Name);
  finally
    List.Free;
  end;
end;

procedure TevSFtpBackupUploader.SetHost(const AValue: String);
begin
  FSFTP.Address := AValue;
end;

procedure TevSFtpBackupUploader.SetPassword(const AValue: String);
begin
  FSFTP.Password := AValue;
end;

procedure TevSFtpBackupUploader.SetPort(const AValue: Integer);
begin
  FSFTP.Port := AValue;
end;

procedure TevSFtpBackupUploader.SetUser(const AValue: String);
begin
  FSFTP.Username := AValue;
end;

end.
