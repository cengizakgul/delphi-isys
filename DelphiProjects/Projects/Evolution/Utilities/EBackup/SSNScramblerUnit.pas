unit SSNScramblerUnit;

interface

uses Classes, SysUtils, SyncObjs, Math;

type

  ISSNElement = interface
    ['{CF8B324D-C00E-42D9-A110-A7AD16BBC1F5}']
    function GetDistanceToPrev : integer;
    procedure CalcDistance(APrevElement : ISSNElement);
    function GetNumber : integer;
    function TakeNewNumber : String;
  end;

  ISSNList = interface
    ['{847B0BAF-C471-4856-AA90-D9B53F40FED8}']
    procedure Clear;
    procedure FillFromList(const AList : TStringList);
    function GetFirst : String;
    function GetNext : String;
  end;

  TSSNList = class(TInterfacedObject, ISSNList)
  private
    FList : TInterfaceList;
    FStartIndex, FCurrentIndex : integer;
    FCritSect : TCriticalSection;
  protected
    function RandomStartIndex : integer;
  public
    procedure Clear;
    procedure FillFromList(const AList : TStringList);
    function GetFirst : String;
    function GetNext : String;
    procedure Lock;
    procedure Unlock;

    constructor Create;
    destructor Destroy; override;
  end;

  TSSNElement = class(TInterfacedObject, ISSNElement)
  private
    FNumber : integer;
    FDistance : integer;
  protected
  public
    function GetDistanceToPrev : integer;
    function TakeNewNumber : String;
    procedure CalcDistance(APrevElement : ISSNElement);
    function GetNumber : integer;

    constructor Create(ASSN : String);
    destructor Destroy; override;
  end;

  function CheckSSNFormat(ASSN : String) : boolean;
  function CheckSSNRange(ASSN : String) : boolean;

implementation

  function CheckSSNFormat(ASSN : String) : boolean;
  var
    tmp, tmp1 : String;
    i : integer;
  begin
    result := false;
    tmp := Trim(ASSN);
    if (Length(ASSN) <> 11) then exit;
    tmp1 := Copy(tmp, 4, 1);
    if (tmp1 <> '.') and (tmp1 <> '-') then exit;
    tmp1 := Copy(tmp, 7, 1);
    if (tmp1 <> '.') and (tmp1 <> '-') then exit;
    for i := 1 to Length(tmp) do
    begin
      if not (tmp[i] in ['0'..'9']) then
      begin
        if (i = 4) or (i = 7) then continue
        else exit;
      end;
    end;
    result := true;
  end;

  function CheckSSNRange(ASSN : String) : boolean;
  var
    tmp : String;
    i : integer;
  begin
    result := false;
    tmp := Copy(ASSN,1,3) + Copy(ASSN, 5,2) + Copy(ASSN,8,4);
    i := StrToInt(tmp);
    if not ((i >= 730000000) and (i <= 999999999)) then exit;
    result := true;
  end;

{ TSSNList }

procedure TSSNList.Clear;
begin
  Lock;
  try
    FCurrentIndex := 1;
    FList.Clear;
    FStartIndex := 0;
  finally
    Unlock;
  end;
end;

constructor TSSNList.Create;
begin
  inherited;
  Randomize;
  FList := TInterfaceList.Create;
  FCurrentIndex := 1;
  FStartIndex := 0;
  FCritSect := TCriticalSection.Create;
end;

destructor TSSNList.Destroy;
begin
  FCritSect.Free;
  FList.Free;
  inherited;
end;

procedure TSSNList.FillFromList(const AList: TStringList);
var
  i, j : integer;
  tmpElement : ISSNElement;
begin
  Lock;
  try
    FCurrentIndex := 1;
    FList.Clear;

//    tmpElement := TSSNElement.Create('730-00-0000');
//    FList.Add(tmpElement);

    AList.Sorted := true;

    // adding the first and the last placeholders
    if not AList.Find('730-00-0000', j) then
      AList.Add('730-00-0000');
    if not AList.Find('999-99-9999', j) then
      AList.Add('999-99-9999');

    // preparing the list by taking 730-00-0000, 731-00-000 .. 999-00-000 values
    for i := 731 to 999 do
    begin
      if not AList.Find(IntToStr(i) + '-00-0000', j) then
      begin
        AList.Add(IntToStr(i) + '-00-0000');
      end;
    end;

    AList.Sorted := true;
    for i := 0 to AList.Count - 1 do
      if CheckSSNFormat(AList[i]) and CheckSSNRange(AList[i]) then
      begin
        tmpElement := TSSNElement.Create(AList[i]);
        FList.Add(tmpElement);
      end;

//    tmpElement := TSSNElement.Create('999-99-9999');
//    FList.Add(tmpElement);

    for i := 1  to FList.Count - 1 do
    begin
      tmpElement := FList[i] as ISSNElement;
      tmpElement.CalcDistance(FList[i-1] as ISSNElement);
    end;
  finally
    Unlock;
  end;

  FStartIndex := RandomStartIndex;
end;

function TSSNList.GetFirst: String;
begin
  Lock;
  try
    FStartIndex := RandomStartIndex;
    FCurrentIndex := FStartIndex;
    result := GetNext;
  finally
    Unlock;
  end;
end;

function TSSNList.GetNext: String;
var
  i : integer;
  tmp : ISSNElement;
begin
  Lock;
  try
    result := '';
    if FList.Count < 2 then
      raise Exception.Create('List is empty!')
    else
    begin
      for i := FCurrentIndex to Flist.Count - 1 do
      begin
        tmp := FList[i] as ISSNElement;
        if tmp.GetDistanceToPrev > 0 then
        begin
          result := tmp.TakeNewNumber;
          FCurrentIndex := i;
          exit;
        end;
      end;
      for i := 1 to FStartIndex do
      begin
        tmp := FList[i] as ISSNElement;
        if tmp.GetDistanceToPrev > 0 then
        begin
          result := tmp.TakeNewNumber;
          FCurrentIndex := i;
          exit;
        end;
      end;
      raise Exception.Create('Cannot find any value!')
    end;
  finally
    Unlock;
  end;
end;

procedure TSSNList.Lock;
begin
  FCritSect.Enter;
end;

function TSSNList.RandomStartIndex: integer;
var
  i, j : integer;
begin
  Randomize;
  i := RandomRange(730, 999) * 1000000;
  for j := 0 to FList.Count - 1 do
    if (FList[j] as ISSNElement).GetNumber = i then
    begin
      result := j;
      exit;
    end;
  raise Exception.Create('Cannot generate random SSN area!');  
end;

procedure TSSNList.Unlock;
begin
  FCritSect.Leave;
end;

{ TSSNElement }

procedure TSSNElement.CalcDistance(APrevElement: ISSNElement);
begin
  if FNumber < APrevElement.GetNumber then
    raise Exception.Create('Current element cannot be less then previous one!');
  FDistance := FNumber - APrevElement.GetNumber - 1;
end;

constructor TSSNElement.Create(ASSN: String);
var
  tmp : String;
begin
  if not CheckSSNFormat(ASSN) then
    raise Exception.Create('Wrong format of SSN: ' + ASSN)
  else
  begin
    tmp := Copy(ASSN,1,3) + Copy(ASSN, 5,2) + Copy(ASSN,8,4);
    FNumber := StrToInt(tmp);
    if not CheckSSNRange(ASSN) then
      raise Exception.Create('Value is in wrong range: ' +IntToStr(FNumber));
  end;
end;

destructor TSSNElement.Destroy;
begin

  inherited;
end;

function TSSNElement.GetDistanceToPrev: integer;
begin
  result := FDistance;
end;

function TSSNElement.GetNumber: integer;
begin
  result := FNumber;
end;

function TSSNElement.TakeNewNumber: String;
begin
  if FDistance > 0 then
  begin
    result := IntToStr(FNumber - FDistance);
    Dec(FDistance);
    result := Copy(result, 1, 3) + '-' + Copy(result, 4, 2) + '-' + Copy(result, 6, 4);
  end
  else
   raise Exception.Create('No free elements avaliabe!');
end;

end.
