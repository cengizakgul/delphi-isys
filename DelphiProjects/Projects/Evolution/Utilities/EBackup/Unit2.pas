// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit Unit2;

interface

uses
  Windows, Classes, Controls, SysUtils, ComCtrls, IBQuery, IBDatabase, Forms, EvTypes,
  RegistryFunctions, ISZippingRoutines, syncobjs, IniFiles, IsSMTPClient, IsFTP, isBasicUtils,
  isUtils, evBackupUploader, evSFtpBackupUploader, EvExceptions, evConsts;

type
  TJobType = (jtBackup, jtRestore);

  TWorkingThread = class(TThread)
  public
    Scrambling : boolean;
    CLNbr : integer;
    DBName: String;
    Source: String;
    Destination: String;
    DoEraseBackupFiles: Boolean;
    JobType: TJobType;
    TmpDbPath : String;
    procedure Execute; override;
  end;

function ParamExists(MyParam: String): Boolean;
function BackupLoop(ServerName, MySource, MyDestination: String; NbrThreads: Integer; aProgressBar: TProgressBar; MyList: TStringList; IniName: String): Integer;
function RestoreLoop(ServerName, AMySource, MyDestination: String; EraseBackupFiles: Boolean; NbrThreads: Integer; aProgressBar: TProgressBar; AScrambling : boolean = false; ATMPDatabasePath : STring = ''): Integer;
procedure ReadRegistry(var DirPath, Source, Destination, SMTPServer, SMTPUserID, EmailFrom, EmailTo, FTPHostName, FTPLoginName, FTPPassword, FTPGlobalDBs,
  FTPSecured, FtpPort, SFtpPort, TMPDatabasePath: String);
procedure MainBackgroundLoop;
procedure CorrectPath(var Path: String);
procedure ConvertToText(FileHandle: THandle; DBName: String);
procedure GetClientDBList(ServerName, MySource: String; MyList: TStringList);
procedure GetClientNbrs(ServerName, MySource, CustomNumber: String; MyList: TStringList);
procedure EmailLog(FSMTPServer, FSMTPUserID, FEMailFrom, FEmailTo: String);
procedure ZipBackup(FDestination: String);
procedure UnzipBackup(FDestination: String);
procedure PutToFTP(FDestination, FFTPHostName, FFTPLoginName, FFTPPassword: String; DoFTPGlobalDBs: Boolean; SecuredFTP: Boolean; PortNumber: Integer);
procedure GetFromFTP(FDestination, FFTPHostName, FFTPLoginName, FFTPPassword: String);
function ClearDir(const Path: string; Delete: Boolean = False): Boolean;

var
  Password: String = 'isys99';

implementation

uses
  Unit1, StrUtils, ScramblerUnit;

var
  ResultFile: TextFile;
  CSection: TCriticalSection;
  ThreadCount, TotalCount, ErrorCount: Integer;
  BackupReso: Boolean;
  IBPath: String;

function StrPCopySafe(Dest: PChar; const Source: string; const DestBufSize: Cardinal): PChar;
begin
  if Cardinal(Length(Source)) + 1 > DestBufSize then
    raise ERangeError.Create('Cannot perform string copy operation. Allocated buffer is to small.');

  Result := StrPCopy(Dest, Source);
end;


function GetLocalMachineName: string;
var
  cBuffer: array[0..MAX_COMPUTERNAME_LENGTH] of Char;
  iSize: DWORD;
begin
  iSize := SizeOf(cBuffer);
  FillChar(cBuffer, iSize, 0);
  GetComputerName(cBuffer, iSize);
  Result := cBuffer;
end;

procedure TWorkingThread.Execute;
var
  StartupInfo1: TStartupInfo;
  ProcessInfo1: TProcessInformation;
  pDest: array[0..512] of char;
  MyFileHandle: THandle;
  SA: TSecurityAttributes;
  E2: Exception;
begin
  try
    CSection.Enter;
    try
      Inc(ThreadCount);
    finally
      CSection.Leave;
    end;

    MyFileHandle:=0;
    try
      FillChar(SA, SizeOf(TSecurityAttributes), 0);
      SA.nLength:=SizeOf(SA);
      SA.lpSecurityDescriptor:=nil;
      SA.bInheritHandle:=TRUE;

      FillChar(StartupInfo1, SizeOf(TStartupInfo), 0);
      StartupInfo1.cb:=SizeOf(TStartupInfo);
      StartupInfo1.lpDesktop:=nil;
      StartupInfo1.dwFlags:=STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      StartupInfo1.wshowWindow:=SW_HIDE;
      CSection.Enter;
      try
        MyFileHandle:=CreateFile(PChar(ExtractFilePath(Application.ExeName)+DBName+'.tmp'), GENERIC_WRITE or GENERIC_READ, 0, @SA, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
      finally
        CSection.Leave;
      end;
      StartupInfo1.hStdError:=MyFileHandle;
      case JobType of
        jtBackup:
          StrPCopySafe(pDest,'"' + IBPath + 'gbak.exe" -b -t -g -user SYSDBA -password ' + Password + ' "'+Source+DBName+'" "'+Destination+Copy(DBName,1,Length(DBName)-3)+'gbk"', Length(pDest));
        jtRestore:
          StrPCopySafe(pDest,'"' + IBPath + 'gbak.exe" -c -rep -p 8192 -user SYSDBA -password ' + Password + ' "'+Source+DBName+'" "'+Destination+Copy(DBName,1,Length(DBName)-3)+'gdb"', Length(pDest));
      end;
      if CreateProcess(nil, pDest, nil, @SA, True, NORMAL_PRIORITY_CLASS , nil, nil, StartupInfo1, ProcessInfo1) then
      begin
        WaitForSingleObject(ProcessInfo1.hProcess, INFINITE);
        CloseHandle(ProcessInfo1.hProcess);
        CloseHandle(ProcessInfo1.hThread);
      end
      else
      try
        RaiseLastOSError;
      except
        on E: Exception do
          raise Exception.Create('There was an error creating GBAK.exe process!'#13#10 + E.Message);
      end;
      CloseHandle(MyFileHandle);
      repeat
        MyFileHandle:=CreateFile(PChar(ExtractFilePath(Application.ExeName)+DBName+'.tmp'), GENERIC_WRITE or GENERIC_READ, 0, @SA, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
      until MyFileHandle<>INVALID_HANDLE_VALUE;
      CSection.Enter;
      try
        ConvertToText(MyFileHandle, DBName);
      finally
        CSection.Leave;
      end;
      if (JobType = jtRestore) and DoEraseBackupFiles then
        DeleteFile(Source+DBName);
    finally
      CloseHandle(MyFileHandle);
      repeat
      until DeleteFile(ExtractFilePath(Application.ExeName)+DBName+'.tmp');
      CSection.Enter;
      try
        Dec(ThreadCount);
        Inc(TotalCount);
      finally
        CSection.Leave;
      end;
    end;
    if (JobType = jtRestore) and (Scrambling) then
      Scramble(Destination+Copy(DBName,1,Length(DBName)-3)+'gdb', TmpDbPath, FB_Admin, Password, GetClNbr(DBName));
  except
    on E: Exception do
    begin
      E2 := Exception.CreateHelp(E.Message, E.HelpContext);
      PostMessage(Backup.Handle, WM_THREAD_EXCEPTION, Integer(Pointer(E2)), 0);
    end;
  end;
end;

function ParamExists(MyParam: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 1 to ParamCount do
    if UpperCase(ParamStr(I)) = UpperCase(MyParam) then
    begin
      Result := True;
      Break;
    end;
end;

procedure GetClientDBList(ServerName, MySource: String; MyList: TStringList);
var
  MyDatabase: TIBDatabase;
  MyTransaction: TIBTransaction;
  MyQuery: TIBQuery;
  FullSource: String;
begin
  if ServerName = '' then
    FullSource := MySource
  else
    FullSource := ServerName + ':' + MySource;

  MyDatabase := TIBDatabase.Create(Nil);
  MyTransaction := TIBTransaction.Create(Nil);
  MyQuery := TIBQuery.Create(Nil);
  try
    MyDatabase.LoginPrompt := False;
    MyDatabase.DatabaseName := FullSource + 'CL_BASE.gdb';
    MyDatabase.Params.Add('isc_dpb_user_name=SYSDBA');
    MyDatabase.Params.Add('isc_dpb_password=' + Password);
    MyDatabase.IdleTimer := 0;
    try
      MyDatabase.Connected := True;
    except
      raise EInvalidParameters.CreateHelp('Wrong server name, password or backup source directory.', IDH_InvalidParameters);
    end;

    MyTransaction.DefaultDatabase := MyDatabase;
    MyTransaction.DefaultAction := TARollback;
    MyTransaction.Active := True;

    MyQuery.Database := MyDatabase;
    MyQuery.ParamCheck := True;
    MyQuery.SQL.Text:='select * from ALL_CL_NUMBERS(''' + MySource + ''') order by CL_NBR';
    MyQuery.Transaction := MyTransaction;
    try
      MyQuery.Open;
    except
      raise Exception.CreateHelp('Error getting database names. Check if the UDF is installed correctly.', IDH_IncorrectInstallation);
    end;

    MyList.Clear;
    with MyQuery do
    begin
      while not EOF do
      begin
        MyList.Add('CL_' + FieldByName('CL_NBR').AsString + '.gdb');
        Next;
      end;
      Close;
    end;
    MyDatabase.Connected := False;
  finally
    MyQuery.Free;
    MyTransaction.Free;
    MyDatabase.Free;
  end;
end;

procedure GetClientNbrs(ServerName, MySource, CustomNumber: String; MyList: TStringList);
var
  MyDatabase: TIBDatabase;
  MyTransaction: TIBTransaction;
  MyQuery: TIBQuery;
  FullSource, MyNumbers: String;
begin
  MyNumbers := ',' + CustomNumber + ',';

  if ServerName = '' then
    FullSource := MySource
  else
    FullSource := ServerName + ':' + MySource;

  MyDatabase := TIBDatabase.Create(Nil);
  MyTransaction := TIBTransaction.Create(Nil);
  MyQuery := TIBQuery.Create(Nil);
  try
    MyDatabase.LoginPrompt := False;
    MyDatabase.DatabaseName := FullSource + 'TMP_TBLS.gdb';
    MyDatabase.Params.Add('isc_dpb_user_name=SYSDBA');
    MyDatabase.Params.Add('isc_dpb_password=' + Password);
    MyDatabase.IdleTimer := 0;
    try
      MyDatabase.Connected := True;
    except
      raise EInvalidParameters.CreateHelp('Wrong server name, password or backup source directory.', IDH_InvalidParameters);
    end;

    MyTransaction.DefaultDatabase := MyDatabase;
    MyTransaction.DefaultAction := TARollback;
    MyTransaction.Active := True;

    MyQuery.Database := MyDatabase;
    MyQuery.ParamCheck := True;
    MyQuery.SQL.Text:='select * from TMP_CL order by CL_NBR';
    MyQuery.Transaction := MyTransaction;
    MyQuery.Open;

    with MyQuery do
    begin
      while not EOF do
      begin
        if Pos(',' + FieldByName('CUSTOM_CLIENT_NUMBER').AsString + ',', MyNumbers) <> 0 then
          MyList.Add('CL_' + FieldByName('CL_NBR').AsString + '.gdb');
        Next;
      end;
      Close;
    end;
    MyDatabase.Connected := False;
  finally
    MyQuery.Free;
    MyTransaction.Free;
    MyDatabase.Free;
  end;
end;

function BackupLoop(ServerName, MySource, MyDestination: String; NbrThreads: Integer; aProgressBar: TProgressBar; MyList: TStringList; IniName: String): Integer;
var
  FullSource, DBName, SystemPath, SBureauPath, TempTablesPath, ClientPath, ResoPath, TempPath: String;
  DBFile: TextFile;
  SearchRec1: TSearchRec;
  DBList: TStringList;
  OldCursor: TCursor;
  MyIniFile: TIniFile;

  procedure GetDBList(FullPath: String);
  begin
    if Pos(':', FullPath) = 0 then
    begin
      ServerName := '';
      MySource := FullPath;
    end
    else
    begin
      ServerName := Copy(FullPath, 1, Pos(':', FullPath) - 1);
      MySource := Copy(FullPath, Pos(':', FullPath) + 1, Length(FullPath));
    end;
    GetClientDBList(ServerName, MySource, DBList);
  end;

  procedure TestDBPaths(aPath: String);
  begin
    if (SystemPath = aPath) and (aPath <> '') then
    begin
      SystemPath := '';
      DBList.Add('SYSTEM.gdb');
    end;
    if (SBureauPath = aPath) and (aPath <> '') then
    begin
      SBureauPath := '';
      DBList.Add('S_BUREAU.gdb');
    end;
    if (TempTablesPath = aPath) and (aPath <> '') then
    begin
      TempTablesPath := '';
      DBList.Add('TMP_TBLS.gdb');
    end;
    if (ResoPath = aPath) and (aPath <> '') then
    begin
      ResoPath := '';
      DBList.Add('RESOLUTION.gdb');
    end;
  end;

  procedure DoActualBackup;
  var
    I: Integer;
  begin
    if ServerName='' then
      Writeln(ResultFile, GetLocalMachineName + ': BACKUP OF Localhost STARTED ON ' + FormatDateTime('dd/mm/yyyy" AT "hh:nn:ss', Now))
    else
      Writeln(ResultFile, GetLocalMachineName + ': BACKUP OF ' + ServerName + ' STARTED ON ' + FormatDateTime('dd/mm/yyyy" AT "hh:nn:ss', Now));
    Writeln(ResultFile, 'SOURCE: ' + MySource + '; DESTINATION: ' + MyDestination);
    Writeln(ResultFile);

    if Assigned(aProgressBar) then
      aProgressBar.Max := DBList.Count;
    ThreadCount := 0;
    TotalCount := 0;
    CSection := TCriticalSection.Create;
    try
      for I := 0 to DBList.Count - 1 do
      begin
        with TWorkingThread.Create(True) do
        begin
          FreeOnTerminate := True;
          DBName := DBList[I];
          Source := FullSource;
          Destination := MyDestination;
          JobType := jtBackup;
          Resume;
        end;
        repeat
          Sleep(500);
          if Assigned(aProgressBar) then
            aProgressBar.Position := TotalCount;
          Application.ProcessMessages;
        until ThreadCount < NbrThreads;
      end;
      while ThreadCount > 0 do
      begin
        Sleep(500);
        if Assigned(aProgressBar) then
          aProgressBar.Position := TotalCount;
        Application.ProcessMessages;
      end;
    finally
      CSection.Free;
    end;

    Writeln(ResultFile,'BACKUP ENDED ON ' + FormatDateTime('dd/mm/yyyy" AT "hh:nn:ss', Now));
    Writeln(ResultFile);
  end;

begin
  ErrorCount := 0;
  OldCursor := Screen.Cursor;
  Screen.Cursor := crSQLWait;
  try
    if ServerName= '' then
      FullSource := MySource
    else
      FullSource := ServerName + ':' + MySource;

    DBList := TStringList.Create;
    try
      AssignFile(ResultFile, ExtractFilePath(Application.ExeName)+'backup.log');
      if FileExists(ExtractFilePath(Application.ExeName)+'backup.log') then
        Append(ResultFile)
      else
        Rewrite(ResultFile);

      if IniName = '' then
      begin
        if FileExists(ExtractFilePath(Application.ExeName)+'include.ini') then
        begin
          AssignFile(DBFile, ExtractFilePath(Application.ExeName)+'include.ini');
          Reset(DBFile);
          try
            while not EOF(DBFile) do
            begin
              Readln(DBFile, DBName);
              if DBName <> '' then
                DBList.Add(DBName);
            end;
          finally
            CloseFile(DBFile);
          end;
        end
        else
        if Assigned(MyList) then
        begin
          DBList.Assign(MyList);
        end
        else
        begin
          if FindFirst(ExtractFilePath(Application.ExeName)+'clients.*', faAnyFile, SearchRec1) = 0 then
          begin
            AssignFile(DBFile, SearchRec1.Name);
            Reset(DBFile);
            try
              while not EOF(DBFile) do
              begin
                Readln(DBFile, DBName);
                if DBName <> '' then
                  DBList.Add(DBName);
              end;
            finally
              CloseFile(DBFile);
            end;
            DeleteFile(SearchRec1.Name);
          end
          else
            GetClientDBList(ServerName, MySource, DBList);
          FindClose(SearchRec1);

          DBList.Add('SYSTEM.gdb');
          DBList.Add('S_BUREAU.gdb');
          DBList.Add('TMP_TBLS.gdb');
          DBList.Add('CL_BASE.gdb');
          if BackupReso then
            DBList.Add('RESOLUTION.gdb');
        end;
        DoActualBackup;
      end
      else
      begin
        MyIniFile := TIniFile.Create(IniName);
        try
          SystemPath := ExtractFilePathUD(MyIniFile.ReadString('Aliases', 'System', ''));
          SBureauPath := ExtractFilePathUD(MyIniFile.ReadString('Aliases', 'ServiceBureau', ''));
          TempTablesPath := ExtractFilePathUD(MyIniFile.ReadString('Aliases', 'Sb_Temporary', ''));
          ClientPath := MyIniFile.ReadString('Aliases', 'Client', '');
          if BackupReso then
            ResoPath := ExtractFilePathUD(MyIniFile.ReadString('Aliases', 'Resolution', ''));
          repeat
            if Pos(']', ClientPath) > 0 then
              Delete(ClientPath, 1, Pos(']', ClientPath));
            if Pos(';', ClientPath) > 0 then
              TempPath := Copy(ClientPath, 1, Pos(';', ClientPath) - 1)
            else
              TempPath := ClientPath;
            TempPath := ExtractFilePathUD(TempPath);
            DBList.Clear;
            GetDBList(TempPath);
            TestDBPaths(TempPath);
            FullSource := TempPath;
            DoActualBackup;
            if Pos(';', ClientPath) > 0 then
              Delete(ClientPath, 1, Pos(';', ClientPath))
            else
              ClientPath := '';
          until ClientPath = '';
          if SystemPath <> '' then
          begin
            TestDBPaths(SystemPath);
            FullSource := SystemPath;
            DoActualBackup;
          end;
          if SBureauPath <> '' then
          begin
            TestDBPaths(SBureauPath);
            FullSource := SBureauPath;
            DoActualBackup;
          end;
          if TempTablesPath <> '' then
          begin
            TestDBPaths(TempTablesPath);
            FullSource := TempTablesPath;
            DoActualBackup;
          end;
          if ResoPath <> '' then
          begin
            TestDBPaths(ResoPath);
            FullSource := ResoPath;
            DoActualBackup;
          end;
        finally
          MyIniFile.Free;
        end;
      end;
    finally
      CloseFile(ResultFile);
      DBList.Free;
    end;

    if Assigned(aProgressBar) then
      aProgressBar.Position := TotalCount;
    Result := ErrorCount;
  finally
    Screen.Cursor := OldCursor;
  end;
end;

function RestoreLoop(ServerName, AMySource, MyDestination: String; EraseBackupFiles: Boolean; NbrThreads: Integer; aProgressBar: TProgressBar; AScrambling : boolean = false; ATMPDatabasePath : String = ''): Integer;
var
  SearchRec1: TSearchRec;
  MyResult, TotalFiles: Integer;
  FullDestination: String;
  OldCursor: TCursor;
  Newpath : String;
  MySource : String;

begin
  MySource := AMySource;
  Result := 0;
  ErrorCount := 0;
  OldCursor := Screen.Cursor;
  Screen.Cursor := crSQLWait;
  try
    if ServerName='' then
      FullDestination:=MyDestination
    else
      FullDestination:=ServerName+':'+MyDestination;

    TotalFiles := 0;
    MyResult:=FindFirst(MySource+'*.gbk', faAnyFile, SearchRec1);
    try
      while MyResult = 0 do
      begin
        Inc(TotalFiles);
        MyResult := FindNext(SearchRec1);
      end;
    finally
      FindClose(SearchRec1);
    end;
    if Assigned(aProgressBar) then
      aProgressBar.Max := TotalFiles;

    AssignFile(ResultFile, ExtractFilePath(Application.ExeName)+'backup.log');
    if FileExists(ExtractFilePath(Application.ExeName)+'backup.log') then
    begin
      Append(ResultFile);
      Writeln(ResultFile, '');
    end
    else
      Rewrite(ResultFile);

    try
      if ServerName='' then
        Writeln(ResultFile, GetLocalMachineName + ': RESTORE TO Localhost STARTED ON ' + FormatDateTime('dd/mm/yyyy" AT "hh:nn:ss', Now))
      else
        Writeln(ResultFile, GetLocalMachineName + ': RESTORE TO ' + ServerName + ' STARTED ON ' + FormatDateTime('dd/mm/yyyy" AT "hh:nn:ss', Now));
        Writeln(ResultFile, 'SOURCE: ' + MySource + '; DESTINATION: ' + MyDestination);
      NewPath := MySource;        
      if AScrambling then
      begin
        NewPath := AppTempFolder;
        ForceDirectories(Newpath);
        Writeln(ResultFile, 'SCRAMBLING is ON. Temporary folder is ' + NewPath);
      end;
      Writeln(ResultFile);

      // renaming files before scrambling
      if AScrambling then
      begin
        MyResult:=FindFirst(MySource+'*.gbk', faAnyFile, SearchRec1);
        try
          while MyResult = 0 do
          begin
            if CheckName(ExtractFileName(SearchRec1.Name)) then
              if GetClNbr(ExtractFileName(SearchRec1.Name)) < minScrabledNbr then
                if not Windows.CopyFile(PChar(MySource + SearchRec1.Name), PChar(NewPath + 'CL_' + IntToStr(minScrabledNbr + GetClNbr(ExtractFileName(SearchRec1.Name))) + '.gbk'), false) then
                begin
                  result := 1;
                  Writeln(ResultFile, 'Cannot copy file for scrambling: ' + SearchRec1.Name + #13#10 + 'To: ' + NewPath + 'CL_' + IntToStr(minScrabledNbr + GetClNbr(ExtractFileName(SearchRec1.Name))) + '.gbk');
                  Writeln(ResultFile, 'Task is cancelled');
                  exit;
                end
                else
                  Writeln(ResultFile, 'Client number was changed for scrambling: ' + 'CL_' + IntToStr(minScrabledNbr + GetClNbr(ExtractFileName(SearchRec1.Name))) + '.gbk')
              else
              begin
                result := 1;
                Writeln(ResultFile, 'File is already scrambled: ' + SearchRec1.Name);
                Writeln(ResultFile, 'Task is cancelled');
                exit;
              end;
            MyResult := FindNext(SearchRec1);
          end;
        finally
          FindClose(SearchRec1);
        end;
      end;
      MySource := NewPath;
      Flush(ResultFile);

      MyResult:=FindFirst(MySource+'*.gbk', faAnyFile, SearchRec1);
      ThreadCount := 0;
      TotalCount := 0;
      CSection := TCriticalSection.Create;
      try
        while MyResult = 0 do
        begin
          if ThreadCount < NbrThreads then
          begin
            with TWorkingThread.Create(True) do
            begin
              Scrambling := AScrambling;
              FreeOnTerminate := True;
              DBName := ExtractFileName(SearchRec1.Name);
              TmpDbPath := ATMPDatabasePath;
              Source := MySource;
              Destination := FullDestination;
              JobType := jtRestore;
              DoEraseBackupFiles := EraseBackupFiles;
              Resume;
            end;
            MyResult := FindNext(SearchRec1);
          end;
          Sleep(500);
          if Assigned(aProgressBar) then
            aProgressBar.Position := TotalCount;
          Application.ProcessMessages;
        end;
        while ThreadCount > 0 do
        begin
          Sleep(500);
          if Assigned(aProgressBar) then
            aProgressBar.Position := TotalCount;
          Application.ProcessMessages;
        end;
      finally
        FindClose(SearchRec1);
        CSection.Free;
      end;

    finally
      if AScrambling then
      try
        ClearDir(Newpath);
      except
      end;
      Writeln(ResultFile,'RESTORE ENDED ON ' + FormatDateTime('dd/mm/yyyy" AT "hh:nn:ss', Now));
      Writeln(ResultFile);
      CloseFile(ResultFile);
    end;

    if Assigned(aProgressBar) then
      aProgressBar.Position := TotalCount;
    Result := ErrorCount;
  finally
    Screen.Cursor := OldCursor;
  end;
end;

procedure ReadRegistry(var DirPath, Source, Destination, SMTPServer, SMTPUserID, EmailFrom, EmailTo, FTPHostName, FTPLoginName, FTPPassword, FTPGlobalDBs,
  FTPSecured, FtpPort, SFtpPort, TMPDatabasePath: String);
begin
  DirPath      := ReadFromRegistry('', 'Server', '');
  Source       := ReadFromRegistry('', 'Source', '');
  Destination  := ReadFromRegistry('', 'Destination', '');
  SMTPServer   := ReadFromRegistry('', 'SMTP Server', '');
  SMTPUserID   := ReadFromRegistry('', 'SMTP User ID', '');
  EmailFrom    := ReadFromRegistry('', 'Email From', '');
  EmailTo      := ReadFromRegistry('', 'Email To', '');
  FTPHostName  := ReadFromRegistry('', 'FTP Host Name', '');
  FTPLoginName := ReadFromRegistry('', 'FTP Login Name', '');
  FTPPassword  := ReadFromRegistry('', 'FTP Password', '');
  FTPGlobalDBs := ReadFromRegistry('', 'FTP Global DBs', '');
  FTPSecured  := ReadFromRegistry('', 'FTP SSH', '');
  FtpPort     := ReadFromRegistry('', 'FTP port', '21');
  SFtpPort    := ReadFromRegistry('', 'SFTP port', '22');
  TMPDatabasePath:=ReadFromRegistry('', 'TMP database path', '');
end;

procedure CorrectPath(var Path: String);
begin
  if (Path[Length(Path)]<>'\') and (Path[Length(Path)]<>'/') then
  begin
    if Pos('/', Path)=0 then
      Path:=Path+'\'
    else
      Path:=Path+'/';
  end;
end;

procedure MainBackgroundLoop;
var
  FServer, FSource, FDestination, FSMTPServer, FSMTPUserID, FEmailFrom, FEmailTo,
  FFTPHostName, FFTPLoginName, FFTPPassword, FFTPGlobalDBs, FFTPSecured,
  FFtpPort, FSFtpPort, OptPath, TMPDatabasePath: String;
  ActualPort: Integer;
  SearchRec1: TSearchRec;
  MyResult, I, SplitRatio, NbrThreads: Integer;
  DBList: TStringList;
  ClientsFile: TextFile;
begin
  ReadRegistry(FServer, FSource, FDestination, FSMTPServer, FSMTPUserID, FEmailFrom,
     FEmailTo, FFTPHostName, FFTPLoginName, FFTPPassword, FFTPGlobalDBs, FFTPSecured,
     FFtpPort, FSFtpPort, TMPDatabasePath);
  OptPath := '';

  if ParamExists('/use_ini_file') then
  begin
    for I := 1 to ParamCount - 1 do
    if UpperCase(ParamStr(I)) = '/USE_INI_FILE' then
    begin
      OptPath := ParamStr(I + 1);
      if OptPath[1] = '/' then
        OptPath := ''
      else
        CorrectPath(OptPath);
      Break;
    end;
    if OptPath = '' then
      OptPath := ExtractFilePath(Application.ExeName);
    OptPath := OptPath + 'Connect.ini';
  end
  else
  begin
    if ParamExists('/source') then
    begin
      for I := 1 to ParamCount do
        if UpperCase(ParamStr(I)) = '/SOURCE' then
        begin
          FSource := ParamStr(I + 1);
          Break;
        end;
    end;

    if ParamExists('/destination') then
    begin
      for I := 1 to ParamCount do
        if UpperCase(ParamStr(I)) = '/DESTINATION' then
        begin
          FDestination := ParamStr(I + 1);
          Break;
        end;
    end;

    if ParamExists('/server') then
    begin
      for I := 1 to ParamCount do
        if UpperCase(ParamStr(I)) = '/SERVER' then
        begin
          FServer := ParamStr(I + 1);
          Break;
        end;
    end;
  end;

  CorrectPath(FSource);
  CorrectPath(FDestination);

  MyResult := FindFirst(FDestination+'*.*', faAnyFile, SearchRec1);
  while MyResult = 0 do
  begin
    DeleteFile(FDestination+ExtractFileName(SearchRec1.Name));
    MyResult := FindNext(SearchRec1);
  end;

  if ParamExists('/overwrite') then
  begin
    DeleteFile(ExtractFilePath(Application.ExeName) + 'backup.log');
  end;

  BackupReso := ParamExists('/resolution');

  NbrThreads := 1;
  if ParamExists('/threads') then
  begin
    for I := 1 to ParamCount do
      if UpperCase(ParamStr(I)) = '/THREADS' then
      begin
        try
          NbrThreads := StrToInt(ParamStr(I + 1));
        except
          raise EInvalidParameters.CreateHelp('Number of threads needs to be defined in command line!', IDH_InvalidParameters);
        end;
        Break;
      end;
  end;

  if ParamExists('/password') then
  begin
    for I := 1 to ParamCount do
      if UpperCase(ParamStr(I)) = '/PASSWORD' then
      begin
        Password := ParamStr(I + 1);
        Break;
      end;
  end;

  if ParamExists('/split') then
  begin
    SplitRatio := 0;
    for I := 1 to ParamCount do
    if UpperCase(ParamStr(I)) = '/SPLIT' then
    begin
      try
        SplitRatio := StrToInt(ParamStr(I + 1));
      except
        raise EInvalidParameters.CreateHelp('Clients split ratio needs to be defined in command line!', IDH_InvalidParameters);
      end;
      Break;
    end;

    MyResult := FindFirst(ExtractFilePath(Application.ExeName)+'clients.*', faAnyFile, SearchRec1);
    while MyResult = 0 do
    begin
      DeleteFile(ExtractFilePath(Application.ExeName) + ExtractFileName(SearchRec1.Name));
      MyResult := FindNext(SearchRec1);
    end;

    if SplitRatio > 1 then
    begin
      DBList := TStringList.Create;
      try
        GetClientDBList(FServer, FSource, DBList);
        for I := 0 to DBList.Count - 1 do
        begin
          if I mod ((DBList.Count div SplitRatio) + 1) = 0 then
          begin
            if I <> 0 then
              CloseFile(ClientsFile);
            AssignFile(ClientsFile, ExtractFilePath(Application.ExeName) + 'clients.' + IntToStr((I div ((DBList.Count div SplitRatio) + 1)) + 1));
            Rewrite(ClientsFile);
          end;
          writeln(ClientsFile, DBList[I]);
        end;
        if DBList.Count > 0 then
          CloseFile(ClientsFile);
      finally
        DBList.Free;
      end;
    end;
  end;

  try
    BackupLoop(FServer, FSource, FDestination, NbrThreads, Nil, Nil, OptPath);
  except
    Exit;
  end;

  if ParamExists('/restore') then
  begin
    OptPath := '';
    for I := 1 to ParamCount - 1 do
    if UpperCase(ParamStr(I)) = '/RESTORE' then
    begin
      OptPath := ParamStr(I + 1);
      if OptPath[1] = '/' then
        OptPath := ''
      else
        CorrectPath(OptPath);
      Break;
    end;
    if OptPath = '' then
      RestoreLoop(FServer, FDestination, FSource, ParamExists('/erase'), NbrThreads, Nil)
    else
      RestoreLoop('', FDestination, OptPath, ParamExists('/erase'), NbrThreads, Nil);
  end;

  if ParamExists('/zipped') then
    ZipBackup(FDestination);

  if UpperCase(FFTPSecured) = 'Y' then
    ActualPort := StrToInt(FSFtpPort)
  else
    ActualPort := StrToInt(FFtpPort);

  if ParamExists('/ftp') then
    PutToFTP(FDestination, FFTPHostName, FFTPLoginName, FFTPPassword, (UpperCase(FFTPGlobalDBs) = 'Y'), (UpperCase(FFTPSecured) = 'Y'), ActualPort);

  if ParamExists('/email') then
    EmailLog(FSMTPServer, FSMTPUserID, FEmailFrom, FEmailTo);

  FindClose(SearchRec1);
end;

procedure ConvertToText(FileHandle: THandle; DBName: String);
var
  Buffer: Pointer;
  BytesToRead, BytesRead: LongWord;
  temp: PChar;
begin
  BytesToRead:=GetFileSize(FileHandle, nil);
  if BytesToRead > 4 then
  begin
    Inc(ErrorCount);
    GetMem(Buffer, BytesToRead);
    try
      ReadFile(FileHandle, Buffer^, BytesToRead, BytesRead, nil);
      temp := StrAlloc(BytesRead+1);
      try
        StrLCopy(temp, Buffer, BytesRead);
        temp[BytesRead] := #0;
        Writeln(ResultFile,'Errors for '+DBName+' database.');
        Writeln(ResultFile,'---------------------------------------------------');
        Write(ResultFile,Copy(string(temp),1,BytesRead));
        Writeln(ResultFile);
      finally
        StrDispose(temp);
      end;
    finally
      FreeMem(Buffer);
    end;
  end;
end;

procedure EmailLog(FSMTPServer, FSMTPUserID, FEmailFrom, FEmailTo: String);
var
  SMTP: TIsSMTPClient;
begin
  SMTP := TIsSMTPClient.Create(FSMTPServer, 25, FSMTPUserID, '');
  try
    SMTP.SendQuickAttach(FEMailFrom, FEMailTo,
                         'log file', 'EBackup log file is attached.',
                         ExtractFilePath(Application.ExeName)+'backup.log');
  finally
    SMTP.Free;
  end;
end;

procedure ZipBackup(FDestination: String);
var
  SearchRec1: TSearchRec;
  MyResult: Integer;
begin
  MyResult:=FindFirst(FDestination+'*.gbk', faAnyFile, SearchRec1);
  while MyResult=0 do
  begin
    if FileExists(FDestination+ExtractFileName(SearchRec1.Name)) then
      AddToFile(FDestination+Copy(ExtractFileName(SearchRec1.Name), 1,
          Length(ExtractFileName(SearchRec1.Name))-3)+'zip', 
          FDestination, ExtractFileName(SearchRec1.Name));
    DeleteFile(FDestination+ExtractFileName(SearchRec1.Name));
    MyResult:=FindNext(SearchRec1);
  end;
  FindClose(SearchRec1);
end;

procedure UnzipBackup(FDestination: String);
var
  SearchRec1: TSearchRec;
  MyResult: Integer;
begin
  MyResult := FindFirst(FDestination + '*.zip', faAnyFile, SearchRec1);
  while MyResult = 0 do
  begin
    ExtractFromFile(FDestination + SearchRec1.Name, FDestination, '*.gbk');
    DeleteFile(FDestination + ExtractFileName(SearchRec1.Name));
    MyResult:=FindNext(SearchRec1);
  end;
  FindClose(SearchRec1);
end;

procedure PutToFTP(FDestination, FFTPHostName, FFTPLoginName, FFTPPassword: String; DoFTPGlobalDBs: Boolean; SecuredFTP: Boolean; PortNumber: Integer);
var
  MyFtp: IevBackupUploader;
  SearchRec1: TSearchRec;
  MyResult: Integer;
  i: Integer;
begin
  if SecuredFTP then
    MyFtp := TevSFtpBackupUploader.Create
  else
    MyFtp := TevFtpBackupUploader.Create;

  MyFtp.SetHost(FFTPHostName);

  MyFtp.SetPort(PortNumber);

  MyFtp.SetUser(FFTPLoginName);
  MyFtp.SetPassword(FFTPPassword);
  MyFtp.Connect;
  MyFtp.ReadDirectory;

  for i := 0 to Pred(MyFtp.GetDirectoryListing.Count) do
    if ((Length(MyFtp.GetDirectoryListing[i]) > 3) and
       (LeftStr(MyFtp.GetDirectoryListing[i], 3) = 'CL_')) or
       (Trim(MyFtp.GetDirectoryListing[i])='SYSTEM.zip') or
       (Trim(MyFtp.GetDirectoryListing[i])='S_BUREAU.zip') or
       (Trim(MyFtp.GetDirectoryListing[i])='TMP_TBLS.zip')     then
      MyFtp.Delete(MyFtp.GetDirectoryListing[i]);

  MyResult := FindFirst(FDestination+'*.zip', faAnyFile, SearchRec1);
  while MyResult=0 do
  begin
    if not (not DoFTPGlobalDBs and ((UpperCase(ExtractFileName(SearchRec1.Name))='SYSTEM.ZIP')
    or (UpperCase(ExtractFileName(SearchRec1.Name))='S_BUREAU.ZIP')
    or (UpperCase(ExtractFileName(SearchRec1.Name))='TMP_TBLS.ZIP'))) then
      MyFtp.Put(FDestination+ExtractFileName(SearchRec1.Name));
    MyResult:=FindNext(SearchRec1);
  end;

  MyFtp.Disconnect;
  FindClose(SearchRec1);
end;

procedure GetFromFTP(FDestination, FFTPHostName, FFTPLoginName, FFTPPassword: String);
var
  MyFtp: TIsFTP;
  i: Integer;
begin
  MyFtp:=TIsFTP.Create;
  try
    MyFtp.Host := FFTPHostName;
    MyFtp.User := FFTPLoginName;
    MyFtp.Password := FFTPPassword;
    MyFtp.Connect;

    MyFtp.Get('SYSTEM.zip', FDestination + 'SYSTEM.zip', True);
    MyFtp.Get('S_BUREAU.zip', FDestination + 'S_BUREAU.zip', True);
    MyFtp.Get('TMP_TBLS.zip', FDestination + 'TMP_TBLS.zip', True);
    MyFtp.Get('RESOLUTION.zip', FDestination + 'RESOLUTION.zip', True);

    for i := 0 to Pred(MyFtp.DirectoryListing.Count) do
      if (Length(MyFtp.DirectoryListing[i]) > 3) and
         (LeftStr(MyFtp.DirectoryListing[i], 3) = 'CL_') then
         MyFtp.Get(MyFtp.DirectoryListing[i], FDestination + MyFtp.DirectoryListing[i], True);

    MyFtp.Disconnect;
  finally
    MyFtp.Free;
  end;
  
end;

function ClearDir(const Path: string; Delete: Boolean = False): Boolean;
const
  FileNotFound = 18;
var
  FileInfo: TSearchRec;
  DosCode: Integer;

  function DirExists(Name: string): Boolean;
  begin
    Result :=  DirectoryExists(Name) or FileExists(Name);
  end;

  function NormalDir(const DirName: string): string;
  begin
    Result := DirName;
    if (Result <> '') and not (AnsiLastChar(Result)^ in [':', '\']) then
    begin
      if (Length(Result) = 1) and (UpCase(Result[1]) in ['A'..'Z']) then
        Result := Result + ':\'
      else
        Result := Result + '\';
    end;
  end;

begin
  Result := DirExists(Path);
  if not Result then Exit;
  DosCode := SysUtils.FindFirst(NormalDir(Path) + '*.*', faAnyFile, FileInfo);
  try
    while DosCode = 0 do
    begin
      if (FileInfo.Name[1] <> '.') then
      begin
        if (FileInfo.Attr and faDirectory = faDirectory) then
          Result := ClearDir(NormalDir(Path) + FileInfo.Name, True) and Result
        else
          Result := Sysutils.DeleteFile(NormalDir(Path) + FileInfo.Name) and Result;
      end;
      DosCode := SysUtils.FindNext(FileInfo);
    end;
  finally
    Sysutils.FindClose(FileInfo);
  end;
  if Delete and Result and (DosCode = FileNotFound) and
    not ((Length(Path) = 2) and (Path[2] = ':')) then
  begin
    Result := RemoveDir(Path);
//    Result := (IOResult = 0) and Result;
  end;
end;

initialization
  BackupReso := False;
  IBPath := GetIBPath;

end.
