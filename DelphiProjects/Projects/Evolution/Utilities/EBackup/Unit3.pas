// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, isUtils;

type
  TLogViewer = class(TForm)
    FindDialog1: TFindDialog;
    RichEdit1: TRichEdit;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FindDialog1Find(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LogViewer: TLogViewer;

implementation

{$R *.DFM}

procedure TLogViewer.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Char(Key) = 'F') and (ssCtrl in Shift) then
  begin
    FindDialog1.Position := Point(RichEdit1.Left + RichEdit1.Width div 2, RichEdit1.Top + RichEdit1.Height div 2);
    FindDialog1.Execute;
  end;
end;

procedure TLogViewer.FindDialog1Find(Sender: TObject);
var
  FoundAt: LongInt;
  StartPos, ToEnd: Integer;
  MyOptions: TSearchTypes;
begin
  with RichEdit1 do
  begin
    if SelLength <> 0 then
      StartPos := SelStart + SelLength
    else
      StartPos := SelStart;
    ToEnd := Length(Text) - StartPos;
    if frWholeWord in FindDialog1.Options then
      MyOptions := MyOptions + [stWholeWord];
    if frMatchCase in FindDialog1.Options then
      MyOptions := MyOptions + [stMatchCase];
    FoundAt := FindText(FindDialog1.FindText, StartPos, ToEnd, MyOptions);

    if FoundAt <> -1 then
    begin
      SetFocus;
      SelStart := FoundAt;
      SelLength := Length(FindDialog1.FindText);
      Perform(EM_SCROLLCARET, 0, 0);
    end
    else
    begin
      Beep;
      isMessage('Text not found!');
    end;
  end;
end;

end.
