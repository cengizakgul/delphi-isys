object Backup: TBackup
  Left = 386
  Top = 53
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Evolution Backup'
  ClientHeight = 607
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 217
    Height = 607
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Backup/Restore'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 209
        Height = 65
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 183
          Height = 16
          Caption = 'Backup Server (blank for local)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Edit1: TEdit
          Left = 8
          Top = 32
          Width = 193
          Height = 21
          TabOrder = 0
          OnChange = Edit1Change
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 65
        Width = 209
        Height = 64
        Align = alTop
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 8
          Width = 149
          Height = 16
          Caption = 'Backup Source Directory'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SpeedButton1: TSpeedButton
          Left = 176
          Top = 32
          Width = 23
          Height = 22
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = SpeedButton1Click
        end
        object Edit2: TEdit
          Left = 8
          Top = 32
          Width = 161
          Height = 21
          TabOrder = 0
          OnChange = Edit2Change
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 129
        Width = 209
        Height = 64
        Align = alTop
        TabOrder = 2
        object Label3: TLabel
          Left = 8
          Top = 8
          Width = 173
          Height = 16
          Caption = 'Backup Destination Directory'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SpeedButton2: TSpeedButton
          Left = 176
          Top = 32
          Width = 23
          Height = 22
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          OnClick = SpeedButton2Click
        end
        object Edit3: TEdit
          Left = 8
          Top = 32
          Width = 161
          Height = 21
          TabOrder = 0
          OnChange = Edit3Change
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 441
        Width = 209
        Height = 138
        Align = alClient
        TabOrder = 4
        object Label10: TLabel
          Left = 8
          Top = 8
          Width = 116
          Height = 16
          Caption = 'Number of Threads'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SpeedButton3: TSpeedButton
          Left = 165
          Top = 72
          Width = 36
          Height = 33
          Hint = 'View Log File'
          Glyph.Data = {
            36040000424D360400000000000076000000280000001E0000001E0000000100
            080000000000C003000000000000000000001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000FFFFFF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00080808080808
            0808080808080808080808080808080808080808040404080000080808080808
            0808080808080808080808080808080808080804040404040000080808080808
            0808080808080808080808080808080808080404040404040000080808080808
            080808080808080C0C0C0C0C0C0C0C0808040404040404080000080808080808
            08080808080C0C08080808080808080C0C0C0404040408080000080808080808
            080808080C0808080808080808080808080C0C04040808080000080808080808
            0808080C0808080F0F0F08080707070708080C0C080808080000080808080808
            08080C0808080F0F08080808080707070708080C080808080000080808080808
            08080C08080F0F0808080808080808070707080C080808080000080808080808
            080C0000000F00000000000008080808070707080C0808080000080808080808
            080C0E0E0F0F0E000000000000080808080707080C0808080000080808080808
            080C0E0E0F0E0E000000000000080808080707080C0808080000080808080808
            080C0E0E0F0E0E000000000000080808080807080C0808080000080808080808
            080C0E0E0E0E0E000000000000080808080808080C0808080000000000000000
            000C0E0E0E0E0E000000000000080808080808080C080808000000000E0E0E0E
            0E0C0E0E0E0E0E000000000000080808080808080C080808000000000E0E0E0E
            0E0C0E0E0E0E0E000000000000080808080808080C080808000000000E0E0E0E
            0E0E0C0E0E0E0E0000000000000808080808080C08080808000000000E0E0E0E
            0E0E0C0E0E0E0E0000000000000808080F08080C08080808000000000E0E0E0E
            0E0E0E0C0E0E0E00000F0F0F0F0F0F0F08080C0808080808000000000E0E0E0E
            0E0E0E0E0C0E0E0000000F0F0F0F0808080C0808080808080000000000000000
            00000000000C0C00000000000008080C0C080808080808080000000000000000
            000000000000000C0C0C0C0C0C0C0C0808080808080808080000000000000000
            0000000000000000000008080808080808080808080808080000000000070707
            0707070707000000000008080808080808080808080808080000000000070707
            0707000007000000000008080808080808080808080808080000000000070707
            0707000007000000000008080808080808080808080808080000000000070707
            0707000007000000000008080808080808080808080808080000000000070707
            0707070707000000000808080808080808080808080808080000000000000000
            0000000000000000080808080808080808080808080808080000}
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton3Click
        end
        object DoBackupNowButton: TButton
          Left = 8
          Top = 72
          Width = 153
          Height = 33
          Caption = 'Do Backup Now'
          Default = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = DoBackupNowButtonClick
        end
        object RadioGroup1: TRadioGroup
          Left = 8
          Top = 24
          Width = 193
          Height = 41
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemIndex = 0
          Items.Strings = (
            'Backup'
            'Restore')
          ParentFont = False
          TabOrder = 2
          OnClick = RadioGroup1Click
        end
        object Edit10: TEdit
          Left = 144
          Top = 8
          Width = 25
          Height = 21
          TabOrder = 0
          Text = '1'
        end
        object UpDown1: TUpDown
          Left = 169
          Top = 8
          Width = 15
          Height = 21
          Associate = Edit10
          Min = 1
          Max = 64
          Position = 1
          TabOrder = 1
        end
        object ProgressBar1: TProgressBar
          Left = 1
          Top = 113
          Width = 207
          Height = 24
          Align = alBottom
          Smooth = True
          TabOrder = 4
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 193
        Width = 209
        Height = 64
        Align = alTop
        TabOrder = 3
        object Label11: TLabel
          Left = 8
          Top = 8
          Width = 165
          Height = 16
          Caption = 'Password (blank for default)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Edit11: TEdit
          Left = 8
          Top = 32
          Width = 193
          Height = 21
          PasswordChar = '*'
          TabOrder = 0
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 257
        Width = 209
        Height = 184
        Align = alTop
        TabOrder = 5
        object Label12: TLabel
          Left = 8
          Top = 56
          Width = 74
          Height = 16
          Caption = 'Custom CL #'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 8
          Top = 80
          Width = 72
          Height = 16
          Caption = 'Internal CL #'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 8
          Top = 128
          Width = 99
          Height = 16
          Caption = 'TMP_TBLS path'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Edit12: TEdit
          Left = 88
          Top = 56
          Width = 113
          Height = 21
          TabOrder = 0
        end
        object CheckBox2: TCheckBox
          Left = 8
          Top = 8
          Width = 65
          Height = 17
          Alignment = taLeftJustify
          Caption = 'System'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 1
        end
        object CheckBox3: TCheckBox
          Left = 88
          Top = 8
          Width = 113
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Service Bureau'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 2
        end
        object CheckBox4: TCheckBox
          Left = 88
          Top = 24
          Width = 113
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Temp Tables'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 3
        end
        object CheckBox5: TCheckBox
          Left = 8
          Top = 24
          Width = 65
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Clients'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 4
        end
        object Edit13: TEdit
          Left = 88
          Top = 80
          Width = 113
          Height = 21
          TabOrder = 5
        end
        object CheckBox9: TCheckBox
          Left = 88
          Top = 40
          Width = 113
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Resolution'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object ScrambleCheck: TCheckBox
          Left = 8
          Top = 107
          Width = 113
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Scramble data'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          Visible = False
          OnClick = ScrambleCheckClick
        end
        object TmpDbPath: TEdit
          Left = 8
          Top = 147
          Width = 193
          Height = 21
          TabOrder = 8
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Email/FTP Config.'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 209
        Height = 210
        Align = alTop
        Caption = 'Email'
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 24
          Width = 64
          Height = 13
          Caption = 'SMTP Server'
        end
        object Label5: TLabel
          Left = 8
          Top = 64
          Width = 117
          Height = 13
          Caption = 'SMTP User ID (Optional)'
        end
        object Label6: TLabel
          Left = 8
          Top = 144
          Width = 13
          Height = 13
          Caption = 'To'
        end
        object Label15: TLabel
          Left = 8
          Top = 104
          Width = 23
          Height = 13
          Caption = 'From'
        end
        object SmtpHostEdit: TEdit
          Left = 8
          Top = 40
          Width = 193
          Height = 21
          TabOrder = 0
        end
        object SmtpUserEdit: TEdit
          Left = 8
          Top = 80
          Width = 193
          Height = 21
          TabOrder = 1
        end
        object EMailToEdit: TEdit
          Left = 8
          Top = 160
          Width = 193
          Height = 21
          TabOrder = 3
        end
        object TestEmailButton: TButton
          Left = 120
          Top = 184
          Width = 75
          Height = 20
          Caption = 'Test'
          TabOrder = 4
          OnClick = TestEmailButtonClick
        end
        object CheckBox8: TCheckBox
          Left = 8
          Top = 184
          Width = 97
          Height = 17
          Caption = 'Email Results'
          TabOrder = 5
        end
        object EMailFromEdit: TEdit
          Left = 8
          Top = 120
          Width = 193
          Height = 21
          TabOrder = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 210
        Width = 209
        Height = 369
        Align = alClient
        Caption = 'FTP'
        TabOrder = 1
        object Label7: TLabel
          Left = 8
          Top = 56
          Width = 53
          Height = 13
          Caption = 'Host Name'
        end
        object Label8: TLabel
          Left = 8
          Top = 96
          Width = 57
          Height = 13
          Caption = 'Login Name'
        end
        object Label9: TLabel
          Left = 8
          Top = 136
          Width = 46
          Height = 13
          Caption = 'Password'
        end
        object Label13: TLabel
          Left = 8
          Top = 16
          Width = 170
          Height = 13
          Caption = 'Preloaded FTP Settings (FTP.ini file)'
        end
        object Label116: TLabel
          Left = 56
          Top = 181
          Width = 41
          Height = 13
          Caption = 'FTP port'
        end
        object Label17: TLabel
          Left = 16
          Top = 208
          Width = 78
          Height = 13
          Caption = 'Secure FTP port'
        end
        object FtpHostEdit: TEdit
          Left = 8
          Top = 72
          Width = 193
          Height = 21
          TabOrder = 0
        end
        object FtpLoginEdit: TEdit
          Left = 8
          Top = 112
          Width = 193
          Height = 21
          TabOrder = 1
        end
        object FtpPasswordEdit: TEdit
          Left = 8
          Top = 152
          Width = 193
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object TestFtpButton: TButton
          Left = 120
          Top = 240
          Width = 75
          Height = 20
          Caption = 'Test'
          TabOrder = 3
          OnClick = TestFtpButtonClick
        end
        object CheckBox1: TCheckBox
          Left = 8
          Top = 264
          Width = 193
          Height = 17
          Caption = 'FTP Global Databases'
          TabOrder = 4
        end
        object BackupToFtpZippedChkBox: TCheckBox
          Left = 8
          Top = 288
          Width = 177
          Height = 17
          Caption = 'Backup to FTP (zipped)'
          TabOrder = 5
        end
        object CheckBox7: TCheckBox
          Left = 8
          Top = 312
          Width = 193
          Height = 17
          Caption = 'Restore from FTP (must be zipped)'
          TabOrder = 6
        end
        object ComboBox1: TComboBox
          Left = 8
          Top = 32
          Width = 193
          Height = 21
          ItemHeight = 0
          TabOrder = 7
          OnChange = ComboBox1Change
        end
        object SecureChkBox: TCheckBox
          Left = 8
          Top = 336
          Width = 97
          Height = 17
          Caption = 'Secure FTP'
          TabOrder = 8
        end
        object FtpPortEdit: TEdit
          Left = 104
          Top = 181
          Width = 97
          Height = 21
          TabOrder = 9
          Text = '21'
        end
        object SFtpPortEdit: TEdit
          Left = 104
          Top = 208
          Width = 97
          Height = 21
          TabOrder = 10
          Text = '22'
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Interbase Database (*.gdb)|*.gdb'
    Left = 168
    Top = 184
  end
end
