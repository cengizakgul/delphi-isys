unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, isPerfScore, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    btnRecalc: TButton;
    Label1: TLabel;
    lCPUScore: TLabel;
    Label2: TLabel;
    lMEMScore: TLabel;
    Label4: TLabel;
    lIOScore: TLabel;
    Label6: TLabel;
    lTotalScore: TLabel;
    Bevel1: TBevel;
    procedure btnRecalcClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FMeeter: TisScoreMeter;
    FCPUScore: Cardinal;
    FMEMScore: Cardinal;
    FIOScore: Cardinal;
    procedure UpdateScoreInfo;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.btnRecalcClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  btnRecalc.Enabled := False;
  btnRecalc.Update;
  try
    FCPUScore := FMeeter.CPUScore;
    FMEMScore := FMeeter.MEMScore;
    FIOScore := FMeeter.IOScore;
  finally
    btnRecalc.Enabled := True;
    Screen.Cursor := crDefault;
  end;

  UpdateScoreInfo;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FMeeter := TisScoreMeter.Create;
  UpdateScoreInfo;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FMeeter.Free;
end;

procedure TForm1.UpdateScoreInfo;
begin
  lCPUScore.Caption := IntToStr(FCPUScore);
  lMEMScore.Caption := IntToStr(FMEMScore);
  lIOScore.Caption := IntToStr(FIOScore);
  lTotalScore.Caption := IntToStr(FCPUScore + FMEMScore + FIOScore);
end;

end.



