object Form1: TForm1
  Left = 458
  Top = 226
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Evo Score'
  ClientHeight = 206
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 13
    Top = 17
    Width = 67
    Height = 16
    Caption = 'CPU Score'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lCPUScore: TLabel
    Left = 166
    Top = 18
    Width = 77
    Height = 16
    Alignment = taRightJustify
    Caption = 'lCPUScore'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 12
    Top = 40
    Width = 88
    Height = 16
    Caption = 'Memory Score'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lMEMScore: TLabel
    Left = 163
    Top = 41
    Width = 80
    Height = 16
    Alignment = taRightJustify
    Caption = 'lMEMScore'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 12
    Top = 66
    Width = 56
    Height = 16
    Caption = 'I/O Score'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lIOScore: TLabel
    Left = 182
    Top = 67
    Width = 61
    Height = 16
    Alignment = taRightJustify
    Caption = 'lIOScore'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 12
    Top = 105
    Width = 82
    Height = 16
    Caption = 'Total Score'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lTotalScore: TLabel
    Left = 161
    Top = 106
    Width = 82
    Height = 16
    Alignment = taRightJustify
    Caption = 'lTotalScore'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 7
    Top = 92
    Width = 243
    Height = 9
    Shape = bsTopLine
  end
  object btnRecalc: TButton
    Left = 88
    Top = 161
    Width = 75
    Height = 25
    Caption = 'Recalculate'
    TabOrder = 0
    OnClick = btnRecalcClick
  end
end
