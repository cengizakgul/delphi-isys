unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdUDPBase, IdUDPServer,  IdSocketHandle,
  StdCtrls, ExtCtrls, ComCtrls, ImgList;

const
  WM_UDP_RECEIVED_9469 = WM_USER + 11;

type
  TForm1 = class(TForm)
    IdUDPServer9469: TIdUDPServer;
    Panel3: TPanel;
    Label2: TLabel;
    tvSources: TTreeView;
    ImageList: TImageList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure IdUDPServer9469UDPRead(Sender: TObject; AData: TStream;
      ABinding: TIdSocketHandle);
  private
    FTopNode: TTreeNode;
    FNode9469: TTreeNode;
    procedure WMUDPRRecieved9469(var Message: TMessage); message WM_UDP_RECEIVED_9469;
    procedure SetPicture(ANode: TTreeNode; AImageIndex: Integer);
  public
  end;

  TTreeNodeIP = class(TTreeNode)
  private
    FPacketData: String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  IdUDPServer9469.Active := False;  
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   FTopNode := tvSources.Items.Add(nil, 'Evolution UDP');
   SetPicture(FTopNode, 0);

   FNode9469 := tvSources.Items.AddChild(FTopNode, 'Port 9469 (DB data change event)');
   SetPicture(FNode9469, 1);

   FTopNode.Expanded := True;
end;

procedure TForm1.IdUDPServer9469UDPRead(Sender: TObject; AData: TStream; ABinding: TIdSocketHandle);
var
  s: PString;
  ip: PString;
begin
  New(s);
  New(ip);

  SetLength(s^, AData.Size);
  AData.Position := 0;
  AData.Read(s^[1], AData.Size);

  ip^ := ABinding.PeerIP;
  PostMessage(Handle, WM_UDP_RECEIVED_9469, Cardinal(s), Cardinal(ip));
end;

procedure TForm1.WMUDPRRecieved9469(var Message: TMessage);
var
  RDHost: String;
  s, ip: PString;
  i: Integer;
  Nrd: TTreeNode;
  Nip: TTreeNodeIP;
begin
  try
    s := PString(Pointer(Message.WParam));
    ip := PString(Pointer(Message.LParam));

    i := Pos('RD=', s^);
    if i = 0 then
      RDHost := 'Unknown'
    else
    begin
      RDHost := Copy(s^, i + 3, Length(s^) - i - 2);
      i := Pos(' ', RDHost);
      RDHost := Copy(RDHost, 1, i - 1);
    end;

    Nrd := nil;
    for i := 0 to FNode9469.Count - 1 do
     if FNode9469.Item[i].Text = RDHost then
     begin
       Nrd := FNode9469.Item[i];
       break;
     end;

     if not Assigned(Nrd) then
     begin
       Nrd := tvSources.Items.AddChild(FNode9469, RDHost);
       SetPicture(Nrd, 2);
     end;


    Nip := nil;
    for i := 0 to Nrd.Count - 1 do
     if Nrd.Item[i].Text = ip^ then
     begin
       Nip := Nrd.Item[i] as TTreeNodeIP;
       break;
     end;


     if not Assigned(Nip) then
     begin
       Nip := TTreeNodeIP.Create(tvSources.Items);
       SetPicture(Nip, 3);
       tvSources.Items.AddNode(Nip, Nrd, ip^, nil, naAddChild);
     end;

     Nip.FPacketData := s^;
   finally
     Dispose(s);
     Dispose(ip);
   end;
end;

procedure TForm1.SetPicture(ANode: TTreeNode; AImageIndex: Integer);
begin
  ANode.ImageIndex := AImageIndex;
  ANode.SelectedIndex := AImageIndex;
  ANode.StateIndex := AImageIndex;  
end;

end.
