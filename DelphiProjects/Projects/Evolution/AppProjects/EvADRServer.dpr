program EvADRServer;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  EvServerStart,
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_evTCPClient in '..\EvoClient\EvolutionGUI\mod_evTCPClient.pas',
  mod_EvolutionGUIPlug in '..\EvoClient\EvolutionGUI\mod_EvolutionGUIPlug.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  mod_ADR_Server in '..\Modules\ADR_Server\mod_ADR_Server.pas',

// Debugging
{  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_EMailer in '..\Modules\EMailer\mod_EMailer.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_ADR_Client in '..\Modules\ADR_Client\mod_ADR_Client.pas';
}
  pxy_GlobalSettings in '..\Modules\Global_Settings\pxy_GlobalSettings.pas',
  pxy_License in '..\Modules\License\pxy_License.pas',
  pxy_Security in '..\Modules\Security\pxy_Security.pas',
  pxy_GlobalTimeSource in '..\Modules\Global_Time_Source\pxy_GlobalTimeSource.pas',
  pxy_EMailer in '..\Modules\EMailer\pxy_EMailer.pas',
  pxy_DB_Access in '..\Modules\DB_Access\pxy_DB_Access.pas',
  pxy_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\pxy_GlobalFlagsManager.pas',
  pxy_ADR_Client in '..\Modules\ADR_Client\pxy_ADR_Client.pas';

{$R *.res}
begin
  RunEvoServer(EvoADRServerAppInfo.Name, EvoADRServerAppInfo.AppID);
end.
