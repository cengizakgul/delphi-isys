program CheckTables;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  DataModule in '..\Utilities\CheckTables\DataModule.pas' {DM: TDataModule},
  MainForm in '..\Utilities\CheckTables\MainForm.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Check tables utility';
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
