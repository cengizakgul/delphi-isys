program EvDBMerge;

uses
  Forms,
  MainFrm in '..\Utilities\EvDBMerge\MainFrm.pas' {Main},
  Processing in '..\Utilities\EvDBMerge\Processing.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Evo DB Merge';
  Application.CreateForm(TMain, Main);
  Application.Run;
end.
