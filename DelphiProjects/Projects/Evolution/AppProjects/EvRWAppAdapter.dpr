library EvRWAppAdapter;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isBasicUtils,
  isAppIDs,
  mod_evTCPClient in '..\EvoClient\EvolutionGUI\mod_evTCPClient.pas',
  mod_RAS_Access in '..\Modules\RAS_Access\mod_RAS_Access.pas',
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_Data_Access_Light in '..\Modules\Data_Access\mod_Data_Access_Light.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',    
  pxy_Security in '..\Modules\Security\pxy_Security.pas',
  pxy_DB_Access in '..\Modules\DB_Access\pxy_DB_Access.pas',
  pxy_License in '..\Modules\License\pxy_License.pas',
  pxy_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\pxy_GlobalFlagsManager.pas',
  pxy_GlobalTimeSource in '..\Modules\Global_Time_Source\pxy_GlobalTimeSource.pas',
  pxy_Payroll_Check_Print in '..\Modules\Payroll_Check_Print\pxy_Payroll_Check_Print.pas',
  EvoRWAdapterDM in '..\EvoClient\rwAppAdapter\EvoRWAdapterDM.pas' {EvRWAdapter: TDataModule},
  evQBTlbCompaniesFrm in '..\EvoClient\rwAppAdapter\evQBTlbCompaniesFrm.pas' {evQBTlbCompanies};

{$R *.res}

begin
  SetAppID(EvoRWAdapterAppInfo.AppID);
  CheckIfTampered;
end.
