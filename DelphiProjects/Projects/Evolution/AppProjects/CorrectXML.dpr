program CorrectXML;

uses
  Forms,
  CorrectNMXMLFrm in '..\Utilities\CorrectXML\CorrectNMXMLFrm.pas' {CorrectNMXML},
  Postprocess in '..\Utilities\CorrectXML\Postprocess.pas';

{$R *.res}
begin
  Application.Initialize;
  Application.Title := 'Correct XML';

  FillReportList;  // before creation form;

  if ParamCount > 0 then
  begin
    MainBackgroundLoop;
  end
  else
  begin
    Application.CreateForm(TCorrectNMXML, CorrectNMXML);
    Application.Run;
  end;

end.
