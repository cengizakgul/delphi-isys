program EvRequestBroker;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  EvServerStart,
  pxy_Async_Funct in '..\Modules\Async_Funct\pxy_Async_Funct.pas',
  pxy_DB_Access in '..\Modules\DB_Access\pxy_DB_Access.pas',
  pxy_RW_Engine_Remote in '..\Modules\RW_Engine_Remote\pxy_RW_Engine_Remote.pas',
  pxy_Security in '..\Modules\Security\pxy_Security.pas',
  mod_GlobalCallbacksBrk in '..\Modules\Global_Callbacks\mod_GlobalCallbacksBrk.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_EMailer in '..\Modules\EMailer\mod_EMailer.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_RW_Engine_Local in '..\Modules\RW_Engine_Local\mod_RW_Engine_Local.pas',
  mod_Scheduler in '..\Modules\Scheduler\mod_Scheduler.pas',
  mod_Task_Queue in '..\Modules\Task_Queue\mod_Task_Queue.pas',
  mod_VMR_Local in '..\Modules\VMR_Local\mod_VMR_Local.pas',
  mod_VMR_Remote in '..\Modules\VMR_Remote\mod_VMR_Remote.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_Version_Update_Remote in '..\Modules\Version_Update_Remote\mod_Version_Update_Remote.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  mod_Single_Sign_On in '..\Modules\Single_Sign_On\mod_Single_Sign_On.pas',
  mod_DashboardDataProvider in '..\Modules\Dashboard_Data_Provider\mod_DashboardDataProvider.pas',
  mod_BBClient in '..\Modules\BB_Client\mod_BBClient.pas',
  mod_RBServer in '..\EvoServer\RequestBroker\mod_RBServer.pas';


{$R *.res}

begin
  RunEvoServer(EvoRequestBrokerAppInfo.Name, EvoRequestBrokerAppInfo.AppID);
end.
