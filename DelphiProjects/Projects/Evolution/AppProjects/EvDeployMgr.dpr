program EvDeployMgr;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  isServerServiceFrm,
  EvDeploymentController in '..\EvoServer\DeploymentManager\EvDeploymentController.pas';

{$R *.res}

procedure _Init;
begin
  DeploymentController := TevDeploymentController.Create;
  DeploymentController.Active := True;
end;

procedure _Deinit;
begin
  DeploymentController := nil;
end;

begin
  RunISServer(EvoDeployMgrAppInfo.Name, EvoDeployMgrAppInfo.Name, EvoDeployMgrAppInfo.AppID,
    @_Init, @_Deinit, nil);
end.
