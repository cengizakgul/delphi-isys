program EvRequestProcSA;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  EvServerStart,
  mod_GlobalCallbacksBrk in '..\Modules\Global_Callbacks\mod_GlobalCallbacksBrk.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_API in '..\Modules\API\mod_API.pas',
  mod_Async_Funct in '..\Modules\Async_Funct\mod_Async_Funct.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_EMailer in '..\Modules\EMailer\mod_EMailer.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_Misc_Calc in '..\Modules\Misc_Calc\mod_Misc_Calc.pas',
  mod_Payroll_Calc in '..\Modules\Payroll_Calc\mod_Payroll_Calc.pas',
  mod_Payroll_Check_Print in '..\Modules\Payroll_Check_Print\mod_Payroll_Check_Print.pas',
  mod_ACH in '..\Modules\ACH\mod_ACH.pas',
  mod_Payroll_Process in '..\Modules\Payroll_Process\mod_Payroll_Process.pas',
  mod_RW_Engine_Local in '..\Modules\RW_Engine_Local\mod_RW_Engine_Local.pas',
  mod_RW_Engine_Remote in '..\Modules\RW_Engine_Remote\mod_RW_Engine_Remote.pas',
  mod_Scheduler in '..\Modules\Scheduler\mod_Scheduler.pas',
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_Task_Queue in '..\Modules\Task_Queue\mod_Task_Queue.pas',
  mod_VMR_Local in '..\Modules\VMR_Local\mod_VMR_Local.pas',
  mod_VMR_Remote in '..\Modules\VMR_Remote\mod_VMR_Remote.pas',
  mod_Version_Update_Remote in '..\Modules\Version_Update_Remote\mod_Version_Update_Remote.pas',
  mod_BBClient in '..\Modules\BB_Client\mod_BBClient.pas',
  mod_RPServer in '..\EvoServer\RequestProcessor\mod_RPServer.pas',
  mod_EvoX_Remote in '..\Modules\EvoX_Remote\mod_EvoX_Remote.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  mod_Single_Sign_On in '..\Modules\Single_Sign_On\mod_Single_Sign_On.pas',
  mod_DashboardDataProvider in '..\Modules\Dashboard_Data_Provider\mod_DashboardDataProvider.pas';
  
{$R *.res}

begin
  RunEvoServer(EvoRequestProcSAAppInfo.Name, EvoRequestProcSAAppInfo.AppID);
end.
