library EvRWAppAdapterSA;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isBasicUtils,
  isAppIDs,
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_Data_Access_Light in '..\Modules\Data_Access\mod_Data_Access_Light.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  mod_Payroll_Check_Print in '..\Modules\Payroll_Check_Print\mod_Payroll_Check_Print.pas',
  EvoRWAdapterDM in '..\EvoClient\rwAppAdapter\EvoRWAdapterDM.pas' {EvRWAdapter: TDataModule},
  evQBTlbCompaniesFrm in '..\EvoClient\rwAppAdapter\evQBTlbCompaniesFrm.pas' {evQBTlbCompanies};

{$R *.res}

begin
  SetAppID(EvoSARWAdapterAppInfo.AppID);
  CheckIfTampered;
end.
