program BBConfigurator;

uses
  {$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  BBConfigMainFormUnit in '..\Utilities\BigBrother\BBConfigMainFormUnit.pas' {BBConfigMainForm},
  BBConfigDataUnit in '..\Utilities\BigBrother\BBConfigDataUnit.pas' {BBConfigData: TDataModule},
  SBLicenseUnit in '..\Utilities\BigBrother\SBLicenseUnit.pas',
  SBLicenseReaderUnit in '..\Utilities\BigBrother\SBLicenseReaderUnit.pas',
  SBLicenseWriterUnit in '..\Utilities\BigBrother\SBLicenseWriterUnit.pas',
  UnitShowDefaultsForm in '..\Utilities\BigBrother\UnitShowDefaultsForm.pas' {ShowDefaultForm},
  EditFormUnit in '..\Utilities\BigBrother\EditFormUnit.pas' {EditForm},
  UpdateReportFormUnit in '..\Utilities\BigBrother\UpdateReportFormUnit.pas' {UpdateReportForm},
  TasksListFormUnit in '..\Utilities\BigBrother\TasksListFormUnit.pas' {TasksListForm},
  VersionChoiseFormUnit in '..\Utilities\BigBrother\VersionChoiseFormUnit.pas' {VersionChoiseForm},
  VendorsFormUnit in '..\Utilities\BigBrother\VendorsFormUnit.pas' {VendorsForm},
  APIKeysEditFormUnit in '..\Utilities\BigBrother\APIKeysEditFormUnit.pas' {APIKeysEditForm},
  GeneratorFormUnit in '..\Utilities\BigBrother\GeneratorFormUnit.pas' {GeneratorForm},
  BBUtilsUnit in '..\Utilities\BigBrother\BBUtilsUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TBBConfigData, BBConfigData);
  Application.CreateForm(TBBConfigMainForm, BBConfigMainForm);
  Application.CreateForm(TShowDefaultForm, ShowDefaultForm);
  Application.CreateForm(TEditForm, EditForm);
  Application.CreateForm(TUpdateReportForm, UpdateReportForm);
  Application.CreateForm(TTasksListForm, TasksListForm);
  Application.CreateForm(TVendorsForm, VendorsForm);
  Application.CreateForm(TAPIKeysEditForm, APIKeysEditForm);
  Application.CreateForm(TGeneratorForm, GeneratorForm);
  Application.Run;
end.
