program EvAPIAdapter;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  EvServerStart,
  pxy_License in '..\Modules\License\pxy_License.pas',
  pxy_Misc_Calc in '..\Modules\Misc_Calc\pxy_Misc_Calc.pas',
  mod_Payroll_Calc in '..\Modules\Payroll_Calc\mod_Payroll_Calc.pas',
  pxy_Payroll_Check_Print in '..\Modules\Payroll_Check_Print\pxy_Payroll_Check_Print.pas',
  pxy_Payroll_Process in '..\Modules\Payroll_Process\pxy_Payroll_Process.pas',
  pxy_VMR_Remote in '..\Modules\VMR_Remote\pxy_VMR_Remote.pas',
  pxy_DB_Access in '..\Modules\DB_Access\pxy_DB_Access.pas',
  pxy_RW_Engine_Remote in '..\Modules\RW_Engine_Remote\pxy_RW_Engine_Remote.pas',
  pxy_Security in '..\Modules\Security\pxy_Security.pas',
  pxy_Task_Queue in '..\Modules\Task_Queue\pxy_Task_Queue.pas',
  pxy_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\pxy_GlobalFlagsManager.pas',
  pxy_GlobalTimeSource in '..\Modules\Global_Time_Source\pxy_GlobalTimeSource.pas',
  pxy_EMailer in '..\Modules\EMailer\pxy_EMailer.pas',
  pxy_EvoX_Remote in '..\Modules\EvoX_Remote\pxy_EvoX_Remote.pas',
  pxy_Single_Sign_On in '..\Modules\Single_Sign_On\pxy_Single_Sign_On.pas',  
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_APIEvolutionGUIPlug in '..\EvoServer\APIAdapter\mod_APIEvolutionGUIPlug.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_evTCPClient in '..\EvoClient\EvolutionGUI\mod_evTCPClient.pas',
  mod_EvoX_Local in '..\Modules\EvoX_Local\mod_EvoX_Local.pas',
  mod_RW_Engine_Local in '..\Modules\RW_Engine_Local\mod_RW_Engine_Local.pas',
  mod_VMR_Local in '..\Modules\VMR_Local\mod_VMR_Local.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  mod_API in '..\Modules\API\mod_API.pas';

{$R *.res}

begin
  RunEvoServer(EvoAPIAdapterAppInfo.Name, EvoAPIAdapterAppInfo.AppID);
end.
