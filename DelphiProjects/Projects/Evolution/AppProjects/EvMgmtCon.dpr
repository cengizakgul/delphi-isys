program EvMgmtCon;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  isServerServiceFrm,
  mcTypes in '..\EvoServer\ManagementConsole\mcTypes.pas',
  mcManagementConsole in '..\EvoServer\ManagementConsole\mcManagementConsole.pas',
  mcEvolutionControl in '..\EvoServer\ManagementConsole\mcEvolutionControl.pas',
  mcHTTPPresenter in '..\EvoServer\ManagementConsole\mcHTTPPresenter.pas';

{$R *.res}

procedure _Init;
begin
  Mainboard := TmcMainboard.Create;
  Mainboard.Initialize;
end;

procedure _Deinit;
begin
  Mainboard := nil;
end;

begin
  RunISServer(EvoMgmtConsoleAppInfo.Name, EvoMgmtConsoleAppInfo.Name, EvoMgmtConsoleAppInfo.AppID,
    @_Init, @_Deinit, nil);
end.
