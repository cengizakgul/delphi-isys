program IsDBTest;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  IsDBTestFrm in '..\Utilities\IsDBTest\IsDBTestFrm.pas' {Form5},
  dtDBAccess in '..\Utilities\IsDBTest\dtDBAccess.pas',
  dtTest in '..\Utilities\IsDBTest\dtTest.pas',
  dtConnectionTest in '..\Utilities\IsDBTest\dtConnectionTest.pas',
  dtFullTest in '..\Utilities\IsDBTest\dtFullTest.pas',
  dtStoredProcTest in '..\Utilities\IsDBTest\dtStoredProcTest.pas',
  dtDataLoadTest in '..\Utilities\IsDBTest\dtDataLoadTest.pas',
  dtSettings in '..\Utilities\IsDBTest\dtSettings.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm5, Form5);
  Application.Run;
end.
