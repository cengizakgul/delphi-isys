program EvBigBrother;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  SysUtils,
  Windows,
  Forms,
  SvcMgr,
  MainUnit in '..\Utilities\BigBrother\MainUnit.pas' {EvBigBrotherService10: TService},
  ConnectionsPoolUnit in '..\Utilities\BigBrother\ConnectionsPoolUnit.pas',
  GlovalObjectsUnit in '..\Utilities\BigBrother\GlovalObjectsUnit.pas',
  LazyEmailNotifierUnit in '..\Utilities\BigBrother\LazyEmailNotifierUnit.pas',
  MyTCPServerUnit in '..\Utilities\BigBrother\MyTCPServerUnit.pas',
  NewBBUnit in '..\Utilities\BigBrother\NewBBUnit.pas',
  SBLicenseWriterUnit in '..\Utilities\BigBrother\SBLicenseWriterUnit.pas',
  SBLicenseReaderUnit in '..\Utilities\BigBrother\SBLicenseReaderUnit.pas',
  SBLicenseUnit in '..\Utilities\BigBrother\SBLicenseUnit.pas',
  BBMainFormUnit in '..\Utilities\BigBrother\BBMainFormUnit.pas' {BBMainForm},
  EventReceiverUnit in '..\Utilities\BigBrother\EventReceiverUnit.pas',
  RDProxyUnit in '..\Utilities\BigBrother\RDProxyUnit.pas',
  BBConnection in '..\Utilities\BigBrother\BBConnection.pas',
  BBUtilsUnit in '..\Utilities\BigBrother\BBUtilsUnit.pas';

{$R *.RES}

begin
  if (ParamCount = 1) and (UpperCase(ParamStr(1)) = '/EXE') then
  begin
    Forms.Application.Initialize;
    Forms.Application.CreateForm(TBBMainForm, BBMainForm);
  Forms.Application.Run;
  end
  else
  begin
    SvcMgr.Application.Initialize;
    SvcMgr.Application.CreateForm(TEvBigBrotherService10, EvBigBrotherService10);
    SvcMgr.Application.Run;
  end;
end.
