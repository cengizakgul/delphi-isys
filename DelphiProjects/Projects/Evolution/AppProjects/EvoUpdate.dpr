program EvoUpdate;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
{$IFDEF EVO9}
  mod_evTCPClient,
  pxy_Security,
  mod_Version_Update_Local,
  pxy_Version_Update_Remote,
  EvUpdate10 in '..\Utilities\EvoUpdate\EvUpdate10.pas',
{$ENDIF}

{$IFDEF ROLLBACK}
  EvRollback in '..\Utilities\EvoUpdate\EvRollback.pas',
{$ENDIF}

  EvUpdate in '..\Utilities\EvoUpdate\EvUpdate.pas';


{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'EvoUpdate';
  RunUpdate;
end.
