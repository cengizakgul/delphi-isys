program SetDefaultRights;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  MainForm in '..\Utilities\SetDefaultRights\MainForm.pas' {Form1},
  DataModule in '..\Utilities\SetDefaultRights\DataModule.pas' {DM: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Set Default Rights utility';
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
