program EvoImportTaxes;

uses
  {$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  isAppIDs,
  EvClientStart,
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_EMailer in '..\Modules\EMailer\mod_EMailer.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_StatisticsClt in '..\Modules\Statistics\mod_StatisticsClt.pas',
  EvoImportTaxesGUI in '..\Utilities\EvoImportTaxes\EvoImportTaxesGUI.pas',
  ImportTaxesFrm in '..\Utilities\EvoImportTaxes\ImportTaxesFrm.pas' {ImportTaxesForm};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'EvoImportTaxes';

  RunEvClient('EvoImportTaxes', EvoSAAppInfo.AppID);
end.
