program EvADRClient;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  EvServerStart,
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_evTCPClient in '..\EvoClient\EvolutionGUI\mod_evTCPClient.pas',
  mod_EvolutionGUIPlug in '..\EvoClient\EvolutionGUI\mod_EvolutionGUIPlug.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  mod_ADR_Client in '..\Modules\ADR_Client\mod_ADR_Client.pas',
  pxy_GlobalSettings in '..\Modules\Global_Settings\pxy_GlobalSettings.pas',
  pxy_License in '..\Modules\License\pxy_License.pas',
  pxy_Security in '..\Modules\Security\pxy_Security.pas',
  pxy_GlobalTimeSource in '..\Modules\Global_Time_Source\pxy_GlobalTimeSource.pas',
  pxy_EMailer in '..\Modules\EMailer\pxy_EMailer.pas',
  pxy_DB_Access in '..\Modules\DB_Access\pxy_DB_Access.pas',
  pxy_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\pxy_GlobalFlagsManager.pas',
  pxy_ADR_Server in '..\Modules\ADR_Server\pxy_ADR_Server.pas';

{$R *.res}
begin
  RunEvoServer(EvoADRClientAppInfo.Name, EvoADRClientAppInfo.AppID);
end.
