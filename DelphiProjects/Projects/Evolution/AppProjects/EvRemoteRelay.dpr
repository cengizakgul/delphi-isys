program EvRemoteRelay;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  isServerServiceFrm,
  evTypes,
  evExceptions,
  evRemoteRelayServer in '..\EvoServer\RemoteRelay\evRemoteRelayServer.pas';


{$R *.res}

procedure _Init;
begin
  Mainboard := TevRRMainboard.Create;
  Mainboard.Initialize;
end;

procedure _Deinit;
begin
  Mainboard := nil;
end;

begin
  RunISServer(EvoRemoteRelayAppInfo.Name, EvoRemoteRelayAppInfo.Name, EvoRemoteRelayAppInfo.AppID,
              @_Init, @_Deinit, nil);
end.
