program EBackup;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  Forms,
  Unit1 in '..\Utilities\EBackup\Unit1.pas' {Backup},
  Unit2 in '..\Utilities\EBackup\Unit2.pas',
  Unit3 in '..\Utilities\EBackup\Unit3.pas' {LogViewer},
  evBackupUploader in '..\Utilities\EBackup\evBackupUploader.pas',
  evSFtpBackupUploader in '..\Utilities\EBackup\evSFtpBackupUploader.pas',
  ScramblerUnit in '..\Utilities\EBackup\ScramblerUnit.pas',
  SSNScramblerUnit in '..\Utilities\EBackup\SSNScramblerUnit.pas';

{$R *.RES}
begin
  Application.Initialize;
  if ParamExists('/hidden') then
  begin
    MainBackgroundLoop;
  end
  else
  begin
    Application.CreateForm(TBackup, Backup);
  Application.Run;
  end;
end.
