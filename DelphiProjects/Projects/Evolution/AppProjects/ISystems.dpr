program ISystems;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  isGUIApp,
  EvInitApp,
  EvContext,
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_EMailer in '..\Modules\EMailer\mod_EMailer.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  EvISystemsGUIPackage in '..\Utilities\ISystems\EvISystemsGUIPackage.pas',
  MainForm in '..\Utilities\ISystems\Code_Generator\MainForm.pas' {MainFormFrm: TFrame},
  evLicenseCodeGenerator in '..\Utilities\ISystems\Code_Generator\evLicenseCodeGenerator.pas',
  SMainForm in '..\Utilities\ISystems\DAT_Creator\SMainForm.pas' {MainCreator: TFrame},
  SSMainForm in '..\Utilities\ISystems\Security_Shell\SSMainForm.pas' {frm: TFrame},
  MainFrm in '..\Utilities\ISystems\SyncReports\MainFrm.pas' {frmSyncReport: TFrame},
  Unit1 in '..\Utilities\ISystems\Client_Mover\Unit1.pas' {FormA1: TFrame},
  ISMain in '..\Utilities\ISystems\ISMain.pas' {Form4},
  sfldeMainForm in '..\Utilities\ISystems\FieldLevelSecurityEditor\sfldeMainForm.pas' {flseMainForm: TFrame},
  EvSystemDBUtilsFrm in '..\Utilities\ISystems\SystemDB\EvSystemDBUtilsFrm.pas' {EvSystemDBUtils: TFrame};

{$R *.res}

procedure _Init;
begin
  InitializeEvoApp;

  ctx_StartWait('Please wait while Evolution is loading ...');
  try
    ctx_EvolutionGUI.CreateMainForm;
  finally
    ctx_EndWait;
  end;
end;


procedure _Deinit;
begin
  ctx_EvolutionGUI.DestroyMainForm;
  UninitializeEvoApp;
end;


begin
  RunISGUI(EvoUtilISystemsAppInfo.Name, EvoUtilISystemsAppInfo.AppID, @_Init, @_Deinit);
end.
