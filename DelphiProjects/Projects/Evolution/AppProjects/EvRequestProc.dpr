program EvRequestProc;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  isAppIDs,
  EvServerStart,
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_Async_Funct in '..\Modules\Async_Funct\mod_Async_Funct.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_Misc_Calc in '..\Modules\Misc_Calc\mod_Misc_Calc.pas',
  mod_Payroll_Calc in '..\Modules\Payroll_Calc\mod_Payroll_Calc.pas',
  mod_Payroll_Check_Print in '..\Modules\Payroll_Check_Print\mod_Payroll_Check_Print.pas',
  mod_Payroll_Process in '..\Modules\Payroll_Process\mod_Payroll_Process.pas',
  mod_ACH in '..\Modules\ACH\mod_ACH.pas',
  mod_RW_Engine_Local in '..\Modules\RW_Engine_Local\mod_RW_Engine_Local.pas',
  mod_RW_Engine_Remote in '..\Modules\RW_Engine_Remote\mod_RW_Engine_Remote.pas',
  mod_GlobalCallbacksSrv in '..\Modules\Global_Callbacks\mod_GlobalCallbacksSrv.pas',
  pxy_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\pxy_GlobalFlagsManager.pas',
  pxy_GlobalSettings in '..\Modules\Global_Settings\pxy_GlobalSettings.pas',
  pxy_GlobalTimeSource in '..\Modules\Global_Time_Source\pxy_GlobalTimeSource.pas',
  pxy_EMailer in '..\Modules\EMailer\pxy_EMailer.pas',
  pxy_Task_Queue in '..\Modules\Task_Queue\pxy_Task_Queue.pas',
  pxy_License in '..\Modules\License\pxy_License.pas',
  pxy_VMR_Remote in '..\Modules\VMR_Remote\pxy_VMR_Remote.pas',
  mod_VMR_Local in '..\Modules\VMR_Local\mod_VMR_Local.pas',
  mod_RPServer in '..\EvoServer\RequestProcessor\mod_RPServer.pas',
  mod_EvoX_Remote in '..\Modules\EvoX_Remote\mod_EvoX_Remote.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas',
  pxy_Single_Sign_On in '..\Modules\Single_Sign_On\pxy_Single_Sign_On.pas',
  mod_DashboardDataProvider in '..\Modules\Dashboard_Data_Provider\mod_DashboardDataProvider.pas';

{$R *.res}

begin
  RunEvoServer(EvoRequestProcAppInfo.Name, EvoRequestProcAppInfo.AppID);
end.
