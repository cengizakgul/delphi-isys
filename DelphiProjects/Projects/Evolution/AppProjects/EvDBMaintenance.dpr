program EvDBMaintenance;

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManagerErrorStack.inc}
  SysUtils,
  isAppIDs,
  isGUIApp,
  EvInitApp,
  EvContext,
  EvDBMaintGUI,
  isBasicUtils,
  isSingleInstanceApp,
  evAutonomousMode,
  evDBMaintController,
  evControllerInterfaces,
  mod_Security in '..\Modules\Security\mod_Security.pas',
  mod_EMailer in '..\Modules\EMailer\mod_EMailer.pas',
  mod_Data_Access in '..\Modules\Data_Access\mod_Data_Access.pas',
  mod_DB_Access in '..\Modules\DB_Access\mod_DB_Access.pas',
  mod_DB_Maintenance in '..\Modules\DB_Maintenance\mod_DB_Maintenance.pas',
  mod_License in '..\Modules\License\mod_License.pas',
  mod_GlobalCallbacksClt in '..\Modules\Global_Callbacks\mod_GlobalCallbacksClt.pas',
  mod_GlobalFlagsManager in '..\Modules\Global_Flags_Manager\mod_GlobalFlagsManager.pas',
  mod_GlobalSettings in '..\Modules\Global_Settings\mod_GlobalSettings.pas',
  mod_GlobalTimeSource in '..\Modules\Global_Time_Source\mod_GlobalTimeSource.pas',
  mod_StatisticsSrv in '..\Modules\Statistics\mod_StatisticsSrv.pas';

{$R *.RES}

var Controller: IevAppController;

procedure _Init;
begin
  CheckCondition(CheckSingleInstanceApp(AppID), ExtractFileName(AppFileName) + ' is already running');

  InitializeEvoApp;

  Controller := TevDBMController.Create;
  Controller.Active := True;

  if AppSwitches.Count = 0 then
    ctx_EvolutionGUI.CreateMainForm
  else
    ExecAutonomousMode;
end;


procedure _Deinit;
begin
  Controller.Active := False;
  Controller := nil;
  ctx_EvolutionGUI.DestroyMainForm;
  UninitializeEvoApp;
end;


begin
  RunISGUI(EvoUtilDBMaintenanceAppInfo.Name, EvoUtilDBMaintenanceAppInfo.AppID, @_Init, @_Deinit);
end.
