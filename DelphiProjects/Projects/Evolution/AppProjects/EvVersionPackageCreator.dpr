program EvVersionPackageCreator;

{$APPTYPE CONSOLE}
{$R *.RES}

uses
{$INCLUDE  ..\..\..\Common\IsClasses\isMemoryManager.inc}
  SysUtils,
  Main in '..\Utilities\EvVersionPackageCreator\Main.pas';

begin
  try
    Run;
  except
    on E: Exception do
    begin
      Writeln(ErrOutput, E.Message);
      ExitCode := 1;
    end;
  end;
end.
