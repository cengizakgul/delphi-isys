#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <sys/utsname.h>
#include <time.h>
#include <unistd.h>		// for readlink()
#include <sys/time.h>		// for gettimeofday()
#include "global.h"

/*
 * return pointer to first digit in string
 * return NULL if digit was not found
 */
char *findDigit(char *lpcStr)
{
 char *lpcRes=NULL;
 if (lpcStr!=NULL)
  {
   while(*lpcStr!=0 && lpcRes==NULL)
    {
     if(*lpcStr>=48 && *lpcStr<=57)
      lpcRes=lpcStr;
     lpcStr++;
    }
  }
 return lpcRes;
}

/*
 * return pointer to first digit in string
 * return NULL if digit was not found
 */
char *findNoDigit(char *lpcStr)
{
 char *lpcRes=NULL;
 if (lpcStr!=NULL)
  {
   while(*lpcStr!=0 && lpcRes==NULL)
    {
     if(*lpcStr<48 || *lpcStr>57)
      lpcRes=lpcStr;
     lpcStr++;
    }
  }
 return lpcRes;
}

/*
 * return first positive number that found in string
 * return -1 if number was not found
 */
long long int getNumber(const char *lpcStr)
{
 long long int liNum=-1;
 if (lpcStr!=NULL)
  {
   lpcStr=findDigit((char *)lpcStr);
   if(lpcStr!=NULL)
    {
     liNum=0;
     while(*lpcStr>=48 && *lpcStr<=57)
      {
       liNum=liNum*10+*lpcStr-48;
       lpcStr++;
      }
    }
  }
 return liNum;
}

/*
 * Return pseudounique identifier: pid_sec_msec
 */
void myUID(char *lpcUID)
{
 pid_t liPid;
 struct timeval ltv;

 liPid=getpid();
 gettimeofday(&ltv,NULL);
 sprintf(lpcUID,"%ld_%ld_%ld", (long)liPid, (long)ltv.tv_sec, (long)ltv.tv_usec);
}

int isSysdba(const char *lpcUserName)
{
  return strcmp(lpcUserName, "SYSDBA") == 0 ? 1 : 0;
}
