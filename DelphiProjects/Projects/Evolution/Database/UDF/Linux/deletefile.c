/* deletefile.c
 *
 * 	Given the name of a file, delete it on the DB server side.
 * 	The low-numbered returns (0..1) are explicitly tested for
 * 	in the stored procedure code, but >= 100 encode errno and
 * 	should be decoded by the user.
 */
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/* Result
 * * 0 - OK
 * * 1 - Source file doesn't exist
 * * >100 - 100+errno
 * */

#define DELETEFILE_OK		0	// all OK
#define DELETEFILE_NOEXIST	1	// file doesn't exist
#define DELETEFILE_ERRNO	100	// biased errno

int DeleteFile(const char *lpcFilename)
{
	const int rc = unlink(lpcFilename);
	const int err = errno;

	if (rc == 0)
		return DELETEFILE_OK;

	// Pick off the errors we want to handle separately
	switch (err)
	{
	  case ENOENT:
		return DELETEFILE_NOEXIST;

	  default:
		return (DELETEFILE_ERRNO + err);
	}
}


#if TESTING
int main()
{
	const char *fname = "/tmp/testfile";

	const int rc = DeleteFile(fname);

	printf("DeleteFile(%s) --> %d\n", fname, rc);

	if (rc >= 100)	printf("error = %s\n", strerror(rc-100));

	return EXIT_SUCCESS;

}
#endif
