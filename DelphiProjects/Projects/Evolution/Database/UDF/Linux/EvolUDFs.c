#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <limits.h>
#include "global.h"

#define MAX_PATH_LENGTH 1024
#define BLOCK_LENGTH 8192

// NOTE: "VERTAG" is replaced by the version info in "version.h"
//
// #define VERTAG VERTAG_6_VERTAG
//
// so this can be searched.
//
void VERTAG(void)
{

}


/* this function is for Offline Remote and needs to be finished */
/* if we want to compile C UDFs on Windows */
long WriteLog(char *FileName, BLOB LogData, long *FileAction)
{
  UNUSED_PARAMETER(FileName);
  UNUSED_PARAMETER(LogData);
  UNUSED_PARAMETER(FileAction);

  return(-1);
}

long CreateClient(const char *IBpath, const char *DBClientName)
{
  char *NewFileName;
  char *OldFileName;
  char *buffer;
  int fhandlenew, fhandleold, bytesread;

  NewFileName=(char *) malloc(MAX_PATH_LENGTH+12);
  strcpy(NewFileName, IBpath);
  strcat(NewFileName, DBClientName);
  if ((fhandlenew=open(NewFileName, 0))!=-1)
  {
    close(fhandlenew);
    freemem(&NewFileName);
    return(1); /* Database already exists */
  }

  OldFileName=(char *) malloc(MAX_PATH_LENGTH+12);
  strcpy(OldFileName, IBpath);
  strcat(OldFileName, "CL_BASE.gdb"); 
  if ((fhandleold=open(OldFileName, 0))==-1)
  {
    freemem(&NewFileName);
    freemem(&OldFileName);
    return(2); /* Database copy failed */
  }
  freemem(&OldFileName);
  if ((fhandlenew=open(NewFileName, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR))==-1)
  {
    freemem(&NewFileName);
    close(fhandleold);
    return(2); /* Database copy failed */
  }

  buffer=(char *) malloc(BLOCK_LENGTH);
  do
  {
    bytesread=read(fhandleold, buffer, BLOCK_LENGTH);
    if (write(fhandlenew, buffer, bytesread)!=bytesread)
    {    
      close(fhandleold);
      close(fhandleold);
      remove(NewFileName);
      freemem(&NewFileName);
      freemem(&buffer);
      return(2); /* Database copy failed */
    }
  }   
  while (bytesread==BLOCK_LENGTH);

  close(fhandlenew);
  close(fhandleold);
  freemem(&NewFileName);
  freemem(&buffer);
  return(0);
}

long NextClientNumber(const char *IBpath, long *CurrentClientNbr)
{
  char *temp = NULL;
  char *strnum;
  long i, myresult;
  DIR *dirp;
  struct dirent *direntp;

  myresult=9999999;
  temp = malloc(NAME_MAX+1);
  dirp=opendir(IBpath);
  if (dirp==NULL) 
  {
    return(myresult);
  }

  while ((direntp=readdir(dirp))!=NULL) 
  { 
    strcpy(temp, direntp->d_name);
  
    for (i=strlen(temp)-1; i>=0; i--)
    {
      temp[i]=toupper(temp[i]);
    }
  
    if ((temp[0]=='C') && (temp[1]=='L') && (temp[2]=='_') && (strstr(temp, ".GDB")!=NULL) && (strcmp(temp, "CL_BASE.GDB")!=0))
    {
      strnum=(char *) malloc(strlen(temp)-6);
      for (i=3; i< (int)(strlen(temp)-4); i++)
      {
        strnum[i-3]=temp[i];
      }
      strnum[strlen(temp)-7]='\0';
    
      if ((atoi(strnum)<myresult) && (atoi(strnum)>(*CurrentClientNbr)))
        myresult=atoi(strnum);
	
      freemem(&strnum);
    }
  }
  closedir(dirp);
  freemem(&temp);
  return(myresult);
}

// Exported as UDF "GETEVOLUDFVERSION"
// 
// This is also dynamically loaded by evo-udfver.

int GetVersion (void)
{
 return INTVERSION;
}

// loaded dynamically by evo-udfver

const char *GetUDFDate(void)
{
	return RELEASE;
}
