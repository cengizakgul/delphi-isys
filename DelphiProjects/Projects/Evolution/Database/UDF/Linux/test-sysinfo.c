// test-sysinfo.c
//
//	This is a main program that lives only to *test* the sysinfo module,
//	so it can be compiled separately from the UDFs.
//
//	Output should be along the lines of:
// 
// 		FB.version=LI-V2.0.7.13318 Firebird 2.0
// 		OS.type=Linux
// 		OS.version=2.6.32-358.6.1.el6.x86_64
// 		CPU.CPUs=1
// 		CPU.Cores=2
// 		CPU.Threads=4

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "global.h"

int main()
{
	SetFirebirdPath("/opt/firebird/");

	char infobuf[2048];

	const int rc = GetSystemInfo(infobuf);

	printf("GetSystemInfo returned rc=%d (%s)\n", rc, printable_GETSYSTEMINFO(rc));

	if (rc != GETSYSTEMINFO_OK)
	{
		printf("(failed: exiting)\n");
		exit(EXIT_FAILURE);
	}

	puts(infobuf);

	return 0;
}

