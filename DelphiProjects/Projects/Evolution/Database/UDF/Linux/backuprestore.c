// backuprestore.c
//
// 	This module is responsible for server-side backup and restore
// 	operations directed from the DB Manager; the backups are all
// 	done entirely locally on the server with no network I/O save
// 	for a minor bit of chitchat with the server for control.
//
// 	IMPORTANT: this module *requires* that the Linux "bzip2" and
// 	"bunzip2" commands are installed; this is mostly common, but
// 	we have to document the external command dependencies.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include "global.h"

// We support a few kinds of compression, but we really need to know
// about the common extensions so we can detect when we are given one
// we don't know how to handle.
//
// This way if we are given a .zip file but we don't have .zip support,
// we can at least fail gracefully (with suitable error message) rather
// than barf on a bad backup file.
//

#define COMPRESS_NONE   0
#define COMPRESS_BZIP2  1
#define COMPRESS_ZIP    2
#define COMPRESS_GZIP   3

static int runcompression(const char *compressprogram, const char *format, ... )
	__attribute__((format(printf,2,3)));

static int exten_to_compression(const char *exten);

#define BACKUPDBRESULT_OK		0	// all is well
#define BACKUPDBRESULT_NODB             1	// bad/missing DB name
#define BACKUPDBRESULT_BADCREDS         2	// username or password is empty or bad
#define BACKUPDBRESULT_BACKUPERROR      3	// error while backing up db
#define BACKUPDBRESULT_COMPRESSERROR    4	// error while compressing file
#define BACKUPDBRESULT_NOFBPATH         5	// cannot find Firebird path

static int _BackupDatabase(const char *gdbfile, const char *backupfile, const char *fbuser, const char *fbpass)
{
	if ( isNullOrEmpty(gdbfile) || isNullOrEmpty(backupfile) )
	{
		LOGMSG(("  ERROR: Empty/NULL db or backup file name"));
		return BACKUPDBRESULT_NODB;
	}

	// ALEKSEY: this FileExists() test was not in your code, but it probably
	// belongs, but this does change the semantics so I'd like to you review
	// it. If the filename doesn't exist, it will fail later in the backup,
	// so maybe you'd just as soon it fail with the same error in that case
	// (so you don't look for a *missing* filename).

	if ( FileExists(gdbfile) != 0 )
	{
		LOGMSG(("  ERROR: source DB not found"));

//		return BACKUPDBRESULT_NODB;			// maybe-better semantics
		return BACKUPDBRESULT_BACKUPERROR;		// but this matches current semantics
	}

	if ( isNullOrEmpty(fbuser)  ||  isNullOrEmpty(fbpass) )
	{
		LOGMSG(("  ERROR: user/pass credentials empty"));

		return BACKUPDBRESULT_BADCREDS;
	}

	// Firebird path to create the gbak command name
	//
	// Usually: "/opt/firebird/bin/gbak"

	char gbakpath[DEFAULT_FILENAME_SIZE];

	if ( GetGBAKPathBuf(gbakpath, sizeof gbakpath) <= 0)
	{
		LOGMSG(("  ERROR: Cannot find gbak path"));

		return BACKUPDBRESULT_NOFBPATH;
	}

	// COMPRESSING?
	//
	// Look at the backup filename to see what the user's requested
	// backup format is, whether bzip2 or zip. This is based entirely
	// on the file extension, which is .bz2 .gz or .zip
	//
	// If we *are* compressing we have to create the .gbk filename
	// from the full backup name so we have an intermediate place
	// to put this stuff.
	//

	char gbkfile[DEFAULT_FILENAME_SIZE];

	strcpy(gbkfile, backupfile);

	char *exten = strrchr(gbkfile, '.');

	const int compressionType = exten_to_compression(exten);

	if ( compressionType != COMPRESS_NONE )
	{
		// kill the compression extension in the gbk filename
		//
		// Example: /db/evolution/CL_100.gbk.zip --> /db/evolution/CL_100.gbk
		//
		*exten = '\0';
	}

	// SAFETY CHECK: be *absolutely sure* that the backup filename (or the compressed
	// version) does not end in .gdb in case somebody got the parameters wrong; this
	// would be bad news because we'd delete these files on failure.
	//
	// Note that when we're not doing compression, "backupfile" and "gbkfile" are
	// identical, but it's pointless to check for this special case because they
	// both pass or fail together.

	if ( endswith(backupfile, ".gdb")  ||  endswith(gbkfile, ".gdb") )
	{
		LOGMSG(("  ERROR: backup file %s is a .gdb file -- cannot procede", backupfile));

		// DO NOT remove *any* files in this case!

		return BACKUPDBRESULT_BACKUPERROR;
	}

	// Prepare the command-line parameters so we have safely-quoted args, then build
	// the backup command line.

	char cmdline[DEFAULT_CMDLINE_SIZE];

	char quoted_fbuser [DEFAULT_USERNAME_SIZE];
	char quoted_fbpass [DEFAULT_PASSWORD_SIZE];
	char quoted_gdbfile[DEFAULT_FILENAME_SIZE];
	char quoted_gbkfile[DEFAULT_FILENAME_SIZE];
	char quoted_backupfile[DEFAULT_FILENAME_SIZE];

	if ( quote_for_shell(quoted_gbkfile,    sizeof quoted_gbkfile,    gbkfile) < 0
	  || quote_for_shell(quoted_gdbfile,    sizeof quoted_gdbfile,    gdbfile) < 0
	  || quote_for_shell(quoted_fbuser,     sizeof quoted_fbuser,     fbuser ) < 0
	  || quote_for_shell(quoted_fbpass,     sizeof quoted_fbpass,     fbpass ) < 0 
	  || quote_for_shell(quoted_backupfile, sizeof quoted_backupfile, fbpass ) < 0 )	// compressed form
	{
		// overflow of these quoted buffers

		LOGMSG(("  ERROR: overflow creating backup filename"));
	
		return BACKUPDBRESULT_BACKUPERROR;
	}

	if ( safe_snprintf(cmdline, sizeof cmdline,
		"%s -USER %s -PASSWORD %s -BACKUP -GARBAGE_COLLECT -TRANSPORTABLE %s %s",
		gbakpath,
		quoted_fbuser,
		quoted_fbpass,
		quoted_gdbfile,
		quoted_gbkfile) < 0)
	{
		LOGMSG(("  ERROR: overflow creating backup command"));
		return BACKUPDBRESULT_BACKUPERROR;
	}

	int rc;	// command return code(s)

	LOGMSG(("  run %s", cmdline));

	if ( (rc = RunShellCmd(cmdline)) != 0 )
	{
		LOGMSG(("  ERROR: backup command failed: rc=%d", rc));

		unlink(gbkfile);

		return BACKUPDBRESULT_BACKUPERROR;
	}

	// Now we're done if there is no compression, but we may still
	// need it; run it if so, setting the return value as needed.

	rc = BACKUPDBRESULT_OK;

	switch (compressionType)
	{
	  case COMPRESS_BZIP2:
		rc = runcompression("bzip2", "bzip2 --compress --quiet --force %s", quoted_gbkfile);
		break;

	  case COMPRESS_ZIP:
		rc = runcompression("zip", "zip %s %s", quoted_backupfile, quoted_gbkfile);
		break;

	  case COMPRESS_GZIP:
		// --compress option not supported like it is in bzip2
		rc = runcompression("gzip", "gzip --quiet --force %s", quoted_gbkfile);
		break;

	  default:
		LOGMSG(("  unsupported compression method for file %s", backupfile));
		return BACKUPDBRESULT_COMPRESSERROR;

	  case COMPRESS_NONE:
		// Nothing to see here
		break;
	}

	if (rc != BACKUPDBRESULT_OK)
	{
		unlink(gbkfile);
		unlink(backupfile);
	}

	return rc;
}


// RestoreDatabase()
//
// 	Restore the given database from the backup file (.gbk or maybe .gbk.bz2)).
// 	Included are the Firebird username and password.
//
// 	IMPORTANT: this procedure will *delete* the backup file no matter whether
//
//

#define RESTOREDBRESULT_OK              0	// OK
#define RESTOREDBRESULT_NODB            1	// bad backup or db name (empty or null)
#define RESTOREDBRESULT_BADCREDS        2	// username or password are empty
#define RESTOREDBRESULT_RESTOREERROR    3	// error while restoring db
#define RESTOREDBRESULT_DECOMPRESSERROR 4	// error while decompressing archive
#define RESTOREDBRESULT_DBMOVEERROR     5	// error while moving db file
#define RESTOREDBRESULT_NOFBPATH        6	// cannot find firebird path

static int _RestoreDatabase( const char *backupfile, const char *gdbfile,
                     const char *fbuser, const char *fbpass )
{
	// If either of the filenames is invalid, it's an error
	//
	if ( isNullOrEmpty(gdbfile)  ||  isNullOrEmpty(backupfile) )
	{
		LOGMSG(("  ERROR: Empty/NULL db or backup file name"));
		return RESTOREDBRESULT_NODB;
	}

	if ( FileExists(backupfile) != 0)	// not found
	{
		LOGMSG(("  ERROR: backup file %s not found", backupfile));
		return RESTOREDBRESULT_RESTOREERROR;
	}
	//
	// verify that .gdb does not appear anywhere in the backup
	// path filename; to do this means we probably have some
	// kind of mistake, such as switching around .gbk and .gdb
	// filenames. We really don't want to remove any .gdb files.
	//
	// After this step we will be safe to delete this filename
	// anytime there's an error.
	
	if ( strstr(backupfile, ".gdb") != 0 )
	{
		LOGMSG(("  ERROR: backup filename can't include .gdb anywhere"));

		return RESTOREDBRESULT_RESTOREERROR;
	}

	// Verify that we have non-empty credentials
	//
	if ( isNullOrEmpty(fbuser)  ||  isNullOrEmpty(fbpass) )
	{
		LOGMSG(("  ERROR: user/pass credentials empty"));

		return RESTOREDBRESULT_BADCREDS;
	}

	char gbakpath[DEFAULT_FILENAME_SIZE];

	if ( GetGBAKPathBuf(gbakpath, sizeof gbakpath) <= 0)
	{
		LOGMSG(("  ERROR: Cannot find gbak path"));

		return RESTOREDBRESULT_NOFBPATH;
	}

	// Create the name of the .gbk file, which might be the same as
	// the backup file (if no compression), or there might be an
	// extension that identifies this as compressed.  We locate the
	// file extension and see if we know about it; if it's compressed,
	// we make a note of the type and then truncate the extension, which
	// (we hope) leaves us with a .gbk file behind.
	//
	// backupfile                               gbkfile
	// /db/evolution/CL_100.gbk             --> /db/evolution/CL_100.gbk
	// /db/evolution/CL_100.gbk.bz2         --> /db/evolution/CL_100.gbk
	// /db/evolution/CL_100.gbk.xxx         --> /db/evolution/CL_100.gbk.xxx
	//
	// The latter case of an unknown extension really is kinda wierd, it's
	// probably a 

	char gbkfile[DEFAULT_FILENAME_SIZE];

	strcpy(gbkfile, backupfile);		// CHECK OVERFLOW

	char *exten = strrchr(gbkfile, '.');

	if (exten == 0)
	{
		// No extention *at all*? This has to be an error
		LOGMSG(("  no extension found in backup filename %s", backupfile));
		return RESTOREDBRESULT_RESTOREERROR;
	}

	const int compressionType = exten_to_compression(exten);

	if ( compressionType != COMPRESS_NONE )
	{
		// kill the extension (such as .bz2)
		*exten = '\0';
	}

	// Create the temporary restore file that gets the gdb data
	// without disturbing the real .gdb file.
	//
	// /db/evolution/CL_123.gdb --> /db/evolution/CL_123.gdb.tmp
	//

	char gdbtmpfile[DEFAULT_FILENAME_SIZE];

	if ( safe_snprintf(gdbtmpfile, sizeof gdbtmpfile, "%s.tmp", gdbfile) < 0)
	{
		LOGMSG(("  ERROR: .gdb.tmp filename overflow"));

		// Delete anything?
		unlink(backupfile);
		unlink(gbkfile);

		return RESTOREDBRESULT_RESTOREERROR;
	}

	// Now create all the quoted versions of all these parameters so
	// we can include them in shell commands.

	char quoted_backupfile[DEFAULT_FILENAME_SIZE],
	     quoted_gbkfile[DEFAULT_FILENAME_SIZE],
	     quoted_gdbfile[DEFAULT_FILENAME_SIZE],
	     quoted_tmpfile[DEFAULT_FILENAME_SIZE],
	     quoted_fbuser [DEFAULT_USERNAME_SIZE],
	     quoted_fbpass [DEFAULT_USERNAME_SIZE];

	if ( quote_for_shell(quoted_backupfile, sizeof quoted_backupfile, backupfile) < 0
	  || quote_for_shell(quoted_gbkfile,    sizeof quoted_gbkfile,    gbkfile) < 0
	  || quote_for_shell(quoted_gdbfile,    sizeof quoted_gdbfile,    gdbfile) < 0
	  || quote_for_shell(quoted_fbuser,     sizeof quoted_fbuser,     fbuser) < 0
	  || quote_for_shell(quoted_fbpass,     sizeof quoted_fbpass,     fbpass) < 0
	  || quote_for_shell(quoted_tmpfile,    sizeof quoted_tmpfile,    gdbtmpfile) < 0 )
	{
		LOGMSG(("  ERROR: overflow creating quoted filenames/usernames"));

		unlink(backupfile);
		unlink(gbkfile);

		return RESTOREDBRESULT_RESTOREERROR;
	}

	// Ok, so we have valid filenames all around, time to decompress if
	// necessary depending on the type.

	char cmdline[DEFAULT_CMDLINE_SIZE];

	int rc;

	switch (compressionType)
	{
	  case COMPRESS_BZIP2:
		if ( safe_snprintf(cmdline, sizeof cmdline,
			 "bzip2 --decompress --force --quiet %s",
			 quoted_backupfile) < 0 )
		{
			LOGMSG(("  ERROR: overflow creating bzip2 cmdline"));

			// delete the the backup file that was bad?
			// this doesn't feel right - but maybe

			unlink(backupfile);
			unlink(gbkfile);

			return RESTOREDBRESULT_DECOMPRESSERROR;
		}

		LOGMSG(("  run %s", cmdline));

		if ( (rc = RunShellCmd(cmdline)) != 0 )
		{
			LOGMSG(("  ERROR: bzip2 decompress command failed rc=%d", rc));

			unlink(gbkfile);
			unlink(backupfile);

			return RESTOREDBRESULT_DECOMPRESSERROR;
		}

		break;

	  default:
	  case COMPRESS_GZIP:
	  case COMPRESS_ZIP:
		LOGMSG(("  ERROR: unsupported decompression method for file %s", backupfile));

		unlink(gbkfile);
		unlink(backupfile);

		return RESTOREDBRESULT_DECOMPRESSERROR;

	  case COMPRESS_NONE:
		// no 
		break;
	}

	// At this point we expect to have a gbk file to restore to a .gdb.tmp file,
	//
	if ( safe_snprintf(cmdline, sizeof cmdline,
		"%s -USER %s -PASSWORD %s -REPLACE -PAGE_SIZE %d %s %s",
		gbakpath,
		quoted_fbuser,
		quoted_fbpass,
		DB_PAGE_SIZE,
		quoted_gbkfile,
		quoted_tmpfile) < 0 )
	{
		LOGMSG(("  ERROR: cannot create gbak command line"));

		unlink(gbkfile);
		unlink(backupfile);

		return RESTOREDBRESULT_RESTOREERROR;
	}

	LOGMSG(("  run %s", cmdline));

	if ( (rc = RunShellCmd(cmdline)) != 0)
	{
		LOGMSG(("  ERROR: gbak restore command failed rc=%d", rc));

		unlink(gbkfile);
		unlink(backupfile);

		return RESTOREDBRESULT_RESTOREERROR;
	}

	// At this point we're *done* with the backup files, so we
	// can delete them and not worry about it in the future.

	unlink(gbkfile);
	unlink(backupfile);

	if ( MoveDatabaseFile(gdbtmpfile, gdbfile, fbpass) != 0)
	{
		LOGMSG(("  ERROR: MoveDatabaseFile(%s, %s, %s) failed", gdbtmpfile, gdbfile, fbpass));

		unlink(gdbtmpfile);

		return RESTOREDBRESULT_DBMOVEERROR;
	}

	LOGMSG(("  returning success"));

	return RESTOREDBRESULT_OK;
}

// runcompression()
//
// 	Run the given command line as a compression program, returning one
// 	of the big-picture return codes. The first arg of compressionprogram
// 	is here not for executing, but for reporting in error messages.
//
// 	Note that we don't remove any files if things fail; that's for the
// 	caller to decide.

static int runcompression(const char *compressprogram, const char *format, ... )
{
	char cmdline[DEFAULT_CMDLINE_SIZE];

	va_list args;

	va_start(args, format);
	const int n = safe_vsnprintf(cmdline, sizeof cmdline, format, args);
	va_end(args);

	if (n < 0 )
	{
		LOGMSG(("  ERROR: overflow of %s cmdline", compressprogram));

		return BACKUPDBRESULT_COMPRESSERROR;
	}

	LOGMSG(("  run %s", cmdline));

	int rc;

	if ( (rc = RunShellCmd(cmdline)) != 0 )
	{
		LOGMSG(("  ERROR: %s command failed: rc=%d", compressprogram, rc));

		return BACKUPDBRESULT_COMPRESSERROR;
	}

	return BACKUPDBRESULT_OK;
}

// Given a file extension, return the type of compression implied. If it's
// straight .gbk (or unknown), then it's COMPRESS_NONE.
// 
// NOTE: just because we know about an extension / compression type doesn't
// mean we know how to actually support it in the code.

static int exten_to_compression(const char *exten)
{
	if (exten == 0) return COMPRESS_NONE;

	if (strcmp(exten, ".gbk") == 0)	return COMPRESS_NONE;
	if (strcmp(exten, ".bz2") == 0) return COMPRESS_BZIP2;
	if (strcmp(exten, ".zip") == 0) return COMPRESS_ZIP;
	if (strcmp(exten, ".gz" ) == 0) return COMPRESS_GZIP;

	return COMPRESS_NONE;
}

int BackupDatabase(const char *gdbfile, const char *backupfile, const char *fbuser, const char *fbpass)
{
	LOGMSG(("enter BackupDatabase('%s', '%s', '%s', '%s')", gdbfile, backupfile, fbuser, fbpass));

	int rc = _BackupDatabase(gdbfile, backupfile, fbuser, fbpass);

	LOGMSG(("  BackupDatabase returns %d", rc));

	return rc;
}

int RestoreDatabase(const char *backupfile, const char *gdbfile, const char *fbuser, const char *fbpass)
{
	LOGMSG(("enter RestoreDatabase('%s', '%s', '%s', '%s')", gdbfile, backupfile, fbuser, fbpass));

	int rc = _RestoreDatabase(backupfile, gdbfile, fbuser, fbpass);

	LOGMSG(("  RestoreDatabase returns %d", rc));

	return rc;
}
