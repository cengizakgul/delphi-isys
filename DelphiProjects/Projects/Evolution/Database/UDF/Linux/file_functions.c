#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include "global.h"

// Result=0 if file exists
int FileExists(const char *lpcFilename)
{
	struct stat lsStat;
	int liResult;
	liResult=stat(lpcFilename, &lsStat);
	return liResult;
}

/*
* Result
* 0 - is regular file
* 1 - can't retrieve information
* 2 - isn't file
*/
int isRegFile(const char *lpcFilename)
{
	struct stat lsStat;
	int liResult = 1; 

	if (lstat(lpcFilename,&lsStat)==0)
		liResult = S_ISREG(lsStat.st_mode)?0:2;

	return liResult;
}

/* Result
* 0 - OK
* 1 - Command execute wrong
*/
int RunShellCmd(const char *lpcCmd)
{
	int liResult = 0;
	char *lpcCmdNew;

	if (liResult==0)
	{
		lpcCmdNew = malloc((strlen(lpcCmd)+18));
		sprintf(lpcCmdNew,"%s > /dev/null 2>&1",lpcCmd);

		if (system(lpcCmdNew)!=0)
			liResult = 1;
		freemem(&lpcCmdNew);
	}

	return liResult;
}

/* Result
* 0 - OK
* 1 - Command execute wrong
*/
int RunShellCmdEx(const char *lpcCmd, const char *lpcOut, const char *lpcErr)
{
	int liResult = 0;
	char *lpcCmdNew;

	if (liResult==0)
	{
		lpcCmdNew = malloc((strlen(lpcCmd) + strlen(lpcOut) + strlen(lpcErr) + 10));
		sprintf(lpcCmdNew,"%s >%s 2>%s",lpcCmd,lpcOut,lpcErr);

		if (system(lpcCmdNew)!=0)
			liResult = 1;
		freemem(&lpcCmdNew);
	}

	return liResult;
}

/* Result
* 0 - OK
* 1 - Source file doesn't exists
* 2 - Can't copy file
*/
int CopyFile(const char *lpcSrcFilename, const char *lpcDstFilename)
{
	int liResult = 0;
	char *lpcCmd = NULL;

	if (FileExists(lpcSrcFilename)!=0)
		liResult = 1;

	if (liResult==0)
	{
		lpcCmd = (char *) malloc(sizeof(char)*(strlen(lpcSrcFilename) + strlen(lpcDstFilename) + 12));
		sprintf(lpcCmd,"cp -r \"%s\" \"%s\"",lpcSrcFilename, lpcDstFilename);
		liResult = RunShellCmd(lpcCmd)?2:0;
	}

	freemem(&lpcCmd);

	return liResult;
}

/* Result
* 0 - OK
* 1 - Source file doesn't exists
* 2 - Can't rename/move file
*/
int MoveFile(const char *lpcSrcFilename, const char *lpcDstFilename)
{
	int liResult = 0;

	char *lpcCmd = NULL;

	if (FileExists(lpcSrcFilename)!=0)
		liResult = 1;

	if (liResult==0)
	{
		lpcCmd = (char *) malloc(sizeof(char)*(strlen(lpcSrcFilename) + strlen(lpcDstFilename) + 12));
		sprintf(lpcCmd,"mv -f \"%s\" \"%s\"",lpcSrcFilename, lpcDstFilename);
		liResult = RunShellCmd(lpcCmd)?2:0;
	}

	freemem(&lpcCmd);

	return liResult;
}

#if FALSE

// Moved to deletefile.c -- SJF 2013/08/14

/* Result
* 0 - OK
* 1 - Source file doesn't exists
* 2 - Can't delete file
*/
int DeleteFile(const char *lpcFilename)
{
	int liResult = 0;
	char *lpcCmd = NULL;

	if (FileExists(lpcFilename)!=0)
		liResult = 1;

	if (liResult==0)
	{
		lpcCmd = (char *) malloc((strlen(lpcFilename) + 9));
		sprintf(lpcCmd,"rm -f \"%s\"",lpcFilename);
		liResult = RunShellCmd(lpcCmd)?2:0;
	}

	freemem(&lpcCmd);

	return liResult;
}
#endif

/* Result
*  -1 - Can't open file
*  0-... - file size
*/
long long int GetFileSize(const char *lpcFilename)
{
	struct stat lsStat;
	long long int liResult = -1;

	if (stat(lpcFilename, &lsStat)==0)
		liResult = lsStat.st_size;
	return liResult;
}

/* Result
* 0 - OK
* 1 - BLOB is empty
* 2 - Can't open directory (permission denied or something else)
*/
int GetFileList(const char *lpcDir, const char *lpcDelim, BLOB lbData)
{
	int liResult = 0;
	DIR *lpdDir = NULL;
	struct dirent *lpeEnt;
	char *lpcBuffer = NULL;
	char lpcWriteBuffer[8192]; 

	if (!lbData)
		liResult = 1;
	else
		if (!lbData->blob_handle)
			liResult = 1;

	if (liResult == 0)
	{
		lpdDir = opendir(lpcDir); 
		if (!lpdDir)
			liResult = 2;
	}

	if (liResult == 0)
	{
		lpcBuffer = malloc(NAME_MAX+strlen(lpcDir)+1);
		lpcBuffer[0]=0;
		lpcWriteBuffer[0]=0;
		while ((lpeEnt=readdir(lpdDir))!=NULL)
		{
			if(strcmp(".", lpeEnt->d_name) && strcmp("..", lpeEnt->d_name))
			{
				sprintf(lpcBuffer,"%s/%s",lpcDir,lpeEnt->d_name);
				if (isRegFile(lpcBuffer)==0)
				{
					if (strlen(lpcWriteBuffer)>0)
						(*lbData->blob_put_segment)(lbData->blob_handle, lpcWriteBuffer, strlen(lpcWriteBuffer));
					sprintf(lpcWriteBuffer,"%s%s",lpeEnt->d_name,lpcDelim);
				}
			}
		}
		if (strlen(lpcWriteBuffer)>0) // cut last delimiter
		{
			lpcWriteBuffer[strlen(lpcWriteBuffer)-strlen(lpcDelim)]=0;
			(*lbData->blob_put_segment)(lbData->blob_handle, lpcWriteBuffer, strlen(lpcWriteBuffer));
		}
		freemem(&lpcBuffer);
	} 

	if (lpdDir!=NULL)
		closedir(lpdDir);

	return liResult;
}

/* Result
* 0 - OK
* 1 - Source file doesn't exists
* 2 - Error lock DB
* 3 - Error copy DB-file
* 4 - Error unlock DB
* 5 - Error unlock restored DB
* 6 - Can't find firebird path
*/
int CopyDatabaseFile(const char *lpcSrc, const char *lpcDst, const char *lpcSYSDBAPassword)
{
	int liResult = 0;
	char *lpcSrcDB = NULL, *lpcDstDB = NULL, *lpcTmpName = NULL;
	char *lpcCmd = NULL;
	char *lpcFirebird = NULL;
	int liSrc, liDst;
	int liLocked = 0;

	liSrc = strlen(lpcSrc);
	liDst = strlen(lpcDst);
	lpcSrcDB = malloc(liSrc+3);
	lpcDstDB = malloc(liDst+3);
	lpcCmd = malloc( (liSrc>liDst?liSrc:liDst) + 12);
	sprintf(lpcSrcDB,"\"%s\"",lpcSrc); 
	sprintf(lpcDstDB,"\"%s\"",lpcDst);

	if(FileExists(lpcSrc)!=0)
		liResult = 1;

	lpcFirebird = GetFirebirdPath();
	if (*lpcFirebird==0)
		liResult = 6;

	if (!liResult)
	{
		lpcCmd = malloc( strlen(lpcFirebird) + (liSrc>liDst?liSrc:liDst) + 16 + 2); // +2 for quotes in new filenames
		sprintf(lpcCmd,"%sbin/nbackup -L %s",lpcFirebird,lpcSrcDB);
		liResult = RunShellCmd(lpcCmd)?2:0;
	}

	if (!liResult)
	{
		liLocked=1;

		lpcTmpName = malloc(strlen(lpcDst) + 5);
		sprintf(lpcTmpName, "%s.tmp", lpcDst);

		liResult = CopyFile(lpcSrc,lpcTmpName)?3:0;

		if (liResult)
		{
			DeleteFile(lpcTmpName);
		}
		else
		{
          liResult = MoveDatabaseFile(lpcTmpName, lpcDst, lpcSYSDBAPassword);
          liResult = liResult ? 3 : 0; 
		}

		freemem(&lpcTmpName);
	}

	// if DB locked, then we need to unlock it
	if (liLocked)
	{
		sprintf(lpcCmd,"%sbin/nbackup -N %s",lpcFirebird,lpcSrcDB);
		liLocked = RunShellCmd(lpcCmd)?4:0;
		liResult = !liResult ? liLocked : liResult;
	}

	if (!liResult)
	{
		sprintf(lpcCmd,"%sbin/nbackup -F %s",lpcFirebird,lpcDstDB);
		liResult = RunShellCmd(lpcCmd)?5:0;
	}

	freemem(&lpcCmd);
	freemem(&lpcDstDB);
	freemem(&lpcSrcDB);
	freemem(&lpcFirebird);

	return liResult;
}


/* Result
* 0 - OK
* 1 - source or target database is empty
* 2 - username or password is empty
* 3 - error while backup and restore database
* 4 - error while overwriting existing database 
* 5 - Can't find firebird path
*/
int CopyDatabase(const char *lpcSrcDBName, const char *lpcSrcUsername, const char *lpcSrcPassword,
                 const char *lpcDstDBName, const char *lpcDstUsername, const char *lpcDstPassword)
{
	int liResult = 0;
	char *lpcQSrcDBName = NULL;
	char *lpcQDstDBName = NULL;
	char *lpcTmpName = NULL; 
	char *lpcCmd = NULL;
	char *lpcFirebird = NULL;

	if ((strlen(lpcSrcDBName)==0 || strlen(lpcDstDBName)==0))
		liResult=1;

	if (!liResult && (strlen(lpcSrcUsername)==0 || strlen(lpcSrcPassword)==0 || strlen(lpcDstUsername)==0 || strlen(lpcDstPassword)==0))
		liResult=2;

	lpcFirebird = GetFirebirdPath();
	if (*lpcFirebird==0)
		liResult = 5;

	if (!liResult)
	{
		lpcQSrcDBName = malloc(strlen(lpcSrcDBName)+3);
		lpcQDstDBName = malloc(strlen(lpcDstDBName)+3);

		sprintf(lpcQSrcDBName,"\"%s\"",lpcSrcDBName);
		sprintf(lpcQDstDBName,"\"%s\"",lpcDstDBName);

		lpcTmpName = malloc(strlen(lpcDstDBName) + 5);
		sprintf(lpcTmpName, "%s.tmp", lpcDstDBName);

		lpcCmd = malloc(strlen(lpcQSrcDBName) + strlen(lpcTmpName) + 2*strlen(lpcFirebird) + strlen(lpcSrcUsername) + strlen(lpcSrcPassword) + strlen(lpcDstUsername) + strlen(lpcDstPassword) + 200); //200 a sufficient chars for commands
		sprintf(lpcCmd,"%sbin/gbak -B -G -USER %s -PASSWORD %s %s stdout 2> /dev/null | %sbin/gbak -REP -P 8192 -USER %s -PASSWORD %s stdin %s",lpcFirebird,lpcSrcUsername,lpcSrcPassword,lpcQSrcDBName,lpcFirebird,lpcDstUsername,lpcDstPassword,lpcTmpName);
		liResult = RunShellCmd(lpcCmd)?3:0;

		if (liResult)
		{
			DeleteFile(lpcTmpName);
		}
		else
		{
			if (isSysdba(lpcDstUsername))
			{
				liResult = MoveDatabaseFile(lpcTmpName, lpcDstDBName, lpcDstPassword);
			}
			else
			{
				liResult = MoveDatabaseFile(lpcTmpName, lpcDstDBName, "");
			}

			liResult = liResult ? 4 : 0; 
		}

		freemem(&lpcTmpName);
	}

	freemem(&lpcQSrcDBName);
	freemem(&lpcQDstDBName); 
	freemem(&lpcCmd); 
	freemem(&lpcFirebird);

	return liResult;
}

/* Result
* 0 - OK
* 1 - DB file not exist
* 2 - error while execute command
* 3 - can't find Firebird path
*/
int RunGfix(const char *lpcSysdbaPwd, const char *lpcDBPath, const char *lpcOptions)
{
	int liResult = 0;
	char *lpcCmd = NULL;
	char lpcOut[200], lpcErr[200], lpcUID[50];
	FILE *lfErr;
	char lpcStr[1024];
	char *lpcFirebird=NULL;

	if (FileExists(lpcDBPath)!=0)
		liResult = 1;

	if (liResult==0)
	{
		lpcFirebird = GetFirebirdPath();
		if (*lpcFirebird==0)
			liResult = 3;
	}

	if (liResult==0)
	{
		myUID(lpcUID);
		lpcCmd = (char *) malloc(strlen(lpcFirebird) + strlen(lpcDBPath) + strlen(lpcSysdbaPwd) + strlen(lpcOptions) + 100);
		sprintf(lpcOut,"/tmp/UDFOut_%s.log",lpcUID);
		sprintf(lpcErr,"/tmp/UDFErr_%s.log",lpcUID);
		sprintf(lpcCmd,"%sbin/gfix -user SYSDBA -password %s \"%s\" %s",lpcFirebird,lpcSysdbaPwd,lpcDBPath,lpcOptions);

		RunShellCmdEx(lpcCmd,lpcOut,lpcErr); //gfix don't return error value
		lfErr=fopen(lpcErr,"r");
		if (lfErr!=NULL)
		{
			while(fgets(lpcStr,1023,lfErr)!=NULL)
				if(strstr(lpcStr,"mode is invalid for database")==NULL)
					liResult=2;
			fclose(lfErr);
		}

		DeleteFile(lpcOut);
		DeleteFile(lpcErr);
	}

	freemem(&lpcCmd);
	freemem(&lpcFirebird);

	return liResult;
}

/* Result
* 0 - OK
* 1 - DB file not exist
* 2 - error while execute command
*/
int ShutdownDB(const char *lpcSysdbaPwd, const char *lpcDBPath)
{
	int liResult = 0;

	liResult=RunGfix(lpcSysdbaPwd, lpcDBPath, "-shut full -force 0");

	return liResult;
}

/* Result
* 0 - OK
* 1 - DB file not exist
* 2 - error while execute command
*/
int OnlineDB(const char *lpcSysdbaPwd, const char *lpcDBPath)
{
	int liResult = 0;

	liResult=RunGfix(lpcSysdbaPwd, lpcDBPath, "-online normal");

	return liResult;
}

/* Result
* 0 - OK
* 1 - Source file doesn't exists
* 2 - Can't delete file 
*/
int DeleteDatabaseFile(const char *lpcDBPath, const char *lpcSYSDBAPassword)
{
	// Ignore ShutdownDB/OnlineDB errors as DB may be corrupted or the file is not a DB
	int liResult = 0;
	int liSysdba = 0;

	if (FileExists(lpcDBPath)!=0)
	{
		return 1;
	}

	liSysdba = strlen(lpcSYSDBAPassword) > 0 ? 1 : 0;

	if (liSysdba == 1)
		ShutdownDB(lpcSYSDBAPassword, lpcDBPath); 

	if (liResult==0)
	{
		liResult = DeleteFile(lpcDBPath) ? 2 : 0;

		if (liResult==3 && liSysdba == 1)
			OnlineDB(lpcSYSDBAPassword, lpcDBPath);
	}

	return liResult;
}


/* Result
* 0 - OK
* 1 - Can't find source database
* 2 - error while renaming dest database 
* 3 - error while renaming source database
*/
int MoveDatabaseFile(const char *lpcSrcDBPath, const char *lpcDstDBPath, const char *lpcSYSDBAPassword)
{
	int liResult = 0;
	char *lpcTmpName = NULL;

	LOGMSG(("enter MoveDatabaseFile('%s', '%s', '%s')", lpcSrcDBPath, lpcDstDBPath, lpcSYSDBAPassword));

	if (FileExists(lpcSrcDBPath) != 0)
		return 1; 

	if (FileExists(lpcDstDBPath) == 0)
	{ 
		lpcTmpName = malloc(strlen(lpcDstDBPath) + 5);
		sprintf(lpcTmpName, "%s.old", lpcDstDBPath);
		if (MoveFile(lpcDstDBPath, lpcTmpName))
		{
			return 2;
		}

		liResult = MoveFile(lpcSrcDBPath, lpcDstDBPath) ? 3 : 0;

		if (liResult) 
		{
			MoveFile(lpcTmpName, lpcDstDBPath);
		}
		else
		{
			liResult = DeleteDatabaseFile(lpcTmpName, lpcSYSDBAPassword);
		}

		freemem(&lpcTmpName);
	}

	else
		liResult = MoveFile(lpcSrcDBPath, lpcDstDBPath) ? 3 : 0;

	return liResult;
}
