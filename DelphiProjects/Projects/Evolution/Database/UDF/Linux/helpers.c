#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <sys/utsname.h>
#include <time.h>
#include <unistd.h>		// for readlink()
#include <sys/time.h>		// for gettimeofday()
#include <stdarg.h>
#include <ctype.h>
#include "global.h"

// SetFirebirdPath()
//
//	This call is for *testing only* - it allows us to tell the library
//	where the Firebird path is. Though when used with UDFs at runtime,
//	it always finds it find via /proc/self/exe, individual test programs
//	won't work this way so we have to set it manually.
//
//	In practice this just prefills the cache that obviates having to look
//	up the path again later.
//	

static char cached_firebirdpath[2048] = "";

void SetFirebirdPath(const char *path)
{
	strcopy(cached_firebirdpath, path, sizeof cached_firebirdpath);
}

// returnstring()
//
//	A number of our functions return either a copy of a string if all
//	is well, or a malloc'd empty string on failure. Here, n is the number
//	of non-NUL bytes, which makes it easy to allocate memory properly.

static char *returnstring(char *s, int n)
{
	char *rc = 0;

	if (s == 0  ||  n <= 0)
	{
		rc = malloc(1);
		rc[0] = '\0';
	}
	else
	{
		rc = malloc(n+1);
		strcpy(rc, s);
	}
	return rc;
}

// GetGBAKPathBuf()
//
//	Fill the buffer with the full path of the GBAK command,
//	returning the number of non-NUL bytes filling the buffer.
//	Return is <0 on error, including truncation due to a
//	too-small buffer.

int GetGBAKPathBuf(char *obuf, size_t osize)
{
	const int n = GetFirebirdPathBuf(obuf, osize - 16);	// room for "bin/gbak"

	if (n <= 0) return -1;

	char *p = obuf + n;

	p += nstrcpy(p, "bin/gbak");

	return (int)(p - obuf);
}


// GetFirebirdPathBuf()
//
//	Fill the given buffer with the path of the Firebird
//	directory; it's usually /opt/firebird/ but we need to
//	be sure it works no matter what.   NOTE that the path
//	always includes the trailing slash.
//
//	We figure this out by finding out where our own executable
//	is from, and this ought to work whether it's fb_inet_server
//	(through the network) or via isql/gbak/gfix.
//
//	Return is the number of non-NUL bytes in the output buffer
//	if we were able to fetch it, or <0 if not.
//
//	We *always* NUL terminate the passed-in buffer whether
//	there's an error or not.
//
//	NOTE: we have one level of caching so we look up the path
//	at most once. And the user can SetFirebirdPath() to prefill
//	the cache for a test program.
//

int GetFirebirdPathBuf(char *obuf, size_t osize)
{
	// If we're testing, take the caller's word for where
	// Firebird is.
	//
	//	
	if ( cached_firebirdpath[0] != 0)
	{
		return strlen( strcopy(obuf, cached_firebirdpath, osize) );
	}

	// TESTING: if 

	// IMPORTANT: readlink() does *not* put a NUL byte at the end of the
	// buffer, and it does truncate if the buffer is not large enough,
	// and this means we have to check for both.
	//
	int rc = readlink("/proc/self/exe", obuf, osize-1);

	if (rc <= 0)
	{
		// error: could not read the link; this shouldn't
		// ever really happen.

		obuf[0] = '\0';

		return -1;
	}

	else if (rc >= (int)(osize-1))
	{
		// Truncation: that's the same as an error because a
		// partial path is of no use to us.

		obuf[0] = '\0';

		return -1;
	}

	// OK, we're good here - we have the full path value (which is
	// likely /opt/firebird/bin/isql or /opt/firebird/bin/fb_inet_server
	// so look for firebird/bin in the path and truncate at the bin part.
	//
	char *p;

	if ( (p = strstr(obuf, "firebird/bin")) == 0 )
	{
		// we're not in any firebird area
		//
		obuf[0] = '\0';

		return -1;
	}

	p += 9;			// 9 = strlen("firebird/");

	*p = '\0';

	strcopy(cached_firebirdpath, obuf, sizeof cached_firebirdpath);

	return (int)(p - obuf);
}

// GetFirebirdPath()
//
// 	Return a *malloc'd* pointer to a string with the path to the parent
// 	directory where the Firebird code is installed on the Linux system,
// 	usually /opt/firebird/, and if we can't find it, return an empty
// 	(but not NULL) string.
//
// 	CALLER MUST FREE THIS MEMORY EVEN ON FAILURE!

char *GetFirebirdPath(void)
{
	char pathbuf[1024];

	int n = GetFirebirdPathBuf(pathbuf, sizeof pathbuf);

	return returnstring(pathbuf, n);
}

#if 0
// CreateGbakCommand()
//
// 	We do enough gbak that we want a common way to format this command,x
// 	including getting all the quoting right;
//

int CreateGbakCommand(char *obuf, size_t osize,
	const char *fbuser, const char *fbpass,
	const char *srcdb, const char *dstdb,
	const char *options)
{
	char *obuf_orig = obuf;
	char *omax      = obuf + osize - 16;

	int n;

	// find full path of gbak command
	if ( (n = GetGBAKPathBuf(obuf, (int)(omax - obuf))) < 0)
		return -1;

	obuf += n;

	// add the username and password parts
	//
	char quoted_fbuser[DEFAULT_USERNAME_SIZE];
	char quoted_fbpass[DEFAULT_USERNAME_SIZE];

	if ( quote_for_shell(quoted_fbuser, sizeof quoted_fbuser, fbuser) < 0
	  || quote_for_shell(quoted_fbpass, sizeof quoted_fbpass, fbpass) < 0 )
	{
		return -1;
	}

	if ( (n = safe_snprintf(obuf, (int)(omax - obuf), " -USER %s -PASSWORD %s", quoted_fbuser, quoted_fbpass)) < 0)
	{
		return -1;
	}

	obuf += n;

	// add in the options: after the credentials, before the filenames
	if ( (n = safe_snprintf(obuf, (int)(omax - obuf), " %s", options)) < 0 )
	{
		return -1;
	}

	obuf += n;

	// add in the two filenames
	char quoted_srcdb[DEFAULT_FILENAME_SIZE];
	char quoted_dstdb[DEFAULT_FILENAME_SIZE];

	if ( quote_for_shell(quoted_srcdb, sizeof quoted_srcdb, srcdb) < 0
	  || quote_for_shell(quoted_dstdb, sizeof quoted_dstdb, dstdb) < 0 )
	{
		return -1;
	}

	if ( (n = safe_snprintf(obuf, (int)(omax - obuf), " %s %s", quoted_srcdb, quoted_dstdb)) < 0 )
	{
		return -1;
	}

	return (int)(obuf - obuf_orig);		// length of new string
}
#endif


// GetSelfPathBuf()
//
//	Fill the caller's buffer with the full path of the UDFs, which is
//	normally /opt/firebird/UDF/EvolUDF, returning the number of non-NUL
//	bytes stored in the buffer.

int GetSelfPathBuf(char *obuf, size_t osize)
{
	int n = GetFirebirdPathBuf(obuf, osize);

	if (n <= 0)
	{
		// can't find path - bummer. Insure NUL terminated.

		obuf[0] = '\0';

		return -1;
	}

	// points to the NUL byte after /opt/firebird/
	char *p = obuf + n;

	p += sprintf(p, "UDF/%s", gpcUDFLib);	// TODO: insure no overflow!

	return (int)(p - obuf);
}

char *GetSelfPath()
{
	char pathbuf[1024];

	int n = GetSelfPathBuf(pathbuf, sizeof pathbuf);

	return returnstring(pathbuf, n);
}


// safe_vsnprintf()
//
// 	This wraps the snprintf() library by insuring that we get a useful
// 	return in the event of overflow. The usual return from snprintf()
//	is the number of non-NUL bytes written to the output, but if the
//	output is truncated, the return value is the same as "osize". This
//	means we overflowed but it's a bummer to check.
//
//	So this routine detects that case and returns -1 if there was 
//	an error, which is a lot easier to check.

int safe_vsnprintf(char *obuf, size_t osize, const char *format, va_list args)
{
	int n = vsnprintf(obuf, osize, format, args);

	if (n < 0)
	{
		obuf[0] = '\0';
		return n;
	}
	else if (n >= (int)(osize - 1))
	{
		// NOTE: we assume that the output buffer is full,
		// but we still absolutely insure that the final
		// NUL byte has been written

		obuf[osize-1] = '\0';

		return -1;
	}
	else
	{
		// success!
		return n;
	}
}

//
// safe_snprintf()
//
//	Likewise for snprintf, it relies on the underlying safe_vsnprintf
//	for the heavy lifting.

int safe_snprintf(char *obuf, size_t osize, const char *format, ...)
{
	va_list args;

	va_start(args, format);

	const int n = safe_vsnprintf(obuf, osize, format, args);

	va_end(args);

	return n;
}

// quote_for_shell()
//
//	Given a string, format it into the output buffer so that it's suitably
//	formatted for inclusion in a shell command line. We quote and escape
//	as needed, and though technically we don't need to quote anything if
//	it's just alphanumeric, it's easier and more reliable if we 
//
//	Return is the number of non-NUL bytes transfered to the output buffer,
//	or <0 on overflow.
//
int quote_for_shell(char *obuf, size_t osize, const char *instr)
{
	char *obuf_orig = obuf;
	char *omax = obuf + osize - 3;	// leave room for closing quote and NUL

	*obuf++ = '\'';

	for ( ; *instr  &&  (obuf < omax) ; instr++ )
	{
		if (*instr == '\''  ||  *instr == '\\')
			*obuf++ = '\\';

		*obuf++ = *instr;
	}

	// Overflow?
	
	if (obuf >= omax)
	{
		obuf_orig[0] = '\0';
		return -1;
	}

	// always finish it off with a closing quote and a final NUL

	*obuf++ = '\'';
	*obuf   = '\0';

	return (int)(obuf - obuf_orig);
}

// endswith()
//
//	Return true if the big long string ends with the given small string.
//	Mostly useful to see if a filename ends with a certain extension
//	(such as .gdb).
//
//	If the small string is empty, that's almost certainly a bug, but we
//	consider this to be a true return.

int endswith(const char *bigstring, const char *smallstring)
{
	const int smalllen = strlen(smallstring);
	const int biglen   = strlen(bigstring);

	const int offset = biglen - smalllen;

	// the small string is longer than the big string?
	if (offset < 0) return FALSE;

	return strcmp(bigstring + offset, smallstring) == 0;
}

/*
 * nstrcpy()
 *
 *	Copy dest to src, return # of bytes copied (not including
 *	the final NUL byte).
 *
 *	NOTE: this does NOT know about the size of the output buffer;
 *	use with caution only when you know it won't overflow.
 */
int nstrcpy(char *dst, const char *src)
{
char	*dst_old = dst;

	while ( (*dst = *src++) != 0 )
		dst++;

	return (int)(dst - dst_old);
}

// freemem()
//
//	Given a pointer, release it back to the managed pool
//	only if it's not NULL, and then NULL it out so it won't
//	be a stray pointer.
//
//	It's no harm calling this with a NULL pointer.

void freemem(char **p)
{
	if (p && *p)
	{
		free(*p);
		*p = 0;
	}
}

// strip()
// 
// 	Given a pointer to a string, remove all trailing whitespace
// 	from it, returning the original string.
//
// 	NOTE: this does *not* do memory allocation and should not
// 	be used for strings that have to be returned to Firebird
//
char * strip(char *str)
{
char	*old = str;	/* save ptr to original string          */
char	*lnsp = 0;	/* ptr to last non-space in string      */

	for ( ; *str; str++)
		if (!isspace(*str))
			lnsp = str;
	if ( lnsp )
		lnsp[1] = '\0';
	else
		*old = '\0';

	return old;
}


// GetFirebirdVersion()
//
// Query the current system to find out what Firebird version we have;
// we can find it either from gfix or isql, though isql is somewhat
// better in that can be run by any user (gfix is root or firebird
// only).
//
//	# gfix -z 2>&1
//	gfix version LI-V2.0.7.13318 Firebird 2.0
//	.............^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
//	# isql -z -i /dev/null 2>&1
//	ISQL Version: LI-V2.0.7.13318 Firebird 2.0
//	..............^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
// The indicated version string is what's populated.
//
// Return is the number of non-NUL bytes that fill the given buffer,
// or <0 on error.

int GetFirebirdVersionBuf(char *obuf, size_t osize)
{
char	cmdline[2048],
	*cmdp = cmdline;

	int sz;
	if ( (sz = GetFirebirdPathBuf(cmdp, sizeof cmdline)) < 0)
	{
//		printf("Cannot get firebird path\n");
		return -1;
	}
	cmdp += sz;

	// Choose the command to run

//	cmdp += sprintf(cmdp, "bin/gfix -z 2>&1");
	cmdp += sprintf(cmdp, "bin/isql -z -i /dev/null 2>&1");

	FILE *fp = popen(cmdline, "r");

	if (fp == 0)
	{
		// printf("Error: cannot run command \"%s\"\n", cmdline);
		return -1;
	}

	char versionbuf[256], *foundver = 0;

	while ( foundver == 0   &&  fgets(versionbuf, sizeof versionbuf, fp)  )
	{
		strip(versionbuf);		// removing trailing whitespace

		if ( (foundver = strstr(versionbuf, "version")) != 0 )
		{
			// gfix version LI-V2.0.7.13318 Firebird 2.0
			foundver += 8;		// sizeof(version) + 1
		}
		else if ( (foundver = strstr(versionbuf, "Version:")) != 0 )
		{
			// ISQL Version: LI-V2.0.7.13318 Firebird 2.0
			foundver += 9;		// sizeof(Version:) + 1
		}
	}

	fclose(fp);

	if (foundver)
	{
		return strlen(strcopy(obuf, foundver, osize));
	}
	else
	{
		return -1;
	}
}

// strcopy()
//
//	This is like strcpy() but it takes the size of the output
//	buffer, and we *never* go past it, even with a NUL byte
//	and even if truncation occurs, we still always include the
//	NUL byte.

char *strcopy(char *dst, const char *src, size_t osize)
{
	// this looks like a hack: 
	if (osize <= 0)
	{
		dst[0] = '\0';
		return dst;
	}

	strncpy(dst, src, osize-1);

	dst[osize-1] = '\0';

	return dst;
}
