#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h> 
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <limits.h>
#include <ib_util.h>
#include "global.h"

char * ConvertNullString (const char *sz, const char *NullString)
{
  char * sz_result;
  if (strlen(sz) == 0)
  {
    sz_result = (char *) MALLOC(strlen(NullString) + 1);
    strcpy(sz_result, NullString);
  }
  else
  {
    sz_result = (char *) MALLOC(strlen(sz) + 1);
    strcpy(sz_result, sz);
  }

  return sz_result;
}

ISC_QUAD * ConvertNullDate (const ISC_QUAD *ib_date, const ISC_QUAD *NullDate)
{
  ISC_QUAD * result = (ISC_QUAD *) MALLOC(sizeof(ISC_QUAD));

  if ((ib_date->isc_quad_high == 0) && (ib_date->isc_quad_low == 0))
    *result = *NullDate;
  else
    *result = *ib_date;

  return result;
}

int ConvertNullInteger (const int *Value, const int *NullInteger)
{
  if (*Value == 0)
    return *NullInteger;
  else
    return *Value;
}

double ConvertNullDouble (const double *Value, const double *NullDouble)
{
  if (*Value == 0)
    return *NullDouble;
  else
    return *Value;
}

char * IfString (const int *Condition, const char *MyIfString, const char *MyElseString)
{
  char * sz_result;
  if (*Condition != 0)
  {
    sz_result = (char *) MALLOC(strlen(MyIfString) + 1);
    strcpy(sz_result, MyIfString);
  }
  else
  {
    sz_result = (char *) MALLOC(strlen(MyElseString) + 1);
    strcpy(sz_result, MyElseString);
  }

  return sz_result;
}

ISC_QUAD * IfDate (const int *Condition, const ISC_QUAD *MyIfDate, const ISC_QUAD *MyElseDate)
{
  ISC_QUAD * result = (ISC_QUAD *) MALLOC(sizeof(ISC_QUAD));

  if (*Condition != 0)
    *result = *MyIfDate;
  else
    *result = *MyElseDate;

  return result;
}

int IfInteger (const int *Condition, const int *MyIfInteger, const int *MyElseInteger)
{
  if (*Condition != 0)
    return *MyIfInteger;
  else
    return *MyElseInteger;
}

double IfDouble (const int *Condition, const double *MyIfDouble, const double *MyElseDouble)
{
  if (*Condition != 0)
    return *MyIfDouble;
  else
    return *MyElseDouble;
}

int CompareString (const char *FirstString, const char *Operation, const char *SecondString)
{
  if (strcmp(Operation, "=") == 0)
  {
    if (strcmp(FirstString, SecondString) == 0)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">") == 0)
  {
    if (strcmp(FirstString, SecondString) > 0)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, "<") == 0)
  {
    if (strcmp(FirstString, SecondString) < 0)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">=") == 0)
  {
    if (strcmp(FirstString, SecondString) < 0)
      return (0);
    else
      return (1);
  }
  else
  if (strcmp(Operation, "<=") == 0)
  {
    if (strcmp(FirstString, SecondString) > 0)
      return (0);
    else
      return (1);
  }
  else
  {
    if (strcmp(FirstString, SecondString) == 0)
      return (0);
    else
      return (1);
  }
}

int CompareDate (const ISC_QUAD *FirstDate, const char *Operation, const ISC_QUAD *SecondDate)
{
  struct tm tm_date1, tm_date2;
  time_t date1, date2;

  isc_decode_date(FirstDate, &tm_date1);
  isc_decode_date(SecondDate, &tm_date2);
  date1 = mktime(&tm_date1);
  date2 = mktime(&tm_date2);

  if (strcmp(Operation, "=") == 0)
  {
    if (difftime(date1, date2) == 0)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">") == 0)
  {
    if (difftime(date1, date2) > 0)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, "<") == 0)
  {
    if (difftime(date1, date2) < 0)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">=") == 0)
  {
    if (difftime(date1, date2) < 0)
      return (0);
    else
      return (1);
  }
  else
  if (strcmp(Operation, "<=") == 0)
  {
    if (difftime(date1, date2) > 0)
      return (0);
    else
      return (1);
  }
  else
  {
    if (difftime(date1, date2) == 0)
      return (0);
    else
      return (1);
  }
}

int CompareInteger (const int *FirstInteger, const char *Operation, const int *SecondInteger)
{
  if (strcmp(Operation, "=") == 0)
  {
    if (*FirstInteger == *SecondInteger)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">") == 0)
  {
    if (*FirstInteger > *SecondInteger)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, "<") == 0)
  {
    if (*FirstInteger < *SecondInteger)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">=") == 0)
  {
    if (*FirstInteger < *SecondInteger)
      return (0);
    else
      return (1);
  }
  else
  if (strcmp(Operation, "<=") == 0)
  {
    if (*FirstInteger > *SecondInteger)
      return (0);
    else
      return (1);
  }
  else
  {
    if (*FirstInteger == *SecondInteger)
      return (0);
    else
      return (1);
  }
}

int CompareDouble (const double *FirstDouble, const char *Operation, const double *SecondDouble)
{
  if (strcmp(Operation, "=") == 0)
  {
    if (*FirstDouble == *SecondDouble)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">") == 0)
  {
    if (*FirstDouble > *SecondDouble)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, "<") == 0)
  {
    if (*FirstDouble < *SecondDouble)
      return (1);
    else
      return (0);
  }
  else
  if (strcmp(Operation, ">=") == 0)
  {
    if (*FirstDouble < *SecondDouble)
      return (0);
    else
      return (1);
  }
  else
  if (strcmp(Operation, "<=") == 0)
  {
    if (*FirstDouble > *SecondDouble)
      return (0);
    else
      return (1);
  }
  else
  {
    if (*FirstDouble == *SecondDouble)
      return (0);
    else
      return (1);
  }
}

int GetBlobSize(BLOB b)
{
  if (!b->blob_handle)
    return -1;
  else                              
    return (b->blob_total_length);
}

/*
 * Result:
 * 0 - OK
 * 1 - Can't find source library location
 * 2 - Received BLOB is empty
 * 3 - Can't open file for writing new library
 * 4 - error copying new library instead of the old 
 * 5 - can't set right permission by chmod
 *
 *
 * Exported as UPDATEUDF
 */
int UpdateUDF(BLOB lbData)
{
 char *lpcPath=NULL;
 char *lpcFileNew = NULL;
 char *lpcStr = NULL;
 int liPos = 0;
 int liResult = 0;

 // 1. Check right filename and right blob
 lpcPath = GetSelfPath();
 liPos = PosSubStr(gpcUDFLib,lpcPath);
 if (liPos<0)
   liResult = 1;
 if (!lbData)
   liResult = 2;
 else
   if (!lbData->blob_handle)
     liResult = 2;

 // 2. Save blob to file
 if (liResult == 0)
   {
     lpcFileNew = malloc(sizeof(char)*(strlen(lpcPath)+5));
     sprintf(lpcFileNew,"%s.new",lpcPath);

     liResult = SaveBlobToFile(lpcFileNew, lbData)?3:0;
  }

 // 3. Copy new library over old with creating backup file
 if (liResult == 0)
   {
     lpcStr = malloc(sizeof(char)*(24+strlen(lpcFileNew)+strlen(lpcPath)));
     sprintf(lpcStr,"cp -b %s %s >/dev/null 2>&1", lpcFileNew, lpcPath);

     if (system(lpcStr)!=0)
      liResult=4;

     if (chmod(lpcPath, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP)!=0)
      liResult=5;
   }

  freemem(&lpcStr);
  freemem(&lpcFileNew);
  freemem(&lpcPath);

 return (liResult);
}

/* 
 * Result:
 * 0 - OK
 * 1 - Can't open file
 * 2 - BLOB is empty
 * 3 - filename is empty
 *
 * Exported in UDF as SAVEBLOBTOFILE
 */
int SaveBlobToFile (const char *lpcFileName, BLOB lbData)
{
 char *lpcBuffer = NULL;
 int liResult = 0;
 unsigned short liLen, liCLen;
 FILE *lfUDF = NULL;

 if (!lbData)
   liResult = 2;
 else
   if (!lbData->blob_handle)
     liResult = 2;

 if (!lpcFileName)
   liResult = 3;

 if (liResult == 0)
   {
     lfUDF = fopen(lpcFileName, "w");
     if (!lfUDF)
       liResult = 1;
   }

 if (liResult == 0)
   {
     liLen = lbData->blob_max_segment;
     lpcBuffer = malloc (sizeof(char)*(liLen + 1L));
     while ((*lbData->blob_get_segment) (lbData->blob_handle, lpcBuffer, liLen, &liCLen))
        fwrite(lpcBuffer,sizeof(char),liCLen,lfUDF);
     fclose(lfUDF);
   }

  freemem(&lpcBuffer);

 return liResult;
}

/*
 * Result:
 * 0 - OK
 * 1 - Can't open file
 * 2 - BLOB is NULL
 * 3 - Filename is empty 
 *
 * Exported in UDFs as LOADBLOBFROMFILE
 */
int LoadBlobFromFile (const char *lpcFileName, BLOB lbData)
{

 unsigned short liMaxBufSize = 8192;
 FILE *lfFile = NULL;
 long liFileLength, liBufSize, liLen, liReadLen;
 int liResult = 0;
 char *lpcBuffer = NULL;

 if (lbData == NULL)
   liResult = 2;
 else
   if (lbData->blob_handle == NULL)
     liResult = 2;

 if (lpcFileName == NULL)
   liResult = 3;
 else
   if (strlen(lpcFileName) == 0)
     liResult = 3;

 if (liResult == 0)
  {
   lfFile = fopen(lpcFileName,"r");
    if (lfFile == NULL)
     liResult = 1;
  }

 if (liResult==0)
  {
   fseek(lfFile, 0L, SEEK_END);
   liFileLength = ftell(lfFile);
   fseek(lfFile, 0L, SEEK_SET);
 
   if (liFileLength>liMaxBufSize)
     liBufSize = liMaxBufSize;
   else
     liBufSize = liFileLength;

   lpcBuffer = (char *) malloc(sizeof(char)*liBufSize);

   while(liFileLength>0)
    {
     if (liFileLength>liBufSize)
      liLen = liBufSize;
     else
      liLen = liFileLength;
     liReadLen = fread(lpcBuffer,sizeof(char),liLen,lfFile);

     (*lbData->blob_put_segment) (lbData->blob_handle, lpcBuffer, liReadLen);
     liFileLength = liFileLength - liReadLen;
    }

   fclose(lfFile);
   freemem(&lpcBuffer);

  }

 return liResult;
}

/* Result
 * substring if it have been found
 * "" if error
 *
 * Exported as BLOBSUBSTR
 */
char *BlobSubstr(BLOB lbData, int *lpiStart, int *lpiCount)
{
 char *lpcResult = NULL;
 int liResult = 0;
 char *lpcBuffer = NULL;
 short liLen, liCLen;
 int liPos, liGet, liStart, liCount;

 if (lbData == NULL)
  liResult = 1;
 else
  if (lbData->blob_handle == NULL)
   liResult = 1;

 if (lpiStart==NULL || lpiCount==NULL)
  liResult = 2;
 else
  {
   liStart=*lpiStart;
   liCount=*lpiCount;
  }

 if (!liResult)
  if (liStart<0 || liCount<=0)
   liResult = 3;

 if (!liResult)
  {
   liLen = lbData->blob_max_segment;
   lpcBuffer = malloc (liLen + 1L);
   lpcResult = MALLOC(liCount+1);
   liPos = 0;
   liGet = 1;
   liCLen = 0;
   do
    {
     liPos += liCLen;
     liGet = (*lbData->blob_get_segment) (lbData->blob_handle, lpcBuffer, liLen, &liCLen);
     lpcBuffer[liCLen]=0;

     if (liGet && liPos+liCLen>liStart && liPos<liStart+liCount)
      strncpy(lpcResult+(liPos<liStart?0:liPos-liStart), lpcBuffer+(liPos>liStart?0:liStart-liPos), liCLen-(liStart+liCount<liPos+liCLen?liPos+liCLen-liStart-liCount:0)-(liPos<liStart?liStart-liPos:0)); 
    }
   while (liGet && liPos+liCLen<liStart+liCount);

   if (liPos+liCLen>liStart)
    lpcResult[liPos+liCLen>=liStart+liCount?liCount:liPos+liCLen-liStart]=0;
   else
    lpcResult[0]=0;
  }
 else
  {
   lpcResult = MALLOC(1);
   lpcResult[0]=0;
  }

  freemem(&lpcBuffer);
 return lpcResult;
}

/* Result
 *  0 - OK
 *  1 - Can't find firebird path
 *  2 - Error while modify password
 */
int UpdateEUSER(const char *lpcSysdbaPwd, const char *lpcEuserPwd)
{
 int liResult = 0;
 char *lpcFirebird = NULL;
 char *lpcCmd = NULL;

 lpcFirebird = GetFirebirdPath();
 if (*lpcFirebird==0)
  liResult = 1;

 if (!liResult)
  {
   lpcCmd = malloc(strlen(lpcFirebird) + strlen(lpcSysdbaPwd) + strlen(lpcEuserPwd) + 100);
   // first - add
   sprintf(lpcCmd,"%sbin/gsec -user SYSDBA -password %s -add EUSER -pw %s",lpcFirebird, lpcSysdbaPwd, lpcEuserPwd);
   RunShellCmd(lpcCmd); 
   // second - modify
   sprintf(lpcCmd,"%sbin/gsec -user SYSDBA -password %s -modify EUSER -pw %s",lpcFirebird, lpcSysdbaPwd, lpcEuserPwd);
   liResult = RunShellCmd(lpcCmd)?2:0;
  }

  freemem(&lpcCmd);
  freemem(&lpcFirebird);

 return liResult;
} 

/* Result
 *  0 - OK
 *  1 - Can't find firebird path
 *  2 - Error while execute gsec
 */
int UpdateSYSDBA(const char *lpcOldPwd, const char *lpcNewPwd)
{
 int liResult = 0;
 char *lpcFirebird = NULL;
 char *lpcCmd = NULL;

 lpcFirebird = GetFirebirdPath();
 if (*lpcFirebird==0)
  liResult = 1;

 if (!liResult)
  {
   lpcCmd = malloc(strlen(lpcFirebird) + strlen(lpcOldPwd) + strlen(lpcNewPwd) + 100);
   sprintf(lpcCmd,"%sbin/gsec -user SYSDBA -password %s -modify SYSDBA -pw %s",lpcFirebird, lpcOldPwd, lpcNewPwd);
   liResult = RunShellCmd(lpcCmd)?2:0;
  }

  freemem(&lpcCmd);
  freemem(&lpcFirebird);

 return liResult;
}

 /*
  * 0 - OK
  * 1 - can't get CPU information
  * 2 - can't find CPU information in /proc/stat
  */
int GetServerLoadInfo(char *lpcRes)
{
 int liRes=0;
 char *lpcPos=NULL;
 FILE *lfProc;
 char lpcStr[1024];
 long long int liCPU[8];
 int i;

 for(i=0;i<8;i++)
  liCPU[i]=0;

 lfProc=fopen("/proc/stat","r");
 if (!lfProc)
  liRes=1;

 if (liRes!=1)
  {
   liRes=2;
   while (fgets(lpcStr,sizeof(lpcStr)-1,lfProc)!=NULL)
    {
     lpcPos=strstr(lpcStr,"cpu ");
     if (lpcPos!=NULL)
      {
       liRes=0;
       for(i=0;i<7;i++)
        {
         lpcPos=findDigit(lpcPos);
         if(lpcPos!=NULL)
          liCPU[i]=getNumber(lpcPos);
         lpcPos=findNoDigit(lpcPos);
        }
       while(lpcPos!=NULL)
        {
         lpcPos=findDigit(lpcPos);
         if(lpcPos!=NULL)
          liCPU[7]=liCPU[7]+getNumber(lpcPos);
         lpcPos=findNoDigit(lpcPos);
        }
      }
    }
   fclose(lfProc);
  }

/* liCPU:
 * [0] = user: normal processes executing in user mode
 * [1] = nice: niced processes executing in user mode
 * [2] = system: processes executing in kernel mode
 * [3] = idle: twiddling thumbs
 * [4] = iowait: waiting for I/O to complete
 * [5] = irq: servicing interrupts
 * [6] = softirq: servicing softirqs
 * [7] = other
 */

 if (liRes==0)
  sprintf(lpcRes,"CPU=%lld\nWA=%lld\nIDLE=%lld",liCPU[0]+liCPU[1]+liCPU[2]+liCPU[5]+liCPU[6]+liCPU[7],liCPU[4],liCPU[3]);
 else
  *lpcRes=0;

 return liRes;
}

