Plymouth UDF Notes - S. Friedl

2013/06/30

These UDFs have been reworked to bring them to full ANSI C standards
and generally cleaned up structurally; no more individual header files
for each associated .c file (they all now go in global.h); added max
compiler warnings.

The only functional changes have been in the DefragmentDatabase()
function, which has been rewritten to be clean and bulletproof; plus 
the very recent additions of BackupDatabase() and RestoreDatabase()
from Aleksey.

There are a number of helper routines that allow for things such as
quoting of parameters for the shell which should be used throughout
the code.

KEY CHANGES
-----------

From the original UDF2 code a number of changes were made:

* full use of ANSI C prototypes

* extensive use of the /const/ keyword

* cleaned up the Makefile extensively and enabled max compiler
  warnings; required some attention to the code to clean up things
  the compiler pointed out.

* combined all the function declarations in the various .h files
  into the single "global.h" file, and all .c files include it.
  This insures that every declaration collides with its definition,
  and that all parts of the code can see every available function.

* addition of various helper functions, especially those related to
  preventing buffer overflows. "safe_snprintf" is at the heart of
  this, promising that it will never write past the end of its buffer
  (and allowing the caller to detect that it would have).

* broke out the DefragmentDatabase() function into its own defrag.c
  file so it can be considered separately from those functions left
  behind in file_functions.c

LOGGING
-------

For testing we require some mechanisms for logging, though we may not
wish to use it in final deliveries of code; my inclination is that early
releases should include this code so we can detect bad stuff.

The original UDF code attempted to save into /var/log/EvolUDFs.log, but
the Firebird user won't always have the rights to create this file, so
it's not clear that this was ever the right place.

This code puts logging in /tmp/EvolUDFs.log and takes precautions to insure
that all users have the ability to write to the file.

Logging is enabled if the /tmp/EvolUDFs.log file exists, disabled if
not. This makes it off by default (the logfile won't exist) but makes
it easy to just turn on by touching the file and making sure it's
owned by Firebird.

When the logfile grows to >10 mbyte, it's automatically renamed to
/tmp/EvolUDFs.log.old, and there is only one level of logfile saving;
this won't ever fill up the disk.

Adding logging to the code is done with a macro:

	LOGMSG(("Entering function XYZPDQ('%s', %d)", filename, number));

The double parentheses is there as a kind of compiler trick that allows
us to add a compiler flag (-DNDEBUG) to *completely* eliminate these
function calls from the code, avoiding even the limited function-call
overhead.

I recommend leaving this log stuff on for a while.

BUILDING THE CODE
-----------------

The code is compiled on a Linux system, and it does require a bit
of setup in advance;

* Firebird 2.0.7 has to be installed; typically this is with an rpm
  package, and it installs to /opt/firebird 

* Requires Linux packages "make", "gcc", "zip", "unzip" and "sudo".

Once done, unpack the ZIP file to an empty directory and type "make".
It should look something like this:

	$ make
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic EvolUDFs.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic date_functions.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic string_functions.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic math_functions.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic misc_functions.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic file_functions.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic lib_functions.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic helpers.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic defrag.c
	gcc -c -fPIC -std=gnu99 -W -Wall -pedantic logging.c
	gcc EvolUDFs.o date_functions.o string_functions.o math_functions.o
	    misc_functions.o file_functions.o lib_functions.o helpers.o defrag.o
	    logging.o -o EvolUDFs -shared -lib_util -lgds

At this point the EvolUDFs file is in the local directory and ready to use.

For testing you can install it with "make install"; the recipe in the makefile
uses the sudo command to elevate permissions to allow a non-root user to
install the software in /opt/firebird/UDF - DO NOT DEVELOP AS ROOT!

