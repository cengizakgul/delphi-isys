// version.h
// 
// 	This file contains the macros for version definitions, and it's only
// 	here so the version stuff can be documented properly (and separately
// 	from the global.h code).
//
// VERSION HISTORY
//
// 2013/08/14 1.7; updated DeleteFile() (and added deletefile.c)
// 2013/07/29 1.6; Fixed GetSystemInfo(); added evo-udfver program
// 2013/07/03 1.5; BackupDatabase() now properly deletes backup files; 
//                 added version.h mechanism
// 2013/07/02 1.4; Steve's first release, includes backup/restore code
// 2013/04/29 1.3; initial release for Plymouth
//

#define VERSION "1.7"
#define RELEASE "2013-AUG-14"
#define INTVERSION 6           // Version information

// VERTAG is actually the name of a function found in EvolUDFs.c, so this
// effectively changes it to read;
//
//
// 	void VERTAG_6_VERTAG()
// 	{
// 	}
//
// Which Aleksey presumably looks for somehow.
//
#define VERTAG VERTAG_6_VERTAG
