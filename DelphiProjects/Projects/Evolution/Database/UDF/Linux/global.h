// global.h
//
//	This file should be included by all other .c files after the
//	system-y files; it declares all functions used throughout the
//	code so they are available everywhere.
//
// GCC FUNCTION ATTRIBUTES
// -----------------------
//
//	This code makes heavy use of the GCC __attribute__ to give the
//	compiler extra hints about how to generate better code or detect
//	more errors. They're applied to function declarations (and as
//	such only appear in this file).
//
//	Ref: http://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html
//
//	The common ones:
//
//	__attribute__((nonull(#,#,...))) - Says that pointer params <#>
//		are expected to be non-null, and if the compiler detects
//		that there *is* a NULL to be passed (at compile time) it
//		will generate a warning. 
//
//	__attribute__((format(printf,#,#)) - says that this is a printf-like
//		function and should check the format string for suitability.
//
//
//
#include "ibase.h"
#include "ib_util.h"
#include "version.h"

#define MALLOC ib_util_malloc 
//#define MALLOC malloc
 
#define LOC_STRING_SIZE 256

// Sizes of parameters of stored procedures, used to allocate
// arrays internally. We always add some extra space to allow
// for the quoting expansion (quotes, plus escape chars).
#define QUOTED_OVERHEAD		128	// add for quoting support

#define	DEFAULT_FILENAME_SIZE	(1024 + QUOTED_OVERHEAD)
#define	DEFAULT_USERNAME_SIZE	(40 + QUOTED_OVERHEAD)
#define DEFAULT_PASSWORD_SIZE	(40 + QUOTED_OVERHEAD)
#define DEFAULT_CMDLINE_SIZE	(8192)


#define gpcUDFLib "EvolUDFs"   // Filename of UDF library 

// Firebird page size; required for gbak restores
#define DB_PAGE_SIZE	8192

typedef struct blob {
        short   (*blob_get_segment) ();
        void    *blob_handle;
        int     blob_number_segments;
        int     blob_max_segment;
        int     blob_total_length;
        void    (*blob_put_segment) ();
} blob;

typedef blob *BLOB;


/* quiet the code about these when known to be unused */
#define UNUSED_PARAMETER(p)	( (void) (p) )
#define UNUSED_VARIABLE(p)	( (void) (p) )

#ifndef TRUE
#  define TRUE 1
#  define FALSE 0
#endif

// LOGGING FUNCTIONS (in logging.c)
//
// The _logmsg() function shouldn't be called by the user directly,
// because we want the ability to compile the code to exclude it
// fully, but even if the code is included it's still only enabled
// by request.
//
// Use the LOGMSG() macro with two sets of parens, which allows the
// code to do the right thing if debugging is disabled:
//
// 	LOGMSG(("hello, world %d", 123));
//
// If -DNDEBUG is passed on the compiler command line, this code is
// completely elided.  The choice of NDEBUG mirrors the macro used
// for assertions.
//

extern void _logmsg(const char *format, ...)
		__attribute__((format(printf, 1, 2)));

extern void WriteLinuxLog (const char *, const char *);

extern int loggingLevel;	// <=0 means off

extern int SetDebugLevel(const int *level);

#ifdef NDEBUG
# define LOGMSG(x)	((void) 0)
#else
# define LOGMSG(x)	_logmsg x
#endif


// EvolUDFs.h
extern long CreateClient (const char *path, const char *clname)
	__attribute__((nonnull(1,2)));

extern long NextClientNumber (const char *path, long *currclnum)
	__attribute__((nonnull(1,2)));

extern long WriteLog (char*, BLOB, long *)
	__attribute__((nonnull(1,3)));

extern int GetVersion (void);

// date_functions.h
extern ISC_QUAD * AddYear (const ISC_QUAD *, const int *)
	__attribute__((nonnull(1,2)));

extern ISC_QUAD * AddMonth (const ISC_QUAD *, const int *)
	__attribute__((nonnull(1,2)));

extern int AgeInYears (const ISC_QUAD *, const ISC_QUAD *)
	__attribute__((nonnull(1,2)));

extern int AgeInMonths (const ISC_QUAD *, const ISC_QUAD *)
	__attribute__((nonnull(1,2)));

extern int AgeInWeeks (const ISC_QUAD *, const ISC_QUAD *)
	__attribute__((nonnull(1,2)));

extern int AgeInDays (const ISC_QUAD *, const ISC_QUAD *)
	__attribute__((nonnull(1,2)));

extern int DayOfYear (const ISC_QUAD *)
	__attribute__((nonnull(1)));

extern int DayOfMonth (const ISC_QUAD *)
	__attribute__((nonnull(1)));

extern int DayOfWeek (const ISC_QUAD *)
	__attribute__((nonnull(1)));

extern ISC_QUAD * MaxDate (const ISC_QUAD *, const ISC_QUAD *)
	__attribute__((nonnull(1,2)));

extern ISC_QUAD * MinDate (const ISC_QUAD *, const ISC_QUAD *)
	__attribute__((nonnull(1,2)));

extern int Month (const ISC_QUAD *)
	__attribute__((nonnull(1)));

extern int Quarter (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern int Year (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * StripDate (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * StripTime (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern int WeekOfYear (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * BeginYear (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * BeginQuarter (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * BeginMonth (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * EndYear (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * EndQuarter (const ISC_QUAD *)
	__attribute__((nonnull(1)));
extern ISC_QUAD * EndMonth (const ISC_QUAD *)
	__attribute__((nonnull(1)));

extern char * FormatDate (const ISC_QUAD *, const char *)
	__attribute__((nonnull(1,2)));

// file_functions.h

extern int FileExists(const char *fname);
extern int CopyFile (const char *, const char *);
extern int MoveFile (const char *, const char *);
extern int DeleteFile (const char *);
extern long long int GetFileSize (const char *);
extern int GetFileList (const char *, const char *, BLOB);
extern int RunShellCmd (const char *);
extern int CopyDatabaseFile (const char *, const char *, const char *);
extern int CopyDatabase (const char *, const char *, const char *, const char *, const char *, const char *);
extern int RunGfix(const char *, const char *, const char *);
extern int ShutdownDB (const char *, const char *);
extern int OnlineDB (const char *, const char *);
extern int DeleteDatabaseFile (const char *path, const char *sysdbapass);
extern int MoveDatabaseFile (const char *, const char *, const char *);

// defrag.c
extern int DefragmentDatabase (const char *, const char *, const char *);


// lib_functions.h

extern char *findDigit (char *);
extern char *findNoDigit (char *);
extern long long int getNumber (const char *);
extern void myUID (char *);
extern int isSysdba (const char *);

// helpers.c

extern void SetFirebirdPath(const char *path);

extern int GetFirebirdPathBuf(char *obuf, size_t osize)
		__attribute__((nonnull(1)));

extern char *GetFirebirdPath(void);

extern int GetGBAKPathBuf(char *obuf, size_t osize)
		__attribute__((nonnull(1)));

extern int CreateGbakCommand(char *obuf, size_t osize, const char *fbuser, const char *fbpass,
	const char *srcdb, const char *dstdb, const char *options)
		__attribute__((nonnull));	// all of them

extern int GetSelfPathBuf(char *obuf, size_t osize)
		__attribute__((nonnull(1)));

extern char *GetSelfPath(void);

#ifdef va_start
extern int safe_vsnprintf(char *obuf, size_t osize, const char *format, va_list args)
		__attribute__((format(printf, 3, 0)))
		__attribute__((nonnull(1,3)));
#endif

extern int safe_snprintf(char *obuf, size_t osize, const char *format, ...)
		__attribute__((format(printf, 3, 4)))
		__attribute__((nonnull(1,3)));

extern int quote_for_shell(char *obuf, size_t osize, const char *instr)
		__attribute__((nonnull(1,3)));

extern int endswith(const char *bigstring, const char *smallstring)
		__attribute__((nonnull(1,2)));

extern int nstrcpy(char *dst, const char *src)
		__attribute__((nonnull(1,2)));

extern void freemem(char **p);

extern int GetFirebirdVersionBuf(char *obuf, size_t osize);

extern char *strcopy(char *dst, const char *src, size_t osize);

// extern int isNullOrEmpty(const char *s);

// math_functions.h

extern double RoundAll (double *, int *);
extern int Trunc (double *);
extern int Sign (double *);
extern int IntAnd (int *, int *);
extern int IntOr (int *, int *);
extern int IntXor (int *, int *);
extern int IntNot (int *);
extern double Abs (double *);

// misc_functions.h
extern char * ConvertNullString (const char *, const char *);
extern ISC_QUAD * ConvertNullDate (const ISC_QUAD *, const ISC_QUAD *);
extern int ConvertNullInteger (const int *, const int *);
extern double ConvertNullDouble (const double *, const double *);
extern char * IfString (const int *, const char *, const char *);
extern ISC_QUAD * IfDate (const int *, const ISC_QUAD *, const ISC_QUAD *);
extern int IfInteger (const int *, const int *, const int *);
extern double IfDouble (const int *, const double *, const double *);
extern int CompareString (const char *str1, const char *op, const char *str2);
extern int CompareDate (const ISC_QUAD *date1, const char *, const ISC_QUAD *date2);
extern int CompareInteger (const int *int1, const char *op, const int *int2);
extern int CompareDouble (const double *dbl1, const char *op, const double *dbl2);
extern int GetBlobSize (BLOB);
extern int UpdateUDF (BLOB);
extern int SaveBlobToFile (const char *, BLOB);
extern int LoadBlobFromFile (const char *, BLOB);
extern char * BlobSubstr (BLOB, int *, int *);
extern int UpdateEUSER (const char*, const char*);
extern int UpdateSYSDBA (const char*, const char*);
// extern int GetSystemInfo (char *);	// moved to helpers.c
extern int GetServerLoadInfo (char *);

// string_functions.c
extern char * ASCIIChar (const int *);
extern char * FindWord (const char *, const int *);
extern int FindWordIndex (const char *, const int *);
extern char * Trim (const char *);
extern char * LeftTrim (const char *);
extern char * RightTrim (const char *);
extern char * StrCopy (const char *, const int *, const int *);
extern char * PadLeft (const char *, const char *, const int *);
extern char * PadRight (const char *, const char *, const int *);
extern char * ProperCase (const char *);
extern char * UpperCase (const char *);
extern char * LowerCase (const char *);
extern int StrLen (const char *);
extern char * StripString (const char *, const char *);
extern int PosSubStr (const char *, const char *);
extern char *strip(char *);

// backuprestore.c
extern int BackupDatabase( const char *lpcDBName, const char *lpcBackupFile,
                           const char *lpcUsername, const char *lpcPassword );
extern int RestoreDatabase( const char *lpcBackupFile, const char *lpcDBName,
                            const char *lpcUsername, const char *lpcPassword );

// sysinfo.c - summary of all data we collect (CPU, etc.)

struct evo_sysinfo {

	char	fbversion[128];		// FB.version=LI-V2.0.7.13318 Firebird 2.0

	// From utsname; these are typically 65 in Linux, but we make extra room
	char	ostype[128];		// OS.type=Linux		        (utsname.sysname)
	char	osversion[128];		// OS.version=2.6.32-358.6.1.el6.x86_64 (utsname.release)

	int	cpusockets;		// CPU.CPUs=%d		// physical CPU chips (sockets)
	int	realcores;		// CPU.Cores=%d		// total # of real CPUs
	int	processors;		// CPU.Threads=%d	// total # of real + HT CPUs

	int	udfver;			// Version number of the UDFs (1.X)
	char	udfdate[64];		// date of the UDFs
};

// return values from FillSystemInfo
#define GETSYSTEMINFO_OK           0
#define GETSYSTEMINFO_NO_CPUINFO   1
#define GETSYSTEMINFO_NO_LINUXVER  2
#define GETSYSTEMINFO_NO_FBVER     3

extern const char *printable_GETSYSTEMINFO(int code);

extern int FillSystemInfo(struct evo_sysinfo *evoinfo); // for testing - not a UDF
extern int GetSystemInfo(char *obuf);                   // this is the UDF

// This is used to quickly determine if a string is "empty", and it's
// inlined for speed.
//
// Note that if it's not /static/ it faults; not sure why.

static inline int isNullOrEmpty(const char *s)
{
	return (s == 0) || (*s == 0);
}
