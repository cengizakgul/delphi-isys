#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "global.h"

#define iswordchar(sz, i, start)       \
          (isalnum(sz[i]) ||           \
	   ((i > start) &&             \
	    !isspace(sz[i-1]) &&       \
	    (sz[i] == '\'')))          \

static int find_char (char c, const char *sz)
{
  int i = 0;
  int len = strlen(sz);

  for (i = 0 ; 
       (i < len) && (c != sz[i]) ;
       i++) ;

  if (i == len)
    return 0;
  else
    return -1;
}

/* Implementations */

char * ASCIIChar (const int *c)
{
  char *sz = (char *) MALLOC (2);
  sz[0] = (char) *c;
  sz[1] = (char) 0;

  return sz;
}

char * FindWord (const char *sz, const int *index)
{
  char sz_res[LOC_STRING_SIZE];
  char * sz_result;
  int i;
  int j = 0;
  int len = strlen(sz);

  i = FindWordIndex (sz, index);

  while (i < len && j < LOC_STRING_SIZE - 1 &&
	 iswordchar(sz, i, *index)) {
    sz_res[j] = sz[i];
    j++;
    i++;
  }
  sz_res[j] = (char) 0;
  sz_result = (char *) MALLOC(j + 1);
  sprintf(sz_result, "%s", sz_res);

  return sz_result;
}

int FindWordIndex (const char *sz, const int *index)
{
  char sz_res[LOC_STRING_SIZE];
  int i = *index;
  int j = 0;
  int len = strlen(sz);

  while (i < len && j < LOC_STRING_SIZE - 1 &&
	 !iswordchar(sz, i, *index)) {
    sz_res[j] = sz[i];
    j++;
    i++;
  }

  return (i >= len) ? -1 : i;
}

char * Trim (const char *sz)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
  int i, j, k;

  for (i = 0 ; 
       (i < len) && isspace(sz[i]) ;
       i++) ;

  for (j = len - 1 ;
       (j >= 0) && isspace(sz[j]) ;
       j--) ;

  for (k = i ; k <= j ; k++)
    sz_result[k - i] = sz[k];
  sz_result[k - i] = (char) 0;

  return sz_result;
}

char * LeftTrim (const char *sz)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
  int i;

  for (i = 0 ; 
       (i < len) && isspace(sz[i]) ;
       i++) ;

  sprintf(sz_result, "%s", &sz[i]);
  return sz_result;
}

char * RightTrim (const char *sz)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
  int i, j;

  for (i = len - 1 ;
       (i >= 0) && isspace(sz[i]) ;
       i--) ;

  for (j = 0 ; j <= i ; j++)
    sz_result[j] = sz[j];
  sz_result[j] = (char) 0;

  return sz_result;
}

char * StrCopy (const char *sz, const int *index, const int *count)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (*count + 1);
  int i;

  for (i = *index ; (i < len) && (i - *index < *count) ; i++)
    sz_result[i - *index] = sz[i];
  sz_result[i - *index] = (char) 0;

  return sz_result;
}

char * PadLeft (const char *sz, const char *sz_pad, const int *size)
{
  int len_sz = strlen(sz);
  int len_sz_pad = strlen(sz_pad);
  int len_sz_result = *size > len_sz ? *size : len_sz;
  char * sz_result = (char *) MALLOC (len_sz_result + 1);
  int i, j;

  sz_result[len_sz_result] = (char) 0;

  for (i = len_sz - 1 ; i >= 0 ; i--)
    sz_result[len_sz_result - (len_sz - i - 1) - 1] = sz[i];

  for (i = len_sz_result - (len_sz - i - 1) - 1 ;
       i >= 0 ;
       ) {
    for (j = len_sz_pad - 1 ;
	 (j >= 0) && (i >= 0) ;
	 j--, i--)
      sz_result[i] = sz_pad[j];
  }

  return sz_result;
}

char * PadRight (const char *sz, const char *sz_pad, const int *size)
{
  int len_sz = strlen(sz);
  int len_sz_pad = strlen(sz_pad);
  int len_sz_result = *size > len_sz ? *size : len_sz;
  char * sz_result = (char *) MALLOC (len_sz_result + 1);
  int i, j;

  sz_result[len_sz_result] = (char) 0;

  for (i = 0 ; i < len_sz ; i++)
    sz_result[i] = sz[i];

  while (i < len_sz_result)
    for (j = 0 ;
	 (j < len_sz_pad) && (i < len_sz_result) ;
	 j++, i++)
      sz_result[i] = sz_pad[j];

  return sz_result;
}

char * ProperCase (const char *sz)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
  int i;
  int word_begin = 1;

  for (i = 0 ; i < len ; i++)
    if (isalnum(sz[i]) && word_begin) {
      word_begin = 0;
      sz_result[i] = toupper(sz[i]);
    }
    else if (!isalnum(sz[i])) {
      word_begin = 1;
      sz_result[i] = sz[i];
    } else
      sz_result[i] = sz[i];

  sz_result[len] = '\0';
  return sz_result;
}

char * UpperCase (const char *sz)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
  int i;

  for (i = 0 ; i < len ; i++)
    sz_result[i] = toupper(sz[i]);

  sz_result[len] = '\0';
  return sz_result;
}

char * LowerCase (const char *sz)
{
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
  int i;

  for (i = 0 ; i < len ; i++)
    sz_result[i] = tolower(sz[i]);

  sz_result[len] = '\0';
  return sz_result;
}

int StrLen (const char *sz)
{
    return strlen(sz);
}

char * StripString (const char *sz, const char *sz_strip_chars)
{
  int i;
  int j = 0;
  int len = strlen(sz);
  char * sz_result = (char *) MALLOC (len + 1);
 
  for (i = 0 ; i < len ; i++)
    if (!find_char(sz[i], sz_strip_chars)) {
      sz_result[j] = sz[i];
      j++;
    }
  sz_result[j] = (char) 0;
  return sz_result;
}
   
/*
 * PosSubStr -
 *   This is most certainly *not* the most efficient way to search for
 *   a string, but I don't really care.
 */
int PosSubStr (const char *sz_search, const char *sz)
{
  int i;
  int result = -1;
  int len_sz = strlen(sz);
  int len_sz_search = strlen(sz_search);

  for (i = 0 ; 
       (i < len_sz - len_sz_search + 1) && (result == -1) ;
       i++)
    if (strncmp(&sz[i], sz_search, len_sz_search) == 0)
      result = i;

  return result;
}
