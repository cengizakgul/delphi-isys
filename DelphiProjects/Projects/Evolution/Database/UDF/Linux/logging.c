// logging.c
// 
// 	THis module is used to enable logging and other debugging
// 	information, mainly for testing. We can call a function that
// 	enables verbose logging, which is done here.
//
// 	This is slightly tricky: the _logmsg() function does the real
// 	work, but we need to be able to compile out the calls to this
// 	entirely, so the logging code is not even found at all.
//
//	
//
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include "global.h"

int	loggingLevel = 0;

// WHERE TO PUT THE LOGS. The problem with putting them in /var/log is
// that only root can do this; the firebird user (which runs out of
// xinetd) does not. So we park it in /tmp.
// 
// We also want to insure we don't have crazy runaway logfiles, so
// when one file gets > 10mbyte, we move it to a .old version. We
// maintain only *one* old version so it doesn't fill the disk with
// a raft of historical versions.

#define	MAXLOGFILE	(10 * 1000000)		// rotate to .old when bigger than this

static const char logfilename[] = "/tmp/EvolUDFs.log";
static const char oldfilename[] = "/tmp/EvolUDFs.log.old";


// _logmsg()
//
// 	Save a message to the temporary logfile for debugging, but only
// 	if the global logging level is >0.  If we're unable to create the
// 	logfile, we turn off logging entirely.
//
// 	Note that on a busy system, there could be more than one process
// 	logging at a time, so we really have to be careful with atomic
// 	writes so that all parts of the line are written at once, not in
// 	pieces, or we'll get wierd intermixing in the output file.
//
// 	So we avoid buffered I/O and just do it ourselves in with low-leve
// 	write() code (and a large buffer to build the stuff in).
//

#ifndef NDEBUG
void _logmsg(const char *format, ...)
{
static int first_time = TRUE;

	// in case we need to create the file
	mode_t filemode = S_IRUSR | S_IWUSR	// rw-------
			| S_IRGRP | S_IWGRP	// ---rw----
			| S_IROTH | S_IWOTH;	// ------rw-

	// FIRST TIME HERE
	//
	// 1) look for autologging; if the logfile exists, we assume we turn
	// on logging by default.
	//
	// 2) If the logfile is too large, we rotate it to a ".old" version,
	// but only one so we don't have a full disk drive in/tmp.
	//
	// This has no impact on future setting of the logging levels, and
	// the size check is done only when we enter this the first time.
	//

	if ( first_time )
	{
		struct stat stbuf;

		if ( stat(logfilename, &stbuf) == 0 )	// file exists?
		{
			loggingLevel = 1;	// turn on logging

			// if the file is too big, move it. Note that it's
			// possible to have a race condition if more than one
			// fb_inet_server starts up at once, but we don't think
			// this will matter all that much
			//
			// NOTE: it's *possible* that this will fail if the /tmp
			// directory has the sticky bit set, which prevents users
			// from removing or renaming files owned by somebody else.
			//
			// $ ls -ld /tmp
			// drwxrwxrwt. 12 root root 12288 Jul  1 15:37 /tmp/
			// ---------^ sticky bit
			//
			// In practice this won't matter much, because we expect
			// most UDFs to be called mainly by fb_inet_server (which
			// always runs as "firebird"), but if we add logging
			// to other UDF functions that can be called by isql
			// when run as root, we will have issues with this.
			//
			// This means this will fail sometimes, but it will get
			// cleaned up sooner or later. Probably.
			
			if (stbuf.st_size > MAXLOGFILE)
				rename(logfilename, oldfilename);
		}


		first_time = FALSE;
	}


	if (loggingLevel <= 0) return;

	// Attempt to open/create the logfile - if we cannot do it, we're
	// *done* for the duration of this server, so we turn off logging
	// so as not to keep trying over and over.

	int ofd = open(logfilename, O_WRONLY | O_APPEND | O_CREAT, filemode);

	if (ofd < 0)
	{
		// can't open the logfile, don't try any more

		loggingLevel = 0;
	}

	else 
	{
#if NOTYET
		// If we're root, find out if the file is owned by root (or at
		// least firebird) and change ownership if possible. This doesn't
		// matter for now, maybe we'll fix it later.

		struct stat stbuf;

		if ( geteuid() == 0  &&  fstat(ofd, &stbuf) == 0 )
		{
			if ( stbuf.st_uid == 0 )
			{
				// TODO
				// owned by root, we have to change it
				// Look up firebird user
			}
		}
#endif


		va_list args;

		// Format our output line into a buffer so large we are
		// unlikely to ever overflow it; this is just a single text
		// line of output.

		char iobuf[32*1024],		// plenty big
		     *p    = iobuf,
		     *pmax = iobuf + sizeof iobuf - 3;	// room for newline + NUL

		time_t now;
		time(&now);

		struct tm timeinfo;
		localtime_r(&now, &timeinfo);

		p += sprintf(p, "%02d/%02d/%04d-%02d:%02d:%02d",
			timeinfo.tm_mon + 1,
			timeinfo.tm_mday,
			timeinfo.tm_year + 1900,
			timeinfo.tm_hour,
			timeinfo.tm_min,
			timeinfo.tm_sec);

		p += sprintf(p, ": %05d: ", getpid());

		va_start(args, format);

		safe_vsnprintf(p, (int)(pmax - p), format, args);

		va_end(args);

		// Whether safe_vsnprintf() truncated or not, it still
		// output as much of the line as possible, so we'll just
		// add a newline and write the line to the file.

		int n = strlen(iobuf);

		// trim trailing whitespace before adding newline

		while ( n > 0  &&  isspace( iobuf[n-1]) )
			n--;

		iobuf[n++] = '\n';
		iobuf[n  ] = '\0';

		write(ofd, iobuf, n);

		close(ofd);
	}
}
#endif /* NDEBUG */

// Set the global flag for the current session (mainly for debugging),
// returning the old logging level.
// 
// To enable debugging in the current session, one runs
//
// 	select setdebuglevel(1) from rdb$database;
//
// We may never really need this

int SetDebugLevel(const int *level)
{
	const int oldlevel = loggingLevel;

	loggingLevel = *level;

	return oldlevel;
}

/*
 * Open default log file and write message to log
 */
void WriteLinuxLog(const char *lpcProc, const char *lpcMess)
{
	FILE *lfLog;
	char *lpcStr;
	time_t rawtime;
	char lpcTime[80];

	if ( (lfLog = fopen(logfilename, "a")) != 0 )
	{
		lpcStr = (char *) malloc(sizeof(char)*(strlen(lpcProc)+strlen(lpcMess)+88));
		time(&rawtime);

		struct tm timeinfo;
		localtime_r(&rawtime, &timeinfo);

		strftime(lpcTime, sizeof lpcTime, "%x %X", &timeinfo);
		sprintf(lpcStr,"%s: %s - %s\n",lpcTime,lpcProc,lpcMess);
		fwrite(lpcStr,sizeof(char),strlen(lpcStr),lfLog);
		freemem(&lpcStr);
		fclose(lfLog);
	}
}
