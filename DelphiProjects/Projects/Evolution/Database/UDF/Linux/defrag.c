// defrag.c
// 
// 	The sweeping of DB files is complicated enough that we're putting
// 	it in a different module so it doesn't pollute many of the other 
// 	parts of the system.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>
#include <stdarg.h>
#include "global.h"

// DefragmentDatabase
//
//	This performs a sweep of the given database filename, and we try
//	*really hard* to avoid shenanigans by the other end. We require
//	the Firebird username and password as well.
//
//	This sweep uses a flow-through mechanism, where the backup streams
//	to the standard output, and then restores into a temporary file.
//	If all is well, the original gdb file is replaced with the
//	temporary version.
//
//	Return values:

#define DEFRAGRESULT_OK             0
#define DEFRAGRESULT_DBNOTFOUND     1	// cannot find source gdb file
#define DEFRAGRESULT_BADCREDS       2	// username or password are empty (or bad)
#define DEFRAGRESULT_GBAKERROR      3	// error while backing up or restoring databse
#define DEFRAGRESULT_DBMOVEERROR    4	// error moving the database 
#define DEFRAGRESULT_NOFBPATH       5	// cannot find Firebird path

static int _DefragmentDatabase(const char *gdbname, const char *fbuser, const char *fbpass)
{
	// PRE-CHECKS
	//
	//	Before we really do anything, make sure the file is found and we
	//	have semi-valid credentials (in the sense that the're not NULL
	//	or blank).
	//
	//	TODO: should we also check that the file is a *regular* file?
	//	It's tempting to make that check too, but if we return "db not
	//	found" it's not clear that the caller will understand this
	//	case.

	if ( isNullOrEmpty(gdbname) )
	{
		LOGMSG((" null/empty db name"));
		return DEFRAGRESULT_DBNOTFOUND;
	}

	if (FileExists(gdbname) != 0)
	{
		LOGMSG((" file %s not found", gdbname));

		return DEFRAGRESULT_DBNOTFOUND;
	}

	if ( isNullOrEmpty(fbuser) || isNullOrEmpty(fbpass) )
	{
		LOGMSG((" user/pass credentials empty"));

		return DEFRAGRESULT_BADCREDS;
	}

	// FIND FIREBIRD PATH; we need this to construct the
	// full path of the gbak command, and we get this path
	// from the path of this very executable; we assume it's
	// either isql or fb_inet_server (or maybe gbak)

	char gbakcommand[DEFAULT_FILENAME_SIZE];

	if ( GetGBAKPathBuf(gbakcommand, sizeof gbakcommand) <= 0 )
	{
		// Big hairy deal if we can't find this - ugh

		LOGMSG(("  Cannot find gbak path"));

		return DEFRAGRESULT_NOFBPATH;
	}

	// CREATE QUOTED FILENAMES
	//
	// dbname	/db/evolution/CL_123.gdb
	// tmppath	/db/evolution/CL_123.gdb.tmp
	//
	// Quoted versions insure no funny business

	char tmppath       [DEFAULT_FILENAME_SIZE];
	char quoted_gdbname[DEFAULT_FILENAME_SIZE];
	char quoted_tmppath[DEFAULT_FILENAME_SIZE];
	char quoted_fbuser [DEFAULT_USERNAME_SIZE];
	char quoted_fbpass [DEFAULT_PASSWORD_SIZE];

	if ( safe_snprintf(tmppath, sizeof tmppath, "%s.tmp", gdbname) < 0
	  || quote_for_shell(quoted_gdbname, sizeof quoted_gdbname, gdbname) < 0
	  || quote_for_shell(quoted_tmppath, sizeof quoted_tmppath, tmppath) < 0
	  || quote_for_shell(quoted_fbuser,  sizeof quoted_fbuser,  fbuser)  < 0
	  || quote_for_shell(quoted_fbpass,  sizeof quoted_fbpass,  fbpass)  < 0 )
	{
		// overflow of temporary filename buffer
		LOGMSG(("  overflow while quoting for shell"));
		return DEFRAGRESULT_GBAKERROR;
	}

	// BUILD BIG COMMAND LINE
	//
	//	It won't ever be as large as 8192, but this is a fine upper
	//	limit. This command line is quite long, and NOTE that the
	//	two strings are concatenated.

	char cmdline[DEFAULT_CMDLINE_SIZE];

	if ( safe_snprintf( cmdline, sizeof cmdline, 
	 	  "%s -BACKUP -GARBAGE_COLLECT -USER %s -PASSWORD %s %s stdout 2>/dev/null"
		"| %s -REPLACE_DATABASE -PAGE_SIZE %d -USER %s -PASSWORD %s stdin %s",
		gbakcommand,
		  quoted_fbuser,
		  quoted_fbpass,
		  quoted_gdbname,

		gbakcommand,
		  DB_PAGE_SIZE,
		  quoted_fbuser,
		  quoted_fbpass,
		  quoted_tmppath) < 0)
	{
		LOGMSG(("  ERROR: overflow creating sweep command"));
		return DEFRAGRESULT_GBAKERROR;
	}


	// DO IT!
	//
	//	Run the command, expecting to get a zero response. If it
	//	fails, we have to dump the temporary file if it happened
	//	to have been created (we don't care about error returns).

	LOGMSG(("  run %s", cmdline));

	if (RunShellCmd(cmdline) != 0)
	{
		LOGMSG(("  RunShellCmd failed"));

		unlink(tmppath);		// don't need the temp file any more

		return DEFRAGRESULT_GBAKERROR;
	}

	// WHY DO WE DO THIS?

	if (! isSysdba(fbuser))
		fbpass = "";

	LOGMSG(("  calling MoveDatabaseFile('%s', '%s', '%s')", tmppath, gdbname, fbpass));

	return (MoveDatabaseFile(tmppath, gdbname, fbpass) == 0)
		? DEFRAGRESULT_OK
		: DEFRAGRESULT_DBMOVEERROR;
}

// Wrapper so we always report the return value to the caller

int DefragmentDatabase(const char *gdbname, const char *fbuser, const char *fbpass)
{
	LOGMSG(("enter DefragmentDatabase('%s', '%s', '%s')", gdbname, fbuser, fbpass));

	int rc = _DefragmentDatabase(gdbname, fbuser, fbpass);

	LOGMSG(("  DefragmentDatabase returns %d", rc));

	return rc;
}
