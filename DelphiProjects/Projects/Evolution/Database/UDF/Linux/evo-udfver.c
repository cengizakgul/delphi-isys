/* evo-udfver.c
 * 
 * 	This program dynamically loads the EvolUDFs and finds out the version
 * 	information, allowing a quick standalone check on the Linux system of
 * 	which version any given UDF is. It does *not* requiring connecting to
 * 	any database.
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include "global.h"

static const char *udfpath = "/opt/firebird/UDF/EvolUDFs";

int main(int argc, char **argv)
{
	// command line: if the user provided an alternate name,
	// use it. Otherwise we stick with the default.

	for ( argv++; *argv; argv++ )
	{
		udfpath = *argv;
	}


	void *dlhandle = dlopen(udfpath, RTLD_LAZY);

	if ( dlhandle == 0 )
	{
		printf("Cannot dlopen(%s) [%s]\n", udfpath, dlerror());
		exit(EXIT_FAILURE);
	}

	dlerror();	// reset error counter

	// GetVersion: returns the integral sub-part of the version info
	// (for 1.6, it returns 6)

	int (*GetVersion)(void) = dlsym(dlhandle, "GetVersion");

	if ( GetVersion == 0 )
	{
		printf("Cannot find GetVersion symbol [%s]\n", dlerror());
		exit(EXIT_FAILURE);
	}
	
	const int ver = GetVersion();

	printf("%s version = %d\n", udfpath, ver);

	// GetUDFDate: in the form "2013-JUL-29"
	// Found in Ver >= 6

	const char *(*GetUDFDate)(void) = dlsym(dlhandle, "GetUDFDate");

	if ( GetUDFDate != 0 )
	{
		const char *date = GetUDFDate();

		printf("%s date = %s\n", udfpath, date);
	}

	dlclose(dlhandle);

	exit(EXIT_SUCCESS);
}
