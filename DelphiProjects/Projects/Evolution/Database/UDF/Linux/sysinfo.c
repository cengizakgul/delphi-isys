// sysinfo.c
// 
// 	This module takes care of computing the system info data that the
// 	middle tier needs, and the info returned is in the form
//
// 		FB.version=LI-V2.0.7.13318 Firebird 2.0
// 		OS.type=Linux
// 		OS.version=2.6.32-358.6.1.el6.x86_64
// 		CPU.CPUs=1
// 		CPU.Cores=2
// 		CPU.Threads=4
//
// 	(as a long string with newlines between).
//
//	Each section is computed separately.
//
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/utsname.h>
#include "global.h"


// FillCPUInfo()
//
//	THIS IS TRICKY
//
//	This formerly got its data from the "lscpu" program, but it's not
//	widely available enough so we have to roll our own by reading from
//	/proc/cpuinfo.
//
//	It took quite some time to get this algorithm sorted out, but it seems
//	to work everywhere.
//
//	This algorithm *requires* that all the CPUs in the machine are the
//	same, and there are probably corner cases that make it not work
//	right.
//
//	These are the items we're looking for:
//
//	  ***	processor       : 1
//		vendor_id       : GenuineIntel
//		cpu family      : 6
//		model           : 54
//		model name      : Intel(R) Atom(TM) CPU D2700   @ 2.13GHz
//		stepping        : 1
//		cpu MHz         : 2128.004
//		cache size      : 512 KB
//		physical id     : 0
//	  ***	siblings        : 4
//		core id         : 0
//	  ***	cpu cores       : 2
//		apicid          : 1
//		initial apicid  : 1
//		fpu             : yes
//		fpu_exception   : yes
//		cpuid level     : 10
//		wp              : yes
//		flags           : fpu vme de pse tsc {many deleted}
//		bogomips        : 4256.00
//		clflush size    : 64
//		cache_alignment : 64
//		address sizes   : 36 bits physical, 48 bits virtual
//		power management:
//
//	By counting the "processor" lines, we learn how many total cores there
//	are, both real and hyperthreading.  
//
//	"Siblings" are how many real+HT cores there are per socketed chip
//
//	"cpu cores" are hnw many real cores there are per socketed chip
//
//	From here we can calculate everything else

static int FillCPUInfo(struct evo_sysinfo *evoinfo)
{
	FILE *ifp;

	if ( (ifp = fopen("/proc/cpuinfo", "r")) == 0 )
	{
		// FAIL: could not open cpuinfo
		return -1;
	}

	char iobuf[1024];		// flags line can be really long

	int processors = 0;		// count of all "processor" lines
	int siblings   = 1;
	int cpucores   = 1;

	while ( fgets(iobuf, sizeof iobuf, ifp) )
	{
		// split the line into name/value pairs. If we don't have
		// the value, it's probably a blank line or something.

		char *tag = iobuf;
		char *val = strchr(iobuf, ':');

		if (val == 0) continue;

		*val++ = '\0';

		// tag and value both have to be stripped

		strip(tag);

		// skip the leading space after the :
		for ( ; isspace(*val); val++ )
			;

		strip(val);	// including the newline

		if ( strcmp(tag, "processor") == 0 )
		{
			// we don't care about the processor ID, we just count them
			processors++;
		}

		else if ( strcmp(tag, "siblings") == 0 )
		{
			// total real + virtual CPUs per socket

			siblings = atoi(val);
		}
		else if ( strcmp(tag, "cpu cores") == 0)
		{
			// total real CPUs per socket
			cpucores = atoi(val);
		}
	}

	fclose(ifp);

	// do the math.

	evoinfo->processors = processors;
	evoinfo->cpusockets = processors / siblings;
	evoinfo->realcores  = evoinfo->cpusockets * cpucores;

	return 0;
}

// FillSystemInfo
//
//	Given the sysinfo buffer, fill all the parts of it, returning one
//	of the GETSYSTEMINFO_* result macros.  This is meant to be used
//	in a standalone way, not called directly as a UDF (GetSystemInfo
//	should be used for that).  We expect test-sysinfo.c will be used
//	to exercise this function.

int FillSystemInfo(struct evo_sysinfo *evoinfo)
{
	if ( GetFirebirdVersionBuf(evoinfo->fbversion, sizeof evoinfo->fbversion) <= 0)
	{
		// FAILED: couldn't get Evo info
		return GETSYSTEMINFO_NO_FBVER;
	}

	// UTSNAME stuff
	//
	struct utsname udata;

	if ( uname(&udata) != 0)
	{
		// FAILED: could not get system name
		return GETSYSTEMINFO_NO_LINUXVER;
	}
	else
	{
		strcopy(evoinfo->ostype,    udata.sysname, sizeof evoinfo->ostype);
		strcopy(evoinfo->osversion, udata.release, sizeof evoinfo->osversion);
	}

	if ( FillCPUInfo(evoinfo) != 0)
	{
		// FAILED: can't find the CPU info (don't know why)
		return GETSYSTEMINFO_NO_CPUINFO;
	}

	// Now the UDF info; nobody strictly asked for it, but it seems
	// like a reasonable place to put it.
	evoinfo->udfver = INTVERSION;			// 6
	strcopy(evoinfo->udfdate, RELEASE, sizeof evoinfo->udfdate);	// "2013-JUL-29"

	return GETSYSTEMINFO_OK;
}

// for convenient error reporting by test-sysinfo

const char *printable_GETSYSTEMINFO(int rc)
{
	switch (rc)
	{
	  case GETSYSTEMINFO_OK:          return "GETSYSTEMINFO_OK";
	  case GETSYSTEMINFO_NO_CPUINFO:  return "GETSYSTEMINFO_NO_CPUINFO";
	  case GETSYSTEMINFO_NO_LINUXVER: return "GETSYSTEMINFO_NO_LINUXVER";
	  case GETSYSTEMINFO_NO_FBVER:    return "GETSYSTEMINFO_NO_FBVER";
	  default:                        return "???";
	}
}

// GetSystemInfo
//
// 	Populate the given buffer with the evo_sysinfo data as a single long 
// 	string, with a newline after each line. Return is the same as FillSystemInfo()
//
// 	SECURITY: we do not know the size of the pointed-to buffer, which makes
// 	me nervous; it should really be a bounded buffer. But the SERVICE.gdb
// 	database has this function with a huge size:
// 	
// 		sql> show function getsysteminfo;
//
// 		Function GETSYSTEMINFO:
// 		Function library is EvolUDFs
// 		Entry point is GetSystemInfo
// 		Returns  CSTRING(32000) CHARACTER SET NONE
//
//	So we're unlikely to overflow anything.
//
int GetSystemInfo(char *obuf)
{
	obuf[0] = '\0';		// insure always NUL terminated

	struct evo_sysinfo einfo;
	
	const int rc = FillSystemInfo(&einfo);

	if (rc != GETSYSTEMINFO_OK) return rc;

	char *p = obuf;

	p += sprintf(p, "OS.type=%s\n",         einfo.ostype);
	p += sprintf(p, "OS.version=%s\n",      einfo.osversion);
	p += sprintf(p, "FB.version=%s\n",      einfo.fbversion);
	p += sprintf(p, "CPU.CPUs=%d\n",        einfo.cpusockets);
	p += sprintf(p, "CPU.Cores=%d\n",       einfo.realcores);
	p += sprintf(p, "CPU.Threads=%d\n",     einfo.processors);

	return GETSYSTEMINFO_OK;
}
