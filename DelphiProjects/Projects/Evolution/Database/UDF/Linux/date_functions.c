#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "global.h"

#define SUNDAY 0
#define MONDAY 1
#define TUESDAY 2
#define WEDNESDAY 3
#define THURSDAY 4
#define FRIDAY 5
#define SATURDAY 6

#define JANUARY 0
#define FEBRUARY 1
#define MARCH 2
#define APRIL 3
#define MAY 4
#define JUNE 5
#define JULY 6
#define AUGUST 7
#define SEPTEMBER 8
#define OCTOBER 9
#define NOVEMBER 10
#define DECEMBER 11


// isleap()
//
//      These happen every 4 years, *except* not every 100 years, *but* ever 400 years.
//
//      By using an inline function it's easier to document and be sure we got it right
//      when compared to a single expression.

static inline int isleap(int year)
{
	if ((year % 4) != 0)    return FALSE;
	if ((year % 400) == 0)  return TRUE;
	if ((year % 100) == 0)  return FALSE;

	return TRUE;
}

static ISC_QUAD * gen_ib_date (const struct tm *t)
{
  ISC_QUAD *ibd = (ISC_QUAD *) MALLOC (sizeof(ISC_QUAD));
  isc_encode_date(t, ibd);
  return ibd;
}


ISC_QUAD * AddYear (const ISC_QUAD *ib_date, const int *years_to_add)
{
  struct tm t;

  isc_decode_date(ib_date, &t);
  t.tm_year += *years_to_add;

  /*
   * If it's February, and the day of the month is 29, and
   * it's *not* a leap year, then adjust the day
   */
  if (t.tm_mon == FEBRUARY && t.tm_mday > 28 && !isleap(t.tm_year + 1900))
    t.tm_mday = 28;

  return (ISC_QUAD *) gen_ib_date(&t);
}

ISC_QUAD * AddMonth (const ISC_QUAD *ib_date, const int *months_to_add)
{
  struct tm t;
  int tm_mon, tm_year;

  isc_decode_date (ib_date, &t);
  tm_mon = t.tm_mon + (*months_to_add % 12);
  tm_year = t.tm_year + (*months_to_add / 12);
  if ((*months_to_add > 0) && (tm_mon < t.tm_mon))
    tm_year++;
  else if ((*months_to_add < 0) && (tm_mon > t.tm_mon))
    tm_year--;

  /*
   * Now, check that the month day is correctly adjusted.
   * 30 days has Sep, Apr, Jun, Nov all the rest have 31 except Feb
   */
  if (tm_mon != FEBRUARY) {
    if (t.tm_mday > 30) 
      switch (tm_mon) {
      case APRIL:
      case JUNE:
      case SEPTEMBER:
      case NOVEMBER:
	t.tm_mday = 30;
      }
  }
  else if (t.tm_mday > 28)
    t.tm_mday = isleap(tm_year + 1900) ? 29 : 28;

  t.tm_mon = tm_mon;
  t.tm_year = tm_year;

  return (ISC_QUAD *) gen_ib_date(&t);
}

int AgeInYears (const ISC_QUAD *ib_date, const ISC_QUAD *ib_ref_date)
{
  struct tm t, t_ref;
  int tmp;

  isc_decode_date(ib_date, &t);
  isc_decode_date(ib_ref_date, &t_ref);

  tmp = t.tm_year - t_ref.tm_year;
  if (t_ref.tm_yday < t.tm_yday)
    tmp--;
  if (tmp < 0)
    tmp = 0;
  
  return(tmp);
}

int AgeInMonths (const ISC_QUAD *ib_date, const ISC_QUAD *ib_ref_date)
{
  struct tm t, t_ref;

  isc_decode_date(ib_date, &t);
  isc_decode_date(ib_ref_date, &t_ref);

  return ((t.tm_year - t_ref.tm_year) * 12) + t.tm_mon - t_ref.tm_mon;
}

/*
 * Bases it's computation on the fact that the year has
 * an *average* of 52 weeks. So this is most certainly an
 * approximate function, but I'm quite confident that it is
 * a reasonably precise function.
 */
int AgeInWeeks (const ISC_QUAD *ib_date, const ISC_QUAD *ib_ref_date)
{
  struct tm t, t_ref;

  isc_decode_date(ib_date, &t);
  isc_decode_date(ib_ref_date, &t_ref);

  return
    ((t.tm_year - t_ref.tm_year) * 52) +
    (((t.tm_yday - t.tm_wday) -
      (t_ref.tm_yday - t_ref.tm_wday)) / 7);
}

int AgeInDays (const ISC_QUAD *ib_date, const ISC_QUAD *ib_ref_date)
{
  struct tm t, t_ref;
  int i, tmp;

  isc_decode_date(ib_date, &t);
  isc_decode_date(ib_ref_date, &t_ref);

  if (t.tm_year == t_ref.tm_year)
    return(t.tm_yday - t_ref.tm_yday);

  tmp = t.tm_yday + 1 + (365 - t_ref.tm_yday + isleap(t_ref.tm_year + 1900));   
  for (i = t_ref.tm_year + 1; i < t.tm_year; i++)
    tmp += 365 + isleap(i);
  return (tmp);
}

int DayOfYear (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  return t.tm_yday + 1;
}

int DayOfMonth (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  return t.tm_mday;
}

int DayOfWeek (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  return t.tm_wday + 1;
}

ISC_QUAD * MaxDate (const ISC_QUAD *ib_date_1, const ISC_QUAD *ib_date_2)
{
  ISC_QUAD * result = (ISC_QUAD *) MALLOC(sizeof(ISC_QUAD));

  if ((ib_date_1->isc_quad_high > ib_date_2->isc_quad_high) ||
      ((ib_date_1->isc_quad_high == ib_date_2->isc_quad_high) &&
       (ib_date_1->isc_quad_low > ib_date_2->isc_quad_low)))
    *result = *ib_date_1;
  else
    *result = *ib_date_2;

  return result;
}

ISC_QUAD * MinDate (const ISC_QUAD *ib_date_1, const ISC_QUAD *ib_date_2)
{
  ISC_QUAD * result = (ISC_QUAD *) MALLOC(sizeof(ISC_QUAD));

  if ((ib_date_1->isc_quad_high < ib_date_2->isc_quad_high) ||
      ((ib_date_1->isc_quad_high == ib_date_2->isc_quad_high) &&
       (ib_date_1->isc_quad_low < ib_date_2->isc_quad_low)))
    *result = *ib_date_1;
  else
    *result = *ib_date_2;

  return result;
}

int Month (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  return t.tm_mon + 1;
}

int Quarter (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  return (t.tm_mon / 4) + 1;
}

int Year (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  return t.tm_year + 1900;
}

ISC_QUAD * StripDate (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  t.tm_mday = 0;
  t.tm_mon = 0;
  t.tm_year = 0;
  t.tm_wday = 0;
  t.tm_yday = 0;

  return gen_ib_date(&t);
}

ISC_QUAD * StripTime (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  t.tm_sec = 0;
  t.tm_min = 0;
  t.tm_hour = 0;
  t.tm_isdst = 0;

  return gen_ib_date(&t);
}

int WeekOfYear (const ISC_QUAD *ib_date)
{
  struct tm t;
  int dow_ybeg, yr, wk;

  isc_decode_date(ib_date, &t);

  yr = t.tm_year + 1900;

  /* On what day of the week did the year begin? */
  dow_ybeg = ((t.tm_wday + 7) - (t.tm_yday % 7)) % 7;

  wk = ((t.tm_yday - t.tm_wday + 7) / 7) + 1;
  if (dow_ybeg == SUNDAY)
    return wk - 1;
  else
    return wk;
}

ISC_QUAD * BeginYear (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  t.tm_mday = 1;
  t.tm_mon = JANUARY;

  return gen_ib_date(&t);
}

ISC_QUAD * BeginQuarter (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  t.tm_mday = 1;
  t.tm_mon = (t.tm_mon / 3) * 3;

  return gen_ib_date(&t);
}

ISC_QUAD * BeginMonth (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  t.tm_mday = 1;

  return gen_ib_date(&t);
}

ISC_QUAD * EndYear (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  t.tm_mday = 31;
  t.tm_mon = DECEMBER;

  return gen_ib_date(&t);
}

ISC_QUAD * EndQuarter (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  if ((t.tm_mon == JANUARY) || (t.tm_mon == FEBRUARY) || (t.tm_mon == MARCH))
  {
    t.tm_mday = 31;
    t.tm_mon = MARCH;
  }   
  else
  if ((t.tm_mon == APRIL) || (t.tm_mon == MAY) || (t.tm_mon == JUNE))
  {
    t.tm_mday = 30;
    t.tm_mon = JUNE;
  }   
  else
  if ((t.tm_mon == JULY) || (t.tm_mon == AUGUST) || (t.tm_mon == SEPTEMBER))
  {
    t.tm_mday = 30;
    t.tm_mon = SEPTEMBER;
  }   
  else
  {
    t.tm_mday = 31;
    t.tm_mon = DECEMBER;
  }   


  return gen_ib_date(&t);
}

ISC_QUAD * EndMonth (const ISC_QUAD *ib_date)
{
  struct tm t;
  isc_decode_date(ib_date, &t);

  if (t.tm_mon != FEBRUARY)
  {
    if ((t.tm_mon == SEPTEMBER) || (t.tm_mon == APRIL) || (t.tm_mon == JUNE) || (t.tm_mon == NOVEMBER))
      t.tm_mday = 30;
    else
      t.tm_mday = 31;
  }   
  else
  {
    t.tm_mday = isleap(t.tm_year + 1900) ? 29 : 28;
  }   


  return gen_ib_date(&t);
}

char * FormatDate(const ISC_QUAD *ib_date, const char *FormatString)
{
  struct tm t;
  int i;
  int count;
  char * NewFormat;
  char * UpperFS;
  char * result = (char *) MALLOC(LOC_STRING_SIZE * 4);
  strcpy(result, "");

  isc_decode_date(ib_date, &t);

  if ((strlen(FormatString) == 0) || (toupper(FormatString[0]) == 'C'))
  {
    strftime(result, LOC_STRING_SIZE * 4, "%c", &t);
    return result;  
  }

  NewFormat = (char *) malloc(LOC_STRING_SIZE);
  UpperFS = (char *) malloc(strlen(FormatString) + 1);

  strcpy(UpperFS, FormatString);
  for (i = strlen(UpperFS) - 1; i >= 0; i--)
    UpperFS[i] = toupper(UpperFS[i]);

  i = 0;
  while (i < (int)strlen(FormatString))
  {
    switch (FormatString[i])
    { 
      case 'd':
      case 'D':
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'D')
	{
	  i++;
	  count++;
	}
	i--;
	switch (count)
	{
	  case 3:
	    strftime(NewFormat, LOC_STRING_SIZE, "%a", &t);
	    strcat(result, NewFormat);
	    break;
	  case 4:
	    strftime(NewFormat, LOC_STRING_SIZE, "%A", &t);
	    strcat(result, NewFormat);
	    break;
	  default:
	    strftime(NewFormat, LOC_STRING_SIZE, "%d", &t);
	    if ((NewFormat[0] == '0') && ((count == 1) || (count == 5)))
      	      strncat(result, &NewFormat[1], 1);
	    else
      	      strcat(result, NewFormat);
	    break;
	}
	break;
      case 'm':
      case 'M':
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'M')
	{
	  i++;
	  count++;
	}
	i--;
	switch (count)
	{
	  case 1:
	  case 2:
	    strftime(NewFormat, LOC_STRING_SIZE, "%m", &t);
	    if ((NewFormat[0] == '0') && (count == 1))
      	      strncat(result, &NewFormat[1], 1);
	    else
      	      strcat(result, NewFormat);
	    break;
	  case 3:
	    strftime(NewFormat, LOC_STRING_SIZE, "%b", &t);
	    strcat(result, NewFormat);
	    break;
	  case 4:
	    strftime(NewFormat, LOC_STRING_SIZE, "%B", &t);
	    strcat(result, NewFormat);
	    break;
	}
	break;
      case 'y':
      case 'Y':
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'Y')
	{
	  i++;
	  count++;
	}
	i--;
	switch (count)
	{
	  case 1:
	  case 2:
	    strftime(NewFormat, LOC_STRING_SIZE, "%y", &t);
      	    strcat(result, NewFormat);
	    break;
	  default:
	    strftime(NewFormat, LOC_STRING_SIZE, "%Y", &t);
	    strcat(result, NewFormat);
	    break;
	}
	break;
      case 'h':
      case 'H':
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'H')
	{
	  i++;
	  count++;
	}
	i--;
        if (strstr(UpperFS, "AM/PM") || strstr(UpperFS, "A/P"))
          strftime(NewFormat, LOC_STRING_SIZE, "%I", &t);
	else
          strftime(NewFormat, LOC_STRING_SIZE, "%H", &t);
	if ((NewFormat[0] == '0') && (count == 1))
      	  strncat(result, &NewFormat[1], 1);
        else
      	  strcat(result, NewFormat);
	break;
      case 'n':
      case 'N':
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'N')
	{
	  i++;
	  count++;
	}
	i--;
	strftime(NewFormat, LOC_STRING_SIZE, "%M", &t);
	if ((NewFormat[0] == '0') && (count == 1))
      	  strncat(result, &NewFormat[1], 1);
        else
      	  strcat(result, NewFormat);
	break;
      case 's':
      case 'S':
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'S')
	{
	  i++;
	  count++;
	}
	i--;
	strftime(NewFormat, LOC_STRING_SIZE, "%S", &t);
	if ((NewFormat[0] == '0') && (count == 1))
      	  strncat(result, &NewFormat[1], 1);
        else
      	  strcat(result, NewFormat);
	break;
      case 'z':
      case 'Z': /* milliseconds do not exist */
        count = 1;
	i++;
	while (toupper(FormatString[i]) == 'Z')
	{
	  i++;
	  count++;
	}
	i--;
	break;
      case 'a':
      case 'A':
        if ((FormatString[i+1] == '/') && (toupper(FormatString[i+2]) == 'P'))
	{
          strftime(NewFormat, LOC_STRING_SIZE, "%H", &t);
	  if (atoi(NewFormat) > 11)
            strncat(result, &FormatString[i+2], 1);
	  else
            strncat(result, &FormatString[i], 1);
	  i += 2;
	}
	else
        if ((toupper(FormatString[i+1]) == 'M') && (FormatString[i+2] == '/') && (toupper(FormatString[i+3]) == 'P') && (toupper(FormatString[i+4]) == 'M'))
	{
          strftime(NewFormat, LOC_STRING_SIZE, "%H", &t);
	  if (atoi(NewFormat) > 11)
            strncat(result, &FormatString[i+3], 2);
	  else
            strncat(result, &FormatString[i], 2);
	  i += 4;
	}
	else
          strncat(result, &FormatString[i], 1);
	break;
      case '\'':
	i++;
	while ((FormatString[i] != '\'') && (i < (int)strlen(FormatString)))
	{
          strncat(result, &FormatString[i], 1);
	  i++;
	}
	i--;
	break;
      case '\"':
	i++;
	while ((FormatString[i] != '\"') && (i < (int)strlen(FormatString)))
	{
          strncat(result, &FormatString[i], 1);
	  i++;
	}
	i--;
	break;
      default:
        strncat(result, &FormatString[i], 1);
	break;
    }
    i++;
  }

  freemem(&NewFormat);
  freemem(&UpperFS);
  return result;
}
