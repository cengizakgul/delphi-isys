#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "global.h"

double RoundAll (double *value, int *precision)
{
  double newvalue;

  newvalue = *value * pow(10, *precision);
  if (newvalue < 0)
    return ((int)(newvalue - 0.5)) / pow(10, *precision);
  else
    return ((int)(newvalue + 0.5)) / pow(10, *precision);
}

int Trunc (double *value)
{
  return (int) (*value);
}

int Sign (double *value)
{
  if (*value > 0)
    return (1);
  else
  if (*value < 0)
    return (-1);
  else
    return (0);
}

int IntAnd (int *FirstInt, int *SecondInt)
{
  if ((*FirstInt != 0) && (*SecondInt != 0))
    return (1);
  else
    return (0);
}

int IntOr (int *FirstInt, int *SecondInt)
{
  if ((*FirstInt != 0) || (*SecondInt != 0))
    return (1);
  else
    return (0);
}

int IntXor (int *FirstInt, int *SecondInt)
{
  if (((*FirstInt == 0) && (*SecondInt != 0)) || ((*FirstInt != 0) && (*SecondInt == 0)))
    return (1);
  else
    return (0);
}

int IntNot (int *FirstInt)
{
  if (*FirstInt == 0)
    return (1);
  else
    return (0);
}

double Abs (double *value)
{
  return fabs(*value);
}
