CONNECT "thor:/db/orange/CL_2.gdb" USER "SYSDBA" PASSWORD "pps97";

update rdb$procedures set rdb$owner_name='EUSER';
update rdb$relations set rdb$owner_name='EUSER' where rdb$system_flag = 0 or rdb$system_flag is null;
delete from rdb$user_privileges where rdb$grantor = 'SYSDBA';
delete from rdb$user_privileges where rdb$user = 'SYSDBA';
commit;
grant all on rdb$roles to EUSER;

COMMIT;
