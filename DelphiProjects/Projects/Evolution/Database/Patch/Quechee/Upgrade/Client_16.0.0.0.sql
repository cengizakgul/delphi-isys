/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.16';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^

CREATE GENERATOR G_CO_BENEFIT_ENROLLMENT
^

CREATE GENERATOR G_CO_BENEFIT_ENROLLMENT_VER
^


DROP TRIGGER T_AD_CL_E_DS_9
^


DROP TRIGGER T_AIU_CL_E_DS_3
^


DROP TRIGGER T_AU_CL_E_DS_9
^


DROP TRIGGER T_BIU_CL_E_DS_2
^

ALTER TABLE CL_E_DS
ADD ROUND_OT_CALCULATION EV_CHAR1  NOT NULL
^

ALTER TABLE CL_E_DS
ALTER COLUMN ROUND_OT_CALCULATION POSITION 91
^

UPDATE CL_E_DS SET ROUND_OT_CALCULATION = 'N'
^

ALTER TABLE CL_E_DS
ADD MINIMUM_HW_HOURS EV_AMOUNT 
^

ALTER TABLE CL_E_DS
ALTER COLUMN MINIMUM_HW_HOURS POSITION 92
^

ALTER TABLE CL_E_DS
ADD MAXIMUM_HW_HOURS EV_AMOUNT 
^

ALTER TABLE CL_E_DS
ALTER COLUMN MAXIMUM_HW_HOURS POSITION 93
^

ALTER TABLE CL_E_DS
ADD HW_FUNDING_CL_E_D_GROUPS_NBR EV_INT 
^

ALTER TABLE CL_E_DS
ALTER COLUMN HW_FUNDING_CL_E_D_GROUPS_NBR POSITION 94
^

ALTER TABLE CL_E_DS
ADD HW_EXCESS_CL_E_D_GROUPS_NBR EV_INT 
^

ALTER TABLE CL_E_DS
ALTER COLUMN HW_EXCESS_CL_E_D_GROUPS_NBR POSITION 95
^

COMMIT
^

CREATE INDEX FK_CL_E_DS_14 ON CL_E_DS (HW_FUNDING_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_CL_E_DS_15 ON CL_E_DS (HW_EXCESS_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^


DROP TRIGGER T_AUD_CL_E_D_GROUPS_2
^

DROP INDEX FK_CL_UNION_2
^

COMMIT
^

CREATE INDEX FK_CL_UNION_1 ON CL_UNION (CL_E_D_GROUPS_NBR)
^


DROP TRIGGER T_AD_CO_9
^


DROP TRIGGER T_AIU_CO_3
^


DROP TRIGGER T_AU_CO_9
^


DROP TRIGGER T_AU_CO_8
^


DROP TRIGGER T_BIU_CO_2
^

ALTER TABLE CO
ADD ANNUAL_FORM_TYPE EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ANNUAL_FORM_TYPE POSITION 284
^

UPDATE CO SET ANNUAL_FORM_TYPE = 'F'
^

ALTER TABLE CO
ADD PRENOTE EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN PRENOTE POSITION 285
^

UPDATE CO SET PRENOTE = 'N'
^

ALTER TABLE CO
ADD ENABLE_DD EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ENABLE_DD POSITION 286
^

UPDATE CO SET ENABLE_DD = 'N'
^

ALTER TABLE CO
ADD ESS_OR_EP EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ESS_OR_EP POSITION 287
^

UPDATE CO SET ESS_OR_EP = 'S'
^

ALTER TABLE CO
ADD ACA_EDUCATION_ORG EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ACA_EDUCATION_ORG POSITION 288
^

UPDATE CO SET ACA_EDUCATION_ORG = 'N'
^

ALTER TABLE CO
ADD ACA_STANDARD_HOURS EV_AMOUNT 
^

ALTER TABLE CO
ALTER COLUMN ACA_STANDARD_HOURS POSITION 289
^

ALTER TABLE CO
ADD ACA_DEFAULT_STATUS EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ACA_DEFAULT_STATUS POSITION 290
^

UPDATE CO SET ACA_DEFAULT_STATUS = 'N'
^

ALTER TABLE CO
ADD ACA_CONTROL_GROUP EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ACA_CONTROL_GROUP POSITION 291
^

UPDATE CO SET ACA_CONTROL_GROUP = 'A'
^

ALTER TABLE CO
ADD ACA_INITIAL_PERIOD_FROM EV_DATE 
^

ALTER TABLE CO
ALTER COLUMN ACA_INITIAL_PERIOD_FROM POSITION 292
^

ALTER TABLE CO
ADD ACA_INITIAL_PERIOD_TO EV_DATE 
^

ALTER TABLE CO
ALTER COLUMN ACA_INITIAL_PERIOD_TO POSITION 293
^

ALTER TABLE CO
ADD ACA_STABILITY_PERIOD_FROM EV_DATE 
^

ALTER TABLE CO
ALTER COLUMN ACA_STABILITY_PERIOD_FROM POSITION 294
^

ALTER TABLE CO
ADD ACA_STABILITY_PERIOD_TO EV_DATE 
^

ALTER TABLE CO
ALTER COLUMN ACA_STABILITY_PERIOD_TO POSITION 295
^

ALTER TABLE CO
ADD ACA_USE_AVG_HOURS_WORKED EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ACA_USE_AVG_HOURS_WORKED POSITION 296
^

UPDATE CO SET ACA_USE_AVG_HOURS_WORKED = 'N'
^

ALTER TABLE CO
ADD ACA_AVG_HOURS_WORKED_PERIOD EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ACA_AVG_HOURS_WORKED_PERIOD POSITION 297
^

UPDATE CO SET ACA_AVG_HOURS_WORKED_PERIOD = 'W'
^

ALTER TABLE CO
ADD ACA_ADD_EARN_CL_E_D_GROUPS_NBR EV_INT 
^

ALTER TABLE CO
ALTER COLUMN ACA_ADD_EARN_CL_E_D_GROUPS_NBR POSITION 298
^

COMMIT
^

CREATE INDEX FK_CO_30 ON CO (ACA_ADD_EARN_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^


DROP TRIGGER T_AD_CO_AUTO_ENLIST_RETURNS_9
^


DROP TRIGGER T_AU_CO_AUTO_ENLIST_RETURNS_9
^


DROP TRIGGER T_BIU_CO_AUTO_ENLIST_RETURNS_2
^

ALTER TABLE CO_AUTO_ENLIST_RETURNS
ADD CO_REPORTS_NBR EV_INT 
^

ALTER TABLE CO_AUTO_ENLIST_RETURNS
ALTER COLUMN CO_REPORTS_NBR POSITION 8
^

COMMIT
^

CREATE INDEX FK_CO_AUTO_ENLIST_RETURNS_2 ON CO_AUTO_ENLIST_RETURNS (CO_REPORTS_NBR)
^


DROP TRIGGER T_AUD_CO_REPORTS_2
^


DROP TRIGGER T_AD_CO_SERVICES_9
^


DROP TRIGGER T_AU_CO_SERVICES_9
^

ALTER TABLE CO_SERVICES
ADD NOTES EV_BLOB_TXT 
^

ALTER TABLE CO_SERVICES
ALTER COLUMN NOTES POSITION 24
^


DROP TRIGGER T_AD_CO_TAX_RETURN_QUEUE_9
^


DROP TRIGGER T_AU_CO_TAX_RETURN_QUEUE_9
^


DROP TRIGGER T_BIU_CO_TAX_RETURN_QUEUE_2
^

ALTER TABLE CO_TAX_RETURN_QUEUE
ADD CO_REPORTS_NBR EV_INT 
^

ALTER TABLE CO_TAX_RETURN_QUEUE
ALTER COLUMN CO_REPORTS_NBR POSITION 18
^

COMMIT
^

CREATE INDEX FK_CO_TAX_RETURN_QUEUE_3 ON CO_TAX_RETURN_QUEUE (CO_REPORTS_NBR)
^


DROP TRIGGER T_AD_CO_WORKERS_COMP_9
^


DROP TRIGGER T_AIU_CO_WORKERS_COMP_3
^


DROP TRIGGER T_AD_EE_9
^


DROP TRIGGER T_AIU_EE_3
^


DROP TRIGGER T_AU_EE_9
^

ALTER TABLE EE
ADD ACA_STANDARD_HOURS EV_AMOUNT 
^

ALTER TABLE EE
ALTER COLUMN ACA_STANDARD_HOURS POSITION 148
^

ALTER TABLE EE
ADD ACA_COVERAGE_OFFER EV_STR2 
^

ALTER TABLE EE
ALTER COLUMN ACA_COVERAGE_OFFER POSITION 149
^

update EE set ACA_STATUS = 'N' where ACA_STATUS = 'T'
^
update EE set ACA_STATUS = 'F' where ACA_STATUS = 'U'
^
update EE set ACA_STATUS = 'P' where ACA_STATUS = 'A'
^
update EE set ACA_STATUS = 'P' where ACA_STATUS = 'H'
^
update EE set ACA_STATUS = 'V' where ACA_STATUS = '1'
^
update EE set ACA_STATUS = 'V' where ACA_STATUS = 'O'
^
update EE set ACA_STATUS = 'V' where ACA_STATUS = 'D'
^


DROP TRIGGER T_AD_EE_BENEFIT_PAYMENT_9
^

DROP TRIGGER T_AU_EE_BENEFIT_PAYMENT_9
^

DROP TRIGGER T_BIU_EE_BENEFIT_PAYMENT_3
^

ALTER TABLE EE_BENEFIT_PAYMENT
ALTER COLUMN PAYMENT_NUMBER TYPE EV_STR20
^

DROP INDEX FK_EE_BENEFIT_REFUSAL_2
^

DROP INDEX FK_EE_BENEFIT_REFUSAL_3
^

DROP INDEX FK_EE_BENEFIT_REFUSAL_4
^

DROP INDEX LK_EE_BENEFIT_REFUSAL
^


DROP TRIGGER T_AD_EE_BENEFIT_REFUSAL_9
^


DROP TRIGGER T_AU_EE_BENEFIT_REFUSAL_9
^


DROP TRIGGER T_BIU_EE_BENEFIT_REFUSAL_2
^


DROP TRIGGER T_BIU_EE_BENEFIT_REFUSAL_3
^

DROP TRIGGER T_AD_CO_BENEFIT_CATEGORY_9
^


DROP TRIGGER T_AUD_CO_BENEFIT_CATEGORY_2
^


DROP TRIGGER T_AU_CO_BENEFIT_CATEGORY_9
^

ALTER TABLE EE_BENEFIT_REFUSAL
ADD STATUS EV_CHAR1  NOT NULL
^

ALTER TABLE EE_BENEFIT_REFUSAL
ALTER COLUMN STATUS POSITION 10
^

UPDATE EE_BENEFIT_REFUSAL SET STATUS = 'C'
^

ALTER TABLE EE_BENEFIT_REFUSAL
ADD STATUS_DATE EV_DATE 
^

ALTER TABLE EE_BENEFIT_REFUSAL
ALTER COLUMN STATUS_DATE POSITION 11
^

ALTER TABLE EE_BENEFIT_REFUSAL
ADD CO_BENEFIT_ENROLLMENT_NBR EV_INT  NOT NULL
^

COMMIT
^

CREATE INDEX FK_EE_BENEFIT_REFUSAL_2 ON EE_BENEFIT_REFUSAL (HOME_STATE_NBR)
^

CREATE INDEX FK_EE_BENEFIT_REFUSAL_3 ON EE_BENEFIT_REFUSAL (SUI_STATE_NBR)
^

CREATE INDEX FK_EE_BENEFIT_REFUSAL_4 ON EE_BENEFIT_REFUSAL (CO_BENEFIT_ENROLLMENT_NBR)
^

CREATE INDEX LK_EE_BENEFIT_REFUSAL ON EE_BENEFIT_REFUSAL (EE_NBR,CO_BENEFIT_ENROLLMENT_NBR,SY_HR_REFUSAL_REASON_NBR)
^


DROP TRIGGER T_AD_EE_CHANGE_REQUEST_9
^


DROP TRIGGER T_AU_EE_CHANGE_REQUEST_9
^

ALTER TABLE EE_CHANGE_REQUEST
ADD STATUS EV_CHAR1  NOT NULL
^

ALTER TABLE EE_CHANGE_REQUEST
ALTER COLUMN STATUS POSITION 10
^

UPDATE EE_CHANGE_REQUEST SET STATUS = 'C'
^

ALTER TABLE EE_CHANGE_REQUEST
ADD STATUS_DATE EV_DATE 
^

ALTER TABLE EE_CHANGE_REQUEST
ALTER COLUMN STATUS_DATE POSITION 11
^

DROP INDEX LK_EE_CHILD_SUPPORT_CASES
^


DROP TRIGGER T_BIU_EE_CHILD_SUPPORT_CASES_3
^


DROP TRIGGER T_AD_EE_DIRECT_DEPOSIT_9
^


DROP TRIGGER T_AU_EE_DIRECT_DEPOSIT_9
^

ALTER TABLE EE_DIRECT_DEPOSIT
ADD SHOW_IN_EE_PORTAL EV_CHAR1  NOT NULL
^

ALTER TABLE EE_DIRECT_DEPOSIT
ALTER COLUMN SHOW_IN_EE_PORTAL POSITION 16
^

UPDATE EE_DIRECT_DEPOSIT SET SHOW_IN_EE_PORTAL = 'N'
^

DROP INDEX FK_EE_SCHEDULED_E_DS_1
^

DROP INDEX FK_EE_SCHEDULED_E_DS_10
^

DROP INDEX FK_EE_SCHEDULED_E_DS_11
^

DROP INDEX FK_EE_SCHEDULED_E_DS_12
^

DROP INDEX FK_EE_SCHEDULED_E_DS_13
^

DROP INDEX FK_EE_SCHEDULED_E_DS_14
^

DROP INDEX FK_EE_SCHEDULED_E_DS_2
^

DROP INDEX FK_EE_SCHEDULED_E_DS_3
^

DROP INDEX FK_EE_SCHEDULED_E_DS_4
^

DROP INDEX FK_EE_SCHEDULED_E_DS_5
^

DROP INDEX FK_EE_SCHEDULED_E_DS_6
^

DROP INDEX FK_EE_SCHEDULED_E_DS_7
^

DROP INDEX FK_EE_SCHEDULED_E_DS_8
^

DROP INDEX FK_EE_SCHEDULED_E_DS_9
^


DROP TRIGGER T_AD_EE_SCHEDULED_E_DS_9
^


DROP TRIGGER T_AU_EE_SCHEDULED_E_DS_9
^


DROP TRIGGER T_BIU_EE_SCHEDULED_E_DS_2
^


DROP TRIGGER T_BI_EE_SCHEDULED_E_DS_1
^


DROP TRIGGER T_BU_EE_SCHEDULED_E_DS_1
^

ALTER TABLE EE_SCHEDULED_E_DS
ADD MINIMUM_HW_HOURS EV_AMOUNT 
^

ALTER TABLE EE_SCHEDULED_E_DS
ALTER COLUMN MINIMUM_HW_HOURS POSITION 57
^

ALTER TABLE EE_SCHEDULED_E_DS
ADD MAXIMUM_HW_HOURS EV_AMOUNT 
^

ALTER TABLE EE_SCHEDULED_E_DS
ALTER COLUMN MAXIMUM_HW_HOURS POSITION 58
^

ALTER TABLE EE_SCHEDULED_E_DS
ADD HW_FUNDING_CL_E_D_GROUPS_NBR EV_INT 
^

ALTER TABLE EE_SCHEDULED_E_DS
ALTER COLUMN HW_FUNDING_CL_E_D_GROUPS_NBR POSITION 59
^

ALTER TABLE EE_SCHEDULED_E_DS
ADD HW_EXCESS_CL_E_D_GROUPS_NBR EV_INT 
^

ALTER TABLE EE_SCHEDULED_E_DS
ALTER COLUMN HW_EXCESS_CL_E_D_GROUPS_NBR POSITION 60
^

COMMIT
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_1 ON EE_SCHEDULED_E_DS (CL_E_DS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_10 ON EE_SCHEDULED_E_DS (EE_CHILD_SUPPORT_CASES_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_11 ON EE_SCHEDULED_E_DS (EE_DIRECT_DEPOSIT_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_12 ON EE_SCHEDULED_E_DS (EE_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_13 ON EE_SCHEDULED_E_DS (EE_BENEFITS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_14 ON EE_SCHEDULED_E_DS (CO_BENEFIT_SUBTYPE_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_2 ON EE_SCHEDULED_E_DS (CL_AGENCY_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_3 ON EE_SCHEDULED_E_DS (CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_4 ON EE_SCHEDULED_E_DS (MAX_PPP_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_5 ON EE_SCHEDULED_E_DS (MIN_PPP_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_6 ON EE_SCHEDULED_E_DS (SCHEDULED_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_7 ON EE_SCHEDULED_E_DS (MAX_AVG_AMT_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_8 ON EE_SCHEDULED_E_DS (MAX_AVG_HRS_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_9 ON EE_SCHEDULED_E_DS (THRESHOLD_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_15 ON EE_SCHEDULED_E_DS (HW_FUNDING_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_EE_SCHEDULED_E_DS_16 ON EE_SCHEDULED_E_DS (HW_EXCESS_CL_E_D_GROUPS_NBR,EFFECTIVE_UNTIL)
^


DROP TRIGGER T_AD_EE_SIGNATURE_9
^


DROP TRIGGER T_AU_EE_SIGNATURE_9
^


DROP TRIGGER T_BIU_EE_SIGNATURE_2
^

ALTER TABLE EE_SIGNATURE
ADD CO_BENEFIT_ENROLLMENT_NBR EV_INT 
^

ALTER TABLE EE_SIGNATURE
ALTER COLUMN CO_BENEFIT_ENROLLMENT_NBR POSITION 8
^

COMMIT
^

CREATE INDEX FK_EE_SIGNATURE_2 ON EE_SIGNATURE (CO_BENEFIT_ENROLLMENT_NBR)
^


DROP TRIGGER T_AD_EE_TIME_OFF_ACCRUAL_OPE_9
^


DROP TRIGGER T_AU_EE_TIME_OFF_ACCRUAL_OPE_9
^

ALTER TABLE EE_TIME_OFF_ACCRUAL_OPER
ADD MANAGER_NOTE EV_STR40 
^

ALTER TABLE EE_TIME_OFF_ACCRUAL_OPER
ALTER COLUMN MANAGER_NOTE POSITION 19
^


DROP TRIGGER T_BIUD_PR_CHECK_LINES_2
^

CREATE TABLE  CO_BENEFIT_ENROLLMENT
( 
     REC_VERSION EV_INT NOT NULL,
     CO_BENEFIT_ENROLLMENT_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CO_BENEFIT_CATEGORY_NBR EV_INT NOT NULL,
     ENROLLMENT_START_DATE EV_DATE NOT NULL,
     ENROLLMENT_END_DATE EV_DATE NOT NULL,
        CONSTRAINT PK_CO_BENEFIT_ENROLLMENT PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_CO_BENEFIT_ENROLLMENT_1 UNIQUE (CO_BENEFIT_ENROLLMENT_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_CO_BENEFIT_ENROLLMENT_2 UNIQUE (CO_BENEFIT_ENROLLMENT_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_CO_BENEFIT_ENROLLMENT_1 ON CO_BENEFIT_ENROLLMENT (CO_BENEFIT_CATEGORY_NBR)
^


GRANT ALL ON CO_BENEFIT_ENROLLMENT TO EUSER
^


EXECUTE BLOCK
AS
DECLARE VARIABLE pREC_VERSION INTEGER;
DECLARE VARIABLE pCO_BENEFIT_ENROLLMENT_NBR INTEGER;
DECLARE VARIABLE pCO_BENEFIT_CATEGORY_NBR INTEGER;
DECLARE VARIABLE pENROLLMENT_START_DATE TIMESTAMP;
DECLARE VARIABLE pENROLLMENT_END_DATE TIMESTAMP;
BEGIN

  FOR SELECT CO_BENEFIT_CATEGORY_NBR, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE
      FROM CO_BENEFIT_CATEGORY
      WHERE CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL
      INTO :pCO_BENEFIT_CATEGORY_NBR, :pENROLLMENT_START_DATE, :pENROLLMENT_END_DATE
  DO
  BEGIN

    pREC_VERSION = NEXT VALUE FOR G_CO_BENEFIT_ENROLLMENT_VER;
    pCO_BENEFIT_ENROLLMENT_NBR = NEXT VALUE FOR G_CO_BENEFIT_ENROLLMENT;

    INSERT INTO CO_BENEFIT_ENROLLMENT (REC_VERSION, CO_BENEFIT_ENROLLMENT_NBR, EFFECTIVE_DATE, EFFECTIVE_UNTIL,
                     CO_BENEFIT_CATEGORY_NBR, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE)
           VALUES(:pREC_VERSION, :pCO_BENEFIT_ENROLLMENT_NBR, '1/1/1900', '12/31/9999',
                    :pCO_BENEFIT_CATEGORY_NBR, :pENROLLMENT_START_DATE, :pENROLLMENT_END_DATE);

  END

  UPDATE EE_BENEFIT_REFUSAL a
    SET a.CO_BENEFIT_ENROLLMENT_NBR =
             (SELECT CO_BENEFIT_ENROLLMENT_NBR  FROM CO_BENEFIT_ENROLLMENT b
                  WHERE a.CO_BENEFIT_CATEGORY_NBR = b.CO_BENEFIT_CATEGORY_NBR AND
                         CURRENT_DATE >= b.EFFECTIVE_DATE AND CURRENT_DATE < b.EFFECTIVE_UNTIL)
        WHERE CURRENT_DATE >= a.EFFECTIVE_DATE AND CURRENT_DATE < a.EFFECTIVE_UNTIL;

END
^


ALTER TABLE CO_BENEFIT_CATEGORY
ALTER COLUMN READ_ONLY POSITION 15
^

ALTER TABLE CO_BENEFIT_CATEGORY
DROP ENROLLMENT_END_DATE
^


ALTER TABLE CO_BENEFIT_CATEGORY
DROP ENROLLMENT_START_DATE
^

ALTER TABLE EE_BENEFIT_REFUSAL
DROP CO_BENEFIT_CATEGORY_NBR
^

ALTER TABLE EE_BENEFIT_REFUSAL
ALTER COLUMN CO_BENEFIT_ENROLLMENT_NBR POSITION 6
^


DROP TRIGGER T_AD_EE_BENEFITS_9
^

DROP TRIGGER T_AU_EE_BENEFITS_9
^

DROP TRIGGER T_BIU_EE_BENEFITS_2
^

DROP INDEX FK_EE_BENEFITS_5
^

DROP INDEX FK_EE_BENEFITS_6
^

DROP INDEX FK_EE_BENEFITS_7
^

ALTER TABLE EE_BENEFITS
ADD STATUS EV_CHAR1  NOT NULL
^

ALTER TABLE EE_BENEFITS
ALTER COLUMN STATUS POSITION 28
^

UPDATE EE_BENEFITS SET STATUS = 'C'
^

ALTER TABLE EE_BENEFITS
ADD BENEFIT_ENROLLMENT_COMPLETE EV_DATETIME 
^

ALTER TABLE EE_BENEFITS
ALTER COLUMN BENEFIT_ENROLLMENT_COMPLETE POSITION 29
^

ALTER TABLE EE_BENEFITS
ADD CO_BENEFIT_ENROLLMENT_NBR EV_INT 
^

ALTER TABLE EE_BENEFITS
ALTER COLUMN CO_BENEFIT_ENROLLMENT_NBR POSITION 30
^

COMMIT
^

CREATE INDEX FK_EE_BENEFITS_5 ON EE_BENEFITS (CO_BENEFIT_ENROLLMENT_NBR)
^


CREATE OR ALTER PROCEDURE CHECK_INTEGRITY1
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check CL */
  child_table = 'CL';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'AGENCY_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_AGENCY */
  child_table = 'CL_AGENCY';

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'CL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_BANK_ACCOUNT */
  child_table = 'CL_BANK_ACCOUNT';

  parent_table = 'CL_BLOB';
  child_field = 'LOGO_CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'SIGNATURE_CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_CO_CONSOLIDATION */
  child_table = 'CL_CO_CONSOLIDATION';

  parent_table = 'CO';
  child_field = 'PRIMARY_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_DELIVERY_GROUP */
  child_table = 'CL_DELIVERY_GROUP';

  parent_table = 'CL_DELIVERY_METHOD';
  child_field = 'PRIMARY_CL_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_METHOD';
  child_field = 'SECOND_CL_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_DS */
  child_table = 'CL_E_DS';

  parent_table = 'CL_E_DS';
  child_field = 'OFFSET_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MIN_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'PIECE_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'OT_ALL_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'DEFAULT_CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_3_PARTY_SICK_PAY_ADMIN';
  child_field = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_MAX_AVG_AMT_GRP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_MAX_AVG_HRS_GRP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_THRESHOLD_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_FUNDING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_EXCESS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_GROUP_CODES */
  child_table = 'CL_E_D_GROUP_CODES';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_LOCAL_EXMPT_EXCLD */
  child_table = 'CL_E_D_LOCAL_EXMPT_EXCLD';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_STATE_EXMPT_EXCLD */
  child_table = 'CL_E_D_STATE_EXMPT_EXCLD';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_FUNDS */
  child_table = 'CL_FUNDS';

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_EDUCATION */
  child_table = 'CL_HR_PERSON_EDUCATION';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_SCHOOL';
  child_field = 'CL_HR_SCHOOL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_HANDICAPS */
  child_table = 'CL_HR_PERSON_HANDICAPS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_SKILLS */
  child_table = 'CL_HR_PERSON_SKILLS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_SKILLS';
  child_field = 'CL_HR_SKILLS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_MAIL_BOX_GROUP */
  child_table = 'CL_MAIL_BOX_GROUP';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'UP_LEVEL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_MAIL_BOX_GROUP_OPTION */
  child_table = 'CL_MAIL_BOX_GROUP_OPTION';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'CL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PENSION */
  child_table = 'CL_PENSION';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PERSON_DEPENDENTS */
  child_table = 'CL_PERSON_DEPENDENTS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PERSON_DOCUMENTS */
  child_table = 'CL_PERSON_DOCUMENTS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_UNION */
  child_table = 'CL_UNION';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_UNION_DUES */
  child_table = 'CL_UNION_DUES';

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO */
  child_table = 'CO';

  parent_table = 'CL_COMMON_PAYMASTER';
  child_field = 'CL_COMMON_PAYMASTER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'WORKERS_COMP_CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'ANNUAL_CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'QUARTER_CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'AGENCY_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'MANUAL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'W_COMP_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'AUTO_RD_DFLT_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_SDI_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_SUI_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'ACA_ADD_EARN_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO_NAMES */
  child_table = 'CO_ADDITIONAL_INFO_NAMES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO_VALUES */
  child_table = 'CO_ADDITIONAL_INFO_VALUES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_AUTO_ENLIST_RETURNS */
  child_table = 'CO_AUTO_ENLIST_RETURNS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BANK_ACCOUNT_REGISTER */
  child_table = 'CO_BANK_ACCOUNT_REGISTER';

  parent_table = 'CO_MANUAL_ACH';
  child_field = 'CO_MANUAL_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_ACH';
  child_field = 'CO_PR_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_PAYMENT_ACH';
  child_field = 'CO_TAX_PAYMENT_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_MISCELLANEOUS_CHECKS';
  child_field = 'PR_MISCELLANEOUS_CHECKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BANK_ACC_REG_DETAILS */
  child_table = 'CO_BANK_ACC_REG_DETAILS';

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'CO_BANK_ACCOUNT_REGISTER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BATCH_LOCAL_OR_TEMPS */
  child_table = 'CO_BATCH_LOCAL_OR_TEMPS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BATCH_STATES_OR_TEMPS */
  child_table = 'CO_BATCH_STATES_OR_TEMPS';

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BILLING_HISTORY */
  child_table = 'CO_BILLING_HISTORY';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CO_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BILLING_HISTORY_DETAIL */
  child_table = 'CO_BILLING_HISTORY_DETAIL';

  parent_table = 'CO_BILLING_HISTORY';
  child_field = 'CO_BILLING_HISTORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SERVICES';
  child_field = 'CO_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRANCH */
  child_table = 'CO_BRANCH';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRANCH_LOCALS */
  child_table = 'CO_BRANCH_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRCH_PR_BATCH_DEFLT_ED */
  child_table = 'CO_BRCH_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_CALENDAR_DEFAULTS */
  child_table = 'CO_CALENDAR_DEFAULTS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPARTMENT */
  child_table = 'CO_DEPARTMENT';

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPARTMENT_LOCALS */
  child_table = 'CO_DEPARTMENT_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPT_PR_BATCH_DEFLT_ED */
  child_table = 'CO_DEPT_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIVISION */
  child_table = 'CO_DIVISION';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIVISION_LOCALS */
  child_table = 'CO_DIVISION_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIV_PR_BATCH_DEFLT_ED */
  child_table = 'CO_DIV_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ENLIST_GROUPS */
  child_table = 'CO_ENLIST_GROUPS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_E_D_CODES */
  child_table = 'CO_E_D_CODES';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_FED_TAX_LIABILITIES */
  child_table = 'CO_FED_TAX_LIABILITIES';

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GENERAL_LEDGER */
  child_table = 'CO_GENERAL_LEDGER';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP */
  child_table = 'CO_GROUP';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP_MANAGER */
  child_table = 'CO_GROUP_MANAGER';

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP_MEMBER */
  child_table = 'CO_GROUP_MEMBER';

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_APPLICANT */
  child_table = 'CO_HR_APPLICANT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_RECRUITERS';
  child_field = 'CO_HR_RECRUITERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_REFERRALS';
  child_field = 'CO_HR_REFERRALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_CAR';
  child_field = 'CO_HR_CAR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_APPLICANT_INTERVIEW */
  child_table = 'CO_HR_APPLICANT_INTERVIEW';

  parent_table = 'CO_HR_APPLICANT';
  child_field = 'CO_HR_APPLICANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_ATTENDANCE_TYPES */
  child_table = 'CO_HR_ATTENDANCE_TYPES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_CAR */
  child_table = 'CO_HR_CAR';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_PERFORMANCE_RATINGS */
  child_table = 'CO_HR_PERFORMANCE_RATINGS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_POSITIONS */
  child_table = 'CO_HR_POSITIONS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_PROPERTY */
  child_table = 'CO_HR_PROPERTY';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_RECRUITERS */
  child_table = 'CO_HR_RECRUITERS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_REFERRALS */
  child_table = 'CO_HR_REFERRALS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_SALARY_GRADES */
  child_table = 'CO_HR_SALARY_GRADES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_SUPERVISORS */
  child_table = 'CO_HR_SUPERVISORS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOBS */
  child_table = 'CO_JOBS';

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOBS_LOCALS */
  child_table = 'CO_JOBS_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOB_GROUPS */
  child_table = 'CO_JOB_GROUPS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCAL_TAX */
  child_table = 'CO_LOCAL_TAX';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCAL_TAX_LIABILITIES */
  child_table = 'CO_LOCAL_TAX_LIABILITIES';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NONRES_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_MANUAL_ACH */
  child_table = 'CO_MANUAL_ACH';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'TO_EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'FROM_EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'FROM_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TO_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'FROM_EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'TO_EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'FROM_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'TO_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PAY_GROUP */
  child_table = 'CO_PAY_GROUP';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PENSIONS */
  child_table = 'CO_PENSIONS';

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY1 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY1 TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY2
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check CO_PHONE */
  child_table = 'CO_PHONE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_ACH */
  child_table = 'CO_PR_ACH';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_BATCH_DEFLT_ED */
  child_table = 'CO_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_CHECK_TEMPLATES */
  child_table = 'CO_PR_CHECK_TEMPLATES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_CHECK_TEMPLATE_E_DS */
  child_table = 'CO_PR_CHECK_TEMPLATE_E_DS';

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_FILTERS */
  child_table = 'CO_PR_FILTERS';

  parent_table = 'CO_PAY_GROUP';
  child_field = 'CO_PAY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_REPORTS */
  child_table = 'CO_REPORTS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'OVERRIDE_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SALESPERSON */
  child_table = 'CO_SALESPERSON';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SALESPERSON_FLAT_AMT */
  child_table = 'CO_SALESPERSON_FLAT_AMT';

  parent_table = 'CO_SALESPERSON';
  child_field = 'CO_SALESPERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SERVICES */
  child_table = 'CO_SERVICES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SHIFTS */
  child_table = 'CO_SHIFTS';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_STATES */
  child_table = 'CO_STATES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_STATE_TAX_LIABILITIES */
  child_table = 'CO_STATE_TAX_LIABILITIES';

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_STORAGE */
  child_table = 'CO_STORAGE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SUI */
  child_table = 'CO_SUI';

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SUI_LIABILITIES */
  child_table = 'CO_SUI_LIABILITIES';

  parent_table = 'CO_SUI';
  child_field = 'CO_SUI_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_DEPOSITS */
  child_table = 'CO_TAX_DEPOSITS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_PAYMENT_ACH */
  child_table = 'CO_TAX_PAYMENT_ACH';

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_RETURN_QUEUE */
  child_table = 'CO_TAX_RETURN_QUEUE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_RETURN_RUNS */
  child_table = 'CO_TAX_RETURN_RUNS';

  parent_table = 'CO_TAX_RETURN_QUEUE';
  child_field = 'CO_TAX_RETURN_QUEUE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TEAM */
  child_table = 'CO_TEAM';

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TEAM_LOCALS */
  child_table = 'CO_TEAM_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TEAM_PR_BATCH_DEFLT_ED */
  child_table = 'CO_TEAM_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TIME_OFF_ACCRUAL */
  child_table = 'CO_TIME_OFF_ACCRUAL';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'USED_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_ATTENDANCE_TYPES';
  child_field = 'CO_HR_ATTENDANCE_TYPES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TIME_OFF_ACCRUAL_RATES */
  child_table = 'CO_TIME_OFF_ACCRUAL_RATES';

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TIME_OFF_ACCRUAL_TIERS */
  child_table = 'CO_TIME_OFF_ACCRUAL_TIERS';

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_UNIONS */
  child_table = 'CO_UNIONS';

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_WORKERS_COMP */
  child_table = 'CO_WORKERS_COMP';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE */
  child_table = 'EE';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'ALD_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SHIFTS';
  child_field = 'AUTOPAY_CO_SHIFTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_APPLICANT';
  child_field = 'CO_HR_APPLICANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PAY_GROUP';
  child_field = 'CO_PAY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_PERFORMANCE_RATINGS';
  child_field = 'CO_HR_PERFORMANCE_RATINGS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_RECRUITERS';
  child_field = 'CO_HR_RECRUITERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_REFERRALS';
  child_field = 'CO_HR_REFERRALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_UNIONS';
  child_field = 'CO_UNIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITION_GRADES';
  child_field = 'CO_HR_POSITION_GRADES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_DISCOUNT';
  child_field = 'CO_BENEFIT_DISCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'HOME_TAX_EE_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_ADDITIONAL_INFO */
  child_table = 'EE_ADDITIONAL_INFO';

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_VALUES';
  child_field = 'CO_ADDITIONAL_INFO_VALUES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'INFO_BLOB';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_AUTOLABOR_DISTRIBUTION */
  child_table = 'EE_AUTOLABOR_DISTRIBUTION';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFITS */
  child_table = 'EE_BENEFITS';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_ENROLLMENT';
  child_field = 'CO_BENEFIT_ENROLLMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFIT_PAYMENT */
  child_table = 'EE_BENEFIT_PAYMENT';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_CHANGE_REQUEST */
  child_table = 'EE_CHANGE_REQUEST';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_SIGNATURE';
  child_field = 'SIGNATURE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_CHILD_SUPPORT_CASES */
  child_table = 'EE_CHILD_SUPPORT_CASES';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'DEPENDENT_HEALTH_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_DIRECT_DEPOSIT */
  child_table = 'EE_DIRECT_DEPOSIT';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_EMERGENCY_CONTACTS */
  child_table = 'EE_EMERGENCY_CONTACTS';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_ATTENDANCE */
  child_table = 'EE_HR_ATTENDANCE';

  parent_table = 'CO_HR_ATTENDANCE_TYPES';
  child_field = 'CO_HR_ATTENDANCE_TYPES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_CAR */
  child_table = 'EE_HR_CAR';

  parent_table = 'CO_HR_CAR';
  child_field = 'CO_HR_CAR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_CO_PROVIDED_EDUCATN */
  child_table = 'EE_HR_CO_PROVIDED_EDUCATN';

  parent_table = 'CL_HR_SCHOOL';
  child_field = 'CL_HR_SCHOOL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_COURSE';
  child_field = 'CL_HR_COURSE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_INJURY_OCCURRENCE */
  child_table = 'EE_HR_INJURY_OCCURRENCE';

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_PERFORMANCE_RATINGS */
  child_table = 'EE_HR_PERFORMANCE_RATINGS';

  parent_table = 'CO_HR_PERFORMANCE_RATINGS';
  child_field = 'CO_HR_PERFORMANCE_RATINGS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_REASON_CODES';
  child_field = 'CL_HR_REASON_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_PROPERTY_TRACKING */
  child_table = 'EE_HR_PROPERTY_TRACKING';

  parent_table = 'CO_HR_PROPERTY';
  child_field = 'CO_HR_PROPERTY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_LOCALS */
  child_table = 'EE_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_PENSION_FUND_SPLITS */
  child_table = 'EE_PENSION_FUND_SPLITS';

  parent_table = 'CL_FUNDS';
  child_field = 'CL_FUNDS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_PIECE_WORK */
  child_table = 'EE_PIECE_WORK';

  parent_table = 'CL_PIECES';
  child_field = 'CL_PIECES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_RATES */
  child_table = 'EE_RATES';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITION_GRADES';
  child_field = 'CO_HR_POSITION_GRADES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_SCHEDULED_E_DS */
  child_table = 'EE_SCHEDULED_E_DS';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_PPP_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MIN_PPP_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SCHEDULED_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'THRESHOLD_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_CHILD_SUPPORT_CASES';
  child_field = 'EE_CHILD_SUPPORT_CASES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_BENEFITS';
  child_field = 'EE_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_FUNDING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_EXCESS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_STATES */
  child_table = 'EE_STATES';

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'SDI_APPLY_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'SUI_APPLY_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'RECIPROCAL_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_TIME_OFF_ACCRUAL */
  child_table = 'EE_TIME_OFF_ACCRUAL';

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_TIME_OFF_ACCRUAL_OPER */
  child_table = 'EE_TIME_OFF_ACCRUAL_OPER';

  parent_table = 'EE_TIME_OFF_ACCRUAL';
  child_field = 'EE_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_BATCH';
  child_field = 'PR_BATCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_TIME_OFF_ACCRUAL_OPER';
  child_field = 'CONNECTED_EE_TOA_OPER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_TIME_OFF_ACCRUAL_OPER';
  child_field = 'ADJUSTED_EE_TOA_OPER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_WORK_SHIFTS */
  child_table = 'EE_WORK_SHIFTS';

  parent_table = 'CO_SHIFTS';
  child_field = 'CO_SHIFTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR */
  child_table = 'PR';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_BATCH */
  child_table = 'PR_BATCH';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_FILTERS';
  child_field = 'CO_PR_FILTERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK */
  child_table = 'PR_CHECK';

  parent_table = 'PR_BATCH';
  child_field = 'PR_BATCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'DATA_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'NOTES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY2 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY2 TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY3
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check PR_CHECK_LINES */
  child_table = 'PR_CHECK_LINES';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_MISCELLANEOUS_CHECKS';
  child_field = 'PR_MISCELLANEOUS_CHECKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'EE_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'EE_SUI_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SHIFTS';
  child_field = 'CO_SHIFTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PIECES';
  child_field = 'CL_PIECES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_SCHEDULED_E_DS';
  child_field = 'EE_SCHEDULED_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'REDUCING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_LINE_LOCALS */
  child_table = 'PR_CHECK_LINE_LOCALS';

  parent_table = 'PR_CHECK_LINES';
  child_field = 'PR_CHECK_LINES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_LOCALS';
  child_field = 'EE_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_LOCALS */
  child_table = 'PR_CHECK_LOCALS';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_LOCALS';
  child_field = 'EE_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_LOCALS';
  child_field = 'NONRES_EE_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_STATES */
  child_table = 'PR_CHECK_STATES';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'EE_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_SUI */
  child_table = 'PR_CHECK_SUI';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SUI';
  child_field = 'CO_SUI_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_MISCELLANEOUS_CHECKS */
  child_table = 'PR_MISCELLANEOUS_CHECKS';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_REPORTS */
  child_table = 'PR_REPORTS';

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_REPRINT_HISTORY */
  child_table = 'PR_REPRINT_HISTORY';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_REPRINT_HISTORY_DETAIL */
  child_table = 'PR_REPRINT_HISTORY_DETAIL';

  parent_table = 'PR_REPRINT_HISTORY';
  child_field = 'PR_REPRINT_HISTORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SCHEDULED_EVENT */
  child_table = 'PR_SCHEDULED_EVENT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SCHEDULED_EVENT_BATCH */
  child_table = 'PR_SCHEDULED_EVENT_BATCH';

  parent_table = 'PR_SCHEDULED_EVENT';
  child_field = 'PR_SCHEDULED_EVENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SCHEDULED_E_DS */
  child_table = 'PR_SCHEDULED_E_DS';

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SERVICES */
  child_table = 'PR_SERVICES';

  parent_table = 'CO_SERVICES';
  child_field = 'CO_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_POSITION_GRADES */
  child_table = 'CO_HR_POSITION_GRADES';

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SALARY_GRADES';
  child_field = 'CO_HR_SALARY_GRADES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO */
  child_table = 'CO_ADDITIONAL_INFO';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_VALUES';
  child_field = 'CO_ADDITIONAL_INFO_VALUES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'INFO_BLOB';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFITS */
  child_table = 'CO_BENEFITS';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'EE_DEDUCTION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'ER_DEDUCTION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'EE_COBRA_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'ER_COBRA_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_CATEGORY';
  child_field = 'CO_BENEFIT_CATEGORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_CATEGORY */
  child_table = 'CO_BENEFIT_CATEGORY';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_DISCOUNT */
  child_table = 'CO_BENEFIT_DISCOUNT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PACKAGE */
  child_table = 'CO_BENEFIT_PACKAGE';

  parent_table = 'CO_E_D_CODES';
  child_field = 'PAYOUT_ED_CODE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PKG_ASMNT */
  child_table = 'CO_BENEFIT_PKG_ASMNT';

  parent_table = 'CO_BENEFIT_PACKAGE';
  child_field = 'CO_BENEFIT_PACKAGE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PKG_DETAIL */
  child_table = 'CO_BENEFIT_PKG_DETAIL';

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_PACKAGE';
  child_field = 'CO_BENEFIT_PACKAGE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PROVIDERS */
  child_table = 'CO_BENEFIT_PROVIDERS';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_RATES */
  child_table = 'CO_BENEFIT_RATES';

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_SETUP */
  child_table = 'CO_BENEFIT_SETUP';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_STATES */
  child_table = 'CO_BENEFIT_STATES';

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_SUBTYPE */
  child_table = 'CO_BENEFIT_SUBTYPE';

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_USER_REPORTS */
  child_table = 'CO_USER_REPORTS';

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFICIARY */
  child_table = 'EE_BENEFICIARY';

  parent_table = 'EE_BENEFITS';
  child_field = 'EE_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON_DEPENDENTS';
  child_field = 'CL_PERSON_DEPENDENTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFIT_REFUSAL */
  child_table = 'EE_BENEFIT_REFUSAL';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'HOME_STATE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'SUI_STATE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_ENROLLMENT';
  child_field = 'CO_BENEFIT_ENROLLMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_SIGNATURE */
  child_table = 'EE_SIGNATURE';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_ENROLLMENT';
  child_field = 'CO_BENEFIT_ENROLLMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCATIONS */
  child_table = 'CO_LOCATIONS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_QEC_RUN */
  child_table = 'CO_QEC_RUN';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_ENROLLMENT */
  child_table = 'CO_BENEFIT_ENROLLMENT';

  parent_table = 'CO_BENEFIT_CATEGORY';
  child_field = 'CO_BENEFIT_CATEGORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY3 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY3 TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_workers_comp(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE workers_comp_code VARCHAR(8);
DECLARE VARIABLE p_workers_comp_code VARCHAR(8);
DECLARE VARIABLE co_states_nbr INTEGER;
DECLARE VARIABLE p_co_states_nbr INTEGER;
DECLARE VARIABLE rate NUMERIC(18,6);
DECLARE VARIABLE p_rate NUMERIC(18,6);
DECLARE VARIABLE experience_rating NUMERIC(18,6);
DECLARE VARIABLE p_experience_rating NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_workers_comp_nbr , workers_comp_code, co_states_nbr, rate, experience_rating
        FROM co_workers_comp
        ORDER BY co_workers_comp_nbr, effective_date
        INTO :rec_version, :effective_date, :co_workers_comp_nbr, :workers_comp_code, :co_states_nbr, :rate, :experience_rating
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (effective_date = '1/1/1900') OR (p_workers_comp_code IS DISTINCT FROM workers_comp_code) OR (p_co_states_nbr IS DISTINCT FROM co_states_nbr) OR (p_rate IS DISTINCT FROM rate) OR (p_experience_rating IS DISTINCT FROM experience_rating)) THEN
      BEGIN
        curr_nbr = co_workers_comp_nbr;
        p_workers_comp_code = workers_comp_code;
        p_co_states_nbr = co_states_nbr;
        p_rate = rate;
        p_experience_rating = experience_rating;
      END
      ELSE
        DELETE FROM co_workers_comp WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, workers_comp_code, co_states_nbr, rate, experience_rating
        FROM co_workers_comp
        WHERE co_workers_comp_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :workers_comp_code, :co_states_nbr, :rate, :experience_rating
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_workers_comp_code IS DISTINCT FROM workers_comp_code) OR (p_co_states_nbr IS DISTINCT FROM co_states_nbr) OR (p_rate IS DISTINCT FROM rate) OR (p_experience_rating IS DISTINCT FROM experience_rating)) THEN
      BEGIN
        p_workers_comp_code = workers_comp_code;
        p_co_states_nbr = co_states_nbr;
        p_rate = rate;
        p_experience_rating = experience_rating;
      END
      ELSE
        DELETE FROM co_workers_comp WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_WORKERS_COMP TO EUSER
^

CREATE OR ALTER PROCEDURE pack_ee_scheduled_e_ds(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_scheduled_e_ds_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE ee_direct_deposit_nbr INTEGER;
DECLARE VARIABLE p_ee_direct_deposit_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_scheduled_e_ds_nbr , ee_direct_deposit_nbr
        FROM ee_scheduled_e_ds
        ORDER BY ee_scheduled_e_ds_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_scheduled_e_ds_nbr, :ee_direct_deposit_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_scheduled_e_ds_nbr) OR (effective_date = '1/1/1900') OR (p_ee_direct_deposit_nbr IS DISTINCT FROM ee_direct_deposit_nbr)) THEN
      BEGIN
        curr_nbr = ee_scheduled_e_ds_nbr;
        p_ee_direct_deposit_nbr = ee_direct_deposit_nbr;
      END
      ELSE
        DELETE FROM ee_scheduled_e_ds WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, ee_direct_deposit_nbr
        FROM ee_scheduled_e_ds
        WHERE ee_scheduled_e_ds_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :ee_direct_deposit_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_ee_direct_deposit_nbr IS DISTINCT FROM ee_direct_deposit_nbr)) THEN
      BEGIN
        p_ee_direct_deposit_nbr = ee_direct_deposit_nbr;
      END
      ELSE
        DELETE FROM ee_scheduled_e_ds WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE PACK_EE_SCHEDULED_E_DS TO EUSER
^


CREATE OR ALTER PROCEDURE pack_ee(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE p_cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE p_co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE p_co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE p_co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE p_co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE p_new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE p_current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE p_co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE p_tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE p_w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE p_w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE p_w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE p_w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE p_w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE p_w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE p_home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE p_exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE p_exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE p_base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE p_company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE p_distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE p_tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE p_total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE p_pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE p_makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE p_salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE p_highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE p_corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE p_co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE p_co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE p_healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
DECLARE VARIABLE p_aca_status CHAR(1);
DECLARE VARIABLE aca_coverage_offer VARCHAR(2);
DECLARE VARIABLE p_aca_coverage_offer VARCHAR(2);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_nbr , cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status, aca_coverage_offer
        FROM ee
        ORDER BY ee_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_nbr, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status, :aca_coverage_offer
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_nbr) OR (effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override) OR (p_aca_status IS DISTINCT FROM aca_status) OR (p_aca_coverage_offer IS DISTINCT FROM aca_coverage_offer)) THEN
      BEGIN
        curr_nbr = ee_nbr;
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
        p_aca_status = aca_status;
        p_aca_coverage_offer = aca_coverage_offer;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status, aca_coverage_offer
        FROM ee
        WHERE ee_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status, :aca_coverage_offer
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override) OR (p_aca_status IS DISTINCT FROM aca_status) OR (p_aca_coverage_offer IS DISTINCT FROM aca_coverage_offer)) THEN
      BEGIN
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
        p_aca_status = aca_status;
        p_aca_coverage_offer = aca_coverage_offer;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE PACK_EE TO EUSER
^

CREATE OR ALTER PROCEDURE pack_all
AS
BEGIN
  EXECUTE PROCEDURE pack_cl (NULL);
  EXECUTE PROCEDURE pack_cl_3_party_sick_pay_admin (NULL);
  EXECUTE PROCEDURE pack_cl_e_ds (NULL);
  EXECUTE PROCEDURE pack_cl_e_d_local_exmpt_excld (NULL);
  EXECUTE PROCEDURE pack_cl_e_d_state_exmpt_excld (NULL);
  EXECUTE PROCEDURE pack_cl_pension (NULL);
  EXECUTE PROCEDURE pack_cl_person (NULL);
  EXECUTE PROCEDURE pack_co (NULL);
  EXECUTE PROCEDURE pack_co_branch (NULL);
  EXECUTE PROCEDURE pack_co_department (NULL);
  EXECUTE PROCEDURE pack_co_division (NULL);
  EXECUTE PROCEDURE pack_co_jobs (NULL);
  EXECUTE PROCEDURE pack_co_local_tax (NULL);
  EXECUTE PROCEDURE pack_co_states (NULL);
  EXECUTE PROCEDURE pack_co_sui (NULL);
  EXECUTE PROCEDURE pack_co_team (NULL);
  EXECUTE PROCEDURE pack_co_workers_comp (NULL);
  EXECUTE PROCEDURE pack_ee (NULL);
  EXECUTE PROCEDURE pack_ee_locals (NULL);
  EXECUTE PROCEDURE pack_ee_rates (NULL);
  EXECUTE PROCEDURE pack_ee_scheduled_e_ds (NULL);
  EXECUTE PROCEDURE pack_ee_states (NULL);
END
^

GRANT EXECUTE ON PROCEDURE PACK_ALL TO EUSER
^



GRANT EXECUTE ON PROCEDURE PACK_ALL TO EUSER
^

CREATE OR ALTER PROCEDURE del_ee_scheduled_e_ds(nbr INTEGER)
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  FOR SELECT rec_version FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = :nbr ORDER BY effective_date DESC FOR UPDATE WITH LOCK INTO rec_version
  DO
  BEGIN
    DELETE FROM ee_scheduled_e_ds WHERE rec_version = :rec_version;
  END
END
^

GRANT EXECUTE ON PROCEDURE DEL_EE_SCHEDULED_E_DS TO EUSER
^



GRANT EXECUTE ON PROCEDURE DEL_EE_SCHEDULED_E_DS TO EUSER
^

CREATE OR ALTER PROCEDURE del_co_benefit_enrollment(nbr INTEGER)
AS
BEGIN
  DELETE FROM co_benefit_enrollment WHERE co_benefit_enrollment_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_CO_BENEFIT_ENROLLMENT TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_PRINTED_MANUAL_CHECK
(PR_CHECK_NBR INTEGER)

AS
DECLARE VARIABLE printed INTEGER;
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'PR_LOCK_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  SELECT count(*) P
  FROM PR_CHECK x
  JOIN PR_REPRINT_HISTORY h on h.PR_NBR=x.PR_NBR
  JOIN PR_REPRINT_HISTORY_DETAIL d on d.PR_REPRINT_HISTORY_NBR=h.PR_REPRINT_HISTORY_NBR
  WHERE x.PR_CHECK_NBR = :PR_CHECK_NBR and x.CHECK_TYPE='M'
  and d.MISCELLANEOUS_CHECK='N' and d.BEGIN_CHECK_NUMBER<=:PR_CHECK_NBR and d.END_CHECK_NUMBER>=:PR_CHECK_NBR
  INTO :printed;

  IF (printed > 0) THEN
    EXECUTE PROCEDURE raise_error('B1', 'You can not change manual check that has been printed!');
END

^

GRANT EXECUTE ON PROCEDURE CHECK_PRINTED_MANUAL_CHECK TO EUSER
^

ALTER TABLE CO_BENEFIT_ENROLLMENT
ADD CONSTRAINT C_CO_BENEFIT_ENROLLMENT_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_CL_E_DS_9 FOR CL_E_DS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE custom_e_d_code_number VARCHAR(20);
DECLARE VARIABLE e_d_code_type CHAR(2);
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE ee_exempt_exclude_oasdi CHAR(1);
DECLARE VARIABLE ee_exempt_exclude_medicare CHAR(1);
DECLARE VARIABLE ee_exempt_exclude_federal CHAR(1);
DECLARE VARIABLE ee_exempt_exclude_eic CHAR(1);
DECLARE VARIABLE er_exempt_exclude_oasdi CHAR(1);
DECLARE VARIABLE er_exempt_exclude_medicare CHAR(1);
DECLARE VARIABLE er_exempt_exclude_fui CHAR(1);
DECLARE VARIABLE w2_box VARCHAR(12);
DECLARE VARIABLE cl_pension_nbr INTEGER;
DECLARE VARIABLE cl_3_party_sick_pay_admin_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(11, OLD.rec_version, OLD.cl_e_ds_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', custom_e_d_code_number, e_d_code_type, description, ee_exempt_exclude_oasdi, ee_exempt_exclude_medicare, ee_exempt_exclude_federal, ee_exempt_exclude_eic, er_exempt_exclude_oasdi, er_exempt_exclude_medicare, er_exempt_exclude_fui, w2_box, cl_pension_nbr, cl_3_party_sick_pay_admin_nbr  FROM cl_e_ds WHERE cl_e_ds_nbr = OLD.cl_e_ds_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :custom_e_d_code_number, :e_d_code_type, :description, :ee_exempt_exclude_oasdi, :ee_exempt_exclude_medicare, :ee_exempt_exclude_federal, :ee_exempt_exclude_eic, :er_exempt_exclude_oasdi, :er_exempt_exclude_medicare, :er_exempt_exclude_fui, :w2_box, :cl_pension_nbr, :cl_3_party_sick_pay_admin_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 175, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2576, OLD.effective_until);

  /* CUSTOM_E_D_CODE_NUMBER */
  IF (last_record = 'Y' OR custom_e_d_code_number IS DISTINCT FROM OLD.custom_e_d_code_number) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 199, OLD.custom_e_d_code_number);

  /* E_D_CODE_TYPE */
  IF (last_record = 'Y' OR e_d_code_type IS DISTINCT FROM OLD.e_d_code_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 200, OLD.e_d_code_type);

  /* DESCRIPTION */
  IF (last_record = 'Y' OR description IS DISTINCT FROM OLD.description) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 201, OLD.description);

  /* EE_EXEMPT_EXCLUDE_OASDI */
  IF (last_record = 'Y' OR ee_exempt_exclude_oasdi IS DISTINCT FROM OLD.ee_exempt_exclude_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 216, OLD.ee_exempt_exclude_oasdi);

  /* EE_EXEMPT_EXCLUDE_MEDICARE */
  IF (last_record = 'Y' OR ee_exempt_exclude_medicare IS DISTINCT FROM OLD.ee_exempt_exclude_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 217, OLD.ee_exempt_exclude_medicare);

  /* EE_EXEMPT_EXCLUDE_FEDERAL */
  IF (last_record = 'Y' OR ee_exempt_exclude_federal IS DISTINCT FROM OLD.ee_exempt_exclude_federal) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 218, OLD.ee_exempt_exclude_federal);

  /* EE_EXEMPT_EXCLUDE_EIC */
  IF (last_record = 'Y' OR ee_exempt_exclude_eic IS DISTINCT FROM OLD.ee_exempt_exclude_eic) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 219, OLD.ee_exempt_exclude_eic);

  /* ER_EXEMPT_EXCLUDE_OASDI */
  IF (last_record = 'Y' OR er_exempt_exclude_oasdi IS DISTINCT FROM OLD.er_exempt_exclude_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 220, OLD.er_exempt_exclude_oasdi);

  /* ER_EXEMPT_EXCLUDE_MEDICARE */
  IF (last_record = 'Y' OR er_exempt_exclude_medicare IS DISTINCT FROM OLD.er_exempt_exclude_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 221, OLD.er_exempt_exclude_medicare);

  /* ER_EXEMPT_EXCLUDE_FUI */
  IF (last_record = 'Y' OR er_exempt_exclude_fui IS DISTINCT FROM OLD.er_exempt_exclude_fui) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 222, OLD.er_exempt_exclude_fui);

  /* W2_BOX */
  IF ((last_record = 'Y' AND OLD.w2_box IS NOT NULL) OR (last_record = 'N' AND w2_box IS DISTINCT FROM OLD.w2_box)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 202, OLD.w2_box);

  /* CL_PENSION_NBR */
  IF ((last_record = 'Y' AND OLD.cl_pension_nbr IS NOT NULL) OR (last_record = 'N' AND cl_pension_nbr IS DISTINCT FROM OLD.cl_pension_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 191, OLD.cl_pension_nbr);

  /* CL_3_PARTY_SICK_PAY_ADMIN_NBR */
  IF ((last_record = 'Y' AND OLD.cl_3_party_sick_pay_admin_nbr IS NOT NULL) OR (last_record = 'N' AND cl_3_party_sick_pay_admin_nbr IS DISTINCT FROM OLD.cl_3_party_sick_pay_admin_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 196, OLD.cl_3_party_sick_pay_admin_nbr);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CL_E_D_GROUPS_NBR */
    IF (OLD.cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 185, OLD.cl_e_d_groups_nbr);

    /* SKIP_HOURS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 213, OLD.skip_hours);

    /* OVERRIDE_EE_RATE_NUMBER */
    IF (OLD.override_ee_rate_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 186, OLD.override_ee_rate_number);

    /* OVERRIDE_FED_TAX_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 214, OLD.override_fed_tax_type);

    /* UPDATE_HOURS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 215, OLD.update_hours);

    /* DEFAULT_CL_AGENCY_NBR */
    IF (OLD.default_cl_agency_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 187, OLD.default_cl_agency_nbr);

    /* MIN_CL_E_D_GROUPS_NBR */
    IF (OLD.min_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 188, OLD.min_cl_e_d_groups_nbr);

    /* MAX_CL_E_D_GROUPS_NBR */
    IF (OLD.max_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 189, OLD.max_cl_e_d_groups_nbr);

    /* CL_UNION_NBR */
    IF (OLD.cl_union_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 190, OLD.cl_union_nbr);

    /* PRIORITY_TO_EXCLUDE */
    IF (OLD.priority_to_exclude IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 192, OLD.priority_to_exclude);

    /* MAKE_UP_DEDUCTION_SHORTFALL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 223, OLD.make_up_deduction_shortfall);

    /* OT_ALL_CL_E_D_GROUPS_NBR */
    IF (OLD.ot_all_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 193, OLD.ot_all_cl_e_d_groups_nbr);

    /* PIECE_CL_E_D_GROUPS_NBR */
    IF (OLD.piece_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 194, OLD.piece_cl_e_d_groups_nbr);

    /* PW_MIN_WAGE_MAKE_UP_METHOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 224, OLD.pw_min_wage_make_up_method);

    /* TIP_MINIMUM_WAGE_MAKE_UP_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 225, OLD.tip_minimum_wage_make_up_type);

    /* OFFSET_CL_E_DS_NBR */
    IF (OLD.offset_cl_e_ds_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 195, OLD.offset_cl_e_ds_nbr);

    /* SHOW_YTD_ON_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 226, OLD.show_ytd_on_checks);

    /* SCHEDULED_DEFAULTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 227, OLD.scheduled_defaults);

    /* SD_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 228, OLD.sd_frequency);

    /* SD_EFFECTIVE_START_DATE */
    IF (OLD.sd_effective_start_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 176, OLD.sd_effective_start_date);

    /* SD_CALCULATION_METHOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 229, OLD.sd_calculation_method);

    /* SD_EXPRESSION */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@SD_EXPRESSION');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@SD_EXPRESSION', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 177, :blob_nbr);
    END

    /* SD_AUTO */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 230, OLD.sd_auto);

    /* SD_ROUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 231, OLD.sd_round);

    /* SD_EXCLUDE_WEEK_1 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 232, OLD.sd_exclude_week_1);

    /* SD_EXCLUDE_WEEK_2 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 233, OLD.sd_exclude_week_2);

    /* SD_EXCLUDE_WEEK_3 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 234, OLD.sd_exclude_week_3);

    /* SD_EXCLUDE_WEEK_4 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 235, OLD.sd_exclude_week_4);

    /* SD_EXCLUDE_WEEK_5 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 236, OLD.sd_exclude_week_5);

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 203, OLD.filler);

    /* OVERRIDE_RATE_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 237, OLD.override_rate_type);

    /* SHOW_ON_INPUT_WORKSHEET */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 238, OLD.show_on_input_worksheet);

    /* SY_STATE_NBR */
    IF (OLD.sy_state_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 197, OLD.sy_state_nbr);

    /* GL_OFFSET_TAG */
    IF (OLD.gl_offset_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 204, OLD.gl_offset_tag);

    /* SHOW_ED_ON_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 239, OLD.show_ed_on_checks);

    /* NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 174, :blob_nbr);
    END

    /* OVERRIDE_RATE */
    IF (OLD.override_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 173, OLD.override_rate);

    /* OVERRIDE_FED_TAX_VALUE */
    IF (OLD.override_fed_tax_value IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 172, OLD.override_fed_tax_value);

    /* PAY_PERIOD_MINIMUM_AMOUNT */
    IF (OLD.pay_period_minimum_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 171, OLD.pay_period_minimum_amount);

    /* PAY_PERIOD_MINIMUM_PERCENT_OF */
    IF (OLD.pay_period_minimum_percent_of IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 170, OLD.pay_period_minimum_percent_of);

    /* PAY_PERIOD_MAXIMUM_AMOUNT */
    IF (OLD.pay_period_maximum_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 169, OLD.pay_period_maximum_amount);

    /* PAY_PERIOD_MAXIMUM_PERCENT_OF */
    IF (OLD.pay_period_maximum_percent_of IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 168, OLD.pay_period_maximum_percent_of);

    /* ANNUAL_MAXIMUM */
    IF (OLD.annual_maximum IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 167, OLD.annual_maximum);

    /* MATCH_FIRST_PERCENTAGE */
    IF (OLD.match_first_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 166, OLD.match_first_percentage);

    /* MATCH_FIRST_AMOUNT */
    IF (OLD.match_first_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 165, OLD.match_first_amount);

    /* MATCH_SECOND_PERCENTAGE */
    IF (OLD.match_second_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 164, OLD.match_second_percentage);

    /* MATCH_SECOND_AMOUNT */
    IF (OLD.match_second_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 163, OLD.match_second_amount);

    /* FLAT_MATCH_AMOUNT */
    IF (OLD.flat_match_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 162, OLD.flat_match_amount);

    /* REGULAR_OT_RATE */
    IF (OLD.regular_ot_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 161, OLD.regular_ot_rate);

    /* PIECEWORK_MINIMUM_WAGE */
    IF (OLD.piecework_minimum_wage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 160, OLD.piecework_minimum_wage);

    /* SD_AMOUNT */
    IF (OLD.sd_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 159, OLD.sd_amount);

    /* SD_RATE */
    IF (OLD.sd_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 158, OLD.sd_rate);

    /* APPLY_BEFORE_TAXES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 240, OLD.apply_before_taxes);

    /* DEFAULT_HOURS */
    IF (OLD.default_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 178, OLD.default_hours);

    /* SHOW_WHICH_BALANCE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 241, OLD.show_which_balance);

    /* OVERSTATE_HOURS_FOR_OVERTIME */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 212, OLD.overstate_hours_for_overtime);

    /* WHICH_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 211, OLD.which_checks);

    /* MONTH_NUMBER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 210, OLD.month_number);

    /* SD_PLAN_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 209, OLD.sd_plan_type);

    /* SD_WHICH_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 208, OLD.sd_which_checks);

    /* SD_PRIORITY_NUMBER */
    IF (OLD.sd_priority_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 183, OLD.sd_priority_number);

    /* SD_ALWAYS_PAY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 207, OLD.sd_always_pay);

    /* SD_DEDUCTIONS_TO_ZERO */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 206, OLD.sd_deductions_to_zero);

    /* SD_MAX_AVG_AMT_GRP_NBR */
    IF (OLD.sd_max_avg_amt_grp_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 182, OLD.sd_max_avg_amt_grp_nbr);

    /* SD_MAX_AVG_HRS_GRP_NBR */
    IF (OLD.sd_max_avg_hrs_grp_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 181, OLD.sd_max_avg_hrs_grp_nbr);

    /* SD_MAX_AVERAGE_HOURLY_WAGE_RATE */
    IF (OLD.sd_max_average_hourly_wage_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 157, OLD.sd_max_average_hourly_wage_rate);

    /* SD_THRESHOLD_E_D_GROUPS_NBR */
    IF (OLD.sd_threshold_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 180, OLD.sd_threshold_e_d_groups_nbr);

    /* SD_THRESHOLD_AMOUNT */
    IF (OLD.sd_threshold_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 156, OLD.sd_threshold_amount);

    /* SD_USE_PENSION_LIMIT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 205, OLD.sd_use_pension_limit);

    /* SD_DEDUCT_WHOLE_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 243, OLD.sd_deduct_whole_check);

    /* TARGET_DESCRIPTION */
    IF (OLD.target_description IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 179, OLD.target_description);

    /* COBRA_ELIGIBLE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 244, OLD.cobra_eligible);

    /* SHOW_ON_REPORT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2866, OLD.show_on_report);

    /* ROUND_OT_CALCULATION */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3219, OLD.round_ot_calculation);

    /* MINIMUM_HW_HOURS */
    IF (OLD.minimum_hw_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3228, OLD.minimum_hw_hours);

    /* MAXIMUM_HW_HOURS */
    IF (OLD.maximum_hw_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3229, OLD.maximum_hw_hours);

    /* HW_FUNDING_CL_E_D_GROUPS_NBR */
    IF (OLD.hw_funding_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3230, OLD.hw_funding_cl_e_d_groups_nbr);

    /* HW_EXCESS_CL_E_D_GROUPS_NBR */
    IF (OLD.hw_excess_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3231, OLD.hw_excess_cl_e_d_groups_nbr);

  END
END

^

CREATE TRIGGER T_AIU_CL_E_DS_3 FOR CL_E_DS After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) OR (OLD.skip_hours IS DISTINCT FROM NEW.skip_hours) OR (OLD.override_ee_rate_number IS DISTINCT FROM NEW.override_ee_rate_number) OR (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) OR (OLD.update_hours IS DISTINCT FROM NEW.update_hours) OR (OLD.default_cl_agency_nbr IS DISTINCT FROM NEW.default_cl_agency_nbr) OR (OLD.min_cl_e_d_groups_nbr IS DISTINCT FROM NEW.min_cl_e_d_groups_nbr) OR (OLD.max_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_cl_e_d_groups_nbr) OR (OLD.cl_union_nbr IS DISTINCT FROM NEW.cl_union_nbr) OR (OLD.priority_to_exclude IS DISTINCT FROM NEW.priority_to_exclude) OR (OLD.make_up_deduction_shortfall IS DISTINCT FROM NEW.make_up_deduction_shortfall) OR (OLD.ot_all_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ot_all_cl_e_d_groups_nbr) OR (OLD.piece_cl_e_d_groups_nbr IS DISTINCT FROM NEW.piece_cl_e_d_groups_nbr) OR (OLD.pw_min_wage_make_up_method IS DISTINCT FROM NEW.pw_min_wage_make_up_method) OR (OLD.tip_minimum_wage_make_up_type IS DISTINCT FROM NEW.tip_minimum_wage_make_up_type) OR (OLD.offset_cl_e_ds_nbr IS DISTINCT FROM NEW.offset_cl_e_ds_nbr) OR (OLD.show_ytd_on_checks IS DISTINCT FROM NEW.show_ytd_on_checks) OR (OLD.scheduled_defaults IS DISTINCT FROM NEW.scheduled_defaults) OR (OLD.sd_frequency IS DISTINCT FROM NEW.sd_frequency) OR (OLD.sd_effective_start_date IS DISTINCT FROM NEW.sd_effective_start_date) OR (OLD.sd_calculation_method IS DISTINCT FROM NEW.sd_calculation_method) OR (rdb$get_context('USER_TRANSACTION', '@SD_EXPRESSION') IS NOT NULL) OR (OLD.sd_auto IS DISTINCT FROM NEW.sd_auto) OR (OLD.sd_round IS DISTINCT FROM NEW.sd_round) OR (OLD.sd_exclude_week_1 IS DISTINCT FROM NEW.sd_exclude_week_1) OR (OLD.sd_exclude_week_2 IS DISTINCT FROM NEW.sd_exclude_week_2) OR (OLD.sd_exclude_week_3 IS DISTINCT FROM NEW.sd_exclude_week_3) OR (OLD.sd_exclude_week_4 IS DISTINCT FROM NEW.sd_exclude_week_4) OR (OLD.sd_exclude_week_5 IS DISTINCT FROM NEW.sd_exclude_week_5) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.override_rate_type IS DISTINCT FROM NEW.override_rate_type) OR (OLD.show_on_input_worksheet IS DISTINCT FROM NEW.show_on_input_worksheet) OR (OLD.sy_state_nbr IS DISTINCT FROM NEW.sy_state_nbr) OR (OLD.gl_offset_tag IS DISTINCT FROM NEW.gl_offset_tag) OR (OLD.show_ed_on_checks IS DISTINCT FROM NEW.show_ed_on_checks) OR (rdb$get_context('USER_TRANSACTION', '@NOTES') IS NOT NULL) OR (OLD.override_rate IS DISTINCT FROM NEW.override_rate) OR (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) OR (OLD.pay_period_minimum_amount IS DISTINCT FROM NEW.pay_period_minimum_amount) OR (OLD.pay_period_minimum_percent_of IS DISTINCT FROM NEW.pay_period_minimum_percent_of) OR (OLD.pay_period_maximum_amount IS DISTINCT FROM NEW.pay_period_maximum_amount) OR (OLD.pay_period_maximum_percent_of IS DISTINCT FROM NEW.pay_period_maximum_percent_of) OR (OLD.annual_maximum IS DISTINCT FROM NEW.annual_maximum) OR (OLD.match_first_percentage IS DISTINCT FROM NEW.match_first_percentage) OR (OLD.match_first_amount IS DISTINCT FROM NEW.match_first_amount) OR (OLD.match_second_percentage IS DISTINCT FROM NEW.match_second_percentage) OR (OLD.match_second_amount IS DISTINCT FROM NEW.match_second_amount) OR (OLD.flat_match_amount IS DISTINCT FROM NEW.flat_match_amount) OR (OLD.regular_ot_rate IS DISTINCT FROM NEW.regular_ot_rate) OR (OLD.piecework_minimum_wage IS DISTINCT FROM NEW.piecework_minimum_wage) OR (OLD.sd_amount IS DISTINCT FROM NEW.sd_amount) OR (OLD.sd_rate IS DISTINCT FROM NEW.sd_rate) OR (OLD.apply_before_taxes IS DISTINCT FROM NEW.apply_before_taxes) OR (OLD.default_hours IS DISTINCT FROM NEW.default_hours) OR (OLD.show_which_balance IS DISTINCT FROM NEW.show_which_balance) OR (OLD.overstate_hours_for_overtime IS DISTINCT FROM NEW.overstate_hours_for_overtime) OR (OLD.which_checks IS DISTINCT FROM NEW.which_checks) OR (OLD.month_number IS DISTINCT FROM NEW.month_number) OR (OLD.sd_plan_type IS DISTINCT FROM NEW.sd_plan_type) OR (OLD.sd_which_checks IS DISTINCT FROM NEW.sd_which_checks) OR (OLD.sd_priority_number IS DISTINCT FROM NEW.sd_priority_number) OR (OLD.sd_always_pay IS DISTINCT FROM NEW.sd_always_pay) OR (OLD.sd_deductions_to_zero IS DISTINCT FROM NEW.sd_deductions_to_zero) OR (OLD.sd_max_avg_amt_grp_nbr IS DISTINCT FROM NEW.sd_max_avg_amt_grp_nbr) OR (OLD.sd_max_avg_hrs_grp_nbr IS DISTINCT FROM NEW.sd_max_avg_hrs_grp_nbr) OR (OLD.sd_max_average_hourly_wage_rate IS DISTINCT FROM NEW.sd_max_average_hourly_wage_rate) OR (OLD.sd_threshold_e_d_groups_nbr IS DISTINCT FROM NEW.sd_threshold_e_d_groups_nbr) OR (OLD.sd_threshold_amount IS DISTINCT FROM NEW.sd_threshold_amount) OR (OLD.sd_use_pension_limit IS DISTINCT FROM NEW.sd_use_pension_limit) OR (OLD.sd_deduct_whole_check IS DISTINCT FROM NEW.sd_deduct_whole_check) OR (OLD.target_description IS DISTINCT FROM NEW.target_description) OR (OLD.cobra_eligible IS DISTINCT FROM NEW.cobra_eligible) OR (OLD.show_on_report IS DISTINCT FROM NEW.show_on_report) OR (OLD.round_ot_calculation IS DISTINCT FROM NEW.round_ot_calculation) OR (OLD.minimum_hw_hours IS DISTINCT FROM NEW.minimum_hw_hours) OR (OLD.maximum_hw_hours IS DISTINCT FROM NEW.maximum_hw_hours) OR (OLD.hw_funding_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_funding_cl_e_d_groups_nbr) OR (OLD.hw_excess_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_excess_cl_e_d_groups_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE cl_e_ds SET 
      cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr, skip_hours = NEW.skip_hours, override_ee_rate_number = NEW.override_ee_rate_number, override_fed_tax_type = NEW.override_fed_tax_type, update_hours = NEW.update_hours, default_cl_agency_nbr = NEW.default_cl_agency_nbr, min_cl_e_d_groups_nbr = NEW.min_cl_e_d_groups_nbr, max_cl_e_d_groups_nbr = NEW.max_cl_e_d_groups_nbr, cl_union_nbr = NEW.cl_union_nbr, priority_to_exclude = NEW.priority_to_exclude, make_up_deduction_shortfall = NEW.make_up_deduction_shortfall, ot_all_cl_e_d_groups_nbr = NEW.ot_all_cl_e_d_groups_nbr, piece_cl_e_d_groups_nbr = NEW.piece_cl_e_d_groups_nbr, pw_min_wage_make_up_method = NEW.pw_min_wage_make_up_method, tip_minimum_wage_make_up_type = NEW.tip_minimum_wage_make_up_type, offset_cl_e_ds_nbr = NEW.offset_cl_e_ds_nbr, show_ytd_on_checks = NEW.show_ytd_on_checks, scheduled_defaults = NEW.scheduled_defaults, sd_frequency = NEW.sd_frequency, sd_effective_start_date = NEW.sd_effective_start_date, sd_calculation_method = NEW.sd_calculation_method, sd_expression = NEW.sd_expression, sd_auto = NEW.sd_auto, sd_round = NEW.sd_round, sd_exclude_week_1 = NEW.sd_exclude_week_1, sd_exclude_week_2 = NEW.sd_exclude_week_2, sd_exclude_week_3 = NEW.sd_exclude_week_3, sd_exclude_week_4 = NEW.sd_exclude_week_4, sd_exclude_week_5 = NEW.sd_exclude_week_5, filler = NEW.filler, override_rate_type = NEW.override_rate_type, show_on_input_worksheet = NEW.show_on_input_worksheet, sy_state_nbr = NEW.sy_state_nbr, gl_offset_tag = NEW.gl_offset_tag, show_ed_on_checks = NEW.show_ed_on_checks, notes = NEW.notes, override_rate = NEW.override_rate, override_fed_tax_value = NEW.override_fed_tax_value, pay_period_minimum_amount = NEW.pay_period_minimum_amount, pay_period_minimum_percent_of = NEW.pay_period_minimum_percent_of, pay_period_maximum_amount = NEW.pay_period_maximum_amount, pay_period_maximum_percent_of = NEW.pay_period_maximum_percent_of, annual_maximum = NEW.annual_maximum, match_first_percentage = NEW.match_first_percentage, match_first_amount = NEW.match_first_amount, match_second_percentage = NEW.match_second_percentage, match_second_amount = NEW.match_second_amount, flat_match_amount = NEW.flat_match_amount, regular_ot_rate = NEW.regular_ot_rate, piecework_minimum_wage = NEW.piecework_minimum_wage, sd_amount = NEW.sd_amount, sd_rate = NEW.sd_rate, apply_before_taxes = NEW.apply_before_taxes, default_hours = NEW.default_hours, show_which_balance = NEW.show_which_balance, overstate_hours_for_overtime = NEW.overstate_hours_for_overtime, which_checks = NEW.which_checks, month_number = NEW.month_number, sd_plan_type = NEW.sd_plan_type, sd_which_checks = NEW.sd_which_checks, sd_priority_number = NEW.sd_priority_number, sd_always_pay = NEW.sd_always_pay, sd_deductions_to_zero = NEW.sd_deductions_to_zero, sd_max_avg_amt_grp_nbr = NEW.sd_max_avg_amt_grp_nbr, sd_max_avg_hrs_grp_nbr = NEW.sd_max_avg_hrs_grp_nbr, sd_max_average_hourly_wage_rate = NEW.sd_max_average_hourly_wage_rate, sd_threshold_e_d_groups_nbr = NEW.sd_threshold_e_d_groups_nbr, sd_threshold_amount = NEW.sd_threshold_amount, sd_use_pension_limit = NEW.sd_use_pension_limit, sd_deduct_whole_check = NEW.sd_deduct_whole_check, target_description = NEW.target_description, cobra_eligible = NEW.cobra_eligible, show_on_report = NEW.show_on_report, round_ot_calculation = NEW.round_ot_calculation, minimum_hw_hours = NEW.minimum_hw_hours, maximum_hw_hours = NEW.maximum_hw_hours, hw_funding_cl_e_d_groups_nbr = NEW.hw_funding_cl_e_d_groups_nbr, hw_excess_cl_e_d_groups_nbr = NEW.hw_excess_cl_e_d_groups_nbr
      WHERE cl_e_ds_nbr = NEW.cl_e_ds_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_CL_E_DS_9 FOR CL_E_DS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(11, NEW.rec_version, NEW.cl_e_ds_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 175, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2576, OLD.effective_until);
    changes = changes + 1;
  END

  /* CUSTOM_E_D_CODE_NUMBER */
  IF (OLD.custom_e_d_code_number IS DISTINCT FROM NEW.custom_e_d_code_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 199, OLD.custom_e_d_code_number);
    changes = changes + 1;
  END

  /* E_D_CODE_TYPE */
  IF (OLD.e_d_code_type IS DISTINCT FROM NEW.e_d_code_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 200, OLD.e_d_code_type);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 201, OLD.description);
    changes = changes + 1;
  END

  /* CL_E_D_GROUPS_NBR */
  IF (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 185, OLD.cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* SKIP_HOURS */
  IF (OLD.skip_hours IS DISTINCT FROM NEW.skip_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 213, OLD.skip_hours);
    changes = changes + 1;
  END

  /* OVERRIDE_EE_RATE_NUMBER */
  IF (OLD.override_ee_rate_number IS DISTINCT FROM NEW.override_ee_rate_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 186, OLD.override_ee_rate_number);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_TYPE */
  IF (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 214, OLD.override_fed_tax_type);
    changes = changes + 1;
  END

  /* UPDATE_HOURS */
  IF (OLD.update_hours IS DISTINCT FROM NEW.update_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 215, OLD.update_hours);
    changes = changes + 1;
  END

  /* DEFAULT_CL_AGENCY_NBR */
  IF (OLD.default_cl_agency_nbr IS DISTINCT FROM NEW.default_cl_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 187, OLD.default_cl_agency_nbr);
    changes = changes + 1;
  END

  /* EE_EXEMPT_EXCLUDE_OASDI */
  IF (OLD.ee_exempt_exclude_oasdi IS DISTINCT FROM NEW.ee_exempt_exclude_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 216, OLD.ee_exempt_exclude_oasdi);
    changes = changes + 1;
  END

  /* EE_EXEMPT_EXCLUDE_MEDICARE */
  IF (OLD.ee_exempt_exclude_medicare IS DISTINCT FROM NEW.ee_exempt_exclude_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 217, OLD.ee_exempt_exclude_medicare);
    changes = changes + 1;
  END

  /* EE_EXEMPT_EXCLUDE_FEDERAL */
  IF (OLD.ee_exempt_exclude_federal IS DISTINCT FROM NEW.ee_exempt_exclude_federal) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 218, OLD.ee_exempt_exclude_federal);
    changes = changes + 1;
  END

  /* EE_EXEMPT_EXCLUDE_EIC */
  IF (OLD.ee_exempt_exclude_eic IS DISTINCT FROM NEW.ee_exempt_exclude_eic) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 219, OLD.ee_exempt_exclude_eic);
    changes = changes + 1;
  END

  /* ER_EXEMPT_EXCLUDE_OASDI */
  IF (OLD.er_exempt_exclude_oasdi IS DISTINCT FROM NEW.er_exempt_exclude_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 220, OLD.er_exempt_exclude_oasdi);
    changes = changes + 1;
  END

  /* ER_EXEMPT_EXCLUDE_MEDICARE */
  IF (OLD.er_exempt_exclude_medicare IS DISTINCT FROM NEW.er_exempt_exclude_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 221, OLD.er_exempt_exclude_medicare);
    changes = changes + 1;
  END

  /* ER_EXEMPT_EXCLUDE_FUI */
  IF (OLD.er_exempt_exclude_fui IS DISTINCT FROM NEW.er_exempt_exclude_fui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 222, OLD.er_exempt_exclude_fui);
    changes = changes + 1;
  END

  /* W2_BOX */
  IF (OLD.w2_box IS DISTINCT FROM NEW.w2_box) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 202, OLD.w2_box);
    changes = changes + 1;
  END

  /* MIN_CL_E_D_GROUPS_NBR */
  IF (OLD.min_cl_e_d_groups_nbr IS DISTINCT FROM NEW.min_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 188, OLD.min_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* MAX_CL_E_D_GROUPS_NBR */
  IF (OLD.max_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 189, OLD.max_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* CL_UNION_NBR */
  IF (OLD.cl_union_nbr IS DISTINCT FROM NEW.cl_union_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 190, OLD.cl_union_nbr);
    changes = changes + 1;
  END

  /* CL_PENSION_NBR */
  IF (OLD.cl_pension_nbr IS DISTINCT FROM NEW.cl_pension_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 191, OLD.cl_pension_nbr);
    changes = changes + 1;
  END

  /* PRIORITY_TO_EXCLUDE */
  IF (OLD.priority_to_exclude IS DISTINCT FROM NEW.priority_to_exclude) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 192, OLD.priority_to_exclude);
    changes = changes + 1;
  END

  /* MAKE_UP_DEDUCTION_SHORTFALL */
  IF (OLD.make_up_deduction_shortfall IS DISTINCT FROM NEW.make_up_deduction_shortfall) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 223, OLD.make_up_deduction_shortfall);
    changes = changes + 1;
  END

  /* OT_ALL_CL_E_D_GROUPS_NBR */
  IF (OLD.ot_all_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ot_all_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 193, OLD.ot_all_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* PIECE_CL_E_D_GROUPS_NBR */
  IF (OLD.piece_cl_e_d_groups_nbr IS DISTINCT FROM NEW.piece_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 194, OLD.piece_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* PW_MIN_WAGE_MAKE_UP_METHOD */
  IF (OLD.pw_min_wage_make_up_method IS DISTINCT FROM NEW.pw_min_wage_make_up_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 224, OLD.pw_min_wage_make_up_method);
    changes = changes + 1;
  END

  /* TIP_MINIMUM_WAGE_MAKE_UP_TYPE */
  IF (OLD.tip_minimum_wage_make_up_type IS DISTINCT FROM NEW.tip_minimum_wage_make_up_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 225, OLD.tip_minimum_wage_make_up_type);
    changes = changes + 1;
  END

  /* OFFSET_CL_E_DS_NBR */
  IF (OLD.offset_cl_e_ds_nbr IS DISTINCT FROM NEW.offset_cl_e_ds_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 195, OLD.offset_cl_e_ds_nbr);
    changes = changes + 1;
  END

  /* CL_3_PARTY_SICK_PAY_ADMIN_NBR */
  IF (OLD.cl_3_party_sick_pay_admin_nbr IS DISTINCT FROM NEW.cl_3_party_sick_pay_admin_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 196, OLD.cl_3_party_sick_pay_admin_nbr);
    changes = changes + 1;
  END

  /* SHOW_YTD_ON_CHECKS */
  IF (OLD.show_ytd_on_checks IS DISTINCT FROM NEW.show_ytd_on_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 226, OLD.show_ytd_on_checks);
    changes = changes + 1;
  END

  /* SCHEDULED_DEFAULTS */
  IF (OLD.scheduled_defaults IS DISTINCT FROM NEW.scheduled_defaults) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 227, OLD.scheduled_defaults);
    changes = changes + 1;
  END

  /* SD_FREQUENCY */
  IF (OLD.sd_frequency IS DISTINCT FROM NEW.sd_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 228, OLD.sd_frequency);
    changes = changes + 1;
  END

  /* SD_EFFECTIVE_START_DATE */
  IF (OLD.sd_effective_start_date IS DISTINCT FROM NEW.sd_effective_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 176, OLD.sd_effective_start_date);
    changes = changes + 1;
  END

  /* SD_CALCULATION_METHOD */
  IF (OLD.sd_calculation_method IS DISTINCT FROM NEW.sd_calculation_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 229, OLD.sd_calculation_method);
    changes = changes + 1;
  END

  /* SD_EXPRESSION */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SD_EXPRESSION');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SD_EXPRESSION', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 177, :blob_nbr);
    changes = changes + 1;
  END

  /* SD_AUTO */
  IF (OLD.sd_auto IS DISTINCT FROM NEW.sd_auto) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 230, OLD.sd_auto);
    changes = changes + 1;
  END

  /* SD_ROUND */
  IF (OLD.sd_round IS DISTINCT FROM NEW.sd_round) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 231, OLD.sd_round);
    changes = changes + 1;
  END

  /* SD_EXCLUDE_WEEK_1 */
  IF (OLD.sd_exclude_week_1 IS DISTINCT FROM NEW.sd_exclude_week_1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 232, OLD.sd_exclude_week_1);
    changes = changes + 1;
  END

  /* SD_EXCLUDE_WEEK_2 */
  IF (OLD.sd_exclude_week_2 IS DISTINCT FROM NEW.sd_exclude_week_2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 233, OLD.sd_exclude_week_2);
    changes = changes + 1;
  END

  /* SD_EXCLUDE_WEEK_3 */
  IF (OLD.sd_exclude_week_3 IS DISTINCT FROM NEW.sd_exclude_week_3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 234, OLD.sd_exclude_week_3);
    changes = changes + 1;
  END

  /* SD_EXCLUDE_WEEK_4 */
  IF (OLD.sd_exclude_week_4 IS DISTINCT FROM NEW.sd_exclude_week_4) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 235, OLD.sd_exclude_week_4);
    changes = changes + 1;
  END

  /* SD_EXCLUDE_WEEK_5 */
  IF (OLD.sd_exclude_week_5 IS DISTINCT FROM NEW.sd_exclude_week_5) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 236, OLD.sd_exclude_week_5);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 203, OLD.filler);
    changes = changes + 1;
  END

  /* OVERRIDE_RATE_TYPE */
  IF (OLD.override_rate_type IS DISTINCT FROM NEW.override_rate_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 237, OLD.override_rate_type);
    changes = changes + 1;
  END

  /* SHOW_ON_INPUT_WORKSHEET */
  IF (OLD.show_on_input_worksheet IS DISTINCT FROM NEW.show_on_input_worksheet) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 238, OLD.show_on_input_worksheet);
    changes = changes + 1;
  END

  /* SY_STATE_NBR */
  IF (OLD.sy_state_nbr IS DISTINCT FROM NEW.sy_state_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 197, OLD.sy_state_nbr);
    changes = changes + 1;
  END

  /* GL_OFFSET_TAG */
  IF (OLD.gl_offset_tag IS DISTINCT FROM NEW.gl_offset_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 204, OLD.gl_offset_tag);
    changes = changes + 1;
  END

  /* SHOW_ED_ON_CHECKS */
  IF (OLD.show_ed_on_checks IS DISTINCT FROM NEW.show_ed_on_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 239, OLD.show_ed_on_checks);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 174, :blob_nbr);
    changes = changes + 1;
  END

  /* OVERRIDE_RATE */
  IF (OLD.override_rate IS DISTINCT FROM NEW.override_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 173, OLD.override_rate);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_VALUE */
  IF (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 172, OLD.override_fed_tax_value);
    changes = changes + 1;
  END

  /* PAY_PERIOD_MINIMUM_AMOUNT */
  IF (OLD.pay_period_minimum_amount IS DISTINCT FROM NEW.pay_period_minimum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 171, OLD.pay_period_minimum_amount);
    changes = changes + 1;
  END

  /* PAY_PERIOD_MINIMUM_PERCENT_OF */
  IF (OLD.pay_period_minimum_percent_of IS DISTINCT FROM NEW.pay_period_minimum_percent_of) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 170, OLD.pay_period_minimum_percent_of);
    changes = changes + 1;
  END

  /* PAY_PERIOD_MAXIMUM_AMOUNT */
  IF (OLD.pay_period_maximum_amount IS DISTINCT FROM NEW.pay_period_maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 169, OLD.pay_period_maximum_amount);
    changes = changes + 1;
  END

  /* PAY_PERIOD_MAXIMUM_PERCENT_OF */
  IF (OLD.pay_period_maximum_percent_of IS DISTINCT FROM NEW.pay_period_maximum_percent_of) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 168, OLD.pay_period_maximum_percent_of);
    changes = changes + 1;
  END

  /* ANNUAL_MAXIMUM */
  IF (OLD.annual_maximum IS DISTINCT FROM NEW.annual_maximum) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 167, OLD.annual_maximum);
    changes = changes + 1;
  END

  /* MATCH_FIRST_PERCENTAGE */
  IF (OLD.match_first_percentage IS DISTINCT FROM NEW.match_first_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 166, OLD.match_first_percentage);
    changes = changes + 1;
  END

  /* MATCH_FIRST_AMOUNT */
  IF (OLD.match_first_amount IS DISTINCT FROM NEW.match_first_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 165, OLD.match_first_amount);
    changes = changes + 1;
  END

  /* MATCH_SECOND_PERCENTAGE */
  IF (OLD.match_second_percentage IS DISTINCT FROM NEW.match_second_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 164, OLD.match_second_percentage);
    changes = changes + 1;
  END

  /* MATCH_SECOND_AMOUNT */
  IF (OLD.match_second_amount IS DISTINCT FROM NEW.match_second_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 163, OLD.match_second_amount);
    changes = changes + 1;
  END

  /* FLAT_MATCH_AMOUNT */
  IF (OLD.flat_match_amount IS DISTINCT FROM NEW.flat_match_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 162, OLD.flat_match_amount);
    changes = changes + 1;
  END

  /* REGULAR_OT_RATE */
  IF (OLD.regular_ot_rate IS DISTINCT FROM NEW.regular_ot_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 161, OLD.regular_ot_rate);
    changes = changes + 1;
  END

  /* PIECEWORK_MINIMUM_WAGE */
  IF (OLD.piecework_minimum_wage IS DISTINCT FROM NEW.piecework_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 160, OLD.piecework_minimum_wage);
    changes = changes + 1;
  END

  /* SD_AMOUNT */
  IF (OLD.sd_amount IS DISTINCT FROM NEW.sd_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 159, OLD.sd_amount);
    changes = changes + 1;
  END

  /* SD_RATE */
  IF (OLD.sd_rate IS DISTINCT FROM NEW.sd_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 158, OLD.sd_rate);
    changes = changes + 1;
  END

  /* APPLY_BEFORE_TAXES */
  IF (OLD.apply_before_taxes IS DISTINCT FROM NEW.apply_before_taxes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 240, OLD.apply_before_taxes);
    changes = changes + 1;
  END

  /* DEFAULT_HOURS */
  IF (OLD.default_hours IS DISTINCT FROM NEW.default_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 178, OLD.default_hours);
    changes = changes + 1;
  END

  /* SHOW_WHICH_BALANCE */
  IF (OLD.show_which_balance IS DISTINCT FROM NEW.show_which_balance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 241, OLD.show_which_balance);
    changes = changes + 1;
  END

  /* OVERSTATE_HOURS_FOR_OVERTIME */
  IF (OLD.overstate_hours_for_overtime IS DISTINCT FROM NEW.overstate_hours_for_overtime) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 212, OLD.overstate_hours_for_overtime);
    changes = changes + 1;
  END

  /* WHICH_CHECKS */
  IF (OLD.which_checks IS DISTINCT FROM NEW.which_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 211, OLD.which_checks);
    changes = changes + 1;
  END

  /* MONTH_NUMBER */
  IF (OLD.month_number IS DISTINCT FROM NEW.month_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 210, OLD.month_number);
    changes = changes + 1;
  END

  /* SD_PLAN_TYPE */
  IF (OLD.sd_plan_type IS DISTINCT FROM NEW.sd_plan_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 209, OLD.sd_plan_type);
    changes = changes + 1;
  END

  /* SD_WHICH_CHECKS */
  IF (OLD.sd_which_checks IS DISTINCT FROM NEW.sd_which_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 208, OLD.sd_which_checks);
    changes = changes + 1;
  END

  /* SD_PRIORITY_NUMBER */
  IF (OLD.sd_priority_number IS DISTINCT FROM NEW.sd_priority_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 183, OLD.sd_priority_number);
    changes = changes + 1;
  END

  /* SD_ALWAYS_PAY */
  IF (OLD.sd_always_pay IS DISTINCT FROM NEW.sd_always_pay) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 207, OLD.sd_always_pay);
    changes = changes + 1;
  END

  /* SD_DEDUCTIONS_TO_ZERO */
  IF (OLD.sd_deductions_to_zero IS DISTINCT FROM NEW.sd_deductions_to_zero) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 206, OLD.sd_deductions_to_zero);
    changes = changes + 1;
  END

  /* SD_MAX_AVG_AMT_GRP_NBR */
  IF (OLD.sd_max_avg_amt_grp_nbr IS DISTINCT FROM NEW.sd_max_avg_amt_grp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 182, OLD.sd_max_avg_amt_grp_nbr);
    changes = changes + 1;
  END

  /* SD_MAX_AVG_HRS_GRP_NBR */
  IF (OLD.sd_max_avg_hrs_grp_nbr IS DISTINCT FROM NEW.sd_max_avg_hrs_grp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 181, OLD.sd_max_avg_hrs_grp_nbr);
    changes = changes + 1;
  END

  /* SD_MAX_AVERAGE_HOURLY_WAGE_RATE */
  IF (OLD.sd_max_average_hourly_wage_rate IS DISTINCT FROM NEW.sd_max_average_hourly_wage_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 157, OLD.sd_max_average_hourly_wage_rate);
    changes = changes + 1;
  END

  /* SD_THRESHOLD_E_D_GROUPS_NBR */
  IF (OLD.sd_threshold_e_d_groups_nbr IS DISTINCT FROM NEW.sd_threshold_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 180, OLD.sd_threshold_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* SD_THRESHOLD_AMOUNT */
  IF (OLD.sd_threshold_amount IS DISTINCT FROM NEW.sd_threshold_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 156, OLD.sd_threshold_amount);
    changes = changes + 1;
  END

  /* SD_USE_PENSION_LIMIT */
  IF (OLD.sd_use_pension_limit IS DISTINCT FROM NEW.sd_use_pension_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 205, OLD.sd_use_pension_limit);
    changes = changes + 1;
  END

  /* SD_DEDUCT_WHOLE_CHECK */
  IF (OLD.sd_deduct_whole_check IS DISTINCT FROM NEW.sd_deduct_whole_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 243, OLD.sd_deduct_whole_check);
    changes = changes + 1;
  END

  /* TARGET_DESCRIPTION */
  IF (OLD.target_description IS DISTINCT FROM NEW.target_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 179, OLD.target_description);
    changes = changes + 1;
  END

  /* COBRA_ELIGIBLE */
  IF (OLD.cobra_eligible IS DISTINCT FROM NEW.cobra_eligible) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 244, OLD.cobra_eligible);
    changes = changes + 1;
  END

  /* SHOW_ON_REPORT */
  IF (OLD.show_on_report IS DISTINCT FROM NEW.show_on_report) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2866, OLD.show_on_report);
    changes = changes + 1;
  END

  /* ROUND_OT_CALCULATION */
  IF (OLD.round_ot_calculation IS DISTINCT FROM NEW.round_ot_calculation) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3219, OLD.round_ot_calculation);
    changes = changes + 1;
  END

  /* MINIMUM_HW_HOURS */
  IF (OLD.minimum_hw_hours IS DISTINCT FROM NEW.minimum_hw_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3228, OLD.minimum_hw_hours);
    changes = changes + 1;
  END

  /* MAXIMUM_HW_HOURS */
  IF (OLD.maximum_hw_hours IS DISTINCT FROM NEW.maximum_hw_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3229, OLD.maximum_hw_hours);
    changes = changes + 1;
  END

  /* HW_FUNDING_CL_E_D_GROUPS_NBR */
  IF (OLD.hw_funding_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_funding_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3230, OLD.hw_funding_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* HW_EXCESS_CL_E_D_GROUPS_NBR */
  IF (OLD.hw_excess_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_excess_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3231, OLD.hw_excess_cl_e_d_groups_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CL_E_DS_2 FOR CL_E_DS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CL_E_DS */
  IF ((NEW.offset_cl_e_ds_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.offset_cl_e_ds_nbr IS DISTINCT FROM NEW.offset_cl_e_ds_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.offset_cl_e_ds_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'offset_cl_e_ds_nbr', NEW.offset_cl_e_ds_nbr, 'cl_e_ds', NEW.effective_date);

    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.offset_cl_e_ds_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'offset_cl_e_ds_nbr', NEW.offset_cl_e_ds_nbr, 'cl_e_ds', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_e_d_groups_nbr', NEW.cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_e_d_groups_nbr', NEW.cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.min_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.min_cl_e_d_groups_nbr IS DISTINCT FROM NEW.min_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.min_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'min_cl_e_d_groups_nbr', NEW.min_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.min_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'min_cl_e_d_groups_nbr', NEW.min_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.max_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.max_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'max_cl_e_d_groups_nbr', NEW.max_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'max_cl_e_d_groups_nbr', NEW.max_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.piece_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.piece_cl_e_d_groups_nbr IS DISTINCT FROM NEW.piece_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.piece_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'piece_cl_e_d_groups_nbr', NEW.piece_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.piece_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'piece_cl_e_d_groups_nbr', NEW.piece_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.ot_all_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ot_all_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ot_all_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.ot_all_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'ot_all_cl_e_d_groups_nbr', NEW.ot_all_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.ot_all_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'ot_all_cl_e_d_groups_nbr', NEW.ot_all_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_PENSION */
  IF ((NEW.cl_pension_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_pension_nbr IS DISTINCT FROM NEW.cl_pension_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_pension
    WHERE cl_pension_nbr = NEW.cl_pension_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_pension_nbr', NEW.cl_pension_nbr, 'cl_pension', NEW.effective_date);

    SELECT rec_version FROM cl_pension
    WHERE cl_pension_nbr = NEW.cl_pension_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_pension_nbr', NEW.cl_pension_nbr, 'cl_pension', NEW.effective_until - 1);
  END

  /* Check reference to CL_UNION */
  IF ((NEW.cl_union_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_union_nbr IS DISTINCT FROM NEW.cl_union_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_union
    WHERE cl_union_nbr = NEW.cl_union_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_union_nbr', NEW.cl_union_nbr, 'cl_union', NEW.effective_date);

    SELECT rec_version FROM cl_union
    WHERE cl_union_nbr = NEW.cl_union_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_union_nbr', NEW.cl_union_nbr, 'cl_union', NEW.effective_until - 1);
  END

  /* Check reference to CL_AGENCY */
  IF ((NEW.default_cl_agency_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.default_cl_agency_nbr IS DISTINCT FROM NEW.default_cl_agency_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.default_cl_agency_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'default_cl_agency_nbr', NEW.default_cl_agency_nbr, 'cl_agency', NEW.effective_date);

    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.default_cl_agency_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'default_cl_agency_nbr', NEW.default_cl_agency_nbr, 'cl_agency', NEW.effective_until - 1);
  END

  /* Check reference to CL_3_PARTY_SICK_PAY_ADMIN */
  IF ((NEW.cl_3_party_sick_pay_admin_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_3_party_sick_pay_admin_nbr IS DISTINCT FROM NEW.cl_3_party_sick_pay_admin_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_3_party_sick_pay_admin
    WHERE cl_3_party_sick_pay_admin_nbr = NEW.cl_3_party_sick_pay_admin_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_3_party_sick_pay_admin_nbr', NEW.cl_3_party_sick_pay_admin_nbr, 'cl_3_party_sick_pay_admin', NEW.effective_date);

    SELECT rec_version FROM cl_3_party_sick_pay_admin
    WHERE cl_3_party_sick_pay_admin_nbr = NEW.cl_3_party_sick_pay_admin_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'cl_3_party_sick_pay_admin_nbr', NEW.cl_3_party_sick_pay_admin_nbr, 'cl_3_party_sick_pay_admin', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.sd_max_avg_amt_grp_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sd_max_avg_amt_grp_nbr IS DISTINCT FROM NEW.sd_max_avg_amt_grp_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.sd_max_avg_amt_grp_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'sd_max_avg_amt_grp_nbr', NEW.sd_max_avg_amt_grp_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.sd_max_avg_amt_grp_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'sd_max_avg_amt_grp_nbr', NEW.sd_max_avg_amt_grp_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.sd_max_avg_hrs_grp_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sd_max_avg_hrs_grp_nbr IS DISTINCT FROM NEW.sd_max_avg_hrs_grp_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.sd_max_avg_hrs_grp_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'sd_max_avg_hrs_grp_nbr', NEW.sd_max_avg_hrs_grp_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.sd_max_avg_hrs_grp_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'sd_max_avg_hrs_grp_nbr', NEW.sd_max_avg_hrs_grp_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.sd_threshold_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sd_threshold_e_d_groups_nbr IS DISTINCT FROM NEW.sd_threshold_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.sd_threshold_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'sd_threshold_e_d_groups_nbr', NEW.sd_threshold_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.sd_threshold_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'sd_threshold_e_d_groups_nbr', NEW.sd_threshold_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.hw_funding_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.hw_funding_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_funding_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_funding_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'hw_funding_cl_e_d_groups_nbr', NEW.hw_funding_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_funding_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'hw_funding_cl_e_d_groups_nbr', NEW.hw_funding_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.hw_excess_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.hw_excess_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_excess_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_excess_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'hw_excess_cl_e_d_groups_nbr', NEW.hw_excess_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_excess_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'cl_e_ds', NEW.cl_e_ds_nbr, 'hw_excess_cl_e_d_groups_nbr', NEW.hw_excess_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AUD_CL_E_D_GROUPS_2 FOR CL_E_D_GROUPS After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM cl_e_d_groups WHERE cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM cl_e_d_groups WHERE cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (auto_rd_dflt_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (auto_rd_dflt_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_WORKERS_COMP */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_workers_comp', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_workers_comp', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (min_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (min_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (max_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (max_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (piece_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (piece_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (ot_all_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (ot_all_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (sd_max_avg_amt_grp_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (sd_max_avg_amt_grp_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (sd_max_avg_hrs_grp_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (sd_max_avg_hrs_grp_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (sd_threshold_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (sd_threshold_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_PENSION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_pension
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_pension', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_pension
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_pension', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_UNION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_union
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_union', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_union
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_union', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_D_GROUP_CODES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_group_codes
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_d_group_codes', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_group_codes
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_d_group_codes', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (ald_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (ald_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SHIFTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_shifts
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_shifts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_shifts
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_shifts', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SERVICES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_services
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_services', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_services
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_services', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (max_ppp_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (max_ppp_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (min_ppp_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (min_ppp_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (scheduled_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (scheduled_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (max_avg_amt_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (max_avg_amt_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (max_avg_hrs_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (max_avg_hrs_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (threshold_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (threshold_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR_CHECK_LINES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (reducing_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (reducing_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TIME_OFF_ACCRUAL */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_time_off_accrual
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_time_off_accrual', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_time_off_accrual
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_time_off_accrual', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TIME_OFF_ACCRUAL */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_time_off_accrual
    WHERE (used_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_time_off_accrual', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_time_off_accrual
    WHERE (used_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_time_off_accrual', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_AUTOLABOR_DISTRIBUTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_BENEFITS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_benefits', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_benefits', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_RATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_rates
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_benefit_rates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_rates
    WHERE (cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co_benefit_rates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (aca_add_earn_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (aca_add_earn_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (hw_funding_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (hw_funding_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CL_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (hw_excess_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE (hw_excess_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'cl_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (hw_funding_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (hw_funding_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (hw_excess_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (hw_excess_cl_e_d_groups_nbr = OLD.cl_e_d_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_e_d_groups', OLD.cl_e_d_groups_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_CO_9 FOR CO After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE employer_type CHAR(1);
DECLARE VARIABLE co_locations_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  table_change_nbr = rdb$get_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR');
  IF (table_change_nbr IS NULL) THEN
    EXIT;

  last_record = 'Y';
  SELECT 'N', employer_type, co_locations_nbr  FROM co WHERE co_nbr = OLD.co_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :employer_type, :co_locations_nbr;

  /* EMPLOYER_TYPE */
  IF (last_record = 'Y' OR employer_type IS DISTINCT FROM OLD.employer_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2885, OLD.employer_type);

  /* CO_LOCATIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_locations_nbr IS NOT NULL) OR (last_record = 'N' AND co_locations_nbr IS DISTINCT FROM OLD.co_locations_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3169, OLD.co_locations_nbr);


  IF (last_record = 'Y') THEN
  BEGIN
    /* VT_HEALTHCARE_OFFSET_GL_TAG */
    IF (OLD.vt_healthcare_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 660, OLD.vt_healthcare_offset_gl_tag);

    /* UI_ROUNDING_GL_TAG */
    IF (OLD.ui_rounding_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 661, OLD.ui_rounding_gl_tag);

    /* UI_ROUNDING_OFFSET_GL_TAG */
    IF (OLD.ui_rounding_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 662, OLD.ui_rounding_offset_gl_tag);

    /* REPRINT_TO_BALANCE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 763, OLD.reprint_to_balance);

    /* SHOW_MANUAL_CHECKS_IN_ESS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 765, OLD.show_manual_checks_in_ess);

    /* PAYROLL_REQUIRES_MGR_APPROVAL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 766, OLD.payroll_requires_mgr_approval);

    /* DAYS_PRIOR_TO_CHECK_DATE */
    IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 599, OLD.days_prior_to_check_date);

    /* WC_OFFS_BANK_ACCOUNT_NBR */
    IF (OLD.wc_offs_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 603, OLD.wc_offs_bank_account_nbr);

    /* QTR_LOCK_FOR_TAX_PMTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 767, OLD.qtr_lock_for_tax_pmts);

    /* CO_MAX_AMOUNT_FOR_PAYROLL */
    IF (OLD.co_max_amount_for_payroll IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 515, OLD.co_max_amount_for_payroll);

    /* CO_MAX_AMOUNT_FOR_TAX_IMPOUND */
    IF (OLD.co_max_amount_for_tax_impound IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 516, OLD.co_max_amount_for_tax_impound);

    /* CO_MAX_AMOUNT_FOR_DD */
    IF (OLD.co_max_amount_for_dd IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 517, OLD.co_max_amount_for_dd);

    /* CO_PAYROLL_PROCESS_LIMITATIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 768, OLD.co_payroll_process_limitations);

    /* CO_ACH_PROCESS_LIMITATIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 769, OLD.co_ach_process_limitations);

    /* TRUST_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 770, OLD.trust_impound);

    /* TAX_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 771, OLD.tax_impound);

    /* DD_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 772, OLD.dd_impound);

    /* BILLING_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 773, OLD.billing_impound);

    /* WC_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 774, OLD.wc_impound);

    /* COBRA_CREDIT_GL_TAG */
    IF (OLD.cobra_credit_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 663, OLD.cobra_credit_gl_tag);

    /* CO_EXCEPTION_PAYMENT_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 775, OLD.co_exception_payment_type);

    /* LAST_TAX_RETURN */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 776, OLD.last_tax_return);

    /* ENABLE_HR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 764, OLD.enable_hr);

    /* ENABLE_ESS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 777, OLD.enable_ess);

    /* DUNS_AND_BRADSTREET */
    IF (OLD.duns_and_bradstreet IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 519, OLD.duns_and_bradstreet);

    /* BANK_ACCOUNT_REGISTER_NAME */
    IF (OLD.bank_account_register_name IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 780, OLD.bank_account_register_name);

    /* ENABLE_BENEFITS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2883, OLD.enable_benefits);

    /* ENABLE_TIME_OFF */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2884, OLD.enable_time_off);

    /* WC_FISCAL_YEAR_BEGIN */
    IF (OLD.wc_fiscal_year_begin IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2886, OLD.wc_fiscal_year_begin);

    /* SHOW_PAYSTUBS_ESS_DAYS */
    IF (OLD.show_paystubs_ess_days IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2888, OLD.show_paystubs_ess_days);

    /* ANNUAL_FORM_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3200, OLD.annual_form_type);

    /* PRENOTE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3201, OLD.prenote);

    /* ENABLE_DD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3202, OLD.enable_dd);

    /* ESS_OR_EP */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3203, OLD.ess_or_ep);

    /* ACA_EDUCATION_ORG */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3204, OLD.aca_education_org);

    /* ACA_STANDARD_HOURS */
    IF (OLD.aca_standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3205, OLD.aca_standard_hours);

    /* ACA_DEFAULT_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3206, OLD.aca_default_status);

    /* ACA_CONTROL_GROUP */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3207, OLD.aca_control_group);

    /* ACA_INITIAL_PERIOD_FROM */
    IF (OLD.aca_initial_period_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3208, OLD.aca_initial_period_from);

    /* ACA_INITIAL_PERIOD_TO */
    IF (OLD.aca_initial_period_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3209, OLD.aca_initial_period_to);

    /* ACA_STABILITY_PERIOD_FROM */
    IF (OLD.aca_stability_period_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3210, OLD.aca_stability_period_from);

    /* ACA_STABILITY_PERIOD_TO */
    IF (OLD.aca_stability_period_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3211, OLD.aca_stability_period_to);

    /* ACA_USE_AVG_HOURS_WORKED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3212, OLD.aca_use_avg_hours_worked);

    /* ACA_AVG_HOURS_WORKED_PERIOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3213, OLD.aca_avg_hours_worked_period);

    /* ACA_ADD_EARN_CL_E_D_GROUPS_NBR */
    IF (OLD.aca_add_earn_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3227, OLD.aca_add_earn_cl_e_d_groups_nbr);

  END

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', NULL);
END

^

CREATE TRIGGER T_AIU_CO_3 FOR CO After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.custom_company_number IS DISTINCT FROM NEW.custom_company_number) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.referred_by IS DISTINCT FROM NEW.referred_by) OR (OLD.sb_referrals_nbr IS DISTINCT FROM NEW.sb_referrals_nbr) OR (OLD.start_date IS DISTINCT FROM NEW.start_date) OR (rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES') IS NOT NULL) OR (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) OR (OLD.accountant_contact IS DISTINCT FROM NEW.accountant_contact) OR (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr) OR (OLD.remote IS DISTINCT FROM NEW.remote) OR (OLD.business_start_date IS DISTINCT FROM NEW.business_start_date) OR (OLD.corporation_type IS DISTINCT FROM NEW.corporation_type) OR (rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS') IS NOT NULL) OR (OLD.restaurant IS DISTINCT FROM NEW.restaurant) OR (OLD.days_open IS DISTINCT FROM NEW.days_open) OR (OLD.time_open IS DISTINCT FROM NEW.time_open) OR (OLD.time_close IS DISTINCT FROM NEW.time_close) OR (rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES') IS NOT NULL) OR (OLD.successor_company IS DISTINCT FROM NEW.successor_company) OR (OLD.medical_plan IS DISTINCT FROM NEW.medical_plan) OR (OLD.retirement_age IS DISTINCT FROM NEW.retirement_age) OR (OLD.pay_frequencies IS DISTINCT FROM NEW.pay_frequencies) OR (OLD.general_ledger_format_string IS DISTINCT FROM NEW.general_ledger_format_string) OR (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr) OR (OLD.dbdt_level IS DISTINCT FROM NEW.dbdt_level) OR (OLD.billing_level IS DISTINCT FROM NEW.billing_level) OR (OLD.bank_account_level IS DISTINCT FROM NEW.bank_account_level) OR (OLD.billing_sb_bank_account_nbr IS DISTINCT FROM NEW.billing_sb_bank_account_nbr) OR (OLD.tax_sb_bank_account_nbr IS DISTINCT FROM NEW.tax_sb_bank_account_nbr) OR (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_pr IS DISTINCT FROM NEW.debit_number_days_prior_pr) OR (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_tax IS DISTINCT FROM NEW.debit_number_of_days_prior_tax) OR (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_bill IS DISTINCT FROM NEW.debit_number_days_prior_bill) OR (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_dd IS DISTINCT FROM NEW.debit_number_of_days_prior_dd) OR (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr) OR (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr) OR (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr) OR (OLD.tax_service IS DISTINCT FROM NEW.tax_service) OR (OLD.tax_service_start_date IS DISTINCT FROM NEW.tax_service_start_date) OR (OLD.tax_service_end_date IS DISTINCT FROM NEW.tax_service_end_date) OR (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr) OR (OLD.workers_comp_policy_id IS DISTINCT FROM NEW.workers_comp_policy_id) OR (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_wc IS DISTINCT FROM NEW.debit_number_days_prior_wc) OR (OLD.w_comp_sb_bank_account_nbr IS DISTINCT FROM NEW.w_comp_sb_bank_account_nbr) OR (OLD.eftps_enrollment_status IS DISTINCT FROM NEW.eftps_enrollment_status) OR (OLD.eftps_enrollment_date IS DISTINCT FROM NEW.eftps_enrollment_date) OR (OLD.eftps_sequence_number IS DISTINCT FROM NEW.eftps_sequence_number) OR (OLD.eftps_enrollment_number IS DISTINCT FROM NEW.eftps_enrollment_number) OR (OLD.eftps_name IS DISTINCT FROM NEW.eftps_name) OR (OLD.pin_number IS DISTINCT FROM NEW.pin_number) OR (OLD.ach_sb_bank_account_nbr IS DISTINCT FROM NEW.ach_sb_bank_account_nbr) OR (OLD.trust_service IS DISTINCT FROM NEW.trust_service) OR (OLD.trust_sb_account_nbr IS DISTINCT FROM NEW.trust_sb_account_nbr) OR (OLD.trust_service_start_date IS DISTINCT FROM NEW.trust_service_start_date) OR (OLD.trust_service_end_date IS DISTINCT FROM NEW.trust_service_end_date) OR (OLD.obc IS DISTINCT FROM NEW.obc) OR (OLD.sb_obc_account_nbr IS DISTINCT FROM NEW.sb_obc_account_nbr) OR (OLD.obc_start_date IS DISTINCT FROM NEW.obc_start_date) OR (OLD.obc_end_date IS DISTINCT FROM NEW.obc_end_date) OR (OLD.sy_fed_reporting_agency_nbr IS DISTINCT FROM NEW.sy_fed_reporting_agency_nbr) OR (OLD.sy_fed_tax_payment_agency_nbr IS DISTINCT FROM NEW.sy_fed_tax_payment_agency_nbr) OR (OLD.federal_tax_transfer_method IS DISTINCT FROM NEW.federal_tax_transfer_method) OR (OLD.federal_tax_payment_method IS DISTINCT FROM NEW.federal_tax_payment_method) OR (OLD.fui_sy_tax_payment_agency IS DISTINCT FROM NEW.fui_sy_tax_payment_agency) OR (OLD.fui_sy_tax_report_agency IS DISTINCT FROM NEW.fui_sy_tax_report_agency) OR (OLD.external_tax_export IS DISTINCT FROM NEW.external_tax_export) OR (OLD.non_profit IS DISTINCT FROM NEW.non_profit) OR (OLD.primary_sort_field IS DISTINCT FROM NEW.primary_sort_field) OR (OLD.secondary_sort_field IS DISTINCT FROM NEW.secondary_sort_field) OR (OLD.fiscal_year_end IS DISTINCT FROM NEW.fiscal_year_end) OR (OLD.third_party_in_gross_pr_report IS DISTINCT FROM NEW.third_party_in_gross_pr_report) OR (OLD.sic_code IS DISTINCT FROM NEW.sic_code) OR (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) OR (OLD.autopay_company IS DISTINCT FROM NEW.autopay_company) OR (OLD.pay_frequency_hourly_default IS DISTINCT FROM NEW.pay_frequency_hourly_default) OR (OLD.pay_frequency_salary_default IS DISTINCT FROM NEW.pay_frequency_salary_default) OR (OLD.co_check_primary_sort IS DISTINCT FROM NEW.co_check_primary_sort) OR (OLD.co_check_secondary_sort IS DISTINCT FROM NEW.co_check_secondary_sort) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr) OR (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr) OR (OLD.smoker_default IS DISTINCT FROM NEW.smoker_default) OR (OLD.auto_labor_dist_show_deducts IS DISTINCT FROM NEW.auto_labor_dist_show_deducts) OR (OLD.auto_labor_dist_level IS DISTINCT FROM NEW.auto_labor_dist_level) OR (OLD.distribute_deductions_default IS DISTINCT FROM NEW.distribute_deductions_default) OR (OLD.withholding_default IS DISTINCT FROM NEW.withholding_default) OR (OLD.check_type IS DISTINCT FROM NEW.check_type) OR (OLD.show_shifts_on_check IS DISTINCT FROM NEW.show_shifts_on_check) OR (OLD.show_ytd_on_check IS DISTINCT FROM NEW.show_ytd_on_check) OR (OLD.show_ein_number_on_check IS DISTINCT FROM NEW.show_ein_number_on_check) OR (OLD.show_ss_number_on_check IS DISTINCT FROM NEW.show_ss_number_on_check) OR (OLD.time_off_accrual IS DISTINCT FROM NEW.time_off_accrual) OR (OLD.check_form IS DISTINCT FROM NEW.check_form) OR (OLD.print_manual_check_stubs IS DISTINCT FROM NEW.print_manual_check_stubs) OR (OLD.credit_hold IS DISTINCT FROM NEW.credit_hold) OR (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr) OR (OLD.payroll_password IS DISTINCT FROM NEW.payroll_password) OR (OLD.remote_of_client IS DISTINCT FROM NEW.remote_of_client) OR (OLD.hardware_o_s IS DISTINCT FROM NEW.hardware_o_s) OR (OLD.network IS DISTINCT FROM NEW.network) OR (OLD.modem_speed IS DISTINCT FROM NEW.modem_speed) OR (OLD.modem_connection_type IS DISTINCT FROM NEW.modem_connection_type) OR (OLD.network_administrator IS DISTINCT FROM NEW.network_administrator) OR (OLD.network_administrator_phone IS DISTINCT FROM NEW.network_administrator_phone) OR (OLD.network_admin_phone_type IS DISTINCT FROM NEW.network_admin_phone_type) OR (OLD.external_network_administrator IS DISTINCT FROM NEW.external_network_administrator) OR (OLD.transmission_destination IS DISTINCT FROM NEW.transmission_destination) OR (OLD.last_call_in_date IS DISTINCT FROM NEW.last_call_in_date) OR (OLD.setup_completed IS DISTINCT FROM NEW.setup_completed) OR (OLD.first_monthly_payroll_day IS DISTINCT FROM NEW.first_monthly_payroll_day) OR (OLD.second_monthly_payroll_day IS DISTINCT FROM NEW.second_monthly_payroll_day) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.collate_checks IS DISTINCT FROM NEW.collate_checks) OR (OLD.process_priority IS DISTINCT FROM NEW.process_priority) OR (OLD.check_message IS DISTINCT FROM NEW.check_message) OR (OLD.customer_service_sb_user_nbr IS DISTINCT FROM NEW.customer_service_sb_user_nbr) OR (rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES') IS NOT NULL) OR (rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES') IS NOT NULL) OR (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.billing_general_ledger_tag IS DISTINCT FROM NEW.billing_general_ledger_tag) OR (OLD.federal_general_ledger_tag IS DISTINCT FROM NEW.federal_general_ledger_tag) OR (OLD.ee_oasdi_general_ledger_tag IS DISTINCT FROM NEW.ee_oasdi_general_ledger_tag) OR (OLD.er_oasdi_general_ledger_tag IS DISTINCT FROM NEW.er_oasdi_general_ledger_tag) OR (OLD.ee_medicare_general_ledger_tag IS DISTINCT FROM NEW.ee_medicare_general_ledger_tag) OR (OLD.er_medicare_general_ledger_tag IS DISTINCT FROM NEW.er_medicare_general_ledger_tag) OR (OLD.fui_general_ledger_tag IS DISTINCT FROM NEW.fui_general_ledger_tag) OR (OLD.eic_general_ledger_tag IS DISTINCT FROM NEW.eic_general_ledger_tag) OR (OLD.backup_w_general_ledger_tag IS DISTINCT FROM NEW.backup_w_general_ledger_tag) OR (OLD.net_pay_general_ledger_tag IS DISTINCT FROM NEW.net_pay_general_ledger_tag) OR (OLD.tax_imp_general_ledger_tag IS DISTINCT FROM NEW.tax_imp_general_ledger_tag) OR (OLD.trust_imp_general_ledger_tag IS DISTINCT FROM NEW.trust_imp_general_ledger_tag) OR (OLD.thrd_p_tax_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_tax_general_ledger_tag) OR (OLD.thrd_p_chk_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_chk_general_ledger_tag) OR (OLD.trust_chk_general_ledger_tag IS DISTINCT FROM NEW.trust_chk_general_ledger_tag) OR (OLD.federal_offset_gl_tag IS DISTINCT FROM NEW.federal_offset_gl_tag) OR (OLD.ee_oasdi_offset_gl_tag IS DISTINCT FROM NEW.ee_oasdi_offset_gl_tag) OR (OLD.er_oasdi_offset_gl_tag IS DISTINCT FROM NEW.er_oasdi_offset_gl_tag) OR (OLD.ee_medicare_offset_gl_tag IS DISTINCT FROM NEW.ee_medicare_offset_gl_tag) OR (OLD.er_medicare_offset_gl_tag IS DISTINCT FROM NEW.er_medicare_offset_gl_tag) OR (OLD.fui_offset_gl_tag IS DISTINCT FROM NEW.fui_offset_gl_tag) OR (OLD.eic_offset_gl_tag IS DISTINCT FROM NEW.eic_offset_gl_tag) OR (OLD.backup_w_offset_gl_tag IS DISTINCT FROM NEW.backup_w_offset_gl_tag) OR (OLD.trust_imp_offset_gl_tag IS DISTINCT FROM NEW.trust_imp_offset_gl_tag) OR (OLD.billing_exp_gl_tag IS DISTINCT FROM NEW.billing_exp_gl_tag) OR (OLD.er_oasdi_exp_gl_tag IS DISTINCT FROM NEW.er_oasdi_exp_gl_tag) OR (OLD.er_medicare_exp_gl_tag IS DISTINCT FROM NEW.er_medicare_exp_gl_tag) OR (OLD.auto_enlist IS DISTINCT FROM NEW.auto_enlist) OR (OLD.calculate_locals_first IS DISTINCT FROM NEW.calculate_locals_first) OR (OLD.weekend_action IS DISTINCT FROM NEW.weekend_action) OR (OLD.last_fui_correction IS DISTINCT FROM NEW.last_fui_correction) OR (OLD.last_sui_correction IS DISTINCT FROM NEW.last_sui_correction) OR (OLD.last_quarter_end_correction IS DISTINCT FROM NEW.last_quarter_end_correction) OR (OLD.last_preprocess IS DISTINCT FROM NEW.last_preprocess) OR (OLD.last_preprocess_message IS DISTINCT FROM NEW.last_preprocess_message) OR (OLD.charge_cobra_admin_fee IS DISTINCT FROM NEW.charge_cobra_admin_fee) OR (OLD.cobra_fee_day_of_month_due IS DISTINCT FROM NEW.cobra_fee_day_of_month_due) OR (OLD.cobra_notification_days IS DISTINCT FROM NEW.cobra_notification_days) OR (OLD.cobra_eligibility_confirm_days IS DISTINCT FROM NEW.cobra_eligibility_confirm_days) OR (OLD.show_rates_on_checks IS DISTINCT FROM NEW.show_rates_on_checks) OR (OLD.show_dir_dep_nbr_on_checks IS DISTINCT FROM NEW.show_dir_dep_nbr_on_checks) OR (OLD.fed_943_tax_deposit_frequency IS DISTINCT FROM NEW.fed_943_tax_deposit_frequency) OR (OLD.impound_workers_comp IS DISTINCT FROM NEW.impound_workers_comp) OR (OLD.reverse_check_printing IS DISTINCT FROM NEW.reverse_check_printing) OR (OLD.lock_date IS DISTINCT FROM NEW.lock_date) OR (OLD.auto_increment IS DISTINCT FROM NEW.auto_increment) OR (OLD.maximum_group_term_life IS DISTINCT FROM NEW.maximum_group_term_life) OR (OLD.payrate_precision IS DISTINCT FROM NEW.payrate_precision) OR (OLD.average_hours IS DISTINCT FROM NEW.average_hours) OR (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) OR (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) OR (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) OR (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) OR (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) OR (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) OR (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) OR (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) OR (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) OR (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) OR (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) OR (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) OR (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) OR (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) OR (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) OR (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) OR (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) OR (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) OR (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) OR (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) OR (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) OR (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) OR (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) OR (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) OR (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) OR (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) OR (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) OR (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) OR (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) OR (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) OR (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) OR (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) OR (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) OR (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) OR (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) OR (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) OR (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) OR (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) OR (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) OR (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) OR (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) OR (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) OR (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) OR (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) OR (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) OR (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) OR (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) OR (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) OR (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) OR (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) OR (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) OR (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) OR (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) OR (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) OR (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) OR (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) OR (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) OR (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) OR (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) OR (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) OR (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) OR (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) OR (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) OR (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days) OR (OLD.annual_form_type IS DISTINCT FROM NEW.annual_form_type) OR (OLD.prenote IS DISTINCT FROM NEW.prenote) OR (OLD.enable_dd IS DISTINCT FROM NEW.enable_dd) OR (OLD.ess_or_ep IS DISTINCT FROM NEW.ess_or_ep) OR (OLD.aca_education_org IS DISTINCT FROM NEW.aca_education_org) OR (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) OR (OLD.aca_default_status IS DISTINCT FROM NEW.aca_default_status) OR (OLD.aca_control_group IS DISTINCT FROM NEW.aca_control_group) OR (OLD.aca_initial_period_from IS DISTINCT FROM NEW.aca_initial_period_from) OR (OLD.aca_initial_period_to IS DISTINCT FROM NEW.aca_initial_period_to) OR (OLD.aca_stability_period_from IS DISTINCT FROM NEW.aca_stability_period_from) OR (OLD.aca_stability_period_to IS DISTINCT FROM NEW.aca_stability_period_to) OR (OLD.aca_use_avg_hours_worked IS DISTINCT FROM NEW.aca_use_avg_hours_worked) OR (OLD.aca_avg_hours_worked_period IS DISTINCT FROM NEW.aca_avg_hours_worked_period) OR (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE co SET 
      custom_company_number = NEW.custom_company_number, county = NEW.county, e_mail_address = NEW.e_mail_address, referred_by = NEW.referred_by, sb_referrals_nbr = NEW.sb_referrals_nbr, start_date = NEW.start_date, termination_notes = NEW.termination_notes, sb_accountant_nbr = NEW.sb_accountant_nbr, accountant_contact = NEW.accountant_contact, union_cl_e_ds_nbr = NEW.union_cl_e_ds_nbr, remote = NEW.remote, business_start_date = NEW.business_start_date, corporation_type = NEW.corporation_type, nature_of_business = NEW.nature_of_business, restaurant = NEW.restaurant, days_open = NEW.days_open, time_open = NEW.time_open, time_close = NEW.time_close, company_notes = NEW.company_notes, successor_company = NEW.successor_company, medical_plan = NEW.medical_plan, retirement_age = NEW.retirement_age, pay_frequencies = NEW.pay_frequencies, general_ledger_format_string = NEW.general_ledger_format_string, cl_billing_nbr = NEW.cl_billing_nbr, dbdt_level = NEW.dbdt_level, billing_level = NEW.billing_level, bank_account_level = NEW.bank_account_level, billing_sb_bank_account_nbr = NEW.billing_sb_bank_account_nbr, tax_sb_bank_account_nbr = NEW.tax_sb_bank_account_nbr, payroll_cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr, debit_number_days_prior_pr = NEW.debit_number_days_prior_pr, tax_cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr, debit_number_of_days_prior_tax = NEW.debit_number_of_days_prior_tax, billing_cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr, debit_number_days_prior_bill = NEW.debit_number_days_prior_bill, dd_cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr, debit_number_of_days_prior_dd = NEW.debit_number_of_days_prior_dd, home_co_states_nbr = NEW.home_co_states_nbr, home_sdi_co_states_nbr = NEW.home_sdi_co_states_nbr, home_sui_co_states_nbr = NEW.home_sui_co_states_nbr, tax_service = NEW.tax_service, tax_service_start_date = NEW.tax_service_start_date, tax_service_end_date = NEW.tax_service_end_date, workers_comp_cl_agency_nbr = NEW.workers_comp_cl_agency_nbr, workers_comp_policy_id = NEW.workers_comp_policy_id, w_comp_cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr, debit_number_days_prior_wc = NEW.debit_number_days_prior_wc, w_comp_sb_bank_account_nbr = NEW.w_comp_sb_bank_account_nbr, eftps_enrollment_status = NEW.eftps_enrollment_status, eftps_enrollment_date = NEW.eftps_enrollment_date, eftps_sequence_number = NEW.eftps_sequence_number, eftps_enrollment_number = NEW.eftps_enrollment_number, eftps_name = NEW.eftps_name, pin_number = NEW.pin_number, ach_sb_bank_account_nbr = NEW.ach_sb_bank_account_nbr, trust_service = NEW.trust_service, trust_sb_account_nbr = NEW.trust_sb_account_nbr, trust_service_start_date = NEW.trust_service_start_date, trust_service_end_date = NEW.trust_service_end_date, obc = NEW.obc, sb_obc_account_nbr = NEW.sb_obc_account_nbr, obc_start_date = NEW.obc_start_date, obc_end_date = NEW.obc_end_date, sy_fed_reporting_agency_nbr = NEW.sy_fed_reporting_agency_nbr, sy_fed_tax_payment_agency_nbr = NEW.sy_fed_tax_payment_agency_nbr, federal_tax_transfer_method = NEW.federal_tax_transfer_method, federal_tax_payment_method = NEW.federal_tax_payment_method, fui_sy_tax_payment_agency = NEW.fui_sy_tax_payment_agency, fui_sy_tax_report_agency = NEW.fui_sy_tax_report_agency, external_tax_export = NEW.external_tax_export, non_profit = NEW.non_profit, primary_sort_field = NEW.primary_sort_field, secondary_sort_field = NEW.secondary_sort_field, fiscal_year_end = NEW.fiscal_year_end, third_party_in_gross_pr_report = NEW.third_party_in_gross_pr_report, sic_code = NEW.sic_code, deductions_to_zero = NEW.deductions_to_zero, autopay_company = NEW.autopay_company, pay_frequency_hourly_default = NEW.pay_frequency_hourly_default, pay_frequency_salary_default = NEW.pay_frequency_salary_default, co_check_primary_sort = NEW.co_check_primary_sort, co_check_secondary_sort = NEW.co_check_secondary_sort, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, annual_cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr, quarter_cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr, smoker_default = NEW.smoker_default, auto_labor_dist_show_deducts = NEW.auto_labor_dist_show_deducts, auto_labor_dist_level = NEW.auto_labor_dist_level, distribute_deductions_default = NEW.distribute_deductions_default, withholding_default = NEW.withholding_default, check_type = NEW.check_type, show_shifts_on_check = NEW.show_shifts_on_check, show_ytd_on_check = NEW.show_ytd_on_check, show_ein_number_on_check = NEW.show_ein_number_on_check, show_ss_number_on_check = NEW.show_ss_number_on_check, time_off_accrual = NEW.time_off_accrual, check_form = NEW.check_form, print_manual_check_stubs = NEW.print_manual_check_stubs, credit_hold = NEW.credit_hold, cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr, payroll_password = NEW.payroll_password, remote_of_client = NEW.remote_of_client, hardware_o_s = NEW.hardware_o_s, network = NEW.network, modem_speed = NEW.modem_speed, modem_connection_type = NEW.modem_connection_type, network_administrator = NEW.network_administrator, network_administrator_phone = NEW.network_administrator_phone, network_admin_phone_type = NEW.network_admin_phone_type, external_network_administrator = NEW.external_network_administrator, transmission_destination = NEW.transmission_destination, last_call_in_date = NEW.last_call_in_date, setup_completed = NEW.setup_completed, first_monthly_payroll_day = NEW.first_monthly_payroll_day, second_monthly_payroll_day = NEW.second_monthly_payroll_day, filler = NEW.filler, collate_checks = NEW.collate_checks, process_priority = NEW.process_priority, check_message = NEW.check_message, customer_service_sb_user_nbr = NEW.customer_service_sb_user_nbr, invoice_notes = NEW.invoice_notes, tax_cover_letter_notes = NEW.tax_cover_letter_notes, final_tax_return = NEW.final_tax_return, general_ledger_tag = NEW.general_ledger_tag, billing_general_ledger_tag = NEW.billing_general_ledger_tag, federal_general_ledger_tag = NEW.federal_general_ledger_tag, ee_oasdi_general_ledger_tag = NEW.ee_oasdi_general_ledger_tag, er_oasdi_general_ledger_tag = NEW.er_oasdi_general_ledger_tag, ee_medicare_general_ledger_tag = NEW.ee_medicare_general_ledger_tag, er_medicare_general_ledger_tag = NEW.er_medicare_general_ledger_tag, fui_general_ledger_tag = NEW.fui_general_ledger_tag, eic_general_ledger_tag = NEW.eic_general_ledger_tag, backup_w_general_ledger_tag = NEW.backup_w_general_ledger_tag, net_pay_general_ledger_tag = NEW.net_pay_general_ledger_tag, tax_imp_general_ledger_tag = NEW.tax_imp_general_ledger_tag, trust_imp_general_ledger_tag = NEW.trust_imp_general_ledger_tag, thrd_p_tax_general_ledger_tag = NEW.thrd_p_tax_general_ledger_tag, thrd_p_chk_general_ledger_tag = NEW.thrd_p_chk_general_ledger_tag, trust_chk_general_ledger_tag = NEW.trust_chk_general_ledger_tag, federal_offset_gl_tag = NEW.federal_offset_gl_tag, ee_oasdi_offset_gl_tag = NEW.ee_oasdi_offset_gl_tag, er_oasdi_offset_gl_tag = NEW.er_oasdi_offset_gl_tag, ee_medicare_offset_gl_tag = NEW.ee_medicare_offset_gl_tag, er_medicare_offset_gl_tag = NEW.er_medicare_offset_gl_tag, fui_offset_gl_tag = NEW.fui_offset_gl_tag, eic_offset_gl_tag = NEW.eic_offset_gl_tag, backup_w_offset_gl_tag = NEW.backup_w_offset_gl_tag, trust_imp_offset_gl_tag = NEW.trust_imp_offset_gl_tag, billing_exp_gl_tag = NEW.billing_exp_gl_tag, er_oasdi_exp_gl_tag = NEW.er_oasdi_exp_gl_tag, er_medicare_exp_gl_tag = NEW.er_medicare_exp_gl_tag, auto_enlist = NEW.auto_enlist, calculate_locals_first = NEW.calculate_locals_first, weekend_action = NEW.weekend_action, last_fui_correction = NEW.last_fui_correction, last_sui_correction = NEW.last_sui_correction, last_quarter_end_correction = NEW.last_quarter_end_correction, last_preprocess = NEW.last_preprocess, last_preprocess_message = NEW.last_preprocess_message, charge_cobra_admin_fee = NEW.charge_cobra_admin_fee, cobra_fee_day_of_month_due = NEW.cobra_fee_day_of_month_due, cobra_notification_days = NEW.cobra_notification_days, cobra_eligibility_confirm_days = NEW.cobra_eligibility_confirm_days, show_rates_on_checks = NEW.show_rates_on_checks, show_dir_dep_nbr_on_checks = NEW.show_dir_dep_nbr_on_checks, fed_943_tax_deposit_frequency = NEW.fed_943_tax_deposit_frequency, impound_workers_comp = NEW.impound_workers_comp, reverse_check_printing = NEW.reverse_check_printing, lock_date = NEW.lock_date, auto_increment = NEW.auto_increment, maximum_group_term_life = NEW.maximum_group_term_life, payrate_precision = NEW.payrate_precision, average_hours = NEW.average_hours, maximum_hours_on_check = NEW.maximum_hours_on_check, maximum_dollars_on_check = NEW.maximum_dollars_on_check, discount_rate = NEW.discount_rate, mod_rate = NEW.mod_rate, minimum_tax_threshold = NEW.minimum_tax_threshold, ss_disability_admin_fee = NEW.ss_disability_admin_fee, check_time_off_avail = NEW.check_time_off_avail, agency_check_mb_group_nbr = NEW.agency_check_mb_group_nbr, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_report_mb_group_nbr = NEW.pr_report_mb_group_nbr, pr_report_second_mb_group_nbr = NEW.pr_report_second_mb_group_nbr, tax_check_mb_group_nbr = NEW.tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr = NEW.tax_return_mb_group_nbr, tax_return_second_mb_group_nbr = NEW.tax_return_second_mb_group_nbr, misc_check_form = NEW.misc_check_form, hold_return_queue = NEW.hold_return_queue, number_of_invoice_copies = NEW.number_of_invoice_copies, prorate_flat_fee_for_dbdt = NEW.prorate_flat_fee_for_dbdt, initial_effective_date = NEW.initial_effective_date, sb_other_service_nbr = NEW.sb_other_service_nbr, break_checks_by_dbdt = NEW.break_checks_by_dbdt, summarize_sui = NEW.summarize_sui, show_shortfall_check = NEW.show_shortfall_check, trust_chk_offset_gl_tag = NEW.trust_chk_offset_gl_tag, manual_cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr, wells_fargo_ach_flag = NEW.wells_fargo_ach_flag, billing_check_cpa = NEW.billing_check_cpa, sales_tax_percentage = NEW.sales_tax_percentage, exclude_r_c_b_0r_n = NEW.exclude_r_c_b_0r_n, calculate_states_first = NEW.calculate_states_first, invoice_discount = NEW.invoice_discount, discount_start_date = NEW.discount_start_date, discount_end_date = NEW.discount_end_date, show_time_clock_punch = NEW.show_time_clock_punch, auto_rd_dflt_cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr, auto_reduction_default_ees = NEW.auto_reduction_default_ees, vt_healthcare_gl_tag = NEW.vt_healthcare_gl_tag, vt_healthcare_offset_gl_tag = NEW.vt_healthcare_offset_gl_tag, ui_rounding_gl_tag = NEW.ui_rounding_gl_tag, ui_rounding_offset_gl_tag = NEW.ui_rounding_offset_gl_tag, reprint_to_balance = NEW.reprint_to_balance, show_manual_checks_in_ess = NEW.show_manual_checks_in_ess, payroll_requires_mgr_approval = NEW.payroll_requires_mgr_approval, days_prior_to_check_date = NEW.days_prior_to_check_date, wc_offs_bank_account_nbr = NEW.wc_offs_bank_account_nbr, qtr_lock_for_tax_pmts = NEW.qtr_lock_for_tax_pmts, co_max_amount_for_payroll = NEW.co_max_amount_for_payroll, co_max_amount_for_tax_impound = NEW.co_max_amount_for_tax_impound, co_max_amount_for_dd = NEW.co_max_amount_for_dd, co_payroll_process_limitations = NEW.co_payroll_process_limitations, co_ach_process_limitations = NEW.co_ach_process_limitations, trust_impound = NEW.trust_impound, tax_impound = NEW.tax_impound, dd_impound = NEW.dd_impound, billing_impound = NEW.billing_impound, wc_impound = NEW.wc_impound, cobra_credit_gl_tag = NEW.cobra_credit_gl_tag, co_exception_payment_type = NEW.co_exception_payment_type, last_tax_return = NEW.last_tax_return, enable_hr = NEW.enable_hr, enable_ess = NEW.enable_ess, duns_and_bradstreet = NEW.duns_and_bradstreet, bank_account_register_name = NEW.bank_account_register_name, enable_benefits = NEW.enable_benefits, enable_time_off = NEW.enable_time_off, wc_fiscal_year_begin = NEW.wc_fiscal_year_begin, show_paystubs_ess_days = NEW.show_paystubs_ess_days, annual_form_type = NEW.annual_form_type, prenote = NEW.prenote, enable_dd = NEW.enable_dd, ess_or_ep = NEW.ess_or_ep, aca_education_org = NEW.aca_education_org, aca_standard_hours = NEW.aca_standard_hours, aca_default_status = NEW.aca_default_status, aca_control_group = NEW.aca_control_group, aca_initial_period_from = NEW.aca_initial_period_from, aca_initial_period_to = NEW.aca_initial_period_to, aca_stability_period_from = NEW.aca_stability_period_from, aca_stability_period_to = NEW.aca_stability_period_to, aca_use_avg_hours_worked = NEW.aca_use_avg_hours_worked, aca_avg_hours_worked_period = NEW.aca_avg_hours_worked_period, aca_add_earn_cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr
      WHERE co_nbr = NEW.co_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_CO_8 FOR CO After Update POSITION 8
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(37, NEW.rec_version, NEW.co_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 492, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2628, OLD.effective_until);
    changes = changes + 1;
  END

  /* CUSTOM_COMPANY_NUMBER */
  IF (OLD.custom_company_number IS DISTINCT FROM NEW.custom_company_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 606, OLD.custom_company_number);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 607, OLD.name);
    changes = changes + 1;
  END

  /* DBA */
  IF (OLD.dba IS DISTINCT FROM NEW.dba) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 608, OLD.dba);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 493, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 494, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 609, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 610, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 524, OLD.zip_code);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 525, OLD.county);
    changes = changes + 1;
  END

  /* LEGAL_NAME */
  IF (OLD.legal_name IS DISTINCT FROM NEW.legal_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 611, OLD.legal_name);
    changes = changes + 1;
  END

  /* LEGAL_ADDRESS1 */
  IF (OLD.legal_address1 IS DISTINCT FROM NEW.legal_address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 495, OLD.legal_address1);
    changes = changes + 1;
  END

  /* LEGAL_ADDRESS2 */
  IF (OLD.legal_address2 IS DISTINCT FROM NEW.legal_address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 496, OLD.legal_address2);
    changes = changes + 1;
  END

  /* LEGAL_CITY */
  IF (OLD.legal_city IS DISTINCT FROM NEW.legal_city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 612, OLD.legal_city);
    changes = changes + 1;
  END

  /* LEGAL_STATE */
  IF (OLD.legal_state IS DISTINCT FROM NEW.legal_state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 613, OLD.legal_state);
    changes = changes + 1;
  END

  /* LEGAL_ZIP_CODE */
  IF (OLD.legal_zip_code IS DISTINCT FROM NEW.legal_zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 526, OLD.legal_zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 614, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* REFERRED_BY */
  IF (OLD.referred_by IS DISTINCT FROM NEW.referred_by) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 615, OLD.referred_by);
    changes = changes + 1;
  END

  /* SB_REFERRALS_NBR */
  IF (OLD.sb_referrals_nbr IS DISTINCT FROM NEW.sb_referrals_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 527, OLD.sb_referrals_nbr);
    changes = changes + 1;
  END

  /* START_DATE */
  IF (OLD.start_date IS DISTINCT FROM NEW.start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 577, OLD.start_date);
    changes = changes + 1;
  END

  /* TERMINATION_DATE */
  IF (OLD.termination_date IS DISTINCT FROM NEW.termination_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 578, OLD.termination_date);
    changes = changes + 1;
  END

  /* TERMINATION_CODE */
  IF (OLD.termination_code IS DISTINCT FROM NEW.termination_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 675, OLD.termination_code);
    changes = changes + 1;
  END

  /* TERMINATION_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TERMINATION_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 497, :blob_nbr);
    changes = changes + 1;
  END

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 528, OLD.sb_accountant_nbr);
    changes = changes + 1;
  END

  /* ACCOUNTANT_CONTACT */
  IF (OLD.accountant_contact IS DISTINCT FROM NEW.accountant_contact) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 616, OLD.accountant_contact);
    changes = changes + 1;
  END

  /* PRINT_CPA */
  IF (OLD.print_cpa IS DISTINCT FROM NEW.print_cpa) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 676, OLD.print_cpa);
    changes = changes + 1;
  END

  /* UNION_CL_E_DS_NBR */
  IF (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 529, OLD.union_cl_e_ds_nbr);
    changes = changes + 1;
  END

  /* CL_COMMON_PAYMASTER_NBR */
  IF (OLD.cl_common_paymaster_nbr IS DISTINCT FROM NEW.cl_common_paymaster_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 530, OLD.cl_common_paymaster_nbr);
    changes = changes + 1;
  END

  /* CL_CO_CONSOLIDATION_NBR */
  IF (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 531, OLD.cl_co_consolidation_nbr);
    changes = changes + 1;
  END

  /* FEIN */
  IF (OLD.fein IS DISTINCT FROM NEW.fein) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 677, OLD.fein);
    changes = changes + 1;
  END

  /* REMOTE */
  IF (OLD.remote IS DISTINCT FROM NEW.remote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 678, OLD.remote);
    changes = changes + 1;
  END

  /* BUSINESS_START_DATE */
  IF (OLD.business_start_date IS DISTINCT FROM NEW.business_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 579, OLD.business_start_date);
    changes = changes + 1;
  END

  /* BUSINESS_TYPE */
  IF (OLD.business_type IS DISTINCT FROM NEW.business_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 679, OLD.business_type);
    changes = changes + 1;
  END

  /* CORPORATION_TYPE */
  IF (OLD.corporation_type IS DISTINCT FROM NEW.corporation_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 680, OLD.corporation_type);
    changes = changes + 1;
  END

  /* NATURE_OF_BUSINESS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 498, :blob_nbr);
    changes = changes + 1;
  END

  /* RESTAURANT */
  IF (OLD.restaurant IS DISTINCT FROM NEW.restaurant) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 681, OLD.restaurant);
    changes = changes + 1;
  END

  /* DAYS_OPEN */
  IF (OLD.days_open IS DISTINCT FROM NEW.days_open) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 532, OLD.days_open);
    changes = changes + 1;
  END

  /* TIME_OPEN */
  IF (OLD.time_open IS DISTINCT FROM NEW.time_open) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 499, OLD.time_open);
    changes = changes + 1;
  END

  /* TIME_CLOSE */
  IF (OLD.time_close IS DISTINCT FROM NEW.time_close) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 500, OLD.time_close);
    changes = changes + 1;
  END

  /* COMPANY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COMPANY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 501, :blob_nbr);
    changes = changes + 1;
  END

  /* SUCCESSOR_COMPANY */
  IF (OLD.successor_company IS DISTINCT FROM NEW.successor_company) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 682, OLD.successor_company);
    changes = changes + 1;
  END

  /* MEDICAL_PLAN */
  IF (OLD.medical_plan IS DISTINCT FROM NEW.medical_plan) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 683, OLD.medical_plan);
    changes = changes + 1;
  END

  /* RETIREMENT_AGE */
  IF (OLD.retirement_age IS DISTINCT FROM NEW.retirement_age) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 534, OLD.retirement_age);
    changes = changes + 1;
  END

  /* PAY_FREQUENCIES */
  IF (OLD.pay_frequencies IS DISTINCT FROM NEW.pay_frequencies) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 684, OLD.pay_frequencies);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_FORMAT_STRING */
  IF (OLD.general_ledger_format_string IS DISTINCT FROM NEW.general_ledger_format_string) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 502, OLD.general_ledger_format_string);
    changes = changes + 1;
  END

  /* CL_BILLING_NBR */
  IF (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 535, OLD.cl_billing_nbr);
    changes = changes + 1;
  END

  /* DBDT_LEVEL */
  IF (OLD.dbdt_level IS DISTINCT FROM NEW.dbdt_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 685, OLD.dbdt_level);
    changes = changes + 1;
  END

  /* BILLING_LEVEL */
  IF (OLD.billing_level IS DISTINCT FROM NEW.billing_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 686, OLD.billing_level);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_LEVEL */
  IF (OLD.bank_account_level IS DISTINCT FROM NEW.bank_account_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 687, OLD.bank_account_level);
    changes = changes + 1;
  END

  /* BILLING_SB_BANK_ACCOUNT_NBR */
  IF (OLD.billing_sb_bank_account_nbr IS DISTINCT FROM NEW.billing_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 536, OLD.billing_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* TAX_SB_BANK_ACCOUNT_NBR */
  IF (OLD.tax_sb_bank_account_nbr IS DISTINCT FROM NEW.tax_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 537, OLD.tax_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* PAYROLL_CL_BANK_ACCOUNT_NBR */
  IF (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 538, OLD.payroll_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_DAYS_PRIOR_PR */
  IF (OLD.debit_number_days_prior_pr IS DISTINCT FROM NEW.debit_number_days_prior_pr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 539, OLD.debit_number_days_prior_pr);
    changes = changes + 1;
  END

  /* TAX_CL_BANK_ACCOUNT_NBR */
  IF (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 540, OLD.tax_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_OF_DAYS_PRIOR_TAX */
  IF (OLD.debit_number_of_days_prior_tax IS DISTINCT FROM NEW.debit_number_of_days_prior_tax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 541, OLD.debit_number_of_days_prior_tax);
    changes = changes + 1;
  END

  /* BILLING_CL_BANK_ACCOUNT_NBR */
  IF (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 542, OLD.billing_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_DAYS_PRIOR_BILL */
  IF (OLD.debit_number_days_prior_bill IS DISTINCT FROM NEW.debit_number_days_prior_bill) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.debit_number_days_prior_bill);
    changes = changes + 1;
  END

  /* DD_CL_BANK_ACCOUNT_NBR */
  IF (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.dd_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_OF_DAYS_PRIOR_DD */
  IF (OLD.debit_number_of_days_prior_dd IS DISTINCT FROM NEW.debit_number_of_days_prior_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 545, OLD.debit_number_of_days_prior_dd);
    changes = changes + 1;
  END

  /* HOME_CO_STATES_NBR */
  IF (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 546, OLD.home_co_states_nbr);
    changes = changes + 1;
  END

  /* HOME_SDI_CO_STATES_NBR */
  IF (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 547, OLD.home_sdi_co_states_nbr);
    changes = changes + 1;
  END

  /* HOME_SUI_CO_STATES_NBR */
  IF (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 548, OLD.home_sui_co_states_nbr);
    changes = changes + 1;
  END

  /* TAX_SERVICE */
  IF (OLD.tax_service IS DISTINCT FROM NEW.tax_service) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 688, OLD.tax_service);
    changes = changes + 1;
  END

  /* TAX_SERVICE_START_DATE */
  IF (OLD.tax_service_start_date IS DISTINCT FROM NEW.tax_service_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 580, OLD.tax_service_start_date);
    changes = changes + 1;
  END

  /* TAX_SERVICE_END_DATE */
  IF (OLD.tax_service_end_date IS DISTINCT FROM NEW.tax_service_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 581, OLD.tax_service_end_date);
    changes = changes + 1;
  END

  /* USE_DBA_ON_TAX_RETURN */
  IF (OLD.use_dba_on_tax_return IS DISTINCT FROM NEW.use_dba_on_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 690, OLD.use_dba_on_tax_return);
    changes = changes + 1;
  END

  /* WORKERS_COMP_CL_AGENCY_NBR */
  IF (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 549, OLD.workers_comp_cl_agency_nbr);
    changes = changes + 1;
  END

  /* WORKERS_COMP_POLICY_ID */
  IF (OLD.workers_comp_policy_id IS DISTINCT FROM NEW.workers_comp_policy_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 617, OLD.workers_comp_policy_id);
    changes = changes + 1;
  END

  /* W_COMP_CL_BANK_ACCOUNT_NBR */
  IF (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, OLD.w_comp_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_DAYS_PRIOR_WC */
  IF (OLD.debit_number_days_prior_wc IS DISTINCT FROM NEW.debit_number_days_prior_wc) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, OLD.debit_number_days_prior_wc);
    changes = changes + 1;
  END

  /* W_COMP_SB_BANK_ACCOUNT_NBR */
  IF (OLD.w_comp_sb_bank_account_nbr IS DISTINCT FROM NEW.w_comp_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 552, OLD.w_comp_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* EFTPS_ENROLLMENT_STATUS */
  IF (OLD.eftps_enrollment_status IS DISTINCT FROM NEW.eftps_enrollment_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 691, OLD.eftps_enrollment_status);
    changes = changes + 1;
  END

  /* EFTPS_ENROLLMENT_DATE */
  IF (OLD.eftps_enrollment_date IS DISTINCT FROM NEW.eftps_enrollment_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 503, OLD.eftps_enrollment_date);
    changes = changes + 1;
  END

  /* EFTPS_SEQUENCE_NUMBER */
  IF (OLD.eftps_sequence_number IS DISTINCT FROM NEW.eftps_sequence_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 618, OLD.eftps_sequence_number);
    changes = changes + 1;
  END

  /* EFTPS_ENROLLMENT_NUMBER */
  IF (OLD.eftps_enrollment_number IS DISTINCT FROM NEW.eftps_enrollment_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 619, OLD.eftps_enrollment_number);
    changes = changes + 1;
  END

  /* EFTPS_NAME */
  IF (OLD.eftps_name IS DISTINCT FROM NEW.eftps_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 620, OLD.eftps_name);
    changes = changes + 1;
  END

  /* PIN_NUMBER */
  IF (OLD.pin_number IS DISTINCT FROM NEW.pin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 553, OLD.pin_number);
    changes = changes + 1;
  END

  /* ACH_SB_BANK_ACCOUNT_NBR */
  IF (OLD.ach_sb_bank_account_nbr IS DISTINCT FROM NEW.ach_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 554, OLD.ach_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* TRUST_SERVICE */
  IF (OLD.trust_service IS DISTINCT FROM NEW.trust_service) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 692, OLD.trust_service);
    changes = changes + 1;
  END

  /* TRUST_SB_ACCOUNT_NBR */
  IF (OLD.trust_sb_account_nbr IS DISTINCT FROM NEW.trust_sb_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 555, OLD.trust_sb_account_nbr);
    changes = changes + 1;
  END

  /* TRUST_SERVICE_START_DATE */
  IF (OLD.trust_service_start_date IS DISTINCT FROM NEW.trust_service_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 582, OLD.trust_service_start_date);
    changes = changes + 1;
  END

  /* TRUST_SERVICE_END_DATE */
  IF (OLD.trust_service_end_date IS DISTINCT FROM NEW.trust_service_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 583, OLD.trust_service_end_date);
    changes = changes + 1;
  END

  /* OBC */
  IF (OLD.obc IS DISTINCT FROM NEW.obc) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 693, OLD.obc);
    changes = changes + 1;
  END

  /* SB_OBC_ACCOUNT_NBR */
  IF (OLD.sb_obc_account_nbr IS DISTINCT FROM NEW.sb_obc_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 556, OLD.sb_obc_account_nbr);
    changes = changes + 1;
  END

  /* OBC_START_DATE */
  IF (OLD.obc_start_date IS DISTINCT FROM NEW.obc_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 584, OLD.obc_start_date);
    changes = changes + 1;
  END

  /* OBC_END_DATE */
  IF (OLD.obc_end_date IS DISTINCT FROM NEW.obc_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 585, OLD.obc_end_date);
    changes = changes + 1;
  END

  /* SY_FED_REPORTING_AGENCY_NBR */
  IF (OLD.sy_fed_reporting_agency_nbr IS DISTINCT FROM NEW.sy_fed_reporting_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 557, OLD.sy_fed_reporting_agency_nbr);
    changes = changes + 1;
  END

  /* SY_FED_TAX_PAYMENT_AGENCY_NBR */
  IF (OLD.sy_fed_tax_payment_agency_nbr IS DISTINCT FROM NEW.sy_fed_tax_payment_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 558, OLD.sy_fed_tax_payment_agency_nbr);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.federal_tax_deposit_frequency IS DISTINCT FROM NEW.federal_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 694, OLD.federal_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_TRANSFER_METHOD */
  IF (OLD.federal_tax_transfer_method IS DISTINCT FROM NEW.federal_tax_transfer_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 695, OLD.federal_tax_transfer_method);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_PAYMENT_METHOD */
  IF (OLD.federal_tax_payment_method IS DISTINCT FROM NEW.federal_tax_payment_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 696, OLD.federal_tax_payment_method);
    changes = changes + 1;
  END

  /* FUI_SY_TAX_PAYMENT_AGENCY */
  IF (OLD.fui_sy_tax_payment_agency IS DISTINCT FROM NEW.fui_sy_tax_payment_agency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 559, OLD.fui_sy_tax_payment_agency);
    changes = changes + 1;
  END

  /* FUI_SY_TAX_REPORT_AGENCY */
  IF (OLD.fui_sy_tax_report_agency IS DISTINCT FROM NEW.fui_sy_tax_report_agency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 560, OLD.fui_sy_tax_report_agency);
    changes = changes + 1;
  END

  /* EXTERNAL_TAX_EXPORT */
  IF (OLD.external_tax_export IS DISTINCT FROM NEW.external_tax_export) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 698, OLD.external_tax_export);
    changes = changes + 1;
  END

  /* NON_PROFIT */
  IF (OLD.non_profit IS DISTINCT FROM NEW.non_profit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 699, OLD.non_profit);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_EXEMPT_STATUS */
  IF (OLD.federal_tax_exempt_status IS DISTINCT FROM NEW.federal_tax_exempt_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 700, OLD.federal_tax_exempt_status);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_EE_OASDI */
  IF (OLD.fed_tax_exempt_ee_oasdi IS DISTINCT FROM NEW.fed_tax_exempt_ee_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 701, OLD.fed_tax_exempt_ee_oasdi);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_ER_OASDI */
  IF (OLD.fed_tax_exempt_er_oasdi IS DISTINCT FROM NEW.fed_tax_exempt_er_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 702, OLD.fed_tax_exempt_er_oasdi);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_EE_MEDICARE */
  IF (OLD.fed_tax_exempt_ee_medicare IS DISTINCT FROM NEW.fed_tax_exempt_ee_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 703, OLD.fed_tax_exempt_ee_medicare);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_ER_MEDICARE */
  IF (OLD.fed_tax_exempt_er_medicare IS DISTINCT FROM NEW.fed_tax_exempt_er_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 704, OLD.fed_tax_exempt_er_medicare);
    changes = changes + 1;
  END

  /* FUI_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.fui_tax_deposit_frequency IS DISTINCT FROM NEW.fui_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 705, OLD.fui_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* FUI_TAX_EXEMPT */
  IF (OLD.fui_tax_exempt IS DISTINCT FROM NEW.fui_tax_exempt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 706, OLD.fui_tax_exempt);
    changes = changes + 1;
  END

  /* PRIMARY_SORT_FIELD */
  IF (OLD.primary_sort_field IS DISTINCT FROM NEW.primary_sort_field) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 707, OLD.primary_sort_field);
    changes = changes + 1;
  END

  /* SECONDARY_SORT_FIELD */
  IF (OLD.secondary_sort_field IS DISTINCT FROM NEW.secondary_sort_field) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 708, OLD.secondary_sort_field);
    changes = changes + 1;
  END

  /* FISCAL_YEAR_END */
  IF (OLD.fiscal_year_end IS DISTINCT FROM NEW.fiscal_year_end) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 561, OLD.fiscal_year_end);
    changes = changes + 1;
  END

  /* THIRD_PARTY_IN_GROSS_PR_REPORT */
  IF (OLD.third_party_in_gross_pr_report IS DISTINCT FROM NEW.third_party_in_gross_pr_report) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 709, OLD.third_party_in_gross_pr_report);
    changes = changes + 1;
  END

  /* SIC_CODE */
  IF (OLD.sic_code IS DISTINCT FROM NEW.sic_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 562, OLD.sic_code);
    changes = changes + 1;
  END

  /* DEDUCTIONS_TO_ZERO */
  IF (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 711, OLD.deductions_to_zero);
    changes = changes + 1;
  END

  /* AUTOPAY_COMPANY */
  IF (OLD.autopay_company IS DISTINCT FROM NEW.autopay_company) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 712, OLD.autopay_company);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY_HOURLY_DEFAULT */
  IF (OLD.pay_frequency_hourly_default IS DISTINCT FROM NEW.pay_frequency_hourly_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 713, OLD.pay_frequency_hourly_default);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY_SALARY_DEFAULT */
  IF (OLD.pay_frequency_salary_default IS DISTINCT FROM NEW.pay_frequency_salary_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 714, OLD.pay_frequency_salary_default);
    changes = changes + 1;
  END

  /* CO_CHECK_PRIMARY_SORT */
  IF (OLD.co_check_primary_sort IS DISTINCT FROM NEW.co_check_primary_sort) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 715, OLD.co_check_primary_sort);
    changes = changes + 1;
  END

  /* CO_CHECK_SECONDARY_SORT */
  IF (OLD.co_check_secondary_sort IS DISTINCT FROM NEW.co_check_secondary_sort) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 716, OLD.co_check_secondary_sort);
    changes = changes + 1;
  END

  /* CL_DELIVERY_GROUP_NBR */
  IF (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 563, OLD.cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* ANNUAL_CL_DELIVERY_GROUP_NBR */
  IF (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 564, OLD.annual_cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* QUARTER_CL_DELIVERY_GROUP_NBR */
  IF (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 565, OLD.quarter_cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* SMOKER_DEFAULT */
  IF (OLD.smoker_default IS DISTINCT FROM NEW.smoker_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 717, OLD.smoker_default);
    changes = changes + 1;
  END

  /* AUTO_LABOR_DIST_SHOW_DEDUCTS */
  IF (OLD.auto_labor_dist_show_deducts IS DISTINCT FROM NEW.auto_labor_dist_show_deducts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 718, OLD.auto_labor_dist_show_deducts);
    changes = changes + 1;
  END

  /* AUTO_LABOR_DIST_LEVEL */
  IF (OLD.auto_labor_dist_level IS DISTINCT FROM NEW.auto_labor_dist_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 719, OLD.auto_labor_dist_level);
    changes = changes + 1;
  END

  /* DISTRIBUTE_DEDUCTIONS_DEFAULT */
  IF (OLD.distribute_deductions_default IS DISTINCT FROM NEW.distribute_deductions_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 720, OLD.distribute_deductions_default);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 566, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* WITHHOLDING_DEFAULT */
  IF (OLD.withholding_default IS DISTINCT FROM NEW.withholding_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 721, OLD.withholding_default);
    changes = changes + 1;
  END

  /* APPLY_MISC_LIMIT_TO_1099 */
  IF (OLD.apply_misc_limit_to_1099 IS DISTINCT FROM NEW.apply_misc_limit_to_1099) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 722, OLD.apply_misc_limit_to_1099);
    changes = changes + 1;
  END

  /* CHECK_TYPE */
  IF (OLD.check_type IS DISTINCT FROM NEW.check_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 723, OLD.check_type);
    changes = changes + 1;
  END

  /* MAKE_UP_TAX_DEDUCT_SHORTFALLS */
  IF (OLD.make_up_tax_deduct_shortfalls IS DISTINCT FROM NEW.make_up_tax_deduct_shortfalls) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 724, OLD.make_up_tax_deduct_shortfalls);
    changes = changes + 1;
  END

  /* SHOW_SHIFTS_ON_CHECK */
  IF (OLD.show_shifts_on_check IS DISTINCT FROM NEW.show_shifts_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 725, OLD.show_shifts_on_check);
    changes = changes + 1;
  END

  /* SHOW_YTD_ON_CHECK */
  IF (OLD.show_ytd_on_check IS DISTINCT FROM NEW.show_ytd_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 726, OLD.show_ytd_on_check);
    changes = changes + 1;
  END

  /* SHOW_EIN_NUMBER_ON_CHECK */
  IF (OLD.show_ein_number_on_check IS DISTINCT FROM NEW.show_ein_number_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 727, OLD.show_ein_number_on_check);
    changes = changes + 1;
  END

  /* SHOW_SS_NUMBER_ON_CHECK */
  IF (OLD.show_ss_number_on_check IS DISTINCT FROM NEW.show_ss_number_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 728, OLD.show_ss_number_on_check);
    changes = changes + 1;
  END

  /* TIME_OFF_ACCRUAL */
  IF (OLD.time_off_accrual IS DISTINCT FROM NEW.time_off_accrual) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 729, OLD.time_off_accrual);
    changes = changes + 1;
  END

  /* CHECK_FORM */
  IF (OLD.check_form IS DISTINCT FROM NEW.check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 730, OLD.check_form);
    changes = changes + 1;
  END

  /* PRINT_MANUAL_CHECK_STUBS */
  IF (OLD.print_manual_check_stubs IS DISTINCT FROM NEW.print_manual_check_stubs) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 731, OLD.print_manual_check_stubs);
    changes = changes + 1;
  END

  /* CREDIT_HOLD */
  IF (OLD.credit_hold IS DISTINCT FROM NEW.credit_hold) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 732, OLD.credit_hold);
    changes = changes + 1;
  END

  /* CL_TIMECLOCK_IMPORTS_NBR */
  IF (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 567, OLD.cl_timeclock_imports_nbr);
    changes = changes + 1;
  END

  /* PAYROLL_PASSWORD */
  IF (OLD.payroll_password IS DISTINCT FROM NEW.payroll_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 568, OLD.payroll_password);
    changes = changes + 1;
  END

  /* REMOTE_OF_CLIENT */
  IF (OLD.remote_of_client IS DISTINCT FROM NEW.remote_of_client) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 733, OLD.remote_of_client);
    changes = changes + 1;
  END

  /* HARDWARE_O_S */
  IF (OLD.hardware_o_s IS DISTINCT FROM NEW.hardware_o_s) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 734, OLD.hardware_o_s);
    changes = changes + 1;
  END

  /* NETWORK */
  IF (OLD.network IS DISTINCT FROM NEW.network) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 735, OLD.network);
    changes = changes + 1;
  END

  /* MODEM_SPEED */
  IF (OLD.modem_speed IS DISTINCT FROM NEW.modem_speed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 736, OLD.modem_speed);
    changes = changes + 1;
  END

  /* MODEM_CONNECTION_TYPE */
  IF (OLD.modem_connection_type IS DISTINCT FROM NEW.modem_connection_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 737, OLD.modem_connection_type);
    changes = changes + 1;
  END

  /* NETWORK_ADMINISTRATOR */
  IF (OLD.network_administrator IS DISTINCT FROM NEW.network_administrator) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 621, OLD.network_administrator);
    changes = changes + 1;
  END

  /* NETWORK_ADMINISTRATOR_PHONE */
  IF (OLD.network_administrator_phone IS DISTINCT FROM NEW.network_administrator_phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 622, OLD.network_administrator_phone);
    changes = changes + 1;
  END

  /* NETWORK_ADMIN_PHONE_TYPE */
  IF (OLD.network_admin_phone_type IS DISTINCT FROM NEW.network_admin_phone_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 738, OLD.network_admin_phone_type);
    changes = changes + 1;
  END

  /* EXTERNAL_NETWORK_ADMINISTRATOR */
  IF (OLD.external_network_administrator IS DISTINCT FROM NEW.external_network_administrator) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 739, OLD.external_network_administrator);
    changes = changes + 1;
  END

  /* TRANSMISSION_DESTINATION */
  IF (OLD.transmission_destination IS DISTINCT FROM NEW.transmission_destination) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 569, OLD.transmission_destination);
    changes = changes + 1;
  END

  /* LAST_CALL_IN_DATE */
  IF (OLD.last_call_in_date IS DISTINCT FROM NEW.last_call_in_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 505, OLD.last_call_in_date);
    changes = changes + 1;
  END

  /* SETUP_COMPLETED */
  IF (OLD.setup_completed IS DISTINCT FROM NEW.setup_completed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 741, OLD.setup_completed);
    changes = changes + 1;
  END

  /* FIRST_MONTHLY_PAYROLL_DAY */
  IF (OLD.first_monthly_payroll_day IS DISTINCT FROM NEW.first_monthly_payroll_day) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 570, OLD.first_monthly_payroll_day);
    changes = changes + 1;
  END

  /* SECOND_MONTHLY_PAYROLL_DAY */
  IF (OLD.second_monthly_payroll_day IS DISTINCT FROM NEW.second_monthly_payroll_day) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 571, OLD.second_monthly_payroll_day);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 624, OLD.filler);
    changes = changes + 1;
  END

  /* COLLATE_CHECKS */
  IF (OLD.collate_checks IS DISTINCT FROM NEW.collate_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 742, OLD.collate_checks);
    changes = changes + 1;
  END

  /* PROCESS_PRIORITY */
  IF (OLD.process_priority IS DISTINCT FROM NEW.process_priority) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 572, OLD.process_priority);
    changes = changes + 1;
  END

  /* CHECK_MESSAGE */
  IF (OLD.check_message IS DISTINCT FROM NEW.check_message) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 625, OLD.check_message);
    changes = changes + 1;
  END

  /* CUSTOMER_SERVICE_SB_USER_NBR */
  IF (OLD.customer_service_sb_user_nbr IS DISTINCT FROM NEW.customer_service_sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 573, OLD.customer_service_sb_user_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 506, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 507, :blob_nbr);
    changes = changes + 1;
  END

  /* FINAL_TAX_RETURN */
  IF (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 674, OLD.final_tax_return);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_TAG */
  IF (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 626, OLD.general_ledger_tag);
    changes = changes + 1;
  END

  /* BILLING_GENERAL_LEDGER_TAG */
  IF (OLD.billing_general_ledger_tag IS DISTINCT FROM NEW.billing_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 627, OLD.billing_general_ledger_tag);
    changes = changes + 1;
  END

  /* FEDERAL_GENERAL_LEDGER_TAG */
  IF (OLD.federal_general_ledger_tag IS DISTINCT FROM NEW.federal_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 628, OLD.federal_general_ledger_tag);
    changes = changes + 1;
  END

  /* EE_OASDI_GENERAL_LEDGER_TAG */
  IF (OLD.ee_oasdi_general_ledger_tag IS DISTINCT FROM NEW.ee_oasdi_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 629, OLD.ee_oasdi_general_ledger_tag);
    changes = changes + 1;
  END

  /* ER_OASDI_GENERAL_LEDGER_TAG */
  IF (OLD.er_oasdi_general_ledger_tag IS DISTINCT FROM NEW.er_oasdi_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 630, OLD.er_oasdi_general_ledger_tag);
    changes = changes + 1;
  END

  /* EE_MEDICARE_GENERAL_LEDGER_TAG */
  IF (OLD.ee_medicare_general_ledger_tag IS DISTINCT FROM NEW.ee_medicare_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 631, OLD.ee_medicare_general_ledger_tag);
    changes = changes + 1;
  END

  /* ER_MEDICARE_GENERAL_LEDGER_TAG */
  IF (OLD.er_medicare_general_ledger_tag IS DISTINCT FROM NEW.er_medicare_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 632, OLD.er_medicare_general_ledger_tag);
    changes = changes + 1;
  END

  /* FUI_GENERAL_LEDGER_TAG */
  IF (OLD.fui_general_ledger_tag IS DISTINCT FROM NEW.fui_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 633, OLD.fui_general_ledger_tag);
    changes = changes + 1;
  END

  /* EIC_GENERAL_LEDGER_TAG */
  IF (OLD.eic_general_ledger_tag IS DISTINCT FROM NEW.eic_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 634, OLD.eic_general_ledger_tag);
    changes = changes + 1;
  END

  /* BACKUP_W_GENERAL_LEDGER_TAG */
  IF (OLD.backup_w_general_ledger_tag IS DISTINCT FROM NEW.backup_w_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 635, OLD.backup_w_general_ledger_tag);
    changes = changes + 1;
  END

  /* NET_PAY_GENERAL_LEDGER_TAG */
  IF (OLD.net_pay_general_ledger_tag IS DISTINCT FROM NEW.net_pay_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 636, OLD.net_pay_general_ledger_tag);
    changes = changes + 1;
  END

  /* TAX_IMP_GENERAL_LEDGER_TAG */
  IF (OLD.tax_imp_general_ledger_tag IS DISTINCT FROM NEW.tax_imp_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 637, OLD.tax_imp_general_ledger_tag);
    changes = changes + 1;
  END

  /* TRUST_IMP_GENERAL_LEDGER_TAG */
  IF (OLD.trust_imp_general_ledger_tag IS DISTINCT FROM NEW.trust_imp_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 638, OLD.trust_imp_general_ledger_tag);
    changes = changes + 1;
  END

  /* THRD_P_TAX_GENERAL_LEDGER_TAG */
  IF (OLD.thrd_p_tax_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_tax_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 639, OLD.thrd_p_tax_general_ledger_tag);
    changes = changes + 1;
  END

  /* THRD_P_CHK_GENERAL_LEDGER_TAG */
  IF (OLD.thrd_p_chk_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_chk_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 640, OLD.thrd_p_chk_general_ledger_tag);
    changes = changes + 1;
  END

  /* TRUST_CHK_GENERAL_LEDGER_TAG */
  IF (OLD.trust_chk_general_ledger_tag IS DISTINCT FROM NEW.trust_chk_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 641, OLD.trust_chk_general_ledger_tag);
    changes = changes + 1;
  END

  /* FEDERAL_OFFSET_GL_TAG */
  IF (OLD.federal_offset_gl_tag IS DISTINCT FROM NEW.federal_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 642, OLD.federal_offset_gl_tag);
    changes = changes + 1;
  END

  /* EE_OASDI_OFFSET_GL_TAG */
  IF (OLD.ee_oasdi_offset_gl_tag IS DISTINCT FROM NEW.ee_oasdi_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 643, OLD.ee_oasdi_offset_gl_tag);
    changes = changes + 1;
  END

  /* ER_OASDI_OFFSET_GL_TAG */
  IF (OLD.er_oasdi_offset_gl_tag IS DISTINCT FROM NEW.er_oasdi_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 644, OLD.er_oasdi_offset_gl_tag);
    changes = changes + 1;
  END

  /* EE_MEDICARE_OFFSET_GL_TAG */
  IF (OLD.ee_medicare_offset_gl_tag IS DISTINCT FROM NEW.ee_medicare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 645, OLD.ee_medicare_offset_gl_tag);
    changes = changes + 1;
  END

  /* ER_MEDICARE_OFFSET_GL_TAG */
  IF (OLD.er_medicare_offset_gl_tag IS DISTINCT FROM NEW.er_medicare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 646, OLD.er_medicare_offset_gl_tag);
    changes = changes + 1;
  END

  /* FUI_OFFSET_GL_TAG */
  IF (OLD.fui_offset_gl_tag IS DISTINCT FROM NEW.fui_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 647, OLD.fui_offset_gl_tag);
    changes = changes + 1;
  END

  /* EIC_OFFSET_GL_TAG */
  IF (OLD.eic_offset_gl_tag IS DISTINCT FROM NEW.eic_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 648, OLD.eic_offset_gl_tag);
    changes = changes + 1;
  END

  /* BACKUP_W_OFFSET_GL_TAG */
  IF (OLD.backup_w_offset_gl_tag IS DISTINCT FROM NEW.backup_w_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 649, OLD.backup_w_offset_gl_tag);
    changes = changes + 1;
  END

  /* TRUST_IMP_OFFSET_GL_TAG */
  IF (OLD.trust_imp_offset_gl_tag IS DISTINCT FROM NEW.trust_imp_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 650, OLD.trust_imp_offset_gl_tag);
    changes = changes + 1;
  END

  /* BILLING_EXP_GL_TAG */
  IF (OLD.billing_exp_gl_tag IS DISTINCT FROM NEW.billing_exp_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 651, OLD.billing_exp_gl_tag);
    changes = changes + 1;
  END

  /* ER_OASDI_EXP_GL_TAG */
  IF (OLD.er_oasdi_exp_gl_tag IS DISTINCT FROM NEW.er_oasdi_exp_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 652, OLD.er_oasdi_exp_gl_tag);
    changes = changes + 1;
  END

  /* ER_MEDICARE_EXP_GL_TAG */
  IF (OLD.er_medicare_exp_gl_tag IS DISTINCT FROM NEW.er_medicare_exp_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 653, OLD.er_medicare_exp_gl_tag);
    changes = changes + 1;
  END

  /* AUTO_ENLIST */
  IF (OLD.auto_enlist IS DISTINCT FROM NEW.auto_enlist) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 743, OLD.auto_enlist);
    changes = changes + 1;
  END

  /* CALCULATE_LOCALS_FIRST */
  IF (OLD.calculate_locals_first IS DISTINCT FROM NEW.calculate_locals_first) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 744, OLD.calculate_locals_first);
    changes = changes + 1;
  END

  /* WEEKEND_ACTION */
  IF (OLD.weekend_action IS DISTINCT FROM NEW.weekend_action) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 747, OLD.weekend_action);
    changes = changes + 1;
  END

  /* LAST_FUI_CORRECTION */
  IF (OLD.last_fui_correction IS DISTINCT FROM NEW.last_fui_correction) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 508, OLD.last_fui_correction);
    changes = changes + 1;
  END

  /* LAST_SUI_CORRECTION */
  IF (OLD.last_sui_correction IS DISTINCT FROM NEW.last_sui_correction) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 509, OLD.last_sui_correction);
    changes = changes + 1;
  END

  /* LAST_QUARTER_END_CORRECTION */
  IF (OLD.last_quarter_end_correction IS DISTINCT FROM NEW.last_quarter_end_correction) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 510, OLD.last_quarter_end_correction);
    changes = changes + 1;
  END

  /* LAST_PREPROCESS */
  IF (OLD.last_preprocess IS DISTINCT FROM NEW.last_preprocess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 511, OLD.last_preprocess);
    changes = changes + 1;
  END

  /* LAST_PREPROCESS_MESSAGE */
  IF (OLD.last_preprocess_message IS DISTINCT FROM NEW.last_preprocess_message) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 655, OLD.last_preprocess_message);
    changes = changes + 1;
  END

  /* CHARGE_COBRA_ADMIN_FEE */
  IF (OLD.charge_cobra_admin_fee IS DISTINCT FROM NEW.charge_cobra_admin_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 748, OLD.charge_cobra_admin_fee);
    changes = changes + 1;
  END

  /* COBRA_FEE_DAY_OF_MONTH_DUE */
  IF (OLD.cobra_fee_day_of_month_due IS DISTINCT FROM NEW.cobra_fee_day_of_month_due) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 574, OLD.cobra_fee_day_of_month_due);
    changes = changes + 1;
  END

  /* COBRA_NOTIFICATION_DAYS */
  IF (OLD.cobra_notification_days IS DISTINCT FROM NEW.cobra_notification_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 575, OLD.cobra_notification_days);
    changes = changes + 1;
  END

  /* COBRA_ELIGIBILITY_CONFIRM_DAYS */
  IF (OLD.cobra_eligibility_confirm_days IS DISTINCT FROM NEW.cobra_eligibility_confirm_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 576, OLD.cobra_eligibility_confirm_days);
    changes = changes + 1;
  END

  /* SHOW_RATES_ON_CHECKS */
  IF (OLD.show_rates_on_checks IS DISTINCT FROM NEW.show_rates_on_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 749, OLD.show_rates_on_checks);
    changes = changes + 1;
  END

  /* SHOW_DIR_DEP_NBR_ON_CHECKS */
  IF (OLD.show_dir_dep_nbr_on_checks IS DISTINCT FROM NEW.show_dir_dep_nbr_on_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 750, OLD.show_dir_dep_nbr_on_checks);
    changes = changes + 1;
  END

  /* FED_943_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.fed_943_tax_deposit_frequency IS DISTINCT FROM NEW.fed_943_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 672, OLD.fed_943_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* FED_945_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.fed_945_tax_deposit_frequency IS DISTINCT FROM NEW.fed_945_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 671, OLD.fed_945_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* IMPOUND_WORKERS_COMP */
  IF (OLD.impound_workers_comp IS DISTINCT FROM NEW.impound_workers_comp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 670, OLD.impound_workers_comp);
    changes = changes + 1;
  END

  /* REVERSE_CHECK_PRINTING */
  IF (OLD.reverse_check_printing IS DISTINCT FROM NEW.reverse_check_printing) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 668, OLD.reverse_check_printing);
    changes = changes + 1;
  END

  /* LOCK_DATE */
  IF (OLD.lock_date IS DISTINCT FROM NEW.lock_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 522, OLD.lock_date);
    changes = changes + 1;
  END

  /* FUI_RATE_OVERRIDE */
  IF (OLD.fui_rate_override IS DISTINCT FROM NEW.fui_rate_override) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 490, OLD.fui_rate_override);
    changes = changes + 1;
  END

  /* AUTO_INCREMENT */
  IF (OLD.auto_increment IS DISTINCT FROM NEW.auto_increment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 489, OLD.auto_increment);
    changes = changes + 1;
  END

  /* MAXIMUM_GROUP_TERM_LIFE */
  IF (OLD.maximum_group_term_life IS DISTINCT FROM NEW.maximum_group_term_life) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 488, OLD.maximum_group_term_life);
    changes = changes + 1;
  END

  /* PAYRATE_PRECISION */
  IF (OLD.payrate_precision IS DISTINCT FROM NEW.payrate_precision) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 487, OLD.payrate_precision);
    changes = changes + 1;
  END


  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', table_change_nbr);
  rdb$set_context('USER_TRANSACTION', '@CHANGES', changes);
END

^


CREATE TRIGGER T_AU_CO_9 FOR CO After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  table_change_nbr = rdb$get_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR');
  IF (table_change_nbr IS NULL) THEN
    EXIT;

  changes = rdb$get_context('USER_TRANSACTION', '@CHANGES');

  /* AVERAGE_HOURS */
  IF (OLD.average_hours IS DISTINCT FROM NEW.average_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 486, OLD.average_hours);
    changes = changes + 1;
  END

  /* MAXIMUM_HOURS_ON_CHECK */
  IF (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 485, OLD.maximum_hours_on_check);
    changes = changes + 1;
  END

  /* MAXIMUM_DOLLARS_ON_CHECK */
  IF (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 484, OLD.maximum_dollars_on_check);
    changes = changes + 1;
  END

  /* DISCOUNT_RATE */
  IF (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 483, OLD.discount_rate);
    changes = changes + 1;
  END

  /* MOD_RATE */
  IF (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.mod_rate);
    changes = changes + 1;
  END

  /* MINIMUM_TAX_THRESHOLD */
  IF (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 481, OLD.minimum_tax_threshold);
    changes = changes + 1;
  END

  /* SS_DISABILITY_ADMIN_FEE */
  IF (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 480, OLD.ss_disability_admin_fee);
    changes = changes + 1;
  END

  /* CHECK_TIME_OFF_AVAIL */
  IF (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 751, OLD.check_time_off_avail);
    changes = changes + 1;
  END

  /* AGENCY_CHECK_MB_GROUP_NBR */
  IF (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 586, OLD.agency_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 587, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 588, OLD.pr_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_REPORT_SECOND_MB_GROUP_NBR */
  IF (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 589, OLD.pr_report_second_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_CHECK_MB_GROUP_NBR */
  IF (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 590, OLD.tax_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 591, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 592, OLD.tax_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_SECOND_MB_GROUP_NBR */
  IF (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 593, OLD.tax_return_second_mb_group_nbr);
    changes = changes + 1;
  END

  /* NAME_ON_INVOICE */
  IF (OLD.name_on_invoice IS DISTINCT FROM NEW.name_on_invoice) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 754, OLD.name_on_invoice);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 755, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* HOLD_RETURN_QUEUE */
  IF (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 756, OLD.hold_return_queue);
    changes = changes + 1;
  END

  /* NUMBER_OF_INVOICE_COPIES */
  IF (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 594, OLD.number_of_invoice_copies);
    changes = changes + 1;
  END

  /* PRORATE_FLAT_FEE_FOR_DBDT */
  IF (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 757, OLD.prorate_flat_fee_for_dbdt);
    changes = changes + 1;
  END

  /* INITIAL_EFFECTIVE_DATE */
  IF (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 595, OLD.initial_effective_date);
    changes = changes + 1;
  END

  /* SB_OTHER_SERVICE_NBR */
  IF (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 596, OLD.sb_other_service_nbr);
    changes = changes + 1;
  END

  /* BREAK_CHECKS_BY_DBDT */
  IF (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 758, OLD.break_checks_by_dbdt);
    changes = changes + 1;
  END

  /* SUMMARIZE_SUI */
  IF (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 759, OLD.summarize_sui);
    changes = changes + 1;
  END

  /* SHOW_SHORTFALL_CHECK */
  IF (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 760, OLD.show_shortfall_check);
    changes = changes + 1;
  END

  /* TRUST_CHK_OFFSET_GL_TAG */
  IF (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 656, OLD.trust_chk_offset_gl_tag);
    changes = changes + 1;
  END

  /* MANUAL_CL_BANK_ACCOUNT_NBR */
  IF (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 597, OLD.manual_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* WELLS_FARGO_ACH_FLAG */
  IF (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 657, OLD.wells_fargo_ach_flag);
    changes = changes + 1;
  END

  /* BILLING_CHECK_CPA */
  IF (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 761, OLD.billing_check_cpa);
    changes = changes + 1;
  END

  /* SALES_TAX_PERCENTAGE */
  IF (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 512, OLD.sales_tax_percentage);
    changes = changes + 1;
  END

  /* EXCLUDE_R_C_B_0R_N */
  IF (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 667, OLD.exclude_r_c_b_0r_n);
    changes = changes + 1;
  END

  /* CALCULATE_STATES_FIRST */
  IF (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 666, OLD.calculate_states_first);
    changes = changes + 1;
  END

  /* INVOICE_DISCOUNT */
  IF (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 479, OLD.invoice_discount);
    changes = changes + 1;
  END

  /* DISCOUNT_START_DATE */
  IF (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 521, OLD.discount_start_date);
    changes = changes + 1;
  END

  /* DISCOUNT_END_DATE */
  IF (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 520, OLD.discount_end_date);
    changes = changes + 1;
  END

  /* SHOW_TIME_CLOCK_PUNCH */
  IF (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 664, OLD.show_time_clock_punch);
    changes = changes + 1;
  END

  /* AUTO_RD_DFLT_CL_E_D_GROUPS_NBR */
  IF (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 598, OLD.auto_rd_dflt_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* AUTO_REDUCTION_DEFAULT_EES */
  IF (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 762, OLD.auto_reduction_default_ees);
    changes = changes + 1;
  END

  /* VT_HEALTHCARE_GL_TAG */
  IF (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 659, OLD.vt_healthcare_gl_tag);
    changes = changes + 1;
  END

  /* VT_HEALTHCARE_OFFSET_GL_TAG */
  IF (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 660, OLD.vt_healthcare_offset_gl_tag);
    changes = changes + 1;
  END

  /* UI_ROUNDING_GL_TAG */
  IF (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 661, OLD.ui_rounding_gl_tag);
    changes = changes + 1;
  END

  /* UI_ROUNDING_OFFSET_GL_TAG */
  IF (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 662, OLD.ui_rounding_offset_gl_tag);
    changes = changes + 1;
  END

  /* REPRINT_TO_BALANCE */
  IF (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 763, OLD.reprint_to_balance);
    changes = changes + 1;
  END

  /* SHOW_MANUAL_CHECKS_IN_ESS */
  IF (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 765, OLD.show_manual_checks_in_ess);
    changes = changes + 1;
  END

  /* PAYROLL_REQUIRES_MGR_APPROVAL */
  IF (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 766, OLD.payroll_requires_mgr_approval);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 599, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* WC_OFFS_BANK_ACCOUNT_NBR */
  IF (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 603, OLD.wc_offs_bank_account_nbr);
    changes = changes + 1;
  END

  /* QTR_LOCK_FOR_TAX_PMTS */
  IF (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 767, OLD.qtr_lock_for_tax_pmts);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_PAYROLL */
  IF (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 515, OLD.co_max_amount_for_payroll);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_TAX_IMPOUND */
  IF (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 516, OLD.co_max_amount_for_tax_impound);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_DD */
  IF (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 517, OLD.co_max_amount_for_dd);
    changes = changes + 1;
  END

  /* CO_PAYROLL_PROCESS_LIMITATIONS */
  IF (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 768, OLD.co_payroll_process_limitations);
    changes = changes + 1;
  END

  /* CO_ACH_PROCESS_LIMITATIONS */
  IF (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 769, OLD.co_ach_process_limitations);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 770, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 771, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 772, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 773, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 774, OLD.wc_impound);
    changes = changes + 1;
  END

  /* COBRA_CREDIT_GL_TAG */
  IF (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 663, OLD.cobra_credit_gl_tag);
    changes = changes + 1;
  END

  /* CO_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 775, OLD.co_exception_payment_type);
    changes = changes + 1;
  END

  /* LAST_TAX_RETURN */
  IF (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 776, OLD.last_tax_return);
    changes = changes + 1;
  END

  /* ENABLE_HR */
  IF (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 764, OLD.enable_hr);
    changes = changes + 1;
  END

  /* ENABLE_ESS */
  IF (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 777, OLD.enable_ess);
    changes = changes + 1;
  END

  /* DUNS_AND_BRADSTREET */
  IF (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 519, OLD.duns_and_bradstreet);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_REGISTER_NAME */
  IF (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 780, OLD.bank_account_register_name);
    changes = changes + 1;
  END

  /* ENABLE_BENEFITS */
  IF (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2883, OLD.enable_benefits);
    changes = changes + 1;
  END

  /* ENABLE_TIME_OFF */
  IF (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2884, OLD.enable_time_off);
    changes = changes + 1;
  END

  /* EMPLOYER_TYPE */
  IF (OLD.employer_type IS DISTINCT FROM NEW.employer_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2885, OLD.employer_type);
    changes = changes + 1;
  END

  /* WC_FISCAL_YEAR_BEGIN */
  IF (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2886, OLD.wc_fiscal_year_begin);
    changes = changes + 1;
  END

  /* SHOW_PAYSTUBS_ESS_DAYS */
  IF (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2888, OLD.show_paystubs_ess_days);
    changes = changes + 1;
  END

  /* CO_LOCATIONS_NBR */
  IF (OLD.co_locations_nbr IS DISTINCT FROM NEW.co_locations_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3169, OLD.co_locations_nbr);
    changes = changes + 1;
  END

  /* ANNUAL_FORM_TYPE */
  IF (OLD.annual_form_type IS DISTINCT FROM NEW.annual_form_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3200, OLD.annual_form_type);
    changes = changes + 1;
  END

  /* PRENOTE */
  IF (OLD.prenote IS DISTINCT FROM NEW.prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3201, OLD.prenote);
    changes = changes + 1;
  END

  /* ENABLE_DD */
  IF (OLD.enable_dd IS DISTINCT FROM NEW.enable_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3202, OLD.enable_dd);
    changes = changes + 1;
  END

  /* ESS_OR_EP */
  IF (OLD.ess_or_ep IS DISTINCT FROM NEW.ess_or_ep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3203, OLD.ess_or_ep);
    changes = changes + 1;
  END

  /* ACA_EDUCATION_ORG */
  IF (OLD.aca_education_org IS DISTINCT FROM NEW.aca_education_org) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3204, OLD.aca_education_org);
    changes = changes + 1;
  END

  /* ACA_STANDARD_HOURS */
  IF (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3205, OLD.aca_standard_hours);
    changes = changes + 1;
  END

  /* ACA_DEFAULT_STATUS */
  IF (OLD.aca_default_status IS DISTINCT FROM NEW.aca_default_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3206, OLD.aca_default_status);
    changes = changes + 1;
  END

  /* ACA_CONTROL_GROUP */
  IF (OLD.aca_control_group IS DISTINCT FROM NEW.aca_control_group) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3207, OLD.aca_control_group);
    changes = changes + 1;
  END

  /* ACA_INITIAL_PERIOD_FROM */
  IF (OLD.aca_initial_period_from IS DISTINCT FROM NEW.aca_initial_period_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3208, OLD.aca_initial_period_from);
    changes = changes + 1;
  END

  /* ACA_INITIAL_PERIOD_TO */
  IF (OLD.aca_initial_period_to IS DISTINCT FROM NEW.aca_initial_period_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3209, OLD.aca_initial_period_to);
    changes = changes + 1;
  END

  /* ACA_STABILITY_PERIOD_FROM */
  IF (OLD.aca_stability_period_from IS DISTINCT FROM NEW.aca_stability_period_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3210, OLD.aca_stability_period_from);
    changes = changes + 1;
  END

  /* ACA_STABILITY_PERIOD_TO */
  IF (OLD.aca_stability_period_to IS DISTINCT FROM NEW.aca_stability_period_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3211, OLD.aca_stability_period_to);
    changes = changes + 1;
  END

  /* ACA_USE_AVG_HOURS_WORKED */
  IF (OLD.aca_use_avg_hours_worked IS DISTINCT FROM NEW.aca_use_avg_hours_worked) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3212, OLD.aca_use_avg_hours_worked);
    changes = changes + 1;
  END

  /* ACA_AVG_HOURS_WORKED_PERIOD */
  IF (OLD.aca_avg_hours_worked_period IS DISTINCT FROM NEW.aca_avg_hours_worked_period) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3213, OLD.aca_avg_hours_worked_period);
    changes = changes + 1;
  END

  /* ACA_ADD_EARN_CL_E_D_GROUPS_NBR */
  IF (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3227, OLD.aca_add_earn_cl_e_d_groups_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', NULL);
  rdb$set_context('USER_TRANSACTION', '@CHANGES', NULL);
END

^

CREATE TRIGGER T_BIU_CO_2 FOR CO Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CL_COMMON_PAYMASTER */
  IF ((NEW.cl_common_paymaster_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_common_paymaster_nbr IS DISTINCT FROM NEW.cl_common_paymaster_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_common_paymaster
    WHERE cl_common_paymaster_nbr = NEW.cl_common_paymaster_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_common_paymaster_nbr', NEW.cl_common_paymaster_nbr, 'cl_common_paymaster', NEW.effective_date);

    SELECT rec_version FROM cl_common_paymaster
    WHERE cl_common_paymaster_nbr = NEW.cl_common_paymaster_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_common_paymaster_nbr', NEW.cl_common_paymaster_nbr, 'cl_common_paymaster', NEW.effective_until - 1);
  END

  /* Check reference to CL_AGENCY */
  IF ((NEW.workers_comp_cl_agency_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.workers_comp_cl_agency_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'workers_comp_cl_agency_nbr', NEW.workers_comp_cl_agency_nbr, 'cl_agency', NEW.effective_date);

    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.workers_comp_cl_agency_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'workers_comp_cl_agency_nbr', NEW.workers_comp_cl_agency_nbr, 'cl_agency', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_delivery_group_nbr', NEW.cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_delivery_group_nbr', NEW.cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.annual_cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'annual_cl_delivery_group_nbr', NEW.annual_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'annual_cl_delivery_group_nbr', NEW.annual_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_TIMECLOCK_IMPORTS */
  IF ((NEW.cl_timeclock_imports_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_timeclock_imports
    WHERE cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_timeclock_imports_nbr', NEW.cl_timeclock_imports_nbr, 'cl_timeclock_imports', NEW.effective_date);

    SELECT rec_version FROM cl_timeclock_imports
    WHERE cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_timeclock_imports_nbr', NEW.cl_timeclock_imports_nbr, 'cl_timeclock_imports', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.quarter_cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'quarter_cl_delivery_group_nbr', NEW.quarter_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'quarter_cl_delivery_group_nbr', NEW.quarter_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.agency_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.agency_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'agency_check_mb_group_nbr', NEW.agency_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.agency_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'agency_check_mb_group_nbr', NEW.agency_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_check_mb_group_nbr', NEW.pr_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_check_mb_group_nbr', NEW.pr_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_report_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_mb_group_nbr', NEW.pr_report_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_mb_group_nbr', NEW.pr_report_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_report_second_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_second_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_second_mb_group_nbr', NEW.pr_report_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_second_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_second_mb_group_nbr', NEW.pr_report_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_return_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_mb_group_nbr', NEW.tax_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_mb_group_nbr', NEW.tax_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_ee_return_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_ee_return_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_ee_return_mb_group_nbr', NEW.tax_ee_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_ee_return_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_ee_return_mb_group_nbr', NEW.tax_ee_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_return_second_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_second_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_second_mb_group_nbr', NEW.tax_return_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_second_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_second_mb_group_nbr', NEW.tax_return_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_BILLING */
  IF ((NEW.cl_billing_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_billing
    WHERE cl_billing_nbr = NEW.cl_billing_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_billing_nbr', NEW.cl_billing_nbr, 'cl_billing', NEW.effective_date);

    SELECT rec_version FROM cl_billing
    WHERE cl_billing_nbr = NEW.cl_billing_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_billing_nbr', NEW.cl_billing_nbr, 'cl_billing', NEW.effective_until - 1);
  END

  /* Check reference to CO_WORKERS_COMP */
  IF ((NEW.co_workers_comp_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_workers_comp_nbr', NEW.co_workers_comp_nbr, 'co_workers_comp', NEW.effective_date);

    SELECT rec_version FROM co_workers_comp
    WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_workers_comp_nbr', NEW.co_workers_comp_nbr, 'co_workers_comp', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.manual_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'manual_cl_bank_account_nbr', NEW.manual_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'manual_cl_bank_account_nbr', NEW.manual_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.billing_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'billing_cl_bank_account_nbr', NEW.billing_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'billing_cl_bank_account_nbr', NEW.billing_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.tax_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_cl_bank_account_nbr', NEW.tax_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_cl_bank_account_nbr', NEW.tax_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.w_comp_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'w_comp_cl_bank_account_nbr', NEW.w_comp_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'w_comp_cl_bank_account_nbr', NEW.w_comp_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.payroll_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'payroll_cl_bank_account_nbr', NEW.payroll_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'payroll_cl_bank_account_nbr', NEW.payroll_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.dd_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'dd_cl_bank_account_nbr', NEW.dd_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'dd_cl_bank_account_nbr', NEW.dd_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_CO_CONSOLIDATION */
  IF ((NEW.cl_co_consolidation_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_co_consolidation_nbr', NEW.cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_date);

    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_co_consolidation_nbr', NEW.cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_DS */
  IF ((NEW.union_cl_e_ds_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.union_cl_e_ds_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'union_cl_e_ds_nbr', NEW.union_cl_e_ds_nbr, 'cl_e_ds', NEW.effective_date);

    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.union_cl_e_ds_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'union_cl_e_ds_nbr', NEW.union_cl_e_ds_nbr, 'cl_e_ds', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.auto_rd_dflt_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'auto_rd_dflt_cl_e_d_groups_nbr', NEW.auto_rd_dflt_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'auto_rd_dflt_cl_e_d_groups_nbr', NEW.auto_rd_dflt_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CO_STATES */
  IF ((NEW.home_co_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_co_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_co_states_nbr', NEW.home_co_states_nbr, 'co_states', NEW.effective_date);

    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_co_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_co_states_nbr', NEW.home_co_states_nbr, 'co_states', NEW.effective_until - 1);
  END

  /* Check reference to CO_STATES */
  IF ((NEW.home_sdi_co_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sdi_co_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sdi_co_states_nbr', NEW.home_sdi_co_states_nbr, 'co_states', NEW.effective_date);

    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sdi_co_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sdi_co_states_nbr', NEW.home_sdi_co_states_nbr, 'co_states', NEW.effective_until - 1);
  END

  /* Check reference to CO_STATES */
  IF ((NEW.home_sui_co_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sui_co_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sui_co_states_nbr', NEW.home_sui_co_states_nbr, 'co_states', NEW.effective_date);

    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sui_co_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sui_co_states_nbr', NEW.home_sui_co_states_nbr, 'co_states', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_check_mb_group_nbr', NEW.tax_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_check_mb_group_nbr', NEW.tax_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CO_LOCATIONS */
  IF ((NEW.co_locations_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_locations_nbr IS DISTINCT FROM NEW.co_locations_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_locations
    WHERE co_locations_nbr = NEW.co_locations_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_locations_nbr', NEW.co_locations_nbr, 'co_locations', NEW.effective_date);

    SELECT rec_version FROM co_locations
    WHERE co_locations_nbr = NEW.co_locations_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_locations_nbr', NEW.co_locations_nbr, 'co_locations', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.aca_add_earn_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'aca_add_earn_cl_e_d_groups_nbr', NEW.aca_add_earn_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'aca_add_earn_cl_e_d_groups_nbr', NEW.aca_add_earn_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AD_CO_AUTO_ENLIST_RETURNS_9 FOR CO_AUTO_ENLIST_RETURNS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(40, OLD.rec_version, OLD.co_auto_enlist_returns_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 790, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2634, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 792, OLD.co_nbr);

  /* SY_GL_AGENCY_REPORT_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 793, OLD.sy_gl_agency_report_nbr);

  /* PROCESS_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 794, OLD.process_type);

  /* CO_REPORTS_NBR */
  IF (OLD.co_reports_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3214, OLD.co_reports_nbr);

END

^

CREATE TRIGGER T_AU_CO_AUTO_ENLIST_RETURNS_9 FOR CO_AUTO_ENLIST_RETURNS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(40, NEW.rec_version, NEW.co_auto_enlist_returns_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 790, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2634, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 792, OLD.co_nbr);
    changes = changes + 1;
  END

  /* SY_GL_AGENCY_REPORT_NBR */
  IF (OLD.sy_gl_agency_report_nbr IS DISTINCT FROM NEW.sy_gl_agency_report_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 793, OLD.sy_gl_agency_report_nbr);
    changes = changes + 1;
  END

  /* PROCESS_TYPE */
  IF (OLD.process_type IS DISTINCT FROM NEW.process_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 794, OLD.process_type);
    changes = changes + 1;
  END

  /* CO_REPORTS_NBR */
  IF (OLD.co_reports_nbr IS DISTINCT FROM NEW.co_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3214, OLD.co_reports_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_AUTO_ENLIST_RETURNS_2 FOR CO_AUTO_ENLIST_RETURNS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_auto_enlist_returns', NEW.co_auto_enlist_returns_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_auto_enlist_returns', NEW.co_auto_enlist_returns_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END

  /* Check reference to CO_REPORTS */
  IF ((NEW.co_reports_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_reports_nbr IS DISTINCT FROM NEW.co_reports_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_reports
    WHERE co_reports_nbr = NEW.co_reports_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_auto_enlist_returns', NEW.co_auto_enlist_returns_nbr, 'co_reports_nbr', NEW.co_reports_nbr, 'co_reports', NEW.effective_date);

    SELECT rec_version FROM co_reports
    WHERE co_reports_nbr = NEW.co_reports_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_auto_enlist_returns', NEW.co_auto_enlist_returns_nbr, 'co_reports_nbr', NEW.co_reports_nbr, 'co_reports', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AD_CO_BENEFIT_CATEGORY_9 FOR CO_BENEFIT_CATEGORY After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(158, OLD.rec_version, OLD.co_benefit_category_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3005, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3006, OLD.effective_until);

  /* CATEGORY_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3007, OLD.category_type);

  /* ENROLLMENT_NOTIFY_HR_DAYS */
  IF (OLD.enrollment_notify_hr_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3008, OLD.enrollment_notify_hr_days);

  /* ENROLLMENT_NOTIFY_EE_DAYS */
  IF (OLD.enrollment_notify_ee_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3009, OLD.enrollment_notify_ee_days);

  /* ENROLLMENT_REMINDER_HR_DAYS */
  IF (OLD.enrollment_reminder_hr_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3010, OLD.enrollment_reminder_hr_days);

  /* ENROLLMENT_REMINDER_EE_DAYS */
  IF (OLD.enrollment_reminder_ee_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3011, OLD.enrollment_reminder_ee_days);

  /* REMIND_HR_DAYS */
  IF (OLD.remind_hr_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3012, OLD.remind_hr_days);

  /* REMIND_EE_DAYS */
  IF (OLD.remind_ee_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3013, OLD.remind_ee_days);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3014, :blob_nbr);
  END

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3015, OLD.co_nbr);

  /* ENROLLMENT_FREQUENCY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3016, OLD.enrollment_frequency);

  /* READ_ONLY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3142, OLD.read_only);

END

^

CREATE TRIGGER T_AUD_CO_BENEFIT_CATEGORY_2 FOR CO_BENEFIT_CATEGORY After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_benefit_category WHERE co_benefit_category_nbr = OLD.co_benefit_category_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_benefit_category WHERE co_benefit_category_nbr = OLD.co_benefit_category_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO_BENEFITS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefits
    WHERE (co_benefit_category_nbr = OLD.co_benefit_category_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_category', OLD.co_benefit_category_nbr, 'co_benefits', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefits
    WHERE (co_benefit_category_nbr = OLD.co_benefit_category_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_category', OLD.co_benefit_category_nbr, 'co_benefits', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_ENROLLMENT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_enrollment
    WHERE (co_benefit_category_nbr = OLD.co_benefit_category_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_category', OLD.co_benefit_category_nbr, 'co_benefit_enrollment', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_enrollment
    WHERE (co_benefit_category_nbr = OLD.co_benefit_category_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_category', OLD.co_benefit_category_nbr, 'co_benefit_enrollment', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_CO_BENEFIT_CATEGORY_9 FOR CO_BENEFIT_CATEGORY After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(158, NEW.rec_version, NEW.co_benefit_category_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3005, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3006, OLD.effective_until);
    changes = changes + 1;
  END

  /* CATEGORY_TYPE */
  IF (OLD.category_type IS DISTINCT FROM NEW.category_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3007, OLD.category_type);
    changes = changes + 1;
  END

  /* ENROLLMENT_NOTIFY_HR_DAYS */
  IF (OLD.enrollment_notify_hr_days IS DISTINCT FROM NEW.enrollment_notify_hr_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3008, OLD.enrollment_notify_hr_days);
    changes = changes + 1;
  END

  /* ENROLLMENT_NOTIFY_EE_DAYS */
  IF (OLD.enrollment_notify_ee_days IS DISTINCT FROM NEW.enrollment_notify_ee_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3009, OLD.enrollment_notify_ee_days);
    changes = changes + 1;
  END

  /* ENROLLMENT_REMINDER_HR_DAYS */
  IF (OLD.enrollment_reminder_hr_days IS DISTINCT FROM NEW.enrollment_reminder_hr_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3010, OLD.enrollment_reminder_hr_days);
    changes = changes + 1;
  END

  /* ENROLLMENT_REMINDER_EE_DAYS */
  IF (OLD.enrollment_reminder_ee_days IS DISTINCT FROM NEW.enrollment_reminder_ee_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3011, OLD.enrollment_reminder_ee_days);
    changes = changes + 1;
  END

  /* REMIND_HR_DAYS */
  IF (OLD.remind_hr_days IS DISTINCT FROM NEW.remind_hr_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3012, OLD.remind_hr_days);
    changes = changes + 1;
  END

  /* REMIND_EE_DAYS */
  IF (OLD.remind_ee_days IS DISTINCT FROM NEW.remind_ee_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3013, OLD.remind_ee_days);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3014, :blob_nbr);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3015, OLD.co_nbr);
    changes = changes + 1;
  END

  /* ENROLLMENT_FREQUENCY */
  IF (OLD.enrollment_frequency IS DISTINCT FROM NEW.enrollment_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3016, OLD.enrollment_frequency);
    changes = changes + 1;
  END

  /* READ_ONLY */
  IF (OLD.read_only IS DISTINCT FROM NEW.read_only) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3142, OLD.read_only);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AUD_CO_REPORTS_2 FOR CO_REPORTS After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_reports WHERE co_reports_nbr = OLD.co_reports_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_reports WHERE co_reports_nbr = OLD.co_reports_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in PR_REPORTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_reports
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'pr_reports', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_reports
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'pr_reports', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_USER_REPORTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_user_reports
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'co_user_reports', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_user_reports
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'co_user_reports', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_AUTO_ENLIST_RETURNS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_auto_enlist_returns
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'co_auto_enlist_returns', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_auto_enlist_returns
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'co_auto_enlist_returns', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TAX_RETURN_QUEUE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_return_queue
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'co_tax_return_queue', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_return_queue
    WHERE (co_reports_nbr = OLD.co_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_reports', OLD.co_reports_nbr, 'co_tax_return_queue', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_CO_SERVICES_9 FOR CO_SERVICES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(96, OLD.rec_version, OLD.co_services_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1479, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2746, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1483, OLD.co_nbr);

  /* SB_SERVICES_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1484, OLD.sb_services_nbr);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1492, OLD.name);

  /* EFFECTIVE_START_DATE */
  IF (OLD.effective_start_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1486, OLD.effective_start_date);

  /* EFFECTIVE_END_DATE */
  IF (OLD.effective_end_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1487, OLD.effective_end_date);

  /* SALES_TAX */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1494, OLD.sales_tax);

  /* DISCOUNT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1495, OLD.discount_type);

  /* DISCOUNT_START_DATE */
  IF (OLD.discount_start_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1488, OLD.discount_start_date);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1493, OLD.filler);

  /* FREQUENCY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1497, OLD.frequency);

  /* WEEK_NUMBER */
  IF (OLD.week_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1498, OLD.week_number);

  /* MONTH_NUMBER */
  IF (OLD.month_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1485, OLD.month_number);

  /* OVERRIDE_FLAT_FEE */
  IF (OLD.override_flat_fee IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1478, OLD.override_flat_fee);

  /* DISCOUNT */
  IF (OLD.discount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1477, OLD.discount);

  /* DISCOUNT_END_DATE */
  IF (OLD.discount_end_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1490, OLD.discount_end_date);

  /* CO_E_D_CODES_NBR */
  IF (OLD.co_e_d_codes_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1491, OLD.co_e_d_codes_nbr);

  /* CL_E_D_GROUPS_NBR */
  IF (OLD.cl_e_d_groups_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1481, OLD.cl_e_d_groups_nbr);

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1480, OLD.co_workers_comp_nbr);

  /* OVERRIDE_TAX_RATE */
  IF (OLD.override_tax_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2894, OLD.override_tax_rate);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3216, :blob_nbr);
  END

END

^

CREATE TRIGGER T_AU_CO_SERVICES_9 FOR CO_SERVICES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(96, NEW.rec_version, NEW.co_services_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1479, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2746, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1483, OLD.co_nbr);
    changes = changes + 1;
  END

  /* SB_SERVICES_NBR */
  IF (OLD.sb_services_nbr IS DISTINCT FROM NEW.sb_services_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1484, OLD.sb_services_nbr);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1492, OLD.name);
    changes = changes + 1;
  END

  /* EFFECTIVE_START_DATE */
  IF (OLD.effective_start_date IS DISTINCT FROM NEW.effective_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1486, OLD.effective_start_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_END_DATE */
  IF (OLD.effective_end_date IS DISTINCT FROM NEW.effective_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1487, OLD.effective_end_date);
    changes = changes + 1;
  END

  /* SALES_TAX */
  IF (OLD.sales_tax IS DISTINCT FROM NEW.sales_tax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1494, OLD.sales_tax);
    changes = changes + 1;
  END

  /* DISCOUNT_TYPE */
  IF (OLD.discount_type IS DISTINCT FROM NEW.discount_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1495, OLD.discount_type);
    changes = changes + 1;
  END

  /* DISCOUNT_START_DATE */
  IF (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1488, OLD.discount_start_date);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1493, OLD.filler);
    changes = changes + 1;
  END

  /* FREQUENCY */
  IF (OLD.frequency IS DISTINCT FROM NEW.frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1497, OLD.frequency);
    changes = changes + 1;
  END

  /* WEEK_NUMBER */
  IF (OLD.week_number IS DISTINCT FROM NEW.week_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1498, OLD.week_number);
    changes = changes + 1;
  END

  /* MONTH_NUMBER */
  IF (OLD.month_number IS DISTINCT FROM NEW.month_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1485, OLD.month_number);
    changes = changes + 1;
  END

  /* OVERRIDE_FLAT_FEE */
  IF (OLD.override_flat_fee IS DISTINCT FROM NEW.override_flat_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1478, OLD.override_flat_fee);
    changes = changes + 1;
  END

  /* DISCOUNT */
  IF (OLD.discount IS DISTINCT FROM NEW.discount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1477, OLD.discount);
    changes = changes + 1;
  END

  /* DISCOUNT_END_DATE */
  IF (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1490, OLD.discount_end_date);
    changes = changes + 1;
  END

  /* CO_E_D_CODES_NBR */
  IF (OLD.co_e_d_codes_nbr IS DISTINCT FROM NEW.co_e_d_codes_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1491, OLD.co_e_d_codes_nbr);
    changes = changes + 1;
  END

  /* CL_E_D_GROUPS_NBR */
  IF (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1481, OLD.cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1480, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* OVERRIDE_TAX_RATE */
  IF (OLD.override_tax_rate IS DISTINCT FROM NEW.override_tax_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2894, OLD.override_tax_rate);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3216, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BUD_CO_SERVICES_9 FOR CO_SERVICES Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END
END

^

CREATE TRIGGER T_AD_CO_TAX_RETURN_QUEUE_9 FOR CO_TAX_RETURN_QUEUE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(105, OLD.rec_version, OLD.co_tax_return_queue_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1646, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2764, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1650, OLD.co_nbr);

  /* STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1656, OLD.status);

  /* STATUS_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1647, OLD.status_date);

  /* SB_COPY_PRINTED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1657, OLD.sb_copy_printed);

  /* CLIENT_COPY_PRINTED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1658, OLD.client_copy_printed);

  /* AGENCY_COPY_PRINTED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1659, OLD.agency_copy_printed);

  /* DUE_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1652, OLD.due_date);

  /* PERIOD_END_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1653, OLD.period_end_date);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1655, OLD.filler);

  /* SY_REPORTS_GROUP_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1648, OLD.sy_reports_group_nbr);

  /* CL_CO_CONSOLIDATION_NBR */
  IF (OLD.cl_co_consolidation_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1651, OLD.cl_co_consolidation_nbr);

  /* PRODUCE_ASCII_FILE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1660, OLD.produce_ascii_file);

  /* SY_GL_AGENCY_REPORT_NBR */
  IF (OLD.sy_gl_agency_report_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1654, OLD.sy_gl_agency_report_nbr);

  /* CO_REPORTS_NBR */
  IF (OLD.co_reports_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3215, OLD.co_reports_nbr);

END

^

CREATE TRIGGER T_AU_CO_TAX_RETURN_QUEUE_9 FOR CO_TAX_RETURN_QUEUE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(105, NEW.rec_version, NEW.co_tax_return_queue_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1646, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2764, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1650, OLD.co_nbr);
    changes = changes + 1;
  END

  /* STATUS */
  IF (OLD.status IS DISTINCT FROM NEW.status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1656, OLD.status);
    changes = changes + 1;
  END

  /* STATUS_DATE */
  IF (OLD.status_date IS DISTINCT FROM NEW.status_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1647, OLD.status_date);
    changes = changes + 1;
  END

  /* SB_COPY_PRINTED */
  IF (OLD.sb_copy_printed IS DISTINCT FROM NEW.sb_copy_printed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1657, OLD.sb_copy_printed);
    changes = changes + 1;
  END

  /* CLIENT_COPY_PRINTED */
  IF (OLD.client_copy_printed IS DISTINCT FROM NEW.client_copy_printed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1658, OLD.client_copy_printed);
    changes = changes + 1;
  END

  /* AGENCY_COPY_PRINTED */
  IF (OLD.agency_copy_printed IS DISTINCT FROM NEW.agency_copy_printed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1659, OLD.agency_copy_printed);
    changes = changes + 1;
  END

  /* DUE_DATE */
  IF (OLD.due_date IS DISTINCT FROM NEW.due_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1652, OLD.due_date);
    changes = changes + 1;
  END

  /* PERIOD_END_DATE */
  IF (OLD.period_end_date IS DISTINCT FROM NEW.period_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1653, OLD.period_end_date);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1655, OLD.filler);
    changes = changes + 1;
  END

  /* SY_REPORTS_GROUP_NBR */
  IF (OLD.sy_reports_group_nbr IS DISTINCT FROM NEW.sy_reports_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1648, OLD.sy_reports_group_nbr);
    changes = changes + 1;
  END

  /* CL_CO_CONSOLIDATION_NBR */
  IF (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1651, OLD.cl_co_consolidation_nbr);
    changes = changes + 1;
  END

  /* PRODUCE_ASCII_FILE */
  IF (OLD.produce_ascii_file IS DISTINCT FROM NEW.produce_ascii_file) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1660, OLD.produce_ascii_file);
    changes = changes + 1;
  END

  /* SY_GL_AGENCY_REPORT_NBR */
  IF (OLD.sy_gl_agency_report_nbr IS DISTINCT FROM NEW.sy_gl_agency_report_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1654, OLD.sy_gl_agency_report_nbr);
    changes = changes + 1;
  END

  /* CO_REPORTS_NBR */
  IF (OLD.co_reports_nbr IS DISTINCT FROM NEW.co_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3215, OLD.co_reports_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_TAX_RETURN_QUEUE_2 FOR CO_TAX_RETURN_QUEUE Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_tax_return_queue', NEW.co_tax_return_queue_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_tax_return_queue', NEW.co_tax_return_queue_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END

  /* Check reference to CL_CO_CONSOLIDATION */
  IF ((NEW.cl_co_consolidation_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_tax_return_queue', NEW.co_tax_return_queue_nbr, 'cl_co_consolidation_nbr', NEW.cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_date);

    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_tax_return_queue', NEW.co_tax_return_queue_nbr, 'cl_co_consolidation_nbr', NEW.cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_until - 1);
  END

  /* Check reference to CO_REPORTS */
  IF ((NEW.co_reports_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_reports_nbr IS DISTINCT FROM NEW.co_reports_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_reports
    WHERE co_reports_nbr = NEW.co_reports_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_tax_return_queue', NEW.co_tax_return_queue_nbr, 'co_reports_nbr', NEW.co_reports_nbr, 'co_reports', NEW.effective_date);

    SELECT rec_version FROM co_reports
    WHERE co_reports_nbr = NEW.co_reports_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_tax_return_queue', NEW.co_tax_return_queue_nbr, 'co_reports_nbr', NEW.co_reports_nbr, 'co_reports', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AD_CO_WORKERS_COMP_9 FOR CO_WORKERS_COMP After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE workers_comp_code VARCHAR(8);
DECLARE VARIABLE co_states_nbr INTEGER;
DECLARE VARIABLE rate NUMERIC(18,6);
DECLARE VARIABLE experience_rating NUMERIC(18,6);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(114, OLD.rec_version, OLD.co_workers_comp_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', workers_comp_code, co_states_nbr, rate, experience_rating  FROM co_workers_comp WHERE co_workers_comp_nbr = OLD.co_workers_comp_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :workers_comp_code, :co_states_nbr, :rate, :experience_rating;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1790, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2782, OLD.effective_until);

  /* WORKERS_COMP_CODE */
  IF (last_record = 'Y' OR workers_comp_code IS DISTINCT FROM OLD.workers_comp_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1797, OLD.workers_comp_code);

  /* CO_STATES_NBR */
  IF (last_record = 'Y' OR co_states_nbr IS DISTINCT FROM OLD.co_states_nbr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1793, OLD.co_states_nbr);

  /* RATE */
  IF ((last_record = 'Y' AND OLD.rate IS NOT NULL) OR (last_record = 'N' AND rate IS DISTINCT FROM OLD.rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1789, OLD.rate);

  /* EXPERIENCE_RATING */
  IF ((last_record = 'Y' AND OLD.experience_rating IS NOT NULL) OR (last_record = 'N' AND experience_rating IS DISTINCT FROM OLD.experience_rating)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1788, OLD.experience_rating);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CO_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1792, OLD.co_nbr);

    /* DESCRIPTION */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1795, OLD.description);

    /* CL_E_D_GROUPS_NBR */
    IF (OLD.cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1794, OLD.cl_e_d_groups_nbr);

    /* GL_TAG */
    IF (OLD.gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1796, OLD.gl_tag);

    /* GL_OFFSET_TAG */
    IF (OLD.gl_offset_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1798, OLD.gl_offset_tag);

    /* SUBTRACT_PREMIUM */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1799, OLD.subtract_premium);

  END
END

^

CREATE TRIGGER T_AIU_CO_WORKERS_COMP_3 FOR CO_WORKERS_COMP After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.description IS DISTINCT FROM NEW.description) OR (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) OR (OLD.gl_tag IS DISTINCT FROM NEW.gl_tag) OR (OLD.gl_offset_tag IS DISTINCT FROM NEW.gl_offset_tag) OR (OLD.subtract_premium IS DISTINCT FROM NEW.subtract_premium)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE co_workers_comp SET 
      co_nbr = NEW.co_nbr, description = NEW.description, cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr, gl_tag = NEW.gl_tag, gl_offset_tag = NEW.gl_offset_tag, subtract_premium = NEW.subtract_premium
      WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_EE_9 FOR EE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
DECLARE VARIABLE aca_coverage_offer VARCHAR(2);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, OLD.rec_version, OLD.ee_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status, aca_coverage_offer  FROM ee WHERE ee_nbr = OLD.ee_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status, :aca_coverage_offer;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1810, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2784, OLD.effective_until);

  /* CL_PERSON_NBR */
  IF (last_record = 'Y' OR cl_person_nbr IS DISTINCT FROM OLD.cl_person_nbr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);

  /* CO_DIVISION_NBR */
  IF ((last_record = 'Y' AND OLD.co_division_nbr IS NOT NULL) OR (last_record = 'N' AND co_division_nbr IS DISTINCT FROM OLD.co_division_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);

  /* CO_BRANCH_NBR */
  IF ((last_record = 'Y' AND OLD.co_branch_nbr IS NOT NULL) OR (last_record = 'N' AND co_branch_nbr IS DISTINCT FROM OLD.co_branch_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);

  /* CO_DEPARTMENT_NBR */
  IF ((last_record = 'Y' AND OLD.co_department_nbr IS NOT NULL) OR (last_record = 'N' AND co_department_nbr IS DISTINCT FROM OLD.co_department_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);

  /* CO_TEAM_NBR */
  IF ((last_record = 'Y' AND OLD.co_team_nbr IS NOT NULL) OR (last_record = 'N' AND co_team_nbr IS DISTINCT FROM OLD.co_team_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);

  /* NEW_HIRE_REPORT_SENT */
  IF (last_record = 'Y' OR new_hire_report_sent IS DISTINCT FROM OLD.new_hire_report_sent) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);

  /* CURRENT_TERMINATION_CODE */
  IF (last_record = 'Y' OR current_termination_code IS DISTINCT FROM OLD.current_termination_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1889, OLD.current_termination_code);

  /* CO_HR_POSITIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_hr_positions_nbr IS NOT NULL) OR (last_record = 'N' AND co_hr_positions_nbr IS DISTINCT FROM OLD.co_hr_positions_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);

  /* TIPPED_DIRECTLY */
  IF (last_record = 'Y' OR tipped_directly IS DISTINCT FROM OLD.tipped_directly) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1893, OLD.tipped_directly);

  /* W2_TYPE */
  IF (last_record = 'Y' OR w2_type IS DISTINCT FROM OLD.w2_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1897, OLD.w2_type);

  /* W2_PENSION */
  IF (last_record = 'Y' OR w2_pension IS DISTINCT FROM OLD.w2_pension) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1898, OLD.w2_pension);

  /* W2_DEFERRED_COMP */
  IF (last_record = 'Y' OR w2_deferred_comp IS DISTINCT FROM OLD.w2_deferred_comp) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);

  /* W2_DECEASED */
  IF (last_record = 'Y' OR w2_deceased IS DISTINCT FROM OLD.w2_deceased) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1900, OLD.w2_deceased);

  /* W2_STATUTORY_EMPLOYEE */
  IF (last_record = 'Y' OR w2_statutory_employee IS DISTINCT FROM OLD.w2_statutory_employee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);

  /* W2_LEGAL_REP */
  IF (last_record = 'Y' OR w2_legal_rep IS DISTINCT FROM OLD.w2_legal_rep) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);

  /* HOME_TAX_EE_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.home_tax_ee_states_nbr IS NOT NULL) OR (last_record = 'N' AND home_tax_ee_states_nbr IS DISTINCT FROM OLD.home_tax_ee_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (last_record = 'Y' OR exempt_employee_oasdi IS DISTINCT FROM OLD.exempt_employee_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (last_record = 'Y' OR exempt_employee_medicare IS DISTINCT FROM OLD.exempt_employee_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (last_record = 'Y' OR exempt_exclude_ee_fed IS DISTINCT FROM OLD.exempt_exclude_ee_fed) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);

  /* EXEMPT_EMPLOYER_OASDI */
  IF (last_record = 'Y' OR exempt_employer_oasdi IS DISTINCT FROM OLD.exempt_employer_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (last_record = 'Y' OR exempt_employer_medicare IS DISTINCT FROM OLD.exempt_employer_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);

  /* EXEMPT_EMPLOYER_FUI */
  IF (last_record = 'Y' OR exempt_employer_fui IS DISTINCT FROM OLD.exempt_employer_fui) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);

  /* BASE_RETURNS_ON_THIS_EE */
  IF (last_record = 'Y' OR base_returns_on_this_ee IS DISTINCT FROM OLD.base_returns_on_this_ee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (last_record = 'Y' OR company_or_individual_name IS DISTINCT FROM OLD.company_or_individual_name) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);

  /* DISTRIBUTION_CODE_1099R */
  IF ((last_record = 'Y' AND OLD.distribution_code_1099r IS NOT NULL) OR (last_record = 'N' AND distribution_code_1099r IS DISTINCT FROM OLD.distribution_code_1099r)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);

  /* TAX_AMT_DETERMINED_1099R */
  IF (last_record = 'Y' OR tax_amt_determined_1099r IS DISTINCT FROM OLD.tax_amt_determined_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);

  /* TOTAL_DISTRIBUTION_1099R */
  IF (last_record = 'Y' OR total_distribution_1099r IS DISTINCT FROM OLD.total_distribution_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);

  /* PENSION_PLAN_1099R */
  IF (last_record = 'Y' OR pension_plan_1099r IS DISTINCT FROM OLD.pension_plan_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (last_record = 'Y' OR makeup_fica_on_cleanup_pr IS DISTINCT FROM OLD.makeup_fica_on_cleanup_pr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);

  /* SALARY_AMOUNT */
  IF ((last_record = 'Y' AND OLD.salary_amount IS NOT NULL) OR (last_record = 'N' AND salary_amount IS DISTINCT FROM OLD.salary_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1809, OLD.salary_amount);

  /* HIGHLY_COMPENSATED */
  IF (last_record = 'Y' OR highly_compensated IS DISTINCT FROM OLD.highly_compensated) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1919, OLD.highly_compensated);

  /* CORPORATE_OFFICER */
  IF (last_record = 'Y' OR corporate_officer IS DISTINCT FROM OLD.corporate_officer) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1920, OLD.corporate_officer);

  /* CO_JOBS_NBR */
  IF ((last_record = 'Y' AND OLD.co_jobs_nbr IS NOT NULL) OR (last_record = 'N' AND co_jobs_nbr IS DISTINCT FROM OLD.co_jobs_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);

  /* CO_WORKERS_COMP_NBR */
  IF ((last_record = 'Y' AND OLD.co_workers_comp_nbr IS NOT NULL) OR (last_record = 'N' AND co_workers_comp_nbr IS DISTINCT FROM OLD.co_workers_comp_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);

  /* HEALTHCARE_COVERAGE */
  IF (last_record = 'Y' OR healthcare_coverage IS DISTINCT FROM OLD.healthcare_coverage) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF ((last_record = 'Y' AND OLD.fui_rate_credit_override IS NOT NULL) OR (last_record = 'N' AND fui_rate_credit_override IS DISTINCT FROM OLD.fui_rate_credit_override)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);

  /* ACA_STATUS */
  IF (last_record = 'Y' OR aca_status IS DISTINCT FROM OLD.aca_status) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3188, OLD.aca_status);

  /* ACA_COVERAGE_OFFER */
  IF ((last_record = 'Y' AND OLD.aca_coverage_offer IS NOT NULL) OR (last_record = 'N' AND aca_coverage_offer IS DISTINCT FROM OLD.aca_coverage_offer)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3237, OLD.aca_coverage_offer);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CO_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1823, OLD.co_nbr);

    /* CUSTOM_EMPLOYEE_NUMBER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);

    /* CO_HR_APPLICANT_NBR */
    IF (OLD.co_hr_applicant_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);

    /* CO_HR_RECRUITERS_NBR */
    IF (OLD.co_hr_recruiters_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);

    /* CO_HR_REFERRALS_NBR */
    IF (OLD.co_hr_referrals_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);

    /* ADDRESS1 */
    IF (OLD.address1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1811, OLD.address1);

    /* ADDRESS2 */
    IF (OLD.address2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1812, OLD.address2);

    /* CITY */
    IF (OLD.city IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1869, OLD.city);

    /* STATE */
    IF (OLD.state IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1870, OLD.state);

    /* ZIP_CODE */
    IF (OLD.zip_code IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1831, OLD.zip_code);

    /* COUNTY */
    IF (OLD.county IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1832, OLD.county);

    /* PHONE1 */
    IF (OLD.phone1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1871, OLD.phone1);

    /* DESCRIPTION1 */
    IF (OLD.description1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1834, OLD.description1);

    /* PHONE2 */
    IF (OLD.phone2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1872, OLD.phone2);

    /* DESCRIPTION2 */
    IF (OLD.description2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1835, OLD.description2);

    /* PHONE3 */
    IF (OLD.phone3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1873, OLD.phone3);

    /* DESCRIPTION3 */
    IF (OLD.description3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1833, OLD.description3);

    /* E_MAIL_ADDRESS */
    IF (OLD.e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1874, OLD.e_mail_address);

    /* TIME_CLOCK_NUMBER */
    IF (OLD.time_clock_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1887, OLD.time_clock_number);

    /* ORIGINAL_HIRE_DATE */
    IF (OLD.original_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1848, OLD.original_hire_date);

    /* CURRENT_HIRE_DATE */
    IF (OLD.current_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1849, OLD.current_hire_date);

    /* CURRENT_TERMINATION_DATE */
    IF (OLD.current_termination_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1850, OLD.current_termination_date);

    /* CO_HR_SUPERVISORS_NBR */
    IF (OLD.co_hr_supervisors_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);

    /* AUTOPAY_CO_SHIFTS_NBR */
    IF (OLD.autopay_co_shifts_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);

    /* POSITION_EFFECTIVE_DATE */
    IF (OLD.position_effective_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1851, OLD.position_effective_date);

    /* POSITION_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1890, OLD.position_status);

    /* CO_HR_PERFORMANCE_RATINGS_NBR */
    IF (OLD.co_hr_performance_ratings_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);

    /* REVIEW_DATE */
    IF (OLD.review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1852, OLD.review_date);

    /* NEXT_REVIEW_DATE */
    IF (OLD.next_review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1853, OLD.next_review_date);

    /* PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1891, OLD.pay_frequency);

    /* NEXT_RAISE_DATE */
    IF (OLD.next_raise_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1854, OLD.next_raise_date);

    /* NEXT_PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);

    /* ALD_CL_E_D_GROUPS_NBR */
    IF (OLD.ald_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);

    /* DISTRIBUTE_TAXES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);

    /* CO_PAY_GROUP_NBR */
    IF (OLD.co_pay_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);

    /* CL_DELIVERY_GROUP_NBR */
    IF (OLD.cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);

    /* CO_UNIONS_NBR */
    IF (OLD.co_unions_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);

    /* EIC */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1895, OLD.eic);

    /* FLSA_EXEMPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);

    /* OVERRIDE_FED_TAX_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);

    /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);

    /* SECURITY_CLEARANCE */
    IF (OLD.security_clearance IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1875, OLD.security_clearance);

    /* BADGE_ID */
    IF (OLD.badge_id IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1911, OLD.badge_id);

    /* ON_CALL_FROM */
    IF (OLD.on_call_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1813, OLD.on_call_from);

    /* ON_CALL_TO */
    IF (OLD.on_call_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1814, OLD.on_call_to);

    /* NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1815, :blob_nbr);
    END

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1876, OLD.filler);

    /* GENERAL_LEDGER_TAG */
    IF (OLD.general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);

    /* FEDERAL_MARITAL_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);

    /* NUMBER_OF_DEPENDENTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);

    /* SY_HR_EEO_NBR */
    IF (OLD.sy_hr_eeo_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);

    /* SECONDARY_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1816, :blob_nbr);
    END

    /* CO_PR_CHECK_TEMPLATES_NBR */
    IF (OLD.co_pr_check_templates_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);

    /* GENERATE_SECOND_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1884, OLD.generate_second_check);

    /* STANDARD_HOURS */
    IF (OLD.standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1808, OLD.standard_hours);

    /* NEXT_RAISE_AMOUNT */
    IF (OLD.next_raise_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);

    /* NEXT_RAISE_PERCENTAGE */
    IF (OLD.next_raise_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);

    /* NEXT_RAISE_RATE */
    IF (OLD.next_raise_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);

    /* WORKERS_COMP_WAGE_LIMIT */
    IF (OLD.workers_comp_wage_limit IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);

    /* GROUP_TERM_POLICY_AMOUNT */
    IF (OLD.group_term_policy_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);

    /* OVERRIDE_FED_TAX_VALUE */
    IF (OLD.override_fed_tax_value IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);

    /* CALCULATED_SALARY */
    IF (OLD.calculated_salary IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1801, OLD.calculated_salary);

    /* PR_CHECK_MB_GROUP_NBR */
    IF (OLD.pr_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);

    /* PR_EE_REPORT_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);

    /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_sec_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);

    /* TAX_EE_RETURN_MB_GROUP_NBR */
    IF (OLD.tax_ee_return_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);

    /* EE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1921, OLD.ee_enabled);

    /* GTL_NUMBER_OF_HOURS */
    IF (OLD.gtl_number_of_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);

    /* GTL_RATE */
    IF (OLD.gtl_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1817, OLD.gtl_rate);

    /* AUTO_UPDATE_RATES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);

    /* ELIGIBLE_FOR_REHIRE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);

    /* SELFSERVE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);

    /* SELFSERVE_USERNAME */
    IF (OLD.selfserve_username IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1868, OLD.selfserve_username);

    /* SELFSERVE_PASSWORD */
    IF (OLD.selfserve_password IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1867, OLD.selfserve_password);

    /* SELFSERVE_LAST_LOGIN */
    IF (OLD.selfserve_last_login IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);

    /* PRINT_VOUCHER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1882, OLD.print_voucher);

    /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
    IF (OLD.override_federal_minimum_wage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);

    /* WC_WAGE_LIMIT_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);

    /* LOGIN_QUESTION1 */
    IF (OLD.login_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1864, OLD.login_question1);

    /* LOGIN_ANSWER1 */
    IF (OLD.login_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1879, OLD.login_answer1);

    /* LOGIN_QUESTION2 */
    IF (OLD.login_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1865, OLD.login_question2);

    /* LOGIN_ANSWER2 */
    IF (OLD.login_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1880, OLD.login_answer2);

    /* LOGIN_QUESTION3 */
    IF (OLD.login_question3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1866, OLD.login_question3);

    /* LOGIN_ANSWER3 */
    IF (OLD.login_answer3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1881, OLD.login_answer3);

    /* LOGIN_DATE */
    IF (OLD.login_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2785, OLD.login_date);

    /* LOGIN_ATTEMPTS */
    IF (OLD.login_attempts IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2786, OLD.login_attempts);

    /* CO_BENEFIT_DISCOUNT_NBR */
    IF (OLD.co_benefit_discount_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);

    /* BENEFITS_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);

    /* EXISTING_PATIENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2914, OLD.existing_patient);

    /* PCP */
    IF (OLD.pcp IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2915, OLD.pcp);

    /* TIME_OFF_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);

    /* DEPENDENT_BENEFITS_AVAILABLE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);

    /* DEPENDENT_BENEFITS_AVAIL_DATE */
    IF (OLD.dependent_benefits_avail_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);

    /* BENEFIT_ENROLLMENT_COMPLETE */
    IF (OLD.benefit_enrollment_complete IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);

    /* LAST_QUAL_BENEFIT_EVENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);

    /* LAST_QUAL_BENEFIT_EVENT_DATE */
    IF (OLD.last_qual_benefit_event_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);

    /* W2 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2922, OLD.w2);

    /* W2_FORM_ON_FILE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);

    /* BENEFIT_E_MAIL_ADDRESS */
    IF (OLD.benefit_e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);

    /* CO_HR_POSITION_GRADES_NBR */
    IF (OLD.co_hr_position_grades_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);

    /* SEC_QUESTION1 */
    IF (OLD.sec_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2926, OLD.sec_question1);

    /* SEC_ANSWER1 */
    IF (OLD.sec_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2927, OLD.sec_answer1);

    /* SEC_QUESTION2 */
    IF (OLD.sec_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2928, OLD.sec_question2);

    /* SEC_ANSWER2 */
    IF (OLD.sec_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2929, OLD.sec_answer2);

    /* WORKERS_COMP_MIN_WAGE_LIMIT */
    IF (OLD.workers_comp_min_wage_limit IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3189, OLD.workers_comp_min_wage_limit);

    /* ESS_TERMS_OF_USE_ACCEPT_DATE */
    IF (OLD.ess_terms_of_use_accept_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3192, OLD.ess_terms_of_use_accept_date);

    /* ACA_STANDARD_HOURS */
    IF (OLD.aca_standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3218, OLD.aca_standard_hours);

  END
END

^

CREATE TRIGGER T_AU_EE_9 FOR EE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, NEW.rec_version, NEW.ee_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1810, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2784, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_PERSON_NBR */
  IF (OLD.cl_person_nbr IS DISTINCT FROM NEW.cl_person_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1823, OLD.co_nbr);
    changes = changes + 1;
  END

  /* CO_DIVISION_NBR */
  IF (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);
    changes = changes + 1;
  END

  /* CO_BRANCH_NBR */
  IF (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);
    changes = changes + 1;
  END

  /* CO_DEPARTMENT_NBR */
  IF (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);
    changes = changes + 1;
  END

  /* CO_TEAM_NBR */
  IF (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);
    changes = changes + 1;
  END

  /* CUSTOM_EMPLOYEE_NUMBER */
  IF (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);
    changes = changes + 1;
  END

  /* CO_HR_APPLICANT_NBR */
  IF (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);
    changes = changes + 1;
  END

  /* CO_HR_RECRUITERS_NBR */
  IF (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);
    changes = changes + 1;
  END

  /* CO_HR_REFERRALS_NBR */
  IF (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1811, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1812, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1869, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1870, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1831, OLD.zip_code);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1832, OLD.county);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1871, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1834, OLD.description1);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1872, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1835, OLD.description2);
    changes = changes + 1;
  END

  /* PHONE3 */
  IF (OLD.phone3 IS DISTINCT FROM NEW.phone3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1873, OLD.phone3);
    changes = changes + 1;
  END

  /* DESCRIPTION3 */
  IF (OLD.description3 IS DISTINCT FROM NEW.description3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1833, OLD.description3);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1874, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* TIME_CLOCK_NUMBER */
  IF (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1887, OLD.time_clock_number);
    changes = changes + 1;
  END

  /* ORIGINAL_HIRE_DATE */
  IF (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1848, OLD.original_hire_date);
    changes = changes + 1;
  END

  /* CURRENT_HIRE_DATE */
  IF (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1849, OLD.current_hire_date);
    changes = changes + 1;
  END

  /* NEW_HIRE_REPORT_SENT */
  IF (OLD.new_hire_report_sent IS DISTINCT FROM NEW.new_hire_report_sent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_DATE */
  IF (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1850, OLD.current_termination_date);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_CODE */
  IF (OLD.current_termination_code IS DISTINCT FROM NEW.current_termination_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1889, OLD.current_termination_code);
    changes = changes + 1;
  END

  /* CO_HR_SUPERVISORS_NBR */
  IF (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);
    changes = changes + 1;
  END

  /* AUTOPAY_CO_SHIFTS_NBR */
  IF (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);
    changes = changes + 1;
  END

  /* CO_HR_POSITIONS_NBR */
  IF (OLD.co_hr_positions_nbr IS DISTINCT FROM NEW.co_hr_positions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);
    changes = changes + 1;
  END

  /* POSITION_EFFECTIVE_DATE */
  IF (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1851, OLD.position_effective_date);
    changes = changes + 1;
  END

  /* POSITION_STATUS */
  IF (OLD.position_status IS DISTINCT FROM NEW.position_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1890, OLD.position_status);
    changes = changes + 1;
  END

  /* CO_HR_PERFORMANCE_RATINGS_NBR */
  IF (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);
    changes = changes + 1;
  END

  /* REVIEW_DATE */
  IF (OLD.review_date IS DISTINCT FROM NEW.review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1852, OLD.review_date);
    changes = changes + 1;
  END

  /* NEXT_REVIEW_DATE */
  IF (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1853, OLD.next_review_date);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY */
  IF (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1891, OLD.pay_frequency);
    changes = changes + 1;
  END

  /* NEXT_RAISE_DATE */
  IF (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1854, OLD.next_raise_date);
    changes = changes + 1;
  END

  /* NEXT_PAY_FREQUENCY */
  IF (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);
    changes = changes + 1;
  END

  /* TIPPED_DIRECTLY */
  IF (OLD.tipped_directly IS DISTINCT FROM NEW.tipped_directly) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1893, OLD.tipped_directly);
    changes = changes + 1;
  END

  /* ALD_CL_E_D_GROUPS_NBR */
  IF (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* DISTRIBUTE_TAXES */
  IF (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);
    changes = changes + 1;
  END

  /* CO_PAY_GROUP_NBR */
  IF (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);
    changes = changes + 1;
  END

  /* CL_DELIVERY_GROUP_NBR */
  IF (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* CO_UNIONS_NBR */
  IF (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);
    changes = changes + 1;
  END

  /* EIC */
  IF (OLD.eic IS DISTINCT FROM NEW.eic) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1895, OLD.eic);
    changes = changes + 1;
  END

  /* FLSA_EXEMPT */
  IF (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);
    changes = changes + 1;
  END

  /* W2_TYPE */
  IF (OLD.w2_type IS DISTINCT FROM NEW.w2_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1897, OLD.w2_type);
    changes = changes + 1;
  END

  /* W2_PENSION */
  IF (OLD.w2_pension IS DISTINCT FROM NEW.w2_pension) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1898, OLD.w2_pension);
    changes = changes + 1;
  END

  /* W2_DEFERRED_COMP */
  IF (OLD.w2_deferred_comp IS DISTINCT FROM NEW.w2_deferred_comp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);
    changes = changes + 1;
  END

  /* W2_DECEASED */
  IF (OLD.w2_deceased IS DISTINCT FROM NEW.w2_deceased) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1900, OLD.w2_deceased);
    changes = changes + 1;
  END

  /* W2_STATUTORY_EMPLOYEE */
  IF (OLD.w2_statutory_employee IS DISTINCT FROM NEW.w2_statutory_employee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);
    changes = changes + 1;
  END

  /* W2_LEGAL_REP */
  IF (OLD.w2_legal_rep IS DISTINCT FROM NEW.w2_legal_rep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);
    changes = changes + 1;
  END

  /* HOME_TAX_EE_STATES_NBR */
  IF (OLD.home_tax_ee_states_nbr IS DISTINCT FROM NEW.home_tax_ee_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (OLD.exempt_employee_oasdi IS DISTINCT FROM NEW.exempt_employee_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (OLD.exempt_employee_medicare IS DISTINCT FROM NEW.exempt_employee_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (OLD.exempt_exclude_ee_fed IS DISTINCT FROM NEW.exempt_exclude_ee_fed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_OASDI */
  IF (OLD.exempt_employer_oasdi IS DISTINCT FROM NEW.exempt_employer_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (OLD.exempt_employer_medicare IS DISTINCT FROM NEW.exempt_employer_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_FUI */
  IF (OLD.exempt_employer_fui IS DISTINCT FROM NEW.exempt_employer_fui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_TYPE */
  IF (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);
    changes = changes + 1;
  END

  /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
  IF (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);
    changes = changes + 1;
  END

  /* SECURITY_CLEARANCE */
  IF (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1875, OLD.security_clearance);
    changes = changes + 1;
  END

  /* BADGE_ID */
  IF (OLD.badge_id IS DISTINCT FROM NEW.badge_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1911, OLD.badge_id);
    changes = changes + 1;
  END

  /* ON_CALL_FROM */
  IF (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1813, OLD.on_call_from);
    changes = changes + 1;
  END

  /* ON_CALL_TO */
  IF (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1814, OLD.on_call_to);
    changes = changes + 1;
  END

  /* BASE_RETURNS_ON_THIS_EE */
  IF (OLD.base_returns_on_this_ee IS DISTINCT FROM NEW.base_returns_on_this_ee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1815, :blob_nbr);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1876, OLD.filler);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_TAG */
  IF (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);
    changes = changes + 1;
  END

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (OLD.company_or_individual_name IS DISTINCT FROM NEW.company_or_individual_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);
    changes = changes + 1;
  END

  /* FEDERAL_MARITAL_STATUS */
  IF (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);
    changes = changes + 1;
  END

  /* NUMBER_OF_DEPENDENTS */
  IF (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);
    changes = changes + 1;
  END

  /* DISTRIBUTION_CODE_1099R */
  IF (OLD.distribution_code_1099r IS DISTINCT FROM NEW.distribution_code_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);
    changes = changes + 1;
  END

  /* TAX_AMT_DETERMINED_1099R */
  IF (OLD.tax_amt_determined_1099r IS DISTINCT FROM NEW.tax_amt_determined_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);
    changes = changes + 1;
  END

  /* TOTAL_DISTRIBUTION_1099R */
  IF (OLD.total_distribution_1099r IS DISTINCT FROM NEW.total_distribution_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);
    changes = changes + 1;
  END

  /* PENSION_PLAN_1099R */
  IF (OLD.pension_plan_1099r IS DISTINCT FROM NEW.pension_plan_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);
    changes = changes + 1;
  END

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (OLD.makeup_fica_on_cleanup_pr IS DISTINCT FROM NEW.makeup_fica_on_cleanup_pr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);
    changes = changes + 1;
  END

  /* SY_HR_EEO_NBR */
  IF (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);
    changes = changes + 1;
  END

  /* SECONDARY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1816, :blob_nbr);
    changes = changes + 1;
  END

  /* CO_PR_CHECK_TEMPLATES_NBR */
  IF (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);
    changes = changes + 1;
  END

  /* GENERATE_SECOND_CHECK */
  IF (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1884, OLD.generate_second_check);
    changes = changes + 1;
  END

  /* SALARY_AMOUNT */
  IF (OLD.salary_amount IS DISTINCT FROM NEW.salary_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1809, OLD.salary_amount);
    changes = changes + 1;
  END

  /* STANDARD_HOURS */
  IF (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1808, OLD.standard_hours);
    changes = changes + 1;
  END

  /* NEXT_RAISE_AMOUNT */
  IF (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);
    changes = changes + 1;
  END

  /* NEXT_RAISE_PERCENTAGE */
  IF (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);
    changes = changes + 1;
  END

  /* NEXT_RAISE_RATE */
  IF (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);
    changes = changes + 1;
  END

  /* WORKERS_COMP_WAGE_LIMIT */
  IF (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);
    changes = changes + 1;
  END

  /* GROUP_TERM_POLICY_AMOUNT */
  IF (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_VALUE */
  IF (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);
    changes = changes + 1;
  END

  /* CALCULATED_SALARY */
  IF (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1801, OLD.calculated_salary);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* HIGHLY_COMPENSATED */
  IF (OLD.highly_compensated IS DISTINCT FROM NEW.highly_compensated) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1919, OLD.highly_compensated);
    changes = changes + 1;
  END

  /* CORPORATE_OFFICER */
  IF (OLD.corporate_officer IS DISTINCT FROM NEW.corporate_officer) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1920, OLD.corporate_officer);
    changes = changes + 1;
  END

  /* CO_JOBS_NBR */
  IF (OLD.co_jobs_nbr IS DISTINCT FROM NEW.co_jobs_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* EE_ENABLED */
  IF (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1921, OLD.ee_enabled);
    changes = changes + 1;
  END

  /* GTL_NUMBER_OF_HOURS */
  IF (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);
    changes = changes + 1;
  END

  /* GTL_RATE */
  IF (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1817, OLD.gtl_rate);
    changes = changes + 1;
  END

  /* AUTO_UPDATE_RATES */
  IF (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);
    changes = changes + 1;
  END

  /* ELIGIBLE_FOR_REHIRE */
  IF (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);
    changes = changes + 1;
  END

  /* SELFSERVE_ENABLED */
  IF (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);
    changes = changes + 1;
  END

  /* SELFSERVE_USERNAME */
  IF (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1868, OLD.selfserve_username);
    changes = changes + 1;
  END

  /* SELFSERVE_PASSWORD */
  IF (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1867, OLD.selfserve_password);
    changes = changes + 1;
  END

  /* SELFSERVE_LAST_LOGIN */
  IF (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);
    changes = changes + 1;
  END

  /* PRINT_VOUCHER */
  IF (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1882, OLD.print_voucher);
    changes = changes + 1;
  END

  /* HEALTHCARE_COVERAGE */
  IF (OLD.healthcare_coverage IS DISTINCT FROM NEW.healthcare_coverage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);
    changes = changes + 1;
  END

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF (OLD.fui_rate_credit_override IS DISTINCT FROM NEW.fui_rate_credit_override) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);
    changes = changes + 1;
  END

  /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
  IF (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);
    changes = changes + 1;
  END

  /* WC_WAGE_LIMIT_FREQUENCY */
  IF (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1864, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1879, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1865, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1880, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1866, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1881, OLD.login_answer3);
    changes = changes + 1;
  END

  /* LOGIN_DATE */
  IF (OLD.login_date IS DISTINCT FROM NEW.login_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2785, OLD.login_date);
    changes = changes + 1;
  END

  /* LOGIN_ATTEMPTS */
  IF (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2786, OLD.login_attempts);
    changes = changes + 1;
  END

  /* CO_BENEFIT_DISCOUNT_NBR */
  IF (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);
    changes = changes + 1;
  END

  /* BENEFITS_ENABLED */
  IF (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);
    changes = changes + 1;
  END

  /* EXISTING_PATIENT */
  IF (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2914, OLD.existing_patient);
    changes = changes + 1;
  END

  /* PCP */
  IF (OLD.pcp IS DISTINCT FROM NEW.pcp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2915, OLD.pcp);
    changes = changes + 1;
  END

  /* TIME_OFF_ENABLED */
  IF (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAILABLE */
  IF (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAIL_DATE */
  IF (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);
    changes = changes + 1;
  END

  /* BENEFIT_ENROLLMENT_COMPLETE */
  IF (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT */
  IF (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT_DATE */
  IF (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);
    changes = changes + 1;
  END

  /* W2 */
  IF (OLD.w2 IS DISTINCT FROM NEW.w2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2922, OLD.w2);
    changes = changes + 1;
  END

  /* W2_FORM_ON_FILE */
  IF (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);
    changes = changes + 1;
  END

  /* BENEFIT_E_MAIL_ADDRESS */
  IF (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);
    changes = changes + 1;
  END

  /* CO_HR_POSITION_GRADES_NBR */
  IF (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2926, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2927, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2928, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2929, OLD.sec_answer2);
    changes = changes + 1;
  END

  /* ACA_STATUS */
  IF (OLD.aca_status IS DISTINCT FROM NEW.aca_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3188, OLD.aca_status);
    changes = changes + 1;
  END

  /* WORKERS_COMP_MIN_WAGE_LIMIT */
  IF (OLD.workers_comp_min_wage_limit IS DISTINCT FROM NEW.workers_comp_min_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3189, OLD.workers_comp_min_wage_limit);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE_ACCEPT_DATE */
  IF (OLD.ess_terms_of_use_accept_date IS DISTINCT FROM NEW.ess_terms_of_use_accept_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3192, OLD.ess_terms_of_use_accept_date);
    changes = changes + 1;
  END

  /* ACA_STANDARD_HOURS */
  IF (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3218, OLD.aca_standard_hours);
    changes = changes + 1;
  END

  /* ACA_COVERAGE_OFFER */
  IF (OLD.aca_coverage_offer IS DISTINCT FROM NEW.aca_coverage_offer) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3237, OLD.aca_coverage_offer);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AIU_EE_3 FOR EE After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) OR (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) OR (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) OR (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) OR (OLD.address1 IS DISTINCT FROM NEW.address1) OR (OLD.address2 IS DISTINCT FROM NEW.address2) OR (OLD.city IS DISTINCT FROM NEW.city) OR (OLD.state IS DISTINCT FROM NEW.state) OR (OLD.zip_code IS DISTINCT FROM NEW.zip_code) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.phone1 IS DISTINCT FROM NEW.phone1) OR (OLD.description1 IS DISTINCT FROM NEW.description1) OR (OLD.phone2 IS DISTINCT FROM NEW.phone2) OR (OLD.description2 IS DISTINCT FROM NEW.description2) OR (OLD.phone3 IS DISTINCT FROM NEW.phone3) OR (OLD.description3 IS DISTINCT FROM NEW.description3) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) OR (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) OR (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) OR (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) OR (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) OR (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) OR (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) OR (OLD.position_status IS DISTINCT FROM NEW.position_status) OR (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) OR (OLD.review_date IS DISTINCT FROM NEW.review_date) OR (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) OR (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) OR (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) OR (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) OR (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) OR (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) OR (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) OR (OLD.eic IS DISTINCT FROM NEW.eic) OR (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) OR (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) OR (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) OR (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) OR (OLD.badge_id IS DISTINCT FROM NEW.badge_id) OR (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) OR (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) OR (rdb$get_context('USER_TRANSACTION', '@NOTES') IS NOT NULL) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) OR (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) OR (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) OR (rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES') IS NOT NULL) OR (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) OR (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) OR (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) OR (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) OR (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) OR (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) OR (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) OR (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) OR (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) OR (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) OR (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) OR (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) OR (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) OR (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) OR (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) OR (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) OR (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) OR (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) OR (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) OR (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) OR (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) OR (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) OR (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) OR (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) OR (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) OR (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) OR (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) OR (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) OR (OLD.login_date IS DISTINCT FROM NEW.login_date) OR (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) OR (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) OR (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) OR (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) OR (OLD.pcp IS DISTINCT FROM NEW.pcp) OR (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) OR (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) OR (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) OR (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) OR (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) OR (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) OR (OLD.w2 IS DISTINCT FROM NEW.w2) OR (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) OR (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) OR (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) OR (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) OR (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) OR (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) OR (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) OR (OLD.workers_comp_min_wage_limit IS DISTINCT FROM NEW.workers_comp_min_wage_limit) OR (OLD.ess_terms_of_use_accept_date IS DISTINCT FROM NEW.ess_terms_of_use_accept_date) OR (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE ee SET 
      co_nbr = NEW.co_nbr, custom_employee_number = NEW.custom_employee_number, co_hr_applicant_nbr = NEW.co_hr_applicant_nbr, co_hr_recruiters_nbr = NEW.co_hr_recruiters_nbr, co_hr_referrals_nbr = NEW.co_hr_referrals_nbr, address1 = NEW.address1, address2 = NEW.address2, city = NEW.city, state = NEW.state, zip_code = NEW.zip_code, county = NEW.county, phone1 = NEW.phone1, description1 = NEW.description1, phone2 = NEW.phone2, description2 = NEW.description2, phone3 = NEW.phone3, description3 = NEW.description3, e_mail_address = NEW.e_mail_address, time_clock_number = NEW.time_clock_number, original_hire_date = NEW.original_hire_date, current_hire_date = NEW.current_hire_date, current_termination_date = NEW.current_termination_date, co_hr_supervisors_nbr = NEW.co_hr_supervisors_nbr, autopay_co_shifts_nbr = NEW.autopay_co_shifts_nbr, position_effective_date = NEW.position_effective_date, position_status = NEW.position_status, co_hr_performance_ratings_nbr = NEW.co_hr_performance_ratings_nbr, review_date = NEW.review_date, next_review_date = NEW.next_review_date, pay_frequency = NEW.pay_frequency, next_raise_date = NEW.next_raise_date, next_pay_frequency = NEW.next_pay_frequency, ald_cl_e_d_groups_nbr = NEW.ald_cl_e_d_groups_nbr, distribute_taxes = NEW.distribute_taxes, co_pay_group_nbr = NEW.co_pay_group_nbr, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, co_unions_nbr = NEW.co_unions_nbr, eic = NEW.eic, flsa_exempt = NEW.flsa_exempt, override_fed_tax_type = NEW.override_fed_tax_type, gov_garnish_prior_child_suppt = NEW.gov_garnish_prior_child_suppt, security_clearance = NEW.security_clearance, badge_id = NEW.badge_id, on_call_from = NEW.on_call_from, on_call_to = NEW.on_call_to, notes = NEW.notes, filler = NEW.filler, general_ledger_tag = NEW.general_ledger_tag, federal_marital_status = NEW.federal_marital_status, number_of_dependents = NEW.number_of_dependents, sy_hr_eeo_nbr = NEW.sy_hr_eeo_nbr, secondary_notes = NEW.secondary_notes, co_pr_check_templates_nbr = NEW.co_pr_check_templates_nbr, generate_second_check = NEW.generate_second_check, standard_hours = NEW.standard_hours, next_raise_amount = NEW.next_raise_amount, next_raise_percentage = NEW.next_raise_percentage, next_raise_rate = NEW.next_raise_rate, workers_comp_wage_limit = NEW.workers_comp_wage_limit, group_term_policy_amount = NEW.group_term_policy_amount, override_fed_tax_value = NEW.override_fed_tax_value, calculated_salary = NEW.calculated_salary, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_ee_report_mb_group_nbr = NEW.pr_ee_report_mb_group_nbr, pr_ee_report_sec_mb_group_nbr = NEW.pr_ee_report_sec_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, ee_enabled = NEW.ee_enabled, gtl_number_of_hours = NEW.gtl_number_of_hours, gtl_rate = NEW.gtl_rate, auto_update_rates = NEW.auto_update_rates, eligible_for_rehire = NEW.eligible_for_rehire, selfserve_enabled = NEW.selfserve_enabled, selfserve_username = NEW.selfserve_username, selfserve_password = NEW.selfserve_password, selfserve_last_login = NEW.selfserve_last_login, print_voucher = NEW.print_voucher, override_federal_minimum_wage = NEW.override_federal_minimum_wage, wc_wage_limit_frequency = NEW.wc_wage_limit_frequency, login_question1 = NEW.login_question1, login_answer1 = NEW.login_answer1, login_question2 = NEW.login_question2, login_answer2 = NEW.login_answer2, login_question3 = NEW.login_question3, login_answer3 = NEW.login_answer3, login_date = NEW.login_date, login_attempts = NEW.login_attempts, co_benefit_discount_nbr = NEW.co_benefit_discount_nbr, benefits_enabled = NEW.benefits_enabled, existing_patient = NEW.existing_patient, pcp = NEW.pcp, time_off_enabled = NEW.time_off_enabled, dependent_benefits_available = NEW.dependent_benefits_available, dependent_benefits_avail_date = NEW.dependent_benefits_avail_date, benefit_enrollment_complete = NEW.benefit_enrollment_complete, last_qual_benefit_event = NEW.last_qual_benefit_event, last_qual_benefit_event_date = NEW.last_qual_benefit_event_date, w2 = NEW.w2, w2_form_on_file = NEW.w2_form_on_file, benefit_e_mail_address = NEW.benefit_e_mail_address, co_hr_position_grades_nbr = NEW.co_hr_position_grades_nbr, sec_question1 = NEW.sec_question1, sec_answer1 = NEW.sec_answer1, sec_question2 = NEW.sec_question2, sec_answer2 = NEW.sec_answer2, workers_comp_min_wage_limit = NEW.workers_comp_min_wage_limit, ess_terms_of_use_accept_date = NEW.ess_terms_of_use_accept_date, aca_standard_hours = NEW.aca_standard_hours
      WHERE ee_nbr = NEW.ee_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^


CREATE TRIGGER T_AD_EE_BENEFIT_REFUSAL_9 FOR EE_BENEFIT_REFUSAL After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(170, OLD.rec_version, OLD.ee_benefit_refusal_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3127, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3128, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3129, OLD.ee_nbr);

  /* CO_BENEFIT_ENROLLMENT_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3223, OLD.co_benefit_enrollment_nbr);

  /* SY_HR_REFUSAL_REASON_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3131, OLD.sy_hr_refusal_reason_nbr);

  /* HOME_STATE_NBR */
  IF (OLD.home_state_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3132, OLD.home_state_nbr);

  /* SUI_STATE_NBR */
  IF (OLD.sui_state_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3133, OLD.sui_state_nbr);

  /* STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3221, OLD.status);

  /* STATUS_DATE */
  IF (OLD.status_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3222, OLD.status_date);

END

^

CREATE TRIGGER T_AU_EE_BENEFIT_REFUSAL_9 FOR EE_BENEFIT_REFUSAL After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(170, NEW.rec_version, NEW.ee_benefit_refusal_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3127, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3128, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3129, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* CO_BENEFIT_ENROLLMENT_NBR */
  IF (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3223, OLD.co_benefit_enrollment_nbr);
    changes = changes + 1;
  END

  /* SY_HR_REFUSAL_REASON_NBR */
  IF (OLD.sy_hr_refusal_reason_nbr IS DISTINCT FROM NEW.sy_hr_refusal_reason_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3131, OLD.sy_hr_refusal_reason_nbr);
    changes = changes + 1;
  END

  /* HOME_STATE_NBR */
  IF (OLD.home_state_nbr IS DISTINCT FROM NEW.home_state_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3132, OLD.home_state_nbr);
    changes = changes + 1;
  END

  /* SUI_STATE_NBR */
  IF (OLD.sui_state_nbr IS DISTINCT FROM NEW.sui_state_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3133, OLD.sui_state_nbr);
    changes = changes + 1;
  END

  /* STATUS */
  IF (OLD.status IS DISTINCT FROM NEW.status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3221, OLD.status);
    changes = changes + 1;
  END

  /* STATUS_DATE */
  IF (OLD.status_date IS DISTINCT FROM NEW.status_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3222, OLD.status_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_BENEFIT_REFUSAL_2 FOR EE_BENEFIT_REFUSAL Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to EE */
  IF ((NEW.ee_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_date);

    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_until - 1);
  END

  /* Check reference to EE_STATES */
  IF ((NEW.home_state_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_state_nbr IS DISTINCT FROM NEW.home_state_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee_states
    WHERE ee_states_nbr = NEW.home_state_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'home_state_nbr', NEW.home_state_nbr, 'ee_states', NEW.effective_date);

    SELECT rec_version FROM ee_states
    WHERE ee_states_nbr = NEW.home_state_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'home_state_nbr', NEW.home_state_nbr, 'ee_states', NEW.effective_until - 1);
  END

  /* Check reference to EE_STATES */
  IF ((NEW.sui_state_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sui_state_nbr IS DISTINCT FROM NEW.sui_state_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee_states
    WHERE ee_states_nbr = NEW.sui_state_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'sui_state_nbr', NEW.sui_state_nbr, 'ee_states', NEW.effective_date);

    SELECT rec_version FROM ee_states
    WHERE ee_states_nbr = NEW.sui_state_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'sui_state_nbr', NEW.sui_state_nbr, 'ee_states', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_ENROLLMENT */
  IF ((NEW.co_benefit_enrollment_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_enrollment
    WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'co_benefit_enrollment_nbr', NEW.co_benefit_enrollment_nbr, 'co_benefit_enrollment', NEW.effective_date);

    SELECT rec_version FROM co_benefit_enrollment
    WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefit_refusal', NEW.ee_benefit_refusal_nbr, 'co_benefit_enrollment_nbr', NEW.co_benefit_enrollment_nbr, 'co_benefit_enrollment', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BIU_EE_BENEFIT_REFUSAL_3 FOR EE_BENEFIT_REFUSAL Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_EE_BENEFIT_REFUSAL */
  IF (INSERTING OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) OR (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr) OR (OLD.sy_hr_refusal_reason_nbr IS DISTINCT FROM NEW.sy_hr_refusal_reason_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM ee_benefit_refusal WHERE ee_nbr = NEW.ee_nbr AND co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND sy_hr_refusal_reason_nbr = NEW.sy_hr_refusal_reason_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('ee_benefit_refusal', 'ee_nbr, co_benefit_enrollment_nbr, sy_hr_refusal_reason_nbr',
      CAST(NEW.ee_nbr || ', ' || NEW.co_benefit_enrollment_nbr || ', ' || NEW.sy_hr_refusal_reason_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_AD_EE_CHANGE_REQUEST_9 FOR EE_CHANGE_REQUEST After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(120, OLD.rec_version, OLD.ee_change_request_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1980, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2796, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1983, OLD.ee_nbr);

  /* REQUEST_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1984, OLD.request_type);

  /* REQUEST_DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REQUEST_DATA');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@REQUEST_DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1981, :blob_nbr);
  END

  /* SIGNATURE_NBR */
  IF (OLD.signature_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2901, OLD.signature_nbr);

  /* DESCRIPTION */
  IF (OLD.description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2902, OLD.description);

  /* STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3225, OLD.status);

  /* STATUS_DATE */
  IF (OLD.status_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3226, OLD.status_date);

END

^

CREATE TRIGGER T_AU_EE_CHANGE_REQUEST_9 FOR EE_CHANGE_REQUEST After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(120, NEW.rec_version, NEW.ee_change_request_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1980, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2796, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1983, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* REQUEST_TYPE */
  IF (OLD.request_type IS DISTINCT FROM NEW.request_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1984, OLD.request_type);
    changes = changes + 1;
  END

  /* REQUEST_DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REQUEST_DATA');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@REQUEST_DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1981, :blob_nbr);
    changes = changes + 1;
  END

  /* SIGNATURE_NBR */
  IF (OLD.signature_nbr IS DISTINCT FROM NEW.signature_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2901, OLD.signature_nbr);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2902, OLD.description);
    changes = changes + 1;
  END

  /* STATUS */
  IF (OLD.status IS DISTINCT FROM NEW.status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3225, OLD.status);
    changes = changes + 1;
  END

  /* STATUS_DATE */
  IF (OLD.status_date IS DISTINCT FROM NEW.status_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3226, OLD.status_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_EE_DIRECT_DEPOSIT_9 FOR EE_DIRECT_DEPOSIT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(122, OLD.rec_version, OLD.ee_direct_deposit_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1997, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2800, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1999, OLD.ee_nbr);

  /* EE_BANK_ACCOUNT_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2000, OLD.ee_bank_account_number);

  /* EE_ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2004, OLD.ee_aba_number);

  /* IN_PRENOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2005, OLD.in_prenote);

  /* ADDENDA */
  IF (OLD.addenda IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2002, OLD.addenda);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2001, OLD.filler);

  /* EE_BANK_ACCOUNT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2003, OLD.ee_bank_account_type);

  /* BRANCH_IDENTIFIER */
  IF (OLD.branch_identifier IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2006, OLD.branch_identifier);

  /* ALLOW_HYPHENS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2007, OLD.allow_hyphens);

  /* FORM_ON_FILE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2008, OLD.form_on_file);

  /* IN_PRENOTE_DATE */
  IF (OLD.in_prenote_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3186, OLD.in_prenote_date);

  /* SHOW_IN_EE_PORTAL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3220, OLD.show_in_ee_portal);

END

^

CREATE TRIGGER T_AU_EE_DIRECT_DEPOSIT_9 FOR EE_DIRECT_DEPOSIT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(122, NEW.rec_version, NEW.ee_direct_deposit_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1997, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2800, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1999, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* EE_BANK_ACCOUNT_NUMBER */
  IF (OLD.ee_bank_account_number IS DISTINCT FROM NEW.ee_bank_account_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2000, OLD.ee_bank_account_number);
    changes = changes + 1;
  END

  /* EE_ABA_NUMBER */
  IF (OLD.ee_aba_number IS DISTINCT FROM NEW.ee_aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2004, OLD.ee_aba_number);
    changes = changes + 1;
  END

  /* IN_PRENOTE */
  IF (OLD.in_prenote IS DISTINCT FROM NEW.in_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2005, OLD.in_prenote);
    changes = changes + 1;
  END

  /* ADDENDA */
  IF (OLD.addenda IS DISTINCT FROM NEW.addenda) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2002, OLD.addenda);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2001, OLD.filler);
    changes = changes + 1;
  END

  /* EE_BANK_ACCOUNT_TYPE */
  IF (OLD.ee_bank_account_type IS DISTINCT FROM NEW.ee_bank_account_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2003, OLD.ee_bank_account_type);
    changes = changes + 1;
  END

  /* BRANCH_IDENTIFIER */
  IF (OLD.branch_identifier IS DISTINCT FROM NEW.branch_identifier) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2006, OLD.branch_identifier);
    changes = changes + 1;
  END

  /* ALLOW_HYPHENS */
  IF (OLD.allow_hyphens IS DISTINCT FROM NEW.allow_hyphens) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2007, OLD.allow_hyphens);
    changes = changes + 1;
  END

  /* FORM_ON_FILE */
  IF (OLD.form_on_file IS DISTINCT FROM NEW.form_on_file) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2008, OLD.form_on_file);
    changes = changes + 1;
  END

  /* IN_PRENOTE_DATE */
  IF (OLD.in_prenote_date IS DISTINCT FROM NEW.in_prenote_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3186, OLD.in_prenote_date);
    changes = changes + 1;
  END

  /* SHOW_IN_EE_PORTAL */
  IF (OLD.show_in_ee_portal IS DISTINCT FROM NEW.show_in_ee_portal) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3220, OLD.show_in_ee_portal);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_EE_SCHEDULED_E_DS_1 FOR EE_SCHEDULED_E_DS After Delete POSITION 1
AS
DECLARE VARIABLE effective_until DATE;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it's the first version record */
  IF (OLD.effective_date = '1/1/1900') THEN
  BEGIN
    IF (EXISTS (SELECT 1 FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = OLD.ee_scheduled_e_ds_nbr AND effective_date > OLD.effective_date)) THEN
      EXECUTE PROCEDURE raise_table_error('ee_scheduled_e_ds', OLD.rec_version, '', 'S5', 'First version record cannot be deleted.');
  END

  ELSE
  BEGIN
    /* Correct EFFECTIVE_UNTIL of previous version */
    effective_until = OLD.effective_until;
    SELECT effective_date FROM ee_scheduled_e_ds
    WHERE ee_scheduled_e_ds_nbr = OLD.ee_scheduled_e_ds_nbr AND
          effective_date > OLD.effective_date
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :effective_until;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

    UPDATE ee_scheduled_e_ds SET effective_until = :effective_until
    WHERE ee_scheduled_e_ds_nbr = OLD.ee_scheduled_e_ds_nbr AND effective_until = OLD.effective_date;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AD_EE_SCHEDULED_E_DS_9 FOR EE_SCHEDULED_E_DS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE ee_direct_deposit_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(134, OLD.rec_version, OLD.ee_scheduled_e_ds_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', ee_direct_deposit_nbr  FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = OLD.ee_scheduled_e_ds_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :ee_direct_deposit_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2165, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2824, OLD.effective_until);

  /* EE_DIRECT_DEPOSIT_NBR */
  IF ((last_record = 'Y' AND OLD.ee_direct_deposit_nbr IS NOT NULL) OR (last_record = 'N' AND ee_direct_deposit_nbr IS DISTINCT FROM OLD.ee_direct_deposit_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2175, OLD.ee_direct_deposit_nbr);


  IF (last_record = 'Y') THEN
  BEGIN
    /* EE_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2171, OLD.ee_nbr);

    /* CL_AGENCY_NBR */
    IF (OLD.cl_agency_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2172, OLD.cl_agency_nbr);

    /* CL_E_DS_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2173, OLD.cl_e_ds_nbr);

    /* CL_E_D_GROUPS_NBR */
    IF (OLD.cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2174, OLD.cl_e_d_groups_nbr);

    /* PLAN_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2189, OLD.plan_type);

    /* FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2190, OLD.frequency);

    /* EXCLUDE_WEEK_1 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2191, OLD.exclude_week_1);

    /* EXCLUDE_WEEK_2 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2192, OLD.exclude_week_2);

    /* EXCLUDE_WEEK_3 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2193, OLD.exclude_week_3);

    /* EXCLUDE_WEEK_4 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2194, OLD.exclude_week_4);

    /* EXCLUDE_WEEK_5 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2195, OLD.exclude_week_5);

    /* EFFECTIVE_START_DATE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2182, OLD.effective_start_date);

    /* EFFECTIVE_END_DATE */
    IF (OLD.effective_end_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2183, OLD.effective_end_date);

    /* MIN_PPP_CL_E_D_GROUPS_NBR */
    IF (OLD.min_ppp_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2176, OLD.min_ppp_cl_e_d_groups_nbr);

    /* MAX_PPP_CL_E_D_GROUPS_NBR */
    IF (OLD.max_ppp_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2177, OLD.max_ppp_cl_e_d_groups_nbr);

    /* TARGET_ACTION */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2196, OLD.target_action);

    /* NUMBER_OF_TARGETS_REMAINING */
    IF (OLD.number_of_targets_remaining IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2178, OLD.number_of_targets_remaining);

    /* WHICH_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2197, OLD.which_checks);

    /* CALCULATION_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2198, OLD.calculation_type);

    /* EE_CHILD_SUPPORT_CASES_NBR */
    IF (OLD.ee_child_support_cases_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2179, OLD.ee_child_support_cases_nbr);

    /* CHILD_SUPPORT_STATE */
    IF (OLD.child_support_state IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2185, OLD.child_support_state);

    /* DEDUCT_WHOLE_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2199, OLD.deduct_whole_check);

    /* EXPRESSION */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@EXPRESSION');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@EXPRESSION', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2166, :blob_nbr);
    END

    /* ALWAYS_PAY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2200, OLD.always_pay);

    /* SCHEDULED_E_D_GROUPS_NBR */
    IF (OLD.scheduled_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2180, OLD.scheduled_e_d_groups_nbr);

    /* PRIORITY_NUMBER */
    IF (OLD.priority_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2181, OLD.priority_number);

    /* GARNISNMENT_ID */
    IF (OLD.garnisnment_id IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2150, OLD.garnisnment_id);

    /* THRESHOLD_E_D_GROUPS_NBR */
    IF (OLD.threshold_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2169, OLD.threshold_e_d_groups_nbr);

    /* BENEFIT_AMOUNT_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2188, OLD.benefit_amount_type);

    /* AMOUNT */
    IF (OLD.amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2163, OLD.amount);

    /* PERCENTAGE */
    IF (OLD.percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2164, OLD.percentage);

    /* MINIMUM_WAGE_MULTIPLIER */
    IF (OLD.minimum_wage_multiplier IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2162, OLD.minimum_wage_multiplier);

    /* TARGET_AMOUNT */
    IF (OLD.target_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2161, OLD.target_amount);

    /* MINIMUM_PAY_PERIOD_AMOUNT */
    IF (OLD.minimum_pay_period_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2160, OLD.minimum_pay_period_amount);

    /* MINIMUM_PAY_PERIOD_PERCENTAGE */
    IF (OLD.minimum_pay_period_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2159, OLD.minimum_pay_period_percentage);

    /* MAXIMUM_PAY_PERIOD_AMOUNT */
    IF (OLD.maximum_pay_period_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2158, OLD.maximum_pay_period_amount);

    /* MAXIMUM_PAY_PERIOD_PERCENTAGE */
    IF (OLD.maximum_pay_period_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2157, OLD.maximum_pay_period_percentage);

    /* ANNUAL_MAXIMUM_AMOUNT */
    IF (OLD.annual_maximum_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2156, OLD.annual_maximum_amount);

    /* MAXIMUM_GARNISH_PERCENT */
    IF (OLD.maximum_garnish_percent IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2155, OLD.maximum_garnish_percent);

    /* TAKE_HOME_PAY */
    IF (OLD.take_home_pay IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2154, OLD.take_home_pay);

    /* BALANCE */
    IF (OLD.balance IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2152, OLD.balance);

    /* THRESHOLD_AMOUNT */
    IF (OLD.threshold_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2153, OLD.threshold_amount);

    /* SCHEDULED_E_D_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2201, OLD.scheduled_e_d_enabled);

    /* CAP_STATE_TAX_CREDIT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2202, OLD.cap_state_tax_credit);

    /* MAX_AVG_AMT_CL_E_D_GROUPS_NBR */
    IF (OLD.max_avg_amt_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2168, OLD.max_avg_amt_cl_e_d_groups_nbr);

    /* MAX_AVG_HRS_CL_E_D_GROUPS_NBR */
    IF (OLD.max_avg_hrs_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2167, OLD.max_avg_hrs_cl_e_d_groups_nbr);

    /* MAX_AVERAGE_HOURLY_WAGE_RATE */
    IF (OLD.max_average_hourly_wage_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2151, OLD.max_average_hourly_wage_rate);

    /* DEDUCTIONS_TO_ZERO */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2187, OLD.deductions_to_zero);

    /* USE_PENSION_LIMIT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2186, OLD.use_pension_limit);

    /* EE_BENEFITS_NBR */
    IF (OLD.ee_benefits_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2184, OLD.ee_benefits_nbr);

    /* CO_BENEFIT_SUBTYPE_NBR */
    IF (OLD.co_benefit_subtype_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2937, OLD.co_benefit_subtype_nbr);

    /* MINIMUM_HW_HOURS */
    IF (OLD.minimum_hw_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3232, OLD.minimum_hw_hours);

    /* MAXIMUM_HW_HOURS */
    IF (OLD.maximum_hw_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3233, OLD.maximum_hw_hours);

    /* HW_FUNDING_CL_E_D_GROUPS_NBR */
    IF (OLD.hw_funding_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3234, OLD.hw_funding_cl_e_d_groups_nbr);

    /* HW_EXCESS_CL_E_D_GROUPS_NBR */
    IF (OLD.hw_excess_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3235, OLD.hw_excess_cl_e_d_groups_nbr);

  END
END

^

CREATE TRIGGER T_AIU_EE_SCHEDULED_E_DS_3 FOR EE_SCHEDULED_E_DS After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) OR (OLD.cl_agency_nbr IS DISTINCT FROM NEW.cl_agency_nbr) OR (OLD.cl_e_ds_nbr IS DISTINCT FROM NEW.cl_e_ds_nbr) OR (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) OR (OLD.plan_type IS DISTINCT FROM NEW.plan_type) OR (OLD.frequency IS DISTINCT FROM NEW.frequency) OR (OLD.exclude_week_1 IS DISTINCT FROM NEW.exclude_week_1) OR (OLD.exclude_week_2 IS DISTINCT FROM NEW.exclude_week_2) OR (OLD.exclude_week_3 IS DISTINCT FROM NEW.exclude_week_3) OR (OLD.exclude_week_4 IS DISTINCT FROM NEW.exclude_week_4) OR (OLD.exclude_week_5 IS DISTINCT FROM NEW.exclude_week_5) OR (OLD.effective_start_date IS DISTINCT FROM NEW.effective_start_date) OR (OLD.effective_end_date IS DISTINCT FROM NEW.effective_end_date) OR (OLD.min_ppp_cl_e_d_groups_nbr IS DISTINCT FROM NEW.min_ppp_cl_e_d_groups_nbr) OR (OLD.max_ppp_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_ppp_cl_e_d_groups_nbr) OR (OLD.target_action IS DISTINCT FROM NEW.target_action) OR (OLD.number_of_targets_remaining IS DISTINCT FROM NEW.number_of_targets_remaining) OR (OLD.which_checks IS DISTINCT FROM NEW.which_checks) OR (OLD.calculation_type IS DISTINCT FROM NEW.calculation_type) OR (OLD.ee_child_support_cases_nbr IS DISTINCT FROM NEW.ee_child_support_cases_nbr) OR (OLD.child_support_state IS DISTINCT FROM NEW.child_support_state) OR (OLD.deduct_whole_check IS DISTINCT FROM NEW.deduct_whole_check) OR (rdb$get_context('USER_TRANSACTION', '@EXPRESSION') IS NOT NULL) OR (OLD.always_pay IS DISTINCT FROM NEW.always_pay) OR (OLD.scheduled_e_d_groups_nbr IS DISTINCT FROM NEW.scheduled_e_d_groups_nbr) OR (OLD.priority_number IS DISTINCT FROM NEW.priority_number) OR (OLD.garnisnment_id IS DISTINCT FROM NEW.garnisnment_id) OR (OLD.threshold_e_d_groups_nbr IS DISTINCT FROM NEW.threshold_e_d_groups_nbr) OR (OLD.benefit_amount_type IS DISTINCT FROM NEW.benefit_amount_type) OR (OLD.amount IS DISTINCT FROM NEW.amount) OR (OLD.percentage IS DISTINCT FROM NEW.percentage) OR (OLD.minimum_wage_multiplier IS DISTINCT FROM NEW.minimum_wage_multiplier) OR (OLD.target_amount IS DISTINCT FROM NEW.target_amount) OR (OLD.minimum_pay_period_amount IS DISTINCT FROM NEW.minimum_pay_period_amount) OR (OLD.minimum_pay_period_percentage IS DISTINCT FROM NEW.minimum_pay_period_percentage) OR (OLD.maximum_pay_period_amount IS DISTINCT FROM NEW.maximum_pay_period_amount) OR (OLD.maximum_pay_period_percentage IS DISTINCT FROM NEW.maximum_pay_period_percentage) OR (OLD.annual_maximum_amount IS DISTINCT FROM NEW.annual_maximum_amount) OR (OLD.maximum_garnish_percent IS DISTINCT FROM NEW.maximum_garnish_percent) OR (OLD.take_home_pay IS DISTINCT FROM NEW.take_home_pay) OR (OLD.balance IS DISTINCT FROM NEW.balance) OR (OLD.threshold_amount IS DISTINCT FROM NEW.threshold_amount) OR (OLD.scheduled_e_d_enabled IS DISTINCT FROM NEW.scheduled_e_d_enabled) OR (OLD.cap_state_tax_credit IS DISTINCT FROM NEW.cap_state_tax_credit) OR (OLD.max_avg_amt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_avg_amt_cl_e_d_groups_nbr) OR (OLD.max_avg_hrs_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_avg_hrs_cl_e_d_groups_nbr) OR (OLD.max_average_hourly_wage_rate IS DISTINCT FROM NEW.max_average_hourly_wage_rate) OR (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) OR (OLD.use_pension_limit IS DISTINCT FROM NEW.use_pension_limit) OR (OLD.ee_benefits_nbr IS DISTINCT FROM NEW.ee_benefits_nbr) OR (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr) OR (OLD.minimum_hw_hours IS DISTINCT FROM NEW.minimum_hw_hours) OR (OLD.maximum_hw_hours IS DISTINCT FROM NEW.maximum_hw_hours) OR (OLD.hw_funding_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_funding_cl_e_d_groups_nbr) OR (OLD.hw_excess_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_excess_cl_e_d_groups_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE ee_scheduled_e_ds SET 
      ee_nbr = NEW.ee_nbr, cl_agency_nbr = NEW.cl_agency_nbr, cl_e_ds_nbr = NEW.cl_e_ds_nbr, cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr, plan_type = NEW.plan_type, frequency = NEW.frequency, exclude_week_1 = NEW.exclude_week_1, exclude_week_2 = NEW.exclude_week_2, exclude_week_3 = NEW.exclude_week_3, exclude_week_4 = NEW.exclude_week_4, exclude_week_5 = NEW.exclude_week_5, effective_start_date = NEW.effective_start_date, effective_end_date = NEW.effective_end_date, min_ppp_cl_e_d_groups_nbr = NEW.min_ppp_cl_e_d_groups_nbr, max_ppp_cl_e_d_groups_nbr = NEW.max_ppp_cl_e_d_groups_nbr, target_action = NEW.target_action, number_of_targets_remaining = NEW.number_of_targets_remaining, which_checks = NEW.which_checks, calculation_type = NEW.calculation_type, ee_child_support_cases_nbr = NEW.ee_child_support_cases_nbr, child_support_state = NEW.child_support_state, deduct_whole_check = NEW.deduct_whole_check, expression = NEW.expression, always_pay = NEW.always_pay, scheduled_e_d_groups_nbr = NEW.scheduled_e_d_groups_nbr, priority_number = NEW.priority_number, garnisnment_id = NEW.garnisnment_id, threshold_e_d_groups_nbr = NEW.threshold_e_d_groups_nbr, benefit_amount_type = NEW.benefit_amount_type, amount = NEW.amount, percentage = NEW.percentage, minimum_wage_multiplier = NEW.minimum_wage_multiplier, target_amount = NEW.target_amount, minimum_pay_period_amount = NEW.minimum_pay_period_amount, minimum_pay_period_percentage = NEW.minimum_pay_period_percentage, maximum_pay_period_amount = NEW.maximum_pay_period_amount, maximum_pay_period_percentage = NEW.maximum_pay_period_percentage, annual_maximum_amount = NEW.annual_maximum_amount, maximum_garnish_percent = NEW.maximum_garnish_percent, take_home_pay = NEW.take_home_pay, balance = NEW.balance, threshold_amount = NEW.threshold_amount, scheduled_e_d_enabled = NEW.scheduled_e_d_enabled, cap_state_tax_credit = NEW.cap_state_tax_credit, max_avg_amt_cl_e_d_groups_nbr = NEW.max_avg_amt_cl_e_d_groups_nbr, max_avg_hrs_cl_e_d_groups_nbr = NEW.max_avg_hrs_cl_e_d_groups_nbr, max_average_hourly_wage_rate = NEW.max_average_hourly_wage_rate, deductions_to_zero = NEW.deductions_to_zero, use_pension_limit = NEW.use_pension_limit, ee_benefits_nbr = NEW.ee_benefits_nbr, co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr, minimum_hw_hours = NEW.minimum_hw_hours, maximum_hw_hours = NEW.maximum_hw_hours, hw_funding_cl_e_d_groups_nbr = NEW.hw_funding_cl_e_d_groups_nbr, hw_excess_cl_e_d_groups_nbr = NEW.hw_excess_cl_e_d_groups_nbr
      WHERE ee_scheduled_e_ds_nbr = NEW.ee_scheduled_e_ds_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_EE_SCHEDULED_E_DS_1 FOR EE_SCHEDULED_E_DS After Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((OLD.effective_date IS DISTINCT FROM NEW.effective_date) AND
      (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL of old previous version */
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE ee_scheduled_e_ds SET effective_until = OLD.effective_until
    WHERE ee_scheduled_e_ds_nbr = NEW.ee_scheduled_e_ds_nbr AND effective_until = OLD.effective_date;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AU_EE_SCHEDULED_E_DS_9 FOR EE_SCHEDULED_E_DS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(134, NEW.rec_version, NEW.ee_scheduled_e_ds_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2165, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2824, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2171, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* CL_AGENCY_NBR */
  IF (OLD.cl_agency_nbr IS DISTINCT FROM NEW.cl_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2172, OLD.cl_agency_nbr);
    changes = changes + 1;
  END

  /* CL_E_DS_NBR */
  IF (OLD.cl_e_ds_nbr IS DISTINCT FROM NEW.cl_e_ds_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2173, OLD.cl_e_ds_nbr);
    changes = changes + 1;
  END

  /* CL_E_D_GROUPS_NBR */
  IF (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2174, OLD.cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* EE_DIRECT_DEPOSIT_NBR */
  IF (OLD.ee_direct_deposit_nbr IS DISTINCT FROM NEW.ee_direct_deposit_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2175, OLD.ee_direct_deposit_nbr);
    changes = changes + 1;
  END

  /* PLAN_TYPE */
  IF (OLD.plan_type IS DISTINCT FROM NEW.plan_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2189, OLD.plan_type);
    changes = changes + 1;
  END

  /* FREQUENCY */
  IF (OLD.frequency IS DISTINCT FROM NEW.frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2190, OLD.frequency);
    changes = changes + 1;
  END

  /* EXCLUDE_WEEK_1 */
  IF (OLD.exclude_week_1 IS DISTINCT FROM NEW.exclude_week_1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2191, OLD.exclude_week_1);
    changes = changes + 1;
  END

  /* EXCLUDE_WEEK_2 */
  IF (OLD.exclude_week_2 IS DISTINCT FROM NEW.exclude_week_2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2192, OLD.exclude_week_2);
    changes = changes + 1;
  END

  /* EXCLUDE_WEEK_3 */
  IF (OLD.exclude_week_3 IS DISTINCT FROM NEW.exclude_week_3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2193, OLD.exclude_week_3);
    changes = changes + 1;
  END

  /* EXCLUDE_WEEK_4 */
  IF (OLD.exclude_week_4 IS DISTINCT FROM NEW.exclude_week_4) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2194, OLD.exclude_week_4);
    changes = changes + 1;
  END

  /* EXCLUDE_WEEK_5 */
  IF (OLD.exclude_week_5 IS DISTINCT FROM NEW.exclude_week_5) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2195, OLD.exclude_week_5);
    changes = changes + 1;
  END

  /* EFFECTIVE_START_DATE */
  IF (OLD.effective_start_date IS DISTINCT FROM NEW.effective_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2182, OLD.effective_start_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_END_DATE */
  IF (OLD.effective_end_date IS DISTINCT FROM NEW.effective_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2183, OLD.effective_end_date);
    changes = changes + 1;
  END

  /* MIN_PPP_CL_E_D_GROUPS_NBR */
  IF (OLD.min_ppp_cl_e_d_groups_nbr IS DISTINCT FROM NEW.min_ppp_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2176, OLD.min_ppp_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* MAX_PPP_CL_E_D_GROUPS_NBR */
  IF (OLD.max_ppp_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_ppp_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2177, OLD.max_ppp_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* TARGET_ACTION */
  IF (OLD.target_action IS DISTINCT FROM NEW.target_action) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2196, OLD.target_action);
    changes = changes + 1;
  END

  /* NUMBER_OF_TARGETS_REMAINING */
  IF (OLD.number_of_targets_remaining IS DISTINCT FROM NEW.number_of_targets_remaining) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2178, OLD.number_of_targets_remaining);
    changes = changes + 1;
  END

  /* WHICH_CHECKS */
  IF (OLD.which_checks IS DISTINCT FROM NEW.which_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2197, OLD.which_checks);
    changes = changes + 1;
  END

  /* CALCULATION_TYPE */
  IF (OLD.calculation_type IS DISTINCT FROM NEW.calculation_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2198, OLD.calculation_type);
    changes = changes + 1;
  END

  /* EE_CHILD_SUPPORT_CASES_NBR */
  IF (OLD.ee_child_support_cases_nbr IS DISTINCT FROM NEW.ee_child_support_cases_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2179, OLD.ee_child_support_cases_nbr);
    changes = changes + 1;
  END

  /* CHILD_SUPPORT_STATE */
  IF (OLD.child_support_state IS DISTINCT FROM NEW.child_support_state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2185, OLD.child_support_state);
    changes = changes + 1;
  END

  /* DEDUCT_WHOLE_CHECK */
  IF (OLD.deduct_whole_check IS DISTINCT FROM NEW.deduct_whole_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2199, OLD.deduct_whole_check);
    changes = changes + 1;
  END

  /* EXPRESSION */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@EXPRESSION');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@EXPRESSION', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2166, :blob_nbr);
    changes = changes + 1;
  END

  /* ALWAYS_PAY */
  IF (OLD.always_pay IS DISTINCT FROM NEW.always_pay) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2200, OLD.always_pay);
    changes = changes + 1;
  END

  /* SCHEDULED_E_D_GROUPS_NBR */
  IF (OLD.scheduled_e_d_groups_nbr IS DISTINCT FROM NEW.scheduled_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2180, OLD.scheduled_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* PRIORITY_NUMBER */
  IF (OLD.priority_number IS DISTINCT FROM NEW.priority_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2181, OLD.priority_number);
    changes = changes + 1;
  END

  /* GARNISNMENT_ID */
  IF (OLD.garnisnment_id IS DISTINCT FROM NEW.garnisnment_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2150, OLD.garnisnment_id);
    changes = changes + 1;
  END

  /* THRESHOLD_E_D_GROUPS_NBR */
  IF (OLD.threshold_e_d_groups_nbr IS DISTINCT FROM NEW.threshold_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2169, OLD.threshold_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* BENEFIT_AMOUNT_TYPE */
  IF (OLD.benefit_amount_type IS DISTINCT FROM NEW.benefit_amount_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2188, OLD.benefit_amount_type);
    changes = changes + 1;
  END

  /* AMOUNT */
  IF (OLD.amount IS DISTINCT FROM NEW.amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2163, OLD.amount);
    changes = changes + 1;
  END

  /* PERCENTAGE */
  IF (OLD.percentage IS DISTINCT FROM NEW.percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2164, OLD.percentage);
    changes = changes + 1;
  END

  /* MINIMUM_WAGE_MULTIPLIER */
  IF (OLD.minimum_wage_multiplier IS DISTINCT FROM NEW.minimum_wage_multiplier) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2162, OLD.minimum_wage_multiplier);
    changes = changes + 1;
  END

  /* TARGET_AMOUNT */
  IF (OLD.target_amount IS DISTINCT FROM NEW.target_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2161, OLD.target_amount);
    changes = changes + 1;
  END

  /* MINIMUM_PAY_PERIOD_AMOUNT */
  IF (OLD.minimum_pay_period_amount IS DISTINCT FROM NEW.minimum_pay_period_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2160, OLD.minimum_pay_period_amount);
    changes = changes + 1;
  END

  /* MINIMUM_PAY_PERIOD_PERCENTAGE */
  IF (OLD.minimum_pay_period_percentage IS DISTINCT FROM NEW.minimum_pay_period_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2159, OLD.minimum_pay_period_percentage);
    changes = changes + 1;
  END

  /* MAXIMUM_PAY_PERIOD_AMOUNT */
  IF (OLD.maximum_pay_period_amount IS DISTINCT FROM NEW.maximum_pay_period_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2158, OLD.maximum_pay_period_amount);
    changes = changes + 1;
  END

  /* MAXIMUM_PAY_PERIOD_PERCENTAGE */
  IF (OLD.maximum_pay_period_percentage IS DISTINCT FROM NEW.maximum_pay_period_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2157, OLD.maximum_pay_period_percentage);
    changes = changes + 1;
  END

  /* ANNUAL_MAXIMUM_AMOUNT */
  IF (OLD.annual_maximum_amount IS DISTINCT FROM NEW.annual_maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2156, OLD.annual_maximum_amount);
    changes = changes + 1;
  END

  /* MAXIMUM_GARNISH_PERCENT */
  IF (OLD.maximum_garnish_percent IS DISTINCT FROM NEW.maximum_garnish_percent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2155, OLD.maximum_garnish_percent);
    changes = changes + 1;
  END

  /* TAKE_HOME_PAY */
  IF (OLD.take_home_pay IS DISTINCT FROM NEW.take_home_pay) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2154, OLD.take_home_pay);
    changes = changes + 1;
  END

  /* BALANCE */
  IF (OLD.balance IS DISTINCT FROM NEW.balance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2152, OLD.balance);
    changes = changes + 1;
  END

  /* THRESHOLD_AMOUNT */
  IF (OLD.threshold_amount IS DISTINCT FROM NEW.threshold_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2153, OLD.threshold_amount);
    changes = changes + 1;
  END

  /* SCHEDULED_E_D_ENABLED */
  IF (OLD.scheduled_e_d_enabled IS DISTINCT FROM NEW.scheduled_e_d_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2201, OLD.scheduled_e_d_enabled);
    changes = changes + 1;
  END

  /* CAP_STATE_TAX_CREDIT */
  IF (OLD.cap_state_tax_credit IS DISTINCT FROM NEW.cap_state_tax_credit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2202, OLD.cap_state_tax_credit);
    changes = changes + 1;
  END

  /* MAX_AVG_AMT_CL_E_D_GROUPS_NBR */
  IF (OLD.max_avg_amt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_avg_amt_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2168, OLD.max_avg_amt_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* MAX_AVG_HRS_CL_E_D_GROUPS_NBR */
  IF (OLD.max_avg_hrs_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_avg_hrs_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2167, OLD.max_avg_hrs_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* MAX_AVERAGE_HOURLY_WAGE_RATE */
  IF (OLD.max_average_hourly_wage_rate IS DISTINCT FROM NEW.max_average_hourly_wage_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2151, OLD.max_average_hourly_wage_rate);
    changes = changes + 1;
  END

  /* DEDUCTIONS_TO_ZERO */
  IF (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2187, OLD.deductions_to_zero);
    changes = changes + 1;
  END

  /* USE_PENSION_LIMIT */
  IF (OLD.use_pension_limit IS DISTINCT FROM NEW.use_pension_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2186, OLD.use_pension_limit);
    changes = changes + 1;
  END

  /* EE_BENEFITS_NBR */
  IF (OLD.ee_benefits_nbr IS DISTINCT FROM NEW.ee_benefits_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2184, OLD.ee_benefits_nbr);
    changes = changes + 1;
  END

  /* CO_BENEFIT_SUBTYPE_NBR */
  IF (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2937, OLD.co_benefit_subtype_nbr);
    changes = changes + 1;
  END

  /* MINIMUM_HW_HOURS */
  IF (OLD.minimum_hw_hours IS DISTINCT FROM NEW.minimum_hw_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3232, OLD.minimum_hw_hours);
    changes = changes + 1;
  END

  /* MAXIMUM_HW_HOURS */
  IF (OLD.maximum_hw_hours IS DISTINCT FROM NEW.maximum_hw_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3233, OLD.maximum_hw_hours);
    changes = changes + 1;
  END

  /* HW_FUNDING_CL_E_D_GROUPS_NBR */
  IF (OLD.hw_funding_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_funding_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3234, OLD.hw_funding_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* HW_EXCESS_CL_E_D_GROUPS_NBR */
  IF (OLD.hw_excess_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_excess_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3235, OLD.hw_excess_cl_e_d_groups_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_SCHEDULED_E_DS_2 FOR EE_SCHEDULED_E_DS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CL_E_DS */
  IF ((NEW.cl_e_ds_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_e_ds_nbr IS DISTINCT FROM NEW.cl_e_ds_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.cl_e_ds_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'cl_e_ds_nbr', NEW.cl_e_ds_nbr, 'cl_e_ds', NEW.effective_date);

    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.cl_e_ds_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'cl_e_ds_nbr', NEW.cl_e_ds_nbr, 'cl_e_ds', NEW.effective_until - 1);
  END

  /* Check reference to CL_AGENCY */
  IF ((NEW.cl_agency_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_agency_nbr IS DISTINCT FROM NEW.cl_agency_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.cl_agency_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'cl_agency_nbr', NEW.cl_agency_nbr, 'cl_agency', NEW.effective_date);

    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.cl_agency_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'cl_agency_nbr', NEW.cl_agency_nbr, 'cl_agency', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'cl_e_d_groups_nbr', NEW.cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'cl_e_d_groups_nbr', NEW.cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.max_ppp_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.max_ppp_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_ppp_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_ppp_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'max_ppp_cl_e_d_groups_nbr', NEW.max_ppp_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_ppp_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'max_ppp_cl_e_d_groups_nbr', NEW.max_ppp_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.min_ppp_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.min_ppp_cl_e_d_groups_nbr IS DISTINCT FROM NEW.min_ppp_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.min_ppp_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'min_ppp_cl_e_d_groups_nbr', NEW.min_ppp_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.min_ppp_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'min_ppp_cl_e_d_groups_nbr', NEW.min_ppp_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.scheduled_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.scheduled_e_d_groups_nbr IS DISTINCT FROM NEW.scheduled_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.scheduled_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'scheduled_e_d_groups_nbr', NEW.scheduled_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.scheduled_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'scheduled_e_d_groups_nbr', NEW.scheduled_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.max_avg_amt_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.max_avg_amt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_avg_amt_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_avg_amt_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'max_avg_amt_cl_e_d_groups_nbr', NEW.max_avg_amt_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_avg_amt_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'max_avg_amt_cl_e_d_groups_nbr', NEW.max_avg_amt_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.max_avg_hrs_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.max_avg_hrs_cl_e_d_groups_nbr IS DISTINCT FROM NEW.max_avg_hrs_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_avg_hrs_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'max_avg_hrs_cl_e_d_groups_nbr', NEW.max_avg_hrs_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.max_avg_hrs_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'max_avg_hrs_cl_e_d_groups_nbr', NEW.max_avg_hrs_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.threshold_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.threshold_e_d_groups_nbr IS DISTINCT FROM NEW.threshold_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.threshold_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'threshold_e_d_groups_nbr', NEW.threshold_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.threshold_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'threshold_e_d_groups_nbr', NEW.threshold_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to EE_CHILD_SUPPORT_CASES */
  IF ((NEW.ee_child_support_cases_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_child_support_cases_nbr IS DISTINCT FROM NEW.ee_child_support_cases_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee_child_support_cases
    WHERE ee_child_support_cases_nbr = NEW.ee_child_support_cases_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_child_support_cases_nbr', NEW.ee_child_support_cases_nbr, 'ee_child_support_cases', NEW.effective_date);

    SELECT rec_version FROM ee_child_support_cases
    WHERE ee_child_support_cases_nbr = NEW.ee_child_support_cases_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_child_support_cases_nbr', NEW.ee_child_support_cases_nbr, 'ee_child_support_cases', NEW.effective_until - 1);
  END

  /* Check reference to EE_DIRECT_DEPOSIT */
  IF ((NEW.ee_direct_deposit_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_direct_deposit_nbr IS DISTINCT FROM NEW.ee_direct_deposit_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee_direct_deposit
    WHERE ee_direct_deposit_nbr = NEW.ee_direct_deposit_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_direct_deposit_nbr', NEW.ee_direct_deposit_nbr, 'ee_direct_deposit', NEW.effective_date);

    SELECT rec_version FROM ee_direct_deposit
    WHERE ee_direct_deposit_nbr = NEW.ee_direct_deposit_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_direct_deposit_nbr', NEW.ee_direct_deposit_nbr, 'ee_direct_deposit', NEW.effective_until - 1);
  END

  /* Check reference to EE */
  IF ((NEW.ee_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_date);

    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_until - 1);
  END

  /* Check reference to EE_BENEFITS */
  IF ((NEW.ee_benefits_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_benefits_nbr IS DISTINCT FROM NEW.ee_benefits_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE ee_benefits_nbr = NEW.ee_benefits_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_benefits_nbr', NEW.ee_benefits_nbr, 'ee_benefits', NEW.effective_date);

    SELECT rec_version FROM ee_benefits
    WHERE ee_benefits_nbr = NEW.ee_benefits_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'ee_benefits_nbr', NEW.ee_benefits_nbr, 'ee_benefits', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_SUBTYPE */
  IF ((NEW.co_benefit_subtype_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'co_benefit_subtype_nbr', NEW.co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_date);

    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'co_benefit_subtype_nbr', NEW.co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.hw_funding_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.hw_funding_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_funding_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_funding_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'hw_funding_cl_e_d_groups_nbr', NEW.hw_funding_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_funding_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'hw_funding_cl_e_d_groups_nbr', NEW.hw_funding_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.hw_excess_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.hw_excess_cl_e_d_groups_nbr IS DISTINCT FROM NEW.hw_excess_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_excess_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'hw_excess_cl_e_d_groups_nbr', NEW.hw_excess_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.hw_excess_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_scheduled_e_ds', NEW.ee_scheduled_e_ds_nbr, 'hw_excess_cl_e_d_groups_nbr', NEW.hw_excess_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_EE_SCHEDULED_E_DS_1 FOR EE_SCHEDULED_E_DS Before Insert POSITION 1
AS
DECLARE VARIABLE new_nbr INTEGER;
DECLARE VARIABLE new_effective_date DATE;
DECLARE VARIABLE new_effective_until DATE;
DECLARE VARIABLE prev_rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_ee_scheduled_e_ds_ver;

  new_nbr = NEW.ee_scheduled_e_ds_nbr;
  new_effective_date = NEW.effective_date;
  new_effective_until = NEW.effective_until;

  /* Calc EFFECTIVE_DATE of this version */
  IF (EXISTS (SELECT 1 FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = :new_nbr ROWS 1)) THEN
  BEGIN
    IF (new_effective_date IS NULL) THEN
      new_effective_date = CURRENT_DATE;
  END
  ELSE
    new_effective_date = '1/1/1900';

  /* Correct EFFECTIVE_UNTIL of previous version */
  prev_rec_version = NULL;
  SELECT rec_version FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = :new_nbr AND effective_date < :new_effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :prev_rec_version;

  IF (prev_rec_version IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE ee_scheduled_e_ds SET effective_until = :new_effective_date WHERE rec_version = :prev_rec_version;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END

  /* Calc EFFECTIVE_UNTIL of this version */
  SELECT effective_date FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = :new_nbr AND effective_date > :new_effective_date
  ORDER BY effective_date
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :new_effective_until;

  IF (new_effective_until IS NULL) THEN
    new_effective_until = '12/31/9999';

  /* Final assignment */
  NEW.effective_date = new_effective_date;
  NEW.effective_until = new_effective_until;
END

^

CREATE TRIGGER T_BU_EE_SCHEDULED_E_DS_1 FOR EE_SCHEDULED_E_DS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it is changing system fields */
  IF ((NEW.effective_date IS DISTINCT FROM OLD.effective_date) AND (OLD.effective_date = '1/1/1900')) THEN
    NEW.effective_date = OLD.effective_date;

  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.ee_scheduled_e_ds_nbr IS DISTINCT FROM OLD.ee_scheduled_e_ds_nbr) OR
     (NEW.effective_until IS DISTINCT FROM OLD.effective_until)) THEN
  BEGIN
    IF (NEW.effective_until IS DISTINCT FROM OLD.effective_until) THEN
    BEGIN
      IF (EXISTS (SELECT 1 FROM ee_scheduled_e_ds WHERE ee_scheduled_e_ds_nbr = NEW.ee_scheduled_e_ds_nbr AND effective_date = OLD.effective_until)) THEN
        NEW.effective_until = OLD.effective_until;
      ELSE
        IF (NEW.effective_until IS NULL) THEN
          NEW.effective_until = '12/31/9999';
    END
    ELSE
      EXECUTE PROCEDURE raise_table_error('ee_scheduled_e_ds', NEW.rec_version, '', 'S1', 'System fields cannot be modified');
  END

  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL */
    NEW.effective_until = '12/31/9999';
    SELECT effective_date FROM ee_scheduled_e_ds
    WHERE ee_scheduled_e_ds_nbr = NEW.ee_scheduled_e_ds_nbr AND
          effective_date > NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO NEW.effective_until;

    /* Correct EFFECTIVE_UNTIL of new previous version */
    rec_version = NULL;
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE ee_scheduled_e_ds_nbr = NEW.ee_scheduled_e_ds_nbr AND
          effective_date < NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date DESC
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :rec_version;

    IF (rec_version IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
      UPDATE ee_scheduled_e_ds SET effective_until = NEW.effective_date WHERE rec_version = :rec_version;
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_EE_SIGNATURE_9 FOR EE_SIGNATURE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(171, OLD.rec_version, OLD.ee_signature_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3136, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3137, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3138, OLD.ee_nbr);

  /* DATA */
  IF (OLD.data IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3139, OLD.data);

  /* DOCUMENT */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DOCUMENT');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@DOCUMENT', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3140, :blob_nbr);
  END

  /* CO_BENEFIT_ENROLLMENT_NBR */
  IF (OLD.co_benefit_enrollment_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3224, OLD.co_benefit_enrollment_nbr);

END

^

CREATE TRIGGER T_AU_EE_SIGNATURE_9 FOR EE_SIGNATURE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(171, NEW.rec_version, NEW.ee_signature_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3136, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3137, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3138, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* DATA */
  IF (OLD.data IS DISTINCT FROM NEW.data) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3139, OLD.data);
    changes = changes + 1;
  END

  /* DOCUMENT */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DOCUMENT');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@DOCUMENT', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3140, :blob_nbr);
    changes = changes + 1;
  END

  /* CO_BENEFIT_ENROLLMENT_NBR */
  IF (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3224, OLD.co_benefit_enrollment_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_SIGNATURE_2 FOR EE_SIGNATURE Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to EE */
  IF ((NEW.ee_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_signature', NEW.ee_signature_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_date);

    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_signature', NEW.ee_signature_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_ENROLLMENT */
  IF ((NEW.co_benefit_enrollment_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_enrollment
    WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_signature', NEW.ee_signature_nbr, 'co_benefit_enrollment_nbr', NEW.co_benefit_enrollment_nbr, 'co_benefit_enrollment', NEW.effective_date);

    SELECT rec_version FROM co_benefit_enrollment
    WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_signature', NEW.ee_signature_nbr, 'co_benefit_enrollment_nbr', NEW.co_benefit_enrollment_nbr, 'co_benefit_enrollment', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AD_EE_TIME_OFF_ACCRUAL_OPE_9 FOR EE_TIME_OFF_ACCRUAL_OPER After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(137, OLD.rec_version, OLD.ee_time_off_accrual_oper_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2260, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2830, OLD.effective_until);

  /* EE_TIME_OFF_ACCRUAL_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2262, OLD.ee_time_off_accrual_nbr);

  /* ACCRUAL_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2263, OLD.accrual_date);

  /* ACCRUED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2257, OLD.accrued);

  /* ACCRUED_CAPPED */
  IF (OLD.accrued_capped IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2258, OLD.accrued_capped);

  /* USED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2259, OLD.used);

  /* NOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2269, OLD.note);

  /* OPERATION_CODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2270, OLD.operation_code);

  /* ADJUSTED_EE_TOA_OPER_NBR */
  IF (OLD.adjusted_ee_toa_oper_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2264, OLD.adjusted_ee_toa_oper_nbr);

  /* CONNECTED_EE_TOA_OPER_NBR */
  IF (OLD.connected_ee_toa_oper_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2265, OLD.connected_ee_toa_oper_nbr);

  /* PR_NBR */
  IF (OLD.pr_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2266, OLD.pr_nbr);

  /* PR_BATCH_NBR */
  IF (OLD.pr_batch_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2267, OLD.pr_batch_nbr);

  /* PR_CHECK_NBR */
  IF (OLD.pr_check_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2268, OLD.pr_check_nbr);

  /* REQUEST_DATE */
  IF (OLD.request_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2905, OLD.request_date);

  /* CL_E_DS_NBR */
  IF (OLD.cl_e_ds_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2906, OLD.cl_e_ds_nbr);

  /* MANAGER_NOTE */
  IF (OLD.manager_note IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3217, OLD.manager_note);

END

^

CREATE TRIGGER T_AU_EE_TIME_OFF_ACCRUAL_OPE_9 FOR EE_TIME_OFF_ACCRUAL_OPER After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(137, NEW.rec_version, NEW.ee_time_off_accrual_oper_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2260, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2830, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_TIME_OFF_ACCRUAL_NBR */
  IF (OLD.ee_time_off_accrual_nbr IS DISTINCT FROM NEW.ee_time_off_accrual_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2262, OLD.ee_time_off_accrual_nbr);
    changes = changes + 1;
  END

  /* ACCRUAL_DATE */
  IF (OLD.accrual_date IS DISTINCT FROM NEW.accrual_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2263, OLD.accrual_date);
    changes = changes + 1;
  END

  /* ACCRUED */
  IF (OLD.accrued IS DISTINCT FROM NEW.accrued) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2257, OLD.accrued);
    changes = changes + 1;
  END

  /* ACCRUED_CAPPED */
  IF (OLD.accrued_capped IS DISTINCT FROM NEW.accrued_capped) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2258, OLD.accrued_capped);
    changes = changes + 1;
  END

  /* USED */
  IF (OLD.used IS DISTINCT FROM NEW.used) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2259, OLD.used);
    changes = changes + 1;
  END

  /* NOTE */
  IF (OLD.note IS DISTINCT FROM NEW.note) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2269, OLD.note);
    changes = changes + 1;
  END

  /* OPERATION_CODE */
  IF (OLD.operation_code IS DISTINCT FROM NEW.operation_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2270, OLD.operation_code);
    changes = changes + 1;
  END

  /* ADJUSTED_EE_TOA_OPER_NBR */
  IF (OLD.adjusted_ee_toa_oper_nbr IS DISTINCT FROM NEW.adjusted_ee_toa_oper_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2264, OLD.adjusted_ee_toa_oper_nbr);
    changes = changes + 1;
  END

  /* CONNECTED_EE_TOA_OPER_NBR */
  IF (OLD.connected_ee_toa_oper_nbr IS DISTINCT FROM NEW.connected_ee_toa_oper_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2265, OLD.connected_ee_toa_oper_nbr);
    changes = changes + 1;
  END

  /* PR_NBR */
  IF (OLD.pr_nbr IS DISTINCT FROM NEW.pr_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2266, OLD.pr_nbr);
    changes = changes + 1;
  END

  /* PR_BATCH_NBR */
  IF (OLD.pr_batch_nbr IS DISTINCT FROM NEW.pr_batch_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2267, OLD.pr_batch_nbr);
    changes = changes + 1;
  END

  /* PR_CHECK_NBR */
  IF (OLD.pr_check_nbr IS DISTINCT FROM NEW.pr_check_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2268, OLD.pr_check_nbr);
    changes = changes + 1;
  END

  /* REQUEST_DATE */
  IF (OLD.request_date IS DISTINCT FROM NEW.request_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2905, OLD.request_date);
    changes = changes + 1;
  END

  /* CL_E_DS_NBR */
  IF (OLD.cl_e_ds_nbr IS DISTINCT FROM NEW.cl_e_ds_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2906, OLD.cl_e_ds_nbr);
    changes = changes + 1;
  END

  /* MANAGER_NOTE */
  IF (OLD.manager_note IS DISTINCT FROM NEW.manager_note) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3217, OLD.manager_note);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIUD_PR_CHECK_LINES_2 FOR PR_CHECK_LINES Before Insert or Update or Delete POSITION 2
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (UPDATING AND rdb$get_context('USER_TRANSACTION', 'DETACH_MISC_CHECKS') = 1) THEN
    EXIT;
  IF (INSERTING) THEN
    EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
  ELSE
    EXECUTE PROCEDURE check_processed_pr(OLD.pr_nbr);

  IF (INSERTING) THEN
    EXECUTE PROCEDURE check_printed_manual_check(NEW.pr_check_nbr);
  ELSE
    EXECUTE PROCEDURE check_printed_manual_check(OLD.pr_check_nbr);

END

^

CREATE TRIGGER T_AD_CO_BENEFIT_ENROLLMENT_9 FOR CO_BENEFIT_ENROLLMENT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(174, OLD.rec_version, OLD.co_benefit_enrollment_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3195, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3196, OLD.effective_until);

  /* CO_BENEFIT_CATEGORY_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3197, OLD.co_benefit_category_nbr);

  /* ENROLLMENT_START_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3198, OLD.enrollment_start_date);

  /* ENROLLMENT_END_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3199, OLD.enrollment_end_date);

END

^

CREATE TRIGGER T_AI_CO_BENEFIT_ENROLLMENT_9 FOR CO_BENEFIT_ENROLLMENT After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(174, NEW.rec_version, NEW.co_benefit_enrollment_nbr, 'I');
END

^

CREATE TRIGGER T_AUD_CO_BENEFIT_ENROLLMENT_2 FOR CO_BENEFIT_ENROLLMENT After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_benefit_enrollment WHERE co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_benefit_enrollment WHERE co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in EE_BENEFIT_REFUSAL */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefit_refusal
    WHERE (co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_enrollment', OLD.co_benefit_enrollment_nbr, 'ee_benefit_refusal', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefit_refusal
    WHERE (co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_enrollment', OLD.co_benefit_enrollment_nbr, 'ee_benefit_refusal', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SIGNATURE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_signature
    WHERE (co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_enrollment', OLD.co_benefit_enrollment_nbr, 'ee_signature', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_signature
    WHERE (co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_enrollment', OLD.co_benefit_enrollment_nbr, 'ee_signature', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_BENEFITS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE (co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_enrollment', OLD.co_benefit_enrollment_nbr, 'ee_benefits', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE (co_benefit_enrollment_nbr = OLD.co_benefit_enrollment_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_enrollment', OLD.co_benefit_enrollment_nbr, 'ee_benefits', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_CO_BENEFIT_ENROLLMENT_9 FOR CO_BENEFIT_ENROLLMENT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(174, NEW.rec_version, NEW.co_benefit_enrollment_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3195, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3196, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_BENEFIT_CATEGORY_NBR */
  IF (OLD.co_benefit_category_nbr IS DISTINCT FROM NEW.co_benefit_category_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3197, OLD.co_benefit_category_nbr);
    changes = changes + 1;
  END

  /* ENROLLMENT_START_DATE */
  IF (OLD.enrollment_start_date IS DISTINCT FROM NEW.enrollment_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3198, OLD.enrollment_start_date);
    changes = changes + 1;
  END

  /* ENROLLMENT_END_DATE */
  IF (OLD.enrollment_end_date IS DISTINCT FROM NEW.enrollment_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3199, OLD.enrollment_end_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_BENEFIT_ENROLLMENT_2 FOR CO_BENEFIT_ENROLLMENT Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO_BENEFIT_CATEGORY */
  IF ((NEW.co_benefit_category_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_category_nbr IS DISTINCT FROM NEW.co_benefit_category_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_category
    WHERE co_benefit_category_nbr = NEW.co_benefit_category_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_benefit_enrollment', NEW.co_benefit_enrollment_nbr, 'co_benefit_category_nbr', NEW.co_benefit_category_nbr, 'co_benefit_category', NEW.effective_date);

    SELECT rec_version FROM co_benefit_category
    WHERE co_benefit_category_nbr = NEW.co_benefit_category_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_benefit_enrollment', NEW.co_benefit_enrollment_nbr, 'co_benefit_category_nbr', NEW.co_benefit_category_nbr, 'co_benefit_category', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_CO_BENEFIT_ENROLLMENT_1 FOR CO_BENEFIT_ENROLLMENT Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_co_benefit_enrollment_ver;

  IF (EXISTS (SELECT 1 FROM co_benefit_enrollment WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('co_benefit_enrollment', NEW.co_benefit_enrollment_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_CO_BENEFIT_ENROLLMENT_1 FOR CO_BENEFIT_ENROLLMENT Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.co_benefit_enrollment_nbr IS DISTINCT FROM OLD.co_benefit_enrollment_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('co_benefit_enrollment', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_EE_BENEFIT_PAYMENT_9 FOR EE_BENEFIT_PAYMENT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(119, OLD.rec_version, OLD.ee_benefit_payment_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1964, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2794, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1967, OLD.ee_nbr);

  /* PAYMENT_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1968, OLD.payment_date);

  /* PERIOD_START_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1969, OLD.period_start_date);

  /* PERIOD_END_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1970, OLD.period_end_date);

  /* VOID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1974, OLD.void);

  /* REASON */
  IF (OLD.reason IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1975, OLD.reason);

  /* REMARKS */
  IF (OLD.remarks IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1976, OLD.remarks);

  /* PAYMENT_AMOUNT */
  IF (OLD.payment_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1965, OLD.payment_amount);

  /* PAYMENT_NUMBER */
  IF (OLD.payment_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1971, OLD.payment_number);

  /* COUPON_SENT_DATE */
  IF (OLD.coupon_sent_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1972, OLD.coupon_sent_date);

  /* LETTER_SENT_DATE */
  IF (OLD.letter_sent_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1973, OLD.letter_sent_date);

  /* PAYMENT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1977, OLD.payment_type);

  /* BENEFIT_DEDUCTION_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1978, OLD.benefit_deduction_type);

  /* RECONCILED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1979, OLD.reconciled);

  /* CL_AGENCY_NBR */
  IF (OLD.cl_agency_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2938, OLD.cl_agency_nbr);

  /* BENEFIT_NAME */
  IF (OLD.benefit_name IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2939, OLD.benefit_name);

  /* BENEFIT_SUBTYPE */
  IF (OLD.benefit_subtype IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2940, OLD.benefit_subtype);

END

^

CREATE TRIGGER T_AU_EE_BENEFIT_PAYMENT_9 FOR EE_BENEFIT_PAYMENT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(119, NEW.rec_version, NEW.ee_benefit_payment_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1964, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2794, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1967, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* PAYMENT_DATE */
  IF (OLD.payment_date IS DISTINCT FROM NEW.payment_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1968, OLD.payment_date);
    changes = changes + 1;
  END

  /* PERIOD_START_DATE */
  IF (OLD.period_start_date IS DISTINCT FROM NEW.period_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1969, OLD.period_start_date);
    changes = changes + 1;
  END

  /* PERIOD_END_DATE */
  IF (OLD.period_end_date IS DISTINCT FROM NEW.period_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1970, OLD.period_end_date);
    changes = changes + 1;
  END

  /* VOID */
  IF (OLD.void IS DISTINCT FROM NEW.void) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1974, OLD.void);
    changes = changes + 1;
  END

  /* REASON */
  IF (OLD.reason IS DISTINCT FROM NEW.reason) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1975, OLD.reason);
    changes = changes + 1;
  END

  /* REMARKS */
  IF (OLD.remarks IS DISTINCT FROM NEW.remarks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1976, OLD.remarks);
    changes = changes + 1;
  END

  /* PAYMENT_AMOUNT */
  IF (OLD.payment_amount IS DISTINCT FROM NEW.payment_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1965, OLD.payment_amount);
    changes = changes + 1;
  END

  /* PAYMENT_NUMBER */
  IF (OLD.payment_number IS DISTINCT FROM NEW.payment_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1971, OLD.payment_number);
    changes = changes + 1;
  END

  /* COUPON_SENT_DATE */
  IF (OLD.coupon_sent_date IS DISTINCT FROM NEW.coupon_sent_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1972, OLD.coupon_sent_date);
    changes = changes + 1;
  END

  /* LETTER_SENT_DATE */
  IF (OLD.letter_sent_date IS DISTINCT FROM NEW.letter_sent_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1973, OLD.letter_sent_date);
    changes = changes + 1;
  END

  /* PAYMENT_TYPE */
  IF (OLD.payment_type IS DISTINCT FROM NEW.payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1977, OLD.payment_type);
    changes = changes + 1;
  END

  /* BENEFIT_DEDUCTION_TYPE */
  IF (OLD.benefit_deduction_type IS DISTINCT FROM NEW.benefit_deduction_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1978, OLD.benefit_deduction_type);
    changes = changes + 1;
  END

  /* RECONCILED */
  IF (OLD.reconciled IS DISTINCT FROM NEW.reconciled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1979, OLD.reconciled);
    changes = changes + 1;
  END

  /* CL_AGENCY_NBR */
  IF (OLD.cl_agency_nbr IS DISTINCT FROM NEW.cl_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2938, OLD.cl_agency_nbr);
    changes = changes + 1;
  END

  /* BENEFIT_NAME */
  IF (OLD.benefit_name IS DISTINCT FROM NEW.benefit_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2939, OLD.benefit_name);
    changes = changes + 1;
  END

  /* BENEFIT_SUBTYPE */
  IF (OLD.benefit_subtype IS DISTINCT FROM NEW.benefit_subtype) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2940, OLD.benefit_subtype);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_BENEFIT_PAYMENT_3 FOR EE_BENEFIT_PAYMENT Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_EE_BENEFIT_PAYMENT */
  IF (INSERTING OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) OR (OLD.payment_date IS DISTINCT FROM NEW.payment_date) OR (OLD.payment_number IS DISTINCT FROM NEW.payment_number)) THEN
    IF (EXISTS(SELECT 1 FROM ee_benefit_payment WHERE ee_nbr = NEW.ee_nbr AND payment_date = NEW.payment_date AND payment_number = NEW.payment_number AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('ee_benefit_payment', 'ee_nbr, payment_date, payment_number',
      CAST(NEW.ee_nbr || ', ' || NEW.payment_date || ', ' || NEW.payment_number as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^


CREATE TRIGGER T_AD_EE_BENEFITS_9 FOR EE_BENEFITS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(118, OLD.rec_version, OLD.ee_benefits_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1947, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2792, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1954, OLD.ee_nbr);

  /* EXPIRATION_DATE */
  IF (OLD.expiration_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1948, OLD.expiration_date);

  /* DEDUCTION_FREQUENCY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1957, OLD.deduction_frequency);

  /* ENROLLMENT_STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1958, OLD.enrollment_status);

  /* BENEFIT_EFFECTIVE_DATE */
  IF (OLD.benefit_effective_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1952, OLD.benefit_effective_date);

  /* TOTAL_PREMIUM_AMOUNT */
  IF (OLD.total_premium_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1946, OLD.total_premium_amount);

  /* COBRA_STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1956, OLD.cobra_status);

  /* BENEFIT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1959, OLD.benefit_type);

  /* COBRA_TERMINATION_EVENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1960, OLD.cobra_termination_event);

  /* COBRA_SS_DISABILITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1961, OLD.cobra_ss_disability);

  /* COBRA_MEDICAL_PARTICIPANT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1962, OLD.cobra_medical_participant);

  /* COBRA_QUALIFYING_EVENT_DATE */
  IF (OLD.cobra_qualifying_event_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1955, OLD.cobra_qualifying_event_date);

  /* COBRA_REASON_FOR_REFUSAL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1963, OLD.cobra_reason_for_refusal);

  /* EE_RATE */
  IF (OLD.ee_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1949, OLD.ee_rate);

  /* ER_RATE */
  IF (OLD.er_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1950, OLD.er_rate);

  /* COBRA_RATE */
  IF (OLD.cobra_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1951, OLD.cobra_rate);

  /* CO_BENEFIT_SUBTYPE_NBR */
  IF (OLD.co_benefit_subtype_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2930, OLD.co_benefit_subtype_nbr);

  /* ADDITIONAL_INFO1 */
  IF (OLD.additional_info1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2931, OLD.additional_info1);

  /* ADDITIONAL_INFO2 */
  IF (OLD.additional_info2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2932, OLD.additional_info2);

  /* ADDITIONAL_INFO3 */
  IF (OLD.additional_info3 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2933, OLD.additional_info3);

  /* CO_BENEFITS_NBR */
  IF (OLD.co_benefits_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2934, OLD.co_benefits_nbr);

  /* RATE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2935, OLD.rate_type);

  /* CL_E_D_GROUPS_NBR */
  IF (OLD.cl_e_d_groups_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2936, OLD.cl_e_d_groups_nbr);

  /* STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3236, OLD.status);

  /* BENEFIT_ENROLLMENT_COMPLETE */
  IF (OLD.benefit_enrollment_complete IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3238, OLD.benefit_enrollment_complete);

  /* CO_BENEFIT_ENROLLMENT_NBR */
  IF (OLD.co_benefit_enrollment_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3239, OLD.co_benefit_enrollment_nbr);

END

^

CREATE TRIGGER T_AU_EE_BENEFITS_9 FOR EE_BENEFITS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(118, NEW.rec_version, NEW.ee_benefits_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1947, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2792, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1954, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* EXPIRATION_DATE */
  IF (OLD.expiration_date IS DISTINCT FROM NEW.expiration_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1948, OLD.expiration_date);
    changes = changes + 1;
  END

  /* DEDUCTION_FREQUENCY */
  IF (OLD.deduction_frequency IS DISTINCT FROM NEW.deduction_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1957, OLD.deduction_frequency);
    changes = changes + 1;
  END

  /* ENROLLMENT_STATUS */
  IF (OLD.enrollment_status IS DISTINCT FROM NEW.enrollment_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1958, OLD.enrollment_status);
    changes = changes + 1;
  END

  /* BENEFIT_EFFECTIVE_DATE */
  IF (OLD.benefit_effective_date IS DISTINCT FROM NEW.benefit_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1952, OLD.benefit_effective_date);
    changes = changes + 1;
  END

  /* TOTAL_PREMIUM_AMOUNT */
  IF (OLD.total_premium_amount IS DISTINCT FROM NEW.total_premium_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1946, OLD.total_premium_amount);
    changes = changes + 1;
  END

  /* COBRA_STATUS */
  IF (OLD.cobra_status IS DISTINCT FROM NEW.cobra_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1956, OLD.cobra_status);
    changes = changes + 1;
  END

  /* BENEFIT_TYPE */
  IF (OLD.benefit_type IS DISTINCT FROM NEW.benefit_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1959, OLD.benefit_type);
    changes = changes + 1;
  END

  /* COBRA_TERMINATION_EVENT */
  IF (OLD.cobra_termination_event IS DISTINCT FROM NEW.cobra_termination_event) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1960, OLD.cobra_termination_event);
    changes = changes + 1;
  END

  /* COBRA_SS_DISABILITY */
  IF (OLD.cobra_ss_disability IS DISTINCT FROM NEW.cobra_ss_disability) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1961, OLD.cobra_ss_disability);
    changes = changes + 1;
  END

  /* COBRA_MEDICAL_PARTICIPANT */
  IF (OLD.cobra_medical_participant IS DISTINCT FROM NEW.cobra_medical_participant) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1962, OLD.cobra_medical_participant);
    changes = changes + 1;
  END

  /* COBRA_QUALIFYING_EVENT_DATE */
  IF (OLD.cobra_qualifying_event_date IS DISTINCT FROM NEW.cobra_qualifying_event_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1955, OLD.cobra_qualifying_event_date);
    changes = changes + 1;
  END

  /* COBRA_REASON_FOR_REFUSAL */
  IF (OLD.cobra_reason_for_refusal IS DISTINCT FROM NEW.cobra_reason_for_refusal) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1963, OLD.cobra_reason_for_refusal);
    changes = changes + 1;
  END

  /* EE_RATE */
  IF (OLD.ee_rate IS DISTINCT FROM NEW.ee_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1949, OLD.ee_rate);
    changes = changes + 1;
  END

  /* ER_RATE */
  IF (OLD.er_rate IS DISTINCT FROM NEW.er_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1950, OLD.er_rate);
    changes = changes + 1;
  END

  /* COBRA_RATE */
  IF (OLD.cobra_rate IS DISTINCT FROM NEW.cobra_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1951, OLD.cobra_rate);
    changes = changes + 1;
  END

  /* CO_BENEFIT_SUBTYPE_NBR */
  IF (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2930, OLD.co_benefit_subtype_nbr);
    changes = changes + 1;
  END

  /* ADDITIONAL_INFO1 */
  IF (OLD.additional_info1 IS DISTINCT FROM NEW.additional_info1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2931, OLD.additional_info1);
    changes = changes + 1;
  END

  /* ADDITIONAL_INFO2 */
  IF (OLD.additional_info2 IS DISTINCT FROM NEW.additional_info2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2932, OLD.additional_info2);
    changes = changes + 1;
  END

  /* ADDITIONAL_INFO3 */
  IF (OLD.additional_info3 IS DISTINCT FROM NEW.additional_info3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2933, OLD.additional_info3);
    changes = changes + 1;
  END

  /* CO_BENEFITS_NBR */
  IF (OLD.co_benefits_nbr IS DISTINCT FROM NEW.co_benefits_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2934, OLD.co_benefits_nbr);
    changes = changes + 1;
  END

  /* RATE_TYPE */
  IF (OLD.rate_type IS DISTINCT FROM NEW.rate_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2935, OLD.rate_type);
    changes = changes + 1;
  END

  /* CL_E_D_GROUPS_NBR */
  IF (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2936, OLD.cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* STATUS */
  IF (OLD.status IS DISTINCT FROM NEW.status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3236, OLD.status);
    changes = changes + 1;
  END

  /* BENEFIT_ENROLLMENT_COMPLETE */
  IF (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3238, OLD.benefit_enrollment_complete);
    changes = changes + 1;
  END

  /* CO_BENEFIT_ENROLLMENT_NBR */
  IF (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3239, OLD.co_benefit_enrollment_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_BENEFITS_2 FOR EE_BENEFITS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to EE */
  IF ((NEW.ee_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_date);

    SELECT rec_version FROM ee
    WHERE ee_nbr = NEW.ee_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'ee_nbr', NEW.ee_nbr, 'ee', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_e_d_groups_nbr IS DISTINCT FROM NEW.cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'cl_e_d_groups_nbr', NEW.cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'cl_e_d_groups_nbr', NEW.cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFITS */
  IF ((NEW.co_benefits_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefits_nbr IS DISTINCT FROM NEW.co_benefits_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefits
    WHERE co_benefits_nbr = NEW.co_benefits_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'co_benefits_nbr', NEW.co_benefits_nbr, 'co_benefits', NEW.effective_date);

    SELECT rec_version FROM co_benefits
    WHERE co_benefits_nbr = NEW.co_benefits_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'co_benefits_nbr', NEW.co_benefits_nbr, 'co_benefits', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_SUBTYPE */
  IF ((NEW.co_benefit_subtype_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'co_benefit_subtype_nbr', NEW.co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_date);

    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'co_benefit_subtype_nbr', NEW.co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_ENROLLMENT */
  IF ((NEW.co_benefit_enrollment_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_enrollment_nbr IS DISTINCT FROM NEW.co_benefit_enrollment_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_enrollment
    WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'co_benefit_enrollment_nbr', NEW.co_benefit_enrollment_nbr, 'co_benefit_enrollment', NEW.effective_date);

    SELECT rec_version FROM co_benefit_enrollment
    WHERE co_benefit_enrollment_nbr = NEW.co_benefit_enrollment_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee_benefits', NEW.ee_benefits_nbr, 'co_benefit_enrollment_nbr', NEW.co_benefit_enrollment_nbr, 'co_benefit_enrollment', NEW.effective_until - 1);
  END
END

^


/* Delete removed fields of CO_BENEFIT_CATEGORY */
EXECUTE BLOCK
AS
DECLARE nbr INTEGER;
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  /* ENROLLMENT_START_DATE */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3017;
  DELETE FROM ev_field WHERE nbr = 3017;

  /* ENROLLMENT_END_DATE */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3018;
  DELETE FROM ev_field WHERE nbr = 3018;

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Delete removed fields of EE_BENEFIT_REFUSAL */
EXECUTE BLOCK
AS
DECLARE nbr INTEGER;
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  /* CO_BENEFIT_CATEGORY_NBR */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3130;
  DELETE FROM ev_field WHERE nbr = 3130;

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  UPDATE ev_table SET name = 'EE_SCHEDULED_E_DS', versioned = 'Y' WHERE nbr = 134;
  INSERT INTO ev_table (nbr, name, versioned) VALUES (174, 'CO_BENEFIT_ENROLLMENT', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 3219, 'ROUND_OT_CALCULATION', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 3228, 'MINIMUM_HW_HOURS', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 3229, 'MAXIMUM_HW_HOURS', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 3230, 'HW_FUNDING_CL_E_D_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 3231, 'HW_EXCESS_CL_E_D_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3200, 'ANNUAL_FORM_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3201, 'PRENOTE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3202, 'ENABLE_DD', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3203, 'ESS_OR_EP', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3204, 'ACA_EDUCATION_ORG', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3205, 'ACA_STANDARD_HOURS', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3206, 'ACA_DEFAULT_STATUS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3207, 'ACA_CONTROL_GROUP', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3208, 'ACA_INITIAL_PERIOD_FROM', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3209, 'ACA_INITIAL_PERIOD_TO', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3210, 'ACA_STABILITY_PERIOD_FROM', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3211, 'ACA_STABILITY_PERIOD_TO', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3212, 'ACA_USE_AVG_HOURS_WORKED', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3213, 'ACA_AVG_HOURS_WORKED_PERIOD', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3227, 'ACA_ADD_EARN_CL_E_D_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 3214, 'CO_REPORTS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (96, 3216, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (105, 3215, 'CO_REPORTS_NBR', 'I', NULL, NULL, 'N', 'N');
  UPDATE ev_field SET name = 'WORKERS_COMP_CODE', field_type = 'V', len = 8, scale = NULL, required = 'Y', versioned = 'Y' WHERE nbr = 1797;
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3218, 'ACA_STANDARD_HOURS', 'N', 18, 6, 'N', 'N');
  UPDATE ev_field SET name = 'PAYMENT_NUMBER', field_type = 'V', len = 20, scale = NULL, required = 'N', versioned = 'N' WHERE nbr = 1971;
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (120, 3225, 'STATUS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (120, 3226, 'STATUS_DATE', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (122, 3220, 'SHOW_IN_EE_PORTAL', 'C', 1, NULL, 'Y', 'N');
  UPDATE ev_field SET name = 'EE_DIRECT_DEPOSIT_NBR', field_type = 'I', len = NULL, scale = NULL, required = 'N', versioned = 'Y' WHERE nbr = 2175;
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (134, 3232, 'MINIMUM_HW_HOURS', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (134, 3233, 'MAXIMUM_HW_HOURS', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (134, 3234, 'HW_FUNDING_CL_E_D_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (134, 3235, 'HW_EXCESS_CL_E_D_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (137, 3217, 'MANAGER_NOTE', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (170, 3223, 'CO_BENEFIT_ENROLLMENT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (170, 3221, 'STATUS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (170, 3222, 'STATUS_DATE', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (171, 3224, 'CO_BENEFIT_ENROLLMENT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3193, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3194, 'CO_BENEFIT_ENROLLMENT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3195, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3196, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3197, 'CO_BENEFIT_CATEGORY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3198, 'ENROLLMENT_START_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3199, 'ENROLLMENT_END_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (118, 3236, 'STATUS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3237, 'ACA_COVERAGE_OFFER', 'V', 2, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (118, 3238, 'BENEFIT_ENROLLMENT_COMPLETE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (118, 3239, 'CO_BENEFIT_ENROLLMENT_NBR', 'I', NULL, NULL, 'N', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.0', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
