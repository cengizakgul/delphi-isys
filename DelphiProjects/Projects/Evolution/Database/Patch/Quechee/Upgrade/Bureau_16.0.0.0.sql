/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.6';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


DROP TRIGGER T_AD_SB_SERVICES_9
^


DROP TRIGGER T_AU_SB_SERVICES_9
^

ALTER TABLE SB_SERVICES
ADD PARTNER_BILLING EV_CHAR1  NOT NULL
^

ALTER TABLE SB_SERVICES
ALTER COLUMN PARTNER_BILLING POSITION 20
^

UPDATE SB_SERVICES SET PARTNER_BILLING = 'N'
^

CREATE TRIGGER T_AD_SB_SERVICES_9 FOR SB_SERVICES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, OLD.rec_version, OLD.sb_services_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 367, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 524, OLD.effective_until);

  /* SERVICE_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 374, OLD.service_name);

  /* FREQUENCY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 377, OLD.frequency);

  /* MONTH_NUMBER */
  IF (OLD.month_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 372, OLD.month_number);

  /* BASED_ON_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 378, OLD.based_on_type);

  /* SB_REPORTS_NBR */
  IF (OLD.sb_reports_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 373, OLD.sb_reports_nbr);

  /* COMMISSION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 380, OLD.commission);

  /* SALES_TAXABLE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 381, OLD.sales_taxable);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 375, OLD.filler);

  /* PRODUCT_CODE */
  IF (OLD.product_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 366, OLD.product_code);

  /* WEEK_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 382, OLD.week_number);

  /* SERVICE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 383, OLD.service_type);

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 368, OLD.minimum_amount);

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 369, OLD.maximum_amount);

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 370, OLD.sb_delivery_method_nbr);

  /* TAX_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 376, OLD.tax_type);

  /* PARTNER_BILLING */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 570, OLD.partner_billing);

END

^

CREATE TRIGGER T_AU_SB_SERVICES_9 FOR SB_SERVICES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, NEW.rec_version, NEW.sb_services_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 367, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 524, OLD.effective_until);
    changes = changes + 1;
  END

  /* SERVICE_NAME */
  IF (OLD.service_name IS DISTINCT FROM NEW.service_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 374, OLD.service_name);
    changes = changes + 1;
  END

  /* FREQUENCY */
  IF (OLD.frequency IS DISTINCT FROM NEW.frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 377, OLD.frequency);
    changes = changes + 1;
  END

  /* MONTH_NUMBER */
  IF (OLD.month_number IS DISTINCT FROM NEW.month_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 372, OLD.month_number);
    changes = changes + 1;
  END

  /* BASED_ON_TYPE */
  IF (OLD.based_on_type IS DISTINCT FROM NEW.based_on_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 378, OLD.based_on_type);
    changes = changes + 1;
  END

  /* SB_REPORTS_NBR */
  IF (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 373, OLD.sb_reports_nbr);
    changes = changes + 1;
  END

  /* COMMISSION */
  IF (OLD.commission IS DISTINCT FROM NEW.commission) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 380, OLD.commission);
    changes = changes + 1;
  END

  /* SALES_TAXABLE */
  IF (OLD.sales_taxable IS DISTINCT FROM NEW.sales_taxable) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 381, OLD.sales_taxable);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 375, OLD.filler);
    changes = changes + 1;
  END

  /* PRODUCT_CODE */
  IF (OLD.product_code IS DISTINCT FROM NEW.product_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 366, OLD.product_code);
    changes = changes + 1;
  END

  /* WEEK_NUMBER */
  IF (OLD.week_number IS DISTINCT FROM NEW.week_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 382, OLD.week_number);
    changes = changes + 1;
  END

  /* SERVICE_TYPE */
  IF (OLD.service_type IS DISTINCT FROM NEW.service_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 383, OLD.service_type);
    changes = changes + 1;
  END

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS DISTINCT FROM NEW.minimum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 368, OLD.minimum_amount);
    changes = changes + 1;
  END

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS DISTINCT FROM NEW.maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 369, OLD.maximum_amount);
    changes = changes + 1;
  END

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 370, OLD.sb_delivery_method_nbr);
    changes = changes + 1;
  END

  /* TAX_TYPE */
  IF (OLD.tax_type IS DISTINCT FROM NEW.tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 376, OLD.tax_type);
    changes = changes + 1;
  END

  /* PARTNER_BILLING */
  IF (OLD.partner_billing IS DISTINCT FROM NEW.partner_billing) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 570, OLD.partner_billing);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 570, 'PARTNER_BILLING', 'C', 1, NULL, 'Y', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.0', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
	