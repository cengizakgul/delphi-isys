/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.1';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^




CREATE GENERATOR G_SB_DASHBOARDS
^

CREATE GENERATOR G_SB_DASHBOARDS_VER
^

CREATE GENERATOR G_SB_ENABLED_DASHBOARDS
^

CREATE GENERATOR G_SB_ENABLED_DASHBOARDS_VER
^

CREATE GENERATOR G_SB_ACA_GROUP
^

CREATE GENERATOR G_SB_ACA_GROUP_VER
^

CREATE GENERATOR G_SB_VENDOR
^

CREATE GENERATOR G_SB_VENDOR_VER
^

CREATE GENERATOR G_SB_VENDOR_DETAIL
^

CREATE GENERATOR G_SB_VENDOR_DETAIL_VER
^

CREATE GENERATOR G_SB_VENDOR_DETAIL_VALUES
^

CREATE GENERATOR G_SB_VENDOR_DETAIL_VALUES_VER
^

CREATE GENERATOR G_SB_CUSTOM_VENDORS
^

CREATE GENERATOR G_SB_CUSTOM_VENDORS_VER
^

CREATE DOMAIN EV_BLOB_TXT BLOB SUB_TYPE 1 SEGMENT SIZE 80
^


DROP TRIGGER T_AD_SB_9
^


DROP TRIGGER T_AU_SB_9
^

ALTER TABLE SB
ADD SESSION_LOCKOUT EV_INT 
^

ALTER TABLE SB
ALTER COLUMN SESSION_LOCKOUT POSITION 64
^

ALTER TABLE SB
ADD SESSION_TIMEOUT EV_INT 
^

ALTER TABLE SB
ALTER COLUMN SESSION_TIMEOUT POSITION 65
^


DROP TRIGGER T_AD_SB_USER_9
^


DROP TRIGGER T_AU_SB_USER_9
^


DROP TRIGGER T_BIU_SB_USER_3
^


DROP TRIGGER T_BI_SB_USER_1
^


DROP TRIGGER T_BU_SB_USER_1
^

ALTER TABLE SB_USER
ADD EVOLUTION_PRODUCT EV_CHAR1 
^

ALTER TABLE SB_USER
ALTER COLUMN EVOLUTION_PRODUCT POSITION 35
^

COMMIT
^

UPDATE SB_USER SET  EVOLUTION_PRODUCT = 'N'
^

CREATE TABLE  SB_DASHBOARDS
( 
     REC_VERSION EV_INT NOT NULL,
     SB_DASHBOARDS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     DASHBOARD_TYPE EV_CHAR1,
     SY_ANALYTICS_TIER_NBR EV_INT NOT NULL,
     NOTES EV_BLOB_TXT,
     DASHBOARD_ID EV_INT NOT NULL,
     DESCRIPTION EV_STR80 NOT NULL,
        CONSTRAINT PK_SB_DASHBOARDS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_DASHBOARDS_1 UNIQUE (SB_DASHBOARDS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_DASHBOARDS_2 UNIQUE (SB_DASHBOARDS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_SB_DASHBOARDS_1 ON SB_DASHBOARDS (DESCRIPTION,EFFECTIVE_UNTIL)
^


GRANT ALL ON SB_DASHBOARDS TO EUSER
^

CREATE TABLE  SB_ENABLED_DASHBOARDS
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ENABLED_DASHBOARDS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     DASHBOARD_LEVEL EV_CHAR1,
     DASHBOARD_NBR EV_INT NOT NULL,
        CONSTRAINT PK_SB_ENABLED_DASHBOARDS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ENABLED_DASHBOARDS_1 UNIQUE (SB_ENABLED_DASHBOARDS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ENABLED_DASHBOARDS_2 UNIQUE (SB_ENABLED_DASHBOARDS_NBR,EFFECTIVE_UNTIL)
)
^


GRANT ALL ON SB_ENABLED_DASHBOARDS TO EUSER
^

CREATE TABLE  SB_ACA_GROUP
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ACA_GROUP_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     ACA_GROUP_DESCRIPTION EV_STR40 NOT NULL,
     PRIMARY_CL_NBR EV_INT,
     PRIMARY_CO_NBR EV_INT,
        CONSTRAINT PK_SB_ACA_GROUP PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ACA_GROUP_1 UNIQUE (SB_ACA_GROUP_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ACA_GROUP_2 UNIQUE (SB_ACA_GROUP_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_SB_ACA_GROUP ON SB_ACA_GROUP (ACA_GROUP_DESCRIPTION,EFFECTIVE_UNTIL)
^


GRANT ALL ON SB_ACA_GROUP TO EUSER
^

CREATE TABLE  SB_CUSTOM_VENDORS
( 
     REC_VERSION EV_INT NOT NULL,
     SB_CUSTOM_VENDORS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     VENDOR_NAME EV_STR80 NOT NULL,
     SY_VENDOR_CATEGORIES_NBR EV_INT NOT NULL,
     VENDOR_TYPE EV_CHAR1,
        CONSTRAINT PK_SB_CUSTOM_VENDORS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_CUSTOM_VENDORS_1 UNIQUE (SB_CUSTOM_VENDORS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_CUSTOM_VENDORS_2 UNIQUE (SB_CUSTOM_VENDORS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_VENDOR_NAME ON SB_CUSTOM_VENDORS (VENDOR_NAME,EFFECTIVE_UNTIL)
^


GRANT ALL ON SB_CUSTOM_VENDORS TO EUSER
^

CREATE TABLE  SB_VENDOR
( 
     REC_VERSION EV_INT NOT NULL,
     SB_VENDOR_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     VENDORS_LEVEL EV_CHAR1,
     VENDOR_NBR EV_INT NOT NULL,
        CONSTRAINT PK_SB_VENDOR PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_VENDOR_1 UNIQUE (SB_VENDOR_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_VENDOR_2 UNIQUE (SB_VENDOR_NBR,EFFECTIVE_UNTIL)
)
^


GRANT ALL ON SB_VENDOR TO EUSER
^

CREATE TABLE  SB_VENDOR_DETAIL
( 
     REC_VERSION EV_INT NOT NULL,
     SB_VENDOR_DETAIL_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SB_VENDOR_NBR EV_INT NOT NULL,
     DETAIL EV_STR40 NOT NULL,
     DETAIL_LEVEL EV_CHAR1,
     CO_DETAIL_REQUIRED EV_CHAR1,
        CONSTRAINT PK_SB_VENDOR_DETAIL PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_VENDOR_DETAIL_1 UNIQUE (SB_VENDOR_DETAIL_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_VENDOR_DETAIL_2 UNIQUE (SB_VENDOR_DETAIL_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_VENDOR_DETAIL_1 ON SB_VENDOR_DETAIL (SB_VENDOR_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX LK_SB_VENDOR_DETAIL_1 ON SB_VENDOR_DETAIL (SB_VENDOR_NBR,DETAIL)
^


GRANT ALL ON SB_VENDOR_DETAIL TO EUSER
^

CREATE TABLE  SB_VENDOR_DETAIL_VALUES
( 
     REC_VERSION EV_INT NOT NULL,
     SB_VENDOR_DETAIL_VALUES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SB_VENDOR_DETAIL_NBR EV_INT NOT NULL,
     DETAIL_VALUES EV_STR80 NOT NULL,
        CONSTRAINT PK_SB_VENDOR_DETAIL_VALUES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_VENDOR_DETAIL_VALUES_1 UNIQUE (SB_VENDOR_DETAIL_VALUES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_VENDOR_DETAIL_VALUES_2 UNIQUE (SB_VENDOR_DETAIL_VALUES_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_VENDOR_DETAIL_VALUES_1 ON SB_VENDOR_DETAIL_VALUES (SB_VENDOR_DETAIL_NBR,EFFECTIVE_UNTIL)
^

CREATE INDEX LK_SB_VENDOR_DETAIL_VALUES_1 ON SB_VENDOR_DETAIL_VALUES (SB_VENDOR_DETAIL_NBR,DETAIL_VALUES)
^

GRANT ALL ON SB_VENDOR_DETAIL_VALUES TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check SB_ACCOUNTANT */
  child_table = 'SB_ACCOUNTANT';

  parent_table = 'SB_BANK_ACCOUNTS';
  child_field = 'CREDIT_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANK_ACCOUNTS';
  child_field = 'DEBIT_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY */
  child_table = 'SB_AGENCY';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY_REPORTS */
  child_table = 'SB_AGENCY_REPORTS';

  parent_table = 'SB_AGENCY';
  child_field = 'SB_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_BANK_ACCOUNTS */
  child_table = 'SB_BANK_ACCOUNTS';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'ACH_ORIGIN_SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'LOGO_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'SIGNATURE_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_COMPANY_SVCS */
  child_table = 'SB_DELIVERY_COMPANY_SVCS';

  parent_table = 'SB_DELIVERY_COMPANY';
  child_field = 'SB_DELIVERY_COMPANY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_SERVICE_OPT */
  child_table = 'SB_DELIVERY_SERVICE_OPT';

  parent_table = 'SB_DELIVERY_SERVICE';
  child_field = 'SB_DELIVERY_SERVICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX */
  child_table = 'SB_MAIL_BOX';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'UP_LEVEL_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORT_WRITER_REPORTS';
  child_field = 'SB_COVER_LETTER_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_CONTENT */
  child_table = 'SB_MAIL_BOX_CONTENT';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_OPTION */
  child_table = 'SB_MAIL_BOX_OPTION';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MAIL_BOX_CONTENT';
  child_field = 'SB_MAIL_BOX_CONTENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MEDIA_TYPE_OPTION */
  child_table = 'SB_MEDIA_TYPE_OPTION';

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_QUEUE_PRIORITY */
  child_table = 'SB_QUEUE_PRIORITY';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_CLIENTS */
  child_table = 'SB_SEC_CLIENTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_GROUP_MEMBERS */
  child_table = 'SB_SEC_GROUP_MEMBERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_RIGHTS */
  child_table = 'SB_SEC_RIGHTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_ROW_FILTERS */
  child_table = 'SB_SEC_ROW_FILTERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES */
  child_table = 'SB_SERVICES';

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES_CALCULATIONS */
  child_table = 'SB_SERVICES_CALCULATIONS';

  parent_table = 'SB_SERVICES';
  child_field = 'SB_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TASK */
  child_table = 'SB_TASK';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TEAM_MEMBERS */
  child_table = 'SB_TEAM_MEMBERS';

  parent_table = 'SB_TEAM';
  child_field = 'SB_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER */
  child_table = 'SB_USER';

  parent_table = 'SB_ACCOUNTANT';
  child_field = 'SB_ACCOUNTANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_NOTICE */
  child_table = 'SB_USER_NOTICE';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_PREFERENCES */
  child_table = 'SB_USER_PREFERENCES';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_VENDOR_DETAIL */
  child_table = 'SB_VENDOR_DETAIL';

  parent_table = 'SB_VENDOR';
  child_field = 'SB_VENDOR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_VENDOR_DETAIL_VALUES */
  child_table = 'SB_VENDOR_DETAIL_VALUES';

  parent_table = 'SB_VENDOR_DETAIL';
  child_field = 'SB_VENDOR_DETAIL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sb_user(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE evolution_product CHAR(1);
DECLARE VARIABLE p_evolution_product CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sb_user_nbr , evolution_product
        FROM sb_user
        ORDER BY sb_user_nbr, effective_date
        INTO :rec_version, :effective_date, :sb_user_nbr, :evolution_product
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sb_user_nbr) OR (effective_date = '1/1/1900') OR (p_evolution_product IS DISTINCT FROM evolution_product)) THEN
      BEGIN
        curr_nbr = sb_user_nbr;
        p_evolution_product = evolution_product;
      END
      ELSE
        DELETE FROM sb_user WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, evolution_product
        FROM sb_user
        WHERE sb_user_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :evolution_product
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_evolution_product IS DISTINCT FROM evolution_product)) THEN
      BEGIN
        p_evolution_product = evolution_product;
      END
      ELSE
        DELETE FROM sb_user WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE pack_sb_user TO EUSER
^


CREATE OR ALTER PROCEDURE pack_all
AS
BEGIN
  EXECUTE PROCEDURE pack_sb_user (NULL);
END
^


GRANT EXECUTE ON PROCEDURE PACK_ALL TO EUSER
^


CREATE OR ALTER PROCEDURE del_sb_user(nbr INTEGER)
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  FOR SELECT rec_version FROM sb_user WHERE sb_user_nbr = :nbr ORDER BY effective_date DESC FOR UPDATE WITH LOCK INTO rec_version
  DO
  BEGIN
    DELETE FROM sb_user WHERE rec_version = :rec_version;
  END
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_USER TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_enabled_dashboards(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_enabled_dashboards WHERE sb_enabled_dashboards_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ENABLED_DASHBOARDS TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_dashboards(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_dashboards WHERE sb_dashboards_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_DASHBOARDS TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_aca_group(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_aca_group WHERE sb_aca_group_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ACA_GROUP TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_vendor_detail_values(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_vendor_detail_values WHERE sb_vendor_detail_values_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_VENDOR_DETAIL_VALUES TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_vendor_detail(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_vendor_detail WHERE sb_vendor_detail_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_VENDOR_DETAIL TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_vendor(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_vendor WHERE sb_vendor_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_VENDOR TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_custom_vendors(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_custom_vendors WHERE sb_custom_vendors_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_CUSTOM_VENDORS TO EUSER
^

ALTER TABLE SB_DASHBOARDS
ADD CONSTRAINT C_SB_DASHBOARDS_1 check (effective_date < effective_until)
^

ALTER TABLE SB_ENABLED_DASHBOARDS
ADD CONSTRAINT C_SB_ENABLED_DASHBOARDS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_ACA_GROUP
ADD CONSTRAINT C_SB_ACA_GROUP_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_CUSTOM_VENDORS
ADD CONSTRAINT C_SB_CUSTOM_VENDORS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_VENDOR
ADD CONSTRAINT C_SB_VENDOR_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_VENDOR_DETAIL
ADD CONSTRAINT C_SB_VENDOR_DETAIL_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_VENDOR_DETAIL_VALUES
ADD CONSTRAINT C_SB_VENDOR_DETAIL_VALUES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_SB_9 FOR SB After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, OLD.rec_version, OLD.sb_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 8, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 452, OLD.effective_until);

  /* SB_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 28, OLD.sb_name);

  /* ADDRESS1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 5, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 6, OLD.address2);

  /* CITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 29, OLD.city);

  /* STATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 30, OLD.state);

  /* ZIP_CODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 20, OLD.zip_code);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 31, OLD.e_mail_address);

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);

  /* DEVELOPMENT_MODEM_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 33, OLD.development_modem_number);

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 35, OLD.development_ftp_password);

  /* EIN_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 49, OLD.ein_number);

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);

  /* EFTPS_BANK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);

  /* USE_PRENOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 53, OLD.use_prenote);

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);

  /* PAY_TAX_FROM_PAYABLES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);

  /* AR_EXPORT_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 57, OLD.ar_export_format);

  /* DEFAULT_CHECK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 58, OLD.default_check_format);

  /* MICR_FONT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 59, OLD.micr_font);

  /* MICR_HORIZONTAL_ADJUSTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);

  /* AUTO_SAVE_MINUTES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);

  /* PHONE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 36, OLD.phone);

  /* FAX */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 37, OLD.fax);

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 7, :blob_nbr);
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 4, :blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3, :blob_nbr);
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 27, OLD.ar_import_directory);

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 26, OLD.ach_directory);

  /* SB_URL */
  IF (OLD.sb_url IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 25, OLD.sb_url);

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 18, OLD.days_in_prenote);

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 9, :blob_nbr);
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);

  /* ERROR_SCREEN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 48, OLD.error_screen);

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 15, OLD.pswd_min_length);

  /* PSWD_FORCE_MIXED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);

  /* MISC_CHECK_FORM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 46, OLD.misc_check_form);

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2, :blob_nbr);
  END

  /* MARK_LIABS_PAID_DEFAULT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);

  /* TRUST_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 44, OLD.trust_impound);

  /* TAX_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 43, OLD.tax_impound);

  /* DD_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 42, OLD.dd_impound);

  /* BILLING_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 40, OLD.billing_impound);

  /* WC_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 39, OLD.wc_impound);

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);

  /* SB_EXCEPTION_PAYMENT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);

  /* SB_ACH_FILE_LIMITATIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 543, OLD.dashboard_msg);

  /* EE_LOGIN_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 544, OLD.ee_login_type);

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 550, :blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 551, :blob_nbr);
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 569, :blob_nbr);
  END

  /* THEME */
  IF (OLD.theme IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 571, OLD.theme);

  /* THEME_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@THEME_VALUE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 572, :blob_nbr);
  END

  /* WC_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.wc_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 573, OLD.wc_terms_of_use_modify_date);

  /* ANALYTICS_LICENSE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 574, OLD.analytics_license);

  /* SESSION_LOCKOUT */
  IF (OLD.session_lockout IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 619, OLD.session_lockout);

  /* SESSION_TIMEOUT */
  IF (OLD.session_timeout IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 620, OLD.session_timeout);

END

^

CREATE TRIGGER T_AU_SB_9 FOR SB After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, NEW.rec_version, NEW.sb_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 8, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 452, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_NAME */
  IF (OLD.sb_name IS DISTINCT FROM NEW.sb_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 28, OLD.sb_name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 5, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 6, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 29, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 30, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 20, OLD.zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 31, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS DISTINCT FROM NEW.parent_sb_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_MODEM_NUMBER */
  IF (OLD.development_modem_number IS DISTINCT FROM NEW.development_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 33, OLD.development_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS DISTINCT FROM NEW.development_ftp_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 35, OLD.development_ftp_password);
    changes = changes + 1;
  END

  /* EIN_NUMBER */
  IF (OLD.ein_number IS DISTINCT FROM NEW.ein_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 49, OLD.ein_number);
    changes = changes + 1;
  END

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS DISTINCT FROM NEW.eftps_tin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);
    changes = changes + 1;
  END

  /* EFTPS_BANK_FORMAT */
  IF (OLD.eftps_bank_format IS DISTINCT FROM NEW.eftps_bank_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);
    changes = changes + 1;
  END

  /* USE_PRENOTE */
  IF (OLD.use_prenote IS DISTINCT FROM NEW.use_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 53, OLD.use_prenote);
    changes = changes + 1;
  END

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  IF (OLD.impound_trust_monies_as_receiv IS DISTINCT FROM NEW.impound_trust_monies_as_receiv) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);
    changes = changes + 1;
  END

  /* PAY_TAX_FROM_PAYABLES */
  IF (OLD.pay_tax_from_payables IS DISTINCT FROM NEW.pay_tax_from_payables) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);
    changes = changes + 1;
  END

  /* AR_EXPORT_FORMAT */
  IF (OLD.ar_export_format IS DISTINCT FROM NEW.ar_export_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 57, OLD.ar_export_format);
    changes = changes + 1;
  END

  /* DEFAULT_CHECK_FORMAT */
  IF (OLD.default_check_format IS DISTINCT FROM NEW.default_check_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 58, OLD.default_check_format);
    changes = changes + 1;
  END

  /* MICR_FONT */
  IF (OLD.micr_font IS DISTINCT FROM NEW.micr_font) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 59, OLD.micr_font);
    changes = changes + 1;
  END

  /* MICR_HORIZONTAL_ADJUSTMENT */
  IF (OLD.micr_horizontal_adjustment IS DISTINCT FROM NEW.micr_horizontal_adjustment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);
    changes = changes + 1;
  END

  /* AUTO_SAVE_MINUTES */
  IF (OLD.auto_save_minutes IS DISTINCT FROM NEW.auto_save_minutes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);
    changes = changes + 1;
  END

  /* PHONE */
  IF (OLD.phone IS DISTINCT FROM NEW.phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 36, OLD.phone);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 37, OLD.fax);
    changes = changes + 1;
  END

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 7, :blob_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 4, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3, :blob_nbr);
    changes = changes + 1;
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS DISTINCT FROM NEW.ar_import_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 27, OLD.ar_import_directory);
    changes = changes + 1;
  END

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS DISTINCT FROM NEW.ach_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 26, OLD.ach_directory);
    changes = changes + 1;
  END

  /* SB_URL */
  IF (OLD.sb_url IS DISTINCT FROM NEW.sb_url) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 25, OLD.sb_url);
    changes = changes + 1;
  END

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS DISTINCT FROM NEW.days_in_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 18, OLD.days_in_prenote);
    changes = changes + 1;
  END

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 9, :blob_nbr);
    changes = changes + 1;
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS DISTINCT FROM NEW.user_password_duration_in_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);
    changes = changes + 1;
  END

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS DISTINCT FROM NEW.dummy_tax_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);
    changes = changes + 1;
  END

  /* ERROR_SCREEN */
  IF (OLD.error_screen IS DISTINCT FROM NEW.error_screen) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 48, OLD.error_screen);
    changes = changes + 1;
  END

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS DISTINCT FROM NEW.pswd_min_length) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 15, OLD.pswd_min_length);
    changes = changes + 1;
  END

  /* PSWD_FORCE_MIXED */
  IF (OLD.pswd_force_mixed IS DISTINCT FROM NEW.pswd_force_mixed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 46, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2, :blob_nbr);
    changes = changes + 1;
  END

  /* MARK_LIABS_PAID_DEFAULT */
  IF (OLD.mark_liabs_paid_default IS DISTINCT FROM NEW.mark_liabs_paid_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 44, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 43, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 42, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 40, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 39, OLD.wc_impound);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* SB_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.sb_exception_payment_type IS DISTINCT FROM NEW.sb_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);
    changes = changes + 1;
  END

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS DISTINCT FROM NEW.sb_max_ach_file_total) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);
    changes = changes + 1;
  END

  /* SB_ACH_FILE_LIMITATIONS */
  IF (OLD.sb_ach_file_limitations IS DISTINCT FROM NEW.sb_ach_file_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);
    changes = changes + 1;
  END

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS DISTINCT FROM NEW.sb_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS DISTINCT FROM NEW.dashboard_msg) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.dashboard_msg);
    changes = changes + 1;
  END

  /* EE_LOGIN_TYPE */
  IF (OLD.ee_login_type IS DISTINCT FROM NEW.ee_login_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.ee_login_type);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, :blob_nbr);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS DISTINCT FROM NEW.ess_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 569, :blob_nbr);
    changes = changes + 1;
  END

  /* THEME */
  IF (OLD.theme IS DISTINCT FROM NEW.theme) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 571, OLD.theme);
    changes = changes + 1;
  END

  /* THEME_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@THEME_VALUE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 572, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.wc_terms_of_use_modify_date IS DISTINCT FROM NEW.wc_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 573, OLD.wc_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ANALYTICS_LICENSE */
  IF (OLD.analytics_license IS DISTINCT FROM NEW.analytics_license) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 574, OLD.analytics_license);
    changes = changes + 1;
  END

  /* SESSION_LOCKOUT */
  IF (OLD.session_lockout IS DISTINCT FROM NEW.session_lockout) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 619, OLD.session_lockout);
    changes = changes + 1;
  END

  /* SESSION_TIMEOUT */
  IF (OLD.session_timeout IS DISTINCT FROM NEW.session_timeout) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 620, OLD.session_timeout);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_SB_USER_1 FOR SB_USER After Delete POSITION 1
AS
DECLARE VARIABLE effective_until DATE;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it's the first version record */
  IF (OLD.effective_date = '1/1/1900') THEN
  BEGIN
    IF (EXISTS (SELECT 1 FROM sb_user WHERE sb_user_nbr = OLD.sb_user_nbr AND effective_date > OLD.effective_date)) THEN
      EXECUTE PROCEDURE raise_table_error('sb_user', OLD.rec_version, '', 'S5', 'First version record cannot be deleted.');
  END

  ELSE
  BEGIN
    /* Correct EFFECTIVE_UNTIL of previous version */
    effective_until = OLD.effective_until;
    SELECT effective_date FROM sb_user
    WHERE sb_user_nbr = OLD.sb_user_nbr AND
          effective_date > OLD.effective_date
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :effective_until;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

    UPDATE sb_user SET effective_until = :effective_until
    WHERE sb_user_nbr = OLD.sb_user_nbr AND effective_until = OLD.effective_date;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AD_SB_USER_9 FOR SB_USER After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE evolution_product CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, OLD.rec_version, OLD.sb_user_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', evolution_product  FROM sb_user WHERE sb_user_nbr = OLD.sb_user_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :evolution_product;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 422, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 536, OLD.effective_until);

  /* EVOLUTION_PRODUCT */
  IF ((last_record = 'Y' AND OLD.evolution_product IS NOT NULL) OR (last_record = 'N' AND evolution_product IS DISTINCT FROM OLD.evolution_product)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 621, OLD.evolution_product);


  IF (last_record = 'Y') THEN
  BEGIN
    /* USER_ID */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 435, OLD.user_id);

    /* USER_SIGNATURE */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 419, :blob_nbr);
    END

    /* LAST_NAME */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 420, OLD.last_name);

    /* FIRST_NAME */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 436, OLD.first_name);

    /* MIDDLE_INITIAL */
    IF (OLD.middle_initial IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 440, OLD.middle_initial);

    /* ACTIVE_USER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 441, OLD.active_user);

    /* DEPARTMENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 442, OLD.department);

    /* PASSWORD_CHANGE_DATE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 421, OLD.password_change_date);

    /* SECURITY_LEVEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 443, OLD.security_level);

    /* USER_UPDATE_OPTIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 437, OLD.user_update_options);

    /* USER_FUNCTIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 438, OLD.user_functions);

    /* USER_PASSWORD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 439, OLD.user_password);

    /* EMAIL_ADDRESS */
    IF (OLD.email_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 434, OLD.email_address);

    /* CL_NBR */
    IF (OLD.cl_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 429, OLD.cl_nbr);

    /* SB_ACCOUNTANT_NBR */
    IF (OLD.sb_accountant_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 427, OLD.sb_accountant_nbr);

    /* WRONG_PSWD_ATTEMPTS */
    IF (OLD.wrong_pswd_attempts IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 426, OLD.wrong_pswd_attempts);

    /* LINKS_DATA */
    IF (OLD.links_data IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 433, OLD.links_data);

    /* LOGIN_QUESTION1 */
    IF (OLD.login_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 425, OLD.login_question1);

    /* LOGIN_ANSWER1 */
    IF (OLD.login_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 432, OLD.login_answer1);

    /* LOGIN_QUESTION2 */
    IF (OLD.login_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 424, OLD.login_question2);

    /* LOGIN_ANSWER2 */
    IF (OLD.login_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 431, OLD.login_answer2);

    /* LOGIN_QUESTION3 */
    IF (OLD.login_question3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 423, OLD.login_question3);

    /* LOGIN_ANSWER3 */
    IF (OLD.login_answer3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 430, OLD.login_answer3);

    /* HR_PERSONNEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 545, OLD.hr_personnel);

    /* SEC_QUESTION1 */
    IF (OLD.sec_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 546, OLD.sec_question1);

    /* SEC_ANSWER1 */
    IF (OLD.sec_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 547, OLD.sec_answer1);

    /* SEC_QUESTION2 */
    IF (OLD.sec_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 548, OLD.sec_question2);

    /* SEC_ANSWER2 */
    IF (OLD.sec_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 549, OLD.sec_answer2);

    /* WC_TOU_ACCEPT_DATE */
    IF (OLD.wc_tou_accept_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 575, OLD.wc_tou_accept_date);

    /* ANALYTICS_PERSONNEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 576, OLD.analytics_personnel);

  END
END

^

CREATE TRIGGER T_AIU_SB_USER_3 FOR SB_USER After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.user_id IS DISTINCT FROM NEW.user_id) OR (rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE') IS NOT NULL) OR (OLD.last_name IS DISTINCT FROM NEW.last_name) OR (OLD.first_name IS DISTINCT FROM NEW.first_name) OR (OLD.middle_initial IS DISTINCT FROM NEW.middle_initial) OR (OLD.active_user IS DISTINCT FROM NEW.active_user) OR (OLD.department IS DISTINCT FROM NEW.department) OR (OLD.password_change_date IS DISTINCT FROM NEW.password_change_date) OR (OLD.security_level IS DISTINCT FROM NEW.security_level) OR (OLD.user_update_options IS DISTINCT FROM NEW.user_update_options) OR (OLD.user_functions IS DISTINCT FROM NEW.user_functions) OR (OLD.user_password IS DISTINCT FROM NEW.user_password) OR (OLD.email_address IS DISTINCT FROM NEW.email_address) OR (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) OR (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) OR (OLD.wrong_pswd_attempts IS DISTINCT FROM NEW.wrong_pswd_attempts) OR (OLD.links_data IS DISTINCT FROM NEW.links_data) OR (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) OR (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) OR (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) OR (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) OR (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) OR (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) OR (OLD.hr_personnel IS DISTINCT FROM NEW.hr_personnel) OR (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) OR (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) OR (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) OR (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) OR (OLD.wc_tou_accept_date IS DISTINCT FROM NEW.wc_tou_accept_date) OR (OLD.analytics_personnel IS DISTINCT FROM NEW.analytics_personnel)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE sb_user SET 
      user_id = NEW.user_id, user_signature = NEW.user_signature, last_name = NEW.last_name, first_name = NEW.first_name, middle_initial = NEW.middle_initial, active_user = NEW.active_user, department = NEW.department, password_change_date = NEW.password_change_date, security_level = NEW.security_level, user_update_options = NEW.user_update_options, user_functions = NEW.user_functions, user_password = NEW.user_password, email_address = NEW.email_address, cl_nbr = NEW.cl_nbr, sb_accountant_nbr = NEW.sb_accountant_nbr, wrong_pswd_attempts = NEW.wrong_pswd_attempts, links_data = NEW.links_data, login_question1 = NEW.login_question1, login_answer1 = NEW.login_answer1, login_question2 = NEW.login_question2, login_answer2 = NEW.login_answer2, login_question3 = NEW.login_question3, login_answer3 = NEW.login_answer3, hr_personnel = NEW.hr_personnel, sec_question1 = NEW.sec_question1, sec_answer1 = NEW.sec_answer1, sec_question2 = NEW.sec_question2, sec_answer2 = NEW.sec_answer2, wc_tou_accept_date = NEW.wc_tou_accept_date, analytics_personnel = NEW.analytics_personnel
      WHERE sb_user_nbr = NEW.sb_user_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_SB_USER_1 FOR SB_USER After Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((OLD.effective_date IS DISTINCT FROM NEW.effective_date) AND
      (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL of old previous version */
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sb_user SET effective_until = OLD.effective_until
    WHERE sb_user_nbr = NEW.sb_user_nbr AND effective_until = OLD.effective_date;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AU_SB_USER_9 FOR SB_USER After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, NEW.rec_version, NEW.sb_user_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 422, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 536, OLD.effective_until);
    changes = changes + 1;
  END

  /* USER_ID */
  IF (OLD.user_id IS DISTINCT FROM NEW.user_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 435, OLD.user_id);
    changes = changes + 1;
  END

  /* USER_SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 419, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_NAME */
  IF (OLD.last_name IS DISTINCT FROM NEW.last_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 420, OLD.last_name);
    changes = changes + 1;
  END

  /* FIRST_NAME */
  IF (OLD.first_name IS DISTINCT FROM NEW.first_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 436, OLD.first_name);
    changes = changes + 1;
  END

  /* MIDDLE_INITIAL */
  IF (OLD.middle_initial IS DISTINCT FROM NEW.middle_initial) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 440, OLD.middle_initial);
    changes = changes + 1;
  END

  /* ACTIVE_USER */
  IF (OLD.active_user IS DISTINCT FROM NEW.active_user) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 441, OLD.active_user);
    changes = changes + 1;
  END

  /* DEPARTMENT */
  IF (OLD.department IS DISTINCT FROM NEW.department) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 442, OLD.department);
    changes = changes + 1;
  END

  /* PASSWORD_CHANGE_DATE */
  IF (OLD.password_change_date IS DISTINCT FROM NEW.password_change_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 421, OLD.password_change_date);
    changes = changes + 1;
  END

  /* SECURITY_LEVEL */
  IF (OLD.security_level IS DISTINCT FROM NEW.security_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 443, OLD.security_level);
    changes = changes + 1;
  END

  /* USER_UPDATE_OPTIONS */
  IF (OLD.user_update_options IS DISTINCT FROM NEW.user_update_options) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 437, OLD.user_update_options);
    changes = changes + 1;
  END

  /* USER_FUNCTIONS */
  IF (OLD.user_functions IS DISTINCT FROM NEW.user_functions) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 438, OLD.user_functions);
    changes = changes + 1;
  END

  /* USER_PASSWORD */
  IF (OLD.user_password IS DISTINCT FROM NEW.user_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 439, OLD.user_password);
    changes = changes + 1;
  END

  /* EMAIL_ADDRESS */
  IF (OLD.email_address IS DISTINCT FROM NEW.email_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 434, OLD.email_address);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 429, OLD.cl_nbr);
    changes = changes + 1;
  END

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 427, OLD.sb_accountant_nbr);
    changes = changes + 1;
  END

  /* WRONG_PSWD_ATTEMPTS */
  IF (OLD.wrong_pswd_attempts IS DISTINCT FROM NEW.wrong_pswd_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 426, OLD.wrong_pswd_attempts);
    changes = changes + 1;
  END

  /* LINKS_DATA */
  IF (OLD.links_data IS DISTINCT FROM NEW.links_data) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 433, OLD.links_data);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 425, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 432, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 424, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 431, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 423, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 430, OLD.login_answer3);
    changes = changes + 1;
  END

  /* HR_PERSONNEL */
  IF (OLD.hr_personnel IS DISTINCT FROM NEW.hr_personnel) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 545, OLD.hr_personnel);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 546, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 547, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 548, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 549, OLD.sec_answer2);
    changes = changes + 1;
  END

  /* WC_TOU_ACCEPT_DATE */
  IF (OLD.wc_tou_accept_date IS DISTINCT FROM NEW.wc_tou_accept_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 575, OLD.wc_tou_accept_date);
    changes = changes + 1;
  END

  /* ANALYTICS_PERSONNEL */
  IF (OLD.analytics_personnel IS DISTINCT FROM NEW.analytics_personnel) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 576, OLD.analytics_personnel);
    changes = changes + 1;
  END

  /* EVOLUTION_PRODUCT */
  IF (OLD.evolution_product IS DISTINCT FROM NEW.evolution_product) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 621, OLD.evolution_product);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_USER_1 FOR SB_USER Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.EVOLUTION_PRODUCT is null OR CHAR_LENGTH(TRIM(new.EVOLUTION_PRODUCT))=0) THEN 
      new.EVOLUTION_PRODUCT = 'N'; 

  END 
END

^

CREATE TRIGGER T_BIU_SB_USER_3 FOR SB_USER Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_USER */
  IF (INSERTING OR (OLD.user_id IS DISTINCT FROM NEW.user_id)) THEN
    IF (EXISTS(SELECT 1 FROM sb_user WHERE user_id = NEW.user_id AND sb_user_nbr <> NEW.sb_user_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_user', 'user_id',
      CAST(NEW.user_id as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_USER_1 FOR SB_USER Before Insert POSITION 1
AS
DECLARE VARIABLE new_nbr INTEGER;
DECLARE VARIABLE new_effective_date DATE;
DECLARE VARIABLE new_effective_until DATE;
DECLARE VARIABLE prev_rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_user_ver;

  new_nbr = NEW.sb_user_nbr;
  new_effective_date = NEW.effective_date;
  new_effective_until = NEW.effective_until;

  /* Calc EFFECTIVE_DATE of this version */
  IF (EXISTS (SELECT 1 FROM sb_user WHERE sb_user_nbr = :new_nbr ROWS 1)) THEN
  BEGIN
    IF (new_effective_date IS NULL) THEN
      new_effective_date = CURRENT_DATE;
  END
  ELSE
    new_effective_date = '1/1/1900';

  /* Correct EFFECTIVE_UNTIL of previous version */
  prev_rec_version = NULL;
  SELECT rec_version FROM sb_user WHERE sb_user_nbr = :new_nbr AND effective_date < :new_effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :prev_rec_version;

  IF (prev_rec_version IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sb_user SET effective_until = :new_effective_date WHERE rec_version = :prev_rec_version;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END

  /* Calc EFFECTIVE_UNTIL of this version */
  SELECT effective_date FROM sb_user WHERE sb_user_nbr = :new_nbr AND effective_date > :new_effective_date
  ORDER BY effective_date
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :new_effective_until;

  IF (new_effective_until IS NULL) THEN
    new_effective_until = '12/31/9999';

  /* Final assignment */
  NEW.effective_date = new_effective_date;
  NEW.effective_until = new_effective_until;
END

^

CREATE TRIGGER T_BU_SB_USER_1 FOR SB_USER Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it is changing system fields */
  IF ((NEW.effective_date IS DISTINCT FROM OLD.effective_date) AND (OLD.effective_date = '1/1/1900')) THEN
    NEW.effective_date = OLD.effective_date;

  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_user_nbr IS DISTINCT FROM OLD.sb_user_nbr) OR
     (NEW.effective_until IS DISTINCT FROM OLD.effective_until)) THEN
  BEGIN
    IF (NEW.effective_until IS DISTINCT FROM OLD.effective_until) THEN
    BEGIN
      IF (EXISTS (SELECT 1 FROM sb_user WHERE sb_user_nbr = NEW.sb_user_nbr AND effective_date = OLD.effective_until)) THEN
        NEW.effective_until = OLD.effective_until;
      ELSE
        IF (NEW.effective_until IS NULL) THEN
          NEW.effective_until = '12/31/9999';
    END
    ELSE
      EXECUTE PROCEDURE raise_table_error('sb_user', NEW.rec_version, '', 'S1', 'System fields cannot be modified');
  END

  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL */
    NEW.effective_until = '12/31/9999';
    SELECT effective_date FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
          effective_date > NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO NEW.effective_until;

    /* Correct EFFECTIVE_UNTIL of new previous version */
    rec_version = NULL;
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
          effective_date < NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date DESC
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :rec_version;

    IF (rec_version IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
      UPDATE sb_user SET effective_until = NEW.effective_date WHERE rec_version = :rec_version;
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_SB_DASHBOARDS_9 FOR SB_DASHBOARDS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(49, OLD.rec_version, OLD.sb_dashboards_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 592, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 593, OLD.effective_until);

  /* DASHBOARD_TYPE */
  IF (OLD.dashboard_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 594, OLD.dashboard_type);

  /* SY_ANALYTICS_TIER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 595, OLD.sy_analytics_tier_nbr);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 596, :blob_nbr);
  END

  /* DASHBOARD_ID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 597, OLD.dashboard_id);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 598, OLD.description);

END

^

CREATE TRIGGER T_AI_SB_DASHBOARDS_9 FOR SB_DASHBOARDS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(49, NEW.rec_version, NEW.sb_dashboards_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_DASHBOARDS_9 FOR SB_DASHBOARDS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(49, NEW.rec_version, NEW.sb_dashboards_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 592, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 593, OLD.effective_until);
    changes = changes + 1;
  END

  /* DASHBOARD_TYPE */
  IF (OLD.dashboard_type IS DISTINCT FROM NEW.dashboard_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 594, OLD.dashboard_type);
    changes = changes + 1;
  END

  /* SY_ANALYTICS_TIER_NBR */
  IF (OLD.sy_analytics_tier_nbr IS DISTINCT FROM NEW.sy_analytics_tier_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 595, OLD.sy_analytics_tier_nbr);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 596, :blob_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_ID */
  IF (OLD.dashboard_id IS DISTINCT FROM NEW.dashboard_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 597, OLD.dashboard_id);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 598, OLD.description);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_DASHBOARDS_1 FOR SB_DASHBOARDS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.DASHBOARD_TYPE is null OR CHAR_LENGTH(TRIM(new.DASHBOARD_TYPE))=0) THEN 
      new.DASHBOARD_TYPE = 'N'; 

  END 
END

^

CREATE TRIGGER T_BIU_SB_DASHBOARDS_3 FOR SB_DASHBOARDS Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_DASHBOARDS_1 */
  IF (INSERTING OR (OLD.description IS DISTINCT FROM NEW.description) OR (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    IF (EXISTS(SELECT 1 FROM sb_dashboards WHERE description = NEW.description AND effective_until = NEW.effective_until AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_dashboards', 'description, effective_until',
      CAST(NEW.description || ', ' || NEW.effective_until as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_DASHBOARDS_1 FOR SB_DASHBOARDS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_dashboards_ver;

  IF (EXISTS (SELECT 1 FROM sb_dashboards WHERE sb_dashboards_nbr = NEW.sb_dashboards_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_dashboards', NEW.sb_dashboards_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BUD_SB_DASHBOARDS_9 FOR SB_DASHBOARDS Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END
END

^

CREATE TRIGGER T_BU_SB_DASHBOARDS_1 FOR SB_DASHBOARDS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_dashboards_nbr IS DISTINCT FROM OLD.sb_dashboards_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_dashboards', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_ENABLED_DASHBOARDS_9 FOR SB_ENABLED_DASHBOARDS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(50, OLD.rec_version, OLD.sb_enabled_dashboards_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 601, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 602, OLD.effective_until);

  /* DASHBOARD_LEVEL */
  IF (OLD.dashboard_level IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 603, OLD.dashboard_level);

  /* DASHBOARD_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 604, OLD.dashboard_nbr);

END

^

CREATE TRIGGER T_AI_SB_ENABLED_DASHBOARDS_9 FOR SB_ENABLED_DASHBOARDS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(50, NEW.rec_version, NEW.sb_enabled_dashboards_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ENABLED_DASHBOARDS_9 FOR SB_ENABLED_DASHBOARDS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(50, NEW.rec_version, NEW.sb_enabled_dashboards_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 601, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 602, OLD.effective_until);
    changes = changes + 1;
  END

  /* DASHBOARD_LEVEL */
  IF (OLD.dashboard_level IS DISTINCT FROM NEW.dashboard_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 603, OLD.dashboard_level);
    changes = changes + 1;
  END

  /* DASHBOARD_NBR */
  IF (OLD.dashboard_nbr IS DISTINCT FROM NEW.dashboard_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 604, OLD.dashboard_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ENABLED_DASHBOARDS_1 FOR SB_ENABLED_DASHBOARDS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.DASHBOARD_LEVEL is null OR CHAR_LENGTH(TRIM(new.DASHBOARD_LEVEL))=0) THEN 
      new.DASHBOARD_LEVEL = 'S'; 

  END 
END

^

CREATE TRIGGER T_BI_SB_ENABLED_DASHBOARDS_1 FOR SB_ENABLED_DASHBOARDS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_enabled_dashboards_ver;

  IF (EXISTS (SELECT 1 FROM sb_enabled_dashboards WHERE sb_enabled_dashboards_nbr = NEW.sb_enabled_dashboards_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_enabled_dashboards', NEW.sb_enabled_dashboards_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_ENABLED_DASHBOARDS_1 FOR SB_ENABLED_DASHBOARDS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_enabled_dashboards_nbr IS DISTINCT FROM OLD.sb_enabled_dashboards_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_enabled_dashboards', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_ACA_GROUP_9 FOR SB_ACA_GROUP After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(51, OLD.rec_version, OLD.sb_aca_group_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 607, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 608, OLD.effective_until);

  /* ACA_GROUP_DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 609, OLD.aca_group_description);

  /* PRIMARY_CL_NBR */
  IF (OLD.primary_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 610, OLD.primary_cl_nbr);

  /* PRIMARY_CO_NBR */
  IF (OLD.primary_co_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 611, OLD.primary_co_nbr);

END

^

CREATE TRIGGER T_AI_SB_ACA_GROUP_9 FOR SB_ACA_GROUP After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(51, NEW.rec_version, NEW.sb_aca_group_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ACA_GROUP_9 FOR SB_ACA_GROUP After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(51, NEW.rec_version, NEW.sb_aca_group_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 607, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 608, OLD.effective_until);
    changes = changes + 1;
  END

  /* ACA_GROUP_DESCRIPTION */
  IF (OLD.aca_group_description IS DISTINCT FROM NEW.aca_group_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 609, OLD.aca_group_description);
    changes = changes + 1;
  END

  /* PRIMARY_CL_NBR */
  IF (OLD.primary_cl_nbr IS DISTINCT FROM NEW.primary_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 610, OLD.primary_cl_nbr);
    changes = changes + 1;
  END

  /* PRIMARY_CO_NBR */
  IF (OLD.primary_co_nbr IS DISTINCT FROM NEW.primary_co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 611, OLD.primary_co_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ACA_GROUP_3 FOR SB_ACA_GROUP Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_ACA_GROUP */
  IF (INSERTING OR (OLD.aca_group_description IS DISTINCT FROM NEW.aca_group_description) OR (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    IF (EXISTS(SELECT 1 FROM sb_aca_group WHERE aca_group_description = NEW.aca_group_description AND effective_until = NEW.effective_until AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_aca_group', 'aca_group_description, effective_until',
      CAST(NEW.aca_group_description || ', ' || NEW.effective_until as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_ACA_GROUP_1 FOR SB_ACA_GROUP Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_aca_group_ver;

  IF (EXISTS (SELECT 1 FROM sb_aca_group WHERE sb_aca_group_nbr = NEW.sb_aca_group_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_aca_group', NEW.sb_aca_group_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_ACA_GROUP_1 FOR SB_ACA_GROUP Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_aca_group_nbr IS DISTINCT FROM OLD.sb_aca_group_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_aca_group', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_CUSTOM_VENDORS_9 FOR SB_CUSTOM_VENDORS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(52, OLD.rec_version, OLD.sb_custom_vendors_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 614, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 615, OLD.effective_until);

  /* VENDOR_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 616, OLD.vendor_name);

  /* SY_VENDOR_CATEGORIES_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 617, OLD.sy_vendor_categories_nbr);

  /* VENDOR_TYPE */
  IF (OLD.vendor_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 618, OLD.vendor_type);

END

^

CREATE TRIGGER T_AI_SB_CUSTOM_VENDORS_9 FOR SB_CUSTOM_VENDORS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(52, NEW.rec_version, NEW.sb_custom_vendors_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_CUSTOM_VENDORS_9 FOR SB_CUSTOM_VENDORS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(52, NEW.rec_version, NEW.sb_custom_vendors_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 614, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 615, OLD.effective_until);
    changes = changes + 1;
  END

  /* VENDOR_NAME */
  IF (OLD.vendor_name IS DISTINCT FROM NEW.vendor_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 616, OLD.vendor_name);
    changes = changes + 1;
  END

  /* SY_VENDOR_CATEGORIES_NBR */
  IF (OLD.sy_vendor_categories_nbr IS DISTINCT FROM NEW.sy_vendor_categories_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 617, OLD.sy_vendor_categories_nbr);
    changes = changes + 1;
  END

  /* VENDOR_TYPE */
  IF (OLD.vendor_type IS DISTINCT FROM NEW.vendor_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 618, OLD.vendor_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_CUSTOM_VENDORS_1 FOR SB_CUSTOM_VENDORS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.VENDOR_TYPE is null OR CHAR_LENGTH(TRIM(new.VENDOR_TYPE))=0) THEN 
      new.VENDOR_TYPE = 'O'; 

  END 
END

^

CREATE TRIGGER T_BIU_SB_CUSTOM_VENDORS_3 FOR SB_CUSTOM_VENDORS Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_VENDOR_NAME */
  IF (INSERTING OR (OLD.vendor_name IS DISTINCT FROM NEW.vendor_name) OR (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    IF (EXISTS(SELECT 1 FROM sb_custom_vendors WHERE vendor_name = NEW.vendor_name AND effective_until = NEW.effective_until AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_custom_vendors', 'vendor_name, effective_until',
      CAST(NEW.vendor_name || ', ' || NEW.effective_until as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_CUSTOM_VENDORS_1 FOR SB_CUSTOM_VENDORS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_custom_vendors_ver;

  IF (EXISTS (SELECT 1 FROM sb_custom_vendors WHERE sb_custom_vendors_nbr = NEW.sb_custom_vendors_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_custom_vendors', NEW.sb_custom_vendors_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_CUSTOM_VENDORS_1 FOR SB_CUSTOM_VENDORS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_custom_vendors_nbr IS DISTINCT FROM OLD.sb_custom_vendors_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_custom_vendors', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_VENDOR_9 FOR SB_VENDOR After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(53, OLD.rec_version, OLD.sb_vendor_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 624, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 625, OLD.effective_until);

  /* VENDORS_LEVEL */
  IF (OLD.vendors_level IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 626, OLD.vendors_level);

  /* VENDOR_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 627, OLD.vendor_nbr);

END

^

CREATE TRIGGER T_AI_SB_VENDOR_9 FOR SB_VENDOR After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(53, NEW.rec_version, NEW.sb_vendor_nbr, 'I');
END

^

CREATE TRIGGER T_AUD_SB_VENDOR_2 FOR SB_VENDOR After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_vendor WHERE sb_vendor_nbr = OLD.sb_vendor_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_vendor WHERE sb_vendor_nbr = OLD.sb_vendor_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_VENDOR_DETAIL */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_vendor_detail
    WHERE (sb_vendor_nbr = OLD.sb_vendor_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_vendor', OLD.sb_vendor_nbr, 'sb_vendor_detail', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_vendor_detail
    WHERE (sb_vendor_nbr = OLD.sb_vendor_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_vendor', OLD.sb_vendor_nbr, 'sb_vendor_detail', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_SB_VENDOR_9 FOR SB_VENDOR After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(53, NEW.rec_version, NEW.sb_vendor_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 624, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 625, OLD.effective_until);
    changes = changes + 1;
  END

  /* VENDORS_LEVEL */
  IF (OLD.vendors_level IS DISTINCT FROM NEW.vendors_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 626, OLD.vendors_level);
    changes = changes + 1;
  END

  /* VENDOR_NBR */
  IF (OLD.vendor_nbr IS DISTINCT FROM NEW.vendor_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 627, OLD.vendor_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_VENDOR_1 FOR SB_VENDOR Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.VENDORS_LEVEL is null OR CHAR_LENGTH(TRIM(new.VENDORS_LEVEL))=0) THEN 
      new.VENDORS_LEVEL = 'S'; 

  END 
END

^

CREATE TRIGGER T_BI_SB_VENDOR_1 FOR SB_VENDOR Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_vendor_ver;

  IF (EXISTS (SELECT 1 FROM sb_vendor WHERE sb_vendor_nbr = NEW.sb_vendor_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_vendor', NEW.sb_vendor_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_VENDOR_DETAIL_9 FOR SB_VENDOR_DETAIL After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(54, OLD.rec_version, OLD.sb_vendor_detail_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 630, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 631, OLD.effective_until);

  /* SB_VENDOR_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 632, OLD.sb_vendor_nbr);

  /* DETAIL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 633, OLD.detail);

  /* DETAIL_LEVEL */
  IF (OLD.detail_level IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 634, OLD.detail_level);

  /* CO_DETAIL_REQUIRED */
  IF (OLD.co_detail_required IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 635, OLD.co_detail_required);

END

^

CREATE TRIGGER T_AI_SB_VENDOR_DETAIL_9 FOR SB_VENDOR_DETAIL After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(54, NEW.rec_version, NEW.sb_vendor_detail_nbr, 'I');
END

^

CREATE TRIGGER T_AUD_SB_VENDOR_DETAIL_2 FOR SB_VENDOR_DETAIL After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_vendor_detail WHERE sb_vendor_detail_nbr = OLD.sb_vendor_detail_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_vendor_detail WHERE sb_vendor_detail_nbr = OLD.sb_vendor_detail_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_VENDOR_DETAIL_VALUES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_vendor_detail_values
    WHERE (sb_vendor_detail_nbr = OLD.sb_vendor_detail_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_vendor_detail', OLD.sb_vendor_detail_nbr, 'sb_vendor_detail_values', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_vendor_detail_values
    WHERE (sb_vendor_detail_nbr = OLD.sb_vendor_detail_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_vendor_detail', OLD.sb_vendor_detail_nbr, 'sb_vendor_detail_values', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_SB_VENDOR_DETAIL_9 FOR SB_VENDOR_DETAIL After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(54, NEW.rec_version, NEW.sb_vendor_detail_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 630, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 631, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_VENDOR_NBR */
  IF (OLD.sb_vendor_nbr IS DISTINCT FROM NEW.sb_vendor_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 632, OLD.sb_vendor_nbr);
    changes = changes + 1;
  END

  /* DETAIL */
  IF (OLD.detail IS DISTINCT FROM NEW.detail) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 633, OLD.detail);
    changes = changes + 1;
  END

  /* DETAIL_LEVEL */
  IF (OLD.detail_level IS DISTINCT FROM NEW.detail_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 634, OLD.detail_level);
    changes = changes + 1;
  END

  /* CO_DETAIL_REQUIRED */
  IF (OLD.co_detail_required IS DISTINCT FROM NEW.co_detail_required) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 635, OLD.co_detail_required);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_VENDOR_DETAIL_1 FOR SB_VENDOR_DETAIL Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.DETAIL_LEVEL is null OR CHAR_LENGTH(TRIM(new.DETAIL_LEVEL))=0) THEN 
      new.DETAIL_LEVEL = 'B'; 

    IF (new.CO_DETAIL_REQUIRED is null OR CHAR_LENGTH(TRIM(new.CO_DETAIL_REQUIRED))=0) THEN 
      new.CO_DETAIL_REQUIRED = 'Y'; 

  END 
END

^

CREATE TRIGGER T_BIU_SB_VENDOR_DETAIL_2 FOR SB_VENDOR_DETAIL Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_VENDOR */
  IF ((NEW.sb_vendor_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_vendor_nbr IS DISTINCT FROM NEW.sb_vendor_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_vendor
    WHERE sb_vendor_nbr = NEW.sb_vendor_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_vendor_detail', NEW.sb_vendor_detail_nbr, 'sb_vendor_nbr', NEW.sb_vendor_nbr, 'sb_vendor', NEW.effective_date);

    SELECT rec_version FROM sb_vendor
    WHERE sb_vendor_nbr = NEW.sb_vendor_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_vendor_detail', NEW.sb_vendor_detail_nbr, 'sb_vendor_nbr', NEW.sb_vendor_nbr, 'sb_vendor', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_SB_VENDOR_DETAIL_1 FOR SB_VENDOR_DETAIL Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_vendor_detail_ver;

  IF (EXISTS (SELECT 1 FROM sb_vendor_detail WHERE sb_vendor_detail_nbr = NEW.sb_vendor_detail_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_vendor_detail', NEW.sb_vendor_detail_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_VENDOR_DETAIL_1 FOR SB_VENDOR_DETAIL Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_vendor_detail_nbr IS DISTINCT FROM OLD.sb_vendor_detail_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_vendor_detail', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BIU_SB_VENDOR_DETAIL_3 FOR SB_VENDOR_DETAIL Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_VENDOR_DETAIL_1 */
  IF (INSERTING OR (OLD.sb_vendor_nbr IS DISTINCT FROM NEW.sb_vendor_nbr) OR (OLD.detail IS DISTINCT FROM NEW.detail)) THEN
    IF (EXISTS(SELECT 1 FROM sb_vendor_detail WHERE sb_vendor_nbr = NEW.sb_vendor_nbr AND detail = NEW.detail AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_vendor_detail', 'sb_vendor_nbr, detail',
      CAST(NEW.sb_vendor_nbr || ', ' || NEW.detail as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^


CREATE TRIGGER T_AD_SB_VENDOR_DETAIL_VALUES_9 FOR SB_VENDOR_DETAIL_VALUES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(55, OLD.rec_version, OLD.sb_vendor_detail_values_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 638, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 639, OLD.effective_until);

  /* SB_VENDOR_DETAIL_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 640, OLD.sb_vendor_detail_nbr);

  /* DETAIL_VALUES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 641, OLD.detail_values);

END

^

CREATE TRIGGER T_AI_SB_VENDOR_DETAIL_VALUES_9 FOR SB_VENDOR_DETAIL_VALUES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(55, NEW.rec_version, NEW.sb_vendor_detail_values_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_VENDOR_DETAIL_VALUES_9 FOR SB_VENDOR_DETAIL_VALUES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(55, NEW.rec_version, NEW.sb_vendor_detail_values_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 638, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 639, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_VENDOR_DETAIL_NBR */
  IF (OLD.sb_vendor_detail_nbr IS DISTINCT FROM NEW.sb_vendor_detail_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 640, OLD.sb_vendor_detail_nbr);
    changes = changes + 1;
  END

  /* DETAIL_VALUES */
  IF (OLD.detail_values IS DISTINCT FROM NEW.detail_values) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 641, OLD.detail_values);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_VENDOR_DETAIL_VALUES_2 FOR SB_VENDOR_DETAIL_VALUES Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_VENDOR_DETAIL */
  IF ((NEW.sb_vendor_detail_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_vendor_detail_nbr IS DISTINCT FROM NEW.sb_vendor_detail_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_vendor_detail
    WHERE sb_vendor_detail_nbr = NEW.sb_vendor_detail_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_vendor_detail_values', NEW.sb_vendor_detail_values_nbr, 'sb_vendor_detail_nbr', NEW.sb_vendor_detail_nbr, 'sb_vendor_detail', NEW.effective_date);

    SELECT rec_version FROM sb_vendor_detail
    WHERE sb_vendor_detail_nbr = NEW.sb_vendor_detail_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_vendor_detail_values', NEW.sb_vendor_detail_values_nbr, 'sb_vendor_detail_nbr', NEW.sb_vendor_detail_nbr, 'sb_vendor_detail', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_SB_VENDOR_DETAIL_VALUES_1 FOR SB_VENDOR_DETAIL_VALUES Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_vendor_detail_values_ver;

  IF (EXISTS (SELECT 1 FROM sb_vendor_detail_values WHERE sb_vendor_detail_values_nbr = NEW.sb_vendor_detail_values_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_vendor_detail_values', NEW.sb_vendor_detail_values_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_VENDOR_DETAIL_VALUES_1 FOR SB_VENDOR_DETAIL_VALUES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_vendor_detail_values_nbr IS DISTINCT FROM OLD.sb_vendor_detail_values_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_vendor_detail_values', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^


CREATE TRIGGER T_BIU_SB_VENDOR_DETAIL_VALUES_3 FOR SB_VENDOR_DETAIL_VALUES Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_VENDOR_DETAIL_VALUES_1 */
  IF (INSERTING OR (OLD.sb_vendor_detail_nbr IS DISTINCT FROM NEW.sb_vendor_detail_nbr) OR (OLD.detail_values IS DISTINCT FROM NEW.detail_values)) THEN
    IF (EXISTS(SELECT 1 FROM sb_vendor_detail_values WHERE sb_vendor_detail_nbr = NEW.sb_vendor_detail_nbr AND detail_values = NEW.detail_values AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_vendor_detail_values', 'sb_vendor_detail_nbr, detail_values',
      CAST(NEW.sb_vendor_detail_nbr || ', ' || NEW.detail_values as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^





COMMIT^



DROP INDEX I_EV_TRANSACTION_3
^

CREATE UNIQUE DESCENDING INDEX I_EV_TRANSACTION_3 ON EV_TRANSACTION (COMMIT_NBR)
^


/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  UPDATE ev_table SET name = 'SB_USER', versioned = 'Y' WHERE nbr = 44;
  INSERT INTO ev_table (nbr, name, versioned) VALUES (49, 'SB_DASHBOARDS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (50, 'SB_ENABLED_DASHBOARDS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (51, 'SB_ACA_GROUP', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (52, 'SB_CUSTOM_VENDORS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (53, 'SB_VENDOR', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (54, 'SB_VENDOR_DETAIL', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (55, 'SB_VENDOR_DETAIL_VALUES', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 619, 'SESSION_LOCKOUT', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 620, 'SESSION_TIMEOUT', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 621, 'EVOLUTION_PRODUCT', 'C', 1, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 590, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 591, 'SB_DASHBOARDS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 592, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 593, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 594, 'DASHBOARD_TYPE', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 595, 'SY_ANALYTICS_TIER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 596, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 597, 'DASHBOARD_ID', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (49, 598, 'DESCRIPTION', 'V', 80, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (50, 599, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (50, 600, 'SB_ENABLED_DASHBOARDS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (50, 601, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (50, 602, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (50, 603, 'DASHBOARD_LEVEL', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (50, 604, 'DASHBOARD_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 605, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 606, 'SB_ACA_GROUP_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 607, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 608, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 609, 'ACA_GROUP_DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 610, 'PRIMARY_CL_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (51, 611, 'PRIMARY_CO_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 612, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 613, 'SB_CUSTOM_VENDORS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 614, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 615, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 616, 'VENDOR_NAME', 'V', 80, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 617, 'SY_VENDOR_CATEGORIES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (52, 618, 'VENDOR_TYPE', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (53, 622, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (53, 623, 'SB_VENDOR_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (53, 624, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (53, 625, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (53, 626, 'VENDORS_LEVEL', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (53, 627, 'VENDOR_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 628, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 629, 'SB_VENDOR_DETAIL_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 630, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 631, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 632, 'SB_VENDOR_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 633, 'DETAIL', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 634, 'DETAIL_LEVEL', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (54, 635, 'CO_DETAIL_REQUIRED', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (55, 636, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (55, 637, 'SB_VENDOR_DETAIL_VALUES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (55, 638, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (55, 639, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (55, 640, 'SB_VENDOR_DETAIL_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (55, 641, 'DETAIL_VALUES', 'V', 80, NULL, 'Y', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^








/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.2', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
	