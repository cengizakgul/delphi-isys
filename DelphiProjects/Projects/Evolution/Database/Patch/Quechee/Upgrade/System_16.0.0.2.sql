/* Check current version */

SET TERM ^;

EXECUTE block
AS
DECLARE VARIABLE req_ver VARCHAR(11);
DECLARE VARIABLE ver VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    EXECUTE STATEMENT '"This script must be executed by SYSDBA"';

  req_ver = '16.0.0.1';
  ver = 'UNKNOWN';
  SELECT VERSION
  FROM ev_database
  INTO :ver;

  IF (strcopy(ver, 0, strlen(req_ver)) <> req_ver) THEN
    EXECUTE STATEMENT '"Database version mismatch! Expected ' || req_ver || ' but found ' || ver || '"';
END^








DROP TRIGGER T_AD_SY_DASHBOARDS_9
^


DROP TRIGGER T_AU_SY_DASHBOARDS_9
^


DROP TRIGGER T_BIU_SY_DASHBOARDS_1
^

ALTER TABLE SY_DASHBOARDS
ADD RELEASED EV_CHAR1 
^

ALTER TABLE SY_DASHBOARDS
ALTER COLUMN RELEASED POSITION 10
^


update SY_DASHBOARDS set RELEASED='Y';
^




DROP TRIGGER T_AD_SY_FED_TAX_TABLE_9
^


DROP TRIGGER T_AU_SY_FED_TAX_TABLE_9
^

ALTER TABLE SY_FED_TAX_TABLE
ADD FED_POVERTY_LEVEL EV_AMOUNT 
^

ALTER TABLE SY_FED_TAX_TABLE
ALTER COLUMN FED_POVERTY_LEVEL POSITION 46
^

CREATE OR ALTER PROCEDURE pack_sy_fed_tax_table(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_fed_tax_table_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE exemption_amount NUMERIC(18,6);
DECLARE VARIABLE p_exemption_amount NUMERIC(18,6);
DECLARE VARIABLE federal_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE p_federal_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE federal_tip_credit NUMERIC(18,6);
DECLARE VARIABLE p_federal_tip_credit NUMERIC(18,6);
DECLARE VARIABLE supplemental_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE p_supplemental_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_er_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_ee_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE oasdi_wage_limit NUMERIC(18,6);
DECLARE VARIABLE p_oasdi_wage_limit NUMERIC(18,6);
DECLARE VARIABLE medicare_rate NUMERIC(18,6);
DECLARE VARIABLE p_medicare_rate NUMERIC(18,6);
DECLARE VARIABLE medicare_wage_limit NUMERIC(18,6);
DECLARE VARIABLE p_medicare_wage_limit NUMERIC(18,6);
DECLARE VARIABLE fui_rate_real NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_real NUMERIC(18,6);
DECLARE VARIABLE fui_rate_credit NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_credit NUMERIC(18,6);
DECLARE VARIABLE fui_wage_limit NUMERIC(18,6);
DECLARE VARIABLE p_fui_wage_limit NUMERIC(18,6);
DECLARE VARIABLE sy_401k_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_401k_limit NUMERIC(18,6);
DECLARE VARIABLE sy_403b_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_403b_limit NUMERIC(18,6);
DECLARE VARIABLE sy_457_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_457_limit NUMERIC(18,6);
DECLARE VARIABLE sy_501c_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_501c_limit NUMERIC(18,6);
DECLARE VARIABLE simple_limit NUMERIC(18,6);
DECLARE VARIABLE p_simple_limit NUMERIC(18,6);
DECLARE VARIABLE sep_limit NUMERIC(18,6);
DECLARE VARIABLE p_sep_limit NUMERIC(18,6);
DECLARE VARIABLE deferred_comp_limit NUMERIC(18,6);
DECLARE VARIABLE p_deferred_comp_limit NUMERIC(18,6);
DECLARE VARIABLE fed_deposit_freq_threshold NUMERIC(18,6);
DECLARE VARIABLE p_fed_deposit_freq_threshold NUMERIC(18,6);
DECLARE VARIABLE first_eic_limit NUMERIC(18,6);
DECLARE VARIABLE p_first_eic_limit NUMERIC(18,6);
DECLARE VARIABLE first_eic_percentage NUMERIC(18,6);
DECLARE VARIABLE p_first_eic_percentage NUMERIC(18,6);
DECLARE VARIABLE second_eic_limit NUMERIC(18,6);
DECLARE VARIABLE p_second_eic_limit NUMERIC(18,6);
DECLARE VARIABLE second_eic_amount NUMERIC(18,6);
DECLARE VARIABLE p_second_eic_amount NUMERIC(18,6);
DECLARE VARIABLE third_eic_additional_percent NUMERIC(18,6);
DECLARE VARIABLE p_third_eic_additional_percent NUMERIC(18,6);
DECLARE VARIABLE sy_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE sy_dependent_care_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_dependent_care_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_single_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_hsa_single_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_family_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_hsa_family_limit NUMERIC(18,6);
DECLARE VARIABLE sy_pension_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_pension_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE sy_simple_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_simple_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE compensation_limit NUMERIC(18,6);
DECLARE VARIABLE p_compensation_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_hsa_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE s132_parking_limit NUMERIC(18,6);
DECLARE VARIABLE p_s132_parking_limit NUMERIC(18,6);
DECLARE VARIABLE roth_ira_limit NUMERIC(18,6);
DECLARE VARIABLE p_roth_ira_limit NUMERIC(18,6);
DECLARE VARIABLE sy_ira_catchup_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_ira_catchup_limit NUMERIC(18,6);
DECLARE VARIABLE ee_med_th_limit NUMERIC(18,6);
DECLARE VARIABLE p_ee_med_th_limit NUMERIC(18,6);
DECLARE VARIABLE ee_med_th_rate NUMERIC(18,6);
DECLARE VARIABLE p_ee_med_th_rate NUMERIC(18,6);
DECLARE VARIABLE fsa_limit NUMERIC(18,6);
DECLARE VARIABLE p_fsa_limit NUMERIC(18,6);
DECLARE VARIABLE fed_poverty_level NUMERIC(18,6);
DECLARE VARIABLE p_fed_poverty_level NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_fed_tax_table_nbr , filler, exemption_amount, federal_minimum_wage, federal_tip_credit, supplemental_tax_percentage, oasdi_rate, er_oasdi_rate, ee_oasdi_rate, oasdi_wage_limit, medicare_rate, medicare_wage_limit, fui_rate_real, fui_rate_credit, fui_wage_limit, sy_401k_limit, sy_403b_limit, sy_457_limit, sy_501c_limit, simple_limit, sep_limit, deferred_comp_limit, fed_deposit_freq_threshold, first_eic_limit, first_eic_percentage, second_eic_limit, second_eic_amount, third_eic_additional_percent, sy_catch_up_limit, sy_dependent_care_limit, sy_hsa_single_limit, sy_hsa_family_limit, sy_pension_catch_up_limit, sy_simple_catch_up_limit, compensation_limit, sy_hsa_catch_up_limit, s132_parking_limit, roth_ira_limit, sy_ira_catchup_limit, ee_med_th_limit, ee_med_th_rate, fsa_limit, fed_poverty_level
        FROM sy_fed_tax_table
        ORDER BY sy_fed_tax_table_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_fed_tax_table_nbr, :filler, :exemption_amount, :federal_minimum_wage, :federal_tip_credit, :supplemental_tax_percentage, :oasdi_rate, :er_oasdi_rate, :ee_oasdi_rate, :oasdi_wage_limit, :medicare_rate, :medicare_wage_limit, :fui_rate_real, :fui_rate_credit, :fui_wage_limit, :sy_401k_limit, :sy_403b_limit, :sy_457_limit, :sy_501c_limit, :simple_limit, :sep_limit, :deferred_comp_limit, :fed_deposit_freq_threshold, :first_eic_limit, :first_eic_percentage, :second_eic_limit, :second_eic_amount, :third_eic_additional_percent, :sy_catch_up_limit, :sy_dependent_care_limit, :sy_hsa_single_limit, :sy_hsa_family_limit, :sy_pension_catch_up_limit, :sy_simple_catch_up_limit, :compensation_limit, :sy_hsa_catch_up_limit, :s132_parking_limit, :roth_ira_limit, :sy_ira_catchup_limit, :ee_med_th_limit, :ee_med_th_rate, :fsa_limit, :fed_poverty_level
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_fed_tax_table_nbr) OR (effective_date = '1/1/1900') OR (p_filler IS DISTINCT FROM filler) OR (p_exemption_amount IS DISTINCT FROM exemption_amount) OR (p_federal_minimum_wage IS DISTINCT FROM federal_minimum_wage) OR (p_federal_tip_credit IS DISTINCT FROM federal_tip_credit) OR (p_supplemental_tax_percentage IS DISTINCT FROM supplemental_tax_percentage) OR (p_oasdi_rate IS DISTINCT FROM oasdi_rate) OR (p_er_oasdi_rate IS DISTINCT FROM er_oasdi_rate) OR (p_ee_oasdi_rate IS DISTINCT FROM ee_oasdi_rate) OR (p_oasdi_wage_limit IS DISTINCT FROM oasdi_wage_limit) OR (p_medicare_rate IS DISTINCT FROM medicare_rate) OR (p_medicare_wage_limit IS DISTINCT FROM medicare_wage_limit) OR (p_fui_rate_real IS DISTINCT FROM fui_rate_real) OR (p_fui_rate_credit IS DISTINCT FROM fui_rate_credit) OR (p_fui_wage_limit IS DISTINCT FROM fui_wage_limit) OR (p_sy_401k_limit IS DISTINCT FROM sy_401k_limit) OR (p_sy_403b_limit IS DISTINCT FROM sy_403b_limit) OR (p_sy_457_limit IS DISTINCT FROM sy_457_limit) OR (p_sy_501c_limit IS DISTINCT FROM sy_501c_limit) OR (p_simple_limit IS DISTINCT FROM simple_limit) OR (p_sep_limit IS DISTINCT FROM sep_limit) OR (p_deferred_comp_limit IS DISTINCT FROM deferred_comp_limit) OR (p_fed_deposit_freq_threshold IS DISTINCT FROM fed_deposit_freq_threshold) OR (p_first_eic_limit IS DISTINCT FROM first_eic_limit) OR (p_first_eic_percentage IS DISTINCT FROM first_eic_percentage) OR (p_second_eic_limit IS DISTINCT FROM second_eic_limit) OR (p_second_eic_amount IS DISTINCT FROM second_eic_amount) OR (p_third_eic_additional_percent IS DISTINCT FROM third_eic_additional_percent) OR (p_sy_catch_up_limit IS DISTINCT FROM sy_catch_up_limit) OR (p_sy_dependent_care_limit IS DISTINCT FROM sy_dependent_care_limit) OR (p_sy_hsa_single_limit IS DISTINCT FROM sy_hsa_single_limit) OR (p_sy_hsa_family_limit IS DISTINCT FROM sy_hsa_family_limit) OR (p_sy_pension_catch_up_limit IS DISTINCT FROM sy_pension_catch_up_limit) OR (p_sy_simple_catch_up_limit IS DISTINCT FROM sy_simple_catch_up_limit) OR (p_compensation_limit IS DISTINCT FROM compensation_limit) OR (p_sy_hsa_catch_up_limit IS DISTINCT FROM sy_hsa_catch_up_limit) OR (p_s132_parking_limit IS DISTINCT FROM s132_parking_limit) OR (p_roth_ira_limit IS DISTINCT FROM roth_ira_limit) OR (p_sy_ira_catchup_limit IS DISTINCT FROM sy_ira_catchup_limit) OR (p_ee_med_th_limit IS DISTINCT FROM ee_med_th_limit) OR (p_ee_med_th_rate IS DISTINCT FROM ee_med_th_rate) OR (p_fsa_limit IS DISTINCT FROM fsa_limit) OR (p_fed_poverty_level IS DISTINCT FROM fed_poverty_level)) THEN
      BEGIN
        curr_nbr = sy_fed_tax_table_nbr;
        p_filler = filler;
        p_exemption_amount = exemption_amount;
        p_federal_minimum_wage = federal_minimum_wage;
        p_federal_tip_credit = federal_tip_credit;
        p_supplemental_tax_percentage = supplemental_tax_percentage;
        p_oasdi_rate = oasdi_rate;
        p_er_oasdi_rate = er_oasdi_rate;
        p_ee_oasdi_rate = ee_oasdi_rate;
        p_oasdi_wage_limit = oasdi_wage_limit;
        p_medicare_rate = medicare_rate;
        p_medicare_wage_limit = medicare_wage_limit;
        p_fui_rate_real = fui_rate_real;
        p_fui_rate_credit = fui_rate_credit;
        p_fui_wage_limit = fui_wage_limit;
        p_sy_401k_limit = sy_401k_limit;
        p_sy_403b_limit = sy_403b_limit;
        p_sy_457_limit = sy_457_limit;
        p_sy_501c_limit = sy_501c_limit;
        p_simple_limit = simple_limit;
        p_sep_limit = sep_limit;
        p_deferred_comp_limit = deferred_comp_limit;
        p_fed_deposit_freq_threshold = fed_deposit_freq_threshold;
        p_first_eic_limit = first_eic_limit;
        p_first_eic_percentage = first_eic_percentage;
        p_second_eic_limit = second_eic_limit;
        p_second_eic_amount = second_eic_amount;
        p_third_eic_additional_percent = third_eic_additional_percent;
        p_sy_catch_up_limit = sy_catch_up_limit;
        p_sy_dependent_care_limit = sy_dependent_care_limit;
        p_sy_hsa_single_limit = sy_hsa_single_limit;
        p_sy_hsa_family_limit = sy_hsa_family_limit;
        p_sy_pension_catch_up_limit = sy_pension_catch_up_limit;
        p_sy_simple_catch_up_limit = sy_simple_catch_up_limit;
        p_compensation_limit = compensation_limit;
        p_sy_hsa_catch_up_limit = sy_hsa_catch_up_limit;
        p_s132_parking_limit = s132_parking_limit;
        p_roth_ira_limit = roth_ira_limit;
        p_sy_ira_catchup_limit = sy_ira_catchup_limit;
        p_ee_med_th_limit = ee_med_th_limit;
        p_ee_med_th_rate = ee_med_th_rate;
        p_fsa_limit = fsa_limit;
        p_fed_poverty_level = fed_poverty_level;
      END
      ELSE
        DELETE FROM sy_fed_tax_table WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, filler, exemption_amount, federal_minimum_wage, federal_tip_credit, supplemental_tax_percentage, oasdi_rate, er_oasdi_rate, ee_oasdi_rate, oasdi_wage_limit, medicare_rate, medicare_wage_limit, fui_rate_real, fui_rate_credit, fui_wage_limit, sy_401k_limit, sy_403b_limit, sy_457_limit, sy_501c_limit, simple_limit, sep_limit, deferred_comp_limit, fed_deposit_freq_threshold, first_eic_limit, first_eic_percentage, second_eic_limit, second_eic_amount, third_eic_additional_percent, sy_catch_up_limit, sy_dependent_care_limit, sy_hsa_single_limit, sy_hsa_family_limit, sy_pension_catch_up_limit, sy_simple_catch_up_limit, compensation_limit, sy_hsa_catch_up_limit, s132_parking_limit, roth_ira_limit, sy_ira_catchup_limit, ee_med_th_limit, ee_med_th_rate, fsa_limit, fed_poverty_level
        FROM sy_fed_tax_table
        WHERE sy_fed_tax_table_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :filler, :exemption_amount, :federal_minimum_wage, :federal_tip_credit, :supplemental_tax_percentage, :oasdi_rate, :er_oasdi_rate, :ee_oasdi_rate, :oasdi_wage_limit, :medicare_rate, :medicare_wage_limit, :fui_rate_real, :fui_rate_credit, :fui_wage_limit, :sy_401k_limit, :sy_403b_limit, :sy_457_limit, :sy_501c_limit, :simple_limit, :sep_limit, :deferred_comp_limit, :fed_deposit_freq_threshold, :first_eic_limit, :first_eic_percentage, :second_eic_limit, :second_eic_amount, :third_eic_additional_percent, :sy_catch_up_limit, :sy_dependent_care_limit, :sy_hsa_single_limit, :sy_hsa_family_limit, :sy_pension_catch_up_limit, :sy_simple_catch_up_limit, :compensation_limit, :sy_hsa_catch_up_limit, :s132_parking_limit, :roth_ira_limit, :sy_ira_catchup_limit, :ee_med_th_limit, :ee_med_th_rate, :fsa_limit, :fed_poverty_level
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_filler IS DISTINCT FROM filler) OR (p_exemption_amount IS DISTINCT FROM exemption_amount) OR (p_federal_minimum_wage IS DISTINCT FROM federal_minimum_wage) OR (p_federal_tip_credit IS DISTINCT FROM federal_tip_credit) OR (p_supplemental_tax_percentage IS DISTINCT FROM supplemental_tax_percentage) OR (p_oasdi_rate IS DISTINCT FROM oasdi_rate) OR (p_er_oasdi_rate IS DISTINCT FROM er_oasdi_rate) OR (p_ee_oasdi_rate IS DISTINCT FROM ee_oasdi_rate) OR (p_oasdi_wage_limit IS DISTINCT FROM oasdi_wage_limit) OR (p_medicare_rate IS DISTINCT FROM medicare_rate) OR (p_medicare_wage_limit IS DISTINCT FROM medicare_wage_limit) OR (p_fui_rate_real IS DISTINCT FROM fui_rate_real) OR (p_fui_rate_credit IS DISTINCT FROM fui_rate_credit) OR (p_fui_wage_limit IS DISTINCT FROM fui_wage_limit) OR (p_sy_401k_limit IS DISTINCT FROM sy_401k_limit) OR (p_sy_403b_limit IS DISTINCT FROM sy_403b_limit) OR (p_sy_457_limit IS DISTINCT FROM sy_457_limit) OR (p_sy_501c_limit IS DISTINCT FROM sy_501c_limit) OR (p_simple_limit IS DISTINCT FROM simple_limit) OR (p_sep_limit IS DISTINCT FROM sep_limit) OR (p_deferred_comp_limit IS DISTINCT FROM deferred_comp_limit) OR (p_fed_deposit_freq_threshold IS DISTINCT FROM fed_deposit_freq_threshold) OR (p_first_eic_limit IS DISTINCT FROM first_eic_limit) OR (p_first_eic_percentage IS DISTINCT FROM first_eic_percentage) OR (p_second_eic_limit IS DISTINCT FROM second_eic_limit) OR (p_second_eic_amount IS DISTINCT FROM second_eic_amount) OR (p_third_eic_additional_percent IS DISTINCT FROM third_eic_additional_percent) OR (p_sy_catch_up_limit IS DISTINCT FROM sy_catch_up_limit) OR (p_sy_dependent_care_limit IS DISTINCT FROM sy_dependent_care_limit) OR (p_sy_hsa_single_limit IS DISTINCT FROM sy_hsa_single_limit) OR (p_sy_hsa_family_limit IS DISTINCT FROM sy_hsa_family_limit) OR (p_sy_pension_catch_up_limit IS DISTINCT FROM sy_pension_catch_up_limit) OR (p_sy_simple_catch_up_limit IS DISTINCT FROM sy_simple_catch_up_limit) OR (p_compensation_limit IS DISTINCT FROM compensation_limit) OR (p_sy_hsa_catch_up_limit IS DISTINCT FROM sy_hsa_catch_up_limit) OR (p_s132_parking_limit IS DISTINCT FROM s132_parking_limit) OR (p_roth_ira_limit IS DISTINCT FROM roth_ira_limit) OR (p_sy_ira_catchup_limit IS DISTINCT FROM sy_ira_catchup_limit) OR (p_ee_med_th_limit IS DISTINCT FROM ee_med_th_limit) OR (p_ee_med_th_rate IS DISTINCT FROM ee_med_th_rate) OR (p_fsa_limit IS DISTINCT FROM fsa_limit) OR (p_fed_poverty_level IS DISTINCT FROM fed_poverty_level)) THEN
      BEGIN
        p_filler = filler;
        p_exemption_amount = exemption_amount;
        p_federal_minimum_wage = federal_minimum_wage;
        p_federal_tip_credit = federal_tip_credit;
        p_supplemental_tax_percentage = supplemental_tax_percentage;
        p_oasdi_rate = oasdi_rate;
        p_er_oasdi_rate = er_oasdi_rate;
        p_ee_oasdi_rate = ee_oasdi_rate;
        p_oasdi_wage_limit = oasdi_wage_limit;
        p_medicare_rate = medicare_rate;
        p_medicare_wage_limit = medicare_wage_limit;
        p_fui_rate_real = fui_rate_real;
        p_fui_rate_credit = fui_rate_credit;
        p_fui_wage_limit = fui_wage_limit;
        p_sy_401k_limit = sy_401k_limit;
        p_sy_403b_limit = sy_403b_limit;
        p_sy_457_limit = sy_457_limit;
        p_sy_501c_limit = sy_501c_limit;
        p_simple_limit = simple_limit;
        p_sep_limit = sep_limit;
        p_deferred_comp_limit = deferred_comp_limit;
        p_fed_deposit_freq_threshold = fed_deposit_freq_threshold;
        p_first_eic_limit = first_eic_limit;
        p_first_eic_percentage = first_eic_percentage;
        p_second_eic_limit = second_eic_limit;
        p_second_eic_amount = second_eic_amount;
        p_third_eic_additional_percent = third_eic_additional_percent;
        p_sy_catch_up_limit = sy_catch_up_limit;
        p_sy_dependent_care_limit = sy_dependent_care_limit;
        p_sy_hsa_single_limit = sy_hsa_single_limit;
        p_sy_hsa_family_limit = sy_hsa_family_limit;
        p_sy_pension_catch_up_limit = sy_pension_catch_up_limit;
        p_sy_simple_catch_up_limit = sy_simple_catch_up_limit;
        p_compensation_limit = compensation_limit;
        p_sy_hsa_catch_up_limit = sy_hsa_catch_up_limit;
        p_s132_parking_limit = s132_parking_limit;
        p_roth_ira_limit = roth_ira_limit;
        p_sy_ira_catchup_limit = sy_ira_catchup_limit;
        p_ee_med_th_limit = ee_med_th_limit;
        p_ee_med_th_rate = ee_med_th_rate;
        p_fsa_limit = fsa_limit;
        p_fed_poverty_level = fed_poverty_level;
      END
      ELSE
        DELETE FROM sy_fed_tax_table WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_FED_TAX_TABLE TO EUSER
^

CREATE TRIGGER T_AD_SY_FED_TAX_TABLE_9 FOR SY_FED_TAX_TABLE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE exemption_amount NUMERIC(18,6);
DECLARE VARIABLE federal_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE federal_tip_credit NUMERIC(18,6);
DECLARE VARIABLE supplemental_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE oasdi_wage_limit NUMERIC(18,6);
DECLARE VARIABLE medicare_rate NUMERIC(18,6);
DECLARE VARIABLE medicare_wage_limit NUMERIC(18,6);
DECLARE VARIABLE fui_rate_real NUMERIC(18,6);
DECLARE VARIABLE fui_rate_credit NUMERIC(18,6);
DECLARE VARIABLE fui_wage_limit NUMERIC(18,6);
DECLARE VARIABLE sy_401k_limit NUMERIC(18,6);
DECLARE VARIABLE sy_403b_limit NUMERIC(18,6);
DECLARE VARIABLE sy_457_limit NUMERIC(18,6);
DECLARE VARIABLE sy_501c_limit NUMERIC(18,6);
DECLARE VARIABLE simple_limit NUMERIC(18,6);
DECLARE VARIABLE sep_limit NUMERIC(18,6);
DECLARE VARIABLE deferred_comp_limit NUMERIC(18,6);
DECLARE VARIABLE fed_deposit_freq_threshold NUMERIC(18,6);
DECLARE VARIABLE first_eic_limit NUMERIC(18,6);
DECLARE VARIABLE first_eic_percentage NUMERIC(18,6);
DECLARE VARIABLE second_eic_limit NUMERIC(18,6);
DECLARE VARIABLE second_eic_amount NUMERIC(18,6);
DECLARE VARIABLE third_eic_additional_percent NUMERIC(18,6);
DECLARE VARIABLE sy_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE sy_dependent_care_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_single_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_family_limit NUMERIC(18,6);
DECLARE VARIABLE sy_pension_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE sy_simple_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE compensation_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE s132_parking_limit NUMERIC(18,6);
DECLARE VARIABLE roth_ira_limit NUMERIC(18,6);
DECLARE VARIABLE sy_ira_catchup_limit NUMERIC(18,6);
DECLARE VARIABLE ee_med_th_limit NUMERIC(18,6);
DECLARE VARIABLE ee_med_th_rate NUMERIC(18,6);
DECLARE VARIABLE fsa_limit NUMERIC(18,6);
DECLARE VARIABLE fed_poverty_level NUMERIC(18,6);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(7, OLD.rec_version, OLD.sy_fed_tax_table_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', filler, exemption_amount, federal_minimum_wage, federal_tip_credit, supplemental_tax_percentage, oasdi_rate, er_oasdi_rate, ee_oasdi_rate, oasdi_wage_limit, medicare_rate, medicare_wage_limit, fui_rate_real, fui_rate_credit, fui_wage_limit, sy_401k_limit, sy_403b_limit, sy_457_limit, sy_501c_limit, simple_limit, sep_limit, deferred_comp_limit, fed_deposit_freq_threshold, first_eic_limit, first_eic_percentage, second_eic_limit, second_eic_amount, third_eic_additional_percent, sy_catch_up_limit, sy_dependent_care_limit, sy_hsa_single_limit, sy_hsa_family_limit, sy_pension_catch_up_limit, sy_simple_catch_up_limit, compensation_limit, sy_hsa_catch_up_limit, s132_parking_limit, roth_ira_limit, sy_ira_catchup_limit, ee_med_th_limit, ee_med_th_rate, fsa_limit, fed_poverty_level  FROM sy_fed_tax_table WHERE sy_fed_tax_table_nbr = OLD.sy_fed_tax_table_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :filler, :exemption_amount, :federal_minimum_wage, :federal_tip_credit, :supplemental_tax_percentage, :oasdi_rate, :er_oasdi_rate, :ee_oasdi_rate, :oasdi_wage_limit, :medicare_rate, :medicare_wage_limit, :fui_rate_real, :fui_rate_credit, :fui_wage_limit, :sy_401k_limit, :sy_403b_limit, :sy_457_limit, :sy_501c_limit, :simple_limit, :sep_limit, :deferred_comp_limit, :fed_deposit_freq_threshold, :first_eic_limit, :first_eic_percentage, :second_eic_limit, :second_eic_amount, :third_eic_additional_percent, :sy_catch_up_limit, :sy_dependent_care_limit, :sy_hsa_single_limit, :sy_hsa_family_limit, :sy_pension_catch_up_limit, :sy_simple_catch_up_limit, :compensation_limit, :sy_hsa_catch_up_limit, :s132_parking_limit, :roth_ira_limit, :sy_ira_catchup_limit, :ee_med_th_limit, :ee_med_th_rate, :fsa_limit, :fed_poverty_level;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 82, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 83, OLD.effective_until);

  /* FILLER */
  IF ((last_record = 'Y' AND OLD.filler IS NOT NULL) OR (last_record = 'N' AND filler IS DISTINCT FROM OLD.filler)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 81, OLD.filler);

  /* EXEMPTION_AMOUNT */
  IF ((last_record = 'Y' AND OLD.exemption_amount IS NOT NULL) OR (last_record = 'N' AND exemption_amount IS DISTINCT FROM OLD.exemption_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 78, OLD.exemption_amount);

  /* FEDERAL_MINIMUM_WAGE */
  IF ((last_record = 'Y' AND OLD.federal_minimum_wage IS NOT NULL) OR (last_record = 'N' AND federal_minimum_wage IS DISTINCT FROM OLD.federal_minimum_wage)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 77, OLD.federal_minimum_wage);

  /* FEDERAL_TIP_CREDIT */
  IF ((last_record = 'Y' AND OLD.federal_tip_credit IS NOT NULL) OR (last_record = 'N' AND federal_tip_credit IS DISTINCT FROM OLD.federal_tip_credit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 76, OLD.federal_tip_credit);

  /* SUPPLEMENTAL_TAX_PERCENTAGE */
  IF ((last_record = 'Y' AND OLD.supplemental_tax_percentage IS NOT NULL) OR (last_record = 'N' AND supplemental_tax_percentage IS DISTINCT FROM OLD.supplemental_tax_percentage)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 75, OLD.supplemental_tax_percentage);

  /* OASDI_RATE */
  IF ((last_record = 'Y' AND OLD.oasdi_rate IS NOT NULL) OR (last_record = 'N' AND oasdi_rate IS DISTINCT FROM OLD.oasdi_rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 74, OLD.oasdi_rate);

  /* ER_OASDI_RATE */
  IF ((last_record = 'Y' AND OLD.er_oasdi_rate IS NOT NULL) OR (last_record = 'N' AND er_oasdi_rate IS DISTINCT FROM OLD.er_oasdi_rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 532, OLD.er_oasdi_rate);

  /* EE_OASDI_RATE */
  IF ((last_record = 'Y' AND OLD.ee_oasdi_rate IS NOT NULL) OR (last_record = 'N' AND ee_oasdi_rate IS DISTINCT FROM OLD.ee_oasdi_rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 531, OLD.ee_oasdi_rate);

  /* OASDI_WAGE_LIMIT */
  IF ((last_record = 'Y' AND OLD.oasdi_wage_limit IS NOT NULL) OR (last_record = 'N' AND oasdi_wage_limit IS DISTINCT FROM OLD.oasdi_wage_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 73, OLD.oasdi_wage_limit);

  /* MEDICARE_RATE */
  IF ((last_record = 'Y' AND OLD.medicare_rate IS NOT NULL) OR (last_record = 'N' AND medicare_rate IS DISTINCT FROM OLD.medicare_rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 72, OLD.medicare_rate);

  /* MEDICARE_WAGE_LIMIT */
  IF ((last_record = 'Y' AND OLD.medicare_wage_limit IS NOT NULL) OR (last_record = 'N' AND medicare_wage_limit IS DISTINCT FROM OLD.medicare_wage_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 71, OLD.medicare_wage_limit);

  /* FUI_RATE_REAL */
  IF ((last_record = 'Y' AND OLD.fui_rate_real IS NOT NULL) OR (last_record = 'N' AND fui_rate_real IS DISTINCT FROM OLD.fui_rate_real)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 70, OLD.fui_rate_real);

  /* FUI_RATE_CREDIT */
  IF ((last_record = 'Y' AND OLD.fui_rate_credit IS NOT NULL) OR (last_record = 'N' AND fui_rate_credit IS DISTINCT FROM OLD.fui_rate_credit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 69, OLD.fui_rate_credit);

  /* FUI_WAGE_LIMIT */
  IF ((last_record = 'Y' AND OLD.fui_wage_limit IS NOT NULL) OR (last_record = 'N' AND fui_wage_limit IS DISTINCT FROM OLD.fui_wage_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 68, OLD.fui_wage_limit);

  /* SY_401K_LIMIT */
  IF ((last_record = 'Y' AND OLD.sy_401k_limit IS NOT NULL) OR (last_record = 'N' AND sy_401k_limit IS DISTINCT FROM OLD.sy_401k_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 67, OLD.sy_401k_limit);

  /* SY_403B_LIMIT */
  IF ((last_record = 'Y' AND OLD.sy_403b_limit IS NOT NULL) OR (last_record = 'N' AND sy_403b_limit IS DISTINCT FROM OLD.sy_403b_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 66, OLD.sy_403b_limit);

  /* SY_457_LIMIT */
  IF ((last_record = 'Y' AND OLD.sy_457_limit IS NOT NULL) OR (last_record = 'N' AND sy_457_limit IS DISTINCT FROM OLD.sy_457_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 65, OLD.sy_457_limit);

  /* SY_501C_LIMIT */
  IF ((last_record = 'Y' AND OLD.sy_501c_limit IS NOT NULL) OR (last_record = 'N' AND sy_501c_limit IS DISTINCT FROM OLD.sy_501c_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 64, OLD.sy_501c_limit);

  /* SIMPLE_LIMIT */
  IF ((last_record = 'Y' AND OLD.simple_limit IS NOT NULL) OR (last_record = 'N' AND simple_limit IS DISTINCT FROM OLD.simple_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 63, OLD.simple_limit);

  /* SEP_LIMIT */
  IF ((last_record = 'Y' AND OLD.sep_limit IS NOT NULL) OR (last_record = 'N' AND sep_limit IS DISTINCT FROM OLD.sep_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 62, OLD.sep_limit);

  /* DEFERRED_COMP_LIMIT */
  IF ((last_record = 'Y' AND OLD.deferred_comp_limit IS NOT NULL) OR (last_record = 'N' AND deferred_comp_limit IS DISTINCT FROM OLD.deferred_comp_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 61, OLD.deferred_comp_limit);

  /* FED_DEPOSIT_FREQ_THRESHOLD */
  IF ((last_record = 'Y' AND OLD.fed_deposit_freq_threshold IS NOT NULL) OR (last_record = 'N' AND fed_deposit_freq_threshold IS DISTINCT FROM OLD.fed_deposit_freq_threshold)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 60, OLD.fed_deposit_freq_threshold);

  /* FIRST_EIC_LIMIT */
  IF ((last_record = 'Y' AND OLD.first_eic_limit IS NOT NULL) OR (last_record = 'N' AND first_eic_limit IS DISTINCT FROM OLD.first_eic_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 59, OLD.first_eic_limit);

  /* FIRST_EIC_PERCENTAGE */
  IF ((last_record = 'Y' AND OLD.first_eic_percentage IS NOT NULL) OR (last_record = 'N' AND first_eic_percentage IS DISTINCT FROM OLD.first_eic_percentage)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 58, OLD.first_eic_percentage);

  /* SECOND_EIC_LIMIT */
  IF ((last_record = 'Y' AND OLD.second_eic_limit IS NOT NULL) OR (last_record = 'N' AND second_eic_limit IS DISTINCT FROM OLD.second_eic_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 57, OLD.second_eic_limit);

  /* SECOND_EIC_AMOUNT */
  IF ((last_record = 'Y' AND OLD.second_eic_amount IS NOT NULL) OR (last_record = 'N' AND second_eic_amount IS DISTINCT FROM OLD.second_eic_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 56, OLD.second_eic_amount);

  /* THIRD_EIC_ADDITIONAL_PERCENT */
  IF ((last_record = 'Y' AND OLD.third_eic_additional_percent IS NOT NULL) OR (last_record = 'N' AND third_eic_additional_percent IS DISTINCT FROM OLD.third_eic_additional_percent)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 55, OLD.third_eic_additional_percent);

  /* SY_CATCH_UP_LIMIT */
  IF (last_record = 'Y' OR sy_catch_up_limit IS DISTINCT FROM OLD.sy_catch_up_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 54, OLD.sy_catch_up_limit);

  /* SY_DEPENDENT_CARE_LIMIT */
  IF (last_record = 'Y' OR sy_dependent_care_limit IS DISTINCT FROM OLD.sy_dependent_care_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 53, OLD.sy_dependent_care_limit);

  /* SY_HSA_SINGLE_LIMIT */
  IF (last_record = 'Y' OR sy_hsa_single_limit IS DISTINCT FROM OLD.sy_hsa_single_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 52, OLD.sy_hsa_single_limit);

  /* SY_HSA_FAMILY_LIMIT */
  IF (last_record = 'Y' OR sy_hsa_family_limit IS DISTINCT FROM OLD.sy_hsa_family_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 51, OLD.sy_hsa_family_limit);

  /* SY_PENSION_CATCH_UP_LIMIT */
  IF (last_record = 'Y' OR sy_pension_catch_up_limit IS DISTINCT FROM OLD.sy_pension_catch_up_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 50, OLD.sy_pension_catch_up_limit);

  /* SY_SIMPLE_CATCH_UP_LIMIT */
  IF (last_record = 'Y' OR sy_simple_catch_up_limit IS DISTINCT FROM OLD.sy_simple_catch_up_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 49, OLD.sy_simple_catch_up_limit);

  /* COMPENSATION_LIMIT */
  IF ((last_record = 'Y' AND OLD.compensation_limit IS NOT NULL) OR (last_record = 'N' AND compensation_limit IS DISTINCT FROM OLD.compensation_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 48, OLD.compensation_limit);

  /* SY_HSA_CATCH_UP_LIMIT */
  IF (last_record = 'Y' OR sy_hsa_catch_up_limit IS DISTINCT FROM OLD.sy_hsa_catch_up_limit) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 47, OLD.sy_hsa_catch_up_limit);

  /* S132_PARKING_LIMIT */
  IF ((last_record = 'Y' AND OLD.s132_parking_limit IS NOT NULL) OR (last_record = 'N' AND s132_parking_limit IS DISTINCT FROM OLD.s132_parking_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 46, OLD.s132_parking_limit);

  /* ROTH_IRA_LIMIT */
  IF ((last_record = 'Y' AND OLD.roth_ira_limit IS NOT NULL) OR (last_record = 'N' AND roth_ira_limit IS DISTINCT FROM OLD.roth_ira_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 45, OLD.roth_ira_limit);

  /* SY_IRA_CATCHUP_LIMIT */
  IF ((last_record = 'Y' AND OLD.sy_ira_catchup_limit IS NOT NULL) OR (last_record = 'N' AND sy_ira_catchup_limit IS DISTINCT FROM OLD.sy_ira_catchup_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 44, OLD.sy_ira_catchup_limit);

  /* EE_MED_TH_LIMIT */
  IF ((last_record = 'Y' AND OLD.ee_med_th_limit IS NOT NULL) OR (last_record = 'N' AND ee_med_th_limit IS DISTINCT FROM OLD.ee_med_th_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 561, OLD.ee_med_th_limit);

  /* EE_MED_TH_RATE */
  IF ((last_record = 'Y' AND OLD.ee_med_th_rate IS NOT NULL) OR (last_record = 'N' AND ee_med_th_rate IS DISTINCT FROM OLD.ee_med_th_rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 562, OLD.ee_med_th_rate);

  /* FSA_LIMIT */
  IF ((last_record = 'Y' AND OLD.fsa_limit IS NOT NULL) OR (last_record = 'N' AND fsa_limit IS DISTINCT FROM OLD.fsa_limit)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 583, OLD.fsa_limit);

  /* FED_POVERTY_LEVEL */
  IF ((last_record = 'Y' AND OLD.fed_poverty_level IS NOT NULL) OR (last_record = 'N' AND fed_poverty_level IS DISTINCT FROM OLD.fed_poverty_level)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 618, OLD.fed_poverty_level);

END

^

CREATE TRIGGER T_AU_SY_FED_TAX_TABLE_9 FOR SY_FED_TAX_TABLE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(7, NEW.rec_version, NEW.sy_fed_tax_table_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 82, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 83, OLD.effective_until);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 81, OLD.filler);
    changes = changes + 1;
  END

  /* EXEMPTION_AMOUNT */
  IF (OLD.exemption_amount IS DISTINCT FROM NEW.exemption_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 78, OLD.exemption_amount);
    changes = changes + 1;
  END

  /* FEDERAL_MINIMUM_WAGE */
  IF (OLD.federal_minimum_wage IS DISTINCT FROM NEW.federal_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 77, OLD.federal_minimum_wage);
    changes = changes + 1;
  END

  /* FEDERAL_TIP_CREDIT */
  IF (OLD.federal_tip_credit IS DISTINCT FROM NEW.federal_tip_credit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 76, OLD.federal_tip_credit);
    changes = changes + 1;
  END

  /* SUPPLEMENTAL_TAX_PERCENTAGE */
  IF (OLD.supplemental_tax_percentage IS DISTINCT FROM NEW.supplemental_tax_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 75, OLD.supplemental_tax_percentage);
    changes = changes + 1;
  END

  /* OASDI_RATE */
  IF (OLD.oasdi_rate IS DISTINCT FROM NEW.oasdi_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 74, OLD.oasdi_rate);
    changes = changes + 1;
  END

  /* ER_OASDI_RATE */
  IF (OLD.er_oasdi_rate IS DISTINCT FROM NEW.er_oasdi_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 532, OLD.er_oasdi_rate);
    changes = changes + 1;
  END

  /* EE_OASDI_RATE */
  IF (OLD.ee_oasdi_rate IS DISTINCT FROM NEW.ee_oasdi_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 531, OLD.ee_oasdi_rate);
    changes = changes + 1;
  END

  /* OASDI_WAGE_LIMIT */
  IF (OLD.oasdi_wage_limit IS DISTINCT FROM NEW.oasdi_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 73, OLD.oasdi_wage_limit);
    changes = changes + 1;
  END

  /* MEDICARE_RATE */
  IF (OLD.medicare_rate IS DISTINCT FROM NEW.medicare_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 72, OLD.medicare_rate);
    changes = changes + 1;
  END

  /* MEDICARE_WAGE_LIMIT */
  IF (OLD.medicare_wage_limit IS DISTINCT FROM NEW.medicare_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 71, OLD.medicare_wage_limit);
    changes = changes + 1;
  END

  /* FUI_RATE_REAL */
  IF (OLD.fui_rate_real IS DISTINCT FROM NEW.fui_rate_real) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 70, OLD.fui_rate_real);
    changes = changes + 1;
  END

  /* FUI_RATE_CREDIT */
  IF (OLD.fui_rate_credit IS DISTINCT FROM NEW.fui_rate_credit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 69, OLD.fui_rate_credit);
    changes = changes + 1;
  END

  /* FUI_WAGE_LIMIT */
  IF (OLD.fui_wage_limit IS DISTINCT FROM NEW.fui_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 68, OLD.fui_wage_limit);
    changes = changes + 1;
  END

  /* SY_401K_LIMIT */
  IF (OLD.sy_401k_limit IS DISTINCT FROM NEW.sy_401k_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 67, OLD.sy_401k_limit);
    changes = changes + 1;
  END

  /* SY_403B_LIMIT */
  IF (OLD.sy_403b_limit IS DISTINCT FROM NEW.sy_403b_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 66, OLD.sy_403b_limit);
    changes = changes + 1;
  END

  /* SY_457_LIMIT */
  IF (OLD.sy_457_limit IS DISTINCT FROM NEW.sy_457_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 65, OLD.sy_457_limit);
    changes = changes + 1;
  END

  /* SY_501C_LIMIT */
  IF (OLD.sy_501c_limit IS DISTINCT FROM NEW.sy_501c_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 64, OLD.sy_501c_limit);
    changes = changes + 1;
  END

  /* SIMPLE_LIMIT */
  IF (OLD.simple_limit IS DISTINCT FROM NEW.simple_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 63, OLD.simple_limit);
    changes = changes + 1;
  END

  /* SEP_LIMIT */
  IF (OLD.sep_limit IS DISTINCT FROM NEW.sep_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 62, OLD.sep_limit);
    changes = changes + 1;
  END

  /* DEFERRED_COMP_LIMIT */
  IF (OLD.deferred_comp_limit IS DISTINCT FROM NEW.deferred_comp_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 61, OLD.deferred_comp_limit);
    changes = changes + 1;
  END

  /* FED_DEPOSIT_FREQ_THRESHOLD */
  IF (OLD.fed_deposit_freq_threshold IS DISTINCT FROM NEW.fed_deposit_freq_threshold) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 60, OLD.fed_deposit_freq_threshold);
    changes = changes + 1;
  END

  /* FIRST_EIC_LIMIT */
  IF (OLD.first_eic_limit IS DISTINCT FROM NEW.first_eic_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 59, OLD.first_eic_limit);
    changes = changes + 1;
  END

  /* FIRST_EIC_PERCENTAGE */
  IF (OLD.first_eic_percentage IS DISTINCT FROM NEW.first_eic_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 58, OLD.first_eic_percentage);
    changes = changes + 1;
  END

  /* SECOND_EIC_LIMIT */
  IF (OLD.second_eic_limit IS DISTINCT FROM NEW.second_eic_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 57, OLD.second_eic_limit);
    changes = changes + 1;
  END

  /* SECOND_EIC_AMOUNT */
  IF (OLD.second_eic_amount IS DISTINCT FROM NEW.second_eic_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 56, OLD.second_eic_amount);
    changes = changes + 1;
  END

  /* THIRD_EIC_ADDITIONAL_PERCENT */
  IF (OLD.third_eic_additional_percent IS DISTINCT FROM NEW.third_eic_additional_percent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 55, OLD.third_eic_additional_percent);
    changes = changes + 1;
  END

  /* SY_CATCH_UP_LIMIT */
  IF (OLD.sy_catch_up_limit IS DISTINCT FROM NEW.sy_catch_up_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 54, OLD.sy_catch_up_limit);
    changes = changes + 1;
  END

  /* SY_DEPENDENT_CARE_LIMIT */
  IF (OLD.sy_dependent_care_limit IS DISTINCT FROM NEW.sy_dependent_care_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 53, OLD.sy_dependent_care_limit);
    changes = changes + 1;
  END

  /* SY_HSA_SINGLE_LIMIT */
  IF (OLD.sy_hsa_single_limit IS DISTINCT FROM NEW.sy_hsa_single_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 52, OLD.sy_hsa_single_limit);
    changes = changes + 1;
  END

  /* SY_HSA_FAMILY_LIMIT */
  IF (OLD.sy_hsa_family_limit IS DISTINCT FROM NEW.sy_hsa_family_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 51, OLD.sy_hsa_family_limit);
    changes = changes + 1;
  END

  /* SY_PENSION_CATCH_UP_LIMIT */
  IF (OLD.sy_pension_catch_up_limit IS DISTINCT FROM NEW.sy_pension_catch_up_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 50, OLD.sy_pension_catch_up_limit);
    changes = changes + 1;
  END

  /* SY_SIMPLE_CATCH_UP_LIMIT */
  IF (OLD.sy_simple_catch_up_limit IS DISTINCT FROM NEW.sy_simple_catch_up_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 49, OLD.sy_simple_catch_up_limit);
    changes = changes + 1;
  END

  /* COMPENSATION_LIMIT */
  IF (OLD.compensation_limit IS DISTINCT FROM NEW.compensation_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 48, OLD.compensation_limit);
    changes = changes + 1;
  END

  /* SY_HSA_CATCH_UP_LIMIT */
  IF (OLD.sy_hsa_catch_up_limit IS DISTINCT FROM NEW.sy_hsa_catch_up_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 47, OLD.sy_hsa_catch_up_limit);
    changes = changes + 1;
  END

  /* S132_PARKING_LIMIT */
  IF (OLD.s132_parking_limit IS DISTINCT FROM NEW.s132_parking_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 46, OLD.s132_parking_limit);
    changes = changes + 1;
  END

  /* ROTH_IRA_LIMIT */
  IF (OLD.roth_ira_limit IS DISTINCT FROM NEW.roth_ira_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 45, OLD.roth_ira_limit);
    changes = changes + 1;
  END

  /* SY_IRA_CATCHUP_LIMIT */
  IF (OLD.sy_ira_catchup_limit IS DISTINCT FROM NEW.sy_ira_catchup_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 44, OLD.sy_ira_catchup_limit);
    changes = changes + 1;
  END

  /* EE_MED_TH_LIMIT */
  IF (OLD.ee_med_th_limit IS DISTINCT FROM NEW.ee_med_th_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 561, OLD.ee_med_th_limit);
    changes = changes + 1;
  END

  /* EE_MED_TH_RATE */
  IF (OLD.ee_med_th_rate IS DISTINCT FROM NEW.ee_med_th_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 562, OLD.ee_med_th_rate);
    changes = changes + 1;
  END

  /* FSA_LIMIT */
  IF (OLD.fsa_limit IS DISTINCT FROM NEW.fsa_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 583, OLD.fsa_limit);
    changes = changes + 1;
  END

  /* FED_POVERTY_LEVEL */
  IF (OLD.fed_poverty_level IS DISTINCT FROM NEW.fed_poverty_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 618, OLD.fed_poverty_level);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^





CREATE TRIGGER T_AD_SY_DASHBOARDS_9 FOR SY_DASHBOARDS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, OLD.rec_version, OLD.sy_dashboards_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 586, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 587, OLD.effective_until);

  /* DASHBOARD_TYPE */
  IF (OLD.dashboard_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 588, OLD.dashboard_type);

  /* SY_ANALYTICS_TIER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 589, OLD.sy_analytics_tier_nbr);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 590, :blob_nbr);
  END

  /* DASHBOARD_ID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 591, OLD.dashboard_id);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 592, OLD.description);

  /* RELEASED */
  IF (OLD.released IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 619, OLD.released);

END

^

CREATE TRIGGER T_AU_SY_DASHBOARDS_9 FOR SY_DASHBOARDS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, NEW.rec_version, NEW.sy_dashboards_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 586, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 587, OLD.effective_until);
    changes = changes + 1;
  END

  /* DASHBOARD_TYPE */
  IF (OLD.dashboard_type IS DISTINCT FROM NEW.dashboard_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 588, OLD.dashboard_type);
    changes = changes + 1;
  END

  /* SY_ANALYTICS_TIER_NBR */
  IF (OLD.sy_analytics_tier_nbr IS DISTINCT FROM NEW.sy_analytics_tier_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 589, OLD.sy_analytics_tier_nbr);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 590, :blob_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_ID */
  IF (OLD.dashboard_id IS DISTINCT FROM NEW.dashboard_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 591, OLD.dashboard_id);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 592, OLD.description);
    changes = changes + 1;
  END

  /* RELEASED */
  IF (OLD.released IS DISTINCT FROM NEW.released) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 619, OLD.released);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SY_DASHBOARDS_1 FOR SY_DASHBOARDS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.DASHBOARD_TYPE is null OR CHAR_LENGTH(TRIM(new.DASHBOARD_TYPE))=0) THEN 
      new.DASHBOARD_TYPE = 'N'; 

    IF (new.RELEASED is null OR CHAR_LENGTH(TRIM(new.RELEASED))=0) THEN 
      new.RELEASED = 'N'; 

  END 
END

^




COMMIT^









/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (7, 618, 'FED_POVERTY_LEVEL', 'N', 18, 6, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 619, 'RELEASED', 'C', 1, NULL, 'N', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^



/* Update EV_DATABASE */
EXECUTE block
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (VERSION, description) VALUES ('16.0.0.2', 'Evolution System Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
