/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.0';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


DROP TRIGGER T_AUD_CL_CO_CONSOLIDATION_2
^


DROP TRIGGER T_AD_CO_9
^


DROP TRIGGER T_AIU_CO_3
^


DROP TRIGGER T_AU_CO_9
^


DROP TRIGGER T_BIU_CO_2
^

ALTER TABLE CO
DROP ACA_CONTROL_GROUP
^

ALTER TABLE CO
ADD ACA_CL_CO_CONSOLIDATION_NBR EV_INT 
^

ALTER TABLE CO
ALTER COLUMN ACA_CL_CO_CONSOLIDATION_NBR POSITION 291
^


CREATE INDEX FK_CO_31 ON CO (ACA_CL_CO_CONSOLIDATION_NBR,EFFECTIVE_UNTIL)
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY1
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check CL */
  child_table = 'CL';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'AGENCY_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_AGENCY */
  child_table = 'CL_AGENCY';

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'CL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_BANK_ACCOUNT */
  child_table = 'CL_BANK_ACCOUNT';

  parent_table = 'CL_BLOB';
  child_field = 'LOGO_CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'SIGNATURE_CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_CO_CONSOLIDATION */
  child_table = 'CL_CO_CONSOLIDATION';

  parent_table = 'CO';
  child_field = 'PRIMARY_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_DELIVERY_GROUP */
  child_table = 'CL_DELIVERY_GROUP';

  parent_table = 'CL_DELIVERY_METHOD';
  child_field = 'PRIMARY_CL_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_METHOD';
  child_field = 'SECOND_CL_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_DS */
  child_table = 'CL_E_DS';

  parent_table = 'CL_E_DS';
  child_field = 'OFFSET_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MIN_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'PIECE_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'OT_ALL_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'DEFAULT_CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_3_PARTY_SICK_PAY_ADMIN';
  child_field = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_MAX_AVG_AMT_GRP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_MAX_AVG_HRS_GRP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_THRESHOLD_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_FUNDING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_EXCESS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_GROUP_CODES */
  child_table = 'CL_E_D_GROUP_CODES';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_LOCAL_EXMPT_EXCLD */
  child_table = 'CL_E_D_LOCAL_EXMPT_EXCLD';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_STATE_EXMPT_EXCLD */
  child_table = 'CL_E_D_STATE_EXMPT_EXCLD';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_FUNDS */
  child_table = 'CL_FUNDS';

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_EDUCATION */
  child_table = 'CL_HR_PERSON_EDUCATION';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_SCHOOL';
  child_field = 'CL_HR_SCHOOL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_HANDICAPS */
  child_table = 'CL_HR_PERSON_HANDICAPS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_SKILLS */
  child_table = 'CL_HR_PERSON_SKILLS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_SKILLS';
  child_field = 'CL_HR_SKILLS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_MAIL_BOX_GROUP */
  child_table = 'CL_MAIL_BOX_GROUP';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'UP_LEVEL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_MAIL_BOX_GROUP_OPTION */
  child_table = 'CL_MAIL_BOX_GROUP_OPTION';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'CL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PENSION */
  child_table = 'CL_PENSION';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PERSON_DEPENDENTS */
  child_table = 'CL_PERSON_DEPENDENTS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PERSON_DOCUMENTS */
  child_table = 'CL_PERSON_DOCUMENTS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_UNION */
  child_table = 'CL_UNION';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_UNION_DUES */
  child_table = 'CL_UNION_DUES';

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO */
  child_table = 'CO';

  parent_table = 'CL_COMMON_PAYMASTER';
  child_field = 'CL_COMMON_PAYMASTER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'WORKERS_COMP_CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'ANNUAL_CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'QUARTER_CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'AGENCY_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'MANUAL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'W_COMP_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'AUTO_RD_DFLT_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_SDI_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_SUI_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'ACA_ADD_EARN_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'ACA_CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO_NAMES */
  child_table = 'CO_ADDITIONAL_INFO_NAMES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO_VALUES */
  child_table = 'CO_ADDITIONAL_INFO_VALUES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_AUTO_ENLIST_RETURNS */
  child_table = 'CO_AUTO_ENLIST_RETURNS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BANK_ACCOUNT_REGISTER */
  child_table = 'CO_BANK_ACCOUNT_REGISTER';

  parent_table = 'CO_MANUAL_ACH';
  child_field = 'CO_MANUAL_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_ACH';
  child_field = 'CO_PR_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_PAYMENT_ACH';
  child_field = 'CO_TAX_PAYMENT_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_MISCELLANEOUS_CHECKS';
  child_field = 'PR_MISCELLANEOUS_CHECKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BANK_ACC_REG_DETAILS */
  child_table = 'CO_BANK_ACC_REG_DETAILS';

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'CO_BANK_ACCOUNT_REGISTER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BATCH_LOCAL_OR_TEMPS */
  child_table = 'CO_BATCH_LOCAL_OR_TEMPS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BATCH_STATES_OR_TEMPS */
  child_table = 'CO_BATCH_STATES_OR_TEMPS';

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BILLING_HISTORY */
  child_table = 'CO_BILLING_HISTORY';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CO_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BILLING_HISTORY_DETAIL */
  child_table = 'CO_BILLING_HISTORY_DETAIL';

  parent_table = 'CO_BILLING_HISTORY';
  child_field = 'CO_BILLING_HISTORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SERVICES';
  child_field = 'CO_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRANCH */
  child_table = 'CO_BRANCH';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRANCH_LOCALS */
  child_table = 'CO_BRANCH_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRCH_PR_BATCH_DEFLT_ED */
  child_table = 'CO_BRCH_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_CALENDAR_DEFAULTS */
  child_table = 'CO_CALENDAR_DEFAULTS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPARTMENT */
  child_table = 'CO_DEPARTMENT';

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPARTMENT_LOCALS */
  child_table = 'CO_DEPARTMENT_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPT_PR_BATCH_DEFLT_ED */
  child_table = 'CO_DEPT_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIVISION */
  child_table = 'CO_DIVISION';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIVISION_LOCALS */
  child_table = 'CO_DIVISION_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIV_PR_BATCH_DEFLT_ED */
  child_table = 'CO_DIV_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ENLIST_GROUPS */
  child_table = 'CO_ENLIST_GROUPS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_E_D_CODES */
  child_table = 'CO_E_D_CODES';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_FED_TAX_LIABILITIES */
  child_table = 'CO_FED_TAX_LIABILITIES';

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GENERAL_LEDGER */
  child_table = 'CO_GENERAL_LEDGER';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP */
  child_table = 'CO_GROUP';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP_MANAGER */
  child_table = 'CO_GROUP_MANAGER';

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP_MEMBER */
  child_table = 'CO_GROUP_MEMBER';

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_APPLICANT */
  child_table = 'CO_HR_APPLICANT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_RECRUITERS';
  child_field = 'CO_HR_RECRUITERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_REFERRALS';
  child_field = 'CO_HR_REFERRALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_CAR';
  child_field = 'CO_HR_CAR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_APPLICANT_INTERVIEW */
  child_table = 'CO_HR_APPLICANT_INTERVIEW';

  parent_table = 'CO_HR_APPLICANT';
  child_field = 'CO_HR_APPLICANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_ATTENDANCE_TYPES */
  child_table = 'CO_HR_ATTENDANCE_TYPES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_CAR */
  child_table = 'CO_HR_CAR';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_PERFORMANCE_RATINGS */
  child_table = 'CO_HR_PERFORMANCE_RATINGS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_POSITIONS */
  child_table = 'CO_HR_POSITIONS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_PROPERTY */
  child_table = 'CO_HR_PROPERTY';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_RECRUITERS */
  child_table = 'CO_HR_RECRUITERS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_REFERRALS */
  child_table = 'CO_HR_REFERRALS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_SALARY_GRADES */
  child_table = 'CO_HR_SALARY_GRADES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_SUPERVISORS */
  child_table = 'CO_HR_SUPERVISORS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOBS */
  child_table = 'CO_JOBS';

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOBS_LOCALS */
  child_table = 'CO_JOBS_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOB_GROUPS */
  child_table = 'CO_JOB_GROUPS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCAL_TAX */
  child_table = 'CO_LOCAL_TAX';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCAL_TAX_LIABILITIES */
  child_table = 'CO_LOCAL_TAX_LIABILITIES';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NONRES_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_MANUAL_ACH */
  child_table = 'CO_MANUAL_ACH';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'TO_EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'FROM_EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'FROM_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TO_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'FROM_EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'TO_EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'FROM_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'TO_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PAY_GROUP */
  child_table = 'CO_PAY_GROUP';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PENSIONS */
  child_table = 'CO_PENSIONS';

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY1 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY1 TO EUSER
^

CREATE TRIGGER T_AUD_CL_CO_CONSOLIDATION_2 FOR CL_CO_CONSOLIDATION After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM cl_co_consolidation WHERE cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM cl_co_consolidation WHERE cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_co_consolidation', OLD.cl_co_consolidation_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_co_consolidation', OLD.cl_co_consolidation_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TAX_RETURN_QUEUE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_return_queue
    WHERE (cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_co_consolidation', OLD.cl_co_consolidation_nbr, 'co_tax_return_queue', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_return_queue
    WHERE (cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_co_consolidation', OLD.cl_co_consolidation_nbr, 'co_tax_return_queue', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (aca_cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_co_consolidation', OLD.cl_co_consolidation_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE (aca_cl_co_consolidation_nbr = OLD.cl_co_consolidation_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'cl_co_consolidation', OLD.cl_co_consolidation_nbr, 'co', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_CO_9 FOR CO After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE employer_type CHAR(1);
DECLARE VARIABLE co_locations_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  table_change_nbr = rdb$get_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR');
  IF (table_change_nbr IS NULL) THEN
    EXIT;

  last_record = 'Y';
  SELECT 'N', employer_type, co_locations_nbr  FROM co WHERE co_nbr = OLD.co_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :employer_type, :co_locations_nbr;

  /* EMPLOYER_TYPE */
  IF (last_record = 'Y' OR employer_type IS DISTINCT FROM OLD.employer_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2885, OLD.employer_type);

  /* CO_LOCATIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_locations_nbr IS NOT NULL) OR (last_record = 'N' AND co_locations_nbr IS DISTINCT FROM OLD.co_locations_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3169, OLD.co_locations_nbr);


  IF (last_record = 'Y') THEN
  BEGIN
    /* VT_HEALTHCARE_OFFSET_GL_TAG */
    IF (OLD.vt_healthcare_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 660, OLD.vt_healthcare_offset_gl_tag);

    /* UI_ROUNDING_GL_TAG */
    IF (OLD.ui_rounding_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 661, OLD.ui_rounding_gl_tag);

    /* UI_ROUNDING_OFFSET_GL_TAG */
    IF (OLD.ui_rounding_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 662, OLD.ui_rounding_offset_gl_tag);

    /* REPRINT_TO_BALANCE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 763, OLD.reprint_to_balance);

    /* SHOW_MANUAL_CHECKS_IN_ESS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 765, OLD.show_manual_checks_in_ess);

    /* PAYROLL_REQUIRES_MGR_APPROVAL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 766, OLD.payroll_requires_mgr_approval);

    /* DAYS_PRIOR_TO_CHECK_DATE */
    IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 599, OLD.days_prior_to_check_date);

    /* WC_OFFS_BANK_ACCOUNT_NBR */
    IF (OLD.wc_offs_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 603, OLD.wc_offs_bank_account_nbr);

    /* QTR_LOCK_FOR_TAX_PMTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 767, OLD.qtr_lock_for_tax_pmts);

    /* CO_MAX_AMOUNT_FOR_PAYROLL */
    IF (OLD.co_max_amount_for_payroll IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 515, OLD.co_max_amount_for_payroll);

    /* CO_MAX_AMOUNT_FOR_TAX_IMPOUND */
    IF (OLD.co_max_amount_for_tax_impound IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 516, OLD.co_max_amount_for_tax_impound);

    /* CO_MAX_AMOUNT_FOR_DD */
    IF (OLD.co_max_amount_for_dd IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 517, OLD.co_max_amount_for_dd);

    /* CO_PAYROLL_PROCESS_LIMITATIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 768, OLD.co_payroll_process_limitations);

    /* CO_ACH_PROCESS_LIMITATIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 769, OLD.co_ach_process_limitations);

    /* TRUST_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 770, OLD.trust_impound);

    /* TAX_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 771, OLD.tax_impound);

    /* DD_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 772, OLD.dd_impound);

    /* BILLING_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 773, OLD.billing_impound);

    /* WC_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 774, OLD.wc_impound);

    /* COBRA_CREDIT_GL_TAG */
    IF (OLD.cobra_credit_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 663, OLD.cobra_credit_gl_tag);

    /* CO_EXCEPTION_PAYMENT_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 775, OLD.co_exception_payment_type);

    /* LAST_TAX_RETURN */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 776, OLD.last_tax_return);

    /* ENABLE_HR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 764, OLD.enable_hr);

    /* ENABLE_ESS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 777, OLD.enable_ess);

    /* DUNS_AND_BRADSTREET */
    IF (OLD.duns_and_bradstreet IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 519, OLD.duns_and_bradstreet);

    /* BANK_ACCOUNT_REGISTER_NAME */
    IF (OLD.bank_account_register_name IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 780, OLD.bank_account_register_name);

    /* ENABLE_BENEFITS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2883, OLD.enable_benefits);

    /* ENABLE_TIME_OFF */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2884, OLD.enable_time_off);

    /* WC_FISCAL_YEAR_BEGIN */
    IF (OLD.wc_fiscal_year_begin IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2886, OLD.wc_fiscal_year_begin);

    /* SHOW_PAYSTUBS_ESS_DAYS */
    IF (OLD.show_paystubs_ess_days IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2888, OLD.show_paystubs_ess_days);

    /* ANNUAL_FORM_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3200, OLD.annual_form_type);

    /* PRENOTE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3201, OLD.prenote);

    /* ENABLE_DD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3202, OLD.enable_dd);

    /* ESS_OR_EP */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3203, OLD.ess_or_ep);

    /* ACA_EDUCATION_ORG */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3204, OLD.aca_education_org);

    /* ACA_STANDARD_HOURS */
    IF (OLD.aca_standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3205, OLD.aca_standard_hours);

    /* ACA_DEFAULT_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3206, OLD.aca_default_status);

    /* ACA_CL_CO_CONSOLIDATION_NBR */
    IF (OLD.aca_cl_co_consolidation_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3207, OLD.aca_cl_co_consolidation_nbr);

    /* ACA_INITIAL_PERIOD_FROM */
    IF (OLD.aca_initial_period_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3208, OLD.aca_initial_period_from);

    /* ACA_INITIAL_PERIOD_TO */
    IF (OLD.aca_initial_period_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3209, OLD.aca_initial_period_to);

    /* ACA_STABILITY_PERIOD_FROM */
    IF (OLD.aca_stability_period_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3210, OLD.aca_stability_period_from);

    /* ACA_STABILITY_PERIOD_TO */
    IF (OLD.aca_stability_period_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3211, OLD.aca_stability_period_to);

    /* ACA_USE_AVG_HOURS_WORKED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3212, OLD.aca_use_avg_hours_worked);

    /* ACA_AVG_HOURS_WORKED_PERIOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3213, OLD.aca_avg_hours_worked_period);

    /* ACA_ADD_EARN_CL_E_D_GROUPS_NBR */
    IF (OLD.aca_add_earn_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3227, OLD.aca_add_earn_cl_e_d_groups_nbr);

  END

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', NULL);
END

^

CREATE TRIGGER T_AIU_CO_3 FOR CO After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.custom_company_number IS DISTINCT FROM NEW.custom_company_number) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.referred_by IS DISTINCT FROM NEW.referred_by) OR (OLD.sb_referrals_nbr IS DISTINCT FROM NEW.sb_referrals_nbr) OR (OLD.start_date IS DISTINCT FROM NEW.start_date) OR (rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES') IS NOT NULL) OR (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) OR (OLD.accountant_contact IS DISTINCT FROM NEW.accountant_contact) OR (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr) OR (OLD.remote IS DISTINCT FROM NEW.remote) OR (OLD.business_start_date IS DISTINCT FROM NEW.business_start_date) OR (OLD.corporation_type IS DISTINCT FROM NEW.corporation_type) OR (rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS') IS NOT NULL) OR (OLD.restaurant IS DISTINCT FROM NEW.restaurant) OR (OLD.days_open IS DISTINCT FROM NEW.days_open) OR (OLD.time_open IS DISTINCT FROM NEW.time_open) OR (OLD.time_close IS DISTINCT FROM NEW.time_close) OR (rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES') IS NOT NULL) OR (OLD.successor_company IS DISTINCT FROM NEW.successor_company) OR (OLD.medical_plan IS DISTINCT FROM NEW.medical_plan) OR (OLD.retirement_age IS DISTINCT FROM NEW.retirement_age) OR (OLD.pay_frequencies IS DISTINCT FROM NEW.pay_frequencies) OR (OLD.general_ledger_format_string IS DISTINCT FROM NEW.general_ledger_format_string) OR (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr) OR (OLD.dbdt_level IS DISTINCT FROM NEW.dbdt_level) OR (OLD.billing_level IS DISTINCT FROM NEW.billing_level) OR (OLD.bank_account_level IS DISTINCT FROM NEW.bank_account_level) OR (OLD.billing_sb_bank_account_nbr IS DISTINCT FROM NEW.billing_sb_bank_account_nbr) OR (OLD.tax_sb_bank_account_nbr IS DISTINCT FROM NEW.tax_sb_bank_account_nbr) OR (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_pr IS DISTINCT FROM NEW.debit_number_days_prior_pr) OR (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_tax IS DISTINCT FROM NEW.debit_number_of_days_prior_tax) OR (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_bill IS DISTINCT FROM NEW.debit_number_days_prior_bill) OR (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_dd IS DISTINCT FROM NEW.debit_number_of_days_prior_dd) OR (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr) OR (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr) OR (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr) OR (OLD.tax_service IS DISTINCT FROM NEW.tax_service) OR (OLD.tax_service_start_date IS DISTINCT FROM NEW.tax_service_start_date) OR (OLD.tax_service_end_date IS DISTINCT FROM NEW.tax_service_end_date) OR (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr) OR (OLD.workers_comp_policy_id IS DISTINCT FROM NEW.workers_comp_policy_id) OR (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_wc IS DISTINCT FROM NEW.debit_number_days_prior_wc) OR (OLD.w_comp_sb_bank_account_nbr IS DISTINCT FROM NEW.w_comp_sb_bank_account_nbr) OR (OLD.eftps_enrollment_status IS DISTINCT FROM NEW.eftps_enrollment_status) OR (OLD.eftps_enrollment_date IS DISTINCT FROM NEW.eftps_enrollment_date) OR (OLD.eftps_sequence_number IS DISTINCT FROM NEW.eftps_sequence_number) OR (OLD.eftps_enrollment_number IS DISTINCT FROM NEW.eftps_enrollment_number) OR (OLD.eftps_name IS DISTINCT FROM NEW.eftps_name) OR (OLD.pin_number IS DISTINCT FROM NEW.pin_number) OR (OLD.ach_sb_bank_account_nbr IS DISTINCT FROM NEW.ach_sb_bank_account_nbr) OR (OLD.trust_service IS DISTINCT FROM NEW.trust_service) OR (OLD.trust_sb_account_nbr IS DISTINCT FROM NEW.trust_sb_account_nbr) OR (OLD.trust_service_start_date IS DISTINCT FROM NEW.trust_service_start_date) OR (OLD.trust_service_end_date IS DISTINCT FROM NEW.trust_service_end_date) OR (OLD.obc IS DISTINCT FROM NEW.obc) OR (OLD.sb_obc_account_nbr IS DISTINCT FROM NEW.sb_obc_account_nbr) OR (OLD.obc_start_date IS DISTINCT FROM NEW.obc_start_date) OR (OLD.obc_end_date IS DISTINCT FROM NEW.obc_end_date) OR (OLD.sy_fed_reporting_agency_nbr IS DISTINCT FROM NEW.sy_fed_reporting_agency_nbr) OR (OLD.sy_fed_tax_payment_agency_nbr IS DISTINCT FROM NEW.sy_fed_tax_payment_agency_nbr) OR (OLD.federal_tax_transfer_method IS DISTINCT FROM NEW.federal_tax_transfer_method) OR (OLD.federal_tax_payment_method IS DISTINCT FROM NEW.federal_tax_payment_method) OR (OLD.fui_sy_tax_payment_agency IS DISTINCT FROM NEW.fui_sy_tax_payment_agency) OR (OLD.fui_sy_tax_report_agency IS DISTINCT FROM NEW.fui_sy_tax_report_agency) OR (OLD.external_tax_export IS DISTINCT FROM NEW.external_tax_export) OR (OLD.non_profit IS DISTINCT FROM NEW.non_profit) OR (OLD.primary_sort_field IS DISTINCT FROM NEW.primary_sort_field) OR (OLD.secondary_sort_field IS DISTINCT FROM NEW.secondary_sort_field) OR (OLD.fiscal_year_end IS DISTINCT FROM NEW.fiscal_year_end) OR (OLD.third_party_in_gross_pr_report IS DISTINCT FROM NEW.third_party_in_gross_pr_report) OR (OLD.sic_code IS DISTINCT FROM NEW.sic_code) OR (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) OR (OLD.autopay_company IS DISTINCT FROM NEW.autopay_company) OR (OLD.pay_frequency_hourly_default IS DISTINCT FROM NEW.pay_frequency_hourly_default) OR (OLD.pay_frequency_salary_default IS DISTINCT FROM NEW.pay_frequency_salary_default) OR (OLD.co_check_primary_sort IS DISTINCT FROM NEW.co_check_primary_sort) OR (OLD.co_check_secondary_sort IS DISTINCT FROM NEW.co_check_secondary_sort) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr) OR (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr) OR (OLD.smoker_default IS DISTINCT FROM NEW.smoker_default) OR (OLD.auto_labor_dist_show_deducts IS DISTINCT FROM NEW.auto_labor_dist_show_deducts) OR (OLD.auto_labor_dist_level IS DISTINCT FROM NEW.auto_labor_dist_level) OR (OLD.distribute_deductions_default IS DISTINCT FROM NEW.distribute_deductions_default) OR (OLD.withholding_default IS DISTINCT FROM NEW.withholding_default) OR (OLD.check_type IS DISTINCT FROM NEW.check_type) OR (OLD.show_shifts_on_check IS DISTINCT FROM NEW.show_shifts_on_check) OR (OLD.show_ytd_on_check IS DISTINCT FROM NEW.show_ytd_on_check) OR (OLD.show_ein_number_on_check IS DISTINCT FROM NEW.show_ein_number_on_check) OR (OLD.show_ss_number_on_check IS DISTINCT FROM NEW.show_ss_number_on_check) OR (OLD.time_off_accrual IS DISTINCT FROM NEW.time_off_accrual) OR (OLD.check_form IS DISTINCT FROM NEW.check_form) OR (OLD.print_manual_check_stubs IS DISTINCT FROM NEW.print_manual_check_stubs) OR (OLD.credit_hold IS DISTINCT FROM NEW.credit_hold) OR (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr) OR (OLD.payroll_password IS DISTINCT FROM NEW.payroll_password) OR (OLD.remote_of_client IS DISTINCT FROM NEW.remote_of_client) OR (OLD.hardware_o_s IS DISTINCT FROM NEW.hardware_o_s) OR (OLD.network IS DISTINCT FROM NEW.network) OR (OLD.modem_speed IS DISTINCT FROM NEW.modem_speed) OR (OLD.modem_connection_type IS DISTINCT FROM NEW.modem_connection_type) OR (OLD.network_administrator IS DISTINCT FROM NEW.network_administrator) OR (OLD.network_administrator_phone IS DISTINCT FROM NEW.network_administrator_phone) OR (OLD.network_admin_phone_type IS DISTINCT FROM NEW.network_admin_phone_type) OR (OLD.external_network_administrator IS DISTINCT FROM NEW.external_network_administrator) OR (OLD.transmission_destination IS DISTINCT FROM NEW.transmission_destination) OR (OLD.last_call_in_date IS DISTINCT FROM NEW.last_call_in_date) OR (OLD.setup_completed IS DISTINCT FROM NEW.setup_completed) OR (OLD.first_monthly_payroll_day IS DISTINCT FROM NEW.first_monthly_payroll_day) OR (OLD.second_monthly_payroll_day IS DISTINCT FROM NEW.second_monthly_payroll_day) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.collate_checks IS DISTINCT FROM NEW.collate_checks) OR (OLD.process_priority IS DISTINCT FROM NEW.process_priority) OR (OLD.check_message IS DISTINCT FROM NEW.check_message) OR (OLD.customer_service_sb_user_nbr IS DISTINCT FROM NEW.customer_service_sb_user_nbr) OR (rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES') IS NOT NULL) OR (rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES') IS NOT NULL) OR (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.billing_general_ledger_tag IS DISTINCT FROM NEW.billing_general_ledger_tag) OR (OLD.federal_general_ledger_tag IS DISTINCT FROM NEW.federal_general_ledger_tag) OR (OLD.ee_oasdi_general_ledger_tag IS DISTINCT FROM NEW.ee_oasdi_general_ledger_tag) OR (OLD.er_oasdi_general_ledger_tag IS DISTINCT FROM NEW.er_oasdi_general_ledger_tag) OR (OLD.ee_medicare_general_ledger_tag IS DISTINCT FROM NEW.ee_medicare_general_ledger_tag) OR (OLD.er_medicare_general_ledger_tag IS DISTINCT FROM NEW.er_medicare_general_ledger_tag) OR (OLD.fui_general_ledger_tag IS DISTINCT FROM NEW.fui_general_ledger_tag) OR (OLD.eic_general_ledger_tag IS DISTINCT FROM NEW.eic_general_ledger_tag) OR (OLD.backup_w_general_ledger_tag IS DISTINCT FROM NEW.backup_w_general_ledger_tag) OR (OLD.net_pay_general_ledger_tag IS DISTINCT FROM NEW.net_pay_general_ledger_tag) OR (OLD.tax_imp_general_ledger_tag IS DISTINCT FROM NEW.tax_imp_general_ledger_tag) OR (OLD.trust_imp_general_ledger_tag IS DISTINCT FROM NEW.trust_imp_general_ledger_tag) OR (OLD.thrd_p_tax_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_tax_general_ledger_tag) OR (OLD.thrd_p_chk_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_chk_general_ledger_tag) OR (OLD.trust_chk_general_ledger_tag IS DISTINCT FROM NEW.trust_chk_general_ledger_tag) OR (OLD.federal_offset_gl_tag IS DISTINCT FROM NEW.federal_offset_gl_tag) OR (OLD.ee_oasdi_offset_gl_tag IS DISTINCT FROM NEW.ee_oasdi_offset_gl_tag) OR (OLD.er_oasdi_offset_gl_tag IS DISTINCT FROM NEW.er_oasdi_offset_gl_tag) OR (OLD.ee_medicare_offset_gl_tag IS DISTINCT FROM NEW.ee_medicare_offset_gl_tag) OR (OLD.er_medicare_offset_gl_tag IS DISTINCT FROM NEW.er_medicare_offset_gl_tag) OR (OLD.fui_offset_gl_tag IS DISTINCT FROM NEW.fui_offset_gl_tag) OR (OLD.eic_offset_gl_tag IS DISTINCT FROM NEW.eic_offset_gl_tag) OR (OLD.backup_w_offset_gl_tag IS DISTINCT FROM NEW.backup_w_offset_gl_tag) OR (OLD.trust_imp_offset_gl_tag IS DISTINCT FROM NEW.trust_imp_offset_gl_tag) OR (OLD.billing_exp_gl_tag IS DISTINCT FROM NEW.billing_exp_gl_tag) OR (OLD.er_oasdi_exp_gl_tag IS DISTINCT FROM NEW.er_oasdi_exp_gl_tag) OR (OLD.er_medicare_exp_gl_tag IS DISTINCT FROM NEW.er_medicare_exp_gl_tag) OR (OLD.auto_enlist IS DISTINCT FROM NEW.auto_enlist) OR (OLD.calculate_locals_first IS DISTINCT FROM NEW.calculate_locals_first) OR (OLD.weekend_action IS DISTINCT FROM NEW.weekend_action) OR (OLD.last_fui_correction IS DISTINCT FROM NEW.last_fui_correction) OR (OLD.last_sui_correction IS DISTINCT FROM NEW.last_sui_correction) OR (OLD.last_quarter_end_correction IS DISTINCT FROM NEW.last_quarter_end_correction) OR (OLD.last_preprocess IS DISTINCT FROM NEW.last_preprocess) OR (OLD.last_preprocess_message IS DISTINCT FROM NEW.last_preprocess_message) OR (OLD.charge_cobra_admin_fee IS DISTINCT FROM NEW.charge_cobra_admin_fee) OR (OLD.cobra_fee_day_of_month_due IS DISTINCT FROM NEW.cobra_fee_day_of_month_due) OR (OLD.cobra_notification_days IS DISTINCT FROM NEW.cobra_notification_days) OR (OLD.cobra_eligibility_confirm_days IS DISTINCT FROM NEW.cobra_eligibility_confirm_days) OR (OLD.show_rates_on_checks IS DISTINCT FROM NEW.show_rates_on_checks) OR (OLD.show_dir_dep_nbr_on_checks IS DISTINCT FROM NEW.show_dir_dep_nbr_on_checks) OR (OLD.fed_943_tax_deposit_frequency IS DISTINCT FROM NEW.fed_943_tax_deposit_frequency) OR (OLD.impound_workers_comp IS DISTINCT FROM NEW.impound_workers_comp) OR (OLD.reverse_check_printing IS DISTINCT FROM NEW.reverse_check_printing) OR (OLD.lock_date IS DISTINCT FROM NEW.lock_date) OR (OLD.auto_increment IS DISTINCT FROM NEW.auto_increment) OR (OLD.maximum_group_term_life IS DISTINCT FROM NEW.maximum_group_term_life) OR (OLD.payrate_precision IS DISTINCT FROM NEW.payrate_precision) OR (OLD.average_hours IS DISTINCT FROM NEW.average_hours) OR (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) OR (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) OR (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) OR (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) OR (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) OR (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) OR (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) OR (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) OR (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) OR (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) OR (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) OR (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) OR (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) OR (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) OR (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) OR (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) OR (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) OR (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) OR (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) OR (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) OR (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) OR (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) OR (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) OR (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) OR (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) OR (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) OR (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) OR (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) OR (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) OR (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) OR (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) OR (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) OR (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) OR (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) OR (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) OR (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) OR (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) OR (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) OR (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) OR (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) OR (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) OR (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) OR (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) OR (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) OR (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) OR (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) OR (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) OR (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) OR (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) OR (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) OR (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) OR (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) OR (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) OR (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) OR (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) OR (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) OR (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) OR (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) OR (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) OR (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) OR (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) OR (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) OR (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) OR (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days) OR (OLD.annual_form_type IS DISTINCT FROM NEW.annual_form_type) OR (OLD.prenote IS DISTINCT FROM NEW.prenote) OR (OLD.enable_dd IS DISTINCT FROM NEW.enable_dd) OR (OLD.ess_or_ep IS DISTINCT FROM NEW.ess_or_ep) OR (OLD.aca_education_org IS DISTINCT FROM NEW.aca_education_org) OR (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) OR (OLD.aca_default_status IS DISTINCT FROM NEW.aca_default_status) OR (OLD.aca_cl_co_consolidation_nbr IS DISTINCT FROM NEW.aca_cl_co_consolidation_nbr) OR (OLD.aca_initial_period_from IS DISTINCT FROM NEW.aca_initial_period_from) OR (OLD.aca_initial_period_to IS DISTINCT FROM NEW.aca_initial_period_to) OR (OLD.aca_stability_period_from IS DISTINCT FROM NEW.aca_stability_period_from) OR (OLD.aca_stability_period_to IS DISTINCT FROM NEW.aca_stability_period_to) OR (OLD.aca_use_avg_hours_worked IS DISTINCT FROM NEW.aca_use_avg_hours_worked) OR (OLD.aca_avg_hours_worked_period IS DISTINCT FROM NEW.aca_avg_hours_worked_period) OR (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE co SET 
      custom_company_number = NEW.custom_company_number, county = NEW.county, e_mail_address = NEW.e_mail_address, referred_by = NEW.referred_by, sb_referrals_nbr = NEW.sb_referrals_nbr, start_date = NEW.start_date, termination_notes = NEW.termination_notes, sb_accountant_nbr = NEW.sb_accountant_nbr, accountant_contact = NEW.accountant_contact, union_cl_e_ds_nbr = NEW.union_cl_e_ds_nbr, remote = NEW.remote, business_start_date = NEW.business_start_date, corporation_type = NEW.corporation_type, nature_of_business = NEW.nature_of_business, restaurant = NEW.restaurant, days_open = NEW.days_open, time_open = NEW.time_open, time_close = NEW.time_close, company_notes = NEW.company_notes, successor_company = NEW.successor_company, medical_plan = NEW.medical_plan, retirement_age = NEW.retirement_age, pay_frequencies = NEW.pay_frequencies, general_ledger_format_string = NEW.general_ledger_format_string, cl_billing_nbr = NEW.cl_billing_nbr, dbdt_level = NEW.dbdt_level, billing_level = NEW.billing_level, bank_account_level = NEW.bank_account_level, billing_sb_bank_account_nbr = NEW.billing_sb_bank_account_nbr, tax_sb_bank_account_nbr = NEW.tax_sb_bank_account_nbr, payroll_cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr, debit_number_days_prior_pr = NEW.debit_number_days_prior_pr, tax_cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr, debit_number_of_days_prior_tax = NEW.debit_number_of_days_prior_tax, billing_cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr, debit_number_days_prior_bill = NEW.debit_number_days_prior_bill, dd_cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr, debit_number_of_days_prior_dd = NEW.debit_number_of_days_prior_dd, home_co_states_nbr = NEW.home_co_states_nbr, home_sdi_co_states_nbr = NEW.home_sdi_co_states_nbr, home_sui_co_states_nbr = NEW.home_sui_co_states_nbr, tax_service = NEW.tax_service, tax_service_start_date = NEW.tax_service_start_date, tax_service_end_date = NEW.tax_service_end_date, workers_comp_cl_agency_nbr = NEW.workers_comp_cl_agency_nbr, workers_comp_policy_id = NEW.workers_comp_policy_id, w_comp_cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr, debit_number_days_prior_wc = NEW.debit_number_days_prior_wc, w_comp_sb_bank_account_nbr = NEW.w_comp_sb_bank_account_nbr, eftps_enrollment_status = NEW.eftps_enrollment_status, eftps_enrollment_date = NEW.eftps_enrollment_date, eftps_sequence_number = NEW.eftps_sequence_number, eftps_enrollment_number = NEW.eftps_enrollment_number, eftps_name = NEW.eftps_name, pin_number = NEW.pin_number, ach_sb_bank_account_nbr = NEW.ach_sb_bank_account_nbr, trust_service = NEW.trust_service, trust_sb_account_nbr = NEW.trust_sb_account_nbr, trust_service_start_date = NEW.trust_service_start_date, trust_service_end_date = NEW.trust_service_end_date, obc = NEW.obc, sb_obc_account_nbr = NEW.sb_obc_account_nbr, obc_start_date = NEW.obc_start_date, obc_end_date = NEW.obc_end_date, sy_fed_reporting_agency_nbr = NEW.sy_fed_reporting_agency_nbr, sy_fed_tax_payment_agency_nbr = NEW.sy_fed_tax_payment_agency_nbr, federal_tax_transfer_method = NEW.federal_tax_transfer_method, federal_tax_payment_method = NEW.federal_tax_payment_method, fui_sy_tax_payment_agency = NEW.fui_sy_tax_payment_agency, fui_sy_tax_report_agency = NEW.fui_sy_tax_report_agency, external_tax_export = NEW.external_tax_export, non_profit = NEW.non_profit, primary_sort_field = NEW.primary_sort_field, secondary_sort_field = NEW.secondary_sort_field, fiscal_year_end = NEW.fiscal_year_end, third_party_in_gross_pr_report = NEW.third_party_in_gross_pr_report, sic_code = NEW.sic_code, deductions_to_zero = NEW.deductions_to_zero, autopay_company = NEW.autopay_company, pay_frequency_hourly_default = NEW.pay_frequency_hourly_default, pay_frequency_salary_default = NEW.pay_frequency_salary_default, co_check_primary_sort = NEW.co_check_primary_sort, co_check_secondary_sort = NEW.co_check_secondary_sort, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, annual_cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr, quarter_cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr, smoker_default = NEW.smoker_default, auto_labor_dist_show_deducts = NEW.auto_labor_dist_show_deducts, auto_labor_dist_level = NEW.auto_labor_dist_level, distribute_deductions_default = NEW.distribute_deductions_default, withholding_default = NEW.withholding_default, check_type = NEW.check_type, show_shifts_on_check = NEW.show_shifts_on_check, show_ytd_on_check = NEW.show_ytd_on_check, show_ein_number_on_check = NEW.show_ein_number_on_check, show_ss_number_on_check = NEW.show_ss_number_on_check, time_off_accrual = NEW.time_off_accrual, check_form = NEW.check_form, print_manual_check_stubs = NEW.print_manual_check_stubs, credit_hold = NEW.credit_hold, cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr, payroll_password = NEW.payroll_password, remote_of_client = NEW.remote_of_client, hardware_o_s = NEW.hardware_o_s, network = NEW.network, modem_speed = NEW.modem_speed, modem_connection_type = NEW.modem_connection_type, network_administrator = NEW.network_administrator, network_administrator_phone = NEW.network_administrator_phone, network_admin_phone_type = NEW.network_admin_phone_type, external_network_administrator = NEW.external_network_administrator, transmission_destination = NEW.transmission_destination, last_call_in_date = NEW.last_call_in_date, setup_completed = NEW.setup_completed, first_monthly_payroll_day = NEW.first_monthly_payroll_day, second_monthly_payroll_day = NEW.second_monthly_payroll_day, filler = NEW.filler, collate_checks = NEW.collate_checks, process_priority = NEW.process_priority, check_message = NEW.check_message, customer_service_sb_user_nbr = NEW.customer_service_sb_user_nbr, invoice_notes = NEW.invoice_notes, tax_cover_letter_notes = NEW.tax_cover_letter_notes, final_tax_return = NEW.final_tax_return, general_ledger_tag = NEW.general_ledger_tag, billing_general_ledger_tag = NEW.billing_general_ledger_tag, federal_general_ledger_tag = NEW.federal_general_ledger_tag, ee_oasdi_general_ledger_tag = NEW.ee_oasdi_general_ledger_tag, er_oasdi_general_ledger_tag = NEW.er_oasdi_general_ledger_tag, ee_medicare_general_ledger_tag = NEW.ee_medicare_general_ledger_tag, er_medicare_general_ledger_tag = NEW.er_medicare_general_ledger_tag, fui_general_ledger_tag = NEW.fui_general_ledger_tag, eic_general_ledger_tag = NEW.eic_general_ledger_tag, backup_w_general_ledger_tag = NEW.backup_w_general_ledger_tag, net_pay_general_ledger_tag = NEW.net_pay_general_ledger_tag, tax_imp_general_ledger_tag = NEW.tax_imp_general_ledger_tag, trust_imp_general_ledger_tag = NEW.trust_imp_general_ledger_tag, thrd_p_tax_general_ledger_tag = NEW.thrd_p_tax_general_ledger_tag, thrd_p_chk_general_ledger_tag = NEW.thrd_p_chk_general_ledger_tag, trust_chk_general_ledger_tag = NEW.trust_chk_general_ledger_tag, federal_offset_gl_tag = NEW.federal_offset_gl_tag, ee_oasdi_offset_gl_tag = NEW.ee_oasdi_offset_gl_tag, er_oasdi_offset_gl_tag = NEW.er_oasdi_offset_gl_tag, ee_medicare_offset_gl_tag = NEW.ee_medicare_offset_gl_tag, er_medicare_offset_gl_tag = NEW.er_medicare_offset_gl_tag, fui_offset_gl_tag = NEW.fui_offset_gl_tag, eic_offset_gl_tag = NEW.eic_offset_gl_tag, backup_w_offset_gl_tag = NEW.backup_w_offset_gl_tag, trust_imp_offset_gl_tag = NEW.trust_imp_offset_gl_tag, billing_exp_gl_tag = NEW.billing_exp_gl_tag, er_oasdi_exp_gl_tag = NEW.er_oasdi_exp_gl_tag, er_medicare_exp_gl_tag = NEW.er_medicare_exp_gl_tag, auto_enlist = NEW.auto_enlist, calculate_locals_first = NEW.calculate_locals_first, weekend_action = NEW.weekend_action, last_fui_correction = NEW.last_fui_correction, last_sui_correction = NEW.last_sui_correction, last_quarter_end_correction = NEW.last_quarter_end_correction, last_preprocess = NEW.last_preprocess, last_preprocess_message = NEW.last_preprocess_message, charge_cobra_admin_fee = NEW.charge_cobra_admin_fee, cobra_fee_day_of_month_due = NEW.cobra_fee_day_of_month_due, cobra_notification_days = NEW.cobra_notification_days, cobra_eligibility_confirm_days = NEW.cobra_eligibility_confirm_days, show_rates_on_checks = NEW.show_rates_on_checks, show_dir_dep_nbr_on_checks = NEW.show_dir_dep_nbr_on_checks, fed_943_tax_deposit_frequency = NEW.fed_943_tax_deposit_frequency, impound_workers_comp = NEW.impound_workers_comp, reverse_check_printing = NEW.reverse_check_printing, lock_date = NEW.lock_date, auto_increment = NEW.auto_increment, maximum_group_term_life = NEW.maximum_group_term_life, payrate_precision = NEW.payrate_precision, average_hours = NEW.average_hours, maximum_hours_on_check = NEW.maximum_hours_on_check, maximum_dollars_on_check = NEW.maximum_dollars_on_check, discount_rate = NEW.discount_rate, mod_rate = NEW.mod_rate, minimum_tax_threshold = NEW.minimum_tax_threshold, ss_disability_admin_fee = NEW.ss_disability_admin_fee, check_time_off_avail = NEW.check_time_off_avail, agency_check_mb_group_nbr = NEW.agency_check_mb_group_nbr, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_report_mb_group_nbr = NEW.pr_report_mb_group_nbr, pr_report_second_mb_group_nbr = NEW.pr_report_second_mb_group_nbr, tax_check_mb_group_nbr = NEW.tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr = NEW.tax_return_mb_group_nbr, tax_return_second_mb_group_nbr = NEW.tax_return_second_mb_group_nbr, misc_check_form = NEW.misc_check_form, hold_return_queue = NEW.hold_return_queue, number_of_invoice_copies = NEW.number_of_invoice_copies, prorate_flat_fee_for_dbdt = NEW.prorate_flat_fee_for_dbdt, initial_effective_date = NEW.initial_effective_date, sb_other_service_nbr = NEW.sb_other_service_nbr, break_checks_by_dbdt = NEW.break_checks_by_dbdt, summarize_sui = NEW.summarize_sui, show_shortfall_check = NEW.show_shortfall_check, trust_chk_offset_gl_tag = NEW.trust_chk_offset_gl_tag, manual_cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr, wells_fargo_ach_flag = NEW.wells_fargo_ach_flag, billing_check_cpa = NEW.billing_check_cpa, sales_tax_percentage = NEW.sales_tax_percentage, exclude_r_c_b_0r_n = NEW.exclude_r_c_b_0r_n, calculate_states_first = NEW.calculate_states_first, invoice_discount = NEW.invoice_discount, discount_start_date = NEW.discount_start_date, discount_end_date = NEW.discount_end_date, show_time_clock_punch = NEW.show_time_clock_punch, auto_rd_dflt_cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr, auto_reduction_default_ees = NEW.auto_reduction_default_ees, vt_healthcare_gl_tag = NEW.vt_healthcare_gl_tag, vt_healthcare_offset_gl_tag = NEW.vt_healthcare_offset_gl_tag, ui_rounding_gl_tag = NEW.ui_rounding_gl_tag, ui_rounding_offset_gl_tag = NEW.ui_rounding_offset_gl_tag, reprint_to_balance = NEW.reprint_to_balance, show_manual_checks_in_ess = NEW.show_manual_checks_in_ess, payroll_requires_mgr_approval = NEW.payroll_requires_mgr_approval, days_prior_to_check_date = NEW.days_prior_to_check_date, wc_offs_bank_account_nbr = NEW.wc_offs_bank_account_nbr, qtr_lock_for_tax_pmts = NEW.qtr_lock_for_tax_pmts, co_max_amount_for_payroll = NEW.co_max_amount_for_payroll, co_max_amount_for_tax_impound = NEW.co_max_amount_for_tax_impound, co_max_amount_for_dd = NEW.co_max_amount_for_dd, co_payroll_process_limitations = NEW.co_payroll_process_limitations, co_ach_process_limitations = NEW.co_ach_process_limitations, trust_impound = NEW.trust_impound, tax_impound = NEW.tax_impound, dd_impound = NEW.dd_impound, billing_impound = NEW.billing_impound, wc_impound = NEW.wc_impound, cobra_credit_gl_tag = NEW.cobra_credit_gl_tag, co_exception_payment_type = NEW.co_exception_payment_type, last_tax_return = NEW.last_tax_return, enable_hr = NEW.enable_hr, enable_ess = NEW.enable_ess, duns_and_bradstreet = NEW.duns_and_bradstreet, bank_account_register_name = NEW.bank_account_register_name, enable_benefits = NEW.enable_benefits, enable_time_off = NEW.enable_time_off, wc_fiscal_year_begin = NEW.wc_fiscal_year_begin, show_paystubs_ess_days = NEW.show_paystubs_ess_days, annual_form_type = NEW.annual_form_type, prenote = NEW.prenote, enable_dd = NEW.enable_dd, ess_or_ep = NEW.ess_or_ep, aca_education_org = NEW.aca_education_org, aca_standard_hours = NEW.aca_standard_hours, aca_default_status = NEW.aca_default_status, aca_cl_co_consolidation_nbr = NEW.aca_cl_co_consolidation_nbr, aca_initial_period_from = NEW.aca_initial_period_from, aca_initial_period_to = NEW.aca_initial_period_to, aca_stability_period_from = NEW.aca_stability_period_from, aca_stability_period_to = NEW.aca_stability_period_to, aca_use_avg_hours_worked = NEW.aca_use_avg_hours_worked, aca_avg_hours_worked_period = NEW.aca_avg_hours_worked_period, aca_add_earn_cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr
      WHERE co_nbr = NEW.co_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_CO_9 FOR CO After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  table_change_nbr = rdb$get_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR');
  IF (table_change_nbr IS NULL) THEN
    EXIT;

  changes = rdb$get_context('USER_TRANSACTION', '@CHANGES');

  /* AVERAGE_HOURS */
  IF (OLD.average_hours IS DISTINCT FROM NEW.average_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 486, OLD.average_hours);
    changes = changes + 1;
  END

  /* MAXIMUM_HOURS_ON_CHECK */
  IF (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 485, OLD.maximum_hours_on_check);
    changes = changes + 1;
  END

  /* MAXIMUM_DOLLARS_ON_CHECK */
  IF (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 484, OLD.maximum_dollars_on_check);
    changes = changes + 1;
  END

  /* DISCOUNT_RATE */
  IF (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 483, OLD.discount_rate);
    changes = changes + 1;
  END

  /* MOD_RATE */
  IF (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.mod_rate);
    changes = changes + 1;
  END

  /* MINIMUM_TAX_THRESHOLD */
  IF (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 481, OLD.minimum_tax_threshold);
    changes = changes + 1;
  END

  /* SS_DISABILITY_ADMIN_FEE */
  IF (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 480, OLD.ss_disability_admin_fee);
    changes = changes + 1;
  END

  /* CHECK_TIME_OFF_AVAIL */
  IF (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 751, OLD.check_time_off_avail);
    changes = changes + 1;
  END

  /* AGENCY_CHECK_MB_GROUP_NBR */
  IF (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 586, OLD.agency_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 587, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 588, OLD.pr_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_REPORT_SECOND_MB_GROUP_NBR */
  IF (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 589, OLD.pr_report_second_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_CHECK_MB_GROUP_NBR */
  IF (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 590, OLD.tax_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 591, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 592, OLD.tax_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_SECOND_MB_GROUP_NBR */
  IF (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 593, OLD.tax_return_second_mb_group_nbr);
    changes = changes + 1;
  END

  /* NAME_ON_INVOICE */
  IF (OLD.name_on_invoice IS DISTINCT FROM NEW.name_on_invoice) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 754, OLD.name_on_invoice);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 755, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* HOLD_RETURN_QUEUE */
  IF (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 756, OLD.hold_return_queue);
    changes = changes + 1;
  END

  /* NUMBER_OF_INVOICE_COPIES */
  IF (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 594, OLD.number_of_invoice_copies);
    changes = changes + 1;
  END

  /* PRORATE_FLAT_FEE_FOR_DBDT */
  IF (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 757, OLD.prorate_flat_fee_for_dbdt);
    changes = changes + 1;
  END

  /* INITIAL_EFFECTIVE_DATE */
  IF (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 595, OLD.initial_effective_date);
    changes = changes + 1;
  END

  /* SB_OTHER_SERVICE_NBR */
  IF (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 596, OLD.sb_other_service_nbr);
    changes = changes + 1;
  END

  /* BREAK_CHECKS_BY_DBDT */
  IF (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 758, OLD.break_checks_by_dbdt);
    changes = changes + 1;
  END

  /* SUMMARIZE_SUI */
  IF (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 759, OLD.summarize_sui);
    changes = changes + 1;
  END

  /* SHOW_SHORTFALL_CHECK */
  IF (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 760, OLD.show_shortfall_check);
    changes = changes + 1;
  END

  /* TRUST_CHK_OFFSET_GL_TAG */
  IF (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 656, OLD.trust_chk_offset_gl_tag);
    changes = changes + 1;
  END

  /* MANUAL_CL_BANK_ACCOUNT_NBR */
  IF (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 597, OLD.manual_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* WELLS_FARGO_ACH_FLAG */
  IF (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 657, OLD.wells_fargo_ach_flag);
    changes = changes + 1;
  END

  /* BILLING_CHECK_CPA */
  IF (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 761, OLD.billing_check_cpa);
    changes = changes + 1;
  END

  /* SALES_TAX_PERCENTAGE */
  IF (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 512, OLD.sales_tax_percentage);
    changes = changes + 1;
  END

  /* EXCLUDE_R_C_B_0R_N */
  IF (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 667, OLD.exclude_r_c_b_0r_n);
    changes = changes + 1;
  END

  /* CALCULATE_STATES_FIRST */
  IF (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 666, OLD.calculate_states_first);
    changes = changes + 1;
  END

  /* INVOICE_DISCOUNT */
  IF (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 479, OLD.invoice_discount);
    changes = changes + 1;
  END

  /* DISCOUNT_START_DATE */
  IF (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 521, OLD.discount_start_date);
    changes = changes + 1;
  END

  /* DISCOUNT_END_DATE */
  IF (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 520, OLD.discount_end_date);
    changes = changes + 1;
  END

  /* SHOW_TIME_CLOCK_PUNCH */
  IF (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 664, OLD.show_time_clock_punch);
    changes = changes + 1;
  END

  /* AUTO_RD_DFLT_CL_E_D_GROUPS_NBR */
  IF (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 598, OLD.auto_rd_dflt_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* AUTO_REDUCTION_DEFAULT_EES */
  IF (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 762, OLD.auto_reduction_default_ees);
    changes = changes + 1;
  END

  /* VT_HEALTHCARE_GL_TAG */
  IF (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 659, OLD.vt_healthcare_gl_tag);
    changes = changes + 1;
  END

  /* VT_HEALTHCARE_OFFSET_GL_TAG */
  IF (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 660, OLD.vt_healthcare_offset_gl_tag);
    changes = changes + 1;
  END

  /* UI_ROUNDING_GL_TAG */
  IF (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 661, OLD.ui_rounding_gl_tag);
    changes = changes + 1;
  END

  /* UI_ROUNDING_OFFSET_GL_TAG */
  IF (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 662, OLD.ui_rounding_offset_gl_tag);
    changes = changes + 1;
  END

  /* REPRINT_TO_BALANCE */
  IF (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 763, OLD.reprint_to_balance);
    changes = changes + 1;
  END

  /* SHOW_MANUAL_CHECKS_IN_ESS */
  IF (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 765, OLD.show_manual_checks_in_ess);
    changes = changes + 1;
  END

  /* PAYROLL_REQUIRES_MGR_APPROVAL */
  IF (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 766, OLD.payroll_requires_mgr_approval);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 599, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* WC_OFFS_BANK_ACCOUNT_NBR */
  IF (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 603, OLD.wc_offs_bank_account_nbr);
    changes = changes + 1;
  END

  /* QTR_LOCK_FOR_TAX_PMTS */
  IF (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 767, OLD.qtr_lock_for_tax_pmts);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_PAYROLL */
  IF (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 515, OLD.co_max_amount_for_payroll);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_TAX_IMPOUND */
  IF (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 516, OLD.co_max_amount_for_tax_impound);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_DD */
  IF (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 517, OLD.co_max_amount_for_dd);
    changes = changes + 1;
  END

  /* CO_PAYROLL_PROCESS_LIMITATIONS */
  IF (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 768, OLD.co_payroll_process_limitations);
    changes = changes + 1;
  END

  /* CO_ACH_PROCESS_LIMITATIONS */
  IF (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 769, OLD.co_ach_process_limitations);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 770, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 771, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 772, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 773, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 774, OLD.wc_impound);
    changes = changes + 1;
  END

  /* COBRA_CREDIT_GL_TAG */
  IF (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 663, OLD.cobra_credit_gl_tag);
    changes = changes + 1;
  END

  /* CO_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 775, OLD.co_exception_payment_type);
    changes = changes + 1;
  END

  /* LAST_TAX_RETURN */
  IF (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 776, OLD.last_tax_return);
    changes = changes + 1;
  END

  /* ENABLE_HR */
  IF (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 764, OLD.enable_hr);
    changes = changes + 1;
  END

  /* ENABLE_ESS */
  IF (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 777, OLD.enable_ess);
    changes = changes + 1;
  END

  /* DUNS_AND_BRADSTREET */
  IF (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 519, OLD.duns_and_bradstreet);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_REGISTER_NAME */
  IF (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 780, OLD.bank_account_register_name);
    changes = changes + 1;
  END

  /* ENABLE_BENEFITS */
  IF (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2883, OLD.enable_benefits);
    changes = changes + 1;
  END

  /* ENABLE_TIME_OFF */
  IF (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2884, OLD.enable_time_off);
    changes = changes + 1;
  END

  /* EMPLOYER_TYPE */
  IF (OLD.employer_type IS DISTINCT FROM NEW.employer_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2885, OLD.employer_type);
    changes = changes + 1;
  END

  /* WC_FISCAL_YEAR_BEGIN */
  IF (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2886, OLD.wc_fiscal_year_begin);
    changes = changes + 1;
  END

  /* SHOW_PAYSTUBS_ESS_DAYS */
  IF (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2888, OLD.show_paystubs_ess_days);
    changes = changes + 1;
  END

  /* CO_LOCATIONS_NBR */
  IF (OLD.co_locations_nbr IS DISTINCT FROM NEW.co_locations_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3169, OLD.co_locations_nbr);
    changes = changes + 1;
  END

  /* ANNUAL_FORM_TYPE */
  IF (OLD.annual_form_type IS DISTINCT FROM NEW.annual_form_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3200, OLD.annual_form_type);
    changes = changes + 1;
  END

  /* PRENOTE */
  IF (OLD.prenote IS DISTINCT FROM NEW.prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3201, OLD.prenote);
    changes = changes + 1;
  END

  /* ENABLE_DD */
  IF (OLD.enable_dd IS DISTINCT FROM NEW.enable_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3202, OLD.enable_dd);
    changes = changes + 1;
  END

  /* ESS_OR_EP */
  IF (OLD.ess_or_ep IS DISTINCT FROM NEW.ess_or_ep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3203, OLD.ess_or_ep);
    changes = changes + 1;
  END

  /* ACA_EDUCATION_ORG */
  IF (OLD.aca_education_org IS DISTINCT FROM NEW.aca_education_org) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3204, OLD.aca_education_org);
    changes = changes + 1;
  END

  /* ACA_STANDARD_HOURS */
  IF (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3205, OLD.aca_standard_hours);
    changes = changes + 1;
  END

  /* ACA_DEFAULT_STATUS */
  IF (OLD.aca_default_status IS DISTINCT FROM NEW.aca_default_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3206, OLD.aca_default_status);
    changes = changes + 1;
  END

  /* ACA_CL_CO_CONSOLIDATION_NBR */
  IF (OLD.aca_cl_co_consolidation_nbr IS DISTINCT FROM NEW.aca_cl_co_consolidation_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3207, OLD.aca_cl_co_consolidation_nbr);
    changes = changes + 1;
  END

  /* ACA_INITIAL_PERIOD_FROM */
  IF (OLD.aca_initial_period_from IS DISTINCT FROM NEW.aca_initial_period_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3208, OLD.aca_initial_period_from);
    changes = changes + 1;
  END

  /* ACA_INITIAL_PERIOD_TO */
  IF (OLD.aca_initial_period_to IS DISTINCT FROM NEW.aca_initial_period_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3209, OLD.aca_initial_period_to);
    changes = changes + 1;
  END

  /* ACA_STABILITY_PERIOD_FROM */
  IF (OLD.aca_stability_period_from IS DISTINCT FROM NEW.aca_stability_period_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3210, OLD.aca_stability_period_from);
    changes = changes + 1;
  END

  /* ACA_STABILITY_PERIOD_TO */
  IF (OLD.aca_stability_period_to IS DISTINCT FROM NEW.aca_stability_period_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3211, OLD.aca_stability_period_to);
    changes = changes + 1;
  END

  /* ACA_USE_AVG_HOURS_WORKED */
  IF (OLD.aca_use_avg_hours_worked IS DISTINCT FROM NEW.aca_use_avg_hours_worked) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3212, OLD.aca_use_avg_hours_worked);
    changes = changes + 1;
  END

  /* ACA_AVG_HOURS_WORKED_PERIOD */
  IF (OLD.aca_avg_hours_worked_period IS DISTINCT FROM NEW.aca_avg_hours_worked_period) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3213, OLD.aca_avg_hours_worked_period);
    changes = changes + 1;
  END

  /* ACA_ADD_EARN_CL_E_D_GROUPS_NBR */
  IF (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3227, OLD.aca_add_earn_cl_e_d_groups_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', NULL);
  rdb$set_context('USER_TRANSACTION', '@CHANGES', NULL);
END

^

CREATE TRIGGER T_BIU_CO_2 FOR CO Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CL_COMMON_PAYMASTER */
  IF ((NEW.cl_common_paymaster_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_common_paymaster_nbr IS DISTINCT FROM NEW.cl_common_paymaster_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_common_paymaster
    WHERE cl_common_paymaster_nbr = NEW.cl_common_paymaster_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_common_paymaster_nbr', NEW.cl_common_paymaster_nbr, 'cl_common_paymaster', NEW.effective_date);

    SELECT rec_version FROM cl_common_paymaster
    WHERE cl_common_paymaster_nbr = NEW.cl_common_paymaster_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_common_paymaster_nbr', NEW.cl_common_paymaster_nbr, 'cl_common_paymaster', NEW.effective_until - 1);
  END

  /* Check reference to CL_AGENCY */
  IF ((NEW.workers_comp_cl_agency_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.workers_comp_cl_agency_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'workers_comp_cl_agency_nbr', NEW.workers_comp_cl_agency_nbr, 'cl_agency', NEW.effective_date);

    SELECT rec_version FROM cl_agency
    WHERE cl_agency_nbr = NEW.workers_comp_cl_agency_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'workers_comp_cl_agency_nbr', NEW.workers_comp_cl_agency_nbr, 'cl_agency', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_delivery_group_nbr', NEW.cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_delivery_group_nbr', NEW.cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.annual_cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'annual_cl_delivery_group_nbr', NEW.annual_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'annual_cl_delivery_group_nbr', NEW.annual_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_TIMECLOCK_IMPORTS */
  IF ((NEW.cl_timeclock_imports_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_timeclock_imports
    WHERE cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_timeclock_imports_nbr', NEW.cl_timeclock_imports_nbr, 'cl_timeclock_imports', NEW.effective_date);

    SELECT rec_version FROM cl_timeclock_imports
    WHERE cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_timeclock_imports_nbr', NEW.cl_timeclock_imports_nbr, 'cl_timeclock_imports', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.quarter_cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'quarter_cl_delivery_group_nbr', NEW.quarter_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'quarter_cl_delivery_group_nbr', NEW.quarter_cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.agency_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.agency_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'agency_check_mb_group_nbr', NEW.agency_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.agency_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'agency_check_mb_group_nbr', NEW.agency_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_check_mb_group_nbr', NEW.pr_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_check_mb_group_nbr', NEW.pr_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_report_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_mb_group_nbr', NEW.pr_report_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_mb_group_nbr', NEW.pr_report_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_report_second_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_second_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_second_mb_group_nbr', NEW.pr_report_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_report_second_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'pr_report_second_mb_group_nbr', NEW.pr_report_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_return_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_mb_group_nbr', NEW.tax_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_mb_group_nbr', NEW.tax_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_ee_return_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_ee_return_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_ee_return_mb_group_nbr', NEW.tax_ee_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_ee_return_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_ee_return_mb_group_nbr', NEW.tax_ee_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_return_second_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_second_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_second_mb_group_nbr', NEW.tax_return_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_return_second_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_return_second_mb_group_nbr', NEW.tax_return_second_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_BILLING */
  IF ((NEW.cl_billing_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_billing
    WHERE cl_billing_nbr = NEW.cl_billing_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_billing_nbr', NEW.cl_billing_nbr, 'cl_billing', NEW.effective_date);

    SELECT rec_version FROM cl_billing
    WHERE cl_billing_nbr = NEW.cl_billing_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_billing_nbr', NEW.cl_billing_nbr, 'cl_billing', NEW.effective_until - 1);
  END

  /* Check reference to CO_WORKERS_COMP */
  IF ((NEW.co_workers_comp_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_workers_comp_nbr', NEW.co_workers_comp_nbr, 'co_workers_comp', NEW.effective_date);

    SELECT rec_version FROM co_workers_comp
    WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_workers_comp_nbr', NEW.co_workers_comp_nbr, 'co_workers_comp', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.manual_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'manual_cl_bank_account_nbr', NEW.manual_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'manual_cl_bank_account_nbr', NEW.manual_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.billing_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'billing_cl_bank_account_nbr', NEW.billing_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'billing_cl_bank_account_nbr', NEW.billing_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.tax_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_cl_bank_account_nbr', NEW.tax_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_cl_bank_account_nbr', NEW.tax_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.w_comp_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'w_comp_cl_bank_account_nbr', NEW.w_comp_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'w_comp_cl_bank_account_nbr', NEW.w_comp_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.payroll_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'payroll_cl_bank_account_nbr', NEW.payroll_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'payroll_cl_bank_account_nbr', NEW.payroll_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_BANK_ACCOUNT */
  IF ((NEW.dd_cl_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'dd_cl_bank_account_nbr', NEW.dd_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_date);

    SELECT rec_version FROM cl_bank_account
    WHERE cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'dd_cl_bank_account_nbr', NEW.dd_cl_bank_account_nbr, 'cl_bank_account', NEW.effective_until - 1);
  END

  /* Check reference to CL_CO_CONSOLIDATION */
  IF ((NEW.cl_co_consolidation_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_co_consolidation_nbr', NEW.cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_date);

    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'cl_co_consolidation_nbr', NEW.cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_DS */
  IF ((NEW.union_cl_e_ds_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.union_cl_e_ds_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'union_cl_e_ds_nbr', NEW.union_cl_e_ds_nbr, 'cl_e_ds', NEW.effective_date);

    SELECT rec_version FROM cl_e_ds
    WHERE cl_e_ds_nbr = NEW.union_cl_e_ds_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'union_cl_e_ds_nbr', NEW.union_cl_e_ds_nbr, 'cl_e_ds', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.auto_rd_dflt_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'auto_rd_dflt_cl_e_d_groups_nbr', NEW.auto_rd_dflt_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'auto_rd_dflt_cl_e_d_groups_nbr', NEW.auto_rd_dflt_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CO_STATES */
  IF ((NEW.home_co_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_co_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_co_states_nbr', NEW.home_co_states_nbr, 'co_states', NEW.effective_date);

    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_co_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_co_states_nbr', NEW.home_co_states_nbr, 'co_states', NEW.effective_until - 1);
  END

  /* Check reference to CO_STATES */
  IF ((NEW.home_sdi_co_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sdi_co_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sdi_co_states_nbr', NEW.home_sdi_co_states_nbr, 'co_states', NEW.effective_date);

    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sdi_co_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sdi_co_states_nbr', NEW.home_sdi_co_states_nbr, 'co_states', NEW.effective_until - 1);
  END

  /* Check reference to CO_STATES */
  IF ((NEW.home_sui_co_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sui_co_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sui_co_states_nbr', NEW.home_sui_co_states_nbr, 'co_states', NEW.effective_date);

    SELECT rec_version FROM co_states
    WHERE co_states_nbr = NEW.home_sui_co_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'home_sui_co_states_nbr', NEW.home_sui_co_states_nbr, 'co_states', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_check_mb_group_nbr', NEW.tax_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'tax_check_mb_group_nbr', NEW.tax_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CO_LOCATIONS */
  IF ((NEW.co_locations_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_locations_nbr IS DISTINCT FROM NEW.co_locations_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_locations
    WHERE co_locations_nbr = NEW.co_locations_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_locations_nbr', NEW.co_locations_nbr, 'co_locations', NEW.effective_date);

    SELECT rec_version FROM co_locations
    WHERE co_locations_nbr = NEW.co_locations_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'co_locations_nbr', NEW.co_locations_nbr, 'co_locations', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.aca_add_earn_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'aca_add_earn_cl_e_d_groups_nbr', NEW.aca_add_earn_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'aca_add_earn_cl_e_d_groups_nbr', NEW.aca_add_earn_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CL_CO_CONSOLIDATION */
  IF ((NEW.aca_cl_co_consolidation_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.aca_cl_co_consolidation_nbr IS DISTINCT FROM NEW.aca_cl_co_consolidation_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.aca_cl_co_consolidation_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'aca_cl_co_consolidation_nbr', NEW.aca_cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_date);

    SELECT rec_version FROM cl_co_consolidation
    WHERE cl_co_consolidation_nbr = NEW.aca_cl_co_consolidation_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co', NEW.co_nbr, 'aca_cl_co_consolidation_nbr', NEW.aca_cl_co_consolidation_nbr, 'cl_co_consolidation', NEW.effective_until - 1);
  END
END

^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  UPDATE ev_field SET name = 'ACA_CL_CO_CONSOLIDATION_NBR', field_type = 'I', len = NULL, scale = NULL, required = 'N', versioned = 'N' WHERE nbr = 3207;
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.1', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
