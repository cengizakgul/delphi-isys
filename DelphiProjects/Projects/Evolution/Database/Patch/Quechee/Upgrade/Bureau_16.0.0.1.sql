/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.0';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^







CREATE GENERATOR G_SB_USER_PREFERENCES
^

CREATE GENERATOR G_SB_USER_PREFERENCES_VER
^

CREATE TABLE  SB_USER_PREFERENCES
( 
     REC_VERSION EV_INT NOT NULL,
     SB_USER_PREFERENCES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SB_USER_NBR EV_INT NOT NULL,
     PREFERENCE_TYPE EV_STR40,
     PREFERENCE_VALUE EV_BLOB_BIN,
     LAST_MODIFIED_DATE EV_DATETIME,
        CONSTRAINT PK_SB_USER_PREFERENCES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_USER_PREFERENCES_1 UNIQUE (SB_USER_PREFERENCES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_USER_PREFERENCES_2 UNIQUE (SB_USER_PREFERENCES_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_USER_PREFERENCES_1 ON SB_USER_PREFERENCES (SB_USER_NBR)
^


GRANT ALL ON SB_USER_PREFERENCES TO EUSER
^


DROP TRIGGER T_AD_SB_9
^


DROP TRIGGER T_AU_SB_9
^


DROP TRIGGER T_BUD_SB_9
^

ALTER TABLE SB
ADD THEME EV_STR40 
^

ALTER TABLE SB
ALTER COLUMN THEME POSITION 60
^

ALTER TABLE SB
ADD THEME_VALUE EV_BLOB_BIN 
^

ALTER TABLE SB
ALTER COLUMN THEME_VALUE POSITION 61
^

ALTER TABLE SB
ADD WC_TERMS_OF_USE_MODIFY_DATE EV_DATETIME 
^

ALTER TABLE SB
ALTER COLUMN WC_TERMS_OF_USE_MODIFY_DATE POSITION 62
^

ALTER TABLE SB
ADD ANALYTICS_LICENSE EV_CHAR1  NOT NULL
^

ALTER TABLE SB
ALTER COLUMN ANALYTICS_LICENSE POSITION 63
^

COMMIT^

UPDATE SB SET ANALYTICS_LICENSE = 'N'
^


DROP TRIGGER T_AD_SB_ACCOUNTANT_9
^


DROP TRIGGER T_AU_SB_ACCOUNTANT_9
^

ALTER TABLE SB_ACCOUNTANT
ADD CREDIT_BANK_ACCOUNT_NBR EV_INT 
^

ALTER TABLE SB_ACCOUNTANT
ALTER COLUMN CREDIT_BANK_ACCOUNT_NBR POSITION 23
^

ALTER TABLE SB_ACCOUNTANT
ADD DEBIT_BANK_ACCOUNT_NBR EV_INT 
^

ALTER TABLE SB_ACCOUNTANT
ALTER COLUMN DEBIT_BANK_ACCOUNT_NBR POSITION 24
^

ALTER TABLE SB_ACCOUNTANT
ADD ACCOUNT_NUMBER EV_STR20 
^

ALTER TABLE SB_ACCOUNTANT
ALTER COLUMN ACCOUNT_NUMBER POSITION 25
^

ALTER TABLE SB_ACCOUNTANT
ADD BANK_ACCOUNT_TYPE EV_CHAR1  NOT NULL
^

ALTER TABLE SB_ACCOUNTANT
ALTER COLUMN BANK_ACCOUNT_TYPE POSITION 26
^

ALTER TABLE SB_ACCOUNTANT
ADD SB_BANKS_NBR EV_INT 
^

ALTER TABLE SB_ACCOUNTANT
ALTER COLUMN SB_BANKS_NBR POSITION 27
^

CREATE INDEX FK_SB_ACCOUNTANT_1 ON SB_ACCOUNTANT (CREDIT_BANK_ACCOUNT_NBR)
^

CREATE INDEX FK_SB_ACCOUNTANT_2 ON SB_ACCOUNTANT (DEBIT_BANK_ACCOUNT_NBR)
^

CREATE INDEX FK_SB_ACCOUNTANT_3 ON SB_ACCOUNTANT (SB_BANKS_NBR)
^


COMMIT^

UPDATE SB_ACCOUNTANT SET BANK_ACCOUNT_TYPE = 'C'
^


DROP TRIGGER T_AUD_SB_BANKS_2
^


DROP TRIGGER T_AD_SB_USER_9
^


DROP TRIGGER T_AUD_SB_USER_2
^


DROP TRIGGER T_AU_SB_USER_9
^

ALTER TABLE SB_USER
ADD WC_TOU_ACCEPT_DATE EV_DATETIME 
^

ALTER TABLE SB_USER
ALTER COLUMN WC_TOU_ACCEPT_DATE POSITION 33
^

ALTER TABLE SB_USER
ADD ANALYTICS_PERSONNEL EV_CHAR1  NOT NULL
^

ALTER TABLE SB_USER
ALTER COLUMN ANALYTICS_PERSONNEL POSITION 34
^

COMMIT^

UPDATE SB_USER SET ANALYTICS_PERSONNEL = 'N'
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check SB_ACCOUNTANT */
  child_table = 'SB_ACCOUNTANT';

  parent_table = 'SB_BANK_ACCOUNTS';
  child_field = 'CREDIT_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANK_ACCOUNTS';
  child_field = 'DEBIT_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY */
  child_table = 'SB_AGENCY';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY_REPORTS */
  child_table = 'SB_AGENCY_REPORTS';

  parent_table = 'SB_AGENCY';
  child_field = 'SB_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_BANK_ACCOUNTS */
  child_table = 'SB_BANK_ACCOUNTS';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'ACH_ORIGIN_SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'LOGO_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'SIGNATURE_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_COMPANY_SVCS */
  child_table = 'SB_DELIVERY_COMPANY_SVCS';

  parent_table = 'SB_DELIVERY_COMPANY';
  child_field = 'SB_DELIVERY_COMPANY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_SERVICE_OPT */
  child_table = 'SB_DELIVERY_SERVICE_OPT';

  parent_table = 'SB_DELIVERY_SERVICE';
  child_field = 'SB_DELIVERY_SERVICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX */
  child_table = 'SB_MAIL_BOX';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'UP_LEVEL_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORT_WRITER_REPORTS';
  child_field = 'SB_COVER_LETTER_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_CONTENT */
  child_table = 'SB_MAIL_BOX_CONTENT';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_OPTION */
  child_table = 'SB_MAIL_BOX_OPTION';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MAIL_BOX_CONTENT';
  child_field = 'SB_MAIL_BOX_CONTENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MEDIA_TYPE_OPTION */
  child_table = 'SB_MEDIA_TYPE_OPTION';

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_QUEUE_PRIORITY */
  child_table = 'SB_QUEUE_PRIORITY';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_CLIENTS */
  child_table = 'SB_SEC_CLIENTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_GROUP_MEMBERS */
  child_table = 'SB_SEC_GROUP_MEMBERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_RIGHTS */
  child_table = 'SB_SEC_RIGHTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_ROW_FILTERS */
  child_table = 'SB_SEC_ROW_FILTERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES */
  child_table = 'SB_SERVICES';

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES_CALCULATIONS */
  child_table = 'SB_SERVICES_CALCULATIONS';

  parent_table = 'SB_SERVICES';
  child_field = 'SB_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TASK */
  child_table = 'SB_TASK';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TEAM_MEMBERS */
  child_table = 'SB_TEAM_MEMBERS';

  parent_table = 'SB_TEAM';
  child_field = 'SB_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER */
  child_table = 'SB_USER';

  parent_table = 'SB_ACCOUNTANT';
  child_field = 'SB_ACCOUNTANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_NOTICE */
  child_table = 'SB_USER_NOTICE';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_PREFERENCES */
  child_table = 'SB_USER_PREFERENCES';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_user_preferences(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_user_preferences WHERE sb_user_preferences_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_USER_PREFERENCES TO EUSER
^

ALTER TABLE SB_USER_PREFERENCES
ADD CONSTRAINT C_SB_USER_PREFERENCES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_SB_9 FOR SB After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, OLD.rec_version, OLD.sb_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 8, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 452, OLD.effective_until);

  /* SB_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 28, OLD.sb_name);

  /* ADDRESS1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 5, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 6, OLD.address2);

  /* CITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 29, OLD.city);

  /* STATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 30, OLD.state);

  /* ZIP_CODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 20, OLD.zip_code);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 31, OLD.e_mail_address);

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);

  /* DEVELOPMENT_MODEM_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 33, OLD.development_modem_number);

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 35, OLD.development_ftp_password);

  /* EIN_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 49, OLD.ein_number);

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);

  /* EFTPS_BANK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);

  /* USE_PRENOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 53, OLD.use_prenote);

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);

  /* PAY_TAX_FROM_PAYABLES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);

  /* AR_EXPORT_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 57, OLD.ar_export_format);

  /* DEFAULT_CHECK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 58, OLD.default_check_format);

  /* MICR_FONT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 59, OLD.micr_font);

  /* MICR_HORIZONTAL_ADJUSTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);

  /* AUTO_SAVE_MINUTES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);

  /* PHONE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 36, OLD.phone);

  /* FAX */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 37, OLD.fax);

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 7, :blob_nbr);
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 4, :blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3, :blob_nbr);
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 27, OLD.ar_import_directory);

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 26, OLD.ach_directory);

  /* SB_URL */
  IF (OLD.sb_url IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 25, OLD.sb_url);

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 18, OLD.days_in_prenote);

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 9, :blob_nbr);
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);

  /* ERROR_SCREEN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 48, OLD.error_screen);

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 15, OLD.pswd_min_length);

  /* PSWD_FORCE_MIXED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);

  /* MISC_CHECK_FORM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 46, OLD.misc_check_form);

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2, :blob_nbr);
  END

  /* MARK_LIABS_PAID_DEFAULT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);

  /* TRUST_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 44, OLD.trust_impound);

  /* TAX_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 43, OLD.tax_impound);

  /* DD_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 42, OLD.dd_impound);

  /* BILLING_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 40, OLD.billing_impound);

  /* WC_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 39, OLD.wc_impound);

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);

  /* SB_EXCEPTION_PAYMENT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);

  /* SB_ACH_FILE_LIMITATIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 543, OLD.dashboard_msg);

  /* EE_LOGIN_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 544, OLD.ee_login_type);

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 550, :blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 551, :blob_nbr);
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 569, :blob_nbr);
  END

  /* THEME */
  IF (OLD.theme IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 571, OLD.theme);

  /* THEME_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@THEME_VALUE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 572, :blob_nbr);
  END

  /* WC_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.wc_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 573, OLD.wc_terms_of_use_modify_date);

  /* ANALYTICS_LICENSE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 574, OLD.analytics_license);

END

^

CREATE TRIGGER T_AU_SB_9 FOR SB After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, NEW.rec_version, NEW.sb_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 8, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 452, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_NAME */
  IF (OLD.sb_name IS DISTINCT FROM NEW.sb_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 28, OLD.sb_name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 5, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 6, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 29, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 30, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 20, OLD.zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 31, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS DISTINCT FROM NEW.parent_sb_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_MODEM_NUMBER */
  IF (OLD.development_modem_number IS DISTINCT FROM NEW.development_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 33, OLD.development_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS DISTINCT FROM NEW.development_ftp_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 35, OLD.development_ftp_password);
    changes = changes + 1;
  END

  /* EIN_NUMBER */
  IF (OLD.ein_number IS DISTINCT FROM NEW.ein_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 49, OLD.ein_number);
    changes = changes + 1;
  END

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS DISTINCT FROM NEW.eftps_tin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);
    changes = changes + 1;
  END

  /* EFTPS_BANK_FORMAT */
  IF (OLD.eftps_bank_format IS DISTINCT FROM NEW.eftps_bank_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);
    changes = changes + 1;
  END

  /* USE_PRENOTE */
  IF (OLD.use_prenote IS DISTINCT FROM NEW.use_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 53, OLD.use_prenote);
    changes = changes + 1;
  END

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  IF (OLD.impound_trust_monies_as_receiv IS DISTINCT FROM NEW.impound_trust_monies_as_receiv) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);
    changes = changes + 1;
  END

  /* PAY_TAX_FROM_PAYABLES */
  IF (OLD.pay_tax_from_payables IS DISTINCT FROM NEW.pay_tax_from_payables) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);
    changes = changes + 1;
  END

  /* AR_EXPORT_FORMAT */
  IF (OLD.ar_export_format IS DISTINCT FROM NEW.ar_export_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 57, OLD.ar_export_format);
    changes = changes + 1;
  END

  /* DEFAULT_CHECK_FORMAT */
  IF (OLD.default_check_format IS DISTINCT FROM NEW.default_check_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 58, OLD.default_check_format);
    changes = changes + 1;
  END

  /* MICR_FONT */
  IF (OLD.micr_font IS DISTINCT FROM NEW.micr_font) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 59, OLD.micr_font);
    changes = changes + 1;
  END

  /* MICR_HORIZONTAL_ADJUSTMENT */
  IF (OLD.micr_horizontal_adjustment IS DISTINCT FROM NEW.micr_horizontal_adjustment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);
    changes = changes + 1;
  END

  /* AUTO_SAVE_MINUTES */
  IF (OLD.auto_save_minutes IS DISTINCT FROM NEW.auto_save_minutes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);
    changes = changes + 1;
  END

  /* PHONE */
  IF (OLD.phone IS DISTINCT FROM NEW.phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 36, OLD.phone);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 37, OLD.fax);
    changes = changes + 1;
  END

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 7, :blob_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 4, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3, :blob_nbr);
    changes = changes + 1;
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS DISTINCT FROM NEW.ar_import_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 27, OLD.ar_import_directory);
    changes = changes + 1;
  END

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS DISTINCT FROM NEW.ach_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 26, OLD.ach_directory);
    changes = changes + 1;
  END

  /* SB_URL */
  IF (OLD.sb_url IS DISTINCT FROM NEW.sb_url) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 25, OLD.sb_url);
    changes = changes + 1;
  END

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS DISTINCT FROM NEW.days_in_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 18, OLD.days_in_prenote);
    changes = changes + 1;
  END

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 9, :blob_nbr);
    changes = changes + 1;
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS DISTINCT FROM NEW.user_password_duration_in_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);
    changes = changes + 1;
  END

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS DISTINCT FROM NEW.dummy_tax_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);
    changes = changes + 1;
  END

  /* ERROR_SCREEN */
  IF (OLD.error_screen IS DISTINCT FROM NEW.error_screen) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 48, OLD.error_screen);
    changes = changes + 1;
  END

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS DISTINCT FROM NEW.pswd_min_length) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 15, OLD.pswd_min_length);
    changes = changes + 1;
  END

  /* PSWD_FORCE_MIXED */
  IF (OLD.pswd_force_mixed IS DISTINCT FROM NEW.pswd_force_mixed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 46, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2, :blob_nbr);
    changes = changes + 1;
  END

  /* MARK_LIABS_PAID_DEFAULT */
  IF (OLD.mark_liabs_paid_default IS DISTINCT FROM NEW.mark_liabs_paid_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 44, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 43, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 42, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 40, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 39, OLD.wc_impound);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* SB_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.sb_exception_payment_type IS DISTINCT FROM NEW.sb_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);
    changes = changes + 1;
  END

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS DISTINCT FROM NEW.sb_max_ach_file_total) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);
    changes = changes + 1;
  END

  /* SB_ACH_FILE_LIMITATIONS */
  IF (OLD.sb_ach_file_limitations IS DISTINCT FROM NEW.sb_ach_file_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);
    changes = changes + 1;
  END

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS DISTINCT FROM NEW.sb_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS DISTINCT FROM NEW.dashboard_msg) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.dashboard_msg);
    changes = changes + 1;
  END

  /* EE_LOGIN_TYPE */
  IF (OLD.ee_login_type IS DISTINCT FROM NEW.ee_login_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.ee_login_type);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, :blob_nbr);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS DISTINCT FROM NEW.ess_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 569, :blob_nbr);
    changes = changes + 1;
  END

  /* THEME */
  IF (OLD.theme IS DISTINCT FROM NEW.theme) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 571, OLD.theme);
    changes = changes + 1;
  END

  /* THEME_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@THEME_VALUE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 572, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.wc_terms_of_use_modify_date IS DISTINCT FROM NEW.wc_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 573, OLD.wc_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ANALYTICS_LICENSE */
  IF (OLD.analytics_license IS DISTINCT FROM NEW.analytics_license) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 574, OLD.analytics_license);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BUD_SB_9 FOR SB Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* COVER_LETTER_NOTES */
  IF (DELETING OR (OLD.cover_letter_notes IS DISTINCT FROM NEW.cover_letter_notes)) THEN
  BEGIN
    IF (OLD.cover_letter_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.cover_letter_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', blob_nbr);
  END

  /* INVOICE_NOTES */
  IF (DELETING OR (OLD.invoice_notes IS DISTINCT FROM NEW.invoice_notes)) THEN
  BEGIN
    IF (OLD.invoice_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.invoice_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  IF (DELETING OR (OLD.tax_cover_letter_notes IS DISTINCT FROM NEW.tax_cover_letter_notes)) THEN
  BEGIN
    IF (OLD.tax_cover_letter_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.tax_cover_letter_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', blob_nbr);
  END

  /* SB_LOGO */
  IF (DELETING OR (OLD.sb_logo IS DISTINCT FROM NEW.sb_logo)) THEN
  BEGIN
    IF (OLD.sb_logo IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.sb_logo)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@SB_LOGO', blob_nbr);
  END

  /* VMR_CONFIDENCIAL_NOTES */
  IF (DELETING OR (OLD.vmr_confidencial_notes IS DISTINCT FROM NEW.vmr_confidencial_notes)) THEN
  BEGIN
    IF (OLD.vmr_confidencial_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.vmr_confidencial_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', blob_nbr);
  END

  /* ESS_TERMS_OF_USE */
  IF (DELETING OR (OLD.ess_terms_of_use IS DISTINCT FROM NEW.ess_terms_of_use)) THEN
  BEGIN
    IF (OLD.ess_terms_of_use IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.ess_terms_of_use)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  IF (DELETING OR (OLD.wc_terms_of_use IS DISTINCT FROM NEW.wc_terms_of_use)) THEN
  BEGIN
    IF (OLD.wc_terms_of_use IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.wc_terms_of_use)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', blob_nbr);
  END

  /* ESS_LOGO */
  IF (DELETING OR (OLD.ess_logo IS DISTINCT FROM NEW.ess_logo)) THEN
  BEGIN
    IF (OLD.ess_logo IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.ess_logo)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', blob_nbr);
  END

  /* THEME_VALUE */
  IF (DELETING OR (OLD.theme_value IS DISTINCT FROM NEW.theme_value)) THEN
  BEGIN
    IF (OLD.theme_value IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.theme_value)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', blob_nbr);
  END
END

^

CREATE TRIGGER T_AD_SB_ACCOUNTANT_9 FOR SB_ACCOUNTANT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(2, OLD.rec_version, OLD.sb_accountant_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 66, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 454, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 72, OLD.name);

  /* ADDRESS1 */
  IF (OLD.address1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 62, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 63, OLD.address2);

  /* CITY */
  IF (OLD.city IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 73, OLD.city);

  /* STATE */
  IF (OLD.state IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 79, OLD.state);

  /* ZIP_CODE */
  IF (OLD.zip_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 68, OLD.zip_code);

  /* CONTACT1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 64, OLD.contact1);

  /* PHONE1 */
  IF (OLD.phone1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 74, OLD.phone1);

  /* DESCRIPTION1 */
  IF (OLD.description1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 69, OLD.description1);

  /* CONTACT2 */
  IF (OLD.contact2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 65, OLD.contact2);

  /* PHONE2 */
  IF (OLD.phone2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 75, OLD.phone2);

  /* DESCRIPTION2 */
  IF (OLD.description2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 70, OLD.description2);

  /* FAX */
  IF (OLD.fax IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 76, OLD.fax);

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 71, OLD.fax_description);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 77, OLD.e_mail_address);

  /* PRINT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 78, OLD.print_name);

  /* TITLE */
  IF (OLD.title IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 61, OLD.title);

  /* SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SIGNATURE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 60, :blob_nbr);
  END

  /* CREDIT_BANK_ACCOUNT_NBR */
  IF (OLD.credit_bank_account_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 585, OLD.credit_bank_account_nbr);

  /* DEBIT_BANK_ACCOUNT_NBR */
  IF (OLD.debit_bank_account_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 586, OLD.debit_bank_account_nbr);

  /* ACCOUNT_NUMBER */
  IF (OLD.account_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 587, OLD.account_number);

  /* BANK_ACCOUNT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 588, OLD.bank_account_type);

  /* SB_BANKS_NBR */
  IF (OLD.sb_banks_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 589, OLD.sb_banks_nbr);

END

^

CREATE TRIGGER T_AU_SB_ACCOUNTANT_9 FOR SB_ACCOUNTANT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(2, NEW.rec_version, NEW.sb_accountant_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 66, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 454, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 72, OLD.name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 62, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 63, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 73, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 79, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 68, OLD.zip_code);
    changes = changes + 1;
  END

  /* CONTACT1 */
  IF (OLD.contact1 IS DISTINCT FROM NEW.contact1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 64, OLD.contact1);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 74, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 69, OLD.description1);
    changes = changes + 1;
  END

  /* CONTACT2 */
  IF (OLD.contact2 IS DISTINCT FROM NEW.contact2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 65, OLD.contact2);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 75, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 70, OLD.description2);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 76, OLD.fax);
    changes = changes + 1;
  END

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS DISTINCT FROM NEW.fax_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 71, OLD.fax_description);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 77, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PRINT_NAME */
  IF (OLD.print_name IS DISTINCT FROM NEW.print_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 78, OLD.print_name);
    changes = changes + 1;
  END

  /* TITLE */
  IF (OLD.title IS DISTINCT FROM NEW.title) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 61, OLD.title);
    changes = changes + 1;
  END

  /* SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SIGNATURE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 60, :blob_nbr);
    changes = changes + 1;
  END

  /* CREDIT_BANK_ACCOUNT_NBR */
  IF (OLD.credit_bank_account_nbr IS DISTINCT FROM NEW.credit_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 585, OLD.credit_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_BANK_ACCOUNT_NBR */
  IF (OLD.debit_bank_account_nbr IS DISTINCT FROM NEW.debit_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 586, OLD.debit_bank_account_nbr);
    changes = changes + 1;
  END

  /* ACCOUNT_NUMBER */
  IF (OLD.account_number IS DISTINCT FROM NEW.account_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 587, OLD.account_number);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_TYPE */
  IF (OLD.bank_account_type IS DISTINCT FROM NEW.bank_account_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 588, OLD.bank_account_type);
    changes = changes + 1;
  END

  /* SB_BANKS_NBR */
  IF (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 589, OLD.sb_banks_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ACCOUNTANT_2 FOR SB_ACCOUNTANT Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_BANK_ACCOUNTS */
  IF ((NEW.credit_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.credit_bank_account_nbr IS DISTINCT FROM NEW.credit_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE sb_bank_accounts_nbr = NEW.credit_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_accountant', NEW.sb_accountant_nbr, 'credit_bank_account_nbr', NEW.credit_bank_account_nbr, 'sb_bank_accounts', NEW.effective_date);

    SELECT rec_version FROM sb_bank_accounts
    WHERE sb_bank_accounts_nbr = NEW.credit_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_accountant', NEW.sb_accountant_nbr, 'credit_bank_account_nbr', NEW.credit_bank_account_nbr, 'sb_bank_accounts', NEW.effective_until - 1);
  END

  /* Check reference to SB_BANK_ACCOUNTS */
  IF ((NEW.debit_bank_account_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.debit_bank_account_nbr IS DISTINCT FROM NEW.debit_bank_account_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE sb_bank_accounts_nbr = NEW.debit_bank_account_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_accountant', NEW.sb_accountant_nbr, 'debit_bank_account_nbr', NEW.debit_bank_account_nbr, 'sb_bank_accounts', NEW.effective_date);

    SELECT rec_version FROM sb_bank_accounts
    WHERE sb_bank_accounts_nbr = NEW.debit_bank_account_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_accountant', NEW.sb_accountant_nbr, 'debit_bank_account_nbr', NEW.debit_bank_account_nbr, 'sb_bank_accounts', NEW.effective_until - 1);
  END

  /* Check reference to SB_BANKS */
  IF ((NEW.sb_banks_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.sb_banks_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_accountant', NEW.sb_accountant_nbr, 'sb_banks_nbr', NEW.sb_banks_nbr, 'sb_banks', NEW.effective_date);

    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.sb_banks_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_accountant', NEW.sb_accountant_nbr, 'sb_banks_nbr', NEW.sb_banks_nbr, 'sb_banks', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AUD_SB_BANKS_2 FOR SB_BANKS After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_banks WHERE sb_banks_nbr = OLD.sb_banks_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_banks WHERE sb_banks_nbr = OLD.sb_banks_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_AGENCY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_agency', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_agency', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_BANK_ACCOUNTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_BANK_ACCOUNTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (ach_origin_sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (ach_origin_sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_ACCOUNTANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_accountant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_accountant', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AUD_SB_BANK_ACCOUNTS_2 FOR SB_BANK_ACCOUNTS After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_bank_accounts WHERE sb_bank_accounts_nbr = OLD.sb_bank_accounts_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_bank_accounts WHERE sb_bank_accounts_nbr = OLD.sb_bank_accounts_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_ACCOUNTANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE (credit_bank_account_nbr = OLD.sb_bank_accounts_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_bank_accounts', OLD.sb_bank_accounts_nbr, 'sb_accountant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE (credit_bank_account_nbr = OLD.sb_bank_accounts_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_bank_accounts', OLD.sb_bank_accounts_nbr, 'sb_accountant', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_ACCOUNTANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE (debit_bank_account_nbr = OLD.sb_bank_accounts_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_bank_accounts', OLD.sb_bank_accounts_nbr, 'sb_accountant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE (debit_bank_account_nbr = OLD.sb_bank_accounts_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_bank_accounts', OLD.sb_bank_accounts_nbr, 'sb_accountant', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_SB_USER_9 FOR SB_USER After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, OLD.rec_version, OLD.sb_user_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 422, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 536, OLD.effective_until);

  /* USER_ID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 435, OLD.user_id);

  /* USER_SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 419, :blob_nbr);
  END

  /* LAST_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 420, OLD.last_name);

  /* FIRST_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 436, OLD.first_name);

  /* MIDDLE_INITIAL */
  IF (OLD.middle_initial IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 440, OLD.middle_initial);

  /* ACTIVE_USER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 441, OLD.active_user);

  /* DEPARTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 442, OLD.department);

  /* PASSWORD_CHANGE_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 421, OLD.password_change_date);

  /* SECURITY_LEVEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 443, OLD.security_level);

  /* USER_UPDATE_OPTIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 437, OLD.user_update_options);

  /* USER_FUNCTIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 438, OLD.user_functions);

  /* USER_PASSWORD */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 439, OLD.user_password);

  /* EMAIL_ADDRESS */
  IF (OLD.email_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 434, OLD.email_address);

  /* CL_NBR */
  IF (OLD.cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 429, OLD.cl_nbr);

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 427, OLD.sb_accountant_nbr);

  /* WRONG_PSWD_ATTEMPTS */
  IF (OLD.wrong_pswd_attempts IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 426, OLD.wrong_pswd_attempts);

  /* LINKS_DATA */
  IF (OLD.links_data IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 433, OLD.links_data);

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 425, OLD.login_question1);

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 432, OLD.login_answer1);

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 424, OLD.login_question2);

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 431, OLD.login_answer2);

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 423, OLD.login_question3);

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 430, OLD.login_answer3);

  /* HR_PERSONNEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 545, OLD.hr_personnel);

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 546, OLD.sec_question1);

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 547, OLD.sec_answer1);

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 548, OLD.sec_question2);

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 549, OLD.sec_answer2);

  /* WC_TOU_ACCEPT_DATE */
  IF (OLD.wc_tou_accept_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 575, OLD.wc_tou_accept_date);

  /* ANALYTICS_PERSONNEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 576, OLD.analytics_personnel);

END

^

CREATE TRIGGER T_AUD_SB_USER_2 FOR SB_USER After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_user WHERE sb_user_nbr = OLD.sb_user_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_user WHERE sb_user_nbr = OLD.sb_user_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_QUEUE_PRIORITY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_queue_priority
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_queue_priority', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_queue_priority
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_queue_priority', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_CLIENTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_clients
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_clients', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_clients
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_clients', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_GROUP_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_group_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_group_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_group_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_group_members', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_RIGHTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_rights
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_rights', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_rights
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_rights', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_ROW_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_row_filters
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_row_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_row_filters
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_row_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_TEAM_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_team_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_team_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_team_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_team_members', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_TASK */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_task
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_task', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_task
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_task', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_USER_NOTICE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user_notice
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_user_notice', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user_notice
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_user_notice', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_USER_PREFERENCES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user_preferences
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_user_preferences', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user_preferences
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_user_preferences', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_SB_USER_9 FOR SB_USER After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, NEW.rec_version, NEW.sb_user_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 422, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 536, OLD.effective_until);
    changes = changes + 1;
  END

  /* USER_ID */
  IF (OLD.user_id IS DISTINCT FROM NEW.user_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 435, OLD.user_id);
    changes = changes + 1;
  END

  /* USER_SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 419, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_NAME */
  IF (OLD.last_name IS DISTINCT FROM NEW.last_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 420, OLD.last_name);
    changes = changes + 1;
  END

  /* FIRST_NAME */
  IF (OLD.first_name IS DISTINCT FROM NEW.first_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 436, OLD.first_name);
    changes = changes + 1;
  END

  /* MIDDLE_INITIAL */
  IF (OLD.middle_initial IS DISTINCT FROM NEW.middle_initial) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 440, OLD.middle_initial);
    changes = changes + 1;
  END

  /* ACTIVE_USER */
  IF (OLD.active_user IS DISTINCT FROM NEW.active_user) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 441, OLD.active_user);
    changes = changes + 1;
  END

  /* DEPARTMENT */
  IF (OLD.department IS DISTINCT FROM NEW.department) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 442, OLD.department);
    changes = changes + 1;
  END

  /* PASSWORD_CHANGE_DATE */
  IF (OLD.password_change_date IS DISTINCT FROM NEW.password_change_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 421, OLD.password_change_date);
    changes = changes + 1;
  END

  /* SECURITY_LEVEL */
  IF (OLD.security_level IS DISTINCT FROM NEW.security_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 443, OLD.security_level);
    changes = changes + 1;
  END

  /* USER_UPDATE_OPTIONS */
  IF (OLD.user_update_options IS DISTINCT FROM NEW.user_update_options) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 437, OLD.user_update_options);
    changes = changes + 1;
  END

  /* USER_FUNCTIONS */
  IF (OLD.user_functions IS DISTINCT FROM NEW.user_functions) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 438, OLD.user_functions);
    changes = changes + 1;
  END

  /* USER_PASSWORD */
  IF (OLD.user_password IS DISTINCT FROM NEW.user_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 439, OLD.user_password);
    changes = changes + 1;
  END

  /* EMAIL_ADDRESS */
  IF (OLD.email_address IS DISTINCT FROM NEW.email_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 434, OLD.email_address);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 429, OLD.cl_nbr);
    changes = changes + 1;
  END

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 427, OLD.sb_accountant_nbr);
    changes = changes + 1;
  END

  /* WRONG_PSWD_ATTEMPTS */
  IF (OLD.wrong_pswd_attempts IS DISTINCT FROM NEW.wrong_pswd_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 426, OLD.wrong_pswd_attempts);
    changes = changes + 1;
  END

  /* LINKS_DATA */
  IF (OLD.links_data IS DISTINCT FROM NEW.links_data) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 433, OLD.links_data);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 425, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 432, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 424, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 431, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 423, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 430, OLD.login_answer3);
    changes = changes + 1;
  END

  /* HR_PERSONNEL */
  IF (OLD.hr_personnel IS DISTINCT FROM NEW.hr_personnel) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 545, OLD.hr_personnel);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 546, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 547, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 548, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 549, OLD.sec_answer2);
    changes = changes + 1;
  END

  /* WC_TOU_ACCEPT_DATE */
  IF (OLD.wc_tou_accept_date IS DISTINCT FROM NEW.wc_tou_accept_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 575, OLD.wc_tou_accept_date);
    changes = changes + 1;
  END

  /* ANALYTICS_PERSONNEL */
  IF (OLD.analytics_personnel IS DISTINCT FROM NEW.analytics_personnel) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 576, OLD.analytics_personnel);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_SB_USER_PREFERENCES_9 FOR SB_USER_PREFERENCES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(48, OLD.rec_version, OLD.sb_user_preferences_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 579, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 580, OLD.effective_until);

  /* SB_USER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 581, OLD.sb_user_nbr);

  /* PREFERENCE_TYPE */
  IF (OLD.preference_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 582, OLD.preference_type);

  /* PREFERENCE_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@PREFERENCE_VALUE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@PREFERENCE_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 583, :blob_nbr);
  END

  /* LAST_MODIFIED_DATE */
  IF (OLD.last_modified_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 584, OLD.last_modified_date);

END

^

CREATE TRIGGER T_AI_SB_USER_PREFERENCES_9 FOR SB_USER_PREFERENCES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(48, NEW.rec_version, NEW.sb_user_preferences_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_USER_PREFERENCES_9 FOR SB_USER_PREFERENCES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(48, NEW.rec_version, NEW.sb_user_preferences_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 579, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 580, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 581, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* PREFERENCE_TYPE */
  IF (OLD.preference_type IS DISTINCT FROM NEW.preference_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 582, OLD.preference_type);
    changes = changes + 1;
  END

  /* PREFERENCE_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@PREFERENCE_VALUE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@PREFERENCE_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 583, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_MODIFIED_DATE */
  IF (OLD.last_modified_date IS DISTINCT FROM NEW.last_modified_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 584, OLD.last_modified_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_USER_PREFERENCES_2 FOR SB_USER_PREFERENCES Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_user_preferences', NEW.sb_user_preferences_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_user_preferences', NEW.sb_user_preferences_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_SB_USER_PREFERENCES_1 FOR SB_USER_PREFERENCES Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_user_preferences_ver;

  IF (EXISTS (SELECT 1 FROM sb_user_preferences WHERE sb_user_preferences_nbr = NEW.sb_user_preferences_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_user_preferences', NEW.sb_user_preferences_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BUD_SB_USER_PREFERENCES_9 FOR SB_USER_PREFERENCES Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* PREFERENCE_VALUE */
  IF (DELETING OR (OLD.preference_value IS DISTINCT FROM NEW.preference_value)) THEN
  BEGIN
    IF (OLD.preference_value IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.preference_value)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@PREFERENCE_VALUE', blob_nbr);
  END
END

^

CREATE TRIGGER T_BU_SB_USER_PREFERENCES_1 FOR SB_USER_PREFERENCES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_user_preferences_nbr IS DISTINCT FROM OLD.sb_user_preferences_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_user_preferences', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^






COMMIT^






/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_table (nbr, name, versioned) VALUES (48, 'SB_USER_PREFERENCES', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 571, 'THEME', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 572, 'THEME_VALUE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 573, 'WC_TERMS_OF_USE_MODIFY_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 574, 'ANALYTICS_LICENSE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 585, 'CREDIT_BANK_ACCOUNT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 586, 'DEBIT_BANK_ACCOUNT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 587, 'ACCOUNT_NUMBER', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 588, 'BANK_ACCOUNT_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 589, 'SB_BANKS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 575, 'WC_TOU_ACCEPT_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 576, 'ANALYTICS_PERSONNEL', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 577, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 578, 'SB_USER_PREFERENCES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 579, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 580, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 581, 'SB_USER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 582, 'PREFERENCE_TYPE', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 583, 'PREFERENCE_VALUE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (48, 584, 'LAST_MODIFIED_DATE', 'T', NULL, NULL, 'N', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^








/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.1', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
	