 /* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.1';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^





CREATE GENERATOR G_CO_ACA_CERT_ELIGIBILITY
^

CREATE GENERATOR G_CO_ACA_CERT_ELIGIBILITY_VER
^

CREATE GENERATOR G_CO_BENEFIT_AGE_BANDS
^

CREATE GENERATOR G_CO_BENEFIT_AGE_BANDS_VER
^

CREATE GENERATOR G_CO_DW_STORAGE
^

CREATE GENERATOR G_CO_DW_STORAGE_VER
^

CREATE GENERATOR G_CO_CUSTOM_LABELS
^

CREATE GENERATOR G_CO_CUSTOM_LABELS_VER
^

CREATE GENERATOR G_CO_DOCUMENTS
^

CREATE GENERATOR G_CO_DOCUMENTS_VER
^


DROP TRIGGER T_AD_CO_9
^


DROP TRIGGER T_AIU_CO_3
^


DROP TRIGGER T_AUD_CO_2
^

DROP TRIGGER T_AU_CO_8
^

DROP TRIGGER T_AU_CO_9
^

ALTER TABLE CO
ADD EE_PRINT_VOUCHER_DEFAULT EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN EE_PRINT_VOUCHER_DEFAULT POSITION 299
^

ALTER TABLE CO
ADD ENABLE_ANALYTICS EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ENABLE_ANALYTICS POSITION 300
^

ALTER TABLE CO
ADD ANALYTICS_LICENSE EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN ANALYTICS_LICENSE POSITION 301
^

ALTER TABLE CO
ADD BENEFITS_ELIGIBLE_DEFAULT EV_CHAR1  NOT NULL
^

ALTER TABLE CO
ALTER COLUMN BENEFITS_ELIGIBLE_DEFAULT POSITION 302
^

COMMIT
^

UPDATE CO SET BENEFITS_ELIGIBLE_DEFAULT = 'N', ANALYTICS_LICENSE = 'N', ENABLE_ANALYTICS = 'N', EE_PRINT_VOUCHER_DEFAULT = 'Y'
^

DROP TRIGGER T_AD_CO_ADDITIONAL_INFO_NAME_9
^


DROP TRIGGER T_AU_CO_ADDITIONAL_INFO_NAME_9
^

ALTER TABLE CO_ADDITIONAL_INFO_NAMES
ADD REQUIRED EV_CHAR1  NOT NULL
^

ALTER TABLE CO_ADDITIONAL_INFO_NAMES
ALTER COLUMN REQUIRED POSITION 10
^

COMMIT
^

UPDATE CO_ADDITIONAL_INFO_NAMES SET REQUIRED = 'N'
^

DROP TRIGGER T_AD_CO_BENEFIT_SUBTYPE_9
^


DROP TRIGGER T_AUD_CO_BENEFIT_SUBTYPE_2
^


DROP TRIGGER T_AU_CO_BENEFIT_SUBTYPE_9
^

ALTER TABLE CO_BENEFIT_SUBTYPE
ADD AGE_BANDS EV_CHAR1  NOT NULL
^

ALTER TABLE CO_BENEFIT_SUBTYPE
ALTER COLUMN AGE_BANDS POSITION 7
^

ALTER TABLE CO_BENEFIT_SUBTYPE
ADD ACA_LOWEST_COST EV_CHAR1  NOT NULL
^

ALTER TABLE CO_BENEFIT_SUBTYPE
ALTER COLUMN ACA_LOWEST_COST POSITION 8
^

COMMIT
^

UPDATE CO_BENEFIT_SUBTYPE SET ACA_LOWEST_COST = 'N', AGE_BANDS = 'N'
^

DROP TRIGGER T_AUD_CO_BRANCH_2
^


DROP TRIGGER T_AUD_CO_DEPARTMENT_2
^


DROP TRIGGER T_AUD_CO_DIVISION_2
^


DROP TRIGGER T_AD_CO_HR_SUPERVISORS_9
^


DROP TRIGGER T_AU_CO_HR_SUPERVISORS_9
^


DROP TRIGGER T_BIU_CO_HR_SUPERVISORS_2
^

ALTER TABLE CO_HR_SUPERVISORS
ADD CO_DIVISION_NBR EV_INT 
^

ALTER TABLE CO_HR_SUPERVISORS
ALTER COLUMN CO_DIVISION_NBR POSITION 7
^

ALTER TABLE CO_HR_SUPERVISORS
ADD CO_BRANCH_NBR EV_INT 
^

ALTER TABLE CO_HR_SUPERVISORS
ALTER COLUMN CO_BRANCH_NBR POSITION 8
^

ALTER TABLE CO_HR_SUPERVISORS
ADD CO_DEPARTMENT_NBR EV_INT 
^

ALTER TABLE CO_HR_SUPERVISORS
ALTER COLUMN CO_DEPARTMENT_NBR POSITION 9
^

ALTER TABLE CO_HR_SUPERVISORS
ADD CO_TEAM_NBR EV_INT 
^

ALTER TABLE CO_HR_SUPERVISORS
ALTER COLUMN CO_TEAM_NBR POSITION 10
^

ALTER TABLE CO_HR_SUPERVISORS
ADD CO_PAY_GROUP_NBR EV_INT 
^

ALTER TABLE CO_HR_SUPERVISORS
ALTER COLUMN CO_PAY_GROUP_NBR POSITION 11
^

CREATE INDEX FK_CO_HR_SUPERVISORS_2 ON CO_HR_SUPERVISORS (CO_DIVISION_NBR)
^

CREATE INDEX FK_CO_HR_SUPERVISORS_3 ON CO_HR_SUPERVISORS (CO_BRANCH_NBR)
^

CREATE INDEX FK_CO_HR_SUPERVISORS_4 ON CO_HR_SUPERVISORS (CO_DEPARTMENT_NBR)
^

CREATE INDEX FK_CO_HR_SUPERVISORS_5 ON CO_HR_SUPERVISORS (CO_TEAM_NBR)
^

CREATE INDEX FK_CO_HR_SUPERVISORS_6 ON CO_HR_SUPERVISORS (CO_PAY_GROUP_NBR)
^


DROP TRIGGER T_AUD_CO_PAY_GROUP_2
^


DROP TRIGGER T_AUD_CO_TEAM_2
^


DROP TRIGGER T_AD_EE_9
^


DROP TRIGGER T_AIU_EE_3
^


DROP TRIGGER T_AU_EE_9
^


DROP TRIGGER T_BIU_EE_2
^

ALTER TABLE EE
ADD ACA_CO_BENEFIT_SUBTYPE_NBR EV_INT 
^

ALTER TABLE EE
ALTER COLUMN ACA_CO_BENEFIT_SUBTYPE_NBR POSITION 150
^

ALTER TABLE EE
ADD EE_ACA_SAFE_HARBOR EV_STR2 
^

ALTER TABLE EE
ALTER COLUMN EE_ACA_SAFE_HARBOR POSITION 151
^

ALTER TABLE EE
ADD ACA_POLICY_ORIGIN EV_CHAR1  NOT NULL
^

ALTER TABLE EE
ALTER COLUMN ACA_POLICY_ORIGIN POSITION 152
^

ALTER TABLE EE
ADD ENABLE_ANALYTICS EV_CHAR1  NOT NULL
^

ALTER TABLE EE
ALTER COLUMN ENABLE_ANALYTICS POSITION 153
^

ALTER TABLE EE
ADD BENEFITS_ELIGIBLE EV_CHAR1  NOT NULL
^

ALTER TABLE EE
ALTER COLUMN BENEFITS_ELIGIBLE POSITION 154
^

COMMIT^

CREATE INDEX FK_EE_28 ON EE (ACA_CO_BENEFIT_SUBTYPE_NBR,EFFECTIVE_UNTIL)
^

COMMIT^

UPDATE EE SET BENEFITS_ELIGIBLE = 'N', ENABLE_ANALYTICS = 'Y', ACA_POLICY_ORIGIN = 'B'
^


DROP TRIGGER T_AD_EE_BENEFICIARY_9
^


DROP TRIGGER T_AU_EE_BENEFICIARY_9
^

ALTER TABLE EE_BENEFICIARY
ADD ACA_DEP_PLAN_BEGIN_DATE EV_DATETIME 
^

ALTER TABLE EE_BENEFICIARY
ALTER COLUMN ACA_DEP_PLAN_BEGIN_DATE POSITION 9
^

ALTER TABLE EE_BENEFICIARY
ADD ACA_DEP_PLAN_END_DATE EV_DATETIME 
^

ALTER TABLE EE_BENEFICIARY
ALTER COLUMN ACA_DEP_PLAN_END_DATE POSITION 10
^


DROP TRIGGER T_AD_EE_TIME_OFF_ACCRUAL_9
^


DROP TRIGGER T_AU_EE_TIME_OFF_ACCRUAL_9
^

ALTER TABLE EE_TIME_OFF_ACCRUAL
ADD OVERRIDE_RESET_DATE EV_DATETIME 
^

ALTER TABLE EE_TIME_OFF_ACCRUAL
ALTER COLUMN OVERRIDE_RESET_DATE POSITION 24
^


DROP TRIGGER T_AD_CO_BENEFIT_ENROLLMENT_9
^


DROP TRIGGER T_AU_CO_BENEFIT_ENROLLMENT_9
^

ALTER TABLE CO_BENEFIT_ENROLLMENT
ADD CATEGORY_COVERAGE_START_DATE EV_DATETIME 
^

ALTER TABLE CO_BENEFIT_ENROLLMENT
ALTER COLUMN CATEGORY_COVERAGE_START_DATE POSITION 8
^

ALTER TABLE CO_BENEFIT_ENROLLMENT
ADD CATEGORY_COVERAGE_END_DATE EV_DATETIME 
^

ALTER TABLE CO_BENEFIT_ENROLLMENT
ALTER COLUMN CATEGORY_COVERAGE_END_DATE POSITION 9
^

CREATE TABLE  CO_ACA_CERT_ELIGIBILITY
( 
     REC_VERSION EV_INT NOT NULL,
     CO_ACA_CERT_ELIGIBILITY_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CO_NBR EV_INT NOT NULL,
     ACA_CERT_ELIGIBILITY_TYPE EV_CHAR1 NOT NULL,
     DESCRIPTION EV_STR80,
        CONSTRAINT PK_CO_ACA_CERT_ELIGIBILITY PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_CO_ACA_CERT_ELIGIBILITY_2 UNIQUE (CO_ACA_CERT_ELIGIBILITY_NBR,EFFECTIVE_UNTIL),
        CONSTRAINT AK_CO_ACA_CERT_ELIGIBILITY__1 UNIQUE (CO_ACA_CERT_ELIGIBILITY_NBR,EFFECTIVE_DATE)
)
^

CREATE INDEX FK_CO_ACA_CERT_ELIGIBILITY_1 ON CO_ACA_CERT_ELIGIBILITY (CO_NBR)
^


GRANT ALL ON CO_ACA_CERT_ELIGIBILITY TO EUSER
^

CREATE TABLE  CO_BENEFIT_AGE_BANDS
( 
     REC_VERSION EV_INT NOT NULL,
     CO_BENEFIT_AGE_BANDS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CO_BENEFIT_SUBTYPE_NBR EV_INT NOT NULL,
     EFFECTIVE_START_DATE EV_DATETIME NOT NULL,
     EFFECTIVE_END_DATE EV_DATETIME NOT NULL,
     AGE_MIN EV_INT,
     AGE_MAX EV_INT,
     MULTIPLIER EV_AMOUNT,
        CONSTRAINT PK_CO_BENEFIT_AGE_BANDS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_CO_BENEFIT_AGE_BANDS_1 UNIQUE (CO_BENEFIT_AGE_BANDS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_CO_BENEFIT_AGE_BANDS_2 UNIQUE (CO_BENEFIT_AGE_BANDS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_CO_BENEFIT_SUBTYPE_NBR ON CO_BENEFIT_AGE_BANDS (CO_BENEFIT_SUBTYPE_NBR)
^


GRANT ALL ON CO_BENEFIT_AGE_BANDS TO EUSER
^

CREATE TABLE  CO_CUSTOM_LABELS
( 
     REC_VERSION EV_INT NOT NULL,
     CO_CUSTOM_LABELS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CO_NBR EV_INT NOT NULL,
     DIVISION EV_STR20,
     BRANCH EV_STR20,
     DEPARTMENT EV_STR20,
     TEAM EV_STR20,
     JOB EV_STR20,
        CONSTRAINT PK_CO_CUSTOM_LABELS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_CO_CUSTOM_LABELS_1 UNIQUE (CO_CUSTOM_LABELS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_CO_CUSTOM_LABELS_2 UNIQUE (CO_CUSTOM_LABELS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_CO_CUSTOM_LABELS_1 ON CO_CUSTOM_LABELS (CO_NBR)
^


GRANT ALL ON CO_CUSTOM_LABELS TO EUSER
^

CREATE TABLE  CO_DOCUMENTS
( 
     REC_VERSION EV_INT NOT NULL,
     CO_DOCUMENTS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CO_NBR EV_INT NOT NULL,
     NAME EV_STR40 NOT NULL,
     DOCUMENT EV_BLOB_TXT,
     FILE_NAME EV_STR40,
     BEGIN_DATE EV_DATETIME,
     END_DATE EV_DATETIME,
        CONSTRAINT PK_CO_DOCUMENTS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_CO_DOCUMENTS_1 UNIQUE (CO_DOCUMENTS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_CO_DOCUMENTS_2 UNIQUE (CO_DOCUMENTS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX F_CO_DOCUMENTS_1 ON CO_DOCUMENTS (CO_NBR)
^


GRANT ALL ON CO_DOCUMENTS TO EUSER
^

CREATE TABLE  CO_DW_STORAGE
( 
     REC_VERSION EV_INT NOT NULL,
     CO_DW_STORAGE_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CO_NBR EV_INT NOT NULL,
     DATA_TIMESTAMP EV_DATETIME,
     TAG EV_STR20,
     PARAM_TAG EV_STR128,
     REPORT_PARAMETERS EV_BLOB_BIN,
     REPORT_DATA EV_BLOB_BIN,
        CONSTRAINT PK_CO_DW_STORAGE PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_CO_DW_STORAGE_1 UNIQUE (CO_DW_STORAGE_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_CO_DW_STORAGE_2 UNIQUE (CO_DW_STORAGE_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_CO_DW_STORAGE_1 ON CO_DW_STORAGE (CO_NBR)
^


GRANT ALL ON CO_DW_STORAGE TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY1
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check CL */
  child_table = 'CL';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'AGENCY_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_AGENCY */
  child_table = 'CL_AGENCY';

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'CL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_BANK_ACCOUNT */
  child_table = 'CL_BANK_ACCOUNT';

  parent_table = 'CL_BLOB';
  child_field = 'LOGO_CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'SIGNATURE_CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_CO_CONSOLIDATION */
  child_table = 'CL_CO_CONSOLIDATION';

  parent_table = 'CO';
  child_field = 'PRIMARY_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_DELIVERY_GROUP */
  child_table = 'CL_DELIVERY_GROUP';

  parent_table = 'CL_DELIVERY_METHOD';
  child_field = 'PRIMARY_CL_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_METHOD';
  child_field = 'SECOND_CL_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_DS */
  child_table = 'CL_E_DS';

  parent_table = 'CL_E_DS';
  child_field = 'OFFSET_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MIN_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'PIECE_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'OT_ALL_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'DEFAULT_CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_3_PARTY_SICK_PAY_ADMIN';
  child_field = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_MAX_AVG_AMT_GRP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_MAX_AVG_HRS_GRP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SD_THRESHOLD_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_FUNDING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_EXCESS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_GROUP_CODES */
  child_table = 'CL_E_D_GROUP_CODES';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_LOCAL_EXMPT_EXCLD */
  child_table = 'CL_E_D_LOCAL_EXMPT_EXCLD';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_E_D_STATE_EXMPT_EXCLD */
  child_table = 'CL_E_D_STATE_EXMPT_EXCLD';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_FUNDS */
  child_table = 'CL_FUNDS';

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_EDUCATION */
  child_table = 'CL_HR_PERSON_EDUCATION';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_SCHOOL';
  child_field = 'CL_HR_SCHOOL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_HANDICAPS */
  child_table = 'CL_HR_PERSON_HANDICAPS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_HR_PERSON_SKILLS */
  child_table = 'CL_HR_PERSON_SKILLS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_SKILLS';
  child_field = 'CL_HR_SKILLS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_MAIL_BOX_GROUP */
  child_table = 'CL_MAIL_BOX_GROUP';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'UP_LEVEL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_MAIL_BOX_GROUP_OPTION */
  child_table = 'CL_MAIL_BOX_GROUP_OPTION';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'CL_MAIL_BOX_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PENSION */
  child_table = 'CL_PENSION';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PERSON_DEPENDENTS */
  child_table = 'CL_PERSON_DEPENDENTS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_PERSON_DOCUMENTS */
  child_table = 'CL_PERSON_DOCUMENTS';

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_UNION */
  child_table = 'CL_UNION';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CL_UNION_DUES */
  child_table = 'CL_UNION_DUES';

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO */
  child_table = 'CO';

  parent_table = 'CL_COMMON_PAYMASTER';
  child_field = 'CL_COMMON_PAYMASTER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'WORKERS_COMP_CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'ANNUAL_CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'QUARTER_CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'AGENCY_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_REPORT_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_RETURN_SECOND_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'MANUAL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'W_COMP_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'AUTO_RD_DFLT_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_SDI_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_SUI_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'ACA_ADD_EARN_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'ACA_CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO_NAMES */
  child_table = 'CO_ADDITIONAL_INFO_NAMES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO_VALUES */
  child_table = 'CO_ADDITIONAL_INFO_VALUES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_AUTO_ENLIST_RETURNS */
  child_table = 'CO_AUTO_ENLIST_RETURNS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BANK_ACCOUNT_REGISTER */
  child_table = 'CO_BANK_ACCOUNT_REGISTER';

  parent_table = 'CO_MANUAL_ACH';
  child_field = 'CO_MANUAL_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_ACH';
  child_field = 'CO_PR_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_PAYMENT_ACH';
  child_field = 'CO_TAX_PAYMENT_ACH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_MISCELLANEOUS_CHECKS';
  child_field = 'PR_MISCELLANEOUS_CHECKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BANK_ACC_REG_DETAILS */
  child_table = 'CO_BANK_ACC_REG_DETAILS';

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'CO_BANK_ACCOUNT_REGISTER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BATCH_LOCAL_OR_TEMPS */
  child_table = 'CO_BATCH_LOCAL_OR_TEMPS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BATCH_STATES_OR_TEMPS */
  child_table = 'CO_BATCH_STATES_OR_TEMPS';

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BILLING_HISTORY */
  child_table = 'CO_BILLING_HISTORY';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CO_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BILLING_HISTORY_DETAIL */
  child_table = 'CO_BILLING_HISTORY_DETAIL';

  parent_table = 'CO_BILLING_HISTORY';
  child_field = 'CO_BILLING_HISTORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SERVICES';
  child_field = 'CO_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRANCH */
  child_table = 'CO_BRANCH';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRANCH_LOCALS */
  child_table = 'CO_BRANCH_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BRCH_PR_BATCH_DEFLT_ED */
  child_table = 'CO_BRCH_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_CALENDAR_DEFAULTS */
  child_table = 'CO_CALENDAR_DEFAULTS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPARTMENT */
  child_table = 'CO_DEPARTMENT';

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPARTMENT_LOCALS */
  child_table = 'CO_DEPARTMENT_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DEPT_PR_BATCH_DEFLT_ED */
  child_table = 'CO_DEPT_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIVISION */
  child_table = 'CO_DIVISION';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIVISION_LOCALS */
  child_table = 'CO_DIVISION_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DIV_PR_BATCH_DEFLT_ED */
  child_table = 'CO_DIV_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ENLIST_GROUPS */
  child_table = 'CO_ENLIST_GROUPS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_E_D_CODES */
  child_table = 'CO_E_D_CODES';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_FED_TAX_LIABILITIES */
  child_table = 'CO_FED_TAX_LIABILITIES';

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GENERAL_LEDGER */
  child_table = 'CO_GENERAL_LEDGER';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP */
  child_table = 'CO_GROUP';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP_MANAGER */
  child_table = 'CO_GROUP_MANAGER';

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_GROUP_MEMBER */
  child_table = 'CO_GROUP_MEMBER';

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_APPLICANT */
  child_table = 'CO_HR_APPLICANT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_RECRUITERS';
  child_field = 'CO_HR_RECRUITERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_REFERRALS';
  child_field = 'CO_HR_REFERRALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_CAR';
  child_field = 'CO_HR_CAR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_APPLICANT_INTERVIEW */
  child_table = 'CO_HR_APPLICANT_INTERVIEW';

  parent_table = 'CO_HR_APPLICANT';
  child_field = 'CO_HR_APPLICANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_ATTENDANCE_TYPES */
  child_table = 'CO_HR_ATTENDANCE_TYPES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_CAR */
  child_table = 'CO_HR_CAR';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_PERFORMANCE_RATINGS */
  child_table = 'CO_HR_PERFORMANCE_RATINGS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_POSITIONS */
  child_table = 'CO_HR_POSITIONS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_PROPERTY */
  child_table = 'CO_HR_PROPERTY';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_RECRUITERS */
  child_table = 'CO_HR_RECRUITERS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_REFERRALS */
  child_table = 'CO_HR_REFERRALS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_SALARY_GRADES */
  child_table = 'CO_HR_SALARY_GRADES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_SUPERVISORS */
  child_table = 'CO_HR_SUPERVISORS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PAY_GROUP';
  child_field = 'CO_PAY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOBS */
  child_table = 'CO_JOBS';

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOBS_LOCALS */
  child_table = 'CO_JOBS_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_JOB_GROUPS */
  child_table = 'CO_JOB_GROUPS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCAL_TAX */
  child_table = 'CO_LOCAL_TAX';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCAL_TAX_LIABILITIES */
  child_table = 'CO_LOCAL_TAX_LIABILITIES';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NONRES_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_MANUAL_ACH */
  child_table = 'CO_MANUAL_ACH';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'TO_EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'FROM_EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'FROM_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TO_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'FROM_EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'TO_EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'FROM_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'TO_CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY1 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY1 TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY2
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check CO_PAY_GROUP */
  child_table = 'CO_PAY_GROUP';

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PENSIONS */
  child_table = 'CO_PENSIONS';

  parent_table = 'CL_PENSION';
  child_field = 'CL_PENSION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PHONE */
  child_table = 'CO_PHONE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_ACH */
  child_table = 'CO_PR_ACH';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_BATCH_DEFLT_ED */
  child_table = 'CO_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_CHECK_TEMPLATES */
  child_table = 'CO_PR_CHECK_TEMPLATES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_CHECK_TEMPLATE_E_DS */
  child_table = 'CO_PR_CHECK_TEMPLATE_E_DS';

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_PR_FILTERS */
  child_table = 'CO_PR_FILTERS';

  parent_table = 'CO_PAY_GROUP';
  child_field = 'CO_PAY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_REPORTS */
  child_table = 'CO_REPORTS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'OVERRIDE_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SALESPERSON */
  child_table = 'CO_SALESPERSON';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SALESPERSON_FLAT_AMT */
  child_table = 'CO_SALESPERSON_FLAT_AMT';

  parent_table = 'CO_SALESPERSON';
  child_field = 'CO_SALESPERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SERVICES */
  child_table = 'CO_SERVICES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SHIFTS */
  child_table = 'CO_SHIFTS';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_STATES */
  child_table = 'CO_STATES';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_STATE_TAX_LIABILITIES */
  child_table = 'CO_STATE_TAX_LIABILITIES';

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_STORAGE */
  child_table = 'CO_STORAGE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SUI */
  child_table = 'CO_SUI';

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_SUI_LIABILITIES */
  child_table = 'CO_SUI_LIABILITIES';

  parent_table = 'CO_SUI';
  child_field = 'CO_SUI_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BANK_ACCOUNT_REGISTER';
  child_field = 'IMPOUND_CO_BANK_ACCT_REG_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_DEPOSITS */
  child_table = 'CO_TAX_DEPOSITS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_PAYMENT_ACH */
  child_table = 'CO_TAX_PAYMENT_ACH';

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_RETURN_QUEUE */
  child_table = 'CO_TAX_RETURN_QUEUE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_CO_CONSOLIDATION';
  child_field = 'CL_CO_CONSOLIDATION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TAX_RETURN_RUNS */
  child_table = 'CO_TAX_RETURN_RUNS';

  parent_table = 'CO_TAX_RETURN_QUEUE';
  child_field = 'CO_TAX_RETURN_QUEUE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'CL_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TEAM */
  child_table = 'CO_TEAM';

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'BILLING_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'DD_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'PAYROLL_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BANK_ACCOUNT';
  child_field = 'TAX_CL_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'HOME_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BILLING';
  child_field = 'CL_BILLING_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_TIMECLOCK_IMPORTS';
  child_field = 'CL_TIMECLOCK_IMPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'UNION_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'NEW_HIRE_CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TEAM_LOCALS */
  child_table = 'CO_TEAM_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TEAM_PR_BATCH_DEFLT_ED */
  child_table = 'CO_TEAM_PR_BATCH_DEFLT_ED';

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TIME_OFF_ACCRUAL */
  child_table = 'CO_TIME_OFF_ACCRUAL';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'USED_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_ATTENDANCE_TYPES';
  child_field = 'CO_HR_ATTENDANCE_TYPES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TIME_OFF_ACCRUAL_RATES */
  child_table = 'CO_TIME_OFF_ACCRUAL_RATES';

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_TIME_OFF_ACCRUAL_TIERS */
  child_table = 'CO_TIME_OFF_ACCRUAL_TIERS';

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_UNIONS */
  child_table = 'CO_UNIONS';

  parent_table = 'CL_UNION';
  child_field = 'CL_UNION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_WORKERS_COMP */
  child_table = 'CO_WORKERS_COMP';

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE */
  child_table = 'EE';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'ALD_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SHIFTS';
  child_field = 'AUTOPAY_CO_SHIFTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_APPLICANT';
  child_field = 'CO_HR_APPLICANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PAY_GROUP';
  child_field = 'CO_PAY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_DELIVERY_GROUP';
  child_field = 'CL_DELIVERY_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_PERFORMANCE_RATINGS';
  child_field = 'CO_HR_PERFORMANCE_RATINGS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_RECRUITERS';
  child_field = 'CO_HR_RECRUITERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_REFERRALS';
  child_field = 'CO_HR_REFERRALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_UNIONS';
  child_field = 'CO_UNIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_CHECK_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'PR_EE_REPORT_SEC_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_MAIL_BOX_GROUP';
  child_field = 'TAX_EE_RETURN_MB_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON';
  child_field = 'CL_PERSON_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITION_GRADES';
  child_field = 'CO_HR_POSITION_GRADES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_DISCOUNT';
  child_field = 'CO_BENEFIT_DISCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'HOME_TAX_EE_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'ACA_CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_ADDITIONAL_INFO */
  child_table = 'EE_ADDITIONAL_INFO';

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_VALUES';
  child_field = 'CO_ADDITIONAL_INFO_VALUES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'INFO_BLOB';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_AUTOLABOR_DISTRIBUTION */
  child_table = 'EE_AUTOLABOR_DISTRIBUTION';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFITS */
  child_table = 'EE_BENEFITS';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_ENROLLMENT';
  child_field = 'CO_BENEFIT_ENROLLMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFIT_PAYMENT */
  child_table = 'EE_BENEFIT_PAYMENT';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_CHANGE_REQUEST */
  child_table = 'EE_CHANGE_REQUEST';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_SIGNATURE';
  child_field = 'SIGNATURE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_CHILD_SUPPORT_CASES */
  child_table = 'EE_CHILD_SUPPORT_CASES';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'DEPENDENT_HEALTH_CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_DIRECT_DEPOSIT */
  child_table = 'EE_DIRECT_DEPOSIT';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_EMERGENCY_CONTACTS */
  child_table = 'EE_EMERGENCY_CONTACTS';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_ATTENDANCE */
  child_table = 'EE_HR_ATTENDANCE';

  parent_table = 'CO_HR_ATTENDANCE_TYPES';
  child_field = 'CO_HR_ATTENDANCE_TYPES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_CAR */
  child_table = 'EE_HR_CAR';

  parent_table = 'CO_HR_CAR';
  child_field = 'CO_HR_CAR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_CO_PROVIDED_EDUCATN */
  child_table = 'EE_HR_CO_PROVIDED_EDUCATN';

  parent_table = 'CL_HR_SCHOOL';
  child_field = 'CL_HR_SCHOOL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_COURSE';
  child_field = 'CL_HR_COURSE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_INJURY_OCCURRENCE */
  child_table = 'EE_HR_INJURY_OCCURRENCE';

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_PERFORMANCE_RATINGS */
  child_table = 'EE_HR_PERFORMANCE_RATINGS';

  parent_table = 'CO_HR_PERFORMANCE_RATINGS';
  child_field = 'CO_HR_PERFORMANCE_RATINGS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SUPERVISORS';
  child_field = 'CO_HR_SUPERVISORS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_HR_REASON_CODES';
  child_field = 'CL_HR_REASON_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_HR_PROPERTY_TRACKING */
  child_table = 'EE_HR_PROPERTY_TRACKING';

  parent_table = 'CO_HR_PROPERTY';
  child_field = 'CO_HR_PROPERTY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_LOCALS */
  child_table = 'EE_LOCALS';

  parent_table = 'CO_LOCAL_TAX';
  child_field = 'CO_LOCAL_TAX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_PENSION_FUND_SPLITS */
  child_table = 'EE_PENSION_FUND_SPLITS';

  parent_table = 'CL_FUNDS';
  child_field = 'CL_FUNDS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_PIECE_WORK */
  child_table = 'EE_PIECE_WORK';

  parent_table = 'CL_PIECES';
  child_field = 'CL_PIECES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_RATES */
  child_table = 'EE_RATES';

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_POSITION_GRADES';
  child_field = 'CO_HR_POSITION_GRADES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_SCHEDULED_E_DS */
  child_table = 'EE_SCHEDULED_E_DS';

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_PPP_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MIN_PPP_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'SCHEDULED_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'THRESHOLD_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_CHILD_SUPPORT_CASES';
  child_field = 'EE_CHILD_SUPPORT_CASES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_DIRECT_DEPOSIT';
  child_field = 'EE_DIRECT_DEPOSIT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_BENEFITS';
  child_field = 'EE_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_FUNDING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'HW_EXCESS_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_STATES */
  child_table = 'EE_STATES';

  parent_table = 'CO_STATES';
  child_field = 'CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'SDI_APPLY_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'SUI_APPLY_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_STATES';
  child_field = 'RECIPROCAL_CO_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_TIME_OFF_ACCRUAL */
  child_table = 'EE_TIME_OFF_ACCRUAL';

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TIME_OFF_ACCRUAL';
  child_field = 'RLLOVR_CO_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_TIME_OFF_ACCRUAL_OPER */
  child_table = 'EE_TIME_OFF_ACCRUAL_OPER';

  parent_table = 'EE_TIME_OFF_ACCRUAL';
  child_field = 'EE_TIME_OFF_ACCRUAL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_BATCH';
  child_field = 'PR_BATCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_TIME_OFF_ACCRUAL_OPER';
  child_field = 'CONNECTED_EE_TOA_OPER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_TIME_OFF_ACCRUAL_OPER';
  child_field = 'ADJUSTED_EE_TOA_OPER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_WORK_SHIFTS */
  child_table = 'EE_WORK_SHIFTS';

  parent_table = 'CO_SHIFTS';
  child_field = 'CO_SHIFTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY2 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY2 TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY3
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check PR */
  child_table = 'PR';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_BATCH */
  child_table = 'PR_BATCH';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_CHECK_TEMPLATES';
  child_field = 'CO_PR_CHECK_TEMPLATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_PR_FILTERS';
  child_field = 'CO_PR_FILTERS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK */
  child_table = 'PR_CHECK';

  parent_table = 'PR_BATCH';
  child_field = 'PR_BATCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'DATA_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'NOTES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_LINES */
  child_table = 'PR_CHECK_LINES';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR_MISCELLANEOUS_CHECKS';
  child_field = 'PR_MISCELLANEOUS_CHECKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_JOBS';
  child_field = 'CO_JOBS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_WORKERS_COMP';
  child_field = 'CO_WORKERS_COMP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'EE_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'EE_SUI_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SHIFTS';
  child_field = 'CO_SHIFTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PIECES';
  child_field = 'CL_PIECES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_DS';
  child_field = 'CL_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_SCHEDULED_E_DS';
  child_field = 'EE_SCHEDULED_E_DS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'REDUCING_CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_LINE_LOCALS */
  child_table = 'PR_CHECK_LINE_LOCALS';

  parent_table = 'PR_CHECK_LINES';
  child_field = 'PR_CHECK_LINES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_LOCALS';
  child_field = 'EE_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_LOCALS */
  child_table = 'PR_CHECK_LOCALS';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_LOCALS';
  child_field = 'EE_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_LOCATIONS';
  child_field = 'CO_LOCATIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_LOCALS';
  child_field = 'NONRES_EE_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_STATES */
  child_table = 'PR_CHECK_STATES';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'EE_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_CHECK_SUI */
  child_table = 'PR_CHECK_SUI';

  parent_table = 'PR_CHECK';
  child_field = 'PR_CHECK_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_SUI';
  child_field = 'CO_SUI_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_MISCELLANEOUS_CHECKS */
  child_table = 'PR_MISCELLANEOUS_CHECKS';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TAX_DEPOSITS';
  child_field = 'CO_TAX_DEPOSITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_REPORTS */
  child_table = 'PR_REPORTS';

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_REPRINT_HISTORY */
  child_table = 'PR_REPRINT_HISTORY';

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_REPRINT_HISTORY_DETAIL */
  child_table = 'PR_REPRINT_HISTORY_DETAIL';

  parent_table = 'PR_REPRINT_HISTORY';
  child_field = 'PR_REPRINT_HISTORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SCHEDULED_EVENT */
  child_table = 'PR_SCHEDULED_EVENT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SCHEDULED_EVENT_BATCH */
  child_table = 'PR_SCHEDULED_EVENT_BATCH';

  parent_table = 'PR_SCHEDULED_EVENT';
  child_field = 'PR_SCHEDULED_EVENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SCHEDULED_E_DS */
  child_table = 'PR_SCHEDULED_E_DS';

  parent_table = 'CO_E_D_CODES';
  child_field = 'CO_E_D_CODES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check PR_SERVICES */
  child_table = 'PR_SERVICES';

  parent_table = 'CO_SERVICES';
  child_field = 'CO_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'PR';
  child_field = 'PR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_HR_POSITION_GRADES */
  child_table = 'CO_HR_POSITION_GRADES';

  parent_table = 'CO_HR_POSITIONS';
  child_field = 'CO_HR_POSITIONS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_HR_SALARY_GRADES';
  child_field = 'CO_HR_SALARY_GRADES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ADDITIONAL_INFO */
  child_table = 'CO_ADDITIONAL_INFO';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_NAMES';
  child_field = 'CO_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_ADDITIONAL_INFO_VALUES';
  child_field = 'CO_ADDITIONAL_INFO_VALUES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_BLOB';
  child_field = 'INFO_BLOB';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFITS */
  child_table = 'CO_BENEFITS';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'EE_DEDUCTION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'ER_DEDUCTION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'EE_COBRA_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_E_D_CODES';
  child_field = 'ER_COBRA_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_CATEGORY';
  child_field = 'CO_BENEFIT_CATEGORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_CATEGORY */
  child_table = 'CO_BENEFIT_CATEGORY';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_DISCOUNT */
  child_table = 'CO_BENEFIT_DISCOUNT';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PACKAGE */
  child_table = 'CO_BENEFIT_PACKAGE';

  parent_table = 'CO_E_D_CODES';
  child_field = 'PAYOUT_ED_CODE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PKG_ASMNT */
  child_table = 'CO_BENEFIT_PKG_ASMNT';

  parent_table = 'CO_BENEFIT_PACKAGE';
  child_field = 'CO_BENEFIT_PACKAGE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DIVISION';
  child_field = 'CO_DIVISION_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BRANCH';
  child_field = 'CO_BRANCH_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_DEPARTMENT';
  child_field = 'CO_DEPARTMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_TEAM';
  child_field = 'CO_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_GROUP';
  child_field = 'CO_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PKG_DETAIL */
  child_table = 'CO_BENEFIT_PKG_DETAIL';

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_PACKAGE';
  child_field = 'CO_BENEFIT_PACKAGE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_PROVIDERS */
  child_table = 'CO_BENEFIT_PROVIDERS';

  parent_table = 'CL_AGENCY';
  child_field = 'CL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_RATES */
  child_table = 'CO_BENEFIT_RATES';

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_E_D_GROUPS';
  child_field = 'CL_E_D_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_SETUP */
  child_table = 'CO_BENEFIT_SETUP';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_STATES */
  child_table = 'CO_BENEFIT_STATES';

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_SUBTYPE */
  child_table = 'CO_BENEFIT_SUBTYPE';

  parent_table = 'CO_BENEFITS';
  child_field = 'CO_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_USER_REPORTS */
  child_table = 'CO_USER_REPORTS';

  parent_table = 'CO_REPORTS';
  child_field = 'CO_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFICIARY */
  child_table = 'EE_BENEFICIARY';

  parent_table = 'EE_BENEFITS';
  child_field = 'EE_BENEFITS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CL_PERSON_DEPENDENTS';
  child_field = 'CL_PERSON_DEPENDENTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_BENEFIT_REFUSAL */
  child_table = 'EE_BENEFIT_REFUSAL';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'HOME_STATE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'EE_STATES';
  child_field = 'SUI_STATE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_ENROLLMENT';
  child_field = 'CO_BENEFIT_ENROLLMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check EE_SIGNATURE */
  child_table = 'EE_SIGNATURE';

  parent_table = 'EE';
  child_field = 'EE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'CO_BENEFIT_ENROLLMENT';
  child_field = 'CO_BENEFIT_ENROLLMENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_LOCATIONS */
  child_table = 'CO_LOCATIONS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_QEC_RUN */
  child_table = 'CO_QEC_RUN';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_ENROLLMENT */
  child_table = 'CO_BENEFIT_ENROLLMENT';

  parent_table = 'CO_BENEFIT_CATEGORY';
  child_field = 'CO_BENEFIT_CATEGORY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DOCUMENTS */
  child_table = 'CO_DOCUMENTS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_CUSTOM_LABELS */
  child_table = 'CO_CUSTOM_LABELS';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_DW_STORAGE */
  child_table = 'CO_DW_STORAGE';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_BENEFIT_AGE_BANDS */
  child_table = 'CO_BENEFIT_AGE_BANDS';

  parent_table = 'CO_BENEFIT_SUBTYPE';
  child_field = 'CO_BENEFIT_SUBTYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check CO_ACA_CERT_ELIGIBILITY */
  child_table = 'CO_ACA_CERT_ELIGIBILITY';

  parent_table = 'CO';
  child_field = 'CO_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY3 TO EUSER
^



GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY3 TO EUSER
^

CREATE OR ALTER PROCEDURE pack_ee(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE p_cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE p_co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE p_co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE p_co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE p_co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE p_new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE p_current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE p_co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE p_tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE p_w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE p_w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE p_w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE p_w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE p_w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE p_w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE p_home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE p_exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE p_exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE p_base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE p_company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE p_distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE p_tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE p_total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE p_pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE p_makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE p_salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE p_highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE p_corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE p_co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE p_co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE p_healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
DECLARE VARIABLE p_aca_status CHAR(1);
DECLARE VARIABLE aca_coverage_offer VARCHAR(2);
DECLARE VARIABLE p_aca_coverage_offer VARCHAR(2);
DECLARE VARIABLE aca_co_benefit_subtype_nbr INTEGER;
DECLARE VARIABLE p_aca_co_benefit_subtype_nbr INTEGER;
DECLARE VARIABLE ee_aca_safe_harbor VARCHAR(2);
DECLARE VARIABLE p_ee_aca_safe_harbor VARCHAR(2);
DECLARE VARIABLE aca_policy_origin CHAR(1);
DECLARE VARIABLE p_aca_policy_origin CHAR(1);
DECLARE VARIABLE benefits_eligible CHAR(1);
DECLARE VARIABLE p_benefits_eligible CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_nbr , cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status, aca_coverage_offer, aca_co_benefit_subtype_nbr, ee_aca_safe_harbor, aca_policy_origin, benefits_eligible
        FROM ee
        ORDER BY ee_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_nbr, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status, :aca_coverage_offer, :aca_co_benefit_subtype_nbr, :ee_aca_safe_harbor, :aca_policy_origin, :benefits_eligible
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_nbr) OR (effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override) OR (p_aca_status IS DISTINCT FROM aca_status) OR (p_aca_coverage_offer IS DISTINCT FROM aca_coverage_offer) OR (p_aca_co_benefit_subtype_nbr IS DISTINCT FROM aca_co_benefit_subtype_nbr) OR (p_ee_aca_safe_harbor IS DISTINCT FROM ee_aca_safe_harbor) OR (p_aca_policy_origin IS DISTINCT FROM aca_policy_origin) OR (p_benefits_eligible IS DISTINCT FROM benefits_eligible)) THEN
      BEGIN
        curr_nbr = ee_nbr;
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
        p_aca_status = aca_status;
        p_aca_coverage_offer = aca_coverage_offer;
        p_aca_co_benefit_subtype_nbr = aca_co_benefit_subtype_nbr;
        p_ee_aca_safe_harbor = ee_aca_safe_harbor;
        p_aca_policy_origin = aca_policy_origin;
        p_benefits_eligible = benefits_eligible;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status, aca_coverage_offer, aca_co_benefit_subtype_nbr, ee_aca_safe_harbor, aca_policy_origin, benefits_eligible
        FROM ee
        WHERE ee_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status, :aca_coverage_offer, :aca_co_benefit_subtype_nbr, :ee_aca_safe_harbor, :aca_policy_origin, :benefits_eligible
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override) OR (p_aca_status IS DISTINCT FROM aca_status) OR (p_aca_coverage_offer IS DISTINCT FROM aca_coverage_offer) OR (p_aca_co_benefit_subtype_nbr IS DISTINCT FROM aca_co_benefit_subtype_nbr) OR (p_ee_aca_safe_harbor IS DISTINCT FROM ee_aca_safe_harbor) OR (p_aca_policy_origin IS DISTINCT FROM aca_policy_origin) OR (p_benefits_eligible IS DISTINCT FROM benefits_eligible)) THEN
      BEGIN
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
        p_aca_status = aca_status;
        p_aca_coverage_offer = aca_coverage_offer;
        p_aca_co_benefit_subtype_nbr = aca_co_benefit_subtype_nbr;
        p_ee_aca_safe_harbor = ee_aca_safe_harbor;
        p_aca_policy_origin = aca_policy_origin;
        p_benefits_eligible = benefits_eligible;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_EE TO EUSER
^

CREATE OR ALTER PROCEDURE del_co_documents(nbr INTEGER)
AS
BEGIN
  DELETE FROM co_documents WHERE co_documents_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_CO_DOCUMENTS TO EUSER
^

CREATE OR ALTER PROCEDURE del_co_custom_labels(nbr INTEGER)
AS
BEGIN
  DELETE FROM co_custom_labels WHERE co_custom_labels_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_CO_CUSTOM_LABELS TO EUSER
^

CREATE OR ALTER PROCEDURE del_co_dw_storage(nbr INTEGER)
AS
BEGIN
  DELETE FROM co_dw_storage WHERE co_dw_storage_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_CO_DW_STORAGE TO EUSER
^

CREATE OR ALTER PROCEDURE del_co_benefit_age_bands(nbr INTEGER)
AS
BEGIN
  DELETE FROM co_benefit_age_bands WHERE co_benefit_age_bands_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_CO_BENEFIT_AGE_BANDS TO EUSER
^

CREATE OR ALTER PROCEDURE del_co_aca_cert_eligibility(nbr INTEGER)
AS
BEGIN
  DELETE FROM co_aca_cert_eligibility WHERE co_aca_cert_eligibility_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_CO_ACA_CERT_ELIGIBILITY TO EUSER
^

ALTER TABLE CO_ACA_CERT_ELIGIBILITY
ADD CONSTRAINT C_CO_ACA_CERT_ELIGIBILITY_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE CO_BENEFIT_AGE_BANDS
ADD CONSTRAINT C_CO_BENEFIT_AGE_BANDS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE CO_CUSTOM_LABELS
ADD CONSTRAINT C_CO_CUSTOM_LABELS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE CO_DOCUMENTS
ADD CONSTRAINT C_CO_DOCUMENTS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE CO_DW_STORAGE
ADD CONSTRAINT C_CO_DW_STORAGE_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_CO_9 FOR CO After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE employer_type CHAR(1);
DECLARE VARIABLE co_locations_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  table_change_nbr = rdb$get_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR');
  IF (table_change_nbr IS NULL) THEN
    EXIT;

  last_record = 'Y';
  SELECT 'N', employer_type, co_locations_nbr  FROM co WHERE co_nbr = OLD.co_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :employer_type, :co_locations_nbr;

  /* EMPLOYER_TYPE */
  IF (last_record = 'Y' OR employer_type IS DISTINCT FROM OLD.employer_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2885, OLD.employer_type);

  /* CO_LOCATIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_locations_nbr IS NOT NULL) OR (last_record = 'N' AND co_locations_nbr IS DISTINCT FROM OLD.co_locations_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3169, OLD.co_locations_nbr);


  IF (last_record = 'Y') THEN
  BEGIN
    /* VT_HEALTHCARE_OFFSET_GL_TAG */
    IF (OLD.vt_healthcare_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 660, OLD.vt_healthcare_offset_gl_tag);

    /* UI_ROUNDING_GL_TAG */
    IF (OLD.ui_rounding_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 661, OLD.ui_rounding_gl_tag);

    /* UI_ROUNDING_OFFSET_GL_TAG */
    IF (OLD.ui_rounding_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 662, OLD.ui_rounding_offset_gl_tag);

    /* REPRINT_TO_BALANCE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 763, OLD.reprint_to_balance);

    /* SHOW_MANUAL_CHECKS_IN_ESS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 765, OLD.show_manual_checks_in_ess);

    /* PAYROLL_REQUIRES_MGR_APPROVAL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 766, OLD.payroll_requires_mgr_approval);

    /* DAYS_PRIOR_TO_CHECK_DATE */
    IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 599, OLD.days_prior_to_check_date);

    /* WC_OFFS_BANK_ACCOUNT_NBR */
    IF (OLD.wc_offs_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 603, OLD.wc_offs_bank_account_nbr);

    /* QTR_LOCK_FOR_TAX_PMTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 767, OLD.qtr_lock_for_tax_pmts);

    /* CO_MAX_AMOUNT_FOR_PAYROLL */
    IF (OLD.co_max_amount_for_payroll IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 515, OLD.co_max_amount_for_payroll);

    /* CO_MAX_AMOUNT_FOR_TAX_IMPOUND */
    IF (OLD.co_max_amount_for_tax_impound IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 516, OLD.co_max_amount_for_tax_impound);

    /* CO_MAX_AMOUNT_FOR_DD */
    IF (OLD.co_max_amount_for_dd IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 517, OLD.co_max_amount_for_dd);

    /* CO_PAYROLL_PROCESS_LIMITATIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 768, OLD.co_payroll_process_limitations);

    /* CO_ACH_PROCESS_LIMITATIONS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 769, OLD.co_ach_process_limitations);

    /* TRUST_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 770, OLD.trust_impound);

    /* TAX_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 771, OLD.tax_impound);

    /* DD_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 772, OLD.dd_impound);

    /* BILLING_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 773, OLD.billing_impound);

    /* WC_IMPOUND */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 774, OLD.wc_impound);

    /* COBRA_CREDIT_GL_TAG */
    IF (OLD.cobra_credit_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 663, OLD.cobra_credit_gl_tag);

    /* CO_EXCEPTION_PAYMENT_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 775, OLD.co_exception_payment_type);

    /* LAST_TAX_RETURN */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 776, OLD.last_tax_return);

    /* ENABLE_HR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 764, OLD.enable_hr);

    /* ENABLE_ESS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 777, OLD.enable_ess);

    /* DUNS_AND_BRADSTREET */
    IF (OLD.duns_and_bradstreet IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 519, OLD.duns_and_bradstreet);

    /* BANK_ACCOUNT_REGISTER_NAME */
    IF (OLD.bank_account_register_name IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 780, OLD.bank_account_register_name);

    /* ENABLE_BENEFITS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2883, OLD.enable_benefits);

    /* ENABLE_TIME_OFF */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2884, OLD.enable_time_off);

    /* WC_FISCAL_YEAR_BEGIN */
    IF (OLD.wc_fiscal_year_begin IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2886, OLD.wc_fiscal_year_begin);

    /* SHOW_PAYSTUBS_ESS_DAYS */
    IF (OLD.show_paystubs_ess_days IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2888, OLD.show_paystubs_ess_days);

    /* ANNUAL_FORM_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3200, OLD.annual_form_type);

    /* PRENOTE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3201, OLD.prenote);

    /* ENABLE_DD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3202, OLD.enable_dd);

    /* ESS_OR_EP */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3203, OLD.ess_or_ep);

    /* ACA_EDUCATION_ORG */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3204, OLD.aca_education_org);

    /* ACA_STANDARD_HOURS */
    IF (OLD.aca_standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3205, OLD.aca_standard_hours);

    /* ACA_DEFAULT_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3206, OLD.aca_default_status);

    /* ACA_CL_CO_CONSOLIDATION_NBR */
    IF (OLD.aca_cl_co_consolidation_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3207, OLD.aca_cl_co_consolidation_nbr);

    /* ACA_INITIAL_PERIOD_FROM */
    IF (OLD.aca_initial_period_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3208, OLD.aca_initial_period_from);

    /* ACA_INITIAL_PERIOD_TO */
    IF (OLD.aca_initial_period_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3209, OLD.aca_initial_period_to);

    /* ACA_STABILITY_PERIOD_FROM */
    IF (OLD.aca_stability_period_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3210, OLD.aca_stability_period_from);

    /* ACA_STABILITY_PERIOD_TO */
    IF (OLD.aca_stability_period_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3211, OLD.aca_stability_period_to);

    /* ACA_USE_AVG_HOURS_WORKED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3212, OLD.aca_use_avg_hours_worked);

    /* ACA_AVG_HOURS_WORKED_PERIOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3213, OLD.aca_avg_hours_worked_period);

    /* ACA_ADD_EARN_CL_E_D_GROUPS_NBR */
    IF (OLD.aca_add_earn_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3227, OLD.aca_add_earn_cl_e_d_groups_nbr);

    /* EE_PRINT_VOUCHER_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3240, OLD.ee_print_voucher_default);

    /* ENABLE_ANALYTICS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3268, OLD.enable_analytics);

    /* ANALYTICS_LICENSE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3269, OLD.analytics_license);

    /* BENEFITS_ELIGIBLE_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3288, OLD.benefits_eligible_default);

  END

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', NULL);
END

^

CREATE TRIGGER T_AIU_CO_3 FOR CO After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.custom_company_number IS DISTINCT FROM NEW.custom_company_number) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.referred_by IS DISTINCT FROM NEW.referred_by) OR (OLD.sb_referrals_nbr IS DISTINCT FROM NEW.sb_referrals_nbr) OR (OLD.start_date IS DISTINCT FROM NEW.start_date) OR (rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES') IS NOT NULL) OR (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) OR (OLD.accountant_contact IS DISTINCT FROM NEW.accountant_contact) OR (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr) OR (OLD.remote IS DISTINCT FROM NEW.remote) OR (OLD.business_start_date IS DISTINCT FROM NEW.business_start_date) OR (OLD.corporation_type IS DISTINCT FROM NEW.corporation_type) OR (rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS') IS NOT NULL) OR (OLD.restaurant IS DISTINCT FROM NEW.restaurant) OR (OLD.days_open IS DISTINCT FROM NEW.days_open) OR (OLD.time_open IS DISTINCT FROM NEW.time_open) OR (OLD.time_close IS DISTINCT FROM NEW.time_close) OR (rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES') IS NOT NULL) OR (OLD.successor_company IS DISTINCT FROM NEW.successor_company) OR (OLD.medical_plan IS DISTINCT FROM NEW.medical_plan) OR (OLD.retirement_age IS DISTINCT FROM NEW.retirement_age) OR (OLD.pay_frequencies IS DISTINCT FROM NEW.pay_frequencies) OR (OLD.general_ledger_format_string IS DISTINCT FROM NEW.general_ledger_format_string) OR (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr) OR (OLD.dbdt_level IS DISTINCT FROM NEW.dbdt_level) OR (OLD.billing_level IS DISTINCT FROM NEW.billing_level) OR (OLD.bank_account_level IS DISTINCT FROM NEW.bank_account_level) OR (OLD.billing_sb_bank_account_nbr IS DISTINCT FROM NEW.billing_sb_bank_account_nbr) OR (OLD.tax_sb_bank_account_nbr IS DISTINCT FROM NEW.tax_sb_bank_account_nbr) OR (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_pr IS DISTINCT FROM NEW.debit_number_days_prior_pr) OR (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_tax IS DISTINCT FROM NEW.debit_number_of_days_prior_tax) OR (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_bill IS DISTINCT FROM NEW.debit_number_days_prior_bill) OR (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_dd IS DISTINCT FROM NEW.debit_number_of_days_prior_dd) OR (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr) OR (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr) OR (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr) OR (OLD.tax_service IS DISTINCT FROM NEW.tax_service) OR (OLD.tax_service_start_date IS DISTINCT FROM NEW.tax_service_start_date) OR (OLD.tax_service_end_date IS DISTINCT FROM NEW.tax_service_end_date) OR (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr) OR (OLD.workers_comp_policy_id IS DISTINCT FROM NEW.workers_comp_policy_id) OR (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_wc IS DISTINCT FROM NEW.debit_number_days_prior_wc) OR (OLD.w_comp_sb_bank_account_nbr IS DISTINCT FROM NEW.w_comp_sb_bank_account_nbr) OR (OLD.eftps_enrollment_status IS DISTINCT FROM NEW.eftps_enrollment_status) OR (OLD.eftps_enrollment_date IS DISTINCT FROM NEW.eftps_enrollment_date) OR (OLD.eftps_sequence_number IS DISTINCT FROM NEW.eftps_sequence_number) OR (OLD.eftps_enrollment_number IS DISTINCT FROM NEW.eftps_enrollment_number) OR (OLD.eftps_name IS DISTINCT FROM NEW.eftps_name) OR (OLD.pin_number IS DISTINCT FROM NEW.pin_number) OR (OLD.ach_sb_bank_account_nbr IS DISTINCT FROM NEW.ach_sb_bank_account_nbr) OR (OLD.trust_service IS DISTINCT FROM NEW.trust_service) OR (OLD.trust_sb_account_nbr IS DISTINCT FROM NEW.trust_sb_account_nbr) OR (OLD.trust_service_start_date IS DISTINCT FROM NEW.trust_service_start_date) OR (OLD.trust_service_end_date IS DISTINCT FROM NEW.trust_service_end_date) OR (OLD.obc IS DISTINCT FROM NEW.obc) OR (OLD.sb_obc_account_nbr IS DISTINCT FROM NEW.sb_obc_account_nbr) OR (OLD.obc_start_date IS DISTINCT FROM NEW.obc_start_date) OR (OLD.obc_end_date IS DISTINCT FROM NEW.obc_end_date) OR (OLD.sy_fed_reporting_agency_nbr IS DISTINCT FROM NEW.sy_fed_reporting_agency_nbr) OR (OLD.sy_fed_tax_payment_agency_nbr IS DISTINCT FROM NEW.sy_fed_tax_payment_agency_nbr) OR (OLD.federal_tax_transfer_method IS DISTINCT FROM NEW.federal_tax_transfer_method) OR (OLD.federal_tax_payment_method IS DISTINCT FROM NEW.federal_tax_payment_method) OR (OLD.fui_sy_tax_payment_agency IS DISTINCT FROM NEW.fui_sy_tax_payment_agency) OR (OLD.fui_sy_tax_report_agency IS DISTINCT FROM NEW.fui_sy_tax_report_agency) OR (OLD.external_tax_export IS DISTINCT FROM NEW.external_tax_export) OR (OLD.non_profit IS DISTINCT FROM NEW.non_profit) OR (OLD.primary_sort_field IS DISTINCT FROM NEW.primary_sort_field) OR (OLD.secondary_sort_field IS DISTINCT FROM NEW.secondary_sort_field) OR (OLD.fiscal_year_end IS DISTINCT FROM NEW.fiscal_year_end) OR (OLD.third_party_in_gross_pr_report IS DISTINCT FROM NEW.third_party_in_gross_pr_report) OR (OLD.sic_code IS DISTINCT FROM NEW.sic_code) OR (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) OR (OLD.autopay_company IS DISTINCT FROM NEW.autopay_company) OR (OLD.pay_frequency_hourly_default IS DISTINCT FROM NEW.pay_frequency_hourly_default) OR (OLD.pay_frequency_salary_default IS DISTINCT FROM NEW.pay_frequency_salary_default) OR (OLD.co_check_primary_sort IS DISTINCT FROM NEW.co_check_primary_sort) OR (OLD.co_check_secondary_sort IS DISTINCT FROM NEW.co_check_secondary_sort) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr) OR (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr) OR (OLD.smoker_default IS DISTINCT FROM NEW.smoker_default) OR (OLD.auto_labor_dist_show_deducts IS DISTINCT FROM NEW.auto_labor_dist_show_deducts) OR (OLD.auto_labor_dist_level IS DISTINCT FROM NEW.auto_labor_dist_level) OR (OLD.distribute_deductions_default IS DISTINCT FROM NEW.distribute_deductions_default) OR (OLD.withholding_default IS DISTINCT FROM NEW.withholding_default) OR (OLD.check_type IS DISTINCT FROM NEW.check_type) OR (OLD.show_shifts_on_check IS DISTINCT FROM NEW.show_shifts_on_check) OR (OLD.show_ytd_on_check IS DISTINCT FROM NEW.show_ytd_on_check) OR (OLD.show_ein_number_on_check IS DISTINCT FROM NEW.show_ein_number_on_check) OR (OLD.show_ss_number_on_check IS DISTINCT FROM NEW.show_ss_number_on_check) OR (OLD.time_off_accrual IS DISTINCT FROM NEW.time_off_accrual) OR (OLD.check_form IS DISTINCT FROM NEW.check_form) OR (OLD.print_manual_check_stubs IS DISTINCT FROM NEW.print_manual_check_stubs) OR (OLD.credit_hold IS DISTINCT FROM NEW.credit_hold) OR (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr) OR (OLD.payroll_password IS DISTINCT FROM NEW.payroll_password) OR (OLD.remote_of_client IS DISTINCT FROM NEW.remote_of_client) OR (OLD.hardware_o_s IS DISTINCT FROM NEW.hardware_o_s) OR (OLD.network IS DISTINCT FROM NEW.network) OR (OLD.modem_speed IS DISTINCT FROM NEW.modem_speed) OR (OLD.modem_connection_type IS DISTINCT FROM NEW.modem_connection_type) OR (OLD.network_administrator IS DISTINCT FROM NEW.network_administrator) OR (OLD.network_administrator_phone IS DISTINCT FROM NEW.network_administrator_phone) OR (OLD.network_admin_phone_type IS DISTINCT FROM NEW.network_admin_phone_type) OR (OLD.external_network_administrator IS DISTINCT FROM NEW.external_network_administrator) OR (OLD.transmission_destination IS DISTINCT FROM NEW.transmission_destination) OR (OLD.last_call_in_date IS DISTINCT FROM NEW.last_call_in_date) OR (OLD.setup_completed IS DISTINCT FROM NEW.setup_completed) OR (OLD.first_monthly_payroll_day IS DISTINCT FROM NEW.first_monthly_payroll_day) OR (OLD.second_monthly_payroll_day IS DISTINCT FROM NEW.second_monthly_payroll_day) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.collate_checks IS DISTINCT FROM NEW.collate_checks) OR (OLD.process_priority IS DISTINCT FROM NEW.process_priority) OR (OLD.check_message IS DISTINCT FROM NEW.check_message) OR (OLD.customer_service_sb_user_nbr IS DISTINCT FROM NEW.customer_service_sb_user_nbr) OR (rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES') IS NOT NULL) OR (rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES') IS NOT NULL) OR (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.billing_general_ledger_tag IS DISTINCT FROM NEW.billing_general_ledger_tag) OR (OLD.federal_general_ledger_tag IS DISTINCT FROM NEW.federal_general_ledger_tag) OR (OLD.ee_oasdi_general_ledger_tag IS DISTINCT FROM NEW.ee_oasdi_general_ledger_tag) OR (OLD.er_oasdi_general_ledger_tag IS DISTINCT FROM NEW.er_oasdi_general_ledger_tag) OR (OLD.ee_medicare_general_ledger_tag IS DISTINCT FROM NEW.ee_medicare_general_ledger_tag) OR (OLD.er_medicare_general_ledger_tag IS DISTINCT FROM NEW.er_medicare_general_ledger_tag) OR (OLD.fui_general_ledger_tag IS DISTINCT FROM NEW.fui_general_ledger_tag) OR (OLD.eic_general_ledger_tag IS DISTINCT FROM NEW.eic_general_ledger_tag) OR (OLD.backup_w_general_ledger_tag IS DISTINCT FROM NEW.backup_w_general_ledger_tag) OR (OLD.net_pay_general_ledger_tag IS DISTINCT FROM NEW.net_pay_general_ledger_tag) OR (OLD.tax_imp_general_ledger_tag IS DISTINCT FROM NEW.tax_imp_general_ledger_tag) OR (OLD.trust_imp_general_ledger_tag IS DISTINCT FROM NEW.trust_imp_general_ledger_tag) OR (OLD.thrd_p_tax_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_tax_general_ledger_tag) OR (OLD.thrd_p_chk_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_chk_general_ledger_tag) OR (OLD.trust_chk_general_ledger_tag IS DISTINCT FROM NEW.trust_chk_general_ledger_tag) OR (OLD.federal_offset_gl_tag IS DISTINCT FROM NEW.federal_offset_gl_tag) OR (OLD.ee_oasdi_offset_gl_tag IS DISTINCT FROM NEW.ee_oasdi_offset_gl_tag) OR (OLD.er_oasdi_offset_gl_tag IS DISTINCT FROM NEW.er_oasdi_offset_gl_tag) OR (OLD.ee_medicare_offset_gl_tag IS DISTINCT FROM NEW.ee_medicare_offset_gl_tag) OR (OLD.er_medicare_offset_gl_tag IS DISTINCT FROM NEW.er_medicare_offset_gl_tag) OR (OLD.fui_offset_gl_tag IS DISTINCT FROM NEW.fui_offset_gl_tag) OR (OLD.eic_offset_gl_tag IS DISTINCT FROM NEW.eic_offset_gl_tag) OR (OLD.backup_w_offset_gl_tag IS DISTINCT FROM NEW.backup_w_offset_gl_tag) OR (OLD.trust_imp_offset_gl_tag IS DISTINCT FROM NEW.trust_imp_offset_gl_tag) OR (OLD.billing_exp_gl_tag IS DISTINCT FROM NEW.billing_exp_gl_tag) OR (OLD.er_oasdi_exp_gl_tag IS DISTINCT FROM NEW.er_oasdi_exp_gl_tag) OR (OLD.er_medicare_exp_gl_tag IS DISTINCT FROM NEW.er_medicare_exp_gl_tag) OR (OLD.auto_enlist IS DISTINCT FROM NEW.auto_enlist) OR (OLD.calculate_locals_first IS DISTINCT FROM NEW.calculate_locals_first) OR (OLD.weekend_action IS DISTINCT FROM NEW.weekend_action) OR (OLD.last_fui_correction IS DISTINCT FROM NEW.last_fui_correction) OR (OLD.last_sui_correction IS DISTINCT FROM NEW.last_sui_correction) OR (OLD.last_quarter_end_correction IS DISTINCT FROM NEW.last_quarter_end_correction) OR (OLD.last_preprocess IS DISTINCT FROM NEW.last_preprocess) OR (OLD.last_preprocess_message IS DISTINCT FROM NEW.last_preprocess_message) OR (OLD.charge_cobra_admin_fee IS DISTINCT FROM NEW.charge_cobra_admin_fee) OR (OLD.cobra_fee_day_of_month_due IS DISTINCT FROM NEW.cobra_fee_day_of_month_due) OR (OLD.cobra_notification_days IS DISTINCT FROM NEW.cobra_notification_days) OR (OLD.cobra_eligibility_confirm_days IS DISTINCT FROM NEW.cobra_eligibility_confirm_days) OR (OLD.show_rates_on_checks IS DISTINCT FROM NEW.show_rates_on_checks) OR (OLD.show_dir_dep_nbr_on_checks IS DISTINCT FROM NEW.show_dir_dep_nbr_on_checks) OR (OLD.fed_943_tax_deposit_frequency IS DISTINCT FROM NEW.fed_943_tax_deposit_frequency) OR (OLD.impound_workers_comp IS DISTINCT FROM NEW.impound_workers_comp) OR (OLD.reverse_check_printing IS DISTINCT FROM NEW.reverse_check_printing) OR (OLD.lock_date IS DISTINCT FROM NEW.lock_date) OR (OLD.auto_increment IS DISTINCT FROM NEW.auto_increment) OR (OLD.maximum_group_term_life IS DISTINCT FROM NEW.maximum_group_term_life) OR (OLD.payrate_precision IS DISTINCT FROM NEW.payrate_precision) OR (OLD.average_hours IS DISTINCT FROM NEW.average_hours) OR (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) OR (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) OR (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) OR (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) OR (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) OR (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) OR (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) OR (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) OR (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) OR (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) OR (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) OR (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) OR (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) OR (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) OR (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) OR (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) OR (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) OR (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) OR (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) OR (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) OR (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) OR (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) OR (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) OR (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) OR (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) OR (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) OR (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) OR (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) OR (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) OR (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) OR (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) OR (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) OR (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) OR (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) OR (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) OR (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) OR (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) OR (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) OR (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) OR (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) OR (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) OR (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) OR (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) OR (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) OR (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) OR (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) OR (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) OR (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) OR (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) OR (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) OR (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) OR (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) OR (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) OR (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) OR (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) OR (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) OR (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) OR (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) OR (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) OR (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) OR (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) OR (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) OR (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) OR (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days) OR (OLD.annual_form_type IS DISTINCT FROM NEW.annual_form_type) OR (OLD.prenote IS DISTINCT FROM NEW.prenote) OR (OLD.enable_dd IS DISTINCT FROM NEW.enable_dd) OR (OLD.ess_or_ep IS DISTINCT FROM NEW.ess_or_ep) OR (OLD.aca_education_org IS DISTINCT FROM NEW.aca_education_org) OR (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) OR (OLD.aca_default_status IS DISTINCT FROM NEW.aca_default_status) OR (OLD.aca_cl_co_consolidation_nbr IS DISTINCT FROM NEW.aca_cl_co_consolidation_nbr) OR (OLD.aca_initial_period_from IS DISTINCT FROM NEW.aca_initial_period_from) OR (OLD.aca_initial_period_to IS DISTINCT FROM NEW.aca_initial_period_to) OR (OLD.aca_stability_period_from IS DISTINCT FROM NEW.aca_stability_period_from) OR (OLD.aca_stability_period_to IS DISTINCT FROM NEW.aca_stability_period_to) OR (OLD.aca_use_avg_hours_worked IS DISTINCT FROM NEW.aca_use_avg_hours_worked) OR (OLD.aca_avg_hours_worked_period IS DISTINCT FROM NEW.aca_avg_hours_worked_period) OR (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr) OR (OLD.ee_print_voucher_default IS DISTINCT FROM NEW.ee_print_voucher_default) OR (OLD.enable_analytics IS DISTINCT FROM NEW.enable_analytics) OR (OLD.analytics_license IS DISTINCT FROM NEW.analytics_license) OR (OLD.benefits_eligible_default IS DISTINCT FROM NEW.benefits_eligible_default)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE co SET 
      custom_company_number = NEW.custom_company_number, county = NEW.county, e_mail_address = NEW.e_mail_address, referred_by = NEW.referred_by, sb_referrals_nbr = NEW.sb_referrals_nbr, start_date = NEW.start_date, termination_notes = NEW.termination_notes, sb_accountant_nbr = NEW.sb_accountant_nbr, accountant_contact = NEW.accountant_contact, union_cl_e_ds_nbr = NEW.union_cl_e_ds_nbr, remote = NEW.remote, business_start_date = NEW.business_start_date, corporation_type = NEW.corporation_type, nature_of_business = NEW.nature_of_business, restaurant = NEW.restaurant, days_open = NEW.days_open, time_open = NEW.time_open, time_close = NEW.time_close, company_notes = NEW.company_notes, successor_company = NEW.successor_company, medical_plan = NEW.medical_plan, retirement_age = NEW.retirement_age, pay_frequencies = NEW.pay_frequencies, general_ledger_format_string = NEW.general_ledger_format_string, cl_billing_nbr = NEW.cl_billing_nbr, dbdt_level = NEW.dbdt_level, billing_level = NEW.billing_level, bank_account_level = NEW.bank_account_level, billing_sb_bank_account_nbr = NEW.billing_sb_bank_account_nbr, tax_sb_bank_account_nbr = NEW.tax_sb_bank_account_nbr, payroll_cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr, debit_number_days_prior_pr = NEW.debit_number_days_prior_pr, tax_cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr, debit_number_of_days_prior_tax = NEW.debit_number_of_days_prior_tax, billing_cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr, debit_number_days_prior_bill = NEW.debit_number_days_prior_bill, dd_cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr, debit_number_of_days_prior_dd = NEW.debit_number_of_days_prior_dd, home_co_states_nbr = NEW.home_co_states_nbr, home_sdi_co_states_nbr = NEW.home_sdi_co_states_nbr, home_sui_co_states_nbr = NEW.home_sui_co_states_nbr, tax_service = NEW.tax_service, tax_service_start_date = NEW.tax_service_start_date, tax_service_end_date = NEW.tax_service_end_date, workers_comp_cl_agency_nbr = NEW.workers_comp_cl_agency_nbr, workers_comp_policy_id = NEW.workers_comp_policy_id, w_comp_cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr, debit_number_days_prior_wc = NEW.debit_number_days_prior_wc, w_comp_sb_bank_account_nbr = NEW.w_comp_sb_bank_account_nbr, eftps_enrollment_status = NEW.eftps_enrollment_status, eftps_enrollment_date = NEW.eftps_enrollment_date, eftps_sequence_number = NEW.eftps_sequence_number, eftps_enrollment_number = NEW.eftps_enrollment_number, eftps_name = NEW.eftps_name, pin_number = NEW.pin_number, ach_sb_bank_account_nbr = NEW.ach_sb_bank_account_nbr, trust_service = NEW.trust_service, trust_sb_account_nbr = NEW.trust_sb_account_nbr, trust_service_start_date = NEW.trust_service_start_date, trust_service_end_date = NEW.trust_service_end_date, obc = NEW.obc, sb_obc_account_nbr = NEW.sb_obc_account_nbr, obc_start_date = NEW.obc_start_date, obc_end_date = NEW.obc_end_date, sy_fed_reporting_agency_nbr = NEW.sy_fed_reporting_agency_nbr, sy_fed_tax_payment_agency_nbr = NEW.sy_fed_tax_payment_agency_nbr, federal_tax_transfer_method = NEW.federal_tax_transfer_method, federal_tax_payment_method = NEW.federal_tax_payment_method, fui_sy_tax_payment_agency = NEW.fui_sy_tax_payment_agency, fui_sy_tax_report_agency = NEW.fui_sy_tax_report_agency, external_tax_export = NEW.external_tax_export, non_profit = NEW.non_profit, primary_sort_field = NEW.primary_sort_field, secondary_sort_field = NEW.secondary_sort_field, fiscal_year_end = NEW.fiscal_year_end, third_party_in_gross_pr_report = NEW.third_party_in_gross_pr_report, sic_code = NEW.sic_code, deductions_to_zero = NEW.deductions_to_zero, autopay_company = NEW.autopay_company, pay_frequency_hourly_default = NEW.pay_frequency_hourly_default, pay_frequency_salary_default = NEW.pay_frequency_salary_default, co_check_primary_sort = NEW.co_check_primary_sort, co_check_secondary_sort = NEW.co_check_secondary_sort, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, annual_cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr, quarter_cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr, smoker_default = NEW.smoker_default, auto_labor_dist_show_deducts = NEW.auto_labor_dist_show_deducts, auto_labor_dist_level = NEW.auto_labor_dist_level, distribute_deductions_default = NEW.distribute_deductions_default, withholding_default = NEW.withholding_default, check_type = NEW.check_type, show_shifts_on_check = NEW.show_shifts_on_check, show_ytd_on_check = NEW.show_ytd_on_check, show_ein_number_on_check = NEW.show_ein_number_on_check, show_ss_number_on_check = NEW.show_ss_number_on_check, time_off_accrual = NEW.time_off_accrual, check_form = NEW.check_form, print_manual_check_stubs = NEW.print_manual_check_stubs, credit_hold = NEW.credit_hold, cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr, payroll_password = NEW.payroll_password, remote_of_client = NEW.remote_of_client, hardware_o_s = NEW.hardware_o_s, network = NEW.network, modem_speed = NEW.modem_speed, modem_connection_type = NEW.modem_connection_type, network_administrator = NEW.network_administrator, network_administrator_phone = NEW.network_administrator_phone, network_admin_phone_type = NEW.network_admin_phone_type, external_network_administrator = NEW.external_network_administrator, transmission_destination = NEW.transmission_destination, last_call_in_date = NEW.last_call_in_date, setup_completed = NEW.setup_completed, first_monthly_payroll_day = NEW.first_monthly_payroll_day, second_monthly_payroll_day = NEW.second_monthly_payroll_day, filler = NEW.filler, collate_checks = NEW.collate_checks, process_priority = NEW.process_priority, check_message = NEW.check_message, customer_service_sb_user_nbr = NEW.customer_service_sb_user_nbr, invoice_notes = NEW.invoice_notes, tax_cover_letter_notes = NEW.tax_cover_letter_notes, final_tax_return = NEW.final_tax_return, general_ledger_tag = NEW.general_ledger_tag, billing_general_ledger_tag = NEW.billing_general_ledger_tag, federal_general_ledger_tag = NEW.federal_general_ledger_tag, ee_oasdi_general_ledger_tag = NEW.ee_oasdi_general_ledger_tag, er_oasdi_general_ledger_tag = NEW.er_oasdi_general_ledger_tag, ee_medicare_general_ledger_tag = NEW.ee_medicare_general_ledger_tag, er_medicare_general_ledger_tag = NEW.er_medicare_general_ledger_tag, fui_general_ledger_tag = NEW.fui_general_ledger_tag, eic_general_ledger_tag = NEW.eic_general_ledger_tag, backup_w_general_ledger_tag = NEW.backup_w_general_ledger_tag, net_pay_general_ledger_tag = NEW.net_pay_general_ledger_tag, tax_imp_general_ledger_tag = NEW.tax_imp_general_ledger_tag, trust_imp_general_ledger_tag = NEW.trust_imp_general_ledger_tag, thrd_p_tax_general_ledger_tag = NEW.thrd_p_tax_general_ledger_tag, thrd_p_chk_general_ledger_tag = NEW.thrd_p_chk_general_ledger_tag, trust_chk_general_ledger_tag = NEW.trust_chk_general_ledger_tag, federal_offset_gl_tag = NEW.federal_offset_gl_tag, ee_oasdi_offset_gl_tag = NEW.ee_oasdi_offset_gl_tag, er_oasdi_offset_gl_tag = NEW.er_oasdi_offset_gl_tag, ee_medicare_offset_gl_tag = NEW.ee_medicare_offset_gl_tag, er_medicare_offset_gl_tag = NEW.er_medicare_offset_gl_tag, fui_offset_gl_tag = NEW.fui_offset_gl_tag, eic_offset_gl_tag = NEW.eic_offset_gl_tag, backup_w_offset_gl_tag = NEW.backup_w_offset_gl_tag, trust_imp_offset_gl_tag = NEW.trust_imp_offset_gl_tag, billing_exp_gl_tag = NEW.billing_exp_gl_tag, er_oasdi_exp_gl_tag = NEW.er_oasdi_exp_gl_tag, er_medicare_exp_gl_tag = NEW.er_medicare_exp_gl_tag, auto_enlist = NEW.auto_enlist, calculate_locals_first = NEW.calculate_locals_first, weekend_action = NEW.weekend_action, last_fui_correction = NEW.last_fui_correction, last_sui_correction = NEW.last_sui_correction, last_quarter_end_correction = NEW.last_quarter_end_correction, last_preprocess = NEW.last_preprocess, last_preprocess_message = NEW.last_preprocess_message, charge_cobra_admin_fee = NEW.charge_cobra_admin_fee, cobra_fee_day_of_month_due = NEW.cobra_fee_day_of_month_due, cobra_notification_days = NEW.cobra_notification_days, cobra_eligibility_confirm_days = NEW.cobra_eligibility_confirm_days, show_rates_on_checks = NEW.show_rates_on_checks, show_dir_dep_nbr_on_checks = NEW.show_dir_dep_nbr_on_checks, fed_943_tax_deposit_frequency = NEW.fed_943_tax_deposit_frequency, impound_workers_comp = NEW.impound_workers_comp, reverse_check_printing = NEW.reverse_check_printing, lock_date = NEW.lock_date, auto_increment = NEW.auto_increment, maximum_group_term_life = NEW.maximum_group_term_life, payrate_precision = NEW.payrate_precision, average_hours = NEW.average_hours, maximum_hours_on_check = NEW.maximum_hours_on_check, maximum_dollars_on_check = NEW.maximum_dollars_on_check, discount_rate = NEW.discount_rate, mod_rate = NEW.mod_rate, minimum_tax_threshold = NEW.minimum_tax_threshold, ss_disability_admin_fee = NEW.ss_disability_admin_fee, check_time_off_avail = NEW.check_time_off_avail, agency_check_mb_group_nbr = NEW.agency_check_mb_group_nbr, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_report_mb_group_nbr = NEW.pr_report_mb_group_nbr, pr_report_second_mb_group_nbr = NEW.pr_report_second_mb_group_nbr, tax_check_mb_group_nbr = NEW.tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr = NEW.tax_return_mb_group_nbr, tax_return_second_mb_group_nbr = NEW.tax_return_second_mb_group_nbr, misc_check_form = NEW.misc_check_form, hold_return_queue = NEW.hold_return_queue, number_of_invoice_copies = NEW.number_of_invoice_copies, prorate_flat_fee_for_dbdt = NEW.prorate_flat_fee_for_dbdt, initial_effective_date = NEW.initial_effective_date, sb_other_service_nbr = NEW.sb_other_service_nbr, break_checks_by_dbdt = NEW.break_checks_by_dbdt, summarize_sui = NEW.summarize_sui, show_shortfall_check = NEW.show_shortfall_check, trust_chk_offset_gl_tag = NEW.trust_chk_offset_gl_tag, manual_cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr, wells_fargo_ach_flag = NEW.wells_fargo_ach_flag, billing_check_cpa = NEW.billing_check_cpa, sales_tax_percentage = NEW.sales_tax_percentage, exclude_r_c_b_0r_n = NEW.exclude_r_c_b_0r_n, calculate_states_first = NEW.calculate_states_first, invoice_discount = NEW.invoice_discount, discount_start_date = NEW.discount_start_date, discount_end_date = NEW.discount_end_date, show_time_clock_punch = NEW.show_time_clock_punch, auto_rd_dflt_cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr, auto_reduction_default_ees = NEW.auto_reduction_default_ees, vt_healthcare_gl_tag = NEW.vt_healthcare_gl_tag, vt_healthcare_offset_gl_tag = NEW.vt_healthcare_offset_gl_tag, ui_rounding_gl_tag = NEW.ui_rounding_gl_tag, ui_rounding_offset_gl_tag = NEW.ui_rounding_offset_gl_tag, reprint_to_balance = NEW.reprint_to_balance, show_manual_checks_in_ess = NEW.show_manual_checks_in_ess, payroll_requires_mgr_approval = NEW.payroll_requires_mgr_approval, days_prior_to_check_date = NEW.days_prior_to_check_date, wc_offs_bank_account_nbr = NEW.wc_offs_bank_account_nbr, qtr_lock_for_tax_pmts = NEW.qtr_lock_for_tax_pmts, co_max_amount_for_payroll = NEW.co_max_amount_for_payroll, co_max_amount_for_tax_impound = NEW.co_max_amount_for_tax_impound, co_max_amount_for_dd = NEW.co_max_amount_for_dd, co_payroll_process_limitations = NEW.co_payroll_process_limitations, co_ach_process_limitations = NEW.co_ach_process_limitations, trust_impound = NEW.trust_impound, tax_impound = NEW.tax_impound, dd_impound = NEW.dd_impound, billing_impound = NEW.billing_impound, wc_impound = NEW.wc_impound, cobra_credit_gl_tag = NEW.cobra_credit_gl_tag, co_exception_payment_type = NEW.co_exception_payment_type, last_tax_return = NEW.last_tax_return, enable_hr = NEW.enable_hr, enable_ess = NEW.enable_ess, duns_and_bradstreet = NEW.duns_and_bradstreet, bank_account_register_name = NEW.bank_account_register_name, enable_benefits = NEW.enable_benefits, enable_time_off = NEW.enable_time_off, wc_fiscal_year_begin = NEW.wc_fiscal_year_begin, show_paystubs_ess_days = NEW.show_paystubs_ess_days, annual_form_type = NEW.annual_form_type, prenote = NEW.prenote, enable_dd = NEW.enable_dd, ess_or_ep = NEW.ess_or_ep, aca_education_org = NEW.aca_education_org, aca_standard_hours = NEW.aca_standard_hours, aca_default_status = NEW.aca_default_status, aca_cl_co_consolidation_nbr = NEW.aca_cl_co_consolidation_nbr, aca_initial_period_from = NEW.aca_initial_period_from, aca_initial_period_to = NEW.aca_initial_period_to, aca_stability_period_from = NEW.aca_stability_period_from, aca_stability_period_to = NEW.aca_stability_period_to, aca_use_avg_hours_worked = NEW.aca_use_avg_hours_worked, aca_avg_hours_worked_period = NEW.aca_avg_hours_worked_period, aca_add_earn_cl_e_d_groups_nbr = NEW.aca_add_earn_cl_e_d_groups_nbr, ee_print_voucher_default = NEW.ee_print_voucher_default, enable_analytics = NEW.enable_analytics, analytics_license = NEW.analytics_license, benefits_eligible_default = NEW.benefits_eligible_default
      WHERE co_nbr = NEW.co_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AUD_CO_2 FOR CO After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co WHERE co_nbr = OLD.co_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co WHERE co_nbr = OLD.co_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CL_CO_CONSOLIDATION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_co_consolidation
    WHERE (primary_co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'cl_co_consolidation', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM cl_co_consolidation
    WHERE (primary_co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'cl_co_consolidation', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_WORKERS_COMP */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_workers_comp', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_workers_comp', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_STATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_states', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_states
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_states', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_ADDITIONAL_INFO_NAMES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_additional_info_names
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_additional_info_names', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_additional_info_names
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_additional_info_names', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_ADDITIONAL_INFO_VALUES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_additional_info_values
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_additional_info_values', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_additional_info_values
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_additional_info_values', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_AUTO_ENLIST_RETURNS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_auto_enlist_returns
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_auto_enlist_returns', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_auto_enlist_returns
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_auto_enlist_returns', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SUI */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_sui
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_sui', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_sui
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_sui', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_E_D_CODES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_e_d_codes
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_e_d_codes', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_e_d_codes
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_e_d_codes', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DIVISION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_division
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_division', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_division
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_division', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_LOCAL_TAX */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_local_tax
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_local_tax', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_local_tax
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_local_tax', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_JOBS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_jobs
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_jobs', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_jobs
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_jobs', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_JOB_GROUPS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_job_groups
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_job_groups', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_job_groups
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_job_groups', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_UNIONS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_unions
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_unions', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_unions
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_unions', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_APPLICANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_RECRUITERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_recruiters
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_recruiters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_recruiters
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_recruiters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_REFERRALS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_referrals
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_referrals', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_referrals
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_referrals', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_CAR */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_car
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_car', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_car
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_car', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SUPERVISORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_POSITIONS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_positions
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_positions', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_positions
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_positions', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_ATTENDANCE_TYPES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_attendance_types
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_attendance_types', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_attendance_types
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_attendance_types', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_PERFORMANCE_RATINGS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_performance_ratings
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_performance_ratings', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_performance_ratings
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_performance_ratings', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SALARY_GRADES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_salary_grades
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_salary_grades', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_salary_grades
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_salary_grades', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_PROPERTY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_property
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_property', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_property
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_hr_property', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PENSIONS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pensions
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pensions', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pensions
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pensions', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PHONE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_phone
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_phone', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_phone
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_phone', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SHIFTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_shifts
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_shifts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_shifts
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_shifts', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PAY_GROUP */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pay_group
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pay_group', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pay_group
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pay_group', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_CHECK_TEMPLATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_check_templates
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_check_templates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_check_templates
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_check_templates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_GROUP */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_group
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_group', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_group
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_group', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SALESPERSON */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_salesperson
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_salesperson', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_salesperson
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_salesperson', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_REPORTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_reports
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_reports', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_reports
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_reports', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SERVICES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_services
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_services', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_services
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_services', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'pr', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'pr', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TAX_DEPOSITS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_deposits
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_tax_deposits', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_deposits
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_tax_deposits', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR_SCHEDULED_EVENT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_scheduled_event
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'pr_scheduled_event', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_scheduled_event
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'pr_scheduled_event', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_MANUAL_ACH */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_manual_ach
    WHERE (from_co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_manual_ach', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_manual_ach
    WHERE (from_co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_manual_ach', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_MANUAL_ACH */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_manual_ach
    WHERE (to_co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_manual_ach', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_manual_ach
    WHERE (to_co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_manual_ach', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_MANUAL_ACH */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_manual_ach
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_manual_ach', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_manual_ach
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_manual_ach', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_ACH */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_ach
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_ach', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_ach
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_ach', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TAX_PAYMENT_ACH */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_payment_ach
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_tax_payment_ach', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_payment_ach
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_tax_payment_ach', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BANK_ACCOUNT_REGISTER */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_bank_account_register
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_bank_account_register', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_bank_account_register
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_bank_account_register', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BILLING_HISTORY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_CALENDAR_DEFAULTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_calendar_defaults
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_calendar_defaults', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_calendar_defaults
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_calendar_defaults', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_ENLIST_GROUPS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_enlist_groups
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_enlist_groups', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_enlist_groups
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_enlist_groups', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_FED_TAX_LIABILITIES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_fed_tax_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_fed_tax_liabilities', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_fed_tax_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_fed_tax_liabilities', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_STATE_TAX_LIABILITIES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_state_tax_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_state_tax_liabilities', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_state_tax_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_state_tax_liabilities', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_LOCAL_TAX_LIABILITIES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_local_tax_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_local_tax_liabilities', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_local_tax_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_local_tax_liabilities', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_SUI_LIABILITIES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_sui_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_sui_liabilities', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_sui_liabilities
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_sui_liabilities', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_GENERAL_LEDGER */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_general_ledger
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_general_ledger', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_general_ledger
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_general_ledger', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_BATCH_DEFLT_ED */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_batch_deflt_ed
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_batch_deflt_ed
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_STORAGE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_storage
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_storage', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_storage
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_storage', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TAX_RETURN_QUEUE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_return_queue
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_tax_return_queue', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_tax_return_queue
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_tax_return_queue', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TIME_OFF_ACCRUAL */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_time_off_accrual
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_time_off_accrual', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_time_off_accrual
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_time_off_accrual', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_AUTOLABOR_DISTRIBUTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_ADDITIONAL_INFO */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_additional_info
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_additional_info', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_additional_info
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_additional_info', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFITS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefits
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefits', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefits
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefits', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_CATEGORY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_category
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_category', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_category
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_category', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_DISCOUNT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_discount
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_discount', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_discount
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_discount', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_PACKAGE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_package
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_package', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_package
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_package', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_PROVIDERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_providers
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_providers', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_providers
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_providers', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_SETUP */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_setup
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_setup', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_setup
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_benefit_setup', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_USER_REPORTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_user_reports
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_user_reports', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_user_reports
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_user_reports', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_LOCATIONS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_locations
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_locations', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_locations
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_locations', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_QEC_RUN */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_qec_run
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_qec_run', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_qec_run
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_qec_run', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DOCUMENTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_documents
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_documents', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_documents
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_documents', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_CUSTOM_LABELS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_custom_labels
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_custom_labels', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_custom_labels
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_custom_labels', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DW_STORAGE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_dw_storage
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_dw_storage', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_dw_storage
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_dw_storage', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_ACA_CERT_ELIGIBILITY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_aca_cert_eligibility
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_aca_cert_eligibility', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_aca_cert_eligibility
    WHERE (co_nbr = OLD.co_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co', OLD.co_nbr, 'co_aca_cert_eligibility', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_CO_8 FOR CO After Update POSITION 8
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(37, NEW.rec_version, NEW.co_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 492, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2628, OLD.effective_until);
    changes = changes + 1;
  END

  /* CUSTOM_COMPANY_NUMBER */
  IF (OLD.custom_company_number IS DISTINCT FROM NEW.custom_company_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 606, OLD.custom_company_number);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 607, OLD.name);
    changes = changes + 1;
  END

  /* DBA */
  IF (OLD.dba IS DISTINCT FROM NEW.dba) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 608, OLD.dba);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 493, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 494, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 609, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 610, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 524, OLD.zip_code);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 525, OLD.county);
    changes = changes + 1;
  END

  /* LEGAL_NAME */
  IF (OLD.legal_name IS DISTINCT FROM NEW.legal_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 611, OLD.legal_name);
    changes = changes + 1;
  END

  /* LEGAL_ADDRESS1 */
  IF (OLD.legal_address1 IS DISTINCT FROM NEW.legal_address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 495, OLD.legal_address1);
    changes = changes + 1;
  END

  /* LEGAL_ADDRESS2 */
  IF (OLD.legal_address2 IS DISTINCT FROM NEW.legal_address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 496, OLD.legal_address2);
    changes = changes + 1;
  END

  /* LEGAL_CITY */
  IF (OLD.legal_city IS DISTINCT FROM NEW.legal_city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 612, OLD.legal_city);
    changes = changes + 1;
  END

  /* LEGAL_STATE */
  IF (OLD.legal_state IS DISTINCT FROM NEW.legal_state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 613, OLD.legal_state);
    changes = changes + 1;
  END

  /* LEGAL_ZIP_CODE */
  IF (OLD.legal_zip_code IS DISTINCT FROM NEW.legal_zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 526, OLD.legal_zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 614, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* REFERRED_BY */
  IF (OLD.referred_by IS DISTINCT FROM NEW.referred_by) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 615, OLD.referred_by);
    changes = changes + 1;
  END

  /* SB_REFERRALS_NBR */
  IF (OLD.sb_referrals_nbr IS DISTINCT FROM NEW.sb_referrals_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 527, OLD.sb_referrals_nbr);
    changes = changes + 1;
  END

  /* START_DATE */
  IF (OLD.start_date IS DISTINCT FROM NEW.start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 577, OLD.start_date);
    changes = changes + 1;
  END

  /* TERMINATION_DATE */
  IF (OLD.termination_date IS DISTINCT FROM NEW.termination_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 578, OLD.termination_date);
    changes = changes + 1;
  END

  /* TERMINATION_CODE */
  IF (OLD.termination_code IS DISTINCT FROM NEW.termination_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 675, OLD.termination_code);
    changes = changes + 1;
  END

  /* TERMINATION_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TERMINATION_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 497, :blob_nbr);
    changes = changes + 1;
  END

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 528, OLD.sb_accountant_nbr);
    changes = changes + 1;
  END

  /* ACCOUNTANT_CONTACT */
  IF (OLD.accountant_contact IS DISTINCT FROM NEW.accountant_contact) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 616, OLD.accountant_contact);
    changes = changes + 1;
  END

  /* PRINT_CPA */
  IF (OLD.print_cpa IS DISTINCT FROM NEW.print_cpa) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 676, OLD.print_cpa);
    changes = changes + 1;
  END

  /* UNION_CL_E_DS_NBR */
  IF (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 529, OLD.union_cl_e_ds_nbr);
    changes = changes + 1;
  END

  /* CL_COMMON_PAYMASTER_NBR */
  IF (OLD.cl_common_paymaster_nbr IS DISTINCT FROM NEW.cl_common_paymaster_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 530, OLD.cl_common_paymaster_nbr);
    changes = changes + 1;
  END

  /* CL_CO_CONSOLIDATION_NBR */
  IF (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 531, OLD.cl_co_consolidation_nbr);
    changes = changes + 1;
  END

  /* FEIN */
  IF (OLD.fein IS DISTINCT FROM NEW.fein) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 677, OLD.fein);
    changes = changes + 1;
  END

  /* REMOTE */
  IF (OLD.remote IS DISTINCT FROM NEW.remote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 678, OLD.remote);
    changes = changes + 1;
  END

  /* BUSINESS_START_DATE */
  IF (OLD.business_start_date IS DISTINCT FROM NEW.business_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 579, OLD.business_start_date);
    changes = changes + 1;
  END

  /* BUSINESS_TYPE */
  IF (OLD.business_type IS DISTINCT FROM NEW.business_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 679, OLD.business_type);
    changes = changes + 1;
  END

  /* CORPORATION_TYPE */
  IF (OLD.corporation_type IS DISTINCT FROM NEW.corporation_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 680, OLD.corporation_type);
    changes = changes + 1;
  END

  /* NATURE_OF_BUSINESS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 498, :blob_nbr);
    changes = changes + 1;
  END

  /* RESTAURANT */
  IF (OLD.restaurant IS DISTINCT FROM NEW.restaurant) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 681, OLD.restaurant);
    changes = changes + 1;
  END

  /* DAYS_OPEN */
  IF (OLD.days_open IS DISTINCT FROM NEW.days_open) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 532, OLD.days_open);
    changes = changes + 1;
  END

  /* TIME_OPEN */
  IF (OLD.time_open IS DISTINCT FROM NEW.time_open) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 499, OLD.time_open);
    changes = changes + 1;
  END

  /* TIME_CLOSE */
  IF (OLD.time_close IS DISTINCT FROM NEW.time_close) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 500, OLD.time_close);
    changes = changes + 1;
  END

  /* COMPANY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COMPANY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 501, :blob_nbr);
    changes = changes + 1;
  END

  /* SUCCESSOR_COMPANY */
  IF (OLD.successor_company IS DISTINCT FROM NEW.successor_company) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 682, OLD.successor_company);
    changes = changes + 1;
  END

  /* MEDICAL_PLAN */
  IF (OLD.medical_plan IS DISTINCT FROM NEW.medical_plan) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 683, OLD.medical_plan);
    changes = changes + 1;
  END

  /* RETIREMENT_AGE */
  IF (OLD.retirement_age IS DISTINCT FROM NEW.retirement_age) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 534, OLD.retirement_age);
    changes = changes + 1;
  END

  /* PAY_FREQUENCIES */
  IF (OLD.pay_frequencies IS DISTINCT FROM NEW.pay_frequencies) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 684, OLD.pay_frequencies);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_FORMAT_STRING */
  IF (OLD.general_ledger_format_string IS DISTINCT FROM NEW.general_ledger_format_string) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 502, OLD.general_ledger_format_string);
    changes = changes + 1;
  END

  /* CL_BILLING_NBR */
  IF (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 535, OLD.cl_billing_nbr);
    changes = changes + 1;
  END

  /* DBDT_LEVEL */
  IF (OLD.dbdt_level IS DISTINCT FROM NEW.dbdt_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 685, OLD.dbdt_level);
    changes = changes + 1;
  END

  /* BILLING_LEVEL */
  IF (OLD.billing_level IS DISTINCT FROM NEW.billing_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 686, OLD.billing_level);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_LEVEL */
  IF (OLD.bank_account_level IS DISTINCT FROM NEW.bank_account_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 687, OLD.bank_account_level);
    changes = changes + 1;
  END

  /* BILLING_SB_BANK_ACCOUNT_NBR */
  IF (OLD.billing_sb_bank_account_nbr IS DISTINCT FROM NEW.billing_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 536, OLD.billing_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* TAX_SB_BANK_ACCOUNT_NBR */
  IF (OLD.tax_sb_bank_account_nbr IS DISTINCT FROM NEW.tax_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 537, OLD.tax_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* PAYROLL_CL_BANK_ACCOUNT_NBR */
  IF (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 538, OLD.payroll_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_DAYS_PRIOR_PR */
  IF (OLD.debit_number_days_prior_pr IS DISTINCT FROM NEW.debit_number_days_prior_pr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 539, OLD.debit_number_days_prior_pr);
    changes = changes + 1;
  END

  /* TAX_CL_BANK_ACCOUNT_NBR */
  IF (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 540, OLD.tax_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_OF_DAYS_PRIOR_TAX */
  IF (OLD.debit_number_of_days_prior_tax IS DISTINCT FROM NEW.debit_number_of_days_prior_tax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 541, OLD.debit_number_of_days_prior_tax);
    changes = changes + 1;
  END

  /* BILLING_CL_BANK_ACCOUNT_NBR */
  IF (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 542, OLD.billing_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_DAYS_PRIOR_BILL */
  IF (OLD.debit_number_days_prior_bill IS DISTINCT FROM NEW.debit_number_days_prior_bill) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.debit_number_days_prior_bill);
    changes = changes + 1;
  END

  /* DD_CL_BANK_ACCOUNT_NBR */
  IF (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.dd_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_OF_DAYS_PRIOR_DD */
  IF (OLD.debit_number_of_days_prior_dd IS DISTINCT FROM NEW.debit_number_of_days_prior_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 545, OLD.debit_number_of_days_prior_dd);
    changes = changes + 1;
  END

  /* HOME_CO_STATES_NBR */
  IF (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 546, OLD.home_co_states_nbr);
    changes = changes + 1;
  END

  /* HOME_SDI_CO_STATES_NBR */
  IF (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 547, OLD.home_sdi_co_states_nbr);
    changes = changes + 1;
  END

  /* HOME_SUI_CO_STATES_NBR */
  IF (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 548, OLD.home_sui_co_states_nbr);
    changes = changes + 1;
  END

  /* TAX_SERVICE */
  IF (OLD.tax_service IS DISTINCT FROM NEW.tax_service) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 688, OLD.tax_service);
    changes = changes + 1;
  END

  /* TAX_SERVICE_START_DATE */
  IF (OLD.tax_service_start_date IS DISTINCT FROM NEW.tax_service_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 580, OLD.tax_service_start_date);
    changes = changes + 1;
  END

  /* TAX_SERVICE_END_DATE */
  IF (OLD.tax_service_end_date IS DISTINCT FROM NEW.tax_service_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 581, OLD.tax_service_end_date);
    changes = changes + 1;
  END

  /* USE_DBA_ON_TAX_RETURN */
  IF (OLD.use_dba_on_tax_return IS DISTINCT FROM NEW.use_dba_on_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 690, OLD.use_dba_on_tax_return);
    changes = changes + 1;
  END

  /* WORKERS_COMP_CL_AGENCY_NBR */
  IF (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 549, OLD.workers_comp_cl_agency_nbr);
    changes = changes + 1;
  END

  /* WORKERS_COMP_POLICY_ID */
  IF (OLD.workers_comp_policy_id IS DISTINCT FROM NEW.workers_comp_policy_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 617, OLD.workers_comp_policy_id);
    changes = changes + 1;
  END

  /* W_COMP_CL_BANK_ACCOUNT_NBR */
  IF (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, OLD.w_comp_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* DEBIT_NUMBER_DAYS_PRIOR_WC */
  IF (OLD.debit_number_days_prior_wc IS DISTINCT FROM NEW.debit_number_days_prior_wc) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, OLD.debit_number_days_prior_wc);
    changes = changes + 1;
  END

  /* W_COMP_SB_BANK_ACCOUNT_NBR */
  IF (OLD.w_comp_sb_bank_account_nbr IS DISTINCT FROM NEW.w_comp_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 552, OLD.w_comp_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* EFTPS_ENROLLMENT_STATUS */
  IF (OLD.eftps_enrollment_status IS DISTINCT FROM NEW.eftps_enrollment_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 691, OLD.eftps_enrollment_status);
    changes = changes + 1;
  END

  /* EFTPS_ENROLLMENT_DATE */
  IF (OLD.eftps_enrollment_date IS DISTINCT FROM NEW.eftps_enrollment_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 503, OLD.eftps_enrollment_date);
    changes = changes + 1;
  END

  /* EFTPS_SEQUENCE_NUMBER */
  IF (OLD.eftps_sequence_number IS DISTINCT FROM NEW.eftps_sequence_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 618, OLD.eftps_sequence_number);
    changes = changes + 1;
  END

  /* EFTPS_ENROLLMENT_NUMBER */
  IF (OLD.eftps_enrollment_number IS DISTINCT FROM NEW.eftps_enrollment_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 619, OLD.eftps_enrollment_number);
    changes = changes + 1;
  END

  /* EFTPS_NAME */
  IF (OLD.eftps_name IS DISTINCT FROM NEW.eftps_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 620, OLD.eftps_name);
    changes = changes + 1;
  END

  /* PIN_NUMBER */
  IF (OLD.pin_number IS DISTINCT FROM NEW.pin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 553, OLD.pin_number);
    changes = changes + 1;
  END

  /* ACH_SB_BANK_ACCOUNT_NBR */
  IF (OLD.ach_sb_bank_account_nbr IS DISTINCT FROM NEW.ach_sb_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 554, OLD.ach_sb_bank_account_nbr);
    changes = changes + 1;
  END

  /* TRUST_SERVICE */
  IF (OLD.trust_service IS DISTINCT FROM NEW.trust_service) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 692, OLD.trust_service);
    changes = changes + 1;
  END

  /* TRUST_SB_ACCOUNT_NBR */
  IF (OLD.trust_sb_account_nbr IS DISTINCT FROM NEW.trust_sb_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 555, OLD.trust_sb_account_nbr);
    changes = changes + 1;
  END

  /* TRUST_SERVICE_START_DATE */
  IF (OLD.trust_service_start_date IS DISTINCT FROM NEW.trust_service_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 582, OLD.trust_service_start_date);
    changes = changes + 1;
  END

  /* TRUST_SERVICE_END_DATE */
  IF (OLD.trust_service_end_date IS DISTINCT FROM NEW.trust_service_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 583, OLD.trust_service_end_date);
    changes = changes + 1;
  END

  /* OBC */
  IF (OLD.obc IS DISTINCT FROM NEW.obc) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 693, OLD.obc);
    changes = changes + 1;
  END

  /* SB_OBC_ACCOUNT_NBR */
  IF (OLD.sb_obc_account_nbr IS DISTINCT FROM NEW.sb_obc_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 556, OLD.sb_obc_account_nbr);
    changes = changes + 1;
  END

  /* OBC_START_DATE */
  IF (OLD.obc_start_date IS DISTINCT FROM NEW.obc_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 584, OLD.obc_start_date);
    changes = changes + 1;
  END

  /* OBC_END_DATE */
  IF (OLD.obc_end_date IS DISTINCT FROM NEW.obc_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 585, OLD.obc_end_date);
    changes = changes + 1;
  END

  /* SY_FED_REPORTING_AGENCY_NBR */
  IF (OLD.sy_fed_reporting_agency_nbr IS DISTINCT FROM NEW.sy_fed_reporting_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 557, OLD.sy_fed_reporting_agency_nbr);
    changes = changes + 1;
  END

  /* SY_FED_TAX_PAYMENT_AGENCY_NBR */
  IF (OLD.sy_fed_tax_payment_agency_nbr IS DISTINCT FROM NEW.sy_fed_tax_payment_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 558, OLD.sy_fed_tax_payment_agency_nbr);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.federal_tax_deposit_frequency IS DISTINCT FROM NEW.federal_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 694, OLD.federal_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_TRANSFER_METHOD */
  IF (OLD.federal_tax_transfer_method IS DISTINCT FROM NEW.federal_tax_transfer_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 695, OLD.federal_tax_transfer_method);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_PAYMENT_METHOD */
  IF (OLD.federal_tax_payment_method IS DISTINCT FROM NEW.federal_tax_payment_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 696, OLD.federal_tax_payment_method);
    changes = changes + 1;
  END

  /* FUI_SY_TAX_PAYMENT_AGENCY */
  IF (OLD.fui_sy_tax_payment_agency IS DISTINCT FROM NEW.fui_sy_tax_payment_agency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 559, OLD.fui_sy_tax_payment_agency);
    changes = changes + 1;
  END

  /* FUI_SY_TAX_REPORT_AGENCY */
  IF (OLD.fui_sy_tax_report_agency IS DISTINCT FROM NEW.fui_sy_tax_report_agency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 560, OLD.fui_sy_tax_report_agency);
    changes = changes + 1;
  END

  /* EXTERNAL_TAX_EXPORT */
  IF (OLD.external_tax_export IS DISTINCT FROM NEW.external_tax_export) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 698, OLD.external_tax_export);
    changes = changes + 1;
  END

  /* NON_PROFIT */
  IF (OLD.non_profit IS DISTINCT FROM NEW.non_profit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 699, OLD.non_profit);
    changes = changes + 1;
  END

  /* FEDERAL_TAX_EXEMPT_STATUS */
  IF (OLD.federal_tax_exempt_status IS DISTINCT FROM NEW.federal_tax_exempt_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 700, OLD.federal_tax_exempt_status);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_EE_OASDI */
  IF (OLD.fed_tax_exempt_ee_oasdi IS DISTINCT FROM NEW.fed_tax_exempt_ee_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 701, OLD.fed_tax_exempt_ee_oasdi);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_ER_OASDI */
  IF (OLD.fed_tax_exempt_er_oasdi IS DISTINCT FROM NEW.fed_tax_exempt_er_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 702, OLD.fed_tax_exempt_er_oasdi);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_EE_MEDICARE */
  IF (OLD.fed_tax_exempt_ee_medicare IS DISTINCT FROM NEW.fed_tax_exempt_ee_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 703, OLD.fed_tax_exempt_ee_medicare);
    changes = changes + 1;
  END

  /* FED_TAX_EXEMPT_ER_MEDICARE */
  IF (OLD.fed_tax_exempt_er_medicare IS DISTINCT FROM NEW.fed_tax_exempt_er_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 704, OLD.fed_tax_exempt_er_medicare);
    changes = changes + 1;
  END

  /* FUI_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.fui_tax_deposit_frequency IS DISTINCT FROM NEW.fui_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 705, OLD.fui_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* FUI_TAX_EXEMPT */
  IF (OLD.fui_tax_exempt IS DISTINCT FROM NEW.fui_tax_exempt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 706, OLD.fui_tax_exempt);
    changes = changes + 1;
  END

  /* PRIMARY_SORT_FIELD */
  IF (OLD.primary_sort_field IS DISTINCT FROM NEW.primary_sort_field) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 707, OLD.primary_sort_field);
    changes = changes + 1;
  END

  /* SECONDARY_SORT_FIELD */
  IF (OLD.secondary_sort_field IS DISTINCT FROM NEW.secondary_sort_field) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 708, OLD.secondary_sort_field);
    changes = changes + 1;
  END

  /* FISCAL_YEAR_END */
  IF (OLD.fiscal_year_end IS DISTINCT FROM NEW.fiscal_year_end) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 561, OLD.fiscal_year_end);
    changes = changes + 1;
  END

  /* THIRD_PARTY_IN_GROSS_PR_REPORT */
  IF (OLD.third_party_in_gross_pr_report IS DISTINCT FROM NEW.third_party_in_gross_pr_report) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 709, OLD.third_party_in_gross_pr_report);
    changes = changes + 1;
  END

  /* SIC_CODE */
  IF (OLD.sic_code IS DISTINCT FROM NEW.sic_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 562, OLD.sic_code);
    changes = changes + 1;
  END

  /* DEDUCTIONS_TO_ZERO */
  IF (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 711, OLD.deductions_to_zero);
    changes = changes + 1;
  END

  /* AUTOPAY_COMPANY */
  IF (OLD.autopay_company IS DISTINCT FROM NEW.autopay_company) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 712, OLD.autopay_company);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY_HOURLY_DEFAULT */
  IF (OLD.pay_frequency_hourly_default IS DISTINCT FROM NEW.pay_frequency_hourly_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 713, OLD.pay_frequency_hourly_default);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY_SALARY_DEFAULT */
  IF (OLD.pay_frequency_salary_default IS DISTINCT FROM NEW.pay_frequency_salary_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 714, OLD.pay_frequency_salary_default);
    changes = changes + 1;
  END

  /* CO_CHECK_PRIMARY_SORT */
  IF (OLD.co_check_primary_sort IS DISTINCT FROM NEW.co_check_primary_sort) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 715, OLD.co_check_primary_sort);
    changes = changes + 1;
  END

  /* CO_CHECK_SECONDARY_SORT */
  IF (OLD.co_check_secondary_sort IS DISTINCT FROM NEW.co_check_secondary_sort) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 716, OLD.co_check_secondary_sort);
    changes = changes + 1;
  END

  /* CL_DELIVERY_GROUP_NBR */
  IF (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 563, OLD.cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* ANNUAL_CL_DELIVERY_GROUP_NBR */
  IF (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 564, OLD.annual_cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* QUARTER_CL_DELIVERY_GROUP_NBR */
  IF (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 565, OLD.quarter_cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* SMOKER_DEFAULT */
  IF (OLD.smoker_default IS DISTINCT FROM NEW.smoker_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 717, OLD.smoker_default);
    changes = changes + 1;
  END

  /* AUTO_LABOR_DIST_SHOW_DEDUCTS */
  IF (OLD.auto_labor_dist_show_deducts IS DISTINCT FROM NEW.auto_labor_dist_show_deducts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 718, OLD.auto_labor_dist_show_deducts);
    changes = changes + 1;
  END

  /* AUTO_LABOR_DIST_LEVEL */
  IF (OLD.auto_labor_dist_level IS DISTINCT FROM NEW.auto_labor_dist_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 719, OLD.auto_labor_dist_level);
    changes = changes + 1;
  END

  /* DISTRIBUTE_DEDUCTIONS_DEFAULT */
  IF (OLD.distribute_deductions_default IS DISTINCT FROM NEW.distribute_deductions_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 720, OLD.distribute_deductions_default);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 566, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* WITHHOLDING_DEFAULT */
  IF (OLD.withholding_default IS DISTINCT FROM NEW.withholding_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 721, OLD.withholding_default);
    changes = changes + 1;
  END

  /* APPLY_MISC_LIMIT_TO_1099 */
  IF (OLD.apply_misc_limit_to_1099 IS DISTINCT FROM NEW.apply_misc_limit_to_1099) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 722, OLD.apply_misc_limit_to_1099);
    changes = changes + 1;
  END

  /* CHECK_TYPE */
  IF (OLD.check_type IS DISTINCT FROM NEW.check_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 723, OLD.check_type);
    changes = changes + 1;
  END

  /* MAKE_UP_TAX_DEDUCT_SHORTFALLS */
  IF (OLD.make_up_tax_deduct_shortfalls IS DISTINCT FROM NEW.make_up_tax_deduct_shortfalls) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 724, OLD.make_up_tax_deduct_shortfalls);
    changes = changes + 1;
  END

  /* SHOW_SHIFTS_ON_CHECK */
  IF (OLD.show_shifts_on_check IS DISTINCT FROM NEW.show_shifts_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 725, OLD.show_shifts_on_check);
    changes = changes + 1;
  END

  /* SHOW_YTD_ON_CHECK */
  IF (OLD.show_ytd_on_check IS DISTINCT FROM NEW.show_ytd_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 726, OLD.show_ytd_on_check);
    changes = changes + 1;
  END

  /* SHOW_EIN_NUMBER_ON_CHECK */
  IF (OLD.show_ein_number_on_check IS DISTINCT FROM NEW.show_ein_number_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 727, OLD.show_ein_number_on_check);
    changes = changes + 1;
  END

  /* SHOW_SS_NUMBER_ON_CHECK */
  IF (OLD.show_ss_number_on_check IS DISTINCT FROM NEW.show_ss_number_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 728, OLD.show_ss_number_on_check);
    changes = changes + 1;
  END

  /* TIME_OFF_ACCRUAL */
  IF (OLD.time_off_accrual IS DISTINCT FROM NEW.time_off_accrual) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 729, OLD.time_off_accrual);
    changes = changes + 1;
  END

  /* CHECK_FORM */
  IF (OLD.check_form IS DISTINCT FROM NEW.check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 730, OLD.check_form);
    changes = changes + 1;
  END

  /* PRINT_MANUAL_CHECK_STUBS */
  IF (OLD.print_manual_check_stubs IS DISTINCT FROM NEW.print_manual_check_stubs) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 731, OLD.print_manual_check_stubs);
    changes = changes + 1;
  END

  /* CREDIT_HOLD */
  IF (OLD.credit_hold IS DISTINCT FROM NEW.credit_hold) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 732, OLD.credit_hold);
    changes = changes + 1;
  END

  /* CL_TIMECLOCK_IMPORTS_NBR */
  IF (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 567, OLD.cl_timeclock_imports_nbr);
    changes = changes + 1;
  END

  /* PAYROLL_PASSWORD */
  IF (OLD.payroll_password IS DISTINCT FROM NEW.payroll_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 568, OLD.payroll_password);
    changes = changes + 1;
  END

  /* REMOTE_OF_CLIENT */
  IF (OLD.remote_of_client IS DISTINCT FROM NEW.remote_of_client) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 733, OLD.remote_of_client);
    changes = changes + 1;
  END

  /* HARDWARE_O_S */
  IF (OLD.hardware_o_s IS DISTINCT FROM NEW.hardware_o_s) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 734, OLD.hardware_o_s);
    changes = changes + 1;
  END

  /* NETWORK */
  IF (OLD.network IS DISTINCT FROM NEW.network) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 735, OLD.network);
    changes = changes + 1;
  END

  /* MODEM_SPEED */
  IF (OLD.modem_speed IS DISTINCT FROM NEW.modem_speed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 736, OLD.modem_speed);
    changes = changes + 1;
  END

  /* MODEM_CONNECTION_TYPE */
  IF (OLD.modem_connection_type IS DISTINCT FROM NEW.modem_connection_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 737, OLD.modem_connection_type);
    changes = changes + 1;
  END

  /* NETWORK_ADMINISTRATOR */
  IF (OLD.network_administrator IS DISTINCT FROM NEW.network_administrator) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 621, OLD.network_administrator);
    changes = changes + 1;
  END

  /* NETWORK_ADMINISTRATOR_PHONE */
  IF (OLD.network_administrator_phone IS DISTINCT FROM NEW.network_administrator_phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 622, OLD.network_administrator_phone);
    changes = changes + 1;
  END

  /* NETWORK_ADMIN_PHONE_TYPE */
  IF (OLD.network_admin_phone_type IS DISTINCT FROM NEW.network_admin_phone_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 738, OLD.network_admin_phone_type);
    changes = changes + 1;
  END

  /* EXTERNAL_NETWORK_ADMINISTRATOR */
  IF (OLD.external_network_administrator IS DISTINCT FROM NEW.external_network_administrator) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 739, OLD.external_network_administrator);
    changes = changes + 1;
  END

  /* TRANSMISSION_DESTINATION */
  IF (OLD.transmission_destination IS DISTINCT FROM NEW.transmission_destination) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 569, OLD.transmission_destination);
    changes = changes + 1;
  END

  /* LAST_CALL_IN_DATE */
  IF (OLD.last_call_in_date IS DISTINCT FROM NEW.last_call_in_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 505, OLD.last_call_in_date);
    changes = changes + 1;
  END

  /* SETUP_COMPLETED */
  IF (OLD.setup_completed IS DISTINCT FROM NEW.setup_completed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 741, OLD.setup_completed);
    changes = changes + 1;
  END

  /* FIRST_MONTHLY_PAYROLL_DAY */
  IF (OLD.first_monthly_payroll_day IS DISTINCT FROM NEW.first_monthly_payroll_day) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 570, OLD.first_monthly_payroll_day);
    changes = changes + 1;
  END

  /* SECOND_MONTHLY_PAYROLL_DAY */
  IF (OLD.second_monthly_payroll_day IS DISTINCT FROM NEW.second_monthly_payroll_day) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 571, OLD.second_monthly_payroll_day);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 624, OLD.filler);
    changes = changes + 1;
  END

  /* COLLATE_CHECKS */
  IF (OLD.collate_checks IS DISTINCT FROM NEW.collate_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 742, OLD.collate_checks);
    changes = changes + 1;
  END

  /* PROCESS_PRIORITY */
  IF (OLD.process_priority IS DISTINCT FROM NEW.process_priority) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 572, OLD.process_priority);
    changes = changes + 1;
  END

  /* CHECK_MESSAGE */
  IF (OLD.check_message IS DISTINCT FROM NEW.check_message) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 625, OLD.check_message);
    changes = changes + 1;
  END

  /* CUSTOMER_SERVICE_SB_USER_NBR */
  IF (OLD.customer_service_sb_user_nbr IS DISTINCT FROM NEW.customer_service_sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 573, OLD.customer_service_sb_user_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 506, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 507, :blob_nbr);
    changes = changes + 1;
  END

  /* FINAL_TAX_RETURN */
  IF (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 674, OLD.final_tax_return);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_TAG */
  IF (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 626, OLD.general_ledger_tag);
    changes = changes + 1;
  END

  /* BILLING_GENERAL_LEDGER_TAG */
  IF (OLD.billing_general_ledger_tag IS DISTINCT FROM NEW.billing_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 627, OLD.billing_general_ledger_tag);
    changes = changes + 1;
  END

  /* FEDERAL_GENERAL_LEDGER_TAG */
  IF (OLD.federal_general_ledger_tag IS DISTINCT FROM NEW.federal_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 628, OLD.federal_general_ledger_tag);
    changes = changes + 1;
  END

  /* EE_OASDI_GENERAL_LEDGER_TAG */
  IF (OLD.ee_oasdi_general_ledger_tag IS DISTINCT FROM NEW.ee_oasdi_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 629, OLD.ee_oasdi_general_ledger_tag);
    changes = changes + 1;
  END

  /* ER_OASDI_GENERAL_LEDGER_TAG */
  IF (OLD.er_oasdi_general_ledger_tag IS DISTINCT FROM NEW.er_oasdi_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 630, OLD.er_oasdi_general_ledger_tag);
    changes = changes + 1;
  END

  /* EE_MEDICARE_GENERAL_LEDGER_TAG */
  IF (OLD.ee_medicare_general_ledger_tag IS DISTINCT FROM NEW.ee_medicare_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 631, OLD.ee_medicare_general_ledger_tag);
    changes = changes + 1;
  END

  /* ER_MEDICARE_GENERAL_LEDGER_TAG */
  IF (OLD.er_medicare_general_ledger_tag IS DISTINCT FROM NEW.er_medicare_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 632, OLD.er_medicare_general_ledger_tag);
    changes = changes + 1;
  END

  /* FUI_GENERAL_LEDGER_TAG */
  IF (OLD.fui_general_ledger_tag IS DISTINCT FROM NEW.fui_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 633, OLD.fui_general_ledger_tag);
    changes = changes + 1;
  END

  /* EIC_GENERAL_LEDGER_TAG */
  IF (OLD.eic_general_ledger_tag IS DISTINCT FROM NEW.eic_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 634, OLD.eic_general_ledger_tag);
    changes = changes + 1;
  END

  /* BACKUP_W_GENERAL_LEDGER_TAG */
  IF (OLD.backup_w_general_ledger_tag IS DISTINCT FROM NEW.backup_w_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 635, OLD.backup_w_general_ledger_tag);
    changes = changes + 1;
  END

  /* NET_PAY_GENERAL_LEDGER_TAG */
  IF (OLD.net_pay_general_ledger_tag IS DISTINCT FROM NEW.net_pay_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 636, OLD.net_pay_general_ledger_tag);
    changes = changes + 1;
  END

  /* TAX_IMP_GENERAL_LEDGER_TAG */
  IF (OLD.tax_imp_general_ledger_tag IS DISTINCT FROM NEW.tax_imp_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 637, OLD.tax_imp_general_ledger_tag);
    changes = changes + 1;
  END

  /* TRUST_IMP_GENERAL_LEDGER_TAG */
  IF (OLD.trust_imp_general_ledger_tag IS DISTINCT FROM NEW.trust_imp_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 638, OLD.trust_imp_general_ledger_tag);
    changes = changes + 1;
  END

  /* THRD_P_TAX_GENERAL_LEDGER_TAG */
  IF (OLD.thrd_p_tax_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_tax_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 639, OLD.thrd_p_tax_general_ledger_tag);
    changes = changes + 1;
  END

  /* THRD_P_CHK_GENERAL_LEDGER_TAG */
  IF (OLD.thrd_p_chk_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_chk_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 640, OLD.thrd_p_chk_general_ledger_tag);
    changes = changes + 1;
  END

  /* TRUST_CHK_GENERAL_LEDGER_TAG */
  IF (OLD.trust_chk_general_ledger_tag IS DISTINCT FROM NEW.trust_chk_general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 641, OLD.trust_chk_general_ledger_tag);
    changes = changes + 1;
  END

  /* FEDERAL_OFFSET_GL_TAG */
  IF (OLD.federal_offset_gl_tag IS DISTINCT FROM NEW.federal_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 642, OLD.federal_offset_gl_tag);
    changes = changes + 1;
  END

  /* EE_OASDI_OFFSET_GL_TAG */
  IF (OLD.ee_oasdi_offset_gl_tag IS DISTINCT FROM NEW.ee_oasdi_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 643, OLD.ee_oasdi_offset_gl_tag);
    changes = changes + 1;
  END

  /* ER_OASDI_OFFSET_GL_TAG */
  IF (OLD.er_oasdi_offset_gl_tag IS DISTINCT FROM NEW.er_oasdi_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 644, OLD.er_oasdi_offset_gl_tag);
    changes = changes + 1;
  END

  /* EE_MEDICARE_OFFSET_GL_TAG */
  IF (OLD.ee_medicare_offset_gl_tag IS DISTINCT FROM NEW.ee_medicare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 645, OLD.ee_medicare_offset_gl_tag);
    changes = changes + 1;
  END

  /* ER_MEDICARE_OFFSET_GL_TAG */
  IF (OLD.er_medicare_offset_gl_tag IS DISTINCT FROM NEW.er_medicare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 646, OLD.er_medicare_offset_gl_tag);
    changes = changes + 1;
  END

  /* FUI_OFFSET_GL_TAG */
  IF (OLD.fui_offset_gl_tag IS DISTINCT FROM NEW.fui_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 647, OLD.fui_offset_gl_tag);
    changes = changes + 1;
  END

  /* EIC_OFFSET_GL_TAG */
  IF (OLD.eic_offset_gl_tag IS DISTINCT FROM NEW.eic_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 648, OLD.eic_offset_gl_tag);
    changes = changes + 1;
  END

  /* BACKUP_W_OFFSET_GL_TAG */
  IF (OLD.backup_w_offset_gl_tag IS DISTINCT FROM NEW.backup_w_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 649, OLD.backup_w_offset_gl_tag);
    changes = changes + 1;
  END

  /* TRUST_IMP_OFFSET_GL_TAG */
  IF (OLD.trust_imp_offset_gl_tag IS DISTINCT FROM NEW.trust_imp_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 650, OLD.trust_imp_offset_gl_tag);
    changes = changes + 1;
  END

  /* BILLING_EXP_GL_TAG */
  IF (OLD.billing_exp_gl_tag IS DISTINCT FROM NEW.billing_exp_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 651, OLD.billing_exp_gl_tag);
    changes = changes + 1;
  END

  /* ER_OASDI_EXP_GL_TAG */
  IF (OLD.er_oasdi_exp_gl_tag IS DISTINCT FROM NEW.er_oasdi_exp_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 652, OLD.er_oasdi_exp_gl_tag);
    changes = changes + 1;
  END

  /* ER_MEDICARE_EXP_GL_TAG */
  IF (OLD.er_medicare_exp_gl_tag IS DISTINCT FROM NEW.er_medicare_exp_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 653, OLD.er_medicare_exp_gl_tag);
    changes = changes + 1;
  END

  /* AUTO_ENLIST */
  IF (OLD.auto_enlist IS DISTINCT FROM NEW.auto_enlist) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 743, OLD.auto_enlist);
    changes = changes + 1;
  END

  /* CALCULATE_LOCALS_FIRST */
  IF (OLD.calculate_locals_first IS DISTINCT FROM NEW.calculate_locals_first) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 744, OLD.calculate_locals_first);
    changes = changes + 1;
  END

  /* WEEKEND_ACTION */
  IF (OLD.weekend_action IS DISTINCT FROM NEW.weekend_action) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 747, OLD.weekend_action);
    changes = changes + 1;
  END

  /* LAST_FUI_CORRECTION */
  IF (OLD.last_fui_correction IS DISTINCT FROM NEW.last_fui_correction) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 508, OLD.last_fui_correction);
    changes = changes + 1;
  END

  /* LAST_SUI_CORRECTION */
  IF (OLD.last_sui_correction IS DISTINCT FROM NEW.last_sui_correction) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 509, OLD.last_sui_correction);
    changes = changes + 1;
  END

  /* LAST_QUARTER_END_CORRECTION */
  IF (OLD.last_quarter_end_correction IS DISTINCT FROM NEW.last_quarter_end_correction) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 510, OLD.last_quarter_end_correction);
    changes = changes + 1;
  END

  /* LAST_PREPROCESS */
  IF (OLD.last_preprocess IS DISTINCT FROM NEW.last_preprocess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 511, OLD.last_preprocess);
    changes = changes + 1;
  END

  /* LAST_PREPROCESS_MESSAGE */
  IF (OLD.last_preprocess_message IS DISTINCT FROM NEW.last_preprocess_message) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 655, OLD.last_preprocess_message);
    changes = changes + 1;
  END

  /* CHARGE_COBRA_ADMIN_FEE */
  IF (OLD.charge_cobra_admin_fee IS DISTINCT FROM NEW.charge_cobra_admin_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 748, OLD.charge_cobra_admin_fee);
    changes = changes + 1;
  END

  /* COBRA_FEE_DAY_OF_MONTH_DUE */
  IF (OLD.cobra_fee_day_of_month_due IS DISTINCT FROM NEW.cobra_fee_day_of_month_due) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 574, OLD.cobra_fee_day_of_month_due);
    changes = changes + 1;
  END

  /* COBRA_NOTIFICATION_DAYS */
  IF (OLD.cobra_notification_days IS DISTINCT FROM NEW.cobra_notification_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 575, OLD.cobra_notification_days);
    changes = changes + 1;
  END

  /* COBRA_ELIGIBILITY_CONFIRM_DAYS */
  IF (OLD.cobra_eligibility_confirm_days IS DISTINCT FROM NEW.cobra_eligibility_confirm_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 576, OLD.cobra_eligibility_confirm_days);
    changes = changes + 1;
  END

  /* SHOW_RATES_ON_CHECKS */
  IF (OLD.show_rates_on_checks IS DISTINCT FROM NEW.show_rates_on_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 749, OLD.show_rates_on_checks);
    changes = changes + 1;
  END

  /* SHOW_DIR_DEP_NBR_ON_CHECKS */
  IF (OLD.show_dir_dep_nbr_on_checks IS DISTINCT FROM NEW.show_dir_dep_nbr_on_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 750, OLD.show_dir_dep_nbr_on_checks);
    changes = changes + 1;
  END

  /* FED_943_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.fed_943_tax_deposit_frequency IS DISTINCT FROM NEW.fed_943_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 672, OLD.fed_943_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* FED_945_TAX_DEPOSIT_FREQUENCY */
  IF (OLD.fed_945_tax_deposit_frequency IS DISTINCT FROM NEW.fed_945_tax_deposit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 671, OLD.fed_945_tax_deposit_frequency);
    changes = changes + 1;
  END

  /* IMPOUND_WORKERS_COMP */
  IF (OLD.impound_workers_comp IS DISTINCT FROM NEW.impound_workers_comp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 670, OLD.impound_workers_comp);
    changes = changes + 1;
  END

  /* REVERSE_CHECK_PRINTING */
  IF (OLD.reverse_check_printing IS DISTINCT FROM NEW.reverse_check_printing) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 668, OLD.reverse_check_printing);
    changes = changes + 1;
  END

  /* LOCK_DATE */
  IF (OLD.lock_date IS DISTINCT FROM NEW.lock_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 522, OLD.lock_date);
    changes = changes + 1;
  END

  /* FUI_RATE_OVERRIDE */
  IF (OLD.fui_rate_override IS DISTINCT FROM NEW.fui_rate_override) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 490, OLD.fui_rate_override);
    changes = changes + 1;
  END

  /* AUTO_INCREMENT */
  IF (OLD.auto_increment IS DISTINCT FROM NEW.auto_increment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 489, OLD.auto_increment);
    changes = changes + 1;
  END

  /* MAXIMUM_GROUP_TERM_LIFE */
  IF (OLD.maximum_group_term_life IS DISTINCT FROM NEW.maximum_group_term_life) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 488, OLD.maximum_group_term_life);
    changes = changes + 1;
  END

  /* PAYRATE_PRECISION */
  IF (OLD.payrate_precision IS DISTINCT FROM NEW.payrate_precision) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 487, OLD.payrate_precision);
    changes = changes + 1;
  END


  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', table_change_nbr);
  rdb$set_context('USER_TRANSACTION', '@CHANGES', changes);
END

^

CREATE TRIGGER T_AU_CO_9 FOR CO After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  table_change_nbr = rdb$get_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR');
  IF (table_change_nbr IS NULL) THEN
    EXIT;

  changes = rdb$get_context('USER_TRANSACTION', '@CHANGES');

  /* AVERAGE_HOURS */
  IF (OLD.average_hours IS DISTINCT FROM NEW.average_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 486, OLD.average_hours);
    changes = changes + 1;
  END

  /* MAXIMUM_HOURS_ON_CHECK */
  IF (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 485, OLD.maximum_hours_on_check);
    changes = changes + 1;
  END

  /* MAXIMUM_DOLLARS_ON_CHECK */
  IF (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 484, OLD.maximum_dollars_on_check);
    changes = changes + 1;
  END

  /* DISCOUNT_RATE */
  IF (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 483, OLD.discount_rate);
    changes = changes + 1;
  END

  /* MOD_RATE */
  IF (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.mod_rate);
    changes = changes + 1;
  END

  /* MINIMUM_TAX_THRESHOLD */
  IF (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 481, OLD.minimum_tax_threshold);
    changes = changes + 1;
  END

  /* SS_DISABILITY_ADMIN_FEE */
  IF (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 480, OLD.ss_disability_admin_fee);
    changes = changes + 1;
  END

  /* CHECK_TIME_OFF_AVAIL */
  IF (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 751, OLD.check_time_off_avail);
    changes = changes + 1;
  END

  /* AGENCY_CHECK_MB_GROUP_NBR */
  IF (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 586, OLD.agency_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 587, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 588, OLD.pr_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_REPORT_SECOND_MB_GROUP_NBR */
  IF (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 589, OLD.pr_report_second_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_CHECK_MB_GROUP_NBR */
  IF (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 590, OLD.tax_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 591, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 592, OLD.tax_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_SECOND_MB_GROUP_NBR */
  IF (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 593, OLD.tax_return_second_mb_group_nbr);
    changes = changes + 1;
  END

  /* NAME_ON_INVOICE */
  IF (OLD.name_on_invoice IS DISTINCT FROM NEW.name_on_invoice) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 754, OLD.name_on_invoice);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 755, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* HOLD_RETURN_QUEUE */
  IF (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 756, OLD.hold_return_queue);
    changes = changes + 1;
  END

  /* NUMBER_OF_INVOICE_COPIES */
  IF (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 594, OLD.number_of_invoice_copies);
    changes = changes + 1;
  END

  /* PRORATE_FLAT_FEE_FOR_DBDT */
  IF (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 757, OLD.prorate_flat_fee_for_dbdt);
    changes = changes + 1;
  END

  /* INITIAL_EFFECTIVE_DATE */
  IF (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 595, OLD.initial_effective_date);
    changes = changes + 1;
  END

  /* SB_OTHER_SERVICE_NBR */
  IF (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 596, OLD.sb_other_service_nbr);
    changes = changes + 1;
  END

  /* BREAK_CHECKS_BY_DBDT */
  IF (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 758, OLD.break_checks_by_dbdt);
    changes = changes + 1;
  END

  /* SUMMARIZE_SUI */
  IF (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 759, OLD.summarize_sui);
    changes = changes + 1;
  END

  /* SHOW_SHORTFALL_CHECK */
  IF (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 760, OLD.show_shortfall_check);
    changes = changes + 1;
  END

  /* TRUST_CHK_OFFSET_GL_TAG */
  IF (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 656, OLD.trust_chk_offset_gl_tag);
    changes = changes + 1;
  END

  /* MANUAL_CL_BANK_ACCOUNT_NBR */
  IF (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 597, OLD.manual_cl_bank_account_nbr);
    changes = changes + 1;
  END

  /* WELLS_FARGO_ACH_FLAG */
  IF (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 657, OLD.wells_fargo_ach_flag);
    changes = changes + 1;
  END

  /* BILLING_CHECK_CPA */
  IF (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 761, OLD.billing_check_cpa);
    changes = changes + 1;
  END

  /* SALES_TAX_PERCENTAGE */
  IF (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 512, OLD.sales_tax_percentage);
    changes = changes + 1;
  END

  /* EXCLUDE_R_C_B_0R_N */
  IF (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 667, OLD.exclude_r_c_b_0r_n);
    changes = changes + 1;
  END

  /* CALCULATE_STATES_FIRST */
  IF (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 666, OLD.calculate_states_first);
    changes = changes + 1;
  END

  /* INVOICE_DISCOUNT */
  IF (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 479, OLD.invoice_discount);
    changes = changes + 1;
  END

  /* DISCOUNT_START_DATE */
  IF (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 521, OLD.discount_start_date);
    changes = changes + 1;
  END

  /* DISCOUNT_END_DATE */
  IF (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 520, OLD.discount_end_date);
    changes = changes + 1;
  END

  /* SHOW_TIME_CLOCK_PUNCH */
  IF (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 664, OLD.show_time_clock_punch);
    changes = changes + 1;
  END

  /* AUTO_RD_DFLT_CL_E_D_GROUPS_NBR */
  IF (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 598, OLD.auto_rd_dflt_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* AUTO_REDUCTION_DEFAULT_EES */
  IF (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 762, OLD.auto_reduction_default_ees);
    changes = changes + 1;
  END

  /* VT_HEALTHCARE_GL_TAG */
  IF (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 659, OLD.vt_healthcare_gl_tag);
    changes = changes + 1;
  END

  /* VT_HEALTHCARE_OFFSET_GL_TAG */
  IF (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 660, OLD.vt_healthcare_offset_gl_tag);
    changes = changes + 1;
  END

  /* UI_ROUNDING_GL_TAG */
  IF (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 661, OLD.ui_rounding_gl_tag);
    changes = changes + 1;
  END

  /* UI_ROUNDING_OFFSET_GL_TAG */
  IF (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 662, OLD.ui_rounding_offset_gl_tag);
    changes = changes + 1;
  END

  /* REPRINT_TO_BALANCE */
  IF (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 763, OLD.reprint_to_balance);
    changes = changes + 1;
  END

  /* SHOW_MANUAL_CHECKS_IN_ESS */
  IF (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 765, OLD.show_manual_checks_in_ess);
    changes = changes + 1;
  END

  /* PAYROLL_REQUIRES_MGR_APPROVAL */
  IF (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 766, OLD.payroll_requires_mgr_approval);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 599, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* WC_OFFS_BANK_ACCOUNT_NBR */
  IF (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 603, OLD.wc_offs_bank_account_nbr);
    changes = changes + 1;
  END

  /* QTR_LOCK_FOR_TAX_PMTS */
  IF (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 767, OLD.qtr_lock_for_tax_pmts);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_PAYROLL */
  IF (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 515, OLD.co_max_amount_for_payroll);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_TAX_IMPOUND */
  IF (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 516, OLD.co_max_amount_for_tax_impound);
    changes = changes + 1;
  END

  /* CO_MAX_AMOUNT_FOR_DD */
  IF (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 517, OLD.co_max_amount_for_dd);
    changes = changes + 1;
  END

  /* CO_PAYROLL_PROCESS_LIMITATIONS */
  IF (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 768, OLD.co_payroll_process_limitations);
    changes = changes + 1;
  END

  /* CO_ACH_PROCESS_LIMITATIONS */
  IF (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 769, OLD.co_ach_process_limitations);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 770, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 771, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 772, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 773, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 774, OLD.wc_impound);
    changes = changes + 1;
  END

  /* COBRA_CREDIT_GL_TAG */
  IF (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 663, OLD.cobra_credit_gl_tag);
    changes = changes + 1;
  END

  /* CO_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 775, OLD.co_exception_payment_type);
    changes = changes + 1;
  END

  /* LAST_TAX_RETURN */
  IF (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 776, OLD.last_tax_return);
    changes = changes + 1;
  END

  /* ENABLE_HR */
  IF (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 764, OLD.enable_hr);
    changes = changes + 1;
  END

  /* ENABLE_ESS */
  IF (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 777, OLD.enable_ess);
    changes = changes + 1;
  END

  /* DUNS_AND_BRADSTREET */
  IF (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 519, OLD.duns_and_bradstreet);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_REGISTER_NAME */
  IF (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 780, OLD.bank_account_register_name);
    changes = changes + 1;
  END

  /* ENABLE_BENEFITS */
  IF (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2883, OLD.enable_benefits);
    changes = changes + 1;
  END

  /* ENABLE_TIME_OFF */
  IF (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2884, OLD.enable_time_off);
    changes = changes + 1;
  END

  /* EMPLOYER_TYPE */
  IF (OLD.employer_type IS DISTINCT FROM NEW.employer_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2885, OLD.employer_type);
    changes = changes + 1;
  END

  /* WC_FISCAL_YEAR_BEGIN */
  IF (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2886, OLD.wc_fiscal_year_begin);
    changes = changes + 1;
  END

  /* SHOW_PAYSTUBS_ESS_DAYS */
  IF (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2888, OLD.show_paystubs_ess_days);
    changes = changes + 1;
  END

  /* CO_LOCATIONS_NBR */
  IF (OLD.co_locations_nbr IS DISTINCT FROM NEW.co_locations_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3169, OLD.co_locations_nbr);
    changes = changes + 1;
  END

  /* ANNUAL_FORM_TYPE */
  IF (OLD.annual_form_type IS DISTINCT FROM NEW.annual_form_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3200, OLD.annual_form_type);
    changes = changes + 1;
  END

  /* PRENOTE */
  IF (OLD.prenote IS DISTINCT FROM NEW.prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3201, OLD.prenote);
    changes = changes + 1;
  END

  /* ENABLE_DD */
  IF (OLD.enable_dd IS DISTINCT FROM NEW.enable_dd) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3202, OLD.enable_dd);
    changes = changes + 1;
  END

  /* ESS_OR_EP */
  IF (OLD.ess_or_ep IS DISTINCT FROM NEW.ess_or_ep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3203, OLD.ess_or_ep);
    changes = changes + 1;
  END

  /* ACA_EDUCATION_ORG */
  IF (OLD.aca_education_org IS DISTINCT FROM NEW.aca_education_org) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3204, OLD.aca_education_org);
    changes = changes + 1;
  END

  /* ACA_STANDARD_HOURS */
  IF (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3205, OLD.aca_standard_hours);
    changes = changes + 1;
  END

  /* ACA_DEFAULT_STATUS */
  IF (OLD.aca_default_status IS DISTINCT FROM NEW.aca_default_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3206, OLD.aca_default_status);
    changes = changes + 1;
  END

  /* ACA_CL_CO_CONSOLIDATION_NBR */
  IF (OLD.aca_cl_co_consolidation_nbr IS DISTINCT FROM NEW.aca_cl_co_consolidation_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3207, OLD.aca_cl_co_consolidation_nbr);
    changes = changes + 1;
  END

  /* ACA_INITIAL_PERIOD_FROM */
  IF (OLD.aca_initial_period_from IS DISTINCT FROM NEW.aca_initial_period_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3208, OLD.aca_initial_period_from);
    changes = changes + 1;
  END

  /* ACA_INITIAL_PERIOD_TO */
  IF (OLD.aca_initial_period_to IS DISTINCT FROM NEW.aca_initial_period_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3209, OLD.aca_initial_period_to);
    changes = changes + 1;
  END

  /* ACA_STABILITY_PERIOD_FROM */
  IF (OLD.aca_stability_period_from IS DISTINCT FROM NEW.aca_stability_period_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3210, OLD.aca_stability_period_from);
    changes = changes + 1;
  END

  /* ACA_STABILITY_PERIOD_TO */
  IF (OLD.aca_stability_period_to IS DISTINCT FROM NEW.aca_stability_period_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3211, OLD.aca_stability_period_to);
    changes = changes + 1;
  END

  /* ACA_USE_AVG_HOURS_WORKED */
  IF (OLD.aca_use_avg_hours_worked IS DISTINCT FROM NEW.aca_use_avg_hours_worked) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3212, OLD.aca_use_avg_hours_worked);
    changes = changes + 1;
  END

  /* ACA_AVG_HOURS_WORKED_PERIOD */
  IF (OLD.aca_avg_hours_worked_period IS DISTINCT FROM NEW.aca_avg_hours_worked_period) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3213, OLD.aca_avg_hours_worked_period);
    changes = changes + 1;
  END

  /* ACA_ADD_EARN_CL_E_D_GROUPS_NBR */
  IF (OLD.aca_add_earn_cl_e_d_groups_nbr IS DISTINCT FROM NEW.aca_add_earn_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3227, OLD.aca_add_earn_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* EE_PRINT_VOUCHER_DEFAULT */
  IF (OLD.ee_print_voucher_default IS DISTINCT FROM NEW.ee_print_voucher_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3240, OLD.ee_print_voucher_default);
    changes = changes + 1;
  END

  /* ENABLE_ANALYTICS */
  IF (OLD.enable_analytics IS DISTINCT FROM NEW.enable_analytics) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3268, OLD.enable_analytics);
    changes = changes + 1;
  END

  /* ANALYTICS_LICENSE */
  IF (OLD.analytics_license IS DISTINCT FROM NEW.analytics_license) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3269, OLD.analytics_license);
    changes = changes + 1;
  END

  /* BENEFITS_ELIGIBLE_DEFAULT */
  IF (OLD.benefits_eligible_default IS DISTINCT FROM NEW.benefits_eligible_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3288, OLD.benefits_eligible_default);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', NULL);
  rdb$set_context('USER_TRANSACTION', '@CHANGES', NULL);
END

^

CREATE TRIGGER T_AD_CO_ADDITIONAL_INFO_NAME_9 FOR CO_ADDITIONAL_INFO_NAMES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, OLD.rec_version, OLD.co_additional_info_names_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 781, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2630, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 783, OLD.co_nbr);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 784, OLD.name);

  /* DATA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2889, OLD.data_type);

  /* CATEGORY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2890, OLD.category);

  /* SEQ_NUM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2891, OLD.seq_num);

  /* REQUIRED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3246, OLD.required);

END

^

CREATE TRIGGER T_AU_CO_ADDITIONAL_INFO_NAME_9 FOR CO_ADDITIONAL_INFO_NAMES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, NEW.rec_version, NEW.co_additional_info_names_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 781, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2630, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 783, OLD.co_nbr);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 784, OLD.name);
    changes = changes + 1;
  END

  /* DATA_TYPE */
  IF (OLD.data_type IS DISTINCT FROM NEW.data_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2889, OLD.data_type);
    changes = changes + 1;
  END

  /* CATEGORY */
  IF (OLD.category IS DISTINCT FROM NEW.category) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2890, OLD.category);
    changes = changes + 1;
  END

  /* SEQ_NUM */
  IF (OLD.seq_num IS DISTINCT FROM NEW.seq_num) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2891, OLD.seq_num);
    changes = changes + 1;
  END

  /* REQUIRED */
  IF (OLD.required IS DISTINCT FROM NEW.required) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3246, OLD.required);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_CO_BENEFIT_SUBTYPE_9 FOR CO_BENEFIT_SUBTYPE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(167, OLD.rec_version, OLD.co_benefit_subtype_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3106, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3107, OLD.effective_until);

  /* CO_BENEFITS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3108, OLD.co_benefits_nbr);

  /* DESCRIPTION */
  IF (OLD.description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3109, OLD.description);

  /* AGE_BANDS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3300, OLD.age_bands);

  /* ACA_LOWEST_COST */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3242, OLD.aca_lowest_cost);

END

^

CREATE TRIGGER T_AUD_CO_BENEFIT_SUBTYPE_2 FOR CO_BENEFIT_SUBTYPE After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_benefit_subtype WHERE co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_benefit_subtype WHERE co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO_E_D_CODES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_e_d_codes
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'co_e_d_codes', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_e_d_codes
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'co_e_d_codes', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_BENEFITS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'ee_benefits', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_benefits
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'ee_benefits', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_SCHEDULED_E_DS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_scheduled_e_ds
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'ee_scheduled_e_ds', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_RATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_rates
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'co_benefit_rates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_rates
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'co_benefit_rates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_AGE_BANDS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_age_bands
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'co_benefit_age_bands', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_age_bands
    WHERE (co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'co_benefit_age_bands', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (aca_co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (aca_co_benefit_subtype_nbr = OLD.co_benefit_subtype_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_benefit_subtype', OLD.co_benefit_subtype_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_CO_BENEFIT_SUBTYPE_9 FOR CO_BENEFIT_SUBTYPE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(167, NEW.rec_version, NEW.co_benefit_subtype_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3106, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3107, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_BENEFITS_NBR */
  IF (OLD.co_benefits_nbr IS DISTINCT FROM NEW.co_benefits_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3108, OLD.co_benefits_nbr);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3109, OLD.description);
    changes = changes + 1;
  END

  /* AGE_BANDS */
  IF (OLD.age_bands IS DISTINCT FROM NEW.age_bands) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3300, OLD.age_bands);
    changes = changes + 1;
  END

  /* ACA_LOWEST_COST */
  IF (OLD.aca_lowest_cost IS DISTINCT FROM NEW.aca_lowest_cost) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3242, OLD.aca_lowest_cost);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AUD_CO_BRANCH_2 FOR CO_BRANCH After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_branch WHERE co_branch_nbr = OLD.co_branch_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_branch WHERE co_branch_nbr = OLD.co_branch_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO_DEPARTMENT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_department
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_department', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_department
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_department', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BRANCH_LOCALS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_branch_locals
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_branch_locals', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_branch_locals
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_branch_locals', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_APPLICANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_RATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR_CHECK_LINES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BILLING_HISTORY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BRCH_PR_BATCH_DEFLT_ED */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_brch_pr_batch_deflt_ed
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_brch_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_brch_pr_batch_deflt_ed
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_brch_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_AUTOLABOR_DISTRIBUTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_PKG_ASMNT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SUPERVISORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_branch_nbr = OLD.co_branch_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_branch', OLD.co_branch_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AUD_CO_DEPARTMENT_2 FOR CO_DEPARTMENT After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_department WHERE co_department_nbr = OLD.co_department_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_department WHERE co_department_nbr = OLD.co_department_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO_TEAM */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_team
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_team', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_team
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_team', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DEPARTMENT_LOCALS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_department_locals
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_department_locals', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_department_locals
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_department_locals', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_APPLICANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_RATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR_CHECK_LINES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BILLING_HISTORY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DEPT_PR_BATCH_DEFLT_ED */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_dept_pr_batch_deflt_ed
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_dept_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_dept_pr_batch_deflt_ed
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_dept_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_AUTOLABOR_DISTRIBUTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_PKG_ASMNT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SUPERVISORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_department_nbr = OLD.co_department_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_department', OLD.co_department_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AUD_CO_DIVISION_2 FOR CO_DIVISION After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_division WHERE co_division_nbr = OLD.co_division_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_division WHERE co_division_nbr = OLD.co_division_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO_BRANCH */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_branch
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_branch', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_branch
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_branch', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DIVISION_LOCALS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_division_locals
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_division_locals', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_division_locals
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_division_locals', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_APPLICANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_RATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR_CHECK_LINES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BILLING_HISTORY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_DIV_PR_BATCH_DEFLT_ED */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_div_pr_batch_deflt_ed
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_div_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_div_pr_batch_deflt_ed
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_div_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_AUTOLABOR_DISTRIBUTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_PKG_ASMNT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SUPERVISORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_division_nbr = OLD.co_division_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_division', OLD.co_division_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_CO_HR_SUPERVISORS_9 FOR CO_HR_SUPERVISORS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(75, OLD.rec_version, OLD.co_hr_supervisors_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1232, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2704, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1234, OLD.co_nbr);

  /* SUPERVISOR_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1235, OLD.supervisor_name);

  /* CO_DIVISION_NBR */
  IF (OLD.co_division_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3270, OLD.co_division_nbr);

  /* CO_BRANCH_NBR */
  IF (OLD.co_branch_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3271, OLD.co_branch_nbr);

  /* CO_DEPARTMENT_NBR */
  IF (OLD.co_department_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3272, OLD.co_department_nbr);

  /* CO_TEAM_NBR */
  IF (OLD.co_team_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3273, OLD.co_team_nbr);

  /* CO_PAY_GROUP_NBR */
  IF (OLD.co_pay_group_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3274, OLD.co_pay_group_nbr);

END

^

CREATE TRIGGER T_AU_CO_HR_SUPERVISORS_9 FOR CO_HR_SUPERVISORS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(75, NEW.rec_version, NEW.co_hr_supervisors_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1232, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2704, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1234, OLD.co_nbr);
    changes = changes + 1;
  END

  /* SUPERVISOR_NAME */
  IF (OLD.supervisor_name IS DISTINCT FROM NEW.supervisor_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1235, OLD.supervisor_name);
    changes = changes + 1;
  END

  /* CO_DIVISION_NBR */
  IF (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3270, OLD.co_division_nbr);
    changes = changes + 1;
  END

  /* CO_BRANCH_NBR */
  IF (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3271, OLD.co_branch_nbr);
    changes = changes + 1;
  END

  /* CO_DEPARTMENT_NBR */
  IF (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3272, OLD.co_department_nbr);
    changes = changes + 1;
  END

  /* CO_TEAM_NBR */
  IF (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3273, OLD.co_team_nbr);
    changes = changes + 1;
  END

  /* CO_PAY_GROUP_NBR */
  IF (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3274, OLD.co_pay_group_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_HR_SUPERVISORS_2 FOR CO_HR_SUPERVISORS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END

  /* Check reference to CO_DIVISION */
  IF ((NEW.co_division_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_division
    WHERE co_division_nbr = NEW.co_division_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_division_nbr', NEW.co_division_nbr, 'co_division', NEW.effective_date);

    SELECT rec_version FROM co_division
    WHERE co_division_nbr = NEW.co_division_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_division_nbr', NEW.co_division_nbr, 'co_division', NEW.effective_until - 1);
  END

  /* Check reference to CO_BRANCH */
  IF ((NEW.co_branch_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_branch
    WHERE co_branch_nbr = NEW.co_branch_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_branch_nbr', NEW.co_branch_nbr, 'co_branch', NEW.effective_date);

    SELECT rec_version FROM co_branch
    WHERE co_branch_nbr = NEW.co_branch_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_branch_nbr', NEW.co_branch_nbr, 'co_branch', NEW.effective_until - 1);
  END

  /* Check reference to CO_DEPARTMENT */
  IF ((NEW.co_department_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_department
    WHERE co_department_nbr = NEW.co_department_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_department_nbr', NEW.co_department_nbr, 'co_department', NEW.effective_date);

    SELECT rec_version FROM co_department
    WHERE co_department_nbr = NEW.co_department_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_department_nbr', NEW.co_department_nbr, 'co_department', NEW.effective_until - 1);
  END

  /* Check reference to CO_TEAM */
  IF ((NEW.co_team_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_team
    WHERE co_team_nbr = NEW.co_team_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_team_nbr', NEW.co_team_nbr, 'co_team', NEW.effective_date);

    SELECT rec_version FROM co_team
    WHERE co_team_nbr = NEW.co_team_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_team_nbr', NEW.co_team_nbr, 'co_team', NEW.effective_until - 1);
  END

  /* Check reference to CO_PAY_GROUP */
  IF ((NEW.co_pay_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_pay_group
    WHERE co_pay_group_nbr = NEW.co_pay_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_pay_group_nbr', NEW.co_pay_group_nbr, 'co_pay_group', NEW.effective_date);

    SELECT rec_version FROM co_pay_group
    WHERE co_pay_group_nbr = NEW.co_pay_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_hr_supervisors', NEW.co_hr_supervisors_nbr, 'co_pay_group_nbr', NEW.co_pay_group_nbr, 'co_pay_group', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AUD_CO_PAY_GROUP_2 FOR CO_PAY_GROUP After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_pay_group WHERE co_pay_group_nbr = OLD.co_pay_group_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_pay_group WHERE co_pay_group_nbr = OLD.co_pay_group_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_pay_group_nbr = OLD.co_pay_group_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_pay_group', OLD.co_pay_group_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_pay_group_nbr = OLD.co_pay_group_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_pay_group', OLD.co_pay_group_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_pay_group_nbr = OLD.co_pay_group_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_pay_group', OLD.co_pay_group_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_pay_group_nbr = OLD.co_pay_group_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_pay_group', OLD.co_pay_group_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SUPERVISORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_pay_group_nbr = OLD.co_pay_group_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_pay_group', OLD.co_pay_group_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_pay_group_nbr = OLD.co_pay_group_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_pay_group', OLD.co_pay_group_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AUD_CO_TEAM_2 FOR CO_TEAM After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM co_team WHERE co_team_nbr = OLD.co_team_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM co_team WHERE co_team_nbr = OLD.co_team_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in CO_TEAM_LOCALS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_team_locals
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_team_locals', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_team_locals
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_team_locals', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_APPLICANT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_hr_applicant', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'ee', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_RATES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_rates
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'ee_rates', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_PR_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_pr_filters
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_pr_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in PR_CHECK_LINES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM pr_check_lines
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'pr_check_lines', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BILLING_HISTORY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_billing_history
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_billing_history', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_TEAM_PR_BATCH_DEFLT_ED */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_team_pr_batch_deflt_ed
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_team_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_team_pr_batch_deflt_ed
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_team_pr_batch_deflt_ed', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in EE_AUTOLABOR_DISTRIBUTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM ee_autolabor_distribution
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'ee_autolabor_distribution', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_BENEFIT_PKG_ASMNT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_pkg_asmnt
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_benefit_pkg_asmnt', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in CO_HR_SUPERVISORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE (co_team_nbr = OLD.co_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'co_team', OLD.co_team_nbr, 'co_hr_supervisors', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_EE_9 FOR EE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
DECLARE VARIABLE aca_coverage_offer VARCHAR(2);
DECLARE VARIABLE aca_co_benefit_subtype_nbr INTEGER;
DECLARE VARIABLE ee_aca_safe_harbor VARCHAR(2);
DECLARE VARIABLE aca_policy_origin CHAR(1);
DECLARE VARIABLE benefits_eligible CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, OLD.rec_version, OLD.ee_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status, aca_coverage_offer, aca_co_benefit_subtype_nbr, ee_aca_safe_harbor, aca_policy_origin, benefits_eligible  FROM ee WHERE ee_nbr = OLD.ee_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status, :aca_coverage_offer, :aca_co_benefit_subtype_nbr, :ee_aca_safe_harbor, :aca_policy_origin, :benefits_eligible;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1810, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2784, OLD.effective_until);

  /* CL_PERSON_NBR */
  IF (last_record = 'Y' OR cl_person_nbr IS DISTINCT FROM OLD.cl_person_nbr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);

  /* CO_DIVISION_NBR */
  IF ((last_record = 'Y' AND OLD.co_division_nbr IS NOT NULL) OR (last_record = 'N' AND co_division_nbr IS DISTINCT FROM OLD.co_division_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);

  /* CO_BRANCH_NBR */
  IF ((last_record = 'Y' AND OLD.co_branch_nbr IS NOT NULL) OR (last_record = 'N' AND co_branch_nbr IS DISTINCT FROM OLD.co_branch_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);

  /* CO_DEPARTMENT_NBR */
  IF ((last_record = 'Y' AND OLD.co_department_nbr IS NOT NULL) OR (last_record = 'N' AND co_department_nbr IS DISTINCT FROM OLD.co_department_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);

  /* CO_TEAM_NBR */
  IF ((last_record = 'Y' AND OLD.co_team_nbr IS NOT NULL) OR (last_record = 'N' AND co_team_nbr IS DISTINCT FROM OLD.co_team_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);

  /* NEW_HIRE_REPORT_SENT */
  IF (last_record = 'Y' OR new_hire_report_sent IS DISTINCT FROM OLD.new_hire_report_sent) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);

  /* CURRENT_TERMINATION_CODE */
  IF (last_record = 'Y' OR current_termination_code IS DISTINCT FROM OLD.current_termination_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1889, OLD.current_termination_code);

  /* CO_HR_POSITIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_hr_positions_nbr IS NOT NULL) OR (last_record = 'N' AND co_hr_positions_nbr IS DISTINCT FROM OLD.co_hr_positions_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);

  /* TIPPED_DIRECTLY */
  IF (last_record = 'Y' OR tipped_directly IS DISTINCT FROM OLD.tipped_directly) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1893, OLD.tipped_directly);

  /* W2_TYPE */
  IF (last_record = 'Y' OR w2_type IS DISTINCT FROM OLD.w2_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1897, OLD.w2_type);

  /* W2_PENSION */
  IF (last_record = 'Y' OR w2_pension IS DISTINCT FROM OLD.w2_pension) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1898, OLD.w2_pension);

  /* W2_DEFERRED_COMP */
  IF (last_record = 'Y' OR w2_deferred_comp IS DISTINCT FROM OLD.w2_deferred_comp) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);

  /* W2_DECEASED */
  IF (last_record = 'Y' OR w2_deceased IS DISTINCT FROM OLD.w2_deceased) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1900, OLD.w2_deceased);

  /* W2_STATUTORY_EMPLOYEE */
  IF (last_record = 'Y' OR w2_statutory_employee IS DISTINCT FROM OLD.w2_statutory_employee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);

  /* W2_LEGAL_REP */
  IF (last_record = 'Y' OR w2_legal_rep IS DISTINCT FROM OLD.w2_legal_rep) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);

  /* HOME_TAX_EE_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.home_tax_ee_states_nbr IS NOT NULL) OR (last_record = 'N' AND home_tax_ee_states_nbr IS DISTINCT FROM OLD.home_tax_ee_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (last_record = 'Y' OR exempt_employee_oasdi IS DISTINCT FROM OLD.exempt_employee_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (last_record = 'Y' OR exempt_employee_medicare IS DISTINCT FROM OLD.exempt_employee_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (last_record = 'Y' OR exempt_exclude_ee_fed IS DISTINCT FROM OLD.exempt_exclude_ee_fed) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);

  /* EXEMPT_EMPLOYER_OASDI */
  IF (last_record = 'Y' OR exempt_employer_oasdi IS DISTINCT FROM OLD.exempt_employer_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (last_record = 'Y' OR exempt_employer_medicare IS DISTINCT FROM OLD.exempt_employer_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);

  /* EXEMPT_EMPLOYER_FUI */
  IF (last_record = 'Y' OR exempt_employer_fui IS DISTINCT FROM OLD.exempt_employer_fui) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);

  /* BASE_RETURNS_ON_THIS_EE */
  IF (last_record = 'Y' OR base_returns_on_this_ee IS DISTINCT FROM OLD.base_returns_on_this_ee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (last_record = 'Y' OR company_or_individual_name IS DISTINCT FROM OLD.company_or_individual_name) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);

  /* DISTRIBUTION_CODE_1099R */
  IF ((last_record = 'Y' AND OLD.distribution_code_1099r IS NOT NULL) OR (last_record = 'N' AND distribution_code_1099r IS DISTINCT FROM OLD.distribution_code_1099r)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);

  /* TAX_AMT_DETERMINED_1099R */
  IF (last_record = 'Y' OR tax_amt_determined_1099r IS DISTINCT FROM OLD.tax_amt_determined_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);

  /* TOTAL_DISTRIBUTION_1099R */
  IF (last_record = 'Y' OR total_distribution_1099r IS DISTINCT FROM OLD.total_distribution_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);

  /* PENSION_PLAN_1099R */
  IF (last_record = 'Y' OR pension_plan_1099r IS DISTINCT FROM OLD.pension_plan_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (last_record = 'Y' OR makeup_fica_on_cleanup_pr IS DISTINCT FROM OLD.makeup_fica_on_cleanup_pr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);

  /* SALARY_AMOUNT */
  IF ((last_record = 'Y' AND OLD.salary_amount IS NOT NULL) OR (last_record = 'N' AND salary_amount IS DISTINCT FROM OLD.salary_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1809, OLD.salary_amount);

  /* HIGHLY_COMPENSATED */
  IF (last_record = 'Y' OR highly_compensated IS DISTINCT FROM OLD.highly_compensated) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1919, OLD.highly_compensated);

  /* CORPORATE_OFFICER */
  IF (last_record = 'Y' OR corporate_officer IS DISTINCT FROM OLD.corporate_officer) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1920, OLD.corporate_officer);

  /* CO_JOBS_NBR */
  IF ((last_record = 'Y' AND OLD.co_jobs_nbr IS NOT NULL) OR (last_record = 'N' AND co_jobs_nbr IS DISTINCT FROM OLD.co_jobs_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);

  /* CO_WORKERS_COMP_NBR */
  IF ((last_record = 'Y' AND OLD.co_workers_comp_nbr IS NOT NULL) OR (last_record = 'N' AND co_workers_comp_nbr IS DISTINCT FROM OLD.co_workers_comp_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);

  /* HEALTHCARE_COVERAGE */
  IF (last_record = 'Y' OR healthcare_coverage IS DISTINCT FROM OLD.healthcare_coverage) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF ((last_record = 'Y' AND OLD.fui_rate_credit_override IS NOT NULL) OR (last_record = 'N' AND fui_rate_credit_override IS DISTINCT FROM OLD.fui_rate_credit_override)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);

  /* ACA_STATUS */
  IF (last_record = 'Y' OR aca_status IS DISTINCT FROM OLD.aca_status) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3188, OLD.aca_status);

  /* ACA_COVERAGE_OFFER */
  IF ((last_record = 'Y' AND OLD.aca_coverage_offer IS NOT NULL) OR (last_record = 'N' AND aca_coverage_offer IS DISTINCT FROM OLD.aca_coverage_offer)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3237, OLD.aca_coverage_offer);

  /* ACA_CO_BENEFIT_SUBTYPE_NBR */
  IF ((last_record = 'Y' AND OLD.aca_co_benefit_subtype_nbr IS NOT NULL) OR (last_record = 'N' AND aca_co_benefit_subtype_nbr IS DISTINCT FROM OLD.aca_co_benefit_subtype_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3243, OLD.aca_co_benefit_subtype_nbr);

  /* EE_ACA_SAFE_HARBOR */
  IF ((last_record = 'Y' AND OLD.ee_aca_safe_harbor IS NOT NULL) OR (last_record = 'N' AND ee_aca_safe_harbor IS DISTINCT FROM OLD.ee_aca_safe_harbor)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3244, OLD.ee_aca_safe_harbor);

  /* ACA_POLICY_ORIGIN */
  IF (last_record = 'Y' OR aca_policy_origin IS DISTINCT FROM OLD.aca_policy_origin) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3245, OLD.aca_policy_origin);

  /* BENEFITS_ELIGIBLE */
  IF (last_record = 'Y' OR benefits_eligible IS DISTINCT FROM OLD.benefits_eligible) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3289, OLD.benefits_eligible);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CO_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1823, OLD.co_nbr);

    /* CUSTOM_EMPLOYEE_NUMBER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);

    /* CO_HR_APPLICANT_NBR */
    IF (OLD.co_hr_applicant_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);

    /* CO_HR_RECRUITERS_NBR */
    IF (OLD.co_hr_recruiters_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);

    /* CO_HR_REFERRALS_NBR */
    IF (OLD.co_hr_referrals_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);

    /* ADDRESS1 */
    IF (OLD.address1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1811, OLD.address1);

    /* ADDRESS2 */
    IF (OLD.address2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1812, OLD.address2);

    /* CITY */
    IF (OLD.city IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1869, OLD.city);

    /* STATE */
    IF (OLD.state IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1870, OLD.state);

    /* ZIP_CODE */
    IF (OLD.zip_code IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1831, OLD.zip_code);

    /* COUNTY */
    IF (OLD.county IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1832, OLD.county);

    /* PHONE1 */
    IF (OLD.phone1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1871, OLD.phone1);

    /* DESCRIPTION1 */
    IF (OLD.description1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1834, OLD.description1);

    /* PHONE2 */
    IF (OLD.phone2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1872, OLD.phone2);

    /* DESCRIPTION2 */
    IF (OLD.description2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1835, OLD.description2);

    /* PHONE3 */
    IF (OLD.phone3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1873, OLD.phone3);

    /* DESCRIPTION3 */
    IF (OLD.description3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1833, OLD.description3);

    /* E_MAIL_ADDRESS */
    IF (OLD.e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1874, OLD.e_mail_address);

    /* TIME_CLOCK_NUMBER */
    IF (OLD.time_clock_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1887, OLD.time_clock_number);

    /* ORIGINAL_HIRE_DATE */
    IF (OLD.original_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1848, OLD.original_hire_date);

    /* CURRENT_HIRE_DATE */
    IF (OLD.current_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1849, OLD.current_hire_date);

    /* CURRENT_TERMINATION_DATE */
    IF (OLD.current_termination_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1850, OLD.current_termination_date);

    /* CO_HR_SUPERVISORS_NBR */
    IF (OLD.co_hr_supervisors_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);

    /* AUTOPAY_CO_SHIFTS_NBR */
    IF (OLD.autopay_co_shifts_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);

    /* POSITION_EFFECTIVE_DATE */
    IF (OLD.position_effective_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1851, OLD.position_effective_date);

    /* POSITION_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1890, OLD.position_status);

    /* CO_HR_PERFORMANCE_RATINGS_NBR */
    IF (OLD.co_hr_performance_ratings_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);

    /* REVIEW_DATE */
    IF (OLD.review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1852, OLD.review_date);

    /* NEXT_REVIEW_DATE */
    IF (OLD.next_review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1853, OLD.next_review_date);

    /* PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1891, OLD.pay_frequency);

    /* NEXT_RAISE_DATE */
    IF (OLD.next_raise_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1854, OLD.next_raise_date);

    /* NEXT_PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);

    /* ALD_CL_E_D_GROUPS_NBR */
    IF (OLD.ald_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);

    /* DISTRIBUTE_TAXES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);

    /* CO_PAY_GROUP_NBR */
    IF (OLD.co_pay_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);

    /* CL_DELIVERY_GROUP_NBR */
    IF (OLD.cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);

    /* CO_UNIONS_NBR */
    IF (OLD.co_unions_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);

    /* EIC */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1895, OLD.eic);

    /* FLSA_EXEMPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);

    /* OVERRIDE_FED_TAX_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);

    /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);

    /* SECURITY_CLEARANCE */
    IF (OLD.security_clearance IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1875, OLD.security_clearance);

    /* BADGE_ID */
    IF (OLD.badge_id IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1911, OLD.badge_id);

    /* ON_CALL_FROM */
    IF (OLD.on_call_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1813, OLD.on_call_from);

    /* ON_CALL_TO */
    IF (OLD.on_call_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1814, OLD.on_call_to);

    /* NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1815, :blob_nbr);
    END

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1876, OLD.filler);

    /* GENERAL_LEDGER_TAG */
    IF (OLD.general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);

    /* FEDERAL_MARITAL_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);

    /* NUMBER_OF_DEPENDENTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);

    /* SY_HR_EEO_NBR */
    IF (OLD.sy_hr_eeo_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);

    /* SECONDARY_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1816, :blob_nbr);
    END

    /* CO_PR_CHECK_TEMPLATES_NBR */
    IF (OLD.co_pr_check_templates_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);

    /* GENERATE_SECOND_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1884, OLD.generate_second_check);

    /* STANDARD_HOURS */
    IF (OLD.standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1808, OLD.standard_hours);

    /* NEXT_RAISE_AMOUNT */
    IF (OLD.next_raise_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);

    /* NEXT_RAISE_PERCENTAGE */
    IF (OLD.next_raise_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);

    /* NEXT_RAISE_RATE */
    IF (OLD.next_raise_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);

    /* WORKERS_COMP_WAGE_LIMIT */
    IF (OLD.workers_comp_wage_limit IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);

    /* GROUP_TERM_POLICY_AMOUNT */
    IF (OLD.group_term_policy_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);

    /* OVERRIDE_FED_TAX_VALUE */
    IF (OLD.override_fed_tax_value IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);

    /* CALCULATED_SALARY */
    IF (OLD.calculated_salary IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1801, OLD.calculated_salary);

    /* PR_CHECK_MB_GROUP_NBR */
    IF (OLD.pr_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);

    /* PR_EE_REPORT_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);

    /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_sec_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);

    /* TAX_EE_RETURN_MB_GROUP_NBR */
    IF (OLD.tax_ee_return_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);

    /* EE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1921, OLD.ee_enabled);

    /* GTL_NUMBER_OF_HOURS */
    IF (OLD.gtl_number_of_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);

    /* GTL_RATE */
    IF (OLD.gtl_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1817, OLD.gtl_rate);

    /* AUTO_UPDATE_RATES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);

    /* ELIGIBLE_FOR_REHIRE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);

    /* SELFSERVE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);

    /* SELFSERVE_USERNAME */
    IF (OLD.selfserve_username IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1868, OLD.selfserve_username);

    /* SELFSERVE_PASSWORD */
    IF (OLD.selfserve_password IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1867, OLD.selfserve_password);

    /* SELFSERVE_LAST_LOGIN */
    IF (OLD.selfserve_last_login IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);

    /* PRINT_VOUCHER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1882, OLD.print_voucher);

    /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
    IF (OLD.override_federal_minimum_wage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);

    /* WC_WAGE_LIMIT_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);

    /* LOGIN_QUESTION1 */
    IF (OLD.login_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1864, OLD.login_question1);

    /* LOGIN_ANSWER1 */
    IF (OLD.login_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1879, OLD.login_answer1);

    /* LOGIN_QUESTION2 */
    IF (OLD.login_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1865, OLD.login_question2);

    /* LOGIN_ANSWER2 */
    IF (OLD.login_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1880, OLD.login_answer2);

    /* LOGIN_QUESTION3 */
    IF (OLD.login_question3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1866, OLD.login_question3);

    /* LOGIN_ANSWER3 */
    IF (OLD.login_answer3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1881, OLD.login_answer3);

    /* LOGIN_DATE */
    IF (OLD.login_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2785, OLD.login_date);

    /* LOGIN_ATTEMPTS */
    IF (OLD.login_attempts IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2786, OLD.login_attempts);

    /* CO_BENEFIT_DISCOUNT_NBR */
    IF (OLD.co_benefit_discount_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);

    /* BENEFITS_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);

    /* EXISTING_PATIENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2914, OLD.existing_patient);

    /* PCP */
    IF (OLD.pcp IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2915, OLD.pcp);

    /* TIME_OFF_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);

    /* DEPENDENT_BENEFITS_AVAILABLE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);

    /* DEPENDENT_BENEFITS_AVAIL_DATE */
    IF (OLD.dependent_benefits_avail_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);

    /* BENEFIT_ENROLLMENT_COMPLETE */
    IF (OLD.benefit_enrollment_complete IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);

    /* LAST_QUAL_BENEFIT_EVENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);

    /* LAST_QUAL_BENEFIT_EVENT_DATE */
    IF (OLD.last_qual_benefit_event_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);

    /* W2 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2922, OLD.w2);

    /* W2_FORM_ON_FILE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);

    /* BENEFIT_E_MAIL_ADDRESS */
    IF (OLD.benefit_e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);

    /* CO_HR_POSITION_GRADES_NBR */
    IF (OLD.co_hr_position_grades_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);

    /* SEC_QUESTION1 */
    IF (OLD.sec_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2926, OLD.sec_question1);

    /* SEC_ANSWER1 */
    IF (OLD.sec_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2927, OLD.sec_answer1);

    /* SEC_QUESTION2 */
    IF (OLD.sec_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2928, OLD.sec_question2);

    /* SEC_ANSWER2 */
    IF (OLD.sec_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2929, OLD.sec_answer2);

    /* WORKERS_COMP_MIN_WAGE_LIMIT */
    IF (OLD.workers_comp_min_wage_limit IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3189, OLD.workers_comp_min_wage_limit);

    /* ESS_TERMS_OF_USE_ACCEPT_DATE */
    IF (OLD.ess_terms_of_use_accept_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3192, OLD.ess_terms_of_use_accept_date);

    /* ACA_STANDARD_HOURS */
    IF (OLD.aca_standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3218, OLD.aca_standard_hours);

    /* ENABLE_ANALYTICS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3267, OLD.enable_analytics);

  END
END

^

CREATE TRIGGER T_AIU_EE_3 FOR EE After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) OR (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) OR (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) OR (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) OR (OLD.address1 IS DISTINCT FROM NEW.address1) OR (OLD.address2 IS DISTINCT FROM NEW.address2) OR (OLD.city IS DISTINCT FROM NEW.city) OR (OLD.state IS DISTINCT FROM NEW.state) OR (OLD.zip_code IS DISTINCT FROM NEW.zip_code) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.phone1 IS DISTINCT FROM NEW.phone1) OR (OLD.description1 IS DISTINCT FROM NEW.description1) OR (OLD.phone2 IS DISTINCT FROM NEW.phone2) OR (OLD.description2 IS DISTINCT FROM NEW.description2) OR (OLD.phone3 IS DISTINCT FROM NEW.phone3) OR (OLD.description3 IS DISTINCT FROM NEW.description3) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) OR (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) OR (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) OR (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) OR (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) OR (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) OR (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) OR (OLD.position_status IS DISTINCT FROM NEW.position_status) OR (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) OR (OLD.review_date IS DISTINCT FROM NEW.review_date) OR (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) OR (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) OR (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) OR (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) OR (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) OR (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) OR (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) OR (OLD.eic IS DISTINCT FROM NEW.eic) OR (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) OR (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) OR (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) OR (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) OR (OLD.badge_id IS DISTINCT FROM NEW.badge_id) OR (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) OR (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) OR (rdb$get_context('USER_TRANSACTION', '@NOTES') IS NOT NULL) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) OR (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) OR (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) OR (rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES') IS NOT NULL) OR (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) OR (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) OR (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) OR (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) OR (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) OR (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) OR (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) OR (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) OR (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) OR (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) OR (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) OR (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) OR (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) OR (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) OR (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) OR (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) OR (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) OR (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) OR (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) OR (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) OR (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) OR (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) OR (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) OR (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) OR (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) OR (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) OR (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) OR (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) OR (OLD.login_date IS DISTINCT FROM NEW.login_date) OR (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) OR (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) OR (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) OR (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) OR (OLD.pcp IS DISTINCT FROM NEW.pcp) OR (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) OR (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) OR (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) OR (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) OR (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) OR (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) OR (OLD.w2 IS DISTINCT FROM NEW.w2) OR (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) OR (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) OR (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) OR (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) OR (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) OR (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) OR (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) OR (OLD.workers_comp_min_wage_limit IS DISTINCT FROM NEW.workers_comp_min_wage_limit) OR (OLD.ess_terms_of_use_accept_date IS DISTINCT FROM NEW.ess_terms_of_use_accept_date) OR (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) OR (OLD.enable_analytics IS DISTINCT FROM NEW.enable_analytics)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE ee SET 
      co_nbr = NEW.co_nbr, custom_employee_number = NEW.custom_employee_number, co_hr_applicant_nbr = NEW.co_hr_applicant_nbr, co_hr_recruiters_nbr = NEW.co_hr_recruiters_nbr, co_hr_referrals_nbr = NEW.co_hr_referrals_nbr, address1 = NEW.address1, address2 = NEW.address2, city = NEW.city, state = NEW.state, zip_code = NEW.zip_code, county = NEW.county, phone1 = NEW.phone1, description1 = NEW.description1, phone2 = NEW.phone2, description2 = NEW.description2, phone3 = NEW.phone3, description3 = NEW.description3, e_mail_address = NEW.e_mail_address, time_clock_number = NEW.time_clock_number, original_hire_date = NEW.original_hire_date, current_hire_date = NEW.current_hire_date, current_termination_date = NEW.current_termination_date, co_hr_supervisors_nbr = NEW.co_hr_supervisors_nbr, autopay_co_shifts_nbr = NEW.autopay_co_shifts_nbr, position_effective_date = NEW.position_effective_date, position_status = NEW.position_status, co_hr_performance_ratings_nbr = NEW.co_hr_performance_ratings_nbr, review_date = NEW.review_date, next_review_date = NEW.next_review_date, pay_frequency = NEW.pay_frequency, next_raise_date = NEW.next_raise_date, next_pay_frequency = NEW.next_pay_frequency, ald_cl_e_d_groups_nbr = NEW.ald_cl_e_d_groups_nbr, distribute_taxes = NEW.distribute_taxes, co_pay_group_nbr = NEW.co_pay_group_nbr, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, co_unions_nbr = NEW.co_unions_nbr, eic = NEW.eic, flsa_exempt = NEW.flsa_exempt, override_fed_tax_type = NEW.override_fed_tax_type, gov_garnish_prior_child_suppt = NEW.gov_garnish_prior_child_suppt, security_clearance = NEW.security_clearance, badge_id = NEW.badge_id, on_call_from = NEW.on_call_from, on_call_to = NEW.on_call_to, notes = NEW.notes, filler = NEW.filler, general_ledger_tag = NEW.general_ledger_tag, federal_marital_status = NEW.federal_marital_status, number_of_dependents = NEW.number_of_dependents, sy_hr_eeo_nbr = NEW.sy_hr_eeo_nbr, secondary_notes = NEW.secondary_notes, co_pr_check_templates_nbr = NEW.co_pr_check_templates_nbr, generate_second_check = NEW.generate_second_check, standard_hours = NEW.standard_hours, next_raise_amount = NEW.next_raise_amount, next_raise_percentage = NEW.next_raise_percentage, next_raise_rate = NEW.next_raise_rate, workers_comp_wage_limit = NEW.workers_comp_wage_limit, group_term_policy_amount = NEW.group_term_policy_amount, override_fed_tax_value = NEW.override_fed_tax_value, calculated_salary = NEW.calculated_salary, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_ee_report_mb_group_nbr = NEW.pr_ee_report_mb_group_nbr, pr_ee_report_sec_mb_group_nbr = NEW.pr_ee_report_sec_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, ee_enabled = NEW.ee_enabled, gtl_number_of_hours = NEW.gtl_number_of_hours, gtl_rate = NEW.gtl_rate, auto_update_rates = NEW.auto_update_rates, eligible_for_rehire = NEW.eligible_for_rehire, selfserve_enabled = NEW.selfserve_enabled, selfserve_username = NEW.selfserve_username, selfserve_password = NEW.selfserve_password, selfserve_last_login = NEW.selfserve_last_login, print_voucher = NEW.print_voucher, override_federal_minimum_wage = NEW.override_federal_minimum_wage, wc_wage_limit_frequency = NEW.wc_wage_limit_frequency, login_question1 = NEW.login_question1, login_answer1 = NEW.login_answer1, login_question2 = NEW.login_question2, login_answer2 = NEW.login_answer2, login_question3 = NEW.login_question3, login_answer3 = NEW.login_answer3, login_date = NEW.login_date, login_attempts = NEW.login_attempts, co_benefit_discount_nbr = NEW.co_benefit_discount_nbr, benefits_enabled = NEW.benefits_enabled, existing_patient = NEW.existing_patient, pcp = NEW.pcp, time_off_enabled = NEW.time_off_enabled, dependent_benefits_available = NEW.dependent_benefits_available, dependent_benefits_avail_date = NEW.dependent_benefits_avail_date, benefit_enrollment_complete = NEW.benefit_enrollment_complete, last_qual_benefit_event = NEW.last_qual_benefit_event, last_qual_benefit_event_date = NEW.last_qual_benefit_event_date, w2 = NEW.w2, w2_form_on_file = NEW.w2_form_on_file, benefit_e_mail_address = NEW.benefit_e_mail_address, co_hr_position_grades_nbr = NEW.co_hr_position_grades_nbr, sec_question1 = NEW.sec_question1, sec_answer1 = NEW.sec_answer1, sec_question2 = NEW.sec_question2, sec_answer2 = NEW.sec_answer2, workers_comp_min_wage_limit = NEW.workers_comp_min_wage_limit, ess_terms_of_use_accept_date = NEW.ess_terms_of_use_accept_date, aca_standard_hours = NEW.aca_standard_hours, enable_analytics = NEW.enable_analytics
      WHERE ee_nbr = NEW.ee_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_EE_9 FOR EE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, NEW.rec_version, NEW.ee_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1810, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2784, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_PERSON_NBR */
  IF (OLD.cl_person_nbr IS DISTINCT FROM NEW.cl_person_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1823, OLD.co_nbr);
    changes = changes + 1;
  END

  /* CO_DIVISION_NBR */
  IF (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);
    changes = changes + 1;
  END

  /* CO_BRANCH_NBR */
  IF (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);
    changes = changes + 1;
  END

  /* CO_DEPARTMENT_NBR */
  IF (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);
    changes = changes + 1;
  END

  /* CO_TEAM_NBR */
  IF (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);
    changes = changes + 1;
  END

  /* CUSTOM_EMPLOYEE_NUMBER */
  IF (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);
    changes = changes + 1;
  END

  /* CO_HR_APPLICANT_NBR */
  IF (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);
    changes = changes + 1;
  END

  /* CO_HR_RECRUITERS_NBR */
  IF (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);
    changes = changes + 1;
  END

  /* CO_HR_REFERRALS_NBR */
  IF (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1811, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1812, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1869, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1870, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1831, OLD.zip_code);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1832, OLD.county);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1871, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1834, OLD.description1);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1872, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1835, OLD.description2);
    changes = changes + 1;
  END

  /* PHONE3 */
  IF (OLD.phone3 IS DISTINCT FROM NEW.phone3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1873, OLD.phone3);
    changes = changes + 1;
  END

  /* DESCRIPTION3 */
  IF (OLD.description3 IS DISTINCT FROM NEW.description3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1833, OLD.description3);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1874, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* TIME_CLOCK_NUMBER */
  IF (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1887, OLD.time_clock_number);
    changes = changes + 1;
  END

  /* ORIGINAL_HIRE_DATE */
  IF (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1848, OLD.original_hire_date);
    changes = changes + 1;
  END

  /* CURRENT_HIRE_DATE */
  IF (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1849, OLD.current_hire_date);
    changes = changes + 1;
  END

  /* NEW_HIRE_REPORT_SENT */
  IF (OLD.new_hire_report_sent IS DISTINCT FROM NEW.new_hire_report_sent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_DATE */
  IF (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1850, OLD.current_termination_date);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_CODE */
  IF (OLD.current_termination_code IS DISTINCT FROM NEW.current_termination_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1889, OLD.current_termination_code);
    changes = changes + 1;
  END

  /* CO_HR_SUPERVISORS_NBR */
  IF (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);
    changes = changes + 1;
  END

  /* AUTOPAY_CO_SHIFTS_NBR */
  IF (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);
    changes = changes + 1;
  END

  /* CO_HR_POSITIONS_NBR */
  IF (OLD.co_hr_positions_nbr IS DISTINCT FROM NEW.co_hr_positions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);
    changes = changes + 1;
  END

  /* POSITION_EFFECTIVE_DATE */
  IF (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1851, OLD.position_effective_date);
    changes = changes + 1;
  END

  /* POSITION_STATUS */
  IF (OLD.position_status IS DISTINCT FROM NEW.position_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1890, OLD.position_status);
    changes = changes + 1;
  END

  /* CO_HR_PERFORMANCE_RATINGS_NBR */
  IF (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);
    changes = changes + 1;
  END

  /* REVIEW_DATE */
  IF (OLD.review_date IS DISTINCT FROM NEW.review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1852, OLD.review_date);
    changes = changes + 1;
  END

  /* NEXT_REVIEW_DATE */
  IF (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1853, OLD.next_review_date);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY */
  IF (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1891, OLD.pay_frequency);
    changes = changes + 1;
  END

  /* NEXT_RAISE_DATE */
  IF (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1854, OLD.next_raise_date);
    changes = changes + 1;
  END

  /* NEXT_PAY_FREQUENCY */
  IF (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);
    changes = changes + 1;
  END

  /* TIPPED_DIRECTLY */
  IF (OLD.tipped_directly IS DISTINCT FROM NEW.tipped_directly) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1893, OLD.tipped_directly);
    changes = changes + 1;
  END

  /* ALD_CL_E_D_GROUPS_NBR */
  IF (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* DISTRIBUTE_TAXES */
  IF (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);
    changes = changes + 1;
  END

  /* CO_PAY_GROUP_NBR */
  IF (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);
    changes = changes + 1;
  END

  /* CL_DELIVERY_GROUP_NBR */
  IF (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* CO_UNIONS_NBR */
  IF (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);
    changes = changes + 1;
  END

  /* EIC */
  IF (OLD.eic IS DISTINCT FROM NEW.eic) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1895, OLD.eic);
    changes = changes + 1;
  END

  /* FLSA_EXEMPT */
  IF (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);
    changes = changes + 1;
  END

  /* W2_TYPE */
  IF (OLD.w2_type IS DISTINCT FROM NEW.w2_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1897, OLD.w2_type);
    changes = changes + 1;
  END

  /* W2_PENSION */
  IF (OLD.w2_pension IS DISTINCT FROM NEW.w2_pension) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1898, OLD.w2_pension);
    changes = changes + 1;
  END

  /* W2_DEFERRED_COMP */
  IF (OLD.w2_deferred_comp IS DISTINCT FROM NEW.w2_deferred_comp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);
    changes = changes + 1;
  END

  /* W2_DECEASED */
  IF (OLD.w2_deceased IS DISTINCT FROM NEW.w2_deceased) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1900, OLD.w2_deceased);
    changes = changes + 1;
  END

  /* W2_STATUTORY_EMPLOYEE */
  IF (OLD.w2_statutory_employee IS DISTINCT FROM NEW.w2_statutory_employee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);
    changes = changes + 1;
  END

  /* W2_LEGAL_REP */
  IF (OLD.w2_legal_rep IS DISTINCT FROM NEW.w2_legal_rep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);
    changes = changes + 1;
  END

  /* HOME_TAX_EE_STATES_NBR */
  IF (OLD.home_tax_ee_states_nbr IS DISTINCT FROM NEW.home_tax_ee_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (OLD.exempt_employee_oasdi IS DISTINCT FROM NEW.exempt_employee_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (OLD.exempt_employee_medicare IS DISTINCT FROM NEW.exempt_employee_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (OLD.exempt_exclude_ee_fed IS DISTINCT FROM NEW.exempt_exclude_ee_fed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_OASDI */
  IF (OLD.exempt_employer_oasdi IS DISTINCT FROM NEW.exempt_employer_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (OLD.exempt_employer_medicare IS DISTINCT FROM NEW.exempt_employer_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_FUI */
  IF (OLD.exempt_employer_fui IS DISTINCT FROM NEW.exempt_employer_fui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_TYPE */
  IF (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);
    changes = changes + 1;
  END

  /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
  IF (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);
    changes = changes + 1;
  END

  /* SECURITY_CLEARANCE */
  IF (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1875, OLD.security_clearance);
    changes = changes + 1;
  END

  /* BADGE_ID */
  IF (OLD.badge_id IS DISTINCT FROM NEW.badge_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1911, OLD.badge_id);
    changes = changes + 1;
  END

  /* ON_CALL_FROM */
  IF (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1813, OLD.on_call_from);
    changes = changes + 1;
  END

  /* ON_CALL_TO */
  IF (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1814, OLD.on_call_to);
    changes = changes + 1;
  END

  /* BASE_RETURNS_ON_THIS_EE */
  IF (OLD.base_returns_on_this_ee IS DISTINCT FROM NEW.base_returns_on_this_ee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1815, :blob_nbr);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1876, OLD.filler);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_TAG */
  IF (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);
    changes = changes + 1;
  END

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (OLD.company_or_individual_name IS DISTINCT FROM NEW.company_or_individual_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);
    changes = changes + 1;
  END

  /* FEDERAL_MARITAL_STATUS */
  IF (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);
    changes = changes + 1;
  END

  /* NUMBER_OF_DEPENDENTS */
  IF (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);
    changes = changes + 1;
  END

  /* DISTRIBUTION_CODE_1099R */
  IF (OLD.distribution_code_1099r IS DISTINCT FROM NEW.distribution_code_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);
    changes = changes + 1;
  END

  /* TAX_AMT_DETERMINED_1099R */
  IF (OLD.tax_amt_determined_1099r IS DISTINCT FROM NEW.tax_amt_determined_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);
    changes = changes + 1;
  END

  /* TOTAL_DISTRIBUTION_1099R */
  IF (OLD.total_distribution_1099r IS DISTINCT FROM NEW.total_distribution_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);
    changes = changes + 1;
  END

  /* PENSION_PLAN_1099R */
  IF (OLD.pension_plan_1099r IS DISTINCT FROM NEW.pension_plan_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);
    changes = changes + 1;
  END

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (OLD.makeup_fica_on_cleanup_pr IS DISTINCT FROM NEW.makeup_fica_on_cleanup_pr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);
    changes = changes + 1;
  END

  /* SY_HR_EEO_NBR */
  IF (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);
    changes = changes + 1;
  END

  /* SECONDARY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1816, :blob_nbr);
    changes = changes + 1;
  END

  /* CO_PR_CHECK_TEMPLATES_NBR */
  IF (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);
    changes = changes + 1;
  END

  /* GENERATE_SECOND_CHECK */
  IF (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1884, OLD.generate_second_check);
    changes = changes + 1;
  END

  /* SALARY_AMOUNT */
  IF (OLD.salary_amount IS DISTINCT FROM NEW.salary_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1809, OLD.salary_amount);
    changes = changes + 1;
  END

  /* STANDARD_HOURS */
  IF (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1808, OLD.standard_hours);
    changes = changes + 1;
  END

  /* NEXT_RAISE_AMOUNT */
  IF (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);
    changes = changes + 1;
  END

  /* NEXT_RAISE_PERCENTAGE */
  IF (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);
    changes = changes + 1;
  END

  /* NEXT_RAISE_RATE */
  IF (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);
    changes = changes + 1;
  END

  /* WORKERS_COMP_WAGE_LIMIT */
  IF (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);
    changes = changes + 1;
  END

  /* GROUP_TERM_POLICY_AMOUNT */
  IF (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_VALUE */
  IF (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);
    changes = changes + 1;
  END

  /* CALCULATED_SALARY */
  IF (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1801, OLD.calculated_salary);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* HIGHLY_COMPENSATED */
  IF (OLD.highly_compensated IS DISTINCT FROM NEW.highly_compensated) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1919, OLD.highly_compensated);
    changes = changes + 1;
  END

  /* CORPORATE_OFFICER */
  IF (OLD.corporate_officer IS DISTINCT FROM NEW.corporate_officer) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1920, OLD.corporate_officer);
    changes = changes + 1;
  END

  /* CO_JOBS_NBR */
  IF (OLD.co_jobs_nbr IS DISTINCT FROM NEW.co_jobs_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* EE_ENABLED */
  IF (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1921, OLD.ee_enabled);
    changes = changes + 1;
  END

  /* GTL_NUMBER_OF_HOURS */
  IF (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);
    changes = changes + 1;
  END

  /* GTL_RATE */
  IF (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1817, OLD.gtl_rate);
    changes = changes + 1;
  END

  /* AUTO_UPDATE_RATES */
  IF (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);
    changes = changes + 1;
  END

  /* ELIGIBLE_FOR_REHIRE */
  IF (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);
    changes = changes + 1;
  END

  /* SELFSERVE_ENABLED */
  IF (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);
    changes = changes + 1;
  END

  /* SELFSERVE_USERNAME */
  IF (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1868, OLD.selfserve_username);
    changes = changes + 1;
  END

  /* SELFSERVE_PASSWORD */
  IF (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1867, OLD.selfserve_password);
    changes = changes + 1;
  END

  /* SELFSERVE_LAST_LOGIN */
  IF (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);
    changes = changes + 1;
  END

  /* PRINT_VOUCHER */
  IF (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1882, OLD.print_voucher);
    changes = changes + 1;
  END

  /* HEALTHCARE_COVERAGE */
  IF (OLD.healthcare_coverage IS DISTINCT FROM NEW.healthcare_coverage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);
    changes = changes + 1;
  END

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF (OLD.fui_rate_credit_override IS DISTINCT FROM NEW.fui_rate_credit_override) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);
    changes = changes + 1;
  END

  /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
  IF (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);
    changes = changes + 1;
  END

  /* WC_WAGE_LIMIT_FREQUENCY */
  IF (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1864, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1879, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1865, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1880, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1866, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1881, OLD.login_answer3);
    changes = changes + 1;
  END

  /* LOGIN_DATE */
  IF (OLD.login_date IS DISTINCT FROM NEW.login_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2785, OLD.login_date);
    changes = changes + 1;
  END

  /* LOGIN_ATTEMPTS */
  IF (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2786, OLD.login_attempts);
    changes = changes + 1;
  END

  /* CO_BENEFIT_DISCOUNT_NBR */
  IF (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);
    changes = changes + 1;
  END

  /* BENEFITS_ENABLED */
  IF (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);
    changes = changes + 1;
  END

  /* EXISTING_PATIENT */
  IF (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2914, OLD.existing_patient);
    changes = changes + 1;
  END

  /* PCP */
  IF (OLD.pcp IS DISTINCT FROM NEW.pcp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2915, OLD.pcp);
    changes = changes + 1;
  END

  /* TIME_OFF_ENABLED */
  IF (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAILABLE */
  IF (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAIL_DATE */
  IF (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);
    changes = changes + 1;
  END

  /* BENEFIT_ENROLLMENT_COMPLETE */
  IF (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT */
  IF (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT_DATE */
  IF (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);
    changes = changes + 1;
  END

  /* W2 */
  IF (OLD.w2 IS DISTINCT FROM NEW.w2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2922, OLD.w2);
    changes = changes + 1;
  END

  /* W2_FORM_ON_FILE */
  IF (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);
    changes = changes + 1;
  END

  /* BENEFIT_E_MAIL_ADDRESS */
  IF (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);
    changes = changes + 1;
  END

  /* CO_HR_POSITION_GRADES_NBR */
  IF (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2926, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2927, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2928, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2929, OLD.sec_answer2);
    changes = changes + 1;
  END

  /* ACA_STATUS */
  IF (OLD.aca_status IS DISTINCT FROM NEW.aca_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3188, OLD.aca_status);
    changes = changes + 1;
  END

  /* WORKERS_COMP_MIN_WAGE_LIMIT */
  IF (OLD.workers_comp_min_wage_limit IS DISTINCT FROM NEW.workers_comp_min_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3189, OLD.workers_comp_min_wage_limit);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE_ACCEPT_DATE */
  IF (OLD.ess_terms_of_use_accept_date IS DISTINCT FROM NEW.ess_terms_of_use_accept_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3192, OLD.ess_terms_of_use_accept_date);
    changes = changes + 1;
  END

  /* ACA_STANDARD_HOURS */
  IF (OLD.aca_standard_hours IS DISTINCT FROM NEW.aca_standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3218, OLD.aca_standard_hours);
    changes = changes + 1;
  END

  /* ACA_COVERAGE_OFFER */
  IF (OLD.aca_coverage_offer IS DISTINCT FROM NEW.aca_coverage_offer) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3237, OLD.aca_coverage_offer);
    changes = changes + 1;
  END

  /* ACA_CO_BENEFIT_SUBTYPE_NBR */
  IF (OLD.aca_co_benefit_subtype_nbr IS DISTINCT FROM NEW.aca_co_benefit_subtype_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3243, OLD.aca_co_benefit_subtype_nbr);
    changes = changes + 1;
  END

  /* EE_ACA_SAFE_HARBOR */
  IF (OLD.ee_aca_safe_harbor IS DISTINCT FROM NEW.ee_aca_safe_harbor) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3244, OLD.ee_aca_safe_harbor);
    changes = changes + 1;
  END

  /* ACA_POLICY_ORIGIN */
  IF (OLD.aca_policy_origin IS DISTINCT FROM NEW.aca_policy_origin) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3245, OLD.aca_policy_origin);
    changes = changes + 1;
  END

  /* ENABLE_ANALYTICS */
  IF (OLD.enable_analytics IS DISTINCT FROM NEW.enable_analytics) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3267, OLD.enable_analytics);
    changes = changes + 1;
  END

  /* BENEFITS_ELIGIBLE */
  IF (OLD.benefits_eligible IS DISTINCT FROM NEW.benefits_eligible) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3289, OLD.benefits_eligible);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_2 FOR EE Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO_DIVISION */
  IF ((NEW.co_division_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_division
    WHERE co_division_nbr = NEW.co_division_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_division_nbr', NEW.co_division_nbr, 'co_division', NEW.effective_date);

    SELECT rec_version FROM co_division
    WHERE co_division_nbr = NEW.co_division_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_division_nbr', NEW.co_division_nbr, 'co_division', NEW.effective_until - 1);
  END

  /* Check reference to CO_BRANCH */
  IF ((NEW.co_branch_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_branch
    WHERE co_branch_nbr = NEW.co_branch_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_branch_nbr', NEW.co_branch_nbr, 'co_branch', NEW.effective_date);

    SELECT rec_version FROM co_branch
    WHERE co_branch_nbr = NEW.co_branch_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_branch_nbr', NEW.co_branch_nbr, 'co_branch', NEW.effective_until - 1);
  END

  /* Check reference to CO_DEPARTMENT */
  IF ((NEW.co_department_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_department
    WHERE co_department_nbr = NEW.co_department_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_department_nbr', NEW.co_department_nbr, 'co_department', NEW.effective_date);

    SELECT rec_version FROM co_department
    WHERE co_department_nbr = NEW.co_department_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_department_nbr', NEW.co_department_nbr, 'co_department', NEW.effective_until - 1);
  END

  /* Check reference to CO_TEAM */
  IF ((NEW.co_team_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_team
    WHERE co_team_nbr = NEW.co_team_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_team_nbr', NEW.co_team_nbr, 'co_team', NEW.effective_date);

    SELECT rec_version FROM co_team
    WHERE co_team_nbr = NEW.co_team_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_team_nbr', NEW.co_team_nbr, 'co_team', NEW.effective_until - 1);
  END

  /* Check reference to CL_E_D_GROUPS */
  IF ((NEW.ald_cl_e_d_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.ald_cl_e_d_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'ald_cl_e_d_groups_nbr', NEW.ald_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_date);

    SELECT rec_version FROM cl_e_d_groups
    WHERE cl_e_d_groups_nbr = NEW.ald_cl_e_d_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'ald_cl_e_d_groups_nbr', NEW.ald_cl_e_d_groups_nbr, 'cl_e_d_groups', NEW.effective_until - 1);
  END

  /* Check reference to CO_SHIFTS */
  IF ((NEW.autopay_co_shifts_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_shifts
    WHERE co_shifts_nbr = NEW.autopay_co_shifts_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'autopay_co_shifts_nbr', NEW.autopay_co_shifts_nbr, 'co_shifts', NEW.effective_date);

    SELECT rec_version FROM co_shifts
    WHERE co_shifts_nbr = NEW.autopay_co_shifts_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'autopay_co_shifts_nbr', NEW.autopay_co_shifts_nbr, 'co_shifts', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_APPLICANT */
  IF ((NEW.co_hr_applicant_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_applicant
    WHERE co_hr_applicant_nbr = NEW.co_hr_applicant_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_applicant_nbr', NEW.co_hr_applicant_nbr, 'co_hr_applicant', NEW.effective_date);

    SELECT rec_version FROM co_hr_applicant
    WHERE co_hr_applicant_nbr = NEW.co_hr_applicant_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_applicant_nbr', NEW.co_hr_applicant_nbr, 'co_hr_applicant', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_POSITIONS */
  IF ((NEW.co_hr_positions_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_positions_nbr IS DISTINCT FROM NEW.co_hr_positions_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_positions
    WHERE co_hr_positions_nbr = NEW.co_hr_positions_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_positions_nbr', NEW.co_hr_positions_nbr, 'co_hr_positions', NEW.effective_date);

    SELECT rec_version FROM co_hr_positions
    WHERE co_hr_positions_nbr = NEW.co_hr_positions_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_positions_nbr', NEW.co_hr_positions_nbr, 'co_hr_positions', NEW.effective_until - 1);
  END

  /* Check reference to CO_PAY_GROUP */
  IF ((NEW.co_pay_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_pay_group
    WHERE co_pay_group_nbr = NEW.co_pay_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_pay_group_nbr', NEW.co_pay_group_nbr, 'co_pay_group', NEW.effective_date);

    SELECT rec_version FROM co_pay_group
    WHERE co_pay_group_nbr = NEW.co_pay_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_pay_group_nbr', NEW.co_pay_group_nbr, 'co_pay_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_DELIVERY_GROUP */
  IF ((NEW.cl_delivery_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.cl_delivery_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'cl_delivery_group_nbr', NEW.cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_date);

    SELECT rec_version FROM cl_delivery_group
    WHERE cl_delivery_group_nbr = NEW.cl_delivery_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'cl_delivery_group_nbr', NEW.cl_delivery_group_nbr, 'cl_delivery_group', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_PERFORMANCE_RATINGS */
  IF ((NEW.co_hr_performance_ratings_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_performance_ratings
    WHERE co_hr_performance_ratings_nbr = NEW.co_hr_performance_ratings_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_performance_ratings_nbr', NEW.co_hr_performance_ratings_nbr, 'co_hr_performance_ratings', NEW.effective_date);

    SELECT rec_version FROM co_hr_performance_ratings
    WHERE co_hr_performance_ratings_nbr = NEW.co_hr_performance_ratings_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_performance_ratings_nbr', NEW.co_hr_performance_ratings_nbr, 'co_hr_performance_ratings', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_RECRUITERS */
  IF ((NEW.co_hr_recruiters_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_recruiters
    WHERE co_hr_recruiters_nbr = NEW.co_hr_recruiters_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_recruiters_nbr', NEW.co_hr_recruiters_nbr, 'co_hr_recruiters', NEW.effective_date);

    SELECT rec_version FROM co_hr_recruiters
    WHERE co_hr_recruiters_nbr = NEW.co_hr_recruiters_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_recruiters_nbr', NEW.co_hr_recruiters_nbr, 'co_hr_recruiters', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_REFERRALS */
  IF ((NEW.co_hr_referrals_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_referrals
    WHERE co_hr_referrals_nbr = NEW.co_hr_referrals_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_referrals_nbr', NEW.co_hr_referrals_nbr, 'co_hr_referrals', NEW.effective_date);

    SELECT rec_version FROM co_hr_referrals
    WHERE co_hr_referrals_nbr = NEW.co_hr_referrals_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_referrals_nbr', NEW.co_hr_referrals_nbr, 'co_hr_referrals', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_SUPERVISORS */
  IF ((NEW.co_hr_supervisors_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_supervisors
    WHERE co_hr_supervisors_nbr = NEW.co_hr_supervisors_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_supervisors_nbr', NEW.co_hr_supervisors_nbr, 'co_hr_supervisors', NEW.effective_date);

    SELECT rec_version FROM co_hr_supervisors
    WHERE co_hr_supervisors_nbr = NEW.co_hr_supervisors_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_supervisors_nbr', NEW.co_hr_supervisors_nbr, 'co_hr_supervisors', NEW.effective_until - 1);
  END

  /* Check reference to CO_UNIONS */
  IF ((NEW.co_unions_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_unions
    WHERE co_unions_nbr = NEW.co_unions_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_unions_nbr', NEW.co_unions_nbr, 'co_unions', NEW.effective_date);

    SELECT rec_version FROM co_unions
    WHERE co_unions_nbr = NEW.co_unions_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_unions_nbr', NEW.co_unions_nbr, 'co_unions', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_check_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_check_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'pr_check_mb_group_nbr', NEW.pr_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_check_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'pr_check_mb_group_nbr', NEW.pr_check_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_ee_report_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_ee_report_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'pr_ee_report_mb_group_nbr', NEW.pr_ee_report_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_ee_report_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'pr_ee_report_mb_group_nbr', NEW.pr_ee_report_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.pr_ee_report_sec_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_ee_report_sec_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'pr_ee_report_sec_mb_group_nbr', NEW.pr_ee_report_sec_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.pr_ee_report_sec_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'pr_ee_report_sec_mb_group_nbr', NEW.pr_ee_report_sec_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CL_MAIL_BOX_GROUP */
  IF ((NEW.tax_ee_return_mb_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_ee_return_mb_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'tax_ee_return_mb_group_nbr', NEW.tax_ee_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_date);

    SELECT rec_version FROM cl_mail_box_group
    WHERE cl_mail_box_group_nbr = NEW.tax_ee_return_mb_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'tax_ee_return_mb_group_nbr', NEW.tax_ee_return_mb_group_nbr, 'cl_mail_box_group', NEW.effective_until - 1);
  END

  /* Check reference to CO_JOBS */
  IF ((NEW.co_jobs_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_jobs_nbr IS DISTINCT FROM NEW.co_jobs_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_jobs
    WHERE co_jobs_nbr = NEW.co_jobs_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_jobs_nbr', NEW.co_jobs_nbr, 'co_jobs', NEW.effective_date);

    SELECT rec_version FROM co_jobs
    WHERE co_jobs_nbr = NEW.co_jobs_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_jobs_nbr', NEW.co_jobs_nbr, 'co_jobs', NEW.effective_until - 1);
  END

  /* Check reference to CO_WORKERS_COMP */
  IF ((NEW.co_workers_comp_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_workers_comp
    WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_workers_comp_nbr', NEW.co_workers_comp_nbr, 'co_workers_comp', NEW.effective_date);

    SELECT rec_version FROM co_workers_comp
    WHERE co_workers_comp_nbr = NEW.co_workers_comp_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_workers_comp_nbr', NEW.co_workers_comp_nbr, 'co_workers_comp', NEW.effective_until - 1);
  END

  /* Check reference to CL_PERSON */
  IF ((NEW.cl_person_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.cl_person_nbr IS DISTINCT FROM NEW.cl_person_nbr))) THEN
  BEGIN
    SELECT rec_version FROM cl_person
    WHERE cl_person_nbr = NEW.cl_person_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'cl_person_nbr', NEW.cl_person_nbr, 'cl_person', NEW.effective_date);

    SELECT rec_version FROM cl_person
    WHERE cl_person_nbr = NEW.cl_person_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'cl_person_nbr', NEW.cl_person_nbr, 'cl_person', NEW.effective_until - 1);
  END

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END

  /* Check reference to CO_PR_CHECK_TEMPLATES */
  IF ((NEW.co_pr_check_templates_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_pr_check_templates
    WHERE co_pr_check_templates_nbr = NEW.co_pr_check_templates_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_pr_check_templates_nbr', NEW.co_pr_check_templates_nbr, 'co_pr_check_templates', NEW.effective_date);

    SELECT rec_version FROM co_pr_check_templates
    WHERE co_pr_check_templates_nbr = NEW.co_pr_check_templates_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_pr_check_templates_nbr', NEW.co_pr_check_templates_nbr, 'co_pr_check_templates', NEW.effective_until - 1);
  END

  /* Check reference to CO_HR_POSITION_GRADES */
  IF ((NEW.co_hr_position_grades_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_hr_position_grades
    WHERE co_hr_position_grades_nbr = NEW.co_hr_position_grades_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_position_grades_nbr', NEW.co_hr_position_grades_nbr, 'co_hr_position_grades', NEW.effective_date);

    SELECT rec_version FROM co_hr_position_grades
    WHERE co_hr_position_grades_nbr = NEW.co_hr_position_grades_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_hr_position_grades_nbr', NEW.co_hr_position_grades_nbr, 'co_hr_position_grades', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_DISCOUNT */
  IF ((NEW.co_benefit_discount_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_discount
    WHERE co_benefit_discount_nbr = NEW.co_benefit_discount_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_benefit_discount_nbr', NEW.co_benefit_discount_nbr, 'co_benefit_discount', NEW.effective_date);

    SELECT rec_version FROM co_benefit_discount
    WHERE co_benefit_discount_nbr = NEW.co_benefit_discount_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'co_benefit_discount_nbr', NEW.co_benefit_discount_nbr, 'co_benefit_discount', NEW.effective_until - 1);
  END

  /* Check reference to EE_STATES */
  IF ((NEW.home_tax_ee_states_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.home_tax_ee_states_nbr IS DISTINCT FROM NEW.home_tax_ee_states_nbr))) THEN
  BEGIN
    SELECT rec_version FROM ee_states
    WHERE ee_states_nbr = NEW.home_tax_ee_states_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'home_tax_ee_states_nbr', NEW.home_tax_ee_states_nbr, 'ee_states', NEW.effective_date);

    SELECT rec_version FROM ee_states
    WHERE ee_states_nbr = NEW.home_tax_ee_states_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'home_tax_ee_states_nbr', NEW.home_tax_ee_states_nbr, 'ee_states', NEW.effective_until - 1);
  END

  /* Check reference to CO_BENEFIT_SUBTYPE */
  IF ((NEW.aca_co_benefit_subtype_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.aca_co_benefit_subtype_nbr IS DISTINCT FROM NEW.aca_co_benefit_subtype_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.aca_co_benefit_subtype_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'aca_co_benefit_subtype_nbr', NEW.aca_co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_date);

    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.aca_co_benefit_subtype_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'ee', NEW.ee_nbr, 'aca_co_benefit_subtype_nbr', NEW.aca_co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_AD_EE_BENEFICIARY_9 FOR EE_BENEFICIARY After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(169, OLD.rec_version, OLD.ee_beneficiary_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3119, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3120, OLD.effective_until);

  /* EE_BENEFITS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3121, OLD.ee_benefits_nbr);

  /* CL_PERSON_DEPENDENTS_NBR */
  IF (OLD.cl_person_dependents_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3122, OLD.cl_person_dependents_nbr);

  /* PERCENTAGE */
  IF (OLD.percentage IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3123, OLD.percentage);

  /* PRIMARY_BENEFICIARY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3124, OLD.primary_beneficiary);

  /* ACA_DEP_PLAN_BEGIN_DATE */
  IF (OLD.aca_dep_plan_begin_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3241, OLD.aca_dep_plan_begin_date);

  /* ACA_DEP_PLAN_END_DATE */
  IF (OLD.aca_dep_plan_end_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3301, OLD.aca_dep_plan_end_date);

END

^

CREATE TRIGGER T_AU_EE_BENEFICIARY_9 FOR EE_BENEFICIARY After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(169, NEW.rec_version, NEW.ee_beneficiary_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3119, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3120, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_BENEFITS_NBR */
  IF (OLD.ee_benefits_nbr IS DISTINCT FROM NEW.ee_benefits_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3121, OLD.ee_benefits_nbr);
    changes = changes + 1;
  END

  /* CL_PERSON_DEPENDENTS_NBR */
  IF (OLD.cl_person_dependents_nbr IS DISTINCT FROM NEW.cl_person_dependents_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3122, OLD.cl_person_dependents_nbr);
    changes = changes + 1;
  END

  /* PERCENTAGE */
  IF (OLD.percentage IS DISTINCT FROM NEW.percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3123, OLD.percentage);
    changes = changes + 1;
  END

  /* PRIMARY_BENEFICIARY */
  IF (OLD.primary_beneficiary IS DISTINCT FROM NEW.primary_beneficiary) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3124, OLD.primary_beneficiary);
    changes = changes + 1;
  END

  /* ACA_DEP_PLAN_BEGIN_DATE */
  IF (OLD.aca_dep_plan_begin_date IS DISTINCT FROM NEW.aca_dep_plan_begin_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3241, OLD.aca_dep_plan_begin_date);
    changes = changes + 1;
  END

  /* ACA_DEP_PLAN_END_DATE */
  IF (OLD.aca_dep_plan_end_date IS DISTINCT FROM NEW.aca_dep_plan_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3301, OLD.aca_dep_plan_end_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_EE_TIME_OFF_ACCRUAL_9 FOR EE_TIME_OFF_ACCRUAL After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(136, OLD.rec_version, OLD.ee_time_off_accrual_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2232, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2828, OLD.effective_until);

  /* EE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2241, OLD.ee_nbr);

  /* CO_TIME_OFF_ACCRUAL_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2242, OLD.co_time_off_accrual_nbr);

  /* EFFECTIVE_ACCRUAL_DATE */
  IF (OLD.effective_accrual_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2244, OLD.effective_accrual_date);

  /* NEXT_ACCRUE_DATE */
  IF (OLD.next_accrue_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2245, OLD.next_accrue_date);

  /* RLLOVR_CO_TIME_OFF_ACCRUAL_NBR */
  IF (OLD.rllovr_co_time_off_accrual_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2243, OLD.rllovr_co_time_off_accrual_nbr);

  /* ROLLOVER_DATE */
  IF (OLD.rollover_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2246, OLD.rollover_date);

  /* NEXT_RESET_DATE */
  IF (OLD.next_reset_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2247, OLD.next_reset_date);

  /* CURRENT_ACCRUED */
  IF (OLD.current_accrued IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2231, OLD.current_accrued);

  /* CURRENT_USED */
  IF (OLD.current_used IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2230, OLD.current_used);

  /* ANNUAL_ACCRUAL_MAXIMUM */
  IF (OLD.annual_accrual_maximum IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2229, OLD.annual_accrual_maximum);

  /* OVERRIDE_RATE */
  IF (OLD.override_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2228, OLD.override_rate);

  /* JUST_RESET */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2253, OLD.just_reset);

  /* MARKED_ACCRUED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2233, OLD.marked_accrued);

  /* MARKED_USED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2234, OLD.marked_used);

  /* STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2254, OLD.status);

  /* ROLLOVER_FREQ */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2255, OLD.rollover_freq);

  /* ROLLOVER_MONTH_NUMBER */
  IF (OLD.rollover_month_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2251, OLD.rollover_month_number);

  /* ROLLOVER_PAYROLL_OF_MONTH */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2256, OLD.rollover_payroll_of_month);

  /* ROLLOVER_OFFSET */
  IF (OLD.rollover_offset IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2252, OLD.rollover_offset);

  /* OVERRIDE_RESET_DATE */
  IF (OLD.override_reset_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3275, OLD.override_reset_date);

END

^

CREATE TRIGGER T_AU_EE_TIME_OFF_ACCRUAL_9 FOR EE_TIME_OFF_ACCRUAL After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(136, NEW.rec_version, NEW.ee_time_off_accrual_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2232, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2828, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2241, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* CO_TIME_OFF_ACCRUAL_NBR */
  IF (OLD.co_time_off_accrual_nbr IS DISTINCT FROM NEW.co_time_off_accrual_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2242, OLD.co_time_off_accrual_nbr);
    changes = changes + 1;
  END

  /* EFFECTIVE_ACCRUAL_DATE */
  IF (OLD.effective_accrual_date IS DISTINCT FROM NEW.effective_accrual_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2244, OLD.effective_accrual_date);
    changes = changes + 1;
  END

  /* NEXT_ACCRUE_DATE */
  IF (OLD.next_accrue_date IS DISTINCT FROM NEW.next_accrue_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2245, OLD.next_accrue_date);
    changes = changes + 1;
  END

  /* RLLOVR_CO_TIME_OFF_ACCRUAL_NBR */
  IF (OLD.rllovr_co_time_off_accrual_nbr IS DISTINCT FROM NEW.rllovr_co_time_off_accrual_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2243, OLD.rllovr_co_time_off_accrual_nbr);
    changes = changes + 1;
  END

  /* ROLLOVER_DATE */
  IF (OLD.rollover_date IS DISTINCT FROM NEW.rollover_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2246, OLD.rollover_date);
    changes = changes + 1;
  END

  /* NEXT_RESET_DATE */
  IF (OLD.next_reset_date IS DISTINCT FROM NEW.next_reset_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2247, OLD.next_reset_date);
    changes = changes + 1;
  END

  /* CURRENT_ACCRUED */
  IF (OLD.current_accrued IS DISTINCT FROM NEW.current_accrued) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2231, OLD.current_accrued);
    changes = changes + 1;
  END

  /* CURRENT_USED */
  IF (OLD.current_used IS DISTINCT FROM NEW.current_used) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2230, OLD.current_used);
    changes = changes + 1;
  END

  /* ANNUAL_ACCRUAL_MAXIMUM */
  IF (OLD.annual_accrual_maximum IS DISTINCT FROM NEW.annual_accrual_maximum) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2229, OLD.annual_accrual_maximum);
    changes = changes + 1;
  END

  /* OVERRIDE_RATE */
  IF (OLD.override_rate IS DISTINCT FROM NEW.override_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2228, OLD.override_rate);
    changes = changes + 1;
  END

  /* JUST_RESET */
  IF (OLD.just_reset IS DISTINCT FROM NEW.just_reset) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2253, OLD.just_reset);
    changes = changes + 1;
  END

  /* MARKED_ACCRUED */
  IF (OLD.marked_accrued IS DISTINCT FROM NEW.marked_accrued) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2233, OLD.marked_accrued);
    changes = changes + 1;
  END

  /* MARKED_USED */
  IF (OLD.marked_used IS DISTINCT FROM NEW.marked_used) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2234, OLD.marked_used);
    changes = changes + 1;
  END

  /* STATUS */
  IF (OLD.status IS DISTINCT FROM NEW.status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2254, OLD.status);
    changes = changes + 1;
  END

  /* ROLLOVER_FREQ */
  IF (OLD.rollover_freq IS DISTINCT FROM NEW.rollover_freq) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2255, OLD.rollover_freq);
    changes = changes + 1;
  END

  /* ROLLOVER_MONTH_NUMBER */
  IF (OLD.rollover_month_number IS DISTINCT FROM NEW.rollover_month_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2251, OLD.rollover_month_number);
    changes = changes + 1;
  END

  /* ROLLOVER_PAYROLL_OF_MONTH */
  IF (OLD.rollover_payroll_of_month IS DISTINCT FROM NEW.rollover_payroll_of_month) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2256, OLD.rollover_payroll_of_month);
    changes = changes + 1;
  END

  /* ROLLOVER_OFFSET */
  IF (OLD.rollover_offset IS DISTINCT FROM NEW.rollover_offset) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2252, OLD.rollover_offset);
    changes = changes + 1;
  END

  /* OVERRIDE_RESET_DATE */
  IF (OLD.override_reset_date IS DISTINCT FROM NEW.override_reset_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3275, OLD.override_reset_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_CO_BENEFIT_ENROLLMENT_9 FOR CO_BENEFIT_ENROLLMENT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(174, OLD.rec_version, OLD.co_benefit_enrollment_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3195, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3196, OLD.effective_until);

  /* CO_BENEFIT_CATEGORY_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3197, OLD.co_benefit_category_nbr);

  /* ENROLLMENT_START_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3198, OLD.enrollment_start_date);

  /* ENROLLMENT_END_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3199, OLD.enrollment_end_date);

  /* CATEGORY_COVERAGE_START_DATE */
  IF (OLD.category_coverage_start_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3286, OLD.category_coverage_start_date);

  /* CATEGORY_COVERAGE_END_DATE */
  IF (OLD.category_coverage_end_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3287, OLD.category_coverage_end_date);

END

^

CREATE TRIGGER T_AU_CO_BENEFIT_ENROLLMENT_9 FOR CO_BENEFIT_ENROLLMENT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(174, NEW.rec_version, NEW.co_benefit_enrollment_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3195, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3196, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_BENEFIT_CATEGORY_NBR */
  IF (OLD.co_benefit_category_nbr IS DISTINCT FROM NEW.co_benefit_category_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3197, OLD.co_benefit_category_nbr);
    changes = changes + 1;
  END

  /* ENROLLMENT_START_DATE */
  IF (OLD.enrollment_start_date IS DISTINCT FROM NEW.enrollment_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3198, OLD.enrollment_start_date);
    changes = changes + 1;
  END

  /* ENROLLMENT_END_DATE */
  IF (OLD.enrollment_end_date IS DISTINCT FROM NEW.enrollment_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3199, OLD.enrollment_end_date);
    changes = changes + 1;
  END

  /* CATEGORY_COVERAGE_START_DATE */
  IF (OLD.category_coverage_start_date IS DISTINCT FROM NEW.category_coverage_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3286, OLD.category_coverage_start_date);
    changes = changes + 1;
  END

  /* CATEGORY_COVERAGE_END_DATE */
  IF (OLD.category_coverage_end_date IS DISTINCT FROM NEW.category_coverage_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3287, OLD.category_coverage_end_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_CO_ACA_CERT_ELIGIBILITY_9 FOR CO_ACA_CERT_ELIGIBILITY After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(179, OLD.rec_version, OLD.co_aca_cert_eligibility_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3304, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3305, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3306, OLD.co_nbr);

  /* ACA_CERT_ELIGIBILITY_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3307, OLD.aca_cert_eligibility_type);

  /* DESCRIPTION */
  IF (OLD.description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3308, OLD.description);

END

^

CREATE TRIGGER T_AI_CO_ACA_CERT_ELIGIBILITY_9 FOR CO_ACA_CERT_ELIGIBILITY After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(179, NEW.rec_version, NEW.co_aca_cert_eligibility_nbr, 'I');
END

^

CREATE TRIGGER T_AU_CO_ACA_CERT_ELIGIBILITY_9 FOR CO_ACA_CERT_ELIGIBILITY After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(179, NEW.rec_version, NEW.co_aca_cert_eligibility_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3304, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3305, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3306, OLD.co_nbr);
    changes = changes + 1;
  END

  /* ACA_CERT_ELIGIBILITY_TYPE */
  IF (OLD.aca_cert_eligibility_type IS DISTINCT FROM NEW.aca_cert_eligibility_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3307, OLD.aca_cert_eligibility_type);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3308, OLD.description);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_ACA_CERT_ELIGIBILITY_2 FOR CO_ACA_CERT_ELIGIBILITY Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_aca_cert_eligibility', NEW.co_aca_cert_eligibility_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_aca_cert_eligibility', NEW.co_aca_cert_eligibility_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_CO_ACA_CERT_ELIGIBILITY_1 FOR CO_ACA_CERT_ELIGIBILITY Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_co_aca_cert_eligibility_ver;

  IF (EXISTS (SELECT 1 FROM co_aca_cert_eligibility WHERE co_aca_cert_eligibility_nbr = NEW.co_aca_cert_eligibility_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('co_aca_cert_eligibility', NEW.co_aca_cert_eligibility_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_CO_ACA_CERT_ELIGIBILITY_1 FOR CO_ACA_CERT_ELIGIBILITY Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.co_aca_cert_eligibility_nbr IS DISTINCT FROM OLD.co_aca_cert_eligibility_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('co_aca_cert_eligibility', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_CO_BENEFIT_AGE_BANDS_9 FOR CO_BENEFIT_AGE_BANDS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(178, OLD.rec_version, OLD.co_benefit_age_bands_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3292, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3293, OLD.effective_until);

  /* CO_BENEFIT_SUBTYPE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3294, OLD.co_benefit_subtype_nbr);

  /* EFFECTIVE_START_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3295, OLD.effective_start_date);

  /* EFFECTIVE_END_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3296, OLD.effective_end_date);

  /* AGE_MIN */
  IF (OLD.age_min IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3297, OLD.age_min);

  /* AGE_MAX */
  IF (OLD.age_max IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3298, OLD.age_max);

  /* MULTIPLIER */
  IF (OLD.multiplier IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3299, OLD.multiplier);

END

^

CREATE TRIGGER T_AI_CO_BENEFIT_AGE_BANDS_9 FOR CO_BENEFIT_AGE_BANDS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(178, NEW.rec_version, NEW.co_benefit_age_bands_nbr, 'I');
END

^

CREATE TRIGGER T_AU_CO_BENEFIT_AGE_BANDS_9 FOR CO_BENEFIT_AGE_BANDS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(178, NEW.rec_version, NEW.co_benefit_age_bands_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3292, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3293, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_BENEFIT_SUBTYPE_NBR */
  IF (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3294, OLD.co_benefit_subtype_nbr);
    changes = changes + 1;
  END

  /* EFFECTIVE_START_DATE */
  IF (OLD.effective_start_date IS DISTINCT FROM NEW.effective_start_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3295, OLD.effective_start_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_END_DATE */
  IF (OLD.effective_end_date IS DISTINCT FROM NEW.effective_end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3296, OLD.effective_end_date);
    changes = changes + 1;
  END

  /* AGE_MIN */
  IF (OLD.age_min IS DISTINCT FROM NEW.age_min) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3297, OLD.age_min);
    changes = changes + 1;
  END

  /* AGE_MAX */
  IF (OLD.age_max IS DISTINCT FROM NEW.age_max) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3298, OLD.age_max);
    changes = changes + 1;
  END

  /* MULTIPLIER */
  IF (OLD.multiplier IS DISTINCT FROM NEW.multiplier) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3299, OLD.multiplier);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_BENEFIT_AGE_BANDS_2 FOR CO_BENEFIT_AGE_BANDS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO_BENEFIT_SUBTYPE */
  IF ((NEW.co_benefit_subtype_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_benefit_subtype_nbr IS DISTINCT FROM NEW.co_benefit_subtype_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_benefit_age_bands', NEW.co_benefit_age_bands_nbr, 'co_benefit_subtype_nbr', NEW.co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_date);

    SELECT rec_version FROM co_benefit_subtype
    WHERE co_benefit_subtype_nbr = NEW.co_benefit_subtype_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_benefit_age_bands', NEW.co_benefit_age_bands_nbr, 'co_benefit_subtype_nbr', NEW.co_benefit_subtype_nbr, 'co_benefit_subtype', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_CO_BENEFIT_AGE_BANDS_1 FOR CO_BENEFIT_AGE_BANDS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_co_benefit_age_bands_ver;

  IF (EXISTS (SELECT 1 FROM co_benefit_age_bands WHERE co_benefit_age_bands_nbr = NEW.co_benefit_age_bands_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('co_benefit_age_bands', NEW.co_benefit_age_bands_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_CO_BENEFIT_AGE_BANDS_1 FOR CO_BENEFIT_AGE_BANDS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.co_benefit_age_bands_nbr IS DISTINCT FROM OLD.co_benefit_age_bands_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('co_benefit_age_bands', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_CO_CUSTOM_LABELS_9 FOR CO_CUSTOM_LABELS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(176, OLD.rec_version, OLD.co_custom_labels_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3259, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3260, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3261, OLD.co_nbr);

  /* DIVISION */
  IF (OLD.division IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3262, OLD.division);

  /* BRANCH */
  IF (OLD.branch IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3263, OLD.branch);

  /* DEPARTMENT */
  IF (OLD.department IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3264, OLD.department);

  /* TEAM */
  IF (OLD.team IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3265, OLD.team);

  /* JOB */
  IF (OLD.job IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3266, OLD.job);

END

^

CREATE TRIGGER T_AI_CO_CUSTOM_LABELS_9 FOR CO_CUSTOM_LABELS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(176, NEW.rec_version, NEW.co_custom_labels_nbr, 'I');
END

^

CREATE TRIGGER T_AU_CO_CUSTOM_LABELS_9 FOR CO_CUSTOM_LABELS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(176, NEW.rec_version, NEW.co_custom_labels_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3259, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3260, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3261, OLD.co_nbr);
    changes = changes + 1;
  END

  /* DIVISION */
  IF (OLD.division IS DISTINCT FROM NEW.division) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3262, OLD.division);
    changes = changes + 1;
  END

  /* BRANCH */
  IF (OLD.branch IS DISTINCT FROM NEW.branch) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3263, OLD.branch);
    changes = changes + 1;
  END

  /* DEPARTMENT */
  IF (OLD.department IS DISTINCT FROM NEW.department) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3264, OLD.department);
    changes = changes + 1;
  END

  /* TEAM */
  IF (OLD.team IS DISTINCT FROM NEW.team) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3265, OLD.team);
    changes = changes + 1;
  END

  /* JOB */
  IF (OLD.job IS DISTINCT FROM NEW.job) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3266, OLD.job);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_CUSTOM_LABELS_2 FOR CO_CUSTOM_LABELS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_custom_labels', NEW.co_custom_labels_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_custom_labels', NEW.co_custom_labels_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_CO_CUSTOM_LABELS_1 FOR CO_CUSTOM_LABELS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_co_custom_labels_ver;

  IF (EXISTS (SELECT 1 FROM co_custom_labels WHERE co_custom_labels_nbr = NEW.co_custom_labels_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('co_custom_labels', NEW.co_custom_labels_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_CO_CUSTOM_LABELS_1 FOR CO_CUSTOM_LABELS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.co_custom_labels_nbr IS DISTINCT FROM OLD.co_custom_labels_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('co_custom_labels', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_CO_DOCUMENTS_9 FOR CO_DOCUMENTS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(175, OLD.rec_version, OLD.co_documents_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3249, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3250, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3251, OLD.co_nbr);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3252, OLD.name);

  /* DOCUMENT */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DOCUMENT');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@DOCUMENT', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3253, :blob_nbr);
  END

  /* FILE_NAME */
  IF (OLD.file_name IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3254, OLD.file_name);

  /* BEGIN_DATE */
  IF (OLD.begin_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3255, OLD.begin_date);

  /* END_DATE */
  IF (OLD.end_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3256, OLD.end_date);

END

^

CREATE TRIGGER T_AI_CO_DOCUMENTS_9 FOR CO_DOCUMENTS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(175, NEW.rec_version, NEW.co_documents_nbr, 'I');
END

^

CREATE TRIGGER T_AU_CO_DOCUMENTS_9 FOR CO_DOCUMENTS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(175, NEW.rec_version, NEW.co_documents_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3249, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3250, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3251, OLD.co_nbr);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3252, OLD.name);
    changes = changes + 1;
  END

  /* DOCUMENT */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DOCUMENT');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@DOCUMENT', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3253, :blob_nbr);
    changes = changes + 1;
  END

  /* FILE_NAME */
  IF (OLD.file_name IS DISTINCT FROM NEW.file_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3254, OLD.file_name);
    changes = changes + 1;
  END

  /* BEGIN_DATE */
  IF (OLD.begin_date IS DISTINCT FROM NEW.begin_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3255, OLD.begin_date);
    changes = changes + 1;
  END

  /* END_DATE */
  IF (OLD.end_date IS DISTINCT FROM NEW.end_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3256, OLD.end_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_DOCUMENTS_2 FOR CO_DOCUMENTS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_documents', NEW.co_documents_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_documents', NEW.co_documents_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_CO_DOCUMENTS_1 FOR CO_DOCUMENTS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_co_documents_ver;

  IF (EXISTS (SELECT 1 FROM co_documents WHERE co_documents_nbr = NEW.co_documents_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('co_documents', NEW.co_documents_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BUD_CO_DOCUMENTS_9 FOR CO_DOCUMENTS Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* DOCUMENT */
  IF (DELETING OR (OLD.document IS DISTINCT FROM NEW.document)) THEN
  BEGIN
    IF (OLD.document IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.document)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@DOCUMENT', blob_nbr);
  END
END

^

CREATE TRIGGER T_BU_CO_DOCUMENTS_1 FOR CO_DOCUMENTS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.co_documents_nbr IS DISTINCT FROM OLD.co_documents_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('co_documents', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_CO_DW_STORAGE_9 FOR CO_DW_STORAGE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(177, OLD.rec_version, OLD.co_dw_storage_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3278, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3279, OLD.effective_until);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3280, OLD.co_nbr);

  /* DATA_TIMESTAMP */
  IF (OLD.data_timestamp IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3281, OLD.data_timestamp);

  /* TAG */
  IF (OLD.tag IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3282, OLD.tag);

  /* PARAM_TAG */
  IF (OLD.param_tag IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3283, OLD.param_tag);

  /* REPORT_PARAMETERS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_PARAMETERS');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@REPORT_PARAMETERS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3284, :blob_nbr);
  END

  /* REPORT_DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_DATA');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@REPORT_DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3285, :blob_nbr);
  END

END

^

CREATE TRIGGER T_AI_CO_DW_STORAGE_9 FOR CO_DW_STORAGE After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(177, NEW.rec_version, NEW.co_dw_storage_nbr, 'I');
END

^

CREATE TRIGGER T_AU_CO_DW_STORAGE_9 FOR CO_DW_STORAGE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(177, NEW.rec_version, NEW.co_dw_storage_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3278, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3279, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3280, OLD.co_nbr);
    changes = changes + 1;
  END

  /* DATA_TIMESTAMP */
  IF (OLD.data_timestamp IS DISTINCT FROM NEW.data_timestamp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3281, OLD.data_timestamp);
    changes = changes + 1;
  END

  /* TAG */
  IF (OLD.tag IS DISTINCT FROM NEW.tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3282, OLD.tag);
    changes = changes + 1;
  END

  /* PARAM_TAG */
  IF (OLD.param_tag IS DISTINCT FROM NEW.param_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3283, OLD.param_tag);
    changes = changes + 1;
  END

  /* REPORT_PARAMETERS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_PARAMETERS');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@REPORT_PARAMETERS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3284, :blob_nbr);
    changes = changes + 1;
  END

  /* REPORT_DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_DATA');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@REPORT_DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3285, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_DW_STORAGE_2 FOR CO_DW_STORAGE Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to CO */
  IF ((NEW.co_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr))) THEN
  BEGIN
    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_dw_storage', NEW.co_dw_storage_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_date);

    SELECT rec_version FROM co
    WHERE co_nbr = NEW.co_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'co_dw_storage', NEW.co_dw_storage_nbr, 'co_nbr', NEW.co_nbr, 'co', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_CO_DW_STORAGE_1 FOR CO_DW_STORAGE Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_co_dw_storage_ver;

  IF (EXISTS (SELECT 1 FROM co_dw_storage WHERE co_dw_storage_nbr = NEW.co_dw_storage_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('co_dw_storage', NEW.co_dw_storage_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BUD_CO_DW_STORAGE_9 FOR CO_DW_STORAGE Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* REPORT_PARAMETERS */
  IF (DELETING OR (OLD.report_parameters IS DISTINCT FROM NEW.report_parameters)) THEN
  BEGIN
    IF (OLD.report_parameters IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.report_parameters)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@REPORT_PARAMETERS', blob_nbr);
  END

  /* REPORT_DATA */
  IF (DELETING OR (OLD.report_data IS DISTINCT FROM NEW.report_data)) THEN
  BEGIN
    IF (OLD.report_data IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.report_data)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@REPORT_DATA', blob_nbr);
  END
END

^

CREATE TRIGGER T_BU_CO_DW_STORAGE_1 FOR CO_DW_STORAGE Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.co_dw_storage_nbr IS DISTINCT FROM OLD.co_dw_storage_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('co_dw_storage', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^



COMMIT^



/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_table (nbr, name, versioned) VALUES (175, 'CO_DOCUMENTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (176, 'CO_CUSTOM_LABELS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (177, 'CO_DW_STORAGE', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (178, 'CO_BENEFIT_AGE_BANDS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (179, 'CO_ACA_CERT_ELIGIBILITY', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3240, 'EE_PRINT_VOUCHER_DEFAULT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3268, 'ENABLE_ANALYTICS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3269, 'ANALYTICS_LICENSE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 3288, 'BENEFITS_ELIGIBLE_DEFAULT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 3246, 'REQUIRED', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (75, 3270, 'CO_DIVISION_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (75, 3271, 'CO_BRANCH_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (75, 3272, 'CO_DEPARTMENT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (75, 3273, 'CO_TEAM_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (75, 3274, 'CO_PAY_GROUP_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3243, 'ACA_CO_BENEFIT_SUBTYPE_NBR', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3244, 'EE_ACA_SAFE_HARBOR', 'V', 2, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3245, 'ACA_POLICY_ORIGIN', 'C', 1, NULL, 'Y', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3267, 'ENABLE_ANALYTICS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3289, 'BENEFITS_ELIGIBLE', 'C', 1, NULL, 'Y', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (136, 3275, 'OVERRIDE_RESET_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (167, 3300, 'AGE_BANDS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (167, 3242, 'ACA_LOWEST_COST', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (169, 3241, 'ACA_DEP_PLAN_BEGIN_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (169, 3301, 'ACA_DEP_PLAN_END_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3286, 'CATEGORY_COVERAGE_START_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (174, 3287, 'CATEGORY_COVERAGE_END_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3247, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3248, 'CO_DOCUMENTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3249, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3250, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3251, 'CO_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3252, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3253, 'DOCUMENT', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3254, 'FILE_NAME', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3255, 'BEGIN_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (175, 3256, 'END_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3257, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3258, 'CO_CUSTOM_LABELS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3259, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3260, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3261, 'CO_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3262, 'DIVISION', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3263, 'BRANCH', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3264, 'DEPARTMENT', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3265, 'TEAM', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (176, 3266, 'JOB', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3276, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3277, 'CO_DW_STORAGE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3278, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3279, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3280, 'CO_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3281, 'DATA_TIMESTAMP', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3282, 'TAG', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3283, 'PARAM_TAG', 'V', 128, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3284, 'REPORT_PARAMETERS', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (177, 3285, 'REPORT_DATA', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3290, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3291, 'CO_BENEFIT_AGE_BANDS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3292, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3293, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3294, 'CO_BENEFIT_SUBTYPE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3295, 'EFFECTIVE_START_DATE', 'T', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3296, 'EFFECTIVE_END_DATE', 'T', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3297, 'AGE_MIN', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3298, 'AGE_MAX', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (178, 3299, 'MULTIPLIER', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3302, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3303, 'CO_ACA_CERT_ELIGIBILITY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3304, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3305, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3306, 'CO_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3307, 'ACA_CERT_ELIGIBILITY_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (179, 3308, 'DESCRIPTION', 'V', 80, NULL, 'N', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^



/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.2', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
