/* Check current version */

SET TERM ^;

EXECUTE block
AS
DECLARE VARIABLE req_ver VARCHAR(11);
DECLARE VARIABLE ver VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    EXECUTE STATEMENT '"This script must be executed by SYSDBA"';

  req_ver = '16.0.0.0';
  ver = 'UNKNOWN';
  SELECT VERSION
  FROM ev_database
  INTO :ver;

  IF (strcopy(ver, 0, strlen(req_ver)) <> req_ver) THEN
    EXECUTE STATEMENT '"Database version mismatch! Expected ' || req_ver || ' but found ' || ver || '"';
END^



CREATE DOMAIN EV_STR80 AS
 VARCHAR(80)
 COLLATE NONE
^

COMMIT
^


CREATE GENERATOR G_SY_DASHBOARDS
^

CREATE GENERATOR G_SY_DASHBOARDS_VER
^

CREATE GENERATOR G_SY_ANALYTICS_TIER
^

CREATE GENERATOR G_SY_ANALYTICS_TIER_VER
^

CREATE GENERATOR G_SY_VENDOR_CATEGORIES
^

CREATE GENERATOR G_SY_VENDOR_CATEGORIES_VER
^

CREATE GENERATOR G_SY_VENDORS
^

CREATE GENERATOR G_SY_VENDORS_VER
^


CREATE TABLE  SY_ANALYTICS_TIER
( 
     REC_VERSION EV_INT NOT NULL,
     SY_ANALYTICS_TIER_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     TIER_ID EV_STR40 NOT NULL,
     DASHBOARD_DEFAULT_COUNT EV_INT,
     DASHBOARD_MAX_COUNT EV_INT,
     USERS_DEFAULT_COUNT EV_INT,
     USERS_MAX_COUNT EV_INT,
     LOOKBACK_YEARS_DEFAULT EV_INT,
     LOOKBACK_YEARS_MAX EV_INT,
     MOBILE_APP EV_CHAR1,
     INTERNAL_BENCHMARKING EV_CHAR1,
        CONSTRAINT PK_SY_ANALYTICS_TIER PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SY_ANALYTICS_TIER_1 UNIQUE (SY_ANALYTICS_TIER_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SY_ANALYTICS_TIER_2 UNIQUE (SY_ANALYTICS_TIER_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_SY_ANALYTICS_TIER_1 ON SY_ANALYTICS_TIER (TIER_ID,EFFECTIVE_UNTIL)
^


GRANT ALL ON SY_ANALYTICS_TIER TO EUSER
^

CREATE TABLE  SY_DASHBOARDS
( 
     REC_VERSION EV_INT NOT NULL,
     SY_DASHBOARDS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     DASHBOARD_TYPE EV_CHAR1,
     SY_ANALYTICS_TIER_NBR EV_INT NOT NULL,
     NOTES EV_BLOB_TXT,
     DASHBOARD_ID EV_INT NOT NULL,
     DESCRIPTION EV_STR80 NOT NULL,
        CONSTRAINT PK_SY_DASHBOARDS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SY_DASHBOARDS_1 UNIQUE (SY_DASHBOARDS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SY_DASHBOARDS_2 UNIQUE (SY_DASHBOARDS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SY_DASHBOARDS_1 ON SY_DASHBOARDS (SY_ANALYTICS_TIER_NBR,EFFECTIVE_UNTIL)
^


GRANT ALL ON SY_DASHBOARDS TO EUSER
^

CREATE TABLE  SY_VENDORS
( 
     REC_VERSION EV_INT NOT NULL,
     SY_VENDORS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     VENDOR_NAME EV_STR80 NOT NULL,
     SY_VENDOR_CATEGORIES_NBR EV_INT NOT NULL,
     VENDOR_TYPE EV_CHAR1,
        CONSTRAINT PK_SY_VENDORS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SY_VENDORS_1 UNIQUE (SY_VENDORS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SY_VENDORS_2 UNIQUE (SY_VENDORS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_SY_VENDORS_1 ON SY_VENDORS (VENDOR_NAME,EFFECTIVE_UNTIL)
^

CREATE INDEX FK_SY_VENDORS_1 ON SY_VENDORS (SY_VENDOR_CATEGORIES_NBR,EFFECTIVE_UNTIL)
^


GRANT ALL ON SY_VENDORS TO EUSER
^

CREATE TABLE  SY_VENDOR_CATEGORIES
( 
     REC_VERSION EV_INT NOT NULL,
     SY_VENDOR_CATEGORIES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     DESCRIPTION EV_STR40 NOT NULL,
        CONSTRAINT PK_SY_VENDOR_CATEGORIES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SY_VENDOR_CATEGORIES_1 UNIQUE (SY_VENDOR_CATEGORIES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SY_VENDOR_CATEGORIES_2 UNIQUE (SY_VENDOR_CATEGORIES_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_SY_VENDOR_CATEGORIES_1 ON SY_VENDOR_CATEGORIES (DESCRIPTION,EFFECTIVE_UNTIL)
^


GRANT ALL ON SY_VENDOR_CATEGORIES TO EUSER
^




CREATE OR ALTER PROCEDURE CHECK_INTEGRITY
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check SY_COUNTY */
  child_table = 'SY_COUNTY';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_DELIVERY_METHOD */
  child_table = 'SY_DELIVERY_METHOD';

  parent_table = 'SY_DELIVERY_SERVICE';
  child_field = 'SY_DELIVERY_SERVICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_FED_REPORTING_AGENCY */
  child_table = 'SY_FED_REPORTING_AGENCY';

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_GLOBAL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_FED_TAX_PAYMENT_AGENCY */
  child_table = 'SY_FED_TAX_PAYMENT_AGENCY';

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_GLOBAL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_GLOBAL_AGENCY */
  child_table = 'SY_GLOBAL_AGENCY';

  parent_table = 'SY_GL_AGENCY_FIELD_OFFICE';
  child_field = 'TAX_PAYMENT_FIELD_OFFICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'CUSTOM_DEBIT_SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_GL_AGENCY_FIELD_OFFICE */
  child_table = 'SY_GL_AGENCY_FIELD_OFFICE';

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_GLOBAL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_GL_AGENCY_HOLIDAYS */
  child_table = 'SY_GL_AGENCY_HOLIDAYS';

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_GLOBAL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_GL_AGENCY_REPORT */
  child_table = 'SY_GL_AGENCY_REPORT';

  parent_table = 'SY_GL_AGENCY_FIELD_OFFICE';
  child_field = 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS_GROUP';
  child_field = 'SY_REPORTS_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_LOCALS */
  child_table = 'SY_LOCALS';

  parent_table = 'SY_COUNTY';
  child_field = 'SY_COUNTY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'SY_TAX_PMT_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_LOCAL_REPORTING_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_LOCAL_TAX_PMT_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_LOCAL_DEPOSIT_FREQ */
  child_table = 'SY_LOCAL_DEPOSIT_FREQ';

  parent_table = 'SY_LOCALS';
  child_field = 'SY_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_LOCAL_DEPOSIT_FREQ';
  child_field = 'FIRST_THRESHOLD_DEP_FREQ_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_LOCAL_DEPOSIT_FREQ';
  child_field = 'SECOND_THRESHOLD_DEP_FREQ_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'TAX_COUPON_SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'QE_TAX_COUPON_SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GL_AGENCY_FIELD_OFFICE';
  child_field = 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GL_AGENCY_FIELD_OFFICE';
  child_field = 'QE_SY_GL_AGENCY_OFFICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS_GROUP';
  child_field = 'SY_REPORTS_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_LOCAL_EXEMPTIONS */
  child_table = 'SY_LOCAL_EXEMPTIONS';

  parent_table = 'SY_LOCALS';
  child_field = 'SY_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_LOCAL_MARITAL_STATUS */
  child_table = 'SY_LOCAL_MARITAL_STATUS';

  parent_table = 'SY_LOCALS';
  child_field = 'SY_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_STATE_MARITAL_STATUS';
  child_field = 'SY_STATE_MARITAL_STATUS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_LOCAL_TAX_CHART */
  child_table = 'SY_LOCAL_TAX_CHART';

  parent_table = 'SY_LOCALS';
  child_field = 'SY_LOCALS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_LOCAL_MARITAL_STATUS';
  child_field = 'SY_LOCAL_MARITAL_STATUS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_RECIPROCATED_STATES */
  child_table = 'SY_RECIPROCATED_STATES';

  parent_table = 'SY_STATES';
  child_field = 'MAIN_STATE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_STATES';
  child_field = 'PARTICIPANT_STATE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_REPORTS */
  child_table = 'SY_REPORTS';

  parent_table = 'SY_REPORT_WRITER_REPORTS';
  child_field = 'SY_REPORT_WRITER_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS_GROUP';
  child_field = 'SY_REPORTS_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_REPORTS_GROUP */
  child_table = 'SY_REPORTS_GROUP';

  parent_table = 'SY_REPORTS';
  child_field = 'SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GL_AGENCY_REPORT';
  child_field = 'SY_GL_AGENCY_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_REPORT_GROUP_MEMBERS */
  child_table = 'SY_REPORT_GROUP_MEMBERS';

  parent_table = 'SY_REPORT_WRITER_REPORTS';
  child_field = 'SY_REPORT_WRITER_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORT_GROUPS';
  child_field = 'SY_REPORT_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_STATES */
  child_table = 'SY_STATES';

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_STATE_TAX_PMT_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'SY_TAX_PMT_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_STATE_REPORTING_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_STATE_DEPOSIT_FREQ */
  child_table = 'SY_STATE_DEPOSIT_FREQ';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GL_AGENCY_FIELD_OFFICE';
  child_field = 'SY_GL_AGENCY_FIELD_OFFICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'TAX_COUPON_SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'QE_TAX_COUPON_SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_STATE_DEPOSIT_FREQ';
  child_field = 'THRESHOLD_DEP_FREQUENCY_NUMBER';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_STATE_DEPOSIT_FREQ';
  child_field = 'SECOND_THRESHOLD_DEP_FREQ_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS_GROUP';
  child_field = 'SY_REPORTS_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GL_AGENCY_FIELD_OFFICE';
  child_field = 'QE_SY_GL_AGENCY_OFFICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_STATE_EXEMPTIONS */
  child_table = 'SY_STATE_EXEMPTIONS';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_STATE_MARITAL_STATUS */
  child_table = 'SY_STATE_MARITAL_STATUS';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_STATE_TAX_CHART */
  child_table = 'SY_STATE_TAX_CHART';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_STATE_MARITAL_STATUS';
  child_field = 'SY_STATE_MARITAL_STATUS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_SUI */
  child_table = 'SY_SUI';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'SY_TAX_PMT_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_SUI_REPORTING_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_SUI_TAX_PMT_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS';
  child_field = 'TAX_COUPON_SY_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS_GROUP';
  child_field = 'SY_REPORTS_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_HR_REFUSAL_REASON */
  child_table = 'SY_HR_REFUSAL_REASON';

  parent_table = 'SY_STATES';
  child_field = 'SY_STATES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_AGENCY_DEPOSIT_FREQ */
  child_table = 'SY_AGENCY_DEPOSIT_FREQ';

  parent_table = 'SY_GLOBAL_AGENCY';
  child_field = 'SY_GLOBAL_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SY_REPORTS_GROUP';
  child_field = 'SY_REPORTS_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_REPORTS_VERSIONS */
  child_table = 'SY_REPORTS_VERSIONS';

  parent_table = 'SY_REPORT_WRITER_REPORTS';
  child_field = 'SY_REPORT_WRITER_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_DASHBOARDS */
  child_table = 'SY_DASHBOARDS';

  parent_table = 'SY_ANALYTICS_TIER';
  child_field = 'SY_ANALYTICS_TIER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SY_VENDORS */
  child_table = 'SY_VENDORS';

  parent_table = 'SY_VENDOR_CATEGORIES';
  child_field = 'SY_VENDOR_CATEGORIES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY TO EUSER
^


CREATE OR ALTER PROCEDURE del_sy_dashboards(nbr INTEGER)
AS
BEGIN
  DELETE FROM sy_dashboards WHERE sy_dashboards_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SY_DASHBOARDS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_analytics_tier(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_analytics_tier_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE tier_id VARCHAR(40);
DECLARE VARIABLE p_tier_id VARCHAR(40);
DECLARE VARIABLE dashboard_default_count INTEGER;
DECLARE VARIABLE p_dashboard_default_count INTEGER;
DECLARE VARIABLE dashboard_max_count INTEGER;
DECLARE VARIABLE p_dashboard_max_count INTEGER;
DECLARE VARIABLE users_default_count INTEGER;
DECLARE VARIABLE p_users_default_count INTEGER;
DECLARE VARIABLE users_max_count INTEGER;
DECLARE VARIABLE p_users_max_count INTEGER;
DECLARE VARIABLE lookback_years_default INTEGER;
DECLARE VARIABLE p_lookback_years_default INTEGER;
DECLARE VARIABLE lookback_years_max INTEGER;
DECLARE VARIABLE p_lookback_years_max INTEGER;
DECLARE VARIABLE mobile_app CHAR(1);
DECLARE VARIABLE p_mobile_app CHAR(1);
DECLARE VARIABLE internal_benchmarking CHAR(1);
DECLARE VARIABLE p_internal_benchmarking CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_analytics_tier_nbr , tier_id, dashboard_default_count, dashboard_max_count, users_default_count, users_max_count, lookback_years_default, lookback_years_max, mobile_app, internal_benchmarking
        FROM sy_analytics_tier
        ORDER BY sy_analytics_tier_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_analytics_tier_nbr, :tier_id, :dashboard_default_count, :dashboard_max_count, :users_default_count, :users_max_count, :lookback_years_default, :lookback_years_max, :mobile_app, :internal_benchmarking
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_analytics_tier_nbr) OR (effective_date = '1/1/1900') OR (p_tier_id IS DISTINCT FROM tier_id) OR (p_dashboard_default_count IS DISTINCT FROM dashboard_default_count) OR (p_dashboard_max_count IS DISTINCT FROM dashboard_max_count) OR (p_users_default_count IS DISTINCT FROM users_default_count) OR (p_users_max_count IS DISTINCT FROM users_max_count) OR (p_lookback_years_default IS DISTINCT FROM lookback_years_default) OR (p_lookback_years_max IS DISTINCT FROM lookback_years_max) OR (p_mobile_app IS DISTINCT FROM mobile_app) OR (p_internal_benchmarking IS DISTINCT FROM internal_benchmarking)) THEN
      BEGIN
        curr_nbr = sy_analytics_tier_nbr;
        p_tier_id = tier_id;
        p_dashboard_default_count = dashboard_default_count;
        p_dashboard_max_count = dashboard_max_count;
        p_users_default_count = users_default_count;
        p_users_max_count = users_max_count;
        p_lookback_years_default = lookback_years_default;
        p_lookback_years_max = lookback_years_max;
        p_mobile_app = mobile_app;
        p_internal_benchmarking = internal_benchmarking;
      END
      ELSE
        DELETE FROM sy_analytics_tier WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, tier_id, dashboard_default_count, dashboard_max_count, users_default_count, users_max_count, lookback_years_default, lookback_years_max, mobile_app, internal_benchmarking
        FROM sy_analytics_tier
        WHERE sy_analytics_tier_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :tier_id, :dashboard_default_count, :dashboard_max_count, :users_default_count, :users_max_count, :lookback_years_default, :lookback_years_max, :mobile_app, :internal_benchmarking
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_tier_id IS DISTINCT FROM tier_id) OR (p_dashboard_default_count IS DISTINCT FROM dashboard_default_count) OR (p_dashboard_max_count IS DISTINCT FROM dashboard_max_count) OR (p_users_default_count IS DISTINCT FROM users_default_count) OR (p_users_max_count IS DISTINCT FROM users_max_count) OR (p_lookback_years_default IS DISTINCT FROM lookback_years_default) OR (p_lookback_years_max IS DISTINCT FROM lookback_years_max) OR (p_mobile_app IS DISTINCT FROM mobile_app) OR (p_internal_benchmarking IS DISTINCT FROM internal_benchmarking)) THEN
      BEGIN
        p_tier_id = tier_id;
        p_dashboard_default_count = dashboard_default_count;
        p_dashboard_max_count = dashboard_max_count;
        p_users_default_count = users_default_count;
        p_users_max_count = users_max_count;
        p_lookback_years_default = lookback_years_default;
        p_lookback_years_max = lookback_years_max;
        p_mobile_app = mobile_app;
        p_internal_benchmarking = internal_benchmarking;
      END
      ELSE
        DELETE FROM sy_analytics_tier WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE pack_sy_analytics_tier TO EUSER
^

CREATE OR ALTER PROCEDURE del_sy_analytics_tier(nbr INTEGER)
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  FOR SELECT rec_version FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = :nbr ORDER BY effective_date DESC FOR UPDATE WITH LOCK INTO rec_version
  DO
  BEGIN
    DELETE FROM sy_analytics_tier WHERE rec_version = :rec_version;
  END
END
^

GRANT EXECUTE ON PROCEDURE DEL_SY_ANALYTICS_TIER TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_vendors(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_vendors_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE vendor_name VARCHAR(80);
DECLARE VARIABLE p_vendor_name VARCHAR(80);
DECLARE VARIABLE vendor_type CHAR(1);
DECLARE VARIABLE p_vendor_type CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_vendors_nbr , vendor_name, vendor_type
        FROM sy_vendors
        ORDER BY sy_vendors_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_vendors_nbr, :vendor_name, :vendor_type
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_vendors_nbr) OR (effective_date = '1/1/1900') OR (p_vendor_name IS DISTINCT FROM vendor_name) OR (p_vendor_type IS DISTINCT FROM vendor_type)) THEN
      BEGIN
        curr_nbr = sy_vendors_nbr;
        p_vendor_name = vendor_name;
        p_vendor_type = vendor_type;
      END
      ELSE
        DELETE FROM sy_vendors WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, vendor_name, vendor_type
        FROM sy_vendors
        WHERE sy_vendors_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :vendor_name, :vendor_type
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_vendor_name IS DISTINCT FROM vendor_name) OR (p_vendor_type IS DISTINCT FROM vendor_type)) THEN
      BEGIN
        p_vendor_name = vendor_name;
        p_vendor_type = vendor_type;
      END
      ELSE
        DELETE FROM sy_vendors WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE pack_sy_vendors TO EUSER
^

CREATE OR ALTER PROCEDURE del_sy_vendors(nbr INTEGER)
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  FOR SELECT rec_version FROM sy_vendors WHERE sy_vendors_nbr = :nbr ORDER BY effective_date DESC FOR UPDATE WITH LOCK INTO rec_version
  DO
  BEGIN
    DELETE FROM sy_vendors WHERE rec_version = :rec_version;
  END
END
^

GRANT EXECUTE ON PROCEDURE DEL_SY_VENDORS TO EUSER
^

CREATE OR ALTER PROCEDURE del_sy_vendor_categories(nbr INTEGER)
AS
BEGIN
  DELETE FROM sy_vendor_categories WHERE sy_vendor_categories_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SY_VENDOR_CATEGORIES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_all
AS
BEGIN
  EXECUTE PROCEDURE pack_sy_fed_exemptions (NULL);
  EXECUTE PROCEDURE pack_sy_fed_tax_table (NULL);
  EXECUTE PROCEDURE pack_sy_fed_tax_table_brackets (NULL);
  EXECUTE PROCEDURE pack_sy_global_agency (NULL);
  EXECUTE PROCEDURE pack_sy_gl_agency_field_office (NULL);
  EXECUTE PROCEDURE pack_sy_gl_agency_holidays (NULL);
  EXECUTE PROCEDURE pack_sy_gl_agency_report (NULL);
  EXECUTE PROCEDURE pack_sy_locals (NULL);
  EXECUTE PROCEDURE pack_sy_local_deposit_freq (NULL);
  EXECUTE PROCEDURE pack_sy_local_exemptions (NULL);
  EXECUTE PROCEDURE pack_sy_local_marital_status (NULL);
  EXECUTE PROCEDURE pack_sy_local_tax_chart (NULL);
  EXECUTE PROCEDURE pack_sy_reciprocated_states (NULL);
  EXECUTE PROCEDURE pack_sy_reports (NULL);
  EXECUTE PROCEDURE pack_sy_reports_group (NULL);
  EXECUTE PROCEDURE pack_sy_states (NULL);
  EXECUTE PROCEDURE pack_sy_state_deposit_freq (NULL);
  EXECUTE PROCEDURE pack_sy_state_exemptions (NULL);
  EXECUTE PROCEDURE pack_sy_state_marital_status (NULL);
  EXECUTE PROCEDURE pack_sy_state_tax_chart (NULL);
  EXECUTE PROCEDURE pack_sy_sui (NULL);
  EXECUTE PROCEDURE pack_sy_agency_deposit_freq (NULL);
  EXECUTE PROCEDURE pack_sy_analytics_tier (NULL);
  EXECUTE PROCEDURE pack_sy_vendors (NULL);
END
^

GRANT EXECUTE ON PROCEDURE PACK_ALL TO EUSER
^


ALTER TABLE SY_ANALYTICS_TIER
ADD CONSTRAINT C_SY_ANALYTICS_TIER_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SY_DASHBOARDS
ADD CONSTRAINT C_SY_DASHBOARDS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SY_VENDORS
ADD CONSTRAINT C_SY_VENDORS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SY_VENDOR_CATEGORIES
ADD CONSTRAINT C_SY_VENDOR_CATEGORIES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_SY_ANALYTICS_TIER_1 FOR SY_ANALYTICS_TIER After Delete POSITION 1
AS
DECLARE VARIABLE effective_until DATE;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it's the first version record */
  IF (OLD.effective_date = '1/1/1900') THEN
  BEGIN
    IF (EXISTS (SELECT 1 FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr AND effective_date > OLD.effective_date)) THEN
      EXECUTE PROCEDURE raise_table_error('sy_analytics_tier', OLD.rec_version, '', 'S5', 'First version record cannot be deleted.');
  END

  ELSE
  BEGIN
    /* Correct EFFECTIVE_UNTIL of previous version */
    effective_until = OLD.effective_until;
    SELECT effective_date FROM sy_analytics_tier
    WHERE sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr AND
          effective_date > OLD.effective_date
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :effective_until;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

    UPDATE sy_analytics_tier SET effective_until = :effective_until
    WHERE sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr AND effective_until = OLD.effective_date;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AD_SY_ANALYTICS_TIER_9 FOR SY_ANALYTICS_TIER After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE tier_id VARCHAR(40);
DECLARE VARIABLE dashboard_default_count INTEGER;
DECLARE VARIABLE dashboard_max_count INTEGER;
DECLARE VARIABLE users_default_count INTEGER;
DECLARE VARIABLE users_max_count INTEGER;
DECLARE VARIABLE lookback_years_default INTEGER;
DECLARE VARIABLE lookback_years_max INTEGER;
DECLARE VARIABLE mobile_app CHAR(1);
DECLARE VARIABLE internal_benchmarking CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(45, OLD.rec_version, OLD.sy_analytics_tier_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', tier_id, dashboard_default_count, dashboard_max_count, users_default_count, users_max_count, lookback_years_default, lookback_years_max, mobile_app, internal_benchmarking  FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :tier_id, :dashboard_default_count, :dashboard_max_count, :users_default_count, :users_max_count, :lookback_years_default, :lookback_years_max, :mobile_app, :internal_benchmarking;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 595, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 596, OLD.effective_until);

  /* TIER_ID */
  IF (last_record = 'Y' OR tier_id IS DISTINCT FROM OLD.tier_id) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 597, OLD.tier_id);

  /* DASHBOARD_DEFAULT_COUNT */
  IF ((last_record = 'Y' AND OLD.dashboard_default_count IS NOT NULL) OR (last_record = 'N' AND dashboard_default_count IS DISTINCT FROM OLD.dashboard_default_count)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 598, OLD.dashboard_default_count);

  /* DASHBOARD_MAX_COUNT */
  IF ((last_record = 'Y' AND OLD.dashboard_max_count IS NOT NULL) OR (last_record = 'N' AND dashboard_max_count IS DISTINCT FROM OLD.dashboard_max_count)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 599, OLD.dashboard_max_count);

  /* USERS_DEFAULT_COUNT */
  IF ((last_record = 'Y' AND OLD.users_default_count IS NOT NULL) OR (last_record = 'N' AND users_default_count IS DISTINCT FROM OLD.users_default_count)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 600, OLD.users_default_count);

  /* USERS_MAX_COUNT */
  IF ((last_record = 'Y' AND OLD.users_max_count IS NOT NULL) OR (last_record = 'N' AND users_max_count IS DISTINCT FROM OLD.users_max_count)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 601, OLD.users_max_count);

  /* LOOKBACK_YEARS_DEFAULT */
  IF ((last_record = 'Y' AND OLD.lookback_years_default IS NOT NULL) OR (last_record = 'N' AND lookback_years_default IS DISTINCT FROM OLD.lookback_years_default)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 602, OLD.lookback_years_default);

  /* LOOKBACK_YEARS_MAX */
  IF ((last_record = 'Y' AND OLD.lookback_years_max IS NOT NULL) OR (last_record = 'N' AND lookback_years_max IS DISTINCT FROM OLD.lookback_years_max)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 603, OLD.lookback_years_max);

  /* MOBILE_APP */
  IF ((last_record = 'Y' AND OLD.mobile_app IS NOT NULL) OR (last_record = 'N' AND mobile_app IS DISTINCT FROM OLD.mobile_app)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 604, OLD.mobile_app);

  /* INTERNAL_BENCHMARKING */
  IF ((last_record = 'Y' AND OLD.internal_benchmarking IS NOT NULL) OR (last_record = 'N' AND internal_benchmarking IS DISTINCT FROM OLD.internal_benchmarking)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 605, OLD.internal_benchmarking);

END

^

CREATE TRIGGER T_AI_SY_ANALYTICS_TIER_9 FOR SY_ANALYTICS_TIER After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(45, NEW.rec_version, NEW.sy_analytics_tier_nbr, 'I');
END

^

CREATE TRIGGER T_AUD_SY_ANALYTICS_TIER_2 FOR SY_ANALYTICS_TIER After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SY_DASHBOARDS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sy_dashboards
    WHERE (sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sy_analytics_tier', OLD.sy_analytics_tier_nbr, 'sy_dashboards', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sy_dashboards
    WHERE (sy_analytics_tier_nbr = OLD.sy_analytics_tier_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sy_analytics_tier', OLD.sy_analytics_tier_nbr, 'sy_dashboards', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_SY_ANALYTICS_TIER_1 FOR SY_ANALYTICS_TIER After Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((OLD.effective_date IS DISTINCT FROM NEW.effective_date) AND
      (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL of old previous version */
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sy_analytics_tier SET effective_until = OLD.effective_until
    WHERE sy_analytics_tier_nbr = NEW.sy_analytics_tier_nbr AND effective_until = OLD.effective_date;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AU_SY_ANALYTICS_TIER_9 FOR SY_ANALYTICS_TIER After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(45, NEW.rec_version, NEW.sy_analytics_tier_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 595, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 596, OLD.effective_until);
    changes = changes + 1;
  END

  /* TIER_ID */
  IF (OLD.tier_id IS DISTINCT FROM NEW.tier_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 597, OLD.tier_id);
    changes = changes + 1;
  END

  /* DASHBOARD_DEFAULT_COUNT */
  IF (OLD.dashboard_default_count IS DISTINCT FROM NEW.dashboard_default_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 598, OLD.dashboard_default_count);
    changes = changes + 1;
  END

  /* DASHBOARD_MAX_COUNT */
  IF (OLD.dashboard_max_count IS DISTINCT FROM NEW.dashboard_max_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 599, OLD.dashboard_max_count);
    changes = changes + 1;
  END

  /* USERS_DEFAULT_COUNT */
  IF (OLD.users_default_count IS DISTINCT FROM NEW.users_default_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 600, OLD.users_default_count);
    changes = changes + 1;
  END

  /* USERS_MAX_COUNT */
  IF (OLD.users_max_count IS DISTINCT FROM NEW.users_max_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 601, OLD.users_max_count);
    changes = changes + 1;
  END

  /* LOOKBACK_YEARS_DEFAULT */
  IF (OLD.lookback_years_default IS DISTINCT FROM NEW.lookback_years_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 602, OLD.lookback_years_default);
    changes = changes + 1;
  END

  /* LOOKBACK_YEARS_MAX */
  IF (OLD.lookback_years_max IS DISTINCT FROM NEW.lookback_years_max) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 603, OLD.lookback_years_max);
    changes = changes + 1;
  END

  /* MOBILE_APP */
  IF (OLD.mobile_app IS DISTINCT FROM NEW.mobile_app) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 604, OLD.mobile_app);
    changes = changes + 1;
  END

  /* INTERNAL_BENCHMARKING */
  IF (OLD.internal_benchmarking IS DISTINCT FROM NEW.internal_benchmarking) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 605, OLD.internal_benchmarking);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SY_ANALYTICS_TIER_1 FOR SY_ANALYTICS_TIER Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.MOBILE_APP is null OR CHAR_LENGTH(TRIM(new.MOBILE_APP))=0) THEN 
      new.MOBILE_APP = 'N'; 

    IF (new.INTERNAL_BENCHMARKING is null OR CHAR_LENGTH(TRIM(new.INTERNAL_BENCHMARKING))=0) THEN 
      new.INTERNAL_BENCHMARKING = 'N'; 

  END 
END

^

CREATE TRIGGER T_BIU_SY_ANALYTICS_TIER_3 FOR SY_ANALYTICS_TIER Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SY_ANALYTICS_TIER_1 */
  IF (INSERTING OR (OLD.tier_id IS DISTINCT FROM NEW.tier_id) OR (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    IF (EXISTS(SELECT 1 FROM sy_analytics_tier WHERE tier_id = NEW.tier_id AND effective_until = NEW.effective_until AND sy_analytics_tier_nbr <> NEW.sy_analytics_tier_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sy_analytics_tier', 'tier_id, effective_until',
      CAST(NEW.tier_id || ', ' || NEW.effective_until as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SY_ANALYTICS_TIER_1 FOR SY_ANALYTICS_TIER Before Insert POSITION 1
AS
DECLARE VARIABLE new_nbr INTEGER;
DECLARE VARIABLE new_effective_date DATE;
DECLARE VARIABLE new_effective_until DATE;
DECLARE VARIABLE prev_rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sy_analytics_tier_ver;

  new_nbr = NEW.sy_analytics_tier_nbr;
  new_effective_date = NEW.effective_date;
  new_effective_until = NEW.effective_until;

  /* Calc EFFECTIVE_DATE of this version */
  IF (EXISTS (SELECT 1 FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = :new_nbr ROWS 1)) THEN
  BEGIN
    IF (new_effective_date IS NULL) THEN
      new_effective_date = CURRENT_DATE;
  END
  ELSE
    new_effective_date = '1/1/1900';

  /* Correct EFFECTIVE_UNTIL of previous version */
  prev_rec_version = NULL;
  SELECT rec_version FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = :new_nbr AND effective_date < :new_effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :prev_rec_version;

  IF (prev_rec_version IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sy_analytics_tier SET effective_until = :new_effective_date WHERE rec_version = :prev_rec_version;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END

  /* Calc EFFECTIVE_UNTIL of this version */
  SELECT effective_date FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = :new_nbr AND effective_date > :new_effective_date
  ORDER BY effective_date
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :new_effective_until;

  IF (new_effective_until IS NULL) THEN
    new_effective_until = '12/31/9999';

  /* Final assignment */
  NEW.effective_date = new_effective_date;
  NEW.effective_until = new_effective_until;
END

^

CREATE TRIGGER T_BU_SY_ANALYTICS_TIER_1 FOR SY_ANALYTICS_TIER Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it is changing system fields */
  IF ((NEW.effective_date IS DISTINCT FROM OLD.effective_date) AND (OLD.effective_date = '1/1/1900')) THEN
    NEW.effective_date = OLD.effective_date;

  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sy_analytics_tier_nbr IS DISTINCT FROM OLD.sy_analytics_tier_nbr) OR
     (NEW.effective_until IS DISTINCT FROM OLD.effective_until)) THEN
  BEGIN
    IF (NEW.effective_until IS DISTINCT FROM OLD.effective_until) THEN
    BEGIN
      IF (EXISTS (SELECT 1 FROM sy_analytics_tier WHERE sy_analytics_tier_nbr = NEW.sy_analytics_tier_nbr AND effective_date = OLD.effective_until)) THEN
        NEW.effective_until = OLD.effective_until;
      ELSE
        IF (NEW.effective_until IS NULL) THEN
          NEW.effective_until = '12/31/9999';
    END
    ELSE
      EXECUTE PROCEDURE raise_table_error('sy_analytics_tier', NEW.rec_version, '', 'S1', 'System fields cannot be modified');
  END

  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL */
    NEW.effective_until = '12/31/9999';
    SELECT effective_date FROM sy_analytics_tier
    WHERE sy_analytics_tier_nbr = NEW.sy_analytics_tier_nbr AND
          effective_date > NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO NEW.effective_until;

    /* Correct EFFECTIVE_UNTIL of new previous version */
    rec_version = NULL;
    SELECT rec_version FROM sy_analytics_tier
    WHERE sy_analytics_tier_nbr = NEW.sy_analytics_tier_nbr AND
          effective_date < NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date DESC
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :rec_version;

    IF (rec_version IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
      UPDATE sy_analytics_tier SET effective_until = NEW.effective_date WHERE rec_version = :rec_version;
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_SY_DASHBOARDS_9 FOR SY_DASHBOARDS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, OLD.rec_version, OLD.sy_dashboards_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 586, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 587, OLD.effective_until);

  /* DASHBOARD_TYPE */
  IF (OLD.dashboard_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 588, OLD.dashboard_type);

  /* SY_ANALYTICS_TIER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 589, OLD.sy_analytics_tier_nbr);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 590, :blob_nbr);
  END

  /* DASHBOARD_ID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 591, OLD.dashboard_id);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 592, OLD.description);

END

^

CREATE TRIGGER T_AI_SY_DASHBOARDS_9 FOR SY_DASHBOARDS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(44, NEW.rec_version, NEW.sy_dashboards_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SY_DASHBOARDS_9 FOR SY_DASHBOARDS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, NEW.rec_version, NEW.sy_dashboards_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 586, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 587, OLD.effective_until);
    changes = changes + 1;
  END

  /* DASHBOARD_TYPE */
  IF (OLD.dashboard_type IS DISTINCT FROM NEW.dashboard_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 588, OLD.dashboard_type);
    changes = changes + 1;
  END

  /* SY_ANALYTICS_TIER_NBR */
  IF (OLD.sy_analytics_tier_nbr IS DISTINCT FROM NEW.sy_analytics_tier_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 589, OLD.sy_analytics_tier_nbr);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 590, :blob_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_ID */
  IF (OLD.dashboard_id IS DISTINCT FROM NEW.dashboard_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 591, OLD.dashboard_id);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 592, OLD.description);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SY_DASHBOARDS_1 FOR SY_DASHBOARDS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.DASHBOARD_TYPE is null OR CHAR_LENGTH(TRIM(new.DASHBOARD_TYPE))=0) THEN 
      new.DASHBOARD_TYPE = 'N'; 

  END 
END

^

CREATE TRIGGER T_BIU_SY_DASHBOARDS_2 FOR SY_DASHBOARDS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SY_ANALYTICS_TIER */
  IF ((NEW.sy_analytics_tier_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sy_analytics_tier_nbr IS DISTINCT FROM NEW.sy_analytics_tier_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sy_analytics_tier
    WHERE sy_analytics_tier_nbr = NEW.sy_analytics_tier_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sy_dashboards', NEW.sy_dashboards_nbr, 'sy_analytics_tier_nbr', NEW.sy_analytics_tier_nbr, 'sy_analytics_tier', NEW.effective_date);

    SELECT rec_version FROM sy_analytics_tier
    WHERE sy_analytics_tier_nbr = NEW.sy_analytics_tier_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sy_dashboards', NEW.sy_dashboards_nbr, 'sy_analytics_tier_nbr', NEW.sy_analytics_tier_nbr, 'sy_analytics_tier', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_SY_DASHBOARDS_1 FOR SY_DASHBOARDS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sy_dashboards_ver;

  IF (EXISTS (SELECT 1 FROM sy_dashboards WHERE sy_dashboards_nbr = NEW.sy_dashboards_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sy_dashboards', NEW.sy_dashboards_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BUD_SY_DASHBOARDS_9 FOR SY_DASHBOARDS Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END
END

^

CREATE TRIGGER T_BU_SY_DASHBOARDS_1 FOR SY_DASHBOARDS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sy_dashboards_nbr IS DISTINCT FROM OLD.sy_dashboards_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sy_dashboards', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SY_VENDORS_1 FOR SY_VENDORS After Delete POSITION 1
AS
DECLARE VARIABLE effective_until DATE;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it's the first version record */
  IF (OLD.effective_date = '1/1/1900') THEN
  BEGIN
    IF (EXISTS (SELECT 1 FROM sy_vendors WHERE sy_vendors_nbr = OLD.sy_vendors_nbr AND effective_date > OLD.effective_date)) THEN
      EXECUTE PROCEDURE raise_table_error('sy_vendors', OLD.rec_version, '', 'S5', 'First version record cannot be deleted.');
  END

  ELSE
  BEGIN
    /* Correct EFFECTIVE_UNTIL of previous version */
    effective_until = OLD.effective_until;
    SELECT effective_date FROM sy_vendors
    WHERE sy_vendors_nbr = OLD.sy_vendors_nbr AND
          effective_date > OLD.effective_date
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :effective_until;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

    UPDATE sy_vendors SET effective_until = :effective_until
    WHERE sy_vendors_nbr = OLD.sy_vendors_nbr AND effective_until = OLD.effective_date;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AD_SY_VENDORS_9 FOR SY_VENDORS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE vendor_name VARCHAR(80);
DECLARE VARIABLE vendor_type CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(47, OLD.rec_version, OLD.sy_vendors_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', vendor_name, vendor_type  FROM sy_vendors WHERE sy_vendors_nbr = OLD.sy_vendors_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :vendor_name, :vendor_type;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 613, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 614, OLD.effective_until);

  /* VENDOR_NAME */
  IF (last_record = 'Y' OR vendor_name IS DISTINCT FROM OLD.vendor_name) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 615, OLD.vendor_name);

  /* VENDOR_TYPE */
  IF ((last_record = 'Y' AND OLD.vendor_type IS NOT NULL) OR (last_record = 'N' AND vendor_type IS DISTINCT FROM OLD.vendor_type)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 617, OLD.vendor_type);


  IF (last_record = 'Y') THEN
  BEGIN
    /* SY_VENDOR_CATEGORIES_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 616, OLD.sy_vendor_categories_nbr);

  END
END

^

CREATE TRIGGER T_AIU_SY_VENDORS_3 FOR SY_VENDORS After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.sy_vendor_categories_nbr IS DISTINCT FROM NEW.sy_vendor_categories_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE sy_vendors SET 
      sy_vendor_categories_nbr = NEW.sy_vendor_categories_nbr
      WHERE sy_vendors_nbr = NEW.sy_vendors_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AI_SY_VENDORS_9 FOR SY_VENDORS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(47, NEW.rec_version, NEW.sy_vendors_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SY_VENDORS_1 FOR SY_VENDORS After Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((OLD.effective_date IS DISTINCT FROM NEW.effective_date) AND
      (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL of old previous version */
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sy_vendors SET effective_until = OLD.effective_until
    WHERE sy_vendors_nbr = NEW.sy_vendors_nbr AND effective_until = OLD.effective_date;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AU_SY_VENDORS_9 FOR SY_VENDORS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(47, NEW.rec_version, NEW.sy_vendors_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 613, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 614, OLD.effective_until);
    changes = changes + 1;
  END

  /* VENDOR_NAME */
  IF (OLD.vendor_name IS DISTINCT FROM NEW.vendor_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 615, OLD.vendor_name);
    changes = changes + 1;
  END

  /* SY_VENDOR_CATEGORIES_NBR */
  IF (OLD.sy_vendor_categories_nbr IS DISTINCT FROM NEW.sy_vendor_categories_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 616, OLD.sy_vendor_categories_nbr);
    changes = changes + 1;
  END

  /* VENDOR_TYPE */
  IF (OLD.vendor_type IS DISTINCT FROM NEW.vendor_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 617, OLD.vendor_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SY_VENDORS_1 FOR SY_VENDORS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.VENDOR_TYPE is null OR CHAR_LENGTH(TRIM(new.VENDOR_TYPE))=0) THEN 
      new.VENDOR_TYPE = 'S'; 

  END 
END

^

CREATE TRIGGER T_BIU_SY_VENDORS_2 FOR SY_VENDORS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SY_VENDOR_CATEGORIES */
  IF ((NEW.sy_vendor_categories_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sy_vendor_categories_nbr IS DISTINCT FROM NEW.sy_vendor_categories_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sy_vendor_categories
    WHERE sy_vendor_categories_nbr = NEW.sy_vendor_categories_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sy_vendors', NEW.sy_vendors_nbr, 'sy_vendor_categories_nbr', NEW.sy_vendor_categories_nbr, 'sy_vendor_categories', NEW.effective_date);

    SELECT rec_version FROM sy_vendor_categories
    WHERE sy_vendor_categories_nbr = NEW.sy_vendor_categories_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sy_vendors', NEW.sy_vendors_nbr, 'sy_vendor_categories_nbr', NEW.sy_vendor_categories_nbr, 'sy_vendor_categories', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BIU_SY_VENDORS_3 FOR SY_VENDORS Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SY_VENDORS_1 */
  IF (INSERTING OR (OLD.vendor_name IS DISTINCT FROM NEW.vendor_name) OR (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    IF (EXISTS(SELECT 1 FROM sy_vendors WHERE vendor_name = NEW.vendor_name AND effective_until = NEW.effective_until AND sy_vendors_nbr <> NEW.sy_vendors_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sy_vendors', 'vendor_name, effective_until',
      CAST(NEW.vendor_name || ', ' || NEW.effective_until as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SY_VENDORS_1 FOR SY_VENDORS Before Insert POSITION 1
AS
DECLARE VARIABLE new_nbr INTEGER;
DECLARE VARIABLE new_effective_date DATE;
DECLARE VARIABLE new_effective_until DATE;
DECLARE VARIABLE prev_rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sy_vendors_ver;

  new_nbr = NEW.sy_vendors_nbr;
  new_effective_date = NEW.effective_date;
  new_effective_until = NEW.effective_until;

  /* Calc EFFECTIVE_DATE of this version */
  IF (EXISTS (SELECT 1 FROM sy_vendors WHERE sy_vendors_nbr = :new_nbr ROWS 1)) THEN
  BEGIN
    IF (new_effective_date IS NULL) THEN
      new_effective_date = CURRENT_DATE;
  END
  ELSE
    new_effective_date = '1/1/1900';

  /* Correct EFFECTIVE_UNTIL of previous version */
  prev_rec_version = NULL;
  SELECT rec_version FROM sy_vendors WHERE sy_vendors_nbr = :new_nbr AND effective_date < :new_effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :prev_rec_version;

  IF (prev_rec_version IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sy_vendors SET effective_until = :new_effective_date WHERE rec_version = :prev_rec_version;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END

  /* Calc EFFECTIVE_UNTIL of this version */
  SELECT effective_date FROM sy_vendors WHERE sy_vendors_nbr = :new_nbr AND effective_date > :new_effective_date
  ORDER BY effective_date
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :new_effective_until;

  IF (new_effective_until IS NULL) THEN
    new_effective_until = '12/31/9999';

  /* Final assignment */
  NEW.effective_date = new_effective_date;
  NEW.effective_until = new_effective_until;
END

^

CREATE TRIGGER T_BU_SY_VENDORS_1 FOR SY_VENDORS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it is changing system fields */
  IF ((NEW.effective_date IS DISTINCT FROM OLD.effective_date) AND (OLD.effective_date = '1/1/1900')) THEN
    NEW.effective_date = OLD.effective_date;

  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sy_vendors_nbr IS DISTINCT FROM OLD.sy_vendors_nbr) OR
     (NEW.effective_until IS DISTINCT FROM OLD.effective_until)) THEN
  BEGIN
    IF (NEW.effective_until IS DISTINCT FROM OLD.effective_until) THEN
    BEGIN
      IF (EXISTS (SELECT 1 FROM sy_vendors WHERE sy_vendors_nbr = NEW.sy_vendors_nbr AND effective_date = OLD.effective_until)) THEN
        NEW.effective_until = OLD.effective_until;
      ELSE
        IF (NEW.effective_until IS NULL) THEN
          NEW.effective_until = '12/31/9999';
    END
    ELSE
      EXECUTE PROCEDURE raise_table_error('sy_vendors', NEW.rec_version, '', 'S1', 'System fields cannot be modified');
  END

  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL */
    NEW.effective_until = '12/31/9999';
    SELECT effective_date FROM sy_vendors
    WHERE sy_vendors_nbr = NEW.sy_vendors_nbr AND
          effective_date > NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO NEW.effective_until;

    /* Correct EFFECTIVE_UNTIL of new previous version */
    rec_version = NULL;
    SELECT rec_version FROM sy_vendors
    WHERE sy_vendors_nbr = NEW.sy_vendors_nbr AND
          effective_date < NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date DESC
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :rec_version;

    IF (rec_version IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
      UPDATE sy_vendors SET effective_until = NEW.effective_date WHERE rec_version = :rec_version;
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_SY_VENDOR_CATEGORIES_9 FOR SY_VENDOR_CATEGORIES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(46, OLD.rec_version, OLD.sy_vendor_categories_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 608, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 609, OLD.effective_until);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 610, OLD.description);

END

^

CREATE TRIGGER T_AI_SY_VENDOR_CATEGORIES_9 FOR SY_VENDOR_CATEGORIES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(46, NEW.rec_version, NEW.sy_vendor_categories_nbr, 'I');
END

^

CREATE TRIGGER T_AUD_SY_VENDOR_CATEGORIES_2 FOR SY_VENDOR_CATEGORIES After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sy_vendor_categories WHERE sy_vendor_categories_nbr = OLD.sy_vendor_categories_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sy_vendor_categories WHERE sy_vendor_categories_nbr = OLD.sy_vendor_categories_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SY_VENDORS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sy_vendors
    WHERE (sy_vendor_categories_nbr = OLD.sy_vendor_categories_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sy_vendor_categories', OLD.sy_vendor_categories_nbr, 'sy_vendors', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sy_vendors
    WHERE (sy_vendor_categories_nbr = OLD.sy_vendor_categories_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sy_vendor_categories', OLD.sy_vendor_categories_nbr, 'sy_vendors', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_SY_VENDOR_CATEGORIES_9 FOR SY_VENDOR_CATEGORIES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(46, NEW.rec_version, NEW.sy_vendor_categories_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 608, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 609, OLD.effective_until);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 610, OLD.description);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SY_VENDOR_CATEGORIES_3 FOR SY_VENDOR_CATEGORIES Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SY_VENDOR_CATEGORIES_1 */
  IF (INSERTING OR (OLD.description IS DISTINCT FROM NEW.description) OR (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    IF (EXISTS(SELECT 1 FROM sy_vendor_categories WHERE description = NEW.description AND effective_until = NEW.effective_until AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sy_vendor_categories', 'description, effective_until',
      CAST(NEW.description || ', ' || NEW.effective_until as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SY_VENDOR_CATEGORIES_1 FOR SY_VENDOR_CATEGORIES Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sy_vendor_categories_ver;

  IF (EXISTS (SELECT 1 FROM sy_vendor_categories WHERE sy_vendor_categories_nbr = NEW.sy_vendor_categories_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sy_vendor_categories', NEW.sy_vendor_categories_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SY_VENDOR_CATEGORIES_1 FOR SY_VENDOR_CATEGORIES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sy_vendor_categories_nbr IS DISTINCT FROM OLD.sy_vendor_categories_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sy_vendor_categories', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^






COMMIT
^

/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_table (nbr, name, versioned) VALUES (44, 'SY_DASHBOARDS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (45, 'SY_ANALYTICS_TIER', 'Y');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (47, 'SY_VENDORS', 'Y');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (46, 'SY_VENDOR_CATEGORIES', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 584, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 585, 'SY_DASHBOARDS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 586, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 587, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 588, 'DASHBOARD_TYPE', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 589, 'SY_ANALYTICS_TIER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 590, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 591, 'DASHBOARD_ID', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 592, 'DESCRIPTION', 'V', 80, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 593, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 594, 'SY_ANALYTICS_TIER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 595, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 596, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 597, 'TIER_ID', 'V', 40, NULL, 'Y', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 598, 'DASHBOARD_DEFAULT_COUNT', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 599, 'DASHBOARD_MAX_COUNT', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 600, 'USERS_DEFAULT_COUNT', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 601, 'USERS_MAX_COUNT', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 602, 'LOOKBACK_YEARS_DEFAULT', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 603, 'LOOKBACK_YEARS_MAX', 'I', NULL, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 604, 'MOBILE_APP', 'C', 1, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 605, 'INTERNAL_BENCHMARKING', 'C', 1, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 611, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 612, 'SY_VENDORS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 613, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 614, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 615, 'VENDOR_NAME', 'V', 80, NULL, 'Y', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 616, 'SY_VENDOR_CATEGORIES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (47, 617, 'VENDOR_TYPE', 'C', 1, NULL, 'N', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 606, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 607, 'SY_VENDOR_CATEGORIES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 608, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 609, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 610, 'DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^




EXECUTE BLOCK AS
DECLARE VARIABLE Sy INTEGER;
DECLARE VARIABLE Sy_ver INTEGER;

BEGIN

 Execute procedure DO_AFTER_START_TRANSACTION(0,null);


  Sy = NEXT VALUE FOR G_SY_ANALYTICS_TIER;
  Sy_ver = NEXT VALUE FOR G_SY_ANALYTICS_TIER_VER;


  INSERT INTO SY_ANALYTICS_TIER(REC_VERSION, SY_ANALYTICS_TIER_NBR, TIER_ID,
                 DASHBOARD_DEFAULT_COUNT,  DASHBOARD_MAX_COUNT,
                 USERS_DEFAULT_COUNT, USERS_MAX_COUNT,
                 LOOKBACK_YEARS_DEFAULT, LOOKBACK_YEARS_MAX,
                 MOBILE_APP, INTERNAL_BENCHMARKING )
      VALUES(:Sy_ver, :Sy, 'BASIC', 3,5, 2,5, 1,3, 'N', 'N');


  Execute procedure DO_BEFORE_COMMIT_TRANSACTION(null);
END
^

COMMIT^


/* Update EV_DATABASE */
EXECUTE block
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (VERSION, description) VALUES ('16.0.0.1', 'Evolution System Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
