/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.2';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^





CREATE GENERATOR G_SB_ANALYTICS_ENABLED
^

CREATE GENERATOR G_SB_ANALYTICS_ENABLED_VER
^

CREATE TABLE  SB_ANALYTICS_ENABLED
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ANALYTICS_ENABLED_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CL_NBR EV_INT NOT NULL,
     CO_NBR EV_INT NOT NULL,
     ENABLE_ANALYTICS EV_CHAR1 NOT NULL,
        CONSTRAINT PK_SB_ANALYTICS_ENABLED PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ANALYTICS_ENABLED_1 UNIQUE (SB_ANALYTICS_ENABLED_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ANALYTICS_ENABLED_2 UNIQUE (SB_ANALYTICS_ENABLED_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX LK_SB_ANALYTICS_ENABLED_1 ON SB_ANALYTICS_ENABLED (CL_NBR,CO_NBR,ENABLE_ANALYTICS)
^


GRANT ALL ON SB_ANALYTICS_ENABLED TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_analytics_enabled(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_analytics_enabled WHERE sb_analytics_enabled_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ANALYTICS_ENABLED TO EUSER
^

ALTER TABLE SB_ANALYTICS_ENABLED
ADD CONSTRAINT C_SB_ANALYTICS_ENABLED_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_SB_ANALYTICS_ENABLED_9 FOR SB_ANALYTICS_ENABLED After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(56, OLD.rec_version, OLD.sb_analytics_enabled_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 644, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 645, OLD.effective_until);

  /* CL_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 646, OLD.cl_nbr);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 647, OLD.co_nbr);

  /* ENABLE_ANALYTICS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 648, OLD.enable_analytics);

END

^

CREATE TRIGGER T_AI_SB_ANALYTICS_ENABLED_9 FOR SB_ANALYTICS_ENABLED After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(56, NEW.rec_version, NEW.sb_analytics_enabled_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ANALYTICS_ENABLED_9 FOR SB_ANALYTICS_ENABLED After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(56, NEW.rec_version, NEW.sb_analytics_enabled_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 644, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 645, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 646, OLD.cl_nbr);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 647, OLD.co_nbr);
    changes = changes + 1;
  END

  /* ENABLE_ANALYTICS */
  IF (OLD.enable_analytics IS DISTINCT FROM NEW.enable_analytics) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 648, OLD.enable_analytics);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ANALYTICS_ENABLED_1 FOR SB_ANALYTICS_ENABLED Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.ENABLE_ANALYTICS is null OR CHAR_LENGTH(TRIM(new.ENABLE_ANALYTICS))=0) THEN 
      new.ENABLE_ANALYTICS = 'N'; 

  END 
END

^

CREATE TRIGGER T_BIU_SB_ANALYTICS_ENABLED_3 FOR SB_ANALYTICS_ENABLED Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_ANALYTICS_ENABLED_1 */
  IF (INSERTING OR (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.enable_analytics IS DISTINCT FROM NEW.enable_analytics)) THEN
    IF (EXISTS(SELECT 1 FROM sb_analytics_enabled WHERE cl_nbr = NEW.cl_nbr AND co_nbr = NEW.co_nbr AND enable_analytics = NEW.enable_analytics AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_analytics_enabled', 'cl_nbr, co_nbr, enable_analytics',
      CAST(NEW.cl_nbr || ', ' || NEW.co_nbr || ', ' || NEW.enable_analytics as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_ANALYTICS_ENABLED_1 FOR SB_ANALYTICS_ENABLED Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_analytics_enabled_ver;

  IF (EXISTS (SELECT 1 FROM sb_analytics_enabled WHERE sb_analytics_enabled_nbr = NEW.sb_analytics_enabled_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_analytics_enabled', NEW.sb_analytics_enabled_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_ANALYTICS_ENABLED_1 FOR SB_ANALYTICS_ENABLED Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_analytics_enabled_nbr IS DISTINCT FROM OLD.sb_analytics_enabled_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_analytics_enabled', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^






COMMIT^




/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_table (nbr, name, versioned) VALUES (56, 'SB_ANALYTICS_ENABLED', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 642, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 643, 'SB_ANALYTICS_ENABLED_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 644, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 645, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 646, 'CL_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 647, 'CO_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (56, 648, 'ENABLE_ANALYTICS', 'C', 1, NULL, 'Y', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^




/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('16.0.0.3', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
	