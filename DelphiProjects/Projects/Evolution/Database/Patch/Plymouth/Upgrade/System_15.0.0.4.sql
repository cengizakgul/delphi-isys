/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.3';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^

COMMIT^

DROP INDEX LK_SY_COUNTY
^


DROP TRIGGER T_BIU_SY_COUNTY_3
^

CREATE INDEX LK_SY_COUNTY ON SY_COUNTY (COUNTY_NAME,SY_STATES_NBR)
^

CREATE TRIGGER T_BIU_SY_COUNTY_3 FOR SY_COUNTY Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SY_COUNTY */
  IF (INSERTING OR (OLD.county_name IS DISTINCT FROM NEW.county_name) OR (OLD.sy_states_nbr IS DISTINCT FROM NEW.sy_states_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sy_county WHERE county_name = NEW.county_name AND sy_states_nbr = NEW.sy_states_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sy_county', 'county_name, sy_states_nbr',
      CAST(NEW.county_name || ', ' || NEW.sy_states_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.4', 'Evolution System Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
