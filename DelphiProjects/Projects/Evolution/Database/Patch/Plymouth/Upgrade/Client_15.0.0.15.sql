/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.14';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


DROP TRIGGER T_AD_EE_9
^


DROP TRIGGER T_AU_EE_9
^

ALTER TABLE EE ADD ACA_STATUS EV_CHAR1 NOT NULL
^

ALTER TABLE EE ALTER COLUMN ACA_STATUS POSITION 145
^

CREATE OR ALTER PROCEDURE pack_ee(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE p_cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE p_co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE p_co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE p_co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE p_co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE p_new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE p_current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE p_co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE p_tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE p_w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE p_w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE p_w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE p_w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE p_w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE p_w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE p_home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE p_exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE p_exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE p_base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE p_company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE p_distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE p_tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE p_total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE p_pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE p_makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE p_salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE p_highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE p_corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE p_co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE p_co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE p_healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
DECLARE VARIABLE p_aca_status CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_nbr , cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status
        FROM ee
        ORDER BY ee_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_nbr, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_nbr) OR (effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override) OR (p_aca_status IS DISTINCT FROM aca_status)) THEN
      BEGIN
        curr_nbr = ee_nbr;
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
        p_aca_status = aca_status;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status
        FROM ee
        WHERE ee_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override) OR (p_aca_status IS DISTINCT FROM aca_status)) THEN
      BEGIN
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
        p_aca_status = aca_status;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_EE TO EUSER
^

CREATE TRIGGER T_AD_EE_9 FOR EE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, OLD.rec_version, OLD.ee_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status  FROM ee WHERE ee_nbr = OLD.ee_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1810, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2784, OLD.effective_until);

  /* CL_PERSON_NBR */
  IF (last_record = 'Y' OR cl_person_nbr IS DISTINCT FROM OLD.cl_person_nbr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);

  /* CO_DIVISION_NBR */
  IF ((last_record = 'Y' AND OLD.co_division_nbr IS NOT NULL) OR (last_record = 'N' AND co_division_nbr IS DISTINCT FROM OLD.co_division_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);

  /* CO_BRANCH_NBR */
  IF ((last_record = 'Y' AND OLD.co_branch_nbr IS NOT NULL) OR (last_record = 'N' AND co_branch_nbr IS DISTINCT FROM OLD.co_branch_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);

  /* CO_DEPARTMENT_NBR */
  IF ((last_record = 'Y' AND OLD.co_department_nbr IS NOT NULL) OR (last_record = 'N' AND co_department_nbr IS DISTINCT FROM OLD.co_department_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);

  /* CO_TEAM_NBR */
  IF ((last_record = 'Y' AND OLD.co_team_nbr IS NOT NULL) OR (last_record = 'N' AND co_team_nbr IS DISTINCT FROM OLD.co_team_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);

  /* NEW_HIRE_REPORT_SENT */
  IF (last_record = 'Y' OR new_hire_report_sent IS DISTINCT FROM OLD.new_hire_report_sent) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);

  /* CURRENT_TERMINATION_CODE */
  IF (last_record = 'Y' OR current_termination_code IS DISTINCT FROM OLD.current_termination_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1889, OLD.current_termination_code);

  /* CO_HR_POSITIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_hr_positions_nbr IS NOT NULL) OR (last_record = 'N' AND co_hr_positions_nbr IS DISTINCT FROM OLD.co_hr_positions_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);

  /* TIPPED_DIRECTLY */
  IF (last_record = 'Y' OR tipped_directly IS DISTINCT FROM OLD.tipped_directly) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1893, OLD.tipped_directly);

  /* W2_TYPE */
  IF (last_record = 'Y' OR w2_type IS DISTINCT FROM OLD.w2_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1897, OLD.w2_type);

  /* W2_PENSION */
  IF (last_record = 'Y' OR w2_pension IS DISTINCT FROM OLD.w2_pension) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1898, OLD.w2_pension);

  /* W2_DEFERRED_COMP */
  IF (last_record = 'Y' OR w2_deferred_comp IS DISTINCT FROM OLD.w2_deferred_comp) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);

  /* W2_DECEASED */
  IF (last_record = 'Y' OR w2_deceased IS DISTINCT FROM OLD.w2_deceased) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1900, OLD.w2_deceased);

  /* W2_STATUTORY_EMPLOYEE */
  IF (last_record = 'Y' OR w2_statutory_employee IS DISTINCT FROM OLD.w2_statutory_employee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);

  /* W2_LEGAL_REP */
  IF (last_record = 'Y' OR w2_legal_rep IS DISTINCT FROM OLD.w2_legal_rep) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);

  /* HOME_TAX_EE_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.home_tax_ee_states_nbr IS NOT NULL) OR (last_record = 'N' AND home_tax_ee_states_nbr IS DISTINCT FROM OLD.home_tax_ee_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (last_record = 'Y' OR exempt_employee_oasdi IS DISTINCT FROM OLD.exempt_employee_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (last_record = 'Y' OR exempt_employee_medicare IS DISTINCT FROM OLD.exempt_employee_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (last_record = 'Y' OR exempt_exclude_ee_fed IS DISTINCT FROM OLD.exempt_exclude_ee_fed) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);

  /* EXEMPT_EMPLOYER_OASDI */
  IF (last_record = 'Y' OR exempt_employer_oasdi IS DISTINCT FROM OLD.exempt_employer_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (last_record = 'Y' OR exempt_employer_medicare IS DISTINCT FROM OLD.exempt_employer_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);

  /* EXEMPT_EMPLOYER_FUI */
  IF (last_record = 'Y' OR exempt_employer_fui IS DISTINCT FROM OLD.exempt_employer_fui) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);

  /* BASE_RETURNS_ON_THIS_EE */
  IF (last_record = 'Y' OR base_returns_on_this_ee IS DISTINCT FROM OLD.base_returns_on_this_ee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (last_record = 'Y' OR company_or_individual_name IS DISTINCT FROM OLD.company_or_individual_name) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);

  /* DISTRIBUTION_CODE_1099R */
  IF ((last_record = 'Y' AND OLD.distribution_code_1099r IS NOT NULL) OR (last_record = 'N' AND distribution_code_1099r IS DISTINCT FROM OLD.distribution_code_1099r)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);

  /* TAX_AMT_DETERMINED_1099R */
  IF (last_record = 'Y' OR tax_amt_determined_1099r IS DISTINCT FROM OLD.tax_amt_determined_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);

  /* TOTAL_DISTRIBUTION_1099R */
  IF (last_record = 'Y' OR total_distribution_1099r IS DISTINCT FROM OLD.total_distribution_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);

  /* PENSION_PLAN_1099R */
  IF (last_record = 'Y' OR pension_plan_1099r IS DISTINCT FROM OLD.pension_plan_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (last_record = 'Y' OR makeup_fica_on_cleanup_pr IS DISTINCT FROM OLD.makeup_fica_on_cleanup_pr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);

  /* SALARY_AMOUNT */
  IF ((last_record = 'Y' AND OLD.salary_amount IS NOT NULL) OR (last_record = 'N' AND salary_amount IS DISTINCT FROM OLD.salary_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1809, OLD.salary_amount);

  /* HIGHLY_COMPENSATED */
  IF (last_record = 'Y' OR highly_compensated IS DISTINCT FROM OLD.highly_compensated) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1919, OLD.highly_compensated);

  /* CORPORATE_OFFICER */
  IF (last_record = 'Y' OR corporate_officer IS DISTINCT FROM OLD.corporate_officer) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1920, OLD.corporate_officer);

  /* CO_JOBS_NBR */
  IF ((last_record = 'Y' AND OLD.co_jobs_nbr IS NOT NULL) OR (last_record = 'N' AND co_jobs_nbr IS DISTINCT FROM OLD.co_jobs_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);

  /* CO_WORKERS_COMP_NBR */
  IF ((last_record = 'Y' AND OLD.co_workers_comp_nbr IS NOT NULL) OR (last_record = 'N' AND co_workers_comp_nbr IS DISTINCT FROM OLD.co_workers_comp_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);

  /* HEALTHCARE_COVERAGE */
  IF (last_record = 'Y' OR healthcare_coverage IS DISTINCT FROM OLD.healthcare_coverage) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF ((last_record = 'Y' AND OLD.fui_rate_credit_override IS NOT NULL) OR (last_record = 'N' AND fui_rate_credit_override IS DISTINCT FROM OLD.fui_rate_credit_override)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);

  /* ACA_STATUS */
  IF (last_record = 'Y' OR aca_status IS DISTINCT FROM OLD.aca_status) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3188, OLD.aca_status);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CO_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1823, OLD.co_nbr);

    /* CUSTOM_EMPLOYEE_NUMBER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);

    /* CO_HR_APPLICANT_NBR */
    IF (OLD.co_hr_applicant_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);

    /* CO_HR_RECRUITERS_NBR */
    IF (OLD.co_hr_recruiters_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);

    /* CO_HR_REFERRALS_NBR */
    IF (OLD.co_hr_referrals_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);

    /* ADDRESS1 */
    IF (OLD.address1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1811, OLD.address1);

    /* ADDRESS2 */
    IF (OLD.address2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1812, OLD.address2);

    /* CITY */
    IF (OLD.city IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1869, OLD.city);

    /* STATE */
    IF (OLD.state IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1870, OLD.state);

    /* ZIP_CODE */
    IF (OLD.zip_code IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1831, OLD.zip_code);

    /* COUNTY */
    IF (OLD.county IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1832, OLD.county);

    /* PHONE1 */
    IF (OLD.phone1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1871, OLD.phone1);

    /* DESCRIPTION1 */
    IF (OLD.description1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1834, OLD.description1);

    /* PHONE2 */
    IF (OLD.phone2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1872, OLD.phone2);

    /* DESCRIPTION2 */
    IF (OLD.description2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1835, OLD.description2);

    /* PHONE3 */
    IF (OLD.phone3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1873, OLD.phone3);

    /* DESCRIPTION3 */
    IF (OLD.description3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1833, OLD.description3);

    /* E_MAIL_ADDRESS */
    IF (OLD.e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1874, OLD.e_mail_address);

    /* TIME_CLOCK_NUMBER */
    IF (OLD.time_clock_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1887, OLD.time_clock_number);

    /* ORIGINAL_HIRE_DATE */
    IF (OLD.original_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1848, OLD.original_hire_date);

    /* CURRENT_HIRE_DATE */
    IF (OLD.current_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1849, OLD.current_hire_date);

    /* CURRENT_TERMINATION_DATE */
    IF (OLD.current_termination_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1850, OLD.current_termination_date);

    /* CO_HR_SUPERVISORS_NBR */
    IF (OLD.co_hr_supervisors_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);

    /* AUTOPAY_CO_SHIFTS_NBR */
    IF (OLD.autopay_co_shifts_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);

    /* POSITION_EFFECTIVE_DATE */
    IF (OLD.position_effective_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1851, OLD.position_effective_date);

    /* POSITION_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1890, OLD.position_status);

    /* CO_HR_PERFORMANCE_RATINGS_NBR */
    IF (OLD.co_hr_performance_ratings_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);

    /* REVIEW_DATE */
    IF (OLD.review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1852, OLD.review_date);

    /* NEXT_REVIEW_DATE */
    IF (OLD.next_review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1853, OLD.next_review_date);

    /* PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1891, OLD.pay_frequency);

    /* NEXT_RAISE_DATE */
    IF (OLD.next_raise_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1854, OLD.next_raise_date);

    /* NEXT_PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);

    /* ALD_CL_E_D_GROUPS_NBR */
    IF (OLD.ald_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);

    /* DISTRIBUTE_TAXES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);

    /* CO_PAY_GROUP_NBR */
    IF (OLD.co_pay_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);

    /* CL_DELIVERY_GROUP_NBR */
    IF (OLD.cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);

    /* CO_UNIONS_NBR */
    IF (OLD.co_unions_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);

    /* EIC */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1895, OLD.eic);

    /* FLSA_EXEMPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);

    /* OVERRIDE_FED_TAX_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);

    /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);

    /* SECURITY_CLEARANCE */
    IF (OLD.security_clearance IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1875, OLD.security_clearance);

    /* BADGE_ID */
    IF (OLD.badge_id IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1911, OLD.badge_id);

    /* ON_CALL_FROM */
    IF (OLD.on_call_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1813, OLD.on_call_from);

    /* ON_CALL_TO */
    IF (OLD.on_call_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1814, OLD.on_call_to);

    /* NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1815, :blob_nbr);
    END

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1876, OLD.filler);

    /* GENERAL_LEDGER_TAG */
    IF (OLD.general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);

    /* FEDERAL_MARITAL_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);

    /* NUMBER_OF_DEPENDENTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);

    /* SY_HR_EEO_NBR */
    IF (OLD.sy_hr_eeo_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);

    /* SECONDARY_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1816, :blob_nbr);
    END

    /* CO_PR_CHECK_TEMPLATES_NBR */
    IF (OLD.co_pr_check_templates_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);

    /* GENERATE_SECOND_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1884, OLD.generate_second_check);

    /* STANDARD_HOURS */
    IF (OLD.standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1808, OLD.standard_hours);

    /* NEXT_RAISE_AMOUNT */
    IF (OLD.next_raise_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);

    /* NEXT_RAISE_PERCENTAGE */
    IF (OLD.next_raise_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);

    /* NEXT_RAISE_RATE */
    IF (OLD.next_raise_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);

    /* WORKERS_COMP_WAGE_LIMIT */
    IF (OLD.workers_comp_wage_limit IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);

    /* GROUP_TERM_POLICY_AMOUNT */
    IF (OLD.group_term_policy_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);

    /* OVERRIDE_FED_TAX_VALUE */
    IF (OLD.override_fed_tax_value IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);

    /* CALCULATED_SALARY */
    IF (OLD.calculated_salary IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1801, OLD.calculated_salary);

    /* PR_CHECK_MB_GROUP_NBR */
    IF (OLD.pr_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);

    /* PR_EE_REPORT_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);

    /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_sec_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);

    /* TAX_EE_RETURN_MB_GROUP_NBR */
    IF (OLD.tax_ee_return_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);

    /* EE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1921, OLD.ee_enabled);

    /* GTL_NUMBER_OF_HOURS */
    IF (OLD.gtl_number_of_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);

    /* GTL_RATE */
    IF (OLD.gtl_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1817, OLD.gtl_rate);

    /* AUTO_UPDATE_RATES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);

    /* ELIGIBLE_FOR_REHIRE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);

    /* SELFSERVE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);

    /* SELFSERVE_USERNAME */
    IF (OLD.selfserve_username IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1868, OLD.selfserve_username);

    /* SELFSERVE_PASSWORD */
    IF (OLD.selfserve_password IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1867, OLD.selfserve_password);

    /* SELFSERVE_LAST_LOGIN */
    IF (OLD.selfserve_last_login IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);

    /* PRINT_VOUCHER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1882, OLD.print_voucher);

    /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
    IF (OLD.override_federal_minimum_wage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);

    /* WC_WAGE_LIMIT_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);

    /* LOGIN_QUESTION1 */
    IF (OLD.login_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1864, OLD.login_question1);

    /* LOGIN_ANSWER1 */
    IF (OLD.login_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1879, OLD.login_answer1);

    /* LOGIN_QUESTION2 */
    IF (OLD.login_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1865, OLD.login_question2);

    /* LOGIN_ANSWER2 */
    IF (OLD.login_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1880, OLD.login_answer2);

    /* LOGIN_QUESTION3 */
    IF (OLD.login_question3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1866, OLD.login_question3);

    /* LOGIN_ANSWER3 */
    IF (OLD.login_answer3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1881, OLD.login_answer3);

    /* LOGIN_DATE */
    IF (OLD.login_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2785, OLD.login_date);

    /* LOGIN_ATTEMPTS */
    IF (OLD.login_attempts IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2786, OLD.login_attempts);

    /* CO_BENEFIT_DISCOUNT_NBR */
    IF (OLD.co_benefit_discount_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);

    /* BENEFITS_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);

    /* EXISTING_PATIENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2914, OLD.existing_patient);

    /* PCP */
    IF (OLD.pcp IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2915, OLD.pcp);

    /* TIME_OFF_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);

    /* DEPENDENT_BENEFITS_AVAILABLE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);

    /* DEPENDENT_BENEFITS_AVAIL_DATE */
    IF (OLD.dependent_benefits_avail_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);

    /* BENEFIT_ENROLLMENT_COMPLETE */
    IF (OLD.benefit_enrollment_complete IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);

    /* LAST_QUAL_BENEFIT_EVENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);

    /* LAST_QUAL_BENEFIT_EVENT_DATE */
    IF (OLD.last_qual_benefit_event_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);

    /* W2 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2922, OLD.w2);

    /* W2_FORM_ON_FILE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);

    /* BENEFIT_E_MAIL_ADDRESS */
    IF (OLD.benefit_e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);

    /* CO_HR_POSITION_GRADES_NBR */
    IF (OLD.co_hr_position_grades_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);

    /* SEC_QUESTION1 */
    IF (OLD.sec_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2926, OLD.sec_question1);

    /* SEC_ANSWER1 */
    IF (OLD.sec_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2927, OLD.sec_answer1);

    /* SEC_QUESTION2 */
    IF (OLD.sec_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2928, OLD.sec_question2);

    /* SEC_ANSWER2 */
    IF (OLD.sec_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2929, OLD.sec_answer2);

  END
END
^

CREATE TRIGGER T_AU_EE_9 FOR EE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, NEW.rec_version, NEW.ee_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1810, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2784, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_PERSON_NBR */
  IF (OLD.cl_person_nbr IS DISTINCT FROM NEW.cl_person_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1823, OLD.co_nbr);
    changes = changes + 1;
  END

  /* CO_DIVISION_NBR */
  IF (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);
    changes = changes + 1;
  END

  /* CO_BRANCH_NBR */
  IF (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);
    changes = changes + 1;
  END

  /* CO_DEPARTMENT_NBR */
  IF (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);
    changes = changes + 1;
  END

  /* CO_TEAM_NBR */
  IF (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);
    changes = changes + 1;
  END

  /* CUSTOM_EMPLOYEE_NUMBER */
  IF (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);
    changes = changes + 1;
  END

  /* CO_HR_APPLICANT_NBR */
  IF (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);
    changes = changes + 1;
  END

  /* CO_HR_RECRUITERS_NBR */
  IF (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);
    changes = changes + 1;
  END

  /* CO_HR_REFERRALS_NBR */
  IF (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1811, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1812, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1869, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1870, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1831, OLD.zip_code);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1832, OLD.county);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1871, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1834, OLD.description1);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1872, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1835, OLD.description2);
    changes = changes + 1;
  END

  /* PHONE3 */
  IF (OLD.phone3 IS DISTINCT FROM NEW.phone3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1873, OLD.phone3);
    changes = changes + 1;
  END

  /* DESCRIPTION3 */
  IF (OLD.description3 IS DISTINCT FROM NEW.description3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1833, OLD.description3);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1874, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* TIME_CLOCK_NUMBER */
  IF (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1887, OLD.time_clock_number);
    changes = changes + 1;
  END

  /* ORIGINAL_HIRE_DATE */
  IF (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1848, OLD.original_hire_date);
    changes = changes + 1;
  END

  /* CURRENT_HIRE_DATE */
  IF (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1849, OLD.current_hire_date);
    changes = changes + 1;
  END

  /* NEW_HIRE_REPORT_SENT */
  IF (OLD.new_hire_report_sent IS DISTINCT FROM NEW.new_hire_report_sent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_DATE */
  IF (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1850, OLD.current_termination_date);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_CODE */
  IF (OLD.current_termination_code IS DISTINCT FROM NEW.current_termination_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1889, OLD.current_termination_code);
    changes = changes + 1;
  END

  /* CO_HR_SUPERVISORS_NBR */
  IF (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);
    changes = changes + 1;
  END

  /* AUTOPAY_CO_SHIFTS_NBR */
  IF (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);
    changes = changes + 1;
  END

  /* CO_HR_POSITIONS_NBR */
  IF (OLD.co_hr_positions_nbr IS DISTINCT FROM NEW.co_hr_positions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);
    changes = changes + 1;
  END

  /* POSITION_EFFECTIVE_DATE */
  IF (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1851, OLD.position_effective_date);
    changes = changes + 1;
  END

  /* POSITION_STATUS */
  IF (OLD.position_status IS DISTINCT FROM NEW.position_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1890, OLD.position_status);
    changes = changes + 1;
  END

  /* CO_HR_PERFORMANCE_RATINGS_NBR */
  IF (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);
    changes = changes + 1;
  END

  /* REVIEW_DATE */
  IF (OLD.review_date IS DISTINCT FROM NEW.review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1852, OLD.review_date);
    changes = changes + 1;
  END

  /* NEXT_REVIEW_DATE */
  IF (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1853, OLD.next_review_date);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY */
  IF (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1891, OLD.pay_frequency);
    changes = changes + 1;
  END

  /* NEXT_RAISE_DATE */
  IF (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1854, OLD.next_raise_date);
    changes = changes + 1;
  END

  /* NEXT_PAY_FREQUENCY */
  IF (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);
    changes = changes + 1;
  END

  /* TIPPED_DIRECTLY */
  IF (OLD.tipped_directly IS DISTINCT FROM NEW.tipped_directly) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1893, OLD.tipped_directly);
    changes = changes + 1;
  END

  /* ALD_CL_E_D_GROUPS_NBR */
  IF (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* DISTRIBUTE_TAXES */
  IF (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);
    changes = changes + 1;
  END

  /* CO_PAY_GROUP_NBR */
  IF (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);
    changes = changes + 1;
  END

  /* CL_DELIVERY_GROUP_NBR */
  IF (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* CO_UNIONS_NBR */
  IF (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);
    changes = changes + 1;
  END

  /* EIC */
  IF (OLD.eic IS DISTINCT FROM NEW.eic) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1895, OLD.eic);
    changes = changes + 1;
  END

  /* FLSA_EXEMPT */
  IF (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);
    changes = changes + 1;
  END

  /* W2_TYPE */
  IF (OLD.w2_type IS DISTINCT FROM NEW.w2_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1897, OLD.w2_type);
    changes = changes + 1;
  END

  /* W2_PENSION */
  IF (OLD.w2_pension IS DISTINCT FROM NEW.w2_pension) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1898, OLD.w2_pension);
    changes = changes + 1;
  END

  /* W2_DEFERRED_COMP */
  IF (OLD.w2_deferred_comp IS DISTINCT FROM NEW.w2_deferred_comp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);
    changes = changes + 1;
  END

  /* W2_DECEASED */
  IF (OLD.w2_deceased IS DISTINCT FROM NEW.w2_deceased) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1900, OLD.w2_deceased);
    changes = changes + 1;
  END

  /* W2_STATUTORY_EMPLOYEE */
  IF (OLD.w2_statutory_employee IS DISTINCT FROM NEW.w2_statutory_employee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);
    changes = changes + 1;
  END

  /* W2_LEGAL_REP */
  IF (OLD.w2_legal_rep IS DISTINCT FROM NEW.w2_legal_rep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);
    changes = changes + 1;
  END

  /* HOME_TAX_EE_STATES_NBR */
  IF (OLD.home_tax_ee_states_nbr IS DISTINCT FROM NEW.home_tax_ee_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (OLD.exempt_employee_oasdi IS DISTINCT FROM NEW.exempt_employee_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (OLD.exempt_employee_medicare IS DISTINCT FROM NEW.exempt_employee_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (OLD.exempt_exclude_ee_fed IS DISTINCT FROM NEW.exempt_exclude_ee_fed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_OASDI */
  IF (OLD.exempt_employer_oasdi IS DISTINCT FROM NEW.exempt_employer_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (OLD.exempt_employer_medicare IS DISTINCT FROM NEW.exempt_employer_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_FUI */
  IF (OLD.exempt_employer_fui IS DISTINCT FROM NEW.exempt_employer_fui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_TYPE */
  IF (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);
    changes = changes + 1;
  END

  /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
  IF (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);
    changes = changes + 1;
  END

  /* SECURITY_CLEARANCE */
  IF (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1875, OLD.security_clearance);
    changes = changes + 1;
  END

  /* BADGE_ID */
  IF (OLD.badge_id IS DISTINCT FROM NEW.badge_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1911, OLD.badge_id);
    changes = changes + 1;
  END

  /* ON_CALL_FROM */
  IF (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1813, OLD.on_call_from);
    changes = changes + 1;
  END

  /* ON_CALL_TO */
  IF (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1814, OLD.on_call_to);
    changes = changes + 1;
  END

  /* BASE_RETURNS_ON_THIS_EE */
  IF (OLD.base_returns_on_this_ee IS DISTINCT FROM NEW.base_returns_on_this_ee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1815, :blob_nbr);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1876, OLD.filler);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_TAG */
  IF (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);
    changes = changes + 1;
  END

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (OLD.company_or_individual_name IS DISTINCT FROM NEW.company_or_individual_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);
    changes = changes + 1;
  END

  /* FEDERAL_MARITAL_STATUS */
  IF (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);
    changes = changes + 1;
  END

  /* NUMBER_OF_DEPENDENTS */
  IF (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);
    changes = changes + 1;
  END

  /* DISTRIBUTION_CODE_1099R */
  IF (OLD.distribution_code_1099r IS DISTINCT FROM NEW.distribution_code_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);
    changes = changes + 1;
  END

  /* TAX_AMT_DETERMINED_1099R */
  IF (OLD.tax_amt_determined_1099r IS DISTINCT FROM NEW.tax_amt_determined_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);
    changes = changes + 1;
  END

  /* TOTAL_DISTRIBUTION_1099R */
  IF (OLD.total_distribution_1099r IS DISTINCT FROM NEW.total_distribution_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);
    changes = changes + 1;
  END

  /* PENSION_PLAN_1099R */
  IF (OLD.pension_plan_1099r IS DISTINCT FROM NEW.pension_plan_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);
    changes = changes + 1;
  END

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (OLD.makeup_fica_on_cleanup_pr IS DISTINCT FROM NEW.makeup_fica_on_cleanup_pr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);
    changes = changes + 1;
  END

  /* SY_HR_EEO_NBR */
  IF (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);
    changes = changes + 1;
  END

  /* SECONDARY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1816, :blob_nbr);
    changes = changes + 1;
  END

  /* CO_PR_CHECK_TEMPLATES_NBR */
  IF (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);
    changes = changes + 1;
  END

  /* GENERATE_SECOND_CHECK */
  IF (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1884, OLD.generate_second_check);
    changes = changes + 1;
  END

  /* SALARY_AMOUNT */
  IF (OLD.salary_amount IS DISTINCT FROM NEW.salary_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1809, OLD.salary_amount);
    changes = changes + 1;
  END

  /* STANDARD_HOURS */
  IF (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1808, OLD.standard_hours);
    changes = changes + 1;
  END

  /* NEXT_RAISE_AMOUNT */
  IF (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);
    changes = changes + 1;
  END

  /* NEXT_RAISE_PERCENTAGE */
  IF (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);
    changes = changes + 1;
  END

  /* NEXT_RAISE_RATE */
  IF (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);
    changes = changes + 1;
  END

  /* WORKERS_COMP_WAGE_LIMIT */
  IF (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);
    changes = changes + 1;
  END

  /* GROUP_TERM_POLICY_AMOUNT */
  IF (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_VALUE */
  IF (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);
    changes = changes + 1;
  END

  /* CALCULATED_SALARY */
  IF (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1801, OLD.calculated_salary);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* HIGHLY_COMPENSATED */
  IF (OLD.highly_compensated IS DISTINCT FROM NEW.highly_compensated) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1919, OLD.highly_compensated);
    changes = changes + 1;
  END

  /* CORPORATE_OFFICER */
  IF (OLD.corporate_officer IS DISTINCT FROM NEW.corporate_officer) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1920, OLD.corporate_officer);
    changes = changes + 1;
  END

  /* CO_JOBS_NBR */
  IF (OLD.co_jobs_nbr IS DISTINCT FROM NEW.co_jobs_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* EE_ENABLED */
  IF (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1921, OLD.ee_enabled);
    changes = changes + 1;
  END

  /* GTL_NUMBER_OF_HOURS */
  IF (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);
    changes = changes + 1;
  END

  /* GTL_RATE */
  IF (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1817, OLD.gtl_rate);
    changes = changes + 1;
  END

  /* AUTO_UPDATE_RATES */
  IF (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);
    changes = changes + 1;
  END

  /* ELIGIBLE_FOR_REHIRE */
  IF (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);
    changes = changes + 1;
  END

  /* SELFSERVE_ENABLED */
  IF (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);
    changes = changes + 1;
  END

  /* SELFSERVE_USERNAME */
  IF (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1868, OLD.selfserve_username);
    changes = changes + 1;
  END

  /* SELFSERVE_PASSWORD */
  IF (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1867, OLD.selfserve_password);
    changes = changes + 1;
  END

  /* SELFSERVE_LAST_LOGIN */
  IF (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);
    changes = changes + 1;
  END

  /* PRINT_VOUCHER */
  IF (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1882, OLD.print_voucher);
    changes = changes + 1;
  END

  /* HEALTHCARE_COVERAGE */
  IF (OLD.healthcare_coverage IS DISTINCT FROM NEW.healthcare_coverage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);
    changes = changes + 1;
  END

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF (OLD.fui_rate_credit_override IS DISTINCT FROM NEW.fui_rate_credit_override) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);
    changes = changes + 1;
  END

  /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
  IF (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);
    changes = changes + 1;
  END

  /* WC_WAGE_LIMIT_FREQUENCY */
  IF (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1864, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1879, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1865, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1880, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1866, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1881, OLD.login_answer3);
    changes = changes + 1;
  END

  /* LOGIN_DATE */
  IF (OLD.login_date IS DISTINCT FROM NEW.login_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2785, OLD.login_date);
    changes = changes + 1;
  END

  /* LOGIN_ATTEMPTS */
  IF (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2786, OLD.login_attempts);
    changes = changes + 1;
  END

  /* CO_BENEFIT_DISCOUNT_NBR */
  IF (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);
    changes = changes + 1;
  END

  /* BENEFITS_ENABLED */
  IF (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);
    changes = changes + 1;
  END

  /* EXISTING_PATIENT */
  IF (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2914, OLD.existing_patient);
    changes = changes + 1;
  END

  /* PCP */
  IF (OLD.pcp IS DISTINCT FROM NEW.pcp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2915, OLD.pcp);
    changes = changes + 1;
  END

  /* TIME_OFF_ENABLED */
  IF (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAILABLE */
  IF (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAIL_DATE */
  IF (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);
    changes = changes + 1;
  END

  /* BENEFIT_ENROLLMENT_COMPLETE */
  IF (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT */
  IF (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT_DATE */
  IF (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);
    changes = changes + 1;
  END

  /* W2 */
  IF (OLD.w2 IS DISTINCT FROM NEW.w2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2922, OLD.w2);
    changes = changes + 1;
  END

  /* W2_FORM_ON_FILE */
  IF (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);
    changes = changes + 1;
  END

  /* BENEFIT_E_MAIL_ADDRESS */
  IF (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);
    changes = changes + 1;
  END

  /* CO_HR_POSITION_GRADES_NBR */
  IF (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2926, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2927, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2928, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2929, OLD.sec_answer2);
    changes = changes + 1;
  END

  /* ACA_STATUS */
  IF (OLD.aca_status IS DISTINCT FROM NEW.aca_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3188, OLD.aca_status);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^

COMMIT^


/* Update field ACA_STATUS field with the default value */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  UPDATE ee SET aca_status = position_status;
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^



/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (115, 3188, 'ACA_STATUS', 'C', 1, NULL, 'Y', 'Y');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.15', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
