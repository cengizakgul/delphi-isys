/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.2';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^

COMMIT^

DROP INDEX LK_SY_GL_AGENCY_FIELD_OFFICE
^

DROP TRIGGER T_BIU_SY_GL_AGENCY_FIELD_OFFI_3
^

CREATE INDEX LK_SY_GL_AGENCY_FIELD_OFFICE ON SY_GL_AGENCY_FIELD_OFFICE (ADDITIONAL_NAME,SY_GLOBAL_AGENCY_NBR)
^

CREATE TRIGGER T_BIU_SY_GL_AGENCY_FIELD_OFFI_3 FOR SY_GL_AGENCY_FIELD_OFFICE Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SY_GL_AGENCY_FIELD_OFFICE */
  IF (INSERTING OR (OLD.additional_name IS DISTINCT FROM NEW.additional_name) OR (OLD.sy_global_agency_nbr IS DISTINCT FROM NEW.sy_global_agency_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sy_gl_agency_field_office WHERE additional_name = NEW.additional_name AND sy_global_agency_nbr = NEW.sy_global_agency_nbr AND sy_gl_agency_field_office_nbr <> NEW.sy_gl_agency_field_office_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sy_gl_agency_field_office', 'additional_name, sy_global_agency_nbr',
      CAST(NEW.additional_name || ', ' || NEW.sy_global_agency_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^

/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.3', 'Evolution System Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
