/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.5';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


EXECUTE BLOCK
AS
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';
END
^


DROP TRIGGER T_AD_SB_9
^


DROP TRIGGER T_AU_SB_9
^


DROP TRIGGER T_BUD_SB_9
^

ALTER TABLE SB
ADD ESS_TERMS_OF_USE_MODIFY_DATE EV_DATE 
^

ALTER TABLE SB
ALTER COLUMN ESS_TERMS_OF_USE_MODIFY_DATE POSITION 58
^

ALTER TABLE SB
ADD ESS_LOGO EV_BLOB_BIN 
^

ALTER TABLE SB
ALTER COLUMN ESS_LOGO POSITION 59
^


DROP TRIGGER T_AD_SB_MAIL_BOX_CONTENT_9
^


DROP TRIGGER T_AU_SB_MAIL_BOX_CONTENT_9
^

ALTER TABLE SB_MAIL_BOX_CONTENT
ALTER COLUMN FILE_NAME TYPE EV_STR80
^


DROP TRIGGER T_AD_SB_TASK_9
^


DROP TRIGGER T_AU_SB_TASK_9
^

ALTER TABLE SB_TASK
ADD NOTES EV_STR255 
^

ALTER TABLE SB_TASK
ALTER COLUMN NOTES POSITION 10
^

CREATE TRIGGER T_AD_SB_9 FOR SB After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, OLD.rec_version, OLD.sb_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 8, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 452, OLD.effective_until);

  /* SB_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 28, OLD.sb_name);

  /* ADDRESS1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 5, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 6, OLD.address2);

  /* CITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 29, OLD.city);

  /* STATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 30, OLD.state);

  /* ZIP_CODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 20, OLD.zip_code);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 31, OLD.e_mail_address);

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);

  /* DEVELOPMENT_MODEM_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 33, OLD.development_modem_number);

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 35, OLD.development_ftp_password);

  /* EIN_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 49, OLD.ein_number);

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);

  /* EFTPS_BANK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);

  /* USE_PRENOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 53, OLD.use_prenote);

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);

  /* PAY_TAX_FROM_PAYABLES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);

  /* AR_EXPORT_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 57, OLD.ar_export_format);

  /* DEFAULT_CHECK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 58, OLD.default_check_format);

  /* MICR_FONT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 59, OLD.micr_font);

  /* MICR_HORIZONTAL_ADJUSTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);

  /* AUTO_SAVE_MINUTES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);

  /* PHONE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 36, OLD.phone);

  /* FAX */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 37, OLD.fax);

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 7, :blob_nbr);
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 4, :blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3, :blob_nbr);
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 27, OLD.ar_import_directory);

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 26, OLD.ach_directory);

  /* SB_URL */
  IF (OLD.sb_url IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 25, OLD.sb_url);

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 18, OLD.days_in_prenote);

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 9, :blob_nbr);
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);

  /* ERROR_SCREEN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 48, OLD.error_screen);

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 15, OLD.pswd_min_length);

  /* PSWD_FORCE_MIXED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);

  /* MISC_CHECK_FORM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 46, OLD.misc_check_form);

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2, :blob_nbr);
  END

  /* MARK_LIABS_PAID_DEFAULT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);

  /* TRUST_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 44, OLD.trust_impound);

  /* TAX_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 43, OLD.tax_impound);

  /* DD_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 42, OLD.dd_impound);

  /* BILLING_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 40, OLD.billing_impound);

  /* WC_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 39, OLD.wc_impound);

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);

  /* SB_EXCEPTION_PAYMENT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);

  /* SB_ACH_FILE_LIMITATIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 543, OLD.dashboard_msg);

  /* EE_LOGIN_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 544, OLD.ee_login_type);

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 550, :blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 551, :blob_nbr);
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 569, :blob_nbr);
  END

END

^

CREATE TRIGGER T_AU_SB_9 FOR SB After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, NEW.rec_version, NEW.sb_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 8, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 452, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_NAME */
  IF (OLD.sb_name IS DISTINCT FROM NEW.sb_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 28, OLD.sb_name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 5, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 6, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 29, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 30, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 20, OLD.zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 31, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS DISTINCT FROM NEW.parent_sb_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_MODEM_NUMBER */
  IF (OLD.development_modem_number IS DISTINCT FROM NEW.development_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 33, OLD.development_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS DISTINCT FROM NEW.development_ftp_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 35, OLD.development_ftp_password);
    changes = changes + 1;
  END

  /* EIN_NUMBER */
  IF (OLD.ein_number IS DISTINCT FROM NEW.ein_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 49, OLD.ein_number);
    changes = changes + 1;
  END

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS DISTINCT FROM NEW.eftps_tin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);
    changes = changes + 1;
  END

  /* EFTPS_BANK_FORMAT */
  IF (OLD.eftps_bank_format IS DISTINCT FROM NEW.eftps_bank_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);
    changes = changes + 1;
  END

  /* USE_PRENOTE */
  IF (OLD.use_prenote IS DISTINCT FROM NEW.use_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 53, OLD.use_prenote);
    changes = changes + 1;
  END

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  IF (OLD.impound_trust_monies_as_receiv IS DISTINCT FROM NEW.impound_trust_monies_as_receiv) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);
    changes = changes + 1;
  END

  /* PAY_TAX_FROM_PAYABLES */
  IF (OLD.pay_tax_from_payables IS DISTINCT FROM NEW.pay_tax_from_payables) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);
    changes = changes + 1;
  END

  /* AR_EXPORT_FORMAT */
  IF (OLD.ar_export_format IS DISTINCT FROM NEW.ar_export_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 57, OLD.ar_export_format);
    changes = changes + 1;
  END

  /* DEFAULT_CHECK_FORMAT */
  IF (OLD.default_check_format IS DISTINCT FROM NEW.default_check_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 58, OLD.default_check_format);
    changes = changes + 1;
  END

  /* MICR_FONT */
  IF (OLD.micr_font IS DISTINCT FROM NEW.micr_font) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 59, OLD.micr_font);
    changes = changes + 1;
  END

  /* MICR_HORIZONTAL_ADJUSTMENT */
  IF (OLD.micr_horizontal_adjustment IS DISTINCT FROM NEW.micr_horizontal_adjustment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);
    changes = changes + 1;
  END

  /* AUTO_SAVE_MINUTES */
  IF (OLD.auto_save_minutes IS DISTINCT FROM NEW.auto_save_minutes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);
    changes = changes + 1;
  END

  /* PHONE */
  IF (OLD.phone IS DISTINCT FROM NEW.phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 36, OLD.phone);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 37, OLD.fax);
    changes = changes + 1;
  END

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 7, :blob_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 4, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3, :blob_nbr);
    changes = changes + 1;
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS DISTINCT FROM NEW.ar_import_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 27, OLD.ar_import_directory);
    changes = changes + 1;
  END

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS DISTINCT FROM NEW.ach_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 26, OLD.ach_directory);
    changes = changes + 1;
  END

  /* SB_URL */
  IF (OLD.sb_url IS DISTINCT FROM NEW.sb_url) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 25, OLD.sb_url);
    changes = changes + 1;
  END

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS DISTINCT FROM NEW.days_in_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 18, OLD.days_in_prenote);
    changes = changes + 1;
  END

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 9, :blob_nbr);
    changes = changes + 1;
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS DISTINCT FROM NEW.user_password_duration_in_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);
    changes = changes + 1;
  END

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS DISTINCT FROM NEW.dummy_tax_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);
    changes = changes + 1;
  END

  /* ERROR_SCREEN */
  IF (OLD.error_screen IS DISTINCT FROM NEW.error_screen) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 48, OLD.error_screen);
    changes = changes + 1;
  END

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS DISTINCT FROM NEW.pswd_min_length) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 15, OLD.pswd_min_length);
    changes = changes + 1;
  END

  /* PSWD_FORCE_MIXED */
  IF (OLD.pswd_force_mixed IS DISTINCT FROM NEW.pswd_force_mixed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 46, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2, :blob_nbr);
    changes = changes + 1;
  END

  /* MARK_LIABS_PAID_DEFAULT */
  IF (OLD.mark_liabs_paid_default IS DISTINCT FROM NEW.mark_liabs_paid_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 44, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 43, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 42, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 40, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 39, OLD.wc_impound);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* SB_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.sb_exception_payment_type IS DISTINCT FROM NEW.sb_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);
    changes = changes + 1;
  END

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS DISTINCT FROM NEW.sb_max_ach_file_total) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);
    changes = changes + 1;
  END

  /* SB_ACH_FILE_LIMITATIONS */
  IF (OLD.sb_ach_file_limitations IS DISTINCT FROM NEW.sb_ach_file_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);
    changes = changes + 1;
  END

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS DISTINCT FROM NEW.sb_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS DISTINCT FROM NEW.dashboard_msg) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.dashboard_msg);
    changes = changes + 1;
  END

  /* EE_LOGIN_TYPE */
  IF (OLD.ee_login_type IS DISTINCT FROM NEW.ee_login_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.ee_login_type);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, :blob_nbr);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS DISTINCT FROM NEW.ess_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 569, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BUD_SB_9 FOR SB Before Update or Delete POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* COVER_LETTER_NOTES */
  IF (DELETING OR (OLD.cover_letter_notes IS DISTINCT FROM NEW.cover_letter_notes)) THEN
  BEGIN
    IF (OLD.cover_letter_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.cover_letter_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', blob_nbr);
  END

  /* INVOICE_NOTES */
  IF (DELETING OR (OLD.invoice_notes IS DISTINCT FROM NEW.invoice_notes)) THEN
  BEGIN
    IF (OLD.invoice_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.invoice_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  IF (DELETING OR (OLD.tax_cover_letter_notes IS DISTINCT FROM NEW.tax_cover_letter_notes)) THEN
  BEGIN
    IF (OLD.tax_cover_letter_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.tax_cover_letter_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', blob_nbr);
  END

  /* SB_LOGO */
  IF (DELETING OR (OLD.sb_logo IS DISTINCT FROM NEW.sb_logo)) THEN
  BEGIN
    IF (OLD.sb_logo IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.sb_logo)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@SB_LOGO', blob_nbr);
  END

  /* VMR_CONFIDENCIAL_NOTES */
  IF (DELETING OR (OLD.vmr_confidencial_notes IS DISTINCT FROM NEW.vmr_confidencial_notes)) THEN
  BEGIN
    IF (OLD.vmr_confidencial_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.vmr_confidencial_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', blob_nbr);
  END

  /* ESS_TERMS_OF_USE */
  IF (DELETING OR (OLD.ess_terms_of_use IS DISTINCT FROM NEW.ess_terms_of_use)) THEN
  BEGIN
    IF (OLD.ess_terms_of_use IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.ess_terms_of_use)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  IF (DELETING OR (OLD.wc_terms_of_use IS DISTINCT FROM NEW.wc_terms_of_use)) THEN
  BEGIN
    IF (OLD.wc_terms_of_use IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.wc_terms_of_use)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', blob_nbr);
  END

  /* ESS_LOGO */
  IF (DELETING OR (OLD.ess_logo IS DISTINCT FROM NEW.ess_logo)) THEN
  BEGIN
    IF (OLD.ess_logo IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.ess_logo)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', blob_nbr);
  END
END

^

CREATE TRIGGER T_AD_SB_TASK_9 FOR SB_TASK After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(40, OLD.rec_version, OLD.sb_task_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 400, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 528, OLD.effective_until);

  /* SB_USER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 402, OLD.sb_user_nbr);

  /* SCHEDULE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 403, OLD.schedule);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 404, OLD.description);

  /* TASK */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TASK');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TASK', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 399, :blob_nbr);
  END

  /* LAST_RUN */
  IF (OLD.last_run IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 552, OLD.last_run);

  /* NOTES */
  IF (OLD.notes IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 567, OLD.notes);

END

^

CREATE TRIGGER T_AU_SB_TASK_9 FOR SB_TASK After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(40, NEW.rec_version, NEW.sb_task_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 400, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 528, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 402, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* SCHEDULE */
  IF (OLD.schedule IS DISTINCT FROM NEW.schedule) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 403, OLD.schedule);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 404, OLD.description);
    changes = changes + 1;
  END

  /* TASK */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TASK');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TASK', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 399, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_RUN */
  IF (OLD.last_run IS DISTINCT FROM NEW.last_run) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 552, OLD.last_run);
    changes = changes + 1;
  END

  /* NOTES */
  IF (OLD.notes IS DISTINCT FROM NEW.notes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 567, OLD.notes);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(17, OLD.rec_version, OLD.sb_mail_box_content_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 252, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 482, OLD.effective_until);

  /* SB_MAIL_BOX_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 254, OLD.sb_mail_box_nbr);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 257, OLD.description);

  /* FILE_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 256, OLD.file_name);

  /* MEDIA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 258, OLD.media_type);

  /* PAGE_COUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 253, OLD.page_count);

END

^

CREATE TRIGGER T_AU_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(17, NEW.rec_version, NEW.sb_mail_box_content_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 252, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_MAIL_BOX_NBR */
  IF (OLD.sb_mail_box_nbr IS DISTINCT FROM NEW.sb_mail_box_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 254, OLD.sb_mail_box_nbr);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 257, OLD.description);
    changes = changes + 1;
  END

  /* FILE_NAME */
  IF (OLD.file_name IS DISTINCT FROM NEW.file_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 256, OLD.file_name);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 258, OLD.media_type);
    changes = changes + 1;
  END

  /* PAGE_COUNT */
  IF (OLD.page_count IS DISTINCT FROM NEW.page_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 253, OLD.page_count);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 568, 'ESS_TERMS_OF_USE_MODIFY_DATE', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 569, 'ESS_LOGO', 'B', NULL, NULL, 'N', 'N');
  UPDATE ev_field SET name = 'FILE_NAME', field_type = 'V', len = 80, scale = NULL, required = 'Y', versioned = 'N' WHERE nbr = 256;
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 567, 'NOTES', 'V', 255, NULL, 'N', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.6', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
