/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '14.0.0.0';
  VER = 'UNKNOWN';
  SELECT CAST(MAJOR_VERSION AS VARCHAR(2)) || '.' ||
         CAST(MINOR_VERSION AS VARCHAR(2)) || '.' ||
         CAST(PATCH_VERSION AS VARCHAR(2)) || '.' ||
         CAST(BUILD_VERSION AS VARCHAR(2))
  FROM VERSION_INFO
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^

COMMIT^

SET TERM ;^


/*<progress>Preparing for data conversion</progress>*/
/* Delete some unwanted records */

DELETE FROM sb_option WHERE active_record IN ('P','N'); 
DELETE FROM sb_mail_box_option WHERE active_record IN ('P','N');
DELETE FROM sb_mail_box_content WHERE active_record IN ('P','N'); 
DELETE FROM sb_mail_box WHERE active_record IN ('P','N');
COMMIT;


/* CUT OFF AUDIT AND DELETED DATA BEFORE 2003 */

DELETE FROM sb_accountant WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_holidays WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sales_tax_states WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_referrals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_global_agency_contacts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_company WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_banks WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_team WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_user WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_agency WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_bank_accounts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_services WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_company_svcs WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_agency_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_services_calculations WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_team_members WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_group_members WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_rights WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_report_writer_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_row_filters WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_templates WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_clients WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_task WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_queue_priority WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_task_runs WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_method WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_mail_box_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_service_opt WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_paper_info WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_multiclient_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_media_type WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_service WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_mail_box WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_mail_box_content WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_media_type_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_other_service WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_enlist_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_tax_payment WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_storage WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;

/* Fix integrity issues */

UPDATE sb SET eftps_bank_format = 'B' WHERE eftps_bank_format IS NULL;
UPDATE sb_agency SET account_type = 'C' WHERE account_type IS NULL;
UPDATE sb_delivery_company SET delivery_contact_phone_type = 'P' WHERE delivery_contact_phone_type IS NULL;
UPDATE sb_delivery_company SET supplies_contact_phone_type = 'P' WHERE supplies_contact_phone_type IS NULL;
UPDATE sb_services SET week_number = ' ' WHERE week_number IS NULL;
COMMIT;



/* Update empty blobs */

SET TERM ^;

ALTER TRIGGER tu_sb_blob INACTIVE^

ALTER TRIGGER td_sb_blob INACTIVE^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE tbl_name VARCHAR(64);
DECLARE VARIABLE fld_name VARCHAR(64);
DECLARE VARIABLE sql VARCHAR(1024);
BEGIN
  FOR SELECT TRIM(r.rdb$relation_name), TRIM(f.rdb$field_name)
      FROM rdb$relations r, rdb$relation_fields f, rdb$fields dt
      WHERE
        (r.rdb$system_flag = 0 OR r.rdb$system_flag IS NULL) AND
        (f.rdb$system_flag = 0 OR f.rdb$system_flag IS NULL) AND
        f.rdb$relation_name = r.rdb$relation_name AND
        (f.rdb$null_flag = 0 OR f.rdb$null_flag IS NULL) AND
        f.rdb$field_source = dt.rdb$field_name AND
        dt.rdb$field_type = 261
  INTO :tbl_name, :fld_name
  DO
  BEGIN
    sql = 'UPDATE ' || tbl_name || ' SET ' || fld_name || ' = NULL WHERE GetBlobSize(' || fld_name || ') = 0';

    EXECUTE STATEMENT sql;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE nbr INTEGER;
DECLARE VARIABLE bnbr INTEGER;
BEGIN
  FOR SELECT t.sb_bank_accounts_nbr, b.sb_blob_nbr
      FROM sb_bank_accounts t, sb_blob b
      WHERE t.logo_sb_blob_nbr = b.sb_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE sb_bank_accounts SET logo_sb_blob_nbr = NULL WHERE sb_bank_accounts_nbr = :nbr;
    DELETE FROM sb_blob WHERE sb_blob_nbr = :bnbr;
  END

  FOR SELECT t.sb_bank_accounts_nbr, b.sb_blob_nbr
      FROM sb_bank_accounts t, sb_blob b
      WHERE t.signature_sb_blob_nbr = b.sb_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE sb_bank_accounts SET signature_sb_blob_nbr = NULL WHERE sb_bank_accounts_nbr = :nbr;
    DELETE FROM sb_blob WHERE sb_blob_nbr = :bnbr;
  END
END^

/* PREPARE DB STRUCTURE */

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$trigger_name FROM rdb$triggers
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP TRIGGER ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$procedure_name FROM rdb$procedures
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP PROCEDURE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$generator_name FROM rdb$generators
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    IF ((s <> 'NEXT_INVOICE_NUMBER_GEN') AND (s <> 'CL_GEN')) THEN
      EXECUTE STATEMENT 'DROP SEQUENCE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$exception_name FROM rdb$exceptions
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP EXCEPTION ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$index_name FROM rdb$indices i WHERE
        (rdb$system_flag = 0 OR rdb$system_flag IS NULL) AND
        NOT EXISTS (SELECT rdb$index_name FROM rdb$relation_constraints
        WHERE rdb$index_name = i.rdb$index_name)
      INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP INDEX ' || s;
  END
END^

COMMIT^

SET TERM ;^


/* DROP JUNK TABLES */

DROP TABLE sb_report_groups;
DROP TABLE sb_report_group_members;
DROP TABLE sb_sched_event_history;
DROP TABLE sb_scheduled_events;
DROP TABLE sb_mail_box_job;
DROP TABLE sb_bank_accounts_phone;
DROP TABLE sb_tax_return_runs;
DROP TABLE sb_exceptions;
DROP TABLE version_info;
DROP TABLE change_log;
DROP TABLE transaction_info;

COMMIT;

/* Prepare X_TRANSACTION table */

CREATE TABLE x_transaction (
    start_time   TIMESTAMP,
    user_id      INTEGER);

COMMIT;

INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_accountant;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_holidays;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sales_tax_states;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_referrals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_global_agency_contacts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_company;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_banks;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_team;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_user;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_agency;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_bank_accounts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_services;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_company_svcs;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_agency_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_services_calculations;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_team_members;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_group_members;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_rights;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_report_writer_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_row_filters;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_templates;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_clients;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT change_date, 0 FROM sb_blob;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_task;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_queue_priority;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_task_runs;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_paper_info;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_multiclient_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_method;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_media_type;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_service;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_mail_box;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_mail_box_content;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_mail_box_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_service_opt;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_media_type_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_other_service;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT CAST('1/1/2000' AS TIMESTAMP), 0 FROM sb_user_notice;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_enlist_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_tax_payment;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_storage;
COMMIT;


/* CREATE TEMP TABLES */

CREATE TABLE xb (
    sb_nbr                          INTEGER NOT NULL,
    sb_name                         VARCHAR(60) NOT NULL,
    address1                        VARCHAR(30) NOT NULL,
    address2                        VARCHAR(30),
    city                            VARCHAR(20) NOT NULL,
    state                           CHAR(2) NOT NULL,
    zip_code                        VARCHAR(10) NOT NULL,
    e_mail_address                  VARCHAR(80),
    parent_sb_number                INTEGER,
    parent_sb_modem_number          VARCHAR(20),
    development_modem_number        VARCHAR(20) NOT NULL,
    development_ftp_site            VARCHAR(80),
    development_ftp_password        VARCHAR(128),
    ein_number                      VARCHAR(9) NOT NULL,
    eftps_tin_number                VARCHAR(9),
    eftps_bank_format               CHAR(1),
    batch_or_edi_filing             CHAR(1),
    use_prenote                     CHAR(1) NOT NULL,
    impound_trust_monies_as_receiv  CHAR(1) NOT NULL,
    pay_tax_from_payables           CHAR(1) NOT NULL,
    tax_export_format               CHAR(1) NOT NULL,
    ar_export_format                CHAR(1) NOT NULL,
    default_check_format            CHAR(1) NOT NULL,
    micr_font                       CHAR(1) NOT NULL,
    micr_horizontal_adjustment      INTEGER NOT NULL,
    auto_save_minutes               INTEGER NOT NULL,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    phone                           VARCHAR(20) NOT NULL,
    fax                             VARCHAR(20) NOT NULL,
    cover_letter_notes              BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    invoice_notes                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    tax_cover_letter_notes          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    ar_import_directory             VARCHAR(80),
    ach_directory                   VARCHAR(80),
    sb_url                          VARCHAR(80),
    days_in_prenote                 INTEGER,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    sb_logo                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    non_tax_cover_letter_notes      BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    tax_recon_cover_letter_notes    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    defferred_comp_limit            NUMERIC(18,6),
    user_password_duration_in_days  NUMERIC(18,6),
    dummy_tax_cl_nbr                INTEGER,
    dummy_tax_co_nbr                INTEGER,
    error_screen                    CHAR(1) NOT NULL,
    pswd_min_length                 INTEGER,
    pswd_force_mixed                CHAR(1) NOT NULL,
    misc_check_form                 CHAR(1) NOT NULL,
    vmr_confidencial_notes          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    mark_liabs_paid_default         CHAR(1) NOT NULL,
    trust_impound                   CHAR(1) NOT NULL,
    tax_impound                     CHAR(1) NOT NULL,
    dd_impound                      CHAR(1) NOT NULL,
    billing_impound                 CHAR(1) NOT NULL,
    wc_impound                      CHAR(1) NOT NULL,
    days_prior_to_check_date        INTEGER,
    sb_exception_payment_type       CHAR(1) NOT NULL,
    sb_max_ach_file_total           NUMERIC(18,6),
    sb_ach_file_limitations         CHAR(1) NOT NULL,
    sb_cl_nbr                       INTEGER,
    dashboard_msg                   VARCHAR(200),
    ee_login_type                   CHAR(1) NOT NULL,
    ess_terms_of_use                BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    wc_terms_of_use                 BLOB SUB_TYPE 1 SEGMENT SIZE 80
);


CREATE TABLE xb_accountant (
    sb_accountant_nbr  INTEGER NOT NULL,
    name               VARCHAR(40) NOT NULL,
    address1           VARCHAR(30),
    address2           VARCHAR(30),
    city               VARCHAR(20),
    state              VARCHAR(3),
    zip_code           VARCHAR(10),
    contact1           VARCHAR(30) NOT NULL,
    phone1             VARCHAR(20),
    description1       VARCHAR(10),
    contact2           VARCHAR(30),
    phone2             VARCHAR(20),
    description2       VARCHAR(10),
    fax                VARCHAR(20),
    fax_description    VARCHAR(10),
    e_mail_address     VARCHAR(80),
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    print_name         VARCHAR(40) NOT NULL,
    title              VARCHAR(30),
    signature          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_agency (
    sb_agency_nbr                INTEGER NOT NULL,
    name                         VARCHAR(40) NOT NULL,
    address1                     VARCHAR(30),
    address2                     VARCHAR(30),
    city                         VARCHAR(20),
    state                        CHAR(2),
    zip_code                     VARCHAR(10),
    contact1                     VARCHAR(30),
    phone1                       VARCHAR(20),
    description1                 VARCHAR(10),
    contact2                     VARCHAR(30),
    phone2                       VARCHAR(20),
    description2                 VARCHAR(10),
    fax                          VARCHAR(20),
    fax_description              VARCHAR(10),
    e_mail_address               VARCHAR(80),
    agency_type                  CHAR(1) NOT NULL,
    sb_banks_nbr                 INTEGER,
    account_number               VARCHAR(20),
    account_type                 CHAR(1),
    negative_direct_dep_allowed  CHAR(1) NOT NULL,
    method                       CHAR(1),
    modem_number                 VARCHAR(20),
    transfer_protocol            CHAR(1),
    notes                        BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                       VARCHAR(512),
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    print_name                   VARCHAR(40) NOT NULL,
    county                       VARCHAR(20),
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    bulk_filer_number            VARCHAR(20)
);


CREATE TABLE xb_agency_reports (
    sb_agency_reports_nbr  INTEGER NOT NULL,
    sb_agency_nbr          INTEGER NOT NULL,
    sb_reports_nbr         INTEGER NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xb_bank_accounts (
    sb_bank_accounts_nbr         INTEGER NOT NULL,
    sb_banks_nbr                 INTEGER NOT NULL,
    custom_bank_account_number   VARCHAR(20) NOT NULL,
    bank_account_type            CHAR(1) NOT NULL,
    name_description             VARCHAR(40),
    ach_origin_sb_banks_nbr      INTEGER NOT NULL,
    check_form_default           CHAR(1),
    bank_returns                 CHAR(1) NOT NULL,
    custom_header_record         VARCHAR(80),
    next_available_check_number  INTEGER NOT NULL,
    end_check_number             INTEGER,
    next_begin_check_number      INTEGER,
    next_end_check_number        INTEGER,
    filler                       VARCHAR(512),
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    suppress_offset_account      CHAR(1) NOT NULL,
    block_negative_checks        CHAR(1) NOT NULL,
    block_trust_impound          CHAR(1) NOT NULL,
    block_tax_impound            CHAR(1) NOT NULL,
    block_billing_impound        CHAR(1) NOT NULL,
    batch_filer_id               VARCHAR(9),
    master_inquiry_pin           VARCHAR(10),
    logo_sb_blob_nbr             INTEGER,
    signature_sb_blob_nbr        INTEGER,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    block_debit_ach              CHAR(1) NOT NULL,
    beginning_balance            NUMERIC(18,6),
    operating_account            CHAR(1) NOT NULL,
    billing_account              CHAR(1) NOT NULL,
    ach_account                  CHAR(1) NOT NULL,
    trust_account                CHAR(1) NOT NULL,
    tax_account                  CHAR(1) NOT NULL,
    obc_account                  CHAR(1) NOT NULL,
    workers_comp_account         CHAR(1) NOT NULL,
    reccuring_wire_number        INTEGER,
    bank_check                   CHAR(1) NOT NULL
);


CREATE TABLE xb_banks (
    sb_banks_nbr                    INTEGER NOT NULL,
    name                            VARCHAR(40) NOT NULL,
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    contact1                        VARCHAR(30) NOT NULL,
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax                             VARCHAR(20),
    fax_description                 VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    aba_number                      CHAR(9) NOT NULL,
    top_aba_number                  VARCHAR(10) NOT NULL,
    bottom_aba_number               VARCHAR(10) NOT NULL,
    addenda                         VARCHAR(12) NOT NULL,
    check_template                  BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    use_check_template              CHAR(1) NOT NULL,
    micr_account_start_position     INTEGER,
    micr_check_number_start_positn  INTEGER,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    print_name                      VARCHAR(40) NOT NULL,
    branch_identifier               VARCHAR(9),
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    allow_hyphens                   CHAR(1) NOT NULL
);


CREATE TABLE xb_blob (
    sb_blob_nbr  INTEGER NOT NULL,
    data         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_company (
    sb_delivery_company_nbr      INTEGER NOT NULL,
    name                         VARCHAR(40) NOT NULL,
    delivery_contact             VARCHAR(40),
    delivery_contact_phone       VARCHAR(20),
    delivery_contact_phone_type  CHAR(1),
    supplies_contact             VARCHAR(40),
    supplies_contact_phone       VARCHAR(20),
    supplies_contact_phone_type  CHAR(1),
    web_site                     VARCHAR(40),
    delivery_company_notes       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_company_svcs (
    sb_delivery_company_svcs_nbr  INTEGER NOT NULL,
    sb_delivery_company_nbr       INTEGER NOT NULL,
    description                   VARCHAR(40),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    reference_fee                 NUMERIC(18,6)
);


CREATE TABLE xb_delivery_method (
    sb_delivery_method_nbr  INTEGER NOT NULL,
    sy_delivery_method_nbr  INTEGER NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_service (
    sb_delivery_service_nbr  INTEGER NOT NULL,
    sy_delivery_service_nbr  INTEGER NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_service_opt (
    sb_delivery_service_opt_nbr  INTEGER NOT NULL,
    sb_delivery_service_nbr      INTEGER NOT NULL,
    sb_delivery_method_nbr       INTEGER,
    option_tag                   VARCHAR(40) NOT NULL,
    option_integer_value         INTEGER,
    option_string_value          VARCHAR(512),
    effective_date               TIMESTAMP NOT NULL,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL
);


CREATE TABLE xb_enlist_groups (
    sb_enlist_groups_nbr  INTEGER NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    changed_by            INTEGER NOT NULL,
    active_record         CHAR(1) NOT NULL,
    sy_report_groups_nbr  INTEGER NOT NULL,
    media_type            CHAR(1) NOT NULL,
    process_type          CHAR(1) NOT NULL
);


CREATE TABLE xb_global_agency_contacts (
    sb_global_agency_contacts_nbr  INTEGER NOT NULL,
    sy_global_agency_nbr           INTEGER NOT NULL,
    contact_name                   VARCHAR(30) NOT NULL,
    phone                          VARCHAR(20),
    fax                            VARCHAR(20),
    e_mail_address                 VARCHAR(80),
    notes                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    tax_return_pin                 VARCHAR(20)
);


CREATE TABLE xb_holidays (
    sb_holidays_nbr  INTEGER NOT NULL,
    holiday_date     DATE NOT NULL,
    holiday_name     VARCHAR(40) NOT NULL,
    used_by          CHAR(1) NOT NULL,
    changed_by       INTEGER NOT NULL,
    creation_date    TIMESTAMP NOT NULL,
    effective_date   TIMESTAMP NOT NULL,
    active_record    CHAR(1) NOT NULL
);


CREATE TABLE xb_mail_box (
    sb_mail_box_nbr                 INTEGER NOT NULL,
    cl_nbr                          INTEGER,
    cl_mail_box_group_nbr           INTEGER,
    cost                            NUMERIC(18,6),
    required                        CHAR(1) NOT NULL,
    notification_email              VARCHAR(80),
    pr_nbr                          INTEGER,
    released_time                   TIMESTAMP,
    printed_time                    TIMESTAMP,
    sb_cover_letter_report_nbr      INTEGER,
    scanned_time                    TIMESTAMP,
    shipped_time                    TIMESTAMP,
    sy_cover_letter_report_nbr      INTEGER,
    note                            VARCHAR(512),
    up_level_mail_box_nbr           INTEGER,
    tracking_info                   VARCHAR(40),
    auto_release_type               CHAR(1) NOT NULL,
    barcode                         VARCHAR(40) NOT NULL,
    created_time                    TIMESTAMP NOT NULL,
    dispose_content_after_shipping  CHAR(1) NOT NULL,
    addressee                       VARCHAR(512) NOT NULL,
    description                     VARCHAR(40) NOT NULL,
    sb_delivery_method_nbr          INTEGER NOT NULL,
    sb_media_type_nbr               INTEGER NOT NULL,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    effective_date                  TIMESTAMP NOT NULL
);


CREATE TABLE xb_mail_box_content (
    sb_mail_box_content_nbr  INTEGER NOT NULL,
    sb_mail_box_nbr          INTEGER NOT NULL,
    description              VARCHAR(512) NOT NULL,
    file_name                VARCHAR(40) NOT NULL,
    media_type               CHAR(1) NOT NULL,
    page_count               INTEGER NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    sb_mail_box_job_nbr      INTEGER
);


CREATE TABLE xb_mail_box_option (
    sb_mail_box_option_nbr   INTEGER NOT NULL,
    sb_mail_box_nbr          INTEGER NOT NULL,
    sb_mail_box_content_nbr  INTEGER,
    option_tag               VARCHAR(80) NOT NULL,
    option_integer_value     INTEGER,
    option_string_value      VARCHAR(512),
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    sb_mail_box_job_nbr      INTEGER
);


CREATE TABLE xb_media_type (
    sb_media_type_nbr  INTEGER NOT NULL,
    sy_media_type_nbr  INTEGER NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_media_type_option (
    sb_media_type_option_nbr  INTEGER NOT NULL,
    sb_media_type_nbr         INTEGER NOT NULL,
    option_tag                VARCHAR(40) NOT NULL,
    option_integer_value      INTEGER,
    option_string_value       VARCHAR(512),
    effective_date            TIMESTAMP NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xb_multiclient_reports (
    sb_multiclient_reports_nbr  INTEGER NOT NULL,
    description                 VARCHAR(40) NOT NULL,
    report_level                CHAR(1) NOT NULL,
    report_nbr                  INTEGER NOT NULL,
    input_params                BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    effective_date              TIMESTAMP NOT NULL,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL
);


CREATE TABLE xb_option (
    sb_option_nbr         INTEGER NOT NULL,
    option_tag            VARCHAR(80) NOT NULL,
    option_integer_value  INTEGER,
    option_string_value   VARCHAR(512),
    effective_date        TIMESTAMP NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xb_other_service (
    sb_other_service_nbr  INTEGER NOT NULL,
    name                  VARCHAR(60) NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL,
    changed_by            INTEGER NOT NULL
);


CREATE TABLE xb_paper_info (
    sb_paper_info_nbr  INTEGER NOT NULL,
    description        VARCHAR(40) NOT NULL,
    height             NUMERIC(18,6) NOT NULL,
    width              NUMERIC(18,6) NOT NULL,
    weight             NUMERIC(18,6) NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL,
    media_type         CHAR(1) NOT NULL
);


CREATE TABLE xb_queue_priority (
    sb_queue_priority_nbr  INTEGER NOT NULL,
    package_id             INTEGER NOT NULL,
    method_name            VARCHAR(255) NOT NULL,
    priority               INTEGER NOT NULL,
    sb_user_nbr            INTEGER,
    sb_sec_groups_nbr      INTEGER,
    effective_date         TIMESTAMP NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xb_referrals (
    sb_referrals_nbr  INTEGER NOT NULL,
    name              VARCHAR(40) NOT NULL,
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL
);


CREATE TABLE xb_report_writer_reports (
    sb_report_writer_reports_nbr  INTEGER NOT NULL,
    report_description            VARCHAR(40) NOT NULL,
    report_type                   CHAR(1) NOT NULL,
    report_file                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    media_type                    CHAR(1) NOT NULL,
    class_name                    VARCHAR(40),
    ancestor_class_name           VARCHAR(40)
);


CREATE TABLE xb_reports (
    sb_reports_nbr             INTEGER NOT NULL,
    description                VARCHAR(40) NOT NULL,
    comments                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                 INTEGER NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    active_record              CHAR(1) NOT NULL,
    report_writer_reports_nbr  INTEGER NOT NULL,
    report_level               CHAR(1) NOT NULL,
    input_params               BLOB SUB_TYPE 0 SEGMENT SIZE 80
);


CREATE TABLE xb_sales_tax_states (
    sb_sales_tax_states_nbr  INTEGER NOT NULL,
    state                    CHAR(2) NOT NULL,
    state_tax_id             VARCHAR(19) NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    sales_tax_percentage     NUMERIC(18,6)
);


CREATE TABLE xb_sec_clients (
    sb_sec_clients_nbr  INTEGER NOT NULL,
    cl_nbr              INTEGER NOT NULL,
    sb_sec_groups_nbr   INTEGER,
    sb_user_nbr         INTEGER,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_group_members (
    sb_sec_group_members_nbr  INTEGER NOT NULL,
    sb_sec_groups_nbr         INTEGER NOT NULL,
    sb_user_nbr               INTEGER NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_groups (
    sb_sec_groups_nbr  INTEGER NOT NULL,
    name               VARCHAR(40) NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_rights (
    sb_sec_rights_nbr  INTEGER NOT NULL,
    sb_sec_groups_nbr  INTEGER,
    sb_user_nbr        INTEGER,
    tag                VARCHAR(128) NOT NULL,
    context            VARCHAR(128) NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_row_filters (
    sb_sec_row_filters_nbr  INTEGER NOT NULL,
    sb_sec_groups_nbr       INTEGER,
    sb_user_nbr             INTEGER,
    database_type           CHAR(1) NOT NULL,
    table_name              VARCHAR(40) NOT NULL,
    filter_type             CHAR(1) NOT NULL,
    custom_expr             VARCHAR(255),
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    cl_nbr                  INTEGER,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_templates (
    sb_sec_templates_nbr  INTEGER NOT NULL,
    name                  VARCHAR(40) NOT NULL,
    template              BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xb_services (
    sb_services_nbr                INTEGER NOT NULL,
    service_name                   VARCHAR(40) NOT NULL,
    frequency                      CHAR(1) NOT NULL,
    month_number                   VARCHAR(2),
    based_on_type                  CHAR(1) NOT NULL,
    next_based_on_type             CHAR(1),
    next_based_on_type_begin_date  TIMESTAMP,
    sb_reports_nbr                 INTEGER,
    commission                     CHAR(1) NOT NULL,
    sales_taxable                  CHAR(1) NOT NULL,
    filler                         VARCHAR(512),
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    product_code                   VARCHAR(6),
    week_number                    CHAR(1),
    service_type                   CHAR(1) NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    minimum_amount                 NUMERIC(18,6),
    maximum_amount                 NUMERIC(18,6),
    sb_delivery_method_nbr         INTEGER,
    tax_type                       CHAR(1) NOT NULL
);


CREATE TABLE xb_services_calculations (
    sb_services_calculations_nbr  INTEGER NOT NULL,
    sb_services_nbr               INTEGER NOT NULL,
    next_min_quantity_begin_date  TIMESTAMP,
    next_max_quantity_begin_date  TIMESTAMP,
    next_per_item_begin_date      TIMESTAMP,
    next_flat_amount_begin_date   TIMESTAMP,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    minimum_quantity              NUMERIC(18,6),
    next_minimum_quantity         NUMERIC(18,6),
    maximum_quantity              NUMERIC(18,6),
    next_maximum_quantity         NUMERIC(18,6),
    per_item_rate                 NUMERIC(18,6),
    next_per_item_rate            NUMERIC(18,6),
    flat_amount                   NUMERIC(18,6),
    next_flat_amount              NUMERIC(18,6)
);


CREATE TABLE xb_storage (
    sb_storage_nbr  INTEGER NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL,
    changed_by      INTEGER NOT NULL,
    tag             VARCHAR(20) NOT NULL,
    storage_data    BLOB SUB_TYPE 1 SEGMENT SIZE 80 NOT NULL
);


CREATE TABLE xb_task (
    sb_task_nbr     INTEGER NOT NULL,
    sb_user_nbr     INTEGER NOT NULL,
    schedule        VARCHAR(255) NOT NULL,
    description     VARCHAR(255) NOT NULL,
    task            BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    last_run        TIMESTAMP,
    effective_date  TIMESTAMP NOT NULL,
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL
);


CREATE TABLE xb_tax_payment (
    sb_tax_payment_nbr  INTEGER NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    changed_by          INTEGER NOT NULL,
    active_record       CHAR(1) NOT NULL,
    description         VARCHAR(80),
    status              CHAR(1) NOT NULL,
    status_date         TIMESTAMP
);


CREATE TABLE xb_team (
    sb_team_nbr       INTEGER NOT NULL,
    team_description  VARCHAR(40) NOT NULL,
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL,
    cr_category       CHAR(1) NOT NULL
);


CREATE TABLE xb_team_members (
    sb_team_members_nbr  INTEGER NOT NULL,
    sb_team_nbr          INTEGER NOT NULL,
    sb_user_nbr          INTEGER NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL
);


CREATE TABLE xb_user (
    sb_user_nbr           INTEGER NOT NULL,
    user_id               VARCHAR(128) NOT NULL,
    user_signature        BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    last_name             VARCHAR(30) NOT NULL,
    first_name            VARCHAR(20) NOT NULL,
    middle_initial        CHAR(1),
    active_user           CHAR(1) NOT NULL,
    department            CHAR(1) NOT NULL,
    password_change_date  TIMESTAMP NOT NULL,
    security_level        CHAR(1) NOT NULL,
    user_update_options   VARCHAR(512) NOT NULL,
    user_functions        VARCHAR(512) NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    user_password         VARCHAR(32) NOT NULL,
    email_address         VARCHAR(80),
    user_gui_settings     BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    cl_nbr                INTEGER,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL,
    sb_accountant_nbr     INTEGER,
    wrong_pswd_attempts   INTEGER,
    links_data            VARCHAR(128),
    login_question1       INTEGER,
    login_answer1         VARCHAR(80),
    login_question2       INTEGER,
    login_answer2         VARCHAR(80),
    login_question3       INTEGER,
    login_answer3         VARCHAR(80),
    hr_personnel          CHAR(1) NOT NULL,
    sec_question1         INTEGER,
    sec_answer1           VARCHAR(80),
    sec_question2         INTEGER,
    sec_answer2           VARCHAR(80)
);


CREATE TABLE xb_user_notice (
    sb_user_notice_nbr  INTEGER NOT NULL,
    sb_user_nbr         INTEGER NOT NULL,
    name                VARCHAR(128) NOT NULL,
    notes               BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    task                BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    last_dismiss        TIMESTAMP,
    next_reminder       TIMESTAMP,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);

COMMIT;


/* MOVE DATA INTO TEMP TABLES */


INSERT INTO xb_banks SELECT * FROM sb_banks;
COMMIT;
DROP TABLE sb_banks;
COMMIT;

INSERT INTO xb_blob (sb_blob_nbr, data, changed_by, creation_date, effective_date, active_record) SELECT sb_blob_nbr, data, 0, change_date, CAST('1/1/2000' AS TIMESTAMP), 'C' FROM sb_blob;
COMMIT;
DROP TABLE sb_blob;
COMMIT; 

INSERT INTO xb_delivery_company SELECT * FROM sb_delivery_company;
COMMIT;
DROP TABLE sb_delivery_company;
COMMIT;

INSERT INTO xb_delivery_company_svcs SELECT * FROM sb_delivery_company_svcs;
COMMIT;
DROP TABLE sb_delivery_company_svcs;
COMMIT;

INSERT INTO xb_delivery_method SELECT * FROM sb_delivery_method;
COMMIT;
DROP TABLE sb_delivery_method;
COMMIT;

INSERT INTO xb_delivery_service SELECT * FROM sb_delivery_service;
COMMIT;
DROP TABLE sb_delivery_service;
COMMIT;

INSERT INTO xb_delivery_service_opt SELECT * FROM sb_delivery_service_opt;
COMMIT;
DROP TABLE sb_delivery_service_opt;
COMMIT;

INSERT INTO xb_enlist_groups SELECT * FROM sb_enlist_groups;
COMMIT;
DROP TABLE sb_enlist_groups;
COMMIT;

INSERT INTO xb_global_agency_contacts SELECT * FROM sb_global_agency_contacts;
COMMIT;
DROP TABLE sb_global_agency_contacts;
COMMIT;

INSERT INTO xb_holidays SELECT * FROM sb_holidays;
COMMIT;
DROP TABLE sb_holidays;
COMMIT;

INSERT INTO xb_mail_box SELECT * FROM sb_mail_box;
COMMIT;
DROP TABLE sb_mail_box;
COMMIT;

INSERT INTO xb_mail_box_content SELECT * FROM sb_mail_box_content;
COMMIT;
DROP TABLE sb_mail_box_content;
COMMIT;

INSERT INTO xb_mail_box_option SELECT * FROM sb_mail_box_option;
COMMIT;
DROP TABLE sb_mail_box_option;
COMMIT;

INSERT INTO xb_media_type SELECT * FROM sb_media_type;
COMMIT;
DROP TABLE sb_media_type;
COMMIT;

INSERT INTO xb_media_type_option SELECT * FROM sb_media_type_option;
COMMIT;
DROP TABLE sb_media_type_option;
COMMIT;

INSERT INTO xb SELECT * FROM sb;
COMMIT;
DROP TABLE sb;
COMMIT;

INSERT INTO xb_accountant SELECT * FROM sb_accountant;
COMMIT;
DROP TABLE sb_accountant;
COMMIT;

INSERT INTO xb_agency SELECT * FROM sb_agency;
COMMIT;
DROP TABLE sb_agency;
COMMIT;

INSERT INTO xb_agency_reports SELECT * FROM sb_agency_reports;
COMMIT;
DROP TABLE sb_agency_reports;
COMMIT;

INSERT INTO xb_bank_accounts SELECT * FROM sb_bank_accounts;
COMMIT;
DROP TABLE sb_bank_accounts;
COMMIT;

INSERT INTO xb_multiclient_reports SELECT * FROM sb_multiclient_reports;
COMMIT;
DROP TABLE sb_multiclient_reports;
COMMIT;

INSERT INTO xb_option SELECT * FROM sb_option;
COMMIT;
DROP TABLE sb_option;
COMMIT;

INSERT INTO xb_other_service SELECT * FROM sb_other_service;
COMMIT;
DROP TABLE sb_other_service;
COMMIT;

INSERT INTO xb_paper_info SELECT * FROM sb_paper_info;
COMMIT;
DROP TABLE sb_paper_info;
COMMIT;

INSERT INTO xb_queue_priority SELECT * FROM sb_queue_priority;
COMMIT;
DROP TABLE sb_queue_priority;
COMMIT;

INSERT INTO xb_referrals SELECT * FROM sb_referrals;
COMMIT;
DROP TABLE sb_referrals;
COMMIT;

INSERT INTO xb_report_writer_reports SELECT * FROM sb_report_writer_reports;
COMMIT;
DROP TABLE sb_report_writer_reports;
COMMIT;

INSERT INTO xb_reports SELECT * FROM sb_reports;
COMMIT;
DROP TABLE sb_reports;
COMMIT;

INSERT INTO xb_sales_tax_states SELECT * FROM sb_sales_tax_states;
COMMIT;
DROP TABLE sb_sales_tax_states;
COMMIT;


INSERT INTO xb_sec_clients SELECT * FROM sb_sec_clients;
COMMIT;
DROP TABLE sb_sec_clients;
COMMIT;

INSERT INTO xb_sec_group_members SELECT * FROM sb_sec_group_members;
COMMIT;
DROP TABLE sb_sec_group_members;
COMMIT;

INSERT INTO xb_sec_groups SELECT * FROM sb_sec_groups;
COMMIT;
DROP TABLE sb_sec_groups;
COMMIT;

INSERT INTO xb_sec_rights SELECT * FROM sb_sec_rights;
COMMIT;
DROP TABLE sb_sec_rights;
COMMIT;

INSERT INTO xb_sec_row_filters SELECT * FROM sb_sec_row_filters;
COMMIT;
DROP TABLE sb_sec_row_filters;
COMMIT;

INSERT INTO xb_sec_templates SELECT * FROM sb_sec_templates;
COMMIT;
DROP TABLE sb_sec_templates;
COMMIT;

INSERT INTO xb_services SELECT * FROM sb_services;
COMMIT;
DROP TABLE sb_services;
COMMIT;

INSERT INTO xb_services_calculations SELECT * FROM sb_services_calculations;
COMMIT;
DROP TABLE sb_services_calculations;
COMMIT;

INSERT INTO xb_task 
  (sb_task_nbr, sb_user_nbr, schedule, description, task, effective_date, changed_by, creation_date, active_record) 
  SELECT sb_task_nbr, sb_user_nbr, schedule, description, task, effective_date, changed_by, creation_date, active_record FROM sb_task;
COMMIT;
DROP TABLE sb_task;
COMMIT;

INSERT INTO xb_storage SELECT * FROM sb_storage;
COMMIT;
DROP TABLE sb_storage;
COMMIT;


SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_task_nbr INTEGER;
DECLARE VARIABLE last_run TIMESTAMP;
BEGIN
  FOR SELECT sb_task_nbr, MAX(creation_date) last_run
      FROM sb_task_runs
      WHERE  active_record = 'C'
      GROUP BY sb_task_nbr
      INTO :sb_task_nbr, :last_run
  DO
  BEGIN
    UPDATE xb_task SET last_run = :last_run WHERE sb_task_nbr = :sb_task_nbr AND active_record = 'C';
  END
END^

SET TERM ;^

COMMIT;
DROP TABLE sb_task_runs;
COMMIT;

INSERT INTO xb_tax_payment SELECT * FROM sb_tax_payment;
COMMIT;
DROP TABLE sb_tax_payment;
COMMIT;

INSERT INTO xb_team SELECT * FROM sb_team;
COMMIT;
DROP TABLE sb_team;
COMMIT;

INSERT INTO xb_team_members SELECT * FROM sb_team_members;
COMMIT;
DROP TABLE sb_team_members;
COMMIT;

INSERT INTO xb_user SELECT * FROM sb_user;
COMMIT;
DROP TABLE sb_user;
COMMIT;

INSERT INTO xb_user_notice (sb_user_notice_nbr, sb_user_nbr, name, notes, task, last_dismiss, 
  next_reminder, changed_by, creation_date, effective_date, active_record) 
  SELECT sb_user_notice_nbr, sb_user_nbr, name, notes, task, last_dismiss, 
  next_reminder,
  0, CAST('1/1/2000' AS TIMESTAMP), CAST('1/1/2000' AS TIMESTAMP), 'C'  FROM sb_user_notice;
COMMIT;
DROP TABLE sb_user_notice;
COMMIT;


/* Create a new structure */
/*<progress>Creating new structure</progress>*/
/******************************************************************************/
/****                               Domains                                ****/
/******************************************************************************/

CREATE DOMAIN EV_AMOUNT AS
NUMERIC(18,6);

CREATE DOMAIN EV_BLOB_BIN AS
BLOB SUB_TYPE 0 SEGMENT SIZE 80;

CREATE DOMAIN EV_CHAR1 AS
CHAR(1);

CREATE DOMAIN EV_CHAR2 AS
CHAR(2);

CREATE DOMAIN EV_CHAR9 AS
CHAR(9);

CREATE DOMAIN EV_DATE AS
DATE;

CREATE DOMAIN EV_DATETIME AS
TIMESTAMP;

CREATE DOMAIN EV_INT AS
INTEGER;

CREATE DOMAIN EV_SINT AS
SMALLINT;

CREATE DOMAIN EV_STR10 AS
VARCHAR(10);

CREATE DOMAIN EV_STR12 AS
VARCHAR(12);

CREATE DOMAIN EV_STR128 AS
VARCHAR(128);

CREATE DOMAIN EV_STR16 AS
VARCHAR(16);

CREATE DOMAIN EV_STR19 AS
VARCHAR(19);

CREATE DOMAIN EV_STR2 AS
VARCHAR(2);

CREATE DOMAIN EV_STR20 AS
VARCHAR(20);

CREATE DOMAIN EV_STR200 AS
VARCHAR(200);

CREATE DOMAIN EV_STR255 AS
VARCHAR(255);

CREATE DOMAIN EV_STR3 AS
VARCHAR(3);

CREATE DOMAIN EV_STR30 AS
VARCHAR(30);

CREATE DOMAIN EV_STR32 AS
VARCHAR(32);

CREATE DOMAIN EV_STR40 AS
VARCHAR(40);

CREATE DOMAIN EV_STR512 AS
VARCHAR(512);

CREATE DOMAIN EV_STR6 AS
VARCHAR(6);

CREATE DOMAIN EV_STR60 AS
VARCHAR(60);

CREATE DOMAIN EV_STR64 AS
VARCHAR(64);

CREATE DOMAIN EV_STR80 AS
VARCHAR(80);

CREATE DOMAIN EV_STR9 AS
VARCHAR(9);



/******************************************************************************/
/****                              Generators                              ****/
/******************************************************************************/

CREATE GENERATOR G_CL;
SET GENERATOR G_CL TO 0;

CREATE GENERATOR G_EV_FIELD_CHANGE_BLOB;
SET GENERATOR G_EV_FIELD_CHANGE_BLOB TO 0;

CREATE GENERATOR G_EV_TABLE_CHANGE;
SET GENERATOR G_EV_TABLE_CHANGE TO 0;

CREATE GENERATOR G_EV_TRANSACTION;
SET GENERATOR G_EV_TRANSACTION TO 0;

CREATE GENERATOR G_EV_TRANSACTION_COMMIT;
SET GENERATOR G_EV_TRANSACTION_COMMIT TO 0;

CREATE GENERATOR G_NEXT_INVOICE_NUMBER;
SET GENERATOR G_NEXT_INVOICE_NUMBER TO 0;

CREATE GENERATOR G_SB;
SET GENERATOR G_SB TO 0;

CREATE GENERATOR G_SB_ACCOUNTANT;
SET GENERATOR G_SB_ACCOUNTANT TO 0;

CREATE GENERATOR G_SB_ACCOUNTANT_VER;
SET GENERATOR G_SB_ACCOUNTANT_VER TO 0;

CREATE GENERATOR G_SB_AGENCY;
SET GENERATOR G_SB_AGENCY TO 0;

CREATE GENERATOR G_SB_AGENCY_REPORTS;
SET GENERATOR G_SB_AGENCY_REPORTS TO 0;

CREATE GENERATOR G_SB_AGENCY_REPORTS_VER;
SET GENERATOR G_SB_AGENCY_REPORTS_VER TO 0;

CREATE GENERATOR G_SB_AGENCY_VER;
SET GENERATOR G_SB_AGENCY_VER TO 0;

CREATE GENERATOR G_SB_BANKS;
SET GENERATOR G_SB_BANKS TO 0;

CREATE GENERATOR G_SB_BANKS_VER;
SET GENERATOR G_SB_BANKS_VER TO 0;

CREATE GENERATOR G_SB_BANK_ACCOUNTS;
SET GENERATOR G_SB_BANK_ACCOUNTS TO 0;

CREATE GENERATOR G_SB_BANK_ACCOUNTS_VER;
SET GENERATOR G_SB_BANK_ACCOUNTS_VER TO 0;

CREATE GENERATOR G_SB_BLOB;
SET GENERATOR G_SB_BLOB TO 0;

CREATE GENERATOR G_SB_BLOB_VER;
SET GENERATOR G_SB_BLOB_VER TO 0;

CREATE GENERATOR G_SB_DELIVERY_COMPANY;
SET GENERATOR G_SB_DELIVERY_COMPANY TO 0;

CREATE GENERATOR G_SB_DELIVERY_COMPANY_SVCS;
SET GENERATOR G_SB_DELIVERY_COMPANY_SVCS TO 0;

CREATE GENERATOR G_SB_DELIVERY_COMPANY_SVCS_VER;
SET GENERATOR G_SB_DELIVERY_COMPANY_SVCS_VER TO 0;

CREATE GENERATOR G_SB_DELIVERY_COMPANY_VER;
SET GENERATOR G_SB_DELIVERY_COMPANY_VER TO 0;

CREATE GENERATOR G_SB_DELIVERY_METHOD;
SET GENERATOR G_SB_DELIVERY_METHOD TO 0;

CREATE GENERATOR G_SB_DELIVERY_METHOD_VER;
SET GENERATOR G_SB_DELIVERY_METHOD_VER TO 0;

CREATE GENERATOR G_SB_DELIVERY_SERVICE;
SET GENERATOR G_SB_DELIVERY_SERVICE TO 0;

CREATE GENERATOR G_SB_DELIVERY_SERVICE_OPT;
SET GENERATOR G_SB_DELIVERY_SERVICE_OPT TO 0;

CREATE GENERATOR G_SB_DELIVERY_SERVICE_OPT_VER;
SET GENERATOR G_SB_DELIVERY_SERVICE_OPT_VER TO 0;

CREATE GENERATOR G_SB_DELIVERY_SERVICE_VER;
SET GENERATOR G_SB_DELIVERY_SERVICE_VER TO 0;

CREATE GENERATOR G_SB_ENLIST_GROUPS;
SET GENERATOR G_SB_ENLIST_GROUPS TO 0;

CREATE GENERATOR G_SB_ENLIST_GROUPS_VER;
SET GENERATOR G_SB_ENLIST_GROUPS_VER TO 0;

CREATE GENERATOR G_SB_GLOBAL_AGENCY_CONTACTS;
SET GENERATOR G_SB_GLOBAL_AGENCY_CONTACTS TO 0;

CREATE GENERATOR G_SB_GLOBAL_AGENCY_CONTACTS_VER;
SET GENERATOR G_SB_GLOBAL_AGENCY_CONTACTS_VER TO 0;

CREATE GENERATOR G_SB_HOLIDAYS;
SET GENERATOR G_SB_HOLIDAYS TO 0;

CREATE GENERATOR G_SB_HOLIDAYS_VER;
SET GENERATOR G_SB_HOLIDAYS_VER TO 0;

CREATE GENERATOR G_SB_MAIL_BOX;
SET GENERATOR G_SB_MAIL_BOX TO 0;

CREATE GENERATOR G_SB_MAIL_BOX_CONTENT;
SET GENERATOR G_SB_MAIL_BOX_CONTENT TO 0;

CREATE GENERATOR G_SB_MAIL_BOX_CONTENT_VER;
SET GENERATOR G_SB_MAIL_BOX_CONTENT_VER TO 0;

CREATE GENERATOR G_SB_MAIL_BOX_OPTION;
SET GENERATOR G_SB_MAIL_BOX_OPTION TO 0;

CREATE GENERATOR G_SB_MAIL_BOX_OPTION_VER;
SET GENERATOR G_SB_MAIL_BOX_OPTION_VER TO 0;

CREATE GENERATOR G_SB_MAIL_BOX_VER;
SET GENERATOR G_SB_MAIL_BOX_VER TO 0;

CREATE GENERATOR G_SB_MEDIA_TYPE;
SET GENERATOR G_SB_MEDIA_TYPE TO 0;

CREATE GENERATOR G_SB_MEDIA_TYPE_OPTION;
SET GENERATOR G_SB_MEDIA_TYPE_OPTION TO 0;

CREATE GENERATOR G_SB_MEDIA_TYPE_OPTION_VER;
SET GENERATOR G_SB_MEDIA_TYPE_OPTION_VER TO 0;

CREATE GENERATOR G_SB_MEDIA_TYPE_VER;
SET GENERATOR G_SB_MEDIA_TYPE_VER TO 0;

CREATE GENERATOR G_SB_MULTICLIENT_REPORTS;
SET GENERATOR G_SB_MULTICLIENT_REPORTS TO 0;

CREATE GENERATOR G_SB_MULTICLIENT_REPORTS_VER;
SET GENERATOR G_SB_MULTICLIENT_REPORTS_VER TO 0;

CREATE GENERATOR G_SB_OPTION;
SET GENERATOR G_SB_OPTION TO 0;

CREATE GENERATOR G_SB_OPTION_VER;
SET GENERATOR G_SB_OPTION_VER TO 0;

CREATE GENERATOR G_SB_OTHER_SERVICE;
SET GENERATOR G_SB_OTHER_SERVICE TO 0;

CREATE GENERATOR G_SB_OTHER_SERVICE_VER;
SET GENERATOR G_SB_OTHER_SERVICE_VER TO 0;

CREATE GENERATOR G_SB_PAPER_INFO;
SET GENERATOR G_SB_PAPER_INFO TO 0;

CREATE GENERATOR G_SB_PAPER_INFO_VER;
SET GENERATOR G_SB_PAPER_INFO_VER TO 0;

CREATE GENERATOR G_SB_QUEUE_PRIORITY;
SET GENERATOR G_SB_QUEUE_PRIORITY TO 0;

CREATE GENERATOR G_SB_QUEUE_PRIORITY_VER;
SET GENERATOR G_SB_QUEUE_PRIORITY_VER TO 0;

CREATE GENERATOR G_SB_REFERRALS;
SET GENERATOR G_SB_REFERRALS TO 0;

CREATE GENERATOR G_SB_REFERRALS_VER;
SET GENERATOR G_SB_REFERRALS_VER TO 0;

CREATE GENERATOR G_SB_REPORTS;
SET GENERATOR G_SB_REPORTS TO 0;

CREATE GENERATOR G_SB_REPORTS_VER;
SET GENERATOR G_SB_REPORTS_VER TO 0;

CREATE GENERATOR G_SB_REPORT_GROUPS;
SET GENERATOR G_SB_REPORT_GROUPS TO 0;

CREATE GENERATOR G_SB_REPORT_GROUPS_VER;
SET GENERATOR G_SB_REPORT_GROUPS_VER TO 0;

CREATE GENERATOR G_SB_REPORT_GROUP_MEMBERS;
SET GENERATOR G_SB_REPORT_GROUP_MEMBERS TO 0;

CREATE GENERATOR G_SB_REPORT_GROUP_MEMBERS_VER;
SET GENERATOR G_SB_REPORT_GROUP_MEMBERS_VER TO 0;

CREATE GENERATOR G_SB_REPORT_WRITER_REPORTS;
SET GENERATOR G_SB_REPORT_WRITER_REPORTS TO 0;

CREATE GENERATOR G_SB_REPORT_WRITER_REPORTS_VER;
SET GENERATOR G_SB_REPORT_WRITER_REPORTS_VER TO 0;

CREATE GENERATOR G_SB_SALES_TAX_STATES;
SET GENERATOR G_SB_SALES_TAX_STATES TO 0;

CREATE GENERATOR G_SB_SALES_TAX_STATES_VER;
SET GENERATOR G_SB_SALES_TAX_STATES_VER TO 0;

CREATE GENERATOR G_SB_SEC_CLIENTS;
SET GENERATOR G_SB_SEC_CLIENTS TO 0;

CREATE GENERATOR G_SB_SEC_CLIENTS_VER;
SET GENERATOR G_SB_SEC_CLIENTS_VER TO 0;

CREATE GENERATOR G_SB_SEC_GROUPS;
SET GENERATOR G_SB_SEC_GROUPS TO 0;

CREATE GENERATOR G_SB_SEC_GROUPS_VER;
SET GENERATOR G_SB_SEC_GROUPS_VER TO 0;

CREATE GENERATOR G_SB_SEC_GROUP_MEMBERS;
SET GENERATOR G_SB_SEC_GROUP_MEMBERS TO 0;

CREATE GENERATOR G_SB_SEC_GROUP_MEMBERS_VER;
SET GENERATOR G_SB_SEC_GROUP_MEMBERS_VER TO 0;

CREATE GENERATOR G_SB_SEC_RIGHTS;
SET GENERATOR G_SB_SEC_RIGHTS TO 0;

CREATE GENERATOR G_SB_SEC_RIGHTS_VER;
SET GENERATOR G_SB_SEC_RIGHTS_VER TO 0;

CREATE GENERATOR G_SB_SEC_ROW_FILTERS;
SET GENERATOR G_SB_SEC_ROW_FILTERS TO 0;

CREATE GENERATOR G_SB_SEC_ROW_FILTERS_VER;
SET GENERATOR G_SB_SEC_ROW_FILTERS_VER TO 0;

CREATE GENERATOR G_SB_SEC_TEMPLATES;
SET GENERATOR G_SB_SEC_TEMPLATES TO 0;

CREATE GENERATOR G_SB_SEC_TEMPLATES_VER;
SET GENERATOR G_SB_SEC_TEMPLATES_VER TO 0;

CREATE GENERATOR G_SB_SERVICES;
SET GENERATOR G_SB_SERVICES TO 0;

CREATE GENERATOR G_SB_SERVICES_CALCULATIONS;
SET GENERATOR G_SB_SERVICES_CALCULATIONS TO 0;

CREATE GENERATOR G_SB_SERVICES_CALCULATIONS_VER;
SET GENERATOR G_SB_SERVICES_CALCULATIONS_VER TO 0;

CREATE GENERATOR G_SB_SERVICES_VER;
SET GENERATOR G_SB_SERVICES_VER TO 0;

CREATE GENERATOR G_SB_STORAGE;
SET GENERATOR G_SB_STORAGE TO 0;

CREATE GENERATOR G_SB_STORAGE_VER;
SET GENERATOR G_SB_STORAGE_VER TO 0;

CREATE GENERATOR G_SB_TASK;
SET GENERATOR G_SB_TASK TO 0;

CREATE GENERATOR G_SB_TASK_VER;
SET GENERATOR G_SB_TASK_VER TO 0;

CREATE GENERATOR G_SB_TAX_PAYMENT;
SET GENERATOR G_SB_TAX_PAYMENT TO 0;

CREATE GENERATOR G_SB_TAX_PAYMENT_VER;
SET GENERATOR G_SB_TAX_PAYMENT_VER TO 0;

CREATE GENERATOR G_SB_TEAM;
SET GENERATOR G_SB_TEAM TO 0;

CREATE GENERATOR G_SB_TEAM_MEMBERS;
SET GENERATOR G_SB_TEAM_MEMBERS TO 0;

CREATE GENERATOR G_SB_TEAM_MEMBERS_VER;
SET GENERATOR G_SB_TEAM_MEMBERS_VER TO 0;

CREATE GENERATOR G_SB_TEAM_VER;
SET GENERATOR G_SB_TEAM_VER TO 0;

CREATE GENERATOR G_SB_USER;
SET GENERATOR G_SB_USER TO 0;

CREATE GENERATOR G_SB_USER_NOTICE;
SET GENERATOR G_SB_USER_NOTICE TO 0;

CREATE GENERATOR G_SB_USER_NOTICE_VER;
SET GENERATOR G_SB_USER_NOTICE_VER TO 0;

CREATE GENERATOR G_SB_USER_VER;
SET GENERATOR G_SB_USER_VER TO 0;

CREATE GENERATOR G_SB_VER;
SET GENERATOR G_SB_VER TO 0;



/******************************************************************************/
/****                              Exceptions                              ****/
/******************************************************************************/

CREATE EXCEPTION EX_ERROR '';



SET TERM ^ ; 



/******************************************************************************/
/****                          Stored Procedures                           ****/
/******************************************************************************/

CREATE PROCEDURE ALL_CL_NUMBERS (
    DB_PATH VARCHAR(255))
RETURNS (
    CL_NBR INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE CHECK_INTEGRITY
RETURNS (
    PARENT_TABLE VARCHAR(64),
    PARENT_NBR INTEGER,
    CHILD_TABLE VARCHAR(64),
    CHILD_NBR INTEGER,
    CHILD_FIELD VARCHAR(64))
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE CHECK_TABLE_INTEGRITY (
    CHILD_TABLE VARCHAR(64),
    PARENT_TABLE VARCHAR(64),
    CHILD_FIELD VARCHAR(64))
RETURNS (
    CHILD_NBR INTEGER,
    INVALID_PARENT_NBR INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE CL_COPY (
    DB_PATH VARCHAR(255),
    CLIENT_NAME VARCHAR(255))
RETURNS (
    ERROR_TYPE INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE CL_NBR
RETURNS (
    NUMBER INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE DEL_SB (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_ACCOUNTANT (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_AGENCY (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_AGENCY_REPORTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_BANK_ACCOUNTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_BANKS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_BLOB (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_DELIVERY_COMPANY (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_DELIVERY_COMPANY_SVCS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_DELIVERY_METHOD (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_DELIVERY_SERVICE (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_DELIVERY_SERVICE_OPT (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_ENLIST_GROUPS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_GLOBAL_AGENCY_CONTACTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_HOLIDAYS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_MAIL_BOX (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_MAIL_BOX_CONTENT (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_MAIL_BOX_OPTION (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_MEDIA_TYPE (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_MEDIA_TYPE_OPTION (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_MULTICLIENT_REPORTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_OPTION (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_OTHER_SERVICE (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_PAPER_INFO (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_QUEUE_PRIORITY (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_REFERRALS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_REPORT_WRITER_REPORTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_REPORTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SALES_TAX_STATES (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SEC_CLIENTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SEC_GROUP_MEMBERS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SEC_GROUPS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SEC_RIGHTS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SEC_ROW_FILTERS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SEC_TEMPLATES (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SERVICES (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_SERVICES_CALCULATIONS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_STORAGE (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_TASK (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_TAX_PAYMENT (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_TEAM (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_TEAM_MEMBERS (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_USER (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DEL_SB_USER_NOTICE (
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DO_AFTER_START_TRANSACTION (
    USER_ID INTEGER,
    START_TIME TIMESTAMP)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE DO_BEFORE_COMMIT_TRANSACTION (
    COMMIT_TIME TIMESTAMP)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE EV_AUDIT_DEL (
    CUT_OFF_DATE TIMESTAMP)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE LIST_TOP_MAIL_BOXES
RETURNS (
    NBR INTEGER,
    UP_NBR INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE LIST_TOP_MAIL_BOXES2
RETURNS (
    NBR INTEGER,
    UP_NBR INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE PROCEDURE PACK_ALL
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE RAISE_CHILDREN_EXIST (
    PARENT_TABLE VARCHAR(64),
    PARENT_NBR INTEGER,
    CHILD_TABLE VARCHAR(64),
    PERIOD_BEGIN DATE,
    PERIOD_END DATE)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE RAISE_DUPLICATE_RECORD (
    TABLE_NAME VARCHAR(64),
    FIELDS VARCHAR(512),
    FIELD_VALUES VARCHAR(512),
    EFFECTIVE_DATE DATE,
    EFFECTIVE_UNTIL DATE)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE RAISE_ERROR (
    ERROR_CODE VARCHAR(40),
    ERROR_TEXT VARCHAR(1024))
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE RAISE_ONE_VERSION_ONLY (
    TABLE_NAME VARCHAR(64),
    NBR INTEGER)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE RAISE_PARENT_NOT_EXIST (
    CHILD_TABLE VARCHAR(64),
    CHILD_NBR INTEGER,
    CHILD_FIELD VARCHAR(64),
    CHILD_FIELD_NBR INTEGER,
    PARENT_TABLE VARCHAR(64),
    AS_OF_DATE DATE)
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE RAISE_TABLE_ERROR (
    TABLE_NAME VARCHAR(64),
    REC_VERSION INTEGER,
    FIELD_NAME VARCHAR(64),
    ERROR_CODE VARCHAR(40),
    ERROR_TEXT VARCHAR(1024))
AS
BEGIN
  EXIT;
END^





CREATE PROCEDURE UPDATE_INDEX_STATISTICS
AS
BEGIN
  EXIT;
END^






SET TERM ; ^



/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/



CREATE TABLE EV_DATABASE (
    "VERSION"    EV_STR16 NOT NULL,
    DESCRIPTION  EV_STR128
);


CREATE TABLE EV_FIELD (
    NBR           EV_SINT NOT NULL,
    EV_TABLE_NBR  EV_SINT NOT NULL,
    NAME          EV_STR64 NOT NULL,
    FIELD_TYPE    EV_CHAR1 NOT NULL,
    LEN           EV_SINT,
    SCALE         EV_SINT,
    REQUIRED      EV_CHAR1 NOT NULL,
    VERSIONED     EV_CHAR1 NOT NULL
);


CREATE TABLE EV_FIELD_CHANGE (
    EV_TABLE_CHANGE_NBR  EV_INT NOT NULL,
    EV_FIELD_NBR         EV_SINT NOT NULL,
    OLD_VALUE            EV_STR512
);


CREATE TABLE EV_FIELD_CHANGE_BLOB (
    NBR   EV_INT NOT NULL,
    DATA  EV_BLOB_BIN NOT NULL
);


CREATE TABLE EV_TABLE (
    NBR        EV_SINT NOT NULL,
    NAME       EV_STR64 NOT NULL,
    VERSIONED  EV_CHAR1 NOT NULL
);


CREATE TABLE EV_TABLE_CHANGE (
    NBR                 EV_INT NOT NULL,
    EV_TABLE_NBR        EV_SINT NOT NULL,
    EV_TRANSACTION_NBR  EV_INT NOT NULL,
    RECORD_ID           EV_INT NOT NULL,
    RECORD_NBR          EV_INT NOT NULL,
    CHANGE_TYPE         EV_CHAR1 NOT NULL
);


CREATE TABLE EV_TRANSACTION (
    NBR          EV_INT NOT NULL,
    COMMIT_NBR   EV_INT NOT NULL,
    START_TIME   EV_DATETIME NOT NULL,
    COMMIT_TIME  EV_DATETIME NOT NULL,
    USER_ID      EV_INT NOT NULL
);


CREATE TABLE SB (
    REC_VERSION                     EV_INT NOT NULL,
    SB_NBR                          EV_INT NOT NULL,
    EFFECTIVE_DATE                  EV_DATE NOT NULL,
    EFFECTIVE_UNTIL                 EV_DATE NOT NULL,
    SB_NAME                         EV_STR60 NOT NULL,
    ADDRESS1                        EV_STR30 NOT NULL,
    ADDRESS2                        EV_STR30,
    CITY                            EV_STR20 NOT NULL,
    STATE                           EV_CHAR2 NOT NULL,
    ZIP_CODE                        EV_STR10 NOT NULL,
    E_MAIL_ADDRESS                  EV_STR80,
    PARENT_SB_MODEM_NUMBER          EV_STR20,
    DEVELOPMENT_MODEM_NUMBER        EV_STR20 NOT NULL,
    DEVELOPMENT_FTP_PASSWORD        EV_STR128,
    EIN_NUMBER                      EV_STR9 NOT NULL,
    EFTPS_TIN_NUMBER                EV_STR9,
    EFTPS_BANK_FORMAT               EV_CHAR1 NOT NULL,
    USE_PRENOTE                     EV_CHAR1 NOT NULL,
    IMPOUND_TRUST_MONIES_AS_RECEIV  EV_CHAR1 NOT NULL,
    PAY_TAX_FROM_PAYABLES           EV_CHAR1 NOT NULL,
    AR_EXPORT_FORMAT                EV_CHAR1 NOT NULL,
    DEFAULT_CHECK_FORMAT            EV_CHAR1 NOT NULL,
    MICR_FONT                       EV_CHAR1 NOT NULL,
    MICR_HORIZONTAL_ADJUSTMENT      EV_INT NOT NULL,
    AUTO_SAVE_MINUTES               EV_INT NOT NULL,
    PHONE                           EV_STR20 NOT NULL,
    FAX                             EV_STR20 NOT NULL,
    COVER_LETTER_NOTES              EV_BLOB_BIN,
    INVOICE_NOTES                   EV_BLOB_BIN,
    TAX_COVER_LETTER_NOTES          EV_BLOB_BIN,
    AR_IMPORT_DIRECTORY             EV_STR80,
    ACH_DIRECTORY                   EV_STR80,
    SB_URL                          EV_STR80,
    DAYS_IN_PRENOTE                 EV_INT,
    SB_LOGO                         EV_BLOB_BIN,
    USER_PASSWORD_DURATION_IN_DAYS  EV_AMOUNT,
    DUMMY_TAX_CL_NBR                EV_INT,
    ERROR_SCREEN                    EV_CHAR1 NOT NULL,
    PSWD_MIN_LENGTH                 EV_INT,
    PSWD_FORCE_MIXED                EV_CHAR1 NOT NULL,
    MISC_CHECK_FORM                 EV_CHAR1 NOT NULL,
    VMR_CONFIDENCIAL_NOTES          EV_BLOB_BIN,
    MARK_LIABS_PAID_DEFAULT         EV_CHAR1 NOT NULL,
    TRUST_IMPOUND                   EV_CHAR1 NOT NULL,
    TAX_IMPOUND                     EV_CHAR1 NOT NULL,
    DD_IMPOUND                      EV_CHAR1 NOT NULL,
    BILLING_IMPOUND                 EV_CHAR1 NOT NULL,
    WC_IMPOUND                      EV_CHAR1 NOT NULL,
    DAYS_PRIOR_TO_CHECK_DATE        EV_INT,
    SB_EXCEPTION_PAYMENT_TYPE       EV_CHAR1 NOT NULL,
    SB_MAX_ACH_FILE_TOTAL           EV_AMOUNT,
    SB_ACH_FILE_LIMITATIONS         EV_CHAR1 NOT NULL,
    SB_CL_NBR                       EV_INT,
    DASHBOARD_MSG                   EV_STR200,
    EE_LOGIN_TYPE                   EV_CHAR1 NOT NULL,
    ESS_TERMS_OF_USE                EV_BLOB_BIN,
    WC_TERMS_OF_USE                 EV_BLOB_BIN
);


CREATE TABLE SB_ACCOUNTANT (
    REC_VERSION        EV_INT NOT NULL,
    SB_ACCOUNTANT_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE     EV_DATE NOT NULL,
    EFFECTIVE_UNTIL    EV_DATE NOT NULL,
    NAME               EV_STR40 NOT NULL,
    ADDRESS1           EV_STR30,
    ADDRESS2           EV_STR30,
    CITY               EV_STR20,
    STATE              EV_STR3,
    ZIP_CODE           EV_STR10,
    CONTACT1           EV_STR30 NOT NULL,
    PHONE1             EV_STR20,
    DESCRIPTION1       EV_STR10,
    CONTACT2           EV_STR30,
    PHONE2             EV_STR20,
    DESCRIPTION2       EV_STR10,
    FAX                EV_STR20,
    FAX_DESCRIPTION    EV_STR10,
    E_MAIL_ADDRESS     EV_STR80,
    PRINT_NAME         EV_STR40 NOT NULL,
    TITLE              EV_STR30,
    SIGNATURE          EV_BLOB_BIN
);


CREATE TABLE SB_AGENCY (
    REC_VERSION                  EV_INT NOT NULL,
    SB_AGENCY_NBR                EV_INT NOT NULL,
    EFFECTIVE_DATE               EV_DATE NOT NULL,
    EFFECTIVE_UNTIL              EV_DATE NOT NULL,
    NAME                         EV_STR40 NOT NULL,
    ADDRESS1                     EV_STR30,
    ADDRESS2                     EV_STR30,
    CITY                         EV_STR20,
    STATE                        EV_CHAR2,
    ZIP_CODE                     EV_STR10,
    CONTACT1                     EV_STR30,
    PHONE1                       EV_STR20,
    DESCRIPTION1                 EV_STR10,
    CONTACT2                     EV_STR30,
    PHONE2                       EV_STR20,
    DESCRIPTION2                 EV_STR10,
    FAX                          EV_STR20,
    FAX_DESCRIPTION              EV_STR10,
    E_MAIL_ADDRESS               EV_STR80,
    AGENCY_TYPE                  EV_CHAR1 NOT NULL,
    SB_BANKS_NBR                 EV_INT,
    ACCOUNT_NUMBER               EV_STR20,
    ACCOUNT_TYPE                 EV_CHAR1 NOT NULL,
    NEGATIVE_DIRECT_DEP_ALLOWED  EV_CHAR1 NOT NULL,
    MODEM_NUMBER                 EV_STR20,
    NOTES                        EV_BLOB_BIN,
    FILLER                       EV_STR512,
    PRINT_NAME                   EV_STR40 NOT NULL,
    COUNTY                       EV_STR20
);


CREATE TABLE SB_AGENCY_REPORTS (
    REC_VERSION            EV_INT NOT NULL,
    SB_AGENCY_REPORTS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE         EV_DATE NOT NULL,
    EFFECTIVE_UNTIL        EV_DATE NOT NULL,
    SB_AGENCY_NBR          EV_INT NOT NULL,
    SB_REPORTS_NBR         EV_INT NOT NULL
);


CREATE TABLE SB_BANK_ACCOUNTS (
    REC_VERSION                  EV_INT NOT NULL,
    SB_BANK_ACCOUNTS_NBR         EV_INT NOT NULL,
    EFFECTIVE_DATE               EV_DATE NOT NULL,
    EFFECTIVE_UNTIL              EV_DATE NOT NULL,
    SB_BANKS_NBR                 EV_INT NOT NULL,
    CUSTOM_BANK_ACCOUNT_NUMBER   EV_STR20 NOT NULL,
    BANK_ACCOUNT_TYPE            EV_CHAR1 NOT NULL,
    NAME_DESCRIPTION             EV_STR40,
    ACH_ORIGIN_SB_BANKS_NBR      EV_INT NOT NULL,
    BANK_RETURNS                 EV_CHAR1 NOT NULL,
    CUSTOM_HEADER_RECORD         EV_STR80,
    NEXT_AVAILABLE_CHECK_NUMBER  EV_INT NOT NULL,
    END_CHECK_NUMBER             EV_INT,
    NEXT_BEGIN_CHECK_NUMBER      EV_INT,
    NEXT_END_CHECK_NUMBER        EV_INT,
    FILLER                       EV_STR512,
    SUPPRESS_OFFSET_ACCOUNT      EV_CHAR1 NOT NULL,
    BLOCK_NEGATIVE_CHECKS        EV_CHAR1 NOT NULL,
    BATCH_FILER_ID               EV_STR9,
    MASTER_INQUIRY_PIN           EV_STR10,
    LOGO_SB_BLOB_NBR             EV_INT,
    SIGNATURE_SB_BLOB_NBR        EV_INT,
    BEGINNING_BALANCE            EV_AMOUNT,
    OPERATING_ACCOUNT            EV_CHAR1 NOT NULL,
    BILLING_ACCOUNT              EV_CHAR1 NOT NULL,
    ACH_ACCOUNT                  EV_CHAR1 NOT NULL,
    TRUST_ACCOUNT                EV_CHAR1 NOT NULL,
    TAX_ACCOUNT                  EV_CHAR1 NOT NULL,
    OBC_ACCOUNT                  EV_CHAR1 NOT NULL,
    WORKERS_COMP_ACCOUNT         EV_CHAR1 NOT NULL,
    RECCURING_WIRE_NUMBER        EV_INT,
    BANK_CHECK                   EV_CHAR1 NOT NULL
);


CREATE TABLE SB_BANKS (
    REC_VERSION                     EV_INT NOT NULL,
    SB_BANKS_NBR                    EV_INT NOT NULL,
    EFFECTIVE_DATE                  EV_DATE NOT NULL,
    EFFECTIVE_UNTIL                 EV_DATE NOT NULL,
    NAME                            EV_STR40 NOT NULL,
    ADDRESS1                        EV_STR30,
    ADDRESS2                        EV_STR30,
    CITY                            EV_STR20,
    STATE                           EV_CHAR2,
    ZIP_CODE                        EV_STR10,
    CONTACT1                        EV_STR30 NOT NULL,
    PHONE1                          EV_STR20,
    DESCRIPTION1                    EV_STR10,
    CONTACT2                        EV_STR30,
    PHONE2                          EV_STR20,
    DESCRIPTION2                    EV_STR10,
    FAX                             EV_STR20,
    FAX_DESCRIPTION                 EV_STR10,
    E_MAIL_ADDRESS                  EV_STR80,
    ABA_NUMBER                      EV_CHAR9 NOT NULL,
    TOP_ABA_NUMBER                  EV_STR10 NOT NULL,
    BOTTOM_ABA_NUMBER               EV_STR10 NOT NULL,
    ADDENDA                         EV_STR12 NOT NULL,
    CHECK_TEMPLATE                  EV_BLOB_BIN NOT NULL,
    USE_CHECK_TEMPLATE              EV_CHAR1 NOT NULL,
    MICR_ACCOUNT_START_POSITION     EV_INT,
    MICR_CHECK_NUMBER_START_POSITN  EV_INT,
    FILLER                          EV_STR512,
    PRINT_NAME                      EV_STR40 NOT NULL,
    BRANCH_IDENTIFIER               EV_STR9,
    ALLOW_HYPHENS                   EV_CHAR1 NOT NULL
);


CREATE TABLE SB_BLOB (
    REC_VERSION      EV_INT NOT NULL,
    EFFECTIVE_DATE   EV_DATE NOT NULL,
    SB_BLOB_NBR      EV_INT NOT NULL,
    EFFECTIVE_UNTIL  EV_DATE NOT NULL,
    DATA             EV_BLOB_BIN
);


CREATE TABLE SB_DELIVERY_COMPANY (
    REC_VERSION                  EV_INT NOT NULL,
    SB_DELIVERY_COMPANY_NBR      EV_INT NOT NULL,
    EFFECTIVE_DATE               EV_DATE NOT NULL,
    EFFECTIVE_UNTIL              EV_DATE NOT NULL,
    NAME                         EV_STR40 NOT NULL,
    DELIVERY_CONTACT             EV_STR40,
    DELIVERY_CONTACT_PHONE       EV_STR20,
    DELIVERY_CONTACT_PHONE_TYPE  EV_CHAR1 NOT NULL,
    SUPPLIES_CONTACT             EV_STR40,
    SUPPLIES_CONTACT_PHONE       EV_STR20,
    SUPPLIES_CONTACT_PHONE_TYPE  EV_CHAR1 NOT NULL,
    WEB_SITE                     EV_STR40,
    DELIVERY_COMPANY_NOTES       EV_BLOB_BIN
);


CREATE TABLE SB_DELIVERY_COMPANY_SVCS (
    REC_VERSION                   EV_INT NOT NULL,
    SB_DELIVERY_COMPANY_SVCS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE                EV_DATE NOT NULL,
    EFFECTIVE_UNTIL               EV_DATE NOT NULL,
    SB_DELIVERY_COMPANY_NBR       EV_INT NOT NULL,
    DESCRIPTION                   EV_STR40 NOT NULL,
    REFERENCE_FEE                 EV_AMOUNT
);


CREATE TABLE SB_DELIVERY_METHOD (
    REC_VERSION             EV_INT NOT NULL,
    SB_DELIVERY_METHOD_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE          EV_DATE NOT NULL,
    EFFECTIVE_UNTIL         EV_DATE NOT NULL,
    SY_DELIVERY_METHOD_NBR  EV_INT NOT NULL
);


CREATE TABLE SB_DELIVERY_SERVICE (
    REC_VERSION              EV_INT NOT NULL,
    SB_DELIVERY_SERVICE_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE           EV_DATE NOT NULL,
    EFFECTIVE_UNTIL          EV_DATE NOT NULL,
    SY_DELIVERY_SERVICE_NBR  EV_INT NOT NULL
);


CREATE TABLE SB_DELIVERY_SERVICE_OPT (
    REC_VERSION                  EV_INT NOT NULL,
    SB_DELIVERY_SERVICE_OPT_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE               EV_DATE NOT NULL,
    EFFECTIVE_UNTIL              EV_DATE NOT NULL,
    SB_DELIVERY_SERVICE_NBR      EV_INT NOT NULL,
    SB_DELIVERY_METHOD_NBR       EV_INT,
    OPTION_TAG                   EV_STR40 NOT NULL,
    OPTION_INTEGER_VALUE         EV_INT,
    OPTION_STRING_VALUE          EV_STR512
);


CREATE TABLE SB_ENLIST_GROUPS (
    REC_VERSION           EV_INT NOT NULL,
    SB_ENLIST_GROUPS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE        EV_DATE NOT NULL,
    EFFECTIVE_UNTIL       EV_DATE NOT NULL,
    SY_REPORT_GROUPS_NBR  EV_INT NOT NULL,
    MEDIA_TYPE            EV_CHAR1 NOT NULL,
    PROCESS_TYPE          EV_CHAR1 NOT NULL
);


CREATE TABLE SB_GLOBAL_AGENCY_CONTACTS (
    REC_VERSION                    EV_INT NOT NULL,
    SB_GLOBAL_AGENCY_CONTACTS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE                 EV_DATE NOT NULL,
    EFFECTIVE_UNTIL                EV_DATE NOT NULL,
    SY_GLOBAL_AGENCY_NBR           EV_INT NOT NULL,
    CONTACT_NAME                   EV_STR30 NOT NULL,
    PHONE                          EV_STR20,
    FAX                            EV_STR20,
    E_MAIL_ADDRESS                 EV_STR80,
    NOTES                          EV_BLOB_BIN
);


CREATE TABLE SB_HOLIDAYS (
    REC_VERSION      EV_INT NOT NULL,
    SB_HOLIDAYS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE   EV_DATE NOT NULL,
    EFFECTIVE_UNTIL  EV_DATE NOT NULL,
    HOLIDAY_DATE     EV_DATE NOT NULL,
    HOLIDAY_NAME     EV_STR40 NOT NULL,
    USED_BY          EV_CHAR1 NOT NULL
);


CREATE TABLE SB_MAIL_BOX (
    REC_VERSION                     EV_INT NOT NULL,
    SB_MAIL_BOX_NBR                 EV_INT NOT NULL,
    EFFECTIVE_DATE                  EV_DATE NOT NULL,
    EFFECTIVE_UNTIL                 EV_DATE NOT NULL,
    CL_NBR                          EV_INT,
    CL_MAIL_BOX_GROUP_NBR           EV_INT,
    COST                            EV_AMOUNT,
    REQUIRED                        EV_CHAR1 NOT NULL,
    NOTIFICATION_EMAIL              EV_STR80,
    PR_NBR                          EV_INT,
    RELEASED_TIME                   EV_DATETIME,
    PRINTED_TIME                    EV_DATETIME,
    SB_COVER_LETTER_REPORT_NBR      EV_INT,
    SCANNED_TIME                    EV_DATETIME,
    SHIPPED_TIME                    EV_DATETIME,
    SY_COVER_LETTER_REPORT_NBR      EV_INT,
    NOTE                            EV_STR512,
    UP_LEVEL_MAIL_BOX_NBR           EV_INT,
    TRACKING_INFO                   EV_STR40,
    AUTO_RELEASE_TYPE               EV_CHAR1 NOT NULL,
    BARCODE                         EV_STR40 NOT NULL,
    CREATED_TIME                    EV_DATETIME NOT NULL,
    DISPOSE_CONTENT_AFTER_SHIPPING  EV_CHAR1 NOT NULL,
    ADDRESSEE                       EV_STR512 NOT NULL,
    DESCRIPTION                     EV_STR40 NOT NULL,
    SB_DELIVERY_METHOD_NBR          EV_INT NOT NULL,
    SB_MEDIA_TYPE_NBR               EV_INT NOT NULL
);


CREATE TABLE SB_MAIL_BOX_CONTENT (
    REC_VERSION              EV_INT NOT NULL,
    SB_MAIL_BOX_CONTENT_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE           EV_DATE NOT NULL,
    EFFECTIVE_UNTIL          EV_DATE NOT NULL,
    SB_MAIL_BOX_NBR          EV_INT NOT NULL,
    DESCRIPTION              EV_STR512 NOT NULL,
    FILE_NAME                EV_STR40 NOT NULL,
    MEDIA_TYPE               EV_CHAR1 NOT NULL,
    PAGE_COUNT               EV_INT NOT NULL
);


CREATE TABLE SB_MAIL_BOX_OPTION (
    REC_VERSION              EV_INT NOT NULL,
    SB_MAIL_BOX_OPTION_NBR   EV_INT NOT NULL,
    EFFECTIVE_DATE           EV_DATE NOT NULL,
    EFFECTIVE_UNTIL          EV_DATE NOT NULL,
    SB_MAIL_BOX_NBR          EV_INT NOT NULL,
    SB_MAIL_BOX_CONTENT_NBR  EV_INT,
    OPTION_TAG               EV_STR80 NOT NULL,
    OPTION_INTEGER_VALUE     EV_INT,
    OPTION_STRING_VALUE      EV_STR512
);


CREATE TABLE SB_MEDIA_TYPE (
    REC_VERSION        EV_INT NOT NULL,
    SB_MEDIA_TYPE_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE     EV_DATE NOT NULL,
    EFFECTIVE_UNTIL    EV_DATE NOT NULL,
    SY_MEDIA_TYPE_NBR  EV_INT NOT NULL
);


CREATE TABLE SB_MEDIA_TYPE_OPTION (
    REC_VERSION               EV_INT NOT NULL,
    SB_MEDIA_TYPE_OPTION_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE            EV_DATE NOT NULL,
    EFFECTIVE_UNTIL           EV_DATE NOT NULL,
    SB_MEDIA_TYPE_NBR         EV_INT NOT NULL,
    OPTION_TAG                EV_STR40 NOT NULL,
    OPTION_INTEGER_VALUE      EV_INT,
    OPTION_STRING_VALUE       EV_STR512
);


CREATE TABLE SB_MULTICLIENT_REPORTS (
    REC_VERSION                 EV_INT NOT NULL,
    SB_MULTICLIENT_REPORTS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE              EV_DATE NOT NULL,
    EFFECTIVE_UNTIL             EV_DATE NOT NULL,
    DESCRIPTION                 EV_STR40 NOT NULL,
    REPORT_LEVEL                EV_CHAR1 NOT NULL,
    REPORT_NBR                  EV_INT NOT NULL,
    INPUT_PARAMS                EV_BLOB_BIN
);


CREATE TABLE SB_OPTION (
    REC_VERSION           EV_INT NOT NULL,
    SB_OPTION_NBR         EV_INT NOT NULL,
    EFFECTIVE_DATE        EV_DATE NOT NULL,
    EFFECTIVE_UNTIL       EV_DATE NOT NULL,
    OPTION_TAG            EV_STR80 NOT NULL,
    OPTION_INTEGER_VALUE  EV_INT,
    OPTION_STRING_VALUE   EV_STR512
);


CREATE TABLE SB_OTHER_SERVICE (
    REC_VERSION           EV_INT NOT NULL,
    SB_OTHER_SERVICE_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE        EV_DATE NOT NULL,
    EFFECTIVE_UNTIL       EV_DATE NOT NULL,
    NAME                  EV_STR60 NOT NULL
);


CREATE TABLE SB_PAPER_INFO (
    REC_VERSION        EV_INT NOT NULL,
    SB_PAPER_INFO_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE     EV_DATE NOT NULL,
    EFFECTIVE_UNTIL    EV_DATE NOT NULL,
    DESCRIPTION        EV_STR40 NOT NULL,
    HEIGHT             EV_AMOUNT NOT NULL,
    WIDTH              EV_AMOUNT NOT NULL,
    WEIGHT             EV_AMOUNT NOT NULL,
    MEDIA_TYPE         EV_CHAR1 NOT NULL
);


CREATE TABLE SB_QUEUE_PRIORITY (
    REC_VERSION            EV_INT NOT NULL,
    SB_QUEUE_PRIORITY_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE         EV_DATE NOT NULL,
    EFFECTIVE_UNTIL        EV_DATE NOT NULL,
    THREADS                EV_INT NOT NULL,
    METHOD_NAME            EV_STR255 NOT NULL,
    PRIORITY               EV_INT NOT NULL,
    SB_USER_NBR            EV_INT,
    SB_SEC_GROUPS_NBR      EV_INT
);


CREATE TABLE SB_REFERRALS (
    REC_VERSION       EV_INT NOT NULL,
    SB_REFERRALS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE    EV_DATE NOT NULL,
    EFFECTIVE_UNTIL   EV_DATE NOT NULL,
    NAME              EV_STR40 NOT NULL
);


CREATE TABLE SB_REPORT_WRITER_REPORTS (
    REC_VERSION                   EV_INT NOT NULL,
    SB_REPORT_WRITER_REPORTS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE                EV_DATE NOT NULL,
    EFFECTIVE_UNTIL               EV_DATE NOT NULL,
    REPORT_DESCRIPTION            EV_STR40 NOT NULL,
    REPORT_TYPE                   EV_CHAR1 NOT NULL,
    REPORT_FILE                   EV_BLOB_BIN,
    NOTES                         EV_BLOB_BIN,
    MEDIA_TYPE                    EV_CHAR1 NOT NULL,
    CLASS_NAME                    EV_STR40,
    ANCESTOR_CLASS_NAME           EV_STR40
);


CREATE TABLE SB_REPORTS (
    REC_VERSION                EV_INT NOT NULL,
    SB_REPORTS_NBR             EV_INT NOT NULL,
    EFFECTIVE_DATE             EV_DATE NOT NULL,
    EFFECTIVE_UNTIL            EV_DATE NOT NULL,
    DESCRIPTION                EV_STR40 NOT NULL,
    COMMENTS                   EV_BLOB_BIN,
    REPORT_WRITER_REPORTS_NBR  EV_INT NOT NULL,
    REPORT_LEVEL               EV_CHAR1 NOT NULL,
    INPUT_PARAMS               EV_BLOB_BIN
);


CREATE TABLE SB_SALES_TAX_STATES (
    REC_VERSION              EV_INT NOT NULL,
    SB_SALES_TAX_STATES_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE           EV_DATE NOT NULL,
    EFFECTIVE_UNTIL          EV_DATE NOT NULL,
    STATE                    EV_CHAR2 NOT NULL,
    STATE_TAX_ID             EV_STR19 NOT NULL,
    SALES_TAX_PERCENTAGE     EV_AMOUNT
);


CREATE TABLE SB_SEC_CLIENTS (
    REC_VERSION         EV_INT NOT NULL,
    SB_SEC_CLIENTS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE      EV_DATE NOT NULL,
    EFFECTIVE_UNTIL     EV_DATE NOT NULL,
    CL_NBR              EV_INT NOT NULL,
    SB_SEC_GROUPS_NBR   EV_INT,
    SB_USER_NBR         EV_INT
);


CREATE TABLE SB_SEC_GROUP_MEMBERS (
    REC_VERSION               EV_INT NOT NULL,
    SB_SEC_GROUP_MEMBERS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE            EV_DATE NOT NULL,
    EFFECTIVE_UNTIL           EV_DATE NOT NULL,
    SB_SEC_GROUPS_NBR         EV_INT NOT NULL,
    SB_USER_NBR               EV_INT NOT NULL
);


CREATE TABLE SB_SEC_GROUPS (
    REC_VERSION        EV_INT NOT NULL,
    SB_SEC_GROUPS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE     EV_DATE NOT NULL,
    EFFECTIVE_UNTIL    EV_DATE NOT NULL,
    NAME               EV_STR40 NOT NULL
);


CREATE TABLE SB_SEC_RIGHTS (
    REC_VERSION        EV_INT NOT NULL,
    SB_SEC_RIGHTS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE     EV_DATE NOT NULL,
    EFFECTIVE_UNTIL    EV_DATE NOT NULL,
    SB_SEC_GROUPS_NBR  EV_INT,
    SB_USER_NBR        EV_INT,
    TAG                EV_STR128 NOT NULL,
    CONTEXT            EV_STR128 NOT NULL
);


CREATE TABLE SB_SEC_ROW_FILTERS (
    REC_VERSION             EV_INT NOT NULL,
    SB_SEC_ROW_FILTERS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE          EV_DATE NOT NULL,
    EFFECTIVE_UNTIL         EV_DATE NOT NULL,
    SB_SEC_GROUPS_NBR       EV_INT,
    SB_USER_NBR             EV_INT,
    DATABASE_TYPE           EV_CHAR1 NOT NULL,
    TABLE_NAME              EV_STR40 NOT NULL,
    FILTER_TYPE             EV_CHAR1 NOT NULL,
    CUSTOM_EXPR             EV_STR255,
    CL_NBR                  EV_INT
);


CREATE TABLE SB_SEC_TEMPLATES (
    REC_VERSION           EV_INT NOT NULL,
    SB_SEC_TEMPLATES_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE        EV_DATE NOT NULL,
    EFFECTIVE_UNTIL       EV_DATE NOT NULL,
    NAME                  EV_STR40 NOT NULL,
    TEMPLATE              EV_BLOB_BIN NOT NULL
);


CREATE TABLE SB_SERVICES (
    REC_VERSION             EV_INT NOT NULL,
    SB_SERVICES_NBR         EV_INT NOT NULL,
    EFFECTIVE_DATE          EV_DATE NOT NULL,
    EFFECTIVE_UNTIL         EV_DATE NOT NULL,
    SERVICE_NAME            EV_STR40 NOT NULL,
    FREQUENCY               EV_CHAR1 NOT NULL,
    MONTH_NUMBER            EV_STR2,
    BASED_ON_TYPE           EV_CHAR1 NOT NULL,
    SB_REPORTS_NBR          EV_INT,
    COMMISSION              EV_CHAR1 NOT NULL,
    SALES_TAXABLE           EV_CHAR1 NOT NULL,
    FILLER                  EV_STR512,
    PRODUCT_CODE            EV_STR6,
    WEEK_NUMBER             EV_CHAR1 NOT NULL,
    SERVICE_TYPE            EV_CHAR1 NOT NULL,
    MINIMUM_AMOUNT          EV_AMOUNT,
    MAXIMUM_AMOUNT          EV_AMOUNT,
    SB_DELIVERY_METHOD_NBR  EV_INT,
    TAX_TYPE                EV_CHAR1 NOT NULL
);


CREATE TABLE SB_SERVICES_CALCULATIONS (
    REC_VERSION                   EV_INT NOT NULL,
    SB_SERVICES_CALCULATIONS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE                EV_DATE NOT NULL,
    EFFECTIVE_UNTIL               EV_DATE NOT NULL,
    SB_SERVICES_NBR               EV_INT NOT NULL,
    NEXT_MIN_QUANTITY_BEGIN_DATE  EV_DATETIME,
    NEXT_MAX_QUANTITY_BEGIN_DATE  EV_DATETIME,
    NEXT_PER_ITEM_BEGIN_DATE      EV_DATETIME,
    NEXT_FLAT_AMOUNT_BEGIN_DATE   EV_DATETIME,
    MINIMUM_QUANTITY              EV_AMOUNT,
    NEXT_MINIMUM_QUANTITY         EV_AMOUNT,
    MAXIMUM_QUANTITY              EV_AMOUNT,
    NEXT_MAXIMUM_QUANTITY         EV_AMOUNT,
    PER_ITEM_RATE                 EV_AMOUNT,
    NEXT_PER_ITEM_RATE            EV_AMOUNT,
    FLAT_AMOUNT                   EV_AMOUNT,
    NEXT_FLAT_AMOUNT              EV_AMOUNT
);


CREATE TABLE SB_STORAGE (
    REC_VERSION      EV_INT NOT NULL,
    SB_STORAGE_NBR   EV_INT NOT NULL,
    EFFECTIVE_DATE   EV_DATE NOT NULL,
    EFFECTIVE_UNTIL  EV_DATE NOT NULL,
    TAG              EV_STR20 NOT NULL,
    STORAGE_DATA     EV_BLOB_BIN NOT NULL
);


CREATE TABLE SB_TASK (
    REC_VERSION      EV_INT NOT NULL,
    SB_TASK_NBR      EV_INT NOT NULL,
    EFFECTIVE_DATE   EV_DATE NOT NULL,
    EFFECTIVE_UNTIL  EV_DATE NOT NULL,
    SB_USER_NBR      EV_INT NOT NULL,
    SCHEDULE         EV_STR255 NOT NULL,
    DESCRIPTION      EV_STR255 NOT NULL,
    TASK             EV_BLOB_BIN NOT NULL,
    LAST_RUN         EV_DATETIME
);


CREATE TABLE SB_TAX_PAYMENT (
    REC_VERSION         EV_INT NOT NULL,
    SB_TAX_PAYMENT_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE      EV_DATE NOT NULL,
    EFFECTIVE_UNTIL     EV_DATE NOT NULL,
    DESCRIPTION         EV_STR80,
    STATUS              EV_CHAR1 NOT NULL,
    STATUS_DATE         EV_DATETIME
);


CREATE TABLE SB_TEAM (
    REC_VERSION       EV_INT NOT NULL,
    SB_TEAM_NBR       EV_INT NOT NULL,
    EFFECTIVE_DATE    EV_DATE NOT NULL,
    EFFECTIVE_UNTIL   EV_DATE NOT NULL,
    TEAM_DESCRIPTION  EV_STR40 NOT NULL,
    CR_CATEGORY       EV_CHAR1 NOT NULL
);


CREATE TABLE SB_TEAM_MEMBERS (
    REC_VERSION          EV_INT NOT NULL,
    SB_TEAM_MEMBERS_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE       EV_DATE NOT NULL,
    EFFECTIVE_UNTIL      EV_DATE NOT NULL,
    SB_TEAM_NBR          EV_INT NOT NULL,
    SB_USER_NBR          EV_INT NOT NULL
);


CREATE TABLE SB_USER (
    REC_VERSION           EV_INT NOT NULL,
    SB_USER_NBR           EV_INT NOT NULL,
    EFFECTIVE_DATE        EV_DATE NOT NULL,
    EFFECTIVE_UNTIL       EV_DATE NOT NULL,
    USER_ID               EV_STR128 NOT NULL,
    USER_SIGNATURE        EV_BLOB_BIN,
    LAST_NAME             EV_STR30 NOT NULL,
    FIRST_NAME            EV_STR20 NOT NULL,
    MIDDLE_INITIAL        EV_CHAR1,
    ACTIVE_USER           EV_CHAR1 NOT NULL,
    DEPARTMENT            EV_CHAR1 NOT NULL,
    PASSWORD_CHANGE_DATE  EV_DATETIME NOT NULL,
    SECURITY_LEVEL        EV_CHAR1 NOT NULL,
    USER_UPDATE_OPTIONS   EV_STR512 NOT NULL,
    USER_FUNCTIONS        EV_STR512 NOT NULL,
    USER_PASSWORD         EV_STR32 NOT NULL,
    EMAIL_ADDRESS         EV_STR80,
    CL_NBR                EV_INT,
    SB_ACCOUNTANT_NBR     EV_INT,
    WRONG_PSWD_ATTEMPTS   EV_INT,
    LINKS_DATA            EV_STR128,
    LOGIN_QUESTION1       EV_INT,
    LOGIN_ANSWER1         EV_STR80,
    LOGIN_QUESTION2       EV_INT,
    LOGIN_ANSWER2         EV_STR80,
    LOGIN_QUESTION3       EV_INT,
    LOGIN_ANSWER3         EV_STR80,
    HR_PERSONNEL          EV_CHAR1 NOT NULL,
    SEC_QUESTION1         EV_INT,
    SEC_ANSWER1           EV_STR80,
    SEC_QUESTION2         EV_INT,
    SEC_ANSWER2           EV_STR80
);


CREATE TABLE SB_USER_NOTICE (
    REC_VERSION         EV_INT NOT NULL,
    SB_USER_NOTICE_NBR  EV_INT NOT NULL,
    EFFECTIVE_DATE      EV_DATE NOT NULL,
    EFFECTIVE_UNTIL     EV_DATE NOT NULL,
    SB_USER_NBR         EV_INT NOT NULL,
    NAME                EV_STR128 NOT NULL,
    NOTES               EV_BLOB_BIN,
    TASK                EV_BLOB_BIN,
    LAST_DISMISS        EV_DATETIME,
    NEXT_REMINDER       EV_DATETIME
);




/* Check constraints definition */

ALTER TABLE SB_BLOB ADD CONSTRAINT C_SB_BLOB_1 check (effective_date < effective_until);
ALTER TABLE SB ADD CONSTRAINT C_SB_1 check (effective_date < effective_until);
ALTER TABLE SB_ACCOUNTANT ADD CONSTRAINT C_SB_ACCOUNTANT_1 check (effective_date < effective_until);
ALTER TABLE SB_AGENCY ADD CONSTRAINT C_SB_AGENCY_1 check (effective_date < effective_until);
ALTER TABLE SB_AGENCY_REPORTS ADD CONSTRAINT C_SB_AGENCY_REPORTS_1 check (effective_date < effective_until);
ALTER TABLE SB_BANKS ADD CONSTRAINT C_SB_BANKS_1 check (effective_date < effective_until);
ALTER TABLE SB_BANK_ACCOUNTS ADD CONSTRAINT C_SB_BANK_ACCOUNTS_1 check (effective_date < effective_until);
ALTER TABLE SB_DELIVERY_COMPANY ADD CONSTRAINT C_SB_DELIVERY_COMPANY_1 check (effective_date < effective_until);
ALTER TABLE SB_DELIVERY_COMPANY_SVCS ADD CONSTRAINT C_SB_DELIVERY_COMPANY_SVCS_1 check (effective_date < effective_until);
ALTER TABLE SB_DELIVERY_METHOD ADD CONSTRAINT C_SB_DELIVERY_METHOD_1 check (effective_date < effective_until);
ALTER TABLE SB_DELIVERY_SERVICE ADD CONSTRAINT C_SB_DELIVERY_SERVICE_1 check (effective_date < effective_until);
ALTER TABLE SB_DELIVERY_SERVICE_OPT ADD CONSTRAINT C_SB_DELIVERY_SERVICE_OPT_1 check (effective_date < effective_until);
ALTER TABLE SB_ENLIST_GROUPS ADD CONSTRAINT C_SB_ENLIST_GROUPS_1 check (effective_date < effective_until);
ALTER TABLE SB_GLOBAL_AGENCY_CONTACTS ADD CONSTRAINT C_SB_GLOBAL_AGENCY_CONTACTS_1 check (effective_date < effective_until);
ALTER TABLE SB_HOLIDAYS ADD CONSTRAINT C_SB_HOLIDAYS_1 check (effective_date < effective_until);
ALTER TABLE SB_MAIL_BOX ADD CONSTRAINT C_SB_MAIL_BOX_1 check (effective_date < effective_until);
ALTER TABLE SB_MAIL_BOX_CONTENT ADD CONSTRAINT C_SB_MAIL_BOX_CONTENT_1 check (effective_date < effective_until);
ALTER TABLE SB_MAIL_BOX_OPTION ADD CONSTRAINT C_SB_MAIL_BOX_OPTION_1 check (effective_date < effective_until);
ALTER TABLE SB_MEDIA_TYPE ADD CONSTRAINT C_SB_MEDIA_TYPE_1 check (effective_date < effective_until);
ALTER TABLE SB_MEDIA_TYPE_OPTION ADD CONSTRAINT C_SB_MEDIA_TYPE_OPTION_1 check (effective_date < effective_until);
ALTER TABLE SB_MULTICLIENT_REPORTS ADD CONSTRAINT C_SB_MULTICLIENT_REPORTS_1 check (effective_date < effective_until);
ALTER TABLE SB_OPTION ADD CONSTRAINT C_SB_OPTION_1 check (effective_date < effective_until);
ALTER TABLE SB_OTHER_SERVICE ADD CONSTRAINT C_SB_OTHER_SERVICE_1 check (effective_date < effective_until);
ALTER TABLE SB_PAPER_INFO ADD CONSTRAINT C_SB_PAPER_INFO_1 check (effective_date < effective_until);
ALTER TABLE SB_QUEUE_PRIORITY ADD CONSTRAINT C_SB_QUEUE_PRIORITY_1 check (effective_date < effective_until);
ALTER TABLE SB_REFERRALS ADD CONSTRAINT C_SB_REFERRALS_1 check (effective_date < effective_until);
ALTER TABLE SB_REPORTS ADD CONSTRAINT C_SB_REPORTS_1 check (effective_date < effective_until);
ALTER TABLE SB_REPORT_WRITER_REPORTS ADD CONSTRAINT C_SB_REPORT_WRITER_REPORTS_1 check (effective_date < effective_until);
ALTER TABLE SB_SALES_TAX_STATES ADD CONSTRAINT C_SB_SALES_TAX_STATES_1 check (effective_date < effective_until);
ALTER TABLE SB_SEC_CLIENTS ADD CONSTRAINT C_SB_SEC_CLIENTS_1 check (effective_date < effective_until);
ALTER TABLE SB_SEC_GROUPS ADD CONSTRAINT C_SB_SEC_GROUPS_1 check (effective_date < effective_until);
ALTER TABLE SB_SEC_GROUP_MEMBERS ADD CONSTRAINT C_SB_SEC_GROUP_MEMBERS_1 check (effective_date < effective_until);
ALTER TABLE SB_SEC_RIGHTS ADD CONSTRAINT C_SB_SEC_RIGHTS_1 check (effective_date < effective_until);
ALTER TABLE SB_SEC_ROW_FILTERS ADD CONSTRAINT C_SB_SEC_ROW_FILTERS_1 check (effective_date < effective_until);
ALTER TABLE SB_SEC_TEMPLATES ADD CONSTRAINT C_SB_SEC_TEMPLATES_1 check (effective_date < effective_until);
ALTER TABLE SB_SERVICES ADD CONSTRAINT C_SB_SERVICES_1 check (effective_date < effective_until);
ALTER TABLE SB_SERVICES_CALCULATIONS ADD CONSTRAINT C_SB_SERVICES_CALCULATIONS_1 check (effective_date < effective_until);
ALTER TABLE SB_TASK ADD CONSTRAINT C_SB_TASK_1 check (effective_date < effective_until);
ALTER TABLE SB_TAX_PAYMENT ADD CONSTRAINT C_SB_TAX_PAYMENT_1 check (effective_date < effective_until);
ALTER TABLE SB_TEAM ADD CONSTRAINT C_SB_TEAM_1 check (effective_date < effective_until);
ALTER TABLE SB_TEAM_MEMBERS ADD CONSTRAINT C_SB_TEAM_MEMBERS_1 check (effective_date < effective_until);
ALTER TABLE SB_USER ADD CONSTRAINT C_SB_USER_1 check (effective_date < effective_until);
ALTER TABLE SB_USER_NOTICE ADD CONSTRAINT C_SB_USER_NOTICE_1 check (effective_date < effective_until);
ALTER TABLE SB_STORAGE ADD CONSTRAINT C_SB_STORAGE_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL);
ALTER TABLE EV_TABLE_CHANGE ADD CONSTRAINT FC_EV_TABLE_CHANGE_1 CHECK (CHANGE_TYPE in ('I', 'U', 'D', 'R'));
ALTER TABLE EV_FIELD ADD CONSTRAINT FC_EV_FIELD_1 CHECK (FIELD_TYPE in ('I', 'V', 'C', 'F', 'E', 'N', 'D', 'T', 'B', 'S')
);
ALTER TABLE EV_FIELD ADD CONSTRAINT FC_EV_FIELD_2 CHECK (REQUIRED in ('N', 'Y'));


/******************************************************************************/
/****                          Unique Constraints                          ****/
/******************************************************************************/

ALTER TABLE EV_FIELD ADD CONSTRAINT AK_EV_FIELD UNIQUE (NAME, EV_TABLE_NBR);
ALTER TABLE EV_TABLE ADD CONSTRAINT AK_EV_TABLE UNIQUE (NAME);
ALTER TABLE EV_TRANSACTION ADD CONSTRAINT AK_A_TRANSACTION UNIQUE (COMMIT_NBR);
ALTER TABLE SB ADD CONSTRAINT AK_SB_1 UNIQUE (SB_NBR, EFFECTIVE_DATE);
ALTER TABLE SB ADD CONSTRAINT AK_SB_2 UNIQUE (SB_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_ACCOUNTANT ADD CONSTRAINT AK_SB_ACCOUNTANT_1 UNIQUE (SB_ACCOUNTANT_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_ACCOUNTANT ADD CONSTRAINT AK_SB_ACCOUNTANT_2 UNIQUE (SB_ACCOUNTANT_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_AGENCY ADD CONSTRAINT AK_SB_AGENCY_1 UNIQUE (SB_AGENCY_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_AGENCY ADD CONSTRAINT AK_SB_AGENCY_2 UNIQUE (SB_AGENCY_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_AGENCY_REPORTS ADD CONSTRAINT AK_SB_AGENCY_REPORTS_1 UNIQUE (SB_AGENCY_REPORTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_AGENCY_REPORTS ADD CONSTRAINT AK_SB_AGENCY_REPORTS_2 UNIQUE (SB_AGENCY_REPORTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_BANKS ADD CONSTRAINT AK_SB_BANKS_1 UNIQUE (SB_BANKS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_BANKS ADD CONSTRAINT AK_SB_BANKS_2 UNIQUE (SB_BANKS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_BANK_ACCOUNTS ADD CONSTRAINT AK_SB_BANK_ACCOUNTS_1 UNIQUE (SB_BANK_ACCOUNTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_BANK_ACCOUNTS ADD CONSTRAINT AK_SB_BANK_ACCOUNTS_2 UNIQUE (SB_BANK_ACCOUNTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_BLOB ADD CONSTRAINT AK_SB_BLOB_1 UNIQUE (SB_BLOB_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_BLOB ADD CONSTRAINT AK_SB_BLOB_2 UNIQUE (SB_BLOB_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_DELIVERY_COMPANY ADD CONSTRAINT AK_SB_DELIVERY_COMPANY_1 UNIQUE (SB_DELIVERY_COMPANY_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_DELIVERY_COMPANY ADD CONSTRAINT AK_SB_DELIVERY_COMPANY_2 UNIQUE (SB_DELIVERY_COMPANY_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_DELIVERY_COMPANY_SVCS ADD CONSTRAINT AK_SB_DELIVERY_COMPANY_SVCS_1 UNIQUE (SB_DELIVERY_COMPANY_SVCS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_DELIVERY_COMPANY_SVCS ADD CONSTRAINT AK_SB_DELIVERY_COMPANY_SVCS_2 UNIQUE (SB_DELIVERY_COMPANY_SVCS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_DELIVERY_METHOD ADD CONSTRAINT AK_SB_DELIVERY_METHOD_1 UNIQUE (SB_DELIVERY_METHOD_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_DELIVERY_METHOD ADD CONSTRAINT AK_SB_DELIVERY_METHOD_2 UNIQUE (SB_DELIVERY_METHOD_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_DELIVERY_SERVICE ADD CONSTRAINT AK_SB_DELIVERY_SERVICE_1 UNIQUE (SB_DELIVERY_SERVICE_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_DELIVERY_SERVICE ADD CONSTRAINT AK_SB_DELIVERY_SERVICE_2 UNIQUE (SB_DELIVERY_SERVICE_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_DELIVERY_SERVICE_OPT ADD CONSTRAINT AK_SB_DELIVERY_SERVICE_OPT_1 UNIQUE (SB_DELIVERY_SERVICE_OPT_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_DELIVERY_SERVICE_OPT ADD CONSTRAINT AK_SB_DELIVERY_SERVICE_OPT_2 UNIQUE (SB_DELIVERY_SERVICE_OPT_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_ENLIST_GROUPS ADD CONSTRAINT AK_SB_ENLIST_GROUPS_1 UNIQUE (SB_ENLIST_GROUPS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_ENLIST_GROUPS ADD CONSTRAINT AK_SB_ENLIST_GROUPS_2 UNIQUE (SB_ENLIST_GROUPS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_GLOBAL_AGENCY_CONTACTS ADD CONSTRAINT AK_SB_GLOBAL_AGENCY_CONTACTS_1 UNIQUE (SB_GLOBAL_AGENCY_CONTACTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_GLOBAL_AGENCY_CONTACTS ADD CONSTRAINT AK_SB_GLOBAL_AGENCY_CONTACTS_2 UNIQUE (SB_GLOBAL_AGENCY_CONTACTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_HOLIDAYS ADD CONSTRAINT AK_SB_HOLIDAYS_1 UNIQUE (SB_HOLIDAYS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_HOLIDAYS ADD CONSTRAINT AK_SB_HOLIDAYS_2 UNIQUE (SB_HOLIDAYS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_MAIL_BOX ADD CONSTRAINT AK_SB_MAIL_BOX_1 UNIQUE (SB_MAIL_BOX_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_MAIL_BOX ADD CONSTRAINT AK_SB_MAIL_BOX_2 UNIQUE (SB_MAIL_BOX_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_MAIL_BOX_CONTENT ADD CONSTRAINT AK_SB_MAIL_BOX_CONTENT_1 UNIQUE (SB_MAIL_BOX_CONTENT_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_MAIL_BOX_CONTENT ADD CONSTRAINT AK_SB_MAIL_BOX_CONTENT_2 UNIQUE (SB_MAIL_BOX_CONTENT_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_MAIL_BOX_OPTION ADD CONSTRAINT AK_SB_MAIL_BOX_OPTION_1 UNIQUE (SB_MAIL_BOX_OPTION_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_MAIL_BOX_OPTION ADD CONSTRAINT AK_SB_MAIL_BOX_OPTION_2 UNIQUE (SB_MAIL_BOX_OPTION_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_MEDIA_TYPE ADD CONSTRAINT AK_SB_MEDIA_TYPE_1 UNIQUE (SB_MEDIA_TYPE_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_MEDIA_TYPE ADD CONSTRAINT AK_SB_MEDIA_TYPE_2 UNIQUE (SB_MEDIA_TYPE_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_MEDIA_TYPE_OPTION ADD CONSTRAINT AK_SB_MEDIA_TYPE_OPTION_1 UNIQUE (SB_MEDIA_TYPE_OPTION_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_MEDIA_TYPE_OPTION ADD CONSTRAINT AK_SB_MEDIA_TYPE_OPTION_2 UNIQUE (SB_MEDIA_TYPE_OPTION_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_MULTICLIENT_REPORTS ADD CONSTRAINT AK_SB_MULTICLIENT_REPORTS_1 UNIQUE (SB_MULTICLIENT_REPORTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_MULTICLIENT_REPORTS ADD CONSTRAINT AK_SB_MULTICLIENT_REPORTS_2 UNIQUE (SB_MULTICLIENT_REPORTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_OPTION ADD CONSTRAINT AK_SB_OPTION_1 UNIQUE (SB_OPTION_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_OPTION ADD CONSTRAINT AK_SB_OPTION_2 UNIQUE (SB_OPTION_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_OTHER_SERVICE ADD CONSTRAINT AK_SB_OTHER_SERVICE_1 UNIQUE (SB_OTHER_SERVICE_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_OTHER_SERVICE ADD CONSTRAINT AK_SB_OTHER_SERVICE_2 UNIQUE (SB_OTHER_SERVICE_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_PAPER_INFO ADD CONSTRAINT AK_SB_PAPER_INFO_1 UNIQUE (SB_PAPER_INFO_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_PAPER_INFO ADD CONSTRAINT AK_SB_PAPER_INFO_2 UNIQUE (SB_PAPER_INFO_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_QUEUE_PRIORITY ADD CONSTRAINT AK_SB_QUEUE_PRIORITY_1 UNIQUE (SB_QUEUE_PRIORITY_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_QUEUE_PRIORITY ADD CONSTRAINT AK_SB_QUEUE_PRIORITY_2 UNIQUE (SB_QUEUE_PRIORITY_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_REFERRALS ADD CONSTRAINT AK_SB_REFERRALS_1 UNIQUE (SB_REFERRALS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_REFERRALS ADD CONSTRAINT AK_SB_REFERRALS_2 UNIQUE (SB_REFERRALS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_REPORTS ADD CONSTRAINT AK_SB_REPORTS_1 UNIQUE (SB_REPORTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_REPORTS ADD CONSTRAINT AK_SB_REPORTS_2 UNIQUE (SB_REPORTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_REPORT_WRITER_REPORTS ADD CONSTRAINT AK_SB_REPORT_WRITER_REPORTS_1 UNIQUE (SB_REPORT_WRITER_REPORTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_REPORT_WRITER_REPORTS ADD CONSTRAINT AK_SB_REPORT_WRITER_REPORTS_2 UNIQUE (SB_REPORT_WRITER_REPORTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SALES_TAX_STATES ADD CONSTRAINT AK_SB_SALES_TAX_STATES_1 UNIQUE (SB_SALES_TAX_STATES_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SALES_TAX_STATES ADD CONSTRAINT AK_SB_SALES_TAX_STATES_2 UNIQUE (SB_SALES_TAX_STATES_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SEC_CLIENTS ADD CONSTRAINT AK_SB_SEC_CLIENTS_1 UNIQUE (SB_SEC_CLIENTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SEC_CLIENTS ADD CONSTRAINT AK_SB_SEC_CLIENTS_2 UNIQUE (SB_SEC_CLIENTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SEC_GROUPS ADD CONSTRAINT AK_SB_SEC_GROUPS_1 UNIQUE (SB_SEC_GROUPS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SEC_GROUPS ADD CONSTRAINT AK_SB_SEC_GROUPS_2 UNIQUE (SB_SEC_GROUPS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SEC_GROUP_MEMBERS ADD CONSTRAINT AK_SB_SEC_GROUP_MEMBERS_1 UNIQUE (SB_SEC_GROUP_MEMBERS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SEC_GROUP_MEMBERS ADD CONSTRAINT AK_SB_SEC_GROUP_MEMBERS_2 UNIQUE (SB_SEC_GROUP_MEMBERS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SEC_RIGHTS ADD CONSTRAINT AK_SB_SEC_RIGHTS_1 UNIQUE (SB_SEC_RIGHTS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SEC_RIGHTS ADD CONSTRAINT AK_SB_SEC_RIGHTS_2 UNIQUE (SB_SEC_RIGHTS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SEC_ROW_FILTERS ADD CONSTRAINT AK_SB_SEC_ROW_FILTERS_1 UNIQUE (SB_SEC_ROW_FILTERS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SEC_ROW_FILTERS ADD CONSTRAINT AK_SB_SEC_ROW_FILTERS_2 UNIQUE (SB_SEC_ROW_FILTERS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SEC_TEMPLATES ADD CONSTRAINT AK_SB_SEC_TEMPLATES_1 UNIQUE (SB_SEC_TEMPLATES_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SEC_TEMPLATES ADD CONSTRAINT AK_SB_SEC_TEMPLATES_2 UNIQUE (SB_SEC_TEMPLATES_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SERVICES ADD CONSTRAINT AK_SB_SERVICES_1 UNIQUE (SB_SERVICES_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SERVICES ADD CONSTRAINT AK_SB_SERVICES_2 UNIQUE (SB_SERVICES_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_SERVICES_CALCULATIONS ADD CONSTRAINT AK_SB_SERVICES_CALCULATIONS_1 UNIQUE (SB_SERVICES_CALCULATIONS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_SERVICES_CALCULATIONS ADD CONSTRAINT AK_SB_SERVICES_CALCULATIONS_2 UNIQUE (SB_SERVICES_CALCULATIONS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_STORAGE ADD CONSTRAINT AK_SB_STORAGE_1 UNIQUE (SB_STORAGE_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_STORAGE ADD CONSTRAINT AK_SB_STORAGE_2 UNIQUE (SB_STORAGE_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_TASK ADD CONSTRAINT AK_SB_TASK_1 UNIQUE (SB_TASK_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_TASK ADD CONSTRAINT AK_SB_TASK_2 UNIQUE (SB_TASK_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_TAX_PAYMENT ADD CONSTRAINT AK_SB_TAX_PAYMENT_1 UNIQUE (SB_TAX_PAYMENT_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_TAX_PAYMENT ADD CONSTRAINT AK_SB_TAX_PAYMENT_2 UNIQUE (SB_TAX_PAYMENT_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_TEAM ADD CONSTRAINT AK_SB_TEAM_1 UNIQUE (SB_TEAM_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_TEAM ADD CONSTRAINT AK_SB_TEAM_2 UNIQUE (SB_TEAM_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_TEAM_MEMBERS ADD CONSTRAINT AK_SB_TEAM_MEMBERS_1 UNIQUE (SB_TEAM_MEMBERS_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_TEAM_MEMBERS ADD CONSTRAINT AK_SB_TEAM_MEMBERS_2 UNIQUE (SB_TEAM_MEMBERS_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_USER ADD CONSTRAINT AK_SB_USER_1 UNIQUE (SB_USER_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_USER ADD CONSTRAINT AK_SB_USER_2 UNIQUE (SB_USER_NBR, EFFECTIVE_UNTIL);
ALTER TABLE SB_USER_NOTICE ADD CONSTRAINT AK_SB_USER_NOTICE_1 UNIQUE (SB_USER_NOTICE_NBR, EFFECTIVE_DATE);
ALTER TABLE SB_USER_NOTICE ADD CONSTRAINT AK_SB_USER_NOTICE_2 UNIQUE (SB_USER_NOTICE_NBR, EFFECTIVE_UNTIL);


/******************************************************************************/
/****                             Primary Keys                             ****/
/******************************************************************************/

ALTER TABLE EV_FIELD ADD CONSTRAINT PK_EV_FIELD PRIMARY KEY (NBR);
ALTER TABLE EV_FIELD_CHANGE ADD CONSTRAINT PK_EV_FIELD_CHANGE PRIMARY KEY (EV_TABLE_CHANGE_NBR, EV_FIELD_NBR);
ALTER TABLE EV_FIELD_CHANGE_BLOB ADD CONSTRAINT PK_EV_FIELD_CHANGE_BLOB PRIMARY KEY (NBR);
ALTER TABLE EV_TABLE ADD CONSTRAINT PK_EV_TABLE PRIMARY KEY (NBR);
ALTER TABLE EV_TABLE_CHANGE ADD CONSTRAINT PK_EV_TABLE_CHANGE PRIMARY KEY (NBR);
ALTER TABLE EV_TRANSACTION ADD CONSTRAINT PK_EV_TRANSACTION PRIMARY KEY (NBR);
ALTER TABLE SB ADD CONSTRAINT PK_SB PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_ACCOUNTANT ADD CONSTRAINT PK_SB_ACCOUNTANT PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_AGENCY ADD CONSTRAINT PK_SB_AGENCY PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_AGENCY_REPORTS ADD CONSTRAINT PK_SB_AGENCY_REPORTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_BANKS ADD CONSTRAINT PK_SB_BANKS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_BANK_ACCOUNTS ADD CONSTRAINT PK_SB_BANK_ACCOUNTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_BLOB ADD CONSTRAINT PK_SB_BLOB PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_DELIVERY_COMPANY ADD CONSTRAINT PK_SB_DELIVERY_COMPANY PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_DELIVERY_COMPANY_SVCS ADD CONSTRAINT PK_SB_DELIVERY_COMPANY_SVCS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_DELIVERY_METHOD ADD CONSTRAINT PK_SB_DELIVERY_METHOD PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_DELIVERY_SERVICE ADD CONSTRAINT PK_SB_DELIVERY_SERVICE PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_DELIVERY_SERVICE_OPT ADD CONSTRAINT PK_SB_DELIVERY_SERVICE_OPT PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_ENLIST_GROUPS ADD CONSTRAINT PK_SB_ENLIST_GROUPS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_GLOBAL_AGENCY_CONTACTS ADD CONSTRAINT PK_SB_GLOBAL_AGENCY_CONTACTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_HOLIDAYS ADD CONSTRAINT PK_SB_HOLIDAYS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_MAIL_BOX ADD CONSTRAINT PK_SB_MAIL_BOX PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_MAIL_BOX_CONTENT ADD CONSTRAINT PK_SB_MAIL_BOX_CONTENT PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_MAIL_BOX_OPTION ADD CONSTRAINT PK_SB_MAIL_BOX_OPTION PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_MEDIA_TYPE ADD CONSTRAINT PK_SB_MEDIA_TYPE PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_MEDIA_TYPE_OPTION ADD CONSTRAINT PK_SB_MEDIA_TYPE_OPTION PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_MULTICLIENT_REPORTS ADD CONSTRAINT PK_SB_MULTICLIENT_REPORTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_OPTION ADD CONSTRAINT PK_SB_OPTION PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_OTHER_SERVICE ADD CONSTRAINT PK_SB_OTHER_SERVICE PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_PAPER_INFO ADD CONSTRAINT PK_SB_PAPER_INFO PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_QUEUE_PRIORITY ADD CONSTRAINT PK_SB_QUEUE_PRIORITY PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_REFERRALS ADD CONSTRAINT PK_SB_REFERRALS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_REPORTS ADD CONSTRAINT PK_SB_REPORTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_REPORT_WRITER_REPORTS ADD CONSTRAINT PK_SB_REPORT_WRITER_REPORTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SALES_TAX_STATES ADD CONSTRAINT PK_SB_SALES_TAX_STATES PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SEC_CLIENTS ADD CONSTRAINT PK_SB_SEC_CLIENTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SEC_GROUPS ADD CONSTRAINT PK_SB_SEC_GROUPS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SEC_GROUP_MEMBERS ADD CONSTRAINT PK_SB_SEC_GROUP_MEMBERS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SEC_RIGHTS ADD CONSTRAINT PK_SB_SEC_RIGHTS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SEC_ROW_FILTERS ADD CONSTRAINT PK_SB_SEC_ROW_FILTERS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SEC_TEMPLATES ADD CONSTRAINT PK_SB_SEC_TEMPLATES PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SERVICES ADD CONSTRAINT PK_SB_SERVICES PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_SERVICES_CALCULATIONS ADD CONSTRAINT PK_SB_SERVICES_CALCULATIONS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_STORAGE ADD CONSTRAINT PK_SB_STORAGE PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_TASK ADD CONSTRAINT PK_SB_TASK PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_TAX_PAYMENT ADD CONSTRAINT PK_SB_TAX_PAYMENT PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_TEAM ADD CONSTRAINT PK_SB_TEAM PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_TEAM_MEMBERS ADD CONSTRAINT PK_SB_TEAM_MEMBERS PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_USER ADD CONSTRAINT PK_SB_USER PRIMARY KEY (REC_VERSION);
ALTER TABLE SB_USER_NOTICE ADD CONSTRAINT PK_SB_USER_NOTICE PRIMARY KEY (REC_VERSION);


/******************************************************************************/
/****                             Foreign Keys                             ****/
/******************************************************************************/

ALTER TABLE EV_FIELD ADD CONSTRAINT FK_EV_FIELD_1 FOREIGN KEY (EV_TABLE_NBR) REFERENCES EV_TABLE (NBR);
ALTER TABLE EV_FIELD_CHANGE ADD CONSTRAINT FK_EV_FIELD_CHANGE_1 FOREIGN KEY (EV_FIELD_NBR) REFERENCES EV_FIELD (NBR);
ALTER TABLE EV_FIELD_CHANGE ADD CONSTRAINT FK_EV_FIELD_CHANGE_2 FOREIGN KEY (EV_TABLE_CHANGE_NBR) REFERENCES EV_TABLE_CHANGE (NBR);
ALTER TABLE EV_TABLE_CHANGE ADD CONSTRAINT FK_EV_TABLE_CHANGE_1 FOREIGN KEY (EV_TABLE_NBR) REFERENCES EV_TABLE (NBR);
ALTER TABLE EV_TABLE_CHANGE ADD CONSTRAINT FK_EV_TABLE_CHANGE_2 FOREIGN KEY (EV_TRANSACTION_NBR) REFERENCES EV_TRANSACTION (NBR);


/******************************************************************************/
/****                               Indices                                ****/
/******************************************************************************/

CREATE INDEX I_EV_TABLE_CHANGE_1 ON EV_TABLE_CHANGE (EV_TABLE_NBR, RECORD_ID);
CREATE INDEX I_EV_TABLE_CHANGE_2 ON EV_TABLE_CHANGE (EV_TABLE_NBR, RECORD_NBR);
CREATE INDEX I_EV_TRANSACTION_1 ON EV_TRANSACTION (COMMIT_TIME, USER_ID);
CREATE INDEX I_EV_TRANSACTION_2 ON EV_TRANSACTION (START_TIME, USER_ID);
CREATE UNIQUE DESCENDING INDEX I_EV_TRANSACTION_3 ON EV_TRANSACTION (COMMIT_NBR);
CREATE INDEX LK_SB ON SB (SB_NAME);
CREATE INDEX LK_SB_ACCOUNTANT ON SB_ACCOUNTANT (NAME);
CREATE INDEX FK_SB_AGENCY_1 ON SB_AGENCY (SB_BANKS_NBR);
CREATE INDEX LK_SB_AGENCY ON SB_AGENCY (NAME);
CREATE INDEX FK_SB_AGENCY_REPORTS_1 ON SB_AGENCY_REPORTS (SB_AGENCY_NBR);
CREATE INDEX FK_SB_AGENCY_REPORTS_2 ON SB_AGENCY_REPORTS (SB_REPORTS_NBR);
CREATE INDEX LK_SB_AGENCY_REPORTS ON SB_AGENCY_REPORTS (SB_AGENCY_NBR, SB_REPORTS_NBR);
CREATE INDEX LK_SB_BANKS ON SB_BANKS (NAME);
CREATE INDEX FK_SB_BANK_ACCOUNTS_1 ON SB_BANK_ACCOUNTS (SB_BANKS_NBR);
CREATE INDEX FK_SB_BANK_ACCOUNTS_2 ON SB_BANK_ACCOUNTS (ACH_ORIGIN_SB_BANKS_NBR);
CREATE INDEX FK_SB_BANK_ACCOUNTS_3 ON SB_BANK_ACCOUNTS (LOGO_SB_BLOB_NBR);
CREATE INDEX FK_SB_BANK_ACCOUNTS_4 ON SB_BANK_ACCOUNTS (SIGNATURE_SB_BLOB_NBR);
CREATE INDEX LK_SB_BANK_ACCOUNTS ON SB_BANK_ACCOUNTS (SB_BANKS_NBR, BANK_ACCOUNT_TYPE, CUSTOM_BANK_ACCOUNT_NUMBER);
CREATE INDEX LK_SB_DELIVERY_COMPANY ON SB_DELIVERY_COMPANY (NAME);
CREATE INDEX FK_SB_DELIVERY_COMPANY_SVCS_1 ON SB_DELIVERY_COMPANY_SVCS (SB_DELIVERY_COMPANY_NBR);
CREATE INDEX LK_SB_DELIVERY_COMPANY_SVCS ON SB_DELIVERY_COMPANY_SVCS (DESCRIPTION);
CREATE INDEX FK_SB_DELIVERY_SERVICE_OPT_1 ON SB_DELIVERY_SERVICE_OPT (SB_DELIVERY_SERVICE_NBR);
CREATE INDEX FK_SB_DELIVERY_SERVICE_OPT_2 ON SB_DELIVERY_SERVICE_OPT (SB_DELIVERY_METHOD_NBR);
CREATE INDEX LK_SB_ENLIST_GROUPS ON SB_ENLIST_GROUPS (SY_REPORT_GROUPS_NBR);
CREATE INDEX LK_SB_GLOBAL_AGENCY_CONTACTS ON SB_GLOBAL_AGENCY_CONTACTS (CONTACT_NAME, SY_GLOBAL_AGENCY_NBR);
CREATE INDEX LK_SB_HOLIDAYS ON SB_HOLIDAYS (HOLIDAY_DATE);
CREATE INDEX FK_SB_MAIL_BOX_1 ON SB_MAIL_BOX (UP_LEVEL_MAIL_BOX_NBR);
CREATE INDEX FK_SB_MAIL_BOX_2 ON SB_MAIL_BOX (SB_DELIVERY_METHOD_NBR);
CREATE INDEX FK_SB_MAIL_BOX_3 ON SB_MAIL_BOX (SB_MEDIA_TYPE_NBR);
CREATE INDEX FK_SB_MAIL_BOX_4 ON SB_MAIL_BOX (SB_COVER_LETTER_REPORT_NBR);
CREATE INDEX LK_SB_MAIL_BOX ON SB_MAIL_BOX (BARCODE);
CREATE INDEX FK_SB_MAIL_BOX_CONTENT_1 ON SB_MAIL_BOX_CONTENT (SB_MAIL_BOX_NBR);
CREATE INDEX FK_SB_MAIL_BOX_OPTION_1 ON SB_MAIL_BOX_OPTION (SB_MAIL_BOX_NBR);
CREATE INDEX FK_SB_MAIL_BOX_OPTION_2 ON SB_MAIL_BOX_OPTION (SB_MAIL_BOX_CONTENT_NBR);
CREATE INDEX LK_SB_MEDIA_TYPE ON SB_MEDIA_TYPE (SY_MEDIA_TYPE_NBR);
CREATE INDEX FK_SB_MEDIA_TYPE_OPTION_1 ON SB_MEDIA_TYPE_OPTION (SB_MEDIA_TYPE_NBR);
CREATE INDEX LK_SB_MEDIA_TYPE_OPTION ON SB_MEDIA_TYPE_OPTION (OPTION_TAG, SB_MEDIA_TYPE_NBR);
CREATE INDEX LK_SB_MULTICLIENT_REPORTS ON SB_MULTICLIENT_REPORTS (DESCRIPTION);
CREATE INDEX LK_SB_OPTION ON SB_OPTION (OPTION_TAG);
CREATE INDEX LK_SB_OTHER_SERVICE ON SB_OTHER_SERVICE (NAME);
CREATE INDEX LK_SB_PAPER_INFO ON SB_PAPER_INFO (DESCRIPTION);
CREATE INDEX FK_SB_QUEUE_PRIORITY_1 ON SB_QUEUE_PRIORITY (SB_USER_NBR);
CREATE INDEX FK_SB_QUEUE_PRIORITY_2 ON SB_QUEUE_PRIORITY (SB_SEC_GROUPS_NBR);
CREATE INDEX LK_SB_REFERRALS ON SB_REFERRALS (NAME);
CREATE INDEX LK_SB_REPORTS ON SB_REPORTS (DESCRIPTION);
CREATE INDEX LK_SB_REPORT_WRITER_REPORTS ON SB_REPORT_WRITER_REPORTS (REPORT_DESCRIPTION);
CREATE INDEX LK_SB_SALES_TAX_STATES ON SB_SALES_TAX_STATES (STATE);
CREATE INDEX FK_SB_SEC_CLIENTS_1 ON SB_SEC_CLIENTS (SB_SEC_GROUPS_NBR);
CREATE INDEX FK_SB_SEC_CLIENTS_2 ON SB_SEC_CLIENTS (SB_USER_NBR);
CREATE INDEX LK_SB_SEC_GROUPS ON SB_SEC_GROUPS (NAME);
CREATE INDEX FK_SB_SEC_GROUP_MEMBERS_1 ON SB_SEC_GROUP_MEMBERS (SB_SEC_GROUPS_NBR);
CREATE INDEX FK_SB_SEC_GROUP_MEMBERS_2 ON SB_SEC_GROUP_MEMBERS (SB_USER_NBR);
CREATE INDEX LK_SB_SEC_GROUP_MEMBERS ON SB_SEC_GROUP_MEMBERS (SB_SEC_GROUPS_NBR, SB_USER_NBR);
CREATE INDEX FK_SB_SEC_RIGHTS_1 ON SB_SEC_RIGHTS (SB_SEC_GROUPS_NBR);
CREATE INDEX FK_SB_SEC_RIGHTS_2 ON SB_SEC_RIGHTS (SB_USER_NBR);
CREATE INDEX FK_SB_SEC_ROW_FILTERS_1 ON SB_SEC_ROW_FILTERS (SB_SEC_GROUPS_NBR);
CREATE INDEX FK_SB_SEC_ROW_FILTERS_2 ON SB_SEC_ROW_FILTERS (SB_USER_NBR);
CREATE INDEX LK_SB_SEC_TEMPLATES ON SB_SEC_TEMPLATES (NAME);
CREATE INDEX FK_SB_SERVICES_1 ON SB_SERVICES (SB_REPORTS_NBR);
CREATE INDEX FK_SB_SERVICES_2 ON SB_SERVICES (SB_DELIVERY_METHOD_NBR);
CREATE INDEX LK_SB_SERVICES ON SB_SERVICES (SERVICE_NAME);
CREATE INDEX FK_SB_SERVICES_CALCULATIONS_1 ON SB_SERVICES_CALCULATIONS (SB_SERVICES_NBR);
CREATE INDEX LK_SB_STORAGE ON SB_STORAGE (TAG);
CREATE INDEX FK_SB_TASK_1 ON SB_TASK (SB_USER_NBR);
CREATE INDEX LK_SB_TEAM ON SB_TEAM (TEAM_DESCRIPTION);
CREATE INDEX FK_SB_TEAM_MEMBERS_1 ON SB_TEAM_MEMBERS (SB_TEAM_NBR);
CREATE INDEX FK_SB_TEAM_MEMBERS_2 ON SB_TEAM_MEMBERS (SB_USER_NBR);
CREATE INDEX LK_SB_TEAM_MEMBERS ON SB_TEAM_MEMBERS (SB_TEAM_NBR, SB_USER_NBR);
CREATE INDEX FK_SB_USER_1 ON SB_USER (SB_ACCOUNTANT_NBR);
CREATE INDEX LK_SB_USER ON SB_USER (USER_ID);
CREATE INDEX FK_SB_USER_NOTICE_1 ON SB_USER_NOTICE (SB_USER_NBR);
CREATE INDEX LK_SB_USER_NOTICE ON SB_USER_NOTICE (NAME);


/******************************************************************************/
/****                               Triggers                               ****/
/******************************************************************************/


SET TERM ^ ;



/******************************************************************************/
/****                         Triggers for tables                          ****/
/******************************************************************************/



/* Trigger: T_AD_SB_9 */
CREATE TRIGGER T_AD_SB_9 FOR SB
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, OLD.rec_version, OLD.sb_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 8, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 452, OLD.effective_until);

  /* SB_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 28, OLD.sb_name);

  /* ADDRESS1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 5, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 6, OLD.address2);

  /* CITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 29, OLD.city);

  /* STATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 30, OLD.state);

  /* ZIP_CODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 20, OLD.zip_code);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 31, OLD.e_mail_address);

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);

  /* DEVELOPMENT_MODEM_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 33, OLD.development_modem_number);

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 35, OLD.development_ftp_password);

  /* EIN_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 49, OLD.ein_number);

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);

  /* EFTPS_BANK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);

  /* USE_PRENOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 53, OLD.use_prenote);

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);

  /* PAY_TAX_FROM_PAYABLES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);

  /* AR_EXPORT_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 57, OLD.ar_export_format);

  /* DEFAULT_CHECK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 58, OLD.default_check_format);

  /* MICR_FONT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 59, OLD.micr_font);

  /* MICR_HORIZONTAL_ADJUSTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);

  /* AUTO_SAVE_MINUTES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);

  /* PHONE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 36, OLD.phone);

  /* FAX */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 37, OLD.fax);

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 7, :blob_nbr);
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 4, :blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3, :blob_nbr);
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 27, OLD.ar_import_directory);

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 26, OLD.ach_directory);

  /* SB_URL */
  IF (OLD.sb_url IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 25, OLD.sb_url);

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 18, OLD.days_in_prenote);

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 9, :blob_nbr);
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);

  /* ERROR_SCREEN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 48, OLD.error_screen);

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 15, OLD.pswd_min_length);

  /* PSWD_FORCE_MIXED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);

  /* MISC_CHECK_FORM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 46, OLD.misc_check_form);

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2, :blob_nbr);
  END

  /* MARK_LIABS_PAID_DEFAULT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);

  /* TRUST_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 44, OLD.trust_impound);

  /* TAX_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 43, OLD.tax_impound);

  /* DD_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 42, OLD.dd_impound);

  /* BILLING_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 40, OLD.billing_impound);

  /* WC_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 39, OLD.wc_impound);

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);

  /* SB_EXCEPTION_PAYMENT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);

  /* SB_ACH_FILE_LIMITATIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 543, OLD.dashboard_msg);

  /* EE_LOGIN_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 544, OLD.ee_login_type);

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 550, :blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 551, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_ACCOUNTANT_9 */
CREATE TRIGGER T_AD_SB_ACCOUNTANT_9 FOR SB_ACCOUNTANT
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(2, OLD.rec_version, OLD.sb_accountant_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 66, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 454, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 72, OLD.name);

  /* ADDRESS1 */
  IF (OLD.address1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 62, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 63, OLD.address2);

  /* CITY */
  IF (OLD.city IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 73, OLD.city);

  /* STATE */
  IF (OLD.state IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 79, OLD.state);

  /* ZIP_CODE */
  IF (OLD.zip_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 68, OLD.zip_code);

  /* CONTACT1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 64, OLD.contact1);

  /* PHONE1 */
  IF (OLD.phone1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 74, OLD.phone1);

  /* DESCRIPTION1 */
  IF (OLD.description1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 69, OLD.description1);

  /* CONTACT2 */
  IF (OLD.contact2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 65, OLD.contact2);

  /* PHONE2 */
  IF (OLD.phone2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 75, OLD.phone2);

  /* DESCRIPTION2 */
  IF (OLD.description2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 70, OLD.description2);

  /* FAX */
  IF (OLD.fax IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 76, OLD.fax);

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 71, OLD.fax_description);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 77, OLD.e_mail_address);

  /* PRINT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 78, OLD.print_name);

  /* TITLE */
  IF (OLD.title IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 61, OLD.title);

  /* SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SIGNATURE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 60, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_AGENCY_9 */
CREATE TRIGGER T_AD_SB_AGENCY_9 FOR SB_AGENCY
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(3, OLD.rec_version, OLD.sb_agency_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 85, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 456, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 93, OLD.name);

  /* ADDRESS1 */
  IF (OLD.address1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 80, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 81, OLD.address2);

  /* CITY */
  IF (OLD.city IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 94, OLD.city);

  /* STATE */
  IF (OLD.state IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 95, OLD.state);

  /* ZIP_CODE */
  IF (OLD.zip_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 87, OLD.zip_code);

  /* CONTACT1 */
  IF (OLD.contact1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 82, OLD.contact1);

  /* PHONE1 */
  IF (OLD.phone1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 96, OLD.phone1);

  /* DESCRIPTION1 */
  IF (OLD.description1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 88, OLD.description1);

  /* CONTACT2 */
  IF (OLD.contact2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 83, OLD.contact2);

  /* PHONE2 */
  IF (OLD.phone2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 97, OLD.phone2);

  /* DESCRIPTION2 */
  IF (OLD.description2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 89, OLD.description2);

  /* FAX */
  IF (OLD.fax IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 98, OLD.fax);

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 90, OLD.fax_description);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 99, OLD.e_mail_address);

  /* AGENCY_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 105, OLD.agency_type);

  /* SB_BANKS_NBR */
  IF (OLD.sb_banks_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 91, OLD.sb_banks_nbr);

  /* ACCOUNT_NUMBER */
  IF (OLD.account_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 100, OLD.account_number);

  /* ACCOUNT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 106, OLD.account_type);

  /* NEGATIVE_DIRECT_DEP_ALLOWED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 107, OLD.negative_direct_dep_allowed);

  /* MODEM_NUMBER */
  IF (OLD.modem_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 101, OLD.modem_number);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 84, :blob_nbr);
  END

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 102, OLD.filler);

  /* PRINT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 103, OLD.print_name);

  /* COUNTY */
  IF (OLD.county IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 92, OLD.county);

END
^


/* Trigger: T_AD_SB_AGENCY_REPORTS_9 */
CREATE TRIGGER T_AD_SB_AGENCY_REPORTS_9 FOR SB_AGENCY_REPORTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(4, OLD.rec_version, OLD.sb_agency_reports_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 110, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 458, OLD.effective_until);

  /* SB_AGENCY_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 112, OLD.sb_agency_nbr);

  /* SB_REPORTS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 113, OLD.sb_reports_nbr);

END
^


/* Trigger: T_AD_SB_BANKS_9 */
CREATE TRIGGER T_AD_SB_BANKS_9 FOR SB_BANKS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(5, OLD.rec_version, OLD.sb_banks_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 119, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 460, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 129, OLD.name);

  /* ADDRESS1 */
  IF (OLD.address1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 114, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 115, OLD.address2);

  /* CITY */
  IF (OLD.city IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 130, OLD.city);

  /* STATE */
  IF (OLD.state IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 131, OLD.state);

  /* ZIP_CODE */
  IF (OLD.zip_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 121, OLD.zip_code);

  /* CONTACT1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 116, OLD.contact1);

  /* PHONE1 */
  IF (OLD.phone1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 132, OLD.phone1);

  /* DESCRIPTION1 */
  IF (OLD.description1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 122, OLD.description1);

  /* CONTACT2 */
  IF (OLD.contact2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 117, OLD.contact2);

  /* PHONE2 */
  IF (OLD.phone2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 133, OLD.phone2);

  /* DESCRIPTION2 */
  IF (OLD.description2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 123, OLD.description2);

  /* FAX */
  IF (OLD.fax IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 134, OLD.fax);

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 124, OLD.fax_description);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 135, OLD.e_mail_address);

  /* ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 140, OLD.aba_number);

  /* TOP_ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 125, OLD.top_aba_number);

  /* BOTTOM_ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 126, OLD.bottom_aba_number);

  /* ADDENDA */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 136, OLD.addenda);

  /* CHECK_TEMPLATE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@CHECK_TEMPLATE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@CHECK_TEMPLATE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 118, :blob_nbr);
  END

  /* USE_CHECK_TEMPLATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 141, OLD.use_check_template);

  /* MICR_ACCOUNT_START_POSITION */
  IF (OLD.micr_account_start_position IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 127, OLD.micr_account_start_position);

  /* MICR_CHECK_NUMBER_START_POSITN */
  IF (OLD.micr_check_number_start_positn IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 128, OLD.micr_check_number_start_positn);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 137, OLD.filler);

  /* PRINT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 138, OLD.print_name);

  /* BRANCH_IDENTIFIER */
  IF (OLD.branch_identifier IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 139, OLD.branch_identifier);

  /* ALLOW_HYPHENS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 142, OLD.allow_hyphens);

END
^


/* Trigger: T_AD_SB_BANK_ACCOUNTS_9 */
CREATE TRIGGER T_AD_SB_BANK_ACCOUNTS_9 FOR SB_BANK_ACCOUNTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(6, OLD.rec_version, OLD.sb_bank_accounts_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 143, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 462, OLD.effective_until);

  /* SB_BANKS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 147, OLD.sb_banks_nbr);

  /* CUSTOM_BANK_ACCOUNT_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 156, OLD.custom_bank_account_number);

  /* BANK_ACCOUNT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 172, OLD.bank_account_type);

  /* NAME_DESCRIPTION */
  IF (OLD.name_description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 157, OLD.name_description);

  /* ACH_ORIGIN_SB_BANKS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 148, OLD.ach_origin_sb_banks_nbr);

  /* BANK_RETURNS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 174, OLD.bank_returns);

  /* CUSTOM_HEADER_RECORD */
  IF (OLD.custom_header_record IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 158, OLD.custom_header_record);

  /* NEXT_AVAILABLE_CHECK_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 149, OLD.next_available_check_number);

  /* END_CHECK_NUMBER */
  IF (OLD.end_check_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 150, OLD.end_check_number);

  /* NEXT_BEGIN_CHECK_NUMBER */
  IF (OLD.next_begin_check_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 151, OLD.next_begin_check_number);

  /* NEXT_END_CHECK_NUMBER */
  IF (OLD.next_end_check_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 152, OLD.next_end_check_number);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 159, OLD.filler);

  /* SUPPRESS_OFFSET_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 171, OLD.suppress_offset_account);

  /* BLOCK_NEGATIVE_CHECKS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 170, OLD.block_negative_checks);

  /* BATCH_FILER_ID */
  IF (OLD.batch_filer_id IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 175, OLD.batch_filer_id);

  /* MASTER_INQUIRY_PIN */
  IF (OLD.master_inquiry_pin IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 153, OLD.master_inquiry_pin);

  /* LOGO_SB_BLOB_NBR */
  IF (OLD.logo_sb_blob_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 154, OLD.logo_sb_blob_nbr);

  /* SIGNATURE_SB_BLOB_NBR */
  IF (OLD.signature_sb_blob_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 155, OLD.signature_sb_blob_nbr);

  /* BEGINNING_BALANCE */
  IF (OLD.beginning_balance IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 144, OLD.beginning_balance);

  /* OPERATING_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 166, OLD.operating_account);

  /* BILLING_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 165, OLD.billing_account);

  /* ACH_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 164, OLD.ach_account);

  /* TRUST_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 163, OLD.trust_account);

  /* TAX_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 162, OLD.tax_account);

  /* OBC_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 161, OLD.obc_account);

  /* WORKERS_COMP_ACCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 160, OLD.workers_comp_account);

  /* RECCURING_WIRE_NUMBER */
  IF (OLD.reccuring_wire_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 145, OLD.reccuring_wire_number);

  /* BANK_CHECK */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 553, OLD.bank_check);

END
^


/* Trigger: T_AD_SB_BLOB_9 */
CREATE TRIGGER T_AD_SB_BLOB_9 FOR SB_BLOB
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(7, OLD.rec_version, OLD.sb_blob_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 541, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 542, OLD.effective_until);

  /* DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DATA');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 177, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_DELIVERY_COMPANY_9 */
CREATE TRIGGER T_AD_SB_DELIVERY_COMPANY_9 FOR SB_DELIVERY_COMPANY
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(8, OLD.rec_version, OLD.sb_delivery_company_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 180, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 464, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 182, OLD.name);

  /* DELIVERY_CONTACT */
  IF (OLD.delivery_contact IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 183, OLD.delivery_contact);

  /* DELIVERY_CONTACT_PHONE */
  IF (OLD.delivery_contact_phone IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 184, OLD.delivery_contact_phone);

  /* DELIVERY_CONTACT_PHONE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 188, OLD.delivery_contact_phone_type);

  /* SUPPLIES_CONTACT */
  IF (OLD.supplies_contact IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 185, OLD.supplies_contact);

  /* SUPPLIES_CONTACT_PHONE */
  IF (OLD.supplies_contact_phone IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 186, OLD.supplies_contact_phone);

  /* SUPPLIES_CONTACT_PHONE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 189, OLD.supplies_contact_phone_type);

  /* WEB_SITE */
  IF (OLD.web_site IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 187, OLD.web_site);

  /* DELIVERY_COMPANY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DELIVERY_COMPANY_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@DELIVERY_COMPANY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 179, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_DELIVERY_COMPANY_SVC_9 */
CREATE TRIGGER T_AD_SB_DELIVERY_COMPANY_SVC_9 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(9, OLD.rec_version, OLD.sb_delivery_company_svcs_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 190, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 466, OLD.effective_until);

  /* SB_DELIVERY_COMPANY_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 193, OLD.sb_delivery_company_nbr);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 194, OLD.description);

  /* REFERENCE_FEE */
  IF (OLD.reference_fee IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 191, OLD.reference_fee);

END
^


/* Trigger: T_AD_SB_DELIVERY_METHOD_9 */
CREATE TRIGGER T_AD_SB_DELIVERY_METHOD_9 FOR SB_DELIVERY_METHOD
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(10, OLD.rec_version, OLD.sb_delivery_method_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 195, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 468, OLD.effective_until);

  /* SY_DELIVERY_METHOD_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 196, OLD.sy_delivery_method_nbr);

END
^


/* Trigger: T_AD_SB_DELIVERY_SERVICE_9 */
CREATE TRIGGER T_AD_SB_DELIVERY_SERVICE_9 FOR SB_DELIVERY_SERVICE
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(11, OLD.rec_version, OLD.sb_delivery_service_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 198, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 470, OLD.effective_until);

  /* SY_DELIVERY_SERVICE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 199, OLD.sy_delivery_service_nbr);

END
^


/* Trigger: T_AD_SB_DELIVERY_SERVICE_OPT_9 */
CREATE TRIGGER T_AD_SB_DELIVERY_SERVICE_OPT_9 FOR SB_DELIVERY_SERVICE_OPT
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(12, OLD.rec_version, OLD.sb_delivery_service_opt_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 201, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 472, OLD.effective_until);

  /* SB_DELIVERY_SERVICE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 204, OLD.sb_delivery_service_nbr);

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 203, OLD.sb_delivery_method_nbr);

  /* OPTION_TAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 206, OLD.option_tag);

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 202, OLD.option_integer_value);

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 207, OLD.option_string_value);

END
^


/* Trigger: T_AD_SB_ENLIST_GROUPS_9 */
CREATE TRIGGER T_AD_SB_ENLIST_GROUPS_9 FOR SB_ENLIST_GROUPS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(13, OLD.rec_version, OLD.sb_enlist_groups_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 208, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 474, OLD.effective_until);

  /* SY_REPORT_GROUPS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 209, OLD.sy_report_groups_nbr);

  /* MEDIA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 212, OLD.media_type);

  /* PROCESS_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 211, OLD.process_type);

END
^


/* Trigger: T_AD_SB_GLOBAL_AGENCY_CONTAC_9 */
CREATE TRIGGER T_AD_SB_GLOBAL_AGENCY_CONTAC_9 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(14, OLD.rec_version, OLD.sb_global_agency_contacts_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 215, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 476, OLD.effective_until);

  /* SY_GLOBAL_AGENCY_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 217, OLD.sy_global_agency_nbr);

  /* CONTACT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 213, OLD.contact_name);

  /* PHONE */
  IF (OLD.phone IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 219, OLD.phone);

  /* FAX */
  IF (OLD.fax IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 220, OLD.fax);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 221, OLD.e_mail_address);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 214, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_HOLIDAYS_9 */
CREATE TRIGGER T_AD_SB_HOLIDAYS_9 FOR SB_HOLIDAYS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(15, OLD.rec_version, OLD.sb_holidays_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 222, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 478, OLD.effective_until);

  /* HOLIDAY_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 223, OLD.holiday_date);

  /* HOLIDAY_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 225, OLD.holiday_name);

  /* USED_BY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 226, OLD.used_by);

END
^


/* Trigger: T_AD_SB_MAIL_BOX_9 */
CREATE TRIGGER T_AD_SB_MAIL_BOX_9 FOR SB_MAIL_BOX
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(16, OLD.rec_version, OLD.sb_mail_box_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 233, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 480, OLD.effective_until);

  /* CL_NBR */
  IF (OLD.cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 241, OLD.cl_nbr);

  /* CL_MAIL_BOX_GROUP_NBR */
  IF (OLD.cl_mail_box_group_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 240, OLD.cl_mail_box_group_nbr);

  /* COST */
  IF (OLD.cost IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 232, OLD.cost);

  /* REQUIRED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 251, OLD.required);

  /* NOTIFICATION_EMAIL */
  IF (OLD.notification_email IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 248, OLD.notification_email);

  /* PR_NBR */
  IF (OLD.pr_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 239, OLD.pr_nbr);

  /* RELEASED_TIME */
  IF (OLD.released_time IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 231, OLD.released_time);

  /* PRINTED_TIME */
  IF (OLD.printed_time IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 230, OLD.printed_time);

  /* SB_COVER_LETTER_REPORT_NBR */
  IF (OLD.sb_cover_letter_report_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 238, OLD.sb_cover_letter_report_nbr);

  /* SCANNED_TIME */
  IF (OLD.scanned_time IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 229, OLD.scanned_time);

  /* SHIPPED_TIME */
  IF (OLD.shipped_time IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 228, OLD.shipped_time);

  /* SY_COVER_LETTER_REPORT_NBR */
  IF (OLD.sy_cover_letter_report_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 237, OLD.sy_cover_letter_report_nbr);

  /* NOTE */
  IF (OLD.note IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 247, OLD.note);

  /* UP_LEVEL_MAIL_BOX_NBR */
  IF (OLD.up_level_mail_box_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 236, OLD.up_level_mail_box_nbr);

  /* TRACKING_INFO */
  IF (OLD.tracking_info IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 246, OLD.tracking_info);

  /* AUTO_RELEASE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 250, OLD.auto_release_type);

  /* BARCODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 245, OLD.barcode);

  /* CREATED_TIME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 227, OLD.created_time);

  /* DISPOSE_CONTENT_AFTER_SHIPPING */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 249, OLD.dispose_content_after_shipping);

  /* ADDRESSEE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 244, OLD.addressee);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 243, OLD.description);

  /* SB_DELIVERY_METHOD_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 235, OLD.sb_delivery_method_nbr);

  /* SB_MEDIA_TYPE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 234, OLD.sb_media_type_nbr);

END
^


/* Trigger: T_AD_SB_MAIL_BOX_CONTENT_9 */
CREATE TRIGGER T_AD_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(17, OLD.rec_version, OLD.sb_mail_box_content_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 252, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 482, OLD.effective_until);

  /* SB_MAIL_BOX_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 254, OLD.sb_mail_box_nbr);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 257, OLD.description);

  /* FILE_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 256, OLD.file_name);

  /* MEDIA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 258, OLD.media_type);

  /* PAGE_COUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 253, OLD.page_count);

END
^


/* Trigger: T_AD_SB_MAIL_BOX_OPTION_9 */
CREATE TRIGGER T_AD_SB_MAIL_BOX_OPTION_9 FOR SB_MAIL_BOX_OPTION
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(18, OLD.rec_version, OLD.sb_mail_box_option_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 259, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 484, OLD.effective_until);

  /* SB_MAIL_BOX_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 262, OLD.sb_mail_box_nbr);

  /* SB_MAIL_BOX_CONTENT_NBR */
  IF (OLD.sb_mail_box_content_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 261, OLD.sb_mail_box_content_nbr);

  /* OPTION_TAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 265, OLD.option_tag);

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 260, OLD.option_integer_value);

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 264, OLD.option_string_value);

END
^


/* Trigger: T_AD_SB_MEDIA_TYPE_9 */
CREATE TRIGGER T_AD_SB_MEDIA_TYPE_9 FOR SB_MEDIA_TYPE
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(19, OLD.rec_version, OLD.sb_media_type_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 266, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 486, OLD.effective_until);

  /* SY_MEDIA_TYPE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 267, OLD.sy_media_type_nbr);

END
^


/* Trigger: T_AD_SB_MEDIA_TYPE_OPTION_9 */
CREATE TRIGGER T_AD_SB_MEDIA_TYPE_OPTION_9 FOR SB_MEDIA_TYPE_OPTION
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(20, OLD.rec_version, OLD.sb_media_type_option_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 269, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 488, OLD.effective_until);

  /* SB_MEDIA_TYPE_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 271, OLD.sb_media_type_nbr);

  /* OPTION_TAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 274, OLD.option_tag);

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 270, OLD.option_integer_value);

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 273, OLD.option_string_value);

END
^


/* Trigger: T_AD_SB_MULTICLIENT_REPORTS_9 */
CREATE TRIGGER T_AD_SB_MULTICLIENT_REPORTS_9 FOR SB_MULTICLIENT_REPORTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(21, OLD.rec_version, OLD.sb_multiclient_reports_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 275, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 490, OLD.effective_until);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 279, OLD.description);

  /* REPORT_LEVEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 280, OLD.report_level);

  /* REPORT_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 277, OLD.report_nbr);

  /* INPUT_PARAMS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INPUT_PARAMS');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INPUT_PARAMS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 276, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_OPTION_9 */
CREATE TRIGGER T_AD_SB_OPTION_9 FOR SB_OPTION
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(22, OLD.rec_version, OLD.sb_option_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 281, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 492, OLD.effective_until);

  /* OPTION_TAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 285, OLD.option_tag);

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 282, OLD.option_integer_value);

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 284, OLD.option_string_value);

END
^


/* Trigger: T_AD_SB_OTHER_SERVICE_9 */
CREATE TRIGGER T_AD_SB_OTHER_SERVICE_9 FOR SB_OTHER_SERVICE
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(23, OLD.rec_version, OLD.sb_other_service_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 286, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 494, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 288, OLD.name);

END
^


/* Trigger: T_AD_SB_PAPER_INFO_9 */
CREATE TRIGGER T_AD_SB_PAPER_INFO_9 FOR SB_PAPER_INFO
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(24, OLD.rec_version, OLD.sb_paper_info_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 289, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 496, OLD.effective_until);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 294, OLD.description);

  /* HEIGHT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 292, OLD.height);

  /* WIDTH */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 291, OLD.width);

  /* WEIGHT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 290, OLD.weight);

  /* MEDIA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 295, OLD.media_type);

END
^


/* Trigger: T_AD_SB_QUEUE_PRIORITY_9 */
CREATE TRIGGER T_AD_SB_QUEUE_PRIORITY_9 FOR SB_QUEUE_PRIORITY
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(25, OLD.rec_version, OLD.sb_queue_priority_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 296, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 498, OLD.effective_until);

  /* THREADS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 298, OLD.threads);

  /* METHOD_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 302, OLD.method_name);

  /* PRIORITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 299, OLD.priority);

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 300, OLD.sb_user_nbr);

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 301, OLD.sb_sec_groups_nbr);

END
^


/* Trigger: T_AD_SB_REFERRALS_9 */
CREATE TRIGGER T_AD_SB_REFERRALS_9 FOR SB_REFERRALS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(26, OLD.rec_version, OLD.sb_referrals_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 303, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 500, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 305, OLD.name);

END
^


/* Trigger: T_AD_SB_REPORTS_9 */
CREATE TRIGGER T_AD_SB_REPORTS_9 FOR SB_REPORTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(27, OLD.rec_version, OLD.sb_reports_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 308, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 502, OLD.effective_until);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 311, OLD.description);

  /* COMMENTS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COMMENTS');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@COMMENTS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 307, :blob_nbr);
  END

  /* REPORT_WRITER_REPORTS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 309, OLD.report_writer_reports_nbr);

  /* REPORT_LEVEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 312, OLD.report_level);

  /* INPUT_PARAMS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INPUT_PARAMS');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INPUT_PARAMS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 306, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_REPORT_WRITER_REPORT_9 */
CREATE TRIGGER T_AD_SB_REPORT_WRITER_REPORT_9 FOR SB_REPORT_WRITER_REPORTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(30, OLD.rec_version, OLD.sb_report_writer_reports_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 322, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 508, OLD.effective_until);

  /* REPORT_DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 326, OLD.report_description);

  /* REPORT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 328, OLD.report_type);

  /* REPORT_FILE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_FILE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@REPORT_FILE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 321, :blob_nbr);
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 320, :blob_nbr);
  END

  /* MEDIA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 327, OLD.media_type);

  /* CLASS_NAME */
  IF (OLD.class_name IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 325, OLD.class_name);

  /* ANCESTOR_CLASS_NAME */
  IF (OLD.ancestor_class_name IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 324, OLD.ancestor_class_name);

END
^


/* Trigger: T_AD_SB_SALES_TAX_STATES_9 */
CREATE TRIGGER T_AD_SB_SALES_TAX_STATES_9 FOR SB_SALES_TAX_STATES
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(31, OLD.rec_version, OLD.sb_sales_tax_states_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 329, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 510, OLD.effective_until);

  /* STATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 332, OLD.state);

  /* STATE_TAX_ID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 333, OLD.state_tax_id);

  /* SALES_TAX_PERCENTAGE */
  IF (OLD.sales_tax_percentage IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 330, OLD.sales_tax_percentage);

END
^


/* Trigger: T_AD_SB_SEC_CLIENTS_9 */
CREATE TRIGGER T_AD_SB_SEC_CLIENTS_9 FOR SB_SEC_CLIENTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(32, OLD.rec_version, OLD.sb_sec_clients_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 334, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 512, OLD.effective_until);

  /* CL_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 337, OLD.cl_nbr);

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 336, OLD.sb_sec_groups_nbr);

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 335, OLD.sb_user_nbr);

END
^


/* Trigger: T_AD_SB_SEC_GROUPS_9 */
CREATE TRIGGER T_AD_SB_SEC_GROUPS_9 FOR SB_SEC_GROUPS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(33, OLD.rec_version, OLD.sb_sec_groups_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 339, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 514, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 341, OLD.name);

END
^


/* Trigger: T_AD_SB_SEC_GROUP_MEMBERS_9 */
CREATE TRIGGER T_AD_SB_SEC_GROUP_MEMBERS_9 FOR SB_SEC_GROUP_MEMBERS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(34, OLD.rec_version, OLD.sb_sec_group_members_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 342, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 516, OLD.effective_until);

  /* SB_SEC_GROUPS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 344, OLD.sb_sec_groups_nbr);

  /* SB_USER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 343, OLD.sb_user_nbr);

END
^


/* Trigger: T_AD_SB_SEC_RIGHTS_9 */
CREATE TRIGGER T_AD_SB_SEC_RIGHTS_9 FOR SB_SEC_RIGHTS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(35, OLD.rec_version, OLD.sb_sec_rights_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 346, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 518, OLD.effective_until);

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 348, OLD.sb_sec_groups_nbr);

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 347, OLD.sb_user_nbr);

  /* TAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 351, OLD.tag);

  /* CONTEXT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 350, OLD.context);

END
^


/* Trigger: T_AD_SB_SEC_ROW_FILTERS_9 */
CREATE TRIGGER T_AD_SB_SEC_ROW_FILTERS_9 FOR SB_SEC_ROW_FILTERS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(36, OLD.rec_version, OLD.sb_sec_row_filters_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 352, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 520, OLD.effective_until);

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 355, OLD.sb_sec_groups_nbr);

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 356, OLD.sb_user_nbr);

  /* DATABASE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 358, OLD.database_type);

  /* TABLE_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 357, OLD.table_name);

  /* FILTER_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 359, OLD.filter_type);

  /* CUSTOM_EXPR */
  IF (OLD.custom_expr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 360, OLD.custom_expr);

  /* CL_NBR */
  IF (OLD.cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 353, OLD.cl_nbr);

END
^


/* Trigger: T_AD_SB_SEC_TEMPLATES_9 */
CREATE TRIGGER T_AD_SB_SEC_TEMPLATES_9 FOR SB_SEC_TEMPLATES
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(37, OLD.rec_version, OLD.sb_sec_templates_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 362, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 522, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 364, OLD.name);

  /* TEMPLATE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TEMPLATE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TEMPLATE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 361, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_SERVICES_9 */
CREATE TRIGGER T_AD_SB_SERVICES_9 FOR SB_SERVICES
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, OLD.rec_version, OLD.sb_services_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 367, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 524, OLD.effective_until);

  /* SERVICE_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 374, OLD.service_name);

  /* FREQUENCY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 377, OLD.frequency);

  /* MONTH_NUMBER */
  IF (OLD.month_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 372, OLD.month_number);

  /* BASED_ON_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 378, OLD.based_on_type);

  /* SB_REPORTS_NBR */
  IF (OLD.sb_reports_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 373, OLD.sb_reports_nbr);

  /* COMMISSION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 380, OLD.commission);

  /* SALES_TAXABLE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 381, OLD.sales_taxable);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 375, OLD.filler);

  /* PRODUCT_CODE */
  IF (OLD.product_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 366, OLD.product_code);

  /* WEEK_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 382, OLD.week_number);

  /* SERVICE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 383, OLD.service_type);

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 368, OLD.minimum_amount);

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 369, OLD.maximum_amount);

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 370, OLD.sb_delivery_method_nbr);

  /* TAX_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 376, OLD.tax_type);

END
^


/* Trigger: T_AD_SB_SERVICES_CALCULATION_9 */
CREATE TRIGGER T_AD_SB_SERVICES_CALCULATION_9 FOR SB_SERVICES_CALCULATIONS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(39, OLD.rec_version, OLD.sb_services_calculations_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 388, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 526, OLD.effective_until);

  /* SB_SERVICES_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 398, OLD.sb_services_nbr);

  /* NEXT_MIN_QUANTITY_BEGIN_DATE */
  IF (OLD.next_min_quantity_begin_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 384, OLD.next_min_quantity_begin_date);

  /* NEXT_MAX_QUANTITY_BEGIN_DATE */
  IF (OLD.next_max_quantity_begin_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 385, OLD.next_max_quantity_begin_date);

  /* NEXT_PER_ITEM_BEGIN_DATE */
  IF (OLD.next_per_item_begin_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 386, OLD.next_per_item_begin_date);

  /* NEXT_FLAT_AMOUNT_BEGIN_DATE */
  IF (OLD.next_flat_amount_begin_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 387, OLD.next_flat_amount_begin_date);

  /* MINIMUM_QUANTITY */
  IF (OLD.minimum_quantity IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 389, OLD.minimum_quantity);

  /* NEXT_MINIMUM_QUANTITY */
  IF (OLD.next_minimum_quantity IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 390, OLD.next_minimum_quantity);

  /* MAXIMUM_QUANTITY */
  IF (OLD.maximum_quantity IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 391, OLD.maximum_quantity);

  /* NEXT_MAXIMUM_QUANTITY */
  IF (OLD.next_maximum_quantity IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 392, OLD.next_maximum_quantity);

  /* PER_ITEM_RATE */
  IF (OLD.per_item_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 393, OLD.per_item_rate);

  /* NEXT_PER_ITEM_RATE */
  IF (OLD.next_per_item_rate IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 394, OLD.next_per_item_rate);

  /* FLAT_AMOUNT */
  IF (OLD.flat_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 395, OLD.flat_amount);

  /* NEXT_FLAT_AMOUNT */
  IF (OLD.next_flat_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 396, OLD.next_flat_amount);

END
^


/* Trigger: T_AD_SB_STORAGE_9 */
CREATE TRIGGER T_AD_SB_STORAGE_9 FOR SB_STORAGE
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(46, OLD.rec_version, OLD.sb_storage_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 556, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 557, OLD.effective_until);

  /* TAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 558, OLD.tag);

  /* STORAGE_DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@STORAGE_DATA');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@STORAGE_DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 559, :blob_nbr);
  END

END
^


/* Trigger: T_AD_SB_TASK_9 */
CREATE TRIGGER T_AD_SB_TASK_9 FOR SB_TASK
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(40, OLD.rec_version, OLD.sb_task_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 400, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 528, OLD.effective_until);

  /* SB_USER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 402, OLD.sb_user_nbr);

  /* SCHEDULE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 403, OLD.schedule);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 404, OLD.description);

  /* TASK */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TASK');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TASK', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 399, :blob_nbr);
  END

  /* LAST_RUN */
  IF (OLD.last_run IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 552, OLD.last_run);

END
^


/* Trigger: T_AD_SB_TAX_PAYMENT_9 */
CREATE TRIGGER T_AD_SB_TAX_PAYMENT_9 FOR SB_TAX_PAYMENT
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(41, OLD.rec_version, OLD.sb_tax_payment_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 405, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 530, OLD.effective_until);

  /* DESCRIPTION */
  IF (OLD.description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 408, OLD.description);

  /* STATUS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 409, OLD.status);

  /* STATUS_DATE */
  IF (OLD.status_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 406, OLD.status_date);

END
^


/* Trigger: T_AD_SB_TEAM_9 */
CREATE TRIGGER T_AD_SB_TEAM_9 FOR SB_TEAM
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(42, OLD.rec_version, OLD.sb_team_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 410, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 532, OLD.effective_until);

  /* TEAM_DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 412, OLD.team_description);

  /* CR_CATEGORY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 413, OLD.cr_category);

END
^


/* Trigger: T_AD_SB_TEAM_MEMBERS_9 */
CREATE TRIGGER T_AD_SB_TEAM_MEMBERS_9 FOR SB_TEAM_MEMBERS
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(43, OLD.rec_version, OLD.sb_team_members_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 414, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 534, OLD.effective_until);

  /* SB_TEAM_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 416, OLD.sb_team_nbr);

  /* SB_USER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 417, OLD.sb_user_nbr);

END
^


/* Trigger: T_AD_SB_USER_9 */
CREATE TRIGGER T_AD_SB_USER_9 FOR SB_USER
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, OLD.rec_version, OLD.sb_user_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 422, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 536, OLD.effective_until);

  /* USER_ID */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 435, OLD.user_id);

  /* USER_SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 419, :blob_nbr);
  END

  /* LAST_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 420, OLD.last_name);

  /* FIRST_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 436, OLD.first_name);

  /* MIDDLE_INITIAL */
  IF (OLD.middle_initial IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 440, OLD.middle_initial);

  /* ACTIVE_USER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 441, OLD.active_user);

  /* DEPARTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 442, OLD.department);

  /* PASSWORD_CHANGE_DATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 421, OLD.password_change_date);

  /* SECURITY_LEVEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 443, OLD.security_level);

  /* USER_UPDATE_OPTIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 437, OLD.user_update_options);

  /* USER_FUNCTIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 438, OLD.user_functions);

  /* USER_PASSWORD */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 439, OLD.user_password);

  /* EMAIL_ADDRESS */
  IF (OLD.email_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 434, OLD.email_address);

  /* CL_NBR */
  IF (OLD.cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 429, OLD.cl_nbr);

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 427, OLD.sb_accountant_nbr);

  /* WRONG_PSWD_ATTEMPTS */
  IF (OLD.wrong_pswd_attempts IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 426, OLD.wrong_pswd_attempts);

  /* LINKS_DATA */
  IF (OLD.links_data IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 433, OLD.links_data);

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 425, OLD.login_question1);

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 432, OLD.login_answer1);

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 424, OLD.login_question2);

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 431, OLD.login_answer2);

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 423, OLD.login_question3);

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 430, OLD.login_answer3);

  /* HR_PERSONNEL */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 545, OLD.hr_personnel);

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 546, OLD.sec_question1);

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 547, OLD.sec_answer1);

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 548, OLD.sec_question2);

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 549, OLD.sec_answer2);

END
^


/* Trigger: T_AD_SB_USER_NOTICE_9 */
CREATE TRIGGER T_AD_SB_USER_NOTICE_9 FOR SB_USER_NOTICE
ACTIVE AFTER DELETE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(45, OLD.rec_version, OLD.sb_user_notice_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 539, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 538, OLD.effective_until);

  /* SB_USER_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 448, OLD.sb_user_nbr);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 450, OLD.name);

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 447, :blob_nbr);
  END

  /* TASK */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TASK');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TASK', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 446, :blob_nbr);
  END

  /* LAST_DISMISS */
  IF (OLD.last_dismiss IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 445, OLD.last_dismiss);

  /* NEXT_REMINDER */
  IF (OLD.next_reminder IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 444, OLD.next_reminder);

END
^


/* Trigger: T_AI_SB_9 */
CREATE TRIGGER T_AI_SB_9 FOR SB
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(1, NEW.rec_version, NEW.sb_nbr, 'I');
END
^


/* Trigger: T_AI_SB_ACCOUNTANT_9 */
CREATE TRIGGER T_AI_SB_ACCOUNTANT_9 FOR SB_ACCOUNTANT
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(2, NEW.rec_version, NEW.sb_accountant_nbr, 'I');
END
^


/* Trigger: T_AI_SB_AGENCY_9 */
CREATE TRIGGER T_AI_SB_AGENCY_9 FOR SB_AGENCY
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(3, NEW.rec_version, NEW.sb_agency_nbr, 'I');
END
^


/* Trigger: T_AI_SB_AGENCY_REPORTS_9 */
CREATE TRIGGER T_AI_SB_AGENCY_REPORTS_9 FOR SB_AGENCY_REPORTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(4, NEW.rec_version, NEW.sb_agency_reports_nbr, 'I');
END
^


/* Trigger: T_AI_SB_BANKS_9 */
CREATE TRIGGER T_AI_SB_BANKS_9 FOR SB_BANKS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(5, NEW.rec_version, NEW.sb_banks_nbr, 'I');
END
^


/* Trigger: T_AI_SB_BANK_ACCOUNTS_9 */
CREATE TRIGGER T_AI_SB_BANK_ACCOUNTS_9 FOR SB_BANK_ACCOUNTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(6, NEW.rec_version, NEW.sb_bank_accounts_nbr, 'I');
END
^


/* Trigger: T_AI_SB_BLOB_9 */
CREATE TRIGGER T_AI_SB_BLOB_9 FOR SB_BLOB
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(7, NEW.rec_version, NEW.sb_blob_nbr, 'I');
END
^


/* Trigger: T_AI_SB_DELIVERY_COMPANY_9 */
CREATE TRIGGER T_AI_SB_DELIVERY_COMPANY_9 FOR SB_DELIVERY_COMPANY
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(8, NEW.rec_version, NEW.sb_delivery_company_nbr, 'I');
END
^


/* Trigger: T_AI_SB_DELIVERY_COMPANY_SVC_9 */
CREATE TRIGGER T_AI_SB_DELIVERY_COMPANY_SVC_9 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(9, NEW.rec_version, NEW.sb_delivery_company_svcs_nbr, 'I');
END
^


/* Trigger: T_AI_SB_DELIVERY_METHOD_9 */
CREATE TRIGGER T_AI_SB_DELIVERY_METHOD_9 FOR SB_DELIVERY_METHOD
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(10, NEW.rec_version, NEW.sb_delivery_method_nbr, 'I');
END
^


/* Trigger: T_AI_SB_DELIVERY_SERVICE_9 */
CREATE TRIGGER T_AI_SB_DELIVERY_SERVICE_9 FOR SB_DELIVERY_SERVICE
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(11, NEW.rec_version, NEW.sb_delivery_service_nbr, 'I');
END
^


/* Trigger: T_AI_SB_DELIVERY_SERVICE_OPT_9 */
CREATE TRIGGER T_AI_SB_DELIVERY_SERVICE_OPT_9 FOR SB_DELIVERY_SERVICE_OPT
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(12, NEW.rec_version, NEW.sb_delivery_service_opt_nbr, 'I');
END
^


/* Trigger: T_AI_SB_ENLIST_GROUPS_9 */
CREATE TRIGGER T_AI_SB_ENLIST_GROUPS_9 FOR SB_ENLIST_GROUPS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(13, NEW.rec_version, NEW.sb_enlist_groups_nbr, 'I');
END
^


/* Trigger: T_AI_SB_GLOBAL_AGENCY_CONTAC_9 */
CREATE TRIGGER T_AI_SB_GLOBAL_AGENCY_CONTAC_9 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(14, NEW.rec_version, NEW.sb_global_agency_contacts_nbr, 'I');
END
^


/* Trigger: T_AI_SB_HOLIDAYS_9 */
CREATE TRIGGER T_AI_SB_HOLIDAYS_9 FOR SB_HOLIDAYS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(15, NEW.rec_version, NEW.sb_holidays_nbr, 'I');
END
^


/* Trigger: T_AI_SB_MAIL_BOX_9 */
CREATE TRIGGER T_AI_SB_MAIL_BOX_9 FOR SB_MAIL_BOX
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(16, NEW.rec_version, NEW.sb_mail_box_nbr, 'I');
END
^


/* Trigger: T_AI_SB_MAIL_BOX_CONTENT_9 */
CREATE TRIGGER T_AI_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(17, NEW.rec_version, NEW.sb_mail_box_content_nbr, 'I');
END
^


/* Trigger: T_AI_SB_MAIL_BOX_OPTION_9 */
CREATE TRIGGER T_AI_SB_MAIL_BOX_OPTION_9 FOR SB_MAIL_BOX_OPTION
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(18, NEW.rec_version, NEW.sb_mail_box_option_nbr, 'I');
END
^


/* Trigger: T_AI_SB_MEDIA_TYPE_9 */
CREATE TRIGGER T_AI_SB_MEDIA_TYPE_9 FOR SB_MEDIA_TYPE
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(19, NEW.rec_version, NEW.sb_media_type_nbr, 'I');
END
^


/* Trigger: T_AI_SB_MEDIA_TYPE_OPTION_9 */
CREATE TRIGGER T_AI_SB_MEDIA_TYPE_OPTION_9 FOR SB_MEDIA_TYPE_OPTION
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(20, NEW.rec_version, NEW.sb_media_type_option_nbr, 'I');
END
^


/* Trigger: T_AI_SB_MULTICLIENT_REPORTS_9 */
CREATE TRIGGER T_AI_SB_MULTICLIENT_REPORTS_9 FOR SB_MULTICLIENT_REPORTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(21, NEW.rec_version, NEW.sb_multiclient_reports_nbr, 'I');
END
^


/* Trigger: T_AI_SB_OPTION_9 */
CREATE TRIGGER T_AI_SB_OPTION_9 FOR SB_OPTION
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(22, NEW.rec_version, NEW.sb_option_nbr, 'I');
END
^


/* Trigger: T_AI_SB_OTHER_SERVICE_9 */
CREATE TRIGGER T_AI_SB_OTHER_SERVICE_9 FOR SB_OTHER_SERVICE
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(23, NEW.rec_version, NEW.sb_other_service_nbr, 'I');
END
^


/* Trigger: T_AI_SB_PAPER_INFO_9 */
CREATE TRIGGER T_AI_SB_PAPER_INFO_9 FOR SB_PAPER_INFO
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(24, NEW.rec_version, NEW.sb_paper_info_nbr, 'I');
END
^


/* Trigger: T_AI_SB_QUEUE_PRIORITY_9 */
CREATE TRIGGER T_AI_SB_QUEUE_PRIORITY_9 FOR SB_QUEUE_PRIORITY
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(25, NEW.rec_version, NEW.sb_queue_priority_nbr, 'I');
END
^


/* Trigger: T_AI_SB_REFERRALS_9 */
CREATE TRIGGER T_AI_SB_REFERRALS_9 FOR SB_REFERRALS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(26, NEW.rec_version, NEW.sb_referrals_nbr, 'I');
END
^


/* Trigger: T_AI_SB_REPORTS_9 */
CREATE TRIGGER T_AI_SB_REPORTS_9 FOR SB_REPORTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(27, NEW.rec_version, NEW.sb_reports_nbr, 'I');
END
^


/* Trigger: T_AI_SB_REPORT_WRITER_REPORT_9 */
CREATE TRIGGER T_AI_SB_REPORT_WRITER_REPORT_9 FOR SB_REPORT_WRITER_REPORTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(30, NEW.rec_version, NEW.sb_report_writer_reports_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SALES_TAX_STATES_9 */
CREATE TRIGGER T_AI_SB_SALES_TAX_STATES_9 FOR SB_SALES_TAX_STATES
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(31, NEW.rec_version, NEW.sb_sales_tax_states_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SEC_CLIENTS_9 */
CREATE TRIGGER T_AI_SB_SEC_CLIENTS_9 FOR SB_SEC_CLIENTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(32, NEW.rec_version, NEW.sb_sec_clients_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SEC_GROUPS_9 */
CREATE TRIGGER T_AI_SB_SEC_GROUPS_9 FOR SB_SEC_GROUPS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(33, NEW.rec_version, NEW.sb_sec_groups_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SEC_GROUP_MEMBERS_9 */
CREATE TRIGGER T_AI_SB_SEC_GROUP_MEMBERS_9 FOR SB_SEC_GROUP_MEMBERS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(34, NEW.rec_version, NEW.sb_sec_group_members_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SEC_RIGHTS_9 */
CREATE TRIGGER T_AI_SB_SEC_RIGHTS_9 FOR SB_SEC_RIGHTS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(35, NEW.rec_version, NEW.sb_sec_rights_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SEC_ROW_FILTERS_9 */
CREATE TRIGGER T_AI_SB_SEC_ROW_FILTERS_9 FOR SB_SEC_ROW_FILTERS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(36, NEW.rec_version, NEW.sb_sec_row_filters_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SEC_TEMPLATES_9 */
CREATE TRIGGER T_AI_SB_SEC_TEMPLATES_9 FOR SB_SEC_TEMPLATES
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(37, NEW.rec_version, NEW.sb_sec_templates_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SERVICES_9 */
CREATE TRIGGER T_AI_SB_SERVICES_9 FOR SB_SERVICES
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(38, NEW.rec_version, NEW.sb_services_nbr, 'I');
END
^


/* Trigger: T_AI_SB_SERVICES_CALCULATION_9 */
CREATE TRIGGER T_AI_SB_SERVICES_CALCULATION_9 FOR SB_SERVICES_CALCULATIONS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(39, NEW.rec_version, NEW.sb_services_calculations_nbr, 'I');
END
^


/* Trigger: T_AI_SB_STORAGE_9 */
CREATE TRIGGER T_AI_SB_STORAGE_9 FOR SB_STORAGE
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(46, NEW.rec_version, NEW.sb_storage_nbr, 'I');
END
^


/* Trigger: T_AI_SB_TASK_9 */
CREATE TRIGGER T_AI_SB_TASK_9 FOR SB_TASK
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(40, NEW.rec_version, NEW.sb_task_nbr, 'I');
END
^


/* Trigger: T_AI_SB_TAX_PAYMENT_9 */
CREATE TRIGGER T_AI_SB_TAX_PAYMENT_9 FOR SB_TAX_PAYMENT
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(41, NEW.rec_version, NEW.sb_tax_payment_nbr, 'I');
END
^


/* Trigger: T_AI_SB_TEAM_9 */
CREATE TRIGGER T_AI_SB_TEAM_9 FOR SB_TEAM
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(42, NEW.rec_version, NEW.sb_team_nbr, 'I');
END
^


/* Trigger: T_AI_SB_TEAM_MEMBERS_9 */
CREATE TRIGGER T_AI_SB_TEAM_MEMBERS_9 FOR SB_TEAM_MEMBERS
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(43, NEW.rec_version, NEW.sb_team_members_nbr, 'I');
END
^


/* Trigger: T_AI_SB_USER_9 */
CREATE TRIGGER T_AI_SB_USER_9 FOR SB_USER
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(44, NEW.rec_version, NEW.sb_user_nbr, 'I');
END
^


/* Trigger: T_AI_SB_USER_NOTICE_9 */
CREATE TRIGGER T_AI_SB_USER_NOTICE_9 FOR SB_USER_NOTICE
ACTIVE AFTER INSERT POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(45, NEW.rec_version, NEW.sb_user_notice_nbr, 'I');
END
^


/* Trigger: T_AUD_SB_ACCOUNTANT_2 */
CREATE TRIGGER T_AUD_SB_ACCOUNTANT_2 FOR SB_ACCOUNTANT
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_accountant WHERE sb_accountant_nbr = OLD.sb_accountant_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_accountant WHERE sb_accountant_nbr = OLD.sb_accountant_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_USER */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE (sb_accountant_nbr = OLD.sb_accountant_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_accountant', OLD.sb_accountant_nbr, 'sb_user', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE (sb_accountant_nbr = OLD.sb_accountant_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_accountant', OLD.sb_accountant_nbr, 'sb_user', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_AGENCY_2 */
CREATE TRIGGER T_AUD_SB_AGENCY_2 FOR SB_AGENCY
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_agency WHERE sb_agency_nbr = OLD.sb_agency_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_agency WHERE sb_agency_nbr = OLD.sb_agency_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_AGENCY_REPORTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency_reports
    WHERE (sb_agency_nbr = OLD.sb_agency_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_agency', OLD.sb_agency_nbr, 'sb_agency_reports', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency_reports
    WHERE (sb_agency_nbr = OLD.sb_agency_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_agency', OLD.sb_agency_nbr, 'sb_agency_reports', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_BANKS_2 */
CREATE TRIGGER T_AUD_SB_BANKS_2 FOR SB_BANKS
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_banks WHERE sb_banks_nbr = OLD.sb_banks_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_banks WHERE sb_banks_nbr = OLD.sb_banks_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_AGENCY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_agency', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_agency', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_BANK_ACCOUNTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_BANK_ACCOUNTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (ach_origin_sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (ach_origin_sb_banks_nbr = OLD.sb_banks_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_banks', OLD.sb_banks_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_BLOB_2 */
CREATE TRIGGER T_AUD_SB_BLOB_2 FOR SB_BLOB
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_blob WHERE sb_blob_nbr = OLD.sb_blob_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_blob WHERE sb_blob_nbr = OLD.sb_blob_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_BANK_ACCOUNTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (logo_sb_blob_nbr = OLD.sb_blob_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_blob', OLD.sb_blob_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (logo_sb_blob_nbr = OLD.sb_blob_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_blob', OLD.sb_blob_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_BANK_ACCOUNTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (signature_sb_blob_nbr = OLD.sb_blob_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_blob', OLD.sb_blob_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_bank_accounts
    WHERE (signature_sb_blob_nbr = OLD.sb_blob_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_blob', OLD.sb_blob_nbr, 'sb_bank_accounts', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_DELIVERY_COMPANY_2 */
CREATE TRIGGER T_AUD_SB_DELIVERY_COMPANY_2 FOR SB_DELIVERY_COMPANY
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_delivery_company WHERE sb_delivery_company_nbr = OLD.sb_delivery_company_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_delivery_company WHERE sb_delivery_company_nbr = OLD.sb_delivery_company_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_DELIVERY_COMPANY_SVCS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_company_svcs
    WHERE (sb_delivery_company_nbr = OLD.sb_delivery_company_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_company', OLD.sb_delivery_company_nbr, 'sb_delivery_company_svcs', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_company_svcs
    WHERE (sb_delivery_company_nbr = OLD.sb_delivery_company_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_company', OLD.sb_delivery_company_nbr, 'sb_delivery_company_svcs', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_DELIVERY_METHOD_2 */
CREATE TRIGGER T_AUD_SB_DELIVERY_METHOD_2 FOR SB_DELIVERY_METHOD
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_delivery_method WHERE sb_delivery_method_nbr = OLD.sb_delivery_method_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_delivery_method WHERE sb_delivery_method_nbr = OLD.sb_delivery_method_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_SERVICES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_services
    WHERE (sb_delivery_method_nbr = OLD.sb_delivery_method_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_method', OLD.sb_delivery_method_nbr, 'sb_services', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_services
    WHERE (sb_delivery_method_nbr = OLD.sb_delivery_method_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_method', OLD.sb_delivery_method_nbr, 'sb_services', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_DELIVERY_SERVICE_OPT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_service_opt
    WHERE (sb_delivery_method_nbr = OLD.sb_delivery_method_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_method', OLD.sb_delivery_method_nbr, 'sb_delivery_service_opt', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_service_opt
    WHERE (sb_delivery_method_nbr = OLD.sb_delivery_method_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_method', OLD.sb_delivery_method_nbr, 'sb_delivery_service_opt', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_MAIL_BOX */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (sb_delivery_method_nbr = OLD.sb_delivery_method_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_method', OLD.sb_delivery_method_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (sb_delivery_method_nbr = OLD.sb_delivery_method_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_method', OLD.sb_delivery_method_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_DELIVERY_SERVICE_2 */
CREATE TRIGGER T_AUD_SB_DELIVERY_SERVICE_2 FOR SB_DELIVERY_SERVICE
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_delivery_service WHERE sb_delivery_service_nbr = OLD.sb_delivery_service_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_delivery_service WHERE sb_delivery_service_nbr = OLD.sb_delivery_service_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_DELIVERY_SERVICE_OPT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_service_opt
    WHERE (sb_delivery_service_nbr = OLD.sb_delivery_service_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_service', OLD.sb_delivery_service_nbr, 'sb_delivery_service_opt', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_service_opt
    WHERE (sb_delivery_service_nbr = OLD.sb_delivery_service_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_delivery_service', OLD.sb_delivery_service_nbr, 'sb_delivery_service_opt', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_MAIL_BOX_2 */
CREATE TRIGGER T_AUD_SB_MAIL_BOX_2 FOR SB_MAIL_BOX
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_mail_box WHERE sb_mail_box_nbr = OLD.sb_mail_box_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_mail_box WHERE sb_mail_box_nbr = OLD.sb_mail_box_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_MAIL_BOX */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (up_level_mail_box_nbr = OLD.sb_mail_box_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box', OLD.sb_mail_box_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (up_level_mail_box_nbr = OLD.sb_mail_box_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box', OLD.sb_mail_box_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_MAIL_BOX_CONTENT */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_content
    WHERE (sb_mail_box_nbr = OLD.sb_mail_box_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box', OLD.sb_mail_box_nbr, 'sb_mail_box_content', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_content
    WHERE (sb_mail_box_nbr = OLD.sb_mail_box_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box', OLD.sb_mail_box_nbr, 'sb_mail_box_content', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_MAIL_BOX_OPTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_option
    WHERE (sb_mail_box_nbr = OLD.sb_mail_box_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box', OLD.sb_mail_box_nbr, 'sb_mail_box_option', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_option
    WHERE (sb_mail_box_nbr = OLD.sb_mail_box_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box', OLD.sb_mail_box_nbr, 'sb_mail_box_option', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_MAIL_BOX_CONTENT_2 */
CREATE TRIGGER T_AUD_SB_MAIL_BOX_CONTENT_2 FOR SB_MAIL_BOX_CONTENT
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_mail_box_content WHERE sb_mail_box_content_nbr = OLD.sb_mail_box_content_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_mail_box_content WHERE sb_mail_box_content_nbr = OLD.sb_mail_box_content_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_MAIL_BOX_OPTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_option
    WHERE (sb_mail_box_content_nbr = OLD.sb_mail_box_content_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box_content', OLD.sb_mail_box_content_nbr, 'sb_mail_box_option', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_option
    WHERE (sb_mail_box_content_nbr = OLD.sb_mail_box_content_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_mail_box_content', OLD.sb_mail_box_content_nbr, 'sb_mail_box_option', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_MEDIA_TYPE_2 */
CREATE TRIGGER T_AUD_SB_MEDIA_TYPE_2 FOR SB_MEDIA_TYPE
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_media_type WHERE sb_media_type_nbr = OLD.sb_media_type_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_media_type WHERE sb_media_type_nbr = OLD.sb_media_type_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_MEDIA_TYPE_OPTION */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_media_type_option
    WHERE (sb_media_type_nbr = OLD.sb_media_type_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_media_type', OLD.sb_media_type_nbr, 'sb_media_type_option', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_media_type_option
    WHERE (sb_media_type_nbr = OLD.sb_media_type_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_media_type', OLD.sb_media_type_nbr, 'sb_media_type_option', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_MAIL_BOX */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (sb_media_type_nbr = OLD.sb_media_type_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_media_type', OLD.sb_media_type_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (sb_media_type_nbr = OLD.sb_media_type_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_media_type', OLD.sb_media_type_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_REPORTS_2 */
CREATE TRIGGER T_AUD_SB_REPORTS_2 FOR SB_REPORTS
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_reports WHERE sb_reports_nbr = OLD.sb_reports_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_reports WHERE sb_reports_nbr = OLD.sb_reports_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_AGENCY_REPORTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency_reports
    WHERE (sb_reports_nbr = OLD.sb_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_reports', OLD.sb_reports_nbr, 'sb_agency_reports', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_agency_reports
    WHERE (sb_reports_nbr = OLD.sb_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_reports', OLD.sb_reports_nbr, 'sb_agency_reports', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SERVICES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_services
    WHERE (sb_reports_nbr = OLD.sb_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_reports', OLD.sb_reports_nbr, 'sb_services', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_services
    WHERE (sb_reports_nbr = OLD.sb_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_reports', OLD.sb_reports_nbr, 'sb_services', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_REPORT_WRITER_REPORT_2 */
CREATE TRIGGER T_AUD_SB_REPORT_WRITER_REPORT_2 FOR SB_REPORT_WRITER_REPORTS
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_report_writer_reports WHERE sb_report_writer_reports_nbr = OLD.sb_report_writer_reports_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_report_writer_reports WHERE sb_report_writer_reports_nbr = OLD.sb_report_writer_reports_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_MAIL_BOX */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (sb_cover_letter_report_nbr = OLD.sb_report_writer_reports_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_report_writer_reports', OLD.sb_report_writer_reports_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE (sb_cover_letter_report_nbr = OLD.sb_report_writer_reports_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_report_writer_reports', OLD.sb_report_writer_reports_nbr, 'sb_mail_box', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_SEC_GROUPS_2 */
CREATE TRIGGER T_AUD_SB_SEC_GROUPS_2 FOR SB_SEC_GROUPS
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_sec_groups WHERE sb_sec_groups_nbr = OLD.sb_sec_groups_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_sec_groups WHERE sb_sec_groups_nbr = OLD.sb_sec_groups_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_QUEUE_PRIORITY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_queue_priority
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_queue_priority', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_queue_priority
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_queue_priority', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_CLIENTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_clients
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_clients', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_clients
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_clients', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_GROUP_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_group_members
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_group_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_group_members
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_group_members', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_RIGHTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_rights
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_rights', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_rights
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_rights', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_ROW_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_row_filters
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_row_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_row_filters
    WHERE (sb_sec_groups_nbr = OLD.sb_sec_groups_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_sec_groups', OLD.sb_sec_groups_nbr, 'sb_sec_row_filters', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_SERVICES_2 */
CREATE TRIGGER T_AUD_SB_SERVICES_2 FOR SB_SERVICES
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_services WHERE sb_services_nbr = OLD.sb_services_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_services WHERE sb_services_nbr = OLD.sb_services_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_SERVICES_CALCULATIONS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_services_calculations
    WHERE (sb_services_nbr = OLD.sb_services_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_services', OLD.sb_services_nbr, 'sb_services_calculations', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_services_calculations
    WHERE (sb_services_nbr = OLD.sb_services_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_services', OLD.sb_services_nbr, 'sb_services_calculations', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_TEAM_2 */
CREATE TRIGGER T_AUD_SB_TEAM_2 FOR SB_TEAM
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_team WHERE sb_team_nbr = OLD.sb_team_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_team WHERE sb_team_nbr = OLD.sb_team_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_TEAM_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_team_members
    WHERE (sb_team_nbr = OLD.sb_team_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_team', OLD.sb_team_nbr, 'sb_team_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_team_members
    WHERE (sb_team_nbr = OLD.sb_team_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_team', OLD.sb_team_nbr, 'sb_team_members', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AUD_SB_USER_2 */
CREATE TRIGGER T_AUD_SB_USER_2 FOR SB_USER
ACTIVE AFTER UPDATE OR DELETE POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_user WHERE sb_user_nbr = OLD.sb_user_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_user WHERE sb_user_nbr = OLD.sb_user_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_QUEUE_PRIORITY */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_queue_priority
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_queue_priority', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_queue_priority
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_queue_priority', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_CLIENTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_clients
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_clients', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_clients
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_clients', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_GROUP_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_group_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_group_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_group_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_group_members', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_RIGHTS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_rights
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_rights', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_rights
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_rights', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_SEC_ROW_FILTERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_row_filters
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_row_filters', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_row_filters
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_sec_row_filters', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_TEAM_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_team_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_team_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_team_members
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_team_members', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_TASK */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_task
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_task', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_task
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_task', OLD.effective_date, OLD.effective_until);
  END

  /* Check children in SB_USER_NOTICE */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user_notice
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_user_notice', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_user_notice
    WHERE (sb_user_nbr = OLD.sb_user_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_user', OLD.sb_user_nbr, 'sb_user_notice', OLD.effective_date, OLD.effective_until);
  END
END
^


/* Trigger: T_AU_SB_9 */
CREATE TRIGGER T_AU_SB_9 FOR SB
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, NEW.rec_version, NEW.sb_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 8, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 452, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_NAME */
  IF (OLD.sb_name IS DISTINCT FROM NEW.sb_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 28, OLD.sb_name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 5, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 6, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 29, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 30, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 20, OLD.zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 31, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS DISTINCT FROM NEW.parent_sb_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_MODEM_NUMBER */
  IF (OLD.development_modem_number IS DISTINCT FROM NEW.development_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 33, OLD.development_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS DISTINCT FROM NEW.development_ftp_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 35, OLD.development_ftp_password);
    changes = changes + 1;
  END

  /* EIN_NUMBER */
  IF (OLD.ein_number IS DISTINCT FROM NEW.ein_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 49, OLD.ein_number);
    changes = changes + 1;
  END

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS DISTINCT FROM NEW.eftps_tin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);
    changes = changes + 1;
  END

  /* EFTPS_BANK_FORMAT */
  IF (OLD.eftps_bank_format IS DISTINCT FROM NEW.eftps_bank_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);
    changes = changes + 1;
  END

  /* USE_PRENOTE */
  IF (OLD.use_prenote IS DISTINCT FROM NEW.use_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 53, OLD.use_prenote);
    changes = changes + 1;
  END

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  IF (OLD.impound_trust_monies_as_receiv IS DISTINCT FROM NEW.impound_trust_monies_as_receiv) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);
    changes = changes + 1;
  END

  /* PAY_TAX_FROM_PAYABLES */
  IF (OLD.pay_tax_from_payables IS DISTINCT FROM NEW.pay_tax_from_payables) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);
    changes = changes + 1;
  END

  /* AR_EXPORT_FORMAT */
  IF (OLD.ar_export_format IS DISTINCT FROM NEW.ar_export_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 57, OLD.ar_export_format);
    changes = changes + 1;
  END

  /* DEFAULT_CHECK_FORMAT */
  IF (OLD.default_check_format IS DISTINCT FROM NEW.default_check_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 58, OLD.default_check_format);
    changes = changes + 1;
  END

  /* MICR_FONT */
  IF (OLD.micr_font IS DISTINCT FROM NEW.micr_font) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 59, OLD.micr_font);
    changes = changes + 1;
  END

  /* MICR_HORIZONTAL_ADJUSTMENT */
  IF (OLD.micr_horizontal_adjustment IS DISTINCT FROM NEW.micr_horizontal_adjustment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);
    changes = changes + 1;
  END

  /* AUTO_SAVE_MINUTES */
  IF (OLD.auto_save_minutes IS DISTINCT FROM NEW.auto_save_minutes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);
    changes = changes + 1;
  END

  /* PHONE */
  IF (OLD.phone IS DISTINCT FROM NEW.phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 36, OLD.phone);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 37, OLD.fax);
    changes = changes + 1;
  END

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 7, :blob_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 4, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3, :blob_nbr);
    changes = changes + 1;
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS DISTINCT FROM NEW.ar_import_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 27, OLD.ar_import_directory);
    changes = changes + 1;
  END

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS DISTINCT FROM NEW.ach_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 26, OLD.ach_directory);
    changes = changes + 1;
  END

  /* SB_URL */
  IF (OLD.sb_url IS DISTINCT FROM NEW.sb_url) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 25, OLD.sb_url);
    changes = changes + 1;
  END

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS DISTINCT FROM NEW.days_in_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 18, OLD.days_in_prenote);
    changes = changes + 1;
  END

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 9, :blob_nbr);
    changes = changes + 1;
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS DISTINCT FROM NEW.user_password_duration_in_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);
    changes = changes + 1;
  END

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS DISTINCT FROM NEW.dummy_tax_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);
    changes = changes + 1;
  END

  /* ERROR_SCREEN */
  IF (OLD.error_screen IS DISTINCT FROM NEW.error_screen) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 48, OLD.error_screen);
    changes = changes + 1;
  END

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS DISTINCT FROM NEW.pswd_min_length) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 15, OLD.pswd_min_length);
    changes = changes + 1;
  END

  /* PSWD_FORCE_MIXED */
  IF (OLD.pswd_force_mixed IS DISTINCT FROM NEW.pswd_force_mixed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 46, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2, :blob_nbr);
    changes = changes + 1;
  END

  /* MARK_LIABS_PAID_DEFAULT */
  IF (OLD.mark_liabs_paid_default IS DISTINCT FROM NEW.mark_liabs_paid_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 44, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 43, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 42, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 40, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 39, OLD.wc_impound);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* SB_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.sb_exception_payment_type IS DISTINCT FROM NEW.sb_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);
    changes = changes + 1;
  END

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS DISTINCT FROM NEW.sb_max_ach_file_total) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);
    changes = changes + 1;
  END

  /* SB_ACH_FILE_LIMITATIONS */
  IF (OLD.sb_ach_file_limitations IS DISTINCT FROM NEW.sb_ach_file_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);
    changes = changes + 1;
  END

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS DISTINCT FROM NEW.sb_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS DISTINCT FROM NEW.dashboard_msg) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.dashboard_msg);
    changes = changes + 1;
  END

  /* EE_LOGIN_TYPE */
  IF (OLD.ee_login_type IS DISTINCT FROM NEW.ee_login_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.ee_login_type);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_ACCOUNTANT_9 */
CREATE TRIGGER T_AU_SB_ACCOUNTANT_9 FOR SB_ACCOUNTANT
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(2, NEW.rec_version, NEW.sb_accountant_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 66, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 454, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 72, OLD.name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 62, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 63, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 73, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 79, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 68, OLD.zip_code);
    changes = changes + 1;
  END

  /* CONTACT1 */
  IF (OLD.contact1 IS DISTINCT FROM NEW.contact1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 64, OLD.contact1);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 74, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 69, OLD.description1);
    changes = changes + 1;
  END

  /* CONTACT2 */
  IF (OLD.contact2 IS DISTINCT FROM NEW.contact2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 65, OLD.contact2);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 75, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 70, OLD.description2);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 76, OLD.fax);
    changes = changes + 1;
  END

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS DISTINCT FROM NEW.fax_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 71, OLD.fax_description);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 77, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PRINT_NAME */
  IF (OLD.print_name IS DISTINCT FROM NEW.print_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 78, OLD.print_name);
    changes = changes + 1;
  END

  /* TITLE */
  IF (OLD.title IS DISTINCT FROM NEW.title) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 61, OLD.title);
    changes = changes + 1;
  END

  /* SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SIGNATURE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 60, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_AGENCY_9 */
CREATE TRIGGER T_AU_SB_AGENCY_9 FOR SB_AGENCY
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(3, NEW.rec_version, NEW.sb_agency_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 85, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 456, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 93, OLD.name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 80, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 81, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 94, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 95, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 87, OLD.zip_code);
    changes = changes + 1;
  END

  /* CONTACT1 */
  IF (OLD.contact1 IS DISTINCT FROM NEW.contact1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 82, OLD.contact1);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 96, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 88, OLD.description1);
    changes = changes + 1;
  END

  /* CONTACT2 */
  IF (OLD.contact2 IS DISTINCT FROM NEW.contact2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 83, OLD.contact2);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 97, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 89, OLD.description2);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 98, OLD.fax);
    changes = changes + 1;
  END

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS DISTINCT FROM NEW.fax_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 90, OLD.fax_description);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 99, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* AGENCY_TYPE */
  IF (OLD.agency_type IS DISTINCT FROM NEW.agency_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 105, OLD.agency_type);
    changes = changes + 1;
  END

  /* SB_BANKS_NBR */
  IF (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 91, OLD.sb_banks_nbr);
    changes = changes + 1;
  END

  /* ACCOUNT_NUMBER */
  IF (OLD.account_number IS DISTINCT FROM NEW.account_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 100, OLD.account_number);
    changes = changes + 1;
  END

  /* ACCOUNT_TYPE */
  IF (OLD.account_type IS DISTINCT FROM NEW.account_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 106, OLD.account_type);
    changes = changes + 1;
  END

  /* NEGATIVE_DIRECT_DEP_ALLOWED */
  IF (OLD.negative_direct_dep_allowed IS DISTINCT FROM NEW.negative_direct_dep_allowed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 107, OLD.negative_direct_dep_allowed);
    changes = changes + 1;
  END

  /* MODEM_NUMBER */
  IF (OLD.modem_number IS DISTINCT FROM NEW.modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 101, OLD.modem_number);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 84, :blob_nbr);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 102, OLD.filler);
    changes = changes + 1;
  END

  /* PRINT_NAME */
  IF (OLD.print_name IS DISTINCT FROM NEW.print_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 103, OLD.print_name);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 92, OLD.county);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_AGENCY_REPORTS_9 */
CREATE TRIGGER T_AU_SB_AGENCY_REPORTS_9 FOR SB_AGENCY_REPORTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(4, NEW.rec_version, NEW.sb_agency_reports_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 110, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 458, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_AGENCY_NBR */
  IF (OLD.sb_agency_nbr IS DISTINCT FROM NEW.sb_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 112, OLD.sb_agency_nbr);
    changes = changes + 1;
  END

  /* SB_REPORTS_NBR */
  IF (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 113, OLD.sb_reports_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_BANKS_9 */
CREATE TRIGGER T_AU_SB_BANKS_9 FOR SB_BANKS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(5, NEW.rec_version, NEW.sb_banks_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 119, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 460, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 129, OLD.name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 114, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 115, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 130, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 131, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 121, OLD.zip_code);
    changes = changes + 1;
  END

  /* CONTACT1 */
  IF (OLD.contact1 IS DISTINCT FROM NEW.contact1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 116, OLD.contact1);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 132, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 122, OLD.description1);
    changes = changes + 1;
  END

  /* CONTACT2 */
  IF (OLD.contact2 IS DISTINCT FROM NEW.contact2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 117, OLD.contact2);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 133, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 123, OLD.description2);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 134, OLD.fax);
    changes = changes + 1;
  END

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS DISTINCT FROM NEW.fax_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 124, OLD.fax_description);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 135, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* ABA_NUMBER */
  IF (OLD.aba_number IS DISTINCT FROM NEW.aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 140, OLD.aba_number);
    changes = changes + 1;
  END

  /* TOP_ABA_NUMBER */
  IF (OLD.top_aba_number IS DISTINCT FROM NEW.top_aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 125, OLD.top_aba_number);
    changes = changes + 1;
  END

  /* BOTTOM_ABA_NUMBER */
  IF (OLD.bottom_aba_number IS DISTINCT FROM NEW.bottom_aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 126, OLD.bottom_aba_number);
    changes = changes + 1;
  END

  /* ADDENDA */
  IF (OLD.addenda IS DISTINCT FROM NEW.addenda) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 136, OLD.addenda);
    changes = changes + 1;
  END

  /* CHECK_TEMPLATE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@CHECK_TEMPLATE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@CHECK_TEMPLATE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 118, :blob_nbr);
    changes = changes + 1;
  END

  /* USE_CHECK_TEMPLATE */
  IF (OLD.use_check_template IS DISTINCT FROM NEW.use_check_template) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 141, OLD.use_check_template);
    changes = changes + 1;
  END

  /* MICR_ACCOUNT_START_POSITION */
  IF (OLD.micr_account_start_position IS DISTINCT FROM NEW.micr_account_start_position) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 127, OLD.micr_account_start_position);
    changes = changes + 1;
  END

  /* MICR_CHECK_NUMBER_START_POSITN */
  IF (OLD.micr_check_number_start_positn IS DISTINCT FROM NEW.micr_check_number_start_positn) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 128, OLD.micr_check_number_start_positn);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 137, OLD.filler);
    changes = changes + 1;
  END

  /* PRINT_NAME */
  IF (OLD.print_name IS DISTINCT FROM NEW.print_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 138, OLD.print_name);
    changes = changes + 1;
  END

  /* BRANCH_IDENTIFIER */
  IF (OLD.branch_identifier IS DISTINCT FROM NEW.branch_identifier) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 139, OLD.branch_identifier);
    changes = changes + 1;
  END

  /* ALLOW_HYPHENS */
  IF (OLD.allow_hyphens IS DISTINCT FROM NEW.allow_hyphens) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 142, OLD.allow_hyphens);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_BANK_ACCOUNTS_9 */
CREATE TRIGGER T_AU_SB_BANK_ACCOUNTS_9 FOR SB_BANK_ACCOUNTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(6, NEW.rec_version, NEW.sb_bank_accounts_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 143, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 462, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_BANKS_NBR */
  IF (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 147, OLD.sb_banks_nbr);
    changes = changes + 1;
  END

  /* CUSTOM_BANK_ACCOUNT_NUMBER */
  IF (OLD.custom_bank_account_number IS DISTINCT FROM NEW.custom_bank_account_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 156, OLD.custom_bank_account_number);
    changes = changes + 1;
  END

  /* BANK_ACCOUNT_TYPE */
  IF (OLD.bank_account_type IS DISTINCT FROM NEW.bank_account_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 172, OLD.bank_account_type);
    changes = changes + 1;
  END

  /* NAME_DESCRIPTION */
  IF (OLD.name_description IS DISTINCT FROM NEW.name_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 157, OLD.name_description);
    changes = changes + 1;
  END

  /* ACH_ORIGIN_SB_BANKS_NBR */
  IF (OLD.ach_origin_sb_banks_nbr IS DISTINCT FROM NEW.ach_origin_sb_banks_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 148, OLD.ach_origin_sb_banks_nbr);
    changes = changes + 1;
  END

  /* BANK_RETURNS */
  IF (OLD.bank_returns IS DISTINCT FROM NEW.bank_returns) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 174, OLD.bank_returns);
    changes = changes + 1;
  END

  /* CUSTOM_HEADER_RECORD */
  IF (OLD.custom_header_record IS DISTINCT FROM NEW.custom_header_record) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 158, OLD.custom_header_record);
    changes = changes + 1;
  END

  /* NEXT_AVAILABLE_CHECK_NUMBER */
  IF (OLD.next_available_check_number IS DISTINCT FROM NEW.next_available_check_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 149, OLD.next_available_check_number);
    changes = changes + 1;
  END

  /* END_CHECK_NUMBER */
  IF (OLD.end_check_number IS DISTINCT FROM NEW.end_check_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 150, OLD.end_check_number);
    changes = changes + 1;
  END

  /* NEXT_BEGIN_CHECK_NUMBER */
  IF (OLD.next_begin_check_number IS DISTINCT FROM NEW.next_begin_check_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 151, OLD.next_begin_check_number);
    changes = changes + 1;
  END

  /* NEXT_END_CHECK_NUMBER */
  IF (OLD.next_end_check_number IS DISTINCT FROM NEW.next_end_check_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 152, OLD.next_end_check_number);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 159, OLD.filler);
    changes = changes + 1;
  END

  /* SUPPRESS_OFFSET_ACCOUNT */
  IF (OLD.suppress_offset_account IS DISTINCT FROM NEW.suppress_offset_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 171, OLD.suppress_offset_account);
    changes = changes + 1;
  END

  /* BLOCK_NEGATIVE_CHECKS */
  IF (OLD.block_negative_checks IS DISTINCT FROM NEW.block_negative_checks) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 170, OLD.block_negative_checks);
    changes = changes + 1;
  END

  /* BATCH_FILER_ID */
  IF (OLD.batch_filer_id IS DISTINCT FROM NEW.batch_filer_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 175, OLD.batch_filer_id);
    changes = changes + 1;
  END

  /* MASTER_INQUIRY_PIN */
  IF (OLD.master_inquiry_pin IS DISTINCT FROM NEW.master_inquiry_pin) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 153, OLD.master_inquiry_pin);
    changes = changes + 1;
  END

  /* LOGO_SB_BLOB_NBR */
  IF (OLD.logo_sb_blob_nbr IS DISTINCT FROM NEW.logo_sb_blob_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 154, OLD.logo_sb_blob_nbr);
    changes = changes + 1;
  END

  /* SIGNATURE_SB_BLOB_NBR */
  IF (OLD.signature_sb_blob_nbr IS DISTINCT FROM NEW.signature_sb_blob_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 155, OLD.signature_sb_blob_nbr);
    changes = changes + 1;
  END

  /* BEGINNING_BALANCE */
  IF (OLD.beginning_balance IS DISTINCT FROM NEW.beginning_balance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 144, OLD.beginning_balance);
    changes = changes + 1;
  END

  /* OPERATING_ACCOUNT */
  IF (OLD.operating_account IS DISTINCT FROM NEW.operating_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 166, OLD.operating_account);
    changes = changes + 1;
  END

  /* BILLING_ACCOUNT */
  IF (OLD.billing_account IS DISTINCT FROM NEW.billing_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 165, OLD.billing_account);
    changes = changes + 1;
  END

  /* ACH_ACCOUNT */
  IF (OLD.ach_account IS DISTINCT FROM NEW.ach_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 164, OLD.ach_account);
    changes = changes + 1;
  END

  /* TRUST_ACCOUNT */
  IF (OLD.trust_account IS DISTINCT FROM NEW.trust_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 163, OLD.trust_account);
    changes = changes + 1;
  END

  /* TAX_ACCOUNT */
  IF (OLD.tax_account IS DISTINCT FROM NEW.tax_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 162, OLD.tax_account);
    changes = changes + 1;
  END

  /* OBC_ACCOUNT */
  IF (OLD.obc_account IS DISTINCT FROM NEW.obc_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 161, OLD.obc_account);
    changes = changes + 1;
  END

  /* WORKERS_COMP_ACCOUNT */
  IF (OLD.workers_comp_account IS DISTINCT FROM NEW.workers_comp_account) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 160, OLD.workers_comp_account);
    changes = changes + 1;
  END

  /* RECCURING_WIRE_NUMBER */
  IF (OLD.reccuring_wire_number IS DISTINCT FROM NEW.reccuring_wire_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 145, OLD.reccuring_wire_number);
    changes = changes + 1;
  END

  /* BANK_CHECK */
  IF (OLD.bank_check IS DISTINCT FROM NEW.bank_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 553, OLD.bank_check);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_BLOB_9 */
CREATE TRIGGER T_AU_SB_BLOB_9 FOR SB_BLOB
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(7, NEW.rec_version, NEW.sb_blob_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 541, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 542, OLD.effective_until);
    changes = changes + 1;
  END

  /* DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DATA');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 177, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_DELIVERY_COMPANY_9 */
CREATE TRIGGER T_AU_SB_DELIVERY_COMPANY_9 FOR SB_DELIVERY_COMPANY
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(8, NEW.rec_version, NEW.sb_delivery_company_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 180, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 464, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 182, OLD.name);
    changes = changes + 1;
  END

  /* DELIVERY_CONTACT */
  IF (OLD.delivery_contact IS DISTINCT FROM NEW.delivery_contact) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 183, OLD.delivery_contact);
    changes = changes + 1;
  END

  /* DELIVERY_CONTACT_PHONE */
  IF (OLD.delivery_contact_phone IS DISTINCT FROM NEW.delivery_contact_phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 184, OLD.delivery_contact_phone);
    changes = changes + 1;
  END

  /* DELIVERY_CONTACT_PHONE_TYPE */
  IF (OLD.delivery_contact_phone_type IS DISTINCT FROM NEW.delivery_contact_phone_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 188, OLD.delivery_contact_phone_type);
    changes = changes + 1;
  END

  /* SUPPLIES_CONTACT */
  IF (OLD.supplies_contact IS DISTINCT FROM NEW.supplies_contact) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 185, OLD.supplies_contact);
    changes = changes + 1;
  END

  /* SUPPLIES_CONTACT_PHONE */
  IF (OLD.supplies_contact_phone IS DISTINCT FROM NEW.supplies_contact_phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 186, OLD.supplies_contact_phone);
    changes = changes + 1;
  END

  /* SUPPLIES_CONTACT_PHONE_TYPE */
  IF (OLD.supplies_contact_phone_type IS DISTINCT FROM NEW.supplies_contact_phone_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 189, OLD.supplies_contact_phone_type);
    changes = changes + 1;
  END

  /* WEB_SITE */
  IF (OLD.web_site IS DISTINCT FROM NEW.web_site) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 187, OLD.web_site);
    changes = changes + 1;
  END

  /* DELIVERY_COMPANY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@DELIVERY_COMPANY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@DELIVERY_COMPANY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 179, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_DELIVERY_COMPANY_SVC_9 */
CREATE TRIGGER T_AU_SB_DELIVERY_COMPANY_SVC_9 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(9, NEW.rec_version, NEW.sb_delivery_company_svcs_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 190, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 466, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_DELIVERY_COMPANY_NBR */
  IF (OLD.sb_delivery_company_nbr IS DISTINCT FROM NEW.sb_delivery_company_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 193, OLD.sb_delivery_company_nbr);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 194, OLD.description);
    changes = changes + 1;
  END

  /* REFERENCE_FEE */
  IF (OLD.reference_fee IS DISTINCT FROM NEW.reference_fee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 191, OLD.reference_fee);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_DELIVERY_METHOD_9 */
CREATE TRIGGER T_AU_SB_DELIVERY_METHOD_9 FOR SB_DELIVERY_METHOD
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(10, NEW.rec_version, NEW.sb_delivery_method_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 195, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 468, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_DELIVERY_METHOD_NBR */
  IF (OLD.sy_delivery_method_nbr IS DISTINCT FROM NEW.sy_delivery_method_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 196, OLD.sy_delivery_method_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_DELIVERY_SERVICE_9 */
CREATE TRIGGER T_AU_SB_DELIVERY_SERVICE_9 FOR SB_DELIVERY_SERVICE
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(11, NEW.rec_version, NEW.sb_delivery_service_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 198, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 470, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_DELIVERY_SERVICE_NBR */
  IF (OLD.sy_delivery_service_nbr IS DISTINCT FROM NEW.sy_delivery_service_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 199, OLD.sy_delivery_service_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_DELIVERY_SERVICE_OPT_9 */
CREATE TRIGGER T_AU_SB_DELIVERY_SERVICE_OPT_9 FOR SB_DELIVERY_SERVICE_OPT
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(12, NEW.rec_version, NEW.sb_delivery_service_opt_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 201, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 472, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_DELIVERY_SERVICE_NBR */
  IF (OLD.sb_delivery_service_nbr IS DISTINCT FROM NEW.sb_delivery_service_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 204, OLD.sb_delivery_service_nbr);
    changes = changes + 1;
  END

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 203, OLD.sb_delivery_method_nbr);
    changes = changes + 1;
  END

  /* OPTION_TAG */
  IF (OLD.option_tag IS DISTINCT FROM NEW.option_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 206, OLD.option_tag);
    changes = changes + 1;
  END

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS DISTINCT FROM NEW.option_integer_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 202, OLD.option_integer_value);
    changes = changes + 1;
  END

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS DISTINCT FROM NEW.option_string_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 207, OLD.option_string_value);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_ENLIST_GROUPS_9 */
CREATE TRIGGER T_AU_SB_ENLIST_GROUPS_9 FOR SB_ENLIST_GROUPS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(13, NEW.rec_version, NEW.sb_enlist_groups_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 208, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 474, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_REPORT_GROUPS_NBR */
  IF (OLD.sy_report_groups_nbr IS DISTINCT FROM NEW.sy_report_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 209, OLD.sy_report_groups_nbr);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 212, OLD.media_type);
    changes = changes + 1;
  END

  /* PROCESS_TYPE */
  IF (OLD.process_type IS DISTINCT FROM NEW.process_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 211, OLD.process_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_GLOBAL_AGENCY_CONTAC_9 */
CREATE TRIGGER T_AU_SB_GLOBAL_AGENCY_CONTAC_9 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(14, NEW.rec_version, NEW.sb_global_agency_contacts_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 215, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 476, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_GLOBAL_AGENCY_NBR */
  IF (OLD.sy_global_agency_nbr IS DISTINCT FROM NEW.sy_global_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 217, OLD.sy_global_agency_nbr);
    changes = changes + 1;
  END

  /* CONTACT_NAME */
  IF (OLD.contact_name IS DISTINCT FROM NEW.contact_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 213, OLD.contact_name);
    changes = changes + 1;
  END

  /* PHONE */
  IF (OLD.phone IS DISTINCT FROM NEW.phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 219, OLD.phone);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 220, OLD.fax);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 221, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 214, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_HOLIDAYS_9 */
CREATE TRIGGER T_AU_SB_HOLIDAYS_9 FOR SB_HOLIDAYS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(15, NEW.rec_version, NEW.sb_holidays_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 222, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 478, OLD.effective_until);
    changes = changes + 1;
  END

  /* HOLIDAY_DATE */
  IF (OLD.holiday_date IS DISTINCT FROM NEW.holiday_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 223, OLD.holiday_date);
    changes = changes + 1;
  END

  /* HOLIDAY_NAME */
  IF (OLD.holiday_name IS DISTINCT FROM NEW.holiday_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 225, OLD.holiday_name);
    changes = changes + 1;
  END

  /* USED_BY */
  IF (OLD.used_by IS DISTINCT FROM NEW.used_by) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 226, OLD.used_by);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_MAIL_BOX_9 */
CREATE TRIGGER T_AU_SB_MAIL_BOX_9 FOR SB_MAIL_BOX
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(16, NEW.rec_version, NEW.sb_mail_box_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 233, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 480, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 241, OLD.cl_nbr);
    changes = changes + 1;
  END

  /* CL_MAIL_BOX_GROUP_NBR */
  IF (OLD.cl_mail_box_group_nbr IS DISTINCT FROM NEW.cl_mail_box_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 240, OLD.cl_mail_box_group_nbr);
    changes = changes + 1;
  END

  /* COST */
  IF (OLD.cost IS DISTINCT FROM NEW.cost) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 232, OLD.cost);
    changes = changes + 1;
  END

  /* REQUIRED */
  IF (OLD.required IS DISTINCT FROM NEW.required) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 251, OLD.required);
    changes = changes + 1;
  END

  /* NOTIFICATION_EMAIL */
  IF (OLD.notification_email IS DISTINCT FROM NEW.notification_email) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 248, OLD.notification_email);
    changes = changes + 1;
  END

  /* PR_NBR */
  IF (OLD.pr_nbr IS DISTINCT FROM NEW.pr_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 239, OLD.pr_nbr);
    changes = changes + 1;
  END

  /* RELEASED_TIME */
  IF (OLD.released_time IS DISTINCT FROM NEW.released_time) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 231, OLD.released_time);
    changes = changes + 1;
  END

  /* PRINTED_TIME */
  IF (OLD.printed_time IS DISTINCT FROM NEW.printed_time) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 230, OLD.printed_time);
    changes = changes + 1;
  END

  /* SB_COVER_LETTER_REPORT_NBR */
  IF (OLD.sb_cover_letter_report_nbr IS DISTINCT FROM NEW.sb_cover_letter_report_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 238, OLD.sb_cover_letter_report_nbr);
    changes = changes + 1;
  END

  /* SCANNED_TIME */
  IF (OLD.scanned_time IS DISTINCT FROM NEW.scanned_time) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 229, OLD.scanned_time);
    changes = changes + 1;
  END

  /* SHIPPED_TIME */
  IF (OLD.shipped_time IS DISTINCT FROM NEW.shipped_time) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 228, OLD.shipped_time);
    changes = changes + 1;
  END

  /* SY_COVER_LETTER_REPORT_NBR */
  IF (OLD.sy_cover_letter_report_nbr IS DISTINCT FROM NEW.sy_cover_letter_report_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 237, OLD.sy_cover_letter_report_nbr);
    changes = changes + 1;
  END

  /* NOTE */
  IF (OLD.note IS DISTINCT FROM NEW.note) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 247, OLD.note);
    changes = changes + 1;
  END

  /* UP_LEVEL_MAIL_BOX_NBR */
  IF (OLD.up_level_mail_box_nbr IS DISTINCT FROM NEW.up_level_mail_box_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 236, OLD.up_level_mail_box_nbr);
    changes = changes + 1;
  END

  /* TRACKING_INFO */
  IF (OLD.tracking_info IS DISTINCT FROM NEW.tracking_info) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 246, OLD.tracking_info);
    changes = changes + 1;
  END

  /* AUTO_RELEASE_TYPE */
  IF (OLD.auto_release_type IS DISTINCT FROM NEW.auto_release_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 250, OLD.auto_release_type);
    changes = changes + 1;
  END

  /* BARCODE */
  IF (OLD.barcode IS DISTINCT FROM NEW.barcode) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 245, OLD.barcode);
    changes = changes + 1;
  END

  /* CREATED_TIME */
  IF (OLD.created_time IS DISTINCT FROM NEW.created_time) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 227, OLD.created_time);
    changes = changes + 1;
  END

  /* DISPOSE_CONTENT_AFTER_SHIPPING */
  IF (OLD.dispose_content_after_shipping IS DISTINCT FROM NEW.dispose_content_after_shipping) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 249, OLD.dispose_content_after_shipping);
    changes = changes + 1;
  END

  /* ADDRESSEE */
  IF (OLD.addressee IS DISTINCT FROM NEW.addressee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 244, OLD.addressee);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 243, OLD.description);
    changes = changes + 1;
  END

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 235, OLD.sb_delivery_method_nbr);
    changes = changes + 1;
  END

  /* SB_MEDIA_TYPE_NBR */
  IF (OLD.sb_media_type_nbr IS DISTINCT FROM NEW.sb_media_type_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 234, OLD.sb_media_type_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_MAIL_BOX_CONTENT_9 */
CREATE TRIGGER T_AU_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(17, NEW.rec_version, NEW.sb_mail_box_content_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 252, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_MAIL_BOX_NBR */
  IF (OLD.sb_mail_box_nbr IS DISTINCT FROM NEW.sb_mail_box_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 254, OLD.sb_mail_box_nbr);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 257, OLD.description);
    changes = changes + 1;
  END

  /* FILE_NAME */
  IF (OLD.file_name IS DISTINCT FROM NEW.file_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 256, OLD.file_name);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 258, OLD.media_type);
    changes = changes + 1;
  END

  /* PAGE_COUNT */
  IF (OLD.page_count IS DISTINCT FROM NEW.page_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 253, OLD.page_count);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_MAIL_BOX_OPTION_9 */
CREATE TRIGGER T_AU_SB_MAIL_BOX_OPTION_9 FOR SB_MAIL_BOX_OPTION
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(18, NEW.rec_version, NEW.sb_mail_box_option_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 259, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 484, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_MAIL_BOX_NBR */
  IF (OLD.sb_mail_box_nbr IS DISTINCT FROM NEW.sb_mail_box_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 262, OLD.sb_mail_box_nbr);
    changes = changes + 1;
  END

  /* SB_MAIL_BOX_CONTENT_NBR */
  IF (OLD.sb_mail_box_content_nbr IS DISTINCT FROM NEW.sb_mail_box_content_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 261, OLD.sb_mail_box_content_nbr);
    changes = changes + 1;
  END

  /* OPTION_TAG */
  IF (OLD.option_tag IS DISTINCT FROM NEW.option_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 265, OLD.option_tag);
    changes = changes + 1;
  END

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS DISTINCT FROM NEW.option_integer_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 260, OLD.option_integer_value);
    changes = changes + 1;
  END

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS DISTINCT FROM NEW.option_string_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 264, OLD.option_string_value);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_MEDIA_TYPE_9 */
CREATE TRIGGER T_AU_SB_MEDIA_TYPE_9 FOR SB_MEDIA_TYPE
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(19, NEW.rec_version, NEW.sb_media_type_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 266, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 486, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_MEDIA_TYPE_NBR */
  IF (OLD.sy_media_type_nbr IS DISTINCT FROM NEW.sy_media_type_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 267, OLD.sy_media_type_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_MEDIA_TYPE_OPTION_9 */
CREATE TRIGGER T_AU_SB_MEDIA_TYPE_OPTION_9 FOR SB_MEDIA_TYPE_OPTION
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(20, NEW.rec_version, NEW.sb_media_type_option_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 269, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 488, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_MEDIA_TYPE_NBR */
  IF (OLD.sb_media_type_nbr IS DISTINCT FROM NEW.sb_media_type_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 271, OLD.sb_media_type_nbr);
    changes = changes + 1;
  END

  /* OPTION_TAG */
  IF (OLD.option_tag IS DISTINCT FROM NEW.option_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 274, OLD.option_tag);
    changes = changes + 1;
  END

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS DISTINCT FROM NEW.option_integer_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 270, OLD.option_integer_value);
    changes = changes + 1;
  END

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS DISTINCT FROM NEW.option_string_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 273, OLD.option_string_value);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_MULTICLIENT_REPORTS_9 */
CREATE TRIGGER T_AU_SB_MULTICLIENT_REPORTS_9 FOR SB_MULTICLIENT_REPORTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(21, NEW.rec_version, NEW.sb_multiclient_reports_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 275, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 490, OLD.effective_until);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 279, OLD.description);
    changes = changes + 1;
  END

  /* REPORT_LEVEL */
  IF (OLD.report_level IS DISTINCT FROM NEW.report_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 280, OLD.report_level);
    changes = changes + 1;
  END

  /* REPORT_NBR */
  IF (OLD.report_nbr IS DISTINCT FROM NEW.report_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 277, OLD.report_nbr);
    changes = changes + 1;
  END

  /* INPUT_PARAMS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INPUT_PARAMS');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INPUT_PARAMS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 276, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_OPTION_9 */
CREATE TRIGGER T_AU_SB_OPTION_9 FOR SB_OPTION
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(22, NEW.rec_version, NEW.sb_option_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 281, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 492, OLD.effective_until);
    changes = changes + 1;
  END

  /* OPTION_TAG */
  IF (OLD.option_tag IS DISTINCT FROM NEW.option_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 285, OLD.option_tag);
    changes = changes + 1;
  END

  /* OPTION_INTEGER_VALUE */
  IF (OLD.option_integer_value IS DISTINCT FROM NEW.option_integer_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 282, OLD.option_integer_value);
    changes = changes + 1;
  END

  /* OPTION_STRING_VALUE */
  IF (OLD.option_string_value IS DISTINCT FROM NEW.option_string_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 284, OLD.option_string_value);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_OTHER_SERVICE_9 */
CREATE TRIGGER T_AU_SB_OTHER_SERVICE_9 FOR SB_OTHER_SERVICE
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(23, NEW.rec_version, NEW.sb_other_service_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 286, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 494, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 288, OLD.name);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_PAPER_INFO_9 */
CREATE TRIGGER T_AU_SB_PAPER_INFO_9 FOR SB_PAPER_INFO
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(24, NEW.rec_version, NEW.sb_paper_info_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 289, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 496, OLD.effective_until);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 294, OLD.description);
    changes = changes + 1;
  END

  /* HEIGHT */
  IF (OLD.height IS DISTINCT FROM NEW.height) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 292, OLD.height);
    changes = changes + 1;
  END

  /* WIDTH */
  IF (OLD.width IS DISTINCT FROM NEW.width) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 291, OLD.width);
    changes = changes + 1;
  END

  /* WEIGHT */
  IF (OLD.weight IS DISTINCT FROM NEW.weight) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 290, OLD.weight);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 295, OLD.media_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_QUEUE_PRIORITY_9 */
CREATE TRIGGER T_AU_SB_QUEUE_PRIORITY_9 FOR SB_QUEUE_PRIORITY
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(25, NEW.rec_version, NEW.sb_queue_priority_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 296, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 498, OLD.effective_until);
    changes = changes + 1;
  END

  /* THREADS */
  IF (OLD.threads IS DISTINCT FROM NEW.threads) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 298, OLD.threads);
    changes = changes + 1;
  END

  /* METHOD_NAME */
  IF (OLD.method_name IS DISTINCT FROM NEW.method_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 302, OLD.method_name);
    changes = changes + 1;
  END

  /* PRIORITY */
  IF (OLD.priority IS DISTINCT FROM NEW.priority) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 299, OLD.priority);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 300, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 301, OLD.sb_sec_groups_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_REFERRALS_9 */
CREATE TRIGGER T_AU_SB_REFERRALS_9 FOR SB_REFERRALS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(26, NEW.rec_version, NEW.sb_referrals_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 303, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 500, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 305, OLD.name);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_REPORTS_9 */
CREATE TRIGGER T_AU_SB_REPORTS_9 FOR SB_REPORTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(27, NEW.rec_version, NEW.sb_reports_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 308, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 502, OLD.effective_until);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 311, OLD.description);
    changes = changes + 1;
  END

  /* COMMENTS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COMMENTS');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COMMENTS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 307, :blob_nbr);
    changes = changes + 1;
  END

  /* REPORT_WRITER_REPORTS_NBR */
  IF (OLD.report_writer_reports_nbr IS DISTINCT FROM NEW.report_writer_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 309, OLD.report_writer_reports_nbr);
    changes = changes + 1;
  END

  /* REPORT_LEVEL */
  IF (OLD.report_level IS DISTINCT FROM NEW.report_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 312, OLD.report_level);
    changes = changes + 1;
  END

  /* INPUT_PARAMS */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INPUT_PARAMS');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INPUT_PARAMS', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 306, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_REPORT_WRITER_REPORT_9 */
CREATE TRIGGER T_AU_SB_REPORT_WRITER_REPORT_9 FOR SB_REPORT_WRITER_REPORTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(30, NEW.rec_version, NEW.sb_report_writer_reports_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 322, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 508, OLD.effective_until);
    changes = changes + 1;
  END

  /* REPORT_DESCRIPTION */
  IF (OLD.report_description IS DISTINCT FROM NEW.report_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 326, OLD.report_description);
    changes = changes + 1;
  END

  /* REPORT_TYPE */
  IF (OLD.report_type IS DISTINCT FROM NEW.report_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 328, OLD.report_type);
    changes = changes + 1;
  END

  /* REPORT_FILE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_FILE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@REPORT_FILE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 321, :blob_nbr);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 320, :blob_nbr);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 327, OLD.media_type);
    changes = changes + 1;
  END

  /* CLASS_NAME */
  IF (OLD.class_name IS DISTINCT FROM NEW.class_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 325, OLD.class_name);
    changes = changes + 1;
  END

  /* ANCESTOR_CLASS_NAME */
  IF (OLD.ancestor_class_name IS DISTINCT FROM NEW.ancestor_class_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 324, OLD.ancestor_class_name);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SALES_TAX_STATES_9 */
CREATE TRIGGER T_AU_SB_SALES_TAX_STATES_9 FOR SB_SALES_TAX_STATES
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(31, NEW.rec_version, NEW.sb_sales_tax_states_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 329, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 510, OLD.effective_until);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 332, OLD.state);
    changes = changes + 1;
  END

  /* STATE_TAX_ID */
  IF (OLD.state_tax_id IS DISTINCT FROM NEW.state_tax_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 333, OLD.state_tax_id);
    changes = changes + 1;
  END

  /* SALES_TAX_PERCENTAGE */
  IF (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 330, OLD.sales_tax_percentage);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SEC_CLIENTS_9 */
CREATE TRIGGER T_AU_SB_SEC_CLIENTS_9 FOR SB_SEC_CLIENTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(32, NEW.rec_version, NEW.sb_sec_clients_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 334, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 512, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 337, OLD.cl_nbr);
    changes = changes + 1;
  END

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 336, OLD.sb_sec_groups_nbr);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 335, OLD.sb_user_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SEC_GROUPS_9 */
CREATE TRIGGER T_AU_SB_SEC_GROUPS_9 FOR SB_SEC_GROUPS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(33, NEW.rec_version, NEW.sb_sec_groups_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 339, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 514, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 341, OLD.name);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SEC_GROUP_MEMBERS_9 */
CREATE TRIGGER T_AU_SB_SEC_GROUP_MEMBERS_9 FOR SB_SEC_GROUP_MEMBERS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(34, NEW.rec_version, NEW.sb_sec_group_members_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 342, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 516, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 344, OLD.sb_sec_groups_nbr);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 343, OLD.sb_user_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SEC_RIGHTS_9 */
CREATE TRIGGER T_AU_SB_SEC_RIGHTS_9 FOR SB_SEC_RIGHTS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(35, NEW.rec_version, NEW.sb_sec_rights_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 346, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 518, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 348, OLD.sb_sec_groups_nbr);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 347, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* TAG */
  IF (OLD.tag IS DISTINCT FROM NEW.tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 351, OLD.tag);
    changes = changes + 1;
  END

  /* CONTEXT */
  IF (OLD.context IS DISTINCT FROM NEW.context) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 350, OLD.context);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SEC_ROW_FILTERS_9 */
CREATE TRIGGER T_AU_SB_SEC_ROW_FILTERS_9 FOR SB_SEC_ROW_FILTERS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(36, NEW.rec_version, NEW.sb_sec_row_filters_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 352, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 520, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_SEC_GROUPS_NBR */
  IF (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 355, OLD.sb_sec_groups_nbr);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 356, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* DATABASE_TYPE */
  IF (OLD.database_type IS DISTINCT FROM NEW.database_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 358, OLD.database_type);
    changes = changes + 1;
  END

  /* TABLE_NAME */
  IF (OLD.table_name IS DISTINCT FROM NEW.table_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 357, OLD.table_name);
    changes = changes + 1;
  END

  /* FILTER_TYPE */
  IF (OLD.filter_type IS DISTINCT FROM NEW.filter_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 359, OLD.filter_type);
    changes = changes + 1;
  END

  /* CUSTOM_EXPR */
  IF (OLD.custom_expr IS DISTINCT FROM NEW.custom_expr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 360, OLD.custom_expr);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 353, OLD.cl_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SEC_TEMPLATES_9 */
CREATE TRIGGER T_AU_SB_SEC_TEMPLATES_9 FOR SB_SEC_TEMPLATES
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(37, NEW.rec_version, NEW.sb_sec_templates_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 362, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 522, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 364, OLD.name);
    changes = changes + 1;
  END

  /* TEMPLATE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TEMPLATE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TEMPLATE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 361, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SERVICES_9 */
CREATE TRIGGER T_AU_SB_SERVICES_9 FOR SB_SERVICES
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, NEW.rec_version, NEW.sb_services_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 367, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 524, OLD.effective_until);
    changes = changes + 1;
  END

  /* SERVICE_NAME */
  IF (OLD.service_name IS DISTINCT FROM NEW.service_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 374, OLD.service_name);
    changes = changes + 1;
  END

  /* FREQUENCY */
  IF (OLD.frequency IS DISTINCT FROM NEW.frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 377, OLD.frequency);
    changes = changes + 1;
  END

  /* MONTH_NUMBER */
  IF (OLD.month_number IS DISTINCT FROM NEW.month_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 372, OLD.month_number);
    changes = changes + 1;
  END

  /* BASED_ON_TYPE */
  IF (OLD.based_on_type IS DISTINCT FROM NEW.based_on_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 378, OLD.based_on_type);
    changes = changes + 1;
  END

  /* SB_REPORTS_NBR */
  IF (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 373, OLD.sb_reports_nbr);
    changes = changes + 1;
  END

  /* COMMISSION */
  IF (OLD.commission IS DISTINCT FROM NEW.commission) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 380, OLD.commission);
    changes = changes + 1;
  END

  /* SALES_TAXABLE */
  IF (OLD.sales_taxable IS DISTINCT FROM NEW.sales_taxable) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 381, OLD.sales_taxable);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 375, OLD.filler);
    changes = changes + 1;
  END

  /* PRODUCT_CODE */
  IF (OLD.product_code IS DISTINCT FROM NEW.product_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 366, OLD.product_code);
    changes = changes + 1;
  END

  /* WEEK_NUMBER */
  IF (OLD.week_number IS DISTINCT FROM NEW.week_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 382, OLD.week_number);
    changes = changes + 1;
  END

  /* SERVICE_TYPE */
  IF (OLD.service_type IS DISTINCT FROM NEW.service_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 383, OLD.service_type);
    changes = changes + 1;
  END

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS DISTINCT FROM NEW.minimum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 368, OLD.minimum_amount);
    changes = changes + 1;
  END

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS DISTINCT FROM NEW.maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 369, OLD.maximum_amount);
    changes = changes + 1;
  END

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 370, OLD.sb_delivery_method_nbr);
    changes = changes + 1;
  END

  /* TAX_TYPE */
  IF (OLD.tax_type IS DISTINCT FROM NEW.tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 376, OLD.tax_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_SERVICES_CALCULATION_9 */
CREATE TRIGGER T_AU_SB_SERVICES_CALCULATION_9 FOR SB_SERVICES_CALCULATIONS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(39, NEW.rec_version, NEW.sb_services_calculations_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 388, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 526, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_SERVICES_NBR */
  IF (OLD.sb_services_nbr IS DISTINCT FROM NEW.sb_services_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 398, OLD.sb_services_nbr);
    changes = changes + 1;
  END

  /* NEXT_MIN_QUANTITY_BEGIN_DATE */
  IF (OLD.next_min_quantity_begin_date IS DISTINCT FROM NEW.next_min_quantity_begin_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 384, OLD.next_min_quantity_begin_date);
    changes = changes + 1;
  END

  /* NEXT_MAX_QUANTITY_BEGIN_DATE */
  IF (OLD.next_max_quantity_begin_date IS DISTINCT FROM NEW.next_max_quantity_begin_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 385, OLD.next_max_quantity_begin_date);
    changes = changes + 1;
  END

  /* NEXT_PER_ITEM_BEGIN_DATE */
  IF (OLD.next_per_item_begin_date IS DISTINCT FROM NEW.next_per_item_begin_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 386, OLD.next_per_item_begin_date);
    changes = changes + 1;
  END

  /* NEXT_FLAT_AMOUNT_BEGIN_DATE */
  IF (OLD.next_flat_amount_begin_date IS DISTINCT FROM NEW.next_flat_amount_begin_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 387, OLD.next_flat_amount_begin_date);
    changes = changes + 1;
  END

  /* MINIMUM_QUANTITY */
  IF (OLD.minimum_quantity IS DISTINCT FROM NEW.minimum_quantity) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 389, OLD.minimum_quantity);
    changes = changes + 1;
  END

  /* NEXT_MINIMUM_QUANTITY */
  IF (OLD.next_minimum_quantity IS DISTINCT FROM NEW.next_minimum_quantity) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 390, OLD.next_minimum_quantity);
    changes = changes + 1;
  END

  /* MAXIMUM_QUANTITY */
  IF (OLD.maximum_quantity IS DISTINCT FROM NEW.maximum_quantity) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 391, OLD.maximum_quantity);
    changes = changes + 1;
  END

  /* NEXT_MAXIMUM_QUANTITY */
  IF (OLD.next_maximum_quantity IS DISTINCT FROM NEW.next_maximum_quantity) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 392, OLD.next_maximum_quantity);
    changes = changes + 1;
  END

  /* PER_ITEM_RATE */
  IF (OLD.per_item_rate IS DISTINCT FROM NEW.per_item_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 393, OLD.per_item_rate);
    changes = changes + 1;
  END

  /* NEXT_PER_ITEM_RATE */
  IF (OLD.next_per_item_rate IS DISTINCT FROM NEW.next_per_item_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 394, OLD.next_per_item_rate);
    changes = changes + 1;
  END

  /* FLAT_AMOUNT */
  IF (OLD.flat_amount IS DISTINCT FROM NEW.flat_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 395, OLD.flat_amount);
    changes = changes + 1;
  END

  /* NEXT_FLAT_AMOUNT */
  IF (OLD.next_flat_amount IS DISTINCT FROM NEW.next_flat_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 396, OLD.next_flat_amount);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_STORAGE_9 */
CREATE TRIGGER T_AU_SB_STORAGE_9 FOR SB_STORAGE
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(46, NEW.rec_version, NEW.sb_storage_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 556, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 557, OLD.effective_until);
    changes = changes + 1;
  END

  /* TAG */
  IF (OLD.tag IS DISTINCT FROM NEW.tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 558, OLD.tag);
    changes = changes + 1;
  END

  /* STORAGE_DATA */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@STORAGE_DATA');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@STORAGE_DATA', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 559, :blob_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_TASK_9 */
CREATE TRIGGER T_AU_SB_TASK_9 FOR SB_TASK
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(40, NEW.rec_version, NEW.sb_task_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 400, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 528, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 402, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* SCHEDULE */
  IF (OLD.schedule IS DISTINCT FROM NEW.schedule) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 403, OLD.schedule);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 404, OLD.description);
    changes = changes + 1;
  END

  /* TASK */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TASK');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TASK', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 399, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_RUN */
  IF (OLD.last_run IS DISTINCT FROM NEW.last_run) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 552, OLD.last_run);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_TAX_PAYMENT_9 */
CREATE TRIGGER T_AU_SB_TAX_PAYMENT_9 FOR SB_TAX_PAYMENT
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(41, NEW.rec_version, NEW.sb_tax_payment_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 405, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 530, OLD.effective_until);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 408, OLD.description);
    changes = changes + 1;
  END

  /* STATUS */
  IF (OLD.status IS DISTINCT FROM NEW.status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 409, OLD.status);
    changes = changes + 1;
  END

  /* STATUS_DATE */
  IF (OLD.status_date IS DISTINCT FROM NEW.status_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 406, OLD.status_date);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_TEAM_9 */
CREATE TRIGGER T_AU_SB_TEAM_9 FOR SB_TEAM
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(42, NEW.rec_version, NEW.sb_team_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 410, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 532, OLD.effective_until);
    changes = changes + 1;
  END

  /* TEAM_DESCRIPTION */
  IF (OLD.team_description IS DISTINCT FROM NEW.team_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 412, OLD.team_description);
    changes = changes + 1;
  END

  /* CR_CATEGORY */
  IF (OLD.cr_category IS DISTINCT FROM NEW.cr_category) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 413, OLD.cr_category);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_TEAM_MEMBERS_9 */
CREATE TRIGGER T_AU_SB_TEAM_MEMBERS_9 FOR SB_TEAM_MEMBERS
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(43, NEW.rec_version, NEW.sb_team_members_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 414, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 534, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_TEAM_NBR */
  IF (OLD.sb_team_nbr IS DISTINCT FROM NEW.sb_team_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 416, OLD.sb_team_nbr);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 417, OLD.sb_user_nbr);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_USER_9 */
CREATE TRIGGER T_AU_SB_USER_9 FOR SB_USER
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(44, NEW.rec_version, NEW.sb_user_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 422, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 536, OLD.effective_until);
    changes = changes + 1;
  END

  /* USER_ID */
  IF (OLD.user_id IS DISTINCT FROM NEW.user_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 435, OLD.user_id);
    changes = changes + 1;
  END

  /* USER_SIGNATURE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@USER_SIGNATURE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 419, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_NAME */
  IF (OLD.last_name IS DISTINCT FROM NEW.last_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 420, OLD.last_name);
    changes = changes + 1;
  END

  /* FIRST_NAME */
  IF (OLD.first_name IS DISTINCT FROM NEW.first_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 436, OLD.first_name);
    changes = changes + 1;
  END

  /* MIDDLE_INITIAL */
  IF (OLD.middle_initial IS DISTINCT FROM NEW.middle_initial) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 440, OLD.middle_initial);
    changes = changes + 1;
  END

  /* ACTIVE_USER */
  IF (OLD.active_user IS DISTINCT FROM NEW.active_user) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 441, OLD.active_user);
    changes = changes + 1;
  END

  /* DEPARTMENT */
  IF (OLD.department IS DISTINCT FROM NEW.department) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 442, OLD.department);
    changes = changes + 1;
  END

  /* PASSWORD_CHANGE_DATE */
  IF (OLD.password_change_date IS DISTINCT FROM NEW.password_change_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 421, OLD.password_change_date);
    changes = changes + 1;
  END

  /* SECURITY_LEVEL */
  IF (OLD.security_level IS DISTINCT FROM NEW.security_level) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 443, OLD.security_level);
    changes = changes + 1;
  END

  /* USER_UPDATE_OPTIONS */
  IF (OLD.user_update_options IS DISTINCT FROM NEW.user_update_options) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 437, OLD.user_update_options);
    changes = changes + 1;
  END

  /* USER_FUNCTIONS */
  IF (OLD.user_functions IS DISTINCT FROM NEW.user_functions) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 438, OLD.user_functions);
    changes = changes + 1;
  END

  /* USER_PASSWORD */
  IF (OLD.user_password IS DISTINCT FROM NEW.user_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 439, OLD.user_password);
    changes = changes + 1;
  END

  /* EMAIL_ADDRESS */
  IF (OLD.email_address IS DISTINCT FROM NEW.email_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 434, OLD.email_address);
    changes = changes + 1;
  END

  /* CL_NBR */
  IF (OLD.cl_nbr IS DISTINCT FROM NEW.cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 429, OLD.cl_nbr);
    changes = changes + 1;
  END

  /* SB_ACCOUNTANT_NBR */
  IF (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 427, OLD.sb_accountant_nbr);
    changes = changes + 1;
  END

  /* WRONG_PSWD_ATTEMPTS */
  IF (OLD.wrong_pswd_attempts IS DISTINCT FROM NEW.wrong_pswd_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 426, OLD.wrong_pswd_attempts);
    changes = changes + 1;
  END

  /* LINKS_DATA */
  IF (OLD.links_data IS DISTINCT FROM NEW.links_data) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 433, OLD.links_data);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 425, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 432, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 424, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 431, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 423, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 430, OLD.login_answer3);
    changes = changes + 1;
  END

  /* HR_PERSONNEL */
  IF (OLD.hr_personnel IS DISTINCT FROM NEW.hr_personnel) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 545, OLD.hr_personnel);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 546, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 547, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 548, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 549, OLD.sec_answer2);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_AU_SB_USER_NOTICE_9 */
CREATE TRIGGER T_AU_SB_USER_NOTICE_9 FOR SB_USER_NOTICE
ACTIVE AFTER UPDATE POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(45, NEW.rec_version, NEW.sb_user_notice_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 539, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 538, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_USER_NBR */
  IF (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 448, OLD.sb_user_nbr);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 450, OLD.name);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 447, :blob_nbr);
    changes = changes + 1;
  END

  /* TASK */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TASK');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TASK', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 446, :blob_nbr);
    changes = changes + 1;
  END

  /* LAST_DISMISS */
  IF (OLD.last_dismiss IS DISTINCT FROM NEW.last_dismiss) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 445, OLD.last_dismiss);
    changes = changes + 1;
  END

  /* NEXT_REMINDER */
  IF (OLD.next_reminder IS DISTINCT FROM NEW.next_reminder) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 444, OLD.next_reminder);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Trigger: T_BIUD_EV_DATABASE_1 */
CREATE TRIGGER T_BIUD_EV_DATABASE_1 FOR EV_DATABASE
ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 1
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) THEN
    EXECUTE PROCEDURE raise_error('S3', 'Table EV_DATABASE cannot be modified');
END
^


/* Trigger: T_BIUD_EV_FIELD_1 */
CREATE TRIGGER T_BIUD_EV_FIELD_1 FOR EV_FIELD
ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 1
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) THEN
    EXECUTE PROCEDURE raise_error('S3', 'Table EV_FIELD cannot be modified');
END
^


/* Trigger: T_BIUD_EV_TABLE_1 */
CREATE TRIGGER T_BIUD_EV_TABLE_1 FOR EV_TABLE
ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 1
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) THEN
    EXECUTE PROCEDURE raise_error('S3', 'Table EV_TABLE cannot be modified');
END
^


/* Trigger: T_BIU_SB_3 */
CREATE TRIGGER T_BIU_SB_3 FOR SB
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB */
  IF (INSERTING OR (OLD.sb_name IS DISTINCT FROM NEW.sb_name)) THEN
    IF (EXISTS(SELECT 1 FROM sb WHERE sb_name = NEW.sb_name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb', 'sb_name',
      CAST(NEW.sb_name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_ACCOUNTANT_3 */
CREATE TRIGGER T_BIU_SB_ACCOUNTANT_3 FOR SB_ACCOUNTANT
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_ACCOUNTANT */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_accountant WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_accountant', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_AGENCY_2 */
CREATE TRIGGER T_BIU_SB_AGENCY_2 FOR SB_AGENCY
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_BANKS */
  IF ((NEW.sb_banks_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.sb_banks_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_agency', NEW.sb_agency_nbr, 'sb_banks_nbr', NEW.sb_banks_nbr, 'sb_banks', NEW.effective_date);

    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.sb_banks_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_agency', NEW.sb_agency_nbr, 'sb_banks_nbr', NEW.sb_banks_nbr, 'sb_banks', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_AGENCY_3 */
CREATE TRIGGER T_BIU_SB_AGENCY_3 FOR SB_AGENCY
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_AGENCY */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_agency WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_agency', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_AGENCY_REPORTS_2 */
CREATE TRIGGER T_BIU_SB_AGENCY_REPORTS_2 FOR SB_AGENCY_REPORTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_AGENCY */
  IF ((NEW.sb_agency_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_agency_nbr IS DISTINCT FROM NEW.sb_agency_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_agency
    WHERE sb_agency_nbr = NEW.sb_agency_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_agency_reports', NEW.sb_agency_reports_nbr, 'sb_agency_nbr', NEW.sb_agency_nbr, 'sb_agency', NEW.effective_date);

    SELECT rec_version FROM sb_agency
    WHERE sb_agency_nbr = NEW.sb_agency_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_agency_reports', NEW.sb_agency_reports_nbr, 'sb_agency_nbr', NEW.sb_agency_nbr, 'sb_agency', NEW.effective_until - 1);
  END

  /* Check reference to SB_REPORTS */
  IF ((NEW.sb_reports_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_reports
    WHERE sb_reports_nbr = NEW.sb_reports_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_agency_reports', NEW.sb_agency_reports_nbr, 'sb_reports_nbr', NEW.sb_reports_nbr, 'sb_reports', NEW.effective_date);

    SELECT rec_version FROM sb_reports
    WHERE sb_reports_nbr = NEW.sb_reports_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_agency_reports', NEW.sb_agency_reports_nbr, 'sb_reports_nbr', NEW.sb_reports_nbr, 'sb_reports', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_AGENCY_REPORTS_3 */
CREATE TRIGGER T_BIU_SB_AGENCY_REPORTS_3 FOR SB_AGENCY_REPORTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_AGENCY_REPORTS */
  IF (INSERTING OR (OLD.sb_agency_nbr IS DISTINCT FROM NEW.sb_agency_nbr) OR (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_agency_reports WHERE sb_agency_nbr = NEW.sb_agency_nbr AND sb_reports_nbr = NEW.sb_reports_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_agency_reports', 'sb_agency_nbr, sb_reports_nbr',
      CAST(NEW.sb_agency_nbr || ', ' || NEW.sb_reports_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_BANKS_3 */
CREATE TRIGGER T_BIU_SB_BANKS_3 FOR SB_BANKS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_BANKS */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_banks WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_banks', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_BANK_ACCOUNTS_2 */
CREATE TRIGGER T_BIU_SB_BANK_ACCOUNTS_2 FOR SB_BANK_ACCOUNTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_BANKS */
  IF ((NEW.sb_banks_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.sb_banks_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'sb_banks_nbr', NEW.sb_banks_nbr, 'sb_banks', NEW.effective_date);

    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.sb_banks_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'sb_banks_nbr', NEW.sb_banks_nbr, 'sb_banks', NEW.effective_until - 1);
  END

  /* Check reference to SB_BANKS */
  IF ((NEW.ach_origin_sb_banks_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.ach_origin_sb_banks_nbr IS DISTINCT FROM NEW.ach_origin_sb_banks_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.ach_origin_sb_banks_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'ach_origin_sb_banks_nbr', NEW.ach_origin_sb_banks_nbr, 'sb_banks', NEW.effective_date);

    SELECT rec_version FROM sb_banks
    WHERE sb_banks_nbr = NEW.ach_origin_sb_banks_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'ach_origin_sb_banks_nbr', NEW.ach_origin_sb_banks_nbr, 'sb_banks', NEW.effective_until - 1);
  END

  /* Check reference to SB_BLOB */
  IF ((NEW.logo_sb_blob_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.logo_sb_blob_nbr IS DISTINCT FROM NEW.logo_sb_blob_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_blob
    WHERE sb_blob_nbr = NEW.logo_sb_blob_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'logo_sb_blob_nbr', NEW.logo_sb_blob_nbr, 'sb_blob', NEW.effective_date);

    SELECT rec_version FROM sb_blob
    WHERE sb_blob_nbr = NEW.logo_sb_blob_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'logo_sb_blob_nbr', NEW.logo_sb_blob_nbr, 'sb_blob', NEW.effective_until - 1);
  END

  /* Check reference to SB_BLOB */
  IF ((NEW.signature_sb_blob_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.signature_sb_blob_nbr IS DISTINCT FROM NEW.signature_sb_blob_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_blob
    WHERE sb_blob_nbr = NEW.signature_sb_blob_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'signature_sb_blob_nbr', NEW.signature_sb_blob_nbr, 'sb_blob', NEW.effective_date);

    SELECT rec_version FROM sb_blob
    WHERE sb_blob_nbr = NEW.signature_sb_blob_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_bank_accounts', NEW.sb_bank_accounts_nbr, 'signature_sb_blob_nbr', NEW.signature_sb_blob_nbr, 'sb_blob', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_BANK_ACCOUNTS_3 */
CREATE TRIGGER T_BIU_SB_BANK_ACCOUNTS_3 FOR SB_BANK_ACCOUNTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_BANK_ACCOUNTS */
  IF (INSERTING OR (OLD.sb_banks_nbr IS DISTINCT FROM NEW.sb_banks_nbr) OR (OLD.bank_account_type IS DISTINCT FROM NEW.bank_account_type) OR (OLD.custom_bank_account_number IS DISTINCT FROM NEW.custom_bank_account_number)) THEN
    IF (EXISTS(SELECT 1 FROM sb_bank_accounts WHERE sb_banks_nbr = NEW.sb_banks_nbr AND bank_account_type = NEW.bank_account_type AND custom_bank_account_number = NEW.custom_bank_account_number AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_bank_accounts', 'sb_banks_nbr, bank_account_type, custom_bank_account_number',
      CAST(NEW.sb_banks_nbr || ', ' || NEW.bank_account_type || ', ' || NEW.custom_bank_account_number as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_DELIVERY_COMPANY_3 */
CREATE TRIGGER T_BIU_SB_DELIVERY_COMPANY_3 FOR SB_DELIVERY_COMPANY
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_DELIVERY_COMPANY */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_delivery_company WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_delivery_company', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_DELIVERY_COMPANY_SVC_2 */
CREATE TRIGGER T_BIU_SB_DELIVERY_COMPANY_SVC_2 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_DELIVERY_COMPANY */
  IF ((NEW.sb_delivery_company_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_delivery_company_nbr IS DISTINCT FROM NEW.sb_delivery_company_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_company
    WHERE sb_delivery_company_nbr = NEW.sb_delivery_company_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_delivery_company_svcs', NEW.sb_delivery_company_svcs_nbr, 'sb_delivery_company_nbr', NEW.sb_delivery_company_nbr, 'sb_delivery_company', NEW.effective_date);

    SELECT rec_version FROM sb_delivery_company
    WHERE sb_delivery_company_nbr = NEW.sb_delivery_company_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_delivery_company_svcs', NEW.sb_delivery_company_svcs_nbr, 'sb_delivery_company_nbr', NEW.sb_delivery_company_nbr, 'sb_delivery_company', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_DELIVERY_COMPANY_SVC_3 */
CREATE TRIGGER T_BIU_SB_DELIVERY_COMPANY_SVC_3 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_DELIVERY_COMPANY_SVCS */
  IF (INSERTING OR (OLD.description IS DISTINCT FROM NEW.description)) THEN
    IF (EXISTS(SELECT 1 FROM sb_delivery_company_svcs WHERE description = NEW.description AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_delivery_company_svcs', 'description',
      CAST(NEW.description as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_DELIVERY_SERVICE_OPT_2 */
CREATE TRIGGER T_BIU_SB_DELIVERY_SERVICE_OPT_2 FOR SB_DELIVERY_SERVICE_OPT
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_DELIVERY_SERVICE */
  IF ((NEW.sb_delivery_service_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_delivery_service_nbr IS DISTINCT FROM NEW.sb_delivery_service_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_service
    WHERE sb_delivery_service_nbr = NEW.sb_delivery_service_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_delivery_service_opt', NEW.sb_delivery_service_opt_nbr, 'sb_delivery_service_nbr', NEW.sb_delivery_service_nbr, 'sb_delivery_service', NEW.effective_date);

    SELECT rec_version FROM sb_delivery_service
    WHERE sb_delivery_service_nbr = NEW.sb_delivery_service_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_delivery_service_opt', NEW.sb_delivery_service_opt_nbr, 'sb_delivery_service_nbr', NEW.sb_delivery_service_nbr, 'sb_delivery_service', NEW.effective_until - 1);
  END

  /* Check reference to SB_DELIVERY_METHOD */
  IF ((NEW.sb_delivery_method_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_method
    WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_delivery_service_opt', NEW.sb_delivery_service_opt_nbr, 'sb_delivery_method_nbr', NEW.sb_delivery_method_nbr, 'sb_delivery_method', NEW.effective_date);

    SELECT rec_version FROM sb_delivery_method
    WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_delivery_service_opt', NEW.sb_delivery_service_opt_nbr, 'sb_delivery_method_nbr', NEW.sb_delivery_method_nbr, 'sb_delivery_method', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_ENLIST_GROUPS_3 */
CREATE TRIGGER T_BIU_SB_ENLIST_GROUPS_3 FOR SB_ENLIST_GROUPS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_ENLIST_GROUPS */
  IF (INSERTING OR (OLD.sy_report_groups_nbr IS DISTINCT FROM NEW.sy_report_groups_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_enlist_groups WHERE sy_report_groups_nbr = NEW.sy_report_groups_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_enlist_groups', 'sy_report_groups_nbr',
      CAST(NEW.sy_report_groups_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_GLOBAL_AGENCY_CONTAC_3 */
CREATE TRIGGER T_BIU_SB_GLOBAL_AGENCY_CONTAC_3 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_GLOBAL_AGENCY_CONTACTS */
  IF (INSERTING OR (OLD.contact_name IS DISTINCT FROM NEW.contact_name) OR (OLD.sy_global_agency_nbr IS DISTINCT FROM NEW.sy_global_agency_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_global_agency_contacts WHERE contact_name = NEW.contact_name AND sy_global_agency_nbr = NEW.sy_global_agency_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_global_agency_contacts', 'contact_name, sy_global_agency_nbr',
      CAST(NEW.contact_name || ', ' || NEW.sy_global_agency_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_HOLIDAYS_3 */
CREATE TRIGGER T_BIU_SB_HOLIDAYS_3 FOR SB_HOLIDAYS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_HOLIDAYS */
  IF (INSERTING OR (OLD.holiday_date IS DISTINCT FROM NEW.holiday_date)) THEN
    IF (EXISTS(SELECT 1 FROM sb_holidays WHERE holiday_date = NEW.holiday_date AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_holidays', 'holiday_date',
      CAST(NEW.holiday_date as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_MAIL_BOX_2 */
CREATE TRIGGER T_BIU_SB_MAIL_BOX_2 FOR SB_MAIL_BOX
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_MAIL_BOX */
  IF ((NEW.up_level_mail_box_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.up_level_mail_box_nbr IS DISTINCT FROM NEW.up_level_mail_box_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE sb_mail_box_nbr = NEW.up_level_mail_box_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'up_level_mail_box_nbr', NEW.up_level_mail_box_nbr, 'sb_mail_box', NEW.effective_date);

    SELECT rec_version FROM sb_mail_box
    WHERE sb_mail_box_nbr = NEW.up_level_mail_box_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'up_level_mail_box_nbr', NEW.up_level_mail_box_nbr, 'sb_mail_box', NEW.effective_until - 1);
  END

  /* Check reference to SB_DELIVERY_METHOD */
  IF ((NEW.sb_delivery_method_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_method
    WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'sb_delivery_method_nbr', NEW.sb_delivery_method_nbr, 'sb_delivery_method', NEW.effective_date);

    SELECT rec_version FROM sb_delivery_method
    WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'sb_delivery_method_nbr', NEW.sb_delivery_method_nbr, 'sb_delivery_method', NEW.effective_until - 1);
  END

  /* Check reference to SB_MEDIA_TYPE */
  IF ((NEW.sb_media_type_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_media_type_nbr IS DISTINCT FROM NEW.sb_media_type_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_media_type
    WHERE sb_media_type_nbr = NEW.sb_media_type_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'sb_media_type_nbr', NEW.sb_media_type_nbr, 'sb_media_type', NEW.effective_date);

    SELECT rec_version FROM sb_media_type
    WHERE sb_media_type_nbr = NEW.sb_media_type_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'sb_media_type_nbr', NEW.sb_media_type_nbr, 'sb_media_type', NEW.effective_until - 1);
  END

  /* Check reference to SB_REPORT_WRITER_REPORTS */
  IF ((NEW.sb_cover_letter_report_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_cover_letter_report_nbr IS DISTINCT FROM NEW.sb_cover_letter_report_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_report_writer_reports
    WHERE sb_report_writer_reports_nbr = NEW.sb_cover_letter_report_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'sb_cover_letter_report_nbr', NEW.sb_cover_letter_report_nbr, 'sb_report_writer_reports', NEW.effective_date);

    SELECT rec_version FROM sb_report_writer_reports
    WHERE sb_report_writer_reports_nbr = NEW.sb_cover_letter_report_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box', NEW.sb_mail_box_nbr, 'sb_cover_letter_report_nbr', NEW.sb_cover_letter_report_nbr, 'sb_report_writer_reports', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_MAIL_BOX_3 */
CREATE TRIGGER T_BIU_SB_MAIL_BOX_3 FOR SB_MAIL_BOX
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_MAIL_BOX */
  IF (INSERTING OR (OLD.barcode IS DISTINCT FROM NEW.barcode)) THEN
    IF (EXISTS(SELECT 1 FROM sb_mail_box WHERE barcode = NEW.barcode AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_mail_box', 'barcode',
      CAST(NEW.barcode as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_MAIL_BOX_CONTENT_2 */
CREATE TRIGGER T_BIU_SB_MAIL_BOX_CONTENT_2 FOR SB_MAIL_BOX_CONTENT
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_MAIL_BOX */
  IF ((NEW.sb_mail_box_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_mail_box_nbr IS DISTINCT FROM NEW.sb_mail_box_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE sb_mail_box_nbr = NEW.sb_mail_box_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box_content', NEW.sb_mail_box_content_nbr, 'sb_mail_box_nbr', NEW.sb_mail_box_nbr, 'sb_mail_box', NEW.effective_date);

    SELECT rec_version FROM sb_mail_box
    WHERE sb_mail_box_nbr = NEW.sb_mail_box_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box_content', NEW.sb_mail_box_content_nbr, 'sb_mail_box_nbr', NEW.sb_mail_box_nbr, 'sb_mail_box', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_MAIL_BOX_OPTION_2 */
CREATE TRIGGER T_BIU_SB_MAIL_BOX_OPTION_2 FOR SB_MAIL_BOX_OPTION
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_MAIL_BOX */
  IF ((NEW.sb_mail_box_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_mail_box_nbr IS DISTINCT FROM NEW.sb_mail_box_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box
    WHERE sb_mail_box_nbr = NEW.sb_mail_box_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box_option', NEW.sb_mail_box_option_nbr, 'sb_mail_box_nbr', NEW.sb_mail_box_nbr, 'sb_mail_box', NEW.effective_date);

    SELECT rec_version FROM sb_mail_box
    WHERE sb_mail_box_nbr = NEW.sb_mail_box_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box_option', NEW.sb_mail_box_option_nbr, 'sb_mail_box_nbr', NEW.sb_mail_box_nbr, 'sb_mail_box', NEW.effective_until - 1);
  END

  /* Check reference to SB_MAIL_BOX_CONTENT */
  IF ((NEW.sb_mail_box_content_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_mail_box_content_nbr IS DISTINCT FROM NEW.sb_mail_box_content_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_mail_box_content
    WHERE sb_mail_box_content_nbr = NEW.sb_mail_box_content_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box_option', NEW.sb_mail_box_option_nbr, 'sb_mail_box_content_nbr', NEW.sb_mail_box_content_nbr, 'sb_mail_box_content', NEW.effective_date);

    SELECT rec_version FROM sb_mail_box_content
    WHERE sb_mail_box_content_nbr = NEW.sb_mail_box_content_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_mail_box_option', NEW.sb_mail_box_option_nbr, 'sb_mail_box_content_nbr', NEW.sb_mail_box_content_nbr, 'sb_mail_box_content', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_MEDIA_TYPE_3 */
CREATE TRIGGER T_BIU_SB_MEDIA_TYPE_3 FOR SB_MEDIA_TYPE
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_MEDIA_TYPE */
  IF (INSERTING OR (OLD.sy_media_type_nbr IS DISTINCT FROM NEW.sy_media_type_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_media_type WHERE sy_media_type_nbr = NEW.sy_media_type_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_media_type', 'sy_media_type_nbr',
      CAST(NEW.sy_media_type_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_MEDIA_TYPE_OPTION_2 */
CREATE TRIGGER T_BIU_SB_MEDIA_TYPE_OPTION_2 FOR SB_MEDIA_TYPE_OPTION
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_MEDIA_TYPE */
  IF ((NEW.sb_media_type_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_media_type_nbr IS DISTINCT FROM NEW.sb_media_type_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_media_type
    WHERE sb_media_type_nbr = NEW.sb_media_type_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_media_type_option', NEW.sb_media_type_option_nbr, 'sb_media_type_nbr', NEW.sb_media_type_nbr, 'sb_media_type', NEW.effective_date);

    SELECT rec_version FROM sb_media_type
    WHERE sb_media_type_nbr = NEW.sb_media_type_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_media_type_option', NEW.sb_media_type_option_nbr, 'sb_media_type_nbr', NEW.sb_media_type_nbr, 'sb_media_type', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_MEDIA_TYPE_OPTION_3 */
CREATE TRIGGER T_BIU_SB_MEDIA_TYPE_OPTION_3 FOR SB_MEDIA_TYPE_OPTION
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_MEDIA_TYPE_OPTION */
  IF (INSERTING OR (OLD.option_tag IS DISTINCT FROM NEW.option_tag) OR (OLD.sb_media_type_nbr IS DISTINCT FROM NEW.sb_media_type_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_media_type_option WHERE option_tag = NEW.option_tag AND sb_media_type_nbr = NEW.sb_media_type_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_media_type_option', 'option_tag, sb_media_type_nbr',
      CAST(NEW.option_tag || ', ' || NEW.sb_media_type_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_MULTICLIENT_REPORTS_3 */
CREATE TRIGGER T_BIU_SB_MULTICLIENT_REPORTS_3 FOR SB_MULTICLIENT_REPORTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_MULTICLIENT_REPORTS */
  IF (INSERTING OR (OLD.description IS DISTINCT FROM NEW.description)) THEN
    IF (EXISTS(SELECT 1 FROM sb_multiclient_reports WHERE description = NEW.description AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_multiclient_reports', 'description',
      CAST(NEW.description as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_OPTION_3 */
CREATE TRIGGER T_BIU_SB_OPTION_3 FOR SB_OPTION
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_OPTION */
  IF (INSERTING OR (OLD.option_tag IS DISTINCT FROM NEW.option_tag)) THEN
    IF (EXISTS(SELECT 1 FROM sb_option WHERE option_tag = NEW.option_tag AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_option', 'option_tag',
      CAST(NEW.option_tag as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_OTHER_SERVICE_3 */
CREATE TRIGGER T_BIU_SB_OTHER_SERVICE_3 FOR SB_OTHER_SERVICE
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_OTHER_SERVICE */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_other_service WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_other_service', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_PAPER_INFO_3 */
CREATE TRIGGER T_BIU_SB_PAPER_INFO_3 FOR SB_PAPER_INFO
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_PAPER_INFO */
  IF (INSERTING OR (OLD.description IS DISTINCT FROM NEW.description)) THEN
    IF (EXISTS(SELECT 1 FROM sb_paper_info WHERE description = NEW.description AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_paper_info', 'description',
      CAST(NEW.description as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_QUEUE_PRIORITY_2 */
CREATE TRIGGER T_BIU_SB_QUEUE_PRIORITY_2 FOR SB_QUEUE_PRIORITY
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_queue_priority', NEW.sb_queue_priority_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_queue_priority', NEW.sb_queue_priority_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END

  /* Check reference to SB_SEC_GROUPS */
  IF ((NEW.sb_sec_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_queue_priority', NEW.sb_queue_priority_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_date);

    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_queue_priority', NEW.sb_queue_priority_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_REFERRALS_3 */
CREATE TRIGGER T_BIU_SB_REFERRALS_3 FOR SB_REFERRALS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_REFERRALS */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_referrals WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_referrals', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_REPORTS_3 */
CREATE TRIGGER T_BIU_SB_REPORTS_3 FOR SB_REPORTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_REPORTS */
  IF (INSERTING OR (OLD.description IS DISTINCT FROM NEW.description)) THEN
    IF (EXISTS(SELECT 1 FROM sb_reports WHERE description = NEW.description AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_reports', 'description',
      CAST(NEW.description as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_REPORT_WRITER_REPORT_3 */
CREATE TRIGGER T_BIU_SB_REPORT_WRITER_REPORT_3 FOR SB_REPORT_WRITER_REPORTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_REPORT_WRITER_REPORTS */
  IF (INSERTING OR (OLD.report_description IS DISTINCT FROM NEW.report_description)) THEN
    IF (EXISTS(SELECT 1 FROM sb_report_writer_reports WHERE report_description = NEW.report_description AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_report_writer_reports', 'report_description',
      CAST(NEW.report_description as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_SALES_TAX_STATES_3 */
CREATE TRIGGER T_BIU_SB_SALES_TAX_STATES_3 FOR SB_SALES_TAX_STATES
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_SALES_TAX_STATES */
  IF (INSERTING OR (OLD.state IS DISTINCT FROM NEW.state)) THEN
    IF (EXISTS(SELECT 1 FROM sb_sales_tax_states WHERE state = NEW.state AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_sales_tax_states', 'state',
      CAST(NEW.state as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_SEC_CLIENTS_2 */
CREATE TRIGGER T_BIU_SB_SEC_CLIENTS_2 FOR SB_SEC_CLIENTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_SEC_GROUPS */
  IF ((NEW.sb_sec_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_clients', NEW.sb_sec_clients_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_date);

    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_clients', NEW.sb_sec_clients_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_until - 1);
  END

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_clients', NEW.sb_sec_clients_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_clients', NEW.sb_sec_clients_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_SEC_GROUPS_3 */
CREATE TRIGGER T_BIU_SB_SEC_GROUPS_3 FOR SB_SEC_GROUPS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_SEC_GROUPS */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_sec_groups WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_sec_groups', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_SEC_GROUP_MEMBERS_2 */
CREATE TRIGGER T_BIU_SB_SEC_GROUP_MEMBERS_2 FOR SB_SEC_GROUP_MEMBERS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_SEC_GROUPS */
  IF ((NEW.sb_sec_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_group_members', NEW.sb_sec_group_members_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_date);

    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_group_members', NEW.sb_sec_group_members_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_until - 1);
  END

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_group_members', NEW.sb_sec_group_members_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_group_members', NEW.sb_sec_group_members_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_SEC_GROUP_MEMBERS_3 */
CREATE TRIGGER T_BIU_SB_SEC_GROUP_MEMBERS_3 FOR SB_SEC_GROUP_MEMBERS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_SEC_GROUP_MEMBERS */
  IF (INSERTING OR (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr) OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_sec_group_members WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND sb_user_nbr = NEW.sb_user_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_sec_group_members', 'sb_sec_groups_nbr, sb_user_nbr',
      CAST(NEW.sb_sec_groups_nbr || ', ' || NEW.sb_user_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_SEC_RIGHTS_2 */
CREATE TRIGGER T_BIU_SB_SEC_RIGHTS_2 FOR SB_SEC_RIGHTS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_SEC_GROUPS */
  IF ((NEW.sb_sec_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_rights', NEW.sb_sec_rights_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_date);

    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_rights', NEW.sb_sec_rights_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_until - 1);
  END

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_rights', NEW.sb_sec_rights_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_rights', NEW.sb_sec_rights_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_SEC_ROW_FILTERS_2 */
CREATE TRIGGER T_BIU_SB_SEC_ROW_FILTERS_2 FOR SB_SEC_ROW_FILTERS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_SEC_GROUPS */
  IF ((NEW.sb_sec_groups_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_sec_groups_nbr IS DISTINCT FROM NEW.sb_sec_groups_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_row_filters', NEW.sb_sec_row_filters_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_date);

    SELECT rec_version FROM sb_sec_groups
    WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_row_filters', NEW.sb_sec_row_filters_nbr, 'sb_sec_groups_nbr', NEW.sb_sec_groups_nbr, 'sb_sec_groups', NEW.effective_until - 1);
  END

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_row_filters', NEW.sb_sec_row_filters_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_sec_row_filters', NEW.sb_sec_row_filters_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_SEC_TEMPLATES_3 */
CREATE TRIGGER T_BIU_SB_SEC_TEMPLATES_3 FOR SB_SEC_TEMPLATES
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_SEC_TEMPLATES */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_sec_templates WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_sec_templates', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_SERVICES_2 */
CREATE TRIGGER T_BIU_SB_SERVICES_2 FOR SB_SERVICES
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_REPORTS */
  IF ((NEW.sb_reports_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_reports
    WHERE sb_reports_nbr = NEW.sb_reports_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_services', NEW.sb_services_nbr, 'sb_reports_nbr', NEW.sb_reports_nbr, 'sb_reports', NEW.effective_date);

    SELECT rec_version FROM sb_reports
    WHERE sb_reports_nbr = NEW.sb_reports_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_services', NEW.sb_services_nbr, 'sb_reports_nbr', NEW.sb_reports_nbr, 'sb_reports', NEW.effective_until - 1);
  END

  /* Check reference to SB_DELIVERY_METHOD */
  IF ((NEW.sb_delivery_method_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_delivery_method
    WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_services', NEW.sb_services_nbr, 'sb_delivery_method_nbr', NEW.sb_delivery_method_nbr, 'sb_delivery_method', NEW.effective_date);

    SELECT rec_version FROM sb_delivery_method
    WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_services', NEW.sb_services_nbr, 'sb_delivery_method_nbr', NEW.sb_delivery_method_nbr, 'sb_delivery_method', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_SERVICES_3 */
CREATE TRIGGER T_BIU_SB_SERVICES_3 FOR SB_SERVICES
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_SERVICES */
  IF (INSERTING OR (OLD.service_name IS DISTINCT FROM NEW.service_name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_services WHERE service_name = NEW.service_name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_services', 'service_name',
      CAST(NEW.service_name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_SERVICES_CALCULATION_2 */
CREATE TRIGGER T_BIU_SB_SERVICES_CALCULATION_2 FOR SB_SERVICES_CALCULATIONS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_SERVICES */
  IF ((NEW.sb_services_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_services_nbr IS DISTINCT FROM NEW.sb_services_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_services
    WHERE sb_services_nbr = NEW.sb_services_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_services_calculations', NEW.sb_services_calculations_nbr, 'sb_services_nbr', NEW.sb_services_nbr, 'sb_services', NEW.effective_date);

    SELECT rec_version FROM sb_services
    WHERE sb_services_nbr = NEW.sb_services_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_services_calculations', NEW.sb_services_calculations_nbr, 'sb_services_nbr', NEW.sb_services_nbr, 'sb_services', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_STORAGE_3 */
CREATE TRIGGER T_BIU_SB_STORAGE_3 FOR SB_STORAGE
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_STORAGE */
  IF (INSERTING OR (OLD.tag IS DISTINCT FROM NEW.tag)) THEN
    IF (EXISTS(SELECT 1 FROM sb_storage WHERE tag = NEW.tag AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_storage', 'tag',
      CAST(NEW.tag as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_TASK_2 */
CREATE TRIGGER T_BIU_SB_TASK_2 FOR SB_TASK
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_task', NEW.sb_task_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_task', NEW.sb_task_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_TEAM_3 */
CREATE TRIGGER T_BIU_SB_TEAM_3 FOR SB_TEAM
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_TEAM */
  IF (INSERTING OR (OLD.team_description IS DISTINCT FROM NEW.team_description)) THEN
    IF (EXISTS(SELECT 1 FROM sb_team WHERE team_description = NEW.team_description AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_team', 'team_description',
      CAST(NEW.team_description as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_TEAM_MEMBERS_2 */
CREATE TRIGGER T_BIU_SB_TEAM_MEMBERS_2 FOR SB_TEAM_MEMBERS
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_TEAM */
  IF ((NEW.sb_team_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_team_nbr IS DISTINCT FROM NEW.sb_team_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_team
    WHERE sb_team_nbr = NEW.sb_team_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_team_members', NEW.sb_team_members_nbr, 'sb_team_nbr', NEW.sb_team_nbr, 'sb_team', NEW.effective_date);

    SELECT rec_version FROM sb_team
    WHERE sb_team_nbr = NEW.sb_team_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_team_members', NEW.sb_team_members_nbr, 'sb_team_nbr', NEW.sb_team_nbr, 'sb_team', NEW.effective_until - 1);
  END

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_team_members', NEW.sb_team_members_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_team_members', NEW.sb_team_members_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_TEAM_MEMBERS_3 */
CREATE TRIGGER T_BIU_SB_TEAM_MEMBERS_3 FOR SB_TEAM_MEMBERS
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_TEAM_MEMBERS */
  IF (INSERTING OR (OLD.sb_team_nbr IS DISTINCT FROM NEW.sb_team_nbr) OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_team_members WHERE sb_team_nbr = NEW.sb_team_nbr AND sb_user_nbr = NEW.sb_user_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_team_members', 'sb_team_nbr, sb_user_nbr',
      CAST(NEW.sb_team_nbr || ', ' || NEW.sb_user_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_USER_2 */
CREATE TRIGGER T_BIU_SB_USER_2 FOR SB_USER
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_ACCOUNTANT */
  IF ((NEW.sb_accountant_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_accountant
    WHERE sb_accountant_nbr = NEW.sb_accountant_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_user', NEW.sb_user_nbr, 'sb_accountant_nbr', NEW.sb_accountant_nbr, 'sb_accountant', NEW.effective_date);

    SELECT rec_version FROM sb_accountant
    WHERE sb_accountant_nbr = NEW.sb_accountant_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_user', NEW.sb_user_nbr, 'sb_accountant_nbr', NEW.sb_accountant_nbr, 'sb_accountant', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_USER_3 */
CREATE TRIGGER T_BIU_SB_USER_3 FOR SB_USER
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_USER */
  IF (INSERTING OR (OLD.user_id IS DISTINCT FROM NEW.user_id)) THEN
    IF (EXISTS(SELECT 1 FROM sb_user WHERE user_id = NEW.user_id AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_user', 'user_id',
      CAST(NEW.user_id as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BIU_SB_USER_NOTICE_2 */
CREATE TRIGGER T_BIU_SB_USER_NOTICE_2 FOR SB_USER_NOTICE
ACTIVE BEFORE INSERT OR UPDATE POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_USER */
  IF ((NEW.sb_user_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_user_nbr IS DISTINCT FROM NEW.sb_user_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_user_notice', NEW.sb_user_notice_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_date);

    SELECT rec_version FROM sb_user
    WHERE sb_user_nbr = NEW.sb_user_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_user_notice', NEW.sb_user_notice_nbr, 'sb_user_nbr', NEW.sb_user_nbr, 'sb_user', NEW.effective_until - 1);
  END
END
^


/* Trigger: T_BIU_SB_USER_NOTICE_3 */
CREATE TRIGGER T_BIU_SB_USER_NOTICE_3 FOR SB_USER_NOTICE
ACTIVE BEFORE INSERT OR UPDATE POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_USER_NOTICE */
  IF (INSERTING OR (OLD.name IS DISTINCT FROM NEW.name)) THEN
    IF (EXISTS(SELECT 1 FROM sb_user_notice WHERE name = NEW.name AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_user_notice', 'name',
      CAST(NEW.name as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


/* Trigger: T_BI_EV_FIELD_CHANGE_BLOB_1 */
CREATE TRIGGER T_BI_EV_FIELD_CHANGE_BLOB_1 FOR EV_FIELD_CHANGE_BLOB
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  IF (NEW.nbr IS NULL) THEN
    NEW.nbr = NEXT VALUE FOR g_ev_field_change_blob;
END
^


/* Trigger: T_BI_EV_TABLE_CHANGE_1 */
CREATE TRIGGER T_BI_EV_TABLE_CHANGE_1 FOR EV_TABLE_CHANGE
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  IF (NEW.nbr IS NULL) THEN
    NEW.nbr = NEXT VALUE FOR g_ev_table_change;

  IF (NEW.ev_transaction_nbr IS NULL) THEN
    NEW.ev_transaction_nbr = rdb$get_context('USER_TRANSACTION', 'TRN_ID');
END
^


/* Trigger: T_BI_SB_1 */
CREATE TRIGGER T_BI_SB_1 FOR SB
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_ver;

  IF (EXISTS (SELECT 1 FROM sb WHERE sb_nbr = NEW.sb_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb', NEW.sb_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_ACCOUNTANT_1 */
CREATE TRIGGER T_BI_SB_ACCOUNTANT_1 FOR SB_ACCOUNTANT
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_accountant_ver;

  IF (EXISTS (SELECT 1 FROM sb_accountant WHERE sb_accountant_nbr = NEW.sb_accountant_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_accountant', NEW.sb_accountant_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_AGENCY_1 */
CREATE TRIGGER T_BI_SB_AGENCY_1 FOR SB_AGENCY
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_agency_ver;

  IF (EXISTS (SELECT 1 FROM sb_agency WHERE sb_agency_nbr = NEW.sb_agency_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_agency', NEW.sb_agency_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_AGENCY_REPORTS_1 */
CREATE TRIGGER T_BI_SB_AGENCY_REPORTS_1 FOR SB_AGENCY_REPORTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_agency_reports_ver;

  IF (EXISTS (SELECT 1 FROM sb_agency_reports WHERE sb_agency_reports_nbr = NEW.sb_agency_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_agency_reports', NEW.sb_agency_reports_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_BANKS_1 */
CREATE TRIGGER T_BI_SB_BANKS_1 FOR SB_BANKS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_banks_ver;

  IF (EXISTS (SELECT 1 FROM sb_banks WHERE sb_banks_nbr = NEW.sb_banks_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_banks', NEW.sb_banks_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_BANK_ACCOUNTS_1 */
CREATE TRIGGER T_BI_SB_BANK_ACCOUNTS_1 FOR SB_BANK_ACCOUNTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_bank_accounts_ver;

  IF (EXISTS (SELECT 1 FROM sb_bank_accounts WHERE sb_bank_accounts_nbr = NEW.sb_bank_accounts_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_bank_accounts', NEW.sb_bank_accounts_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_BLOB_1 */
CREATE TRIGGER T_BI_SB_BLOB_1 FOR SB_BLOB
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_blob_ver;

  IF (EXISTS (SELECT 1 FROM sb_blob WHERE sb_blob_nbr = NEW.sb_blob_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_blob', NEW.sb_blob_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_DELIVERY_COMPANY_1 */
CREATE TRIGGER T_BI_SB_DELIVERY_COMPANY_1 FOR SB_DELIVERY_COMPANY
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_delivery_company_ver;

  IF (EXISTS (SELECT 1 FROM sb_delivery_company WHERE sb_delivery_company_nbr = NEW.sb_delivery_company_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_delivery_company', NEW.sb_delivery_company_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_DELIVERY_COMPANY_SVC_1 */
CREATE TRIGGER T_BI_SB_DELIVERY_COMPANY_SVC_1 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_delivery_company_svcs_ver;

  IF (EXISTS (SELECT 1 FROM sb_delivery_company_svcs WHERE sb_delivery_company_svcs_nbr = NEW.sb_delivery_company_svcs_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_delivery_company_svcs', NEW.sb_delivery_company_svcs_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_DELIVERY_METHOD_1 */
CREATE TRIGGER T_BI_SB_DELIVERY_METHOD_1 FOR SB_DELIVERY_METHOD
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_delivery_method_ver;

  IF (EXISTS (SELECT 1 FROM sb_delivery_method WHERE sb_delivery_method_nbr = NEW.sb_delivery_method_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_delivery_method', NEW.sb_delivery_method_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_DELIVERY_SERVICE_1 */
CREATE TRIGGER T_BI_SB_DELIVERY_SERVICE_1 FOR SB_DELIVERY_SERVICE
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_delivery_service_ver;

  IF (EXISTS (SELECT 1 FROM sb_delivery_service WHERE sb_delivery_service_nbr = NEW.sb_delivery_service_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_delivery_service', NEW.sb_delivery_service_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_DELIVERY_SERVICE_OPT_1 */
CREATE TRIGGER T_BI_SB_DELIVERY_SERVICE_OPT_1 FOR SB_DELIVERY_SERVICE_OPT
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_delivery_service_opt_ver;

  IF (EXISTS (SELECT 1 FROM sb_delivery_service_opt WHERE sb_delivery_service_opt_nbr = NEW.sb_delivery_service_opt_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_delivery_service_opt', NEW.sb_delivery_service_opt_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_ENLIST_GROUPS_1 */
CREATE TRIGGER T_BI_SB_ENLIST_GROUPS_1 FOR SB_ENLIST_GROUPS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_enlist_groups_ver;

  IF (EXISTS (SELECT 1 FROM sb_enlist_groups WHERE sb_enlist_groups_nbr = NEW.sb_enlist_groups_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_enlist_groups', NEW.sb_enlist_groups_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_GLOBAL_AGENCY_CONTAC_1 */
CREATE TRIGGER T_BI_SB_GLOBAL_AGENCY_CONTAC_1 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_global_agency_contacts_ver;

  IF (EXISTS (SELECT 1 FROM sb_global_agency_contacts WHERE sb_global_agency_contacts_nbr = NEW.sb_global_agency_contacts_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_global_agency_contacts', NEW.sb_global_agency_contacts_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_HOLIDAYS_1 */
CREATE TRIGGER T_BI_SB_HOLIDAYS_1 FOR SB_HOLIDAYS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_holidays_ver;

  IF (EXISTS (SELECT 1 FROM sb_holidays WHERE sb_holidays_nbr = NEW.sb_holidays_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_holidays', NEW.sb_holidays_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_MAIL_BOX_1 */
CREATE TRIGGER T_BI_SB_MAIL_BOX_1 FOR SB_MAIL_BOX
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_mail_box_ver;

  IF (EXISTS (SELECT 1 FROM sb_mail_box WHERE sb_mail_box_nbr = NEW.sb_mail_box_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_mail_box', NEW.sb_mail_box_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_MAIL_BOX_CONTENT_1 */
CREATE TRIGGER T_BI_SB_MAIL_BOX_CONTENT_1 FOR SB_MAIL_BOX_CONTENT
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_mail_box_content_ver;

  IF (EXISTS (SELECT 1 FROM sb_mail_box_content WHERE sb_mail_box_content_nbr = NEW.sb_mail_box_content_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_mail_box_content', NEW.sb_mail_box_content_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_MAIL_BOX_OPTION_1 */
CREATE TRIGGER T_BI_SB_MAIL_BOX_OPTION_1 FOR SB_MAIL_BOX_OPTION
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_mail_box_option_ver;

  IF (EXISTS (SELECT 1 FROM sb_mail_box_option WHERE sb_mail_box_option_nbr = NEW.sb_mail_box_option_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_mail_box_option', NEW.sb_mail_box_option_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_MEDIA_TYPE_1 */
CREATE TRIGGER T_BI_SB_MEDIA_TYPE_1 FOR SB_MEDIA_TYPE
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_media_type_ver;

  IF (EXISTS (SELECT 1 FROM sb_media_type WHERE sb_media_type_nbr = NEW.sb_media_type_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_media_type', NEW.sb_media_type_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_MEDIA_TYPE_OPTION_1 */
CREATE TRIGGER T_BI_SB_MEDIA_TYPE_OPTION_1 FOR SB_MEDIA_TYPE_OPTION
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_media_type_option_ver;

  IF (EXISTS (SELECT 1 FROM sb_media_type_option WHERE sb_media_type_option_nbr = NEW.sb_media_type_option_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_media_type_option', NEW.sb_media_type_option_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_MULTICLIENT_REPORTS_1 */
CREATE TRIGGER T_BI_SB_MULTICLIENT_REPORTS_1 FOR SB_MULTICLIENT_REPORTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_multiclient_reports_ver;

  IF (EXISTS (SELECT 1 FROM sb_multiclient_reports WHERE sb_multiclient_reports_nbr = NEW.sb_multiclient_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_multiclient_reports', NEW.sb_multiclient_reports_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_OPTION_1 */
CREATE TRIGGER T_BI_SB_OPTION_1 FOR SB_OPTION
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_option_ver;

  IF (EXISTS (SELECT 1 FROM sb_option WHERE sb_option_nbr = NEW.sb_option_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_option', NEW.sb_option_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_OTHER_SERVICE_1 */
CREATE TRIGGER T_BI_SB_OTHER_SERVICE_1 FOR SB_OTHER_SERVICE
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_other_service_ver;

  IF (EXISTS (SELECT 1 FROM sb_other_service WHERE sb_other_service_nbr = NEW.sb_other_service_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_other_service', NEW.sb_other_service_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_PAPER_INFO_1 */
CREATE TRIGGER T_BI_SB_PAPER_INFO_1 FOR SB_PAPER_INFO
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_paper_info_ver;

  IF (EXISTS (SELECT 1 FROM sb_paper_info WHERE sb_paper_info_nbr = NEW.sb_paper_info_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_paper_info', NEW.sb_paper_info_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_QUEUE_PRIORITY_1 */
CREATE TRIGGER T_BI_SB_QUEUE_PRIORITY_1 FOR SB_QUEUE_PRIORITY
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_queue_priority_ver;

  IF (EXISTS (SELECT 1 FROM sb_queue_priority WHERE sb_queue_priority_nbr = NEW.sb_queue_priority_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_queue_priority', NEW.sb_queue_priority_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_REFERRALS_1 */
CREATE TRIGGER T_BI_SB_REFERRALS_1 FOR SB_REFERRALS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_referrals_ver;

  IF (EXISTS (SELECT 1 FROM sb_referrals WHERE sb_referrals_nbr = NEW.sb_referrals_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_referrals', NEW.sb_referrals_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_REPORTS_1 */
CREATE TRIGGER T_BI_SB_REPORTS_1 FOR SB_REPORTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_reports_ver;

  IF (EXISTS (SELECT 1 FROM sb_reports WHERE sb_reports_nbr = NEW.sb_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_reports', NEW.sb_reports_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_REPORT_WRITER_REPORT_1 */
CREATE TRIGGER T_BI_SB_REPORT_WRITER_REPORT_1 FOR SB_REPORT_WRITER_REPORTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_report_writer_reports_ver;

  IF (EXISTS (SELECT 1 FROM sb_report_writer_reports WHERE sb_report_writer_reports_nbr = NEW.sb_report_writer_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_report_writer_reports', NEW.sb_report_writer_reports_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SALES_TAX_STATES_1 */
CREATE TRIGGER T_BI_SB_SALES_TAX_STATES_1 FOR SB_SALES_TAX_STATES
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sales_tax_states_ver;

  IF (EXISTS (SELECT 1 FROM sb_sales_tax_states WHERE sb_sales_tax_states_nbr = NEW.sb_sales_tax_states_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sales_tax_states', NEW.sb_sales_tax_states_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SEC_CLIENTS_1 */
CREATE TRIGGER T_BI_SB_SEC_CLIENTS_1 FOR SB_SEC_CLIENTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sec_clients_ver;

  IF (EXISTS (SELECT 1 FROM sb_sec_clients WHERE sb_sec_clients_nbr = NEW.sb_sec_clients_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sec_clients', NEW.sb_sec_clients_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SEC_GROUPS_1 */
CREATE TRIGGER T_BI_SB_SEC_GROUPS_1 FOR SB_SEC_GROUPS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sec_groups_ver;

  IF (EXISTS (SELECT 1 FROM sb_sec_groups WHERE sb_sec_groups_nbr = NEW.sb_sec_groups_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sec_groups', NEW.sb_sec_groups_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SEC_GROUP_MEMBERS_1 */
CREATE TRIGGER T_BI_SB_SEC_GROUP_MEMBERS_1 FOR SB_SEC_GROUP_MEMBERS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sec_group_members_ver;

  IF (EXISTS (SELECT 1 FROM sb_sec_group_members WHERE sb_sec_group_members_nbr = NEW.sb_sec_group_members_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sec_group_members', NEW.sb_sec_group_members_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SEC_RIGHTS_1 */
CREATE TRIGGER T_BI_SB_SEC_RIGHTS_1 FOR SB_SEC_RIGHTS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sec_rights_ver;

  IF (EXISTS (SELECT 1 FROM sb_sec_rights WHERE sb_sec_rights_nbr = NEW.sb_sec_rights_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sec_rights', NEW.sb_sec_rights_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SEC_ROW_FILTERS_1 */
CREATE TRIGGER T_BI_SB_SEC_ROW_FILTERS_1 FOR SB_SEC_ROW_FILTERS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sec_row_filters_ver;

  IF (EXISTS (SELECT 1 FROM sb_sec_row_filters WHERE sb_sec_row_filters_nbr = NEW.sb_sec_row_filters_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sec_row_filters', NEW.sb_sec_row_filters_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SEC_TEMPLATES_1 */
CREATE TRIGGER T_BI_SB_SEC_TEMPLATES_1 FOR SB_SEC_TEMPLATES
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_sec_templates_ver;

  IF (EXISTS (SELECT 1 FROM sb_sec_templates WHERE sb_sec_templates_nbr = NEW.sb_sec_templates_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_sec_templates', NEW.sb_sec_templates_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SERVICES_1 */
CREATE TRIGGER T_BI_SB_SERVICES_1 FOR SB_SERVICES
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_services_ver;

  IF (EXISTS (SELECT 1 FROM sb_services WHERE sb_services_nbr = NEW.sb_services_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_services', NEW.sb_services_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_SERVICES_CALCULATION_1 */
CREATE TRIGGER T_BI_SB_SERVICES_CALCULATION_1 FOR SB_SERVICES_CALCULATIONS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_services_calculations_ver;

  IF (EXISTS (SELECT 1 FROM sb_services_calculations WHERE sb_services_calculations_nbr = NEW.sb_services_calculations_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_services_calculations', NEW.sb_services_calculations_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_STORAGE_1 */
CREATE TRIGGER T_BI_SB_STORAGE_1 FOR SB_STORAGE
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_storage_ver;

  IF (EXISTS (SELECT 1 FROM sb_storage WHERE sb_storage_nbr = NEW.sb_storage_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_storage', NEW.sb_storage_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_TASK_1 */
CREATE TRIGGER T_BI_SB_TASK_1 FOR SB_TASK
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_task_ver;

  IF (EXISTS (SELECT 1 FROM sb_task WHERE sb_task_nbr = NEW.sb_task_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_task', NEW.sb_task_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_TAX_PAYMENT_1 */
CREATE TRIGGER T_BI_SB_TAX_PAYMENT_1 FOR SB_TAX_PAYMENT
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_tax_payment_ver;

  IF (EXISTS (SELECT 1 FROM sb_tax_payment WHERE sb_tax_payment_nbr = NEW.sb_tax_payment_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_tax_payment', NEW.sb_tax_payment_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_TEAM_1 */
CREATE TRIGGER T_BI_SB_TEAM_1 FOR SB_TEAM
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_team_ver;

  IF (EXISTS (SELECT 1 FROM sb_team WHERE sb_team_nbr = NEW.sb_team_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_team', NEW.sb_team_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_TEAM_MEMBERS_1 */
CREATE TRIGGER T_BI_SB_TEAM_MEMBERS_1 FOR SB_TEAM_MEMBERS
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_team_members_ver;

  IF (EXISTS (SELECT 1 FROM sb_team_members WHERE sb_team_members_nbr = NEW.sb_team_members_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_team_members', NEW.sb_team_members_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_USER_1 */
CREATE TRIGGER T_BI_SB_USER_1 FOR SB_USER
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_user_ver;

  IF (EXISTS (SELECT 1 FROM sb_user WHERE sb_user_nbr = NEW.sb_user_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_user', NEW.sb_user_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BI_SB_USER_NOTICE_1 */
CREATE TRIGGER T_BI_SB_USER_NOTICE_1 FOR SB_USER_NOTICE
ACTIVE BEFORE INSERT POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_user_notice_ver;

  IF (EXISTS (SELECT 1 FROM sb_user_notice WHERE sb_user_notice_nbr = NEW.sb_user_notice_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_user_notice', NEW.sb_user_notice_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BUD_EV_FIELD_CHANGE_1 */
CREATE TRIGGER T_BUD_EV_FIELD_CHANGE_1 FOR EV_FIELD_CHANGE
ACTIVE BEFORE UPDATE OR DELETE POSITION 1
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) THEN
    EXECUTE PROCEDURE raise_error('S3', 'Table EV_FIELD_CHANGE cannot be modified');
END
^


/* Trigger: T_BUD_EV_FIELD_CHANGE_BLOB_1 */
CREATE TRIGGER T_BUD_EV_FIELD_CHANGE_BLOB_1 FOR EV_FIELD_CHANGE_BLOB
ACTIVE BEFORE UPDATE OR DELETE POSITION 1
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) THEN
    EXECUTE PROCEDURE raise_error('S3', 'Table EV_FIELD_CHANGE_BLOB cannot be modified');
END
^


/* Trigger: T_BUD_SB_9 */
CREATE TRIGGER T_BUD_SB_9 FOR SB
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* COVER_LETTER_NOTES */
  IF (DELETING OR (OLD.cover_letter_notes IS DISTINCT FROM NEW.cover_letter_notes)) THEN
  BEGIN
    IF (OLD.cover_letter_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.cover_letter_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', blob_nbr);
  END

  /* INVOICE_NOTES */
  IF (DELETING OR (OLD.invoice_notes IS DISTINCT FROM NEW.invoice_notes)) THEN
  BEGIN
    IF (OLD.invoice_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.invoice_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  IF (DELETING OR (OLD.tax_cover_letter_notes IS DISTINCT FROM NEW.tax_cover_letter_notes)) THEN
  BEGIN
    IF (OLD.tax_cover_letter_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.tax_cover_letter_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', blob_nbr);
  END

  /* SB_LOGO */
  IF (DELETING OR (OLD.sb_logo IS DISTINCT FROM NEW.sb_logo)) THEN
  BEGIN
    IF (OLD.sb_logo IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.sb_logo)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@SB_LOGO', blob_nbr);
  END

  /* VMR_CONFIDENCIAL_NOTES */
  IF (DELETING OR (OLD.vmr_confidencial_notes IS DISTINCT FROM NEW.vmr_confidencial_notes)) THEN
  BEGIN
    IF (OLD.vmr_confidencial_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.vmr_confidencial_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', blob_nbr);
  END

  /* ESS_TERMS_OF_USE */
  IF (DELETING OR (OLD.ess_terms_of_use IS DISTINCT FROM NEW.ess_terms_of_use)) THEN
  BEGIN
    IF (OLD.ess_terms_of_use IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.ess_terms_of_use)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  IF (DELETING OR (OLD.wc_terms_of_use IS DISTINCT FROM NEW.wc_terms_of_use)) THEN
  BEGIN
    IF (OLD.wc_terms_of_use IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.wc_terms_of_use)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_ACCOUNTANT_9 */
CREATE TRIGGER T_BUD_SB_ACCOUNTANT_9 FOR SB_ACCOUNTANT
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* SIGNATURE */
  IF (DELETING OR (OLD.signature IS DISTINCT FROM NEW.signature)) THEN
  BEGIN
    IF (OLD.signature IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.signature)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@SIGNATURE', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_AGENCY_9 */
CREATE TRIGGER T_BUD_SB_AGENCY_9 FOR SB_AGENCY
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_BANKS_9 */
CREATE TRIGGER T_BUD_SB_BANKS_9 FOR SB_BANKS
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* CHECK_TEMPLATE */
  IF (DELETING OR (OLD.check_template IS DISTINCT FROM NEW.check_template)) THEN
  BEGIN
    IF (OLD.check_template IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.check_template)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@CHECK_TEMPLATE', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_BLOB_9 */
CREATE TRIGGER T_BUD_SB_BLOB_9 FOR SB_BLOB
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* DATA */
  IF (DELETING OR (OLD.data IS DISTINCT FROM NEW.data)) THEN
  BEGIN
    IF (OLD.data IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.data)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@DATA', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_DELIVERY_COMPANY_9 */
CREATE TRIGGER T_BUD_SB_DELIVERY_COMPANY_9 FOR SB_DELIVERY_COMPANY
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* DELIVERY_COMPANY_NOTES */
  IF (DELETING OR (OLD.delivery_company_notes IS DISTINCT FROM NEW.delivery_company_notes)) THEN
  BEGIN
    IF (OLD.delivery_company_notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.delivery_company_notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@DELIVERY_COMPANY_NOTES', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_GLOBAL_AGENCY_CONTAC_9 */
CREATE TRIGGER T_BUD_SB_GLOBAL_AGENCY_CONTAC_9 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_MULTICLIENT_REPORTS_9 */
CREATE TRIGGER T_BUD_SB_MULTICLIENT_REPORTS_9 FOR SB_MULTICLIENT_REPORTS
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* INPUT_PARAMS */
  IF (DELETING OR (OLD.input_params IS DISTINCT FROM NEW.input_params)) THEN
  BEGIN
    IF (OLD.input_params IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.input_params)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@INPUT_PARAMS', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_REPORTS_9 */
CREATE TRIGGER T_BUD_SB_REPORTS_9 FOR SB_REPORTS
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* COMMENTS */
  IF (DELETING OR (OLD.comments IS DISTINCT FROM NEW.comments)) THEN
  BEGIN
    IF (OLD.comments IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.comments)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@COMMENTS', blob_nbr);
  END

  /* INPUT_PARAMS */
  IF (DELETING OR (OLD.input_params IS DISTINCT FROM NEW.input_params)) THEN
  BEGIN
    IF (OLD.input_params IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.input_params)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@INPUT_PARAMS', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_REPORT_WRITER_REPORT_9 */
CREATE TRIGGER T_BUD_SB_REPORT_WRITER_REPORT_9 FOR SB_REPORT_WRITER_REPORTS
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* REPORT_FILE */
  IF (DELETING OR (OLD.report_file IS DISTINCT FROM NEW.report_file)) THEN
  BEGIN
    IF (OLD.report_file IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.report_file)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@REPORT_FILE', blob_nbr);
  END

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_SEC_TEMPLATES_9 */
CREATE TRIGGER T_BUD_SB_SEC_TEMPLATES_9 FOR SB_SEC_TEMPLATES
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* TEMPLATE */
  IF (DELETING OR (OLD.template IS DISTINCT FROM NEW.template)) THEN
  BEGIN
    IF (OLD.template IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.template)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@TEMPLATE', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_STORAGE_9 */
CREATE TRIGGER T_BUD_SB_STORAGE_9 FOR SB_STORAGE
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* STORAGE_DATA */
  IF (DELETING OR (OLD.storage_data IS DISTINCT FROM NEW.storage_data)) THEN
  BEGIN
    IF (OLD.storage_data IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.storage_data)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@STORAGE_DATA', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_TASK_9 */
CREATE TRIGGER T_BUD_SB_TASK_9 FOR SB_TASK
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* TASK */
  IF (DELETING OR (OLD.task IS DISTINCT FROM NEW.task)) THEN
  BEGIN
    IF (OLD.task IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.task)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@TASK', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_USER_9 */
CREATE TRIGGER T_BUD_SB_USER_9 FOR SB_USER
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* USER_SIGNATURE */
  IF (DELETING OR (OLD.user_signature IS DISTINCT FROM NEW.user_signature)) THEN
  BEGIN
    IF (OLD.user_signature IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.user_signature)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@USER_SIGNATURE', blob_nbr);
  END
END
^


/* Trigger: T_BUD_SB_USER_NOTICE_9 */
CREATE TRIGGER T_BUD_SB_USER_NOTICE_9 FOR SB_USER_NOTICE
ACTIVE BEFORE UPDATE OR DELETE POSITION 9
AS
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  /* Workaround for http://tracker.firebirdsql.org/browse/CORE-1847 */

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* NOTES */
  IF (DELETING OR (OLD.notes IS DISTINCT FROM NEW.notes)) THEN
  BEGIN
    IF (OLD.notes IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.notes)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@NOTES', blob_nbr);
  END

  /* TASK */
  IF (DELETING OR (OLD.task IS DISTINCT FROM NEW.task)) THEN
  BEGIN
    IF (OLD.task IS NULL) THEN
      blob_nbr = 0;
    ELSE
      INSERT INTO ev_field_change_blob (data) VALUES(OLD.task)
      RETURNING nbr INTO :blob_nbr;

    IF (NOT (DELETING AND (blob_nbr = 0))) THEN
      rdb$set_context('USER_TRANSACTION', '@TASK', blob_nbr);
  END
END
^


/* Trigger: T_BU_EV_TABLE_CHANGE_1 */
CREATE TRIGGER T_BU_EV_TABLE_CHANGE_1 FOR EV_TABLE_CHANGE
ACTIVE BEFORE UPDATE POSITION 1
AS
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) THEN
    EXECUTE PROCEDURE raise_error('S3', 'Table EV_TABLE_CHANGE cannot be modified');
END
^


/* Trigger: T_BU_EV_TRANSACTION_1 */
CREATE TRIGGER T_BU_EV_TRANSACTION_1 FOR EV_TRANSACTION
ACTIVE BEFORE UPDATE POSITION 1
AS
BEGIN
  NEW.USER_ID = OLD.USER_ID;
  NEW.START_TIME = OLD.START_TIME;
END
^


/* Trigger: T_BU_SB_1 */
CREATE TRIGGER T_BU_SB_1 FOR SB
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_nbr IS DISTINCT FROM OLD.sb_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_ACCOUNTANT_1 */
CREATE TRIGGER T_BU_SB_ACCOUNTANT_1 FOR SB_ACCOUNTANT
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_accountant_nbr IS DISTINCT FROM OLD.sb_accountant_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_accountant', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_AGENCY_1 */
CREATE TRIGGER T_BU_SB_AGENCY_1 FOR SB_AGENCY
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_agency_nbr IS DISTINCT FROM OLD.sb_agency_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_agency', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_AGENCY_REPORTS_1 */
CREATE TRIGGER T_BU_SB_AGENCY_REPORTS_1 FOR SB_AGENCY_REPORTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_agency_reports_nbr IS DISTINCT FROM OLD.sb_agency_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_agency_reports', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_BANKS_1 */
CREATE TRIGGER T_BU_SB_BANKS_1 FOR SB_BANKS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_banks_nbr IS DISTINCT FROM OLD.sb_banks_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_banks', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_BANK_ACCOUNTS_1 */
CREATE TRIGGER T_BU_SB_BANK_ACCOUNTS_1 FOR SB_BANK_ACCOUNTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_bank_accounts_nbr IS DISTINCT FROM OLD.sb_bank_accounts_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_bank_accounts', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_BLOB_1 */
CREATE TRIGGER T_BU_SB_BLOB_1 FOR SB_BLOB
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_blob_nbr IS DISTINCT FROM OLD.sb_blob_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_blob', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_DELIVERY_COMPANY_1 */
CREATE TRIGGER T_BU_SB_DELIVERY_COMPANY_1 FOR SB_DELIVERY_COMPANY
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_delivery_company_nbr IS DISTINCT FROM OLD.sb_delivery_company_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_delivery_company', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_DELIVERY_COMPANY_SVC_1 */
CREATE TRIGGER T_BU_SB_DELIVERY_COMPANY_SVC_1 FOR SB_DELIVERY_COMPANY_SVCS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_delivery_company_svcs_nbr IS DISTINCT FROM OLD.sb_delivery_company_svcs_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_delivery_company_svcs', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_DELIVERY_METHOD_1 */
CREATE TRIGGER T_BU_SB_DELIVERY_METHOD_1 FOR SB_DELIVERY_METHOD
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_delivery_method_nbr IS DISTINCT FROM OLD.sb_delivery_method_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_delivery_method', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_DELIVERY_SERVICE_1 */
CREATE TRIGGER T_BU_SB_DELIVERY_SERVICE_1 FOR SB_DELIVERY_SERVICE
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_delivery_service_nbr IS DISTINCT FROM OLD.sb_delivery_service_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_delivery_service', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_DELIVERY_SERVICE_OPT_1 */
CREATE TRIGGER T_BU_SB_DELIVERY_SERVICE_OPT_1 FOR SB_DELIVERY_SERVICE_OPT
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_delivery_service_opt_nbr IS DISTINCT FROM OLD.sb_delivery_service_opt_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_delivery_service_opt', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_ENLIST_GROUPS_1 */
CREATE TRIGGER T_BU_SB_ENLIST_GROUPS_1 FOR SB_ENLIST_GROUPS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_enlist_groups_nbr IS DISTINCT FROM OLD.sb_enlist_groups_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_enlist_groups', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_GLOBAL_AGENCY_CONTAC_1 */
CREATE TRIGGER T_BU_SB_GLOBAL_AGENCY_CONTAC_1 FOR SB_GLOBAL_AGENCY_CONTACTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_global_agency_contacts_nbr IS DISTINCT FROM OLD.sb_global_agency_contacts_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_global_agency_contacts', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_HOLIDAYS_1 */
CREATE TRIGGER T_BU_SB_HOLIDAYS_1 FOR SB_HOLIDAYS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_holidays_nbr IS DISTINCT FROM OLD.sb_holidays_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_holidays', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_MAIL_BOX_1 */
CREATE TRIGGER T_BU_SB_MAIL_BOX_1 FOR SB_MAIL_BOX
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_mail_box_nbr IS DISTINCT FROM OLD.sb_mail_box_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_mail_box', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_MAIL_BOX_CONTENT_1 */
CREATE TRIGGER T_BU_SB_MAIL_BOX_CONTENT_1 FOR SB_MAIL_BOX_CONTENT
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_mail_box_content_nbr IS DISTINCT FROM OLD.sb_mail_box_content_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_mail_box_content', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_MAIL_BOX_OPTION_1 */
CREATE TRIGGER T_BU_SB_MAIL_BOX_OPTION_1 FOR SB_MAIL_BOX_OPTION
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_mail_box_option_nbr IS DISTINCT FROM OLD.sb_mail_box_option_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_mail_box_option', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_MEDIA_TYPE_1 */
CREATE TRIGGER T_BU_SB_MEDIA_TYPE_1 FOR SB_MEDIA_TYPE
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_media_type_nbr IS DISTINCT FROM OLD.sb_media_type_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_media_type', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_MEDIA_TYPE_OPTION_1 */
CREATE TRIGGER T_BU_SB_MEDIA_TYPE_OPTION_1 FOR SB_MEDIA_TYPE_OPTION
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_media_type_option_nbr IS DISTINCT FROM OLD.sb_media_type_option_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_media_type_option', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_MULTICLIENT_REPORTS_1 */
CREATE TRIGGER T_BU_SB_MULTICLIENT_REPORTS_1 FOR SB_MULTICLIENT_REPORTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_multiclient_reports_nbr IS DISTINCT FROM OLD.sb_multiclient_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_multiclient_reports', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_OPTION_1 */
CREATE TRIGGER T_BU_SB_OPTION_1 FOR SB_OPTION
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_option_nbr IS DISTINCT FROM OLD.sb_option_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_option', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_OTHER_SERVICE_1 */
CREATE TRIGGER T_BU_SB_OTHER_SERVICE_1 FOR SB_OTHER_SERVICE
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_other_service_nbr IS DISTINCT FROM OLD.sb_other_service_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_other_service', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_PAPER_INFO_1 */
CREATE TRIGGER T_BU_SB_PAPER_INFO_1 FOR SB_PAPER_INFO
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_paper_info_nbr IS DISTINCT FROM OLD.sb_paper_info_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_paper_info', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_QUEUE_PRIORITY_1 */
CREATE TRIGGER T_BU_SB_QUEUE_PRIORITY_1 FOR SB_QUEUE_PRIORITY
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_queue_priority_nbr IS DISTINCT FROM OLD.sb_queue_priority_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_queue_priority', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_REFERRALS_1 */
CREATE TRIGGER T_BU_SB_REFERRALS_1 FOR SB_REFERRALS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_referrals_nbr IS DISTINCT FROM OLD.sb_referrals_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_referrals', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_REPORTS_1 */
CREATE TRIGGER T_BU_SB_REPORTS_1 FOR SB_REPORTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_reports_nbr IS DISTINCT FROM OLD.sb_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_reports', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_REPORT_WRITER_REPORT_1 */
CREATE TRIGGER T_BU_SB_REPORT_WRITER_REPORT_1 FOR SB_REPORT_WRITER_REPORTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_report_writer_reports_nbr IS DISTINCT FROM OLD.sb_report_writer_reports_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_report_writer_reports', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SALES_TAX_STATES_1 */
CREATE TRIGGER T_BU_SB_SALES_TAX_STATES_1 FOR SB_SALES_TAX_STATES
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sales_tax_states_nbr IS DISTINCT FROM OLD.sb_sales_tax_states_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sales_tax_states', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SEC_CLIENTS_1 */
CREATE TRIGGER T_BU_SB_SEC_CLIENTS_1 FOR SB_SEC_CLIENTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sec_clients_nbr IS DISTINCT FROM OLD.sb_sec_clients_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sec_clients', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SEC_GROUPS_1 */
CREATE TRIGGER T_BU_SB_SEC_GROUPS_1 FOR SB_SEC_GROUPS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sec_groups_nbr IS DISTINCT FROM OLD.sb_sec_groups_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sec_groups', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SEC_GROUP_MEMBERS_1 */
CREATE TRIGGER T_BU_SB_SEC_GROUP_MEMBERS_1 FOR SB_SEC_GROUP_MEMBERS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sec_group_members_nbr IS DISTINCT FROM OLD.sb_sec_group_members_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sec_group_members', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SEC_RIGHTS_1 */
CREATE TRIGGER T_BU_SB_SEC_RIGHTS_1 FOR SB_SEC_RIGHTS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sec_rights_nbr IS DISTINCT FROM OLD.sb_sec_rights_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sec_rights', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SEC_ROW_FILTERS_1 */
CREATE TRIGGER T_BU_SB_SEC_ROW_FILTERS_1 FOR SB_SEC_ROW_FILTERS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sec_row_filters_nbr IS DISTINCT FROM OLD.sb_sec_row_filters_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sec_row_filters', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SEC_TEMPLATES_1 */
CREATE TRIGGER T_BU_SB_SEC_TEMPLATES_1 FOR SB_SEC_TEMPLATES
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_sec_templates_nbr IS DISTINCT FROM OLD.sb_sec_templates_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_sec_templates', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SERVICES_1 */
CREATE TRIGGER T_BU_SB_SERVICES_1 FOR SB_SERVICES
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_services_nbr IS DISTINCT FROM OLD.sb_services_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_services', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_SERVICES_CALCULATION_1 */
CREATE TRIGGER T_BU_SB_SERVICES_CALCULATION_1 FOR SB_SERVICES_CALCULATIONS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_services_calculations_nbr IS DISTINCT FROM OLD.sb_services_calculations_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_services_calculations', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_STORAGE_1 */
CREATE TRIGGER T_BU_SB_STORAGE_1 FOR SB_STORAGE
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_storage_nbr IS DISTINCT FROM OLD.sb_storage_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_storage', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_TASK_1 */
CREATE TRIGGER T_BU_SB_TASK_1 FOR SB_TASK
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_task_nbr IS DISTINCT FROM OLD.sb_task_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_task', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_TAX_PAYMENT_1 */
CREATE TRIGGER T_BU_SB_TAX_PAYMENT_1 FOR SB_TAX_PAYMENT
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_tax_payment_nbr IS DISTINCT FROM OLD.sb_tax_payment_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_tax_payment', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_TEAM_1 */
CREATE TRIGGER T_BU_SB_TEAM_1 FOR SB_TEAM
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_team_nbr IS DISTINCT FROM OLD.sb_team_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_team', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_TEAM_MEMBERS_1 */
CREATE TRIGGER T_BU_SB_TEAM_MEMBERS_1 FOR SB_TEAM_MEMBERS
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_team_members_nbr IS DISTINCT FROM OLD.sb_team_members_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_team_members', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_USER_1 */
CREATE TRIGGER T_BU_SB_USER_1 FOR SB_USER
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_user_nbr IS DISTINCT FROM OLD.sb_user_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_user', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


/* Trigger: T_BU_SB_USER_NOTICE_1 */
CREATE TRIGGER T_BU_SB_USER_NOTICE_1 FOR SB_USER_NOTICE
ACTIVE BEFORE UPDATE POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_user_notice_nbr IS DISTINCT FROM OLD.sb_user_notice_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_user_notice', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END
^


SET TERM ; ^



/******************************************************************************/
/****                          Stored Procedures                           ****/
/******************************************************************************/


SET TERM ^ ;

ALTER PROCEDURE ALL_CL_NUMBERS (
    DB_PATH VARCHAR(255))
RETURNS (
    CL_NBR INTEGER)
AS
DECLARE VARIABLE temp_nbr INTEGER;
BEGIN
  temp_nbr = get_next_cl_nbr(db_path, 0);
  WHILE (temp_nbr <> 9999999) DO
  BEGIN
    cl_nbr = temp_nbr;
    SUSPEND;
    temp_nbr = get_next_cl_nbr(db_path, temp_nbr);
  END
END^


ALTER PROCEDURE CHECK_INTEGRITY
RETURNS (
    PARENT_TABLE VARCHAR(64),
    PARENT_NBR INTEGER,
    CHILD_TABLE VARCHAR(64),
    CHILD_NBR INTEGER,
    CHILD_FIELD VARCHAR(64))
AS
BEGIN
  /* Check SB_AGENCY */
  child_table = 'SB_AGENCY';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY_REPORTS */
  child_table = 'SB_AGENCY_REPORTS';

  parent_table = 'SB_AGENCY';
  child_field = 'SB_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_BANK_ACCOUNTS */
  child_table = 'SB_BANK_ACCOUNTS';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'ACH_ORIGIN_SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'LOGO_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'SIGNATURE_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_COMPANY_SVCS */
  child_table = 'SB_DELIVERY_COMPANY_SVCS';

  parent_table = 'SB_DELIVERY_COMPANY';
  child_field = 'SB_DELIVERY_COMPANY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_SERVICE_OPT */
  child_table = 'SB_DELIVERY_SERVICE_OPT';

  parent_table = 'SB_DELIVERY_SERVICE';
  child_field = 'SB_DELIVERY_SERVICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX */
  child_table = 'SB_MAIL_BOX';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'UP_LEVEL_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORT_WRITER_REPORTS';
  child_field = 'SB_COVER_LETTER_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_CONTENT */
  child_table = 'SB_MAIL_BOX_CONTENT';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_OPTION */
  child_table = 'SB_MAIL_BOX_OPTION';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MAIL_BOX_CONTENT';
  child_field = 'SB_MAIL_BOX_CONTENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MEDIA_TYPE_OPTION */
  child_table = 'SB_MEDIA_TYPE_OPTION';

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_QUEUE_PRIORITY */
  child_table = 'SB_QUEUE_PRIORITY';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_CLIENTS */
  child_table = 'SB_SEC_CLIENTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_GROUP_MEMBERS */
  child_table = 'SB_SEC_GROUP_MEMBERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_RIGHTS */
  child_table = 'SB_SEC_RIGHTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_ROW_FILTERS */
  child_table = 'SB_SEC_ROW_FILTERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES */
  child_table = 'SB_SERVICES';

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES_CALCULATIONS */
  child_table = 'SB_SERVICES_CALCULATIONS';

  parent_table = 'SB_SERVICES';
  child_field = 'SB_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TASK */
  child_table = 'SB_TASK';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TEAM_MEMBERS */
  child_table = 'SB_TEAM_MEMBERS';

  parent_table = 'SB_TEAM';
  child_field = 'SB_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER */
  child_table = 'SB_USER';

  parent_table = 'SB_ACCOUNTANT';
  child_field = 'SB_ACCOUNTANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_NOTICE */
  child_table = 'SB_USER_NOTICE';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END^


ALTER PROCEDURE CHECK_TABLE_INTEGRITY (
    CHILD_TABLE VARCHAR(64),
    PARENT_TABLE VARCHAR(64),
    CHILD_FIELD VARCHAR(64))
RETURNS (
    CHILD_NBR INTEGER,
    INVALID_PARENT_NBR INTEGER)
AS
DECLARE VARIABLE q VARCHAR(2048);
BEGIN
  q = 'SELECT t1.t1_key, t1.t2_key
       FROM (SELECT ' || child_table || '_nbr t1_key, ' || child_field || ' t2_key, MIN(effective_date) effective_date, MAX(effective_until) effective_until FROM ' || child_table || ' WHERE ' || child_field || ' IS NOT NULL GROUP BY 1, 2) t1
       LEFT JOIN
       (SELECT ' || parent_table || '_nbr t2_key, MIN(effective_date) effective_date, MAX(effective_until) effective_until FROM ' || parent_table || ' GROUP BY 1) t2
       ON t1.t2_key =  t2.t2_key
       WHERE
         t2.t2_key IS NULL OR
         (t1.effective_date < t2.effective_date OR t1.effective_until > t2.effective_until)';

  FOR EXECUTE STATEMENT q INTO :child_nbr, :invalid_parent_nbr
  DO
    SUSPEND;
END^


ALTER PROCEDURE CL_COPY (
    DB_PATH VARCHAR(255),
    CLIENT_NAME VARCHAR(255))
RETURNS (
    ERROR_TYPE INTEGER)
AS
BEGIN
  error_type = copy_cl_udf(db_path, client_name);
END^


ALTER PROCEDURE CL_NBR
RETURNS (
    NUMBER INTEGER)
AS
BEGIN
  number = NEXT VALUE FOR g_cl;
END^


ALTER PROCEDURE DEL_SB (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb WHERE sb_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_ACCOUNTANT (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_accountant WHERE sb_accountant_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_AGENCY (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_agency WHERE sb_agency_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_AGENCY_REPORTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_agency_reports WHERE sb_agency_reports_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_BANK_ACCOUNTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_bank_accounts WHERE sb_bank_accounts_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_BANKS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_banks WHERE sb_banks_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_BLOB (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_blob WHERE sb_blob_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_DELIVERY_COMPANY (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_delivery_company WHERE sb_delivery_company_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_DELIVERY_COMPANY_SVCS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_delivery_company_svcs WHERE sb_delivery_company_svcs_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_DELIVERY_METHOD (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_delivery_method WHERE sb_delivery_method_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_DELIVERY_SERVICE (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_delivery_service WHERE sb_delivery_service_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_DELIVERY_SERVICE_OPT (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_delivery_service_opt WHERE sb_delivery_service_opt_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_ENLIST_GROUPS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_enlist_groups WHERE sb_enlist_groups_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_GLOBAL_AGENCY_CONTACTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_global_agency_contacts WHERE sb_global_agency_contacts_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_HOLIDAYS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_holidays WHERE sb_holidays_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_MAIL_BOX (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_mail_box WHERE sb_mail_box_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_MAIL_BOX_CONTENT (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_mail_box_content WHERE sb_mail_box_content_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_MAIL_BOX_OPTION (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_mail_box_option WHERE sb_mail_box_option_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_MEDIA_TYPE (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_media_type WHERE sb_media_type_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_MEDIA_TYPE_OPTION (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_media_type_option WHERE sb_media_type_option_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_MULTICLIENT_REPORTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_multiclient_reports WHERE sb_multiclient_reports_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_OPTION (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_option WHERE sb_option_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_OTHER_SERVICE (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_other_service WHERE sb_other_service_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_PAPER_INFO (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_paper_info WHERE sb_paper_info_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_QUEUE_PRIORITY (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_queue_priority WHERE sb_queue_priority_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_REFERRALS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_referrals WHERE sb_referrals_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_REPORT_WRITER_REPORTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_report_writer_reports WHERE sb_report_writer_reports_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_REPORTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_reports WHERE sb_reports_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SALES_TAX_STATES (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sales_tax_states WHERE sb_sales_tax_states_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SEC_CLIENTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sec_clients WHERE sb_sec_clients_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SEC_GROUP_MEMBERS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sec_group_members WHERE sb_sec_group_members_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SEC_GROUPS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sec_groups WHERE sb_sec_groups_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SEC_RIGHTS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sec_rights WHERE sb_sec_rights_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SEC_ROW_FILTERS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sec_row_filters WHERE sb_sec_row_filters_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SEC_TEMPLATES (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_sec_templates WHERE sb_sec_templates_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SERVICES (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_services WHERE sb_services_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_SERVICES_CALCULATIONS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_services_calculations WHERE sb_services_calculations_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_STORAGE (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_storage WHERE sb_storage_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_TASK (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_task WHERE sb_task_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_TAX_PAYMENT (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_tax_payment WHERE sb_tax_payment_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_TEAM (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_team WHERE sb_team_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_TEAM_MEMBERS (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_team_members WHERE sb_team_members_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_USER (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_user WHERE sb_user_nbr = :nbr;
END^


ALTER PROCEDURE DEL_SB_USER_NOTICE (
    NBR INTEGER)
AS
BEGIN
  DELETE FROM sb_user_notice WHERE sb_user_notice_nbr = :nbr;
END^


ALTER PROCEDURE DO_AFTER_START_TRANSACTION (
    USER_ID INTEGER,
    START_TIME TIMESTAMP)
AS
DECLARE VARIABLE curr_trans_id INTEGER;
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  curr_trans_id = NEXT VALUE FOR g_ev_transaction;
  IF (start_time IS NULL) THEN
    start_time = CURRENT_TIME;

  INSERT INTO ev_transaction (nbr, commit_nbr, start_time, commit_time, user_id)
  VALUES (:curr_trans_id, - :curr_trans_id, :start_time,  '12/31/9999', :user_id);

  rdb$set_context('USER_TRANSACTION', 'TRN_ID', curr_trans_id);
  rdb$set_context('USER_TRANSACTION', 'TRN_START', start_time);
END^


ALTER PROCEDURE DO_BEFORE_COMMIT_TRANSACTION (
    COMMIT_TIME TIMESTAMP)
AS
DECLARE VARIABLE curr_trans_id INTEGER;
DECLARE VARIABLE comm_trans_id INTEGER;
BEGIN
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  curr_trans_id = CAST(rdb$get_context('USER_TRANSACTION', 'TRN_ID') AS INTEGER);
  IF (commit_time IS NULL) THEN
    commit_time = CURRENT_TIME;

  IF (EXISTS (SELECT nbr FROM ev_table_change WHERE ev_transaction_nbr = :curr_trans_id)) THEN
  BEGIN
    comm_trans_id = NEXT VALUE FOR g_ev_transaction_commit;
    UPDATE ev_transaction SET commit_nbr = :comm_trans_id, commit_time = :commit_time WHERE nbr = :curr_trans_id;
  END
  ELSE
    DELETE FROM ev_transaction WHERE nbr = :curr_trans_id;
END^


ALTER PROCEDURE EV_AUDIT_DEL (
    CUT_OFF_DATE TIMESTAMP)
AS
declare variable BLOB_NBR integer;
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);

  FOR SELECT CAST (fld.old_value AS INTEGER)
  FROM ev_transaction tr, ev_table_change tbl, ev_field_change fld, ev_field f
  WHERE tr.nbr = tbl.ev_transaction_nbr AND
        tbl.nbr = fld.ev_table_change_nbr AND
        f.nbr = fld.ev_field_nbr AND
        f.field_type = 'B' AND
        fld.old_value IS NOT NULL AND
        tr.commit_time < :cut_off_date
  INTO :blob_nbr
  DO
    DELETE FROM ev_field_change_blob WHERE nbr = :blob_nbr;

  DELETE FROM ev_field_change WHERE
    ev_table_change_nbr IN (SELECT tbl.nbr  FROM ev_transaction tr, ev_table_change tbl
    WHERE tr.nbr = tbl.ev_transaction_nbr AND
    tr.commit_time < :cut_off_date);

  DELETE FROM ev_table_change WHERE
    ev_transaction_nbr IN (SELECT nbr FROM ev_transaction WHERE commit_time < :cut_off_date);

  DELETE FROM ev_transaction WHERE commit_time < :cut_off_date;

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END^


ALTER PROCEDURE LIST_TOP_MAIL_BOXES
RETURNS (
    NBR INTEGER,
    UP_NBR INTEGER)
AS
DECLARE VARIABLE temp_nbr INTEGER;
BEGIN
  FOR SELECT sb_mail_box_nbr, up_level_mail_box_nbr FROM sb_mail_box
  WHERE effective_date <= current_date and effective_until > current_date
  INTO :nbr, :temp_nbr DO
  BEGIN
    up_nbr = NULL;
    WHILE (temp_nbr IS NOT NULL) DO
    BEGIN
      up_nbr=temp_nbr;
      SELECT up_level_mail_box_nbr FROM sb_mail_box
      WHERE sb_mail_box_nbr = :temp_nbr AND
            effective_date <= current_date and effective_until > current_date
      INTO :temp_nbr;
    END

    SUSPEND;
  END
END^


ALTER PROCEDURE LIST_TOP_MAIL_BOXES2
RETURNS (
    NBR INTEGER,
    UP_NBR INTEGER)
AS
DECLARE VARIABLE temp_nbr INTEGER;
BEGIN
  FOR SELECT sb_mail_box_nbr, up_level_mail_box_nbr FROM sb_mail_box
  WHERE effective_date <= CURRENT_DATE AND effective_until > CURRENT_DATE
  INTO :nbr, :temp_nbr DO
  BEGIN
    up_nbr = nbr;
    WHILE (temp_nbr IS NOT NULL) DO
    BEGIN
      up_nbr = temp_nbr;
      SELECT up_level_mail_box_nbr FROM sb_mail_box
      WHERE sb_mail_box_nbr =: temp_nbr AND
            effective_date <= CURRENT_DATE AND effective_until > CURRENT_DATE
      INTO :temp_nbr;
    END
    SUSPEND;
  END
END^


ALTER PROCEDURE PACK_ALL
AS
BEGIN
END^


ALTER PROCEDURE RAISE_CHILDREN_EXIST (
    PARENT_TABLE VARCHAR(64),
    PARENT_NBR INTEGER,
    CHILD_TABLE VARCHAR(64),
    PERIOD_BEGIN DATE,
    PERIOD_END DATE)
AS
DECLARE VARIABLE line_break CHAR(2);
DECLARE VARIABLE error_text VARCHAR(1024);
BEGIN
  line_break = '
';

  error_text =
    'Children records exist' || line_break ||
    'Parent Table:   ' || UPPER(parent_table) || ' (' || convertnullinteger(parent_nbr, 0) || ')' || line_break ||
    'Child  Table:   ' || UPPER(child_table)  || line_break ||
    'Period:         ' || formatdate(period_begin, 'MM/DD/YYYY') || ' - ' || formatdate(period_end, 'MM/DD/YYYY');

  EXECUTE PROCEDURE raise_error('I2', error_text);
END^


ALTER PROCEDURE RAISE_DUPLICATE_RECORD (
    TABLE_NAME VARCHAR(64),
    FIELDS VARCHAR(512),
    FIELD_VALUES VARCHAR(512),
    EFFECTIVE_DATE DATE,
    EFFECTIVE_UNTIL DATE)
AS
DECLARE VARIABLE line_break CHAR(2);
DECLARE VARIABLE error_text VARCHAR(1024);
BEGIN
  line_break = '
';

  error_text =
    'Duplicate record' || line_break ||
    'Table:   ' || UPPER(table_name) || line_break ||
    'Fields:  ' || UPPER(fields) || line_break ||
    'Values:  ' || convertnullstring(field_values, 'NULL');

  IF (effective_date IS NOT NULL) THEN 
  BEGIN
    error_text = error_text || line_break || 'Effective: ' || formatdate(effective_date, 'MM/DD/YYYY');
    IF ((effective_until IS NULL) OR (effective_until = '12/31/9999')) THEN
      error_text = error_text || ' - ...';
    ELSE
      error_text = error_text || ' - ' || formatdate((effective_until - 1), 'MM/DD/YYYY');
  END

  EXECUTE PROCEDURE raise_error('I3', error_text);
END^


ALTER PROCEDURE RAISE_ERROR (
    ERROR_CODE VARCHAR(40),
    ERROR_TEXT VARCHAR(1024))
AS
DECLARE VARIABLE msg VARCHAR(1024);
BEGIN
  IF (error_code <> '') THEN
    msg = error_code || ': ' || error_text;
  ELSE
    msg = error_text;

  EXCEPTION ex_error msg;
END^


ALTER PROCEDURE RAISE_ONE_VERSION_ONLY (
    TABLE_NAME VARCHAR(64),
    NBR INTEGER)
AS
DECLARE VARIABLE msg VARCHAR(1024);
DECLARE VARIABLE line_break CHAR(2);
BEGIN
  line_break = '
';
  msg = 'Only one version per record is allowed' || line_break ||
        'Table:  ' || UPPER(table_name) || ' (' || UPPER(table_name) || '_NBR = ' || convertnullinteger(nbr, 0) || ')';

  EXECUTE PROCEDURE raise_error('S4', msg);
END^


ALTER PROCEDURE RAISE_PARENT_NOT_EXIST (
    CHILD_TABLE VARCHAR(64),
    CHILD_NBR INTEGER,
    CHILD_FIELD VARCHAR(64),
    CHILD_FIELD_NBR INTEGER,
    PARENT_TABLE VARCHAR(64),
    AS_OF_DATE DATE)
AS
DECLARE VARIABLE line_break CHAR(2);
DECLARE VARIABLE error_text VARCHAR(1024);
BEGIN
  line_break = '
';

  error_text =
    'Parent record does not exist' || line_break ||
    'Parent Table:   ' || UPPER(parent_table) || ' (' || convertnullinteger(child_field_nbr, 0) || ')' || line_break ||
    'Child  Table:   ' || UPPER(child_table)  || ' (' || convertnullinteger(child_nbr, 0) || ')   ' || UPPER(child_field) || '(' || convertnullinteger(child_field_nbr, 0) || ')';

  IF (as_of_date IS NOT NULL) THEN
    error_text = error_text || line_break || 'As of Date:     ' || formatdate(as_of_date, 'MM/DD/YYYY');

  EXECUTE PROCEDURE raise_error('I1', error_text);
END^


ALTER PROCEDURE RAISE_TABLE_ERROR (
    TABLE_NAME VARCHAR(64),
    REC_VERSION INTEGER,
    FIELD_NAME VARCHAR(64),
    ERROR_CODE VARCHAR(40),
    ERROR_TEXT VARCHAR(1024))
AS
DECLARE VARIABLE msg VARCHAR(1024);
DECLARE VARIABLE line_break CHAR(2);
BEGIN
  line_break = '
';
  msg = error_text || line_break ||
        'Table:  ' || UPPER(table_name);

  IF (rec_version <> 0) THEN
    msg = msg || ' (REC_VERSION = ' || convertnullinteger(rec_version, 0) || ')';

  IF (field_name <> '') THEN
    msg = msg || line_break ||
        'Field:  ' || UPPER(field_name);

  EXECUTE PROCEDURE raise_error(error_code, msg);
END^


ALTER PROCEDURE UPDATE_INDEX_STATISTICS
AS
DECLARE VARIABLE index_name VARCHAR(31);
BEGIN
  FOR SELECT rdb$index_name FROM rdb$indices INTO :index_name DO
    EXECUTE STATEMENT 'SET statistics INDEX ' || :index_name || ';';
END^



SET TERM ; ^


/******************************************************************************/
/****                             Descriptions                             ****/
/******************************************************************************/



/******************************************************************************/
/****                              Privileges                              ****/
/******************************************************************************/


/* Privileges of users */
GRANT SELECT, REFERENCES ON EV_DATABASE TO EUSER;
GRANT SELECT, REFERENCES ON EV_FIELD TO EUSER;
GRANT ALL ON EV_FIELD_CHANGE TO EUSER;
GRANT ALL ON EV_FIELD_CHANGE_BLOB TO EUSER;
GRANT SELECT, REFERENCES ON EV_TABLE TO EUSER;
GRANT ALL ON EV_TABLE_CHANGE TO EUSER;
GRANT ALL ON EV_TRANSACTION TO EUSER;
GRANT SELECT ON RDB$ROLES TO EUSER;
GRANT ALL ON SB TO EUSER;
GRANT ALL ON SB_ACCOUNTANT TO EUSER;
GRANT ALL ON SB_AGENCY TO EUSER;
GRANT ALL ON SB_AGENCY_REPORTS TO EUSER;
GRANT ALL ON SB_BANKS TO EUSER;
GRANT ALL ON SB_BANK_ACCOUNTS TO EUSER;
GRANT ALL ON SB_BLOB TO EUSER;
GRANT ALL ON SB_DELIVERY_COMPANY TO EUSER;
GRANT ALL ON SB_DELIVERY_COMPANY_SVCS TO EUSER;
GRANT ALL ON SB_DELIVERY_METHOD TO EUSER;
GRANT ALL ON SB_DELIVERY_SERVICE TO EUSER;
GRANT ALL ON SB_DELIVERY_SERVICE_OPT TO EUSER;
GRANT ALL ON SB_ENLIST_GROUPS TO EUSER;
GRANT ALL ON SB_GLOBAL_AGENCY_CONTACTS TO EUSER;
GRANT ALL ON SB_HOLIDAYS TO EUSER;
GRANT ALL ON SB_MAIL_BOX TO EUSER;
GRANT ALL ON SB_MAIL_BOX_CONTENT TO EUSER;
GRANT ALL ON SB_MAIL_BOX_OPTION TO EUSER;
GRANT ALL ON SB_MEDIA_TYPE TO EUSER;
GRANT ALL ON SB_MEDIA_TYPE_OPTION TO EUSER;
GRANT ALL ON SB_MULTICLIENT_REPORTS TO EUSER;
GRANT ALL ON SB_OPTION TO EUSER;
GRANT ALL ON SB_OTHER_SERVICE TO EUSER;
GRANT ALL ON SB_PAPER_INFO TO EUSER;
GRANT ALL ON SB_QUEUE_PRIORITY TO EUSER;
GRANT ALL ON SB_REFERRALS TO EUSER;
GRANT ALL ON SB_REPORTS TO EUSER;
GRANT ALL ON SB_REPORT_WRITER_REPORTS TO EUSER;
GRANT ALL ON SB_SALES_TAX_STATES TO EUSER;
GRANT ALL ON SB_SEC_CLIENTS TO EUSER;
GRANT ALL ON SB_SEC_GROUPS TO EUSER;
GRANT ALL ON SB_SEC_GROUP_MEMBERS TO EUSER;
GRANT ALL ON SB_SEC_RIGHTS TO EUSER;
GRANT ALL ON SB_SEC_ROW_FILTERS TO EUSER;
GRANT ALL ON SB_SEC_TEMPLATES TO EUSER;
GRANT ALL ON SB_SERVICES TO EUSER;
GRANT ALL ON SB_SERVICES_CALCULATIONS TO EUSER;
GRANT ALL ON SB_STORAGE TO EUSER;
GRANT ALL ON SB_TASK TO EUSER;
GRANT ALL ON SB_TAX_PAYMENT TO EUSER;
GRANT ALL ON SB_TEAM TO EUSER;
GRANT ALL ON SB_TEAM_MEMBERS TO EUSER;
GRANT ALL ON SB_USER TO EUSER;
GRANT ALL ON SB_USER_NOTICE TO EUSER;
GRANT EXECUTE ON PROCEDURE ALL_CL_NUMBERS TO EUSER;
GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY TO EUSER;
GRANT EXECUTE ON PROCEDURE CHECK_TABLE_INTEGRITY TO EUSER;
GRANT EXECUTE ON PROCEDURE CL_COPY TO EUSER;
GRANT EXECUTE ON PROCEDURE CL_NBR TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_ACCOUNTANT TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_AGENCY TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_AGENCY_REPORTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_BANKS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_BANK_ACCOUNTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_BLOB TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_DELIVERY_COMPANY TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_DELIVERY_COMPANY_SVCS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_DELIVERY_METHOD TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_DELIVERY_SERVICE TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_DELIVERY_SERVICE_OPT TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_ENLIST_GROUPS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_GLOBAL_AGENCY_CONTACTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_HOLIDAYS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_MAIL_BOX TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_MAIL_BOX_CONTENT TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_MAIL_BOX_OPTION TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_MEDIA_TYPE TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_MEDIA_TYPE_OPTION TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_MULTICLIENT_REPORTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_OPTION TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_OTHER_SERVICE TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_PAPER_INFO TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_QUEUE_PRIORITY TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_REFERRALS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_REPORTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_REPORT_WRITER_REPORTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SALES_TAX_STATES TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SEC_CLIENTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SEC_GROUPS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SEC_GROUP_MEMBERS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SEC_RIGHTS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SEC_ROW_FILTERS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SEC_TEMPLATES TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SERVICES TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_SERVICES_CALCULATIONS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_STORAGE TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_TASK TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_TAX_PAYMENT TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_TEAM TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_TEAM_MEMBERS TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_USER TO EUSER;
GRANT EXECUTE ON PROCEDURE DEL_SB_USER_NOTICE TO EUSER;
GRANT EXECUTE ON PROCEDURE DO_AFTER_START_TRANSACTION TO EUSER;
GRANT EXECUTE ON PROCEDURE DO_BEFORE_COMMIT_TRANSACTION TO EUSER;
GRANT EXECUTE ON PROCEDURE LIST_TOP_MAIL_BOXES TO EUSER;
GRANT EXECUTE ON PROCEDURE LIST_TOP_MAIL_BOXES2 TO EUSER;
GRANT EXECUTE ON PROCEDURE PACK_ALL TO EUSER;
GRANT EXECUTE ON PROCEDURE RAISE_CHILDREN_EXIST TO EUSER;
GRANT EXECUTE ON PROCEDURE RAISE_DUPLICATE_RECORD TO EUSER;
GRANT EXECUTE ON PROCEDURE RAISE_ERROR TO EUSER;
GRANT EXECUTE ON PROCEDURE RAISE_ONE_VERSION_ONLY TO EUSER;
GRANT EXECUTE ON PROCEDURE RAISE_PARENT_NOT_EXIST TO EUSER;
GRANT EXECUTE ON PROCEDURE RAISE_TABLE_ERROR TO EUSER;
COMMIT;


SET TERM ^ ;
/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.4', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT
^

EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_field;
  DELETE FROM ev_table;
  INSERT INTO ev_table (nbr, name, versioned) VALUES (1, 'SB', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (2, 'SB_ACCOUNTANT', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (3, 'SB_AGENCY', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (4, 'SB_AGENCY_REPORTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (5, 'SB_BANKS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (6, 'SB_BANK_ACCOUNTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (7, 'SB_BLOB', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (8, 'SB_DELIVERY_COMPANY', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (9, 'SB_DELIVERY_COMPANY_SVCS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (10, 'SB_DELIVERY_METHOD', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (11, 'SB_DELIVERY_SERVICE', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (12, 'SB_DELIVERY_SERVICE_OPT', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (13, 'SB_ENLIST_GROUPS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (14, 'SB_GLOBAL_AGENCY_CONTACTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (15, 'SB_HOLIDAYS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (16, 'SB_MAIL_BOX', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (17, 'SB_MAIL_BOX_CONTENT', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (18, 'SB_MAIL_BOX_OPTION', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (19, 'SB_MEDIA_TYPE', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (20, 'SB_MEDIA_TYPE_OPTION', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (21, 'SB_MULTICLIENT_REPORTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (22, 'SB_OPTION', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (23, 'SB_OTHER_SERVICE', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (24, 'SB_PAPER_INFO', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (25, 'SB_QUEUE_PRIORITY', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (26, 'SB_REFERRALS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (27, 'SB_REPORTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (30, 'SB_REPORT_WRITER_REPORTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (31, 'SB_SALES_TAX_STATES', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (32, 'SB_SEC_CLIENTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (33, 'SB_SEC_GROUPS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (34, 'SB_SEC_GROUP_MEMBERS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (35, 'SB_SEC_RIGHTS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (36, 'SB_SEC_ROW_FILTERS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (37, 'SB_SEC_TEMPLATES', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (38, 'SB_SERVICES', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (39, 'SB_SERVICES_CALCULATIONS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (40, 'SB_TASK', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (41, 'SB_TAX_PAYMENT', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (42, 'SB_TEAM', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (43, 'SB_TEAM_MEMBERS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (44, 'SB_USER', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (45, 'SB_USER_NOTICE', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (46, 'SB_STORAGE', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 451, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 19, 'SB_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 8, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 452, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 28, 'SB_NAME', 'V', 60, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 5, 'ADDRESS1', 'V', 30, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 6, 'ADDRESS2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 29, 'CITY', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 30, 'STATE', 'C', 2, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 20, 'ZIP_CODE', 'V', 10, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 31, 'E_MAIL_ADDRESS', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 32, 'PARENT_SB_MODEM_NUMBER', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 33, 'DEVELOPMENT_MODEM_NUMBER', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 35, 'DEVELOPMENT_FTP_PASSWORD', 'V', 128, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 49, 'EIN_NUMBER', 'V', 9, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 50, 'EFTPS_TIN_NUMBER', 'V', 9, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 51, 'EFTPS_BANK_FORMAT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 53, 'USE_PRENOTE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 54, 'IMPOUND_TRUST_MONIES_AS_RECEIV', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 55, 'PAY_TAX_FROM_PAYABLES', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 57, 'AR_EXPORT_FORMAT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 58, 'DEFAULT_CHECK_FORMAT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 59, 'MICR_FONT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 22, 'MICR_HORIZONTAL_ADJUSTMENT', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 23, 'AUTO_SAVE_MINUTES', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 36, 'PHONE', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 37, 'FAX', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 7, 'COVER_LETTER_NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 4, 'INVOICE_NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 3, 'TAX_COVER_LETTER_NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 27, 'AR_IMPORT_DIRECTORY', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 26, 'ACH_DIRECTORY', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 25, 'SB_URL', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 18, 'DAYS_IN_PRENOTE', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 9, 'SB_LOGO', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 13, 'USER_PASSWORD_DURATION_IN_DAYS', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 17, 'DUMMY_TAX_CL_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 48, 'ERROR_SCREEN', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 15, 'PSWD_MIN_LENGTH', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 47, 'PSWD_FORCE_MIXED', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 46, 'MISC_CHECK_FORM', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 2, 'VMR_CONFIDENCIAL_NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 45, 'MARK_LIABS_PAID_DEFAULT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 44, 'TRUST_IMPOUND', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 43, 'TAX_IMPOUND', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 42, 'DD_IMPOUND', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 40, 'BILLING_IMPOUND', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 39, 'WC_IMPOUND', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 14, 'DAYS_PRIOR_TO_CHECK_DATE', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 38, 'SB_EXCEPTION_PAYMENT_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 1, 'SB_MAX_ACH_FILE_TOTAL', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 41, 'SB_ACH_FILE_LIMITATIONS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 24, 'SB_CL_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 543, 'DASHBOARD_MSG', 'V', 200, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 544, 'EE_LOGIN_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 550, 'ESS_TERMS_OF_USE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 551, 'WC_TERMS_OF_USE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 453, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 67, 'SB_ACCOUNTANT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 66, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 454, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 72, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 62, 'ADDRESS1', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 63, 'ADDRESS2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 73, 'CITY', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 79, 'STATE', 'V', 3, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 68, 'ZIP_CODE', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 64, 'CONTACT1', 'V', 30, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 74, 'PHONE1', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 69, 'DESCRIPTION1', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 65, 'CONTACT2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 75, 'PHONE2', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 70, 'DESCRIPTION2', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 76, 'FAX', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 71, 'FAX_DESCRIPTION', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 77, 'E_MAIL_ADDRESS', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 78, 'PRINT_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 61, 'TITLE', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (2, 60, 'SIGNATURE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 455, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 86, 'SB_AGENCY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 85, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 456, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 93, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 80, 'ADDRESS1', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 81, 'ADDRESS2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 94, 'CITY', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 95, 'STATE', 'C', 2, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 87, 'ZIP_CODE', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 82, 'CONTACT1', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 96, 'PHONE1', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 88, 'DESCRIPTION1', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 83, 'CONTACT2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 97, 'PHONE2', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 89, 'DESCRIPTION2', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 98, 'FAX', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 90, 'FAX_DESCRIPTION', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 99, 'E_MAIL_ADDRESS', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 105, 'AGENCY_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 91, 'SB_BANKS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 100, 'ACCOUNT_NUMBER', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 106, 'ACCOUNT_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 107, 'NEGATIVE_DIRECT_DEP_ALLOWED', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 101, 'MODEM_NUMBER', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 84, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 102, 'FILLER', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 103, 'PRINT_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (3, 92, 'COUNTY', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (4, 457, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (4, 111, 'SB_AGENCY_REPORTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (4, 110, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (4, 458, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (4, 112, 'SB_AGENCY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (4, 113, 'SB_REPORTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 459, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 120, 'SB_BANKS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 119, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 460, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 129, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 114, 'ADDRESS1', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 115, 'ADDRESS2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 130, 'CITY', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 131, 'STATE', 'C', 2, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 121, 'ZIP_CODE', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 116, 'CONTACT1', 'V', 30, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 132, 'PHONE1', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 122, 'DESCRIPTION1', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 117, 'CONTACT2', 'V', 30, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 133, 'PHONE2', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 123, 'DESCRIPTION2', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 134, 'FAX', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 124, 'FAX_DESCRIPTION', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 135, 'E_MAIL_ADDRESS', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 140, 'ABA_NUMBER', 'C', 9, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 125, 'TOP_ABA_NUMBER', 'V', 10, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 126, 'BOTTOM_ABA_NUMBER', 'V', 10, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 136, 'ADDENDA', 'V', 12, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 118, 'CHECK_TEMPLATE', 'B', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 141, 'USE_CHECK_TEMPLATE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 127, 'MICR_ACCOUNT_START_POSITION', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 128, 'MICR_CHECK_NUMBER_START_POSITN', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 137, 'FILLER', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 138, 'PRINT_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 139, 'BRANCH_IDENTIFIER', 'V', 9, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 142, 'ALLOW_HYPHENS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 461, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 146, 'SB_BANK_ACCOUNTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 143, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 462, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 147, 'SB_BANKS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 156, 'CUSTOM_BANK_ACCOUNT_NUMBER', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 172, 'BANK_ACCOUNT_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 157, 'NAME_DESCRIPTION', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 148, 'ACH_ORIGIN_SB_BANKS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 174, 'BANK_RETURNS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 158, 'CUSTOM_HEADER_RECORD', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 149, 'NEXT_AVAILABLE_CHECK_NUMBER', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 150, 'END_CHECK_NUMBER', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 151, 'NEXT_BEGIN_CHECK_NUMBER', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 152, 'NEXT_END_CHECK_NUMBER', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 159, 'FILLER', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 171, 'SUPPRESS_OFFSET_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 170, 'BLOCK_NEGATIVE_CHECKS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 175, 'BATCH_FILER_ID', 'V', 9, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 153, 'MASTER_INQUIRY_PIN', 'V', 10, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 154, 'LOGO_SB_BLOB_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 155, 'SIGNATURE_SB_BLOB_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 144, 'BEGINNING_BALANCE', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 166, 'OPERATING_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 165, 'BILLING_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 164, 'ACH_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 163, 'TRUST_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 162, 'TAX_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 161, 'OBC_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 160, 'WORKERS_COMP_ACCOUNT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 145, 'RECCURING_WIRE_NUMBER', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (6, 553, 'BANK_CHECK', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (7, 540, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (7, 541, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (7, 178, 'SB_BLOB_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (7, 542, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (7, 177, 'DATA', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 463, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 181, 'SB_DELIVERY_COMPANY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 180, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 464, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 182, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 183, 'DELIVERY_CONTACT', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 184, 'DELIVERY_CONTACT_PHONE', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 188, 'DELIVERY_CONTACT_PHONE_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 185, 'SUPPLIES_CONTACT', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 186, 'SUPPLIES_CONTACT_PHONE', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 189, 'SUPPLIES_CONTACT_PHONE_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 187, 'WEB_SITE', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (8, 179, 'DELIVERY_COMPANY_NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 465, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 192, 'SB_DELIVERY_COMPANY_SVCS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 190, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 466, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 193, 'SB_DELIVERY_COMPANY_NBR', 'I', NULL, NULL, 'Y', 'N');
END
^

EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 194, 'DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (9, 191, 'REFERENCE_FEE', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (10, 467, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (10, 197, 'SB_DELIVERY_METHOD_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (10, 195, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (10, 468, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (10, 196, 'SY_DELIVERY_METHOD_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 469, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 200, 'SB_DELIVERY_SERVICE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 198, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 470, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (11, 199, 'SY_DELIVERY_SERVICE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 471, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 205, 'SB_DELIVERY_SERVICE_OPT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 201, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 472, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 204, 'SB_DELIVERY_SERVICE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 203, 'SB_DELIVERY_METHOD_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 206, 'OPTION_TAG', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 202, 'OPTION_INTEGER_VALUE', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (12, 207, 'OPTION_STRING_VALUE', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 473, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 210, 'SB_ENLIST_GROUPS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 208, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 474, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 209, 'SY_REPORT_GROUPS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 212, 'MEDIA_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (13, 211, 'PROCESS_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 475, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 216, 'SB_GLOBAL_AGENCY_CONTACTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 215, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 476, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 217, 'SY_GLOBAL_AGENCY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 213, 'CONTACT_NAME', 'V', 30, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 219, 'PHONE', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 220, 'FAX', 'V', 20, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 221, 'E_MAIL_ADDRESS', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (14, 214, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 477, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 224, 'SB_HOLIDAYS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 222, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 478, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 223, 'HOLIDAY_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 225, 'HOLIDAY_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (15, 226, 'USED_BY', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 479, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 242, 'SB_MAIL_BOX_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 233, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 480, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 241, 'CL_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 240, 'CL_MAIL_BOX_GROUP_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 232, 'COST', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 251, 'REQUIRED', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 248, 'NOTIFICATION_EMAIL', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 239, 'PR_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 231, 'RELEASED_TIME', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 230, 'PRINTED_TIME', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 238, 'SB_COVER_LETTER_REPORT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 229, 'SCANNED_TIME', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 228, 'SHIPPED_TIME', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 237, 'SY_COVER_LETTER_REPORT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 247, 'NOTE', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 236, 'UP_LEVEL_MAIL_BOX_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 246, 'TRACKING_INFO', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 250, 'AUTO_RELEASE_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 245, 'BARCODE', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 227, 'CREATED_TIME', 'T', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 249, 'DISPOSE_CONTENT_AFTER_SHIPPING', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 244, 'ADDRESSEE', 'V', 512, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 243, 'DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 235, 'SB_DELIVERY_METHOD_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (16, 234, 'SB_MEDIA_TYPE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 481, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 255, 'SB_MAIL_BOX_CONTENT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 252, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 482, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 254, 'SB_MAIL_BOX_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 257, 'DESCRIPTION', 'V', 512, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 256, 'FILE_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 258, 'MEDIA_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 253, 'PAGE_COUNT', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 483, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 263, 'SB_MAIL_BOX_OPTION_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 259, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 484, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 262, 'SB_MAIL_BOX_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 261, 'SB_MAIL_BOX_CONTENT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 265, 'OPTION_TAG', 'V', 80, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 260, 'OPTION_INTEGER_VALUE', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (18, 264, 'OPTION_STRING_VALUE', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (19, 485, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (19, 268, 'SB_MEDIA_TYPE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (19, 266, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (19, 486, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (19, 267, 'SY_MEDIA_TYPE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 487, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 272, 'SB_MEDIA_TYPE_OPTION_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 269, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 488, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 271, 'SB_MEDIA_TYPE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 274, 'OPTION_TAG', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 270, 'OPTION_INTEGER_VALUE', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (20, 273, 'OPTION_STRING_VALUE', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 489, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 278, 'SB_MULTICLIENT_REPORTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 275, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 490, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 279, 'DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 280, 'REPORT_LEVEL', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 277, 'REPORT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (21, 276, 'INPUT_PARAMS', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 491, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 283, 'SB_OPTION_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 281, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 492, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 285, 'OPTION_TAG', 'V', 80, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 282, 'OPTION_INTEGER_VALUE', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (22, 284, 'OPTION_STRING_VALUE', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (23, 493, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (23, 287, 'SB_OTHER_SERVICE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (23, 286, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (23, 494, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (23, 288, 'NAME', 'V', 60, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 495, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 293, 'SB_PAPER_INFO_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 289, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 496, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 294, 'DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 292, 'HEIGHT', 'N', 18, 6, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 291, 'WIDTH', 'N', 18, 6, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 290, 'WEIGHT', 'N', 18, 6, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (24, 295, 'MEDIA_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 497, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 297, 'SB_QUEUE_PRIORITY_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 296, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 498, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 298, 'THREADS', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 302, 'METHOD_NAME', 'V', 255, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 299, 'PRIORITY', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 300, 'SB_USER_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (25, 301, 'SB_SEC_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (26, 499, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (26, 304, 'SB_REFERRALS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (26, 303, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (26, 500, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (26, 305, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 501, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 310, 'SB_REPORTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 308, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 502, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 311, 'DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 307, 'COMMENTS', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 309, 'REPORT_WRITER_REPORTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 312, 'REPORT_LEVEL', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (27, 306, 'INPUT_PARAMS', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 507, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 323, 'SB_REPORT_WRITER_REPORTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 322, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 508, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 326, 'REPORT_DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 328, 'REPORT_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 321, 'REPORT_FILE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 320, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 327, 'MEDIA_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 325, 'CLASS_NAME', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (30, 324, 'ANCESTOR_CLASS_NAME', 'V', 40, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 509, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 331, 'SB_SALES_TAX_STATES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 329, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 510, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 332, 'STATE', 'C', 2, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 333, 'STATE_TAX_ID', 'V', 19, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (31, 330, 'SALES_TAX_PERCENTAGE', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 511, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 338, 'SB_SEC_CLIENTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 334, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 512, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 337, 'CL_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 336, 'SB_SEC_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (32, 335, 'SB_USER_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (33, 513, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (33, 340, 'SB_SEC_GROUPS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (33, 339, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (33, 514, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (33, 341, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (34, 515, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (34, 345, 'SB_SEC_GROUP_MEMBERS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (34, 342, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (34, 516, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (34, 344, 'SB_SEC_GROUPS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (34, 343, 'SB_USER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 517, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 349, 'SB_SEC_RIGHTS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 346, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 518, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 348, 'SB_SEC_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 347, 'SB_USER_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 351, 'TAG', 'V', 128, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (35, 350, 'CONTEXT', 'V', 128, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 519, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
END
^

EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 354, 'SB_SEC_ROW_FILTERS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 352, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 520, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 355, 'SB_SEC_GROUPS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 356, 'SB_USER_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 358, 'DATABASE_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 357, 'TABLE_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 359, 'FILTER_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 360, 'CUSTOM_EXPR', 'V', 255, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (36, 353, 'CL_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 521, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 363, 'SB_SEC_TEMPLATES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 362, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 522, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 364, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (37, 361, 'TEMPLATE', 'B', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 523, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 371, 'SB_SERVICES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 367, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 524, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 374, 'SERVICE_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 377, 'FREQUENCY', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 372, 'MONTH_NUMBER', 'V', 2, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 378, 'BASED_ON_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 373, 'SB_REPORTS_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 380, 'COMMISSION', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 381, 'SALES_TAXABLE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 375, 'FILLER', 'V', 512, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 366, 'PRODUCT_CODE', 'V', 6, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 382, 'WEEK_NUMBER', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 383, 'SERVICE_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 368, 'MINIMUM_AMOUNT', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 369, 'MAXIMUM_AMOUNT', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 370, 'SB_DELIVERY_METHOD_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (38, 376, 'TAX_TYPE', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 525, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 397, 'SB_SERVICES_CALCULATIONS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 388, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 526, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 398, 'SB_SERVICES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 384, 'NEXT_MIN_QUANTITY_BEGIN_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 385, 'NEXT_MAX_QUANTITY_BEGIN_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 386, 'NEXT_PER_ITEM_BEGIN_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 387, 'NEXT_FLAT_AMOUNT_BEGIN_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 389, 'MINIMUM_QUANTITY', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 390, 'NEXT_MINIMUM_QUANTITY', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 391, 'MAXIMUM_QUANTITY', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 392, 'NEXT_MAXIMUM_QUANTITY', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 393, 'PER_ITEM_RATE', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 394, 'NEXT_PER_ITEM_RATE', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 395, 'FLAT_AMOUNT', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (39, 396, 'NEXT_FLAT_AMOUNT', 'N', 18, 6, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 527, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 401, 'SB_TASK_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 400, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 528, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 402, 'SB_USER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 403, 'SCHEDULE', 'V', 255, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 404, 'DESCRIPTION', 'V', 255, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 399, 'TASK', 'B', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (40, 552, 'LAST_RUN', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 529, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 407, 'SB_TAX_PAYMENT_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 405, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 530, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 408, 'DESCRIPTION', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 409, 'STATUS', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (41, 406, 'STATUS_DATE', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (42, 531, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (42, 411, 'SB_TEAM_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (42, 410, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (42, 532, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (42, 412, 'TEAM_DESCRIPTION', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (42, 413, 'CR_CATEGORY', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (43, 533, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (43, 415, 'SB_TEAM_MEMBERS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (43, 414, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (43, 534, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (43, 416, 'SB_TEAM_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (43, 417, 'SB_USER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 535, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 428, 'SB_USER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 422, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 536, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 435, 'USER_ID', 'V', 128, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 419, 'USER_SIGNATURE', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 420, 'LAST_NAME', 'V', 30, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 436, 'FIRST_NAME', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 440, 'MIDDLE_INITIAL', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 441, 'ACTIVE_USER', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 442, 'DEPARTMENT', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 421, 'PASSWORD_CHANGE_DATE', 'T', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 443, 'SECURITY_LEVEL', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 437, 'USER_UPDATE_OPTIONS', 'V', 512, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 438, 'USER_FUNCTIONS', 'V', 512, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 439, 'USER_PASSWORD', 'V', 32, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 434, 'EMAIL_ADDRESS', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 429, 'CL_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 427, 'SB_ACCOUNTANT_NBR', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 426, 'WRONG_PSWD_ATTEMPTS', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 433, 'LINKS_DATA', 'V', 128, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 425, 'LOGIN_QUESTION1', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 432, 'LOGIN_ANSWER1', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 424, 'LOGIN_QUESTION2', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 431, 'LOGIN_ANSWER2', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 423, 'LOGIN_QUESTION3', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 430, 'LOGIN_ANSWER3', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 545, 'HR_PERSONNEL', 'C', 1, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 546, 'SEC_QUESTION1', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 547, 'SEC_ANSWER1', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 548, 'SEC_QUESTION2', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (44, 549, 'SEC_ANSWER2', 'V', 80, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 537, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 449, 'SB_USER_NOTICE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 539, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 538, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 448, 'SB_USER_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 450, 'NAME', 'V', 128, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 447, 'NOTES', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 446, 'TASK', 'B', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 445, 'LAST_DISMISS', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (45, 444, 'NEXT_REMINDER', 'T', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 554, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 555, 'SB_STORAGE_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 556, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 557, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 558, 'TAG', 'V', 20, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (46, 559, 'STORAGE_DATA', 'B', NULL, NULL, 'Y', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT
^

COMMIT ^


/* ================== Move data to EV_TRANSACTION table ================== */
/*<progress>Converting EV_TRANSACTION</progress>*/
ALTER INDEX I_EV_TRANSACTION_1 INACTIVE^
ALTER INDEX I_EV_TRANSACTION_2 INACTIVE^

EXECUTE BLOCK
AS
DECLARE VARIABLE start_time TIMESTAMP;
DECLARE VARIABLE user_id INTEGER;
BEGIN
  FOR SELECT DISTINCT start_time, user_id FROM x_transaction
      ORDER BY start_time
      INTO :start_time, :user_id
  DO
  BEGIN
    INSERT INTO ev_transaction (nbr, commit_nbr, start_time, commit_time, user_id)
    VALUES (NEXT VALUE FOR g_ev_transaction, NEXT VALUE FOR g_ev_transaction_commit, :start_time, :start_time, :user_id);
  END
END^

COMMIT^

DROP TABLE x_transaction ^
COMMIT^

ALTER INDEX I_EV_TRANSACTION_1 ACTIVE^
ALTER INDEX I_EV_TRANSACTION_2 ACTIVE^
COMMIT^


/* Convert existing data */


EXECUTE BLOCK
AS
DECLARE VARIABLE stmt VARCHAR(1000);
BEGIN
  /* Deactivate Indexes */
  FOR SELECT 'ALTER INDEX '||rdb$index_name ||' INACTIVE;'
      FROM rdb$indices
      WHERE (rdb$system_flag IS NULL OR rdb$system_flag = 0) AND
            (rdb$unique_flag IS NULL OR rdb$unique_flag = 0) AND
            rdb$relation_name NOT STARTS 'EV_'
      ORDER BY rdb$foreign_key
  INTO :stmt
  DO
    EXECUTE STATEMENT :stmt;
END^
COMMIT^

/* ================== Convert data of SB ================== */
/*<progress>Converting SB</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb ON xb (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_nbr INTEGER;
DECLARE VARIABLE sb_name VARCHAR(60);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE e_mail_address VARCHAR(80);
DECLARE VARIABLE parent_sb_modem_number VARCHAR(20);
DECLARE VARIABLE development_modem_number VARCHAR(20);
DECLARE VARIABLE development_ftp_password VARCHAR(128);
DECLARE VARIABLE ein_number VARCHAR(9);
DECLARE VARIABLE eftps_tin_number VARCHAR(9);
DECLARE VARIABLE eftps_bank_format CHAR(1);
DECLARE VARIABLE use_prenote CHAR(1);
DECLARE VARIABLE impound_trust_monies_as_receiv CHAR(1);
DECLARE VARIABLE pay_tax_from_payables CHAR(1);
DECLARE VARIABLE ar_export_format CHAR(1);
DECLARE VARIABLE default_check_format CHAR(1);
DECLARE VARIABLE micr_font CHAR(1);
DECLARE VARIABLE micr_horizontal_adjustment INTEGER;
DECLARE VARIABLE auto_save_minutes INTEGER;
DECLARE VARIABLE phone VARCHAR(20);
DECLARE VARIABLE fax VARCHAR(20);
DECLARE VARIABLE cover_letter_notes BLOB;
DECLARE VARIABLE invoice_notes BLOB;
DECLARE VARIABLE tax_cover_letter_notes BLOB;
DECLARE VARIABLE ar_import_directory VARCHAR(80);
DECLARE VARIABLE ach_directory VARCHAR(80);
DECLARE VARIABLE sb_url VARCHAR(80);
DECLARE VARIABLE days_in_prenote INTEGER;
DECLARE VARIABLE sb_logo BLOB;
DECLARE VARIABLE user_password_duration_in_days NUMERIC(18,6);
DECLARE VARIABLE dummy_tax_cl_nbr INTEGER;
DECLARE VARIABLE error_screen CHAR(1);
DECLARE VARIABLE pswd_min_length INTEGER;
DECLARE VARIABLE pswd_force_mixed CHAR(1);
DECLARE VARIABLE misc_check_form CHAR(1);
DECLARE VARIABLE vmr_confidencial_notes BLOB;
DECLARE VARIABLE mark_liabs_paid_default CHAR(1);
DECLARE VARIABLE trust_impound CHAR(1);
DECLARE VARIABLE tax_impound CHAR(1);
DECLARE VARIABLE dd_impound CHAR(1);
DECLARE VARIABLE billing_impound CHAR(1);
DECLARE VARIABLE wc_impound CHAR(1);
DECLARE VARIABLE days_prior_to_check_date INTEGER;
DECLARE VARIABLE sb_exception_payment_type CHAR(1);
DECLARE VARIABLE sb_max_ach_file_total NUMERIC(18,6);
DECLARE VARIABLE sb_ach_file_limitations CHAR(1);
DECLARE VARIABLE sb_cl_nbr INTEGER;
DECLARE VARIABLE dashboard_msg VARCHAR(200);
DECLARE VARIABLE ee_login_type CHAR(1);
DECLARE VARIABLE ess_terms_of_use BLOB;
DECLARE VARIABLE wc_terms_of_use BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_nbr, sb_name, address1, address2, city, state, zip_code, e_mail_address, parent_sb_modem_number, development_modem_number, development_ftp_password, ein_number, eftps_tin_number, eftps_bank_format, use_prenote, impound_trust_monies_as_receiv, pay_tax_from_payables, ar_export_format, default_check_format, micr_font, micr_horizontal_adjustment, auto_save_minutes, phone, fax, cover_letter_notes, invoice_notes, tax_cover_letter_notes, ar_import_directory, ach_directory, sb_url, days_in_prenote, sb_logo, user_password_duration_in_days, dummy_tax_cl_nbr, error_screen, pswd_min_length, pswd_force_mixed, misc_check_form, vmr_confidencial_notes, mark_liabs_paid_default, trust_impound, tax_impound, dd_impound, billing_impound, wc_impound, days_prior_to_check_date, sb_exception_payment_type, sb_max_ach_file_total, sb_ach_file_limitations, sb_cl_nbr, dashboard_msg, ee_login_type, ess_terms_of_use, wc_terms_of_use, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_nbr, :sb_name, :address1, :address2, :city, :state, :zip_code, :e_mail_address, :parent_sb_modem_number, :development_modem_number, :development_ftp_password, :ein_number, :eftps_tin_number, :eftps_bank_format, :use_prenote, :impound_trust_monies_as_receiv, :pay_tax_from_payables, :ar_export_format, :default_check_format, :micr_font, :micr_horizontal_adjustment, :auto_save_minutes, :phone, :fax, :cover_letter_notes, :invoice_notes, :tax_cover_letter_notes, :ar_import_directory, :ach_directory, :sb_url, :days_in_prenote, :sb_logo, :user_password_duration_in_days, :dummy_tax_cl_nbr, :error_screen, :pswd_min_length, :pswd_force_mixed, :misc_check_form, :vmr_confidencial_notes, :mark_liabs_paid_default, :trust_impound, :tax_impound, :dd_impound, :billing_impound, :wc_impound, :days_prior_to_check_date, :sb_exception_payment_type, :sb_max_ach_file_total, :sb_ach_file_limitations, :sb_cl_nbr, :dashboard_msg, :ee_login_type, :ess_terms_of_use, :wc_terms_of_use, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_nbr) THEN
      max_tbl_nbr = sb_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb WHERE sb_nbr = :sb_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb (sb_nbr, sb_name, address1, address2, city, state, zip_code, e_mail_address, parent_sb_modem_number, development_modem_number, development_ftp_password, ein_number, eftps_tin_number, eftps_bank_format, use_prenote, impound_trust_monies_as_receiv, pay_tax_from_payables, ar_export_format, default_check_format, micr_font, micr_horizontal_adjustment, auto_save_minutes, phone, fax, cover_letter_notes, invoice_notes, tax_cover_letter_notes, ar_import_directory, ach_directory, sb_url, days_in_prenote, sb_logo, user_password_duration_in_days, dummy_tax_cl_nbr, error_screen, pswd_min_length, pswd_force_mixed, misc_check_form, vmr_confidencial_notes, mark_liabs_paid_default, trust_impound, tax_impound, dd_impound, billing_impound, wc_impound, days_prior_to_check_date, sb_exception_payment_type, sb_max_ach_file_total, sb_ach_file_limitations, sb_cl_nbr, dashboard_msg, ee_login_type, ess_terms_of_use, wc_terms_of_use, effective_date, effective_until)
      VALUES (:sb_nbr, :sb_name, :address1, :address2, :city, :state, :zip_code, :e_mail_address, :parent_sb_modem_number, :development_modem_number, :development_ftp_password, :ein_number, :eftps_tin_number, :eftps_bank_format, :use_prenote, :impound_trust_monies_as_receiv, :pay_tax_from_payables, :ar_export_format, :default_check_format, :micr_font, :micr_horizontal_adjustment, :auto_save_minutes, :phone, :fax, :cover_letter_notes, :invoice_notes, :tax_cover_letter_notes, :ar_import_directory, :ach_directory, :sb_url, :days_in_prenote, :sb_logo, :user_password_duration_in_days, :dummy_tax_cl_nbr, :error_screen, :pswd_min_length, :pswd_force_mixed, :misc_check_form, :vmr_confidencial_notes, :mark_liabs_paid_default, :trust_impound, :tax_impound, :dd_impound, :billing_impound, :wc_impound, :days_prior_to_check_date, :sb_exception_payment_type, :sb_max_ach_file_total, :sb_ach_file_limitations, :sb_cl_nbr, :dashboard_msg, :ee_login_type, :ess_terms_of_use, :wc_terms_of_use, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb(sb_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb SET sb_name = :sb_name, address1 = :address1, address2 = :address2, city = :city, state = :state, zip_code = :zip_code, e_mail_address = :e_mail_address, parent_sb_modem_number = :parent_sb_modem_number, development_modem_number = :development_modem_number, development_ftp_password = :development_ftp_password, ein_number = :ein_number, eftps_tin_number = :eftps_tin_number, eftps_bank_format = :eftps_bank_format, use_prenote = :use_prenote, impound_trust_monies_as_receiv = :impound_trust_monies_as_receiv, pay_tax_from_payables = :pay_tax_from_payables, ar_export_format = :ar_export_format, default_check_format = :default_check_format, micr_font = :micr_font, micr_horizontal_adjustment = :micr_horizontal_adjustment, auto_save_minutes = :auto_save_minutes, phone = :phone, fax = :fax, cover_letter_notes = :cover_letter_notes, invoice_notes = :invoice_notes, tax_cover_letter_notes = :tax_cover_letter_notes, ar_import_directory = :ar_import_directory, ach_directory = :ach_directory, sb_url = :sb_url, days_in_prenote = :days_in_prenote, sb_logo = :sb_logo, user_password_duration_in_days = :user_password_duration_in_days, dummy_tax_cl_nbr = :dummy_tax_cl_nbr, error_screen = :error_screen, pswd_min_length = :pswd_min_length, pswd_force_mixed = :pswd_force_mixed, misc_check_form = :misc_check_form, vmr_confidencial_notes = :vmr_confidencial_notes, mark_liabs_paid_default = :mark_liabs_paid_default, trust_impound = :trust_impound, tax_impound = :tax_impound, dd_impound = :dd_impound, billing_impound = :billing_impound, wc_impound = :wc_impound, days_prior_to_check_date = :days_prior_to_check_date, sb_exception_payment_type = :sb_exception_payment_type, sb_max_ach_file_total = :sb_max_ach_file_total, sb_ach_file_limitations = :sb_ach_file_limitations, sb_cl_nbr = :sb_cl_nbr, dashboard_msg = :dashboard_msg, ee_login_type = :ee_login_type, ess_terms_of_use = :ess_terms_of_use, wc_terms_of_use = :wc_terms_of_use, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_ACCOUNTANT ================== */
/*<progress>Converting SB_ACCOUNTANT</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_ACCOUNTANT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_ACCOUNTANT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ACCOUNTANT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ACCOUNTANT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ACCOUNTANT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ACCOUNTANT_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_accountant ON xb_accountant (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_accountant_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE state VARCHAR(3);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE contact1 VARCHAR(30);
DECLARE VARIABLE phone1 VARCHAR(20);
DECLARE VARIABLE description1 VARCHAR(10);
DECLARE VARIABLE contact2 VARCHAR(30);
DECLARE VARIABLE phone2 VARCHAR(20);
DECLARE VARIABLE description2 VARCHAR(10);
DECLARE VARIABLE fax VARCHAR(20);
DECLARE VARIABLE fax_description VARCHAR(10);
DECLARE VARIABLE e_mail_address VARCHAR(80);
DECLARE VARIABLE print_name VARCHAR(40);
DECLARE VARIABLE title VARCHAR(30);
DECLARE VARIABLE signature BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_accountant_nbr, name, address1, address2, city, state, zip_code, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, e_mail_address, print_name, title, signature, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_accountant, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_accountant_nbr, :name, :address1, :address2, :city, :state, :zip_code, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :e_mail_address, :print_name, :title, :signature, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_accountant_nbr) THEN
      max_tbl_nbr = sb_accountant_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_accountant WHERE sb_accountant_nbr = :sb_accountant_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_accountant (sb_accountant_nbr, name, address1, address2, city, state, zip_code, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, e_mail_address, print_name, title, signature, effective_date, effective_until)
      VALUES (:sb_accountant_nbr, :name, :address1, :address2, :city, :state, :zip_code, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :e_mail_address, :print_name, :title, :signature, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_accountant(sb_accountant_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_accountant SET name = :name, address1 = :address1, address2 = :address2, city = :city, state = :state, zip_code = :zip_code, contact1 = :contact1, phone1 = :phone1, description1 = :description1, contact2 = :contact2, phone2 = :phone2, description2 = :description2, fax = :fax, fax_description = :fax_description, e_mail_address = :e_mail_address, print_name = :print_name, title = :title, signature = :signature, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_accountant RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_accountant^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_ACCOUNTANT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_ACCOUNTANT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ACCOUNTANT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ACCOUNTANT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ACCOUNTANT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ACCOUNTANT_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_AGENCY ================== */
/*<progress>Converting SB_AGENCY</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_AGENCY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_AGENCY_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_agency ON xb_agency (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_agency_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE contact1 VARCHAR(30);
DECLARE VARIABLE phone1 VARCHAR(20);
DECLARE VARIABLE description1 VARCHAR(10);
DECLARE VARIABLE contact2 VARCHAR(30);
DECLARE VARIABLE phone2 VARCHAR(20);
DECLARE VARIABLE description2 VARCHAR(10);
DECLARE VARIABLE fax VARCHAR(20);
DECLARE VARIABLE fax_description VARCHAR(10);
DECLARE VARIABLE e_mail_address VARCHAR(80);
DECLARE VARIABLE agency_type CHAR(1);
DECLARE VARIABLE sb_banks_nbr INTEGER;
DECLARE VARIABLE account_number VARCHAR(20);
DECLARE VARIABLE account_type CHAR(1);
DECLARE VARIABLE negative_direct_dep_allowed CHAR(1);
DECLARE VARIABLE modem_number VARCHAR(20);
DECLARE VARIABLE notes BLOB;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE print_name VARCHAR(40);
DECLARE VARIABLE county VARCHAR(20);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_agency_nbr, name, address1, address2, city, state, zip_code, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, e_mail_address, agency_type, sb_banks_nbr, account_number, account_type, negative_direct_dep_allowed, modem_number, notes, filler, print_name, county, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_agency, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_agency_nbr, :name, :address1, :address2, :city, :state, :zip_code, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :e_mail_address, :agency_type, :sb_banks_nbr, :account_number, :account_type, :negative_direct_dep_allowed, :modem_number, :notes, :filler, :print_name, :county, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_agency_nbr) THEN
      max_tbl_nbr = sb_agency_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_agency WHERE sb_agency_nbr = :sb_agency_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_agency (sb_agency_nbr, name, address1, address2, city, state, zip_code, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, e_mail_address, agency_type, sb_banks_nbr, account_number, account_type, negative_direct_dep_allowed, modem_number, notes, filler, print_name, county, effective_date, effective_until)
      VALUES (:sb_agency_nbr, :name, :address1, :address2, :city, :state, :zip_code, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :e_mail_address, :agency_type, :sb_banks_nbr, :account_number, :account_type, :negative_direct_dep_allowed, :modem_number, :notes, :filler, :print_name, :county, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_agency(sb_agency_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_agency SET name = :name, address1 = :address1, address2 = :address2, city = :city, state = :state, zip_code = :zip_code, contact1 = :contact1, phone1 = :phone1, description1 = :description1, contact2 = :contact2, phone2 = :phone2, description2 = :description2, fax = :fax, fax_description = :fax_description, e_mail_address = :e_mail_address, agency_type = :agency_type, sb_banks_nbr = :sb_banks_nbr, account_number = :account_number, account_type = :account_type, negative_direct_dep_allowed = :negative_direct_dep_allowed, modem_number = :modem_number, notes = :notes, filler = :filler, print_name = :print_name, county = :county, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_agency RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_agency^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_AGENCY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_AGENCY_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_AGENCY_REPORTS ================== */
/*<progress>Converting SB_AGENCY_REPORTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_AGENCY_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_AGENCY_REPORTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_REPORTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_REPORTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_REPORTS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_agency_reports ON xb_agency_reports (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_agency_reports_nbr INTEGER;
DECLARE VARIABLE sb_agency_nbr INTEGER;
DECLARE VARIABLE sb_reports_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_agency_reports_nbr, sb_agency_nbr, sb_reports_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_agency_reports, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_agency_reports_nbr, :sb_agency_nbr, :sb_reports_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_agency_reports_nbr) THEN
      max_tbl_nbr = sb_agency_reports_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_agency_reports WHERE sb_agency_reports_nbr = :sb_agency_reports_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_agency_reports (sb_agency_reports_nbr, sb_agency_nbr, sb_reports_nbr, effective_date, effective_until)
      VALUES (:sb_agency_reports_nbr, :sb_agency_nbr, :sb_reports_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_agency_reports(sb_agency_reports_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_agency_reports SET sb_agency_nbr = :sb_agency_nbr, sb_reports_nbr = :sb_reports_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_agency_reports RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_agency_reports^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_AGENCY_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_AGENCY_REPORTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_REPORTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_AGENCY_REPORTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_AGENCY_REPORTS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_BANKS ================== */
/*<progress>Converting SB_BANKS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_BANKS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_BANKS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANKS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANKS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANKS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANKS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_banks ON xb_banks (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_banks_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE contact1 VARCHAR(30);
DECLARE VARIABLE phone1 VARCHAR(20);
DECLARE VARIABLE description1 VARCHAR(10);
DECLARE VARIABLE contact2 VARCHAR(30);
DECLARE VARIABLE phone2 VARCHAR(20);
DECLARE VARIABLE description2 VARCHAR(10);
DECLARE VARIABLE fax VARCHAR(20);
DECLARE VARIABLE fax_description VARCHAR(10);
DECLARE VARIABLE e_mail_address VARCHAR(80);
DECLARE VARIABLE aba_number CHAR(9);
DECLARE VARIABLE top_aba_number VARCHAR(10);
DECLARE VARIABLE bottom_aba_number VARCHAR(10);
DECLARE VARIABLE addenda VARCHAR(12);
DECLARE VARIABLE check_template BLOB;
DECLARE VARIABLE use_check_template CHAR(1);
DECLARE VARIABLE micr_account_start_position INTEGER;
DECLARE VARIABLE micr_check_number_start_positn INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE print_name VARCHAR(40);
DECLARE VARIABLE branch_identifier VARCHAR(9);
DECLARE VARIABLE allow_hyphens CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_banks_nbr, name, address1, address2, city, state, zip_code, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, e_mail_address, aba_number, top_aba_number, bottom_aba_number, addenda, check_template, use_check_template, micr_account_start_position, micr_check_number_start_positn, filler, print_name, branch_identifier, allow_hyphens, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_banks, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_banks_nbr, :name, :address1, :address2, :city, :state, :zip_code, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :e_mail_address, :aba_number, :top_aba_number, :bottom_aba_number, :addenda, :check_template, :use_check_template, :micr_account_start_position, :micr_check_number_start_positn, :filler, :print_name, :branch_identifier, :allow_hyphens, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_banks_nbr) THEN
      max_tbl_nbr = sb_banks_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_banks WHERE sb_banks_nbr = :sb_banks_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_banks (sb_banks_nbr, name, address1, address2, city, state, zip_code, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, e_mail_address, aba_number, top_aba_number, bottom_aba_number, addenda, check_template, use_check_template, micr_account_start_position, micr_check_number_start_positn, filler, print_name, branch_identifier, allow_hyphens, effective_date, effective_until)
      VALUES (:sb_banks_nbr, :name, :address1, :address2, :city, :state, :zip_code, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :e_mail_address, :aba_number, :top_aba_number, :bottom_aba_number, :addenda, :check_template, :use_check_template, :micr_account_start_position, :micr_check_number_start_positn, :filler, :print_name, :branch_identifier, :allow_hyphens, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_banks(sb_banks_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_banks SET name = :name, address1 = :address1, address2 = :address2, city = :city, state = :state, zip_code = :zip_code, contact1 = :contact1, phone1 = :phone1, description1 = :description1, contact2 = :contact2, phone2 = :phone2, description2 = :description2, fax = :fax, fax_description = :fax_description, e_mail_address = :e_mail_address, aba_number = :aba_number, top_aba_number = :top_aba_number, bottom_aba_number = :bottom_aba_number, addenda = :addenda, check_template = :check_template, use_check_template = :use_check_template, micr_account_start_position = :micr_account_start_position, micr_check_number_start_positn = :micr_check_number_start_positn, filler = :filler, print_name = :print_name, branch_identifier = :branch_identifier, allow_hyphens = :allow_hyphens, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_banks RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_banks^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_BANKS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_BANKS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANKS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANKS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANKS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANKS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_BANK_ACCOUNTS ================== */
/*<progress>Converting SB_BANK_ACCOUNTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_BANK_ACCOUNTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_BANK_ACCOUNTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANK_ACCOUNTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANK_ACCOUNTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANK_ACCOUNTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANK_ACCOUNTS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_bank_accounts ON xb_bank_accounts (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_bank_accounts_nbr INTEGER;
DECLARE VARIABLE sb_banks_nbr INTEGER;
DECLARE VARIABLE custom_bank_account_number VARCHAR(20);
DECLARE VARIABLE bank_account_type CHAR(1);
DECLARE VARIABLE name_description VARCHAR(40);
DECLARE VARIABLE ach_origin_sb_banks_nbr INTEGER;
DECLARE VARIABLE bank_returns CHAR(1);
DECLARE VARIABLE custom_header_record VARCHAR(80);
DECLARE VARIABLE next_available_check_number INTEGER;
DECLARE VARIABLE end_check_number INTEGER;
DECLARE VARIABLE next_begin_check_number INTEGER;
DECLARE VARIABLE next_end_check_number INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE suppress_offset_account CHAR(1);
DECLARE VARIABLE block_negative_checks CHAR(1);
DECLARE VARIABLE batch_filer_id VARCHAR(9);
DECLARE VARIABLE master_inquiry_pin VARCHAR(10);
DECLARE VARIABLE logo_sb_blob_nbr INTEGER;
DECLARE VARIABLE signature_sb_blob_nbr INTEGER;
DECLARE VARIABLE beginning_balance NUMERIC(18,6);
DECLARE VARIABLE operating_account CHAR(1);
DECLARE VARIABLE billing_account CHAR(1);
DECLARE VARIABLE ach_account CHAR(1);
DECLARE VARIABLE trust_account CHAR(1);
DECLARE VARIABLE tax_account CHAR(1);
DECLARE VARIABLE obc_account CHAR(1);
DECLARE VARIABLE workers_comp_account CHAR(1);
DECLARE VARIABLE reccuring_wire_number INTEGER;
DECLARE VARIABLE bank_check CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_bank_accounts_nbr, sb_banks_nbr, custom_bank_account_number, bank_account_type, name_description, ach_origin_sb_banks_nbr, bank_returns, custom_header_record, next_available_check_number, end_check_number, next_begin_check_number, next_end_check_number, filler, suppress_offset_account, block_negative_checks, batch_filer_id, master_inquiry_pin, logo_sb_blob_nbr, signature_sb_blob_nbr, beginning_balance, operating_account, billing_account, ach_account, trust_account, tax_account, obc_account, workers_comp_account, reccuring_wire_number, bank_check, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_bank_accounts, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_bank_accounts_nbr, :sb_banks_nbr, :custom_bank_account_number, :bank_account_type, :name_description, :ach_origin_sb_banks_nbr, :bank_returns, :custom_header_record, :next_available_check_number, :end_check_number, :next_begin_check_number, :next_end_check_number, :filler, :suppress_offset_account, :block_negative_checks, :batch_filer_id, :master_inquiry_pin, :logo_sb_blob_nbr, :signature_sb_blob_nbr, :beginning_balance, :operating_account, :billing_account, :ach_account, :trust_account, :tax_account, :obc_account, :workers_comp_account, :reccuring_wire_number, :bank_check, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_bank_accounts_nbr) THEN
      max_tbl_nbr = sb_bank_accounts_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_bank_accounts WHERE sb_bank_accounts_nbr = :sb_bank_accounts_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_bank_accounts (sb_bank_accounts_nbr, sb_banks_nbr, custom_bank_account_number, bank_account_type, name_description, ach_origin_sb_banks_nbr, bank_returns, custom_header_record, next_available_check_number, end_check_number, next_begin_check_number, next_end_check_number, filler, suppress_offset_account, block_negative_checks, batch_filer_id, master_inquiry_pin, logo_sb_blob_nbr, signature_sb_blob_nbr, beginning_balance, operating_account, billing_account, ach_account, trust_account, tax_account, obc_account, workers_comp_account, reccuring_wire_number, bank_check, effective_date, effective_until)
      VALUES (:sb_bank_accounts_nbr, :sb_banks_nbr, :custom_bank_account_number, :bank_account_type, :name_description, :ach_origin_sb_banks_nbr, :bank_returns, :custom_header_record, :next_available_check_number, :end_check_number, :next_begin_check_number, :next_end_check_number, :filler, :suppress_offset_account, :block_negative_checks, :batch_filer_id, :master_inquiry_pin, :logo_sb_blob_nbr, :signature_sb_blob_nbr, :beginning_balance, :operating_account, :billing_account, :ach_account, :trust_account, :tax_account, :obc_account, :workers_comp_account, :reccuring_wire_number, :bank_check, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_bank_accounts(sb_bank_accounts_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_bank_accounts SET sb_banks_nbr = :sb_banks_nbr, custom_bank_account_number = :custom_bank_account_number, bank_account_type = :bank_account_type, name_description = :name_description, ach_origin_sb_banks_nbr = :ach_origin_sb_banks_nbr, bank_returns = :bank_returns, custom_header_record = :custom_header_record, next_available_check_number = :next_available_check_number, end_check_number = :end_check_number, next_begin_check_number = :next_begin_check_number, next_end_check_number = :next_end_check_number, filler = :filler, suppress_offset_account = :suppress_offset_account, block_negative_checks = :block_negative_checks, batch_filer_id = :batch_filer_id, master_inquiry_pin = :master_inquiry_pin, logo_sb_blob_nbr = :logo_sb_blob_nbr, signature_sb_blob_nbr = :signature_sb_blob_nbr, beginning_balance = :beginning_balance, operating_account = :operating_account, billing_account = :billing_account, ach_account = :ach_account, trust_account = :trust_account, tax_account = :tax_account, obc_account = :obc_account, workers_comp_account = :workers_comp_account, reccuring_wire_number = :reccuring_wire_number, bank_check = :bank_check, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_bank_accounts RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_bank_accounts^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_BANK_ACCOUNTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_BANK_ACCOUNTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANK_ACCOUNTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANK_ACCOUNTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BANK_ACCOUNTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BANK_ACCOUNTS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_BLOB ================== */
/*<progress>Converting SB_BLOB</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_BLOB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_BLOB_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BLOB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BLOB_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BLOB_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BLOB_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_blob ON xb_blob (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_blob_nbr INTEGER;
DECLARE VARIABLE data BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_blob_nbr, data, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_blob, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_blob_nbr, :data, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_blob_nbr) THEN
      max_tbl_nbr = sb_blob_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_blob WHERE sb_blob_nbr = :sb_blob_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_blob (sb_blob_nbr, data, effective_date, effective_until)
      VALUES (:sb_blob_nbr, :data, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_blob(sb_blob_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_blob SET data = :data, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_blob RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_blob^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_BLOB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_BLOB_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BLOB_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BLOB_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_BLOB_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_BLOB_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_DELIVERY_COMPANY ================== */
/*<progress>Converting SB_DELIVERY_COMPANY</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_COMPANY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_COMPANY_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_delivery_company ON xb_delivery_company (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_delivery_company_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE delivery_contact VARCHAR(40);
DECLARE VARIABLE delivery_contact_phone VARCHAR(20);
DECLARE VARIABLE delivery_contact_phone_type CHAR(1);
DECLARE VARIABLE supplies_contact VARCHAR(40);
DECLARE VARIABLE supplies_contact_phone VARCHAR(20);
DECLARE VARIABLE supplies_contact_phone_type CHAR(1);
DECLARE VARIABLE web_site VARCHAR(40);
DECLARE VARIABLE delivery_company_notes BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_delivery_company_nbr, name, delivery_contact, delivery_contact_phone, delivery_contact_phone_type, supplies_contact, supplies_contact_phone, supplies_contact_phone_type, web_site, delivery_company_notes, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_delivery_company, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_delivery_company_nbr, :name, :delivery_contact, :delivery_contact_phone, :delivery_contact_phone_type, :supplies_contact, :supplies_contact_phone, :supplies_contact_phone_type, :web_site, :delivery_company_notes, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_delivery_company_nbr) THEN
      max_tbl_nbr = sb_delivery_company_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_delivery_company WHERE sb_delivery_company_nbr = :sb_delivery_company_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_delivery_company (sb_delivery_company_nbr, name, delivery_contact, delivery_contact_phone, delivery_contact_phone_type, supplies_contact, supplies_contact_phone, supplies_contact_phone_type, web_site, delivery_company_notes, effective_date, effective_until)
      VALUES (:sb_delivery_company_nbr, :name, :delivery_contact, :delivery_contact_phone, :delivery_contact_phone_type, :supplies_contact, :supplies_contact_phone, :supplies_contact_phone_type, :web_site, :delivery_company_notes, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_delivery_company(sb_delivery_company_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_delivery_company SET name = :name, delivery_contact = :delivery_contact, delivery_contact_phone = :delivery_contact_phone, delivery_contact_phone_type = :delivery_contact_phone_type, supplies_contact = :supplies_contact, supplies_contact_phone = :supplies_contact_phone, supplies_contact_phone_type = :supplies_contact_phone_type, web_site = :web_site, delivery_company_notes = :delivery_company_notes, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_delivery_company RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_delivery_company^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_COMPANY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_COMPANY_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_DELIVERY_COMPANY_SVCS ================== */
/*<progress>Converting SB_DELIVERY_COMPANY_SVCS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_COMPANY_SVC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_COMPANY_SVC_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_SVC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_SVC_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_SVC_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_SVC_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_delivery_company_svcs ON xb_delivery_company_svcs (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_delivery_company_svcs_nbr INTEGER;
DECLARE VARIABLE sb_delivery_company_nbr INTEGER;
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE reference_fee NUMERIC(18,6);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_delivery_company_svcs_nbr, sb_delivery_company_nbr, description, reference_fee, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_delivery_company_svcs, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_delivery_company_svcs_nbr, :sb_delivery_company_nbr, :description, :reference_fee, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_delivery_company_svcs_nbr) THEN
      max_tbl_nbr = sb_delivery_company_svcs_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_delivery_company_svcs WHERE sb_delivery_company_svcs_nbr = :sb_delivery_company_svcs_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_delivery_company_svcs (sb_delivery_company_svcs_nbr, sb_delivery_company_nbr, description, reference_fee, effective_date, effective_until)
      VALUES (:sb_delivery_company_svcs_nbr, :sb_delivery_company_nbr, :description, :reference_fee, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_delivery_company_svcs(sb_delivery_company_svcs_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_delivery_company_svcs SET sb_delivery_company_nbr = :sb_delivery_company_nbr, description = :description, reference_fee = :reference_fee, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_delivery_company_svcs RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_delivery_company_svcs^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_COMPANY_SVC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_COMPANY_SVC_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_SVC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_SVC_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_COMPANY_SVC_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_COMPANY_SVC_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_DELIVERY_METHOD ================== */
/*<progress>Converting SB_DELIVERY_METHOD</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_METHOD_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_METHOD_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_METHOD_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_METHOD_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_METHOD_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_METHOD_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_delivery_method ON xb_delivery_method (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_delivery_method_nbr INTEGER;
DECLARE VARIABLE sy_delivery_method_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_delivery_method_nbr, sy_delivery_method_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_delivery_method, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_delivery_method_nbr, :sy_delivery_method_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_delivery_method_nbr) THEN
      max_tbl_nbr = sb_delivery_method_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_delivery_method WHERE sb_delivery_method_nbr = :sb_delivery_method_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_delivery_method (sb_delivery_method_nbr, sy_delivery_method_nbr, effective_date, effective_until)
      VALUES (:sb_delivery_method_nbr, :sy_delivery_method_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_delivery_method(sb_delivery_method_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_delivery_method SET sy_delivery_method_nbr = :sy_delivery_method_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_delivery_method RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_delivery_method^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_METHOD_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_METHOD_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_METHOD_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_METHOD_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_METHOD_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_METHOD_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_DELIVERY_SERVICE ================== */
/*<progress>Converting SB_DELIVERY_SERVICE</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_SERVICE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_delivery_service ON xb_delivery_service (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_delivery_service_nbr INTEGER;
DECLARE VARIABLE sy_delivery_service_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_delivery_service_nbr, sy_delivery_service_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_delivery_service, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_delivery_service_nbr, :sy_delivery_service_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_delivery_service_nbr) THEN
      max_tbl_nbr = sb_delivery_service_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_delivery_service WHERE sb_delivery_service_nbr = :sb_delivery_service_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_delivery_service (sb_delivery_service_nbr, sy_delivery_service_nbr, effective_date, effective_until)
      VALUES (:sb_delivery_service_nbr, :sy_delivery_service_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_delivery_service(sb_delivery_service_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_delivery_service SET sy_delivery_service_nbr = :sy_delivery_service_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_delivery_service RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_delivery_service^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_SERVICE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_DELIVERY_SERVICE_OPT ================== */
/*<progress>Converting SB_DELIVERY_SERVICE_OPT</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_SERVICE_OPT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_SERVICE_OPT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_OPT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_OPT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_OPT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_OPT_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_delivery_service_opt ON xb_delivery_service_opt (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_delivery_service_opt_nbr INTEGER;
DECLARE VARIABLE sb_delivery_service_nbr INTEGER;
DECLARE VARIABLE sb_delivery_method_nbr INTEGER;
DECLARE VARIABLE option_tag VARCHAR(40);
DECLARE VARIABLE option_integer_value INTEGER;
DECLARE VARIABLE option_string_value VARCHAR(512);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_delivery_service_opt_nbr, sb_delivery_service_nbr, sb_delivery_method_nbr, option_tag, option_integer_value, option_string_value, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_delivery_service_opt, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_delivery_service_opt_nbr, :sb_delivery_service_nbr, :sb_delivery_method_nbr, :option_tag, :option_integer_value, :option_string_value, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_delivery_service_opt_nbr) THEN
      max_tbl_nbr = sb_delivery_service_opt_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_delivery_service_opt WHERE sb_delivery_service_opt_nbr = :sb_delivery_service_opt_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_delivery_service_opt (sb_delivery_service_opt_nbr, sb_delivery_service_nbr, sb_delivery_method_nbr, option_tag, option_integer_value, option_string_value, effective_date, effective_until)
      VALUES (:sb_delivery_service_opt_nbr, :sb_delivery_service_nbr, :sb_delivery_method_nbr, :option_tag, :option_integer_value, :option_string_value, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_delivery_service_opt(sb_delivery_service_opt_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_delivery_service_opt SET sb_delivery_service_nbr = :sb_delivery_service_nbr, sb_delivery_method_nbr = :sb_delivery_method_nbr, option_tag = :option_tag, option_integer_value = :option_integer_value, option_string_value = :option_string_value, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_delivery_service_opt RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_delivery_service_opt^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_DELIVERY_SERVICE_OPT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_DELIVERY_SERVICE_OPT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_OPT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_OPT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_DELIVERY_SERVICE_OPT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_DELIVERY_SERVICE_OPT_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_ENLIST_GROUPS ================== */
/*<progress>Converting SB_ENLIST_GROUPS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_ENLIST_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_ENLIST_GROUPS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ENLIST_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ENLIST_GROUPS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ENLIST_GROUPS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ENLIST_GROUPS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_enlist_groups ON xb_enlist_groups (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_enlist_groups_nbr INTEGER;
DECLARE VARIABLE sy_report_groups_nbr INTEGER;
DECLARE VARIABLE media_type CHAR(1);
DECLARE VARIABLE process_type CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_enlist_groups_nbr, sy_report_groups_nbr, media_type, process_type, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_enlist_groups, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_enlist_groups_nbr, :sy_report_groups_nbr, :media_type, :process_type, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_enlist_groups_nbr) THEN
      max_tbl_nbr = sb_enlist_groups_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_enlist_groups WHERE sb_enlist_groups_nbr = :sb_enlist_groups_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_enlist_groups (sb_enlist_groups_nbr, sy_report_groups_nbr, media_type, process_type, effective_date, effective_until)
      VALUES (:sb_enlist_groups_nbr, :sy_report_groups_nbr, :media_type, :process_type, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_enlist_groups(sb_enlist_groups_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_enlist_groups SET sy_report_groups_nbr = :sy_report_groups_nbr, media_type = :media_type, process_type = :process_type, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_enlist_groups RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_enlist_groups^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_ENLIST_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_ENLIST_GROUPS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ENLIST_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ENLIST_GROUPS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_ENLIST_GROUPS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_ENLIST_GROUPS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_GLOBAL_AGENCY_CONTACTS ================== */
/*<progress>Converting SB_GLOBAL_AGENCY_CONTACTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_GLOBAL_AGENCY_CONTAC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_GLOBAL_AGENCY_CONTAC_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_GLOBAL_AGENCY_CONTAC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_GLOBAL_AGENCY_CONTAC_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_GLOBAL_AGENCY_CONTAC_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_GLOBAL_AGENCY_CONTAC_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_global_agency_contacts ON xb_global_agency_contacts (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_global_agency_contacts_nbr INTEGER;
DECLARE VARIABLE sy_global_agency_nbr INTEGER;
DECLARE VARIABLE contact_name VARCHAR(30);
DECLARE VARIABLE phone VARCHAR(20);
DECLARE VARIABLE fax VARCHAR(20);
DECLARE VARIABLE e_mail_address VARCHAR(80);
DECLARE VARIABLE notes BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_global_agency_contacts_nbr, sy_global_agency_nbr, contact_name, phone, fax, e_mail_address, notes, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_global_agency_contacts, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_global_agency_contacts_nbr, :sy_global_agency_nbr, :contact_name, :phone, :fax, :e_mail_address, :notes, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_global_agency_contacts_nbr) THEN
      max_tbl_nbr = sb_global_agency_contacts_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_global_agency_contacts WHERE sb_global_agency_contacts_nbr = :sb_global_agency_contacts_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_global_agency_contacts (sb_global_agency_contacts_nbr, sy_global_agency_nbr, contact_name, phone, fax, e_mail_address, notes, effective_date, effective_until)
      VALUES (:sb_global_agency_contacts_nbr, :sy_global_agency_nbr, :contact_name, :phone, :fax, :e_mail_address, :notes, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_global_agency_contacts(sb_global_agency_contacts_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_global_agency_contacts SET sy_global_agency_nbr = :sy_global_agency_nbr, contact_name = :contact_name, phone = :phone, fax = :fax, e_mail_address = :e_mail_address, notes = :notes, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_global_agency_contacts RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_global_agency_contacts^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_GLOBAL_AGENCY_CONTAC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_GLOBAL_AGENCY_CONTAC_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_GLOBAL_AGENCY_CONTAC_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_GLOBAL_AGENCY_CONTAC_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_GLOBAL_AGENCY_CONTAC_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_GLOBAL_AGENCY_CONTAC_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_HOLIDAYS ================== */
/*<progress>Converting SB_HOLIDAYS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_HOLIDAYS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_HOLIDAYS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_HOLIDAYS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_HOLIDAYS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_HOLIDAYS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_HOLIDAYS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_holidays ON xb_holidays (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_holidays_nbr INTEGER;
DECLARE VARIABLE holiday_date DATE;
DECLARE VARIABLE holiday_name VARCHAR(40);
DECLARE VARIABLE used_by CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_holidays_nbr, holiday_date, holiday_name, used_by, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_holidays, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_holidays_nbr, :holiday_date, :holiday_name, :used_by, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_holidays_nbr) THEN
      max_tbl_nbr = sb_holidays_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_holidays WHERE sb_holidays_nbr = :sb_holidays_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_holidays (sb_holidays_nbr, holiday_date, holiday_name, used_by, effective_date, effective_until)
      VALUES (:sb_holidays_nbr, :holiday_date, :holiday_name, :used_by, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_holidays(sb_holidays_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_holidays SET holiday_date = :holiday_date, holiday_name = :holiday_name, used_by = :used_by, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_holidays RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_holidays^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_HOLIDAYS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_HOLIDAYS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_HOLIDAYS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_HOLIDAYS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_HOLIDAYS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_HOLIDAYS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_MAIL_BOX ================== */
/*<progress>Converting SB_MAIL_BOX</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MAIL_BOX_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MAIL_BOX_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_mail_box ON xb_mail_box (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_mail_box_nbr INTEGER;
DECLARE VARIABLE cl_nbr INTEGER;
DECLARE VARIABLE cl_mail_box_group_nbr INTEGER;
DECLARE VARIABLE cost NUMERIC(18,6);
DECLARE VARIABLE required CHAR(1);
DECLARE VARIABLE notification_email VARCHAR(80);
DECLARE VARIABLE pr_nbr INTEGER;
DECLARE VARIABLE released_time TIMESTAMP;
DECLARE VARIABLE printed_time TIMESTAMP;
DECLARE VARIABLE sb_cover_letter_report_nbr INTEGER;
DECLARE VARIABLE scanned_time TIMESTAMP;
DECLARE VARIABLE shipped_time TIMESTAMP;
DECLARE VARIABLE sy_cover_letter_report_nbr INTEGER;
DECLARE VARIABLE note VARCHAR(512);
DECLARE VARIABLE up_level_mail_box_nbr INTEGER;
DECLARE VARIABLE tracking_info VARCHAR(40);
DECLARE VARIABLE auto_release_type CHAR(1);
DECLARE VARIABLE barcode VARCHAR(40);
DECLARE VARIABLE created_time TIMESTAMP;
DECLARE VARIABLE dispose_content_after_shipping CHAR(1);
DECLARE VARIABLE addressee VARCHAR(512);
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE sb_delivery_method_nbr INTEGER;
DECLARE VARIABLE sb_media_type_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_mail_box_nbr, cl_nbr, cl_mail_box_group_nbr, cost, required, notification_email, pr_nbr, released_time, printed_time, sb_cover_letter_report_nbr, scanned_time, shipped_time, sy_cover_letter_report_nbr, note, up_level_mail_box_nbr, tracking_info, auto_release_type, barcode, created_time, dispose_content_after_shipping, addressee, description, sb_delivery_method_nbr, sb_media_type_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_mail_box, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_mail_box_nbr, :cl_nbr, :cl_mail_box_group_nbr, :cost, :required, :notification_email, :pr_nbr, :released_time, :printed_time, :sb_cover_letter_report_nbr, :scanned_time, :shipped_time, :sy_cover_letter_report_nbr, :note, :up_level_mail_box_nbr, :tracking_info, :auto_release_type, :barcode, :created_time, :dispose_content_after_shipping, :addressee, :description, :sb_delivery_method_nbr, :sb_media_type_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_mail_box_nbr) THEN
      max_tbl_nbr = sb_mail_box_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_mail_box WHERE sb_mail_box_nbr = :sb_mail_box_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_mail_box (sb_mail_box_nbr, cl_nbr, cl_mail_box_group_nbr, cost, required, notification_email, pr_nbr, released_time, printed_time, sb_cover_letter_report_nbr, scanned_time, shipped_time, sy_cover_letter_report_nbr, note, up_level_mail_box_nbr, tracking_info, auto_release_type, barcode, created_time, dispose_content_after_shipping, addressee, description, sb_delivery_method_nbr, sb_media_type_nbr, effective_date, effective_until)
      VALUES (:sb_mail_box_nbr, :cl_nbr, :cl_mail_box_group_nbr, :cost, :required, :notification_email, :pr_nbr, :released_time, :printed_time, :sb_cover_letter_report_nbr, :scanned_time, :shipped_time, :sy_cover_letter_report_nbr, :note, :up_level_mail_box_nbr, :tracking_info, :auto_release_type, :barcode, :created_time, :dispose_content_after_shipping, :addressee, :description, :sb_delivery_method_nbr, :sb_media_type_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_mail_box(sb_mail_box_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_mail_box SET cl_nbr = :cl_nbr, cl_mail_box_group_nbr = :cl_mail_box_group_nbr, cost = :cost, required = :required, notification_email = :notification_email, pr_nbr = :pr_nbr, released_time = :released_time, printed_time = :printed_time, sb_cover_letter_report_nbr = :sb_cover_letter_report_nbr, scanned_time = :scanned_time, shipped_time = :shipped_time, sy_cover_letter_report_nbr = :sy_cover_letter_report_nbr, note = :note, up_level_mail_box_nbr = :up_level_mail_box_nbr, tracking_info = :tracking_info, auto_release_type = :auto_release_type, barcode = :barcode, created_time = :created_time, dispose_content_after_shipping = :dispose_content_after_shipping, addressee = :addressee, description = :description, sb_delivery_method_nbr = :sb_delivery_method_nbr, sb_media_type_nbr = :sb_media_type_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_mail_box RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_mail_box^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MAIL_BOX_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MAIL_BOX_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_MAIL_BOX_CONTENT ================== */
/*<progress>Converting SB_MAIL_BOX_CONTENT</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MAIL_BOX_CONTENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MAIL_BOX_CONTENT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_CONTENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_CONTENT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_CONTENT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_CONTENT_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_mail_box_content ON xb_mail_box_content (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_mail_box_content_nbr INTEGER;
DECLARE VARIABLE sb_mail_box_nbr INTEGER;
DECLARE VARIABLE description VARCHAR(512);
DECLARE VARIABLE file_name VARCHAR(40);
DECLARE VARIABLE media_type CHAR(1);
DECLARE VARIABLE page_count INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_mail_box_content_nbr, sb_mail_box_nbr, description, file_name, media_type, page_count, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_mail_box_content, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_mail_box_content_nbr, :sb_mail_box_nbr, :description, :file_name, :media_type, :page_count, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_mail_box_content_nbr) THEN
      max_tbl_nbr = sb_mail_box_content_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_mail_box_content WHERE sb_mail_box_content_nbr = :sb_mail_box_content_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_mail_box_content (sb_mail_box_content_nbr, sb_mail_box_nbr, description, file_name, media_type, page_count, effective_date, effective_until)
      VALUES (:sb_mail_box_content_nbr, :sb_mail_box_nbr, :description, :file_name, :media_type, :page_count, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_mail_box_content(sb_mail_box_content_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_mail_box_content SET sb_mail_box_nbr = :sb_mail_box_nbr, description = :description, file_name = :file_name, media_type = :media_type, page_count = :page_count, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_mail_box_content RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_mail_box_content^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MAIL_BOX_CONTENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MAIL_BOX_CONTENT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_CONTENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_CONTENT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_CONTENT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_CONTENT_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_MAIL_BOX_OPTION ================== */
/*<progress>Converting SB_MAIL_BOX_OPTION</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MAIL_BOX_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MAIL_BOX_OPTION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_OPTION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_OPTION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_OPTION_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_mail_box_option ON xb_mail_box_option (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_mail_box_option_nbr INTEGER;
DECLARE VARIABLE sb_mail_box_nbr INTEGER;
DECLARE VARIABLE sb_mail_box_content_nbr INTEGER;
DECLARE VARIABLE option_tag VARCHAR(80);
DECLARE VARIABLE option_integer_value INTEGER;
DECLARE VARIABLE option_string_value VARCHAR(512);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_mail_box_option_nbr, sb_mail_box_nbr, sb_mail_box_content_nbr, option_tag, option_integer_value, option_string_value, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_mail_box_option, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_mail_box_option_nbr, :sb_mail_box_nbr, :sb_mail_box_content_nbr, :option_tag, :option_integer_value, :option_string_value, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_mail_box_option_nbr) THEN
      max_tbl_nbr = sb_mail_box_option_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_mail_box_option WHERE sb_mail_box_option_nbr = :sb_mail_box_option_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_mail_box_option (sb_mail_box_option_nbr, sb_mail_box_nbr, sb_mail_box_content_nbr, option_tag, option_integer_value, option_string_value, effective_date, effective_until)
      VALUES (:sb_mail_box_option_nbr, :sb_mail_box_nbr, :sb_mail_box_content_nbr, :option_tag, :option_integer_value, :option_string_value, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_mail_box_option(sb_mail_box_option_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_mail_box_option SET sb_mail_box_nbr = :sb_mail_box_nbr, sb_mail_box_content_nbr = :sb_mail_box_content_nbr, option_tag = :option_tag, option_integer_value = :option_integer_value, option_string_value = :option_string_value, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_mail_box_option RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_mail_box_option^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MAIL_BOX_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MAIL_BOX_OPTION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_OPTION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MAIL_BOX_OPTION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MAIL_BOX_OPTION_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_MEDIA_TYPE ================== */
/*<progress>Converting SB_MEDIA_TYPE</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MEDIA_TYPE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MEDIA_TYPE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_media_type ON xb_media_type (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_media_type_nbr INTEGER;
DECLARE VARIABLE sy_media_type_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_media_type_nbr, sy_media_type_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_media_type, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_media_type_nbr, :sy_media_type_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_media_type_nbr) THEN
      max_tbl_nbr = sb_media_type_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_media_type WHERE sb_media_type_nbr = :sb_media_type_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_media_type (sb_media_type_nbr, sy_media_type_nbr, effective_date, effective_until)
      VALUES (:sb_media_type_nbr, :sy_media_type_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_media_type(sb_media_type_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_media_type SET sy_media_type_nbr = :sy_media_type_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_media_type RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_media_type^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MEDIA_TYPE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MEDIA_TYPE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_MEDIA_TYPE_OPTION ================== */
/*<progress>Converting SB_MEDIA_TYPE_OPTION</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MEDIA_TYPE_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MEDIA_TYPE_OPTION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_OPTION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_OPTION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_OPTION_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_media_type_option ON xb_media_type_option (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_media_type_option_nbr INTEGER;
DECLARE VARIABLE sb_media_type_nbr INTEGER;
DECLARE VARIABLE option_tag VARCHAR(40);
DECLARE VARIABLE option_integer_value INTEGER;
DECLARE VARIABLE option_string_value VARCHAR(512);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_media_type_option_nbr, sb_media_type_nbr, option_tag, option_integer_value, option_string_value, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_media_type_option, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_media_type_option_nbr, :sb_media_type_nbr, :option_tag, :option_integer_value, :option_string_value, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_media_type_option_nbr) THEN
      max_tbl_nbr = sb_media_type_option_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_media_type_option WHERE sb_media_type_option_nbr = :sb_media_type_option_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_media_type_option (sb_media_type_option_nbr, sb_media_type_nbr, option_tag, option_integer_value, option_string_value, effective_date, effective_until)
      VALUES (:sb_media_type_option_nbr, :sb_media_type_nbr, :option_tag, :option_integer_value, :option_string_value, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_media_type_option(sb_media_type_option_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_media_type_option SET sb_media_type_nbr = :sb_media_type_nbr, option_tag = :option_tag, option_integer_value = :option_integer_value, option_string_value = :option_string_value, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_media_type_option RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_media_type_option^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MEDIA_TYPE_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MEDIA_TYPE_OPTION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_OPTION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MEDIA_TYPE_OPTION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MEDIA_TYPE_OPTION_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_MULTICLIENT_REPORTS ================== */
/*<progress>Converting SB_MULTICLIENT_REPORTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MULTICLIENT_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MULTICLIENT_REPORTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MULTICLIENT_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MULTICLIENT_REPORTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MULTICLIENT_REPORTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MULTICLIENT_REPORTS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_multiclient_reports ON xb_multiclient_reports (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_multiclient_reports_nbr INTEGER;
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE report_level CHAR(1);
DECLARE VARIABLE report_nbr INTEGER;
DECLARE VARIABLE input_params BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_multiclient_reports_nbr, description, report_level, report_nbr, input_params, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_multiclient_reports, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_multiclient_reports_nbr, :description, :report_level, :report_nbr, :input_params, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_multiclient_reports_nbr) THEN
      max_tbl_nbr = sb_multiclient_reports_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_multiclient_reports WHERE sb_multiclient_reports_nbr = :sb_multiclient_reports_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_multiclient_reports (sb_multiclient_reports_nbr, description, report_level, report_nbr, input_params, effective_date, effective_until)
      VALUES (:sb_multiclient_reports_nbr, :description, :report_level, :report_nbr, :input_params, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_multiclient_reports(sb_multiclient_reports_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_multiclient_reports SET description = :description, report_level = :report_level, report_nbr = :report_nbr, input_params = :input_params, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_multiclient_reports RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_multiclient_reports^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_MULTICLIENT_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_MULTICLIENT_REPORTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MULTICLIENT_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MULTICLIENT_REPORTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_MULTICLIENT_REPORTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_MULTICLIENT_REPORTS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_OPTION ================== */
/*<progress>Converting SB_OPTION</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_OPTION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OPTION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OPTION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OPTION_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_option ON xb_option (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_option_nbr INTEGER;
DECLARE VARIABLE option_tag VARCHAR(80);
DECLARE VARIABLE option_integer_value INTEGER;
DECLARE VARIABLE option_string_value VARCHAR(512);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_option_nbr, option_tag, option_integer_value, option_string_value, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_option, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_option_nbr, :option_tag, :option_integer_value, :option_string_value, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_option_nbr) THEN
      max_tbl_nbr = sb_option_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_option WHERE sb_option_nbr = :sb_option_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_option (sb_option_nbr, option_tag, option_integer_value, option_string_value, effective_date, effective_until)
      VALUES (:sb_option_nbr, :option_tag, :option_integer_value, :option_string_value, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_option(sb_option_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_option SET option_tag = :option_tag, option_integer_value = :option_integer_value, option_string_value = :option_string_value, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_option RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_option^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_OPTION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OPTION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OPTION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OPTION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OPTION_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_OTHER_SERVICE ================== */
/*<progress>Converting SB_OTHER_SERVICE</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_OTHER_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_OTHER_SERVICE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OTHER_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OTHER_SERVICE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OTHER_SERVICE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OTHER_SERVICE_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_other_service ON xb_other_service (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_other_service_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(60);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_other_service_nbr, name, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_other_service, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_other_service_nbr, :name, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_other_service_nbr) THEN
      max_tbl_nbr = sb_other_service_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_other_service WHERE sb_other_service_nbr = :sb_other_service_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_other_service (sb_other_service_nbr, name, effective_date, effective_until)
      VALUES (:sb_other_service_nbr, :name, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_other_service(sb_other_service_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_other_service SET name = :name, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_other_service RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_other_service^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_OTHER_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_OTHER_SERVICE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OTHER_SERVICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OTHER_SERVICE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_OTHER_SERVICE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_OTHER_SERVICE_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_PAPER_INFO ================== */
/*<progress>Converting SB_PAPER_INFO</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_PAPER_INFO_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_PAPER_INFO_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_PAPER_INFO_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_PAPER_INFO_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_PAPER_INFO_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_PAPER_INFO_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_paper_info ON xb_paper_info (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_paper_info_nbr INTEGER;
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE height NUMERIC(18,6);
DECLARE VARIABLE width NUMERIC(18,6);
DECLARE VARIABLE weight NUMERIC(18,6);
DECLARE VARIABLE media_type CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_paper_info_nbr, description, height, width, weight, media_type, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_paper_info, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_paper_info_nbr, :description, :height, :width, :weight, :media_type, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_paper_info_nbr) THEN
      max_tbl_nbr = sb_paper_info_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_paper_info WHERE sb_paper_info_nbr = :sb_paper_info_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_paper_info (sb_paper_info_nbr, description, height, width, weight, media_type, effective_date, effective_until)
      VALUES (:sb_paper_info_nbr, :description, :height, :width, :weight, :media_type, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_paper_info(sb_paper_info_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_paper_info SET description = :description, height = :height, width = :width, weight = :weight, media_type = :media_type, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_paper_info RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_paper_info^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_PAPER_INFO_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_PAPER_INFO_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_PAPER_INFO_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_PAPER_INFO_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_PAPER_INFO_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_PAPER_INFO_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_QUEUE_PRIORITY ================== */
/*<progress>Converting SB_QUEUE_PRIORITY</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_QUEUE_PRIORITY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_QUEUE_PRIORITY_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_QUEUE_PRIORITY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_QUEUE_PRIORITY_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_QUEUE_PRIORITY_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_QUEUE_PRIORITY_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_queue_priority ON xb_queue_priority (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_queue_priority_nbr INTEGER;
DECLARE VARIABLE threads INTEGER;
DECLARE VARIABLE method_name VARCHAR(255);
DECLARE VARIABLE priority INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE sb_sec_groups_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_queue_priority_nbr, package_id, method_name, priority, sb_user_nbr, sb_sec_groups_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_queue_priority, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_queue_priority_nbr, :threads, :method_name, :priority, :sb_user_nbr, :sb_sec_groups_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_queue_priority_nbr) THEN
      max_tbl_nbr = sb_queue_priority_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_queue_priority WHERE sb_queue_priority_nbr = :sb_queue_priority_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_queue_priority (sb_queue_priority_nbr, threads, method_name, priority, sb_user_nbr, sb_sec_groups_nbr, effective_date, effective_until)
      VALUES (:sb_queue_priority_nbr, :threads, :method_name, :priority, :sb_user_nbr, :sb_sec_groups_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_queue_priority(sb_queue_priority_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_queue_priority SET threads = :threads, method_name = :method_name, priority = :priority, sb_user_nbr = :sb_user_nbr, sb_sec_groups_nbr = :sb_sec_groups_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_queue_priority RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_queue_priority^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_QUEUE_PRIORITY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_QUEUE_PRIORITY_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_QUEUE_PRIORITY_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_QUEUE_PRIORITY_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_QUEUE_PRIORITY_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_QUEUE_PRIORITY_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_REFERRALS ================== */
/*<progress>Converting SB_REFERRALS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_REFERRALS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_REFERRALS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REFERRALS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REFERRALS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REFERRALS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REFERRALS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_referrals ON xb_referrals (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_referrals_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_referrals_nbr, name, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_referrals, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_referrals_nbr, :name, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_referrals_nbr) THEN
      max_tbl_nbr = sb_referrals_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_referrals WHERE sb_referrals_nbr = :sb_referrals_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_referrals (sb_referrals_nbr, name, effective_date, effective_until)
      VALUES (:sb_referrals_nbr, :name, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_referrals(sb_referrals_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_referrals SET name = :name, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_referrals RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_referrals^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_REFERRALS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_REFERRALS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REFERRALS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REFERRALS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REFERRALS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REFERRALS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_REPORTS ================== */
/*<progress>Converting SB_REPORTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_REPORTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORTS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_reports ON xb_reports (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_reports_nbr INTEGER;
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE comments BLOB;
DECLARE VARIABLE report_writer_reports_nbr INTEGER;
DECLARE VARIABLE report_level CHAR(1);
DECLARE VARIABLE input_params BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_reports_nbr, description, comments, report_writer_reports_nbr, report_level, input_params, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_reports, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_reports_nbr, :description, :comments, :report_writer_reports_nbr, :report_level, :input_params, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_reports_nbr) THEN
      max_tbl_nbr = sb_reports_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_reports WHERE sb_reports_nbr = :sb_reports_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_reports (sb_reports_nbr, description, comments, report_writer_reports_nbr, report_level, input_params, effective_date, effective_until)
      VALUES (:sb_reports_nbr, :description, :comments, :report_writer_reports_nbr, :report_level, :input_params, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_reports(sb_reports_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_reports SET description = :description, comments = :comments, report_writer_reports_nbr = :report_writer_reports_nbr, report_level = :report_level, input_params = :input_params, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_reports RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_reports^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_REPORTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORTS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_REPORT_WRITER_REPORTS ================== */
/*<progress>Converting SB_REPORT_WRITER_REPORTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_REPORT_WRITER_REPORT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_REPORT_WRITER_REPORT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORT_WRITER_REPORT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORT_WRITER_REPORT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORT_WRITER_REPORT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORT_WRITER_REPORT_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_report_writer_reports ON xb_report_writer_reports (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_report_writer_reports_nbr INTEGER;
DECLARE VARIABLE report_description VARCHAR(40);
DECLARE VARIABLE report_type CHAR(1);
DECLARE VARIABLE report_file BLOB;
DECLARE VARIABLE notes BLOB;
DECLARE VARIABLE media_type CHAR(1);
DECLARE VARIABLE class_name VARCHAR(40);
DECLARE VARIABLE ancestor_class_name VARCHAR(40);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_report_writer_reports_nbr, report_description, report_type, report_file, notes, media_type, class_name, ancestor_class_name, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_report_writer_reports, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_report_writer_reports_nbr, :report_description, :report_type, :report_file, :notes, :media_type, :class_name, :ancestor_class_name, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_report_writer_reports_nbr) THEN
      max_tbl_nbr = sb_report_writer_reports_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_report_writer_reports WHERE sb_report_writer_reports_nbr = :sb_report_writer_reports_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_report_writer_reports (sb_report_writer_reports_nbr, report_description, report_type, report_file, notes, media_type, class_name, ancestor_class_name, effective_date, effective_until)
      VALUES (:sb_report_writer_reports_nbr, :report_description, :report_type, :report_file, :notes, :media_type, :class_name, :ancestor_class_name, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_report_writer_reports(sb_report_writer_reports_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_report_writer_reports SET report_description = :report_description, report_type = :report_type, report_file = :report_file, notes = :notes, media_type = :media_type, class_name = :class_name, ancestor_class_name = :ancestor_class_name, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_report_writer_reports RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_report_writer_reports^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_REPORT_WRITER_REPORT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_REPORT_WRITER_REPORT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORT_WRITER_REPORT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORT_WRITER_REPORT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_REPORT_WRITER_REPORT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_REPORT_WRITER_REPORT_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SALES_TAX_STATES ================== */
/*<progress>Converting SB_SALES_TAX_STATES</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SALES_TAX_STATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SALES_TAX_STATES_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SALES_TAX_STATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SALES_TAX_STATES_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SALES_TAX_STATES_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SALES_TAX_STATES_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sales_tax_states ON xb_sales_tax_states (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sales_tax_states_nbr INTEGER;
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE state_tax_id VARCHAR(19);
DECLARE VARIABLE sales_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sales_tax_states_nbr, state, state_tax_id, sales_tax_percentage, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sales_tax_states, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sales_tax_states_nbr, :state, :state_tax_id, :sales_tax_percentage, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sales_tax_states_nbr) THEN
      max_tbl_nbr = sb_sales_tax_states_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sales_tax_states WHERE sb_sales_tax_states_nbr = :sb_sales_tax_states_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sales_tax_states (sb_sales_tax_states_nbr, state, state_tax_id, sales_tax_percentage, effective_date, effective_until)
      VALUES (:sb_sales_tax_states_nbr, :state, :state_tax_id, :sales_tax_percentage, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sales_tax_states(sb_sales_tax_states_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sales_tax_states SET state = :state, state_tax_id = :state_tax_id, sales_tax_percentage = :sales_tax_percentage, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sales_tax_states RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sales_tax_states^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SALES_TAX_STATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SALES_TAX_STATES_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SALES_TAX_STATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SALES_TAX_STATES_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SALES_TAX_STATES_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SALES_TAX_STATES_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SEC_CLIENTS ================== */
/*<progress>Converting SB_SEC_CLIENTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_CLIENTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_CLIENTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_CLIENTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_CLIENTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_CLIENTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_CLIENTS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sec_clients ON xb_sec_clients (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sec_clients_nbr INTEGER;
DECLARE VARIABLE cl_nbr INTEGER;
DECLARE VARIABLE sb_sec_groups_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sec_clients_nbr, cl_nbr, sb_sec_groups_nbr, sb_user_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sec_clients, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sec_clients_nbr, :cl_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sec_clients_nbr) THEN
      max_tbl_nbr = sb_sec_clients_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sec_clients WHERE sb_sec_clients_nbr = :sb_sec_clients_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sec_clients (sb_sec_clients_nbr, cl_nbr, sb_sec_groups_nbr, sb_user_nbr, effective_date, effective_until)
      VALUES (:sb_sec_clients_nbr, :cl_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sec_clients(sb_sec_clients_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sec_clients SET cl_nbr = :cl_nbr, sb_sec_groups_nbr = :sb_sec_groups_nbr, sb_user_nbr = :sb_user_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sec_clients RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sec_clients^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_CLIENTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_CLIENTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_CLIENTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_CLIENTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_CLIENTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_CLIENTS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SEC_GROUPS ================== */
/*<progress>Converting SB_SEC_GROUPS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_GROUPS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUPS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUPS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUPS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sec_groups ON xb_sec_groups (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sec_groups_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sec_groups_nbr, name, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sec_groups, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sec_groups_nbr, :name, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sec_groups_nbr) THEN
      max_tbl_nbr = sb_sec_groups_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sec_groups WHERE sb_sec_groups_nbr = :sb_sec_groups_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sec_groups (sb_sec_groups_nbr, name, effective_date, effective_until)
      VALUES (:sb_sec_groups_nbr, :name, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sec_groups(sb_sec_groups_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sec_groups SET name = :name, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sec_groups RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sec_groups^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_GROUPS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUPS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUPS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUPS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUPS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SEC_GROUP_MEMBERS ================== */
/*<progress>Converting SB_SEC_GROUP_MEMBERS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_GROUP_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_GROUP_MEMBERS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUP_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUP_MEMBERS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUP_MEMBERS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUP_MEMBERS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sec_group_members ON xb_sec_group_members (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sec_group_members_nbr INTEGER;
DECLARE VARIABLE sb_sec_groups_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sec_group_members_nbr, sb_sec_groups_nbr, sb_user_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sec_group_members, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sec_group_members_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sec_group_members_nbr) THEN
      max_tbl_nbr = sb_sec_group_members_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sec_group_members WHERE sb_sec_group_members_nbr = :sb_sec_group_members_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sec_group_members (sb_sec_group_members_nbr, sb_sec_groups_nbr, sb_user_nbr, effective_date, effective_until)
      VALUES (:sb_sec_group_members_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sec_group_members(sb_sec_group_members_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sec_group_members SET sb_sec_groups_nbr = :sb_sec_groups_nbr, sb_user_nbr = :sb_user_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sec_group_members RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sec_group_members^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_GROUP_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_GROUP_MEMBERS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUP_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUP_MEMBERS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_GROUP_MEMBERS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_GROUP_MEMBERS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SEC_RIGHTS ================== */
/*<progress>Converting SB_SEC_RIGHTS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_RIGHTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_RIGHTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_RIGHTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_RIGHTS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_RIGHTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_RIGHTS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sec_rights ON xb_sec_rights (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sec_rights_nbr INTEGER;
DECLARE VARIABLE sb_sec_groups_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE tag VARCHAR(128);
DECLARE VARIABLE context VARCHAR(128);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sec_rights_nbr, sb_sec_groups_nbr, sb_user_nbr, tag, context, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sec_rights, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sec_rights_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :tag, :context, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sec_rights_nbr) THEN
      max_tbl_nbr = sb_sec_rights_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sec_rights WHERE sb_sec_rights_nbr = :sb_sec_rights_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sec_rights (sb_sec_rights_nbr, sb_sec_groups_nbr, sb_user_nbr, tag, context, effective_date, effective_until)
      VALUES (:sb_sec_rights_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :tag, :context, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sec_rights(sb_sec_rights_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sec_rights SET sb_sec_groups_nbr = :sb_sec_groups_nbr, sb_user_nbr = :sb_user_nbr, tag = :tag, context = :context, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sec_rights RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sec_rights^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_RIGHTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_RIGHTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_RIGHTS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_RIGHTS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_RIGHTS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_RIGHTS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SEC_ROW_FILTERS ================== */
/*<progress>Converting SB_SEC_ROW_FILTERS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_ROW_FILTERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_ROW_FILTERS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_ROW_FILTERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_ROW_FILTERS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_ROW_FILTERS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_ROW_FILTERS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sec_row_filters ON xb_sec_row_filters (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sec_row_filters_nbr INTEGER;
DECLARE VARIABLE sb_sec_groups_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE database_type CHAR(1);
DECLARE VARIABLE table_name VARCHAR(40);
DECLARE VARIABLE filter_type CHAR(1);
DECLARE VARIABLE custom_expr VARCHAR(255);
DECLARE VARIABLE cl_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sec_row_filters_nbr, sb_sec_groups_nbr, sb_user_nbr, database_type, table_name, filter_type, custom_expr, cl_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sec_row_filters, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sec_row_filters_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :database_type, :table_name, :filter_type, :custom_expr, :cl_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sec_row_filters_nbr) THEN
      max_tbl_nbr = sb_sec_row_filters_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sec_row_filters WHERE sb_sec_row_filters_nbr = :sb_sec_row_filters_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sec_row_filters (sb_sec_row_filters_nbr, sb_sec_groups_nbr, sb_user_nbr, database_type, table_name, filter_type, custom_expr, cl_nbr, effective_date, effective_until)
      VALUES (:sb_sec_row_filters_nbr, :sb_sec_groups_nbr, :sb_user_nbr, :database_type, :table_name, :filter_type, :custom_expr, :cl_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sec_row_filters(sb_sec_row_filters_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sec_row_filters SET sb_sec_groups_nbr = :sb_sec_groups_nbr, sb_user_nbr = :sb_user_nbr, database_type = :database_type, table_name = :table_name, filter_type = :filter_type, custom_expr = :custom_expr, cl_nbr = :cl_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sec_row_filters RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sec_row_filters^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_ROW_FILTERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_ROW_FILTERS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_ROW_FILTERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_ROW_FILTERS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_ROW_FILTERS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_ROW_FILTERS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SEC_TEMPLATES ================== */
/*<progress>Converting SB_SEC_TEMPLATES</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_TEMPLATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_TEMPLATES_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_TEMPLATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_TEMPLATES_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_TEMPLATES_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_TEMPLATES_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_sec_templates ON xb_sec_templates (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_sec_templates_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE template BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_sec_templates_nbr, name, template, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_sec_templates, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_sec_templates_nbr, :name, :template, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_sec_templates_nbr) THEN
      max_tbl_nbr = sb_sec_templates_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_sec_templates WHERE sb_sec_templates_nbr = :sb_sec_templates_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_sec_templates (sb_sec_templates_nbr, name, template, effective_date, effective_until)
      VALUES (:sb_sec_templates_nbr, :name, :template, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_sec_templates(sb_sec_templates_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_sec_templates SET name = :name, template = :template, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_sec_templates RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_sec_templates^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SEC_TEMPLATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SEC_TEMPLATES_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_TEMPLATES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_TEMPLATES_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SEC_TEMPLATES_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SEC_TEMPLATES_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SERVICES ================== */
/*<progress>Converting SB_SERVICES</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SERVICES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SERVICES_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_services ON xb_services (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_services_nbr INTEGER;
DECLARE VARIABLE service_name VARCHAR(40);
DECLARE VARIABLE frequency CHAR(1);
DECLARE VARIABLE month_number VARCHAR(2);
DECLARE VARIABLE based_on_type CHAR(1);
DECLARE VARIABLE sb_reports_nbr INTEGER;
DECLARE VARIABLE commission CHAR(1);
DECLARE VARIABLE sales_taxable CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE product_code VARCHAR(6);
DECLARE VARIABLE week_number CHAR(1);
DECLARE VARIABLE service_type CHAR(1);
DECLARE VARIABLE minimum_amount NUMERIC(18,6);
DECLARE VARIABLE maximum_amount NUMERIC(18,6);
DECLARE VARIABLE sb_delivery_method_nbr INTEGER;
DECLARE VARIABLE tax_type CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_services_nbr, service_name, frequency, month_number, based_on_type, sb_reports_nbr, commission, sales_taxable, filler, product_code, week_number, service_type, minimum_amount, maximum_amount, sb_delivery_method_nbr, tax_type, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_services, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_services_nbr, :service_name, :frequency, :month_number, :based_on_type, :sb_reports_nbr, :commission, :sales_taxable, :filler, :product_code, :week_number, :service_type, :minimum_amount, :maximum_amount, :sb_delivery_method_nbr, :tax_type, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_services_nbr) THEN
      max_tbl_nbr = sb_services_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_services WHERE sb_services_nbr = :sb_services_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_services (sb_services_nbr, service_name, frequency, month_number, based_on_type, sb_reports_nbr, commission, sales_taxable, filler, product_code, week_number, service_type, minimum_amount, maximum_amount, sb_delivery_method_nbr, tax_type, effective_date, effective_until)
      VALUES (:sb_services_nbr, :service_name, :frequency, :month_number, :based_on_type, :sb_reports_nbr, :commission, :sales_taxable, :filler, :product_code, :week_number, :service_type, :minimum_amount, :maximum_amount, :sb_delivery_method_nbr, :tax_type, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_services(sb_services_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_services SET service_name = :service_name, frequency = :frequency, month_number = :month_number, based_on_type = :based_on_type, sb_reports_nbr = :sb_reports_nbr, commission = :commission, sales_taxable = :sales_taxable, filler = :filler, product_code = :product_code, week_number = :week_number, service_type = :service_type, minimum_amount = :minimum_amount, maximum_amount = :maximum_amount, sb_delivery_method_nbr = :sb_delivery_method_nbr, tax_type = :tax_type, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_services RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_services^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SERVICES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SERVICES_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_SERVICES_CALCULATIONS ================== */
/*<progress>Converting SB_SERVICES_CALCULATIONS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SERVICES_CALCULATION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SERVICES_CALCULATION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_CALCULATION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_CALCULATION_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_CALCULATION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_CALCULATION_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_services_calculations ON xb_services_calculations (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_services_calculations_nbr INTEGER;
DECLARE VARIABLE sb_services_nbr INTEGER;
DECLARE VARIABLE next_min_quantity_begin_date TIMESTAMP;
DECLARE VARIABLE next_max_quantity_begin_date TIMESTAMP;
DECLARE VARIABLE next_per_item_begin_date TIMESTAMP;
DECLARE VARIABLE next_flat_amount_begin_date TIMESTAMP;
DECLARE VARIABLE minimum_quantity NUMERIC(18,6);
DECLARE VARIABLE next_minimum_quantity NUMERIC(18,6);
DECLARE VARIABLE maximum_quantity NUMERIC(18,6);
DECLARE VARIABLE next_maximum_quantity NUMERIC(18,6);
DECLARE VARIABLE per_item_rate NUMERIC(18,6);
DECLARE VARIABLE next_per_item_rate NUMERIC(18,6);
DECLARE VARIABLE flat_amount NUMERIC(18,6);
DECLARE VARIABLE next_flat_amount NUMERIC(18,6);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_services_calculations_nbr, sb_services_nbr, next_min_quantity_begin_date, next_max_quantity_begin_date, next_per_item_begin_date, next_flat_amount_begin_date, minimum_quantity, next_minimum_quantity, maximum_quantity, next_maximum_quantity, per_item_rate, next_per_item_rate, flat_amount, next_flat_amount, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_services_calculations, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_services_calculations_nbr, :sb_services_nbr, :next_min_quantity_begin_date, :next_max_quantity_begin_date, :next_per_item_begin_date, :next_flat_amount_begin_date, :minimum_quantity, :next_minimum_quantity, :maximum_quantity, :next_maximum_quantity, :per_item_rate, :next_per_item_rate, :flat_amount, :next_flat_amount, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_services_calculations_nbr) THEN
      max_tbl_nbr = sb_services_calculations_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_services_calculations WHERE sb_services_calculations_nbr = :sb_services_calculations_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_services_calculations (sb_services_calculations_nbr, sb_services_nbr, next_min_quantity_begin_date, next_max_quantity_begin_date, next_per_item_begin_date, next_flat_amount_begin_date, minimum_quantity, next_minimum_quantity, maximum_quantity, next_maximum_quantity, per_item_rate, next_per_item_rate, flat_amount, next_flat_amount, effective_date, effective_until)
      VALUES (:sb_services_calculations_nbr, :sb_services_nbr, :next_min_quantity_begin_date, :next_max_quantity_begin_date, :next_per_item_begin_date, :next_flat_amount_begin_date, :minimum_quantity, :next_minimum_quantity, :maximum_quantity, :next_maximum_quantity, :per_item_rate, :next_per_item_rate, :flat_amount, :next_flat_amount, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_services_calculations(sb_services_calculations_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_services_calculations SET sb_services_nbr = :sb_services_nbr, next_min_quantity_begin_date = :next_min_quantity_begin_date, next_max_quantity_begin_date = :next_max_quantity_begin_date, next_per_item_begin_date = :next_per_item_begin_date, next_flat_amount_begin_date = :next_flat_amount_begin_date, minimum_quantity = :minimum_quantity, next_minimum_quantity = :next_minimum_quantity, maximum_quantity = :maximum_quantity, next_maximum_quantity = :next_maximum_quantity, per_item_rate = :per_item_rate, next_per_item_rate = :next_per_item_rate, flat_amount = :flat_amount, next_flat_amount = :next_flat_amount, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_services_calculations RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_services_calculations^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_SERVICES_CALCULATION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_SERVICES_CALCULATION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_CALCULATION_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_CALCULATION_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_SERVICES_CALCULATION_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_SERVICES_CALCULATION_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_TASK ================== */
/*<progress>Converting SB_TASK</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TASK_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TASK_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TASK_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TASK_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TASK_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TASK_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_task ON xb_task (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_task_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE schedule VARCHAR(255);
DECLARE VARIABLE description VARCHAR(255);
DECLARE VARIABLE task BLOB;
DECLARE VARIABLE last_run TIMESTAMP;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_task_nbr, sb_user_nbr, schedule, description, task, last_run, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_task, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_task_nbr, :sb_user_nbr, :schedule, :description, :task, :last_run, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_task_nbr) THEN
      max_tbl_nbr = sb_task_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_task WHERE sb_task_nbr = :sb_task_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_task (sb_task_nbr, sb_user_nbr, schedule, description, task, last_run, effective_date, effective_until)
      VALUES (:sb_task_nbr, :sb_user_nbr, :schedule, :description, :task, :last_run, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_task(sb_task_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_task SET sb_user_nbr = :sb_user_nbr, schedule = :schedule, description = :description, task = :task, last_run = :last_run, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_task RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_task^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TASK_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TASK_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TASK_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TASK_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TASK_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TASK_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_TAX_PAYMENT ================== */
/*<progress>Converting SB_TAX_PAYMENT</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TAX_PAYMENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TAX_PAYMENT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TAX_PAYMENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TAX_PAYMENT_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TAX_PAYMENT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TAX_PAYMENT_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_tax_payment ON xb_tax_payment (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_tax_payment_nbr INTEGER;
DECLARE VARIABLE description VARCHAR(80);
DECLARE VARIABLE status CHAR(1);
DECLARE VARIABLE status_date TIMESTAMP;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_tax_payment_nbr, description, status, status_date, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_tax_payment, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_tax_payment_nbr, :description, :status, :status_date, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_tax_payment_nbr) THEN
      max_tbl_nbr = sb_tax_payment_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_tax_payment WHERE sb_tax_payment_nbr = :sb_tax_payment_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_tax_payment (sb_tax_payment_nbr, description, status, status_date, effective_date, effective_until)
      VALUES (:sb_tax_payment_nbr, :description, :status, :status_date, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_tax_payment(sb_tax_payment_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_tax_payment SET description = :description, status = :status, status_date = :status_date, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_tax_payment RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_tax_payment^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TAX_PAYMENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TAX_PAYMENT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TAX_PAYMENT_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TAX_PAYMENT_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TAX_PAYMENT_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TAX_PAYMENT_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_TEAM ================== */
/*<progress>Converting SB_TEAM</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TEAM_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TEAM_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_team ON xb_team (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_team_nbr INTEGER;
DECLARE VARIABLE team_description VARCHAR(40);
DECLARE VARIABLE cr_category CHAR(1);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_team_nbr, team_description, cr_category, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_team, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_team_nbr, :team_description, :cr_category, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_team_nbr) THEN
      max_tbl_nbr = sb_team_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_team WHERE sb_team_nbr = :sb_team_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_team (sb_team_nbr, team_description, cr_category, effective_date, effective_until)
      VALUES (:sb_team_nbr, :team_description, :cr_category, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_team(sb_team_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_team SET team_description = :team_description, cr_category = :cr_category, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_team RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_team^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TEAM_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TEAM_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_TEAM_MEMBERS ================== */
/*<progress>Converting SB_TEAM_MEMBERS</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TEAM_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TEAM_MEMBERS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_MEMBERS_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_MEMBERS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_MEMBERS_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_team_members ON xb_team_members (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_team_members_nbr INTEGER;
DECLARE VARIABLE sb_team_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_team_members_nbr, sb_team_nbr, sb_user_nbr, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_team_members, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_team_members_nbr, :sb_team_nbr, :sb_user_nbr, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_team_members_nbr) THEN
      max_tbl_nbr = sb_team_members_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_team_members WHERE sb_team_members_nbr = :sb_team_members_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_team_members (sb_team_members_nbr, sb_team_nbr, sb_user_nbr, effective_date, effective_until)
      VALUES (:sb_team_members_nbr, :sb_team_nbr, :sb_user_nbr, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_team_members(sb_team_members_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_team_members SET sb_team_nbr = :sb_team_nbr, sb_user_nbr = :sb_user_nbr, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_team_members RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_team_members^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_TEAM_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_TEAM_MEMBERS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_MEMBERS_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_MEMBERS_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_TEAM_MEMBERS_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_TEAM_MEMBERS_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_USER ================== */
/*<progress>Converting SB_USER</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_USER_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_USER_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_user ON xb_user (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE user_id VARCHAR(128);
DECLARE VARIABLE user_signature BLOB;
DECLARE VARIABLE last_name VARCHAR(30);
DECLARE VARIABLE first_name VARCHAR(20);
DECLARE VARIABLE middle_initial CHAR(1);
DECLARE VARIABLE active_user CHAR(1);
DECLARE VARIABLE department CHAR(1);
DECLARE VARIABLE password_change_date TIMESTAMP;
DECLARE VARIABLE security_level CHAR(1);
DECLARE VARIABLE user_update_options VARCHAR(512);
DECLARE VARIABLE user_functions VARCHAR(512);
DECLARE VARIABLE user_password VARCHAR(32);
DECLARE VARIABLE email_address VARCHAR(80);
DECLARE VARIABLE cl_nbr INTEGER;
DECLARE VARIABLE sb_accountant_nbr INTEGER;
DECLARE VARIABLE wrong_pswd_attempts INTEGER;
DECLARE VARIABLE links_data VARCHAR(128);
DECLARE VARIABLE login_question1 INTEGER;
DECLARE VARIABLE login_answer1 VARCHAR(80);
DECLARE VARIABLE login_question2 INTEGER;
DECLARE VARIABLE login_answer2 VARCHAR(80);
DECLARE VARIABLE login_question3 INTEGER;
DECLARE VARIABLE login_answer3 VARCHAR(80);
DECLARE VARIABLE hr_personnel CHAR(1);
DECLARE VARIABLE sec_question1 INTEGER;
DECLARE VARIABLE sec_answer1 VARCHAR(80);
DECLARE VARIABLE sec_question2 INTEGER;
DECLARE VARIABLE sec_answer2 VARCHAR(80);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_user_nbr, xb_user.user_id, user_signature, last_name, first_name, middle_initial, active_user, department, password_change_date, security_level, user_update_options, user_functions, user_password, email_address, cl_nbr, sb_accountant_nbr, wrong_pswd_attempts, links_data, login_question1, login_answer1, login_question2, login_answer2, login_question3, login_answer3, hr_personnel, sec_question1, sec_answer1, sec_question2, sec_answer2, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_user, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_user_nbr, :user_id, :user_signature, :last_name, :first_name, :middle_initial, :active_user, :department, :password_change_date, :security_level, :user_update_options, :user_functions, :user_password, :email_address, :cl_nbr, :sb_accountant_nbr, :wrong_pswd_attempts, :links_data, :login_question1, :login_answer1, :login_question2, :login_answer2, :login_question3, :login_answer3, :hr_personnel, :sec_question1, :sec_answer1, :sec_question2, :sec_answer2, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_user_nbr) THEN
      max_tbl_nbr = sb_user_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_user WHERE sb_user_nbr = :sb_user_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_user (sb_user_nbr, user_id, user_signature, last_name, first_name, middle_initial, active_user, department, password_change_date, security_level, user_update_options, user_functions, user_password, email_address, cl_nbr, sb_accountant_nbr, wrong_pswd_attempts, links_data, login_question1, login_answer1, login_question2, login_answer2, login_question3, login_answer3, hr_personnel, sec_question1, sec_answer1, sec_question2, sec_answer2, effective_date, effective_until)
      VALUES (:sb_user_nbr, :user_id, :user_signature, :last_name, :first_name, :middle_initial, :active_user, :department, :password_change_date, :security_level, :user_update_options, :user_functions, :user_password, :email_address, :cl_nbr, :sb_accountant_nbr, :wrong_pswd_attempts, :links_data, :login_question1, :login_answer1, :login_question2, :login_answer2, :login_question3, :login_answer3, :hr_personnel, :sec_question1, :sec_answer1, :sec_question2, :sec_answer2, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_user(sb_user_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_user SET user_id = :user_id, user_signature = :user_signature, last_name = :last_name, first_name = :first_name, middle_initial = :middle_initial, active_user = :active_user, department = :department, password_change_date = :password_change_date, security_level = :security_level, user_update_options = :user_update_options, user_functions = :user_functions, user_password = :user_password, email_address = :email_address, cl_nbr = :cl_nbr, sb_accountant_nbr = :sb_accountant_nbr, wrong_pswd_attempts = :wrong_pswd_attempts, links_data = :links_data, login_question1 = :login_question1, login_answer1 = :login_answer1, login_question2 = :login_question2, login_answer2 = :login_answer2, login_question3 = :login_question3, login_answer3 = :login_answer3, hr_personnel = :hr_personnel, sec_question1 = :sec_question1, sec_answer1 = :sec_answer1, sec_question2 = :sec_question2, sec_answer2 = :sec_answer2, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_user RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_user^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_USER_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_USER_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_USER_NOTICE ================== */
/*<progress>Converting SB_USER_NOTICE</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_USER_NOTICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_USER_NOTICE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_NOTICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_NOTICE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_NOTICE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_NOTICE_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_user_notice ON xb_user_notice (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_user_notice_nbr INTEGER;
DECLARE VARIABLE sb_user_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(128);
DECLARE VARIABLE notes BLOB;
DECLARE VARIABLE task BLOB;
DECLARE VARIABLE last_dismiss TIMESTAMP;
DECLARE VARIABLE next_reminder TIMESTAMP;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_user_notice_nbr, sb_user_nbr, name, notes, task, last_dismiss, next_reminder, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_user_notice, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_user_notice_nbr, :sb_user_nbr, :name, :notes, :task, :last_dismiss, :next_reminder, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_user_notice_nbr) THEN
      max_tbl_nbr = sb_user_notice_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_user_notice WHERE sb_user_notice_nbr = :sb_user_notice_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_user_notice (sb_user_notice_nbr, sb_user_nbr, name, notes, task, last_dismiss, next_reminder, effective_date, effective_until)
      VALUES (:sb_user_notice_nbr, :sb_user_nbr, :name, :notes, :task, :last_dismiss, :next_reminder, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_user_notice(sb_user_notice_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_user_notice SET sb_user_nbr = :sb_user_nbr, name = :name, notes = :notes, task = :task, last_dismiss = :last_dismiss, next_reminder = :next_reminder, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_user_notice RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_user_notice^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_USER_NOTICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_USER_NOTICE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_NOTICE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_NOTICE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_USER_NOTICE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_USER_NOTICE_3 ACTIVE';
END^
COMMIT^


/* ================== Convert data of SB_STORAGE ================== */
/*<progress>Converting SB_STORAGE</progress>*/
EXECUTE BLOCK
AS
BEGIN
  /* Deactivate integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_STORAGE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_STORAGE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_STORAGE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_STORAGE_2 INACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_STORAGE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_STORAGE_3 INACTIVE';
END^
COMMIT^

CREATE INDEX I_xb_storage ON xb_storage (creation_date)^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_storage_nbr INTEGER;
DECLARE VARIABLE tag VARCHAR(20);
DECLARE VARIABLE storage_data BLOB;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE effective_until DATE;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE curr_effective_date DATE;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE max_tbl_nbr INTEGER;
BEGIN
  curr_effective_date = NULL;
  transaction_nbr = NULL;
  max_tbl_nbr = 0;

  FOR SELECT sb_storage_nbr, tag, storage_data, changed_by, creation_date, effective_date, active_record, t.nbr
  FROM xb_storage, ev_transaction t
  WHERE t.user_id = changed_by AND t.start_time = creation_date
  ORDER BY creation_date, effective_date
  INTO :sb_storage_nbr, :tag, :storage_data, :changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr
  DO
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);

    IF (max_tbl_nbr < sb_storage_nbr) THEN
      max_tbl_nbr = sb_storage_nbr;

    /* Apply record operation */
    rec_version = NULL;
    SELECT rec_version, effective_date FROM sb_storage WHERE sb_storage_nbr = :sb_storage_nbr INTO :rec_version, :curr_effective_date;

    IF (rec_version IS NULL) THEN
    BEGIN
      /* Add record version */
      IF (active_record <> 'N') THEN
      BEGIN
        effective_until = NULL;
      INSERT INTO sb_storage (sb_storage_nbr, tag, storage_data, effective_date, effective_until)
      VALUES (:sb_storage_nbr, :tag, :storage_data, :effective_date, :effective_until);
      END
    END
    ELSE
    BEGIN
      /* Update record version */
      IF (active_record = 'N') THEN
      BEGIN
        IF (rec_version IS NOT NULL) THEN
          EXECUTE PROCEDURE del_sb_storage(sb_storage_nbr);
      END
      ELSE
      BEGIN
        IF (curr_effective_date < effective_date) THEN
          effective_date = curr_effective_date;
        UPDATE sb_storage SET tag = :tag, storage_data = :storage_data, effective_date = :effective_date, effective_until = '12/31/9999'
        WHERE rec_version = :rec_version;
      END
    END
  END

  EXECUTE STATEMENT 'ALTER SEQUENCE g_sb_storage RESTART WITH ' || max_tbl_nbr;
END^
COMMIT^

DROP INDEX I_xb_storage^
COMMIT^

EXECUTE BLOCK
AS
BEGIN
  /* Restore integrity check and business logic */
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_AUD_SB_STORAGE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_AUD_SB_STORAGE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_STORAGE_2')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_STORAGE_2 ACTIVE';
  IF (EXISTS(SELECT 1 FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME = 'T_BIU_SB_STORAGE_3')) THEN
    EXECUTE STATEMENT 'ALTER TRIGGER T_BIU_SB_STORAGE_3 ACTIVE';
END^
COMMIT^


/*<progress>Creating indexes</progress>*/
EXECUTE BLOCK
AS
DECLARE VARIABLE stmt VARCHAR(1000);
BEGIN
  /* Activate Indexes */
  FOR SELECT 'ALTER INDEX '||rdb$index_name ||' ACTIVE;'
      FROM rdb$indices
      WHERE (rdb$system_flag IS NULL OR rdb$system_flag = 0) AND
            (rdb$unique_flag IS NULL OR rdb$unique_flag = 0) AND
            rdb$relation_name NOT STARTS 'EV_'
      ORDER BY rdb$foreign_key
  INTO :stmt
  DO
    EXECUTE STATEMENT :stmt;
END^
COMMIT^


SET TERM ; ^


/*<progress>Optimizing data</progress>*/
/* Delete empty transaction records */
DELETE FROM ev_transaction t WHERE NOT EXISTS (SELECT nbr FROM ev_table_change WHERE ev_transaction_nbr = t.nbr);
COMMIT;

/* Pack transaction records */
ALTER TRIGGER T_BU_EV_TRANSACTION_1 INACTIVE;
ALTER TRIGGER T_BU_EV_TABLE_CHANGE_1 INACTIVE;
COMMIT;

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE nbr INTEGER;
DECLARE VARIABLE start_time TIMESTAMP;
DECLARE VARIABLE commit_time TIMESTAMP;
DECLARE VARIABLE user_id INTEGER;
DECLARE VARIABLE last_nbr INTEGER;
DECLARE VARIABLE last_start_time TIMESTAMP;
DECLARE VARIABLE last_commit_time TIMESTAMP;
DECLARE VARIABLE last_user_id INTEGER;
BEGIN
  last_start_time = NULL;
  last_commit_time = NULL;
  last_user_id = NULL;

  FOR SELECT nbr, start_time, commit_time, user_id
      FROM ev_transaction
      ORDER BY start_time, nbr
      INTO :nbr, :start_time, :commit_time, :user_id
  DO
  BEGIN
    IF ((user_id IS DISTINCT FROM last_user_id) OR (commit_time - last_start_time > 0.003472222)) THEN /* five minutes threshold */
    BEGIN
      IF (last_commit_time IS NOT NULL) THEN
        UPDATE ev_transaction SET commit_time = :last_commit_time WHERE nbr = :last_nbr;

      last_nbr = nbr;
      last_start_time = start_time;
      last_commit_time = NULL;
      last_user_id = user_id;
    END
    ELSE
    BEGIN
      UPDATE ev_table_change SET ev_transaction_nbr = :last_nbr WHERE ev_transaction_nbr = :nbr;
      DELETE FROM ev_transaction WHERE nbr = :nbr;
      last_commit_time = commit_time;
    END
  END

  IF (last_commit_time IS NOT NULL) THEN
    UPDATE ev_transaction SET commit_time = :last_commit_time WHERE nbr = :last_nbr;
END^

COMMIT^

SET TERM ; ^

ALTER TRIGGER T_BU_EV_TRANSACTION_1 ACTIVE;
ALTER TRIGGER T_BU_EV_TABLE_CHANGE_1 ACTIVE;
COMMIT;


/* Drop old tables */
DROP TABLE xb;
DROP TABLE xb_accountant;
DROP TABLE xb_agency;
DROP TABLE xb_agency_reports;
DROP TABLE xb_banks;
DROP TABLE xb_bank_accounts;
DROP TABLE xb_blob;
DROP TABLE xb_delivery_company;
DROP TABLE xb_delivery_company_svcs;
DROP TABLE xb_delivery_method;
DROP TABLE xb_delivery_service;
DROP TABLE xb_delivery_service_opt;
DROP TABLE xb_enlist_groups;
DROP TABLE xb_global_agency_contacts;
DROP TABLE xb_holidays;
DROP TABLE xb_mail_box;
DROP TABLE xb_mail_box_content;
DROP TABLE xb_mail_box_option;
DROP TABLE xb_media_type;
DROP TABLE xb_media_type_option;
DROP TABLE xb_multiclient_reports;
DROP TABLE xb_option;
DROP TABLE xb_other_service;
DROP TABLE xb_paper_info;
DROP TABLE xb_queue_priority;
DROP TABLE xb_referrals;
DROP TABLE xb_reports;
DROP TABLE xb_report_writer_reports;
DROP TABLE xb_sales_tax_states;
DROP TABLE xb_sec_clients;
DROP TABLE xb_sec_groups;
DROP TABLE xb_sec_group_members;
DROP TABLE xb_sec_rights;
DROP TABLE xb_sec_row_filters;
DROP TABLE xb_sec_templates;
DROP TABLE xb_services;
DROP TABLE xb_services_calculations;
DROP TABLE xb_task;
DROP TABLE xb_tax_payment;
DROP TABLE xb_team;
DROP TABLE xb_team_members;
DROP TABLE xb_user;
DROP TABLE xb_user_notice;
DROP TABLE xb_storage;
COMMIT;

/* Finalize conversion */
SET TERM ^ ;


EXECUTE BLOCK
AS
DECLARE VARIABLE nbr INTEGER;
BEGIN
  nbr = gen_id(cl_gen, 0);
  EXECUTE STATEMENT 'ALTER SEQUENCE g_cl RESTART WITH ' || nbr;

  nbr = gen_id(NEXT_INVOICE_NUMBER_GEN, 0);
  EXECUTE STATEMENT 'ALTER SEQUENCE G_NEXT_INVOICE_NUMBER RESTART WITH ' || nbr;
END^
COMMIT^


SET TERM ; ^

DROP SEQUENCE CL_GEN;
DROP SEQUENCE NEXT_INVOICE_NUMBER_GEN;
COMMIT;


SET TERM ^;

EXECUTE BLOCK
AS
BEGIN
  /* Pack duplicate versions */
  EXECUTE PROCEDURE do_after_start_transaction(0, NULL);
  EXECUTE PROCEDURE pack_all;
  EXECUTE PROCEDURE do_before_commit_transaction(NULL);

  WHEN ANY DO
  BEGIN
  END
END^
COMMIT^

EXECUTE PROCEDURE UPDATE_INDEX_STATISTICS^
COMMIT^

SET TERM ;^

