/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.12';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


CREATE OR ALTER PROCEDURE pack_cl_person(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE social_security_number VARCHAR(11);
DECLARE VARIABLE p_social_security_number VARCHAR(11);
DECLARE VARIABLE ein_or_social_security_number CHAR(1);
DECLARE VARIABLE p_ein_or_social_security_number CHAR(1);
DECLARE VARIABLE first_name VARCHAR(20);
DECLARE VARIABLE p_first_name VARCHAR(20);
DECLARE VARIABLE middle_initial CHAR(1);
DECLARE VARIABLE p_middle_initial CHAR(1);
DECLARE VARIABLE last_name VARCHAR(30);
DECLARE VARIABLE p_last_name VARCHAR(30);
DECLARE VARIABLE w2_first_name VARCHAR(20);
DECLARE VARIABLE p_w2_first_name VARCHAR(20);
DECLARE VARIABLE w2_middle_name VARCHAR(20);
DECLARE VARIABLE p_w2_middle_name VARCHAR(20);
DECLARE VARIABLE w2_last_name VARCHAR(30);
DECLARE VARIABLE p_w2_last_name VARCHAR(30);
DECLARE VARIABLE w2_name_suffix VARCHAR(10);
DECLARE VARIABLE p_w2_name_suffix VARCHAR(10);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_person_nbr , social_security_number, ein_or_social_security_number, first_name, middle_initial, last_name, w2_first_name, w2_middle_name, w2_last_name, w2_name_suffix
        FROM cl_person
        ORDER BY cl_person_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_person_nbr, :social_security_number, :ein_or_social_security_number, :first_name, :middle_initial, :last_name, :w2_first_name, :w2_middle_name, :w2_last_name, :w2_name_suffix
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_person_nbr) OR (effective_date = '1/1/1900') OR (p_social_security_number IS DISTINCT FROM social_security_number) OR (p_ein_or_social_security_number IS DISTINCT FROM ein_or_social_security_number) OR (p_first_name IS DISTINCT FROM first_name) OR (p_middle_initial IS DISTINCT FROM middle_initial) OR (p_last_name IS DISTINCT FROM last_name) OR (p_w2_first_name IS DISTINCT FROM w2_first_name) OR (p_w2_middle_name IS DISTINCT FROM w2_middle_name) OR (p_w2_last_name IS DISTINCT FROM w2_last_name) OR (p_w2_name_suffix IS DISTINCT FROM w2_name_suffix)) THEN
      BEGIN
        curr_nbr = cl_person_nbr;
        p_social_security_number = social_security_number;
        p_ein_or_social_security_number = ein_or_social_security_number;
        p_first_name = first_name;
        p_middle_initial = middle_initial;
        p_last_name = last_name;
        p_w2_first_name = w2_first_name;
        p_w2_middle_name = w2_middle_name;
        p_w2_last_name = w2_last_name;
        p_w2_name_suffix = w2_name_suffix;
      END
      ELSE
        DELETE FROM cl_person WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, social_security_number, ein_or_social_security_number, first_name, middle_initial, last_name, w2_first_name, w2_middle_name, w2_last_name, w2_name_suffix
        FROM cl_person
        WHERE cl_person_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :social_security_number, :ein_or_social_security_number, :first_name, :middle_initial, :last_name, :w2_first_name, :w2_middle_name, :w2_last_name, :w2_name_suffix
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_social_security_number IS DISTINCT FROM social_security_number) OR (p_ein_or_social_security_number IS DISTINCT FROM ein_or_social_security_number) OR (p_first_name IS DISTINCT FROM first_name) OR (p_middle_initial IS DISTINCT FROM middle_initial) OR (p_last_name IS DISTINCT FROM last_name) OR (p_w2_first_name IS DISTINCT FROM w2_first_name) OR (p_w2_middle_name IS DISTINCT FROM w2_middle_name) OR (p_w2_last_name IS DISTINCT FROM w2_last_name) OR (p_w2_name_suffix IS DISTINCT FROM w2_name_suffix)) THEN
      BEGIN
        p_social_security_number = social_security_number;
        p_ein_or_social_security_number = ein_or_social_security_number;
        p_first_name = first_name;
        p_middle_initial = middle_initial;
        p_last_name = last_name;
        p_w2_first_name = w2_first_name;
        p_w2_middle_name = w2_middle_name;
        p_w2_last_name = w2_last_name;
        p_w2_name_suffix = w2_name_suffix;
      END
      ELSE
        DELETE FROM cl_person WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL_PERSON TO EUSER
^

CREATE OR ALTER PROCEDURE pack_ee(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE p_cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE p_co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE p_co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE p_co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE p_co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE p_new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE p_current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE p_co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE p_tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE p_w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE p_w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE p_w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE p_w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE p_w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE p_w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE p_home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE p_exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE p_exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE p_base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE p_company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE p_distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE p_tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE p_total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE p_pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE p_makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE p_salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE p_highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE p_corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE p_co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE p_co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE p_healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_credit_override NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_nbr , cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override
        FROM ee
        ORDER BY ee_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_nbr, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_nbr) OR (effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override)) THEN
      BEGIN
        curr_nbr = ee_nbr;
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override
        FROM ee
        WHERE ee_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_cl_person_nbr IS DISTINCT FROM cl_person_nbr) OR (p_co_division_nbr IS DISTINCT FROM co_division_nbr) OR (p_co_branch_nbr IS DISTINCT FROM co_branch_nbr) OR (p_co_department_nbr IS DISTINCT FROM co_department_nbr) OR (p_co_team_nbr IS DISTINCT FROM co_team_nbr) OR (p_new_hire_report_sent IS DISTINCT FROM new_hire_report_sent) OR (p_current_termination_code IS DISTINCT FROM current_termination_code) OR (p_co_hr_positions_nbr IS DISTINCT FROM co_hr_positions_nbr) OR (p_tipped_directly IS DISTINCT FROM tipped_directly) OR (p_w2_type IS DISTINCT FROM w2_type) OR (p_w2_pension IS DISTINCT FROM w2_pension) OR (p_w2_deferred_comp IS DISTINCT FROM w2_deferred_comp) OR (p_w2_deceased IS DISTINCT FROM w2_deceased) OR (p_w2_statutory_employee IS DISTINCT FROM w2_statutory_employee) OR (p_w2_legal_rep IS DISTINCT FROM w2_legal_rep) OR (p_home_tax_ee_states_nbr IS DISTINCT FROM home_tax_ee_states_nbr) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_exclude_ee_fed IS DISTINCT FROM exempt_exclude_ee_fed) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employer_fui IS DISTINCT FROM exempt_employer_fui) OR (p_base_returns_on_this_ee IS DISTINCT FROM base_returns_on_this_ee) OR (p_company_or_individual_name IS DISTINCT FROM company_or_individual_name) OR (p_distribution_code_1099r IS DISTINCT FROM distribution_code_1099r) OR (p_tax_amt_determined_1099r IS DISTINCT FROM tax_amt_determined_1099r) OR (p_total_distribution_1099r IS DISTINCT FROM total_distribution_1099r) OR (p_pension_plan_1099r IS DISTINCT FROM pension_plan_1099r) OR (p_makeup_fica_on_cleanup_pr IS DISTINCT FROM makeup_fica_on_cleanup_pr) OR (p_salary_amount IS DISTINCT FROM salary_amount) OR (p_highly_compensated IS DISTINCT FROM highly_compensated) OR (p_corporate_officer IS DISTINCT FROM corporate_officer) OR (p_co_jobs_nbr IS DISTINCT FROM co_jobs_nbr) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_healthcare_coverage IS DISTINCT FROM healthcare_coverage) OR (p_fui_rate_credit_override IS DISTINCT FROM fui_rate_credit_override)) THEN
      BEGIN
        p_cl_person_nbr = cl_person_nbr;
        p_co_division_nbr = co_division_nbr;
        p_co_branch_nbr = co_branch_nbr;
        p_co_department_nbr = co_department_nbr;
        p_co_team_nbr = co_team_nbr;
        p_new_hire_report_sent = new_hire_report_sent;
        p_current_termination_code = current_termination_code;
        p_co_hr_positions_nbr = co_hr_positions_nbr;
        p_tipped_directly = tipped_directly;
        p_w2_type = w2_type;
        p_w2_pension = w2_pension;
        p_w2_deferred_comp = w2_deferred_comp;
        p_w2_deceased = w2_deceased;
        p_w2_statutory_employee = w2_statutory_employee;
        p_w2_legal_rep = w2_legal_rep;
        p_home_tax_ee_states_nbr = home_tax_ee_states_nbr;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_exclude_ee_fed = exempt_exclude_ee_fed;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employer_fui = exempt_employer_fui;
        p_base_returns_on_this_ee = base_returns_on_this_ee;
        p_company_or_individual_name = company_or_individual_name;
        p_distribution_code_1099r = distribution_code_1099r;
        p_tax_amt_determined_1099r = tax_amt_determined_1099r;
        p_total_distribution_1099r = total_distribution_1099r;
        p_pension_plan_1099r = pension_plan_1099r;
        p_makeup_fica_on_cleanup_pr = makeup_fica_on_cleanup_pr;
        p_salary_amount = salary_amount;
        p_highly_compensated = highly_compensated;
        p_corporate_officer = corporate_officer;
        p_co_jobs_nbr = co_jobs_nbr;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_healthcare_coverage = healthcare_coverage;
        p_fui_rate_credit_override = fui_rate_credit_override;
      END
      ELSE
        DELETE FROM ee WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_EE TO EUSER
^

CREATE OR ALTER PROCEDURE pack_cl(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE reciprocate_sui CHAR(1);
DECLARE VARIABLE p_reciprocate_sui CHAR(1);
DECLARE VARIABLE termination_code CHAR(1);
DECLARE VARIABLE p_termination_code CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_nbr , reciprocate_sui, termination_code
        FROM cl
        ORDER BY cl_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_nbr, :reciprocate_sui, :termination_code
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_nbr) OR (effective_date = '1/1/1900') OR (p_reciprocate_sui IS DISTINCT FROM reciprocate_sui) OR (p_termination_code IS DISTINCT FROM termination_code)) THEN
      BEGIN
        curr_nbr = cl_nbr;
        p_reciprocate_sui = reciprocate_sui;
        p_termination_code = termination_code;
      END
      ELSE
        DELETE FROM cl WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, reciprocate_sui, termination_code
        FROM cl
        WHERE cl_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :reciprocate_sui, :termination_code
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_reciprocate_sui IS DISTINCT FROM reciprocate_sui) OR (p_termination_code IS DISTINCT FROM termination_code)) THEN
      BEGIN
        p_reciprocate_sui = reciprocate_sui;
        p_termination_code = termination_code;
      END
      ELSE
        DELETE FROM cl WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL TO EUSER
^

CREATE OR ALTER PROCEDURE pack_cl_3_party_sick_pay_admin(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_3_party_sick_pay_admin_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE w2 CHAR(1);
DECLARE VARIABLE p_w2 CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_3_party_sick_pay_admin_nbr , w2
        FROM cl_3_party_sick_pay_admin
        ORDER BY cl_3_party_sick_pay_admin_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_3_party_sick_pay_admin_nbr, :w2
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_3_party_sick_pay_admin_nbr) OR (effective_date = '1/1/1900') OR (p_w2 IS DISTINCT FROM w2)) THEN
      BEGIN
        curr_nbr = cl_3_party_sick_pay_admin_nbr;
        p_w2 = w2;
      END
      ELSE
        DELETE FROM cl_3_party_sick_pay_admin WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, w2
        FROM cl_3_party_sick_pay_admin
        WHERE cl_3_party_sick_pay_admin_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :w2
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_w2 IS DISTINCT FROM w2)) THEN
      BEGIN
        p_w2 = w2;
      END
      ELSE
        DELETE FROM cl_3_party_sick_pay_admin WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL_3_PARTY_SICK_PAY_ADMIN TO EUSER
^

CREATE OR ALTER PROCEDURE pack_cl_e_ds(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_e_ds_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE custom_e_d_code_number VARCHAR(20);
DECLARE VARIABLE p_custom_e_d_code_number VARCHAR(20);
DECLARE VARIABLE e_d_code_type CHAR(2);
DECLARE VARIABLE p_e_d_code_type CHAR(2);
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE p_description VARCHAR(40);
DECLARE VARIABLE ee_exempt_exclude_oasdi CHAR(1);
DECLARE VARIABLE p_ee_exempt_exclude_oasdi CHAR(1);
DECLARE VARIABLE ee_exempt_exclude_medicare CHAR(1);
DECLARE VARIABLE p_ee_exempt_exclude_medicare CHAR(1);
DECLARE VARIABLE ee_exempt_exclude_federal CHAR(1);
DECLARE VARIABLE p_ee_exempt_exclude_federal CHAR(1);
DECLARE VARIABLE ee_exempt_exclude_eic CHAR(1);
DECLARE VARIABLE p_ee_exempt_exclude_eic CHAR(1);
DECLARE VARIABLE er_exempt_exclude_oasdi CHAR(1);
DECLARE VARIABLE p_er_exempt_exclude_oasdi CHAR(1);
DECLARE VARIABLE er_exempt_exclude_medicare CHAR(1);
DECLARE VARIABLE p_er_exempt_exclude_medicare CHAR(1);
DECLARE VARIABLE er_exempt_exclude_fui CHAR(1);
DECLARE VARIABLE p_er_exempt_exclude_fui CHAR(1);
DECLARE VARIABLE w2_box VARCHAR(12);
DECLARE VARIABLE p_w2_box VARCHAR(12);
DECLARE VARIABLE cl_pension_nbr INTEGER;
DECLARE VARIABLE p_cl_pension_nbr INTEGER;
DECLARE VARIABLE cl_3_party_sick_pay_admin_nbr INTEGER;
DECLARE VARIABLE p_cl_3_party_sick_pay_admin_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_e_ds_nbr , custom_e_d_code_number, e_d_code_type, description, ee_exempt_exclude_oasdi, ee_exempt_exclude_medicare, ee_exempt_exclude_federal, ee_exempt_exclude_eic, er_exempt_exclude_oasdi, er_exempt_exclude_medicare, er_exempt_exclude_fui, w2_box, cl_pension_nbr, cl_3_party_sick_pay_admin_nbr
        FROM cl_e_ds
        ORDER BY cl_e_ds_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_e_ds_nbr, :custom_e_d_code_number, :e_d_code_type, :description, :ee_exempt_exclude_oasdi, :ee_exempt_exclude_medicare, :ee_exempt_exclude_federal, :ee_exempt_exclude_eic, :er_exempt_exclude_oasdi, :er_exempt_exclude_medicare, :er_exempt_exclude_fui, :w2_box, :cl_pension_nbr, :cl_3_party_sick_pay_admin_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_e_ds_nbr) OR (effective_date = '1/1/1900') OR (p_custom_e_d_code_number IS DISTINCT FROM custom_e_d_code_number) OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_description IS DISTINCT FROM description) OR (p_ee_exempt_exclude_oasdi IS DISTINCT FROM ee_exempt_exclude_oasdi) OR (p_ee_exempt_exclude_medicare IS DISTINCT FROM ee_exempt_exclude_medicare) OR (p_ee_exempt_exclude_federal IS DISTINCT FROM ee_exempt_exclude_federal) OR (p_ee_exempt_exclude_eic IS DISTINCT FROM ee_exempt_exclude_eic) OR (p_er_exempt_exclude_oasdi IS DISTINCT FROM er_exempt_exclude_oasdi) OR (p_er_exempt_exclude_medicare IS DISTINCT FROM er_exempt_exclude_medicare) OR (p_er_exempt_exclude_fui IS DISTINCT FROM er_exempt_exclude_fui) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_cl_pension_nbr IS DISTINCT FROM cl_pension_nbr) OR (p_cl_3_party_sick_pay_admin_nbr IS DISTINCT FROM cl_3_party_sick_pay_admin_nbr)) THEN
      BEGIN
        curr_nbr = cl_e_ds_nbr;
        p_custom_e_d_code_number = custom_e_d_code_number;
        p_e_d_code_type = e_d_code_type;
        p_description = description;
        p_ee_exempt_exclude_oasdi = ee_exempt_exclude_oasdi;
        p_ee_exempt_exclude_medicare = ee_exempt_exclude_medicare;
        p_ee_exempt_exclude_federal = ee_exempt_exclude_federal;
        p_ee_exempt_exclude_eic = ee_exempt_exclude_eic;
        p_er_exempt_exclude_oasdi = er_exempt_exclude_oasdi;
        p_er_exempt_exclude_medicare = er_exempt_exclude_medicare;
        p_er_exempt_exclude_fui = er_exempt_exclude_fui;
        p_w2_box = w2_box;
        p_cl_pension_nbr = cl_pension_nbr;
        p_cl_3_party_sick_pay_admin_nbr = cl_3_party_sick_pay_admin_nbr;
      END
      ELSE
        DELETE FROM cl_e_ds WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, custom_e_d_code_number, e_d_code_type, description, ee_exempt_exclude_oasdi, ee_exempt_exclude_medicare, ee_exempt_exclude_federal, ee_exempt_exclude_eic, er_exempt_exclude_oasdi, er_exempt_exclude_medicare, er_exempt_exclude_fui, w2_box, cl_pension_nbr, cl_3_party_sick_pay_admin_nbr
        FROM cl_e_ds
        WHERE cl_e_ds_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :custom_e_d_code_number, :e_d_code_type, :description, :ee_exempt_exclude_oasdi, :ee_exempt_exclude_medicare, :ee_exempt_exclude_federal, :ee_exempt_exclude_eic, :er_exempt_exclude_oasdi, :er_exempt_exclude_medicare, :er_exempt_exclude_fui, :w2_box, :cl_pension_nbr, :cl_3_party_sick_pay_admin_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_custom_e_d_code_number IS DISTINCT FROM custom_e_d_code_number) OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_description IS DISTINCT FROM description) OR (p_ee_exempt_exclude_oasdi IS DISTINCT FROM ee_exempt_exclude_oasdi) OR (p_ee_exempt_exclude_medicare IS DISTINCT FROM ee_exempt_exclude_medicare) OR (p_ee_exempt_exclude_federal IS DISTINCT FROM ee_exempt_exclude_federal) OR (p_ee_exempt_exclude_eic IS DISTINCT FROM ee_exempt_exclude_eic) OR (p_er_exempt_exclude_oasdi IS DISTINCT FROM er_exempt_exclude_oasdi) OR (p_er_exempt_exclude_medicare IS DISTINCT FROM er_exempt_exclude_medicare) OR (p_er_exempt_exclude_fui IS DISTINCT FROM er_exempt_exclude_fui) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_cl_pension_nbr IS DISTINCT FROM cl_pension_nbr) OR (p_cl_3_party_sick_pay_admin_nbr IS DISTINCT FROM cl_3_party_sick_pay_admin_nbr)) THEN
      BEGIN
        p_custom_e_d_code_number = custom_e_d_code_number;
        p_e_d_code_type = e_d_code_type;
        p_description = description;
        p_ee_exempt_exclude_oasdi = ee_exempt_exclude_oasdi;
        p_ee_exempt_exclude_medicare = ee_exempt_exclude_medicare;
        p_ee_exempt_exclude_federal = ee_exempt_exclude_federal;
        p_ee_exempt_exclude_eic = ee_exempt_exclude_eic;
        p_er_exempt_exclude_oasdi = er_exempt_exclude_oasdi;
        p_er_exempt_exclude_medicare = er_exempt_exclude_medicare;
        p_er_exempt_exclude_fui = er_exempt_exclude_fui;
        p_w2_box = w2_box;
        p_cl_pension_nbr = cl_pension_nbr;
        p_cl_3_party_sick_pay_admin_nbr = cl_3_party_sick_pay_admin_nbr;
      END
      ELSE
        DELETE FROM cl_e_ds WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL_E_DS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_cl_e_d_local_exmpt_excld(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_e_d_local_exmpt_excld_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE cl_e_ds_nbr INTEGER;
DECLARE VARIABLE p_cl_e_ds_nbr INTEGER;
DECLARE VARIABLE sy_locals_nbr INTEGER;
DECLARE VARIABLE p_sy_locals_nbr INTEGER;
DECLARE VARIABLE exempt_exclude CHAR(1);
DECLARE VARIABLE p_exempt_exclude CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_e_d_local_exmpt_excld_nbr , cl_e_ds_nbr, sy_locals_nbr, exempt_exclude
        FROM cl_e_d_local_exmpt_excld
        ORDER BY cl_e_d_local_exmpt_excld_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_e_d_local_exmpt_excld_nbr, :cl_e_ds_nbr, :sy_locals_nbr, :exempt_exclude
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_e_d_local_exmpt_excld_nbr) OR (effective_date = '1/1/1900') OR (p_cl_e_ds_nbr IS DISTINCT FROM cl_e_ds_nbr) OR (p_sy_locals_nbr IS DISTINCT FROM sy_locals_nbr) OR (p_exempt_exclude IS DISTINCT FROM exempt_exclude)) THEN
      BEGIN
        curr_nbr = cl_e_d_local_exmpt_excld_nbr;
        p_cl_e_ds_nbr = cl_e_ds_nbr;
        p_sy_locals_nbr = sy_locals_nbr;
        p_exempt_exclude = exempt_exclude;
      END
      ELSE
        DELETE FROM cl_e_d_local_exmpt_excld WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, cl_e_ds_nbr, sy_locals_nbr, exempt_exclude
        FROM cl_e_d_local_exmpt_excld
        WHERE cl_e_d_local_exmpt_excld_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :cl_e_ds_nbr, :sy_locals_nbr, :exempt_exclude
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_cl_e_ds_nbr IS DISTINCT FROM cl_e_ds_nbr) OR (p_sy_locals_nbr IS DISTINCT FROM sy_locals_nbr) OR (p_exempt_exclude IS DISTINCT FROM exempt_exclude)) THEN
      BEGIN
        p_cl_e_ds_nbr = cl_e_ds_nbr;
        p_sy_locals_nbr = sy_locals_nbr;
        p_exempt_exclude = exempt_exclude;
      END
      ELSE
        DELETE FROM cl_e_d_local_exmpt_excld WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL_E_D_LOCAL_EXMPT_EXCLD TO EUSER
^

CREATE OR ALTER PROCEDURE pack_cl_e_d_state_exmpt_excld(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_e_d_state_exmpt_excld_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE cl_e_ds_nbr INTEGER;
DECLARE VARIABLE p_cl_e_ds_nbr INTEGER;
DECLARE VARIABLE sy_states_nbr INTEGER;
DECLARE VARIABLE p_sy_states_nbr INTEGER;
DECLARE VARIABLE employee_exempt_exclude_state CHAR(1);
DECLARE VARIABLE p_employee_exempt_exclude_state CHAR(1);
DECLARE VARIABLE employee_exempt_exclude_sdi CHAR(1);
DECLARE VARIABLE p_employee_exempt_exclude_sdi CHAR(1);
DECLARE VARIABLE employee_exempt_exclude_sui CHAR(1);
DECLARE VARIABLE p_employee_exempt_exclude_sui CHAR(1);
DECLARE VARIABLE employee_state_or_type CHAR(1);
DECLARE VARIABLE p_employee_state_or_type CHAR(1);
DECLARE VARIABLE employer_exempt_exclude_sdi CHAR(1);
DECLARE VARIABLE p_employer_exempt_exclude_sdi CHAR(1);
DECLARE VARIABLE employer_exempt_exclude_sui CHAR(1);
DECLARE VARIABLE p_employer_exempt_exclude_sui CHAR(1);
DECLARE VARIABLE employee_state_or_value NUMERIC(18,6);
DECLARE VARIABLE p_employee_state_or_value NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_e_d_state_exmpt_excld_nbr , cl_e_ds_nbr, sy_states_nbr, employee_exempt_exclude_state, employee_exempt_exclude_sdi, employee_exempt_exclude_sui, employee_state_or_type, employer_exempt_exclude_sdi, employer_exempt_exclude_sui, employee_state_or_value
        FROM cl_e_d_state_exmpt_excld
        ORDER BY cl_e_d_state_exmpt_excld_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_e_d_state_exmpt_excld_nbr, :cl_e_ds_nbr, :sy_states_nbr, :employee_exempt_exclude_state, :employee_exempt_exclude_sdi, :employee_exempt_exclude_sui, :employee_state_or_type, :employer_exempt_exclude_sdi, :employer_exempt_exclude_sui, :employee_state_or_value
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_e_d_state_exmpt_excld_nbr) OR (effective_date = '1/1/1900') OR (p_cl_e_ds_nbr IS DISTINCT FROM cl_e_ds_nbr) OR (p_sy_states_nbr IS DISTINCT FROM sy_states_nbr) OR (p_employee_exempt_exclude_state IS DISTINCT FROM employee_exempt_exclude_state) OR (p_employee_exempt_exclude_sdi IS DISTINCT FROM employee_exempt_exclude_sdi) OR (p_employee_exempt_exclude_sui IS DISTINCT FROM employee_exempt_exclude_sui) OR (p_employee_state_or_type IS DISTINCT FROM employee_state_or_type) OR (p_employer_exempt_exclude_sdi IS DISTINCT FROM employer_exempt_exclude_sdi) OR (p_employer_exempt_exclude_sui IS DISTINCT FROM employer_exempt_exclude_sui) OR (p_employee_state_or_value IS DISTINCT FROM employee_state_or_value)) THEN
      BEGIN
        curr_nbr = cl_e_d_state_exmpt_excld_nbr;
        p_cl_e_ds_nbr = cl_e_ds_nbr;
        p_sy_states_nbr = sy_states_nbr;
        p_employee_exempt_exclude_state = employee_exempt_exclude_state;
        p_employee_exempt_exclude_sdi = employee_exempt_exclude_sdi;
        p_employee_exempt_exclude_sui = employee_exempt_exclude_sui;
        p_employee_state_or_type = employee_state_or_type;
        p_employer_exempt_exclude_sdi = employer_exempt_exclude_sdi;
        p_employer_exempt_exclude_sui = employer_exempt_exclude_sui;
        p_employee_state_or_value = employee_state_or_value;
      END
      ELSE
        DELETE FROM cl_e_d_state_exmpt_excld WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, cl_e_ds_nbr, sy_states_nbr, employee_exempt_exclude_state, employee_exempt_exclude_sdi, employee_exempt_exclude_sui, employee_state_or_type, employer_exempt_exclude_sdi, employer_exempt_exclude_sui, employee_state_or_value
        FROM cl_e_d_state_exmpt_excld
        WHERE cl_e_d_state_exmpt_excld_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :cl_e_ds_nbr, :sy_states_nbr, :employee_exempt_exclude_state, :employee_exempt_exclude_sdi, :employee_exempt_exclude_sui, :employee_state_or_type, :employer_exempt_exclude_sdi, :employer_exempt_exclude_sui, :employee_state_or_value
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_cl_e_ds_nbr IS DISTINCT FROM cl_e_ds_nbr) OR (p_sy_states_nbr IS DISTINCT FROM sy_states_nbr) OR (p_employee_exempt_exclude_state IS DISTINCT FROM employee_exempt_exclude_state) OR (p_employee_exempt_exclude_sdi IS DISTINCT FROM employee_exempt_exclude_sdi) OR (p_employee_exempt_exclude_sui IS DISTINCT FROM employee_exempt_exclude_sui) OR (p_employee_state_or_type IS DISTINCT FROM employee_state_or_type) OR (p_employer_exempt_exclude_sdi IS DISTINCT FROM employer_exempt_exclude_sdi) OR (p_employer_exempt_exclude_sui IS DISTINCT FROM employer_exempt_exclude_sui) OR (p_employee_state_or_value IS DISTINCT FROM employee_state_or_value)) THEN
      BEGIN
        p_cl_e_ds_nbr = cl_e_ds_nbr;
        p_sy_states_nbr = sy_states_nbr;
        p_employee_exempt_exclude_state = employee_exempt_exclude_state;
        p_employee_exempt_exclude_sdi = employee_exempt_exclude_sdi;
        p_employee_exempt_exclude_sui = employee_exempt_exclude_sui;
        p_employee_state_or_type = employee_state_or_type;
        p_employer_exempt_exclude_sdi = employer_exempt_exclude_sdi;
        p_employer_exempt_exclude_sui = employer_exempt_exclude_sui;
        p_employee_state_or_value = employee_state_or_value;
      END
      ELSE
        DELETE FROM cl_e_d_state_exmpt_excld WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL_E_D_STATE_EXMPT_EXCLD TO EUSER
^

CREATE OR ALTER PROCEDURE pack_cl_pension(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE cl_pension_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE mark_pension_on_w2 CHAR(1);
DECLARE VARIABLE p_mark_pension_on_w2 CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, cl_pension_nbr , mark_pension_on_w2
        FROM cl_pension
        ORDER BY cl_pension_nbr, effective_date
        INTO :rec_version, :effective_date, :cl_pension_nbr, :mark_pension_on_w2
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM cl_pension_nbr) OR (effective_date = '1/1/1900') OR (p_mark_pension_on_w2 IS DISTINCT FROM mark_pension_on_w2)) THEN
      BEGIN
        curr_nbr = cl_pension_nbr;
        p_mark_pension_on_w2 = mark_pension_on_w2;
      END
      ELSE
        DELETE FROM cl_pension WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, mark_pension_on_w2
        FROM cl_pension
        WHERE cl_pension_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :mark_pension_on_w2
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_mark_pension_on_w2 IS DISTINCT FROM mark_pension_on_w2)) THEN
      BEGIN
        p_mark_pension_on_w2 = mark_pension_on_w2;
      END
      ELSE
        DELETE FROM cl_pension WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CL_PENSION TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE p_name VARCHAR(40);
DECLARE VARIABLE dba VARCHAR(40);
DECLARE VARIABLE p_dba VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE p_address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE p_address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE p_city VARCHAR(20);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE p_state CHAR(2);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE p_zip_code VARCHAR(10);
DECLARE VARIABLE legal_name VARCHAR(40);
DECLARE VARIABLE p_legal_name VARCHAR(40);
DECLARE VARIABLE legal_address1 VARCHAR(30);
DECLARE VARIABLE p_legal_address1 VARCHAR(30);
DECLARE VARIABLE legal_address2 VARCHAR(30);
DECLARE VARIABLE p_legal_address2 VARCHAR(30);
DECLARE VARIABLE legal_city VARCHAR(20);
DECLARE VARIABLE p_legal_city VARCHAR(20);
DECLARE VARIABLE legal_state CHAR(2);
DECLARE VARIABLE p_legal_state CHAR(2);
DECLARE VARIABLE legal_zip_code VARCHAR(10);
DECLARE VARIABLE p_legal_zip_code VARCHAR(10);
DECLARE VARIABLE termination_date DATE;
DECLARE VARIABLE p_termination_date DATE;
DECLARE VARIABLE termination_code CHAR(1);
DECLARE VARIABLE p_termination_code CHAR(1);
DECLARE VARIABLE print_cpa CHAR(1);
DECLARE VARIABLE p_print_cpa CHAR(1);
DECLARE VARIABLE fein VARCHAR(9);
DECLARE VARIABLE p_fein VARCHAR(9);
DECLARE VARIABLE business_type CHAR(1);
DECLARE VARIABLE p_business_type CHAR(1);
DECLARE VARIABLE use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE p_use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE federal_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_federal_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE federal_tax_exempt_status CHAR(1);
DECLARE VARIABLE p_federal_tax_exempt_status CHAR(1);
DECLARE VARIABLE fed_tax_exempt_ee_oasdi CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_ee_oasdi CHAR(1);
DECLARE VARIABLE fed_tax_exempt_er_oasdi CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_er_oasdi CHAR(1);
DECLARE VARIABLE fed_tax_exempt_ee_medicare CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_ee_medicare CHAR(1);
DECLARE VARIABLE fed_tax_exempt_er_medicare CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_er_medicare CHAR(1);
DECLARE VARIABLE fui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_fui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE fui_tax_exempt CHAR(1);
DECLARE VARIABLE p_fui_tax_exempt CHAR(1);
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE p_co_workers_comp_nbr INTEGER;
DECLARE VARIABLE apply_misc_limit_to_1099 CHAR(1);
DECLARE VARIABLE p_apply_misc_limit_to_1099 CHAR(1);
DECLARE VARIABLE make_up_tax_deduct_shortfalls CHAR(1);
DECLARE VARIABLE p_make_up_tax_deduct_shortfalls CHAR(1);
DECLARE VARIABLE fed_945_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_fed_945_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE fui_rate_override NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_override NUMERIC(18,6);
DECLARE VARIABLE name_on_invoice CHAR(1);
DECLARE VARIABLE p_name_on_invoice CHAR(1);
DECLARE VARIABLE employer_type CHAR(1);
DECLARE VARIABLE p_employer_type CHAR(1);
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_nbr , name, dba, address1, address2, city, state, zip_code, legal_name, legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, termination_date, termination_code, print_cpa, fein, business_type, use_dba_on_tax_return, federal_tax_deposit_frequency, federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi, fed_tax_exempt_ee_medicare, fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt, co_workers_comp_nbr, apply_misc_limit_to_1099, make_up_tax_deduct_shortfalls, fed_945_tax_deposit_frequency, fui_rate_override, name_on_invoice, employer_type, co_locations_nbr
        FROM co
        ORDER BY co_nbr, effective_date
        INTO :rec_version, :effective_date, :co_nbr, :name, :dba, :address1, :address2, :city, :state, :zip_code, :legal_name, :legal_address1, :legal_address2, :legal_city, :legal_state, :legal_zip_code, :termination_date, :termination_code, :print_cpa, :fein, :business_type, :use_dba_on_tax_return, :federal_tax_deposit_frequency, :federal_tax_exempt_status, :fed_tax_exempt_ee_oasdi, :fed_tax_exempt_er_oasdi, :fed_tax_exempt_ee_medicare, :fed_tax_exempt_er_medicare, :fui_tax_deposit_frequency, :fui_tax_exempt, :co_workers_comp_nbr, :apply_misc_limit_to_1099, :make_up_tax_deduct_shortfalls, :fed_945_tax_deposit_frequency, :fui_rate_override, :name_on_invoice, :employer_type, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_nbr) OR (effective_date = '1/1/1900') OR (p_name IS DISTINCT FROM name) OR (p_dba IS DISTINCT FROM dba) OR (p_address1 IS DISTINCT FROM address1) OR (p_address2 IS DISTINCT FROM address2) OR (p_city IS DISTINCT FROM city) OR (p_state IS DISTINCT FROM state) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_legal_name IS DISTINCT FROM legal_name) OR (p_legal_address1 IS DISTINCT FROM legal_address1) OR (p_legal_address2 IS DISTINCT FROM legal_address2) OR (p_legal_city IS DISTINCT FROM legal_city) OR (p_legal_state IS DISTINCT FROM legal_state) OR (p_legal_zip_code IS DISTINCT FROM legal_zip_code) OR (p_termination_date IS DISTINCT FROM termination_date) OR (p_termination_code IS DISTINCT FROM termination_code) OR (p_print_cpa IS DISTINCT FROM print_cpa) OR (p_fein IS DISTINCT FROM fein) OR (p_business_type IS DISTINCT FROM business_type) OR (p_use_dba_on_tax_return IS DISTINCT FROM use_dba_on_tax_return) OR (p_federal_tax_deposit_frequency IS DISTINCT FROM federal_tax_deposit_frequency) OR (p_federal_tax_exempt_status IS DISTINCT FROM federal_tax_exempt_status) OR (p_fed_tax_exempt_ee_oasdi IS DISTINCT FROM fed_tax_exempt_ee_oasdi) OR (p_fed_tax_exempt_er_oasdi IS DISTINCT FROM fed_tax_exempt_er_oasdi) OR (p_fed_tax_exempt_ee_medicare IS DISTINCT FROM fed_tax_exempt_ee_medicare) OR (p_fed_tax_exempt_er_medicare IS DISTINCT FROM fed_tax_exempt_er_medicare) OR (p_fui_tax_deposit_frequency IS DISTINCT FROM fui_tax_deposit_frequency) OR (p_fui_tax_exempt IS DISTINCT FROM fui_tax_exempt) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_apply_misc_limit_to_1099 IS DISTINCT FROM apply_misc_limit_to_1099) OR (p_make_up_tax_deduct_shortfalls IS DISTINCT FROM make_up_tax_deduct_shortfalls) OR (p_fed_945_tax_deposit_frequency IS DISTINCT FROM fed_945_tax_deposit_frequency) OR (p_fui_rate_override IS DISTINCT FROM fui_rate_override) OR (p_name_on_invoice IS DISTINCT FROM name_on_invoice) OR (p_employer_type IS DISTINCT FROM employer_type) OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_nbr;
        p_name = name;
        p_dba = dba;
        p_address1 = address1;
        p_address2 = address2;
        p_city = city;
        p_state = state;
        p_zip_code = zip_code;
        p_legal_name = legal_name;
        p_legal_address1 = legal_address1;
        p_legal_address2 = legal_address2;
        p_legal_city = legal_city;
        p_legal_state = legal_state;
        p_legal_zip_code = legal_zip_code;
        p_termination_date = termination_date;
        p_termination_code = termination_code;
        p_print_cpa = print_cpa;
        p_fein = fein;
        p_business_type = business_type;
        p_use_dba_on_tax_return = use_dba_on_tax_return;
        p_federal_tax_deposit_frequency = federal_tax_deposit_frequency;
        p_federal_tax_exempt_status = federal_tax_exempt_status;
        p_fed_tax_exempt_ee_oasdi = fed_tax_exempt_ee_oasdi;
        p_fed_tax_exempt_er_oasdi = fed_tax_exempt_er_oasdi;
        p_fed_tax_exempt_ee_medicare = fed_tax_exempt_ee_medicare;
        p_fed_tax_exempt_er_medicare = fed_tax_exempt_er_medicare;
        p_fui_tax_deposit_frequency = fui_tax_deposit_frequency;
        p_fui_tax_exempt = fui_tax_exempt;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_apply_misc_limit_to_1099 = apply_misc_limit_to_1099;
        p_make_up_tax_deduct_shortfalls = make_up_tax_deduct_shortfalls;
        p_fed_945_tax_deposit_frequency = fed_945_tax_deposit_frequency;
        p_fui_rate_override = fui_rate_override;
        p_name_on_invoice = name_on_invoice;
        p_employer_type = employer_type;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, name, dba, address1, address2, city, state, zip_code, legal_name, legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, termination_date, termination_code, print_cpa, fein, business_type, use_dba_on_tax_return, federal_tax_deposit_frequency, federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi, fed_tax_exempt_ee_medicare, fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt, co_workers_comp_nbr, apply_misc_limit_to_1099, make_up_tax_deduct_shortfalls, fed_945_tax_deposit_frequency, fui_rate_override, name_on_invoice, employer_type, co_locations_nbr
        FROM co
        WHERE co_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :name, :dba, :address1, :address2, :city, :state, :zip_code, :legal_name, :legal_address1, :legal_address2, :legal_city, :legal_state, :legal_zip_code, :termination_date, :termination_code, :print_cpa, :fein, :business_type, :use_dba_on_tax_return, :federal_tax_deposit_frequency, :federal_tax_exempt_status, :fed_tax_exempt_ee_oasdi, :fed_tax_exempt_er_oasdi, :fed_tax_exempt_ee_medicare, :fed_tax_exempt_er_medicare, :fui_tax_deposit_frequency, :fui_tax_exempt, :co_workers_comp_nbr, :apply_misc_limit_to_1099, :make_up_tax_deduct_shortfalls, :fed_945_tax_deposit_frequency, :fui_rate_override, :name_on_invoice, :employer_type, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_name IS DISTINCT FROM name) OR (p_dba IS DISTINCT FROM dba) OR (p_address1 IS DISTINCT FROM address1) OR (p_address2 IS DISTINCT FROM address2) OR (p_city IS DISTINCT FROM city) OR (p_state IS DISTINCT FROM state) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_legal_name IS DISTINCT FROM legal_name) OR (p_legal_address1 IS DISTINCT FROM legal_address1) OR (p_legal_address2 IS DISTINCT FROM legal_address2) OR (p_legal_city IS DISTINCT FROM legal_city) OR (p_legal_state IS DISTINCT FROM legal_state) OR (p_legal_zip_code IS DISTINCT FROM legal_zip_code) OR (p_termination_date IS DISTINCT FROM termination_date) OR (p_termination_code IS DISTINCT FROM termination_code) OR (p_print_cpa IS DISTINCT FROM print_cpa) OR (p_fein IS DISTINCT FROM fein) OR (p_business_type IS DISTINCT FROM business_type) OR (p_use_dba_on_tax_return IS DISTINCT FROM use_dba_on_tax_return) OR (p_federal_tax_deposit_frequency IS DISTINCT FROM federal_tax_deposit_frequency) OR (p_federal_tax_exempt_status IS DISTINCT FROM federal_tax_exempt_status) OR (p_fed_tax_exempt_ee_oasdi IS DISTINCT FROM fed_tax_exempt_ee_oasdi) OR (p_fed_tax_exempt_er_oasdi IS DISTINCT FROM fed_tax_exempt_er_oasdi) OR (p_fed_tax_exempt_ee_medicare IS DISTINCT FROM fed_tax_exempt_ee_medicare) OR (p_fed_tax_exempt_er_medicare IS DISTINCT FROM fed_tax_exempt_er_medicare) OR (p_fui_tax_deposit_frequency IS DISTINCT FROM fui_tax_deposit_frequency) OR (p_fui_tax_exempt IS DISTINCT FROM fui_tax_exempt) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_apply_misc_limit_to_1099 IS DISTINCT FROM apply_misc_limit_to_1099) OR (p_make_up_tax_deduct_shortfalls IS DISTINCT FROM make_up_tax_deduct_shortfalls) OR (p_fed_945_tax_deposit_frequency IS DISTINCT FROM fed_945_tax_deposit_frequency) OR (p_fui_rate_override IS DISTINCT FROM fui_rate_override) OR (p_name_on_invoice IS DISTINCT FROM name_on_invoice) OR (p_employer_type IS DISTINCT FROM employer_type) OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_name = name;
        p_dba = dba;
        p_address1 = address1;
        p_address2 = address2;
        p_city = city;
        p_state = state;
        p_zip_code = zip_code;
        p_legal_name = legal_name;
        p_legal_address1 = legal_address1;
        p_legal_address2 = legal_address2;
        p_legal_city = legal_city;
        p_legal_state = legal_state;
        p_legal_zip_code = legal_zip_code;
        p_termination_date = termination_date;
        p_termination_code = termination_code;
        p_print_cpa = print_cpa;
        p_fein = fein;
        p_business_type = business_type;
        p_use_dba_on_tax_return = use_dba_on_tax_return;
        p_federal_tax_deposit_frequency = federal_tax_deposit_frequency;
        p_federal_tax_exempt_status = federal_tax_exempt_status;
        p_fed_tax_exempt_ee_oasdi = fed_tax_exempt_ee_oasdi;
        p_fed_tax_exempt_er_oasdi = fed_tax_exempt_er_oasdi;
        p_fed_tax_exempt_ee_medicare = fed_tax_exempt_ee_medicare;
        p_fed_tax_exempt_er_medicare = fed_tax_exempt_er_medicare;
        p_fui_tax_deposit_frequency = fui_tax_deposit_frequency;
        p_fui_tax_exempt = fui_tax_exempt;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_apply_misc_limit_to_1099 = apply_misc_limit_to_1099;
        p_make_up_tax_deduct_shortfalls = make_up_tax_deduct_shortfalls;
        p_fed_945_tax_deposit_frequency = fed_945_tax_deposit_frequency;
        p_fui_rate_override = fui_rate_override;
        p_name_on_invoice = name_on_invoice;
        p_employer_type = employer_type;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_branch(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_branch_nbr , co_locations_nbr
        FROM co_branch
        ORDER BY co_branch_nbr, effective_date
        INTO :rec_version, :effective_date, :co_branch_nbr, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_branch_nbr) OR (effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_branch_nbr;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_branch WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_locations_nbr
        FROM co_branch
        WHERE co_branch_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_branch WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_BRANCH TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_department(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_department_nbr , co_locations_nbr
        FROM co_department
        ORDER BY co_department_nbr, effective_date
        INTO :rec_version, :effective_date, :co_department_nbr, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_department_nbr) OR (effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_department_nbr;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_department WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_locations_nbr
        FROM co_department
        WHERE co_department_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_department WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_DEPARTMENT TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_division(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_division_nbr , co_locations_nbr
        FROM co_division
        ORDER BY co_division_nbr, effective_date
        INTO :rec_version, :effective_date, :co_division_nbr, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_division_nbr) OR (effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_division_nbr;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_division WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_locations_nbr
        FROM co_division
        WHERE co_division_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_division WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_DIVISION TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_jobs(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_jobs_nbr , co_locations_nbr
        FROM co_jobs
        ORDER BY co_jobs_nbr, effective_date
        INTO :rec_version, :effective_date, :co_jobs_nbr, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_jobs_nbr) OR (effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_jobs_nbr;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_jobs WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_locations_nbr
        FROM co_jobs
        WHERE co_jobs_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_jobs WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_JOBS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_local_tax(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_local_tax_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE exempt CHAR(1);
DECLARE VARIABLE p_exempt CHAR(1);
DECLARE VARIABLE local_ein_number VARCHAR(15);
DECLARE VARIABLE p_local_ein_number VARCHAR(15);
DECLARE VARIABLE tax_return_code VARCHAR(20);
DECLARE VARIABLE p_tax_return_code VARCHAR(20);
DECLARE VARIABLE sy_local_deposit_freq_nbr INTEGER;
DECLARE VARIABLE p_sy_local_deposit_freq_nbr INTEGER;
DECLARE VARIABLE final_tax_return CHAR(1);
DECLARE VARIABLE p_final_tax_return CHAR(1);
DECLARE VARIABLE local_active CHAR(1);
DECLARE VARIABLE p_local_active CHAR(1);
DECLARE VARIABLE self_adjust_tax CHAR(1);
DECLARE VARIABLE p_self_adjust_tax CHAR(1);
DECLARE VARIABLE tax_rate NUMERIC(18,6);
DECLARE VARIABLE p_tax_rate NUMERIC(18,6);
DECLARE VARIABLE miscellaneous_amount NUMERIC(18,6);
DECLARE VARIABLE p_miscellaneous_amount NUMERIC(18,6);
DECLARE VARIABLE cl_e_ds_nbr INTEGER;
DECLARE VARIABLE p_cl_e_ds_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_local_tax_nbr , exempt, local_ein_number, tax_return_code, sy_local_deposit_freq_nbr, final_tax_return, local_active, self_adjust_tax, tax_rate, miscellaneous_amount, cl_e_ds_nbr
        FROM co_local_tax
        ORDER BY co_local_tax_nbr, effective_date
        INTO :rec_version, :effective_date, :co_local_tax_nbr, :exempt, :local_ein_number, :tax_return_code, :sy_local_deposit_freq_nbr, :final_tax_return, :local_active, :self_adjust_tax, :tax_rate, :miscellaneous_amount, :cl_e_ds_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_local_tax_nbr) OR (effective_date = '1/1/1900') OR (p_exempt IS DISTINCT FROM exempt) OR (p_local_ein_number IS DISTINCT FROM local_ein_number) OR (p_tax_return_code IS DISTINCT FROM tax_return_code) OR (p_sy_local_deposit_freq_nbr IS DISTINCT FROM sy_local_deposit_freq_nbr) OR (p_final_tax_return IS DISTINCT FROM final_tax_return) OR (p_local_active IS DISTINCT FROM local_active) OR (p_self_adjust_tax IS DISTINCT FROM self_adjust_tax) OR (p_tax_rate IS DISTINCT FROM tax_rate) OR (p_miscellaneous_amount IS DISTINCT FROM miscellaneous_amount) OR (p_cl_e_ds_nbr IS DISTINCT FROM cl_e_ds_nbr)) THEN
      BEGIN
        curr_nbr = co_local_tax_nbr;
        p_exempt = exempt;
        p_local_ein_number = local_ein_number;
        p_tax_return_code = tax_return_code;
        p_sy_local_deposit_freq_nbr = sy_local_deposit_freq_nbr;
        p_final_tax_return = final_tax_return;
        p_local_active = local_active;
        p_self_adjust_tax = self_adjust_tax;
        p_tax_rate = tax_rate;
        p_miscellaneous_amount = miscellaneous_amount;
        p_cl_e_ds_nbr = cl_e_ds_nbr;
      END
      ELSE
        DELETE FROM co_local_tax WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, exempt, local_ein_number, tax_return_code, sy_local_deposit_freq_nbr, final_tax_return, local_active, self_adjust_tax, tax_rate, miscellaneous_amount, cl_e_ds_nbr
        FROM co_local_tax
        WHERE co_local_tax_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :exempt, :local_ein_number, :tax_return_code, :sy_local_deposit_freq_nbr, :final_tax_return, :local_active, :self_adjust_tax, :tax_rate, :miscellaneous_amount, :cl_e_ds_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_exempt IS DISTINCT FROM exempt) OR (p_local_ein_number IS DISTINCT FROM local_ein_number) OR (p_tax_return_code IS DISTINCT FROM tax_return_code) OR (p_sy_local_deposit_freq_nbr IS DISTINCT FROM sy_local_deposit_freq_nbr) OR (p_final_tax_return IS DISTINCT FROM final_tax_return) OR (p_local_active IS DISTINCT FROM local_active) OR (p_self_adjust_tax IS DISTINCT FROM self_adjust_tax) OR (p_tax_rate IS DISTINCT FROM tax_rate) OR (p_miscellaneous_amount IS DISTINCT FROM miscellaneous_amount) OR (p_cl_e_ds_nbr IS DISTINCT FROM cl_e_ds_nbr)) THEN
      BEGIN
        p_exempt = exempt;
        p_local_ein_number = local_ein_number;
        p_tax_return_code = tax_return_code;
        p_sy_local_deposit_freq_nbr = sy_local_deposit_freq_nbr;
        p_final_tax_return = final_tax_return;
        p_local_active = local_active;
        p_self_adjust_tax = self_adjust_tax;
        p_tax_rate = tax_rate;
        p_miscellaneous_amount = miscellaneous_amount;
        p_cl_e_ds_nbr = cl_e_ds_nbr;
      END
      ELSE
        DELETE FROM co_local_tax WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_LOCAL_TAX TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_states(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_states_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE tax_return_code VARCHAR(20);
DECLARE VARIABLE p_tax_return_code VARCHAR(20);
DECLARE VARIABLE use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE p_use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE sy_state_deposit_freq_nbr INTEGER;
DECLARE VARIABLE p_sy_state_deposit_freq_nbr INTEGER;
DECLARE VARIABLE sui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_sui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE state_non_profit CHAR(1);
DECLARE VARIABLE p_state_non_profit CHAR(1);
DECLARE VARIABLE state_exempt CHAR(1);
DECLARE VARIABLE p_state_exempt CHAR(1);
DECLARE VARIABLE sui_exempt CHAR(1);
DECLARE VARIABLE p_sui_exempt CHAR(1);
DECLARE VARIABLE ee_sdi_exempt CHAR(1);
DECLARE VARIABLE p_ee_sdi_exempt CHAR(1);
DECLARE VARIABLE state_eft_pin_number VARCHAR(10);
DECLARE VARIABLE p_state_eft_pin_number VARCHAR(10);
DECLARE VARIABLE sui_eft_pin_number VARCHAR(10);
DECLARE VARIABLE p_sui_eft_pin_number VARCHAR(10);
DECLARE VARIABLE state_ein VARCHAR(30);
DECLARE VARIABLE p_state_ein VARCHAR(30);
DECLARE VARIABLE sui_ein VARCHAR(30);
DECLARE VARIABLE p_sui_ein VARCHAR(30);
DECLARE VARIABLE state_sdi_ein VARCHAR(30);
DECLARE VARIABLE p_state_sdi_ein VARCHAR(30);
DECLARE VARIABLE state_eft_ein VARCHAR(30);
DECLARE VARIABLE p_state_eft_ein VARCHAR(30);
DECLARE VARIABLE state_sdi_eft_ein VARCHAR(30);
DECLARE VARIABLE p_state_sdi_eft_ein VARCHAR(30);
DECLARE VARIABLE sui_eft_ein VARCHAR(30);
DECLARE VARIABLE p_sui_eft_ein VARCHAR(30);
DECLARE VARIABLE er_sdi_exempt CHAR(1);
DECLARE VARIABLE p_er_sdi_exempt CHAR(1);
DECLARE VARIABLE company_paid_health_insurance CHAR(1);
DECLARE VARIABLE p_company_paid_health_insurance CHAR(1);
DECLARE VARIABLE last_tax_return CHAR(1);
DECLARE VARIABLE p_last_tax_return CHAR(1);
DECLARE VARIABLE tcd_deposit_frequency_nbr INTEGER;
DECLARE VARIABLE p_tcd_deposit_frequency_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_states_nbr , tax_return_code, use_dba_on_tax_return, sy_state_deposit_freq_nbr, sui_tax_deposit_frequency, state_non_profit, state_exempt, sui_exempt, ee_sdi_exempt, state_eft_pin_number, sui_eft_pin_number, state_ein, sui_ein, state_sdi_ein, state_eft_ein, state_sdi_eft_ein, sui_eft_ein, er_sdi_exempt, company_paid_health_insurance, last_tax_return, tcd_deposit_frequency_nbr
        FROM co_states
        ORDER BY co_states_nbr, effective_date
        INTO :rec_version, :effective_date, :co_states_nbr, :tax_return_code, :use_dba_on_tax_return, :sy_state_deposit_freq_nbr, :sui_tax_deposit_frequency, :state_non_profit, :state_exempt, :sui_exempt, :ee_sdi_exempt, :state_eft_pin_number, :sui_eft_pin_number, :state_ein, :sui_ein, :state_sdi_ein, :state_eft_ein, :state_sdi_eft_ein, :sui_eft_ein, :er_sdi_exempt, :company_paid_health_insurance, :last_tax_return, :tcd_deposit_frequency_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_states_nbr) OR (effective_date = '1/1/1900') OR (p_tax_return_code IS DISTINCT FROM tax_return_code) OR (p_use_dba_on_tax_return IS DISTINCT FROM use_dba_on_tax_return) OR (p_sy_state_deposit_freq_nbr IS DISTINCT FROM sy_state_deposit_freq_nbr) OR (p_sui_tax_deposit_frequency IS DISTINCT FROM sui_tax_deposit_frequency) OR (p_state_non_profit IS DISTINCT FROM state_non_profit) OR (p_state_exempt IS DISTINCT FROM state_exempt) OR (p_sui_exempt IS DISTINCT FROM sui_exempt) OR (p_ee_sdi_exempt IS DISTINCT FROM ee_sdi_exempt) OR (p_state_eft_pin_number IS DISTINCT FROM state_eft_pin_number) OR (p_sui_eft_pin_number IS DISTINCT FROM sui_eft_pin_number) OR (p_state_ein IS DISTINCT FROM state_ein) OR (p_sui_ein IS DISTINCT FROM sui_ein) OR (p_state_sdi_ein IS DISTINCT FROM state_sdi_ein) OR (p_state_eft_ein IS DISTINCT FROM state_eft_ein) OR (p_state_sdi_eft_ein IS DISTINCT FROM state_sdi_eft_ein) OR (p_sui_eft_ein IS DISTINCT FROM sui_eft_ein) OR (p_er_sdi_exempt IS DISTINCT FROM er_sdi_exempt) OR (p_company_paid_health_insurance IS DISTINCT FROM company_paid_health_insurance) OR (p_last_tax_return IS DISTINCT FROM last_tax_return) OR (p_tcd_deposit_frequency_nbr IS DISTINCT FROM tcd_deposit_frequency_nbr)) THEN
      BEGIN
        curr_nbr = co_states_nbr;
        p_tax_return_code = tax_return_code;
        p_use_dba_on_tax_return = use_dba_on_tax_return;
        p_sy_state_deposit_freq_nbr = sy_state_deposit_freq_nbr;
        p_sui_tax_deposit_frequency = sui_tax_deposit_frequency;
        p_state_non_profit = state_non_profit;
        p_state_exempt = state_exempt;
        p_sui_exempt = sui_exempt;
        p_ee_sdi_exempt = ee_sdi_exempt;
        p_state_eft_pin_number = state_eft_pin_number;
        p_sui_eft_pin_number = sui_eft_pin_number;
        p_state_ein = state_ein;
        p_sui_ein = sui_ein;
        p_state_sdi_ein = state_sdi_ein;
        p_state_eft_ein = state_eft_ein;
        p_state_sdi_eft_ein = state_sdi_eft_ein;
        p_sui_eft_ein = sui_eft_ein;
        p_er_sdi_exempt = er_sdi_exempt;
        p_company_paid_health_insurance = company_paid_health_insurance;
        p_last_tax_return = last_tax_return;
        p_tcd_deposit_frequency_nbr = tcd_deposit_frequency_nbr;
      END
      ELSE
        DELETE FROM co_states WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, tax_return_code, use_dba_on_tax_return, sy_state_deposit_freq_nbr, sui_tax_deposit_frequency, state_non_profit, state_exempt, sui_exempt, ee_sdi_exempt, state_eft_pin_number, sui_eft_pin_number, state_ein, sui_ein, state_sdi_ein, state_eft_ein, state_sdi_eft_ein, sui_eft_ein, er_sdi_exempt, company_paid_health_insurance, last_tax_return, tcd_deposit_frequency_nbr
        FROM co_states
        WHERE co_states_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :tax_return_code, :use_dba_on_tax_return, :sy_state_deposit_freq_nbr, :sui_tax_deposit_frequency, :state_non_profit, :state_exempt, :sui_exempt, :ee_sdi_exempt, :state_eft_pin_number, :sui_eft_pin_number, :state_ein, :sui_ein, :state_sdi_ein, :state_eft_ein, :state_sdi_eft_ein, :sui_eft_ein, :er_sdi_exempt, :company_paid_health_insurance, :last_tax_return, :tcd_deposit_frequency_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_tax_return_code IS DISTINCT FROM tax_return_code) OR (p_use_dba_on_tax_return IS DISTINCT FROM use_dba_on_tax_return) OR (p_sy_state_deposit_freq_nbr IS DISTINCT FROM sy_state_deposit_freq_nbr) OR (p_sui_tax_deposit_frequency IS DISTINCT FROM sui_tax_deposit_frequency) OR (p_state_non_profit IS DISTINCT FROM state_non_profit) OR (p_state_exempt IS DISTINCT FROM state_exempt) OR (p_sui_exempt IS DISTINCT FROM sui_exempt) OR (p_ee_sdi_exempt IS DISTINCT FROM ee_sdi_exempt) OR (p_state_eft_pin_number IS DISTINCT FROM state_eft_pin_number) OR (p_sui_eft_pin_number IS DISTINCT FROM sui_eft_pin_number) OR (p_state_ein IS DISTINCT FROM state_ein) OR (p_sui_ein IS DISTINCT FROM sui_ein) OR (p_state_sdi_ein IS DISTINCT FROM state_sdi_ein) OR (p_state_eft_ein IS DISTINCT FROM state_eft_ein) OR (p_state_sdi_eft_ein IS DISTINCT FROM state_sdi_eft_ein) OR (p_sui_eft_ein IS DISTINCT FROM sui_eft_ein) OR (p_er_sdi_exempt IS DISTINCT FROM er_sdi_exempt) OR (p_company_paid_health_insurance IS DISTINCT FROM company_paid_health_insurance) OR (p_last_tax_return IS DISTINCT FROM last_tax_return) OR (p_tcd_deposit_frequency_nbr IS DISTINCT FROM tcd_deposit_frequency_nbr)) THEN
      BEGIN
        p_tax_return_code = tax_return_code;
        p_use_dba_on_tax_return = use_dba_on_tax_return;
        p_sy_state_deposit_freq_nbr = sy_state_deposit_freq_nbr;
        p_sui_tax_deposit_frequency = sui_tax_deposit_frequency;
        p_state_non_profit = state_non_profit;
        p_state_exempt = state_exempt;
        p_sui_exempt = sui_exempt;
        p_ee_sdi_exempt = ee_sdi_exempt;
        p_state_eft_pin_number = state_eft_pin_number;
        p_sui_eft_pin_number = sui_eft_pin_number;
        p_state_ein = state_ein;
        p_sui_ein = sui_ein;
        p_state_sdi_ein = state_sdi_ein;
        p_state_eft_ein = state_eft_ein;
        p_state_sdi_eft_ein = state_sdi_eft_ein;
        p_sui_eft_ein = sui_eft_ein;
        p_er_sdi_exempt = er_sdi_exempt;
        p_company_paid_health_insurance = company_paid_health_insurance;
        p_last_tax_return = last_tax_return;
        p_tcd_deposit_frequency_nbr = tcd_deposit_frequency_nbr;
      END
      ELSE
        DELETE FROM co_states WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_STATES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_sui(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_sui_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_states_nbr INTEGER;
DECLARE VARIABLE p_co_states_nbr INTEGER;
DECLARE VARIABLE tax_return_code VARCHAR(20);
DECLARE VARIABLE p_tax_return_code VARCHAR(20);
DECLARE VARIABLE final_tax_return CHAR(1);
DECLARE VARIABLE p_final_tax_return CHAR(1);
DECLARE VARIABLE rate NUMERIC(18,8);
DECLARE VARIABLE p_rate NUMERIC(18,8);
DECLARE VARIABLE last_tax_return CHAR(1);
DECLARE VARIABLE p_last_tax_return CHAR(1);
DECLARE VARIABLE sui_reimburser CHAR(1);
DECLARE VARIABLE p_sui_reimburser CHAR(1);
DECLARE VARIABLE alternate_wage CHAR(1);
DECLARE VARIABLE p_alternate_wage CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_sui_nbr , co_states_nbr, tax_return_code, final_tax_return, rate, last_tax_return, sui_reimburser, alternate_wage
        FROM co_sui
        ORDER BY co_sui_nbr, effective_date
        INTO :rec_version, :effective_date, :co_sui_nbr, :co_states_nbr, :tax_return_code, :final_tax_return, :rate, :last_tax_return, :sui_reimburser, :alternate_wage
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_sui_nbr) OR (effective_date = '1/1/1900') OR (p_co_states_nbr IS DISTINCT FROM co_states_nbr) OR (p_tax_return_code IS DISTINCT FROM tax_return_code) OR (p_final_tax_return IS DISTINCT FROM final_tax_return) OR (p_rate IS DISTINCT FROM rate) OR (p_last_tax_return IS DISTINCT FROM last_tax_return) OR (p_sui_reimburser IS DISTINCT FROM sui_reimburser) OR (p_alternate_wage IS DISTINCT FROM alternate_wage)) THEN
      BEGIN
        curr_nbr = co_sui_nbr;
        p_co_states_nbr = co_states_nbr;
        p_tax_return_code = tax_return_code;
        p_final_tax_return = final_tax_return;
        p_rate = rate;
        p_last_tax_return = last_tax_return;
        p_sui_reimburser = sui_reimburser;
        p_alternate_wage = alternate_wage;
      END
      ELSE
        DELETE FROM co_sui WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_states_nbr, tax_return_code, final_tax_return, rate, last_tax_return, sui_reimburser, alternate_wage
        FROM co_sui
        WHERE co_sui_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_states_nbr, :tax_return_code, :final_tax_return, :rate, :last_tax_return, :sui_reimburser, :alternate_wage
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_states_nbr IS DISTINCT FROM co_states_nbr) OR (p_tax_return_code IS DISTINCT FROM tax_return_code) OR (p_final_tax_return IS DISTINCT FROM final_tax_return) OR (p_rate IS DISTINCT FROM rate) OR (p_last_tax_return IS DISTINCT FROM last_tax_return) OR (p_sui_reimburser IS DISTINCT FROM sui_reimburser) OR (p_alternate_wage IS DISTINCT FROM alternate_wage)) THEN
      BEGIN
        p_co_states_nbr = co_states_nbr;
        p_tax_return_code = tax_return_code;
        p_final_tax_return = final_tax_return;
        p_rate = rate;
        p_last_tax_return = last_tax_return;
        p_sui_reimburser = sui_reimburser;
        p_alternate_wage = alternate_wage;
      END
      ELSE
        DELETE FROM co_sui WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_SUI TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_team(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_team_nbr , co_locations_nbr
        FROM co_team
        ORDER BY co_team_nbr, effective_date
        INTO :rec_version, :effective_date, :co_team_nbr, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_team_nbr) OR (effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_team_nbr;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_team WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_locations_nbr
        FROM co_team
        WHERE co_team_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co_team WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_TEAM TO EUSER
^

CREATE OR ALTER PROCEDURE pack_co_workers_comp(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_states_nbr INTEGER;
DECLARE VARIABLE p_co_states_nbr INTEGER;
DECLARE VARIABLE rate NUMERIC(18,6);
DECLARE VARIABLE p_rate NUMERIC(18,6);
DECLARE VARIABLE experience_rating NUMERIC(18,6);
DECLARE VARIABLE p_experience_rating NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_workers_comp_nbr , co_states_nbr, rate, experience_rating
        FROM co_workers_comp
        ORDER BY co_workers_comp_nbr, effective_date
        INTO :rec_version, :effective_date, :co_workers_comp_nbr, :co_states_nbr, :rate, :experience_rating
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (effective_date = '1/1/1900') OR (p_co_states_nbr IS DISTINCT FROM co_states_nbr) OR (p_rate IS DISTINCT FROM rate) OR (p_experience_rating IS DISTINCT FROM experience_rating)) THEN
      BEGIN
        curr_nbr = co_workers_comp_nbr;
        p_co_states_nbr = co_states_nbr;
        p_rate = rate;
        p_experience_rating = experience_rating;
      END
      ELSE
        DELETE FROM co_workers_comp WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_states_nbr, rate, experience_rating
        FROM co_workers_comp
        WHERE co_workers_comp_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_states_nbr, :rate, :experience_rating
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_states_nbr IS DISTINCT FROM co_states_nbr) OR (p_rate IS DISTINCT FROM rate) OR (p_experience_rating IS DISTINCT FROM experience_rating)) THEN
      BEGIN
        p_co_states_nbr = co_states_nbr;
        p_rate = rate;
        p_experience_rating = experience_rating;
      END
      ELSE
        DELETE FROM co_workers_comp WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO_WORKERS_COMP TO EUSER
^

CREATE OR ALTER PROCEDURE pack_ee_locals(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_locals_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE co_local_tax_nbr INTEGER;
DECLARE VARIABLE p_co_local_tax_nbr INTEGER;
DECLARE VARIABLE exempt_exclude CHAR(1);
DECLARE VARIABLE p_exempt_exclude CHAR(1);
DECLARE VARIABLE ee_tax_code VARCHAR(20);
DECLARE VARIABLE p_ee_tax_code VARCHAR(20);
DECLARE VARIABLE deduct CHAR(1);
DECLARE VARIABLE p_deduct CHAR(1);
DECLARE VARIABLE miscellaneous_amount NUMERIC(18,6);
DECLARE VARIABLE p_miscellaneous_amount NUMERIC(18,6);
DECLARE VARIABLE percentage_of_tax_wages NUMERIC(18,6);
DECLARE VARIABLE p_percentage_of_tax_wages NUMERIC(18,6);
DECLARE VARIABLE local_enabled CHAR(1);
DECLARE VARIABLE p_local_enabled CHAR(1);
DECLARE VARIABLE override_local_tax_type CHAR(1);
DECLARE VARIABLE p_override_local_tax_type CHAR(1);
DECLARE VARIABLE override_local_tax_value NUMERIC(18,6);
DECLARE VARIABLE p_override_local_tax_value NUMERIC(18,6);
DECLARE VARIABLE include_in_pretax CHAR(1);
DECLARE VARIABLE p_include_in_pretax CHAR(1);
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
DECLARE VARIABLE work_address_ovr CHAR(1);
DECLARE VARIABLE p_work_address_ovr CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_locals_nbr , co_local_tax_nbr, exempt_exclude, ee_tax_code, deduct, miscellaneous_amount, percentage_of_tax_wages, local_enabled, override_local_tax_type, override_local_tax_value, include_in_pretax, co_locations_nbr, work_address_ovr
        FROM ee_locals
        ORDER BY ee_locals_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_locals_nbr, :co_local_tax_nbr, :exempt_exclude, :ee_tax_code, :deduct, :miscellaneous_amount, :percentage_of_tax_wages, :local_enabled, :override_local_tax_type, :override_local_tax_value, :include_in_pretax, :co_locations_nbr, :work_address_ovr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_locals_nbr) OR (effective_date = '1/1/1900') OR (p_co_local_tax_nbr IS DISTINCT FROM co_local_tax_nbr) OR (p_exempt_exclude IS DISTINCT FROM exempt_exclude) OR (p_ee_tax_code IS DISTINCT FROM ee_tax_code) OR (p_deduct IS DISTINCT FROM deduct) OR (p_miscellaneous_amount IS DISTINCT FROM miscellaneous_amount) OR (p_percentage_of_tax_wages IS DISTINCT FROM percentage_of_tax_wages) OR (p_local_enabled IS DISTINCT FROM local_enabled) OR (p_override_local_tax_type IS DISTINCT FROM override_local_tax_type) OR (p_override_local_tax_value IS DISTINCT FROM override_local_tax_value) OR (p_include_in_pretax IS DISTINCT FROM include_in_pretax) OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr) OR (p_work_address_ovr IS DISTINCT FROM work_address_ovr)) THEN
      BEGIN
        curr_nbr = ee_locals_nbr;
        p_co_local_tax_nbr = co_local_tax_nbr;
        p_exempt_exclude = exempt_exclude;
        p_ee_tax_code = ee_tax_code;
        p_deduct = deduct;
        p_miscellaneous_amount = miscellaneous_amount;
        p_percentage_of_tax_wages = percentage_of_tax_wages;
        p_local_enabled = local_enabled;
        p_override_local_tax_type = override_local_tax_type;
        p_override_local_tax_value = override_local_tax_value;
        p_include_in_pretax = include_in_pretax;
        p_co_locations_nbr = co_locations_nbr;
        p_work_address_ovr = work_address_ovr;
      END
      ELSE
        DELETE FROM ee_locals WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, co_local_tax_nbr, exempt_exclude, ee_tax_code, deduct, miscellaneous_amount, percentage_of_tax_wages, local_enabled, override_local_tax_type, override_local_tax_value, include_in_pretax, co_locations_nbr, work_address_ovr
        FROM ee_locals
        WHERE ee_locals_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :co_local_tax_nbr, :exempt_exclude, :ee_tax_code, :deduct, :miscellaneous_amount, :percentage_of_tax_wages, :local_enabled, :override_local_tax_type, :override_local_tax_value, :include_in_pretax, :co_locations_nbr, :work_address_ovr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_co_local_tax_nbr IS DISTINCT FROM co_local_tax_nbr) OR (p_exempt_exclude IS DISTINCT FROM exempt_exclude) OR (p_ee_tax_code IS DISTINCT FROM ee_tax_code) OR (p_deduct IS DISTINCT FROM deduct) OR (p_miscellaneous_amount IS DISTINCT FROM miscellaneous_amount) OR (p_percentage_of_tax_wages IS DISTINCT FROM percentage_of_tax_wages) OR (p_local_enabled IS DISTINCT FROM local_enabled) OR (p_override_local_tax_type IS DISTINCT FROM override_local_tax_type) OR (p_override_local_tax_value IS DISTINCT FROM override_local_tax_value) OR (p_include_in_pretax IS DISTINCT FROM include_in_pretax) OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr) OR (p_work_address_ovr IS DISTINCT FROM work_address_ovr)) THEN
      BEGIN
        p_co_local_tax_nbr = co_local_tax_nbr;
        p_exempt_exclude = exempt_exclude;
        p_ee_tax_code = ee_tax_code;
        p_deduct = deduct;
        p_miscellaneous_amount = miscellaneous_amount;
        p_percentage_of_tax_wages = percentage_of_tax_wages;
        p_local_enabled = local_enabled;
        p_override_local_tax_type = override_local_tax_type;
        p_override_local_tax_value = override_local_tax_value;
        p_include_in_pretax = include_in_pretax;
        p_co_locations_nbr = co_locations_nbr;
        p_work_address_ovr = work_address_ovr;
      END
      ELSE
        DELETE FROM ee_locals WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_EE_LOCALS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_ee_rates(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_rates_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE rate_number INTEGER;
DECLARE VARIABLE p_rate_number INTEGER;
DECLARE VARIABLE primary_rate CHAR(1);
DECLARE VARIABLE p_primary_rate CHAR(1);
DECLARE VARIABLE rate_amount NUMERIC(18,6);
DECLARE VARIABLE p_rate_amount NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_rates_nbr , rate_number, primary_rate, rate_amount
        FROM ee_rates
        ORDER BY ee_rates_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_rates_nbr, :rate_number, :primary_rate, :rate_amount
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_rates_nbr) OR (effective_date = '1/1/1900') OR (p_rate_number IS DISTINCT FROM rate_number) OR (p_primary_rate IS DISTINCT FROM primary_rate) OR (p_rate_amount IS DISTINCT FROM rate_amount)) THEN
      BEGIN
        curr_nbr = ee_rates_nbr;
        p_rate_number = rate_number;
        p_primary_rate = primary_rate;
        p_rate_amount = rate_amount;
      END
      ELSE
        DELETE FROM ee_rates WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, rate_number, primary_rate, rate_amount
        FROM ee_rates
        WHERE ee_rates_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :rate_number, :primary_rate, :rate_amount
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_rate_number IS DISTINCT FROM rate_number) OR (p_primary_rate IS DISTINCT FROM primary_rate) OR (p_rate_amount IS DISTINCT FROM rate_amount)) THEN
      BEGIN
        p_rate_number = rate_number;
        p_primary_rate = primary_rate;
        p_rate_amount = rate_amount;
      END
      ELSE
        DELETE FROM ee_rates WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_EE_RATES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_ee_states(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE ee_states_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE state_exempt_exclude CHAR(1);
DECLARE VARIABLE p_state_exempt_exclude CHAR(1);
DECLARE VARIABLE sdi_apply_co_states_nbr INTEGER;
DECLARE VARIABLE p_sdi_apply_co_states_nbr INTEGER;
DECLARE VARIABLE sui_apply_co_states_nbr INTEGER;
DECLARE VARIABLE p_sui_apply_co_states_nbr INTEGER;
DECLARE VARIABLE ee_sdi_exempt_exclude CHAR(1);
DECLARE VARIABLE p_ee_sdi_exempt_exclude CHAR(1);
DECLARE VARIABLE er_sdi_exempt_exclude CHAR(1);
DECLARE VARIABLE p_er_sdi_exempt_exclude CHAR(1);
DECLARE VARIABLE ee_sui_exempt_exclude CHAR(1);
DECLARE VARIABLE p_ee_sui_exempt_exclude CHAR(1);
DECLARE VARIABLE er_sui_exempt_exclude CHAR(1);
DECLARE VARIABLE p_er_sui_exempt_exclude CHAR(1);
DECLARE VARIABLE reciprocal_co_states_nbr INTEGER;
DECLARE VARIABLE p_reciprocal_co_states_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, ee_states_nbr , state_exempt_exclude, sdi_apply_co_states_nbr, sui_apply_co_states_nbr, ee_sdi_exempt_exclude, er_sdi_exempt_exclude, ee_sui_exempt_exclude, er_sui_exempt_exclude, reciprocal_co_states_nbr
        FROM ee_states
        ORDER BY ee_states_nbr, effective_date
        INTO :rec_version, :effective_date, :ee_states_nbr, :state_exempt_exclude, :sdi_apply_co_states_nbr, :sui_apply_co_states_nbr, :ee_sdi_exempt_exclude, :er_sdi_exempt_exclude, :ee_sui_exempt_exclude, :er_sui_exempt_exclude, :reciprocal_co_states_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM ee_states_nbr) OR (effective_date = '1/1/1900') OR (p_state_exempt_exclude IS DISTINCT FROM state_exempt_exclude) OR (p_sdi_apply_co_states_nbr IS DISTINCT FROM sdi_apply_co_states_nbr) OR (p_sui_apply_co_states_nbr IS DISTINCT FROM sui_apply_co_states_nbr) OR (p_ee_sdi_exempt_exclude IS DISTINCT FROM ee_sdi_exempt_exclude) OR (p_er_sdi_exempt_exclude IS DISTINCT FROM er_sdi_exempt_exclude) OR (p_ee_sui_exempt_exclude IS DISTINCT FROM ee_sui_exempt_exclude) OR (p_er_sui_exempt_exclude IS DISTINCT FROM er_sui_exempt_exclude) OR (p_reciprocal_co_states_nbr IS DISTINCT FROM reciprocal_co_states_nbr)) THEN
      BEGIN
        curr_nbr = ee_states_nbr;
        p_state_exempt_exclude = state_exempt_exclude;
        p_sdi_apply_co_states_nbr = sdi_apply_co_states_nbr;
        p_sui_apply_co_states_nbr = sui_apply_co_states_nbr;
        p_ee_sdi_exempt_exclude = ee_sdi_exempt_exclude;
        p_er_sdi_exempt_exclude = er_sdi_exempt_exclude;
        p_ee_sui_exempt_exclude = ee_sui_exempt_exclude;
        p_er_sui_exempt_exclude = er_sui_exempt_exclude;
        p_reciprocal_co_states_nbr = reciprocal_co_states_nbr;
      END
      ELSE
        DELETE FROM ee_states WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, state_exempt_exclude, sdi_apply_co_states_nbr, sui_apply_co_states_nbr, ee_sdi_exempt_exclude, er_sdi_exempt_exclude, ee_sui_exempt_exclude, er_sui_exempt_exclude, reciprocal_co_states_nbr
        FROM ee_states
        WHERE ee_states_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :state_exempt_exclude, :sdi_apply_co_states_nbr, :sui_apply_co_states_nbr, :ee_sdi_exempt_exclude, :er_sdi_exempt_exclude, :ee_sui_exempt_exclude, :er_sui_exempt_exclude, :reciprocal_co_states_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_state_exempt_exclude IS DISTINCT FROM state_exempt_exclude) OR (p_sdi_apply_co_states_nbr IS DISTINCT FROM sdi_apply_co_states_nbr) OR (p_sui_apply_co_states_nbr IS DISTINCT FROM sui_apply_co_states_nbr) OR (p_ee_sdi_exempt_exclude IS DISTINCT FROM ee_sdi_exempt_exclude) OR (p_er_sdi_exempt_exclude IS DISTINCT FROM er_sdi_exempt_exclude) OR (p_ee_sui_exempt_exclude IS DISTINCT FROM ee_sui_exempt_exclude) OR (p_er_sui_exempt_exclude IS DISTINCT FROM er_sui_exempt_exclude) OR (p_reciprocal_co_states_nbr IS DISTINCT FROM reciprocal_co_states_nbr)) THEN
      BEGIN
        p_state_exempt_exclude = state_exempt_exclude;
        p_sdi_apply_co_states_nbr = sdi_apply_co_states_nbr;
        p_sui_apply_co_states_nbr = sui_apply_co_states_nbr;
        p_ee_sdi_exempt_exclude = ee_sdi_exempt_exclude;
        p_er_sdi_exempt_exclude = er_sdi_exempt_exclude;
        p_ee_sui_exempt_exclude = ee_sui_exempt_exclude;
        p_er_sui_exempt_exclude = er_sui_exempt_exclude;
        p_reciprocal_co_states_nbr = reciprocal_co_states_nbr;
      END
      ELSE
        DELETE FROM ee_states WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_EE_STATES TO EUSER
^



/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.13', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
