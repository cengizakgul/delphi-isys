/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.4';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^

COMMIT^


CREATE OR ALTER PROCEDURE pack_sy_fed_exemptions(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_fed_exemptions_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE e_d_code_type VARCHAR(2);
DECLARE VARIABLE p_e_d_code_type VARCHAR(2);
DECLARE VARIABLE exempt_federal CHAR(1);
DECLARE VARIABLE p_exempt_federal CHAR(1);
DECLARE VARIABLE exempt_fui CHAR(1);
DECLARE VARIABLE p_exempt_fui CHAR(1);
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE p_exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE p_exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employee_eic CHAR(1);
DECLARE VARIABLE p_exempt_employee_eic CHAR(1);
DECLARE VARIABLE w2_box VARCHAR(4);
DECLARE VARIABLE p_w2_box VARCHAR(4);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_fed_exemptions_nbr , e_d_code_type, exempt_federal, exempt_fui, exempt_employee_oasdi, exempt_employer_oasdi, exempt_employee_medicare, exempt_employer_medicare, exempt_employee_eic, w2_box
        FROM sy_fed_exemptions
        ORDER BY sy_fed_exemptions_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_fed_exemptions_nbr, :e_d_code_type, :exempt_federal, :exempt_fui, :exempt_employee_oasdi, :exempt_employer_oasdi, :exempt_employee_medicare, :exempt_employer_medicare, :exempt_employee_eic, :w2_box
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_fed_exemptions_nbr) OR (effective_date = '1/1/1900') OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_exempt_federal IS DISTINCT FROM exempt_federal) OR (p_exempt_fui IS DISTINCT FROM exempt_fui) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employee_eic IS DISTINCT FROM exempt_employee_eic) OR (p_w2_box IS DISTINCT FROM w2_box)) THEN
      BEGIN
        curr_nbr = sy_fed_exemptions_nbr;
        p_e_d_code_type = e_d_code_type;
        p_exempt_federal = exempt_federal;
        p_exempt_fui = exempt_fui;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employee_eic = exempt_employee_eic;
        p_w2_box = w2_box;
      END
      ELSE
        DELETE FROM sy_fed_exemptions WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, e_d_code_type, exempt_federal, exempt_fui, exempt_employee_oasdi, exempt_employer_oasdi, exempt_employee_medicare, exempt_employer_medicare, exempt_employee_eic, w2_box
        FROM sy_fed_exemptions
        WHERE sy_fed_exemptions_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :e_d_code_type, :exempt_federal, :exempt_fui, :exempt_employee_oasdi, :exempt_employer_oasdi, :exempt_employee_medicare, :exempt_employer_medicare, :exempt_employee_eic, :w2_box
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_exempt_federal IS DISTINCT FROM exempt_federal) OR (p_exempt_fui IS DISTINCT FROM exempt_fui) OR (p_exempt_employee_oasdi IS DISTINCT FROM exempt_employee_oasdi) OR (p_exempt_employer_oasdi IS DISTINCT FROM exempt_employer_oasdi) OR (p_exempt_employee_medicare IS DISTINCT FROM exempt_employee_medicare) OR (p_exempt_employer_medicare IS DISTINCT FROM exempt_employer_medicare) OR (p_exempt_employee_eic IS DISTINCT FROM exempt_employee_eic) OR (p_w2_box IS DISTINCT FROM w2_box)) THEN
      BEGIN
        p_e_d_code_type = e_d_code_type;
        p_exempt_federal = exempt_federal;
        p_exempt_fui = exempt_fui;
        p_exempt_employee_oasdi = exempt_employee_oasdi;
        p_exempt_employer_oasdi = exempt_employer_oasdi;
        p_exempt_employee_medicare = exempt_employee_medicare;
        p_exempt_employer_medicare = exempt_employer_medicare;
        p_exempt_employee_eic = exempt_employee_eic;
        p_w2_box = w2_box;
      END
      ELSE
        DELETE FROM sy_fed_exemptions WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_FED_EXEMPTIONS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_fed_tax_table(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_fed_tax_table_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE exemption_amount NUMERIC(18,6);
DECLARE VARIABLE p_exemption_amount NUMERIC(18,6);
DECLARE VARIABLE federal_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE p_federal_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE federal_tip_credit NUMERIC(18,6);
DECLARE VARIABLE p_federal_tip_credit NUMERIC(18,6);
DECLARE VARIABLE supplemental_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE p_supplemental_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_er_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_ee_oasdi_rate NUMERIC(18,6);
DECLARE VARIABLE oasdi_wage_limit NUMERIC(18,6);
DECLARE VARIABLE p_oasdi_wage_limit NUMERIC(18,6);
DECLARE VARIABLE medicare_rate NUMERIC(18,6);
DECLARE VARIABLE p_medicare_rate NUMERIC(18,6);
DECLARE VARIABLE medicare_wage_limit NUMERIC(18,6);
DECLARE VARIABLE p_medicare_wage_limit NUMERIC(18,6);
DECLARE VARIABLE fui_rate_real NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_real NUMERIC(18,6);
DECLARE VARIABLE fui_rate_credit NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_credit NUMERIC(18,6);
DECLARE VARIABLE fui_wage_limit NUMERIC(18,6);
DECLARE VARIABLE p_fui_wage_limit NUMERIC(18,6);
DECLARE VARIABLE sy_401k_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_401k_limit NUMERIC(18,6);
DECLARE VARIABLE sy_403b_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_403b_limit NUMERIC(18,6);
DECLARE VARIABLE sy_457_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_457_limit NUMERIC(18,6);
DECLARE VARIABLE sy_501c_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_501c_limit NUMERIC(18,6);
DECLARE VARIABLE simple_limit NUMERIC(18,6);
DECLARE VARIABLE p_simple_limit NUMERIC(18,6);
DECLARE VARIABLE sep_limit NUMERIC(18,6);
DECLARE VARIABLE p_sep_limit NUMERIC(18,6);
DECLARE VARIABLE deferred_comp_limit NUMERIC(18,6);
DECLARE VARIABLE p_deferred_comp_limit NUMERIC(18,6);
DECLARE VARIABLE fed_deposit_freq_threshold NUMERIC(18,6);
DECLARE VARIABLE p_fed_deposit_freq_threshold NUMERIC(18,6);
DECLARE VARIABLE first_eic_limit NUMERIC(18,6);
DECLARE VARIABLE p_first_eic_limit NUMERIC(18,6);
DECLARE VARIABLE first_eic_percentage NUMERIC(18,6);
DECLARE VARIABLE p_first_eic_percentage NUMERIC(18,6);
DECLARE VARIABLE second_eic_limit NUMERIC(18,6);
DECLARE VARIABLE p_second_eic_limit NUMERIC(18,6);
DECLARE VARIABLE second_eic_amount NUMERIC(18,6);
DECLARE VARIABLE p_second_eic_amount NUMERIC(18,6);
DECLARE VARIABLE third_eic_additional_percent NUMERIC(18,6);
DECLARE VARIABLE p_third_eic_additional_percent NUMERIC(18,6);
DECLARE VARIABLE sy_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE sy_dependent_care_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_dependent_care_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_single_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_hsa_single_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_family_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_hsa_family_limit NUMERIC(18,6);
DECLARE VARIABLE sy_pension_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_pension_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE sy_simple_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_simple_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE compensation_limit NUMERIC(18,6);
DECLARE VARIABLE p_compensation_limit NUMERIC(18,6);
DECLARE VARIABLE sy_hsa_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_hsa_catch_up_limit NUMERIC(18,6);
DECLARE VARIABLE s132_parking_limit NUMERIC(18,6);
DECLARE VARIABLE p_s132_parking_limit NUMERIC(18,6);
DECLARE VARIABLE roth_ira_limit NUMERIC(18,6);
DECLARE VARIABLE p_roth_ira_limit NUMERIC(18,6);
DECLARE VARIABLE sy_ira_catchup_limit NUMERIC(18,6);
DECLARE VARIABLE p_sy_ira_catchup_limit NUMERIC(18,6);
DECLARE VARIABLE ee_med_th_limit NUMERIC(18,6);
DECLARE VARIABLE p_ee_med_th_limit NUMERIC(18,6);
DECLARE VARIABLE ee_med_th_rate NUMERIC(18,6);
DECLARE VARIABLE p_ee_med_th_rate NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_fed_tax_table_nbr , filler, exemption_amount, federal_minimum_wage, federal_tip_credit, supplemental_tax_percentage, oasdi_rate, er_oasdi_rate, ee_oasdi_rate, oasdi_wage_limit, medicare_rate, medicare_wage_limit, fui_rate_real, fui_rate_credit, fui_wage_limit, sy_401k_limit, sy_403b_limit, sy_457_limit, sy_501c_limit, simple_limit, sep_limit, deferred_comp_limit, fed_deposit_freq_threshold, first_eic_limit, first_eic_percentage, second_eic_limit, second_eic_amount, third_eic_additional_percent, sy_catch_up_limit, sy_dependent_care_limit, sy_hsa_single_limit, sy_hsa_family_limit, sy_pension_catch_up_limit, sy_simple_catch_up_limit, compensation_limit, sy_hsa_catch_up_limit, s132_parking_limit, roth_ira_limit, sy_ira_catchup_limit, ee_med_th_limit, ee_med_th_rate
        FROM sy_fed_tax_table
        ORDER BY sy_fed_tax_table_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_fed_tax_table_nbr, :filler, :exemption_amount, :federal_minimum_wage, :federal_tip_credit, :supplemental_tax_percentage, :oasdi_rate, :er_oasdi_rate, :ee_oasdi_rate, :oasdi_wage_limit, :medicare_rate, :medicare_wage_limit, :fui_rate_real, :fui_rate_credit, :fui_wage_limit, :sy_401k_limit, :sy_403b_limit, :sy_457_limit, :sy_501c_limit, :simple_limit, :sep_limit, :deferred_comp_limit, :fed_deposit_freq_threshold, :first_eic_limit, :first_eic_percentage, :second_eic_limit, :second_eic_amount, :third_eic_additional_percent, :sy_catch_up_limit, :sy_dependent_care_limit, :sy_hsa_single_limit, :sy_hsa_family_limit, :sy_pension_catch_up_limit, :sy_simple_catch_up_limit, :compensation_limit, :sy_hsa_catch_up_limit, :s132_parking_limit, :roth_ira_limit, :sy_ira_catchup_limit, :ee_med_th_limit, :ee_med_th_rate
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_fed_tax_table_nbr) OR (effective_date = '1/1/1900') OR (p_filler IS DISTINCT FROM filler) OR (p_exemption_amount IS DISTINCT FROM exemption_amount) OR (p_federal_minimum_wage IS DISTINCT FROM federal_minimum_wage) OR (p_federal_tip_credit IS DISTINCT FROM federal_tip_credit) OR (p_supplemental_tax_percentage IS DISTINCT FROM supplemental_tax_percentage) OR (p_oasdi_rate IS DISTINCT FROM oasdi_rate) OR (p_er_oasdi_rate IS DISTINCT FROM er_oasdi_rate) OR (p_ee_oasdi_rate IS DISTINCT FROM ee_oasdi_rate) OR (p_oasdi_wage_limit IS DISTINCT FROM oasdi_wage_limit) OR (p_medicare_rate IS DISTINCT FROM medicare_rate) OR (p_medicare_wage_limit IS DISTINCT FROM medicare_wage_limit) OR (p_fui_rate_real IS DISTINCT FROM fui_rate_real) OR (p_fui_rate_credit IS DISTINCT FROM fui_rate_credit) OR (p_fui_wage_limit IS DISTINCT FROM fui_wage_limit) OR (p_sy_401k_limit IS DISTINCT FROM sy_401k_limit) OR (p_sy_403b_limit IS DISTINCT FROM sy_403b_limit) OR (p_sy_457_limit IS DISTINCT FROM sy_457_limit) OR (p_sy_501c_limit IS DISTINCT FROM sy_501c_limit) OR (p_simple_limit IS DISTINCT FROM simple_limit) OR (p_sep_limit IS DISTINCT FROM sep_limit) OR (p_deferred_comp_limit IS DISTINCT FROM deferred_comp_limit) OR (p_fed_deposit_freq_threshold IS DISTINCT FROM fed_deposit_freq_threshold) OR (p_first_eic_limit IS DISTINCT FROM first_eic_limit) OR (p_first_eic_percentage IS DISTINCT FROM first_eic_percentage) OR (p_second_eic_limit IS DISTINCT FROM second_eic_limit) OR (p_second_eic_amount IS DISTINCT FROM second_eic_amount) OR (p_third_eic_additional_percent IS DISTINCT FROM third_eic_additional_percent) OR (p_sy_catch_up_limit IS DISTINCT FROM sy_catch_up_limit) OR (p_sy_dependent_care_limit IS DISTINCT FROM sy_dependent_care_limit) OR (p_sy_hsa_single_limit IS DISTINCT FROM sy_hsa_single_limit) OR (p_sy_hsa_family_limit IS DISTINCT FROM sy_hsa_family_limit) OR (p_sy_pension_catch_up_limit IS DISTINCT FROM sy_pension_catch_up_limit) OR (p_sy_simple_catch_up_limit IS DISTINCT FROM sy_simple_catch_up_limit) OR (p_compensation_limit IS DISTINCT FROM compensation_limit) OR (p_sy_hsa_catch_up_limit IS DISTINCT FROM sy_hsa_catch_up_limit) OR (p_s132_parking_limit IS DISTINCT FROM s132_parking_limit) OR (p_roth_ira_limit IS DISTINCT FROM roth_ira_limit) OR (p_sy_ira_catchup_limit IS DISTINCT FROM sy_ira_catchup_limit) OR (p_ee_med_th_limit IS DISTINCT FROM ee_med_th_limit) OR (p_ee_med_th_rate IS DISTINCT FROM ee_med_th_rate)) THEN
      BEGIN
        curr_nbr = sy_fed_tax_table_nbr;
        p_filler = filler;
        p_exemption_amount = exemption_amount;
        p_federal_minimum_wage = federal_minimum_wage;
        p_federal_tip_credit = federal_tip_credit;
        p_supplemental_tax_percentage = supplemental_tax_percentage;
        p_oasdi_rate = oasdi_rate;
        p_er_oasdi_rate = er_oasdi_rate;
        p_ee_oasdi_rate = ee_oasdi_rate;
        p_oasdi_wage_limit = oasdi_wage_limit;
        p_medicare_rate = medicare_rate;
        p_medicare_wage_limit = medicare_wage_limit;
        p_fui_rate_real = fui_rate_real;
        p_fui_rate_credit = fui_rate_credit;
        p_fui_wage_limit = fui_wage_limit;
        p_sy_401k_limit = sy_401k_limit;
        p_sy_403b_limit = sy_403b_limit;
        p_sy_457_limit = sy_457_limit;
        p_sy_501c_limit = sy_501c_limit;
        p_simple_limit = simple_limit;
        p_sep_limit = sep_limit;
        p_deferred_comp_limit = deferred_comp_limit;
        p_fed_deposit_freq_threshold = fed_deposit_freq_threshold;
        p_first_eic_limit = first_eic_limit;
        p_first_eic_percentage = first_eic_percentage;
        p_second_eic_limit = second_eic_limit;
        p_second_eic_amount = second_eic_amount;
        p_third_eic_additional_percent = third_eic_additional_percent;
        p_sy_catch_up_limit = sy_catch_up_limit;
        p_sy_dependent_care_limit = sy_dependent_care_limit;
        p_sy_hsa_single_limit = sy_hsa_single_limit;
        p_sy_hsa_family_limit = sy_hsa_family_limit;
        p_sy_pension_catch_up_limit = sy_pension_catch_up_limit;
        p_sy_simple_catch_up_limit = sy_simple_catch_up_limit;
        p_compensation_limit = compensation_limit;
        p_sy_hsa_catch_up_limit = sy_hsa_catch_up_limit;
        p_s132_parking_limit = s132_parking_limit;
        p_roth_ira_limit = roth_ira_limit;
        p_sy_ira_catchup_limit = sy_ira_catchup_limit;
        p_ee_med_th_limit = ee_med_th_limit;
        p_ee_med_th_rate = ee_med_th_rate;
      END
      ELSE
        DELETE FROM sy_fed_tax_table WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, filler, exemption_amount, federal_minimum_wage, federal_tip_credit, supplemental_tax_percentage, oasdi_rate, er_oasdi_rate, ee_oasdi_rate, oasdi_wage_limit, medicare_rate, medicare_wage_limit, fui_rate_real, fui_rate_credit, fui_wage_limit, sy_401k_limit, sy_403b_limit, sy_457_limit, sy_501c_limit, simple_limit, sep_limit, deferred_comp_limit, fed_deposit_freq_threshold, first_eic_limit, first_eic_percentage, second_eic_limit, second_eic_amount, third_eic_additional_percent, sy_catch_up_limit, sy_dependent_care_limit, sy_hsa_single_limit, sy_hsa_family_limit, sy_pension_catch_up_limit, sy_simple_catch_up_limit, compensation_limit, sy_hsa_catch_up_limit, s132_parking_limit, roth_ira_limit, sy_ira_catchup_limit, ee_med_th_limit, ee_med_th_rate
        FROM sy_fed_tax_table
        WHERE sy_fed_tax_table_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :filler, :exemption_amount, :federal_minimum_wage, :federal_tip_credit, :supplemental_tax_percentage, :oasdi_rate, :er_oasdi_rate, :ee_oasdi_rate, :oasdi_wage_limit, :medicare_rate, :medicare_wage_limit, :fui_rate_real, :fui_rate_credit, :fui_wage_limit, :sy_401k_limit, :sy_403b_limit, :sy_457_limit, :sy_501c_limit, :simple_limit, :sep_limit, :deferred_comp_limit, :fed_deposit_freq_threshold, :first_eic_limit, :first_eic_percentage, :second_eic_limit, :second_eic_amount, :third_eic_additional_percent, :sy_catch_up_limit, :sy_dependent_care_limit, :sy_hsa_single_limit, :sy_hsa_family_limit, :sy_pension_catch_up_limit, :sy_simple_catch_up_limit, :compensation_limit, :sy_hsa_catch_up_limit, :s132_parking_limit, :roth_ira_limit, :sy_ira_catchup_limit, :ee_med_th_limit, :ee_med_th_rate
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_filler IS DISTINCT FROM filler) OR (p_exemption_amount IS DISTINCT FROM exemption_amount) OR (p_federal_minimum_wage IS DISTINCT FROM federal_minimum_wage) OR (p_federal_tip_credit IS DISTINCT FROM federal_tip_credit) OR (p_supplemental_tax_percentage IS DISTINCT FROM supplemental_tax_percentage) OR (p_oasdi_rate IS DISTINCT FROM oasdi_rate) OR (p_er_oasdi_rate IS DISTINCT FROM er_oasdi_rate) OR (p_ee_oasdi_rate IS DISTINCT FROM ee_oasdi_rate) OR (p_oasdi_wage_limit IS DISTINCT FROM oasdi_wage_limit) OR (p_medicare_rate IS DISTINCT FROM medicare_rate) OR (p_medicare_wage_limit IS DISTINCT FROM medicare_wage_limit) OR (p_fui_rate_real IS DISTINCT FROM fui_rate_real) OR (p_fui_rate_credit IS DISTINCT FROM fui_rate_credit) OR (p_fui_wage_limit IS DISTINCT FROM fui_wage_limit) OR (p_sy_401k_limit IS DISTINCT FROM sy_401k_limit) OR (p_sy_403b_limit IS DISTINCT FROM sy_403b_limit) OR (p_sy_457_limit IS DISTINCT FROM sy_457_limit) OR (p_sy_501c_limit IS DISTINCT FROM sy_501c_limit) OR (p_simple_limit IS DISTINCT FROM simple_limit) OR (p_sep_limit IS DISTINCT FROM sep_limit) OR (p_deferred_comp_limit IS DISTINCT FROM deferred_comp_limit) OR (p_fed_deposit_freq_threshold IS DISTINCT FROM fed_deposit_freq_threshold) OR (p_first_eic_limit IS DISTINCT FROM first_eic_limit) OR (p_first_eic_percentage IS DISTINCT FROM first_eic_percentage) OR (p_second_eic_limit IS DISTINCT FROM second_eic_limit) OR (p_second_eic_amount IS DISTINCT FROM second_eic_amount) OR (p_third_eic_additional_percent IS DISTINCT FROM third_eic_additional_percent) OR (p_sy_catch_up_limit IS DISTINCT FROM sy_catch_up_limit) OR (p_sy_dependent_care_limit IS DISTINCT FROM sy_dependent_care_limit) OR (p_sy_hsa_single_limit IS DISTINCT FROM sy_hsa_single_limit) OR (p_sy_hsa_family_limit IS DISTINCT FROM sy_hsa_family_limit) OR (p_sy_pension_catch_up_limit IS DISTINCT FROM sy_pension_catch_up_limit) OR (p_sy_simple_catch_up_limit IS DISTINCT FROM sy_simple_catch_up_limit) OR (p_compensation_limit IS DISTINCT FROM compensation_limit) OR (p_sy_hsa_catch_up_limit IS DISTINCT FROM sy_hsa_catch_up_limit) OR (p_s132_parking_limit IS DISTINCT FROM s132_parking_limit) OR (p_roth_ira_limit IS DISTINCT FROM roth_ira_limit) OR (p_sy_ira_catchup_limit IS DISTINCT FROM sy_ira_catchup_limit) OR (p_ee_med_th_limit IS DISTINCT FROM ee_med_th_limit) OR (p_ee_med_th_rate IS DISTINCT FROM ee_med_th_rate)) THEN
      BEGIN
        p_filler = filler;
        p_exemption_amount = exemption_amount;
        p_federal_minimum_wage = federal_minimum_wage;
        p_federal_tip_credit = federal_tip_credit;
        p_supplemental_tax_percentage = supplemental_tax_percentage;
        p_oasdi_rate = oasdi_rate;
        p_er_oasdi_rate = er_oasdi_rate;
        p_ee_oasdi_rate = ee_oasdi_rate;
        p_oasdi_wage_limit = oasdi_wage_limit;
        p_medicare_rate = medicare_rate;
        p_medicare_wage_limit = medicare_wage_limit;
        p_fui_rate_real = fui_rate_real;
        p_fui_rate_credit = fui_rate_credit;
        p_fui_wage_limit = fui_wage_limit;
        p_sy_401k_limit = sy_401k_limit;
        p_sy_403b_limit = sy_403b_limit;
        p_sy_457_limit = sy_457_limit;
        p_sy_501c_limit = sy_501c_limit;
        p_simple_limit = simple_limit;
        p_sep_limit = sep_limit;
        p_deferred_comp_limit = deferred_comp_limit;
        p_fed_deposit_freq_threshold = fed_deposit_freq_threshold;
        p_first_eic_limit = first_eic_limit;
        p_first_eic_percentage = first_eic_percentage;
        p_second_eic_limit = second_eic_limit;
        p_second_eic_amount = second_eic_amount;
        p_third_eic_additional_percent = third_eic_additional_percent;
        p_sy_catch_up_limit = sy_catch_up_limit;
        p_sy_dependent_care_limit = sy_dependent_care_limit;
        p_sy_hsa_single_limit = sy_hsa_single_limit;
        p_sy_hsa_family_limit = sy_hsa_family_limit;
        p_sy_pension_catch_up_limit = sy_pension_catch_up_limit;
        p_sy_simple_catch_up_limit = sy_simple_catch_up_limit;
        p_compensation_limit = compensation_limit;
        p_sy_hsa_catch_up_limit = sy_hsa_catch_up_limit;
        p_s132_parking_limit = s132_parking_limit;
        p_roth_ira_limit = roth_ira_limit;
        p_sy_ira_catchup_limit = sy_ira_catchup_limit;
        p_ee_med_th_limit = ee_med_th_limit;
        p_ee_med_th_rate = ee_med_th_rate;
      END
      ELSE
        DELETE FROM sy_fed_tax_table WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_FED_TAX_TABLE TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_fed_tax_table_brackets(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_fed_tax_table_brackets_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE marital_status CHAR(1);
DECLARE VARIABLE p_marital_status CHAR(1);
DECLARE VARIABLE greater_than_value NUMERIC(18,6);
DECLARE VARIABLE p_greater_than_value NUMERIC(18,6);
DECLARE VARIABLE less_than_value NUMERIC(18,6);
DECLARE VARIABLE p_less_than_value NUMERIC(18,6);
DECLARE VARIABLE percentage NUMERIC(18,6);
DECLARE VARIABLE p_percentage NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_fed_tax_table_brackets_nbr , marital_status, greater_than_value, less_than_value, percentage
        FROM sy_fed_tax_table_brackets
        ORDER BY sy_fed_tax_table_brackets_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_fed_tax_table_brackets_nbr, :marital_status, :greater_than_value, :less_than_value, :percentage
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_fed_tax_table_brackets_nbr) OR (effective_date = '1/1/1900') OR (p_marital_status IS DISTINCT FROM marital_status) OR (p_greater_than_value IS DISTINCT FROM greater_than_value) OR (p_less_than_value IS DISTINCT FROM less_than_value) OR (p_percentage IS DISTINCT FROM percentage)) THEN
      BEGIN
        curr_nbr = sy_fed_tax_table_brackets_nbr;
        p_marital_status = marital_status;
        p_greater_than_value = greater_than_value;
        p_less_than_value = less_than_value;
        p_percentage = percentage;
      END
      ELSE
        DELETE FROM sy_fed_tax_table_brackets WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, marital_status, greater_than_value, less_than_value, percentage
        FROM sy_fed_tax_table_brackets
        WHERE sy_fed_tax_table_brackets_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :marital_status, :greater_than_value, :less_than_value, :percentage
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_marital_status IS DISTINCT FROM marital_status) OR (p_greater_than_value IS DISTINCT FROM greater_than_value) OR (p_less_than_value IS DISTINCT FROM less_than_value) OR (p_percentage IS DISTINCT FROM percentage)) THEN
      BEGIN
        p_marital_status = marital_status;
        p_greater_than_value = greater_than_value;
        p_less_than_value = less_than_value;
        p_percentage = percentage;
      END
      ELSE
        DELETE FROM sy_fed_tax_table_brackets WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_FED_TAX_TABLE_BRACKETS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_global_agency(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_global_agency_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE agency_name VARCHAR(40);
DECLARE VARIABLE p_agency_name VARCHAR(40);
DECLARE VARIABLE accepts_check CHAR(1);
DECLARE VARIABLE p_accepts_check CHAR(1);
DECLARE VARIABLE accepts_debit CHAR(1);
DECLARE VARIABLE p_accepts_debit CHAR(1);
DECLARE VARIABLE accepts_credit CHAR(1);
DECLARE VARIABLE p_accepts_credit CHAR(1);
DECLARE VARIABLE negative_direct_dep_allowed CHAR(1);
DECLARE VARIABLE p_negative_direct_dep_allowed CHAR(1);
DECLARE VARIABLE ignore_holidays CHAR(1);
DECLARE VARIABLE p_ignore_holidays CHAR(1);
DECLARE VARIABLE ignore_weekends CHAR(1);
DECLARE VARIABLE p_ignore_weekends CHAR(1);
DECLARE VARIABLE payment_file_dest_direct VARCHAR(64);
DECLARE VARIABLE p_payment_file_dest_direct VARCHAR(64);
DECLARE VARIABLE receiving_aba_number VARCHAR(9);
DECLARE VARIABLE p_receiving_aba_number VARCHAR(9);
DECLARE VARIABLE receiving_account_number VARCHAR(20);
DECLARE VARIABLE p_receiving_account_number VARCHAR(20);
DECLARE VARIABLE receiving_account_type CHAR(1);
DECLARE VARIABLE p_receiving_account_type CHAR(1);
DECLARE VARIABLE mag_media CHAR(1);
DECLARE VARIABLE p_mag_media CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE p_state CHAR(2);
DECLARE VARIABLE county VARCHAR(40);
DECLARE VARIABLE p_county VARCHAR(40);
DECLARE VARIABLE tax_payment_field_office_nbr INTEGER;
DECLARE VARIABLE p_tax_payment_field_office_nbr INTEGER;
DECLARE VARIABLE notes BLOB;
DECLARE VARIABLE p_notes BLOB;
DECLARE VARIABLE send_zero_eft CHAR(1);
DECLARE VARIABLE p_send_zero_eft CHAR(1);
DECLARE VARIABLE send_zero_coupon CHAR(1);
DECLARE VARIABLE p_send_zero_coupon CHAR(1);
DECLARE VARIABLE print_zero_return CHAR(1);
DECLARE VARIABLE p_print_zero_return CHAR(1);
DECLARE VARIABLE custom_debit_sy_reports_nbr INTEGER;
DECLARE VARIABLE p_custom_debit_sy_reports_nbr INTEGER;
DECLARE VARIABLE custom_debit_after_status CHAR(1);
DECLARE VARIABLE p_custom_debit_after_status CHAR(1);
DECLARE VARIABLE custom_debit_post_to_register CHAR(1);
DECLARE VARIABLE p_custom_debit_post_to_register CHAR(1);
DECLARE VARIABLE check_one_payment_type CHAR(1);
DECLARE VARIABLE p_check_one_payment_type CHAR(1);
DECLARE VARIABLE eftp_one_payment_type CHAR(1);
DECLARE VARIABLE p_eftp_one_payment_type CHAR(1);
DECLARE VARIABLE ach_one_payment_type CHAR(1);
DECLARE VARIABLE p_ach_one_payment_type CHAR(1);
DECLARE VARIABLE agency_type CHAR(1);
DECLARE VARIABLE p_agency_type CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_global_agency_nbr , agency_name, accepts_check, accepts_debit, accepts_credit, negative_direct_dep_allowed, ignore_holidays, ignore_weekends, payment_file_dest_direct, receiving_aba_number, receiving_account_number, receiving_account_type, mag_media, filler, state, county, tax_payment_field_office_nbr, notes, send_zero_eft, send_zero_coupon, print_zero_return, custom_debit_sy_reports_nbr, custom_debit_after_status, custom_debit_post_to_register, check_one_payment_type, eftp_one_payment_type, ach_one_payment_type, agency_type
        FROM sy_global_agency
        ORDER BY sy_global_agency_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_global_agency_nbr, :agency_name, :accepts_check, :accepts_debit, :accepts_credit, :negative_direct_dep_allowed, :ignore_holidays, :ignore_weekends, :payment_file_dest_direct, :receiving_aba_number, :receiving_account_number, :receiving_account_type, :mag_media, :filler, :state, :county, :tax_payment_field_office_nbr, :notes, :send_zero_eft, :send_zero_coupon, :print_zero_return, :custom_debit_sy_reports_nbr, :custom_debit_after_status, :custom_debit_post_to_register, :check_one_payment_type, :eftp_one_payment_type, :ach_one_payment_type, :agency_type
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_global_agency_nbr) OR (effective_date = '1/1/1900') OR (p_agency_name IS DISTINCT FROM agency_name) OR (p_accepts_check IS DISTINCT FROM accepts_check) OR (p_accepts_debit IS DISTINCT FROM accepts_debit) OR (p_accepts_credit IS DISTINCT FROM accepts_credit) OR (p_negative_direct_dep_allowed IS DISTINCT FROM negative_direct_dep_allowed) OR (p_ignore_holidays IS DISTINCT FROM ignore_holidays) OR (p_ignore_weekends IS DISTINCT FROM ignore_weekends) OR (p_payment_file_dest_direct IS DISTINCT FROM payment_file_dest_direct) OR (p_receiving_aba_number IS DISTINCT FROM receiving_aba_number) OR (p_receiving_account_number IS DISTINCT FROM receiving_account_number) OR (p_receiving_account_type IS DISTINCT FROM receiving_account_type) OR (p_mag_media IS DISTINCT FROM mag_media) OR (p_filler IS DISTINCT FROM filler) OR (p_state IS DISTINCT FROM state) OR (p_county IS DISTINCT FROM county) OR (p_tax_payment_field_office_nbr IS DISTINCT FROM tax_payment_field_office_nbr) OR (p_notes IS DISTINCT FROM notes) OR (p_send_zero_eft IS DISTINCT FROM send_zero_eft) OR (p_send_zero_coupon IS DISTINCT FROM send_zero_coupon) OR (p_print_zero_return IS DISTINCT FROM print_zero_return) OR (p_custom_debit_sy_reports_nbr IS DISTINCT FROM custom_debit_sy_reports_nbr) OR (p_custom_debit_after_status IS DISTINCT FROM custom_debit_after_status) OR (p_custom_debit_post_to_register IS DISTINCT FROM custom_debit_post_to_register) OR (p_check_one_payment_type IS DISTINCT FROM check_one_payment_type) OR (p_eftp_one_payment_type IS DISTINCT FROM eftp_one_payment_type) OR (p_ach_one_payment_type IS DISTINCT FROM ach_one_payment_type) OR (p_agency_type IS DISTINCT FROM agency_type)) THEN
      BEGIN
        curr_nbr = sy_global_agency_nbr;
        p_agency_name = agency_name;
        p_accepts_check = accepts_check;
        p_accepts_debit = accepts_debit;
        p_accepts_credit = accepts_credit;
        p_negative_direct_dep_allowed = negative_direct_dep_allowed;
        p_ignore_holidays = ignore_holidays;
        p_ignore_weekends = ignore_weekends;
        p_payment_file_dest_direct = payment_file_dest_direct;
        p_receiving_aba_number = receiving_aba_number;
        p_receiving_account_number = receiving_account_number;
        p_receiving_account_type = receiving_account_type;
        p_mag_media = mag_media;
        p_filler = filler;
        p_state = state;
        p_county = county;
        p_tax_payment_field_office_nbr = tax_payment_field_office_nbr;
        p_notes = notes;
        p_send_zero_eft = send_zero_eft;
        p_send_zero_coupon = send_zero_coupon;
        p_print_zero_return = print_zero_return;
        p_custom_debit_sy_reports_nbr = custom_debit_sy_reports_nbr;
        p_custom_debit_after_status = custom_debit_after_status;
        p_custom_debit_post_to_register = custom_debit_post_to_register;
        p_check_one_payment_type = check_one_payment_type;
        p_eftp_one_payment_type = eftp_one_payment_type;
        p_ach_one_payment_type = ach_one_payment_type;
        p_agency_type = agency_type;
      END
      ELSE
        DELETE FROM sy_global_agency WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, agency_name, accepts_check, accepts_debit, accepts_credit, negative_direct_dep_allowed, ignore_holidays, ignore_weekends, payment_file_dest_direct, receiving_aba_number, receiving_account_number, receiving_account_type, mag_media, filler, state, county, tax_payment_field_office_nbr, notes, send_zero_eft, send_zero_coupon, print_zero_return, custom_debit_sy_reports_nbr, custom_debit_after_status, custom_debit_post_to_register, check_one_payment_type, eftp_one_payment_type, ach_one_payment_type, agency_type
        FROM sy_global_agency
        WHERE sy_global_agency_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :agency_name, :accepts_check, :accepts_debit, :accepts_credit, :negative_direct_dep_allowed, :ignore_holidays, :ignore_weekends, :payment_file_dest_direct, :receiving_aba_number, :receiving_account_number, :receiving_account_type, :mag_media, :filler, :state, :county, :tax_payment_field_office_nbr, :notes, :send_zero_eft, :send_zero_coupon, :print_zero_return, :custom_debit_sy_reports_nbr, :custom_debit_after_status, :custom_debit_post_to_register, :check_one_payment_type, :eftp_one_payment_type, :ach_one_payment_type, :agency_type
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_agency_name IS DISTINCT FROM agency_name) OR (p_accepts_check IS DISTINCT FROM accepts_check) OR (p_accepts_debit IS DISTINCT FROM accepts_debit) OR (p_accepts_credit IS DISTINCT FROM accepts_credit) OR (p_negative_direct_dep_allowed IS DISTINCT FROM negative_direct_dep_allowed) OR (p_ignore_holidays IS DISTINCT FROM ignore_holidays) OR (p_ignore_weekends IS DISTINCT FROM ignore_weekends) OR (p_payment_file_dest_direct IS DISTINCT FROM payment_file_dest_direct) OR (p_receiving_aba_number IS DISTINCT FROM receiving_aba_number) OR (p_receiving_account_number IS DISTINCT FROM receiving_account_number) OR (p_receiving_account_type IS DISTINCT FROM receiving_account_type) OR (p_mag_media IS DISTINCT FROM mag_media) OR (p_filler IS DISTINCT FROM filler) OR (p_state IS DISTINCT FROM state) OR (p_county IS DISTINCT FROM county) OR (p_tax_payment_field_office_nbr IS DISTINCT FROM tax_payment_field_office_nbr) OR (p_notes IS DISTINCT FROM notes) OR (p_send_zero_eft IS DISTINCT FROM send_zero_eft) OR (p_send_zero_coupon IS DISTINCT FROM send_zero_coupon) OR (p_print_zero_return IS DISTINCT FROM print_zero_return) OR (p_custom_debit_sy_reports_nbr IS DISTINCT FROM custom_debit_sy_reports_nbr) OR (p_custom_debit_after_status IS DISTINCT FROM custom_debit_after_status) OR (p_custom_debit_post_to_register IS DISTINCT FROM custom_debit_post_to_register) OR (p_check_one_payment_type IS DISTINCT FROM check_one_payment_type) OR (p_eftp_one_payment_type IS DISTINCT FROM eftp_one_payment_type) OR (p_ach_one_payment_type IS DISTINCT FROM ach_one_payment_type) OR (p_agency_type IS DISTINCT FROM agency_type)) THEN
      BEGIN
        p_agency_name = agency_name;
        p_accepts_check = accepts_check;
        p_accepts_debit = accepts_debit;
        p_accepts_credit = accepts_credit;
        p_negative_direct_dep_allowed = negative_direct_dep_allowed;
        p_ignore_holidays = ignore_holidays;
        p_ignore_weekends = ignore_weekends;
        p_payment_file_dest_direct = payment_file_dest_direct;
        p_receiving_aba_number = receiving_aba_number;
        p_receiving_account_number = receiving_account_number;
        p_receiving_account_type = receiving_account_type;
        p_mag_media = mag_media;
        p_filler = filler;
        p_state = state;
        p_county = county;
        p_tax_payment_field_office_nbr = tax_payment_field_office_nbr;
        p_notes = notes;
        p_send_zero_eft = send_zero_eft;
        p_send_zero_coupon = send_zero_coupon;
        p_print_zero_return = print_zero_return;
        p_custom_debit_sy_reports_nbr = custom_debit_sy_reports_nbr;
        p_custom_debit_after_status = custom_debit_after_status;
        p_custom_debit_post_to_register = custom_debit_post_to_register;
        p_check_one_payment_type = check_one_payment_type;
        p_eftp_one_payment_type = eftp_one_payment_type;
        p_ach_one_payment_type = ach_one_payment_type;
        p_agency_type = agency_type;
      END
      ELSE
        DELETE FROM sy_global_agency WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_GLOBAL_AGENCY TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_gl_agency_field_office(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE additional_name VARCHAR(40);
DECLARE VARIABLE p_additional_name VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE p_address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE p_address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE p_city VARCHAR(20);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE p_zip_code VARCHAR(10);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE p_state CHAR(2);
DECLARE VARIABLE attention_name VARCHAR(40);
DECLARE VARIABLE p_attention_name VARCHAR(40);
DECLARE VARIABLE contact1 VARCHAR(30);
DECLARE VARIABLE p_contact1 VARCHAR(30);
DECLARE VARIABLE phone1 VARCHAR(20);
DECLARE VARIABLE p_phone1 VARCHAR(20);
DECLARE VARIABLE description1 VARCHAR(10);
DECLARE VARIABLE p_description1 VARCHAR(10);
DECLARE VARIABLE contact2 VARCHAR(30);
DECLARE VARIABLE p_contact2 VARCHAR(30);
DECLARE VARIABLE phone2 VARCHAR(20);
DECLARE VARIABLE p_phone2 VARCHAR(20);
DECLARE VARIABLE description2 VARCHAR(10);
DECLARE VARIABLE p_description2 VARCHAR(10);
DECLARE VARIABLE fax VARCHAR(20);
DECLARE VARIABLE p_fax VARCHAR(20);
DECLARE VARIABLE fax_description VARCHAR(10);
DECLARE VARIABLE p_fax_description VARCHAR(10);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE category_type CHAR(1);
DECLARE VARIABLE p_category_type CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_gl_agency_field_office_nbr , additional_name, address1, address2, city, zip_code, state, attention_name, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, filler, category_type
        FROM sy_gl_agency_field_office
        ORDER BY sy_gl_agency_field_office_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_gl_agency_field_office_nbr, :additional_name, :address1, :address2, :city, :zip_code, :state, :attention_name, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :filler, :category_type
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (effective_date = '1/1/1900') OR (p_additional_name IS DISTINCT FROM additional_name) OR (p_address1 IS DISTINCT FROM address1) OR (p_address2 IS DISTINCT FROM address2) OR (p_city IS DISTINCT FROM city) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_state IS DISTINCT FROM state) OR (p_attention_name IS DISTINCT FROM attention_name) OR (p_contact1 IS DISTINCT FROM contact1) OR (p_phone1 IS DISTINCT FROM phone1) OR (p_description1 IS DISTINCT FROM description1) OR (p_contact2 IS DISTINCT FROM contact2) OR (p_phone2 IS DISTINCT FROM phone2) OR (p_description2 IS DISTINCT FROM description2) OR (p_fax IS DISTINCT FROM fax) OR (p_fax_description IS DISTINCT FROM fax_description) OR (p_filler IS DISTINCT FROM filler) OR (p_category_type IS DISTINCT FROM category_type)) THEN
      BEGIN
        curr_nbr = sy_gl_agency_field_office_nbr;
        p_additional_name = additional_name;
        p_address1 = address1;
        p_address2 = address2;
        p_city = city;
        p_zip_code = zip_code;
        p_state = state;
        p_attention_name = attention_name;
        p_contact1 = contact1;
        p_phone1 = phone1;
        p_description1 = description1;
        p_contact2 = contact2;
        p_phone2 = phone2;
        p_description2 = description2;
        p_fax = fax;
        p_fax_description = fax_description;
        p_filler = filler;
        p_category_type = category_type;
      END
      ELSE
        DELETE FROM sy_gl_agency_field_office WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, additional_name, address1, address2, city, zip_code, state, attention_name, contact1, phone1, description1, contact2, phone2, description2, fax, fax_description, filler, category_type
        FROM sy_gl_agency_field_office
        WHERE sy_gl_agency_field_office_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :additional_name, :address1, :address2, :city, :zip_code, :state, :attention_name, :contact1, :phone1, :description1, :contact2, :phone2, :description2, :fax, :fax_description, :filler, :category_type
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_additional_name IS DISTINCT FROM additional_name) OR (p_address1 IS DISTINCT FROM address1) OR (p_address2 IS DISTINCT FROM address2) OR (p_city IS DISTINCT FROM city) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_state IS DISTINCT FROM state) OR (p_attention_name IS DISTINCT FROM attention_name) OR (p_contact1 IS DISTINCT FROM contact1) OR (p_phone1 IS DISTINCT FROM phone1) OR (p_description1 IS DISTINCT FROM description1) OR (p_contact2 IS DISTINCT FROM contact2) OR (p_phone2 IS DISTINCT FROM phone2) OR (p_description2 IS DISTINCT FROM description2) OR (p_fax IS DISTINCT FROM fax) OR (p_fax_description IS DISTINCT FROM fax_description) OR (p_filler IS DISTINCT FROM filler) OR (p_category_type IS DISTINCT FROM category_type)) THEN
      BEGIN
        p_additional_name = additional_name;
        p_address1 = address1;
        p_address2 = address2;
        p_city = city;
        p_zip_code = zip_code;
        p_state = state;
        p_attention_name = attention_name;
        p_contact1 = contact1;
        p_phone1 = phone1;
        p_description1 = description1;
        p_contact2 = contact2;
        p_phone2 = phone2;
        p_description2 = description2;
        p_fax = fax;
        p_fax_description = fax_description;
        p_filler = filler;
        p_category_type = category_type;
      END
      ELSE
        DELETE FROM sy_gl_agency_field_office WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_GL_AGENCY_FIELD_OFFICE TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_gl_agency_holidays(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_gl_agency_holidays_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE holiday_name VARCHAR(40);
DECLARE VARIABLE p_holiday_name VARCHAR(40);
DECLARE VARIABLE calc_type CHAR(1);
DECLARE VARIABLE p_calc_type CHAR(1);
DECLARE VARIABLE week_number SMALLINT;
DECLARE VARIABLE p_week_number SMALLINT;
DECLARE VARIABLE day_of_week SMALLINT;
DECLARE VARIABLE p_day_of_week SMALLINT;
DECLARE VARIABLE month_number SMALLINT;
DECLARE VARIABLE p_month_number SMALLINT;
DECLARE VARIABLE day_number SMALLINT;
DECLARE VARIABLE p_day_number SMALLINT;
DECLARE VARIABLE saturday_offset INTEGER;
DECLARE VARIABLE p_saturday_offset INTEGER;
DECLARE VARIABLE sunday_offset SMALLINT;
DECLARE VARIABLE p_sunday_offset SMALLINT;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_gl_agency_holidays_nbr , holiday_name, calc_type, week_number, day_of_week, month_number, day_number, saturday_offset, sunday_offset
        FROM sy_gl_agency_holidays
        ORDER BY sy_gl_agency_holidays_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_gl_agency_holidays_nbr, :holiday_name, :calc_type, :week_number, :day_of_week, :month_number, :day_number, :saturday_offset, :sunday_offset
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_gl_agency_holidays_nbr) OR (effective_date = '1/1/1900') OR (p_holiday_name IS DISTINCT FROM holiday_name) OR (p_calc_type IS DISTINCT FROM calc_type) OR (p_week_number IS DISTINCT FROM week_number) OR (p_day_of_week IS DISTINCT FROM day_of_week) OR (p_month_number IS DISTINCT FROM month_number) OR (p_day_number IS DISTINCT FROM day_number) OR (p_saturday_offset IS DISTINCT FROM saturday_offset) OR (p_sunday_offset IS DISTINCT FROM sunday_offset)) THEN
      BEGIN
        curr_nbr = sy_gl_agency_holidays_nbr;
        p_holiday_name = holiday_name;
        p_calc_type = calc_type;
        p_week_number = week_number;
        p_day_of_week = day_of_week;
        p_month_number = month_number;
        p_day_number = day_number;
        p_saturday_offset = saturday_offset;
        p_sunday_offset = sunday_offset;
      END
      ELSE
        DELETE FROM sy_gl_agency_holidays WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, holiday_name, calc_type, week_number, day_of_week, month_number, day_number, saturday_offset, sunday_offset
        FROM sy_gl_agency_holidays
        WHERE sy_gl_agency_holidays_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :holiday_name, :calc_type, :week_number, :day_of_week, :month_number, :day_number, :saturday_offset, :sunday_offset
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_holiday_name IS DISTINCT FROM holiday_name) OR (p_calc_type IS DISTINCT FROM calc_type) OR (p_week_number IS DISTINCT FROM week_number) OR (p_day_of_week IS DISTINCT FROM day_of_week) OR (p_month_number IS DISTINCT FROM month_number) OR (p_day_number IS DISTINCT FROM day_number) OR (p_saturday_offset IS DISTINCT FROM saturday_offset) OR (p_sunday_offset IS DISTINCT FROM sunday_offset)) THEN
      BEGIN
        p_holiday_name = holiday_name;
        p_calc_type = calc_type;
        p_week_number = week_number;
        p_day_of_week = day_of_week;
        p_month_number = month_number;
        p_day_number = day_number;
        p_saturday_offset = saturday_offset;
        p_sunday_offset = sunday_offset;
      END
      ELSE
        DELETE FROM sy_gl_agency_holidays WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_GL_AGENCY_HOLIDAYS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_gl_agency_report(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_gl_agency_report_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE system_tax_type CHAR(1);
DECLARE VARIABLE p_system_tax_type CHAR(1);
DECLARE VARIABLE deposit_frequency CHAR(1);
DECLARE VARIABLE p_deposit_frequency CHAR(1);
DECLARE VARIABLE sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE p_sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE when_due_type CHAR(1);
DECLARE VARIABLE p_when_due_type CHAR(1);
DECLARE VARIABLE number_of_days_for_when_due INTEGER;
DECLARE VARIABLE p_number_of_days_for_when_due INTEGER;
DECLARE VARIABLE return_frequency CHAR(1);
DECLARE VARIABLE p_return_frequency CHAR(1);
DECLARE VARIABLE enlist_automatically CHAR(1);
DECLARE VARIABLE p_enlist_automatically CHAR(1);
DECLARE VARIABLE print_when CHAR(1);
DECLARE VARIABLE p_print_when CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE tax_service_filter CHAR(1);
DECLARE VARIABLE p_tax_service_filter CHAR(1);
DECLARE VARIABLE consolidated_filter CHAR(1);
DECLARE VARIABLE p_consolidated_filter CHAR(1);
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_group_nbr INTEGER;
DECLARE VARIABLE tax_return_active CHAR(1);
DECLARE VARIABLE p_tax_return_active CHAR(1);
DECLARE VARIABLE begin_effective_month INTEGER;
DECLARE VARIABLE p_begin_effective_month INTEGER;
DECLARE VARIABLE end_effective_month INTEGER;
DECLARE VARIABLE p_end_effective_month INTEGER;
DECLARE VARIABLE payment_method CHAR(1);
DECLARE VARIABLE p_payment_method CHAR(1);
DECLARE VARIABLE tax945_checks CHAR(1);
DECLARE VARIABLE p_tax945_checks CHAR(1);
DECLARE VARIABLE tax943_company CHAR(1);
DECLARE VARIABLE p_tax943_company CHAR(1);
DECLARE VARIABLE tax944_company CHAR(1);
DECLARE VARIABLE p_tax944_company CHAR(1);
DECLARE VARIABLE taxreturn940 CHAR(1);
DECLARE VARIABLE p_taxreturn940 CHAR(1);
DECLARE VARIABLE tax_return_inactive_start_date TIMESTAMP;
DECLARE VARIABLE p_tax_return_inactive_start_date TIMESTAMP;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_gl_agency_report_nbr , system_tax_type, deposit_frequency, sy_gl_agency_field_office_nbr, when_due_type, number_of_days_for_when_due, return_frequency, enlist_automatically, print_when, filler, tax_service_filter, consolidated_filter, sy_reports_group_nbr, tax_return_active, begin_effective_month, end_effective_month, payment_method, tax945_checks, tax943_company, tax944_company, taxreturn940, tax_return_inactive_start_date
        FROM sy_gl_agency_report
        ORDER BY sy_gl_agency_report_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_gl_agency_report_nbr, :system_tax_type, :deposit_frequency, :sy_gl_agency_field_office_nbr, :when_due_type, :number_of_days_for_when_due, :return_frequency, :enlist_automatically, :print_when, :filler, :tax_service_filter, :consolidated_filter, :sy_reports_group_nbr, :tax_return_active, :begin_effective_month, :end_effective_month, :payment_method, :tax945_checks, :tax943_company, :tax944_company, :taxreturn940, :tax_return_inactive_start_date
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_gl_agency_report_nbr) OR (effective_date = '1/1/1900') OR (p_system_tax_type IS DISTINCT FROM system_tax_type) OR (p_deposit_frequency IS DISTINCT FROM deposit_frequency) OR (p_sy_gl_agency_field_office_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (p_when_due_type IS DISTINCT FROM when_due_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_return_frequency IS DISTINCT FROM return_frequency) OR (p_enlist_automatically IS DISTINCT FROM enlist_automatically) OR (p_print_when IS DISTINCT FROM print_when) OR (p_filler IS DISTINCT FROM filler) OR (p_tax_service_filter IS DISTINCT FROM tax_service_filter) OR (p_consolidated_filter IS DISTINCT FROM consolidated_filter) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (p_tax_return_active IS DISTINCT FROM tax_return_active) OR (p_begin_effective_month IS DISTINCT FROM begin_effective_month) OR (p_end_effective_month IS DISTINCT FROM end_effective_month) OR (p_payment_method IS DISTINCT FROM payment_method) OR (p_tax945_checks IS DISTINCT FROM tax945_checks) OR (p_tax943_company IS DISTINCT FROM tax943_company) OR (p_tax944_company IS DISTINCT FROM tax944_company) OR (p_taxreturn940 IS DISTINCT FROM taxreturn940) OR (p_tax_return_inactive_start_date IS DISTINCT FROM tax_return_inactive_start_date)) THEN
      BEGIN
        curr_nbr = sy_gl_agency_report_nbr;
        p_system_tax_type = system_tax_type;
        p_deposit_frequency = deposit_frequency;
        p_sy_gl_agency_field_office_nbr = sy_gl_agency_field_office_nbr;
        p_when_due_type = when_due_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_return_frequency = return_frequency;
        p_enlist_automatically = enlist_automatically;
        p_print_when = print_when;
        p_filler = filler;
        p_tax_service_filter = tax_service_filter;
        p_consolidated_filter = consolidated_filter;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
        p_tax_return_active = tax_return_active;
        p_begin_effective_month = begin_effective_month;
        p_end_effective_month = end_effective_month;
        p_payment_method = payment_method;
        p_tax945_checks = tax945_checks;
        p_tax943_company = tax943_company;
        p_tax944_company = tax944_company;
        p_taxreturn940 = taxreturn940;
        p_tax_return_inactive_start_date = tax_return_inactive_start_date;
      END
      ELSE
        DELETE FROM sy_gl_agency_report WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, system_tax_type, deposit_frequency, sy_gl_agency_field_office_nbr, when_due_type, number_of_days_for_when_due, return_frequency, enlist_automatically, print_when, filler, tax_service_filter, consolidated_filter, sy_reports_group_nbr, tax_return_active, begin_effective_month, end_effective_month, payment_method, tax945_checks, tax943_company, tax944_company, taxreturn940, tax_return_inactive_start_date
        FROM sy_gl_agency_report
        WHERE sy_gl_agency_report_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :system_tax_type, :deposit_frequency, :sy_gl_agency_field_office_nbr, :when_due_type, :number_of_days_for_when_due, :return_frequency, :enlist_automatically, :print_when, :filler, :tax_service_filter, :consolidated_filter, :sy_reports_group_nbr, :tax_return_active, :begin_effective_month, :end_effective_month, :payment_method, :tax945_checks, :tax943_company, :tax944_company, :taxreturn940, :tax_return_inactive_start_date
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_system_tax_type IS DISTINCT FROM system_tax_type) OR (p_deposit_frequency IS DISTINCT FROM deposit_frequency) OR (p_sy_gl_agency_field_office_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (p_when_due_type IS DISTINCT FROM when_due_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_return_frequency IS DISTINCT FROM return_frequency) OR (p_enlist_automatically IS DISTINCT FROM enlist_automatically) OR (p_print_when IS DISTINCT FROM print_when) OR (p_filler IS DISTINCT FROM filler) OR (p_tax_service_filter IS DISTINCT FROM tax_service_filter) OR (p_consolidated_filter IS DISTINCT FROM consolidated_filter) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (p_tax_return_active IS DISTINCT FROM tax_return_active) OR (p_begin_effective_month IS DISTINCT FROM begin_effective_month) OR (p_end_effective_month IS DISTINCT FROM end_effective_month) OR (p_payment_method IS DISTINCT FROM payment_method) OR (p_tax945_checks IS DISTINCT FROM tax945_checks) OR (p_tax943_company IS DISTINCT FROM tax943_company) OR (p_tax944_company IS DISTINCT FROM tax944_company) OR (p_taxreturn940 IS DISTINCT FROM taxreturn940) OR (p_tax_return_inactive_start_date IS DISTINCT FROM tax_return_inactive_start_date)) THEN
      BEGIN
        p_system_tax_type = system_tax_type;
        p_deposit_frequency = deposit_frequency;
        p_sy_gl_agency_field_office_nbr = sy_gl_agency_field_office_nbr;
        p_when_due_type = when_due_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_return_frequency = return_frequency;
        p_enlist_automatically = enlist_automatically;
        p_print_when = print_when;
        p_filler = filler;
        p_tax_service_filter = tax_service_filter;
        p_consolidated_filter = consolidated_filter;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
        p_tax_return_active = tax_return_active;
        p_begin_effective_month = begin_effective_month;
        p_end_effective_month = end_effective_month;
        p_payment_method = payment_method;
        p_tax945_checks = tax945_checks;
        p_tax943_company = tax943_company;
        p_tax944_company = tax944_company;
        p_taxreturn940 = taxreturn940;
        p_tax_return_inactive_start_date = tax_return_inactive_start_date;
      END
      ELSE
        DELETE FROM sy_gl_agency_report WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_GL_AGENCY_REPORT TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_locals(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_locals_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE p_name VARCHAR(40);
DECLARE VARIABLE local_type CHAR(1);
DECLARE VARIABLE p_local_type CHAR(1);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE p_zip_code VARCHAR(10);
DECLARE VARIABLE local_tax_identifier VARCHAR(20);
DECLARE VARIABLE p_local_tax_identifier VARCHAR(20);
DECLARE VARIABLE agency_number INTEGER;
DECLARE VARIABLE p_agency_number INTEGER;
DECLARE VARIABLE minimum_hours_worked_per CHAR(1);
DECLARE VARIABLE p_minimum_hours_worked_per CHAR(1);
DECLARE VARIABLE calculation_method CHAR(1);
DECLARE VARIABLE p_calculation_method CHAR(1);
DECLARE VARIABLE print_return_if_zero CHAR(1);
DECLARE VARIABLE p_print_return_if_zero CHAR(1);
DECLARE VARIABLE use_misc_tax_return_code CHAR(1);
DECLARE VARIABLE p_use_misc_tax_return_code CHAR(1);
DECLARE VARIABLE sy_local_tax_pmt_agency_nbr INTEGER;
DECLARE VARIABLE p_sy_local_tax_pmt_agency_nbr INTEGER;
DECLARE VARIABLE sy_tax_pmt_report_nbr INTEGER;
DECLARE VARIABLE p_sy_tax_pmt_report_nbr INTEGER;
DECLARE VARIABLE sy_local_reporting_agency_nbr INTEGER;
DECLARE VARIABLE p_sy_local_reporting_agency_nbr INTEGER;
DECLARE VARIABLE tax_deposit_follow_state CHAR(1);
DECLARE VARIABLE p_tax_deposit_follow_state CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE local_active CHAR(1);
DECLARE VARIABLE p_local_active CHAR(1);
DECLARE VARIABLE round_to_nearest_dollar CHAR(1);
DECLARE VARIABLE p_round_to_nearest_dollar CHAR(1);
DECLARE VARIABLE tax_type CHAR(1);
DECLARE VARIABLE p_tax_type CHAR(1);
DECLARE VARIABLE tax_frequency CHAR(1);
DECLARE VARIABLE p_tax_frequency CHAR(1);
DECLARE VARIABLE local_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE p_local_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE tax_rate NUMERIC(18,6);
DECLARE VARIABLE p_tax_rate NUMERIC(18,6);
DECLARE VARIABLE tax_amount NUMERIC(18,6);
DECLARE VARIABLE p_tax_amount NUMERIC(18,6);
DECLARE VARIABLE tax_maximum NUMERIC(18,6);
DECLARE VARIABLE p_tax_maximum NUMERIC(18,6);
DECLARE VARIABLE wage_maximum NUMERIC(18,6);
DECLARE VARIABLE p_wage_maximum NUMERIC(18,6);
DECLARE VARIABLE weekly_tax_cap NUMERIC(18,6);
DECLARE VARIABLE p_weekly_tax_cap NUMERIC(18,6);
DECLARE VARIABLE minimum_hours_worked NUMERIC(18,6);
DECLARE VARIABLE p_minimum_hours_worked NUMERIC(18,6);
DECLARE VARIABLE miscellaneous_amount NUMERIC(18,6);
DECLARE VARIABLE p_miscellaneous_amount NUMERIC(18,6);
DECLARE VARIABLE w2_box VARCHAR(10);
DECLARE VARIABLE p_w2_box VARCHAR(10);
DECLARE VARIABLE pay_with_state CHAR(1);
DECLARE VARIABLE p_pay_with_state CHAR(1);
DECLARE VARIABLE wage_minimum NUMERIC(18,6);
DECLARE VARIABLE p_wage_minimum NUMERIC(18,6);
DECLARE VARIABLE e_d_code_type VARCHAR(2);
DECLARE VARIABLE p_e_d_code_type VARCHAR(2);
DECLARE VARIABLE quarterly_minimum_threshold NUMERIC(18,6);
DECLARE VARIABLE p_quarterly_minimum_threshold NUMERIC(18,6);
DECLARE VARIABLE agency_code VARCHAR(10);
DECLARE VARIABLE p_agency_code VARCHAR(10);
DECLARE VARIABLE combine_for_tax_payments CHAR(1);
DECLARE VARIABLE p_combine_for_tax_payments CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_locals_nbr , name, local_type, zip_code, local_tax_identifier, agency_number, minimum_hours_worked_per, calculation_method, print_return_if_zero, use_misc_tax_return_code, sy_local_tax_pmt_agency_nbr, sy_tax_pmt_report_nbr, sy_local_reporting_agency_nbr, tax_deposit_follow_state, filler, local_active, round_to_nearest_dollar, tax_type, tax_frequency, local_minimum_wage, tax_rate, tax_amount, tax_maximum, wage_maximum, weekly_tax_cap, minimum_hours_worked, miscellaneous_amount, w2_box, pay_with_state, wage_minimum, e_d_code_type, quarterly_minimum_threshold, agency_code, combine_for_tax_payments
        FROM sy_locals
        ORDER BY sy_locals_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_locals_nbr, :name, :local_type, :zip_code, :local_tax_identifier, :agency_number, :minimum_hours_worked_per, :calculation_method, :print_return_if_zero, :use_misc_tax_return_code, :sy_local_tax_pmt_agency_nbr, :sy_tax_pmt_report_nbr, :sy_local_reporting_agency_nbr, :tax_deposit_follow_state, :filler, :local_active, :round_to_nearest_dollar, :tax_type, :tax_frequency, :local_minimum_wage, :tax_rate, :tax_amount, :tax_maximum, :wage_maximum, :weekly_tax_cap, :minimum_hours_worked, :miscellaneous_amount, :w2_box, :pay_with_state, :wage_minimum, :e_d_code_type, :quarterly_minimum_threshold, :agency_code, :combine_for_tax_payments
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_locals_nbr) OR (effective_date = '1/1/1900') OR (p_name IS DISTINCT FROM name) OR (p_local_type IS DISTINCT FROM local_type) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_local_tax_identifier IS DISTINCT FROM local_tax_identifier) OR (p_agency_number IS DISTINCT FROM agency_number) OR (p_minimum_hours_worked_per IS DISTINCT FROM minimum_hours_worked_per) OR (p_calculation_method IS DISTINCT FROM calculation_method) OR (p_print_return_if_zero IS DISTINCT FROM print_return_if_zero) OR (p_use_misc_tax_return_code IS DISTINCT FROM use_misc_tax_return_code) OR (p_sy_local_tax_pmt_agency_nbr IS DISTINCT FROM sy_local_tax_pmt_agency_nbr) OR (p_sy_tax_pmt_report_nbr IS DISTINCT FROM sy_tax_pmt_report_nbr) OR (p_sy_local_reporting_agency_nbr IS DISTINCT FROM sy_local_reporting_agency_nbr) OR (p_tax_deposit_follow_state IS DISTINCT FROM tax_deposit_follow_state) OR (p_filler IS DISTINCT FROM filler) OR (p_local_active IS DISTINCT FROM local_active) OR (p_round_to_nearest_dollar IS DISTINCT FROM round_to_nearest_dollar) OR (p_tax_type IS DISTINCT FROM tax_type) OR (p_tax_frequency IS DISTINCT FROM tax_frequency) OR (p_local_minimum_wage IS DISTINCT FROM local_minimum_wage) OR (p_tax_rate IS DISTINCT FROM tax_rate) OR (p_tax_amount IS DISTINCT FROM tax_amount) OR (p_tax_maximum IS DISTINCT FROM tax_maximum) OR (p_wage_maximum IS DISTINCT FROM wage_maximum) OR (p_weekly_tax_cap IS DISTINCT FROM weekly_tax_cap) OR (p_minimum_hours_worked IS DISTINCT FROM minimum_hours_worked) OR (p_miscellaneous_amount IS DISTINCT FROM miscellaneous_amount) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_pay_with_state IS DISTINCT FROM pay_with_state) OR (p_wage_minimum IS DISTINCT FROM wage_minimum) OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_quarterly_minimum_threshold IS DISTINCT FROM quarterly_minimum_threshold) OR (p_agency_code IS DISTINCT FROM agency_code) OR (p_combine_for_tax_payments IS DISTINCT FROM combine_for_tax_payments)) THEN
      BEGIN
        curr_nbr = sy_locals_nbr;
        p_name = name;
        p_local_type = local_type;
        p_zip_code = zip_code;
        p_local_tax_identifier = local_tax_identifier;
        p_agency_number = agency_number;
        p_minimum_hours_worked_per = minimum_hours_worked_per;
        p_calculation_method = calculation_method;
        p_print_return_if_zero = print_return_if_zero;
        p_use_misc_tax_return_code = use_misc_tax_return_code;
        p_sy_local_tax_pmt_agency_nbr = sy_local_tax_pmt_agency_nbr;
        p_sy_tax_pmt_report_nbr = sy_tax_pmt_report_nbr;
        p_sy_local_reporting_agency_nbr = sy_local_reporting_agency_nbr;
        p_tax_deposit_follow_state = tax_deposit_follow_state;
        p_filler = filler;
        p_local_active = local_active;
        p_round_to_nearest_dollar = round_to_nearest_dollar;
        p_tax_type = tax_type;
        p_tax_frequency = tax_frequency;
        p_local_minimum_wage = local_minimum_wage;
        p_tax_rate = tax_rate;
        p_tax_amount = tax_amount;
        p_tax_maximum = tax_maximum;
        p_wage_maximum = wage_maximum;
        p_weekly_tax_cap = weekly_tax_cap;
        p_minimum_hours_worked = minimum_hours_worked;
        p_miscellaneous_amount = miscellaneous_amount;
        p_w2_box = w2_box;
        p_pay_with_state = pay_with_state;
        p_wage_minimum = wage_minimum;
        p_e_d_code_type = e_d_code_type;
        p_quarterly_minimum_threshold = quarterly_minimum_threshold;
        p_agency_code = agency_code;
        p_combine_for_tax_payments = combine_for_tax_payments;
      END
      ELSE
        DELETE FROM sy_locals WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, name, local_type, zip_code, local_tax_identifier, agency_number, minimum_hours_worked_per, calculation_method, print_return_if_zero, use_misc_tax_return_code, sy_local_tax_pmt_agency_nbr, sy_tax_pmt_report_nbr, sy_local_reporting_agency_nbr, tax_deposit_follow_state, filler, local_active, round_to_nearest_dollar, tax_type, tax_frequency, local_minimum_wage, tax_rate, tax_amount, tax_maximum, wage_maximum, weekly_tax_cap, minimum_hours_worked, miscellaneous_amount, w2_box, pay_with_state, wage_minimum, e_d_code_type, quarterly_minimum_threshold, agency_code, combine_for_tax_payments
        FROM sy_locals
        WHERE sy_locals_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :name, :local_type, :zip_code, :local_tax_identifier, :agency_number, :minimum_hours_worked_per, :calculation_method, :print_return_if_zero, :use_misc_tax_return_code, :sy_local_tax_pmt_agency_nbr, :sy_tax_pmt_report_nbr, :sy_local_reporting_agency_nbr, :tax_deposit_follow_state, :filler, :local_active, :round_to_nearest_dollar, :tax_type, :tax_frequency, :local_minimum_wage, :tax_rate, :tax_amount, :tax_maximum, :wage_maximum, :weekly_tax_cap, :minimum_hours_worked, :miscellaneous_amount, :w2_box, :pay_with_state, :wage_minimum, :e_d_code_type, :quarterly_minimum_threshold, :agency_code, :combine_for_tax_payments
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_name IS DISTINCT FROM name) OR (p_local_type IS DISTINCT FROM local_type) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_local_tax_identifier IS DISTINCT FROM local_tax_identifier) OR (p_agency_number IS DISTINCT FROM agency_number) OR (p_minimum_hours_worked_per IS DISTINCT FROM minimum_hours_worked_per) OR (p_calculation_method IS DISTINCT FROM calculation_method) OR (p_print_return_if_zero IS DISTINCT FROM print_return_if_zero) OR (p_use_misc_tax_return_code IS DISTINCT FROM use_misc_tax_return_code) OR (p_sy_local_tax_pmt_agency_nbr IS DISTINCT FROM sy_local_tax_pmt_agency_nbr) OR (p_sy_tax_pmt_report_nbr IS DISTINCT FROM sy_tax_pmt_report_nbr) OR (p_sy_local_reporting_agency_nbr IS DISTINCT FROM sy_local_reporting_agency_nbr) OR (p_tax_deposit_follow_state IS DISTINCT FROM tax_deposit_follow_state) OR (p_filler IS DISTINCT FROM filler) OR (p_local_active IS DISTINCT FROM local_active) OR (p_round_to_nearest_dollar IS DISTINCT FROM round_to_nearest_dollar) OR (p_tax_type IS DISTINCT FROM tax_type) OR (p_tax_frequency IS DISTINCT FROM tax_frequency) OR (p_local_minimum_wage IS DISTINCT FROM local_minimum_wage) OR (p_tax_rate IS DISTINCT FROM tax_rate) OR (p_tax_amount IS DISTINCT FROM tax_amount) OR (p_tax_maximum IS DISTINCT FROM tax_maximum) OR (p_wage_maximum IS DISTINCT FROM wage_maximum) OR (p_weekly_tax_cap IS DISTINCT FROM weekly_tax_cap) OR (p_minimum_hours_worked IS DISTINCT FROM minimum_hours_worked) OR (p_miscellaneous_amount IS DISTINCT FROM miscellaneous_amount) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_pay_with_state IS DISTINCT FROM pay_with_state) OR (p_wage_minimum IS DISTINCT FROM wage_minimum) OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_quarterly_minimum_threshold IS DISTINCT FROM quarterly_minimum_threshold) OR (p_agency_code IS DISTINCT FROM agency_code) OR (p_combine_for_tax_payments IS DISTINCT FROM combine_for_tax_payments)) THEN
      BEGIN
        p_name = name;
        p_local_type = local_type;
        p_zip_code = zip_code;
        p_local_tax_identifier = local_tax_identifier;
        p_agency_number = agency_number;
        p_minimum_hours_worked_per = minimum_hours_worked_per;
        p_calculation_method = calculation_method;
        p_print_return_if_zero = print_return_if_zero;
        p_use_misc_tax_return_code = use_misc_tax_return_code;
        p_sy_local_tax_pmt_agency_nbr = sy_local_tax_pmt_agency_nbr;
        p_sy_tax_pmt_report_nbr = sy_tax_pmt_report_nbr;
        p_sy_local_reporting_agency_nbr = sy_local_reporting_agency_nbr;
        p_tax_deposit_follow_state = tax_deposit_follow_state;
        p_filler = filler;
        p_local_active = local_active;
        p_round_to_nearest_dollar = round_to_nearest_dollar;
        p_tax_type = tax_type;
        p_tax_frequency = tax_frequency;
        p_local_minimum_wage = local_minimum_wage;
        p_tax_rate = tax_rate;
        p_tax_amount = tax_amount;
        p_tax_maximum = tax_maximum;
        p_wage_maximum = wage_maximum;
        p_weekly_tax_cap = weekly_tax_cap;
        p_minimum_hours_worked = minimum_hours_worked;
        p_miscellaneous_amount = miscellaneous_amount;
        p_w2_box = w2_box;
        p_pay_with_state = pay_with_state;
        p_wage_minimum = wage_minimum;
        p_e_d_code_type = e_d_code_type;
        p_quarterly_minimum_threshold = quarterly_minimum_threshold;
        p_agency_code = agency_code;
        p_combine_for_tax_payments = combine_for_tax_payments;
      END
      ELSE
        DELETE FROM sy_locals WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_LOCALS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_local_deposit_freq(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_local_deposit_freq_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE when_due_type CHAR(1);
DECLARE VARIABLE p_when_due_type CHAR(1);
DECLARE VARIABLE number_of_days_for_when_due INTEGER;
DECLARE VARIABLE p_number_of_days_for_when_due INTEGER;
DECLARE VARIABLE thrd_mnth_due_end_of_next_mnth CHAR(1);
DECLARE VARIABLE p_thrd_mnth_due_end_of_next_mnth CHAR(1);
DECLARE VARIABLE description VARCHAR(20);
DECLARE VARIABLE p_description VARCHAR(20);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE frequency_type CHAR(1);
DECLARE VARIABLE p_frequency_type CHAR(1);
DECLARE VARIABLE hide_from_user CHAR(1);
DECLARE VARIABLE p_hide_from_user CHAR(1);
DECLARE VARIABLE tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE p_tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE inc_tax_payment_code CHAR(1);
DECLARE VARIABLE p_inc_tax_payment_code CHAR(1);
DECLARE VARIABLE change_freq_on_threshold CHAR(1);
DECLARE VARIABLE p_change_freq_on_threshold CHAR(1);
DECLARE VARIABLE first_threshold_dep_freq_nbr INTEGER;
DECLARE VARIABLE p_first_threshold_dep_freq_nbr INTEGER;
DECLARE VARIABLE first_threshold_period CHAR(1);
DECLARE VARIABLE p_first_threshold_period CHAR(1);
DECLARE VARIABLE first_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE p_first_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE second_threshold_dep_freq_nbr INTEGER;
DECLARE VARIABLE p_second_threshold_dep_freq_nbr INTEGER;
DECLARE VARIABLE second_threshold_period CHAR(1);
DECLARE VARIABLE p_second_threshold_period CHAR(1);
DECLARE VARIABLE second_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE p_second_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE p_sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE qe_sy_gl_agency_office_nbr INTEGER;
DECLARE VARIABLE p_qe_sy_gl_agency_office_nbr INTEGER;
DECLARE VARIABLE qe_tax_coupon_sy_reports_nbr INTEGER;
DECLARE VARIABLE p_qe_tax_coupon_sy_reports_nbr INTEGER;
DECLARE VARIABLE pay_and_shiftback CHAR(1);
DECLARE VARIABLE p_pay_and_shiftback CHAR(1);
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_group_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_local_deposit_freq_nbr , when_due_type, number_of_days_for_when_due, thrd_mnth_due_end_of_next_mnth, description, filler, frequency_type, hide_from_user, tax_payment_type_code, inc_tax_payment_code, change_freq_on_threshold, first_threshold_dep_freq_nbr, first_threshold_period, first_threshold_amount, second_threshold_dep_freq_nbr, second_threshold_period, second_threshold_amount, sy_gl_agency_field_office_nbr, qe_sy_gl_agency_office_nbr, qe_tax_coupon_sy_reports_nbr, pay_and_shiftback, sy_reports_group_nbr
        FROM sy_local_deposit_freq
        ORDER BY sy_local_deposit_freq_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_local_deposit_freq_nbr, :when_due_type, :number_of_days_for_when_due, :thrd_mnth_due_end_of_next_mnth, :description, :filler, :frequency_type, :hide_from_user, :tax_payment_type_code, :inc_tax_payment_code, :change_freq_on_threshold, :first_threshold_dep_freq_nbr, :first_threshold_period, :first_threshold_amount, :second_threshold_dep_freq_nbr, :second_threshold_period, :second_threshold_amount, :sy_gl_agency_field_office_nbr, :qe_sy_gl_agency_office_nbr, :qe_tax_coupon_sy_reports_nbr, :pay_and_shiftback, :sy_reports_group_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_local_deposit_freq_nbr) OR (effective_date = '1/1/1900') OR (p_when_due_type IS DISTINCT FROM when_due_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_thrd_mnth_due_end_of_next_mnth IS DISTINCT FROM thrd_mnth_due_end_of_next_mnth) OR (p_description IS DISTINCT FROM description) OR (p_filler IS DISTINCT FROM filler) OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_hide_from_user IS DISTINCT FROM hide_from_user) OR (p_tax_payment_type_code IS DISTINCT FROM tax_payment_type_code) OR (p_inc_tax_payment_code IS DISTINCT FROM inc_tax_payment_code) OR (p_change_freq_on_threshold IS DISTINCT FROM change_freq_on_threshold) OR (p_first_threshold_dep_freq_nbr IS DISTINCT FROM first_threshold_dep_freq_nbr) OR (p_first_threshold_period IS DISTINCT FROM first_threshold_period) OR (p_first_threshold_amount IS DISTINCT FROM first_threshold_amount) OR (p_second_threshold_dep_freq_nbr IS DISTINCT FROM second_threshold_dep_freq_nbr) OR (p_second_threshold_period IS DISTINCT FROM second_threshold_period) OR (p_second_threshold_amount IS DISTINCT FROM second_threshold_amount) OR (p_sy_gl_agency_field_office_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (p_qe_sy_gl_agency_office_nbr IS DISTINCT FROM qe_sy_gl_agency_office_nbr) OR (p_qe_tax_coupon_sy_reports_nbr IS DISTINCT FROM qe_tax_coupon_sy_reports_nbr) OR (p_pay_and_shiftback IS DISTINCT FROM pay_and_shiftback) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr)) THEN
      BEGIN
        curr_nbr = sy_local_deposit_freq_nbr;
        p_when_due_type = when_due_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_thrd_mnth_due_end_of_next_mnth = thrd_mnth_due_end_of_next_mnth;
        p_description = description;
        p_filler = filler;
        p_frequency_type = frequency_type;
        p_hide_from_user = hide_from_user;
        p_tax_payment_type_code = tax_payment_type_code;
        p_inc_tax_payment_code = inc_tax_payment_code;
        p_change_freq_on_threshold = change_freq_on_threshold;
        p_first_threshold_dep_freq_nbr = first_threshold_dep_freq_nbr;
        p_first_threshold_period = first_threshold_period;
        p_first_threshold_amount = first_threshold_amount;
        p_second_threshold_dep_freq_nbr = second_threshold_dep_freq_nbr;
        p_second_threshold_period = second_threshold_period;
        p_second_threshold_amount = second_threshold_amount;
        p_sy_gl_agency_field_office_nbr = sy_gl_agency_field_office_nbr;
        p_qe_sy_gl_agency_office_nbr = qe_sy_gl_agency_office_nbr;
        p_qe_tax_coupon_sy_reports_nbr = qe_tax_coupon_sy_reports_nbr;
        p_pay_and_shiftback = pay_and_shiftback;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
      END
      ELSE
        DELETE FROM sy_local_deposit_freq WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, when_due_type, number_of_days_for_when_due, thrd_mnth_due_end_of_next_mnth, description, filler, frequency_type, hide_from_user, tax_payment_type_code, inc_tax_payment_code, change_freq_on_threshold, first_threshold_dep_freq_nbr, first_threshold_period, first_threshold_amount, second_threshold_dep_freq_nbr, second_threshold_period, second_threshold_amount, sy_gl_agency_field_office_nbr, qe_sy_gl_agency_office_nbr, qe_tax_coupon_sy_reports_nbr, pay_and_shiftback, sy_reports_group_nbr
        FROM sy_local_deposit_freq
        WHERE sy_local_deposit_freq_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :when_due_type, :number_of_days_for_when_due, :thrd_mnth_due_end_of_next_mnth, :description, :filler, :frequency_type, :hide_from_user, :tax_payment_type_code, :inc_tax_payment_code, :change_freq_on_threshold, :first_threshold_dep_freq_nbr, :first_threshold_period, :first_threshold_amount, :second_threshold_dep_freq_nbr, :second_threshold_period, :second_threshold_amount, :sy_gl_agency_field_office_nbr, :qe_sy_gl_agency_office_nbr, :qe_tax_coupon_sy_reports_nbr, :pay_and_shiftback, :sy_reports_group_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_when_due_type IS DISTINCT FROM when_due_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_thrd_mnth_due_end_of_next_mnth IS DISTINCT FROM thrd_mnth_due_end_of_next_mnth) OR (p_description IS DISTINCT FROM description) OR (p_filler IS DISTINCT FROM filler) OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_hide_from_user IS DISTINCT FROM hide_from_user) OR (p_tax_payment_type_code IS DISTINCT FROM tax_payment_type_code) OR (p_inc_tax_payment_code IS DISTINCT FROM inc_tax_payment_code) OR (p_change_freq_on_threshold IS DISTINCT FROM change_freq_on_threshold) OR (p_first_threshold_dep_freq_nbr IS DISTINCT FROM first_threshold_dep_freq_nbr) OR (p_first_threshold_period IS DISTINCT FROM first_threshold_period) OR (p_first_threshold_amount IS DISTINCT FROM first_threshold_amount) OR (p_second_threshold_dep_freq_nbr IS DISTINCT FROM second_threshold_dep_freq_nbr) OR (p_second_threshold_period IS DISTINCT FROM second_threshold_period) OR (p_second_threshold_amount IS DISTINCT FROM second_threshold_amount) OR (p_sy_gl_agency_field_office_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (p_qe_sy_gl_agency_office_nbr IS DISTINCT FROM qe_sy_gl_agency_office_nbr) OR (p_qe_tax_coupon_sy_reports_nbr IS DISTINCT FROM qe_tax_coupon_sy_reports_nbr) OR (p_pay_and_shiftback IS DISTINCT FROM pay_and_shiftback) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr)) THEN
      BEGIN
        p_when_due_type = when_due_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_thrd_mnth_due_end_of_next_mnth = thrd_mnth_due_end_of_next_mnth;
        p_description = description;
        p_filler = filler;
        p_frequency_type = frequency_type;
        p_hide_from_user = hide_from_user;
        p_tax_payment_type_code = tax_payment_type_code;
        p_inc_tax_payment_code = inc_tax_payment_code;
        p_change_freq_on_threshold = change_freq_on_threshold;
        p_first_threshold_dep_freq_nbr = first_threshold_dep_freq_nbr;
        p_first_threshold_period = first_threshold_period;
        p_first_threshold_amount = first_threshold_amount;
        p_second_threshold_dep_freq_nbr = second_threshold_dep_freq_nbr;
        p_second_threshold_period = second_threshold_period;
        p_second_threshold_amount = second_threshold_amount;
        p_sy_gl_agency_field_office_nbr = sy_gl_agency_field_office_nbr;
        p_qe_sy_gl_agency_office_nbr = qe_sy_gl_agency_office_nbr;
        p_qe_tax_coupon_sy_reports_nbr = qe_tax_coupon_sy_reports_nbr;
        p_pay_and_shiftback = pay_and_shiftback;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
      END
      ELSE
        DELETE FROM sy_local_deposit_freq WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_LOCAL_DEPOSIT_FREQ TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_local_exemptions(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_local_exemptions_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE e_d_code_type VARCHAR(2);
DECLARE VARIABLE p_e_d_code_type VARCHAR(2);
DECLARE VARIABLE exempt CHAR(1);
DECLARE VARIABLE p_exempt CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_local_exemptions_nbr , e_d_code_type, exempt
        FROM sy_local_exemptions
        ORDER BY sy_local_exemptions_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_local_exemptions_nbr, :e_d_code_type, :exempt
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_local_exemptions_nbr) OR (effective_date = '1/1/1900') OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_exempt IS DISTINCT FROM exempt)) THEN
      BEGIN
        curr_nbr = sy_local_exemptions_nbr;
        p_e_d_code_type = e_d_code_type;
        p_exempt = exempt;
      END
      ELSE
        DELETE FROM sy_local_exemptions WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, e_d_code_type, exempt
        FROM sy_local_exemptions
        WHERE sy_local_exemptions_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :e_d_code_type, :exempt
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_exempt IS DISTINCT FROM exempt)) THEN
      BEGIN
        p_e_d_code_type = e_d_code_type;
        p_exempt = exempt;
      END
      ELSE
        DELETE FROM sy_local_exemptions WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_LOCAL_EXEMPTIONS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_local_marital_status(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_local_marital_status_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE sy_state_marital_status_nbr INTEGER;
DECLARE VARIABLE p_sy_state_marital_status_nbr INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE standard_deduction_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_amount NUMERIC(18,6);
DECLARE VARIABLE standard_exemption_allow NUMERIC(18,6);
DECLARE VARIABLE p_standard_exemption_allow NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_local_marital_status_nbr , sy_state_marital_status_nbr, filler, standard_deduction_amount, standard_exemption_allow
        FROM sy_local_marital_status
        ORDER BY sy_local_marital_status_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_local_marital_status_nbr, :sy_state_marital_status_nbr, :filler, :standard_deduction_amount, :standard_exemption_allow
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_local_marital_status_nbr) OR (effective_date = '1/1/1900') OR (p_sy_state_marital_status_nbr IS DISTINCT FROM sy_state_marital_status_nbr) OR (p_filler IS DISTINCT FROM filler) OR (p_standard_deduction_amount IS DISTINCT FROM standard_deduction_amount) OR (p_standard_exemption_allow IS DISTINCT FROM standard_exemption_allow)) THEN
      BEGIN
        curr_nbr = sy_local_marital_status_nbr;
        p_sy_state_marital_status_nbr = sy_state_marital_status_nbr;
        p_filler = filler;
        p_standard_deduction_amount = standard_deduction_amount;
        p_standard_exemption_allow = standard_exemption_allow;
      END
      ELSE
        DELETE FROM sy_local_marital_status WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, sy_state_marital_status_nbr, filler, standard_deduction_amount, standard_exemption_allow
        FROM sy_local_marital_status
        WHERE sy_local_marital_status_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :sy_state_marital_status_nbr, :filler, :standard_deduction_amount, :standard_exemption_allow
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_sy_state_marital_status_nbr IS DISTINCT FROM sy_state_marital_status_nbr) OR (p_filler IS DISTINCT FROM filler) OR (p_standard_deduction_amount IS DISTINCT FROM standard_deduction_amount) OR (p_standard_exemption_allow IS DISTINCT FROM standard_exemption_allow)) THEN
      BEGIN
        p_sy_state_marital_status_nbr = sy_state_marital_status_nbr;
        p_filler = filler;
        p_standard_deduction_amount = standard_deduction_amount;
        p_standard_exemption_allow = standard_exemption_allow;
      END
      ELSE
        DELETE FROM sy_local_marital_status WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_LOCAL_MARITAL_STATUS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_local_tax_chart(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_local_tax_chart_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE sy_local_marital_status_nbr INTEGER;
DECLARE VARIABLE p_sy_local_marital_status_nbr INTEGER;
DECLARE VARIABLE minimum NUMERIC(18,6);
DECLARE VARIABLE p_minimum NUMERIC(18,6);
DECLARE VARIABLE maximum NUMERIC(18,6);
DECLARE VARIABLE p_maximum NUMERIC(18,6);
DECLARE VARIABLE percentage NUMERIC(18,6);
DECLARE VARIABLE p_percentage NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_local_tax_chart_nbr , sy_local_marital_status_nbr, minimum, maximum, percentage
        FROM sy_local_tax_chart
        ORDER BY sy_local_tax_chart_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_local_tax_chart_nbr, :sy_local_marital_status_nbr, :minimum, :maximum, :percentage
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_local_tax_chart_nbr) OR (effective_date = '1/1/1900') OR (p_sy_local_marital_status_nbr IS DISTINCT FROM sy_local_marital_status_nbr) OR (p_minimum IS DISTINCT FROM minimum) OR (p_maximum IS DISTINCT FROM maximum) OR (p_percentage IS DISTINCT FROM percentage)) THEN
      BEGIN
        curr_nbr = sy_local_tax_chart_nbr;
        p_sy_local_marital_status_nbr = sy_local_marital_status_nbr;
        p_minimum = minimum;
        p_maximum = maximum;
        p_percentage = percentage;
      END
      ELSE
        DELETE FROM sy_local_tax_chart WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, sy_local_marital_status_nbr, minimum, maximum, percentage
        FROM sy_local_tax_chart
        WHERE sy_local_tax_chart_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :sy_local_marital_status_nbr, :minimum, :maximum, :percentage
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_sy_local_marital_status_nbr IS DISTINCT FROM sy_local_marital_status_nbr) OR (p_minimum IS DISTINCT FROM minimum) OR (p_maximum IS DISTINCT FROM maximum) OR (p_percentage IS DISTINCT FROM percentage)) THEN
      BEGIN
        p_sy_local_marital_status_nbr = sy_local_marital_status_nbr;
        p_minimum = minimum;
        p_maximum = maximum;
        p_percentage = percentage;
      END
      ELSE
        DELETE FROM sy_local_tax_chart WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_LOCAL_TAX_CHART TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_reciprocated_states(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_reciprocated_states_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE participant_state_nbr INTEGER;
DECLARE VARIABLE p_participant_state_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_reciprocated_states_nbr , participant_state_nbr
        FROM sy_reciprocated_states
        ORDER BY sy_reciprocated_states_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_reciprocated_states_nbr, :participant_state_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_reciprocated_states_nbr) OR (effective_date = '1/1/1900') OR (p_participant_state_nbr IS DISTINCT FROM participant_state_nbr)) THEN
      BEGIN
        curr_nbr = sy_reciprocated_states_nbr;
        p_participant_state_nbr = participant_state_nbr;
      END
      ELSE
        DELETE FROM sy_reciprocated_states WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, participant_state_nbr
        FROM sy_reciprocated_states
        WHERE sy_reciprocated_states_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :participant_state_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_participant_state_nbr IS DISTINCT FROM participant_state_nbr)) THEN
      BEGIN
        p_participant_state_nbr = participant_state_nbr;
      END
      ELSE
        DELETE FROM sy_reciprocated_states WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_RECIPROCATED_STATES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_reports(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_reports_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE description VARCHAR(40);
DECLARE VARIABLE p_description VARCHAR(40);
DECLARE VARIABLE active_report CHAR(1);
DECLARE VARIABLE p_active_report CHAR(1);
DECLARE VARIABLE sy_report_writer_reports_nbr INTEGER;
DECLARE VARIABLE p_sy_report_writer_reports_nbr INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE active_year_from DATE;
DECLARE VARIABLE p_active_year_from DATE;
DECLARE VARIABLE active_year_to DATE;
DECLARE VARIABLE p_active_year_to DATE;
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_group_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_reports_nbr , description, active_report, sy_report_writer_reports_nbr, filler, active_year_from, active_year_to, sy_reports_group_nbr
        FROM sy_reports
        ORDER BY sy_reports_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_reports_nbr, :description, :active_report, :sy_report_writer_reports_nbr, :filler, :active_year_from, :active_year_to, :sy_reports_group_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_reports_nbr) OR (effective_date = '1/1/1900') OR (p_description IS DISTINCT FROM description) OR (p_active_report IS DISTINCT FROM active_report) OR (p_sy_report_writer_reports_nbr IS DISTINCT FROM sy_report_writer_reports_nbr) OR (p_filler IS DISTINCT FROM filler) OR (p_active_year_from IS DISTINCT FROM active_year_from) OR (p_active_year_to IS DISTINCT FROM active_year_to) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr)) THEN
      BEGIN
        curr_nbr = sy_reports_nbr;
        p_description = description;
        p_active_report = active_report;
        p_sy_report_writer_reports_nbr = sy_report_writer_reports_nbr;
        p_filler = filler;
        p_active_year_from = active_year_from;
        p_active_year_to = active_year_to;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
      END
      ELSE
        DELETE FROM sy_reports WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, description, active_report, sy_report_writer_reports_nbr, filler, active_year_from, active_year_to, sy_reports_group_nbr
        FROM sy_reports
        WHERE sy_reports_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :description, :active_report, :sy_report_writer_reports_nbr, :filler, :active_year_from, :active_year_to, :sy_reports_group_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_description IS DISTINCT FROM description) OR (p_active_report IS DISTINCT FROM active_report) OR (p_sy_report_writer_reports_nbr IS DISTINCT FROM sy_report_writer_reports_nbr) OR (p_filler IS DISTINCT FROM filler) OR (p_active_year_from IS DISTINCT FROM active_year_from) OR (p_active_year_to IS DISTINCT FROM active_year_to) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr)) THEN
      BEGIN
        p_description = description;
        p_active_report = active_report;
        p_sy_report_writer_reports_nbr = sy_report_writer_reports_nbr;
        p_filler = filler;
        p_active_year_from = active_year_from;
        p_active_year_to = active_year_to;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
      END
      ELSE
        DELETE FROM sy_reports WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_REPORTS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_reports_group(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE sy_reports_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_nbr INTEGER;
DECLARE VARIABLE sy_gl_agency_report_nbr INTEGER;
DECLARE VARIABLE p_sy_gl_agency_report_nbr INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE agency_copy CHAR(1);
DECLARE VARIABLE p_agency_copy CHAR(1);
DECLARE VARIABLE sb_copy CHAR(1);
DECLARE VARIABLE p_sb_copy CHAR(1);
DECLARE VARIABLE cl_copy CHAR(1);
DECLARE VARIABLE p_cl_copy CHAR(1);
DECLARE VARIABLE media_type CHAR(1);
DECLARE VARIABLE p_media_type CHAR(1);
DECLARE VARIABLE name VARCHAR(60);
DECLARE VARIABLE p_name VARCHAR(60);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_reports_group_nbr , sy_reports_nbr, sy_gl_agency_report_nbr, filler, agency_copy, sb_copy, cl_copy, media_type, name
        FROM sy_reports_group
        ORDER BY sy_reports_group_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_reports_group_nbr, :sy_reports_nbr, :sy_gl_agency_report_nbr, :filler, :agency_copy, :sb_copy, :cl_copy, :media_type, :name
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (effective_date = '1/1/1900') OR (p_sy_reports_nbr IS DISTINCT FROM sy_reports_nbr) OR (p_sy_gl_agency_report_nbr IS DISTINCT FROM sy_gl_agency_report_nbr) OR (p_filler IS DISTINCT FROM filler) OR (p_agency_copy IS DISTINCT FROM agency_copy) OR (p_sb_copy IS DISTINCT FROM sb_copy) OR (p_cl_copy IS DISTINCT FROM cl_copy) OR (p_media_type IS DISTINCT FROM media_type) OR (p_name IS DISTINCT FROM name)) THEN
      BEGIN
        curr_nbr = sy_reports_group_nbr;
        p_sy_reports_nbr = sy_reports_nbr;
        p_sy_gl_agency_report_nbr = sy_gl_agency_report_nbr;
        p_filler = filler;
        p_agency_copy = agency_copy;
        p_sb_copy = sb_copy;
        p_cl_copy = cl_copy;
        p_media_type = media_type;
        p_name = name;
      END
      ELSE
        DELETE FROM sy_reports_group WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, sy_reports_nbr, sy_gl_agency_report_nbr, filler, agency_copy, sb_copy, cl_copy, media_type, name
        FROM sy_reports_group
        WHERE sy_reports_group_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :sy_reports_nbr, :sy_gl_agency_report_nbr, :filler, :agency_copy, :sb_copy, :cl_copy, :media_type, :name
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_sy_reports_nbr IS DISTINCT FROM sy_reports_nbr) OR (p_sy_gl_agency_report_nbr IS DISTINCT FROM sy_gl_agency_report_nbr) OR (p_filler IS DISTINCT FROM filler) OR (p_agency_copy IS DISTINCT FROM agency_copy) OR (p_sb_copy IS DISTINCT FROM sb_copy) OR (p_cl_copy IS DISTINCT FROM cl_copy) OR (p_media_type IS DISTINCT FROM media_type) OR (p_name IS DISTINCT FROM name)) THEN
      BEGIN
        p_sy_reports_nbr = sy_reports_nbr;
        p_sy_gl_agency_report_nbr = sy_gl_agency_report_nbr;
        p_filler = filler;
        p_agency_copy = agency_copy;
        p_sb_copy = sb_copy;
        p_cl_copy = cl_copy;
        p_media_type = media_type;
        p_name = name;
      END
      ELSE
        DELETE FROM sy_reports_group WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_REPORTS_GROUP TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_states(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_states_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE state_withholding CHAR(1);
DECLARE VARIABLE p_state_withholding CHAR(1);
DECLARE VARIABLE state_eft_type CHAR(1);
DECLARE VARIABLE p_state_eft_type CHAR(1);
DECLARE VARIABLE eft_notes BLOB;
DECLARE VARIABLE p_eft_notes BLOB;
DECLARE VARIABLE print_state_return_if_zero CHAR(1);
DECLARE VARIABLE p_print_state_return_if_zero CHAR(1);
DECLARE VARIABLE sui_eft_type CHAR(1);
DECLARE VARIABLE p_sui_eft_type CHAR(1);
DECLARE VARIABLE sui_eft_notes BLOB;
DECLARE VARIABLE p_sui_eft_notes BLOB;
DECLARE VARIABLE sdi_reciprocate CHAR(1);
DECLARE VARIABLE p_sdi_reciprocate CHAR(1);
DECLARE VARIABLE sui_reciprocate CHAR(1);
DECLARE VARIABLE p_sui_reciprocate CHAR(1);
DECLARE VARIABLE show_s125_in_gross_wages_sui CHAR(1);
DECLARE VARIABLE p_show_s125_in_gross_wages_sui CHAR(1);
DECLARE VARIABLE print_sui_return_if_zero CHAR(1);
DECLARE VARIABLE p_print_sui_return_if_zero CHAR(1);
DECLARE VARIABLE payroll VARCHAR(8);
DECLARE VARIABLE p_payroll VARCHAR(8);
DECLARE VARIABLE sales VARCHAR(8);
DECLARE VARIABLE p_sales VARCHAR(8);
DECLARE VARIABLE corporate VARCHAR(8);
DECLARE VARIABLE p_corporate VARCHAR(8);
DECLARE VARIABLE railroad VARCHAR(8);
DECLARE VARIABLE p_railroad VARCHAR(8);
DECLARE VARIABLE unemployment VARCHAR(8);
DECLARE VARIABLE p_unemployment VARCHAR(8);
DECLARE VARIABLE other VARCHAR(8);
DECLARE VARIABLE p_other VARCHAR(8);
DECLARE VARIABLE uifsa_new_hire_reporting CHAR(1);
DECLARE VARIABLE p_uifsa_new_hire_reporting CHAR(1);
DECLARE VARIABLE dd_child_support CHAR(1);
DECLARE VARIABLE p_dd_child_support CHAR(1);
DECLARE VARIABLE use_state_misc_tax_return_code CHAR(1);
DECLARE VARIABLE p_use_state_misc_tax_return_code CHAR(1);
DECLARE VARIABLE sy_state_tax_pmt_agency_nbr INTEGER;
DECLARE VARIABLE p_sy_state_tax_pmt_agency_nbr INTEGER;
DECLARE VARIABLE sy_tax_pmt_report_nbr INTEGER;
DECLARE VARIABLE p_sy_tax_pmt_report_nbr INTEGER;
DECLARE VARIABLE sy_state_reporting_agency_nbr INTEGER;
DECLARE VARIABLE p_sy_state_reporting_agency_nbr INTEGER;
DECLARE VARIABLE pay_sdi_with CHAR(1);
DECLARE VARIABLE p_pay_sdi_with CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE tax_deposit_follow_federal CHAR(1);
DECLARE VARIABLE p_tax_deposit_follow_federal CHAR(1);
DECLARE VARIABLE round_to_nearest_dollar CHAR(1);
DECLARE VARIABLE p_round_to_nearest_dollar CHAR(1);
DECLARE VARIABLE sui_tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE p_sui_tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE inc_sui_tax_payment_code CHAR(1);
DECLARE VARIABLE p_inc_sui_tax_payment_code CHAR(1);
DECLARE VARIABLE fips_code CHAR(2);
DECLARE VARIABLE p_fips_code CHAR(2);
DECLARE VARIABLE state_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE p_state_minimum_wage NUMERIC(18,6);
DECLARE VARIABLE state_tip_credit NUMERIC(18,6);
DECLARE VARIABLE p_state_tip_credit NUMERIC(18,6);
DECLARE VARIABLE supplemental_wages_percentage NUMERIC(18,6);
DECLARE VARIABLE p_supplemental_wages_percentage NUMERIC(18,6);
DECLARE VARIABLE weekly_sdi_tax_cap NUMERIC(18,6);
DECLARE VARIABLE p_weekly_sdi_tax_cap NUMERIC(18,6);
DECLARE VARIABLE ee_sdi_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE p_ee_sdi_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE er_sdi_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE p_er_sdi_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE ee_sdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_ee_sdi_rate NUMERIC(18,6);
DECLARE VARIABLE er_sdi_rate NUMERIC(18,6);
DECLARE VARIABLE p_er_sdi_rate NUMERIC(18,6);
DECLARE VARIABLE maximum_garnishment_percent NUMERIC(18,6);
DECLARE VARIABLE p_maximum_garnishment_percent NUMERIC(18,6);
DECLARE VARIABLE garnish_min_wage_multiplier NUMERIC(18,6);
DECLARE VARIABLE p_garnish_min_wage_multiplier NUMERIC(18,6);
DECLARE VARIABLE sales_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE p_sales_tax_percentage NUMERIC(18,6);
DECLARE VARIABLE w2_box VARCHAR(10);
DECLARE VARIABLE p_w2_box VARCHAR(10);
DECLARE VARIABLE reciprocity_type CHAR(1);
DECLARE VARIABLE p_reciprocity_type CHAR(1);
DECLARE VARIABLE print_w2 CHAR(1);
DECLARE VARIABLE p_print_w2 CHAR(1);
DECLARE VARIABLE cap_state_tax_credit CHAR(1);
DECLARE VARIABLE p_cap_state_tax_credit CHAR(1);
DECLARE VARIABLE inactivate_marital_status CHAR(1);
DECLARE VARIABLE p_inactivate_marital_status CHAR(1);
DECLARE VARIABLE state_ot_tip_credit NUMERIC(18,6);
DECLARE VARIABLE p_state_ot_tip_credit NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_states_nbr , state_withholding, state_eft_type, eft_notes, print_state_return_if_zero, sui_eft_type, sui_eft_notes, sdi_reciprocate, sui_reciprocate, show_s125_in_gross_wages_sui, print_sui_return_if_zero, payroll, sales, corporate, railroad, unemployment, other, uifsa_new_hire_reporting, dd_child_support, use_state_misc_tax_return_code, sy_state_tax_pmt_agency_nbr, sy_tax_pmt_report_nbr, sy_state_reporting_agency_nbr, pay_sdi_with, filler, tax_deposit_follow_federal, round_to_nearest_dollar, sui_tax_payment_type_code, inc_sui_tax_payment_code, fips_code, state_minimum_wage, state_tip_credit, supplemental_wages_percentage, weekly_sdi_tax_cap, ee_sdi_maximum_wage, er_sdi_maximum_wage, ee_sdi_rate, er_sdi_rate, maximum_garnishment_percent, garnish_min_wage_multiplier, sales_tax_percentage, w2_box, reciprocity_type, print_w2, cap_state_tax_credit, inactivate_marital_status, state_ot_tip_credit
        FROM sy_states
        ORDER BY sy_states_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_states_nbr, :state_withholding, :state_eft_type, :eft_notes, :print_state_return_if_zero, :sui_eft_type, :sui_eft_notes, :sdi_reciprocate, :sui_reciprocate, :show_s125_in_gross_wages_sui, :print_sui_return_if_zero, :payroll, :sales, :corporate, :railroad, :unemployment, :other, :uifsa_new_hire_reporting, :dd_child_support, :use_state_misc_tax_return_code, :sy_state_tax_pmt_agency_nbr, :sy_tax_pmt_report_nbr, :sy_state_reporting_agency_nbr, :pay_sdi_with, :filler, :tax_deposit_follow_federal, :round_to_nearest_dollar, :sui_tax_payment_type_code, :inc_sui_tax_payment_code, :fips_code, :state_minimum_wage, :state_tip_credit, :supplemental_wages_percentage, :weekly_sdi_tax_cap, :ee_sdi_maximum_wage, :er_sdi_maximum_wage, :ee_sdi_rate, :er_sdi_rate, :maximum_garnishment_percent, :garnish_min_wage_multiplier, :sales_tax_percentage, :w2_box, :reciprocity_type, :print_w2, :cap_state_tax_credit, :inactivate_marital_status, :state_ot_tip_credit
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_states_nbr) OR (effective_date = '1/1/1900') OR (p_state_withholding IS DISTINCT FROM state_withholding) OR (p_state_eft_type IS DISTINCT FROM state_eft_type) OR (p_eft_notes IS DISTINCT FROM eft_notes) OR (p_print_state_return_if_zero IS DISTINCT FROM print_state_return_if_zero) OR (p_sui_eft_type IS DISTINCT FROM sui_eft_type) OR (p_sui_eft_notes IS DISTINCT FROM sui_eft_notes) OR (p_sdi_reciprocate IS DISTINCT FROM sdi_reciprocate) OR (p_sui_reciprocate IS DISTINCT FROM sui_reciprocate) OR (p_show_s125_in_gross_wages_sui IS DISTINCT FROM show_s125_in_gross_wages_sui) OR (p_print_sui_return_if_zero IS DISTINCT FROM print_sui_return_if_zero) OR (p_payroll IS DISTINCT FROM payroll) OR (p_sales IS DISTINCT FROM sales) OR (p_corporate IS DISTINCT FROM corporate) OR (p_railroad IS DISTINCT FROM railroad) OR (p_unemployment IS DISTINCT FROM unemployment) OR (p_other IS DISTINCT FROM other) OR (p_uifsa_new_hire_reporting IS DISTINCT FROM uifsa_new_hire_reporting) OR (p_dd_child_support IS DISTINCT FROM dd_child_support) OR (p_use_state_misc_tax_return_code IS DISTINCT FROM use_state_misc_tax_return_code) OR (p_sy_state_tax_pmt_agency_nbr IS DISTINCT FROM sy_state_tax_pmt_agency_nbr) OR (p_sy_tax_pmt_report_nbr IS DISTINCT FROM sy_tax_pmt_report_nbr) OR (p_sy_state_reporting_agency_nbr IS DISTINCT FROM sy_state_reporting_agency_nbr) OR (p_pay_sdi_with IS DISTINCT FROM pay_sdi_with) OR (p_filler IS DISTINCT FROM filler) OR (p_tax_deposit_follow_federal IS DISTINCT FROM tax_deposit_follow_federal) OR (p_round_to_nearest_dollar IS DISTINCT FROM round_to_nearest_dollar) OR (p_sui_tax_payment_type_code IS DISTINCT FROM sui_tax_payment_type_code) OR (p_inc_sui_tax_payment_code IS DISTINCT FROM inc_sui_tax_payment_code) OR (p_fips_code IS DISTINCT FROM fips_code) OR (p_state_minimum_wage IS DISTINCT FROM state_minimum_wage) OR (p_state_tip_credit IS DISTINCT FROM state_tip_credit) OR (p_supplemental_wages_percentage IS DISTINCT FROM supplemental_wages_percentage) OR (p_weekly_sdi_tax_cap IS DISTINCT FROM weekly_sdi_tax_cap) OR (p_ee_sdi_maximum_wage IS DISTINCT FROM ee_sdi_maximum_wage) OR (p_er_sdi_maximum_wage IS DISTINCT FROM er_sdi_maximum_wage) OR (p_ee_sdi_rate IS DISTINCT FROM ee_sdi_rate) OR (p_er_sdi_rate IS DISTINCT FROM er_sdi_rate) OR (p_maximum_garnishment_percent IS DISTINCT FROM maximum_garnishment_percent) OR (p_garnish_min_wage_multiplier IS DISTINCT FROM garnish_min_wage_multiplier) OR (p_sales_tax_percentage IS DISTINCT FROM sales_tax_percentage) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_reciprocity_type IS DISTINCT FROM reciprocity_type) OR (p_print_w2 IS DISTINCT FROM print_w2) OR (p_cap_state_tax_credit IS DISTINCT FROM cap_state_tax_credit) OR (p_inactivate_marital_status IS DISTINCT FROM inactivate_marital_status) OR (p_state_ot_tip_credit IS DISTINCT FROM state_ot_tip_credit)) THEN
      BEGIN
        curr_nbr = sy_states_nbr;
        p_state_withholding = state_withholding;
        p_state_eft_type = state_eft_type;
        p_eft_notes = eft_notes;
        p_print_state_return_if_zero = print_state_return_if_zero;
        p_sui_eft_type = sui_eft_type;
        p_sui_eft_notes = sui_eft_notes;
        p_sdi_reciprocate = sdi_reciprocate;
        p_sui_reciprocate = sui_reciprocate;
        p_show_s125_in_gross_wages_sui = show_s125_in_gross_wages_sui;
        p_print_sui_return_if_zero = print_sui_return_if_zero;
        p_payroll = payroll;
        p_sales = sales;
        p_corporate = corporate;
        p_railroad = railroad;
        p_unemployment = unemployment;
        p_other = other;
        p_uifsa_new_hire_reporting = uifsa_new_hire_reporting;
        p_dd_child_support = dd_child_support;
        p_use_state_misc_tax_return_code = use_state_misc_tax_return_code;
        p_sy_state_tax_pmt_agency_nbr = sy_state_tax_pmt_agency_nbr;
        p_sy_tax_pmt_report_nbr = sy_tax_pmt_report_nbr;
        p_sy_state_reporting_agency_nbr = sy_state_reporting_agency_nbr;
        p_pay_sdi_with = pay_sdi_with;
        p_filler = filler;
        p_tax_deposit_follow_federal = tax_deposit_follow_federal;
        p_round_to_nearest_dollar = round_to_nearest_dollar;
        p_sui_tax_payment_type_code = sui_tax_payment_type_code;
        p_inc_sui_tax_payment_code = inc_sui_tax_payment_code;
        p_fips_code = fips_code;
        p_state_minimum_wage = state_minimum_wage;
        p_state_tip_credit = state_tip_credit;
        p_supplemental_wages_percentage = supplemental_wages_percentage;
        p_weekly_sdi_tax_cap = weekly_sdi_tax_cap;
        p_ee_sdi_maximum_wage = ee_sdi_maximum_wage;
        p_er_sdi_maximum_wage = er_sdi_maximum_wage;
        p_ee_sdi_rate = ee_sdi_rate;
        p_er_sdi_rate = er_sdi_rate;
        p_maximum_garnishment_percent = maximum_garnishment_percent;
        p_garnish_min_wage_multiplier = garnish_min_wage_multiplier;
        p_sales_tax_percentage = sales_tax_percentage;
        p_w2_box = w2_box;
        p_reciprocity_type = reciprocity_type;
        p_print_w2 = print_w2;
        p_cap_state_tax_credit = cap_state_tax_credit;
        p_inactivate_marital_status = inactivate_marital_status;
        p_state_ot_tip_credit = state_ot_tip_credit;
      END
      ELSE
        DELETE FROM sy_states WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, state_withholding, state_eft_type, eft_notes, print_state_return_if_zero, sui_eft_type, sui_eft_notes, sdi_reciprocate, sui_reciprocate, show_s125_in_gross_wages_sui, print_sui_return_if_zero, payroll, sales, corporate, railroad, unemployment, other, uifsa_new_hire_reporting, dd_child_support, use_state_misc_tax_return_code, sy_state_tax_pmt_agency_nbr, sy_tax_pmt_report_nbr, sy_state_reporting_agency_nbr, pay_sdi_with, filler, tax_deposit_follow_federal, round_to_nearest_dollar, sui_tax_payment_type_code, inc_sui_tax_payment_code, fips_code, state_minimum_wage, state_tip_credit, supplemental_wages_percentage, weekly_sdi_tax_cap, ee_sdi_maximum_wage, er_sdi_maximum_wage, ee_sdi_rate, er_sdi_rate, maximum_garnishment_percent, garnish_min_wage_multiplier, sales_tax_percentage, w2_box, reciprocity_type, print_w2, cap_state_tax_credit, inactivate_marital_status, state_ot_tip_credit
        FROM sy_states
        WHERE sy_states_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :state_withholding, :state_eft_type, :eft_notes, :print_state_return_if_zero, :sui_eft_type, :sui_eft_notes, :sdi_reciprocate, :sui_reciprocate, :show_s125_in_gross_wages_sui, :print_sui_return_if_zero, :payroll, :sales, :corporate, :railroad, :unemployment, :other, :uifsa_new_hire_reporting, :dd_child_support, :use_state_misc_tax_return_code, :sy_state_tax_pmt_agency_nbr, :sy_tax_pmt_report_nbr, :sy_state_reporting_agency_nbr, :pay_sdi_with, :filler, :tax_deposit_follow_federal, :round_to_nearest_dollar, :sui_tax_payment_type_code, :inc_sui_tax_payment_code, :fips_code, :state_minimum_wage, :state_tip_credit, :supplemental_wages_percentage, :weekly_sdi_tax_cap, :ee_sdi_maximum_wage, :er_sdi_maximum_wage, :ee_sdi_rate, :er_sdi_rate, :maximum_garnishment_percent, :garnish_min_wage_multiplier, :sales_tax_percentage, :w2_box, :reciprocity_type, :print_w2, :cap_state_tax_credit, :inactivate_marital_status, :state_ot_tip_credit
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_state_withholding IS DISTINCT FROM state_withholding) OR (p_state_eft_type IS DISTINCT FROM state_eft_type) OR (p_eft_notes IS DISTINCT FROM eft_notes) OR (p_print_state_return_if_zero IS DISTINCT FROM print_state_return_if_zero) OR (p_sui_eft_type IS DISTINCT FROM sui_eft_type) OR (p_sui_eft_notes IS DISTINCT FROM sui_eft_notes) OR (p_sdi_reciprocate IS DISTINCT FROM sdi_reciprocate) OR (p_sui_reciprocate IS DISTINCT FROM sui_reciprocate) OR (p_show_s125_in_gross_wages_sui IS DISTINCT FROM show_s125_in_gross_wages_sui) OR (p_print_sui_return_if_zero IS DISTINCT FROM print_sui_return_if_zero) OR (p_payroll IS DISTINCT FROM payroll) OR (p_sales IS DISTINCT FROM sales) OR (p_corporate IS DISTINCT FROM corporate) OR (p_railroad IS DISTINCT FROM railroad) OR (p_unemployment IS DISTINCT FROM unemployment) OR (p_other IS DISTINCT FROM other) OR (p_uifsa_new_hire_reporting IS DISTINCT FROM uifsa_new_hire_reporting) OR (p_dd_child_support IS DISTINCT FROM dd_child_support) OR (p_use_state_misc_tax_return_code IS DISTINCT FROM use_state_misc_tax_return_code) OR (p_sy_state_tax_pmt_agency_nbr IS DISTINCT FROM sy_state_tax_pmt_agency_nbr) OR (p_sy_tax_pmt_report_nbr IS DISTINCT FROM sy_tax_pmt_report_nbr) OR (p_sy_state_reporting_agency_nbr IS DISTINCT FROM sy_state_reporting_agency_nbr) OR (p_pay_sdi_with IS DISTINCT FROM pay_sdi_with) OR (p_filler IS DISTINCT FROM filler) OR (p_tax_deposit_follow_federal IS DISTINCT FROM tax_deposit_follow_federal) OR (p_round_to_nearest_dollar IS DISTINCT FROM round_to_nearest_dollar) OR (p_sui_tax_payment_type_code IS DISTINCT FROM sui_tax_payment_type_code) OR (p_inc_sui_tax_payment_code IS DISTINCT FROM inc_sui_tax_payment_code) OR (p_fips_code IS DISTINCT FROM fips_code) OR (p_state_minimum_wage IS DISTINCT FROM state_minimum_wage) OR (p_state_tip_credit IS DISTINCT FROM state_tip_credit) OR (p_supplemental_wages_percentage IS DISTINCT FROM supplemental_wages_percentage) OR (p_weekly_sdi_tax_cap IS DISTINCT FROM weekly_sdi_tax_cap) OR (p_ee_sdi_maximum_wage IS DISTINCT FROM ee_sdi_maximum_wage) OR (p_er_sdi_maximum_wage IS DISTINCT FROM er_sdi_maximum_wage) OR (p_ee_sdi_rate IS DISTINCT FROM ee_sdi_rate) OR (p_er_sdi_rate IS DISTINCT FROM er_sdi_rate) OR (p_maximum_garnishment_percent IS DISTINCT FROM maximum_garnishment_percent) OR (p_garnish_min_wage_multiplier IS DISTINCT FROM garnish_min_wage_multiplier) OR (p_sales_tax_percentage IS DISTINCT FROM sales_tax_percentage) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_reciprocity_type IS DISTINCT FROM reciprocity_type) OR (p_print_w2 IS DISTINCT FROM print_w2) OR (p_cap_state_tax_credit IS DISTINCT FROM cap_state_tax_credit) OR (p_inactivate_marital_status IS DISTINCT FROM inactivate_marital_status) OR (p_state_ot_tip_credit IS DISTINCT FROM state_ot_tip_credit)) THEN
      BEGIN
        p_state_withholding = state_withholding;
        p_state_eft_type = state_eft_type;
        p_eft_notes = eft_notes;
        p_print_state_return_if_zero = print_state_return_if_zero;
        p_sui_eft_type = sui_eft_type;
        p_sui_eft_notes = sui_eft_notes;
        p_sdi_reciprocate = sdi_reciprocate;
        p_sui_reciprocate = sui_reciprocate;
        p_show_s125_in_gross_wages_sui = show_s125_in_gross_wages_sui;
        p_print_sui_return_if_zero = print_sui_return_if_zero;
        p_payroll = payroll;
        p_sales = sales;
        p_corporate = corporate;
        p_railroad = railroad;
        p_unemployment = unemployment;
        p_other = other;
        p_uifsa_new_hire_reporting = uifsa_new_hire_reporting;
        p_dd_child_support = dd_child_support;
        p_use_state_misc_tax_return_code = use_state_misc_tax_return_code;
        p_sy_state_tax_pmt_agency_nbr = sy_state_tax_pmt_agency_nbr;
        p_sy_tax_pmt_report_nbr = sy_tax_pmt_report_nbr;
        p_sy_state_reporting_agency_nbr = sy_state_reporting_agency_nbr;
        p_pay_sdi_with = pay_sdi_with;
        p_filler = filler;
        p_tax_deposit_follow_federal = tax_deposit_follow_federal;
        p_round_to_nearest_dollar = round_to_nearest_dollar;
        p_sui_tax_payment_type_code = sui_tax_payment_type_code;
        p_inc_sui_tax_payment_code = inc_sui_tax_payment_code;
        p_fips_code = fips_code;
        p_state_minimum_wage = state_minimum_wage;
        p_state_tip_credit = state_tip_credit;
        p_supplemental_wages_percentage = supplemental_wages_percentage;
        p_weekly_sdi_tax_cap = weekly_sdi_tax_cap;
        p_ee_sdi_maximum_wage = ee_sdi_maximum_wage;
        p_er_sdi_maximum_wage = er_sdi_maximum_wage;
        p_ee_sdi_rate = ee_sdi_rate;
        p_er_sdi_rate = er_sdi_rate;
        p_maximum_garnishment_percent = maximum_garnishment_percent;
        p_garnish_min_wage_multiplier = garnish_min_wage_multiplier;
        p_sales_tax_percentage = sales_tax_percentage;
        p_w2_box = w2_box;
        p_reciprocity_type = reciprocity_type;
        p_print_w2 = print_w2;
        p_cap_state_tax_credit = cap_state_tax_credit;
        p_inactivate_marital_status = inactivate_marital_status;
        p_state_ot_tip_credit = state_ot_tip_credit;
      END
      ELSE
        DELETE FROM sy_states WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_STATES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_state_deposit_freq(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_state_deposit_freq_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE when_due_type CHAR(1);
DECLARE VARIABLE p_when_due_type CHAR(1);
DECLARE VARIABLE number_of_days_for_when_due INTEGER;
DECLARE VARIABLE p_number_of_days_for_when_due INTEGER;
DECLARE VARIABLE threshold_period CHAR(1);
DECLARE VARIABLE p_threshold_period CHAR(1);
DECLARE VARIABLE change_status_on_threshold CHAR(1);
DECLARE VARIABLE p_change_status_on_threshold CHAR(1);
DECLARE VARIABLE thrd_mnth_due_end_of_next_mnth CHAR(1);
DECLARE VARIABLE p_thrd_mnth_due_end_of_next_mnth CHAR(1);
DECLARE VARIABLE description VARCHAR(20);
DECLARE VARIABLE p_description VARCHAR(20);
DECLARE VARIABLE threshold_dep_frequency_number INTEGER;
DECLARE VARIABLE p_threshold_dep_frequency_number INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE second_threshold_period CHAR(1);
DECLARE VARIABLE p_second_threshold_period CHAR(1);
DECLARE VARIABLE tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE p_tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE inc_tax_payment_code CHAR(1);
DECLARE VARIABLE p_inc_tax_payment_code CHAR(1);
DECLARE VARIABLE sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE p_sy_gl_agency_field_office_nbr INTEGER;
DECLARE VARIABLE frequency_type CHAR(1);
DECLARE VARIABLE p_frequency_type CHAR(1);
DECLARE VARIABLE threshold_amount NUMERIC(18,6);
DECLARE VARIABLE p_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE second_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE p_second_threshold_amount NUMERIC(18,6);
DECLARE VARIABLE qe_tax_coupon_sy_reports_nbr INTEGER;
DECLARE VARIABLE p_qe_tax_coupon_sy_reports_nbr INTEGER;
DECLARE VARIABLE qe_sy_gl_agency_office_nbr INTEGER;
DECLARE VARIABLE p_qe_sy_gl_agency_office_nbr INTEGER;
DECLARE VARIABLE hide_from_user CHAR(1);
DECLARE VARIABLE p_hide_from_user CHAR(1);
DECLARE VARIABLE second_threshold_dep_freq_nbr INTEGER;
DECLARE VARIABLE p_second_threshold_dep_freq_nbr INTEGER;
DECLARE VARIABLE pay_and_shiftback CHAR(1);
DECLARE VARIABLE p_pay_and_shiftback CHAR(1);
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_group_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_state_deposit_freq_nbr , when_due_type, number_of_days_for_when_due, threshold_period, change_status_on_threshold, thrd_mnth_due_end_of_next_mnth, description, threshold_dep_frequency_number, filler, second_threshold_period, tax_payment_type_code, inc_tax_payment_code, sy_gl_agency_field_office_nbr, frequency_type, threshold_amount, second_threshold_amount, qe_tax_coupon_sy_reports_nbr, qe_sy_gl_agency_office_nbr, hide_from_user, second_threshold_dep_freq_nbr, pay_and_shiftback, sy_reports_group_nbr
        FROM sy_state_deposit_freq
        ORDER BY sy_state_deposit_freq_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_state_deposit_freq_nbr, :when_due_type, :number_of_days_for_when_due, :threshold_period, :change_status_on_threshold, :thrd_mnth_due_end_of_next_mnth, :description, :threshold_dep_frequency_number, :filler, :second_threshold_period, :tax_payment_type_code, :inc_tax_payment_code, :sy_gl_agency_field_office_nbr, :frequency_type, :threshold_amount, :second_threshold_amount, :qe_tax_coupon_sy_reports_nbr, :qe_sy_gl_agency_office_nbr, :hide_from_user, :second_threshold_dep_freq_nbr, :pay_and_shiftback, :sy_reports_group_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_state_deposit_freq_nbr) OR (effective_date = '1/1/1900') OR (p_when_due_type IS DISTINCT FROM when_due_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_threshold_period IS DISTINCT FROM threshold_period) OR (p_change_status_on_threshold IS DISTINCT FROM change_status_on_threshold) OR (p_thrd_mnth_due_end_of_next_mnth IS DISTINCT FROM thrd_mnth_due_end_of_next_mnth) OR (p_description IS DISTINCT FROM description) OR (p_threshold_dep_frequency_number IS DISTINCT FROM threshold_dep_frequency_number) OR (p_filler IS DISTINCT FROM filler) OR (p_second_threshold_period IS DISTINCT FROM second_threshold_period) OR (p_tax_payment_type_code IS DISTINCT FROM tax_payment_type_code) OR (p_inc_tax_payment_code IS DISTINCT FROM inc_tax_payment_code) OR (p_sy_gl_agency_field_office_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_threshold_amount IS DISTINCT FROM threshold_amount) OR (p_second_threshold_amount IS DISTINCT FROM second_threshold_amount) OR (p_qe_tax_coupon_sy_reports_nbr IS DISTINCT FROM qe_tax_coupon_sy_reports_nbr) OR (p_qe_sy_gl_agency_office_nbr IS DISTINCT FROM qe_sy_gl_agency_office_nbr) OR (p_hide_from_user IS DISTINCT FROM hide_from_user) OR (p_second_threshold_dep_freq_nbr IS DISTINCT FROM second_threshold_dep_freq_nbr) OR (p_pay_and_shiftback IS DISTINCT FROM pay_and_shiftback) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr)) THEN
      BEGIN
        curr_nbr = sy_state_deposit_freq_nbr;
        p_when_due_type = when_due_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_threshold_period = threshold_period;
        p_change_status_on_threshold = change_status_on_threshold;
        p_thrd_mnth_due_end_of_next_mnth = thrd_mnth_due_end_of_next_mnth;
        p_description = description;
        p_threshold_dep_frequency_number = threshold_dep_frequency_number;
        p_filler = filler;
        p_second_threshold_period = second_threshold_period;
        p_tax_payment_type_code = tax_payment_type_code;
        p_inc_tax_payment_code = inc_tax_payment_code;
        p_sy_gl_agency_field_office_nbr = sy_gl_agency_field_office_nbr;
        p_frequency_type = frequency_type;
        p_threshold_amount = threshold_amount;
        p_second_threshold_amount = second_threshold_amount;
        p_qe_tax_coupon_sy_reports_nbr = qe_tax_coupon_sy_reports_nbr;
        p_qe_sy_gl_agency_office_nbr = qe_sy_gl_agency_office_nbr;
        p_hide_from_user = hide_from_user;
        p_second_threshold_dep_freq_nbr = second_threshold_dep_freq_nbr;
        p_pay_and_shiftback = pay_and_shiftback;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
      END
      ELSE
        DELETE FROM sy_state_deposit_freq WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, when_due_type, number_of_days_for_when_due, threshold_period, change_status_on_threshold, thrd_mnth_due_end_of_next_mnth, description, threshold_dep_frequency_number, filler, second_threshold_period, tax_payment_type_code, inc_tax_payment_code, sy_gl_agency_field_office_nbr, frequency_type, threshold_amount, second_threshold_amount, qe_tax_coupon_sy_reports_nbr, qe_sy_gl_agency_office_nbr, hide_from_user, second_threshold_dep_freq_nbr, pay_and_shiftback, sy_reports_group_nbr
        FROM sy_state_deposit_freq
        WHERE sy_state_deposit_freq_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :when_due_type, :number_of_days_for_when_due, :threshold_period, :change_status_on_threshold, :thrd_mnth_due_end_of_next_mnth, :description, :threshold_dep_frequency_number, :filler, :second_threshold_period, :tax_payment_type_code, :inc_tax_payment_code, :sy_gl_agency_field_office_nbr, :frequency_type, :threshold_amount, :second_threshold_amount, :qe_tax_coupon_sy_reports_nbr, :qe_sy_gl_agency_office_nbr, :hide_from_user, :second_threshold_dep_freq_nbr, :pay_and_shiftback, :sy_reports_group_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_when_due_type IS DISTINCT FROM when_due_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_threshold_period IS DISTINCT FROM threshold_period) OR (p_change_status_on_threshold IS DISTINCT FROM change_status_on_threshold) OR (p_thrd_mnth_due_end_of_next_mnth IS DISTINCT FROM thrd_mnth_due_end_of_next_mnth) OR (p_description IS DISTINCT FROM description) OR (p_threshold_dep_frequency_number IS DISTINCT FROM threshold_dep_frequency_number) OR (p_filler IS DISTINCT FROM filler) OR (p_second_threshold_period IS DISTINCT FROM second_threshold_period) OR (p_tax_payment_type_code IS DISTINCT FROM tax_payment_type_code) OR (p_inc_tax_payment_code IS DISTINCT FROM inc_tax_payment_code) OR (p_sy_gl_agency_field_office_nbr IS DISTINCT FROM sy_gl_agency_field_office_nbr) OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_threshold_amount IS DISTINCT FROM threshold_amount) OR (p_second_threshold_amount IS DISTINCT FROM second_threshold_amount) OR (p_qe_tax_coupon_sy_reports_nbr IS DISTINCT FROM qe_tax_coupon_sy_reports_nbr) OR (p_qe_sy_gl_agency_office_nbr IS DISTINCT FROM qe_sy_gl_agency_office_nbr) OR (p_hide_from_user IS DISTINCT FROM hide_from_user) OR (p_second_threshold_dep_freq_nbr IS DISTINCT FROM second_threshold_dep_freq_nbr) OR (p_pay_and_shiftback IS DISTINCT FROM pay_and_shiftback) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr)) THEN
      BEGIN
        p_when_due_type = when_due_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_threshold_period = threshold_period;
        p_change_status_on_threshold = change_status_on_threshold;
        p_thrd_mnth_due_end_of_next_mnth = thrd_mnth_due_end_of_next_mnth;
        p_description = description;
        p_threshold_dep_frequency_number = threshold_dep_frequency_number;
        p_filler = filler;
        p_second_threshold_period = second_threshold_period;
        p_tax_payment_type_code = tax_payment_type_code;
        p_inc_tax_payment_code = inc_tax_payment_code;
        p_sy_gl_agency_field_office_nbr = sy_gl_agency_field_office_nbr;
        p_frequency_type = frequency_type;
        p_threshold_amount = threshold_amount;
        p_second_threshold_amount = second_threshold_amount;
        p_qe_tax_coupon_sy_reports_nbr = qe_tax_coupon_sy_reports_nbr;
        p_qe_sy_gl_agency_office_nbr = qe_sy_gl_agency_office_nbr;
        p_hide_from_user = hide_from_user;
        p_second_threshold_dep_freq_nbr = second_threshold_dep_freq_nbr;
        p_pay_and_shiftback = pay_and_shiftback;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
      END
      ELSE
        DELETE FROM sy_state_deposit_freq WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_STATE_DEPOSIT_FREQ TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_state_exemptions(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_state_exemptions_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE e_d_code_type VARCHAR(2);
DECLARE VARIABLE p_e_d_code_type VARCHAR(2);
DECLARE VARIABLE exempt_state CHAR(1);
DECLARE VARIABLE p_exempt_state CHAR(1);
DECLARE VARIABLE exempt_employee_sdi CHAR(1);
DECLARE VARIABLE p_exempt_employee_sdi CHAR(1);
DECLARE VARIABLE exempt_employer_sdi CHAR(1);
DECLARE VARIABLE p_exempt_employer_sdi CHAR(1);
DECLARE VARIABLE exempt_employee_sui CHAR(1);
DECLARE VARIABLE p_exempt_employee_sui CHAR(1);
DECLARE VARIABLE exempt_employer_sui CHAR(1);
DECLARE VARIABLE p_exempt_employer_sui CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_state_exemptions_nbr , e_d_code_type, exempt_state, exempt_employee_sdi, exempt_employer_sdi, exempt_employee_sui, exempt_employer_sui
        FROM sy_state_exemptions
        ORDER BY sy_state_exemptions_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_state_exemptions_nbr, :e_d_code_type, :exempt_state, :exempt_employee_sdi, :exempt_employer_sdi, :exempt_employee_sui, :exempt_employer_sui
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_state_exemptions_nbr) OR (effective_date = '1/1/1900') OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_exempt_state IS DISTINCT FROM exempt_state) OR (p_exempt_employee_sdi IS DISTINCT FROM exempt_employee_sdi) OR (p_exempt_employer_sdi IS DISTINCT FROM exempt_employer_sdi) OR (p_exempt_employee_sui IS DISTINCT FROM exempt_employee_sui) OR (p_exempt_employer_sui IS DISTINCT FROM exempt_employer_sui)) THEN
      BEGIN
        curr_nbr = sy_state_exemptions_nbr;
        p_e_d_code_type = e_d_code_type;
        p_exempt_state = exempt_state;
        p_exempt_employee_sdi = exempt_employee_sdi;
        p_exempt_employer_sdi = exempt_employer_sdi;
        p_exempt_employee_sui = exempt_employee_sui;
        p_exempt_employer_sui = exempt_employer_sui;
      END
      ELSE
        DELETE FROM sy_state_exemptions WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, e_d_code_type, exempt_state, exempt_employee_sdi, exempt_employer_sdi, exempt_employee_sui, exempt_employer_sui
        FROM sy_state_exemptions
        WHERE sy_state_exemptions_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :e_d_code_type, :exempt_state, :exempt_employee_sdi, :exempt_employer_sdi, :exempt_employee_sui, :exempt_employer_sui
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_exempt_state IS DISTINCT FROM exempt_state) OR (p_exempt_employee_sdi IS DISTINCT FROM exempt_employee_sdi) OR (p_exempt_employer_sdi IS DISTINCT FROM exempt_employer_sdi) OR (p_exempt_employee_sui IS DISTINCT FROM exempt_employee_sui) OR (p_exempt_employer_sui IS DISTINCT FROM exempt_employer_sui)) THEN
      BEGIN
        p_e_d_code_type = e_d_code_type;
        p_exempt_state = exempt_state;
        p_exempt_employee_sdi = exempt_employee_sdi;
        p_exempt_employer_sdi = exempt_employer_sdi;
        p_exempt_employee_sui = exempt_employee_sui;
        p_exempt_employer_sui = exempt_employer_sui;
      END
      ELSE
        DELETE FROM sy_state_exemptions WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_STATE_EXEMPTIONS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_state_marital_status(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_state_marital_status_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE status_type CHAR(2);
DECLARE VARIABLE p_status_type CHAR(2);
DECLARE VARIABLE status_description VARCHAR(40);
DECLARE VARIABLE p_status_description VARCHAR(40);
DECLARE VARIABLE personal_exemptions INTEGER;
DECLARE VARIABLE p_personal_exemptions INTEGER;
DECLARE VARIABLE deduct_federal CHAR(1);
DECLARE VARIABLE p_deduct_federal CHAR(1);
DECLARE VARIABLE deduct_fica CHAR(1);
DECLARE VARIABLE p_deduct_fica CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE state_percent_of_federal NUMERIC(18,6);
DECLARE VARIABLE p_state_percent_of_federal NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_pcnt_gross NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_pcnt_gross NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_min_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_min_amount NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_max_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_max_amount NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_flat_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_flat_amount NUMERIC(18,6);
DECLARE VARIABLE standard_per_exemption_allow NUMERIC(18,6);
DECLARE VARIABLE p_standard_per_exemption_allow NUMERIC(18,6);
DECLARE VARIABLE deduct_federal_maximum_amount NUMERIC(18,6);
DECLARE VARIABLE p_deduct_federal_maximum_amount NUMERIC(18,6);
DECLARE VARIABLE per_dependent_allowance NUMERIC(18,6);
DECLARE VARIABLE p_per_dependent_allowance NUMERIC(18,6);
DECLARE VARIABLE personal_tax_credit_amount NUMERIC(18,6);
DECLARE VARIABLE p_personal_tax_credit_amount NUMERIC(18,6);
DECLARE VARIABLE tax_credit_per_dependent NUMERIC(18,6);
DECLARE VARIABLE p_tax_credit_per_dependent NUMERIC(18,6);
DECLARE VARIABLE tax_credit_per_allowance NUMERIC(18,6);
DECLARE VARIABLE p_tax_credit_per_allowance NUMERIC(18,6);
DECLARE VARIABLE high_income_per_exempt_allow NUMERIC(18,6);
DECLARE VARIABLE p_high_income_per_exempt_allow NUMERIC(18,6);
DECLARE VARIABLE defined_high_income_amount NUMERIC(18,6);
DECLARE VARIABLE p_defined_high_income_amount NUMERIC(18,6);
DECLARE VARIABLE minimum_taxable_income NUMERIC(18,6);
DECLARE VARIABLE p_minimum_taxable_income NUMERIC(18,6);
DECLARE VARIABLE additional_exempt_allowance NUMERIC(18,6);
DECLARE VARIABLE p_additional_exempt_allowance NUMERIC(18,6);
DECLARE VARIABLE additional_deduction_allowance NUMERIC(18,6);
DECLARE VARIABLE p_additional_deduction_allowance NUMERIC(18,6);
DECLARE VARIABLE deduct_fica__maximum_amount NUMERIC(18,6);
DECLARE VARIABLE p_deduct_fica__maximum_amount NUMERIC(18,6);
DECLARE VARIABLE blind_exemption_amount NUMERIC(18,6);
DECLARE VARIABLE p_blind_exemption_amount NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_state_marital_status_nbr , status_type, status_description, personal_exemptions, deduct_federal, deduct_fica, filler, state_percent_of_federal, standard_deduction_pcnt_gross, standard_deduction_min_amount, standard_deduction_max_amount, standard_deduction_flat_amount, standard_per_exemption_allow, deduct_federal_maximum_amount, per_dependent_allowance, personal_tax_credit_amount, tax_credit_per_dependent, tax_credit_per_allowance, high_income_per_exempt_allow, defined_high_income_amount, minimum_taxable_income, additional_exempt_allowance, additional_deduction_allowance, deduct_fica__maximum_amount, blind_exemption_amount
        FROM sy_state_marital_status
        ORDER BY sy_state_marital_status_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_state_marital_status_nbr, :status_type, :status_description, :personal_exemptions, :deduct_federal, :deduct_fica, :filler, :state_percent_of_federal, :standard_deduction_pcnt_gross, :standard_deduction_min_amount, :standard_deduction_max_amount, :standard_deduction_flat_amount, :standard_per_exemption_allow, :deduct_federal_maximum_amount, :per_dependent_allowance, :personal_tax_credit_amount, :tax_credit_per_dependent, :tax_credit_per_allowance, :high_income_per_exempt_allow, :defined_high_income_amount, :minimum_taxable_income, :additional_exempt_allowance, :additional_deduction_allowance, :deduct_fica__maximum_amount, :blind_exemption_amount
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_state_marital_status_nbr) OR (effective_date = '1/1/1900') OR (p_status_type IS DISTINCT FROM status_type) OR (p_status_description IS DISTINCT FROM status_description) OR (p_personal_exemptions IS DISTINCT FROM personal_exemptions) OR (p_deduct_federal IS DISTINCT FROM deduct_federal) OR (p_deduct_fica IS DISTINCT FROM deduct_fica) OR (p_filler IS DISTINCT FROM filler) OR (p_state_percent_of_federal IS DISTINCT FROM state_percent_of_federal) OR (p_standard_deduction_pcnt_gross IS DISTINCT FROM standard_deduction_pcnt_gross) OR (p_standard_deduction_min_amount IS DISTINCT FROM standard_deduction_min_amount) OR (p_standard_deduction_max_amount IS DISTINCT FROM standard_deduction_max_amount) OR (p_standard_deduction_flat_amount IS DISTINCT FROM standard_deduction_flat_amount) OR (p_standard_per_exemption_allow IS DISTINCT FROM standard_per_exemption_allow) OR (p_deduct_federal_maximum_amount IS DISTINCT FROM deduct_federal_maximum_amount) OR (p_per_dependent_allowance IS DISTINCT FROM per_dependent_allowance) OR (p_personal_tax_credit_amount IS DISTINCT FROM personal_tax_credit_amount) OR (p_tax_credit_per_dependent IS DISTINCT FROM tax_credit_per_dependent) OR (p_tax_credit_per_allowance IS DISTINCT FROM tax_credit_per_allowance) OR (p_high_income_per_exempt_allow IS DISTINCT FROM high_income_per_exempt_allow) OR (p_defined_high_income_amount IS DISTINCT FROM defined_high_income_amount) OR (p_minimum_taxable_income IS DISTINCT FROM minimum_taxable_income) OR (p_additional_exempt_allowance IS DISTINCT FROM additional_exempt_allowance) OR (p_additional_deduction_allowance IS DISTINCT FROM additional_deduction_allowance) OR (p_deduct_fica__maximum_amount IS DISTINCT FROM deduct_fica__maximum_amount) OR (p_blind_exemption_amount IS DISTINCT FROM blind_exemption_amount)) THEN
      BEGIN
        curr_nbr = sy_state_marital_status_nbr;
        p_status_type = status_type;
        p_status_description = status_description;
        p_personal_exemptions = personal_exemptions;
        p_deduct_federal = deduct_federal;
        p_deduct_fica = deduct_fica;
        p_filler = filler;
        p_state_percent_of_federal = state_percent_of_federal;
        p_standard_deduction_pcnt_gross = standard_deduction_pcnt_gross;
        p_standard_deduction_min_amount = standard_deduction_min_amount;
        p_standard_deduction_max_amount = standard_deduction_max_amount;
        p_standard_deduction_flat_amount = standard_deduction_flat_amount;
        p_standard_per_exemption_allow = standard_per_exemption_allow;
        p_deduct_federal_maximum_amount = deduct_federal_maximum_amount;
        p_per_dependent_allowance = per_dependent_allowance;
        p_personal_tax_credit_amount = personal_tax_credit_amount;
        p_tax_credit_per_dependent = tax_credit_per_dependent;
        p_tax_credit_per_allowance = tax_credit_per_allowance;
        p_high_income_per_exempt_allow = high_income_per_exempt_allow;
        p_defined_high_income_amount = defined_high_income_amount;
        p_minimum_taxable_income = minimum_taxable_income;
        p_additional_exempt_allowance = additional_exempt_allowance;
        p_additional_deduction_allowance = additional_deduction_allowance;
        p_deduct_fica__maximum_amount = deduct_fica__maximum_amount;
        p_blind_exemption_amount = blind_exemption_amount;
      END
      ELSE
        DELETE FROM sy_state_marital_status WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, status_type, status_description, personal_exemptions, deduct_federal, deduct_fica, filler, state_percent_of_federal, standard_deduction_pcnt_gross, standard_deduction_min_amount, standard_deduction_max_amount, standard_deduction_flat_amount, standard_per_exemption_allow, deduct_federal_maximum_amount, per_dependent_allowance, personal_tax_credit_amount, tax_credit_per_dependent, tax_credit_per_allowance, high_income_per_exempt_allow, defined_high_income_amount, minimum_taxable_income, additional_exempt_allowance, additional_deduction_allowance, deduct_fica__maximum_amount, blind_exemption_amount
        FROM sy_state_marital_status
        WHERE sy_state_marital_status_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :status_type, :status_description, :personal_exemptions, :deduct_federal, :deduct_fica, :filler, :state_percent_of_federal, :standard_deduction_pcnt_gross, :standard_deduction_min_amount, :standard_deduction_max_amount, :standard_deduction_flat_amount, :standard_per_exemption_allow, :deduct_federal_maximum_amount, :per_dependent_allowance, :personal_tax_credit_amount, :tax_credit_per_dependent, :tax_credit_per_allowance, :high_income_per_exempt_allow, :defined_high_income_amount, :minimum_taxable_income, :additional_exempt_allowance, :additional_deduction_allowance, :deduct_fica__maximum_amount, :blind_exemption_amount
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_status_type IS DISTINCT FROM status_type) OR (p_status_description IS DISTINCT FROM status_description) OR (p_personal_exemptions IS DISTINCT FROM personal_exemptions) OR (p_deduct_federal IS DISTINCT FROM deduct_federal) OR (p_deduct_fica IS DISTINCT FROM deduct_fica) OR (p_filler IS DISTINCT FROM filler) OR (p_state_percent_of_federal IS DISTINCT FROM state_percent_of_federal) OR (p_standard_deduction_pcnt_gross IS DISTINCT FROM standard_deduction_pcnt_gross) OR (p_standard_deduction_min_amount IS DISTINCT FROM standard_deduction_min_amount) OR (p_standard_deduction_max_amount IS DISTINCT FROM standard_deduction_max_amount) OR (p_standard_deduction_flat_amount IS DISTINCT FROM standard_deduction_flat_amount) OR (p_standard_per_exemption_allow IS DISTINCT FROM standard_per_exemption_allow) OR (p_deduct_federal_maximum_amount IS DISTINCT FROM deduct_federal_maximum_amount) OR (p_per_dependent_allowance IS DISTINCT FROM per_dependent_allowance) OR (p_personal_tax_credit_amount IS DISTINCT FROM personal_tax_credit_amount) OR (p_tax_credit_per_dependent IS DISTINCT FROM tax_credit_per_dependent) OR (p_tax_credit_per_allowance IS DISTINCT FROM tax_credit_per_allowance) OR (p_high_income_per_exempt_allow IS DISTINCT FROM high_income_per_exempt_allow) OR (p_defined_high_income_amount IS DISTINCT FROM defined_high_income_amount) OR (p_minimum_taxable_income IS DISTINCT FROM minimum_taxable_income) OR (p_additional_exempt_allowance IS DISTINCT FROM additional_exempt_allowance) OR (p_additional_deduction_allowance IS DISTINCT FROM additional_deduction_allowance) OR (p_deduct_fica__maximum_amount IS DISTINCT FROM deduct_fica__maximum_amount) OR (p_blind_exemption_amount IS DISTINCT FROM blind_exemption_amount)) THEN
      BEGIN
        p_status_type = status_type;
        p_status_description = status_description;
        p_personal_exemptions = personal_exemptions;
        p_deduct_federal = deduct_federal;
        p_deduct_fica = deduct_fica;
        p_filler = filler;
        p_state_percent_of_federal = state_percent_of_federal;
        p_standard_deduction_pcnt_gross = standard_deduction_pcnt_gross;
        p_standard_deduction_min_amount = standard_deduction_min_amount;
        p_standard_deduction_max_amount = standard_deduction_max_amount;
        p_standard_deduction_flat_amount = standard_deduction_flat_amount;
        p_standard_per_exemption_allow = standard_per_exemption_allow;
        p_deduct_federal_maximum_amount = deduct_federal_maximum_amount;
        p_per_dependent_allowance = per_dependent_allowance;
        p_personal_tax_credit_amount = personal_tax_credit_amount;
        p_tax_credit_per_dependent = tax_credit_per_dependent;
        p_tax_credit_per_allowance = tax_credit_per_allowance;
        p_high_income_per_exempt_allow = high_income_per_exempt_allow;
        p_defined_high_income_amount = defined_high_income_amount;
        p_minimum_taxable_income = minimum_taxable_income;
        p_additional_exempt_allowance = additional_exempt_allowance;
        p_additional_deduction_allowance = additional_deduction_allowance;
        p_deduct_fica__maximum_amount = deduct_fica__maximum_amount;
        p_blind_exemption_amount = blind_exemption_amount;
      END
      ELSE
        DELETE FROM sy_state_marital_status WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_STATE_MARITAL_STATUS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_state_tax_chart(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_state_tax_chart_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE sy_state_marital_status_nbr INTEGER;
DECLARE VARIABLE p_sy_state_marital_status_nbr INTEGER;
DECLARE VARIABLE entry_type CHAR(1);
DECLARE VARIABLE p_entry_type CHAR(1);
DECLARE VARIABLE minimum NUMERIC(18,6);
DECLARE VARIABLE p_minimum NUMERIC(18,6);
DECLARE VARIABLE maximum NUMERIC(18,6);
DECLARE VARIABLE p_maximum NUMERIC(18,6);
DECLARE VARIABLE percentage NUMERIC(18,6);
DECLARE VARIABLE p_percentage NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_state_tax_chart_nbr , sy_state_marital_status_nbr, entry_type, minimum, maximum, percentage
        FROM sy_state_tax_chart
        ORDER BY sy_state_tax_chart_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_state_tax_chart_nbr, :sy_state_marital_status_nbr, :entry_type, :minimum, :maximum, :percentage
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_state_tax_chart_nbr) OR (effective_date = '1/1/1900') OR (p_sy_state_marital_status_nbr IS DISTINCT FROM sy_state_marital_status_nbr) OR (p_entry_type IS DISTINCT FROM entry_type) OR (p_minimum IS DISTINCT FROM minimum) OR (p_maximum IS DISTINCT FROM maximum) OR (p_percentage IS DISTINCT FROM percentage)) THEN
      BEGIN
        curr_nbr = sy_state_tax_chart_nbr;
        p_sy_state_marital_status_nbr = sy_state_marital_status_nbr;
        p_entry_type = entry_type;
        p_minimum = minimum;
        p_maximum = maximum;
        p_percentage = percentage;
      END
      ELSE
        DELETE FROM sy_state_tax_chart WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, sy_state_marital_status_nbr, entry_type, minimum, maximum, percentage
        FROM sy_state_tax_chart
        WHERE sy_state_tax_chart_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :sy_state_marital_status_nbr, :entry_type, :minimum, :maximum, :percentage
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_sy_state_marital_status_nbr IS DISTINCT FROM sy_state_marital_status_nbr) OR (p_entry_type IS DISTINCT FROM entry_type) OR (p_minimum IS DISTINCT FROM minimum) OR (p_maximum IS DISTINCT FROM maximum) OR (p_percentage IS DISTINCT FROM percentage)) THEN
      BEGIN
        p_sy_state_marital_status_nbr = sy_state_marital_status_nbr;
        p_entry_type = entry_type;
        p_minimum = minimum;
        p_maximum = maximum;
        p_percentage = percentage;
      END
      ELSE
        DELETE FROM sy_state_tax_chart WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_STATE_TAX_CHART TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_sui(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_sui_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE sui_tax_name VARCHAR(40);
DECLARE VARIABLE p_sui_tax_name VARCHAR(40);
DECLARE VARIABLE ee_er_or_ee_other_or_er_other CHAR(1);
DECLARE VARIABLE p_ee_er_or_ee_other_or_er_other CHAR(1);
DECLARE VARIABLE future_default_rate_begin_date TIMESTAMP;
DECLARE VARIABLE p_future_default_rate_begin_date TIMESTAMP;
DECLARE VARIABLE future_max_wage_begin_date TIMESTAMP;
DECLARE VARIABLE p_future_max_wage_begin_date TIMESTAMP;
DECLARE VARIABLE use_misc_tax_return_code CHAR(1);
DECLARE VARIABLE p_use_misc_tax_return_code CHAR(1);
DECLARE VARIABLE sy_sui_tax_pmt_agency_nbr INTEGER;
DECLARE VARIABLE p_sy_sui_tax_pmt_agency_nbr INTEGER;
DECLARE VARIABLE sy_tax_pmt_report_nbr INTEGER;
DECLARE VARIABLE p_sy_tax_pmt_report_nbr INTEGER;
DECLARE VARIABLE sy_sui_reporting_agency_nbr INTEGER;
DECLARE VARIABLE p_sy_sui_reporting_agency_nbr INTEGER;
DECLARE VARIABLE round_to_nearest_dollar CHAR(1);
DECLARE VARIABLE p_round_to_nearest_dollar CHAR(1);
DECLARE VARIABLE sui_active CHAR(1);
DECLARE VARIABLE p_sui_active CHAR(1);
DECLARE VARIABLE new_company_default_rate NUMERIC(18,6);
DECLARE VARIABLE p_new_company_default_rate NUMERIC(18,6);
DECLARE VARIABLE future_default_rate NUMERIC(18,6);
DECLARE VARIABLE p_future_default_rate NUMERIC(18,6);
DECLARE VARIABLE maximum_wage NUMERIC(18,6);
DECLARE VARIABLE p_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE future_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE p_future_maximum_wage NUMERIC(18,6);
DECLARE VARIABLE tax_coupon_sy_reports_nbr INTEGER;
DECLARE VARIABLE p_tax_coupon_sy_reports_nbr INTEGER;
DECLARE VARIABLE frequency_type CHAR(1);
DECLARE VARIABLE p_frequency_type CHAR(1);
DECLARE VARIABLE w2_box VARCHAR(10);
DECLARE VARIABLE p_w2_box VARCHAR(10);
DECLARE VARIABLE pay_with_state CHAR(1);
DECLARE VARIABLE p_pay_with_state CHAR(1);
DECLARE VARIABLE global_rate NUMERIC(18,6);
DECLARE VARIABLE p_global_rate NUMERIC(18,6);
DECLARE VARIABLE e_d_code_type VARCHAR(2);
DECLARE VARIABLE p_e_d_code_type VARCHAR(2);
DECLARE VARIABLE fte_exemption INTEGER;
DECLARE VARIABLE p_fte_exemption INTEGER;
DECLARE VARIABLE fte_weekly_hours INTEGER;
DECLARE VARIABLE p_fte_weekly_hours INTEGER;
DECLARE VARIABLE fte_multiplier INTEGER;
DECLARE VARIABLE p_fte_multiplier INTEGER;
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_group_nbr INTEGER;
DECLARE VARIABLE alternate_taxable_wage_base NUMERIC(18,6);
DECLARE VARIABLE p_alternate_taxable_wage_base NUMERIC(18,6);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_sui_nbr , sui_tax_name, ee_er_or_ee_other_or_er_other, future_default_rate_begin_date, future_max_wage_begin_date, use_misc_tax_return_code, sy_sui_tax_pmt_agency_nbr, sy_tax_pmt_report_nbr, sy_sui_reporting_agency_nbr, round_to_nearest_dollar, sui_active, new_company_default_rate, future_default_rate, maximum_wage, future_maximum_wage, tax_coupon_sy_reports_nbr, frequency_type, w2_box, pay_with_state, global_rate, e_d_code_type, fte_exemption, fte_weekly_hours, fte_multiplier, sy_reports_group_nbr, alternate_taxable_wage_base
        FROM sy_sui
        ORDER BY sy_sui_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_sui_nbr, :sui_tax_name, :ee_er_or_ee_other_or_er_other, :future_default_rate_begin_date, :future_max_wage_begin_date, :use_misc_tax_return_code, :sy_sui_tax_pmt_agency_nbr, :sy_tax_pmt_report_nbr, :sy_sui_reporting_agency_nbr, :round_to_nearest_dollar, :sui_active, :new_company_default_rate, :future_default_rate, :maximum_wage, :future_maximum_wage, :tax_coupon_sy_reports_nbr, :frequency_type, :w2_box, :pay_with_state, :global_rate, :e_d_code_type, :fte_exemption, :fte_weekly_hours, :fte_multiplier, :sy_reports_group_nbr, :alternate_taxable_wage_base
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_sui_nbr) OR (effective_date = '1/1/1900') OR (p_sui_tax_name IS DISTINCT FROM sui_tax_name) OR (p_ee_er_or_ee_other_or_er_other IS DISTINCT FROM ee_er_or_ee_other_or_er_other) OR (p_future_default_rate_begin_date IS DISTINCT FROM future_default_rate_begin_date) OR (p_future_max_wage_begin_date IS DISTINCT FROM future_max_wage_begin_date) OR (p_use_misc_tax_return_code IS DISTINCT FROM use_misc_tax_return_code) OR (p_sy_sui_tax_pmt_agency_nbr IS DISTINCT FROM sy_sui_tax_pmt_agency_nbr) OR (p_sy_tax_pmt_report_nbr IS DISTINCT FROM sy_tax_pmt_report_nbr) OR (p_sy_sui_reporting_agency_nbr IS DISTINCT FROM sy_sui_reporting_agency_nbr) OR (p_round_to_nearest_dollar IS DISTINCT FROM round_to_nearest_dollar) OR (p_sui_active IS DISTINCT FROM sui_active) OR (p_new_company_default_rate IS DISTINCT FROM new_company_default_rate) OR (p_future_default_rate IS DISTINCT FROM future_default_rate) OR (p_maximum_wage IS DISTINCT FROM maximum_wage) OR (p_future_maximum_wage IS DISTINCT FROM future_maximum_wage) OR (p_tax_coupon_sy_reports_nbr IS DISTINCT FROM tax_coupon_sy_reports_nbr) OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_pay_with_state IS DISTINCT FROM pay_with_state) OR (p_global_rate IS DISTINCT FROM global_rate) OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_fte_exemption IS DISTINCT FROM fte_exemption) OR (p_fte_weekly_hours IS DISTINCT FROM fte_weekly_hours) OR (p_fte_multiplier IS DISTINCT FROM fte_multiplier) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (p_alternate_taxable_wage_base IS DISTINCT FROM alternate_taxable_wage_base)) THEN
      BEGIN
        curr_nbr = sy_sui_nbr;
        p_sui_tax_name = sui_tax_name;
        p_ee_er_or_ee_other_or_er_other = ee_er_or_ee_other_or_er_other;
        p_future_default_rate_begin_date = future_default_rate_begin_date;
        p_future_max_wage_begin_date = future_max_wage_begin_date;
        p_use_misc_tax_return_code = use_misc_tax_return_code;
        p_sy_sui_tax_pmt_agency_nbr = sy_sui_tax_pmt_agency_nbr;
        p_sy_tax_pmt_report_nbr = sy_tax_pmt_report_nbr;
        p_sy_sui_reporting_agency_nbr = sy_sui_reporting_agency_nbr;
        p_round_to_nearest_dollar = round_to_nearest_dollar;
        p_sui_active = sui_active;
        p_new_company_default_rate = new_company_default_rate;
        p_future_default_rate = future_default_rate;
        p_maximum_wage = maximum_wage;
        p_future_maximum_wage = future_maximum_wage;
        p_tax_coupon_sy_reports_nbr = tax_coupon_sy_reports_nbr;
        p_frequency_type = frequency_type;
        p_w2_box = w2_box;
        p_pay_with_state = pay_with_state;
        p_global_rate = global_rate;
        p_e_d_code_type = e_d_code_type;
        p_fte_exemption = fte_exemption;
        p_fte_weekly_hours = fte_weekly_hours;
        p_fte_multiplier = fte_multiplier;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
        p_alternate_taxable_wage_base = alternate_taxable_wage_base;
      END
      ELSE
        DELETE FROM sy_sui WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, sui_tax_name, ee_er_or_ee_other_or_er_other, future_default_rate_begin_date, future_max_wage_begin_date, use_misc_tax_return_code, sy_sui_tax_pmt_agency_nbr, sy_tax_pmt_report_nbr, sy_sui_reporting_agency_nbr, round_to_nearest_dollar, sui_active, new_company_default_rate, future_default_rate, maximum_wage, future_maximum_wage, tax_coupon_sy_reports_nbr, frequency_type, w2_box, pay_with_state, global_rate, e_d_code_type, fte_exemption, fte_weekly_hours, fte_multiplier, sy_reports_group_nbr, alternate_taxable_wage_base
        FROM sy_sui
        WHERE sy_sui_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :sui_tax_name, :ee_er_or_ee_other_or_er_other, :future_default_rate_begin_date, :future_max_wage_begin_date, :use_misc_tax_return_code, :sy_sui_tax_pmt_agency_nbr, :sy_tax_pmt_report_nbr, :sy_sui_reporting_agency_nbr, :round_to_nearest_dollar, :sui_active, :new_company_default_rate, :future_default_rate, :maximum_wage, :future_maximum_wage, :tax_coupon_sy_reports_nbr, :frequency_type, :w2_box, :pay_with_state, :global_rate, :e_d_code_type, :fte_exemption, :fte_weekly_hours, :fte_multiplier, :sy_reports_group_nbr, :alternate_taxable_wage_base
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_sui_tax_name IS DISTINCT FROM sui_tax_name) OR (p_ee_er_or_ee_other_or_er_other IS DISTINCT FROM ee_er_or_ee_other_or_er_other) OR (p_future_default_rate_begin_date IS DISTINCT FROM future_default_rate_begin_date) OR (p_future_max_wage_begin_date IS DISTINCT FROM future_max_wage_begin_date) OR (p_use_misc_tax_return_code IS DISTINCT FROM use_misc_tax_return_code) OR (p_sy_sui_tax_pmt_agency_nbr IS DISTINCT FROM sy_sui_tax_pmt_agency_nbr) OR (p_sy_tax_pmt_report_nbr IS DISTINCT FROM sy_tax_pmt_report_nbr) OR (p_sy_sui_reporting_agency_nbr IS DISTINCT FROM sy_sui_reporting_agency_nbr) OR (p_round_to_nearest_dollar IS DISTINCT FROM round_to_nearest_dollar) OR (p_sui_active IS DISTINCT FROM sui_active) OR (p_new_company_default_rate IS DISTINCT FROM new_company_default_rate) OR (p_future_default_rate IS DISTINCT FROM future_default_rate) OR (p_maximum_wage IS DISTINCT FROM maximum_wage) OR (p_future_maximum_wage IS DISTINCT FROM future_maximum_wage) OR (p_tax_coupon_sy_reports_nbr IS DISTINCT FROM tax_coupon_sy_reports_nbr) OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_w2_box IS DISTINCT FROM w2_box) OR (p_pay_with_state IS DISTINCT FROM pay_with_state) OR (p_global_rate IS DISTINCT FROM global_rate) OR (p_e_d_code_type IS DISTINCT FROM e_d_code_type) OR (p_fte_exemption IS DISTINCT FROM fte_exemption) OR (p_fte_weekly_hours IS DISTINCT FROM fte_weekly_hours) OR (p_fte_multiplier IS DISTINCT FROM fte_multiplier) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (p_alternate_taxable_wage_base IS DISTINCT FROM alternate_taxable_wage_base)) THEN
      BEGIN
        p_sui_tax_name = sui_tax_name;
        p_ee_er_or_ee_other_or_er_other = ee_er_or_ee_other_or_er_other;
        p_future_default_rate_begin_date = future_default_rate_begin_date;
        p_future_max_wage_begin_date = future_max_wage_begin_date;
        p_use_misc_tax_return_code = use_misc_tax_return_code;
        p_sy_sui_tax_pmt_agency_nbr = sy_sui_tax_pmt_agency_nbr;
        p_sy_tax_pmt_report_nbr = sy_tax_pmt_report_nbr;
        p_sy_sui_reporting_agency_nbr = sy_sui_reporting_agency_nbr;
        p_round_to_nearest_dollar = round_to_nearest_dollar;
        p_sui_active = sui_active;
        p_new_company_default_rate = new_company_default_rate;
        p_future_default_rate = future_default_rate;
        p_maximum_wage = maximum_wage;
        p_future_maximum_wage = future_maximum_wage;
        p_tax_coupon_sy_reports_nbr = tax_coupon_sy_reports_nbr;
        p_frequency_type = frequency_type;
        p_w2_box = w2_box;
        p_pay_with_state = pay_with_state;
        p_global_rate = global_rate;
        p_e_d_code_type = e_d_code_type;
        p_fte_exemption = fte_exemption;
        p_fte_weekly_hours = fte_weekly_hours;
        p_fte_multiplier = fte_multiplier;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
        p_alternate_taxable_wage_base = alternate_taxable_wage_base;
      END
      ELSE
        DELETE FROM sy_sui WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_SUI TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sy_agency_deposit_freq(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_agency_deposit_freq_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE frequency_type CHAR(1);
DECLARE VARIABLE p_frequency_type CHAR(1);
DECLARE VARIABLE number_of_days_for_when_due INTEGER;
DECLARE VARIABLE p_number_of_days_for_when_due INTEGER;
DECLARE VARIABLE sy_reports_group_nbr INTEGER;
DECLARE VARIABLE p_sy_reports_group_nbr INTEGER;
DECLARE VARIABLE tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE p_tax_payment_type_code VARCHAR(10);
DECLARE VARIABLE when_due_type CHAR(1);
DECLARE VARIABLE p_when_due_type CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_agency_deposit_freq_nbr , frequency_type, number_of_days_for_when_due, sy_reports_group_nbr, tax_payment_type_code, when_due_type
        FROM sy_agency_deposit_freq
        ORDER BY sy_agency_deposit_freq_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_agency_deposit_freq_nbr, :frequency_type, :number_of_days_for_when_due, :sy_reports_group_nbr, :tax_payment_type_code, :when_due_type
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_agency_deposit_freq_nbr) OR (effective_date = '1/1/1900') OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (p_tax_payment_type_code IS DISTINCT FROM tax_payment_type_code) OR (p_when_due_type IS DISTINCT FROM when_due_type)) THEN
      BEGIN
        curr_nbr = sy_agency_deposit_freq_nbr;
        p_frequency_type = frequency_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
        p_tax_payment_type_code = tax_payment_type_code;
        p_when_due_type = when_due_type;
      END
      ELSE
        DELETE FROM sy_agency_deposit_freq WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, frequency_type, number_of_days_for_when_due, sy_reports_group_nbr, tax_payment_type_code, when_due_type
        FROM sy_agency_deposit_freq
        WHERE sy_agency_deposit_freq_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :frequency_type, :number_of_days_for_when_due, :sy_reports_group_nbr, :tax_payment_type_code, :when_due_type
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_frequency_type IS DISTINCT FROM frequency_type) OR (p_number_of_days_for_when_due IS DISTINCT FROM number_of_days_for_when_due) OR (p_sy_reports_group_nbr IS DISTINCT FROM sy_reports_group_nbr) OR (p_tax_payment_type_code IS DISTINCT FROM tax_payment_type_code) OR (p_when_due_type IS DISTINCT FROM when_due_type)) THEN
      BEGIN
        p_frequency_type = frequency_type;
        p_number_of_days_for_when_due = number_of_days_for_when_due;
        p_sy_reports_group_nbr = sy_reports_group_nbr;
        p_tax_payment_type_code = tax_payment_type_code;
        p_when_due_type = when_due_type;
      END
      ELSE
        DELETE FROM sy_agency_deposit_freq WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_AGENCY_DEPOSIT_FREQ TO EUSER
^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.5', 'Evolution System Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
