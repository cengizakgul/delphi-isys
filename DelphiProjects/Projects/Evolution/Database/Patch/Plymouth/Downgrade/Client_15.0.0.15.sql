/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.16';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


DROP TRIGGER T_AD_CO_SUI_9
^


DROP TRIGGER T_AIU_CO_SUI_3
^


DROP TRIGGER T_AU_CO_SUI_9
^


ALTER TABLE CO_SUI
DROP RATE_LAST_REVIEWED_DATE
^


DROP TRIGGER T_AD_CO_8
^


DROP TRIGGER T_AIU_CO_3
^


DROP TRIGGER T_AD_CO_BENEFITS_9
^


DROP TRIGGER T_AU_CO_BENEFITS_9
^


ALTER TABLE CO_BENEFITS
DROP TAXABLE_FRINGE
^

CREATE INDEX LK_CO_PR_BATCH_DEFLT_ED ON CO_PR_BATCH_DEFLT_ED (CO_NBR,CO_E_D_CODES_NBR)
^


DROP TRIGGER T_AD_EE_9
^


DROP TRIGGER T_AIU_EE_3
^


DROP TRIGGER T_AU_EE_9
^


ALTER TABLE EE
DROP WORKERS_COMP_MIN_WAGE_LIMIT
^

ALTER TABLE EE
DROP ESS_TERMS_OF_USE_ACCEPT_DATE
^

CREATE INDEX LK_EE_HR_PERFORMANCE_RATINGS ON EE_HR_PERFORMANCE_RATINGS (EE_NBR,CO_HR_PERFORMANCE_RATINGS_NBR,REVIEW_DATE)
^


DROP TRIGGER T_AD_EE_STATES_9
^

DROP TRIGGER T_AU_EE_STATES_9
^

DROP TRIGGER T_AIU_EE_STATES_3
^

ALTER TABLE EE_STATES
ADD TEMP_STATE_MARITAL_STATUS EV_CHAR2 NOT NULL
^


UPDATE EE_STATES
SET TEMP_STATE_MARITAL_STATUS = STATE_MARITAL_STATUS
^


ALTER TABLE EE_STATES
DROP STATE_MARITAL_STATUS
^

ALTER TABLE EE_STATES
ALTER COLUMN TEMP_STATE_MARITAL_STATUS TO STATE_MARITAL_STATUS
^

ALTER TABLE EE_STATES
ALTER COLUMN STATE_MARITAL_STATUS POSITION 26
^


DROP TRIGGER T_BIUD_PR_BATCH_2
^


DROP TRIGGER T_BIUD_PR_CHECK_2
^


DROP TRIGGER T_BIUD_PR_CHECK_LINES_2
^


DROP TRIGGER T_BIUD_PR_CHECK_LINE_LOCALS_2
^


DROP TRIGGER T_BIUD_PR_CHECK_LOCALS_2
^


DROP TRIGGER T_BIUD_PR_CHECK_STATES_2
^


DROP TRIGGER T_BIUD_PR_CHECK_SUI_2
^


DROP TRIGGER T_BIUD_PR_MISCELLANEOUS_CHECK_2
^

CREATE OR ALTER PROCEDURE pack_co(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE co_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE p_name VARCHAR(40);
DECLARE VARIABLE dba VARCHAR(40);
DECLARE VARIABLE p_dba VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE p_address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE p_address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE p_city VARCHAR(20);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE p_state CHAR(2);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE p_zip_code VARCHAR(10);
DECLARE VARIABLE legal_name VARCHAR(40);
DECLARE VARIABLE p_legal_name VARCHAR(40);
DECLARE VARIABLE legal_address1 VARCHAR(30);
DECLARE VARIABLE p_legal_address1 VARCHAR(30);
DECLARE VARIABLE legal_address2 VARCHAR(30);
DECLARE VARIABLE p_legal_address2 VARCHAR(30);
DECLARE VARIABLE legal_city VARCHAR(20);
DECLARE VARIABLE p_legal_city VARCHAR(20);
DECLARE VARIABLE legal_state CHAR(2);
DECLARE VARIABLE p_legal_state CHAR(2);
DECLARE VARIABLE legal_zip_code VARCHAR(10);
DECLARE VARIABLE p_legal_zip_code VARCHAR(10);
DECLARE VARIABLE termination_date DATE;
DECLARE VARIABLE p_termination_date DATE;
DECLARE VARIABLE termination_code CHAR(1);
DECLARE VARIABLE p_termination_code CHAR(1);
DECLARE VARIABLE print_cpa CHAR(1);
DECLARE VARIABLE p_print_cpa CHAR(1);
DECLARE VARIABLE fein VARCHAR(9);
DECLARE VARIABLE p_fein VARCHAR(9);
DECLARE VARIABLE business_type CHAR(1);
DECLARE VARIABLE p_business_type CHAR(1);
DECLARE VARIABLE use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE p_use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE federal_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_federal_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE federal_tax_exempt_status CHAR(1);
DECLARE VARIABLE p_federal_tax_exempt_status CHAR(1);
DECLARE VARIABLE fed_tax_exempt_ee_oasdi CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_ee_oasdi CHAR(1);
DECLARE VARIABLE fed_tax_exempt_er_oasdi CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_er_oasdi CHAR(1);
DECLARE VARIABLE fed_tax_exempt_ee_medicare CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_ee_medicare CHAR(1);
DECLARE VARIABLE fed_tax_exempt_er_medicare CHAR(1);
DECLARE VARIABLE p_fed_tax_exempt_er_medicare CHAR(1);
DECLARE VARIABLE fui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_fui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE fui_tax_exempt CHAR(1);
DECLARE VARIABLE p_fui_tax_exempt CHAR(1);
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE p_co_workers_comp_nbr INTEGER;
DECLARE VARIABLE apply_misc_limit_to_1099 CHAR(1);
DECLARE VARIABLE p_apply_misc_limit_to_1099 CHAR(1);
DECLARE VARIABLE make_up_tax_deduct_shortfalls CHAR(1);
DECLARE VARIABLE p_make_up_tax_deduct_shortfalls CHAR(1);
DECLARE VARIABLE fed_945_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE p_fed_945_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE fui_rate_override NUMERIC(18,6);
DECLARE VARIABLE p_fui_rate_override NUMERIC(18,6);
DECLARE VARIABLE name_on_invoice CHAR(1);
DECLARE VARIABLE p_name_on_invoice CHAR(1);
DECLARE VARIABLE employer_type CHAR(1);
DECLARE VARIABLE p_employer_type CHAR(1);
DECLARE VARIABLE co_locations_nbr INTEGER;
DECLARE VARIABLE p_co_locations_nbr INTEGER;
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, co_nbr , name, dba, address1, address2, city, state, zip_code, legal_name, legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, termination_date, termination_code, print_cpa, fein, business_type, use_dba_on_tax_return, federal_tax_deposit_frequency, federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi, fed_tax_exempt_ee_medicare, fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt, co_workers_comp_nbr, apply_misc_limit_to_1099, make_up_tax_deduct_shortfalls, fed_945_tax_deposit_frequency, fui_rate_override, name_on_invoice, employer_type, co_locations_nbr
        FROM co
        ORDER BY co_nbr, effective_date
        INTO :rec_version, :effective_date, :co_nbr, :name, :dba, :address1, :address2, :city, :state, :zip_code, :legal_name, :legal_address1, :legal_address2, :legal_city, :legal_state, :legal_zip_code, :termination_date, :termination_code, :print_cpa, :fein, :business_type, :use_dba_on_tax_return, :federal_tax_deposit_frequency, :federal_tax_exempt_status, :fed_tax_exempt_ee_oasdi, :fed_tax_exempt_er_oasdi, :fed_tax_exempt_ee_medicare, :fed_tax_exempt_er_medicare, :fui_tax_deposit_frequency, :fui_tax_exempt, :co_workers_comp_nbr, :apply_misc_limit_to_1099, :make_up_tax_deduct_shortfalls, :fed_945_tax_deposit_frequency, :fui_rate_override, :name_on_invoice, :employer_type, :co_locations_nbr
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM co_nbr) OR (effective_date = '1/1/1900') OR (p_name IS DISTINCT FROM name) OR (p_dba IS DISTINCT FROM dba) OR (p_address1 IS DISTINCT FROM address1) OR (p_address2 IS DISTINCT FROM address2) OR (p_city IS DISTINCT FROM city) OR (p_state IS DISTINCT FROM state) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_legal_name IS DISTINCT FROM legal_name) OR (p_legal_address1 IS DISTINCT FROM legal_address1) OR (p_legal_address2 IS DISTINCT FROM legal_address2) OR (p_legal_city IS DISTINCT FROM legal_city) OR (p_legal_state IS DISTINCT FROM legal_state) OR (p_legal_zip_code IS DISTINCT FROM legal_zip_code) OR (p_termination_date IS DISTINCT FROM termination_date) OR (p_termination_code IS DISTINCT FROM termination_code) OR (p_print_cpa IS DISTINCT FROM print_cpa) OR (p_fein IS DISTINCT FROM fein) OR (p_business_type IS DISTINCT FROM business_type) OR (p_use_dba_on_tax_return IS DISTINCT FROM use_dba_on_tax_return) OR (p_federal_tax_deposit_frequency IS DISTINCT FROM federal_tax_deposit_frequency) OR (p_federal_tax_exempt_status IS DISTINCT FROM federal_tax_exempt_status) OR (p_fed_tax_exempt_ee_oasdi IS DISTINCT FROM fed_tax_exempt_ee_oasdi) OR (p_fed_tax_exempt_er_oasdi IS DISTINCT FROM fed_tax_exempt_er_oasdi) OR (p_fed_tax_exempt_ee_medicare IS DISTINCT FROM fed_tax_exempt_ee_medicare) OR (p_fed_tax_exempt_er_medicare IS DISTINCT FROM fed_tax_exempt_er_medicare) OR (p_fui_tax_deposit_frequency IS DISTINCT FROM fui_tax_deposit_frequency) OR (p_fui_tax_exempt IS DISTINCT FROM fui_tax_exempt) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_apply_misc_limit_to_1099 IS DISTINCT FROM apply_misc_limit_to_1099) OR (p_make_up_tax_deduct_shortfalls IS DISTINCT FROM make_up_tax_deduct_shortfalls) OR (p_fed_945_tax_deposit_frequency IS DISTINCT FROM fed_945_tax_deposit_frequency) OR (p_fui_rate_override IS DISTINCT FROM fui_rate_override) OR (p_name_on_invoice IS DISTINCT FROM name_on_invoice) OR (p_employer_type IS DISTINCT FROM employer_type) OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        curr_nbr = co_nbr;
        p_name = name;
        p_dba = dba;
        p_address1 = address1;
        p_address2 = address2;
        p_city = city;
        p_state = state;
        p_zip_code = zip_code;
        p_legal_name = legal_name;
        p_legal_address1 = legal_address1;
        p_legal_address2 = legal_address2;
        p_legal_city = legal_city;
        p_legal_state = legal_state;
        p_legal_zip_code = legal_zip_code;
        p_termination_date = termination_date;
        p_termination_code = termination_code;
        p_print_cpa = print_cpa;
        p_fein = fein;
        p_business_type = business_type;
        p_use_dba_on_tax_return = use_dba_on_tax_return;
        p_federal_tax_deposit_frequency = federal_tax_deposit_frequency;
        p_federal_tax_exempt_status = federal_tax_exempt_status;
        p_fed_tax_exempt_ee_oasdi = fed_tax_exempt_ee_oasdi;
        p_fed_tax_exempt_er_oasdi = fed_tax_exempt_er_oasdi;
        p_fed_tax_exempt_ee_medicare = fed_tax_exempt_ee_medicare;
        p_fed_tax_exempt_er_medicare = fed_tax_exempt_er_medicare;
        p_fui_tax_deposit_frequency = fui_tax_deposit_frequency;
        p_fui_tax_exempt = fui_tax_exempt;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_apply_misc_limit_to_1099 = apply_misc_limit_to_1099;
        p_make_up_tax_deduct_shortfalls = make_up_tax_deduct_shortfalls;
        p_fed_945_tax_deposit_frequency = fed_945_tax_deposit_frequency;
        p_fui_rate_override = fui_rate_override;
        p_name_on_invoice = name_on_invoice;
        p_employer_type = employer_type;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, name, dba, address1, address2, city, state, zip_code, legal_name, legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, termination_date, termination_code, print_cpa, fein, business_type, use_dba_on_tax_return, federal_tax_deposit_frequency, federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi, fed_tax_exempt_ee_medicare, fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt, co_workers_comp_nbr, apply_misc_limit_to_1099, make_up_tax_deduct_shortfalls, fed_945_tax_deposit_frequency, fui_rate_override, name_on_invoice, employer_type, co_locations_nbr
        FROM co
        WHERE co_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :name, :dba, :address1, :address2, :city, :state, :zip_code, :legal_name, :legal_address1, :legal_address2, :legal_city, :legal_state, :legal_zip_code, :termination_date, :termination_code, :print_cpa, :fein, :business_type, :use_dba_on_tax_return, :federal_tax_deposit_frequency, :federal_tax_exempt_status, :fed_tax_exempt_ee_oasdi, :fed_tax_exempt_er_oasdi, :fed_tax_exempt_ee_medicare, :fed_tax_exempt_er_medicare, :fui_tax_deposit_frequency, :fui_tax_exempt, :co_workers_comp_nbr, :apply_misc_limit_to_1099, :make_up_tax_deduct_shortfalls, :fed_945_tax_deposit_frequency, :fui_rate_override, :name_on_invoice, :employer_type, :co_locations_nbr
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_name IS DISTINCT FROM name) OR (p_dba IS DISTINCT FROM dba) OR (p_address1 IS DISTINCT FROM address1) OR (p_address2 IS DISTINCT FROM address2) OR (p_city IS DISTINCT FROM city) OR (p_state IS DISTINCT FROM state) OR (p_zip_code IS DISTINCT FROM zip_code) OR (p_legal_name IS DISTINCT FROM legal_name) OR (p_legal_address1 IS DISTINCT FROM legal_address1) OR (p_legal_address2 IS DISTINCT FROM legal_address2) OR (p_legal_city IS DISTINCT FROM legal_city) OR (p_legal_state IS DISTINCT FROM legal_state) OR (p_legal_zip_code IS DISTINCT FROM legal_zip_code) OR (p_termination_date IS DISTINCT FROM termination_date) OR (p_termination_code IS DISTINCT FROM termination_code) OR (p_print_cpa IS DISTINCT FROM print_cpa) OR (p_fein IS DISTINCT FROM fein) OR (p_business_type IS DISTINCT FROM business_type) OR (p_use_dba_on_tax_return IS DISTINCT FROM use_dba_on_tax_return) OR (p_federal_tax_deposit_frequency IS DISTINCT FROM federal_tax_deposit_frequency) OR (p_federal_tax_exempt_status IS DISTINCT FROM federal_tax_exempt_status) OR (p_fed_tax_exempt_ee_oasdi IS DISTINCT FROM fed_tax_exempt_ee_oasdi) OR (p_fed_tax_exempt_er_oasdi IS DISTINCT FROM fed_tax_exempt_er_oasdi) OR (p_fed_tax_exempt_ee_medicare IS DISTINCT FROM fed_tax_exempt_ee_medicare) OR (p_fed_tax_exempt_er_medicare IS DISTINCT FROM fed_tax_exempt_er_medicare) OR (p_fui_tax_deposit_frequency IS DISTINCT FROM fui_tax_deposit_frequency) OR (p_fui_tax_exempt IS DISTINCT FROM fui_tax_exempt) OR (p_co_workers_comp_nbr IS DISTINCT FROM co_workers_comp_nbr) OR (p_apply_misc_limit_to_1099 IS DISTINCT FROM apply_misc_limit_to_1099) OR (p_make_up_tax_deduct_shortfalls IS DISTINCT FROM make_up_tax_deduct_shortfalls) OR (p_fed_945_tax_deposit_frequency IS DISTINCT FROM fed_945_tax_deposit_frequency) OR (p_fui_rate_override IS DISTINCT FROM fui_rate_override) OR (p_name_on_invoice IS DISTINCT FROM name_on_invoice) OR (p_employer_type IS DISTINCT FROM employer_type) OR (p_co_locations_nbr IS DISTINCT FROM co_locations_nbr)) THEN
      BEGIN
        p_name = name;
        p_dba = dba;
        p_address1 = address1;
        p_address2 = address2;
        p_city = city;
        p_state = state;
        p_zip_code = zip_code;
        p_legal_name = legal_name;
        p_legal_address1 = legal_address1;
        p_legal_address2 = legal_address2;
        p_legal_city = legal_city;
        p_legal_state = legal_state;
        p_legal_zip_code = legal_zip_code;
        p_termination_date = termination_date;
        p_termination_code = termination_code;
        p_print_cpa = print_cpa;
        p_fein = fein;
        p_business_type = business_type;
        p_use_dba_on_tax_return = use_dba_on_tax_return;
        p_federal_tax_deposit_frequency = federal_tax_deposit_frequency;
        p_federal_tax_exempt_status = federal_tax_exempt_status;
        p_fed_tax_exempt_ee_oasdi = fed_tax_exempt_ee_oasdi;
        p_fed_tax_exempt_er_oasdi = fed_tax_exempt_er_oasdi;
        p_fed_tax_exempt_ee_medicare = fed_tax_exempt_ee_medicare;
        p_fed_tax_exempt_er_medicare = fed_tax_exempt_er_medicare;
        p_fui_tax_deposit_frequency = fui_tax_deposit_frequency;
        p_fui_tax_exempt = fui_tax_exempt;
        p_co_workers_comp_nbr = co_workers_comp_nbr;
        p_apply_misc_limit_to_1099 = apply_misc_limit_to_1099;
        p_make_up_tax_deduct_shortfalls = make_up_tax_deduct_shortfalls;
        p_fed_945_tax_deposit_frequency = fed_945_tax_deposit_frequency;
        p_fui_rate_override = fui_rate_override;
        p_name_on_invoice = name_on_invoice;
        p_employer_type = employer_type;
        p_co_locations_nbr = co_locations_nbr;
      END
      ELSE
        DELETE FROM co WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_CO TO EUSER
^

CREATE TRIGGER T_AD_CO_SUI_9 FOR CO_SUI After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE co_states_nbr INTEGER;
DECLARE VARIABLE tax_return_code VARCHAR(20);
DECLARE VARIABLE final_tax_return CHAR(1);
DECLARE VARIABLE rate NUMERIC(18,8);
DECLARE VARIABLE last_tax_return CHAR(1);
DECLARE VARIABLE sui_reimburser CHAR(1);
DECLARE VARIABLE alternate_wage CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(101, OLD.rec_version, OLD.co_sui_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', co_states_nbr, tax_return_code, final_tax_return, rate, last_tax_return, sui_reimburser, alternate_wage  FROM co_sui WHERE co_sui_nbr = OLD.co_sui_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :co_states_nbr, :tax_return_code, :final_tax_return, :rate, :last_tax_return, :sui_reimburser, :alternate_wage;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1586, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2756, OLD.effective_until);

  /* CO_STATES_NBR */
  IF (last_record = 'Y' OR co_states_nbr IS DISTINCT FROM OLD.co_states_nbr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1590, OLD.co_states_nbr);

  /* TAX_RETURN_CODE */
  IF ((last_record = 'Y' AND OLD.tax_return_code IS NOT NULL) OR (last_record = 'N' AND tax_return_code IS DISTINCT FROM OLD.tax_return_code)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1592, OLD.tax_return_code);

  /* FINAL_TAX_RETURN */
  IF (last_record = 'Y' OR final_tax_return IS DISTINCT FROM OLD.final_tax_return) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1599, OLD.final_tax_return);

  /* RATE */
  IF ((last_record = 'Y' AND OLD.rate IS NOT NULL) OR (last_record = 'N' AND rate IS DISTINCT FROM OLD.rate)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1584, OLD.rate);

  /* LAST_TAX_RETURN */
  IF (last_record = 'Y' OR last_tax_return IS DISTINCT FROM OLD.last_tax_return) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1602, OLD.last_tax_return);

  /* SUI_REIMBURSER */
  IF (last_record = 'Y' OR sui_reimburser IS DISTINCT FROM OLD.sui_reimburser) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1597, OLD.sui_reimburser);

  /* ALTERNATE_WAGE */
  IF (last_record = 'Y' OR alternate_wage IS DISTINCT FROM OLD.alternate_wage) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3187, OLD.alternate_wage);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CO_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1589, OLD.co_nbr);

    /* SY_SUI_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1591, OLD.sy_sui_nbr);

    /* DESCRIPTION */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1593, OLD.description);

    /* PAYMENT_METHOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1600, OLD.payment_method);

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1594, OLD.filler);

    /* GL_TAG */
    IF (OLD.gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1595, OLD.gl_tag);

    /* SUI_ACTIVE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1598, OLD.sui_active);

    /* GL_OFFSET_TAG */
    IF (OLD.gl_offset_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1596, OLD.gl_offset_tag);

    /* APPLIED_FOR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1601, OLD.applied_for);

  END
END

^

CREATE TRIGGER T_AIU_CO_SUI_3 FOR CO_SUI After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.sy_sui_nbr IS DISTINCT FROM NEW.sy_sui_nbr) OR (OLD.description IS DISTINCT FROM NEW.description) OR (OLD.payment_method IS DISTINCT FROM NEW.payment_method) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.gl_tag IS DISTINCT FROM NEW.gl_tag) OR (OLD.sui_active IS DISTINCT FROM NEW.sui_active) OR (OLD.gl_offset_tag IS DISTINCT FROM NEW.gl_offset_tag) OR (OLD.applied_for IS DISTINCT FROM NEW.applied_for)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE co_sui SET 
      co_nbr = NEW.co_nbr, sy_sui_nbr = NEW.sy_sui_nbr, description = NEW.description, payment_method = NEW.payment_method, filler = NEW.filler, gl_tag = NEW.gl_tag, sui_active = NEW.sui_active, gl_offset_tag = NEW.gl_offset_tag, applied_for = NEW.applied_for
      WHERE co_sui_nbr = NEW.co_sui_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_CO_SUI_9 FOR CO_SUI After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(101, NEW.rec_version, NEW.co_sui_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1586, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2756, OLD.effective_until);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1589, OLD.co_nbr);
    changes = changes + 1;
  END

  /* CO_STATES_NBR */
  IF (OLD.co_states_nbr IS DISTINCT FROM NEW.co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1590, OLD.co_states_nbr);
    changes = changes + 1;
  END

  /* SY_SUI_NBR */
  IF (OLD.sy_sui_nbr IS DISTINCT FROM NEW.sy_sui_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1591, OLD.sy_sui_nbr);
    changes = changes + 1;
  END

  /* TAX_RETURN_CODE */
  IF (OLD.tax_return_code IS DISTINCT FROM NEW.tax_return_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1592, OLD.tax_return_code);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1593, OLD.description);
    changes = changes + 1;
  END

  /* PAYMENT_METHOD */
  IF (OLD.payment_method IS DISTINCT FROM NEW.payment_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1600, OLD.payment_method);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1594, OLD.filler);
    changes = changes + 1;
  END

  /* FINAL_TAX_RETURN */
  IF (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1599, OLD.final_tax_return);
    changes = changes + 1;
  END

  /* GL_TAG */
  IF (OLD.gl_tag IS DISTINCT FROM NEW.gl_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1595, OLD.gl_tag);
    changes = changes + 1;
  END

  /* SUI_ACTIVE */
  IF (OLD.sui_active IS DISTINCT FROM NEW.sui_active) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1598, OLD.sui_active);
    changes = changes + 1;
  END

  /* RATE */
  IF (OLD.rate IS DISTINCT FROM NEW.rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1584, OLD.rate);
    changes = changes + 1;
  END

  /* GL_OFFSET_TAG */
  IF (OLD.gl_offset_tag IS DISTINCT FROM NEW.gl_offset_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1596, OLD.gl_offset_tag);
    changes = changes + 1;
  END

  /* APPLIED_FOR */
  IF (OLD.applied_for IS DISTINCT FROM NEW.applied_for) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1601, OLD.applied_for);
    changes = changes + 1;
  END

  /* LAST_TAX_RETURN */
  IF (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1602, OLD.last_tax_return);
    changes = changes + 1;
  END

  /* SUI_REIMBURSER */
  IF (OLD.sui_reimburser IS DISTINCT FROM NEW.sui_reimburser) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1597, OLD.sui_reimburser);
    changes = changes + 1;
  END

  /* ALTERNATE_WAGE */
  IF (OLD.alternate_wage IS DISTINCT FROM NEW.alternate_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3187, OLD.alternate_wage);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_CO_8 FOR CO After Delete POSITION 8
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE name VARCHAR(40);
DECLARE VARIABLE dba VARCHAR(40);
DECLARE VARIABLE address1 VARCHAR(30);
DECLARE VARIABLE address2 VARCHAR(30);
DECLARE VARIABLE city VARCHAR(20);
DECLARE VARIABLE state CHAR(2);
DECLARE VARIABLE zip_code VARCHAR(10);
DECLARE VARIABLE legal_name VARCHAR(40);
DECLARE VARIABLE legal_address1 VARCHAR(30);
DECLARE VARIABLE legal_address2 VARCHAR(30);
DECLARE VARIABLE legal_city VARCHAR(20);
DECLARE VARIABLE legal_state CHAR(2);
DECLARE VARIABLE legal_zip_code VARCHAR(10);
DECLARE VARIABLE termination_date DATE;
DECLARE VARIABLE termination_code CHAR(1);
DECLARE VARIABLE print_cpa CHAR(1);
DECLARE VARIABLE fein VARCHAR(9);
DECLARE VARIABLE business_type CHAR(1);
DECLARE VARIABLE use_dba_on_tax_return CHAR(1);
DECLARE VARIABLE federal_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE federal_tax_exempt_status CHAR(1);
DECLARE VARIABLE fed_tax_exempt_ee_oasdi CHAR(1);
DECLARE VARIABLE fed_tax_exempt_er_oasdi CHAR(1);
DECLARE VARIABLE fed_tax_exempt_ee_medicare CHAR(1);
DECLARE VARIABLE fed_tax_exempt_er_medicare CHAR(1);
DECLARE VARIABLE fui_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE fui_tax_exempt CHAR(1);
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE apply_misc_limit_to_1099 CHAR(1);
DECLARE VARIABLE make_up_tax_deduct_shortfalls CHAR(1);
DECLARE VARIABLE fed_945_tax_deposit_frequency CHAR(1);
DECLARE VARIABLE fui_rate_override NUMERIC(18,6);
DECLARE VARIABLE name_on_invoice CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(37, OLD.rec_version, OLD.co_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', name, dba, address1, address2, city, state, zip_code, legal_name, legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, termination_date, termination_code, print_cpa, fein, business_type, use_dba_on_tax_return, federal_tax_deposit_frequency, federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi, fed_tax_exempt_ee_medicare, fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt, co_workers_comp_nbr, apply_misc_limit_to_1099, make_up_tax_deduct_shortfalls, fed_945_tax_deposit_frequency, fui_rate_override, name_on_invoice  FROM co WHERE co_nbr = OLD.co_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :name, :dba, :address1, :address2, :city, :state, :zip_code, :legal_name, :legal_address1, :legal_address2, :legal_city, :legal_state, :legal_zip_code, :termination_date, :termination_code, :print_cpa, :fein, :business_type, :use_dba_on_tax_return, :federal_tax_deposit_frequency, :federal_tax_exempt_status, :fed_tax_exempt_ee_oasdi, :fed_tax_exempt_er_oasdi, :fed_tax_exempt_ee_medicare, :fed_tax_exempt_er_medicare, :fui_tax_deposit_frequency, :fui_tax_exempt, :co_workers_comp_nbr, :apply_misc_limit_to_1099, :make_up_tax_deduct_shortfalls, :fed_945_tax_deposit_frequency, :fui_rate_override, :name_on_invoice;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 492, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2628, OLD.effective_until);

  /* NAME */
  IF (last_record = 'Y' OR name IS DISTINCT FROM OLD.name) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 607, OLD.name);

  /* DBA */
  IF ((last_record = 'Y' AND OLD.dba IS NOT NULL) OR (last_record = 'N' AND dba IS DISTINCT FROM OLD.dba)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 608, OLD.dba);

  /* ADDRESS1 */
  IF (last_record = 'Y' OR address1 IS DISTINCT FROM OLD.address1) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 493, OLD.address1);

  /* ADDRESS2 */
  IF ((last_record = 'Y' AND OLD.address2 IS NOT NULL) OR (last_record = 'N' AND address2 IS DISTINCT FROM OLD.address2)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 494, OLD.address2);

  /* CITY */
  IF (last_record = 'Y' OR city IS DISTINCT FROM OLD.city) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 609, OLD.city);

  /* STATE */
  IF (last_record = 'Y' OR state IS DISTINCT FROM OLD.state) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 610, OLD.state);

  /* ZIP_CODE */
  IF (last_record = 'Y' OR zip_code IS DISTINCT FROM OLD.zip_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 524, OLD.zip_code);

  /* LEGAL_NAME */
  IF ((last_record = 'Y' AND OLD.legal_name IS NOT NULL) OR (last_record = 'N' AND legal_name IS DISTINCT FROM OLD.legal_name)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 611, OLD.legal_name);

  /* LEGAL_ADDRESS1 */
  IF ((last_record = 'Y' AND OLD.legal_address1 IS NOT NULL) OR (last_record = 'N' AND legal_address1 IS DISTINCT FROM OLD.legal_address1)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 495, OLD.legal_address1);

  /* LEGAL_ADDRESS2 */
  IF ((last_record = 'Y' AND OLD.legal_address2 IS NOT NULL) OR (last_record = 'N' AND legal_address2 IS DISTINCT FROM OLD.legal_address2)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 496, OLD.legal_address2);

  /* LEGAL_CITY */
  IF ((last_record = 'Y' AND OLD.legal_city IS NOT NULL) OR (last_record = 'N' AND legal_city IS DISTINCT FROM OLD.legal_city)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 612, OLD.legal_city);

  /* LEGAL_STATE */
  IF ((last_record = 'Y' AND OLD.legal_state IS NOT NULL) OR (last_record = 'N' AND legal_state IS DISTINCT FROM OLD.legal_state)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 613, OLD.legal_state);

  /* LEGAL_ZIP_CODE */
  IF ((last_record = 'Y' AND OLD.legal_zip_code IS NOT NULL) OR (last_record = 'N' AND legal_zip_code IS DISTINCT FROM OLD.legal_zip_code)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 526, OLD.legal_zip_code);

  /* TERMINATION_DATE */
  IF ((last_record = 'Y' AND OLD.termination_date IS NOT NULL) OR (last_record = 'N' AND termination_date IS DISTINCT FROM OLD.termination_date)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 578, OLD.termination_date);

  /* TERMINATION_CODE */
  IF (last_record = 'Y' OR termination_code IS DISTINCT FROM OLD.termination_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 675, OLD.termination_code);

  /* PRINT_CPA */
  IF (last_record = 'Y' OR print_cpa IS DISTINCT FROM OLD.print_cpa) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 676, OLD.print_cpa);

  /* FEIN */
  IF (last_record = 'Y' OR fein IS DISTINCT FROM OLD.fein) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 677, OLD.fein);

  /* BUSINESS_TYPE */
  IF (last_record = 'Y' OR business_type IS DISTINCT FROM OLD.business_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 679, OLD.business_type);

  /* USE_DBA_ON_TAX_RETURN */
  IF (last_record = 'Y' OR use_dba_on_tax_return IS DISTINCT FROM OLD.use_dba_on_tax_return) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 690, OLD.use_dba_on_tax_return);

  /* FEDERAL_TAX_DEPOSIT_FREQUENCY */
  IF (last_record = 'Y' OR federal_tax_deposit_frequency IS DISTINCT FROM OLD.federal_tax_deposit_frequency) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 694, OLD.federal_tax_deposit_frequency);

  /* FEDERAL_TAX_EXEMPT_STATUS */
  IF (last_record = 'Y' OR federal_tax_exempt_status IS DISTINCT FROM OLD.federal_tax_exempt_status) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 700, OLD.federal_tax_exempt_status);

  /* FED_TAX_EXEMPT_EE_OASDI */
  IF (last_record = 'Y' OR fed_tax_exempt_ee_oasdi IS DISTINCT FROM OLD.fed_tax_exempt_ee_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 701, OLD.fed_tax_exempt_ee_oasdi);

  /* FED_TAX_EXEMPT_ER_OASDI */
  IF (last_record = 'Y' OR fed_tax_exempt_er_oasdi IS DISTINCT FROM OLD.fed_tax_exempt_er_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 702, OLD.fed_tax_exempt_er_oasdi);

  /* FED_TAX_EXEMPT_EE_MEDICARE */
  IF (last_record = 'Y' OR fed_tax_exempt_ee_medicare IS DISTINCT FROM OLD.fed_tax_exempt_ee_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 703, OLD.fed_tax_exempt_ee_medicare);

  /* FED_TAX_EXEMPT_ER_MEDICARE */
  IF (last_record = 'Y' OR fed_tax_exempt_er_medicare IS DISTINCT FROM OLD.fed_tax_exempt_er_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 704, OLD.fed_tax_exempt_er_medicare);

  /* FUI_TAX_DEPOSIT_FREQUENCY */
  IF (last_record = 'Y' OR fui_tax_deposit_frequency IS DISTINCT FROM OLD.fui_tax_deposit_frequency) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 705, OLD.fui_tax_deposit_frequency);

  /* FUI_TAX_EXEMPT */
  IF (last_record = 'Y' OR fui_tax_exempt IS DISTINCT FROM OLD.fui_tax_exempt) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 706, OLD.fui_tax_exempt);

  /* CO_WORKERS_COMP_NBR */
  IF ((last_record = 'Y' AND OLD.co_workers_comp_nbr IS NOT NULL) OR (last_record = 'N' AND co_workers_comp_nbr IS DISTINCT FROM OLD.co_workers_comp_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 566, OLD.co_workers_comp_nbr);

  /* APPLY_MISC_LIMIT_TO_1099 */
  IF (last_record = 'Y' OR apply_misc_limit_to_1099 IS DISTINCT FROM OLD.apply_misc_limit_to_1099) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 722, OLD.apply_misc_limit_to_1099);

  /* MAKE_UP_TAX_DEDUCT_SHORTFALLS */
  IF (last_record = 'Y' OR make_up_tax_deduct_shortfalls IS DISTINCT FROM OLD.make_up_tax_deduct_shortfalls) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 724, OLD.make_up_tax_deduct_shortfalls);

  /* FED_945_TAX_DEPOSIT_FREQUENCY */
  IF (last_record = 'Y' OR fed_945_tax_deposit_frequency IS DISTINCT FROM OLD.fed_945_tax_deposit_frequency) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 671, OLD.fed_945_tax_deposit_frequency);

  /* FUI_RATE_OVERRIDE */
  IF ((last_record = 'Y' AND OLD.fui_rate_override IS NOT NULL) OR (last_record = 'N' AND fui_rate_override IS DISTINCT FROM OLD.fui_rate_override)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 490, OLD.fui_rate_override);

  /* NAME_ON_INVOICE */
  IF (last_record = 'Y' OR name_on_invoice IS DISTINCT FROM OLD.name_on_invoice) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 754, OLD.name_on_invoice);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CUSTOM_COMPANY_NUMBER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 606, OLD.custom_company_number);

    /* COUNTY */
    IF (OLD.county IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 525, OLD.county);

    /* E_MAIL_ADDRESS */
    IF (OLD.e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 614, OLD.e_mail_address);

    /* REFERRED_BY */
    IF (OLD.referred_by IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 615, OLD.referred_by);

    /* SB_REFERRALS_NBR */
    IF (OLD.sb_referrals_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 527, OLD.sb_referrals_nbr);

    /* START_DATE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 577, OLD.start_date);

    /* TERMINATION_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@TERMINATION_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 497, :blob_nbr);
    END

    /* SB_ACCOUNTANT_NBR */
    IF (OLD.sb_accountant_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 528, OLD.sb_accountant_nbr);

    /* ACCOUNTANT_CONTACT */
    IF (OLD.accountant_contact IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 616, OLD.accountant_contact);

    /* UNION_CL_E_DS_NBR */
    IF (OLD.union_cl_e_ds_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 529, OLD.union_cl_e_ds_nbr);

    /* CL_COMMON_PAYMASTER_NBR */
    IF (OLD.cl_common_paymaster_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 530, OLD.cl_common_paymaster_nbr);

    /* CL_CO_CONSOLIDATION_NBR */
    IF (OLD.cl_co_consolidation_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 531, OLD.cl_co_consolidation_nbr);

    /* REMOTE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 678, OLD.remote);

    /* BUSINESS_START_DATE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 579, OLD.business_start_date);

    /* CORPORATION_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 680, OLD.corporation_type);

    /* NATURE_OF_BUSINESS */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 498, :blob_nbr);
    END

    /* RESTAURANT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 681, OLD.restaurant);

    /* DAYS_OPEN */
    IF (OLD.days_open IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 532, OLD.days_open);

    /* TIME_OPEN */
    IF (OLD.time_open IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 499, OLD.time_open);

    /* TIME_CLOSE */
    IF (OLD.time_close IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 500, OLD.time_close);

    /* COMPANY_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@COMPANY_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 501, :blob_nbr);
    END

    /* SUCCESSOR_COMPANY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 682, OLD.successor_company);

    /* MEDICAL_PLAN */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 683, OLD.medical_plan);

    /* RETIREMENT_AGE */
    IF (OLD.retirement_age IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 534, OLD.retirement_age);

    /* PAY_FREQUENCIES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 684, OLD.pay_frequencies);

    /* GENERAL_LEDGER_FORMAT_STRING */
    IF (OLD.general_ledger_format_string IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 502, OLD.general_ledger_format_string);

    /* CL_BILLING_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 535, OLD.cl_billing_nbr);

    /* DBDT_LEVEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 685, OLD.dbdt_level);

    /* BILLING_LEVEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 686, OLD.billing_level);

    /* BANK_ACCOUNT_LEVEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 687, OLD.bank_account_level);

    /* BILLING_SB_BANK_ACCOUNT_NBR */
    IF (OLD.billing_sb_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 536, OLD.billing_sb_bank_account_nbr);

    /* TAX_SB_BANK_ACCOUNT_NBR */
    IF (OLD.tax_sb_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 537, OLD.tax_sb_bank_account_nbr);

    /* PAYROLL_CL_BANK_ACCOUNT_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 538, OLD.payroll_cl_bank_account_nbr);

    /* DEBIT_NUMBER_DAYS_PRIOR_PR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 539, OLD.debit_number_days_prior_pr);

    /* TAX_CL_BANK_ACCOUNT_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 540, OLD.tax_cl_bank_account_nbr);

    /* DEBIT_NUMBER_OF_DAYS_PRIOR_TAX */
    IF (OLD.debit_number_of_days_prior_tax IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 541, OLD.debit_number_of_days_prior_tax);

    /* BILLING_CL_BANK_ACCOUNT_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 542, OLD.billing_cl_bank_account_nbr);

    /* DEBIT_NUMBER_DAYS_PRIOR_BILL */
    IF (OLD.debit_number_days_prior_bill IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 543, OLD.debit_number_days_prior_bill);

    /* DD_CL_BANK_ACCOUNT_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 544, OLD.dd_cl_bank_account_nbr);

    /* DEBIT_NUMBER_OF_DAYS_PRIOR_DD */
    IF (OLD.debit_number_of_days_prior_dd IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 545, OLD.debit_number_of_days_prior_dd);

    /* HOME_CO_STATES_NBR */
    IF (OLD.home_co_states_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 546, OLD.home_co_states_nbr);

    /* HOME_SDI_CO_STATES_NBR */
    IF (OLD.home_sdi_co_states_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 547, OLD.home_sdi_co_states_nbr);

    /* HOME_SUI_CO_STATES_NBR */
    IF (OLD.home_sui_co_states_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 548, OLD.home_sui_co_states_nbr);

    /* TAX_SERVICE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 688, OLD.tax_service);

    /* TAX_SERVICE_START_DATE */
    IF (OLD.tax_service_start_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 580, OLD.tax_service_start_date);

    /* TAX_SERVICE_END_DATE */
    IF (OLD.tax_service_end_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 581, OLD.tax_service_end_date);

    /* WORKERS_COMP_CL_AGENCY_NBR */
    IF (OLD.workers_comp_cl_agency_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 549, OLD.workers_comp_cl_agency_nbr);

    /* WORKERS_COMP_POLICY_ID */
    IF (OLD.workers_comp_policy_id IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 617, OLD.workers_comp_policy_id);

    /* W_COMP_CL_BANK_ACCOUNT_NBR */
    IF (OLD.w_comp_cl_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 550, OLD.w_comp_cl_bank_account_nbr);

    /* DEBIT_NUMBER_DAYS_PRIOR_WC */
    IF (OLD.debit_number_days_prior_wc IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 551, OLD.debit_number_days_prior_wc);

    /* W_COMP_SB_BANK_ACCOUNT_NBR */
    IF (OLD.w_comp_sb_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 552, OLD.w_comp_sb_bank_account_nbr);

    /* EFTPS_ENROLLMENT_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 691, OLD.eftps_enrollment_status);

    /* EFTPS_ENROLLMENT_DATE */
    IF (OLD.eftps_enrollment_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 503, OLD.eftps_enrollment_date);

    /* EFTPS_SEQUENCE_NUMBER */
    IF (OLD.eftps_sequence_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 618, OLD.eftps_sequence_number);

    /* EFTPS_ENROLLMENT_NUMBER */
    IF (OLD.eftps_enrollment_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 619, OLD.eftps_enrollment_number);

    /* EFTPS_NAME */
    IF (OLD.eftps_name IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 620, OLD.eftps_name);

    /* PIN_NUMBER */
    IF (OLD.pin_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 553, OLD.pin_number);

    /* ACH_SB_BANK_ACCOUNT_NBR */
    IF (OLD.ach_sb_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 554, OLD.ach_sb_bank_account_nbr);

    /* TRUST_SERVICE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 692, OLD.trust_service);

    /* TRUST_SB_ACCOUNT_NBR */
    IF (OLD.trust_sb_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 555, OLD.trust_sb_account_nbr);

    /* TRUST_SERVICE_START_DATE */
    IF (OLD.trust_service_start_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 582, OLD.trust_service_start_date);

    /* TRUST_SERVICE_END_DATE */
    IF (OLD.trust_service_end_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 583, OLD.trust_service_end_date);

    /* OBC */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 693, OLD.obc);

    /* SB_OBC_ACCOUNT_NBR */
    IF (OLD.sb_obc_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 556, OLD.sb_obc_account_nbr);

    /* OBC_START_DATE */
    IF (OLD.obc_start_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 584, OLD.obc_start_date);

    /* OBC_END_DATE */
    IF (OLD.obc_end_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 585, OLD.obc_end_date);

    /* SY_FED_REPORTING_AGENCY_NBR */
    IF (OLD.sy_fed_reporting_agency_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 557, OLD.sy_fed_reporting_agency_nbr);

    /* SY_FED_TAX_PAYMENT_AGENCY_NBR */
    IF (OLD.sy_fed_tax_payment_agency_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 558, OLD.sy_fed_tax_payment_agency_nbr);

    /* FEDERAL_TAX_TRANSFER_METHOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 695, OLD.federal_tax_transfer_method);

    /* FEDERAL_TAX_PAYMENT_METHOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 696, OLD.federal_tax_payment_method);

    /* FUI_SY_TAX_PAYMENT_AGENCY */
    IF (OLD.fui_sy_tax_payment_agency IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 559, OLD.fui_sy_tax_payment_agency);

    /* FUI_SY_TAX_REPORT_AGENCY */
    IF (OLD.fui_sy_tax_report_agency IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 560, OLD.fui_sy_tax_report_agency);

    /* EXTERNAL_TAX_EXPORT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 698, OLD.external_tax_export);

    /* NON_PROFIT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 699, OLD.non_profit);

    /* PRIMARY_SORT_FIELD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 707, OLD.primary_sort_field);

    /* SECONDARY_SORT_FIELD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 708, OLD.secondary_sort_field);

    /* FISCAL_YEAR_END */
    IF (OLD.fiscal_year_end IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 561, OLD.fiscal_year_end);

    /* THIRD_PARTY_IN_GROSS_PR_REPORT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 709, OLD.third_party_in_gross_pr_report);

    /* SIC_CODE */
    IF (OLD.sic_code IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 562, OLD.sic_code);

    /* DEDUCTIONS_TO_ZERO */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 711, OLD.deductions_to_zero);

    /* AUTOPAY_COMPANY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 712, OLD.autopay_company);

    /* PAY_FREQUENCY_HOURLY_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 713, OLD.pay_frequency_hourly_default);

    /* PAY_FREQUENCY_SALARY_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 714, OLD.pay_frequency_salary_default);

    /* CO_CHECK_PRIMARY_SORT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 715, OLD.co_check_primary_sort);

    /* CO_CHECK_SECONDARY_SORT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 716, OLD.co_check_secondary_sort);

    /* CL_DELIVERY_GROUP_NBR */
    IF (OLD.cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 563, OLD.cl_delivery_group_nbr);

    /* ANNUAL_CL_DELIVERY_GROUP_NBR */
    IF (OLD.annual_cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 564, OLD.annual_cl_delivery_group_nbr);

    /* QUARTER_CL_DELIVERY_GROUP_NBR */
    IF (OLD.quarter_cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 565, OLD.quarter_cl_delivery_group_nbr);

    /* SMOKER_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 717, OLD.smoker_default);

    /* AUTO_LABOR_DIST_SHOW_DEDUCTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 718, OLD.auto_labor_dist_show_deducts);

    /* AUTO_LABOR_DIST_LEVEL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 719, OLD.auto_labor_dist_level);

    /* DISTRIBUTE_DEDUCTIONS_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 720, OLD.distribute_deductions_default);

    /* WITHHOLDING_DEFAULT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 721, OLD.withholding_default);

    /* CHECK_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 723, OLD.check_type);

    /* SHOW_SHIFTS_ON_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 725, OLD.show_shifts_on_check);

    /* SHOW_YTD_ON_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 726, OLD.show_ytd_on_check);

    /* SHOW_EIN_NUMBER_ON_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 727, OLD.show_ein_number_on_check);

    /* SHOW_SS_NUMBER_ON_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 728, OLD.show_ss_number_on_check);

    /* TIME_OFF_ACCRUAL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 729, OLD.time_off_accrual);

    /* CHECK_FORM */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 730, OLD.check_form);

    /* PRINT_MANUAL_CHECK_STUBS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 731, OLD.print_manual_check_stubs);

    /* CREDIT_HOLD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 732, OLD.credit_hold);

    /* CL_TIMECLOCK_IMPORTS_NBR */
    IF (OLD.cl_timeclock_imports_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 567, OLD.cl_timeclock_imports_nbr);

    /* PAYROLL_PASSWORD */
    IF (OLD.payroll_password IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 568, OLD.payroll_password);

    /* REMOTE_OF_CLIENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 733, OLD.remote_of_client);

    /* HARDWARE_O_S */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 734, OLD.hardware_o_s);

    /* NETWORK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 735, OLD.network);

    /* MODEM_SPEED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 736, OLD.modem_speed);

    /* MODEM_CONNECTION_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 737, OLD.modem_connection_type);

    /* NETWORK_ADMINISTRATOR */
    IF (OLD.network_administrator IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 621, OLD.network_administrator);

    /* NETWORK_ADMINISTRATOR_PHONE */
    IF (OLD.network_administrator_phone IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 622, OLD.network_administrator_phone);

    /* NETWORK_ADMIN_PHONE_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 738, OLD.network_admin_phone_type);

    /* EXTERNAL_NETWORK_ADMINISTRATOR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 739, OLD.external_network_administrator);

    /* TRANSMISSION_DESTINATION */
    IF (OLD.transmission_destination IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 569, OLD.transmission_destination);

    /* LAST_CALL_IN_DATE */
    IF (OLD.last_call_in_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 505, OLD.last_call_in_date);

    /* SETUP_COMPLETED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 741, OLD.setup_completed);

    /* FIRST_MONTHLY_PAYROLL_DAY */
    IF (OLD.first_monthly_payroll_day IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 570, OLD.first_monthly_payroll_day);

    /* SECOND_MONTHLY_PAYROLL_DAY */
    IF (OLD.second_monthly_payroll_day IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 571, OLD.second_monthly_payroll_day);

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 624, OLD.filler);

    /* COLLATE_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 742, OLD.collate_checks);

    /* PROCESS_PRIORITY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 572, OLD.process_priority);

    /* CHECK_MESSAGE */
    IF (OLD.check_message IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 625, OLD.check_message);

    /* CUSTOMER_SERVICE_SB_USER_NBR */
    IF (OLD.customer_service_sb_user_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 573, OLD.customer_service_sb_user_nbr);

    /* INVOICE_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 506, :blob_nbr);
    END

    /* TAX_COVER_LETTER_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 507, :blob_nbr);
    END

    /* FINAL_TAX_RETURN */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 674, OLD.final_tax_return);

    /* GENERAL_LEDGER_TAG */
    IF (OLD.general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 626, OLD.general_ledger_tag);

    /* BILLING_GENERAL_LEDGER_TAG */
    IF (OLD.billing_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 627, OLD.billing_general_ledger_tag);

    /* FEDERAL_GENERAL_LEDGER_TAG */
    IF (OLD.federal_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 628, OLD.federal_general_ledger_tag);

    /* EE_OASDI_GENERAL_LEDGER_TAG */
    IF (OLD.ee_oasdi_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 629, OLD.ee_oasdi_general_ledger_tag);

    /* ER_OASDI_GENERAL_LEDGER_TAG */
    IF (OLD.er_oasdi_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 630, OLD.er_oasdi_general_ledger_tag);

    /* EE_MEDICARE_GENERAL_LEDGER_TAG */
    IF (OLD.ee_medicare_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 631, OLD.ee_medicare_general_ledger_tag);

    /* ER_MEDICARE_GENERAL_LEDGER_TAG */
    IF (OLD.er_medicare_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 632, OLD.er_medicare_general_ledger_tag);

    /* FUI_GENERAL_LEDGER_TAG */
    IF (OLD.fui_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 633, OLD.fui_general_ledger_tag);

    /* EIC_GENERAL_LEDGER_TAG */
    IF (OLD.eic_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 634, OLD.eic_general_ledger_tag);

    /* BACKUP_W_GENERAL_LEDGER_TAG */
    IF (OLD.backup_w_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 635, OLD.backup_w_general_ledger_tag);

    /* NET_PAY_GENERAL_LEDGER_TAG */
    IF (OLD.net_pay_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 636, OLD.net_pay_general_ledger_tag);

    /* TAX_IMP_GENERAL_LEDGER_TAG */
    IF (OLD.tax_imp_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 637, OLD.tax_imp_general_ledger_tag);

    /* TRUST_IMP_GENERAL_LEDGER_TAG */
    IF (OLD.trust_imp_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 638, OLD.trust_imp_general_ledger_tag);

    /* THRD_P_TAX_GENERAL_LEDGER_TAG */
    IF (OLD.thrd_p_tax_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 639, OLD.thrd_p_tax_general_ledger_tag);

    /* THRD_P_CHK_GENERAL_LEDGER_TAG */
    IF (OLD.thrd_p_chk_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 640, OLD.thrd_p_chk_general_ledger_tag);

    /* TRUST_CHK_GENERAL_LEDGER_TAG */
    IF (OLD.trust_chk_general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 641, OLD.trust_chk_general_ledger_tag);

    /* FEDERAL_OFFSET_GL_TAG */
    IF (OLD.federal_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 642, OLD.federal_offset_gl_tag);

    /* EE_OASDI_OFFSET_GL_TAG */
    IF (OLD.ee_oasdi_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 643, OLD.ee_oasdi_offset_gl_tag);

    /* ER_OASDI_OFFSET_GL_TAG */
    IF (OLD.er_oasdi_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 644, OLD.er_oasdi_offset_gl_tag);

    /* EE_MEDICARE_OFFSET_GL_TAG */
    IF (OLD.ee_medicare_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 645, OLD.ee_medicare_offset_gl_tag);

    /* ER_MEDICARE_OFFSET_GL_TAG */
    IF (OLD.er_medicare_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 646, OLD.er_medicare_offset_gl_tag);

    /* FUI_OFFSET_GL_TAG */
    IF (OLD.fui_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 647, OLD.fui_offset_gl_tag);

    /* EIC_OFFSET_GL_TAG */
    IF (OLD.eic_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 648, OLD.eic_offset_gl_tag);

    /* BACKUP_W_OFFSET_GL_TAG */
    IF (OLD.backup_w_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 649, OLD.backup_w_offset_gl_tag);

    /* TRUST_IMP_OFFSET_GL_TAG */
    IF (OLD.trust_imp_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 650, OLD.trust_imp_offset_gl_tag);

    /* BILLING_EXP_GL_TAG */
    IF (OLD.billing_exp_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 651, OLD.billing_exp_gl_tag);

    /* ER_OASDI_EXP_GL_TAG */
    IF (OLD.er_oasdi_exp_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 652, OLD.er_oasdi_exp_gl_tag);

    /* ER_MEDICARE_EXP_GL_TAG */
    IF (OLD.er_medicare_exp_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 653, OLD.er_medicare_exp_gl_tag);

    /* AUTO_ENLIST */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 743, OLD.auto_enlist);

    /* CALCULATE_LOCALS_FIRST */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 744, OLD.calculate_locals_first);

    /* WEEKEND_ACTION */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 747, OLD.weekend_action);

    /* LAST_FUI_CORRECTION */
    IF (OLD.last_fui_correction IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 508, OLD.last_fui_correction);

    /* LAST_SUI_CORRECTION */
    IF (OLD.last_sui_correction IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 509, OLD.last_sui_correction);

    /* LAST_QUARTER_END_CORRECTION */
    IF (OLD.last_quarter_end_correction IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 510, OLD.last_quarter_end_correction);

    /* LAST_PREPROCESS */
    IF (OLD.last_preprocess IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 511, OLD.last_preprocess);

    /* LAST_PREPROCESS_MESSAGE */
    IF (OLD.last_preprocess_message IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 655, OLD.last_preprocess_message);

    /* CHARGE_COBRA_ADMIN_FEE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 748, OLD.charge_cobra_admin_fee);

    /* COBRA_FEE_DAY_OF_MONTH_DUE */
    IF (OLD.cobra_fee_day_of_month_due IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 574, OLD.cobra_fee_day_of_month_due);

    /* COBRA_NOTIFICATION_DAYS */
    IF (OLD.cobra_notification_days IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 575, OLD.cobra_notification_days);

    /* COBRA_ELIGIBILITY_CONFIRM_DAYS */
    IF (OLD.cobra_eligibility_confirm_days IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 576, OLD.cobra_eligibility_confirm_days);

    /* SHOW_RATES_ON_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 749, OLD.show_rates_on_checks);

    /* SHOW_DIR_DEP_NBR_ON_CHECKS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 750, OLD.show_dir_dep_nbr_on_checks);

    /* FED_943_TAX_DEPOSIT_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 672, OLD.fed_943_tax_deposit_frequency);

    /* IMPOUND_WORKERS_COMP */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 670, OLD.impound_workers_comp);

    /* REVERSE_CHECK_PRINTING */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 668, OLD.reverse_check_printing);

    /* LOCK_DATE */
    IF (OLD.lock_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 522, OLD.lock_date);

    /* AUTO_INCREMENT */
    IF (OLD.auto_increment IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 489, OLD.auto_increment);

    /* MAXIMUM_GROUP_TERM_LIFE */
    IF (OLD.maximum_group_term_life IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 488, OLD.maximum_group_term_life);

    /* PAYRATE_PRECISION */
    IF (OLD.payrate_precision IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 487, OLD.payrate_precision);

    /* AVERAGE_HOURS */
    IF (OLD.average_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 486, OLD.average_hours);

    /* MAXIMUM_HOURS_ON_CHECK */
    IF (OLD.maximum_hours_on_check IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 485, OLD.maximum_hours_on_check);

    /* MAXIMUM_DOLLARS_ON_CHECK */
    IF (OLD.maximum_dollars_on_check IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 484, OLD.maximum_dollars_on_check);

    /* DISCOUNT_RATE */
    IF (OLD.discount_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 483, OLD.discount_rate);

    /* MOD_RATE */
    IF (OLD.mod_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 482, OLD.mod_rate);

    /* MINIMUM_TAX_THRESHOLD */
    IF (OLD.minimum_tax_threshold IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 481, OLD.minimum_tax_threshold);

    /* SS_DISABILITY_ADMIN_FEE */
    IF (OLD.ss_disability_admin_fee IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 480, OLD.ss_disability_admin_fee);

    /* CHECK_TIME_OFF_AVAIL */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 751, OLD.check_time_off_avail);

    /* AGENCY_CHECK_MB_GROUP_NBR */
    IF (OLD.agency_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 586, OLD.agency_check_mb_group_nbr);

    /* PR_CHECK_MB_GROUP_NBR */
    IF (OLD.pr_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 587, OLD.pr_check_mb_group_nbr);

    /* PR_REPORT_MB_GROUP_NBR */
    IF (OLD.pr_report_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 588, OLD.pr_report_mb_group_nbr);

    /* PR_REPORT_SECOND_MB_GROUP_NBR */
    IF (OLD.pr_report_second_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 589, OLD.pr_report_second_mb_group_nbr);

    /* TAX_CHECK_MB_GROUP_NBR */
    IF (OLD.tax_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 590, OLD.tax_check_mb_group_nbr);

    /* TAX_EE_RETURN_MB_GROUP_NBR */
    IF (OLD.tax_ee_return_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 591, OLD.tax_ee_return_mb_group_nbr);

    /* TAX_RETURN_MB_GROUP_NBR */
    IF (OLD.tax_return_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 592, OLD.tax_return_mb_group_nbr);

    /* TAX_RETURN_SECOND_MB_GROUP_NBR */
    IF (OLD.tax_return_second_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 593, OLD.tax_return_second_mb_group_nbr);

    /* MISC_CHECK_FORM */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 755, OLD.misc_check_form);

    /* HOLD_RETURN_QUEUE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 756, OLD.hold_return_queue);

    /* NUMBER_OF_INVOICE_COPIES */
    IF (OLD.number_of_invoice_copies IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 594, OLD.number_of_invoice_copies);

    /* PRORATE_FLAT_FEE_FOR_DBDT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 757, OLD.prorate_flat_fee_for_dbdt);

    /* INITIAL_EFFECTIVE_DATE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 595, OLD.initial_effective_date);

    /* SB_OTHER_SERVICE_NBR */
    IF (OLD.sb_other_service_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 596, OLD.sb_other_service_nbr);

    /* BREAK_CHECKS_BY_DBDT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 758, OLD.break_checks_by_dbdt);

    /* SUMMARIZE_SUI */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 759, OLD.summarize_sui);

    /* SHOW_SHORTFALL_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 760, OLD.show_shortfall_check);

    /* TRUST_CHK_OFFSET_GL_TAG */
    IF (OLD.trust_chk_offset_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 656, OLD.trust_chk_offset_gl_tag);

    /* MANUAL_CL_BANK_ACCOUNT_NBR */
    IF (OLD.manual_cl_bank_account_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 597, OLD.manual_cl_bank_account_nbr);

    /* WELLS_FARGO_ACH_FLAG */
    IF (OLD.wells_fargo_ach_flag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 657, OLD.wells_fargo_ach_flag);

    /* BILLING_CHECK_CPA */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 761, OLD.billing_check_cpa);

    /* SALES_TAX_PERCENTAGE */
    IF (OLD.sales_tax_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 512, OLD.sales_tax_percentage);

    /* EXCLUDE_R_C_B_0R_N */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 667, OLD.exclude_r_c_b_0r_n);

    /* CALCULATE_STATES_FIRST */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 666, OLD.calculate_states_first);

    /* INVOICE_DISCOUNT */
    IF (OLD.invoice_discount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 479, OLD.invoice_discount);

    /* DISCOUNT_START_DATE */
    IF (OLD.discount_start_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 521, OLD.discount_start_date);

    /* DISCOUNT_END_DATE */
    IF (OLD.discount_end_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 520, OLD.discount_end_date);

    /* SHOW_TIME_CLOCK_PUNCH */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 664, OLD.show_time_clock_punch);

    /* AUTO_RD_DFLT_CL_E_D_GROUPS_NBR */
    IF (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 598, OLD.auto_rd_dflt_cl_e_d_groups_nbr);

    /* AUTO_REDUCTION_DEFAULT_EES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 762, OLD.auto_reduction_default_ees);

    /* VT_HEALTHCARE_GL_TAG */
    IF (OLD.vt_healthcare_gl_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 659, OLD.vt_healthcare_gl_tag);

  END

  rdb$set_context('USER_TRANSACTION', '@TABLE_CHANGE_NBR', table_change_nbr);
END

^

CREATE TRIGGER T_AIU_CO_3 FOR CO After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.custom_company_number IS DISTINCT FROM NEW.custom_company_number) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.referred_by IS DISTINCT FROM NEW.referred_by) OR (OLD.sb_referrals_nbr IS DISTINCT FROM NEW.sb_referrals_nbr) OR (OLD.start_date IS DISTINCT FROM NEW.start_date) OR (rdb$get_context('USER_TRANSACTION', '@TERMINATION_NOTES') IS NOT NULL) OR (OLD.sb_accountant_nbr IS DISTINCT FROM NEW.sb_accountant_nbr) OR (OLD.accountant_contact IS DISTINCT FROM NEW.accountant_contact) OR (OLD.union_cl_e_ds_nbr IS DISTINCT FROM NEW.union_cl_e_ds_nbr) OR (OLD.cl_common_paymaster_nbr IS DISTINCT FROM NEW.cl_common_paymaster_nbr) OR (OLD.cl_co_consolidation_nbr IS DISTINCT FROM NEW.cl_co_consolidation_nbr) OR (OLD.remote IS DISTINCT FROM NEW.remote) OR (OLD.business_start_date IS DISTINCT FROM NEW.business_start_date) OR (OLD.corporation_type IS DISTINCT FROM NEW.corporation_type) OR (rdb$get_context('USER_TRANSACTION', '@NATURE_OF_BUSINESS') IS NOT NULL) OR (OLD.restaurant IS DISTINCT FROM NEW.restaurant) OR (OLD.days_open IS DISTINCT FROM NEW.days_open) OR (OLD.time_open IS DISTINCT FROM NEW.time_open) OR (OLD.time_close IS DISTINCT FROM NEW.time_close) OR (rdb$get_context('USER_TRANSACTION', '@COMPANY_NOTES') IS NOT NULL) OR (OLD.successor_company IS DISTINCT FROM NEW.successor_company) OR (OLD.medical_plan IS DISTINCT FROM NEW.medical_plan) OR (OLD.retirement_age IS DISTINCT FROM NEW.retirement_age) OR (OLD.pay_frequencies IS DISTINCT FROM NEW.pay_frequencies) OR (OLD.general_ledger_format_string IS DISTINCT FROM NEW.general_ledger_format_string) OR (OLD.cl_billing_nbr IS DISTINCT FROM NEW.cl_billing_nbr) OR (OLD.dbdt_level IS DISTINCT FROM NEW.dbdt_level) OR (OLD.billing_level IS DISTINCT FROM NEW.billing_level) OR (OLD.bank_account_level IS DISTINCT FROM NEW.bank_account_level) OR (OLD.billing_sb_bank_account_nbr IS DISTINCT FROM NEW.billing_sb_bank_account_nbr) OR (OLD.tax_sb_bank_account_nbr IS DISTINCT FROM NEW.tax_sb_bank_account_nbr) OR (OLD.payroll_cl_bank_account_nbr IS DISTINCT FROM NEW.payroll_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_pr IS DISTINCT FROM NEW.debit_number_days_prior_pr) OR (OLD.tax_cl_bank_account_nbr IS DISTINCT FROM NEW.tax_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_tax IS DISTINCT FROM NEW.debit_number_of_days_prior_tax) OR (OLD.billing_cl_bank_account_nbr IS DISTINCT FROM NEW.billing_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_bill IS DISTINCT FROM NEW.debit_number_days_prior_bill) OR (OLD.dd_cl_bank_account_nbr IS DISTINCT FROM NEW.dd_cl_bank_account_nbr) OR (OLD.debit_number_of_days_prior_dd IS DISTINCT FROM NEW.debit_number_of_days_prior_dd) OR (OLD.home_co_states_nbr IS DISTINCT FROM NEW.home_co_states_nbr) OR (OLD.home_sdi_co_states_nbr IS DISTINCT FROM NEW.home_sdi_co_states_nbr) OR (OLD.home_sui_co_states_nbr IS DISTINCT FROM NEW.home_sui_co_states_nbr) OR (OLD.tax_service IS DISTINCT FROM NEW.tax_service) OR (OLD.tax_service_start_date IS DISTINCT FROM NEW.tax_service_start_date) OR (OLD.tax_service_end_date IS DISTINCT FROM NEW.tax_service_end_date) OR (OLD.workers_comp_cl_agency_nbr IS DISTINCT FROM NEW.workers_comp_cl_agency_nbr) OR (OLD.workers_comp_policy_id IS DISTINCT FROM NEW.workers_comp_policy_id) OR (OLD.w_comp_cl_bank_account_nbr IS DISTINCT FROM NEW.w_comp_cl_bank_account_nbr) OR (OLD.debit_number_days_prior_wc IS DISTINCT FROM NEW.debit_number_days_prior_wc) OR (OLD.w_comp_sb_bank_account_nbr IS DISTINCT FROM NEW.w_comp_sb_bank_account_nbr) OR (OLD.eftps_enrollment_status IS DISTINCT FROM NEW.eftps_enrollment_status) OR (OLD.eftps_enrollment_date IS DISTINCT FROM NEW.eftps_enrollment_date) OR (OLD.eftps_sequence_number IS DISTINCT FROM NEW.eftps_sequence_number) OR (OLD.eftps_enrollment_number IS DISTINCT FROM NEW.eftps_enrollment_number) OR (OLD.eftps_name IS DISTINCT FROM NEW.eftps_name) OR (OLD.pin_number IS DISTINCT FROM NEW.pin_number) OR (OLD.ach_sb_bank_account_nbr IS DISTINCT FROM NEW.ach_sb_bank_account_nbr) OR (OLD.trust_service IS DISTINCT FROM NEW.trust_service) OR (OLD.trust_sb_account_nbr IS DISTINCT FROM NEW.trust_sb_account_nbr) OR (OLD.trust_service_start_date IS DISTINCT FROM NEW.trust_service_start_date) OR (OLD.trust_service_end_date IS DISTINCT FROM NEW.trust_service_end_date) OR (OLD.obc IS DISTINCT FROM NEW.obc) OR (OLD.sb_obc_account_nbr IS DISTINCT FROM NEW.sb_obc_account_nbr) OR (OLD.obc_start_date IS DISTINCT FROM NEW.obc_start_date) OR (OLD.obc_end_date IS DISTINCT FROM NEW.obc_end_date) OR (OLD.sy_fed_reporting_agency_nbr IS DISTINCT FROM NEW.sy_fed_reporting_agency_nbr) OR (OLD.sy_fed_tax_payment_agency_nbr IS DISTINCT FROM NEW.sy_fed_tax_payment_agency_nbr) OR (OLD.federal_tax_transfer_method IS DISTINCT FROM NEW.federal_tax_transfer_method) OR (OLD.federal_tax_payment_method IS DISTINCT FROM NEW.federal_tax_payment_method) OR (OLD.fui_sy_tax_payment_agency IS DISTINCT FROM NEW.fui_sy_tax_payment_agency) OR (OLD.fui_sy_tax_report_agency IS DISTINCT FROM NEW.fui_sy_tax_report_agency) OR (OLD.external_tax_export IS DISTINCT FROM NEW.external_tax_export) OR (OLD.non_profit IS DISTINCT FROM NEW.non_profit) OR (OLD.primary_sort_field IS DISTINCT FROM NEW.primary_sort_field) OR (OLD.secondary_sort_field IS DISTINCT FROM NEW.secondary_sort_field) OR (OLD.fiscal_year_end IS DISTINCT FROM NEW.fiscal_year_end) OR (OLD.third_party_in_gross_pr_report IS DISTINCT FROM NEW.third_party_in_gross_pr_report) OR (OLD.sic_code IS DISTINCT FROM NEW.sic_code) OR (OLD.deductions_to_zero IS DISTINCT FROM NEW.deductions_to_zero) OR (OLD.autopay_company IS DISTINCT FROM NEW.autopay_company) OR (OLD.pay_frequency_hourly_default IS DISTINCT FROM NEW.pay_frequency_hourly_default) OR (OLD.pay_frequency_salary_default IS DISTINCT FROM NEW.pay_frequency_salary_default) OR (OLD.co_check_primary_sort IS DISTINCT FROM NEW.co_check_primary_sort) OR (OLD.co_check_secondary_sort IS DISTINCT FROM NEW.co_check_secondary_sort) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.annual_cl_delivery_group_nbr IS DISTINCT FROM NEW.annual_cl_delivery_group_nbr) OR (OLD.quarter_cl_delivery_group_nbr IS DISTINCT FROM NEW.quarter_cl_delivery_group_nbr) OR (OLD.smoker_default IS DISTINCT FROM NEW.smoker_default) OR (OLD.auto_labor_dist_show_deducts IS DISTINCT FROM NEW.auto_labor_dist_show_deducts) OR (OLD.auto_labor_dist_level IS DISTINCT FROM NEW.auto_labor_dist_level) OR (OLD.distribute_deductions_default IS DISTINCT FROM NEW.distribute_deductions_default) OR (OLD.withholding_default IS DISTINCT FROM NEW.withholding_default) OR (OLD.check_type IS DISTINCT FROM NEW.check_type) OR (OLD.show_shifts_on_check IS DISTINCT FROM NEW.show_shifts_on_check) OR (OLD.show_ytd_on_check IS DISTINCT FROM NEW.show_ytd_on_check) OR (OLD.show_ein_number_on_check IS DISTINCT FROM NEW.show_ein_number_on_check) OR (OLD.show_ss_number_on_check IS DISTINCT FROM NEW.show_ss_number_on_check) OR (OLD.time_off_accrual IS DISTINCT FROM NEW.time_off_accrual) OR (OLD.check_form IS DISTINCT FROM NEW.check_form) OR (OLD.print_manual_check_stubs IS DISTINCT FROM NEW.print_manual_check_stubs) OR (OLD.credit_hold IS DISTINCT FROM NEW.credit_hold) OR (OLD.cl_timeclock_imports_nbr IS DISTINCT FROM NEW.cl_timeclock_imports_nbr) OR (OLD.payroll_password IS DISTINCT FROM NEW.payroll_password) OR (OLD.remote_of_client IS DISTINCT FROM NEW.remote_of_client) OR (OLD.hardware_o_s IS DISTINCT FROM NEW.hardware_o_s) OR (OLD.network IS DISTINCT FROM NEW.network) OR (OLD.modem_speed IS DISTINCT FROM NEW.modem_speed) OR (OLD.modem_connection_type IS DISTINCT FROM NEW.modem_connection_type) OR (OLD.network_administrator IS DISTINCT FROM NEW.network_administrator) OR (OLD.network_administrator_phone IS DISTINCT FROM NEW.network_administrator_phone) OR (OLD.network_admin_phone_type IS DISTINCT FROM NEW.network_admin_phone_type) OR (OLD.external_network_administrator IS DISTINCT FROM NEW.external_network_administrator) OR (OLD.transmission_destination IS DISTINCT FROM NEW.transmission_destination) OR (OLD.last_call_in_date IS DISTINCT FROM NEW.last_call_in_date) OR (OLD.setup_completed IS DISTINCT FROM NEW.setup_completed) OR (OLD.first_monthly_payroll_day IS DISTINCT FROM NEW.first_monthly_payroll_day) OR (OLD.second_monthly_payroll_day IS DISTINCT FROM NEW.second_monthly_payroll_day) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.collate_checks IS DISTINCT FROM NEW.collate_checks) OR (OLD.process_priority IS DISTINCT FROM NEW.process_priority) OR (OLD.check_message IS DISTINCT FROM NEW.check_message) OR (OLD.customer_service_sb_user_nbr IS DISTINCT FROM NEW.customer_service_sb_user_nbr) OR (rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES') IS NOT NULL) OR (rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES') IS NOT NULL) OR (OLD.final_tax_return IS DISTINCT FROM NEW.final_tax_return) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.billing_general_ledger_tag IS DISTINCT FROM NEW.billing_general_ledger_tag) OR (OLD.federal_general_ledger_tag IS DISTINCT FROM NEW.federal_general_ledger_tag) OR (OLD.ee_oasdi_general_ledger_tag IS DISTINCT FROM NEW.ee_oasdi_general_ledger_tag) OR (OLD.er_oasdi_general_ledger_tag IS DISTINCT FROM NEW.er_oasdi_general_ledger_tag) OR (OLD.ee_medicare_general_ledger_tag IS DISTINCT FROM NEW.ee_medicare_general_ledger_tag) OR (OLD.er_medicare_general_ledger_tag IS DISTINCT FROM NEW.er_medicare_general_ledger_tag) OR (OLD.fui_general_ledger_tag IS DISTINCT FROM NEW.fui_general_ledger_tag) OR (OLD.eic_general_ledger_tag IS DISTINCT FROM NEW.eic_general_ledger_tag) OR (OLD.backup_w_general_ledger_tag IS DISTINCT FROM NEW.backup_w_general_ledger_tag) OR (OLD.net_pay_general_ledger_tag IS DISTINCT FROM NEW.net_pay_general_ledger_tag) OR (OLD.tax_imp_general_ledger_tag IS DISTINCT FROM NEW.tax_imp_general_ledger_tag) OR (OLD.trust_imp_general_ledger_tag IS DISTINCT FROM NEW.trust_imp_general_ledger_tag) OR (OLD.thrd_p_tax_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_tax_general_ledger_tag) OR (OLD.thrd_p_chk_general_ledger_tag IS DISTINCT FROM NEW.thrd_p_chk_general_ledger_tag) OR (OLD.trust_chk_general_ledger_tag IS DISTINCT FROM NEW.trust_chk_general_ledger_tag) OR (OLD.federal_offset_gl_tag IS DISTINCT FROM NEW.federal_offset_gl_tag) OR (OLD.ee_oasdi_offset_gl_tag IS DISTINCT FROM NEW.ee_oasdi_offset_gl_tag) OR (OLD.er_oasdi_offset_gl_tag IS DISTINCT FROM NEW.er_oasdi_offset_gl_tag) OR (OLD.ee_medicare_offset_gl_tag IS DISTINCT FROM NEW.ee_medicare_offset_gl_tag) OR (OLD.er_medicare_offset_gl_tag IS DISTINCT FROM NEW.er_medicare_offset_gl_tag) OR (OLD.fui_offset_gl_tag IS DISTINCT FROM NEW.fui_offset_gl_tag) OR (OLD.eic_offset_gl_tag IS DISTINCT FROM NEW.eic_offset_gl_tag) OR (OLD.backup_w_offset_gl_tag IS DISTINCT FROM NEW.backup_w_offset_gl_tag) OR (OLD.trust_imp_offset_gl_tag IS DISTINCT FROM NEW.trust_imp_offset_gl_tag) OR (OLD.billing_exp_gl_tag IS DISTINCT FROM NEW.billing_exp_gl_tag) OR (OLD.er_oasdi_exp_gl_tag IS DISTINCT FROM NEW.er_oasdi_exp_gl_tag) OR (OLD.er_medicare_exp_gl_tag IS DISTINCT FROM NEW.er_medicare_exp_gl_tag) OR (OLD.auto_enlist IS DISTINCT FROM NEW.auto_enlist) OR (OLD.calculate_locals_first IS DISTINCT FROM NEW.calculate_locals_first) OR (OLD.weekend_action IS DISTINCT FROM NEW.weekend_action) OR (OLD.last_fui_correction IS DISTINCT FROM NEW.last_fui_correction) OR (OLD.last_sui_correction IS DISTINCT FROM NEW.last_sui_correction) OR (OLD.last_quarter_end_correction IS DISTINCT FROM NEW.last_quarter_end_correction) OR (OLD.last_preprocess IS DISTINCT FROM NEW.last_preprocess) OR (OLD.last_preprocess_message IS DISTINCT FROM NEW.last_preprocess_message) OR (OLD.charge_cobra_admin_fee IS DISTINCT FROM NEW.charge_cobra_admin_fee) OR (OLD.cobra_fee_day_of_month_due IS DISTINCT FROM NEW.cobra_fee_day_of_month_due) OR (OLD.cobra_notification_days IS DISTINCT FROM NEW.cobra_notification_days) OR (OLD.cobra_eligibility_confirm_days IS DISTINCT FROM NEW.cobra_eligibility_confirm_days) OR (OLD.show_rates_on_checks IS DISTINCT FROM NEW.show_rates_on_checks) OR (OLD.show_dir_dep_nbr_on_checks IS DISTINCT FROM NEW.show_dir_dep_nbr_on_checks) OR (OLD.fed_943_tax_deposit_frequency IS DISTINCT FROM NEW.fed_943_tax_deposit_frequency) OR (OLD.impound_workers_comp IS DISTINCT FROM NEW.impound_workers_comp) OR (OLD.reverse_check_printing IS DISTINCT FROM NEW.reverse_check_printing) OR (OLD.lock_date IS DISTINCT FROM NEW.lock_date) OR (OLD.auto_increment IS DISTINCT FROM NEW.auto_increment) OR (OLD.maximum_group_term_life IS DISTINCT FROM NEW.maximum_group_term_life) OR (OLD.payrate_precision IS DISTINCT FROM NEW.payrate_precision) OR (OLD.average_hours IS DISTINCT FROM NEW.average_hours) OR (OLD.maximum_hours_on_check IS DISTINCT FROM NEW.maximum_hours_on_check) OR (OLD.maximum_dollars_on_check IS DISTINCT FROM NEW.maximum_dollars_on_check) OR (OLD.discount_rate IS DISTINCT FROM NEW.discount_rate) OR (OLD.mod_rate IS DISTINCT FROM NEW.mod_rate) OR (OLD.minimum_tax_threshold IS DISTINCT FROM NEW.minimum_tax_threshold) OR (OLD.ss_disability_admin_fee IS DISTINCT FROM NEW.ss_disability_admin_fee) OR (OLD.check_time_off_avail IS DISTINCT FROM NEW.check_time_off_avail) OR (OLD.agency_check_mb_group_nbr IS DISTINCT FROM NEW.agency_check_mb_group_nbr) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_report_mb_group_nbr IS DISTINCT FROM NEW.pr_report_mb_group_nbr) OR (OLD.pr_report_second_mb_group_nbr IS DISTINCT FROM NEW.pr_report_second_mb_group_nbr) OR (OLD.tax_check_mb_group_nbr IS DISTINCT FROM NEW.tax_check_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.tax_return_mb_group_nbr IS DISTINCT FROM NEW.tax_return_mb_group_nbr) OR (OLD.tax_return_second_mb_group_nbr IS DISTINCT FROM NEW.tax_return_second_mb_group_nbr) OR (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) OR (OLD.hold_return_queue IS DISTINCT FROM NEW.hold_return_queue) OR (OLD.number_of_invoice_copies IS DISTINCT FROM NEW.number_of_invoice_copies) OR (OLD.prorate_flat_fee_for_dbdt IS DISTINCT FROM NEW.prorate_flat_fee_for_dbdt) OR (OLD.initial_effective_date IS DISTINCT FROM NEW.initial_effective_date) OR (OLD.sb_other_service_nbr IS DISTINCT FROM NEW.sb_other_service_nbr) OR (OLD.break_checks_by_dbdt IS DISTINCT FROM NEW.break_checks_by_dbdt) OR (OLD.summarize_sui IS DISTINCT FROM NEW.summarize_sui) OR (OLD.show_shortfall_check IS DISTINCT FROM NEW.show_shortfall_check) OR (OLD.trust_chk_offset_gl_tag IS DISTINCT FROM NEW.trust_chk_offset_gl_tag) OR (OLD.manual_cl_bank_account_nbr IS DISTINCT FROM NEW.manual_cl_bank_account_nbr) OR (OLD.wells_fargo_ach_flag IS DISTINCT FROM NEW.wells_fargo_ach_flag) OR (OLD.billing_check_cpa IS DISTINCT FROM NEW.billing_check_cpa) OR (OLD.sales_tax_percentage IS DISTINCT FROM NEW.sales_tax_percentage) OR (OLD.exclude_r_c_b_0r_n IS DISTINCT FROM NEW.exclude_r_c_b_0r_n) OR (OLD.calculate_states_first IS DISTINCT FROM NEW.calculate_states_first) OR (OLD.invoice_discount IS DISTINCT FROM NEW.invoice_discount) OR (OLD.discount_start_date IS DISTINCT FROM NEW.discount_start_date) OR (OLD.discount_end_date IS DISTINCT FROM NEW.discount_end_date) OR (OLD.show_time_clock_punch IS DISTINCT FROM NEW.show_time_clock_punch) OR (OLD.auto_rd_dflt_cl_e_d_groups_nbr IS DISTINCT FROM NEW.auto_rd_dflt_cl_e_d_groups_nbr) OR (OLD.auto_reduction_default_ees IS DISTINCT FROM NEW.auto_reduction_default_ees) OR (OLD.vt_healthcare_gl_tag IS DISTINCT FROM NEW.vt_healthcare_gl_tag) OR (OLD.vt_healthcare_offset_gl_tag IS DISTINCT FROM NEW.vt_healthcare_offset_gl_tag) OR (OLD.ui_rounding_gl_tag IS DISTINCT FROM NEW.ui_rounding_gl_tag) OR (OLD.ui_rounding_offset_gl_tag IS DISTINCT FROM NEW.ui_rounding_offset_gl_tag) OR (OLD.reprint_to_balance IS DISTINCT FROM NEW.reprint_to_balance) OR (OLD.show_manual_checks_in_ess IS DISTINCT FROM NEW.show_manual_checks_in_ess) OR (OLD.payroll_requires_mgr_approval IS DISTINCT FROM NEW.payroll_requires_mgr_approval) OR (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) OR (OLD.wc_offs_bank_account_nbr IS DISTINCT FROM NEW.wc_offs_bank_account_nbr) OR (OLD.qtr_lock_for_tax_pmts IS DISTINCT FROM NEW.qtr_lock_for_tax_pmts) OR (OLD.co_max_amount_for_payroll IS DISTINCT FROM NEW.co_max_amount_for_payroll) OR (OLD.co_max_amount_for_tax_impound IS DISTINCT FROM NEW.co_max_amount_for_tax_impound) OR (OLD.co_max_amount_for_dd IS DISTINCT FROM NEW.co_max_amount_for_dd) OR (OLD.co_payroll_process_limitations IS DISTINCT FROM NEW.co_payroll_process_limitations) OR (OLD.co_ach_process_limitations IS DISTINCT FROM NEW.co_ach_process_limitations) OR (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) OR (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) OR (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) OR (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) OR (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) OR (OLD.cobra_credit_gl_tag IS DISTINCT FROM NEW.cobra_credit_gl_tag) OR (OLD.co_exception_payment_type IS DISTINCT FROM NEW.co_exception_payment_type) OR (OLD.last_tax_return IS DISTINCT FROM NEW.last_tax_return) OR (OLD.enable_hr IS DISTINCT FROM NEW.enable_hr) OR (OLD.enable_ess IS DISTINCT FROM NEW.enable_ess) OR (OLD.duns_and_bradstreet IS DISTINCT FROM NEW.duns_and_bradstreet) OR (OLD.bank_account_register_name IS DISTINCT FROM NEW.bank_account_register_name) OR (OLD.enable_benefits IS DISTINCT FROM NEW.enable_benefits) OR (OLD.enable_time_off IS DISTINCT FROM NEW.enable_time_off) OR (OLD.wc_fiscal_year_begin IS DISTINCT FROM NEW.wc_fiscal_year_begin) OR (OLD.show_paystubs_ess_days IS DISTINCT FROM NEW.show_paystubs_ess_days)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE co SET 
      custom_company_number = NEW.custom_company_number, county = NEW.county, e_mail_address = NEW.e_mail_address, referred_by = NEW.referred_by, sb_referrals_nbr = NEW.sb_referrals_nbr, start_date = NEW.start_date, termination_notes = NEW.termination_notes, sb_accountant_nbr = NEW.sb_accountant_nbr, accountant_contact = NEW.accountant_contact, union_cl_e_ds_nbr = NEW.union_cl_e_ds_nbr, cl_common_paymaster_nbr = NEW.cl_common_paymaster_nbr, cl_co_consolidation_nbr = NEW.cl_co_consolidation_nbr, remote = NEW.remote, business_start_date = NEW.business_start_date, corporation_type = NEW.corporation_type, nature_of_business = NEW.nature_of_business, restaurant = NEW.restaurant, days_open = NEW.days_open, time_open = NEW.time_open, time_close = NEW.time_close, company_notes = NEW.company_notes, successor_company = NEW.successor_company, medical_plan = NEW.medical_plan, retirement_age = NEW.retirement_age, pay_frequencies = NEW.pay_frequencies, general_ledger_format_string = NEW.general_ledger_format_string, cl_billing_nbr = NEW.cl_billing_nbr, dbdt_level = NEW.dbdt_level, billing_level = NEW.billing_level, bank_account_level = NEW.bank_account_level, billing_sb_bank_account_nbr = NEW.billing_sb_bank_account_nbr, tax_sb_bank_account_nbr = NEW.tax_sb_bank_account_nbr, payroll_cl_bank_account_nbr = NEW.payroll_cl_bank_account_nbr, debit_number_days_prior_pr = NEW.debit_number_days_prior_pr, tax_cl_bank_account_nbr = NEW.tax_cl_bank_account_nbr, debit_number_of_days_prior_tax = NEW.debit_number_of_days_prior_tax, billing_cl_bank_account_nbr = NEW.billing_cl_bank_account_nbr, debit_number_days_prior_bill = NEW.debit_number_days_prior_bill, dd_cl_bank_account_nbr = NEW.dd_cl_bank_account_nbr, debit_number_of_days_prior_dd = NEW.debit_number_of_days_prior_dd, home_co_states_nbr = NEW.home_co_states_nbr, home_sdi_co_states_nbr = NEW.home_sdi_co_states_nbr, home_sui_co_states_nbr = NEW.home_sui_co_states_nbr, tax_service = NEW.tax_service, tax_service_start_date = NEW.tax_service_start_date, tax_service_end_date = NEW.tax_service_end_date, workers_comp_cl_agency_nbr = NEW.workers_comp_cl_agency_nbr, workers_comp_policy_id = NEW.workers_comp_policy_id, w_comp_cl_bank_account_nbr = NEW.w_comp_cl_bank_account_nbr, debit_number_days_prior_wc = NEW.debit_number_days_prior_wc, w_comp_sb_bank_account_nbr = NEW.w_comp_sb_bank_account_nbr, eftps_enrollment_status = NEW.eftps_enrollment_status, eftps_enrollment_date = NEW.eftps_enrollment_date, eftps_sequence_number = NEW.eftps_sequence_number, eftps_enrollment_number = NEW.eftps_enrollment_number, eftps_name = NEW.eftps_name, pin_number = NEW.pin_number, ach_sb_bank_account_nbr = NEW.ach_sb_bank_account_nbr, trust_service = NEW.trust_service, trust_sb_account_nbr = NEW.trust_sb_account_nbr, trust_service_start_date = NEW.trust_service_start_date, trust_service_end_date = NEW.trust_service_end_date, obc = NEW.obc, sb_obc_account_nbr = NEW.sb_obc_account_nbr, obc_start_date = NEW.obc_start_date, obc_end_date = NEW.obc_end_date, sy_fed_reporting_agency_nbr = NEW.sy_fed_reporting_agency_nbr, sy_fed_tax_payment_agency_nbr = NEW.sy_fed_tax_payment_agency_nbr, federal_tax_transfer_method = NEW.federal_tax_transfer_method, federal_tax_payment_method = NEW.federal_tax_payment_method, fui_sy_tax_payment_agency = NEW.fui_sy_tax_payment_agency, fui_sy_tax_report_agency = NEW.fui_sy_tax_report_agency, external_tax_export = NEW.external_tax_export, non_profit = NEW.non_profit, primary_sort_field = NEW.primary_sort_field, secondary_sort_field = NEW.secondary_sort_field, fiscal_year_end = NEW.fiscal_year_end, third_party_in_gross_pr_report = NEW.third_party_in_gross_pr_report, sic_code = NEW.sic_code, deductions_to_zero = NEW.deductions_to_zero, autopay_company = NEW.autopay_company, pay_frequency_hourly_default = NEW.pay_frequency_hourly_default, pay_frequency_salary_default = NEW.pay_frequency_salary_default, co_check_primary_sort = NEW.co_check_primary_sort, co_check_secondary_sort = NEW.co_check_secondary_sort, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, annual_cl_delivery_group_nbr = NEW.annual_cl_delivery_group_nbr, quarter_cl_delivery_group_nbr = NEW.quarter_cl_delivery_group_nbr, smoker_default = NEW.smoker_default, auto_labor_dist_show_deducts = NEW.auto_labor_dist_show_deducts, auto_labor_dist_level = NEW.auto_labor_dist_level, distribute_deductions_default = NEW.distribute_deductions_default, withholding_default = NEW.withholding_default, check_type = NEW.check_type, show_shifts_on_check = NEW.show_shifts_on_check, show_ytd_on_check = NEW.show_ytd_on_check, show_ein_number_on_check = NEW.show_ein_number_on_check, show_ss_number_on_check = NEW.show_ss_number_on_check, time_off_accrual = NEW.time_off_accrual, check_form = NEW.check_form, print_manual_check_stubs = NEW.print_manual_check_stubs, credit_hold = NEW.credit_hold, cl_timeclock_imports_nbr = NEW.cl_timeclock_imports_nbr, payroll_password = NEW.payroll_password, remote_of_client = NEW.remote_of_client, hardware_o_s = NEW.hardware_o_s, network = NEW.network, modem_speed = NEW.modem_speed, modem_connection_type = NEW.modem_connection_type, network_administrator = NEW.network_administrator, network_administrator_phone = NEW.network_administrator_phone, network_admin_phone_type = NEW.network_admin_phone_type, external_network_administrator = NEW.external_network_administrator, transmission_destination = NEW.transmission_destination, last_call_in_date = NEW.last_call_in_date, setup_completed = NEW.setup_completed, first_monthly_payroll_day = NEW.first_monthly_payroll_day, second_monthly_payroll_day = NEW.second_monthly_payroll_day, filler = NEW.filler, collate_checks = NEW.collate_checks, process_priority = NEW.process_priority, check_message = NEW.check_message, customer_service_sb_user_nbr = NEW.customer_service_sb_user_nbr, invoice_notes = NEW.invoice_notes, tax_cover_letter_notes = NEW.tax_cover_letter_notes, final_tax_return = NEW.final_tax_return, general_ledger_tag = NEW.general_ledger_tag, billing_general_ledger_tag = NEW.billing_general_ledger_tag, federal_general_ledger_tag = NEW.federal_general_ledger_tag, ee_oasdi_general_ledger_tag = NEW.ee_oasdi_general_ledger_tag, er_oasdi_general_ledger_tag = NEW.er_oasdi_general_ledger_tag, ee_medicare_general_ledger_tag = NEW.ee_medicare_general_ledger_tag, er_medicare_general_ledger_tag = NEW.er_medicare_general_ledger_tag, fui_general_ledger_tag = NEW.fui_general_ledger_tag, eic_general_ledger_tag = NEW.eic_general_ledger_tag, backup_w_general_ledger_tag = NEW.backup_w_general_ledger_tag, net_pay_general_ledger_tag = NEW.net_pay_general_ledger_tag, tax_imp_general_ledger_tag = NEW.tax_imp_general_ledger_tag, trust_imp_general_ledger_tag = NEW.trust_imp_general_ledger_tag, thrd_p_tax_general_ledger_tag = NEW.thrd_p_tax_general_ledger_tag, thrd_p_chk_general_ledger_tag = NEW.thrd_p_chk_general_ledger_tag, trust_chk_general_ledger_tag = NEW.trust_chk_general_ledger_tag, federal_offset_gl_tag = NEW.federal_offset_gl_tag, ee_oasdi_offset_gl_tag = NEW.ee_oasdi_offset_gl_tag, er_oasdi_offset_gl_tag = NEW.er_oasdi_offset_gl_tag, ee_medicare_offset_gl_tag = NEW.ee_medicare_offset_gl_tag, er_medicare_offset_gl_tag = NEW.er_medicare_offset_gl_tag, fui_offset_gl_tag = NEW.fui_offset_gl_tag, eic_offset_gl_tag = NEW.eic_offset_gl_tag, backup_w_offset_gl_tag = NEW.backup_w_offset_gl_tag, trust_imp_offset_gl_tag = NEW.trust_imp_offset_gl_tag, billing_exp_gl_tag = NEW.billing_exp_gl_tag, er_oasdi_exp_gl_tag = NEW.er_oasdi_exp_gl_tag, er_medicare_exp_gl_tag = NEW.er_medicare_exp_gl_tag, auto_enlist = NEW.auto_enlist, calculate_locals_first = NEW.calculate_locals_first, weekend_action = NEW.weekend_action, last_fui_correction = NEW.last_fui_correction, last_sui_correction = NEW.last_sui_correction, last_quarter_end_correction = NEW.last_quarter_end_correction, last_preprocess = NEW.last_preprocess, last_preprocess_message = NEW.last_preprocess_message, charge_cobra_admin_fee = NEW.charge_cobra_admin_fee, cobra_fee_day_of_month_due = NEW.cobra_fee_day_of_month_due, cobra_notification_days = NEW.cobra_notification_days, cobra_eligibility_confirm_days = NEW.cobra_eligibility_confirm_days, show_rates_on_checks = NEW.show_rates_on_checks, show_dir_dep_nbr_on_checks = NEW.show_dir_dep_nbr_on_checks, fed_943_tax_deposit_frequency = NEW.fed_943_tax_deposit_frequency, impound_workers_comp = NEW.impound_workers_comp, reverse_check_printing = NEW.reverse_check_printing, lock_date = NEW.lock_date, auto_increment = NEW.auto_increment, maximum_group_term_life = NEW.maximum_group_term_life, payrate_precision = NEW.payrate_precision, average_hours = NEW.average_hours, maximum_hours_on_check = NEW.maximum_hours_on_check, maximum_dollars_on_check = NEW.maximum_dollars_on_check, discount_rate = NEW.discount_rate, mod_rate = NEW.mod_rate, minimum_tax_threshold = NEW.minimum_tax_threshold, ss_disability_admin_fee = NEW.ss_disability_admin_fee, check_time_off_avail = NEW.check_time_off_avail, agency_check_mb_group_nbr = NEW.agency_check_mb_group_nbr, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_report_mb_group_nbr = NEW.pr_report_mb_group_nbr, pr_report_second_mb_group_nbr = NEW.pr_report_second_mb_group_nbr, tax_check_mb_group_nbr = NEW.tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr = NEW.tax_return_mb_group_nbr, tax_return_second_mb_group_nbr = NEW.tax_return_second_mb_group_nbr, misc_check_form = NEW.misc_check_form, hold_return_queue = NEW.hold_return_queue, number_of_invoice_copies = NEW.number_of_invoice_copies, prorate_flat_fee_for_dbdt = NEW.prorate_flat_fee_for_dbdt, initial_effective_date = NEW.initial_effective_date, sb_other_service_nbr = NEW.sb_other_service_nbr, break_checks_by_dbdt = NEW.break_checks_by_dbdt, summarize_sui = NEW.summarize_sui, show_shortfall_check = NEW.show_shortfall_check, trust_chk_offset_gl_tag = NEW.trust_chk_offset_gl_tag, manual_cl_bank_account_nbr = NEW.manual_cl_bank_account_nbr, wells_fargo_ach_flag = NEW.wells_fargo_ach_flag, billing_check_cpa = NEW.billing_check_cpa, sales_tax_percentage = NEW.sales_tax_percentage, exclude_r_c_b_0r_n = NEW.exclude_r_c_b_0r_n, calculate_states_first = NEW.calculate_states_first, invoice_discount = NEW.invoice_discount, discount_start_date = NEW.discount_start_date, discount_end_date = NEW.discount_end_date, show_time_clock_punch = NEW.show_time_clock_punch, auto_rd_dflt_cl_e_d_groups_nbr = NEW.auto_rd_dflt_cl_e_d_groups_nbr, auto_reduction_default_ees = NEW.auto_reduction_default_ees, vt_healthcare_gl_tag = NEW.vt_healthcare_gl_tag, vt_healthcare_offset_gl_tag = NEW.vt_healthcare_offset_gl_tag, ui_rounding_gl_tag = NEW.ui_rounding_gl_tag, ui_rounding_offset_gl_tag = NEW.ui_rounding_offset_gl_tag, reprint_to_balance = NEW.reprint_to_balance, show_manual_checks_in_ess = NEW.show_manual_checks_in_ess, payroll_requires_mgr_approval = NEW.payroll_requires_mgr_approval, days_prior_to_check_date = NEW.days_prior_to_check_date, wc_offs_bank_account_nbr = NEW.wc_offs_bank_account_nbr, qtr_lock_for_tax_pmts = NEW.qtr_lock_for_tax_pmts, co_max_amount_for_payroll = NEW.co_max_amount_for_payroll, co_max_amount_for_tax_impound = NEW.co_max_amount_for_tax_impound, co_max_amount_for_dd = NEW.co_max_amount_for_dd, co_payroll_process_limitations = NEW.co_payroll_process_limitations, co_ach_process_limitations = NEW.co_ach_process_limitations, trust_impound = NEW.trust_impound, tax_impound = NEW.tax_impound, dd_impound = NEW.dd_impound, billing_impound = NEW.billing_impound, wc_impound = NEW.wc_impound, cobra_credit_gl_tag = NEW.cobra_credit_gl_tag, co_exception_payment_type = NEW.co_exception_payment_type, last_tax_return = NEW.last_tax_return, enable_hr = NEW.enable_hr, enable_ess = NEW.enable_ess, duns_and_bradstreet = NEW.duns_and_bradstreet, bank_account_register_name = NEW.bank_account_register_name, enable_benefits = NEW.enable_benefits, enable_time_off = NEW.enable_time_off, wc_fiscal_year_begin = NEW.wc_fiscal_year_begin, show_paystubs_ess_days = NEW.show_paystubs_ess_days
      WHERE co_nbr = NEW.co_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_CO_BENEFITS_9 FOR CO_BENEFITS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(157, OLD.rec_version, OLD.co_benefits_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2960, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2961, OLD.effective_until);

  /* BENEFIT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2962, OLD.benefit_name);

  /* BENEFIT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2963, OLD.benefit_type);

  /* CERTIFICATE_NUMBER */
  IF (OLD.certificate_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2964, OLD.certificate_number);

  /* GROUP_NUMBER */
  IF (OLD.group_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2965, OLD.group_number);

  /* POLICY_NUMBER */
  IF (OLD.policy_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2966, OLD.policy_number);

  /* FREQUENCY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2967, OLD.frequency);

  /* TAXABLE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2968, OLD.taxable);

  /* EMPLOYEE_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2969, OLD.employee_type);

  /* ROUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2970, OLD.round);

  /* ELIGIBILITY_WAITING_PERIOD */
  IF (OLD.eligibility_waiting_period IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2971, OLD.eligibility_waiting_period);

  /* EXPIRATION_DATE */
  IF (OLD.expiration_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2972, OLD.expiration_date);

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2973, OLD.minimum_amount);

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2974, OLD.maximum_amount);

  /* MINIMUM_AGE_REQUIREMENT */
  IF (OLD.minimum_age_requirement IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2975, OLD.minimum_age_requirement);

  /* CL_AGENCY_NBR */
  IF (OLD.cl_agency_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2976, OLD.cl_agency_nbr);

  /* CO_BENEFIT_CATEGORY_NBR */
  IF (OLD.co_benefit_category_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2977, OLD.co_benefit_category_nbr);

  /* EE_DEDUCTION_NBR */
  IF (OLD.ee_deduction_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2978, OLD.ee_deduction_nbr);

  /* ER_DEDUCTION_NBR */
  IF (OLD.er_deduction_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2979, OLD.er_deduction_nbr);

  /* EE_COBRA_NBR */
  IF (OLD.ee_cobra_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2980, OLD.ee_cobra_nbr);

  /* ER_COBRA_NBR */
  IF (OLD.er_cobra_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2981, OLD.er_cobra_nbr);

  /* W2_FLAG */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2982, OLD.w2_flag);

  /* CO_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2983, OLD.co_nbr);

  /* READ_ONLY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2984, OLD.read_only);

  /* ALLOW_HSA */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2985, OLD.allow_hsa);

  /* REQUIRES_DOB */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2986, OLD.requires_dob);

  /* REQUIRES_SSN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2987, OLD.requires_ssn);

  /* REQUIRES_PCP */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2988, OLD.requires_pcp);

  /* SHOW_RATES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2989, OLD.show_rates);

  /* APPLY_DISCOUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2990, OLD.apply_discount);

  /* DISPLAY_COST_PERIOD */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2991, OLD.display_cost_period);

  /* ALLOW_EE_CONTRIBUTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2992, OLD.allow_ee_contribution);

  /* SHOW_DISCOUNT_RATING */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2993, OLD.show_discount_rating);

  /* QUAL_EVENT_ELIGIBLE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2994, OLD.qual_event_eligible);

  /* QUAL_EVENT_ENROLL_DURATION */
  IF (OLD.qual_event_enroll_duration IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2995, OLD.qual_event_enroll_duration);

  /* ADDITIONAL_INFO_TITLE1 */
  IF (OLD.additional_info_title1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2996, OLD.additional_info_title1);

  /* ADDITIONAL_INFO_TITLE2 */
  IF (OLD.additional_info_title2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2997, OLD.additional_info_title2);

  /* ADDITIONAL_INFO_TITLE3 */
  IF (OLD.additional_info_title3 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2998, OLD.additional_info_title3);

  /* EE_BENEFIT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2999, OLD.ee_benefit);

  /* REQUIRES_BENEFICIARIES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3000, OLD.requires_beneficiaries);

  /* CAFETERIA_PLAN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3001, OLD.cafeteria_plan);

  /* DECLINE_AMOUNT */
  IF (OLD.decline_amount IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3002, OLD.decline_amount);

END

^

CREATE TRIGGER T_AU_CO_BENEFITS_9 FOR CO_BENEFITS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(157, NEW.rec_version, NEW.co_benefits_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2960, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2961, OLD.effective_until);
    changes = changes + 1;
  END

  /* BENEFIT_NAME */
  IF (OLD.benefit_name IS DISTINCT FROM NEW.benefit_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2962, OLD.benefit_name);
    changes = changes + 1;
  END

  /* BENEFIT_TYPE */
  IF (OLD.benefit_type IS DISTINCT FROM NEW.benefit_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2963, OLD.benefit_type);
    changes = changes + 1;
  END

  /* CERTIFICATE_NUMBER */
  IF (OLD.certificate_number IS DISTINCT FROM NEW.certificate_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2964, OLD.certificate_number);
    changes = changes + 1;
  END

  /* GROUP_NUMBER */
  IF (OLD.group_number IS DISTINCT FROM NEW.group_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2965, OLD.group_number);
    changes = changes + 1;
  END

  /* POLICY_NUMBER */
  IF (OLD.policy_number IS DISTINCT FROM NEW.policy_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2966, OLD.policy_number);
    changes = changes + 1;
  END

  /* FREQUENCY */
  IF (OLD.frequency IS DISTINCT FROM NEW.frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2967, OLD.frequency);
    changes = changes + 1;
  END

  /* TAXABLE */
  IF (OLD.taxable IS DISTINCT FROM NEW.taxable) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2968, OLD.taxable);
    changes = changes + 1;
  END

  /* EMPLOYEE_TYPE */
  IF (OLD.employee_type IS DISTINCT FROM NEW.employee_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2969, OLD.employee_type);
    changes = changes + 1;
  END

  /* ROUND */
  IF (OLD.round IS DISTINCT FROM NEW.round) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2970, OLD.round);
    changes = changes + 1;
  END

  /* ELIGIBILITY_WAITING_PERIOD */
  IF (OLD.eligibility_waiting_period IS DISTINCT FROM NEW.eligibility_waiting_period) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2971, OLD.eligibility_waiting_period);
    changes = changes + 1;
  END

  /* EXPIRATION_DATE */
  IF (OLD.expiration_date IS DISTINCT FROM NEW.expiration_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2972, OLD.expiration_date);
    changes = changes + 1;
  END

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS DISTINCT FROM NEW.minimum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2973, OLD.minimum_amount);
    changes = changes + 1;
  END

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS DISTINCT FROM NEW.maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2974, OLD.maximum_amount);
    changes = changes + 1;
  END

  /* MINIMUM_AGE_REQUIREMENT */
  IF (OLD.minimum_age_requirement IS DISTINCT FROM NEW.minimum_age_requirement) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2975, OLD.minimum_age_requirement);
    changes = changes + 1;
  END

  /* CL_AGENCY_NBR */
  IF (OLD.cl_agency_nbr IS DISTINCT FROM NEW.cl_agency_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2976, OLD.cl_agency_nbr);
    changes = changes + 1;
  END

  /* CO_BENEFIT_CATEGORY_NBR */
  IF (OLD.co_benefit_category_nbr IS DISTINCT FROM NEW.co_benefit_category_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2977, OLD.co_benefit_category_nbr);
    changes = changes + 1;
  END

  /* EE_DEDUCTION_NBR */
  IF (OLD.ee_deduction_nbr IS DISTINCT FROM NEW.ee_deduction_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2978, OLD.ee_deduction_nbr);
    changes = changes + 1;
  END

  /* ER_DEDUCTION_NBR */
  IF (OLD.er_deduction_nbr IS DISTINCT FROM NEW.er_deduction_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2979, OLD.er_deduction_nbr);
    changes = changes + 1;
  END

  /* EE_COBRA_NBR */
  IF (OLD.ee_cobra_nbr IS DISTINCT FROM NEW.ee_cobra_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2980, OLD.ee_cobra_nbr);
    changes = changes + 1;
  END

  /* ER_COBRA_NBR */
  IF (OLD.er_cobra_nbr IS DISTINCT FROM NEW.er_cobra_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2981, OLD.er_cobra_nbr);
    changes = changes + 1;
  END

  /* W2_FLAG */
  IF (OLD.w2_flag IS DISTINCT FROM NEW.w2_flag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2982, OLD.w2_flag);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2983, OLD.co_nbr);
    changes = changes + 1;
  END

  /* READ_ONLY */
  IF (OLD.read_only IS DISTINCT FROM NEW.read_only) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2984, OLD.read_only);
    changes = changes + 1;
  END

  /* ALLOW_HSA */
  IF (OLD.allow_hsa IS DISTINCT FROM NEW.allow_hsa) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2985, OLD.allow_hsa);
    changes = changes + 1;
  END

  /* REQUIRES_DOB */
  IF (OLD.requires_dob IS DISTINCT FROM NEW.requires_dob) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2986, OLD.requires_dob);
    changes = changes + 1;
  END

  /* REQUIRES_SSN */
  IF (OLD.requires_ssn IS DISTINCT FROM NEW.requires_ssn) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2987, OLD.requires_ssn);
    changes = changes + 1;
  END

  /* REQUIRES_PCP */
  IF (OLD.requires_pcp IS DISTINCT FROM NEW.requires_pcp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2988, OLD.requires_pcp);
    changes = changes + 1;
  END

  /* SHOW_RATES */
  IF (OLD.show_rates IS DISTINCT FROM NEW.show_rates) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2989, OLD.show_rates);
    changes = changes + 1;
  END

  /* APPLY_DISCOUNT */
  IF (OLD.apply_discount IS DISTINCT FROM NEW.apply_discount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2990, OLD.apply_discount);
    changes = changes + 1;
  END

  /* DISPLAY_COST_PERIOD */
  IF (OLD.display_cost_period IS DISTINCT FROM NEW.display_cost_period) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2991, OLD.display_cost_period);
    changes = changes + 1;
  END

  /* ALLOW_EE_CONTRIBUTION */
  IF (OLD.allow_ee_contribution IS DISTINCT FROM NEW.allow_ee_contribution) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2992, OLD.allow_ee_contribution);
    changes = changes + 1;
  END

  /* SHOW_DISCOUNT_RATING */
  IF (OLD.show_discount_rating IS DISTINCT FROM NEW.show_discount_rating) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2993, OLD.show_discount_rating);
    changes = changes + 1;
  END

  /* QUAL_EVENT_ELIGIBLE */
  IF (OLD.qual_event_eligible IS DISTINCT FROM NEW.qual_event_eligible) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2994, OLD.qual_event_eligible);
    changes = changes + 1;
  END

  /* QUAL_EVENT_ENROLL_DURATION */
  IF (OLD.qual_event_enroll_duration IS DISTINCT FROM NEW.qual_event_enroll_duration) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2995, OLD.qual_event_enroll_duration);
    changes = changes + 1;
  END

  /* ADDITIONAL_INFO_TITLE1 */
  IF (OLD.additional_info_title1 IS DISTINCT FROM NEW.additional_info_title1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2996, OLD.additional_info_title1);
    changes = changes + 1;
  END

  /* ADDITIONAL_INFO_TITLE2 */
  IF (OLD.additional_info_title2 IS DISTINCT FROM NEW.additional_info_title2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2997, OLD.additional_info_title2);
    changes = changes + 1;
  END

  /* ADDITIONAL_INFO_TITLE3 */
  IF (OLD.additional_info_title3 IS DISTINCT FROM NEW.additional_info_title3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2998, OLD.additional_info_title3);
    changes = changes + 1;
  END

  /* EE_BENEFIT */
  IF (OLD.ee_benefit IS DISTINCT FROM NEW.ee_benefit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2999, OLD.ee_benefit);
    changes = changes + 1;
  END

  /* REQUIRES_BENEFICIARIES */
  IF (OLD.requires_beneficiaries IS DISTINCT FROM NEW.requires_beneficiaries) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3000, OLD.requires_beneficiaries);
    changes = changes + 1;
  END

  /* CAFETERIA_PLAN */
  IF (OLD.cafeteria_plan IS DISTINCT FROM NEW.cafeteria_plan) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3001, OLD.cafeteria_plan);
    changes = changes + 1;
  END

  /* DECLINE_AMOUNT */
  IF (OLD.decline_amount IS DISTINCT FROM NEW.decline_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3002, OLD.decline_amount);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_CO_PR_BATCH_DEFLT_ED_3 FOR CO_PR_BATCH_DEFLT_ED Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_CO_PR_BATCH_DEFLT_ED */
  IF (INSERTING OR (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.co_e_d_codes_nbr IS DISTINCT FROM NEW.co_e_d_codes_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM co_pr_batch_deflt_ed WHERE co_nbr = NEW.co_nbr AND co_e_d_codes_nbr = NEW.co_e_d_codes_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('co_pr_batch_deflt_ed', 'co_nbr, co_e_d_codes_nbr',
      CAST(NEW.co_nbr || ', ' || NEW.co_e_d_codes_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_AD_EE_9 FOR EE After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE blob_nbr INTEGER;
DECLARE VARIABLE cl_person_nbr INTEGER;
DECLARE VARIABLE co_division_nbr INTEGER;
DECLARE VARIABLE co_branch_nbr INTEGER;
DECLARE VARIABLE co_department_nbr INTEGER;
DECLARE VARIABLE co_team_nbr INTEGER;
DECLARE VARIABLE new_hire_report_sent CHAR(1);
DECLARE VARIABLE current_termination_code CHAR(1);
DECLARE VARIABLE co_hr_positions_nbr INTEGER;
DECLARE VARIABLE tipped_directly CHAR(1);
DECLARE VARIABLE w2_type CHAR(1);
DECLARE VARIABLE w2_pension CHAR(1);
DECLARE VARIABLE w2_deferred_comp CHAR(1);
DECLARE VARIABLE w2_deceased CHAR(1);
DECLARE VARIABLE w2_statutory_employee CHAR(1);
DECLARE VARIABLE w2_legal_rep CHAR(1);
DECLARE VARIABLE home_tax_ee_states_nbr INTEGER;
DECLARE VARIABLE exempt_employee_oasdi CHAR(1);
DECLARE VARIABLE exempt_employee_medicare CHAR(1);
DECLARE VARIABLE exempt_exclude_ee_fed CHAR(1);
DECLARE VARIABLE exempt_employer_oasdi CHAR(1);
DECLARE VARIABLE exempt_employer_medicare CHAR(1);
DECLARE VARIABLE exempt_employer_fui CHAR(1);
DECLARE VARIABLE base_returns_on_this_ee CHAR(1);
DECLARE VARIABLE company_or_individual_name CHAR(1);
DECLARE VARIABLE distribution_code_1099r VARCHAR(4);
DECLARE VARIABLE tax_amt_determined_1099r CHAR(1);
DECLARE VARIABLE total_distribution_1099r CHAR(1);
DECLARE VARIABLE pension_plan_1099r CHAR(1);
DECLARE VARIABLE makeup_fica_on_cleanup_pr CHAR(1);
DECLARE VARIABLE salary_amount NUMERIC(18,6);
DECLARE VARIABLE highly_compensated CHAR(1);
DECLARE VARIABLE corporate_officer CHAR(1);
DECLARE VARIABLE co_jobs_nbr INTEGER;
DECLARE VARIABLE co_workers_comp_nbr INTEGER;
DECLARE VARIABLE healthcare_coverage CHAR(1);
DECLARE VARIABLE fui_rate_credit_override NUMERIC(18,6);
DECLARE VARIABLE aca_status CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, OLD.rec_version, OLD.ee_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', cl_person_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr, new_hire_report_sent, current_termination_code, co_hr_positions_nbr, tipped_directly, w2_type, w2_pension, w2_deferred_comp, w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi, exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare, exempt_employer_fui, base_returns_on_this_ee, company_or_individual_name, distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, salary_amount, highly_compensated, corporate_officer, co_jobs_nbr, co_workers_comp_nbr, healthcare_coverage, fui_rate_credit_override, aca_status  FROM ee WHERE ee_nbr = OLD.ee_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :cl_person_nbr, :co_division_nbr, :co_branch_nbr, :co_department_nbr, :co_team_nbr, :new_hire_report_sent, :current_termination_code, :co_hr_positions_nbr, :tipped_directly, :w2_type, :w2_pension, :w2_deferred_comp, :w2_deceased, :w2_statutory_employee, :w2_legal_rep, :home_tax_ee_states_nbr, :exempt_employee_oasdi, :exempt_employee_medicare, :exempt_exclude_ee_fed, :exempt_employer_oasdi, :exempt_employer_medicare, :exempt_employer_fui, :base_returns_on_this_ee, :company_or_individual_name, :distribution_code_1099r, :tax_amt_determined_1099r, :total_distribution_1099r, :pension_plan_1099r, :makeup_fica_on_cleanup_pr, :salary_amount, :highly_compensated, :corporate_officer, :co_jobs_nbr, :co_workers_comp_nbr, :healthcare_coverage, :fui_rate_credit_override, :aca_status;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1810, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2784, OLD.effective_until);

  /* CL_PERSON_NBR */
  IF (last_record = 'Y' OR cl_person_nbr IS DISTINCT FROM OLD.cl_person_nbr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);

  /* CO_DIVISION_NBR */
  IF ((last_record = 'Y' AND OLD.co_division_nbr IS NOT NULL) OR (last_record = 'N' AND co_division_nbr IS DISTINCT FROM OLD.co_division_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);

  /* CO_BRANCH_NBR */
  IF ((last_record = 'Y' AND OLD.co_branch_nbr IS NOT NULL) OR (last_record = 'N' AND co_branch_nbr IS DISTINCT FROM OLD.co_branch_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);

  /* CO_DEPARTMENT_NBR */
  IF ((last_record = 'Y' AND OLD.co_department_nbr IS NOT NULL) OR (last_record = 'N' AND co_department_nbr IS DISTINCT FROM OLD.co_department_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);

  /* CO_TEAM_NBR */
  IF ((last_record = 'Y' AND OLD.co_team_nbr IS NOT NULL) OR (last_record = 'N' AND co_team_nbr IS DISTINCT FROM OLD.co_team_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);

  /* NEW_HIRE_REPORT_SENT */
  IF (last_record = 'Y' OR new_hire_report_sent IS DISTINCT FROM OLD.new_hire_report_sent) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);

  /* CURRENT_TERMINATION_CODE */
  IF (last_record = 'Y' OR current_termination_code IS DISTINCT FROM OLD.current_termination_code) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1889, OLD.current_termination_code);

  /* CO_HR_POSITIONS_NBR */
  IF ((last_record = 'Y' AND OLD.co_hr_positions_nbr IS NOT NULL) OR (last_record = 'N' AND co_hr_positions_nbr IS DISTINCT FROM OLD.co_hr_positions_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);

  /* TIPPED_DIRECTLY */
  IF (last_record = 'Y' OR tipped_directly IS DISTINCT FROM OLD.tipped_directly) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1893, OLD.tipped_directly);

  /* W2_TYPE */
  IF (last_record = 'Y' OR w2_type IS DISTINCT FROM OLD.w2_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1897, OLD.w2_type);

  /* W2_PENSION */
  IF (last_record = 'Y' OR w2_pension IS DISTINCT FROM OLD.w2_pension) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1898, OLD.w2_pension);

  /* W2_DEFERRED_COMP */
  IF (last_record = 'Y' OR w2_deferred_comp IS DISTINCT FROM OLD.w2_deferred_comp) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);

  /* W2_DECEASED */
  IF (last_record = 'Y' OR w2_deceased IS DISTINCT FROM OLD.w2_deceased) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1900, OLD.w2_deceased);

  /* W2_STATUTORY_EMPLOYEE */
  IF (last_record = 'Y' OR w2_statutory_employee IS DISTINCT FROM OLD.w2_statutory_employee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);

  /* W2_LEGAL_REP */
  IF (last_record = 'Y' OR w2_legal_rep IS DISTINCT FROM OLD.w2_legal_rep) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);

  /* HOME_TAX_EE_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.home_tax_ee_states_nbr IS NOT NULL) OR (last_record = 'N' AND home_tax_ee_states_nbr IS DISTINCT FROM OLD.home_tax_ee_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (last_record = 'Y' OR exempt_employee_oasdi IS DISTINCT FROM OLD.exempt_employee_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (last_record = 'Y' OR exempt_employee_medicare IS DISTINCT FROM OLD.exempt_employee_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (last_record = 'Y' OR exempt_exclude_ee_fed IS DISTINCT FROM OLD.exempt_exclude_ee_fed) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);

  /* EXEMPT_EMPLOYER_OASDI */
  IF (last_record = 'Y' OR exempt_employer_oasdi IS DISTINCT FROM OLD.exempt_employer_oasdi) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (last_record = 'Y' OR exempt_employer_medicare IS DISTINCT FROM OLD.exempt_employer_medicare) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);

  /* EXEMPT_EMPLOYER_FUI */
  IF (last_record = 'Y' OR exempt_employer_fui IS DISTINCT FROM OLD.exempt_employer_fui) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);

  /* BASE_RETURNS_ON_THIS_EE */
  IF (last_record = 'Y' OR base_returns_on_this_ee IS DISTINCT FROM OLD.base_returns_on_this_ee) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (last_record = 'Y' OR company_or_individual_name IS DISTINCT FROM OLD.company_or_individual_name) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);

  /* DISTRIBUTION_CODE_1099R */
  IF ((last_record = 'Y' AND OLD.distribution_code_1099r IS NOT NULL) OR (last_record = 'N' AND distribution_code_1099r IS DISTINCT FROM OLD.distribution_code_1099r)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);

  /* TAX_AMT_DETERMINED_1099R */
  IF (last_record = 'Y' OR tax_amt_determined_1099r IS DISTINCT FROM OLD.tax_amt_determined_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);

  /* TOTAL_DISTRIBUTION_1099R */
  IF (last_record = 'Y' OR total_distribution_1099r IS DISTINCT FROM OLD.total_distribution_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);

  /* PENSION_PLAN_1099R */
  IF (last_record = 'Y' OR pension_plan_1099r IS DISTINCT FROM OLD.pension_plan_1099r) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (last_record = 'Y' OR makeup_fica_on_cleanup_pr IS DISTINCT FROM OLD.makeup_fica_on_cleanup_pr) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);

  /* SALARY_AMOUNT */
  IF ((last_record = 'Y' AND OLD.salary_amount IS NOT NULL) OR (last_record = 'N' AND salary_amount IS DISTINCT FROM OLD.salary_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1809, OLD.salary_amount);

  /* HIGHLY_COMPENSATED */
  IF (last_record = 'Y' OR highly_compensated IS DISTINCT FROM OLD.highly_compensated) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1919, OLD.highly_compensated);

  /* CORPORATE_OFFICER */
  IF (last_record = 'Y' OR corporate_officer IS DISTINCT FROM OLD.corporate_officer) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1920, OLD.corporate_officer);

  /* CO_JOBS_NBR */
  IF ((last_record = 'Y' AND OLD.co_jobs_nbr IS NOT NULL) OR (last_record = 'N' AND co_jobs_nbr IS DISTINCT FROM OLD.co_jobs_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);

  /* CO_WORKERS_COMP_NBR */
  IF ((last_record = 'Y' AND OLD.co_workers_comp_nbr IS NOT NULL) OR (last_record = 'N' AND co_workers_comp_nbr IS DISTINCT FROM OLD.co_workers_comp_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);

  /* HEALTHCARE_COVERAGE */
  IF (last_record = 'Y' OR healthcare_coverage IS DISTINCT FROM OLD.healthcare_coverage) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF ((last_record = 'Y' AND OLD.fui_rate_credit_override IS NOT NULL) OR (last_record = 'N' AND fui_rate_credit_override IS DISTINCT FROM OLD.fui_rate_credit_override)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);

  /* ACA_STATUS */
  IF (last_record = 'Y' OR aca_status IS DISTINCT FROM OLD.aca_status) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3188, OLD.aca_status);


  IF (last_record = 'Y') THEN
  BEGIN
    /* CO_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1823, OLD.co_nbr);

    /* CUSTOM_EMPLOYEE_NUMBER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);

    /* CO_HR_APPLICANT_NBR */
    IF (OLD.co_hr_applicant_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);

    /* CO_HR_RECRUITERS_NBR */
    IF (OLD.co_hr_recruiters_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);

    /* CO_HR_REFERRALS_NBR */
    IF (OLD.co_hr_referrals_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);

    /* ADDRESS1 */
    IF (OLD.address1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1811, OLD.address1);

    /* ADDRESS2 */
    IF (OLD.address2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1812, OLD.address2);

    /* CITY */
    IF (OLD.city IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1869, OLD.city);

    /* STATE */
    IF (OLD.state IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1870, OLD.state);

    /* ZIP_CODE */
    IF (OLD.zip_code IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1831, OLD.zip_code);

    /* COUNTY */
    IF (OLD.county IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1832, OLD.county);

    /* PHONE1 */
    IF (OLD.phone1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1871, OLD.phone1);

    /* DESCRIPTION1 */
    IF (OLD.description1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1834, OLD.description1);

    /* PHONE2 */
    IF (OLD.phone2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1872, OLD.phone2);

    /* DESCRIPTION2 */
    IF (OLD.description2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1835, OLD.description2);

    /* PHONE3 */
    IF (OLD.phone3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1873, OLD.phone3);

    /* DESCRIPTION3 */
    IF (OLD.description3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1833, OLD.description3);

    /* E_MAIL_ADDRESS */
    IF (OLD.e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1874, OLD.e_mail_address);

    /* TIME_CLOCK_NUMBER */
    IF (OLD.time_clock_number IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1887, OLD.time_clock_number);

    /* ORIGINAL_HIRE_DATE */
    IF (OLD.original_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1848, OLD.original_hire_date);

    /* CURRENT_HIRE_DATE */
    IF (OLD.current_hire_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1849, OLD.current_hire_date);

    /* CURRENT_TERMINATION_DATE */
    IF (OLD.current_termination_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1850, OLD.current_termination_date);

    /* CO_HR_SUPERVISORS_NBR */
    IF (OLD.co_hr_supervisors_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);

    /* AUTOPAY_CO_SHIFTS_NBR */
    IF (OLD.autopay_co_shifts_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);

    /* POSITION_EFFECTIVE_DATE */
    IF (OLD.position_effective_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1851, OLD.position_effective_date);

    /* POSITION_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1890, OLD.position_status);

    /* CO_HR_PERFORMANCE_RATINGS_NBR */
    IF (OLD.co_hr_performance_ratings_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);

    /* REVIEW_DATE */
    IF (OLD.review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1852, OLD.review_date);

    /* NEXT_REVIEW_DATE */
    IF (OLD.next_review_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1853, OLD.next_review_date);

    /* PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1891, OLD.pay_frequency);

    /* NEXT_RAISE_DATE */
    IF (OLD.next_raise_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1854, OLD.next_raise_date);

    /* NEXT_PAY_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);

    /* ALD_CL_E_D_GROUPS_NBR */
    IF (OLD.ald_cl_e_d_groups_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);

    /* DISTRIBUTE_TAXES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);

    /* CO_PAY_GROUP_NBR */
    IF (OLD.co_pay_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);

    /* CL_DELIVERY_GROUP_NBR */
    IF (OLD.cl_delivery_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);

    /* CO_UNIONS_NBR */
    IF (OLD.co_unions_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);

    /* EIC */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1895, OLD.eic);

    /* FLSA_EXEMPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);

    /* OVERRIDE_FED_TAX_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);

    /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);

    /* SECURITY_CLEARANCE */
    IF (OLD.security_clearance IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1875, OLD.security_clearance);

    /* BADGE_ID */
    IF (OLD.badge_id IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1911, OLD.badge_id);

    /* ON_CALL_FROM */
    IF (OLD.on_call_from IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1813, OLD.on_call_from);

    /* ON_CALL_TO */
    IF (OLD.on_call_to IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1814, OLD.on_call_to);

    /* NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1815, :blob_nbr);
    END

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1876, OLD.filler);

    /* GENERAL_LEDGER_TAG */
    IF (OLD.general_ledger_tag IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);

    /* FEDERAL_MARITAL_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);

    /* NUMBER_OF_DEPENDENTS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);

    /* SY_HR_EEO_NBR */
    IF (OLD.sy_hr_eeo_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);

    /* SECONDARY_NOTES */
    blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
    IF (blob_nbr IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1816, :blob_nbr);
    END

    /* CO_PR_CHECK_TEMPLATES_NBR */
    IF (OLD.co_pr_check_templates_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);

    /* GENERATE_SECOND_CHECK */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1884, OLD.generate_second_check);

    /* STANDARD_HOURS */
    IF (OLD.standard_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1808, OLD.standard_hours);

    /* NEXT_RAISE_AMOUNT */
    IF (OLD.next_raise_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);

    /* NEXT_RAISE_PERCENTAGE */
    IF (OLD.next_raise_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);

    /* NEXT_RAISE_RATE */
    IF (OLD.next_raise_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);

    /* WORKERS_COMP_WAGE_LIMIT */
    IF (OLD.workers_comp_wage_limit IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);

    /* GROUP_TERM_POLICY_AMOUNT */
    IF (OLD.group_term_policy_amount IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);

    /* OVERRIDE_FED_TAX_VALUE */
    IF (OLD.override_fed_tax_value IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);

    /* CALCULATED_SALARY */
    IF (OLD.calculated_salary IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1801, OLD.calculated_salary);

    /* PR_CHECK_MB_GROUP_NBR */
    IF (OLD.pr_check_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);

    /* PR_EE_REPORT_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);

    /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
    IF (OLD.pr_ee_report_sec_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);

    /* TAX_EE_RETURN_MB_GROUP_NBR */
    IF (OLD.tax_ee_return_mb_group_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);

    /* EE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1921, OLD.ee_enabled);

    /* GTL_NUMBER_OF_HOURS */
    IF (OLD.gtl_number_of_hours IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);

    /* GTL_RATE */
    IF (OLD.gtl_rate IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1817, OLD.gtl_rate);

    /* AUTO_UPDATE_RATES */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);

    /* ELIGIBLE_FOR_REHIRE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);

    /* SELFSERVE_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);

    /* SELFSERVE_USERNAME */
    IF (OLD.selfserve_username IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1868, OLD.selfserve_username);

    /* SELFSERVE_PASSWORD */
    IF (OLD.selfserve_password IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1867, OLD.selfserve_password);

    /* SELFSERVE_LAST_LOGIN */
    IF (OLD.selfserve_last_login IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);

    /* PRINT_VOUCHER */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1882, OLD.print_voucher);

    /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
    IF (OLD.override_federal_minimum_wage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);

    /* WC_WAGE_LIMIT_FREQUENCY */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);

    /* LOGIN_QUESTION1 */
    IF (OLD.login_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1864, OLD.login_question1);

    /* LOGIN_ANSWER1 */
    IF (OLD.login_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1879, OLD.login_answer1);

    /* LOGIN_QUESTION2 */
    IF (OLD.login_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1865, OLD.login_question2);

    /* LOGIN_ANSWER2 */
    IF (OLD.login_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1880, OLD.login_answer2);

    /* LOGIN_QUESTION3 */
    IF (OLD.login_question3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1866, OLD.login_question3);

    /* LOGIN_ANSWER3 */
    IF (OLD.login_answer3 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1881, OLD.login_answer3);

    /* LOGIN_DATE */
    IF (OLD.login_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2785, OLD.login_date);

    /* LOGIN_ATTEMPTS */
    IF (OLD.login_attempts IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2786, OLD.login_attempts);

    /* CO_BENEFIT_DISCOUNT_NBR */
    IF (OLD.co_benefit_discount_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);

    /* BENEFITS_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);

    /* EXISTING_PATIENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2914, OLD.existing_patient);

    /* PCP */
    IF (OLD.pcp IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2915, OLD.pcp);

    /* TIME_OFF_ENABLED */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);

    /* DEPENDENT_BENEFITS_AVAILABLE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);

    /* DEPENDENT_BENEFITS_AVAIL_DATE */
    IF (OLD.dependent_benefits_avail_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);

    /* BENEFIT_ENROLLMENT_COMPLETE */
    IF (OLD.benefit_enrollment_complete IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);

    /* LAST_QUAL_BENEFIT_EVENT */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);

    /* LAST_QUAL_BENEFIT_EVENT_DATE */
    IF (OLD.last_qual_benefit_event_date IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);

    /* W2 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2922, OLD.w2);

    /* W2_FORM_ON_FILE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);

    /* BENEFIT_E_MAIL_ADDRESS */
    IF (OLD.benefit_e_mail_address IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);

    /* CO_HR_POSITION_GRADES_NBR */
    IF (OLD.co_hr_position_grades_nbr IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);

    /* SEC_QUESTION1 */
    IF (OLD.sec_question1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2926, OLD.sec_question1);

    /* SEC_ANSWER1 */
    IF (OLD.sec_answer1 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2927, OLD.sec_answer1);

    /* SEC_QUESTION2 */
    IF (OLD.sec_question2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2928, OLD.sec_question2);

    /* SEC_ANSWER2 */
    IF (OLD.sec_answer2 IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2929, OLD.sec_answer2);

  END
END

^

CREATE TRIGGER T_AIU_EE_3 FOR EE After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) OR (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) OR (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) OR (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) OR (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) OR (OLD.address1 IS DISTINCT FROM NEW.address1) OR (OLD.address2 IS DISTINCT FROM NEW.address2) OR (OLD.city IS DISTINCT FROM NEW.city) OR (OLD.state IS DISTINCT FROM NEW.state) OR (OLD.zip_code IS DISTINCT FROM NEW.zip_code) OR (OLD.county IS DISTINCT FROM NEW.county) OR (OLD.phone1 IS DISTINCT FROM NEW.phone1) OR (OLD.description1 IS DISTINCT FROM NEW.description1) OR (OLD.phone2 IS DISTINCT FROM NEW.phone2) OR (OLD.description2 IS DISTINCT FROM NEW.description2) OR (OLD.phone3 IS DISTINCT FROM NEW.phone3) OR (OLD.description3 IS DISTINCT FROM NEW.description3) OR (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) OR (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) OR (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) OR (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) OR (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) OR (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) OR (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) OR (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) OR (OLD.position_status IS DISTINCT FROM NEW.position_status) OR (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) OR (OLD.review_date IS DISTINCT FROM NEW.review_date) OR (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) OR (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) OR (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) OR (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) OR (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) OR (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) OR (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) OR (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) OR (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) OR (OLD.eic IS DISTINCT FROM NEW.eic) OR (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) OR (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) OR (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) OR (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) OR (OLD.badge_id IS DISTINCT FROM NEW.badge_id) OR (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) OR (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) OR (rdb$get_context('USER_TRANSACTION', '@NOTES') IS NOT NULL) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) OR (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) OR (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) OR (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) OR (rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES') IS NOT NULL) OR (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) OR (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) OR (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) OR (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) OR (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) OR (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) OR (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) OR (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) OR (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) OR (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) OR (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) OR (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) OR (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) OR (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) OR (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) OR (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) OR (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) OR (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) OR (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) OR (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) OR (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) OR (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) OR (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) OR (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) OR (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) OR (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) OR (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) OR (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) OR (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) OR (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) OR (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) OR (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) OR (OLD.login_date IS DISTINCT FROM NEW.login_date) OR (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) OR (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) OR (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) OR (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) OR (OLD.pcp IS DISTINCT FROM NEW.pcp) OR (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) OR (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) OR (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) OR (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) OR (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) OR (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) OR (OLD.w2 IS DISTINCT FROM NEW.w2) OR (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) OR (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) OR (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) OR (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) OR (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) OR (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) OR (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE ee SET 
      co_nbr = NEW.co_nbr, custom_employee_number = NEW.custom_employee_number, co_hr_applicant_nbr = NEW.co_hr_applicant_nbr, co_hr_recruiters_nbr = NEW.co_hr_recruiters_nbr, co_hr_referrals_nbr = NEW.co_hr_referrals_nbr, address1 = NEW.address1, address2 = NEW.address2, city = NEW.city, state = NEW.state, zip_code = NEW.zip_code, county = NEW.county, phone1 = NEW.phone1, description1 = NEW.description1, phone2 = NEW.phone2, description2 = NEW.description2, phone3 = NEW.phone3, description3 = NEW.description3, e_mail_address = NEW.e_mail_address, time_clock_number = NEW.time_clock_number, original_hire_date = NEW.original_hire_date, current_hire_date = NEW.current_hire_date, current_termination_date = NEW.current_termination_date, co_hr_supervisors_nbr = NEW.co_hr_supervisors_nbr, autopay_co_shifts_nbr = NEW.autopay_co_shifts_nbr, position_effective_date = NEW.position_effective_date, position_status = NEW.position_status, co_hr_performance_ratings_nbr = NEW.co_hr_performance_ratings_nbr, review_date = NEW.review_date, next_review_date = NEW.next_review_date, pay_frequency = NEW.pay_frequency, next_raise_date = NEW.next_raise_date, next_pay_frequency = NEW.next_pay_frequency, ald_cl_e_d_groups_nbr = NEW.ald_cl_e_d_groups_nbr, distribute_taxes = NEW.distribute_taxes, co_pay_group_nbr = NEW.co_pay_group_nbr, cl_delivery_group_nbr = NEW.cl_delivery_group_nbr, co_unions_nbr = NEW.co_unions_nbr, eic = NEW.eic, flsa_exempt = NEW.flsa_exempt, override_fed_tax_type = NEW.override_fed_tax_type, gov_garnish_prior_child_suppt = NEW.gov_garnish_prior_child_suppt, security_clearance = NEW.security_clearance, badge_id = NEW.badge_id, on_call_from = NEW.on_call_from, on_call_to = NEW.on_call_to, notes = NEW.notes, filler = NEW.filler, general_ledger_tag = NEW.general_ledger_tag, federal_marital_status = NEW.federal_marital_status, number_of_dependents = NEW.number_of_dependents, sy_hr_eeo_nbr = NEW.sy_hr_eeo_nbr, secondary_notes = NEW.secondary_notes, co_pr_check_templates_nbr = NEW.co_pr_check_templates_nbr, generate_second_check = NEW.generate_second_check, standard_hours = NEW.standard_hours, next_raise_amount = NEW.next_raise_amount, next_raise_percentage = NEW.next_raise_percentage, next_raise_rate = NEW.next_raise_rate, workers_comp_wage_limit = NEW.workers_comp_wage_limit, group_term_policy_amount = NEW.group_term_policy_amount, override_fed_tax_value = NEW.override_fed_tax_value, calculated_salary = NEW.calculated_salary, pr_check_mb_group_nbr = NEW.pr_check_mb_group_nbr, pr_ee_report_mb_group_nbr = NEW.pr_ee_report_mb_group_nbr, pr_ee_report_sec_mb_group_nbr = NEW.pr_ee_report_sec_mb_group_nbr, tax_ee_return_mb_group_nbr = NEW.tax_ee_return_mb_group_nbr, ee_enabled = NEW.ee_enabled, gtl_number_of_hours = NEW.gtl_number_of_hours, gtl_rate = NEW.gtl_rate, auto_update_rates = NEW.auto_update_rates, eligible_for_rehire = NEW.eligible_for_rehire, selfserve_enabled = NEW.selfserve_enabled, selfserve_username = NEW.selfserve_username, selfserve_password = NEW.selfserve_password, selfserve_last_login = NEW.selfserve_last_login, print_voucher = NEW.print_voucher, override_federal_minimum_wage = NEW.override_federal_minimum_wage, wc_wage_limit_frequency = NEW.wc_wage_limit_frequency, login_question1 = NEW.login_question1, login_answer1 = NEW.login_answer1, login_question2 = NEW.login_question2, login_answer2 = NEW.login_answer2, login_question3 = NEW.login_question3, login_answer3 = NEW.login_answer3, login_date = NEW.login_date, login_attempts = NEW.login_attempts, co_benefit_discount_nbr = NEW.co_benefit_discount_nbr, benefits_enabled = NEW.benefits_enabled, existing_patient = NEW.existing_patient, pcp = NEW.pcp, time_off_enabled = NEW.time_off_enabled, dependent_benefits_available = NEW.dependent_benefits_available, dependent_benefits_avail_date = NEW.dependent_benefits_avail_date, benefit_enrollment_complete = NEW.benefit_enrollment_complete, last_qual_benefit_event = NEW.last_qual_benefit_event, last_qual_benefit_event_date = NEW.last_qual_benefit_event_date, w2 = NEW.w2, w2_form_on_file = NEW.w2_form_on_file, benefit_e_mail_address = NEW.benefit_e_mail_address, co_hr_position_grades_nbr = NEW.co_hr_position_grades_nbr, sec_question1 = NEW.sec_question1, sec_answer1 = NEW.sec_answer1, sec_question2 = NEW.sec_question2, sec_answer2 = NEW.sec_answer2
      WHERE ee_nbr = NEW.ee_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_EE_9 FOR EE After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(115, NEW.rec_version, NEW.ee_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1810, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2784, OLD.effective_until);
    changes = changes + 1;
  END

  /* CL_PERSON_NBR */
  IF (OLD.cl_person_nbr IS DISTINCT FROM NEW.cl_person_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1822, OLD.cl_person_nbr);
    changes = changes + 1;
  END

  /* CO_NBR */
  IF (OLD.co_nbr IS DISTINCT FROM NEW.co_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1823, OLD.co_nbr);
    changes = changes + 1;
  END

  /* CO_DIVISION_NBR */
  IF (OLD.co_division_nbr IS DISTINCT FROM NEW.co_division_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1824, OLD.co_division_nbr);
    changes = changes + 1;
  END

  /* CO_BRANCH_NBR */
  IF (OLD.co_branch_nbr IS DISTINCT FROM NEW.co_branch_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1825, OLD.co_branch_nbr);
    changes = changes + 1;
  END

  /* CO_DEPARTMENT_NBR */
  IF (OLD.co_department_nbr IS DISTINCT FROM NEW.co_department_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1826, OLD.co_department_nbr);
    changes = changes + 1;
  END

  /* CO_TEAM_NBR */
  IF (OLD.co_team_nbr IS DISTINCT FROM NEW.co_team_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1827, OLD.co_team_nbr);
    changes = changes + 1;
  END

  /* CUSTOM_EMPLOYEE_NUMBER */
  IF (OLD.custom_employee_number IS DISTINCT FROM NEW.custom_employee_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1886, OLD.custom_employee_number);
    changes = changes + 1;
  END

  /* CO_HR_APPLICANT_NBR */
  IF (OLD.co_hr_applicant_nbr IS DISTINCT FROM NEW.co_hr_applicant_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1828, OLD.co_hr_applicant_nbr);
    changes = changes + 1;
  END

  /* CO_HR_RECRUITERS_NBR */
  IF (OLD.co_hr_recruiters_nbr IS DISTINCT FROM NEW.co_hr_recruiters_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1829, OLD.co_hr_recruiters_nbr);
    changes = changes + 1;
  END

  /* CO_HR_REFERRALS_NBR */
  IF (OLD.co_hr_referrals_nbr IS DISTINCT FROM NEW.co_hr_referrals_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1830, OLD.co_hr_referrals_nbr);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1811, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1812, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1869, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1870, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1831, OLD.zip_code);
    changes = changes + 1;
  END

  /* COUNTY */
  IF (OLD.county IS DISTINCT FROM NEW.county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1832, OLD.county);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1871, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1834, OLD.description1);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1872, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1835, OLD.description2);
    changes = changes + 1;
  END

  /* PHONE3 */
  IF (OLD.phone3 IS DISTINCT FROM NEW.phone3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1873, OLD.phone3);
    changes = changes + 1;
  END

  /* DESCRIPTION3 */
  IF (OLD.description3 IS DISTINCT FROM NEW.description3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1833, OLD.description3);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1874, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* TIME_CLOCK_NUMBER */
  IF (OLD.time_clock_number IS DISTINCT FROM NEW.time_clock_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1887, OLD.time_clock_number);
    changes = changes + 1;
  END

  /* ORIGINAL_HIRE_DATE */
  IF (OLD.original_hire_date IS DISTINCT FROM NEW.original_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1848, OLD.original_hire_date);
    changes = changes + 1;
  END

  /* CURRENT_HIRE_DATE */
  IF (OLD.current_hire_date IS DISTINCT FROM NEW.current_hire_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1849, OLD.current_hire_date);
    changes = changes + 1;
  END

  /* NEW_HIRE_REPORT_SENT */
  IF (OLD.new_hire_report_sent IS DISTINCT FROM NEW.new_hire_report_sent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1888, OLD.new_hire_report_sent);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_DATE */
  IF (OLD.current_termination_date IS DISTINCT FROM NEW.current_termination_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1850, OLD.current_termination_date);
    changes = changes + 1;
  END

  /* CURRENT_TERMINATION_CODE */
  IF (OLD.current_termination_code IS DISTINCT FROM NEW.current_termination_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1889, OLD.current_termination_code);
    changes = changes + 1;
  END

  /* CO_HR_SUPERVISORS_NBR */
  IF (OLD.co_hr_supervisors_nbr IS DISTINCT FROM NEW.co_hr_supervisors_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1836, OLD.co_hr_supervisors_nbr);
    changes = changes + 1;
  END

  /* AUTOPAY_CO_SHIFTS_NBR */
  IF (OLD.autopay_co_shifts_nbr IS DISTINCT FROM NEW.autopay_co_shifts_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1837, OLD.autopay_co_shifts_nbr);
    changes = changes + 1;
  END

  /* CO_HR_POSITIONS_NBR */
  IF (OLD.co_hr_positions_nbr IS DISTINCT FROM NEW.co_hr_positions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1838, OLD.co_hr_positions_nbr);
    changes = changes + 1;
  END

  /* POSITION_EFFECTIVE_DATE */
  IF (OLD.position_effective_date IS DISTINCT FROM NEW.position_effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1851, OLD.position_effective_date);
    changes = changes + 1;
  END

  /* POSITION_STATUS */
  IF (OLD.position_status IS DISTINCT FROM NEW.position_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1890, OLD.position_status);
    changes = changes + 1;
  END

  /* CO_HR_PERFORMANCE_RATINGS_NBR */
  IF (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1839, OLD.co_hr_performance_ratings_nbr);
    changes = changes + 1;
  END

  /* REVIEW_DATE */
  IF (OLD.review_date IS DISTINCT FROM NEW.review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1852, OLD.review_date);
    changes = changes + 1;
  END

  /* NEXT_REVIEW_DATE */
  IF (OLD.next_review_date IS DISTINCT FROM NEW.next_review_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1853, OLD.next_review_date);
    changes = changes + 1;
  END

  /* PAY_FREQUENCY */
  IF (OLD.pay_frequency IS DISTINCT FROM NEW.pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1891, OLD.pay_frequency);
    changes = changes + 1;
  END

  /* NEXT_RAISE_DATE */
  IF (OLD.next_raise_date IS DISTINCT FROM NEW.next_raise_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1854, OLD.next_raise_date);
    changes = changes + 1;
  END

  /* NEXT_PAY_FREQUENCY */
  IF (OLD.next_pay_frequency IS DISTINCT FROM NEW.next_pay_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1892, OLD.next_pay_frequency);
    changes = changes + 1;
  END

  /* TIPPED_DIRECTLY */
  IF (OLD.tipped_directly IS DISTINCT FROM NEW.tipped_directly) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1893, OLD.tipped_directly);
    changes = changes + 1;
  END

  /* ALD_CL_E_D_GROUPS_NBR */
  IF (OLD.ald_cl_e_d_groups_nbr IS DISTINCT FROM NEW.ald_cl_e_d_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1840, OLD.ald_cl_e_d_groups_nbr);
    changes = changes + 1;
  END

  /* DISTRIBUTE_TAXES */
  IF (OLD.distribute_taxes IS DISTINCT FROM NEW.distribute_taxes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1894, OLD.distribute_taxes);
    changes = changes + 1;
  END

  /* CO_PAY_GROUP_NBR */
  IF (OLD.co_pay_group_nbr IS DISTINCT FROM NEW.co_pay_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1841, OLD.co_pay_group_nbr);
    changes = changes + 1;
  END

  /* CL_DELIVERY_GROUP_NBR */
  IF (OLD.cl_delivery_group_nbr IS DISTINCT FROM NEW.cl_delivery_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1842, OLD.cl_delivery_group_nbr);
    changes = changes + 1;
  END

  /* CO_UNIONS_NBR */
  IF (OLD.co_unions_nbr IS DISTINCT FROM NEW.co_unions_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1843, OLD.co_unions_nbr);
    changes = changes + 1;
  END

  /* EIC */
  IF (OLD.eic IS DISTINCT FROM NEW.eic) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1895, OLD.eic);
    changes = changes + 1;
  END

  /* FLSA_EXEMPT */
  IF (OLD.flsa_exempt IS DISTINCT FROM NEW.flsa_exempt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1896, OLD.flsa_exempt);
    changes = changes + 1;
  END

  /* W2_TYPE */
  IF (OLD.w2_type IS DISTINCT FROM NEW.w2_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1897, OLD.w2_type);
    changes = changes + 1;
  END

  /* W2_PENSION */
  IF (OLD.w2_pension IS DISTINCT FROM NEW.w2_pension) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1898, OLD.w2_pension);
    changes = changes + 1;
  END

  /* W2_DEFERRED_COMP */
  IF (OLD.w2_deferred_comp IS DISTINCT FROM NEW.w2_deferred_comp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1899, OLD.w2_deferred_comp);
    changes = changes + 1;
  END

  /* W2_DECEASED */
  IF (OLD.w2_deceased IS DISTINCT FROM NEW.w2_deceased) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1900, OLD.w2_deceased);
    changes = changes + 1;
  END

  /* W2_STATUTORY_EMPLOYEE */
  IF (OLD.w2_statutory_employee IS DISTINCT FROM NEW.w2_statutory_employee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1901, OLD.w2_statutory_employee);
    changes = changes + 1;
  END

  /* W2_LEGAL_REP */
  IF (OLD.w2_legal_rep IS DISTINCT FROM NEW.w2_legal_rep) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1902, OLD.w2_legal_rep);
    changes = changes + 1;
  END

  /* HOME_TAX_EE_STATES_NBR */
  IF (OLD.home_tax_ee_states_nbr IS DISTINCT FROM NEW.home_tax_ee_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1844, OLD.home_tax_ee_states_nbr);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_OASDI */
  IF (OLD.exempt_employee_oasdi IS DISTINCT FROM NEW.exempt_employee_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1903, OLD.exempt_employee_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYEE_MEDICARE */
  IF (OLD.exempt_employee_medicare IS DISTINCT FROM NEW.exempt_employee_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1904, OLD.exempt_employee_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EXCLUDE_EE_FED */
  IF (OLD.exempt_exclude_ee_fed IS DISTINCT FROM NEW.exempt_exclude_ee_fed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1905, OLD.exempt_exclude_ee_fed);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_OASDI */
  IF (OLD.exempt_employer_oasdi IS DISTINCT FROM NEW.exempt_employer_oasdi) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1906, OLD.exempt_employer_oasdi);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_MEDICARE */
  IF (OLD.exempt_employer_medicare IS DISTINCT FROM NEW.exempt_employer_medicare) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1907, OLD.exempt_employer_medicare);
    changes = changes + 1;
  END

  /* EXEMPT_EMPLOYER_FUI */
  IF (OLD.exempt_employer_fui IS DISTINCT FROM NEW.exempt_employer_fui) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1908, OLD.exempt_employer_fui);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_TYPE */
  IF (OLD.override_fed_tax_type IS DISTINCT FROM NEW.override_fed_tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1909, OLD.override_fed_tax_type);
    changes = changes + 1;
  END

  /* GOV_GARNISH_PRIOR_CHILD_SUPPT */
  IF (OLD.gov_garnish_prior_child_suppt IS DISTINCT FROM NEW.gov_garnish_prior_child_suppt) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1910, OLD.gov_garnish_prior_child_suppt);
    changes = changes + 1;
  END

  /* SECURITY_CLEARANCE */
  IF (OLD.security_clearance IS DISTINCT FROM NEW.security_clearance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1875, OLD.security_clearance);
    changes = changes + 1;
  END

  /* BADGE_ID */
  IF (OLD.badge_id IS DISTINCT FROM NEW.badge_id) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1911, OLD.badge_id);
    changes = changes + 1;
  END

  /* ON_CALL_FROM */
  IF (OLD.on_call_from IS DISTINCT FROM NEW.on_call_from) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1813, OLD.on_call_from);
    changes = changes + 1;
  END

  /* ON_CALL_TO */
  IF (OLD.on_call_to IS DISTINCT FROM NEW.on_call_to) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1814, OLD.on_call_to);
    changes = changes + 1;
  END

  /* BASE_RETURNS_ON_THIS_EE */
  IF (OLD.base_returns_on_this_ee IS DISTINCT FROM NEW.base_returns_on_this_ee) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1912, OLD.base_returns_on_this_ee);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1815, :blob_nbr);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1876, OLD.filler);
    changes = changes + 1;
  END

  /* GENERAL_LEDGER_TAG */
  IF (OLD.general_ledger_tag IS DISTINCT FROM NEW.general_ledger_tag) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1877, OLD.general_ledger_tag);
    changes = changes + 1;
  END

  /* COMPANY_OR_INDIVIDUAL_NAME */
  IF (OLD.company_or_individual_name IS DISTINCT FROM NEW.company_or_individual_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1913, OLD.company_or_individual_name);
    changes = changes + 1;
  END

  /* FEDERAL_MARITAL_STATUS */
  IF (OLD.federal_marital_status IS DISTINCT FROM NEW.federal_marital_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1914, OLD.federal_marital_status);
    changes = changes + 1;
  END

  /* NUMBER_OF_DEPENDENTS */
  IF (OLD.number_of_dependents IS DISTINCT FROM NEW.number_of_dependents) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1846, OLD.number_of_dependents);
    changes = changes + 1;
  END

  /* DISTRIBUTION_CODE_1099R */
  IF (OLD.distribution_code_1099r IS DISTINCT FROM NEW.distribution_code_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1878, OLD.distribution_code_1099r);
    changes = changes + 1;
  END

  /* TAX_AMT_DETERMINED_1099R */
  IF (OLD.tax_amt_determined_1099r IS DISTINCT FROM NEW.tax_amt_determined_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1915, OLD.tax_amt_determined_1099r);
    changes = changes + 1;
  END

  /* TOTAL_DISTRIBUTION_1099R */
  IF (OLD.total_distribution_1099r IS DISTINCT FROM NEW.total_distribution_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1916, OLD.total_distribution_1099r);
    changes = changes + 1;
  END

  /* PENSION_PLAN_1099R */
  IF (OLD.pension_plan_1099r IS DISTINCT FROM NEW.pension_plan_1099r) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1917, OLD.pension_plan_1099r);
    changes = changes + 1;
  END

  /* MAKEUP_FICA_ON_CLEANUP_PR */
  IF (OLD.makeup_fica_on_cleanup_pr IS DISTINCT FROM NEW.makeup_fica_on_cleanup_pr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1918, OLD.makeup_fica_on_cleanup_pr);
    changes = changes + 1;
  END

  /* SY_HR_EEO_NBR */
  IF (OLD.sy_hr_eeo_nbr IS DISTINCT FROM NEW.sy_hr_eeo_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1847, OLD.sy_hr_eeo_nbr);
    changes = changes + 1;
  END

  /* SECONDARY_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SECONDARY_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SECONDARY_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1816, :blob_nbr);
    changes = changes + 1;
  END

  /* CO_PR_CHECK_TEMPLATES_NBR */
  IF (OLD.co_pr_check_templates_nbr IS DISTINCT FROM NEW.co_pr_check_templates_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1820, OLD.co_pr_check_templates_nbr);
    changes = changes + 1;
  END

  /* GENERATE_SECOND_CHECK */
  IF (OLD.generate_second_check IS DISTINCT FROM NEW.generate_second_check) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1884, OLD.generate_second_check);
    changes = changes + 1;
  END

  /* SALARY_AMOUNT */
  IF (OLD.salary_amount IS DISTINCT FROM NEW.salary_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1809, OLD.salary_amount);
    changes = changes + 1;
  END

  /* STANDARD_HOURS */
  IF (OLD.standard_hours IS DISTINCT FROM NEW.standard_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1808, OLD.standard_hours);
    changes = changes + 1;
  END

  /* NEXT_RAISE_AMOUNT */
  IF (OLD.next_raise_amount IS DISTINCT FROM NEW.next_raise_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1807, OLD.next_raise_amount);
    changes = changes + 1;
  END

  /* NEXT_RAISE_PERCENTAGE */
  IF (OLD.next_raise_percentage IS DISTINCT FROM NEW.next_raise_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1806, OLD.next_raise_percentage);
    changes = changes + 1;
  END

  /* NEXT_RAISE_RATE */
  IF (OLD.next_raise_rate IS DISTINCT FROM NEW.next_raise_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1805, OLD.next_raise_rate);
    changes = changes + 1;
  END

  /* WORKERS_COMP_WAGE_LIMIT */
  IF (OLD.workers_comp_wage_limit IS DISTINCT FROM NEW.workers_comp_wage_limit) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1804, OLD.workers_comp_wage_limit);
    changes = changes + 1;
  END

  /* GROUP_TERM_POLICY_AMOUNT */
  IF (OLD.group_term_policy_amount IS DISTINCT FROM NEW.group_term_policy_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1803, OLD.group_term_policy_amount);
    changes = changes + 1;
  END

  /* OVERRIDE_FED_TAX_VALUE */
  IF (OLD.override_fed_tax_value IS DISTINCT FROM NEW.override_fed_tax_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1802, OLD.override_fed_tax_value);
    changes = changes + 1;
  END

  /* CALCULATED_SALARY */
  IF (OLD.calculated_salary IS DISTINCT FROM NEW.calculated_salary) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1801, OLD.calculated_salary);
    changes = changes + 1;
  END

  /* PR_CHECK_MB_GROUP_NBR */
  IF (OLD.pr_check_mb_group_nbr IS DISTINCT FROM NEW.pr_check_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1855, OLD.pr_check_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1856, OLD.pr_ee_report_mb_group_nbr);
    changes = changes + 1;
  END

  /* PR_EE_REPORT_SEC_MB_GROUP_NBR */
  IF (OLD.pr_ee_report_sec_mb_group_nbr IS DISTINCT FROM NEW.pr_ee_report_sec_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1857, OLD.pr_ee_report_sec_mb_group_nbr);
    changes = changes + 1;
  END

  /* TAX_EE_RETURN_MB_GROUP_NBR */
  IF (OLD.tax_ee_return_mb_group_nbr IS DISTINCT FROM NEW.tax_ee_return_mb_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1858, OLD.tax_ee_return_mb_group_nbr);
    changes = changes + 1;
  END

  /* HIGHLY_COMPENSATED */
  IF (OLD.highly_compensated IS DISTINCT FROM NEW.highly_compensated) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1919, OLD.highly_compensated);
    changes = changes + 1;
  END

  /* CORPORATE_OFFICER */
  IF (OLD.corporate_officer IS DISTINCT FROM NEW.corporate_officer) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1920, OLD.corporate_officer);
    changes = changes + 1;
  END

  /* CO_JOBS_NBR */
  IF (OLD.co_jobs_nbr IS DISTINCT FROM NEW.co_jobs_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1859, OLD.co_jobs_nbr);
    changes = changes + 1;
  END

  /* CO_WORKERS_COMP_NBR */
  IF (OLD.co_workers_comp_nbr IS DISTINCT FROM NEW.co_workers_comp_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1860, OLD.co_workers_comp_nbr);
    changes = changes + 1;
  END

  /* EE_ENABLED */
  IF (OLD.ee_enabled IS DISTINCT FROM NEW.ee_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1921, OLD.ee_enabled);
    changes = changes + 1;
  END

  /* GTL_NUMBER_OF_HOURS */
  IF (OLD.gtl_number_of_hours IS DISTINCT FROM NEW.gtl_number_of_hours) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1861, OLD.gtl_number_of_hours);
    changes = changes + 1;
  END

  /* GTL_RATE */
  IF (OLD.gtl_rate IS DISTINCT FROM NEW.gtl_rate) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1817, OLD.gtl_rate);
    changes = changes + 1;
  END

  /* AUTO_UPDATE_RATES */
  IF (OLD.auto_update_rates IS DISTINCT FROM NEW.auto_update_rates) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1922, OLD.auto_update_rates);
    changes = changes + 1;
  END

  /* ELIGIBLE_FOR_REHIRE */
  IF (OLD.eligible_for_rehire IS DISTINCT FROM NEW.eligible_for_rehire) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1923, OLD.eligible_for_rehire);
    changes = changes + 1;
  END

  /* SELFSERVE_ENABLED */
  IF (OLD.selfserve_enabled IS DISTINCT FROM NEW.selfserve_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1883, OLD.selfserve_enabled);
    changes = changes + 1;
  END

  /* SELFSERVE_USERNAME */
  IF (OLD.selfserve_username IS DISTINCT FROM NEW.selfserve_username) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1868, OLD.selfserve_username);
    changes = changes + 1;
  END

  /* SELFSERVE_PASSWORD */
  IF (OLD.selfserve_password IS DISTINCT FROM NEW.selfserve_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1867, OLD.selfserve_password);
    changes = changes + 1;
  END

  /* SELFSERVE_LAST_LOGIN */
  IF (OLD.selfserve_last_login IS DISTINCT FROM NEW.selfserve_last_login) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1800, OLD.selfserve_last_login);
    changes = changes + 1;
  END

  /* PRINT_VOUCHER */
  IF (OLD.print_voucher IS DISTINCT FROM NEW.print_voucher) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1882, OLD.print_voucher);
    changes = changes + 1;
  END

  /* HEALTHCARE_COVERAGE */
  IF (OLD.healthcare_coverage IS DISTINCT FROM NEW.healthcare_coverage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1924, OLD.healthcare_coverage);
    changes = changes + 1;
  END

  /* FUI_RATE_CREDIT_OVERRIDE */
  IF (OLD.fui_rate_credit_override IS DISTINCT FROM NEW.fui_rate_credit_override) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1818, OLD.fui_rate_credit_override);
    changes = changes + 1;
  END

  /* OVERRIDE_FEDERAL_MINIMUM_WAGE */
  IF (OLD.override_federal_minimum_wage IS DISTINCT FROM NEW.override_federal_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1819, OLD.override_federal_minimum_wage);
    changes = changes + 1;
  END

  /* WC_WAGE_LIMIT_FREQUENCY */
  IF (OLD.wc_wage_limit_frequency IS DISTINCT FROM NEW.wc_wage_limit_frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1925, OLD.wc_wage_limit_frequency);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION1 */
  IF (OLD.login_question1 IS DISTINCT FROM NEW.login_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1864, OLD.login_question1);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER1 */
  IF (OLD.login_answer1 IS DISTINCT FROM NEW.login_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1879, OLD.login_answer1);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION2 */
  IF (OLD.login_question2 IS DISTINCT FROM NEW.login_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1865, OLD.login_question2);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER2 */
  IF (OLD.login_answer2 IS DISTINCT FROM NEW.login_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1880, OLD.login_answer2);
    changes = changes + 1;
  END

  /* LOGIN_QUESTION3 */
  IF (OLD.login_question3 IS DISTINCT FROM NEW.login_question3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1866, OLD.login_question3);
    changes = changes + 1;
  END

  /* LOGIN_ANSWER3 */
  IF (OLD.login_answer3 IS DISTINCT FROM NEW.login_answer3) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1881, OLD.login_answer3);
    changes = changes + 1;
  END

  /* LOGIN_DATE */
  IF (OLD.login_date IS DISTINCT FROM NEW.login_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2785, OLD.login_date);
    changes = changes + 1;
  END

  /* LOGIN_ATTEMPTS */
  IF (OLD.login_attempts IS DISTINCT FROM NEW.login_attempts) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2786, OLD.login_attempts);
    changes = changes + 1;
  END

  /* CO_BENEFIT_DISCOUNT_NBR */
  IF (OLD.co_benefit_discount_nbr IS DISTINCT FROM NEW.co_benefit_discount_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2912, OLD.co_benefit_discount_nbr);
    changes = changes + 1;
  END

  /* BENEFITS_ENABLED */
  IF (OLD.benefits_enabled IS DISTINCT FROM NEW.benefits_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2913, OLD.benefits_enabled);
    changes = changes + 1;
  END

  /* EXISTING_PATIENT */
  IF (OLD.existing_patient IS DISTINCT FROM NEW.existing_patient) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2914, OLD.existing_patient);
    changes = changes + 1;
  END

  /* PCP */
  IF (OLD.pcp IS DISTINCT FROM NEW.pcp) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2915, OLD.pcp);
    changes = changes + 1;
  END

  /* TIME_OFF_ENABLED */
  IF (OLD.time_off_enabled IS DISTINCT FROM NEW.time_off_enabled) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2916, OLD.time_off_enabled);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAILABLE */
  IF (OLD.dependent_benefits_available IS DISTINCT FROM NEW.dependent_benefits_available) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2917, OLD.dependent_benefits_available);
    changes = changes + 1;
  END

  /* DEPENDENT_BENEFITS_AVAIL_DATE */
  IF (OLD.dependent_benefits_avail_date IS DISTINCT FROM NEW.dependent_benefits_avail_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2918, OLD.dependent_benefits_avail_date);
    changes = changes + 1;
  END

  /* BENEFIT_ENROLLMENT_COMPLETE */
  IF (OLD.benefit_enrollment_complete IS DISTINCT FROM NEW.benefit_enrollment_complete) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2919, OLD.benefit_enrollment_complete);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT */
  IF (OLD.last_qual_benefit_event IS DISTINCT FROM NEW.last_qual_benefit_event) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2920, OLD.last_qual_benefit_event);
    changes = changes + 1;
  END

  /* LAST_QUAL_BENEFIT_EVENT_DATE */
  IF (OLD.last_qual_benefit_event_date IS DISTINCT FROM NEW.last_qual_benefit_event_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2921, OLD.last_qual_benefit_event_date);
    changes = changes + 1;
  END

  /* W2 */
  IF (OLD.w2 IS DISTINCT FROM NEW.w2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2922, OLD.w2);
    changes = changes + 1;
  END

  /* W2_FORM_ON_FILE */
  IF (OLD.w2_form_on_file IS DISTINCT FROM NEW.w2_form_on_file) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2923, OLD.w2_form_on_file);
    changes = changes + 1;
  END

  /* BENEFIT_E_MAIL_ADDRESS */
  IF (OLD.benefit_e_mail_address IS DISTINCT FROM NEW.benefit_e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2924, OLD.benefit_e_mail_address);
    changes = changes + 1;
  END

  /* CO_HR_POSITION_GRADES_NBR */
  IF (OLD.co_hr_position_grades_nbr IS DISTINCT FROM NEW.co_hr_position_grades_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2925, OLD.co_hr_position_grades_nbr);
    changes = changes + 1;
  END

  /* SEC_QUESTION1 */
  IF (OLD.sec_question1 IS DISTINCT FROM NEW.sec_question1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2926, OLD.sec_question1);
    changes = changes + 1;
  END

  /* SEC_ANSWER1 */
  IF (OLD.sec_answer1 IS DISTINCT FROM NEW.sec_answer1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2927, OLD.sec_answer1);
    changes = changes + 1;
  END

  /* SEC_QUESTION2 */
  IF (OLD.sec_question2 IS DISTINCT FROM NEW.sec_question2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2928, OLD.sec_question2);
    changes = changes + 1;
  END

  /* SEC_ANSWER2 */
  IF (OLD.sec_answer2 IS DISTINCT FROM NEW.sec_answer2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2929, OLD.sec_answer2);
    changes = changes + 1;
  END

  /* ACA_STATUS */
  IF (OLD.aca_status IS DISTINCT FROM NEW.aca_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3188, OLD.aca_status);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_EE_HR_PERFORMANCE_RATIN_3 FOR EE_HR_PERFORMANCE_RATINGS Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_EE_HR_PERFORMANCE_RATINGS */
  IF (INSERTING OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) OR (OLD.co_hr_performance_ratings_nbr IS DISTINCT FROM NEW.co_hr_performance_ratings_nbr) OR (OLD.review_date IS DISTINCT FROM NEW.review_date)) THEN
    IF (EXISTS(SELECT 1 FROM ee_hr_performance_ratings WHERE ee_nbr = NEW.ee_nbr AND co_hr_performance_ratings_nbr = NEW.co_hr_performance_ratings_nbr AND review_date = NEW.review_date AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('ee_hr_performance_ratings', 'ee_nbr, co_hr_performance_ratings_nbr, review_date',
      CAST(NEW.ee_nbr || ', ' || NEW.co_hr_performance_ratings_nbr || ', ' || NEW.review_date as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BIUD_PR_BATCH_2 FOR PR_BATCH Before Insert or Update or Delete POSITION 2
AS
BEGIN         
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;
                    
  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_CHECK_2 FOR PR_CHECK Before Insert or Update or Delete POSITION 2
AS
BEGIN           
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_CHECK_LINES_2 FOR PR_CHECK_LINES Before Insert or Update or Delete POSITION 2
AS
BEGIN                   
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (UPDATING AND rdb$get_context('USER_TRANSACTION', 'DETACH_MISC_CHECKS') = 1) THEN
    EXIT;
  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_CHECK_LINE_LOCALS_2 FOR PR_CHECK_LINE_LOCALS Before Insert or Update or Delete POSITION 2
AS             
DECLARE pr_nbr INTEGER;
BEGIN                 
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;
         
  SELECT pr_nbr FROM pr_check_lines WHERE pr_check_lines_nbr = NEW.pr_check_lines_nbr INTO :pr_nbr;
  EXECUTE PROCEDURE check_processed_pr(pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_CHECK_LOCALS_2 FOR PR_CHECK_LOCALS Before Insert or Update or Delete POSITION 2
AS
BEGIN                 
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_CHECK_STATES_2 FOR PR_CHECK_STATES Before Insert or Update or Delete POSITION 2
AS
BEGIN                              
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_CHECK_SUI_2 FOR PR_CHECK_SUI Before Insert or Update or Delete POSITION 2
AS
BEGIN               
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_BIUD_PR_MISCELLANEOUS_CHECK_2 FOR PR_MISCELLANEOUS_CHECKS Before Insert or Update or Delete POSITION 2
AS
BEGIN                           
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  EXECUTE PROCEDURE check_processed_pr(NEW.pr_nbr);
END
^

CREATE TRIGGER T_AD_EE_STATES_9 FOR EE_STATES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE state_exempt_exclude CHAR(1);
DECLARE VARIABLE sdi_apply_co_states_nbr INTEGER;
DECLARE VARIABLE sui_apply_co_states_nbr INTEGER;
DECLARE VARIABLE ee_sdi_exempt_exclude CHAR(1);
DECLARE VARIABLE er_sdi_exempt_exclude CHAR(1);
DECLARE VARIABLE ee_sui_exempt_exclude CHAR(1);
DECLARE VARIABLE er_sui_exempt_exclude CHAR(1);
DECLARE VARIABLE reciprocal_co_states_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(135, OLD.rec_version, OLD.ee_states_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', state_exempt_exclude, sdi_apply_co_states_nbr, sui_apply_co_states_nbr, ee_sdi_exempt_exclude, er_sdi_exempt_exclude, ee_sui_exempt_exclude, er_sui_exempt_exclude, reciprocal_co_states_nbr  FROM ee_states WHERE ee_states_nbr = OLD.ee_states_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :state_exempt_exclude, :sdi_apply_co_states_nbr, :sui_apply_co_states_nbr, :ee_sdi_exempt_exclude, :er_sdi_exempt_exclude, :ee_sui_exempt_exclude, :er_sui_exempt_exclude, :reciprocal_co_states_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2205, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2826, OLD.effective_until);

  /* STATE_EXEMPT_EXCLUDE */
  IF (last_record = 'Y' OR state_exempt_exclude IS DISTINCT FROM OLD.state_exempt_exclude) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2219, OLD.state_exempt_exclude);

  /* SDI_APPLY_CO_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.sdi_apply_co_states_nbr IS NOT NULL) OR (last_record = 'N' AND sdi_apply_co_states_nbr IS DISTINCT FROM OLD.sdi_apply_co_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2212, OLD.sdi_apply_co_states_nbr);

  /* SUI_APPLY_CO_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.sui_apply_co_states_nbr IS NOT NULL) OR (last_record = 'N' AND sui_apply_co_states_nbr IS DISTINCT FROM OLD.sui_apply_co_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2213, OLD.sui_apply_co_states_nbr);

  /* EE_SDI_EXEMPT_EXCLUDE */
  IF (last_record = 'Y' OR ee_sdi_exempt_exclude IS DISTINCT FROM OLD.ee_sdi_exempt_exclude) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2221, OLD.ee_sdi_exempt_exclude);

  /* ER_SDI_EXEMPT_EXCLUDE */
  IF (last_record = 'Y' OR er_sdi_exempt_exclude IS DISTINCT FROM OLD.er_sdi_exempt_exclude) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2222, OLD.er_sdi_exempt_exclude);

  /* EE_SUI_EXEMPT_EXCLUDE */
  IF (last_record = 'Y' OR ee_sui_exempt_exclude IS DISTINCT FROM OLD.ee_sui_exempt_exclude) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2223, OLD.ee_sui_exempt_exclude);

  /* ER_SUI_EXEMPT_EXCLUDE */
  IF (last_record = 'Y' OR er_sui_exempt_exclude IS DISTINCT FROM OLD.er_sui_exempt_exclude) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2224, OLD.er_sui_exempt_exclude);

  /* RECIPROCAL_CO_STATES_NBR */
  IF ((last_record = 'Y' AND OLD.reciprocal_co_states_nbr IS NOT NULL) OR (last_record = 'N' AND reciprocal_co_states_nbr IS DISTINCT FROM OLD.reciprocal_co_states_nbr)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2214, OLD.reciprocal_co_states_nbr);


  IF (last_record = 'Y') THEN
  BEGIN
    /* EE_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2209, OLD.ee_nbr);

    /* CO_STATES_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2210, OLD.co_states_nbr);

    /* STATE_NUMBER_WITHHOLDING_ALLOW */
    IF (OLD.state_number_withholding_allow IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2211, OLD.state_number_withholding_allow);

    /* OVERRIDE_STATE_TAX_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2220, OLD.override_state_tax_type);

    /* RECIPROCAL_METHOD */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2225, OLD.reciprocal_method);

    /* FILLER */
    IF (OLD.filler IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2216, OLD.filler);

    /* EE_COUNTY */
    IF (OLD.ee_county IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2217, OLD.ee_county);

    /* EE_TAX_CODE */
    IF (OLD.ee_tax_code IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2218, OLD.ee_tax_code);

    /* IMPORTED_MARITAL_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2226, OLD.imported_marital_status);

    /* CALCULATE_TAXABLE_WAGES_1099 */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2227, OLD.calculate_taxable_wages_1099);

    /* OVERRIDE_STATE_TAX_VALUE */
    IF (OLD.override_state_tax_value IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2204, OLD.override_state_tax_value);

    /* RECIPROCAL_AMOUNT_PERCENTAGE */
    IF (OLD.reciprocal_amount_percentage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2203, OLD.reciprocal_amount_percentage);

    /* SY_STATE_MARITAL_STATUS_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2207, OLD.sy_state_marital_status_nbr);

    /* STATE_MARITAL_STATUS */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2215, OLD.state_marital_status);

    /* OVERRIDE_STATE_MINIMUM_WAGE */
    IF (OLD.override_state_minimum_wage IS NOT NULL) THEN
      INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2206, OLD.override_state_minimum_wage);

    /* SALARY_TYPE */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2865, OLD.salary_type);

  END
END

^

CREATE TRIGGER T_AIU_EE_STATES_3 FOR EE_STATES After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) OR (OLD.co_states_nbr IS DISTINCT FROM NEW.co_states_nbr) OR (OLD.state_number_withholding_allow IS DISTINCT FROM NEW.state_number_withholding_allow) OR (OLD.override_state_tax_type IS DISTINCT FROM NEW.override_state_tax_type) OR (OLD.reciprocal_method IS DISTINCT FROM NEW.reciprocal_method) OR (OLD.filler IS DISTINCT FROM NEW.filler) OR (OLD.ee_county IS DISTINCT FROM NEW.ee_county) OR (OLD.ee_tax_code IS DISTINCT FROM NEW.ee_tax_code) OR (OLD.imported_marital_status IS DISTINCT FROM NEW.imported_marital_status) OR (OLD.calculate_taxable_wages_1099 IS DISTINCT FROM NEW.calculate_taxable_wages_1099) OR (OLD.override_state_tax_value IS DISTINCT FROM NEW.override_state_tax_value) OR (OLD.reciprocal_amount_percentage IS DISTINCT FROM NEW.reciprocal_amount_percentage) OR (OLD.sy_state_marital_status_nbr IS DISTINCT FROM NEW.sy_state_marital_status_nbr) OR (OLD.state_marital_status IS DISTINCT FROM NEW.state_marital_status) OR (OLD.override_state_minimum_wage IS DISTINCT FROM NEW.override_state_minimum_wage) OR (OLD.salary_type IS DISTINCT FROM NEW.salary_type)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE ee_states SET 
      ee_nbr = NEW.ee_nbr, co_states_nbr = NEW.co_states_nbr, state_number_withholding_allow = NEW.state_number_withholding_allow, override_state_tax_type = NEW.override_state_tax_type, reciprocal_method = NEW.reciprocal_method, filler = NEW.filler, ee_county = NEW.ee_county, ee_tax_code = NEW.ee_tax_code, imported_marital_status = NEW.imported_marital_status, calculate_taxable_wages_1099 = NEW.calculate_taxable_wages_1099, override_state_tax_value = NEW.override_state_tax_value, reciprocal_amount_percentage = NEW.reciprocal_amount_percentage, sy_state_marital_status_nbr = NEW.sy_state_marital_status_nbr, state_marital_status = NEW.state_marital_status, override_state_minimum_wage = NEW.override_state_minimum_wage, salary_type = NEW.salary_type
      WHERE ee_states_nbr = NEW.ee_states_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AU_EE_STATES_9 FOR EE_STATES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(135, NEW.rec_version, NEW.ee_states_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2205, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2826, OLD.effective_until);
    changes = changes + 1;
  END

  /* EE_NBR */
  IF (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2209, OLD.ee_nbr);
    changes = changes + 1;
  END

  /* CO_STATES_NBR */
  IF (OLD.co_states_nbr IS DISTINCT FROM NEW.co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2210, OLD.co_states_nbr);
    changes = changes + 1;
  END

  /* STATE_NUMBER_WITHHOLDING_ALLOW */
  IF (OLD.state_number_withholding_allow IS DISTINCT FROM NEW.state_number_withholding_allow) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2211, OLD.state_number_withholding_allow);
    changes = changes + 1;
  END

  /* STATE_EXEMPT_EXCLUDE */
  IF (OLD.state_exempt_exclude IS DISTINCT FROM NEW.state_exempt_exclude) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2219, OLD.state_exempt_exclude);
    changes = changes + 1;
  END

  /* OVERRIDE_STATE_TAX_TYPE */
  IF (OLD.override_state_tax_type IS DISTINCT FROM NEW.override_state_tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2220, OLD.override_state_tax_type);
    changes = changes + 1;
  END

  /* SDI_APPLY_CO_STATES_NBR */
  IF (OLD.sdi_apply_co_states_nbr IS DISTINCT FROM NEW.sdi_apply_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2212, OLD.sdi_apply_co_states_nbr);
    changes = changes + 1;
  END

  /* SUI_APPLY_CO_STATES_NBR */
  IF (OLD.sui_apply_co_states_nbr IS DISTINCT FROM NEW.sui_apply_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2213, OLD.sui_apply_co_states_nbr);
    changes = changes + 1;
  END

  /* EE_SDI_EXEMPT_EXCLUDE */
  IF (OLD.ee_sdi_exempt_exclude IS DISTINCT FROM NEW.ee_sdi_exempt_exclude) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2221, OLD.ee_sdi_exempt_exclude);
    changes = changes + 1;
  END

  /* ER_SDI_EXEMPT_EXCLUDE */
  IF (OLD.er_sdi_exempt_exclude IS DISTINCT FROM NEW.er_sdi_exempt_exclude) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2222, OLD.er_sdi_exempt_exclude);
    changes = changes + 1;
  END

  /* EE_SUI_EXEMPT_EXCLUDE */
  IF (OLD.ee_sui_exempt_exclude IS DISTINCT FROM NEW.ee_sui_exempt_exclude) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2223, OLD.ee_sui_exempt_exclude);
    changes = changes + 1;
  END

  /* ER_SUI_EXEMPT_EXCLUDE */
  IF (OLD.er_sui_exempt_exclude IS DISTINCT FROM NEW.er_sui_exempt_exclude) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2224, OLD.er_sui_exempt_exclude);
    changes = changes + 1;
  END

  /* RECIPROCAL_METHOD */
  IF (OLD.reciprocal_method IS DISTINCT FROM NEW.reciprocal_method) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2225, OLD.reciprocal_method);
    changes = changes + 1;
  END

  /* RECIPROCAL_CO_STATES_NBR */
  IF (OLD.reciprocal_co_states_nbr IS DISTINCT FROM NEW.reciprocal_co_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2214, OLD.reciprocal_co_states_nbr);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2216, OLD.filler);
    changes = changes + 1;
  END

  /* EE_COUNTY */
  IF (OLD.ee_county IS DISTINCT FROM NEW.ee_county) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2217, OLD.ee_county);
    changes = changes + 1;
  END

  /* EE_TAX_CODE */
  IF (OLD.ee_tax_code IS DISTINCT FROM NEW.ee_tax_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2218, OLD.ee_tax_code);
    changes = changes + 1;
  END

  /* IMPORTED_MARITAL_STATUS */
  IF (OLD.imported_marital_status IS DISTINCT FROM NEW.imported_marital_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2226, OLD.imported_marital_status);
    changes = changes + 1;
  END

  /* CALCULATE_TAXABLE_WAGES_1099 */
  IF (OLD.calculate_taxable_wages_1099 IS DISTINCT FROM NEW.calculate_taxable_wages_1099) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2227, OLD.calculate_taxable_wages_1099);
    changes = changes + 1;
  END

  /* OVERRIDE_STATE_TAX_VALUE */
  IF (OLD.override_state_tax_value IS DISTINCT FROM NEW.override_state_tax_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2204, OLD.override_state_tax_value);
    changes = changes + 1;
  END

  /* RECIPROCAL_AMOUNT_PERCENTAGE */
  IF (OLD.reciprocal_amount_percentage IS DISTINCT FROM NEW.reciprocal_amount_percentage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2203, OLD.reciprocal_amount_percentage);
    changes = changes + 1;
  END

  /* SY_STATE_MARITAL_STATUS_NBR */
  IF (OLD.sy_state_marital_status_nbr IS DISTINCT FROM NEW.sy_state_marital_status_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2207, OLD.sy_state_marital_status_nbr);
    changes = changes + 1;
  END

  /* STATE_MARITAL_STATUS */
  IF (OLD.state_marital_status IS DISTINCT FROM NEW.state_marital_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2215, OLD.state_marital_status);
    changes = changes + 1;
  END

  /* OVERRIDE_STATE_MINIMUM_WAGE */
  IF (OLD.override_state_minimum_wage IS DISTINCT FROM NEW.override_state_minimum_wage) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2206, OLD.override_state_minimum_wage);
    changes = changes + 1;
  END

  /* SALARY_TYPE */
  IF (OLD.salary_type IS DISTINCT FROM NEW.salary_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2865, OLD.salary_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END
^


/* Delete removed fields of CO_SUI */
EXECUTE BLOCK
AS
DECLARE nbr INTEGER;
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  /* RATE_LAST_REVIEWED_DATE */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3190;
  DELETE FROM ev_field WHERE nbr = 3190;

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

/* Delete removed fields of EE */
EXECUTE BLOCK
AS
DECLARE nbr INTEGER;
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  /* WORKERS_COMP_MIN_WAGE_LIMIT */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3189;
  DELETE FROM ev_field WHERE nbr = 3189;

  /* ESS_TERMS_OF_USE_ACCEPT_DATE */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3192;
  DELETE FROM ev_field WHERE nbr = 3192;

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

/* Delete removed fields of CO_BENEFITS */
EXECUTE BLOCK
AS
DECLARE nbr INTEGER;
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  /* TAXABLE_FRINGE */
  DELETE FROM ev_field_change WHERE ev_field_nbr = 3191;
  DELETE FROM ev_field WHERE nbr = 3191;

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  UPDATE ev_field SET name = 'CL_CO_CONSOLIDATION_NBR', field_type = 'I', len = NULL, scale = NULL, required = 'N', versioned = 'N' WHERE nbr = 531;
  UPDATE ev_field SET name = 'CL_COMMON_PAYMASTER_NBR', field_type = 'I', len = NULL, scale = NULL, required = 'N', versioned = 'N' WHERE nbr = 530;
  UPDATE ev_field SET name = 'STATE_MARITAL_STATUS', field_type = 'C', len = 2, scale = NULL, required = 'Y', versioned = 'N' WHERE nbr = 2215;
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.15', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
