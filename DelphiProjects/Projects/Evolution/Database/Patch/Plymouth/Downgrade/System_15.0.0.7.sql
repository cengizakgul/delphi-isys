/* Check current version */

SET TERM ^;

EXECUTE block
AS
DECLARE VARIABLE req_ver VARCHAR(11);
DECLARE VARIABLE ver VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    EXECUTE STATEMENT '"This script must be executed by SYSDBA"';

  req_ver = '15.0.0.8';
  ver = 'UNKNOWN';
  SELECT VERSION
  FROM ev_database
  INTO :ver;

  IF (strcopy(ver, 0, strlen(req_ver)) <> req_ver) THEN
    EXECUTE STATEMENT '"Database version mismatch! Expected ' || req_ver || ' but found ' || ver || '"';
END^


DROP TRIGGER T_AU_SY_STATE_MARITAL_STATUS_9
^

DROP TRIGGER T_AD_SY_STATE_MARITAL_STATUS_9
^

CREATE OR ALTER PROCEDURE pack_sy_state_marital_status(nbr INTEGER)
AS
BEGIN
END
^


ALTER TABLE SY_STATE_MARITAL_STATUS
ADD TEMP_STATUS_TYPE EV_CHAR2 NOT NULL
^


UPDATE SY_STATE_MARITAL_STATUS
SET TEMP_STATUS_TYPE = STATUS_TYPE
^


ALTER TABLE SY_STATE_MARITAL_STATUS
DROP STATUS_TYPE
^

ALTER TABLE SY_STATE_MARITAL_STATUS
ALTER COLUMN TEMP_STATUS_TYPE TO STATUS_TYPE
^

ALTER TABLE SY_STATE_MARITAL_STATUS
ALTER COLUMN STATUS_TYPE POSITION 6
^


CREATE OR ALTER PROCEDURE pack_sy_state_marital_status(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sy_state_marital_status_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE status_type CHAR(2);
DECLARE VARIABLE p_status_type CHAR(2);
DECLARE VARIABLE status_description VARCHAR(40);
DECLARE VARIABLE p_status_description VARCHAR(40);
DECLARE VARIABLE personal_exemptions INTEGER;
DECLARE VARIABLE p_personal_exemptions INTEGER;
DECLARE VARIABLE deduct_federal CHAR(1);
DECLARE VARIABLE p_deduct_federal CHAR(1);
DECLARE VARIABLE deduct_fica CHAR(1);
DECLARE VARIABLE p_deduct_fica CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE p_filler VARCHAR(512);
DECLARE VARIABLE state_percent_of_federal NUMERIC(18,6);
DECLARE VARIABLE p_state_percent_of_federal NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_pcnt_gross NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_pcnt_gross NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_min_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_min_amount NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_max_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_max_amount NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_flat_amount NUMERIC(18,6);
DECLARE VARIABLE p_standard_deduction_flat_amount NUMERIC(18,6);
DECLARE VARIABLE standard_per_exemption_allow NUMERIC(18,6);
DECLARE VARIABLE p_standard_per_exemption_allow NUMERIC(18,6);
DECLARE VARIABLE deduct_federal_maximum_amount NUMERIC(18,6);
DECLARE VARIABLE p_deduct_federal_maximum_amount NUMERIC(18,6);
DECLARE VARIABLE per_dependent_allowance NUMERIC(18,6);
DECLARE VARIABLE p_per_dependent_allowance NUMERIC(18,6);
DECLARE VARIABLE personal_tax_credit_amount NUMERIC(18,6);
DECLARE VARIABLE p_personal_tax_credit_amount NUMERIC(18,6);
DECLARE VARIABLE tax_credit_per_dependent NUMERIC(18,6);
DECLARE VARIABLE p_tax_credit_per_dependent NUMERIC(18,6);
DECLARE VARIABLE tax_credit_per_allowance NUMERIC(18,6);
DECLARE VARIABLE p_tax_credit_per_allowance NUMERIC(18,6);
DECLARE VARIABLE high_income_per_exempt_allow NUMERIC(18,6);
DECLARE VARIABLE p_high_income_per_exempt_allow NUMERIC(18,6);
DECLARE VARIABLE defined_high_income_amount NUMERIC(18,6);
DECLARE VARIABLE p_defined_high_income_amount NUMERIC(18,6);
DECLARE VARIABLE minimum_taxable_income NUMERIC(18,6);
DECLARE VARIABLE p_minimum_taxable_income NUMERIC(18,6);
DECLARE VARIABLE additional_exempt_allowance NUMERIC(18,6);
DECLARE VARIABLE p_additional_exempt_allowance NUMERIC(18,6);
DECLARE VARIABLE additional_deduction_allowance NUMERIC(18,6);
DECLARE VARIABLE p_additional_deduction_allowance NUMERIC(18,6);
DECLARE VARIABLE deduct_fica__maximum_amount NUMERIC(18,6);
DECLARE VARIABLE p_deduct_fica__maximum_amount NUMERIC(18,6);
DECLARE VARIABLE blind_exemption_amount NUMERIC(18,6);
DECLARE VARIABLE p_blind_exemption_amount NUMERIC(18,6);
DECLARE VARIABLE active_status CHAR(1);
DECLARE VARIABLE p_active_status CHAR(1);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sy_state_marital_status_nbr , status_type, status_description, personal_exemptions, deduct_federal, deduct_fica, filler, state_percent_of_federal, standard_deduction_pcnt_gross, standard_deduction_min_amount, standard_deduction_max_amount, standard_deduction_flat_amount, standard_per_exemption_allow, deduct_federal_maximum_amount, per_dependent_allowance, personal_tax_credit_amount, tax_credit_per_dependent, tax_credit_per_allowance, high_income_per_exempt_allow, defined_high_income_amount, minimum_taxable_income, additional_exempt_allowance, additional_deduction_allowance, deduct_fica__maximum_amount, blind_exemption_amount, active_status
        FROM sy_state_marital_status
        ORDER BY sy_state_marital_status_nbr, effective_date
        INTO :rec_version, :effective_date, :sy_state_marital_status_nbr, :status_type, :status_description, :personal_exemptions, :deduct_federal, :deduct_fica, :filler, :state_percent_of_federal, :standard_deduction_pcnt_gross, :standard_deduction_min_amount, :standard_deduction_max_amount, :standard_deduction_flat_amount, :standard_per_exemption_allow, :deduct_federal_maximum_amount, :per_dependent_allowance, :personal_tax_credit_amount, :tax_credit_per_dependent, :tax_credit_per_allowance, :high_income_per_exempt_allow, :defined_high_income_amount, :minimum_taxable_income, :additional_exempt_allowance, :additional_deduction_allowance, :deduct_fica__maximum_amount, :blind_exemption_amount, :active_status
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sy_state_marital_status_nbr) OR (effective_date = '1/1/1900') OR (p_status_type IS DISTINCT FROM status_type) OR (p_status_description IS DISTINCT FROM status_description) OR (p_personal_exemptions IS DISTINCT FROM personal_exemptions) OR (p_deduct_federal IS DISTINCT FROM deduct_federal) OR (p_deduct_fica IS DISTINCT FROM deduct_fica) OR (p_filler IS DISTINCT FROM filler) OR (p_state_percent_of_federal IS DISTINCT FROM state_percent_of_federal) OR (p_standard_deduction_pcnt_gross IS DISTINCT FROM standard_deduction_pcnt_gross) OR (p_standard_deduction_min_amount IS DISTINCT FROM standard_deduction_min_amount) OR (p_standard_deduction_max_amount IS DISTINCT FROM standard_deduction_max_amount) OR (p_standard_deduction_flat_amount IS DISTINCT FROM standard_deduction_flat_amount) OR (p_standard_per_exemption_allow IS DISTINCT FROM standard_per_exemption_allow) OR (p_deduct_federal_maximum_amount IS DISTINCT FROM deduct_federal_maximum_amount) OR (p_per_dependent_allowance IS DISTINCT FROM per_dependent_allowance) OR (p_personal_tax_credit_amount IS DISTINCT FROM personal_tax_credit_amount) OR (p_tax_credit_per_dependent IS DISTINCT FROM tax_credit_per_dependent) OR (p_tax_credit_per_allowance IS DISTINCT FROM tax_credit_per_allowance) OR (p_high_income_per_exempt_allow IS DISTINCT FROM high_income_per_exempt_allow) OR (p_defined_high_income_amount IS DISTINCT FROM defined_high_income_amount) OR (p_minimum_taxable_income IS DISTINCT FROM minimum_taxable_income) OR (p_additional_exempt_allowance IS DISTINCT FROM additional_exempt_allowance) OR (p_additional_deduction_allowance IS DISTINCT FROM additional_deduction_allowance) OR (p_deduct_fica__maximum_amount IS DISTINCT FROM deduct_fica__maximum_amount) OR (p_blind_exemption_amount IS DISTINCT FROM blind_exemption_amount) OR (p_active_status IS DISTINCT FROM active_status)) THEN
      BEGIN
        curr_nbr = sy_state_marital_status_nbr;
        p_status_type = status_type;
        p_status_description = status_description;
        p_personal_exemptions = personal_exemptions;
        p_deduct_federal = deduct_federal;
        p_deduct_fica = deduct_fica;
        p_filler = filler;
        p_state_percent_of_federal = state_percent_of_federal;
        p_standard_deduction_pcnt_gross = standard_deduction_pcnt_gross;
        p_standard_deduction_min_amount = standard_deduction_min_amount;
        p_standard_deduction_max_amount = standard_deduction_max_amount;
        p_standard_deduction_flat_amount = standard_deduction_flat_amount;
        p_standard_per_exemption_allow = standard_per_exemption_allow;
        p_deduct_federal_maximum_amount = deduct_federal_maximum_amount;
        p_per_dependent_allowance = per_dependent_allowance;
        p_personal_tax_credit_amount = personal_tax_credit_amount;
        p_tax_credit_per_dependent = tax_credit_per_dependent;
        p_tax_credit_per_allowance = tax_credit_per_allowance;
        p_high_income_per_exempt_allow = high_income_per_exempt_allow;
        p_defined_high_income_amount = defined_high_income_amount;
        p_minimum_taxable_income = minimum_taxable_income;
        p_additional_exempt_allowance = additional_exempt_allowance;
        p_additional_deduction_allowance = additional_deduction_allowance;
        p_deduct_fica__maximum_amount = deduct_fica__maximum_amount;
        p_blind_exemption_amount = blind_exemption_amount;
        p_active_status = active_status;
      END
      ELSE
        DELETE FROM sy_state_marital_status WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, status_type, status_description, personal_exemptions, deduct_federal, deduct_fica, filler, state_percent_of_federal, standard_deduction_pcnt_gross, standard_deduction_min_amount, standard_deduction_max_amount, standard_deduction_flat_amount, standard_per_exemption_allow, deduct_federal_maximum_amount, per_dependent_allowance, personal_tax_credit_amount, tax_credit_per_dependent, tax_credit_per_allowance, high_income_per_exempt_allow, defined_high_income_amount, minimum_taxable_income, additional_exempt_allowance, additional_deduction_allowance, deduct_fica__maximum_amount, blind_exemption_amount, active_status
        FROM sy_state_marital_status
        WHERE sy_state_marital_status_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :status_type, :status_description, :personal_exemptions, :deduct_federal, :deduct_fica, :filler, :state_percent_of_federal, :standard_deduction_pcnt_gross, :standard_deduction_min_amount, :standard_deduction_max_amount, :standard_deduction_flat_amount, :standard_per_exemption_allow, :deduct_federal_maximum_amount, :per_dependent_allowance, :personal_tax_credit_amount, :tax_credit_per_dependent, :tax_credit_per_allowance, :high_income_per_exempt_allow, :defined_high_income_amount, :minimum_taxable_income, :additional_exempt_allowance, :additional_deduction_allowance, :deduct_fica__maximum_amount, :blind_exemption_amount, :active_status
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_status_type IS DISTINCT FROM status_type) OR (p_status_description IS DISTINCT FROM status_description) OR (p_personal_exemptions IS DISTINCT FROM personal_exemptions) OR (p_deduct_federal IS DISTINCT FROM deduct_federal) OR (p_deduct_fica IS DISTINCT FROM deduct_fica) OR (p_filler IS DISTINCT FROM filler) OR (p_state_percent_of_federal IS DISTINCT FROM state_percent_of_federal) OR (p_standard_deduction_pcnt_gross IS DISTINCT FROM standard_deduction_pcnt_gross) OR (p_standard_deduction_min_amount IS DISTINCT FROM standard_deduction_min_amount) OR (p_standard_deduction_max_amount IS DISTINCT FROM standard_deduction_max_amount) OR (p_standard_deduction_flat_amount IS DISTINCT FROM standard_deduction_flat_amount) OR (p_standard_per_exemption_allow IS DISTINCT FROM standard_per_exemption_allow) OR (p_deduct_federal_maximum_amount IS DISTINCT FROM deduct_federal_maximum_amount) OR (p_per_dependent_allowance IS DISTINCT FROM per_dependent_allowance) OR (p_personal_tax_credit_amount IS DISTINCT FROM personal_tax_credit_amount) OR (p_tax_credit_per_dependent IS DISTINCT FROM tax_credit_per_dependent) OR (p_tax_credit_per_allowance IS DISTINCT FROM tax_credit_per_allowance) OR (p_high_income_per_exempt_allow IS DISTINCT FROM high_income_per_exempt_allow) OR (p_defined_high_income_amount IS DISTINCT FROM defined_high_income_amount) OR (p_minimum_taxable_income IS DISTINCT FROM minimum_taxable_income) OR (p_additional_exempt_allowance IS DISTINCT FROM additional_exempt_allowance) OR (p_additional_deduction_allowance IS DISTINCT FROM additional_deduction_allowance) OR (p_deduct_fica__maximum_amount IS DISTINCT FROM deduct_fica__maximum_amount) OR (p_blind_exemption_amount IS DISTINCT FROM blind_exemption_amount) OR (p_active_status IS DISTINCT FROM active_status)) THEN
      BEGIN
        p_status_type = status_type;
        p_status_description = status_description;
        p_personal_exemptions = personal_exemptions;
        p_deduct_federal = deduct_federal;
        p_deduct_fica = deduct_fica;
        p_filler = filler;
        p_state_percent_of_federal = state_percent_of_federal;
        p_standard_deduction_pcnt_gross = standard_deduction_pcnt_gross;
        p_standard_deduction_min_amount = standard_deduction_min_amount;
        p_standard_deduction_max_amount = standard_deduction_max_amount;
        p_standard_deduction_flat_amount = standard_deduction_flat_amount;
        p_standard_per_exemption_allow = standard_per_exemption_allow;
        p_deduct_federal_maximum_amount = deduct_federal_maximum_amount;
        p_per_dependent_allowance = per_dependent_allowance;
        p_personal_tax_credit_amount = personal_tax_credit_amount;
        p_tax_credit_per_dependent = tax_credit_per_dependent;
        p_tax_credit_per_allowance = tax_credit_per_allowance;
        p_high_income_per_exempt_allow = high_income_per_exempt_allow;
        p_defined_high_income_amount = defined_high_income_amount;
        p_minimum_taxable_income = minimum_taxable_income;
        p_additional_exempt_allowance = additional_exempt_allowance;
        p_additional_deduction_allowance = additional_deduction_allowance;
        p_deduct_fica__maximum_amount = deduct_fica__maximum_amount;
        p_blind_exemption_amount = blind_exemption_amount;
        p_active_status = active_status;
      END
      ELSE
        DELETE FROM sy_state_marital_status WHERE rec_version = :rec_version;
    END
  END
END
^



GRANT EXECUTE ON PROCEDURE PACK_SY_STATE_MARITAL_STATUS TO EUSER
^

CREATE TRIGGER T_AD_SY_STATE_MARITAL_STATUS_9 FOR SY_STATE_MARITAL_STATUS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE status_type CHAR(2);
DECLARE VARIABLE status_description VARCHAR(40);
DECLARE VARIABLE personal_exemptions INTEGER;
DECLARE VARIABLE deduct_federal CHAR(1);
DECLARE VARIABLE deduct_fica CHAR(1);
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE state_percent_of_federal NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_pcnt_gross NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_min_amount NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_max_amount NUMERIC(18,6);
DECLARE VARIABLE standard_deduction_flat_amount NUMERIC(18,6);
DECLARE VARIABLE standard_per_exemption_allow NUMERIC(18,6);
DECLARE VARIABLE deduct_federal_maximum_amount NUMERIC(18,6);
DECLARE VARIABLE per_dependent_allowance NUMERIC(18,6);
DECLARE VARIABLE personal_tax_credit_amount NUMERIC(18,6);
DECLARE VARIABLE tax_credit_per_dependent NUMERIC(18,6);
DECLARE VARIABLE tax_credit_per_allowance NUMERIC(18,6);
DECLARE VARIABLE high_income_per_exempt_allow NUMERIC(18,6);
DECLARE VARIABLE defined_high_income_amount NUMERIC(18,6);
DECLARE VARIABLE minimum_taxable_income NUMERIC(18,6);
DECLARE VARIABLE additional_exempt_allowance NUMERIC(18,6);
DECLARE VARIABLE additional_deduction_allowance NUMERIC(18,6);
DECLARE VARIABLE deduct_fica__maximum_amount NUMERIC(18,6);
DECLARE VARIABLE blind_exemption_amount NUMERIC(18,6);
DECLARE VARIABLE active_status CHAR(1);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(36, OLD.rec_version, OLD.sy_state_marital_status_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', status_type, status_description, personal_exemptions, deduct_federal, deduct_fica, filler, state_percent_of_federal, standard_deduction_pcnt_gross, standard_deduction_min_amount, standard_deduction_max_amount, standard_deduction_flat_amount, standard_per_exemption_allow, deduct_federal_maximum_amount, per_dependent_allowance, personal_tax_credit_amount, tax_credit_per_dependent, tax_credit_per_allowance, high_income_per_exempt_allow, defined_high_income_amount, minimum_taxable_income, additional_exempt_allowance, additional_deduction_allowance, deduct_fica__maximum_amount, blind_exemption_amount, active_status  FROM sy_state_marital_status WHERE sy_state_marital_status_nbr = OLD.sy_state_marital_status_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :status_type, :status_description, :personal_exemptions, :deduct_federal, :deduct_fica, :filler, :state_percent_of_federal, :standard_deduction_pcnt_gross, :standard_deduction_min_amount, :standard_deduction_max_amount, :standard_deduction_flat_amount, :standard_per_exemption_allow, :deduct_federal_maximum_amount, :per_dependent_allowance, :personal_tax_credit_amount, :tax_credit_per_dependent, :tax_credit_per_allowance, :high_income_per_exempt_allow, :defined_high_income_amount, :minimum_taxable_income, :additional_exempt_allowance, :additional_deduction_allowance, :deduct_fica__maximum_amount, :blind_exemption_amount, :active_status;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 481, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 482, OLD.effective_until);

  /* STATUS_TYPE */
  IF (last_record = 'Y' OR status_type IS DISTINCT FROM OLD.status_type) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 478, OLD.status_type);

  /* STATUS_DESCRIPTION */
  IF (last_record = 'Y' OR status_description IS DISTINCT FROM OLD.status_description) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 477, OLD.status_description);

  /* PERSONAL_EXEMPTIONS */
  IF ((last_record = 'Y' AND OLD.personal_exemptions IS NOT NULL) OR (last_record = 'N' AND personal_exemptions IS DISTINCT FROM OLD.personal_exemptions)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 473, OLD.personal_exemptions);

  /* DEDUCT_FEDERAL */
  IF (last_record = 'Y' OR deduct_federal IS DISTINCT FROM OLD.deduct_federal) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 480, OLD.deduct_federal);

  /* DEDUCT_FICA */
  IF (last_record = 'Y' OR deduct_fica IS DISTINCT FROM OLD.deduct_fica) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 479, OLD.deduct_fica);

  /* FILLER */
  IF ((last_record = 'Y' AND OLD.filler IS NOT NULL) OR (last_record = 'N' AND filler IS DISTINCT FROM OLD.filler)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 476, OLD.filler);

  /* STATE_PERCENT_OF_FEDERAL */
  IF ((last_record = 'Y' AND OLD.state_percent_of_federal IS NOT NULL) OR (last_record = 'N' AND state_percent_of_federal IS DISTINCT FROM OLD.state_percent_of_federal)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 471, OLD.state_percent_of_federal);

  /* STANDARD_DEDUCTION_PCNT_GROSS */
  IF ((last_record = 'Y' AND OLD.standard_deduction_pcnt_gross IS NOT NULL) OR (last_record = 'N' AND standard_deduction_pcnt_gross IS DISTINCT FROM OLD.standard_deduction_pcnt_gross)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 470, OLD.standard_deduction_pcnt_gross);

  /* STANDARD_DEDUCTION_MIN_AMOUNT */
  IF ((last_record = 'Y' AND OLD.standard_deduction_min_amount IS NOT NULL) OR (last_record = 'N' AND standard_deduction_min_amount IS DISTINCT FROM OLD.standard_deduction_min_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 469, OLD.standard_deduction_min_amount);

  /* STANDARD_DEDUCTION_MAX_AMOUNT */
  IF ((last_record = 'Y' AND OLD.standard_deduction_max_amount IS NOT NULL) OR (last_record = 'N' AND standard_deduction_max_amount IS DISTINCT FROM OLD.standard_deduction_max_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 468, OLD.standard_deduction_max_amount);

  /* STANDARD_DEDUCTION_FLAT_AMOUNT */
  IF ((last_record = 'Y' AND OLD.standard_deduction_flat_amount IS NOT NULL) OR (last_record = 'N' AND standard_deduction_flat_amount IS DISTINCT FROM OLD.standard_deduction_flat_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 467, OLD.standard_deduction_flat_amount);

  /* STANDARD_PER_EXEMPTION_ALLOW */
  IF ((last_record = 'Y' AND OLD.standard_per_exemption_allow IS NOT NULL) OR (last_record = 'N' AND standard_per_exemption_allow IS DISTINCT FROM OLD.standard_per_exemption_allow)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 466, OLD.standard_per_exemption_allow);

  /* DEDUCT_FEDERAL_MAXIMUM_AMOUNT */
  IF ((last_record = 'Y' AND OLD.deduct_federal_maximum_amount IS NOT NULL) OR (last_record = 'N' AND deduct_federal_maximum_amount IS DISTINCT FROM OLD.deduct_federal_maximum_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 465, OLD.deduct_federal_maximum_amount);

  /* PER_DEPENDENT_ALLOWANCE */
  IF ((last_record = 'Y' AND OLD.per_dependent_allowance IS NOT NULL) OR (last_record = 'N' AND per_dependent_allowance IS DISTINCT FROM OLD.per_dependent_allowance)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 464, OLD.per_dependent_allowance);

  /* PERSONAL_TAX_CREDIT_AMOUNT */
  IF ((last_record = 'Y' AND OLD.personal_tax_credit_amount IS NOT NULL) OR (last_record = 'N' AND personal_tax_credit_amount IS DISTINCT FROM OLD.personal_tax_credit_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 463, OLD.personal_tax_credit_amount);

  /* TAX_CREDIT_PER_DEPENDENT */
  IF ((last_record = 'Y' AND OLD.tax_credit_per_dependent IS NOT NULL) OR (last_record = 'N' AND tax_credit_per_dependent IS DISTINCT FROM OLD.tax_credit_per_dependent)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 462, OLD.tax_credit_per_dependent);

  /* TAX_CREDIT_PER_ALLOWANCE */
  IF ((last_record = 'Y' AND OLD.tax_credit_per_allowance IS NOT NULL) OR (last_record = 'N' AND tax_credit_per_allowance IS DISTINCT FROM OLD.tax_credit_per_allowance)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 461, OLD.tax_credit_per_allowance);

  /* HIGH_INCOME_PER_EXEMPT_ALLOW */
  IF ((last_record = 'Y' AND OLD.high_income_per_exempt_allow IS NOT NULL) OR (last_record = 'N' AND high_income_per_exempt_allow IS DISTINCT FROM OLD.high_income_per_exempt_allow)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 460, OLD.high_income_per_exempt_allow);

  /* DEFINED_HIGH_INCOME_AMOUNT */
  IF ((last_record = 'Y' AND OLD.defined_high_income_amount IS NOT NULL) OR (last_record = 'N' AND defined_high_income_amount IS DISTINCT FROM OLD.defined_high_income_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 459, OLD.defined_high_income_amount);

  /* MINIMUM_TAXABLE_INCOME */
  IF ((last_record = 'Y' AND OLD.minimum_taxable_income IS NOT NULL) OR (last_record = 'N' AND minimum_taxable_income IS DISTINCT FROM OLD.minimum_taxable_income)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 458, OLD.minimum_taxable_income);

  /* ADDITIONAL_EXEMPT_ALLOWANCE */
  IF ((last_record = 'Y' AND OLD.additional_exempt_allowance IS NOT NULL) OR (last_record = 'N' AND additional_exempt_allowance IS DISTINCT FROM OLD.additional_exempt_allowance)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 457, OLD.additional_exempt_allowance);

  /* ADDITIONAL_DEDUCTION_ALLOWANCE */
  IF ((last_record = 'Y' AND OLD.additional_deduction_allowance IS NOT NULL) OR (last_record = 'N' AND additional_deduction_allowance IS DISTINCT FROM OLD.additional_deduction_allowance)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 456, OLD.additional_deduction_allowance);

  /* DEDUCT_FICA__MAXIMUM_AMOUNT */
  IF ((last_record = 'Y' AND OLD.deduct_fica__maximum_amount IS NOT NULL) OR (last_record = 'N' AND deduct_fica__maximum_amount IS DISTINCT FROM OLD.deduct_fica__maximum_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 455, OLD.deduct_fica__maximum_amount);

  /* BLIND_EXEMPTION_AMOUNT */
  IF ((last_record = 'Y' AND OLD.blind_exemption_amount IS NOT NULL) OR (last_record = 'N' AND blind_exemption_amount IS DISTINCT FROM OLD.blind_exemption_amount)) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 454, OLD.blind_exemption_amount);

  /* ACTIVE_STATUS */
  IF (last_record = 'Y' OR active_status IS DISTINCT FROM OLD.active_status) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 582, OLD.active_status);


  IF (last_record = 'Y') THEN
  BEGIN
    /* SY_STATES_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 474, OLD.sy_states_nbr);

  END
END

^

CREATE TRIGGER T_AU_SY_STATE_MARITAL_STATUS_9 FOR SY_STATE_MARITAL_STATUS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(36, NEW.rec_version, NEW.sy_state_marital_status_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 481, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_STATES_NBR */
  IF (OLD.sy_states_nbr IS DISTINCT FROM NEW.sy_states_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 474, OLD.sy_states_nbr);
    changes = changes + 1;
  END

  /* STATUS_TYPE */
  IF (OLD.status_type IS DISTINCT FROM NEW.status_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 478, OLD.status_type);
    changes = changes + 1;
  END

  /* STATUS_DESCRIPTION */
  IF (OLD.status_description IS DISTINCT FROM NEW.status_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 477, OLD.status_description);
    changes = changes + 1;
  END

  /* PERSONAL_EXEMPTIONS */
  IF (OLD.personal_exemptions IS DISTINCT FROM NEW.personal_exemptions) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 473, OLD.personal_exemptions);
    changes = changes + 1;
  END

  /* DEDUCT_FEDERAL */
  IF (OLD.deduct_federal IS DISTINCT FROM NEW.deduct_federal) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 480, OLD.deduct_federal);
    changes = changes + 1;
  END

  /* DEDUCT_FICA */
  IF (OLD.deduct_fica IS DISTINCT FROM NEW.deduct_fica) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 479, OLD.deduct_fica);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 476, OLD.filler);
    changes = changes + 1;
  END

  /* STATE_PERCENT_OF_FEDERAL */
  IF (OLD.state_percent_of_federal IS DISTINCT FROM NEW.state_percent_of_federal) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 471, OLD.state_percent_of_federal);
    changes = changes + 1;
  END

  /* STANDARD_DEDUCTION_PCNT_GROSS */
  IF (OLD.standard_deduction_pcnt_gross IS DISTINCT FROM NEW.standard_deduction_pcnt_gross) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 470, OLD.standard_deduction_pcnt_gross);
    changes = changes + 1;
  END

  /* STANDARD_DEDUCTION_MIN_AMOUNT */
  IF (OLD.standard_deduction_min_amount IS DISTINCT FROM NEW.standard_deduction_min_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 469, OLD.standard_deduction_min_amount);
    changes = changes + 1;
  END

  /* STANDARD_DEDUCTION_MAX_AMOUNT */
  IF (OLD.standard_deduction_max_amount IS DISTINCT FROM NEW.standard_deduction_max_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 468, OLD.standard_deduction_max_amount);
    changes = changes + 1;
  END

  /* STANDARD_DEDUCTION_FLAT_AMOUNT */
  IF (OLD.standard_deduction_flat_amount IS DISTINCT FROM NEW.standard_deduction_flat_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 467, OLD.standard_deduction_flat_amount);
    changes = changes + 1;
  END

  /* STANDARD_PER_EXEMPTION_ALLOW */
  IF (OLD.standard_per_exemption_allow IS DISTINCT FROM NEW.standard_per_exemption_allow) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 466, OLD.standard_per_exemption_allow);
    changes = changes + 1;
  END

  /* DEDUCT_FEDERAL_MAXIMUM_AMOUNT */
  IF (OLD.deduct_federal_maximum_amount IS DISTINCT FROM NEW.deduct_federal_maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 465, OLD.deduct_federal_maximum_amount);
    changes = changes + 1;
  END

  /* PER_DEPENDENT_ALLOWANCE */
  IF (OLD.per_dependent_allowance IS DISTINCT FROM NEW.per_dependent_allowance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 464, OLD.per_dependent_allowance);
    changes = changes + 1;
  END

  /* PERSONAL_TAX_CREDIT_AMOUNT */
  IF (OLD.personal_tax_credit_amount IS DISTINCT FROM NEW.personal_tax_credit_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 463, OLD.personal_tax_credit_amount);
    changes = changes + 1;
  END

  /* TAX_CREDIT_PER_DEPENDENT */
  IF (OLD.tax_credit_per_dependent IS DISTINCT FROM NEW.tax_credit_per_dependent) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 462, OLD.tax_credit_per_dependent);
    changes = changes + 1;
  END

  /* TAX_CREDIT_PER_ALLOWANCE */
  IF (OLD.tax_credit_per_allowance IS DISTINCT FROM NEW.tax_credit_per_allowance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 461, OLD.tax_credit_per_allowance);
    changes = changes + 1;
  END

  /* HIGH_INCOME_PER_EXEMPT_ALLOW */
  IF (OLD.high_income_per_exempt_allow IS DISTINCT FROM NEW.high_income_per_exempt_allow) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 460, OLD.high_income_per_exempt_allow);
    changes = changes + 1;
  END

  /* DEFINED_HIGH_INCOME_AMOUNT */
  IF (OLD.defined_high_income_amount IS DISTINCT FROM NEW.defined_high_income_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 459, OLD.defined_high_income_amount);
    changes = changes + 1;
  END

  /* MINIMUM_TAXABLE_INCOME */
  IF (OLD.minimum_taxable_income IS DISTINCT FROM NEW.minimum_taxable_income) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 458, OLD.minimum_taxable_income);
    changes = changes + 1;
  END

  /* ADDITIONAL_EXEMPT_ALLOWANCE */
  IF (OLD.additional_exempt_allowance IS DISTINCT FROM NEW.additional_exempt_allowance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 457, OLD.additional_exempt_allowance);
    changes = changes + 1;
  END

  /* ADDITIONAL_DEDUCTION_ALLOWANCE */
  IF (OLD.additional_deduction_allowance IS DISTINCT FROM NEW.additional_deduction_allowance) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 456, OLD.additional_deduction_allowance);
    changes = changes + 1;
  END

  /* DEDUCT_FICA__MAXIMUM_AMOUNT */
  IF (OLD.deduct_fica__maximum_amount IS DISTINCT FROM NEW.deduct_fica__maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 455, OLD.deduct_fica__maximum_amount);
    changes = changes + 1;
  END

  /* BLIND_EXEMPTION_AMOUNT */
  IF (OLD.blind_exemption_amount IS DISTINCT FROM NEW.blind_exemption_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 454, OLD.blind_exemption_amount);
    changes = changes + 1;
  END

  /* ACTIVE_STATUS */
  IF (OLD.active_status IS DISTINCT FROM NEW.active_status) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 582, OLD.active_status);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  UPDATE ev_field SET name = 'STATUS_TYPE', field_type = 'C', len = 2, scale = NULL, required = 'Y', versioned = 'Y' WHERE nbr = 478;
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^


/* Update EV_DATABASE */
EXECUTE block
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (VERSION, description) VALUES ('15.0.0.7', 'Evolution System Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
