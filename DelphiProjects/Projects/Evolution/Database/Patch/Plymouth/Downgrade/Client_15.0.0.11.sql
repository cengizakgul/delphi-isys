/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.12';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


DROP INDEX LK_EE_DIRECT_DEPOSIT
^

DROP TRIGGER T_BIU_EE_DIRECT_DEPOSIT_3
^

CREATE INDEX LK_EE_DIRECT_DEPOSIT ON EE_DIRECT_DEPOSIT (EE_ABA_NUMBER,EE_BANK_ACCOUNT_NUMBER,EE_NBR)
^

CREATE TRIGGER T_BIU_EE_DIRECT_DEPOSIT_3 FOR EE_DIRECT_DEPOSIT Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_EE_DIRECT_DEPOSIT */
  IF (INSERTING OR (OLD.ee_aba_number IS DISTINCT FROM NEW.ee_aba_number) OR (OLD.ee_bank_account_number IS DISTINCT FROM NEW.ee_bank_account_number) OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM ee_direct_deposit WHERE ee_aba_number = NEW.ee_aba_number AND ee_bank_account_number = NEW.ee_bank_account_number AND ee_nbr = NEW.ee_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('ee_direct_deposit', 'ee_aba_number, ee_bank_account_number, ee_nbr',
      CAST(NEW.ee_aba_number || ', ' || NEW.ee_bank_account_number || ', ' || NEW.ee_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^


COMMIT
^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.11', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
