/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '15.0.0.14';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^


CREATE INDEX LK_EE_ADDITIONAL_INFO ON EE_ADDITIONAL_INFO (EE_NBR,CO_ADDITIONAL_INFO_NAMES_NBR)
^

CREATE TRIGGER T_BIU_EE_ADDITIONAL_INFO_3 FOR EE_ADDITIONAL_INFO Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_EE_ADDITIONAL_INFO */
  IF (INSERTING OR (OLD.ee_nbr IS DISTINCT FROM NEW.ee_nbr) OR (OLD.co_additional_info_names_nbr IS DISTINCT FROM NEW.co_additional_info_names_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM ee_additional_info WHERE ee_nbr = NEW.ee_nbr AND co_additional_info_names_nbr = NEW.co_additional_info_names_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('ee_additional_info', 'ee_nbr, co_additional_info_names_nbr',
      CAST(NEW.ee_nbr || ', ' || NEW.co_additional_info_names_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END
^

COMMIT
^


/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('15.0.0.13', 'Evolution Client Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
