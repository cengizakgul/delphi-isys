/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.3';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^



/*  111  */

DROP TRIGGER T_AU_SB_ENLIST_GROUPS_9
^
DROP TRIGGER T_AU_SB_MAIL_BOX_CONTENT_9
^
DROP TRIGGER T_AU_SB_PAPER_INFO_9
^
DROP TRIGGER T_AU_SB_REPORT_WRITER_REPORT_9
^
DROP TRIGGER T_AU_SB_SERVICES_9
^


/*  222  */



CREATE GENERATOR G_SB_ACA_GROUP_ADD_MEMBERS
^

CREATE GENERATOR G_SB_ACA_GROUP_ADD_MEMBERS_VER
^

CREATE GENERATOR G_SB_ACA_OFFER_CODES
^

CREATE GENERATOR G_SB_ACA_OFFER_CODES_VER
^

CREATE GENERATOR G_SB_ACA_RELIEF_CODES
^

CREATE GENERATOR G_SB_ACA_RELIEF_CODES_VER
^

CREATE GENERATOR G_SB_ADDITIONAL_INFO_NAMES
^

CREATE GENERATOR G_SB_ADDITIONAL_INFO_NAMES_VER
^

CREATE GENERATOR G_SB_ADDITIONAL_INFO_VALUES
^

CREATE GENERATOR G_SB_ADDITIONAL_INFO_VALUES_VER
^


DROP TRIGGER T_AD_SB_9
^


DROP TRIGGER T_AU_SB_9
^

ALTER TABLE SB
ADD ENFORCE_EE_DOB_DEFAULT EV_CHAR1 
^

ALTER TABLE SB
ALTER COLUMN ENFORCE_EE_DOB_DEFAULT POSITION 66
^


DROP TRIGGER T_AD_SB_BANKS_9
^


DROP TRIGGER T_AU_SB_BANKS_9
^

ALTER TABLE SB_BANKS
ADD ACH_DATE EV_DATE 
^

ALTER TABLE SB_BANKS
ALTER COLUMN ACH_DATE POSITION 32
^

ALTER TABLE SB_BANKS
ADD ACH_NUMBER EV_CHAR1 
^

ALTER TABLE SB_BANKS
ALTER COLUMN ACH_NUMBER POSITION 33
^


DROP TRIGGER T_AD_SB_ENLIST_GROUPS_9
^

ALTER TABLE SB_ENLIST_GROUPS
ADD TEMP_MEDIA_TYPE EV_STR2 
^

COMMIT^

UPDATE SB_ENLIST_GROUPS
SET TEMP_MEDIA_TYPE = MEDIA_TYPE
^


ALTER TABLE SB_ENLIST_GROUPS
DROP MEDIA_TYPE
^

ALTER TABLE SB_ENLIST_GROUPS
ALTER COLUMN TEMP_MEDIA_TYPE TO MEDIA_TYPE
^

ALTER TABLE SB_ENLIST_GROUPS
ALTER COLUMN MEDIA_TYPE POSITION 6
^


DROP TRIGGER T_AD_SB_MAIL_BOX_CONTENT_9
^

ALTER TABLE SB_MAIL_BOX_CONTENT
ALTER COLUMN MEDIA_TYPE TYPE EV_STR2
^

ALTER TABLE SB_MAIL_BOX_CONTENT
ADD USERSIDE_PRINTED EV_DATETIME 
^

ALTER TABLE SB_MAIL_BOX_CONTENT
ALTER COLUMN USERSIDE_PRINTED POSITION 10
^

ALTER TABLE SB_PAPER_INFO
ALTER COLUMN MEDIA_TYPE TYPE EV_STR2
^

ALTER TABLE SB_REPORT_WRITER_REPORTS
ALTER COLUMN MEDIA_TYPE TYPE EV_STR2
^

ALTER TABLE SB_SERVICES
ALTER COLUMN BASED_ON_TYPE TYPE EV_STR2
^

ALTER TABLE SB_SERVICES
ALTER COLUMN PRODUCT_CODE TYPE EV_STR10
^



/* 111 */

COMMIT
^


update SB_ENLIST_GROUPS set MEDIA_TYPE=trim(MEDIA_TYPE)
^

update SB_ENLIST_GROUPS set MEDIA_TYPE=trim(MEDIA_TYPE)
^

update SB_ENLIST_GROUPS set MEDIA_TYPE = null where  trim(MEDIA_TYPE) = ''
^

update SB_PAPER_INFO set MEDIA_TYPE=trim(MEDIA_TYPE)
^

update SB_REPORT_WRITER_REPORTS set MEDIA_TYPE=trim(MEDIA_TYPE)
^

update SB_SERVICES set BASED_ON_TYPE=trim(BASED_ON_TYPE)
^

update SB_SERVICES set PRODUCT_CODE=trim(PRODUCT_CODE)
^


update SB set ENFORCE_EE_DOB_DEFAULT= 'N'
^

update SB set ENFORCE_EE_DOB_DEFAULT= 'Y' where DUMMY_TAX_CL_NBR = 1
^

update SB set DUMMY_TAX_CL_NBR= null
^

update SB_MAIL_BOX_CONTENT set USERSIDE_PRINTED = 
    Cast(CAST(Trim(StrCopy(DESCRIPTION, 232, 2)) as VARCHAR(2)) || '/' ||
    CAST(Trim(StrCopy(DESCRIPTION, 234, 2)) as VARCHAR(2)) || '/' ||
    CAST(Trim(StrCopy(DESCRIPTION, 228, 4)) as VARCHAR(4)) || ' ' ||
    CAST(Trim(StrCopy(DESCRIPTION, 236, 2)) as VARCHAR(2)) || ':' ||
    CAST(Trim(StrCopy(DESCRIPTION, 238, 2)) as VARCHAR(2)) || ':' ||
    CAST(Trim(StrCopy(DESCRIPTION, 240, 2)) as VARCHAR(2)) as timestamp)
    where CAST(Trim(StrCopy(DESCRIPTION, 228, 14)) as VARCHAR(14)) <> ''
^


COMMIT^

/* 222 */





CREATE TABLE  SB_ACA_GROUP_ADD_MEMBERS
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ACA_GROUP_ADD_MEMBERS_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SB_ACA_GROUP_NBR EV_INT NOT NULL,
     ALE_COMPANY_NAME EV_STR40 NOT NULL,
     ALE_EIN_NUMBER EV_INT,
     ALE_MAIN_COMPANY EV_CHAR1,
     ALE_NUMBER_OF_FTES EV_INT,
        CONSTRAINT PK_SB_ACA_GROUP_ADD_MEMBERS PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ACA_GROUP_ADD_MEMBERS_1 UNIQUE (SB_ACA_GROUP_ADD_MEMBERS_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ACA_GROUP_ADD_MEMBERS_2 UNIQUE (SB_ACA_GROUP_ADD_MEMBERS_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_ACA_GROUP_NBR_1 ON SB_ACA_GROUP_ADD_MEMBERS (SB_ACA_GROUP_NBR,EFFECTIVE_UNTIL)
^


GRANT ALL ON SB_ACA_GROUP_ADD_MEMBERS TO EUSER
^

CREATE TABLE  SB_ACA_OFFER_CODES
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ACA_OFFER_CODES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SY_FED_ACA_OFFER_CODES_NBR EV_INT NOT NULL,
     ACA_OFFER_CODE_DESCRIPTION EV_STR40 NOT NULL,
        CONSTRAINT PK_SB_ACA_OFFER_CODES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ACA_OFFER_CODES_1 UNIQUE (SB_ACA_OFFER_CODES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ACA_OFFER_CODES_2 UNIQUE (SB_ACA_OFFER_CODES_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_ACA_OFFER_CODES_1 ON SB_ACA_OFFER_CODES (SY_FED_ACA_OFFER_CODES_NBR,EFFECTIVE_DATE)
^

CREATE INDEX LK_SB_ACA_OFFER_CODES_1 ON SB_ACA_OFFER_CODES (SY_FED_ACA_OFFER_CODES_NBR)
^


GRANT ALL ON SB_ACA_OFFER_CODES TO EUSER
^

CREATE TABLE  SB_ACA_RELIEF_CODES
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ACA_RELIEF_CODES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SY_FED_ACA_RELIEF_CODES_NBR EV_INT NOT NULL,
     ACA_RELIEF_CODE_DESCRIPTION EV_STR40 NOT NULL,
        CONSTRAINT PK_SB_ACA_RELIEF_CODES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ACA_RELIEF_CODES_1 UNIQUE (SB_ACA_RELIEF_CODES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ACA_RELIEF_CODES_2 UNIQUE (SB_ACA_RELIEF_CODES_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_ACA_RELIEF_CODES_1 ON SB_ACA_RELIEF_CODES (SY_FED_ACA_RELIEF_CODES_NBR,EFFECTIVE_DATE)
^

CREATE INDEX LK_SY_FED_ACA_RELIEF_CODES_1 ON SB_ACA_RELIEF_CODES (SY_FED_ACA_RELIEF_CODES_NBR)
^


GRANT ALL ON SB_ACA_RELIEF_CODES TO EUSER
^

CREATE TABLE  SB_ADDITIONAL_INFO_NAMES
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ADDITIONAL_INFO_NAMES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     CATEGORY EV_CHAR1,
     DATA_TYPE EV_CHAR1,
     NAME EV_STR40 NOT NULL,
     REQUIRED EV_CHAR1,
     SEQ_NUM EV_INT NOT NULL,
     SHOW_IN_EVO_PAYROLL EV_CHAR1,
        CONSTRAINT PK_SB_ADDITIONAL_INFO_NAMES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ADDITIONAL_INFO_NAMES_1 UNIQUE (SB_ADDITIONAL_INFO_NAMES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ADDITIONAL_INFO_NAMES_2 UNIQUE (SB_ADDITIONAL_INFO_NAMES_NBR,EFFECTIVE_UNTIL)
)
^


GRANT ALL ON SB_ADDITIONAL_INFO_NAMES TO EUSER
^

CREATE TABLE  SB_ADDITIONAL_INFO_VALUES
( 
     REC_VERSION EV_INT NOT NULL,
     SB_ADDITIONAL_INFO_VALUES_NBR EV_INT NOT NULL,
     EFFECTIVE_DATE EV_DATE NOT NULL,
     EFFECTIVE_UNTIL EV_DATE NOT NULL,
     SB_ADDITIONAL_INFO_NAMES_NBR EV_INT NOT NULL,
     INFO_VALUE EV_STR40 NOT NULL,
        CONSTRAINT PK_SB_ADDITIONAL_INFO_VALUES PRIMARY KEY (REC_VERSION),
        CONSTRAINT AK_SB_ADDITIONAL_INFO_VALUES_1 UNIQUE (SB_ADDITIONAL_INFO_VALUES_NBR,EFFECTIVE_DATE),
        CONSTRAINT AK_SB_ADDITIONAL_INFO_VALUES_2 UNIQUE (SB_ADDITIONAL_INFO_VALUES_NBR,EFFECTIVE_UNTIL)
)
^

CREATE INDEX FK_SB_ADDITIONAL_INFO_VALUES_1 ON SB_ADDITIONAL_INFO_VALUES (SB_ADDITIONAL_INFO_NAMES_NBR,EFFECTIVE_DATE)
^


GRANT ALL ON SB_ADDITIONAL_INFO_VALUES TO EUSER
^

CREATE OR ALTER PROCEDURE CHECK_INTEGRITY
RETURNS (parent_table VARCHAR(64), parent_nbr INTEGER, child_table VARCHAR(64), child_nbr INTEGER, child_field VARCHAR(64))
AS
BEGIN
  /* Check SB_ACCOUNTANT */
  child_table = 'SB_ACCOUNTANT';

  parent_table = 'SB_BANK_ACCOUNTS';
  child_field = 'CREDIT_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANK_ACCOUNTS';
  child_field = 'DEBIT_BANK_ACCOUNT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY */
  child_table = 'SB_AGENCY';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_AGENCY_REPORTS */
  child_table = 'SB_AGENCY_REPORTS';

  parent_table = 'SB_AGENCY';
  child_field = 'SB_AGENCY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_BANK_ACCOUNTS */
  child_table = 'SB_BANK_ACCOUNTS';

  parent_table = 'SB_BANKS';
  child_field = 'SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BANKS';
  child_field = 'ACH_ORIGIN_SB_BANKS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'LOGO_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_BLOB';
  child_field = 'SIGNATURE_SB_BLOB_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_COMPANY_SVCS */
  child_table = 'SB_DELIVERY_COMPANY_SVCS';

  parent_table = 'SB_DELIVERY_COMPANY';
  child_field = 'SB_DELIVERY_COMPANY_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_DELIVERY_SERVICE_OPT */
  child_table = 'SB_DELIVERY_SERVICE_OPT';

  parent_table = 'SB_DELIVERY_SERVICE';
  child_field = 'SB_DELIVERY_SERVICE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX */
  child_table = 'SB_MAIL_BOX';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'UP_LEVEL_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_REPORT_WRITER_REPORTS';
  child_field = 'SB_COVER_LETTER_REPORT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_CONTENT */
  child_table = 'SB_MAIL_BOX_CONTENT';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MAIL_BOX_OPTION */
  child_table = 'SB_MAIL_BOX_OPTION';

  parent_table = 'SB_MAIL_BOX';
  child_field = 'SB_MAIL_BOX_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_MAIL_BOX_CONTENT';
  child_field = 'SB_MAIL_BOX_CONTENT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_MEDIA_TYPE_OPTION */
  child_table = 'SB_MEDIA_TYPE_OPTION';

  parent_table = 'SB_MEDIA_TYPE';
  child_field = 'SB_MEDIA_TYPE_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_QUEUE_PRIORITY */
  child_table = 'SB_QUEUE_PRIORITY';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_CLIENTS */
  child_table = 'SB_SEC_CLIENTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_GROUP_MEMBERS */
  child_table = 'SB_SEC_GROUP_MEMBERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_RIGHTS */
  child_table = 'SB_SEC_RIGHTS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SEC_ROW_FILTERS */
  child_table = 'SB_SEC_ROW_FILTERS';

  parent_table = 'SB_SEC_GROUPS';
  child_field = 'SB_SEC_GROUPS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES */
  child_table = 'SB_SERVICES';

  parent_table = 'SB_REPORTS';
  child_field = 'SB_REPORTS_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_DELIVERY_METHOD';
  child_field = 'SB_DELIVERY_METHOD_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_SERVICES_CALCULATIONS */
  child_table = 'SB_SERVICES_CALCULATIONS';

  parent_table = 'SB_SERVICES';
  child_field = 'SB_SERVICES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TASK */
  child_table = 'SB_TASK';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_TEAM_MEMBERS */
  child_table = 'SB_TEAM_MEMBERS';

  parent_table = 'SB_TEAM';
  child_field = 'SB_TEAM_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER */
  child_table = 'SB_USER';

  parent_table = 'SB_ACCOUNTANT';
  child_field = 'SB_ACCOUNTANT_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_NOTICE */
  child_table = 'SB_USER_NOTICE';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_USER_PREFERENCES */
  child_table = 'SB_USER_PREFERENCES';

  parent_table = 'SB_USER';
  child_field = 'SB_USER_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_VENDOR_DETAIL */
  child_table = 'SB_VENDOR_DETAIL';

  parent_table = 'SB_VENDOR';
  child_field = 'SB_VENDOR_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_VENDOR_DETAIL_VALUES */
  child_table = 'SB_VENDOR_DETAIL_VALUES';

  parent_table = 'SB_VENDOR_DETAIL';
  child_field = 'SB_VENDOR_DETAIL_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_ACA_GROUP_ADD_MEMBERS */
  child_table = 'SB_ACA_GROUP_ADD_MEMBERS';

  parent_table = 'SB_ACA_GROUP';
  child_field = 'SB_ACA_GROUP_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;

  /* Check SB_ADDITIONAL_INFO_VALUES */
  child_table = 'SB_ADDITIONAL_INFO_VALUES';

  parent_table = 'SB_ADDITIONAL_INFO_NAMES';
  child_field = 'SB_ADDITIONAL_INFO_NAMES_NBR';
  FOR SELECT child_nbr, invalid_parent_nbr FROM check_table_integrity(:child_table, :parent_table, :child_field)
      INTO :child_nbr, :parent_nbr
  DO
    SUSPEND;
END
^

GRANT EXECUTE ON PROCEDURE CHECK_INTEGRITY TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_aca_group_add_members(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_aca_group_add_members WHERE sb_aca_group_add_members_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ACA_GROUP_ADD_MEMBERS TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sb_aca_relief_codes(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sb_aca_relief_codes_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE aca_relief_code_description VARCHAR(40);
DECLARE VARIABLE p_aca_relief_code_description VARCHAR(40);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sb_aca_relief_codes_nbr , aca_relief_code_description
        FROM sb_aca_relief_codes
        ORDER BY sb_aca_relief_codes_nbr, effective_date
        INTO :rec_version, :effective_date, :sb_aca_relief_codes_nbr, :aca_relief_code_description
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sb_aca_relief_codes_nbr) OR (effective_date = '1/1/1900') OR (p_aca_relief_code_description IS DISTINCT FROM aca_relief_code_description)) THEN
      BEGIN
        curr_nbr = sb_aca_relief_codes_nbr;
        p_aca_relief_code_description = aca_relief_code_description;
      END
      ELSE
        DELETE FROM sb_aca_relief_codes WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, aca_relief_code_description
        FROM sb_aca_relief_codes
        WHERE sb_aca_relief_codes_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :aca_relief_code_description
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_aca_relief_code_description IS DISTINCT FROM aca_relief_code_description)) THEN
      BEGIN
        p_aca_relief_code_description = aca_relief_code_description;
      END
      ELSE
        DELETE FROM sb_aca_relief_codes WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE pack_sb_aca_relief_codes TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_aca_relief_codes(nbr INTEGER)
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  FOR SELECT rec_version FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = :nbr ORDER BY effective_date DESC FOR UPDATE WITH LOCK INTO rec_version
  DO
  BEGIN
    DELETE FROM sb_aca_relief_codes WHERE rec_version = :rec_version;
  END
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ACA_RELIEF_CODES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_sb_aca_offer_codes(nbr INTEGER)
AS
DECLARE VARIABLE curr_nbr INTEGER;
DECLARE VARIABLE transaction_nbr INTEGER;
DECLARE VARIABLE rec_version INTEGER;
DECLARE VARIABLE sb_aca_offer_codes_nbr INTEGER;
DECLARE VARIABLE effective_date DATE;
DECLARE VARIABLE aca_offer_code_description VARCHAR(40);
DECLARE VARIABLE p_aca_offer_code_description VARCHAR(40);
BEGIN
  IF (nbr IS NULL) THEN
  BEGIN
    curr_nbr = NULL;
    FOR SELECT rec_version, effective_date, sb_aca_offer_codes_nbr , aca_offer_code_description
        FROM sb_aca_offer_codes
        ORDER BY sb_aca_offer_codes_nbr, effective_date
        INTO :rec_version, :effective_date, :sb_aca_offer_codes_nbr, :aca_offer_code_description
    DO
    BEGIN
      IF ((curr_nbr IS DISTINCT FROM sb_aca_offer_codes_nbr) OR (effective_date = '1/1/1900') OR (p_aca_offer_code_description IS DISTINCT FROM aca_offer_code_description)) THEN
      BEGIN
        curr_nbr = sb_aca_offer_codes_nbr;
        p_aca_offer_code_description = aca_offer_code_description;
      END
      ELSE
        DELETE FROM sb_aca_offer_codes WHERE rec_version = :rec_version;
    END
  END

  ELSE
  BEGIN
    FOR SELECT rec_version, effective_date, aca_offer_code_description
        FROM sb_aca_offer_codes
        WHERE sb_aca_offer_codes_nbr = :nbr
        ORDER BY effective_date
        INTO :rec_version, :effective_date, :aca_offer_code_description
    DO
    BEGIN
      IF ((effective_date = '1/1/1900') OR (p_aca_offer_code_description IS DISTINCT FROM aca_offer_code_description)) THEN
      BEGIN
        p_aca_offer_code_description = aca_offer_code_description;
      END
      ELSE
        DELETE FROM sb_aca_offer_codes WHERE rec_version = :rec_version;
    END
  END
END
^

GRANT EXECUTE ON PROCEDURE pack_sb_aca_offer_codes TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_aca_offer_codes(nbr INTEGER)
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  FOR SELECT rec_version FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = :nbr ORDER BY effective_date DESC FOR UPDATE WITH LOCK INTO rec_version
  DO
  BEGIN
    DELETE FROM sb_aca_offer_codes WHERE rec_version = :rec_version;
  END
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ACA_OFFER_CODES TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_additional_info_names(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_additional_info_names WHERE sb_additional_info_names_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ADDITIONAL_INFO_NAMES TO EUSER
^

CREATE OR ALTER PROCEDURE del_sb_additional_info_values(nbr INTEGER)
AS
BEGIN
  DELETE FROM sb_additional_info_values WHERE sb_additional_info_values_nbr = :nbr;
END
^

GRANT EXECUTE ON PROCEDURE DEL_SB_ADDITIONAL_INFO_VALUES TO EUSER
^

CREATE OR ALTER PROCEDURE pack_all
AS
BEGIN
  EXECUTE PROCEDURE pack_sb_user (NULL);
  EXECUTE PROCEDURE pack_sb_aca_offer_codes (NULL);
  EXECUTE PROCEDURE pack_sb_aca_relief_codes (NULL);
END
^

GRANT EXECUTE ON PROCEDURE PACK_ALL TO EUSER
^

ALTER TABLE SB_ACA_GROUP_ADD_MEMBERS
ADD CONSTRAINT C_SB_ACA_GROUP_ADD_MEMBERS_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_ACA_OFFER_CODES
ADD CONSTRAINT C_SB_ACA_OFFER_CODES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_ACA_RELIEF_CODES
ADD CONSTRAINT C_SB_ACA_RELIEF_CODES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_ADDITIONAL_INFO_NAMES
ADD CONSTRAINT C_SB_ADDITIONAL_INFO_NAMES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

ALTER TABLE SB_ADDITIONAL_INFO_VALUES
ADD CONSTRAINT C_SB_ADDITIONAL_INFO_VALUES_1 CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
^

CREATE TRIGGER T_AD_SB_9 FOR SB After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, OLD.rec_version, OLD.sb_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 8, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 452, OLD.effective_until);

  /* SB_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 28, OLD.sb_name);

  /* ADDRESS1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 5, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 6, OLD.address2);

  /* CITY */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 29, OLD.city);

  /* STATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 30, OLD.state);

  /* ZIP_CODE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 20, OLD.zip_code);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 31, OLD.e_mail_address);

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);

  /* DEVELOPMENT_MODEM_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 33, OLD.development_modem_number);

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 35, OLD.development_ftp_password);

  /* EIN_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 49, OLD.ein_number);

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);

  /* EFTPS_BANK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);

  /* USE_PRENOTE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 53, OLD.use_prenote);

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);

  /* PAY_TAX_FROM_PAYABLES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);

  /* AR_EXPORT_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 57, OLD.ar_export_format);

  /* DEFAULT_CHECK_FORMAT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 58, OLD.default_check_format);

  /* MICR_FONT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 59, OLD.micr_font);

  /* MICR_HORIZONTAL_ADJUSTMENT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);

  /* AUTO_SAVE_MINUTES */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);

  /* PHONE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 36, OLD.phone);

  /* FAX */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 37, OLD.fax);

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 7, :blob_nbr);
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 4, :blob_nbr);
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 3, :blob_nbr);
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 27, OLD.ar_import_directory);

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 26, OLD.ach_directory);

  /* SB_URL */
  IF (OLD.sb_url IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 25, OLD.sb_url);

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 18, OLD.days_in_prenote);

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 9, :blob_nbr);
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);

  /* ERROR_SCREEN */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 48, OLD.error_screen);

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 15, OLD.pswd_min_length);

  /* PSWD_FORCE_MIXED */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);

  /* MISC_CHECK_FORM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 46, OLD.misc_check_form);

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 2, :blob_nbr);
  END

  /* MARK_LIABS_PAID_DEFAULT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);

  /* TRUST_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 44, OLD.trust_impound);

  /* TAX_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 43, OLD.tax_impound);

  /* DD_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 42, OLD.dd_impound);

  /* BILLING_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 40, OLD.billing_impound);

  /* WC_IMPOUND */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 39, OLD.wc_impound);

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);

  /* SB_EXCEPTION_PAYMENT_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);

  /* SB_ACH_FILE_LIMITATIONS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 543, OLD.dashboard_msg);

  /* EE_LOGIN_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 544, OLD.ee_login_type);

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 550, :blob_nbr);
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 551, :blob_nbr);
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 569, :blob_nbr);
  END

  /* THEME */
  IF (OLD.theme IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 571, OLD.theme);

  /* THEME_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@THEME_VALUE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 572, :blob_nbr);
  END

  /* WC_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.wc_terms_of_use_modify_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 573, OLD.wc_terms_of_use_modify_date);

  /* ANALYTICS_LICENSE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 574, OLD.analytics_license);

  /* SESSION_LOCKOUT */
  IF (OLD.session_lockout IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 619, OLD.session_lockout);

  /* SESSION_TIMEOUT */
  IF (OLD.session_timeout IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 620, OLD.session_timeout);

  /* ENFORCE_EE_DOB_DEFAULT */
  IF (OLD.enforce_ee_dob_default IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 689, OLD.enforce_ee_dob_default);

END

^

CREATE TRIGGER T_AU_SB_9 FOR SB After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(1, NEW.rec_version, NEW.sb_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 8, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 452, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_NAME */
  IF (OLD.sb_name IS DISTINCT FROM NEW.sb_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 28, OLD.sb_name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 5, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 6, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 29, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 30, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 20, OLD.zip_code);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 31, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* PARENT_SB_MODEM_NUMBER */
  IF (OLD.parent_sb_modem_number IS DISTINCT FROM NEW.parent_sb_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 32, OLD.parent_sb_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_MODEM_NUMBER */
  IF (OLD.development_modem_number IS DISTINCT FROM NEW.development_modem_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 33, OLD.development_modem_number);
    changes = changes + 1;
  END

  /* DEVELOPMENT_FTP_PASSWORD */
  IF (OLD.development_ftp_password IS DISTINCT FROM NEW.development_ftp_password) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 35, OLD.development_ftp_password);
    changes = changes + 1;
  END

  /* EIN_NUMBER */
  IF (OLD.ein_number IS DISTINCT FROM NEW.ein_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 49, OLD.ein_number);
    changes = changes + 1;
  END

  /* EFTPS_TIN_NUMBER */
  IF (OLD.eftps_tin_number IS DISTINCT FROM NEW.eftps_tin_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 50, OLD.eftps_tin_number);
    changes = changes + 1;
  END

  /* EFTPS_BANK_FORMAT */
  IF (OLD.eftps_bank_format IS DISTINCT FROM NEW.eftps_bank_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 51, OLD.eftps_bank_format);
    changes = changes + 1;
  END

  /* USE_PRENOTE */
  IF (OLD.use_prenote IS DISTINCT FROM NEW.use_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 53, OLD.use_prenote);
    changes = changes + 1;
  END

  /* IMPOUND_TRUST_MONIES_AS_RECEIV */
  IF (OLD.impound_trust_monies_as_receiv IS DISTINCT FROM NEW.impound_trust_monies_as_receiv) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 54, OLD.impound_trust_monies_as_receiv);
    changes = changes + 1;
  END

  /* PAY_TAX_FROM_PAYABLES */
  IF (OLD.pay_tax_from_payables IS DISTINCT FROM NEW.pay_tax_from_payables) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 55, OLD.pay_tax_from_payables);
    changes = changes + 1;
  END

  /* AR_EXPORT_FORMAT */
  IF (OLD.ar_export_format IS DISTINCT FROM NEW.ar_export_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 57, OLD.ar_export_format);
    changes = changes + 1;
  END

  /* DEFAULT_CHECK_FORMAT */
  IF (OLD.default_check_format IS DISTINCT FROM NEW.default_check_format) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 58, OLD.default_check_format);
    changes = changes + 1;
  END

  /* MICR_FONT */
  IF (OLD.micr_font IS DISTINCT FROM NEW.micr_font) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 59, OLD.micr_font);
    changes = changes + 1;
  END

  /* MICR_HORIZONTAL_ADJUSTMENT */
  IF (OLD.micr_horizontal_adjustment IS DISTINCT FROM NEW.micr_horizontal_adjustment) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 22, OLD.micr_horizontal_adjustment);
    changes = changes + 1;
  END

  /* AUTO_SAVE_MINUTES */
  IF (OLD.auto_save_minutes IS DISTINCT FROM NEW.auto_save_minutes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 23, OLD.auto_save_minutes);
    changes = changes + 1;
  END

  /* PHONE */
  IF (OLD.phone IS DISTINCT FROM NEW.phone) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 36, OLD.phone);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 37, OLD.fax);
    changes = changes + 1;
  END

  /* COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 7, :blob_nbr);
    changes = changes + 1;
  END

  /* INVOICE_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@INVOICE_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@INVOICE_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 4, :blob_nbr);
    changes = changes + 1;
  END

  /* TAX_COVER_LETTER_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@TAX_COVER_LETTER_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 3, :blob_nbr);
    changes = changes + 1;
  END

  /* AR_IMPORT_DIRECTORY */
  IF (OLD.ar_import_directory IS DISTINCT FROM NEW.ar_import_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 27, OLD.ar_import_directory);
    changes = changes + 1;
  END

  /* ACH_DIRECTORY */
  IF (OLD.ach_directory IS DISTINCT FROM NEW.ach_directory) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 26, OLD.ach_directory);
    changes = changes + 1;
  END

  /* SB_URL */
  IF (OLD.sb_url IS DISTINCT FROM NEW.sb_url) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 25, OLD.sb_url);
    changes = changes + 1;
  END

  /* DAYS_IN_PRENOTE */
  IF (OLD.days_in_prenote IS DISTINCT FROM NEW.days_in_prenote) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 18, OLD.days_in_prenote);
    changes = changes + 1;
  END

  /* SB_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@SB_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@SB_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 9, :blob_nbr);
    changes = changes + 1;
  END

  /* USER_PASSWORD_DURATION_IN_DAYS */
  IF (OLD.user_password_duration_in_days IS DISTINCT FROM NEW.user_password_duration_in_days) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 13, OLD.user_password_duration_in_days);
    changes = changes + 1;
  END

  /* DUMMY_TAX_CL_NBR */
  IF (OLD.dummy_tax_cl_nbr IS DISTINCT FROM NEW.dummy_tax_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 17, OLD.dummy_tax_cl_nbr);
    changes = changes + 1;
  END

  /* ERROR_SCREEN */
  IF (OLD.error_screen IS DISTINCT FROM NEW.error_screen) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 48, OLD.error_screen);
    changes = changes + 1;
  END

  /* PSWD_MIN_LENGTH */
  IF (OLD.pswd_min_length IS DISTINCT FROM NEW.pswd_min_length) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 15, OLD.pswd_min_length);
    changes = changes + 1;
  END

  /* PSWD_FORCE_MIXED */
  IF (OLD.pswd_force_mixed IS DISTINCT FROM NEW.pswd_force_mixed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 47, OLD.pswd_force_mixed);
    changes = changes + 1;
  END

  /* MISC_CHECK_FORM */
  IF (OLD.misc_check_form IS DISTINCT FROM NEW.misc_check_form) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 46, OLD.misc_check_form);
    changes = changes + 1;
  END

  /* VMR_CONFIDENCIAL_NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@VMR_CONFIDENCIAL_NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 2, :blob_nbr);
    changes = changes + 1;
  END

  /* MARK_LIABS_PAID_DEFAULT */
  IF (OLD.mark_liabs_paid_default IS DISTINCT FROM NEW.mark_liabs_paid_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 45, OLD.mark_liabs_paid_default);
    changes = changes + 1;
  END

  /* TRUST_IMPOUND */
  IF (OLD.trust_impound IS DISTINCT FROM NEW.trust_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 44, OLD.trust_impound);
    changes = changes + 1;
  END

  /* TAX_IMPOUND */
  IF (OLD.tax_impound IS DISTINCT FROM NEW.tax_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 43, OLD.tax_impound);
    changes = changes + 1;
  END

  /* DD_IMPOUND */
  IF (OLD.dd_impound IS DISTINCT FROM NEW.dd_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 42, OLD.dd_impound);
    changes = changes + 1;
  END

  /* BILLING_IMPOUND */
  IF (OLD.billing_impound IS DISTINCT FROM NEW.billing_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 40, OLD.billing_impound);
    changes = changes + 1;
  END

  /* WC_IMPOUND */
  IF (OLD.wc_impound IS DISTINCT FROM NEW.wc_impound) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 39, OLD.wc_impound);
    changes = changes + 1;
  END

  /* DAYS_PRIOR_TO_CHECK_DATE */
  IF (OLD.days_prior_to_check_date IS DISTINCT FROM NEW.days_prior_to_check_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 14, OLD.days_prior_to_check_date);
    changes = changes + 1;
  END

  /* SB_EXCEPTION_PAYMENT_TYPE */
  IF (OLD.sb_exception_payment_type IS DISTINCT FROM NEW.sb_exception_payment_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 38, OLD.sb_exception_payment_type);
    changes = changes + 1;
  END

  /* SB_MAX_ACH_FILE_TOTAL */
  IF (OLD.sb_max_ach_file_total IS DISTINCT FROM NEW.sb_max_ach_file_total) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 1, OLD.sb_max_ach_file_total);
    changes = changes + 1;
  END

  /* SB_ACH_FILE_LIMITATIONS */
  IF (OLD.sb_ach_file_limitations IS DISTINCT FROM NEW.sb_ach_file_limitations) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 41, OLD.sb_ach_file_limitations);
    changes = changes + 1;
  END

  /* SB_CL_NBR */
  IF (OLD.sb_cl_nbr IS DISTINCT FROM NEW.sb_cl_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 24, OLD.sb_cl_nbr);
    changes = changes + 1;
  END

  /* DASHBOARD_MSG */
  IF (OLD.dashboard_msg IS DISTINCT FROM NEW.dashboard_msg) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 543, OLD.dashboard_msg);
    changes = changes + 1;
  END

  /* EE_LOGIN_TYPE */
  IF (OLD.ee_login_type IS DISTINCT FROM NEW.ee_login_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 544, OLD.ee_login_type);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 550, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@WC_TERMS_OF_USE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@WC_TERMS_OF_USE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 551, :blob_nbr);
    changes = changes + 1;
  END

  /* ESS_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.ess_terms_of_use_modify_date IS DISTINCT FROM NEW.ess_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 568, OLD.ess_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ESS_LOGO */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@ESS_LOGO');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@ESS_LOGO', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 569, :blob_nbr);
    changes = changes + 1;
  END

  /* THEME */
  IF (OLD.theme IS DISTINCT FROM NEW.theme) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 571, OLD.theme);
    changes = changes + 1;
  END

  /* THEME_VALUE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@THEME_VALUE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@THEME_VALUE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 572, :blob_nbr);
    changes = changes + 1;
  END

  /* WC_TERMS_OF_USE_MODIFY_DATE */
  IF (OLD.wc_terms_of_use_modify_date IS DISTINCT FROM NEW.wc_terms_of_use_modify_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 573, OLD.wc_terms_of_use_modify_date);
    changes = changes + 1;
  END

  /* ANALYTICS_LICENSE */
  IF (OLD.analytics_license IS DISTINCT FROM NEW.analytics_license) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 574, OLD.analytics_license);
    changes = changes + 1;
  END

  /* SESSION_LOCKOUT */
  IF (OLD.session_lockout IS DISTINCT FROM NEW.session_lockout) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 619, OLD.session_lockout);
    changes = changes + 1;
  END

  /* SESSION_TIMEOUT */
  IF (OLD.session_timeout IS DISTINCT FROM NEW.session_timeout) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 620, OLD.session_timeout);
    changes = changes + 1;
  END

  /* ENFORCE_EE_DOB_DEFAULT */
  IF (OLD.enforce_ee_dob_default IS DISTINCT FROM NEW.enforce_ee_dob_default) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 689, OLD.enforce_ee_dob_default);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_1 FOR SB Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.ENFORCE_EE_DOB_DEFAULT is null OR CHAR_LENGTH(TRIM(new.ENFORCE_EE_DOB_DEFAULT))=0) THEN 
      new.ENFORCE_EE_DOB_DEFAULT = 'N'; 

  END 
END

^

CREATE TRIGGER T_AD_SB_BANKS_9 FOR SB_BANKS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(5, OLD.rec_version, OLD.sb_banks_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 119, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 460, OLD.effective_until);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 129, OLD.name);

  /* ADDRESS1 */
  IF (OLD.address1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 114, OLD.address1);

  /* ADDRESS2 */
  IF (OLD.address2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 115, OLD.address2);

  /* CITY */
  IF (OLD.city IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 130, OLD.city);

  /* STATE */
  IF (OLD.state IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 131, OLD.state);

  /* ZIP_CODE */
  IF (OLD.zip_code IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 121, OLD.zip_code);

  /* CONTACT1 */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 116, OLD.contact1);

  /* PHONE1 */
  IF (OLD.phone1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 132, OLD.phone1);

  /* DESCRIPTION1 */
  IF (OLD.description1 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 122, OLD.description1);

  /* CONTACT2 */
  IF (OLD.contact2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 117, OLD.contact2);

  /* PHONE2 */
  IF (OLD.phone2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 133, OLD.phone2);

  /* DESCRIPTION2 */
  IF (OLD.description2 IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 123, OLD.description2);

  /* FAX */
  IF (OLD.fax IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 134, OLD.fax);

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 124, OLD.fax_description);

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 135, OLD.e_mail_address);

  /* ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 140, OLD.aba_number);

  /* TOP_ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 125, OLD.top_aba_number);

  /* BOTTOM_ABA_NUMBER */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 126, OLD.bottom_aba_number);

  /* ADDENDA */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 136, OLD.addenda);

  /* CHECK_TEMPLATE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@CHECK_TEMPLATE');
  IF (blob_nbr IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', '@CHECK_TEMPLATE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 118, :blob_nbr);
  END

  /* USE_CHECK_TEMPLATE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 141, OLD.use_check_template);

  /* MICR_ACCOUNT_START_POSITION */
  IF (OLD.micr_account_start_position IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 127, OLD.micr_account_start_position);

  /* MICR_CHECK_NUMBER_START_POSITN */
  IF (OLD.micr_check_number_start_positn IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 128, OLD.micr_check_number_start_positn);

  /* FILLER */
  IF (OLD.filler IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 137, OLD.filler);

  /* PRINT_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 138, OLD.print_name);

  /* BRANCH_IDENTIFIER */
  IF (OLD.branch_identifier IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 139, OLD.branch_identifier);

  /* ALLOW_HYPHENS */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 142, OLD.allow_hyphens);

  /* ACH_DATE */
  IF (OLD.ach_date IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 659, OLD.ach_date);

  /* ACH_NUMBER */
  IF (OLD.ach_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 660, OLD.ach_number);

END

^

CREATE TRIGGER T_AU_SB_BANKS_9 FOR SB_BANKS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(5, NEW.rec_version, NEW.sb_banks_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 119, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 460, OLD.effective_until);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 129, OLD.name);
    changes = changes + 1;
  END

  /* ADDRESS1 */
  IF (OLD.address1 IS DISTINCT FROM NEW.address1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 114, OLD.address1);
    changes = changes + 1;
  END

  /* ADDRESS2 */
  IF (OLD.address2 IS DISTINCT FROM NEW.address2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 115, OLD.address2);
    changes = changes + 1;
  END

  /* CITY */
  IF (OLD.city IS DISTINCT FROM NEW.city) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 130, OLD.city);
    changes = changes + 1;
  END

  /* STATE */
  IF (OLD.state IS DISTINCT FROM NEW.state) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 131, OLD.state);
    changes = changes + 1;
  END

  /* ZIP_CODE */
  IF (OLD.zip_code IS DISTINCT FROM NEW.zip_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 121, OLD.zip_code);
    changes = changes + 1;
  END

  /* CONTACT1 */
  IF (OLD.contact1 IS DISTINCT FROM NEW.contact1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 116, OLD.contact1);
    changes = changes + 1;
  END

  /* PHONE1 */
  IF (OLD.phone1 IS DISTINCT FROM NEW.phone1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 132, OLD.phone1);
    changes = changes + 1;
  END

  /* DESCRIPTION1 */
  IF (OLD.description1 IS DISTINCT FROM NEW.description1) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 122, OLD.description1);
    changes = changes + 1;
  END

  /* CONTACT2 */
  IF (OLD.contact2 IS DISTINCT FROM NEW.contact2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 117, OLD.contact2);
    changes = changes + 1;
  END

  /* PHONE2 */
  IF (OLD.phone2 IS DISTINCT FROM NEW.phone2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 133, OLD.phone2);
    changes = changes + 1;
  END

  /* DESCRIPTION2 */
  IF (OLD.description2 IS DISTINCT FROM NEW.description2) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 123, OLD.description2);
    changes = changes + 1;
  END

  /* FAX */
  IF (OLD.fax IS DISTINCT FROM NEW.fax) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 134, OLD.fax);
    changes = changes + 1;
  END

  /* FAX_DESCRIPTION */
  IF (OLD.fax_description IS DISTINCT FROM NEW.fax_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 124, OLD.fax_description);
    changes = changes + 1;
  END

  /* E_MAIL_ADDRESS */
  IF (OLD.e_mail_address IS DISTINCT FROM NEW.e_mail_address) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 135, OLD.e_mail_address);
    changes = changes + 1;
  END

  /* ABA_NUMBER */
  IF (OLD.aba_number IS DISTINCT FROM NEW.aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 140, OLD.aba_number);
    changes = changes + 1;
  END

  /* TOP_ABA_NUMBER */
  IF (OLD.top_aba_number IS DISTINCT FROM NEW.top_aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 125, OLD.top_aba_number);
    changes = changes + 1;
  END

  /* BOTTOM_ABA_NUMBER */
  IF (OLD.bottom_aba_number IS DISTINCT FROM NEW.bottom_aba_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 126, OLD.bottom_aba_number);
    changes = changes + 1;
  END

  /* ADDENDA */
  IF (OLD.addenda IS DISTINCT FROM NEW.addenda) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 136, OLD.addenda);
    changes = changes + 1;
  END

  /* CHECK_TEMPLATE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@CHECK_TEMPLATE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@CHECK_TEMPLATE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 118, :blob_nbr);
    changes = changes + 1;
  END

  /* USE_CHECK_TEMPLATE */
  IF (OLD.use_check_template IS DISTINCT FROM NEW.use_check_template) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 141, OLD.use_check_template);
    changes = changes + 1;
  END

  /* MICR_ACCOUNT_START_POSITION */
  IF (OLD.micr_account_start_position IS DISTINCT FROM NEW.micr_account_start_position) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 127, OLD.micr_account_start_position);
    changes = changes + 1;
  END

  /* MICR_CHECK_NUMBER_START_POSITN */
  IF (OLD.micr_check_number_start_positn IS DISTINCT FROM NEW.micr_check_number_start_positn) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 128, OLD.micr_check_number_start_positn);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 137, OLD.filler);
    changes = changes + 1;
  END

  /* PRINT_NAME */
  IF (OLD.print_name IS DISTINCT FROM NEW.print_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 138, OLD.print_name);
    changes = changes + 1;
  END

  /* BRANCH_IDENTIFIER */
  IF (OLD.branch_identifier IS DISTINCT FROM NEW.branch_identifier) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 139, OLD.branch_identifier);
    changes = changes + 1;
  END

  /* ALLOW_HYPHENS */
  IF (OLD.allow_hyphens IS DISTINCT FROM NEW.allow_hyphens) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 142, OLD.allow_hyphens);
    changes = changes + 1;
  END

  /* ACH_DATE */
  IF (OLD.ach_date IS DISTINCT FROM NEW.ach_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 659, OLD.ach_date);
    changes = changes + 1;
  END

  /* ACH_NUMBER */
  IF (OLD.ach_number IS DISTINCT FROM NEW.ach_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 660, OLD.ach_number);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_SB_ENLIST_GROUPS_9 FOR SB_ENLIST_GROUPS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(13, OLD.rec_version, OLD.sb_enlist_groups_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 208, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 474, OLD.effective_until);

  /* SY_REPORT_GROUPS_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 209, OLD.sy_report_groups_nbr);

  /* MEDIA_TYPE */
  IF (OLD.media_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 212, OLD.media_type);

  /* PROCESS_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 211, OLD.process_type);

END

^

CREATE TRIGGER T_AU_SB_ENLIST_GROUPS_9 FOR SB_ENLIST_GROUPS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(13, NEW.rec_version, NEW.sb_enlist_groups_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 208, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 474, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_REPORT_GROUPS_NBR */
  IF (OLD.sy_report_groups_nbr IS DISTINCT FROM NEW.sy_report_groups_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 209, OLD.sy_report_groups_nbr);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 212, OLD.media_type);
    changes = changes + 1;
  END

  /* PROCESS_TYPE */
  IF (OLD.process_type IS DISTINCT FROM NEW.process_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 211, OLD.process_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AD_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(17, OLD.rec_version, OLD.sb_mail_box_content_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 252, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 482, OLD.effective_until);

  /* SB_MAIL_BOX_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 254, OLD.sb_mail_box_nbr);

  /* DESCRIPTION */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 257, OLD.description);

  /* FILE_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 256, OLD.file_name);

  /* MEDIA_TYPE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 258, OLD.media_type);

  /* PAGE_COUNT */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 253, OLD.page_count);

  /* USERSIDE_PRINTED */
  IF (OLD.userside_printed IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 649, OLD.userside_printed);

END

^

CREATE TRIGGER T_AU_SB_MAIL_BOX_CONTENT_9 FOR SB_MAIL_BOX_CONTENT After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(17, NEW.rec_version, NEW.sb_mail_box_content_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 252, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 482, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_MAIL_BOX_NBR */
  IF (OLD.sb_mail_box_nbr IS DISTINCT FROM NEW.sb_mail_box_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 254, OLD.sb_mail_box_nbr);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 257, OLD.description);
    changes = changes + 1;
  END

  /* FILE_NAME */
  IF (OLD.file_name IS DISTINCT FROM NEW.file_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 256, OLD.file_name);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 258, OLD.media_type);
    changes = changes + 1;
  END

  /* PAGE_COUNT */
  IF (OLD.page_count IS DISTINCT FROM NEW.page_count) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 253, OLD.page_count);
    changes = changes + 1;
  END

  /* USERSIDE_PRINTED */
  IF (OLD.userside_printed IS DISTINCT FROM NEW.userside_printed) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 649, OLD.userside_printed);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AU_SB_PAPER_INFO_9 FOR SB_PAPER_INFO After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(24, NEW.rec_version, NEW.sb_paper_info_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 289, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 496, OLD.effective_until);
    changes = changes + 1;
  END

  /* DESCRIPTION */
  IF (OLD.description IS DISTINCT FROM NEW.description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 294, OLD.description);
    changes = changes + 1;
  END

  /* HEIGHT */
  IF (OLD.height IS DISTINCT FROM NEW.height) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 292, OLD.height);
    changes = changes + 1;
  END

  /* WIDTH */
  IF (OLD.width IS DISTINCT FROM NEW.width) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 291, OLD.width);
    changes = changes + 1;
  END

  /* WEIGHT */
  IF (OLD.weight IS DISTINCT FROM NEW.weight) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 290, OLD.weight);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 295, OLD.media_type);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AU_SB_REPORT_WRITER_REPORT_9 FOR SB_REPORT_WRITER_REPORTS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
DECLARE VARIABLE blob_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(30, NEW.rec_version, NEW.sb_report_writer_reports_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 322, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 508, OLD.effective_until);
    changes = changes + 1;
  END

  /* REPORT_DESCRIPTION */
  IF (OLD.report_description IS DISTINCT FROM NEW.report_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 326, OLD.report_description);
    changes = changes + 1;
  END

  /* REPORT_TYPE */
  IF (OLD.report_type IS DISTINCT FROM NEW.report_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 328, OLD.report_type);
    changes = changes + 1;
  END

  /* REPORT_FILE */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@REPORT_FILE');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@REPORT_FILE', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 321, :blob_nbr);
    changes = changes + 1;
  END

  /* NOTES */
  blob_nbr = rdb$get_context('USER_TRANSACTION', '@NOTES');
  IF (blob_nbr is NOT NULL) THEN
  BEGIN
    IF (blob_nbr = 0) THEN
      blob_nbr = NULL;
    rdb$set_context('USER_TRANSACTION', '@NOTES', NULL);
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 320, :blob_nbr);
    changes = changes + 1;
  END

  /* MEDIA_TYPE */
  IF (OLD.media_type IS DISTINCT FROM NEW.media_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 327, OLD.media_type);
    changes = changes + 1;
  END

  /* CLASS_NAME */
  IF (OLD.class_name IS DISTINCT FROM NEW.class_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 325, OLD.class_name);
    changes = changes + 1;
  END

  /* ANCESTOR_CLASS_NAME */
  IF (OLD.ancestor_class_name IS DISTINCT FROM NEW.ancestor_class_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 324, OLD.ancestor_class_name);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AU_SB_SERVICES_9 FOR SB_SERVICES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(38, NEW.rec_version, NEW.sb_services_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 367, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 524, OLD.effective_until);
    changes = changes + 1;
  END

  /* SERVICE_NAME */
  IF (OLD.service_name IS DISTINCT FROM NEW.service_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 374, OLD.service_name);
    changes = changes + 1;
  END

  /* FREQUENCY */
  IF (OLD.frequency IS DISTINCT FROM NEW.frequency) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 377, OLD.frequency);
    changes = changes + 1;
  END

  /* MONTH_NUMBER */
  IF (OLD.month_number IS DISTINCT FROM NEW.month_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 372, OLD.month_number);
    changes = changes + 1;
  END

  /* BASED_ON_TYPE */
  IF (OLD.based_on_type IS DISTINCT FROM NEW.based_on_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 378, OLD.based_on_type);
    changes = changes + 1;
  END

  /* SB_REPORTS_NBR */
  IF (OLD.sb_reports_nbr IS DISTINCT FROM NEW.sb_reports_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 373, OLD.sb_reports_nbr);
    changes = changes + 1;
  END

  /* COMMISSION */
  IF (OLD.commission IS DISTINCT FROM NEW.commission) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 380, OLD.commission);
    changes = changes + 1;
  END

  /* SALES_TAXABLE */
  IF (OLD.sales_taxable IS DISTINCT FROM NEW.sales_taxable) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 381, OLD.sales_taxable);
    changes = changes + 1;
  END

  /* FILLER */
  IF (OLD.filler IS DISTINCT FROM NEW.filler) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 375, OLD.filler);
    changes = changes + 1;
  END

  /* PRODUCT_CODE */
  IF (OLD.product_code IS DISTINCT FROM NEW.product_code) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 366, OLD.product_code);
    changes = changes + 1;
  END

  /* WEEK_NUMBER */
  IF (OLD.week_number IS DISTINCT FROM NEW.week_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 382, OLD.week_number);
    changes = changes + 1;
  END

  /* SERVICE_TYPE */
  IF (OLD.service_type IS DISTINCT FROM NEW.service_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 383, OLD.service_type);
    changes = changes + 1;
  END

  /* MINIMUM_AMOUNT */
  IF (OLD.minimum_amount IS DISTINCT FROM NEW.minimum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 368, OLD.minimum_amount);
    changes = changes + 1;
  END

  /* MAXIMUM_AMOUNT */
  IF (OLD.maximum_amount IS DISTINCT FROM NEW.maximum_amount) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 369, OLD.maximum_amount);
    changes = changes + 1;
  END

  /* SB_DELIVERY_METHOD_NBR */
  IF (OLD.sb_delivery_method_nbr IS DISTINCT FROM NEW.sb_delivery_method_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 370, OLD.sb_delivery_method_nbr);
    changes = changes + 1;
  END

  /* TAX_TYPE */
  IF (OLD.tax_type IS DISTINCT FROM NEW.tax_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 376, OLD.tax_type);
    changes = changes + 1;
  END

  /* PARTNER_BILLING */
  IF (OLD.partner_billing IS DISTINCT FROM NEW.partner_billing) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 570, OLD.partner_billing);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_AUD_SB_ACA_GROUP_2 FOR SB_ACA_GROUP After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_aca_group WHERE sb_aca_group_nbr = OLD.sb_aca_group_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_aca_group WHERE sb_aca_group_nbr = OLD.sb_aca_group_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_ACA_GROUP_ADD_MEMBERS */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_aca_group_add_members
    WHERE (sb_aca_group_nbr = OLD.sb_aca_group_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_aca_group', OLD.sb_aca_group_nbr, 'sb_aca_group_add_members', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_aca_group_add_members
    WHERE (sb_aca_group_nbr = OLD.sb_aca_group_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_aca_group', OLD.sb_aca_group_nbr, 'sb_aca_group_add_members', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AD_SB_ACA_GROUP_ADD_MEMBER_9 FOR SB_ACA_GROUP_ADD_MEMBERS After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(57, OLD.rec_version, OLD.sb_aca_group_add_members_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 652, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 653, OLD.effective_until);

  /* SB_ACA_GROUP_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 654, OLD.sb_aca_group_nbr);

  /* ALE_COMPANY_NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 655, OLD.ale_company_name);

  /* ALE_EIN_NUMBER */
  IF (OLD.ale_ein_number IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 656, OLD.ale_ein_number);

  /* ALE_MAIN_COMPANY */
  IF (OLD.ale_main_company IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 657, OLD.ale_main_company);

  /* ALE_NUMBER_OF_FTES */
  IF (OLD.ale_number_of_ftes IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 658, OLD.ale_number_of_ftes);

END

^

CREATE TRIGGER T_AI_SB_ACA_GROUP_ADD_MEMBER_9 FOR SB_ACA_GROUP_ADD_MEMBERS After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(57, NEW.rec_version, NEW.sb_aca_group_add_members_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ACA_GROUP_ADD_MEMBER_9 FOR SB_ACA_GROUP_ADD_MEMBERS After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(57, NEW.rec_version, NEW.sb_aca_group_add_members_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 652, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 653, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_ACA_GROUP_NBR */
  IF (OLD.sb_aca_group_nbr IS DISTINCT FROM NEW.sb_aca_group_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 654, OLD.sb_aca_group_nbr);
    changes = changes + 1;
  END

  /* ALE_COMPANY_NAME */
  IF (OLD.ale_company_name IS DISTINCT FROM NEW.ale_company_name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 655, OLD.ale_company_name);
    changes = changes + 1;
  END

  /* ALE_EIN_NUMBER */
  IF (OLD.ale_ein_number IS DISTINCT FROM NEW.ale_ein_number) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 656, OLD.ale_ein_number);
    changes = changes + 1;
  END

  /* ALE_MAIN_COMPANY */
  IF (OLD.ale_main_company IS DISTINCT FROM NEW.ale_main_company) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 657, OLD.ale_main_company);
    changes = changes + 1;
  END

  /* ALE_NUMBER_OF_FTES */
  IF (OLD.ale_number_of_ftes IS DISTINCT FROM NEW.ale_number_of_ftes) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 658, OLD.ale_number_of_ftes);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ACA_GROUP_ADD_MEMBER_1 FOR SB_ACA_GROUP_ADD_MEMBERS Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.ALE_MAIN_COMPANY is null OR CHAR_LENGTH(TRIM(new.ALE_MAIN_COMPANY))=0) THEN 
      new.ALE_MAIN_COMPANY = 'N'; 

  END 
END

^

CREATE TRIGGER T_BIU_SB_ACA_GROUP_ADD_MEMBER_2 FOR SB_ACA_GROUP_ADD_MEMBERS Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_ACA_GROUP */
  IF ((NEW.sb_aca_group_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_aca_group_nbr IS DISTINCT FROM NEW.sb_aca_group_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_aca_group
    WHERE sb_aca_group_nbr = NEW.sb_aca_group_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_aca_group_add_members', NEW.sb_aca_group_add_members_nbr, 'sb_aca_group_nbr', NEW.sb_aca_group_nbr, 'sb_aca_group', NEW.effective_date);

    SELECT rec_version FROM sb_aca_group
    WHERE sb_aca_group_nbr = NEW.sb_aca_group_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_aca_group_add_members', NEW.sb_aca_group_add_members_nbr, 'sb_aca_group_nbr', NEW.sb_aca_group_nbr, 'sb_aca_group', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_SB_ACA_GROUP_ADD_MEMBER_1 FOR SB_ACA_GROUP_ADD_MEMBERS Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_aca_group_add_members_ver;

  IF (EXISTS (SELECT 1 FROM sb_aca_group_add_members WHERE sb_aca_group_add_members_nbr = NEW.sb_aca_group_add_members_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_aca_group_add_members', NEW.sb_aca_group_add_members_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_ACA_GROUP_ADD_MEMBER_1 FOR SB_ACA_GROUP_ADD_MEMBERS Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_aca_group_add_members_nbr IS DISTINCT FROM OLD.sb_aca_group_add_members_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_aca_group_add_members', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_ACA_OFFER_CODES_1 FOR SB_ACA_OFFER_CODES After Delete POSITION 1
AS
DECLARE VARIABLE effective_until DATE;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it's the first version record */
  IF (OLD.effective_date = '1/1/1900') THEN
  BEGIN
    IF (EXISTS (SELECT 1 FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = OLD.sb_aca_offer_codes_nbr AND effective_date > OLD.effective_date)) THEN
      EXECUTE PROCEDURE raise_table_error('sb_aca_offer_codes', OLD.rec_version, '', 'S5', 'First version record cannot be deleted.');
  END

  ELSE
  BEGIN
    /* Correct EFFECTIVE_UNTIL of previous version */
    effective_until = OLD.effective_until;
    SELECT effective_date FROM sb_aca_offer_codes
    WHERE sb_aca_offer_codes_nbr = OLD.sb_aca_offer_codes_nbr AND
          effective_date > OLD.effective_date
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :effective_until;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

    UPDATE sb_aca_offer_codes SET effective_until = :effective_until
    WHERE sb_aca_offer_codes_nbr = OLD.sb_aca_offer_codes_nbr AND effective_until = OLD.effective_date;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AD_SB_ACA_OFFER_CODES_9 FOR SB_ACA_OFFER_CODES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE aca_offer_code_description VARCHAR(40);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(58, OLD.rec_version, OLD.sb_aca_offer_codes_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', aca_offer_code_description  FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = OLD.sb_aca_offer_codes_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :aca_offer_code_description;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 663, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 664, OLD.effective_until);

  /* ACA_OFFER_CODE_DESCRIPTION */
  IF (last_record = 'Y' OR aca_offer_code_description IS DISTINCT FROM OLD.aca_offer_code_description) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 666, OLD.aca_offer_code_description);


  IF (last_record = 'Y') THEN
  BEGIN
    /* SY_FED_ACA_OFFER_CODES_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 665, OLD.sy_fed_aca_offer_codes_nbr);

  END
END

^

CREATE TRIGGER T_AIU_SB_ACA_OFFER_CODES_3 FOR SB_ACA_OFFER_CODES After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.sy_fed_aca_offer_codes_nbr IS DISTINCT FROM NEW.sy_fed_aca_offer_codes_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE sb_aca_offer_codes SET 
      sy_fed_aca_offer_codes_nbr = NEW.sy_fed_aca_offer_codes_nbr
      WHERE sb_aca_offer_codes_nbr = NEW.sb_aca_offer_codes_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AI_SB_ACA_OFFER_CODES_9 FOR SB_ACA_OFFER_CODES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(58, NEW.rec_version, NEW.sb_aca_offer_codes_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ACA_OFFER_CODES_1 FOR SB_ACA_OFFER_CODES After Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((OLD.effective_date IS DISTINCT FROM NEW.effective_date) AND
      (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL of old previous version */
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sb_aca_offer_codes SET effective_until = OLD.effective_until
    WHERE sb_aca_offer_codes_nbr = NEW.sb_aca_offer_codes_nbr AND effective_until = OLD.effective_date;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AU_SB_ACA_OFFER_CODES_9 FOR SB_ACA_OFFER_CODES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(58, NEW.rec_version, NEW.sb_aca_offer_codes_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 663, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 664, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_FED_ACA_OFFER_CODES_NBR */
  IF (OLD.sy_fed_aca_offer_codes_nbr IS DISTINCT FROM NEW.sy_fed_aca_offer_codes_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 665, OLD.sy_fed_aca_offer_codes_nbr);
    changes = changes + 1;
  END

  /* ACA_OFFER_CODE_DESCRIPTION */
  IF (OLD.aca_offer_code_description IS DISTINCT FROM NEW.aca_offer_code_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 666, OLD.aca_offer_code_description);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ACA_OFFER_CODES_3 FOR SB_ACA_OFFER_CODES Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SB_ACA_OFFER_CODES_1 */
  IF (INSERTING OR (OLD.sy_fed_aca_offer_codes_nbr IS DISTINCT FROM NEW.sy_fed_aca_offer_codes_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_aca_offer_codes WHERE sy_fed_aca_offer_codes_nbr = NEW.sy_fed_aca_offer_codes_nbr AND sb_aca_offer_codes_nbr <> NEW.sb_aca_offer_codes_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_aca_offer_codes', 'sy_fed_aca_offer_codes_nbr',
      CAST(NEW.sy_fed_aca_offer_codes_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_ACA_OFFER_CODES_1 FOR SB_ACA_OFFER_CODES Before Insert POSITION 1
AS
DECLARE VARIABLE new_nbr INTEGER;
DECLARE VARIABLE new_effective_date DATE;
DECLARE VARIABLE new_effective_until DATE;
DECLARE VARIABLE prev_rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_aca_offer_codes_ver;

  new_nbr = NEW.sb_aca_offer_codes_nbr;
  new_effective_date = NEW.effective_date;
  new_effective_until = NEW.effective_until;

  /* Calc EFFECTIVE_DATE of this version */
  IF (EXISTS (SELECT 1 FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = :new_nbr ROWS 1)) THEN
  BEGIN
    IF (new_effective_date IS NULL) THEN
      new_effective_date = CURRENT_DATE;
  END
  ELSE
    new_effective_date = '1/1/1900';

  /* Correct EFFECTIVE_UNTIL of previous version */
  prev_rec_version = NULL;
  SELECT rec_version FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = :new_nbr AND effective_date < :new_effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :prev_rec_version;

  IF (prev_rec_version IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sb_aca_offer_codes SET effective_until = :new_effective_date WHERE rec_version = :prev_rec_version;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END

  /* Calc EFFECTIVE_UNTIL of this version */
  SELECT effective_date FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = :new_nbr AND effective_date > :new_effective_date
  ORDER BY effective_date
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :new_effective_until;

  IF (new_effective_until IS NULL) THEN
    new_effective_until = '12/31/9999';

  /* Final assignment */
  NEW.effective_date = new_effective_date;
  NEW.effective_until = new_effective_until;
END

^

CREATE TRIGGER T_BU_SB_ACA_OFFER_CODES_1 FOR SB_ACA_OFFER_CODES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it is changing system fields */
  IF ((NEW.effective_date IS DISTINCT FROM OLD.effective_date) AND (OLD.effective_date = '1/1/1900')) THEN
    NEW.effective_date = OLD.effective_date;

  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_aca_offer_codes_nbr IS DISTINCT FROM OLD.sb_aca_offer_codes_nbr) OR
     (NEW.effective_until IS DISTINCT FROM OLD.effective_until)) THEN
  BEGIN
    IF (NEW.effective_until IS DISTINCT FROM OLD.effective_until) THEN
    BEGIN
      IF (EXISTS (SELECT 1 FROM sb_aca_offer_codes WHERE sb_aca_offer_codes_nbr = NEW.sb_aca_offer_codes_nbr AND effective_date = OLD.effective_until)) THEN
        NEW.effective_until = OLD.effective_until;
      ELSE
        IF (NEW.effective_until IS NULL) THEN
          NEW.effective_until = '12/31/9999';
    END
    ELSE
      EXECUTE PROCEDURE raise_table_error('sb_aca_offer_codes', NEW.rec_version, '', 'S1', 'System fields cannot be modified');
  END

  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL */
    NEW.effective_until = '12/31/9999';
    SELECT effective_date FROM sb_aca_offer_codes
    WHERE sb_aca_offer_codes_nbr = NEW.sb_aca_offer_codes_nbr AND
          effective_date > NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO NEW.effective_until;

    /* Correct EFFECTIVE_UNTIL of new previous version */
    rec_version = NULL;
    SELECT rec_version FROM sb_aca_offer_codes
    WHERE sb_aca_offer_codes_nbr = NEW.sb_aca_offer_codes_nbr AND
          effective_date < NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date DESC
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :rec_version;

    IF (rec_version IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
      UPDATE sb_aca_offer_codes SET effective_until = NEW.effective_date WHERE rec_version = :rec_version;
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_SB_ACA_RELIEF_CODES_1 FOR SB_ACA_RELIEF_CODES After Delete POSITION 1
AS
DECLARE VARIABLE effective_until DATE;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it's the first version record */
  IF (OLD.effective_date = '1/1/1900') THEN
  BEGIN
    IF (EXISTS (SELECT 1 FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = OLD.sb_aca_relief_codes_nbr AND effective_date > OLD.effective_date)) THEN
      EXECUTE PROCEDURE raise_table_error('sb_aca_relief_codes', OLD.rec_version, '', 'S5', 'First version record cannot be deleted.');
  END

  ELSE
  BEGIN
    /* Correct EFFECTIVE_UNTIL of previous version */
    effective_until = OLD.effective_until;
    SELECT effective_date FROM sb_aca_relief_codes
    WHERE sb_aca_relief_codes_nbr = OLD.sb_aca_relief_codes_nbr AND
          effective_date > OLD.effective_date
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :effective_until;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

    UPDATE sb_aca_relief_codes SET effective_until = :effective_until
    WHERE sb_aca_relief_codes_nbr = OLD.sb_aca_relief_codes_nbr AND effective_until = OLD.effective_date;

    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AD_SB_ACA_RELIEF_CODES_9 FOR SB_ACA_RELIEF_CODES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE last_record CHAR(1);
DECLARE VARIABLE aca_relief_code_description VARCHAR(40);
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(59, OLD.rec_version, OLD.sb_aca_relief_codes_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  last_record = 'Y';
  SELECT 'N', aca_relief_code_description  FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = OLD.sb_aca_relief_codes_nbr AND effective_date < OLD.effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :last_record, :aca_relief_code_description;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 669, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (last_record = 'Y' AND OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 670, OLD.effective_until);

  /* ACA_RELIEF_CODE_DESCRIPTION */
  IF (last_record = 'Y' OR aca_relief_code_description IS DISTINCT FROM OLD.aca_relief_code_description) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 672, OLD.aca_relief_code_description);


  IF (last_record = 'Y') THEN
  BEGIN
    /* SY_FED_ACA_RELIEF_CODES_NBR */
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 671, OLD.sy_fed_aca_relief_codes_nbr);

  END
END

^

CREATE TRIGGER T_AIU_SB_ACA_RELIEF_CODES_3 FOR SB_ACA_RELIEF_CODES After Insert or Update POSITION 3
AS
BEGIN
  /* VERSIONING */
  /* Updating non-versioned fields */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS DISTINCT FROM 1) THEN
  BEGIN
    IF ((INSERTING) OR
       (OLD.sy_fed_aca_relief_codes_nbr IS DISTINCT FROM NEW.sy_fed_aca_relief_codes_nbr)) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

      UPDATE sb_aca_relief_codes SET 
      sy_fed_aca_relief_codes_nbr = NEW.sy_fed_aca_relief_codes_nbr
      WHERE sb_aca_relief_codes_nbr = NEW.sb_aca_relief_codes_nbr AND rec_version <> NEW.rec_version;

      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AI_SB_ACA_RELIEF_CODES_9 FOR SB_ACA_RELIEF_CODES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(59, NEW.rec_version, NEW.sb_aca_relief_codes_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ACA_RELIEF_CODES_1 FOR SB_ACA_RELIEF_CODES After Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((OLD.effective_date IS DISTINCT FROM NEW.effective_date) AND
      (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL of old previous version */
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sb_aca_relief_codes SET effective_until = OLD.effective_until
    WHERE sb_aca_relief_codes_nbr = NEW.sb_aca_relief_codes_nbr AND effective_until = OLD.effective_date;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END
END

^

CREATE TRIGGER T_AU_SB_ACA_RELIEF_CODES_9 FOR SB_ACA_RELIEF_CODES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(59, NEW.rec_version, NEW.sb_aca_relief_codes_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 669, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 670, OLD.effective_until);
    changes = changes + 1;
  END

  /* SY_FED_ACA_RELIEF_CODES_NBR */
  IF (OLD.sy_fed_aca_relief_codes_nbr IS DISTINCT FROM NEW.sy_fed_aca_relief_codes_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 671, OLD.sy_fed_aca_relief_codes_nbr);
    changes = changes + 1;
  END

  /* ACA_RELIEF_CODE_DESCRIPTION */
  IF (OLD.aca_relief_code_description IS DISTINCT FROM NEW.aca_relief_code_description) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 672, OLD.aca_relief_code_description);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ACA_RELIEF_CODES_3 FOR SB_ACA_RELIEF_CODES Before Insert or Update POSITION 3
AS
BEGIN
  /* CONSTRAINTS */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check uniqueness for LK_SY_FED_ACA_RELIEF_CODES_1 */
  IF (INSERTING OR (OLD.sy_fed_aca_relief_codes_nbr IS DISTINCT FROM NEW.sy_fed_aca_relief_codes_nbr)) THEN
    IF (EXISTS(SELECT 1 FROM sb_aca_relief_codes WHERE sy_fed_aca_relief_codes_nbr = NEW.sy_fed_aca_relief_codes_nbr AND sb_aca_relief_codes_nbr <> NEW.sb_aca_relief_codes_nbr AND
       ((NEW.effective_date >= effective_date AND NEW.effective_date < effective_until) OR (NEW.effective_until > effective_date AND NEW.effective_until <= effective_until)))) THEN
      EXECUTE PROCEDURE raise_duplicate_record('sb_aca_relief_codes', 'sy_fed_aca_relief_codes_nbr',
      CAST(NEW.sy_fed_aca_relief_codes_nbr as VARCHAR(512)),
      NEW.effective_date,  NEW.effective_until);
END

^

CREATE TRIGGER T_BI_SB_ACA_RELIEF_CODES_1 FOR SB_ACA_RELIEF_CODES Before Insert POSITION 1
AS
DECLARE VARIABLE new_nbr INTEGER;
DECLARE VARIABLE new_effective_date DATE;
DECLARE VARIABLE new_effective_until DATE;
DECLARE VARIABLE prev_rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_aca_relief_codes_ver;

  new_nbr = NEW.sb_aca_relief_codes_nbr;
  new_effective_date = NEW.effective_date;
  new_effective_until = NEW.effective_until;

  /* Calc EFFECTIVE_DATE of this version */
  IF (EXISTS (SELECT 1 FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = :new_nbr ROWS 1)) THEN
  BEGIN
    IF (new_effective_date IS NULL) THEN
      new_effective_date = CURRENT_DATE;
  END
  ELSE
    new_effective_date = '1/1/1900';

  /* Correct EFFECTIVE_UNTIL of previous version */
  prev_rec_version = NULL;
  SELECT rec_version FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = :new_nbr AND effective_date < :new_effective_date
  ORDER BY effective_date DESC
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :prev_rec_version;

  IF (prev_rec_version IS NOT NULL) THEN
  BEGIN
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
    UPDATE sb_aca_relief_codes SET effective_until = :new_effective_date WHERE rec_version = :prev_rec_version;
    rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
  END

  /* Calc EFFECTIVE_UNTIL of this version */
  SELECT effective_date FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = :new_nbr AND effective_date > :new_effective_date
  ORDER BY effective_date
  ROWS 1
  FOR UPDATE WITH LOCK
  INTO :new_effective_until;

  IF (new_effective_until IS NULL) THEN
    new_effective_until = '12/31/9999';

  /* Final assignment */
  NEW.effective_date = new_effective_date;
  NEW.effective_until = new_effective_until;
END

^

CREATE TRIGGER T_BU_SB_ACA_RELIEF_CODES_1 FOR SB_ACA_RELIEF_CODES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  /* Check if it is changing system fields */
  IF ((NEW.effective_date IS DISTINCT FROM OLD.effective_date) AND (OLD.effective_date = '1/1/1900')) THEN
    NEW.effective_date = OLD.effective_date;

  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_aca_relief_codes_nbr IS DISTINCT FROM OLD.sb_aca_relief_codes_nbr) OR
     (NEW.effective_until IS DISTINCT FROM OLD.effective_until)) THEN
  BEGIN
    IF (NEW.effective_until IS DISTINCT FROM OLD.effective_until) THEN
    BEGIN
      IF (EXISTS (SELECT 1 FROM sb_aca_relief_codes WHERE sb_aca_relief_codes_nbr = NEW.sb_aca_relief_codes_nbr AND effective_date = OLD.effective_until)) THEN
        NEW.effective_until = OLD.effective_until;
      ELSE
        IF (NEW.effective_until IS NULL) THEN
          NEW.effective_until = '12/31/9999';
    END
    ELSE
      EXECUTE PROCEDURE raise_table_error('sb_aca_relief_codes', NEW.rec_version, '', 'S1', 'System fields cannot be modified');
  END

  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    /* Correct EFFECTIVE_UNTIL */
    NEW.effective_until = '12/31/9999';
    SELECT effective_date FROM sb_aca_relief_codes
    WHERE sb_aca_relief_codes_nbr = NEW.sb_aca_relief_codes_nbr AND
          effective_date > NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO NEW.effective_until;

    /* Correct EFFECTIVE_UNTIL of new previous version */
    rec_version = NULL;
    SELECT rec_version FROM sb_aca_relief_codes
    WHERE sb_aca_relief_codes_nbr = NEW.sb_aca_relief_codes_nbr AND
          effective_date < NEW.effective_date AND
          rec_version <> NEW.rec_version
    ORDER BY effective_date DESC
    ROWS 1
    FOR UPDATE WITH LOCK
    INTO :rec_version;

    IF (rec_version IS NOT NULL) THEN
    BEGIN
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);
      UPDATE sb_aca_relief_codes SET effective_until = NEW.effective_date WHERE rec_version = :rec_version;
      rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', NULL);
    END
  END
END

^

CREATE TRIGGER T_AD_SB_ADDITIONAL_INFO_NAME_9 FOR SB_ADDITIONAL_INFO_NAMES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(60, OLD.rec_version, OLD.sb_additional_info_names_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 675, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 676, OLD.effective_until);

  /* CATEGORY */
  IF (OLD.category IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 677, OLD.category);

  /* DATA_TYPE */
  IF (OLD.data_type IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 678, OLD.data_type);

  /* NAME */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 679, OLD.name);

  /* REQUIRED */
  IF (OLD.required IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 680, OLD.required);

  /* SEQ_NUM */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 681, OLD.seq_num);

  /* SHOW_IN_EVO_PAYROLL */
  IF (OLD.show_in_evo_payroll IS NOT NULL) THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 682, OLD.show_in_evo_payroll);

END

^

CREATE TRIGGER T_AI_SB_ADDITIONAL_INFO_NAME_9 FOR SB_ADDITIONAL_INFO_NAMES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(60, NEW.rec_version, NEW.sb_additional_info_names_nbr, 'I');
END

^

CREATE TRIGGER T_AUD_SB_ADDITIONAL_INFO_NAME_2 FOR SB_ADDITIONAL_INFO_NAMES After Update or Delete POSITION 2
AS
DECLARE VARIABLE child_rec_version INTEGER;
DECLARE VARIABLE check_period_b DATE;
DECLARE VARIABLE check_period_e DATE;
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

 IF (UPDATING AND
     (OLD.effective_date IS NOT DISTINCT FROM NEW.effective_date) AND
     (OLD.effective_until IS NOT DISTINCT FROM NEW.effective_until)) THEN
   EXIT;

  /* Check children only if entire effective period changed */
  SELECT MIN(effective_date) FROM sb_additional_info_names WHERE sb_additional_info_names_nbr = OLD.sb_additional_info_names_nbr INTO :check_period_b;
  IF (check_period_b is NULL) THEN
    check_period_b = '12/31/9999';
  ELSE IF (check_period_b > OLD.effective_date) THEN
    check_period_b = OLD.effective_date;
  ELSE
    check_period_b = NULL;

  SELECT MAX(effective_until) FROM sb_additional_info_names WHERE sb_additional_info_names_nbr = OLD.sb_additional_info_names_nbr INTO :check_period_e;
  IF (check_period_e is NULL) THEN
    check_period_e = '1/1/1900';
  ELSE IF (check_period_e < OLD.effective_until) THEN
    check_period_e = OLD.effective_until;
  ELSE
    check_period_e = NULL;

  /* Check children in SB_ADDITIONAL_INFO_VALUES */
  IF (check_period_b IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_additional_info_values
    WHERE (sb_additional_info_names_nbr = OLD.sb_additional_info_names_nbr)
    AND effective_date < :check_period_b
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_additional_info_names', OLD.sb_additional_info_names_nbr, 'sb_additional_info_values', OLD.effective_date, OLD.effective_until);
  END

  IF (check_period_e IS NOT NULL) THEN
  BEGIN
    SELECT rec_version FROM sb_additional_info_values
    WHERE (sb_additional_info_names_nbr = OLD.sb_additional_info_names_nbr)
    AND effective_until > :check_period_e
    ROWS 1
    INTO :child_rec_version;

    IF (ROW_COUNT > 0) THEN
      EXECUTE PROCEDURE raise_children_exist(
      'sb_additional_info_names', OLD.sb_additional_info_names_nbr, 'sb_additional_info_values', OLD.effective_date, OLD.effective_until);
  END
END

^

CREATE TRIGGER T_AU_SB_ADDITIONAL_INFO_NAME_9 FOR SB_ADDITIONAL_INFO_NAMES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(60, NEW.rec_version, NEW.sb_additional_info_names_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 675, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 676, OLD.effective_until);
    changes = changes + 1;
  END

  /* CATEGORY */
  IF (OLD.category IS DISTINCT FROM NEW.category) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 677, OLD.category);
    changes = changes + 1;
  END

  /* DATA_TYPE */
  IF (OLD.data_type IS DISTINCT FROM NEW.data_type) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 678, OLD.data_type);
    changes = changes + 1;
  END

  /* NAME */
  IF (OLD.name IS DISTINCT FROM NEW.name) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 679, OLD.name);
    changes = changes + 1;
  END

  /* REQUIRED */
  IF (OLD.required IS DISTINCT FROM NEW.required) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 680, OLD.required);
    changes = changes + 1;
  END

  /* SEQ_NUM */
  IF (OLD.seq_num IS DISTINCT FROM NEW.seq_num) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 681, OLD.seq_num);
    changes = changes + 1;
  END

  /* SHOW_IN_EVO_PAYROLL */
  IF (OLD.show_in_evo_payroll IS DISTINCT FROM NEW.show_in_evo_payroll) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 682, OLD.show_in_evo_payroll);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ADDITIONAL_INFO_NAME_1 FOR SB_ADDITIONAL_INFO_NAMES Before Insert or Update POSITION 1
AS
BEGIN
  /* DEFAULT VALUES */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (inserting or updating) THEN 
  BEGIN 
    IF (new.CATEGORY is null OR CHAR_LENGTH(TRIM(new.CATEGORY))=0) THEN 
      new.CATEGORY = 'C'; 

    IF (new.DATA_TYPE is null OR CHAR_LENGTH(TRIM(new.DATA_TYPE))=0) THEN 
      new.DATA_TYPE = 'S'; 

    IF (new.REQUIRED is null OR CHAR_LENGTH(TRIM(new.REQUIRED))=0) THEN 
      new.REQUIRED = 'N'; 

    IF (new.SHOW_IN_EVO_PAYROLL is null OR CHAR_LENGTH(TRIM(new.SHOW_IN_EVO_PAYROLL))=0) THEN 
      new.SHOW_IN_EVO_PAYROLL = 'N'; 

  END 
END

^

CREATE TRIGGER T_BI_SB_ADDITIONAL_INFO_NAME_1 FOR SB_ADDITIONAL_INFO_NAMES Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_additional_info_names_ver;

  IF (EXISTS (SELECT 1 FROM sb_additional_info_names WHERE sb_additional_info_names_nbr = NEW.sb_additional_info_names_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_additional_info_names', NEW.sb_additional_info_names_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_ADDITIONAL_INFO_NAME_1 FOR SB_ADDITIONAL_INFO_NAMES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_additional_info_names_nbr IS DISTINCT FROM OLD.sb_additional_info_names_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_additional_info_names', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_AD_SB_ADDITIONAL_INFO_VALU_9 FOR SB_ADDITIONAL_INFO_VALUES After Delete POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(61, OLD.rec_version, OLD.sb_additional_info_values_nbr, 'D')
  RETURNING nbr INTO :table_change_nbr;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date <> '1/1/1900') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 685, OLD.effective_date);

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until <> '12/31/9999') THEN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 686, OLD.effective_until);

  /* SB_ADDITIONAL_INFO_NAMES_NBR */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 687, OLD.sb_additional_info_names_nbr);

  /* INFO_VALUE */
  INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value) VALUES(:table_change_nbr, 688, OLD.info_value);

END

^

CREATE TRIGGER T_AI_SB_ADDITIONAL_INFO_VALU_9 FOR SB_ADDITIONAL_INFO_VALUES After Insert POSITION 9
AS
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type) VALUES(61, NEW.rec_version, NEW.sb_additional_info_values_nbr, 'I');
END

^

CREATE TRIGGER T_AU_SB_ADDITIONAL_INFO_VALU_9 FOR SB_ADDITIONAL_INFO_VALUES After Update POSITION 9
AS
DECLARE VARIABLE table_change_nbr INTEGER;
DECLARE VARIABLE changes INTEGER;
BEGIN
  /* AUDIT */
  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  INSERT INTO ev_table_change(ev_table_nbr, record_id, record_nbr, change_type)
  VALUES(61, NEW.rec_version, NEW.sb_additional_info_values_nbr, 'U')
  RETURNING nbr INTO :table_change_nbr;

  changes = 0;

  /* EFFECTIVE_DATE */
  IF (OLD.effective_date IS DISTINCT FROM NEW.effective_date) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 685, OLD.effective_date);
    changes = changes + 1;
  END

  /* EFFECTIVE_UNTIL */
  IF (OLD.effective_until IS DISTINCT FROM NEW.effective_until) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 686, OLD.effective_until);
    changes = changes + 1;
  END

  /* SB_ADDITIONAL_INFO_NAMES_NBR */
  IF (OLD.sb_additional_info_names_nbr IS DISTINCT FROM NEW.sb_additional_info_names_nbr) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 687, OLD.sb_additional_info_names_nbr);
    changes = changes + 1;
  END

  /* INFO_VALUE */
  IF (OLD.info_value IS DISTINCT FROM NEW.info_value) THEN
  BEGIN
    INSERT INTO ev_field_change(ev_table_change_nbr, ev_field_nbr, old_value)
    VALUES(:table_change_nbr, 688, OLD.info_value);
    changes = changes + 1;
  END


  /* Ignore idle change */
  IF (changes = 0) THEN
    DELETE FROM ev_table_change WHERE nbr = :table_change_nbr;
END

^

CREATE TRIGGER T_BIU_SB_ADDITIONAL_INFO_VALU_2 FOR SB_ADDITIONAL_INFO_VALUES Before Insert or Update POSITION 2
AS
DECLARE VARIABLE parent_rec_version INTEGER;
DECLARE VARIABLE forced_check CHAR(1);
BEGIN
  /* INTEGRITY */
  IF (rdb$get_context('USER_TRANSACTION', 'INT_CHANGE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS NOT DISTINCT FROM 1) THEN
    EXIT;

  IF (INSERTING OR
     (OLD.effective_date IS DISTINCT FROM NEW.effective_date) OR
     (OLD.effective_until IS DISTINCT FROM NEW.effective_until)) THEN
    forced_check = 'Y';
  ELSE
    forced_check = 'N';

  /* Check reference to SB_ADDITIONAL_INFO_NAMES */
  IF ((NEW.sb_additional_info_names_nbr IS NOT NULL) AND ((forced_check = 'Y') OR (OLD.sb_additional_info_names_nbr IS DISTINCT FROM NEW.sb_additional_info_names_nbr))) THEN
  BEGIN
    SELECT rec_version FROM sb_additional_info_names
    WHERE sb_additional_info_names_nbr = NEW.sb_additional_info_names_nbr AND
    effective_date <= NEW.effective_date and effective_until > NEW.effective_date
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_additional_info_values', NEW.sb_additional_info_values_nbr, 'sb_additional_info_names_nbr', NEW.sb_additional_info_names_nbr, 'sb_additional_info_names', NEW.effective_date);

    SELECT rec_version FROM sb_additional_info_names
    WHERE sb_additional_info_names_nbr = NEW.sb_additional_info_names_nbr AND
    effective_date < NEW.effective_until and effective_until >= NEW.effective_until
    FOR UPDATE WITH LOCK
    INTO :parent_rec_version;

    IF (ROW_COUNT = 0) THEN
      EXECUTE PROCEDURE raise_parent_not_exist(
      'sb_additional_info_values', NEW.sb_additional_info_values_nbr, 'sb_additional_info_names_nbr', NEW.sb_additional_info_names_nbr, 'sb_additional_info_names', NEW.effective_until - 1);
  END
END

^

CREATE TRIGGER T_BI_SB_ADDITIONAL_INFO_VALU_1 FOR SB_ADDITIONAL_INFO_VALUES Before Insert POSITION 1
AS
BEGIN
  /* VERSIONING */
  IF ((rdb$get_context('USER_TRANSACTION', 'MAINTENANCE') IS DISTINCT FROM 1) OR (NEW.rec_version IS NULL))  THEN
    NEW.rec_version = NEXT VALUE FOR g_sb_additional_info_values_ver;

  IF (EXISTS (SELECT 1 FROM sb_additional_info_values WHERE sb_additional_info_values_nbr = NEW.sb_additional_info_values_nbr)) THEN
    EXECUTE PROCEDURE raise_one_version_only('sb_additional_info_values', NEW.sb_additional_info_values_nbr);

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^

CREATE TRIGGER T_BU_SB_ADDITIONAL_INFO_VALU_1 FOR SB_ADDITIONAL_INFO_VALUES Before Update POSITION 1
AS
DECLARE VARIABLE rec_version INTEGER;
BEGIN
  /* VERSIONING */
  IF ((NEW.rec_version IS DISTINCT FROM OLD.rec_version) OR
     (NEW.sb_additional_info_values_nbr IS DISTINCT FROM OLD.sb_additional_info_values_nbr)) THEN
    EXECUTE PROCEDURE raise_table_error('sb_additional_info_values', NEW.rec_version, '', 'S1', 'System fields cannot be modified');

  NEW.effective_date = '1/1/1900';

  IF (NEW.effective_until IS NULL) THEN
    NEW.effective_until = '12/31/9999';
END

^


















COMMIT^



/* Add/Update tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_table (nbr, name, versioned) VALUES (57, 'SB_ACA_GROUP_ADD_MEMBERS', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (58, 'SB_ACA_OFFER_CODES', 'Y');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (59, 'SB_ACA_RELIEF_CODES', 'Y');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (60, 'SB_ADDITIONAL_INFO_NAMES', 'N');
  INSERT INTO ev_table (nbr, name, versioned) VALUES (61, 'SB_ADDITIONAL_INFO_VALUES', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^


/* Add/Update fields */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (1, 689, 'ENFORCE_EE_DOB_DEFAULT', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 659, 'ACH_DATE', 'D', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (5, 660, 'ACH_NUMBER', 'C', 1, NULL, 'N', 'N');
  UPDATE ev_field SET name = 'MEDIA_TYPE', field_type = 'V', len = 2, scale = NULL, required = 'N', versioned = 'N' WHERE nbr = 212;
  UPDATE ev_field SET name = 'MEDIA_TYPE', field_type = 'V', len = 2, scale = NULL, required = 'Y', versioned = 'N' WHERE nbr = 258;
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (17, 649, 'USERSIDE_PRINTED', 'T', NULL, NULL, 'N', 'N');
  UPDATE ev_field SET name = 'MEDIA_TYPE', field_type = 'V', len = 2, scale = NULL, required = 'Y', versioned = 'N' WHERE nbr = 295;
  UPDATE ev_field SET name = 'MEDIA_TYPE', field_type = 'V', len = 2, scale = NULL, required = 'Y', versioned = 'N' WHERE nbr = 327;
  UPDATE ev_field SET name = 'BASED_ON_TYPE', field_type = 'V', len = 2, scale = NULL, required = 'Y', versioned = 'N' WHERE nbr = 378;
  UPDATE ev_field SET name = 'PRODUCT_CODE', field_type = 'V', len = 10, scale = NULL, required = 'N', versioned = 'N' WHERE nbr = 366;
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 650, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 651, 'SB_ACA_GROUP_ADD_MEMBERS_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 652, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 653, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 654, 'SB_ACA_GROUP_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 655, 'ALE_COMPANY_NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 656, 'ALE_EIN_NUMBER', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 657, 'ALE_MAIN_COMPANY', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (57, 658, 'ALE_NUMBER_OF_FTES', 'I', NULL, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (58, 661, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (58, 662, 'SB_ACA_OFFER_CODES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (58, 663, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (58, 664, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (58, 665, 'SY_FED_ACA_OFFER_CODES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (58, 666, 'ACA_OFFER_CODE_DESCRIPTION', 'V', 40, NULL, 'Y', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (59, 667, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (59, 668, 'SB_ACA_RELIEF_CODES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (59, 669, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (59, 670, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (59, 671, 'SY_FED_ACA_RELIEF_CODES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (59, 672, 'ACA_RELIEF_CODE_DESCRIPTION', 'V', 40, NULL, 'Y', 'Y');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 673, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 674, 'SB_ADDITIONAL_INFO_NAMES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 675, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 676, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 677, 'CATEGORY', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 678, 'DATA_TYPE', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 679, 'NAME', 'V', 40, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 680, 'REQUIRED', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 681, 'SEQ_NUM', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (60, 682, 'SHOW_IN_EVO_PAYROLL', 'C', 1, NULL, 'N', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (61, 683, 'REC_VERSION', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (61, 684, 'SB_ADDITIONAL_INFO_VALUES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (61, 685, 'EFFECTIVE_DATE', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (61, 686, 'EFFECTIVE_UNTIL', 'D', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (61, 687, 'SB_ADDITIONAL_INFO_NAMES_NBR', 'I', NULL, NULL, 'Y', 'N');
  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES (61, 688, 'INFO_VALUE', 'V', 40, NULL, 'Y', 'N');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^





/* Update EV_DATABASE */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('17.0.0.0', 'Evolution Bureau Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT^

SET TERM ;^
    