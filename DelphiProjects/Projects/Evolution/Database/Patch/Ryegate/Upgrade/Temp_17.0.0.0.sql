/* Check current version */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE REQ_VER VARCHAR(11);
DECLARE VARIABLE VER VARCHAR(11);
BEGIN
  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN
    execute statement '"This script must be executed by SYSDBA"';

  REQ_VER = '16.0.0.1';
  VER = 'UNKNOWN';
  SELECT version
  FROM ev_database
  INTO :VER;

  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN
    execute statement '"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '"';
END^




ALTER TABLE TMP_CO
ADD LEGAL_NAME EV_STR75 
^

ALTER TABLE TMP_CO
ALTER COLUMN LEGAL_NAME POSITION 37
^

ALTER TABLE TMP_CO
ADD USE_DBA_ON_TAX_RETURN EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN USE_DBA_ON_TAX_RETURN POSITION 38
^

ALTER TABLE TMP_CO
ADD ENABLE_HR EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN ENABLE_HR POSITION 39
^

ALTER TABLE TMP_CO
ADD ADVANCED_HR_CORE EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN ADVANCED_HR_CORE POSITION 40
^

ALTER TABLE TMP_CO
ADD ADVANCED_HR_ATS EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN ADVANCED_HR_ATS POSITION 41
^

ALTER TABLE TMP_CO
ADD ADVANCED_HR_TALENT_MGMT EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN ADVANCED_HR_TALENT_MGMT POSITION 42
^

ALTER TABLE TMP_CO
ADD ADVANCED_HR_ONLINE_ENROLLMENT EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN ADVANCED_HR_ONLINE_ENROLLMENT POSITION 43
^

ALTER TABLE TMP_CO
ADD PAYROLL_PRODUCT EV_CHAR1 
^

ALTER TABLE TMP_CO
ALTER COLUMN PAYROLL_PRODUCT POSITION 44
^




COMMIT ^



/* Populate EV_DATABASE, EV_TABLE, EV_FIELD tables */
EXECUTE BLOCK
AS
BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  DELETE FROM ev_database;
  INSERT INTO ev_database (version, description) VALUES ('17.0.0.0', 'Evolution Temp Database');
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);
END
^

COMMIT ^

SET TERM ;^
