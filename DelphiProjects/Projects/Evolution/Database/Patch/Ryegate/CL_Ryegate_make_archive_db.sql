
SET TERM ^;

EXECUTE BLOCK
AS

BEGIN
  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

  update cl set cl_nbr = 690000680, CUSTOM_CLIENT_NUMBER = 'F677_ARCHIVE', NAME = 'SCRAMBLED: T_W_Ponessa_ARCHIVE', READ_ONLY = 'R' where CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL;

  update co set CUSTOM_COMPANY_NUMBER = 'F677_ARCHIVE', NAME = 'T W Ponessa_ARCHIVE' where CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL  and custom_company_number = 'F677';

  update EE set SELFSERVE_USERNAME = EE.SELFSERVE_USERNAME || '1' where CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL;


END
^

COMMIT
^


CREATE TRIGGER T_BD_EV_TABLE_CHANGE_99 FOR EV_TABLE_CHANGE ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_field_change WHERE ev_table_change_nbr = OLD.nbr;

END
^


CREATE TRIGGER T_BD_PR_99 FOR PR ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 139 and record_nbr = OLD.PR_NBR; 

END
^


CREATE TRIGGER T_BD_PR_CHECK_99 FOR PR_CHECK ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM CO_BANK_ACCOUNT_REGISTER WHERE PR_CHECK_NBR = OLD.PR_CHECK_NBR;
    DELETE FROM EE_TIME_OFF_ACCRUAL_OPER WHERE PR_CHECK_NBR = OLD.PR_CHECK_NBR;
    DELETE FROM PR_CHECK_LOCALS WHERE PR_CHECK_NBR = OLD.PR_CHECK_NBR;
    DELETE FROM PR_CHECK_STATES WHERE PR_CHECK_NBR = OLD.PR_CHECK_NBR;
    DELETE FROM PR_CHECK_SUI WHERE PR_CHECK_NBR = OLD.PR_CHECK_NBR;

    DELETE FROM PR_CHECK_LINES WHERE PR_CHECK_NBR = OLD.PR_CHECK_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 141 and record_nbr = OLD.PR_CHECK_NBR; 

  END

END
^

CREATE TRIGGER T_BD_PR_BATCH_99 FOR PR_BATCH ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN
    DELETE FROM EE_TIME_OFF_ACCRUAL_OPER WHERE PR_BATCH_NBR = OLD.PR_BATCH_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 140 and record_nbr = OLD.PR_BATCH_NBR; 

  END
END
^


CREATE TRIGGER T_BD_CO_PR_ACH_99 FOR CO_PR_ACH ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM CO_BANK_ACCOUNT_REGISTER WHERE CO_PR_ACH_NBR = OLD.CO_PR_ACH_NBR;
    DELETE FROM CL_BLOB WHERE CL_BLOB_NBR = OLD.CL_BLOB_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 86 and record_nbr = OLD.CO_PR_ACH_NBR; 

  END

END
^


CREATE TRIGGER T_BD_EE_TIME_OFF_ACCRUAL_99 FOR EE_TIME_OFF_ACCRUAL ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM EE_TIME_OFF_ACCRUAL_OPER WHERE EE_TIME_OFF_ACCRUAL_NBR = OLD.EE_TIME_OFF_ACCRUAL_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 136 and record_nbr = OLD.EE_TIME_OFF_ACCRUAL_NBR; 

  END
END
^

CREATE TRIGGER T_BD_CO_BILLING_HISTORY_99 FOR CO_BILLING_HISTORY ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM CO_BILLING_HISTORY_DETAIL WHERE CO_BILLING_HISTORY_NBR = OLD.CO_BILLING_HISTORY_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 45 and record_nbr = OLD.CO_BILLING_HISTORY_NBR; 

  END
END
^

CREATE TRIGGER T_BD_CO_TAX_DEPOSITS_99 FOR CO_TAX_DEPOSITS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM co_bank_account_register WHERE co_tax_deposits_nbr = OLD.CO_TAX_DEPOSITS_NBR;

    DELETE FROM CO_STATE_TAX_LIABILITIES WHERE CO_TAX_DEPOSITS_NBR = OLD.CO_TAX_DEPOSITS_NBR;
    DELETE FROM CO_SUI_LIABILITIES WHERE CO_TAX_DEPOSITS_NBR = OLD.CO_TAX_DEPOSITS_NBR;
    DELETE FROM CO_LOCAL_TAX_LIABILITIES WHERE CO_TAX_DEPOSITS_NBR = OLD.CO_TAX_DEPOSITS_NBR;
    DELETE FROM CO_FED_TAX_LIABILITIES WHERE CO_TAX_DEPOSITS_NBR = OLD.CO_TAX_DEPOSITS_NBR;

    DELETE FROM PR_MISCELLANEOUS_CHECKS WHERE co_tax_deposits_nbr = OLD.CO_TAX_DEPOSITS_NBR;

    DELETE FROM co_tax_payment_ach WHERE co_tax_deposits_nbr = OLD.CO_TAX_DEPOSITS_NBR;


    DELETE FROM ev_table_change WHERE ev_table_nbr = 103 and record_nbr = OLD.CO_TAX_DEPOSITS_NBR; 

  END

END
^

CREATE TRIGGER T_BD_PR_MISCELLANEOUS_CHECKS_99 FOR PR_MISCELLANEOUS_CHECKS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN
    DELETE FROM co_bank_account_register WHERE pr_miscellaneous_checks_nbr = OLD.PR_MISCELLANEOUS_CHECKS_NBR;


    DELETE FROM ev_table_change WHERE ev_table_nbr = 147 and record_nbr = OLD.PR_MISCELLANEOUS_CHECKS_NBR; 

  END

END
^

CREATE TRIGGER T_BD_PR_SCHEDULED_EVENT_99 FOR PR_SCHEDULED_EVENT ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM PR_SCHEDULED_EVENT_BATCH WHERE PR_SCHEDULED_EVENT_NBR = OLD.PR_SCHEDULED_EVENT_NBR;


    DELETE FROM ev_table_change WHERE ev_table_nbr = 151 and record_nbr = OLD.PR_SCHEDULED_EVENT_NBR; 

  END
END
^

CREATE TRIGGER T_BD_EE_TIME_OFF_ACC_OPER_99 FOR EE_TIME_OFF_ACCRUAL_OPER ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 137 and record_nbr = OLD.EE_TIME_OFF_ACCRUAL_OPER_NBR; 

END
^


CREATE TRIGGER T_BD_CO_BILL_HIST_DETAIL_99 FOR CO_BILLING_HISTORY_DETAIL ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 46 and record_nbr = OLD.CO_BILLING_HISTORY_DETAIL_NBR; 

END
^

CREATE TRIGGER T_BD_CO_BANK_ACC_REG_99 FOR CO_BANK_ACCOUNT_REGISTER ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 41 and record_nbr = OLD.CO_BANK_ACCOUNT_REGISTER_NBR; 

END
^

CREATE TRIGGER T_BD_PR_CHECK_LOCALS_99 FOR PR_CHECK_LOCALS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 144 and record_nbr = OLD.PR_CHECK_LOCALS_NBR; 

END
^


CREATE TRIGGER T_BD_PR_CHECK_STATES_99 FOR PR_CHECK_STATES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 145 and record_nbr = OLD.PR_CHECK_STATES_NBR; 

END
^


CREATE TRIGGER T_BD_PR_CHECK_SUI_99 FOR PR_CHECK_SUI ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 146 and record_nbr = OLD.PR_CHECK_SUI_NBR; 

END
^

CREATE TRIGGER T_BD_PR_CHECK_LINES_99 FOR PR_CHECK_LINES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 142 and record_nbr = OLD.PR_CHECK_LINES_NBR; 

  DELETE FROM PR_CHECK_LINE_LOCALS WHERE PR_CHECK_LINES_NBR = OLD.PR_CHECK_LINES_NBR;


END
^

CREATE TRIGGER T_BD_PR_CHECK_LINE_LOCALS_99 FOR PR_CHECK_LINE_LOCALS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 143 and record_nbr = OLD.PR_CHECK_LINE_LOCALS_NBR; 

END
^

CREATE TRIGGER T_BD_CO_FED_TAX_LIABILITIES_99 FOR CO_FED_TAX_LIABILITIES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 60 and record_nbr = OLD.CO_FED_TAX_LIABILITIES_NBR; 

END
^

CREATE TRIGGER T_BD_CO_STATE_TAX_LIAB_99 FOR CO_STATE_TAX_LIABILITIES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 99 and record_nbr = OLD.CO_STATE_TAX_LIABILITIES_NBR; 

END
^

CREATE TRIGGER T_BD_CO_SUI_LIABILITIES_99 FOR CO_SUI_LIABILITIES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 102 and record_nbr = OLD.CO_SUI_LIABILITIES_NBR; 

END
^


CREATE TRIGGER T_BD_CO_LOCAL_TAX_LIAB_99 FOR CO_LOCAL_TAX_LIABILITIES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 81 and record_nbr = OLD.CO_LOCAL_TAX_LIABILITIES_NBR; 

END
^


CREATE TRIGGER T_BD_CO_TAX_PAYMENT_ACH_99 FOR CO_TAX_PAYMENT_ACH ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 104 and record_nbr = OLD.CO_TAX_PAYMENT_ACH_NBR; 

END
^


CREATE TRIGGER T_BD_PR_REPORTS_99 FOR PR_REPORTS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 148 and record_nbr = OLD.PR_REPORTS_NBR; 

END
^

CREATE TRIGGER T_BD_PR_REPRINT_HISTORY_99 FOR PR_REPRINT_HISTORY ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN
    DELETE FROM PR_REPRINT_HISTORY_DETAIL WHERE PR_REPRINT_HISTORY_NBR = OLD.PR_REPRINT_HISTORY_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 149 and record_nbr = OLD.PR_REPRINT_HISTORY_NBR; 

  END
END
^

CREATE TRIGGER T_BD_PR_REPRINT_HIST_DTL_99 FOR PR_REPRINT_HISTORY_DETAIL ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 150 and record_nbr = OLD.PR_REPRINT_HISTORY_DETAIL_NBR; 

END
^



CREATE TRIGGER T_BD_PR_SCHEDULED_E_DS_99 FOR PR_SCHEDULED_E_DS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 153 and record_nbr = OLD.PR_SCHEDULED_E_DS_NBR; 

END
^


CREATE TRIGGER T_BD_PR_SCHD_EVENT_BATCH_99 FOR PR_SCHEDULED_EVENT_BATCH ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 152 and record_nbr = OLD.PR_SCHEDULED_EVENT_BATCH_NBR; 

END
^


CREATE TRIGGER T_BD_PR_SERVICES_99 FOR PR_SERVICES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 154 and record_nbr = OLD.PR_SERVICES_NBR; 

END
^


CREATE TRIGGER T_BD_EE_99 FOR EE ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 115 and record_nbr = OLD.EE_NBR; 

END
^

CREATE TRIGGER T_BD_EE_BENEFITS_99 FOR EE_BENEFITS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM EE_BENEFICIARY WHERE ee_benefits_nbr = OLD.EE_BENEFITS_NBR;
    DELETE FROM ee_scheduled_e_ds WHERE ee_benefits_nbr = OLD.EE_BENEFITS_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 118 and record_nbr = OLD.EE_BENEFITS_NBR; 

  END
END
^

CREATE TRIGGER T_BD_EE_CHILD_SUPP_CASES_99 FOR EE_CHILD_SUPPORT_CASES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM ee_scheduled_e_ds WHERE ee_child_support_cases_nbr = OLD.EE_CHILD_SUPPORT_CASES_NBR;


    DELETE FROM ev_table_change WHERE ev_table_nbr = 121 and record_nbr = OLD.EE_CHILD_SUPPORT_CASES_NBR; 

  END
END
^

CREATE TRIGGER T_BD_EE_DIRECT_DEPOSIT_99 FOR EE_DIRECT_DEPOSIT ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM co_manual_ach WHERE to_ee_direct_deposit_nbr = OLD.EE_DIRECT_DEPOSIT_NBR;
    DELETE FROM ee_scheduled_e_ds WHERE ee_direct_deposit_nbr = OLD.EE_DIRECT_DEPOSIT_NBR;


    DELETE FROM ev_table_change WHERE ev_table_nbr = 122 and record_nbr = OLD.EE_DIRECT_DEPOSIT_NBR; 

  END
END
^

CREATE TRIGGER T_BD_CO_MANUAL_ACH_99 FOR CO_MANUAL_ACH ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN

    DELETE FROM co_bank_account_register WHERE co_manual_ach_nbr = OLD.CO_MANUAL_ACH_NBR;


    DELETE FROM ev_table_change WHERE ev_table_nbr = 82 and record_nbr = OLD.CO_MANUAL_ACH_NBR; 

  END
END
^


CREATE TRIGGER T_BD_CO_GROUP_MANAGER_99 FOR CO_GROUP_MANAGER ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 63 and record_nbr = OLD.CO_GROUP_MANAGER_NBR; 

END
^


CREATE TRIGGER T_BD_CO_GROUP_MEMBER_99 FOR CO_GROUP_MEMBER ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 64 and record_nbr = OLD.CO_GROUP_MEMBER_NBR; 

END
^


CREATE TRIGGER T_BD_CO_TAX_RETURN_RUNS_99 FOR CO_TAX_RETURN_RUNS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN
    DELETE FROM CL_BLOB WHERE CL_BLOB_NBR = OLD.CL_BLOB_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 106 and record_nbr = OLD.CO_TAX_RETURN_RUNS_NBR; 

  END
END
^


CREATE TRIGGER T_BD_EE_ADDITIONAL_INFO_99 FOR EE_ADDITIONAL_INFO ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 116 and record_nbr = OLD.EE_ADDITIONAL_INFO_NBR; 

END
^

CREATE TRIGGER T_BD_EE_AUTOLABOR_DIST_99 FOR EE_AUTOLABOR_DISTRIBUTION ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 117 and record_nbr = OLD.EE_AUTOLABOR_DISTRIBUTION_NBR; 

END
^


CREATE TRIGGER T_BD_EE_BENEFIT_PAYMENT_99 FOR EE_BENEFIT_PAYMENT ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 119 and record_nbr = OLD.EE_BENEFIT_PAYMENT_NBR; 

END
^

CREATE TRIGGER T_BD_EE_BENEFIT_REFUSAL_99 FOR EE_BENEFIT_REFUSAL ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 170 and record_nbr = OLD.EE_BENEFIT_REFUSAL_NBR; 

END
^


CREATE TRIGGER T_BD_EE_BENEFICIARY_99 FOR EE_BENEFICIARY ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
  BEGIN
    DELETE FROM EE_BENEFICIARY_PLAN_DATES WHERE EE_BENEFICIARY_NBR = OLD.EE_BENEFICIARY_NBR;

    DELETE FROM ev_table_change WHERE ev_table_nbr = 169 and record_nbr = OLD.EE_BENEFICIARY_NBR; 
  END
END
^

CREATE TRIGGER T_BD_EE_BENEFICIARY_PLAN_99 FOR EE_BENEFICIARY_PLAN_DATES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 188 and record_nbr = OLD.EE_BENEFICIARY_PLAN_DATES_NBR; 

END
^


CREATE TRIGGER T_BD_EE_SCHEDULED_E_DS_99 FOR EE_SCHEDULED_E_DS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 134 and record_nbr = OLD.EE_SCHEDULED_E_DS_NBR; 

END
^


CREATE TRIGGER T_BD_EE_CHANGE_REQUEST_99 FOR EE_CHANGE_REQUEST ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 120 and record_nbr = OLD.EE_CHANGE_REQUEST_NBR; 

END
^

CREATE TRIGGER T_BD_EE_EMERGENCY_CONTACTS_99 FOR EE_EMERGENCY_CONTACTS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 123 and record_nbr = OLD.EE_EMERGENCY_CONTACTS_NBR; 

END
^
  
CREATE TRIGGER T_BD_EE_LOCALS_99 FOR EE_LOCALS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 130 and record_nbr = OLD.EE_LOCALS_NBR; 

END
^

CREATE TRIGGER T_BD_EE_PENS_FUND_SPLITS_99 FOR EE_PENSION_FUND_SPLITS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 131 and record_nbr = OLD.EE_PENSION_FUND_SPLITS_NBR; 

END
^

CREATE TRIGGER T_BD_EE_PIECE_WORK_99 FOR EE_PIECE_WORK ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 132 and record_nbr = OLD.EE_PIECE_WORK_NBR; 

END
^

CREATE TRIGGER T_BD_EE_RATES_99 FOR EE_RATES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 133 and record_nbr = OLD.EE_RATES_NBR; 

END
^


CREATE TRIGGER T_BD_EE_STATES_99 FOR EE_STATES ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 135 and record_nbr = OLD.EE_STATES_NBR; 

END
^


CREATE TRIGGER T_BD_EE_WORK_SHIFTS_99 FOR EE_WORK_SHIFTS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 138 and record_nbr = OLD.EE_WORK_SHIFTS_NBR; 

END
^


CREATE TRIGGER T_BD_EE_HR_ATTENDANCE_99 FOR EE_HR_ATTENDANCE ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 124 and record_nbr = OLD.EE_HR_ATTENDANCE_NBR; 

END
^


CREATE TRIGGER T_BD_EE_HR_CAR_99 FOR EE_HR_CAR ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 125 and record_nbr = OLD.EE_HR_CAR_NBR; 

END
^

CREATE TRIGGER T_BD_EE_HR_CO_PROV_EDUCATN_99 FOR EE_HR_CO_PROVIDED_EDUCATN ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 126 and record_nbr = OLD.EE_HR_CO_PROVIDED_EDUCATN_NBR; 

END
^


CREATE TRIGGER T_BD_EE_HR_INJURY_OCCURRENCE_99 FOR EE_HR_INJURY_OCCURRENCE ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 127 and record_nbr = OLD.EE_HR_INJURY_OCCURRENCE_NBR; 

END
^


CREATE TRIGGER T_BD_EE_HR_PROPERTY_TRACKING_99 FOR EE_HR_PROPERTY_TRACKING ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 128 and record_nbr = OLD.EE_HR_PROPERTY_TRACKING_NBR; 

END
^

CREATE TRIGGER T_BD_EE_SIGNATURE_99 FOR EE_SIGNATURE ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 171 and record_nbr = OLD.EE_SIGNATURE_NBR; 

END
^

CREATE TRIGGER T_BD_EE_SPREADREPORT_99 FOR EE_SPREADREPORT ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 187 and record_nbr = OLD.EE_SPREADREPORT_NBR; 
END
^


CREATE TRIGGER T_BD_EE_HR_PERFOR_RATINGS_99 FOR EE_HR_PERFORMANCE_RATINGS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 128 and record_nbr = OLD.EE_HR_PERFORMANCE_RATINGS_NBR; 

END
^


CREATE TRIGGER T_BD_CL_PERSON_99 FOR CL_PERSON ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 27 and record_nbr = OLD.CL_PERSON_NBR; 

END
^


CREATE TRIGGER T_BD_CL_PERSON_DEPEND_99 FOR CL_PERSON_DEPENDENTS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 28 and record_nbr = OLD.CL_PERSON_DEPENDENTS_NBR; 

END
^

CREATE TRIGGER T_BD_CL_PERSON_DOCUMENTS_99 FOR CL_PERSON_DOCUMENTS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 29 and record_nbr = OLD.CL_PERSON_DOCUMENTS_NBR; 

END
^

CREATE TRIGGER T_BD_CL_HR_PERSON_EDU_99 FOR CL_HR_PERSON_EDUCATION ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 18 and record_nbr = OLD.CL_HR_PERSON_EDUCATION_NBR; 

END
^

CREATE TRIGGER T_BD_CL_HR_PERSON_HAND_99 FOR CL_HR_PERSON_HANDICAPS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 19 and record_nbr = OLD.CL_HR_PERSON_HANDICAPS_NBR; 

END
^

CREATE TRIGGER T_BD_CL_HR_SKILLS_99 FOR CL_HR_SKILLS ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 23 and record_nbr = OLD.CL_HR_SKILLS_NBR; 

END
^
    
CREATE TRIGGER T_BD_CO_HR_APPLICANT_99 FOR CO_HR_APPLICANT ACTIVE BEFORE DELETE POSITION 99 AS
BEGIN

  IF (rdb$get_context('USER_TRANSACTION', 'INT_SPLIT') IS NOT DISTINCT FROM 1) THEN
    DELETE FROM ev_table_change WHERE ev_table_nbr = 65 and record_nbr = OLD.CO_HR_APPLICANT_NBR; 

END
^

COMMIT
^

                                    
CREATE OR ALTER procedure KEEP_ONLY_ARCHIVED_RECORDS (
    CUTOFF_DATE date)
as
declare variable PR_NBR integer;
declare variable EE_NBR integer;
declare variable CO_TAX_DEPOSITS_NBR integer;
declare variable PR_MISCELLANEOUS_CHECKS_NBR integer;

declare PAYROLLS_TO_DELETE cursor for (
    select PR_NBR from PR 
    where CURRENT_DATE >= EFFECTIVE_DATE and CURRENT_DATE  < EFFECTIVE_UNTIL and
          CHECK_DATE >= :CUTOFF_DATE);

declare EES_TO_DELETE cursor for (
    select EE_NBR from EE 
        where CURRENT_DATE  >= EFFECTIVE_DATE and CURRENT_DATE < EFFECTIVE_UNTIL and
          not exists(select P.PR_NBR from PR P, PR_CHECK C
                     where CURRENT_DATE  >= p.EFFECTIVE_DATE and CURRENT_DATE  < p.EFFECTIVE_UNTIL and
                           CURRENT_DATE  >= C.EFFECTIVE_DATE and CURRENT_DATE  < C.EFFECTIVE_UNTIL and
                           P.PR_NBR = C.PR_NBR and
                           C.EE_NBR = EE.EE_NBR and
                           P.CHECK_DATE < :CUTOFF_DATE));
BEGIN

  rdb$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);
  rdb$set_context('USER_TRANSACTION', 'INT_CHANGE', 1);

  rdb$set_context('USER_TRANSACTION', 'INT_SPLIT', 1);


  OPEN ees_to_delete;

  /* PR */
  OPEN payrolls_to_delete;
  WHILE (1 = 1) DO
  BEGIN
    FETCH payrolls_to_delete INTO :pr_nbr;
    IF (ROW_COUNT = 0) THEN
      LEAVE;


     DELETE FROM pr_check WHERE pr_nbr = :pr_nbr;

     DELETE FROM co_bank_account_register c where
              co_tax_payment_ach_nbr in (select  co_tax_payment_ach_nbr From co_tax_payment_ach a
                          where a.CO_TAX_DEPOSITS_NBR in    
                                (SELECT b.co_tax_deposits_nbr FROM co_tax_deposits b
                                    WHERE ( b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_fed_tax_liabilities    WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                            b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_state_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                            b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_sui_liabilities        WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                            b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_local_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null)
                                          ) and
                                         b.co_tax_deposits_nbr = a.co_tax_deposits_nbr) and
                                         c.CO_TAX_PAYMENT_ACH_NBR = a.CO_TAX_PAYMENT_ACH_NBR);

     DELETE FROM co_bank_account_register c where
               PR_MISCELLANEOUS_CHECKS_NBR IN 
                       (select  PR_MISCELLANEOUS_CHECKS_NBR From PR_MISCELLANEOUS_CHECKS a
                           where a.CO_TAX_DEPOSITS_NBR in   
                                (SELECT b.co_tax_deposits_nbr FROM co_tax_deposits b
                                    WHERE ( b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_fed_tax_liabilities    WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                            b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_state_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                            b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_sui_liabilities        WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                            b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_local_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null)
                                          ) and
                                          b.co_tax_deposits_nbr = a.co_tax_deposits_nbr) and
                                          c.PR_MISCELLANEOUS_CHECKS_NBR = a.PR_MISCELLANEOUS_CHECKS_NBR);

     DELETE FROM co_tax_payment_ach a where
               a.CO_TAX_DEPOSITS_NBR in  (SELECT b.co_tax_deposits_nbr FROM co_tax_deposits b
                                 WHERE (  b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_fed_tax_liabilities    WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                          b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_state_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                          b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_sui_liabilities        WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                          b.co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_local_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null)
                                        ) and
                                        b.co_tax_deposits_nbr = a.co_tax_deposits_nbr);

     DELETE FROM co_tax_deposits WHERE ( co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_fed_tax_liabilities    WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                         co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_state_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                         co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_sui_liabilities        WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null) or
                                         co_tax_deposits_nbr in (SELECT DISTINCT co_tax_deposits_nbr FROM co_local_tax_liabilities  WHERE pr_nbr = :pr_nbr and co_tax_deposits_nbr is not null)
                                       );



     DELETE FROM co_fed_tax_liabilities WHERE pr_nbr = :pr_nbr;
     DELETE FROM co_state_tax_liabilities WHERE pr_nbr = :pr_nbr;
     DELETE FROM co_sui_liabilities WHERE pr_nbr = :pr_nbr;
     DELETE FROM co_local_tax_liabilities WHERE pr_nbr = :pr_nbr;

     FOR SELECT co_tax_deposits_nbr FROM pr_miscellaneous_checks WHERE pr_nbr = :pr_nbr INTO :co_tax_deposits_nbr  DO
     BEGIN

       DELETE FROM pr_miscellaneous_checks WHERE co_tax_deposits_nbr = :co_tax_deposits_nbr;
       DELETE FROM co_tax_deposits WHERE co_tax_deposits_nbr = :co_tax_deposits_nbr;
     END

     DELETE FROM pr_miscellaneous_checks WHERE PR_NBR = :pr_nbr;

     DELETE FROM co_bank_account_register WHERE pr_nbr = :pr_nbr;
     DELETE FROM co_billing_history WHERE pr_nbr = :pr_nbr;
     DELETE FROM co_pr_ach WHERE pr_nbr = :pr_nbr;
     DELETE FROM ee_time_off_accrual_oper WHERE pr_nbr = :pr_nbr;

     DELETE FROM pr_batch WHERE pr_nbr = :pr_nbr;

     DELETE FROM pr_reports WHERE pr_nbr = :pr_nbr;
     DELETE FROM pr_reprint_history WHERE pr_nbr = :pr_nbr;
     DELETE FROM pr_scheduled_e_ds WHERE pr_nbr = :pr_nbr;
     DELETE FROM pr_scheduled_event WHERE pr_nbr = :pr_nbr;
     DELETE FROM PR_SERVICES WHERE PR_NBR = :pr_nbr;

     DELETE FROM co_tax_deposits WHERE pr_nbr = :pr_nbr;

     DELETE FROM ee_hr_attendance WHERE pr_nbr = :pr_nbr;


     DELETE FROM pr WHERE pr_nbr = :pr_nbr;
  END
  CLOSE payrolls_to_delete;

  /* EE */
  WHILE (1 = 1) DO
  BEGIN
    FETCH ees_to_delete INTO :ee_nbr;
    IF (ROW_COUNT = 0) THEN
      LEAVE;

    DELETE FROM co_group_manager WHERE ee_nbr = :ee_nbr;
    DELETE FROM co_group_member WHERE ee_nbr = :ee_nbr;
    DELETE FROM co_tax_return_runs WHERE ee_nbr = :ee_nbr;

    DELETE FROM co_manual_ach WHERE (ee_nbr = :ee_nbr or to_ee_nbr = :ee_nbr or from_ee_nbr = :ee_nbr);


    DELETE FROM ee_additional_info WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_autolabor_distribution WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_benefit_payment WHERE ee_nbr = :ee_nbr;
    DELETE FROM EE_BENEFIT_REFUSAL WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_benefits WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_change_request WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_child_support_cases WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_direct_deposit WHERE ee_nbr = :ee_nbr;


    DELETE FROM ee_emergency_contacts WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_locals WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_pension_fund_splits WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_piece_work WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_rates WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_scheduled_e_ds WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_states WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_work_shifts WHERE ee_nbr = :ee_nbr;

    DELETE FROM ee_hr_attendance WHERE ee_nbr = :ee_nbr;
    DELETE FROM EE_HR_CAR WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_hr_co_provided_educatn WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_hr_injury_occurrence WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_hr_performance_ratings WHERE ee_nbr = :ee_nbr;
    DELETE FROM ee_hr_property_tracking WHERE ee_nbr = :ee_nbr;
    DELETE FROM EE_SIGNATURE WHERE ee_nbr = :ee_nbr;
    DELETE FROM EE_SPREADREPORT WHERE ee_nbr = :ee_nbr;


    DELETE FROM ee_time_off_accrual WHERE ee_nbr = :ee_nbr;


    DELETE FROM ee WHERE ee_nbr = :ee_nbr;
  END
  CLOSE ees_to_delete;


  /* Delete not used Persons */

  DELETE FROM cl_person c WHERE
  NOT EXISTS (SELECT ee_nbr FROM ee e WHERE e.cl_person_nbr = c.cl_person_nbr);


  DELETE FROM cl_person_dependents d WHERE
  NOT EXISTS (SELECT cl_person_nbr FROM cl_person c WHERE c.cl_person_nbr = d.cl_person_nbr);


  DELETE FROM cl_person_documents d WHERE
  NOT EXISTS (SELECT cl_person_nbr FROM cl_person c WHERE c.cl_person_nbr = d.cl_person_nbr);

  DELETE FROM cl_hr_person_education d WHERE
  NOT EXISTS (SELECT cl_person_nbr FROM cl_person c WHERE c.cl_person_nbr = d.cl_person_nbr);

  DELETE FROM cl_hr_person_handicaps d WHERE
  NOT EXISTS (SELECT cl_person_nbr FROM cl_person c WHERE c.cl_person_nbr = d.cl_person_nbr);

  DELETE FROM cl_hr_person_skills d WHERE
  NOT EXISTS (SELECT cl_person_nbr FROM cl_person c WHERE c.cl_person_nbr = d.cl_person_nbr);

  DELETE FROM co_hr_applicant d WHERE
  NOT EXISTS (SELECT cl_person_nbr FROM cl_person c WHERE c.cl_person_nbr = d.cl_person_nbr);

END

^

SET TERM ;^
COMMIT;


EXECUTE PROCEDURE keep_only_archived_records('1/1/2012');
COMMIT;

DROP PROCEDURE keep_only_archived_records;
COMMIT;


DROP TRIGGER T_BD_PR_99;    
COMMIT;
DROP TRIGGER T_BD_PR_CHECK_99;    
COMMIT;
DROP TRIGGER T_BD_PR_BATCH_99;    
COMMIT;
DROP TRIGGER T_BD_CO_PR_ACH_99;    
COMMIT;
DROP TRIGGER T_BD_EE_TIME_OFF_ACCRUAL_99;    
COMMIT;
DROP TRIGGER T_BD_CO_BILLING_HISTORY_99;    
COMMIT;
DROP TRIGGER T_BD_CO_TAX_DEPOSITS_99;    
COMMIT;
DROP TRIGGER T_BD_PR_MISCELLANEOUS_CHECKS_99;    
COMMIT;
DROP TRIGGER T_BD_PR_SCHEDULED_EVENT_99;    
COMMIT;
DROP TRIGGER T_BD_EE_TIME_OFF_ACC_OPER_99;    
COMMIT;
DROP TRIGGER T_BD_CO_BILL_HIST_DETAIL_99;    
COMMIT;
DROP TRIGGER T_BD_CO_BANK_ACC_REG_99;    
COMMIT;
DROP TRIGGER T_BD_PR_CHECK_LOCALS_99;    
COMMIT;
DROP TRIGGER T_BD_PR_CHECK_STATES_99;    
COMMIT;
DROP TRIGGER T_BD_PR_CHECK_SUI_99;    
COMMIT;
DROP TRIGGER T_BD_PR_CHECK_LINES_99;    
COMMIT;
DROP TRIGGER T_BD_PR_CHECK_LINE_LOCALS_99;    
COMMIT;
DROP TRIGGER T_BD_CO_FED_TAX_LIABILITIES_99;    
COMMIT;
DROP TRIGGER T_BD_CO_STATE_TAX_LIAB_99;    
COMMIT;
DROP TRIGGER T_BD_CO_SUI_LIABILITIES_99;    
COMMIT;
DROP TRIGGER T_BD_CO_LOCAL_TAX_LIAB_99;    
COMMIT;
DROP TRIGGER T_BD_CO_TAX_PAYMENT_ACH_99;    
COMMIT;
DROP TRIGGER T_BD_PR_REPORTS_99;    
COMMIT;
DROP TRIGGER T_BD_PR_REPRINT_HISTORY_99;    
COMMIT;
DROP TRIGGER T_BD_PR_REPRINT_HIST_DTL_99;    
COMMIT;
DROP TRIGGER T_BD_PR_SCHEDULED_E_DS_99;    
COMMIT;
DROP TRIGGER T_BD_PR_SCHD_EVENT_BATCH_99;    
COMMIT;
DROP TRIGGER T_BD_PR_SERVICES_99;    
COMMIT;
DROP TRIGGER T_BD_EE_99;    
COMMIT;
DROP TRIGGER T_BD_EE_BENEFITS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_CHILD_SUPP_CASES_99;    
COMMIT;
DROP TRIGGER T_BD_EE_DIRECT_DEPOSIT_99;    
COMMIT;
DROP TRIGGER T_BD_CO_MANUAL_ACH_99;    
COMMIT;
DROP TRIGGER T_BD_CO_GROUP_MANAGER_99;    
COMMIT;
DROP TRIGGER T_BD_CO_GROUP_MEMBER_99;    
COMMIT;
DROP TRIGGER T_BD_CO_TAX_RETURN_RUNS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_ADDITIONAL_INFO_99;    
COMMIT;
DROP TRIGGER T_BD_EE_AUTOLABOR_DIST_99;    
COMMIT;
DROP TRIGGER T_BD_EE_BENEFIT_PAYMENT_99;    
COMMIT;
DROP TRIGGER T_BD_EE_BENEFIT_REFUSAL_99;    
COMMIT;
DROP TRIGGER T_BD_EE_BENEFICIARY_99;    
COMMIT;
DROP TRIGGER T_BD_EE_BENEFICIARY_PLAN_99;    
COMMIT;
DROP TRIGGER T_BD_EE_SCHEDULED_E_DS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_CHANGE_REQUEST_99;    
COMMIT;
DROP TRIGGER T_BD_EE_EMERGENCY_CONTACTS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_LOCALS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_PENS_FUND_SPLITS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_PIECE_WORK_99;    
COMMIT;
DROP TRIGGER T_BD_EE_RATES_99;    
COMMIT;
DROP TRIGGER T_BD_EE_STATES_99;    
COMMIT;
DROP TRIGGER T_BD_EE_WORK_SHIFTS_99;    
COMMIT;
DROP TRIGGER T_BD_EE_HR_ATTENDANCE_99;    
COMMIT;
DROP TRIGGER T_BD_EE_HR_CAR_99;    
COMMIT;
DROP TRIGGER T_BD_EE_HR_CO_PROV_EDUCATN_99;    
COMMIT;
DROP TRIGGER T_BD_EE_HR_INJURY_OCCURRENCE_99;    
COMMIT;
DROP TRIGGER T_BD_EE_HR_PROPERTY_TRACKING_99;    
COMMIT;
DROP TRIGGER T_BD_EE_SIGNATURE_99;    
COMMIT;
DROP TRIGGER T_BD_EE_SPREADREPORT_99;    
COMMIT;
DROP TRIGGER T_BD_EE_HR_PERFOR_RATINGS_99;    
COMMIT;
DROP TRIGGER T_BD_CL_PERSON_99;    
COMMIT;
DROP TRIGGER T_BD_CL_PERSON_DEPEND_99;    
COMMIT;
DROP TRIGGER T_BD_CL_PERSON_DOCUMENTS_99;    
COMMIT;
DROP TRIGGER T_BD_CL_HR_PERSON_EDU_99;    
COMMIT;
DROP TRIGGER T_BD_CL_HR_PERSON_HAND_99;    
COMMIT;
DROP TRIGGER T_BD_CL_HR_SKILLS_99;    
COMMIT;
DROP TRIGGER T_BD_CO_HR_APPLICANT_99;    
COMMIT;
DROP TRIGGER T_BD_EV_TABLE_CHANGE_99;    
COMMIT;


DELETE FROM EV_TRANSACTION t WHERE NOT EXISTS (SELECT NBR FROM EV_TABLE_CHANGE c WHERE c.EV_TRANSACTION_NBR = t.NBR);
COMMIT;

