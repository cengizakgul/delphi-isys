@ECHO OFF

del /F .\DB\TMP_TBLS.gbk
del /F .\DB\SERVICE.gbk
del /F .\DB\S_BUREAU.gbk
del /F .\DB\CL_BASE.gbk

ECHO Backing up SERVICE.gdb
..\..\..\Common\Misc\Firebird\gbak -B -G -USER sysdba -PASSWORD pps97 ivwdld01:/db/xcase/SERVICE_DEV.gdb .\DB\SERVICE.gbk

ECHO Backing up S_BUREAU.gdb
..\..\..\Common\Misc\Firebird\gbak -B -G -USER sysdba -PASSWORD pps97 ivwdld01:/db/xcase/S_BUREAU.gdb .\DB\S_BUREAU.gbk

ECHO Backing up CL_BASE.gdb
..\..\..\Common\Misc\Firebird\gbak -B -G -USER sysdba -PASSWORD pps97 ivwdld01:/db/xcase/CL_BASE.gdb .\DB\CL_BASE.gbk

ECHO Backing up TMP_TBLS.gdb
..\..\..\Common\Misc\Firebird\gbak -B -G -USER sysdba -PASSWORD pps97 ivwdld01:/db/xcase/TMP_TBLS.gdb .\DB\TMP_TBLS.gbk

pause
