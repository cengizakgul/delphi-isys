REM  Archive xCase files into repository

@ECHO OFF
CLS

SET xCaseDir=C:\xCase\
SET ProjectDir=%xCaseDir%Evolution\
SET SevenZip=..\..\..\Common\External\7Zip\7z.exe
SET FileList=..\..\..\Tmp\filelist.txt

IF EXIST %xCaseDir% GOTO OK
ECHO xCase folder not found!
EXIT 1
:OK

IF "%1" == "/B" GOTO backup
SET DestDir=.\
ECHO                                 ATTENTION!
ECHO ALL REPOSITORY FILES IN CURRENT FOLDER WILL BE OVERWRITTEN WITH NEW ONES FROM XCASE! 
ECHO                    PLEASE CLOSE XCASE BEFORE CONTINUING!
PAUSE
GOTO start
:backup
SET DestDir=%xCaseDir%Backup\
mkdir %DestDir%

:start

REM  Archive xCase common files
DEL %DestDir%xCase.zip
DEL %FileList%
ECHO %xCaseDir%bddm.brw > %FileList%
ECHO %xCaseDir%bddmfld.brw >> %FileList%
ECHO %xCaseDir%bmetadef.brw >> %FileList%
ECHO %xCaseDir%bviewsel.brw >> %FileList%
ECHO %xCaseDir%CATALOG.cdx >> %FileList%
ECHO %xCaseDir%DDDEFDOM.cdx >> %FileList%
ECHO %xCaseDir%DDL.cdx >> %FileList%
ECHO %xCaseDir%DDM.cdx >> %FileList%
ECHO %xCaseDir%DDPRTMPL.cdx >> %FileList%
ECHO %xCaseDir%DDTEMPL.cdx >> %FileList%
ECHO %xCaseDir%XCASE.chw >> %FileList%
ECHO %xCaseDir%catalog.dbf >> %FileList%
ECHO %xCaseDir%dddbms.DBF >> %FileList%
ECHO %xCaseDir%DDDEFDOM.dbf >> %FileList%
ECHO %xCaseDir%ddl.DBF >> %FileList%
ECHO %xCaseDir%DDM.DBF >> %FileList%
ECHO %xCaseDir%ddprtmpl.DBF >> %FileList%
ECHO %xCaseDir%ddtempl.DBF >> %FileList%
ECHO %xCaseDir%xcase_n.ers >> %FileList%
ECHO %xCaseDir%DDDEFDOM.fpt >> %FileList%
ECHO %xCaseDir%ddl.FPT >> %FileList%
ECHO %xCaseDir%ddm.FPT >> %FileList%
ECHO %xCaseDir%ddprtmpl.FPT >> %FileList%
ECHO %xCaseDir%ddtempl.FPT >> %FileList%
ECHO %xCaseDir%xcase.INI >> %FileList%
ECHO %xCaseDir%models.mdl >> %FileList%
%SevenZip% a -tzip -mx=9 -y %DestDir%xCase.zip @%FileList%

REM  Archive SYSTEM database model
DEL %DestDir%SYSTEM.zip
DEL %FileList%
ECHO semaphor > %FileList%
ECHO SYSTEM.xcs >> %FileList%
%SevenZip% a -tzip -mx=9 -y -x@%FileList% %DestDir%SYSTEM.zip %ProjectDir%SYSTEM\*

REM  Archive S_BUREAU database model
DEL %DestDir%S_BUREAU.zip
DEL %FileList%
ECHO semaphor > %FileList%
ECHO S_BUREAU.xcs >> %FileList%
%SevenZip% a -tzip -mx=9 -y -x@%FileList% %DestDir%S_BUREAU.zip %ProjectDir%S_BUREAU\*

REM  Archive CL database model
DEL %DestDir%CL.zip
DEL %FileList%
ECHO semaphor > %FileList%
ECHO CL.xcs >> %FileList%
%SevenZip% a -tzip -mx=9 -y -x@%FileList% %DestDir%CL.zip %ProjectDir%CL\*

REM  Archive TEMP_TBLS database model
DEL %DestDir%TEMP_TBLS.zip
DEL %FileList%
ECHO semaphor > %FileList%
ECHO TEMP_TBLS.xcs >> %FileList%
%SevenZip% a -tzip -mx=9 -y -x@%FileList% %DestDir%TEMP_TBLS.zip %ProjectDir%TEMP_TBLS\*

REM  Copy Scripts
RMDIR /S /Q %DestDir%Scripts
MKDIR %DestDir%Scripts
COPY /Y %ProjectDir%Scripts %DestDir%Scripts > NUL

DEL %FileList%
