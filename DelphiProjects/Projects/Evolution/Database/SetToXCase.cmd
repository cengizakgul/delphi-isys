REM  Archive xCase files into repository

@ECHO OFF
CLS

SET xCaseDir=C:\xCase\
SET ProjectDir=%xCaseDir%Evolution\
SET SevenZip=..\..\..\Common\External\7Zip\7z.exe

IF EXIST %xCaseDir% GOTO OK
ECHO xCase folder not found!
EXIT 1
:OK

ECHO                       ATTENTION!
ECHO XCASE FILES WILL BE OVERWRITTEN WITH FILES FROM CURRENT FOLDER! 
ECHO             PLEASE CLOSE XCASE BEFORE CONTINUING!
PAUSE

REM Backup xCase files just in case
CALL GetFromXCase.cmd /B

RMDIR /S /Q %ProjectDir%

REM  Extract xCase common files
%SevenZip% e -y xCase.zip -o%xCaseDir%

REM  Extract SYSTEM database model
SET Prj=%ProjectDir%SYSTEM\
MKDIR %Prj%
DEL /F /Q %Prj%*
%SevenZip% e -y SYSTEM.zip -o%Prj%

REM  Extract S_BUREAU database model
SET Prj=%ProjectDir%S_BUREAU\
MKDIR %Prj%
DEL /F /Q %Prj%*
%SevenZip% e -y S_BUREAU.zip -o%Prj%

REM  Extract CL database model
SET Prj=%ProjectDir%CL\
MKDIR %Prj%
DEL /F /Q %Prj%*
%SevenZip% e -y CL.zip -o%Prj%

REM  Extract TEMP_TBLS database model
SET Prj=%ProjectDir%TEMP_TBLS\
MKDIR %Prj%
DEL /F /Q %Prj%*
%SevenZip% e -y TEMP_TBLS.zip -o%Prj%

REM  Copy Scripts
SET Prj=%ProjectDir%Scripts\
RMDIR /S /Q %Prj%
MKDIR %Prj%
COPY /Y Scripts %Prj% > NUL

START %xCaseDir%xcase.exe
