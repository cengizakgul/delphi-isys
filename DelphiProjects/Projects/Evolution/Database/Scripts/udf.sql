/* DATE-TIME FUNCTIONS */

declare external function AddYear
  timestamp,
  integer
  returns
  timestamp  free_it 
  entry_point 'AddYear' module_name 'EvolUDFs'
^

declare external function AddMonth
  timestamp,
  integer
  returns
  timestamp  free_it 
  entry_point 'AddMonth' module_name 'EvolUDFs'
^

declare external function AgeInYears
  timestamp, timestamp
  returns
  integer by value
  entry_point 'AgeInYears' module_name 'EvolUDFs'
^

declare external function AgeInMonths
  timestamp, timestamp
  returns
  integer by value
  entry_point 'AgeInMonths' module_name 'EvolUDFs'
^

declare external function AgeInWeeks
  timestamp, timestamp
  returns
  integer by value
  entry_point 'AgeInWeeks' module_name 'EvolUDFs'
^

declare external function AgeInDays
  timestamp, timestamp
  returns
  integer by value
  entry_point 'AgeInDays' module_name 'EvolUDFs'
^

declare external function DayOfYear
  timestamp
  returns
  integer by value
  entry_point 'DayOfYear' module_name 'EvolUDFs'
^

declare external function DayOfMonth
  timestamp
  returns
  integer by value
  entry_point 'DayOfMonth' module_name 'EvolUDFs'
^

declare external function DayOfWeek
  timestamp
  returns
  integer by value
  entry_point 'DayOfWeek' module_name 'EvolUDFs'
^

declare external function MaxDate
  timestamp, timestamp
  returns
  timestamp free_it
  entry_point 'MaxDate' module_name 'EvolUDFs'
^

declare external function MinDate
  timestamp, timestamp
  returns
  timestamp free_it
  entry_point 'MinDate' module_name 'EvolUDFs'
^

declare external function ExtractMonth
  timestamp
  returns
  integer by value
  entry_point 'Month' module_name 'EvolUDFs'
^

declare external function ExtractQuarter
  timestamp
  returns
  integer by value
  entry_point 'Quarter' module_name 'EvolUDFs'
^

declare external function ExtractYear
  timestamp
  returns
  integer by value
  entry_point 'Year' module_name 'EvolUDFs'
^

declare external function StripDate
  timestamp
  returns
  timestamp free_it
  entry_point 'StripDate' module_name 'EvolUDFs'
^

declare external function StripTime
  timestamp
  returns
  timestamp free_it
  entry_point 'StripTime' module_name 'EvolUDFs'
^

declare external function WeekOfYear
  timestamp
  returns
  integer by value
  entry_point 'WeekOfYear' module_name 'EvolUDFs'
^

declare external function BeginYear
  timestamp
  returns
  timestamp free_it
  entry_point 'BeginYear' module_name 'EvolUDFs'
^

declare external function BeginQuarter
  timestamp
  returns
  timestamp free_it
  entry_point 'BeginQuarter' module_name 'EvolUDFs'
^

declare external function BeginMonth
  timestamp
  returns
  timestamp free_it
  entry_point 'BeginMonth' module_name 'EvolUDFs'
^

declare external function EndYear
  timestamp
  returns
  timestamp free_it
  entry_point 'EndYear' module_name 'EvolUDFs'
^

declare external function EndQuarter
  timestamp
  returns
  timestamp free_it
  entry_point 'EndQuarter' module_name 'EvolUDFs'
^

declare external function EndMonth
  timestamp
  returns
  timestamp free_it
  entry_point 'EndMonth' module_name 'EvolUDFs'
^

declare external function FormatDate 
  timestamp, cstring(30)
  returns cstring(30) free_it
  entry_point 'FormatDate' module_name 'EvolUDFs'
^

/* STRING FUNCTIONS */

declare external function ASCIIChar
  integer
  returns cstring(2) free_it
  entry_point 'ASCIIChar' module_name 'EvolUDFs'
^

declare external function FindWord
  cstring(8196),
  integer
  returns cstring(254) free_it
  entry_point 'FindWord' module_name 'EvolUDFs'
^

declare external function FindWordIndex
  cstring(8196),
  integer
  returns integer by value
  entry_point 'FindWordIndex' module_name 'EvolUDFs'
^

declare external function Trim
  cstring(512)
  returns
  cstring(512) free_it
  entry_point 'Trim' module_name 'EvolUDFs'
^

declare external function LeftTrim
  cstring(512)
  returns
  cstring(512) free_it
  entry_point 'LeftTrim' module_name 'EvolUDFs'
^

declare external function RightTrim
  cstring(512)
  returns
  cstring(512) free_it
  entry_point 'RightTrim' module_name 'EvolUDFs'
^

declare external function StrCopy
  cstring(512), integer, integer
  returns
  cstring(512) free_it
  entry_point 'StrCopy' module_name 'EvolUDFs'
^

declare external function PadLeft
  cstring(512), cstring(10), integer
  returns
  cstring(512) free_it
  entry_point 'PadLeft' module_name 'EvolUDFs'
^

declare external function PadRight
  cstring(512), cstring(10), integer
  returns
  cstring(512) free_it
  entry_point 'PadRight' module_name 'EvolUDFs'
^

declare external function ProperCase
  cstring(512)
  returns
  cstring(512) free_it
  entry_point 'ProperCase' module_name 'EvolUDFs'
^

declare external function UpperCase
  cstring(512)
  returns
  cstring(512) free_it
  entry_point 'UpperCase' module_name 'EvolUDFs'
^

declare external function LowerCase
  cstring(512)
  returns
  cstring(512) free_it
  entry_point 'LowerCase' module_name 'EvolUDFs'
^

declare external function StrLen
  cstring(512)
  returns
  integer by value
  entry_point 'StrLen' module_name 'EvolUDFs'
^

declare external function StripString
  cstring(512), cstring(512)
  returns
  cstring(512) free_it
  entry_point 'StripString' module_name 'EvolUDFs'
^

declare external function PosSubStr
  cstring(512), cstring(512)
  returns
  integer by value
  entry_point 'PosSubStr' module_name 'EvolUDFs'
^

/* MATH FUNCTIONS */

declare external function Abs
  double precision
  returns
  double precision by value
  entry_point 'Abs' module_name 'EvolUDFs'
^

declare external function RoundAll
  double precision, integer
  returns
  double precision by value
  entry_point 'RoundAll' module_name 'EvolUDFs'
^

declare external function Trunc
  double precision
  returns 
  integer by value
  entry_point 'Trunc' module_name 'EvolUDFs'
^

declare external function Sign
  double precision
  returns 
  integer by value
  entry_point 'Sign' module_name 'EvolUDFs'
^

declare external function IntAnd
  integer, integer
  returns 
  integer by value
  entry_point 'IntAnd' module_name 'EvolUDFs'
^

declare external function IntOr
  integer, integer
  returns 
  integer by value
  entry_point 'IntOr' module_name 'EvolUDFs'
^

declare external function IntXor
  integer, integer
  returns 
  integer by value
  entry_point 'IntXor' module_name 'EvolUDFs'
^

declare external function IntNot
  integer
  returns 
  integer by value
  entry_point 'IntNot' module_name 'EvolUDFs'
^

/* MISCELLANEOUS FUNCTIONS */

declare external function ConvertNullString
  cstring(512), cstring(512)
  returns
  cstring(512) free_it
  entry_point 'ConvertNullString' module_name 'EvolUDFs'
^

declare external function ConvertNullDate
  timestamp, timestamp
  returns
  timestamp free_it
  entry_point 'ConvertNullDate' module_name 'EvolUDFs'
^

declare external function ConvertNullInteger
  integer, integer
  returns
  integer by value
  entry_point 'ConvertNullInteger' module_name 'EvolUDFs'
^

declare external function ConvertNullDouble
  double precision, double precision
  returns
  double precision by value
  entry_point 'ConvertNullDouble' module_name 'EvolUDFs'
^

declare external function IfString
  integer, cstring(512), cstring(512)
  returns
  cstring(512) free_it
  entry_point 'IfString' module_name 'EvolUDFs'
^

declare external function IfDate
  integer, timestamp, timestamp
  returns
  timestamp free_it
  entry_point 'IfDate' module_name 'EvolUDFs'
^

declare external function IfInteger
  integer, integer, integer
  returns
  integer by value
  entry_point 'IfInteger' module_name 'EvolUDFs'
^

declare external function IfDouble
  integer, double precision, double precision
  returns
  double precision by value
  entry_point 'IfDouble' module_name 'EvolUDFs'
^

declare external function CompareString
  cstring(512), cstring(3), cstring(512)
  returns
  integer by value
  entry_point 'CompareString' module_name 'EvolUDFs'
^

declare external function CompareDate
  timestamp, cstring(3), timestamp
  returns
  integer by value
  entry_point 'CompareDate' module_name 'EvolUDFs'
^

declare external function CompareInteger
  integer, cstring(3), integer
  returns
  integer by value
  entry_point 'CompareInteger' module_name 'EvolUDFs'
^

declare external function CompareDouble
  double precision, cstring(3), double precision
  returns
  integer by value
  entry_point 'CompareDouble' module_name 'EvolUDFs'
^

DECLARE EXTERNAL FUNCTION GetBlobSize
  BLOB
  RETURNS INTEGER BY VALUE
  ENTRY_POINT 'GetBlobSize' MODULE_NAME 'EvolUDFs'
^
