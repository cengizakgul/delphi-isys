source ./Evolution/Scripts/EvoLib.tcl 

set table_list [dict list_of_tables]

# System indexes
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [falsevalue]} then {   
>> "ALTER TABLE [$table attr NAME] ADD CONSTRAINT C_[$table attr NAME]_1 check (effective_date < effective_until)"
>> "^"
>> "ALTER TABLE [$table attr NAME] ADD CONSTRAINT PK_[$table attr NAME] PRIMARY KEY (REC_VERSION)"
>> "^"
>> "ALTER TABLE [$table attr NAME] ADD CONSTRAINT AK_[$table attr NAME]_1 UNIQUE ([$table attr NAME]_NBR, EFFECTIVE_DATE)"
>> "^"
>> "ALTER TABLE [$table attr NAME] ADD CONSTRAINT AK_[$table attr NAME]_2 UNIQUE ([$table attr NAME]_NBR, EFFECTIVE_UNTIL)"
>> "^"
  }
}


# Logical key indexes
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }
  
  set lk_fields [getLKfields $table] 
  
  if {[llength $lk_fields] > 0} then {
    set child_ref ""
    foreach lk_field $lk_fields {
	  if {$child_ref != ""} then {set child_ref $child_ref,}	
	  set child_ref $child_ref[$lk_field attr NAME]
    }		
>> "DROP INDEX LK_[$table attr NAME]"
>> "^" 
>> "CREATE INDEX LK_[$table attr NAME] ON [string tolower [$table attr NAME]] ($child_ref)"	
>> "^" 
  }  
}


# Foreign key indexes
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }

  set relations [$table in_relations]
  set rel_nbr 0
  
  foreach relation $relations {       
    if {[$relation attr NORMAL]} then { continue }    

	set rel_nbr [expr $rel_nbr + 1]
    set field_list [$relation list_of_foreign_keys]
    foreach field $field_list {
      if {[$field attr LOGICAL] == [falsevalue]} then {
        set child_ref [string tolower [$field attr NAME]]
        break
      }         
    }           

    if {[isVerTable $table]} then {
	  append child_ref ", effective_until"
	}

>> "DROP INDEX FK_[$table attr NAME]_$rel_nbr"	
>> "^" 
>> "CREATE INDEX FK_[$table attr NAME]_$rel_nbr ON [string tolower [$table attr NAME]] ($child_ref)"
>> "^" 
  }
  
}

# Generators
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [falsevalue]} then {
    set sequence_name [string range [$table attr NAME] 0 28]

>> "CREATE SEQUENCE G_$sequence_name"
>> "^"
>> "CREATE SEQUENCE G_$sequence_name\_VER"
>> "^"
  }
}


# Triggers
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [falsevalue]} then {
    set table_name [$table attr NAME]
    set trig_name [string range $table_name 0 22]	
	set trig_list [$table list_of_triggers]

    if {[isVerAwareTable $table]} then {	

	  set trig T_AI_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER INSERT POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

	  set trig T_AD_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER DELETE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
	  }
	  
	  set trig T_AU_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER UPDATE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

 	  set trig T_BI_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE INSERT POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

 	  set trig T_BU_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE UPDATE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

  	  set trig T_AUD_$trig_name\_2
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER UPDATE OR DELETE POSITION 2"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }
	  
	  
  	  set trig T_BIU_$trig_name\_2
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE INSERT OR UPDATE POSITION 2"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""   
      }
	  
  	  set trig T_AIU_$trig_name\_3
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER INSERT OR UPDATE POSITION 3"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""   
     }

  	  set trig T_AD_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER DELETE POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }
	 
  	  set trig T_AU_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER UPDATE POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

  	  set trig T_BUD_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE UPDATE OR DELETE POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }	  
    }
		
    set trig T_BIU_$trig_name\_3
    if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE INSERT OR UPDATE POSITION 3"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
    }	
  }
}


set proc_list [dict list_of_procedures]  
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }
 
  set table_name [$table attr NAME]
  set proc_name [string range $table_name 0 26]	
	  
  set proc PACK_$proc_name
  if {[procInList $proc_list $proc] == [falsevalue]} then {
>> "CREATE OR ALTER PROCEDURE $proc"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
  }  
  
  set proc DEL_$proc_name
  if {[procInList $proc_list $proc] == [falsevalue]} then {
>> "CREATE OR ALTER PROCEDURE $proc"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
  }    
}	
