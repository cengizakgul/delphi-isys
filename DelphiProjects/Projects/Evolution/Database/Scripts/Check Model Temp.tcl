source ./Evolution/Scripts/EvoLib.tcl 

set table_list [dict list_of_tables]

# Check missing relations
>> "========= Missing relations ========="
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }

  set table_PK_field [$table attr name]
  set table_PK_field [string range $table_PK_field 4 64]  
  set table_PK_field $table_PK_field\_NBR
  
  set field_list [$table list_of_fields]
  set relations [$table in_relations]  
  
  foreach field $field_list { 
    if {[$field attr ev_ext_fk] == [falsevalue] && [string match *_NBR [$field attr name]] == 1 && $table_PK_field != [$field attr name]} then {
	  set found [falsevalue]
  	  set rel_nbr 0
      foreach relation $relations {         
        set rel_nbr [expr $rel_nbr + 1]
        set key_field_list [$relation list_of_foreign_keys]
        foreach key_field $key_field_list {
          if {[$key_field attr LOGICAL] == [falsevalue] && [$field attr name] == [$key_field attr name]} then {
		    set found [truevalue]  
            break
		  }
        }
        if {$found}	then { 
		  break 
		}
      }	
	  
	  if {$found == [falsevalue]} then { 
>> "   [$table attr name].[$field attr name]"
	  }	  
    }           
  } 
}
>> ""
>> ""


# Check duplicate relations
>> "========= Duplicate relations ========="
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }

  set relations [$table in_relations]
  set rel_nbr 0
  
  foreach relation $relations {       
    set rel_nbr [expr $rel_nbr + 1]
    set field_list [$relation list_of_foreign_keys]
	set child_ref ""
    foreach field $field_list {
      if {[$field attr LOGICAL] == [falsevalue]} then {
        set child_ref $child_ref,[string tolower [$field attr NAME]]		
      }         
    }           
  
    foreach relation2 $relations {       
      if {[$relation2 attr NAME] == [$relation attr NAME]} then { continue }    

      set rel_nbr [expr $rel_nbr + 1]
      set field_list [$relation2 list_of_foreign_keys]
	  set child_ref2 ""
      foreach field $field_list {
        if {[$field attr LOGICAL] == [falsevalue]} then {
          set child_ref2 $child_ref2,[string tolower [$field attr NAME]]          
        }         
      }           	  

      if {$child_ref == $child_ref2} then {
>> "   [$table attr NAME]: [$relation attr NAME] and [$relation2 attr NAME]"
		break;
      }
    }
  }  
}
>> ""
>> ""

 # Check Triggers
>> "========= Missing triggers ========="
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }

  set trig_list [$table list_of_triggers]  
  set table_name [$table attr NAME]
  set trig_name [string range $table_name 0 22]	
	  
  set trig T_BD_$trig_name\_1
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  } 
}	
>> ""	
>> ""  
