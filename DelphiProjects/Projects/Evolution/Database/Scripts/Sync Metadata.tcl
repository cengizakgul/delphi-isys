source ./Evolution/Scripts/EvoLib.tcl 

syncEvDatabase

if {$EV_TEMP_DB} then { return }

checkConnected

# Delete removed tables
set table_list [dict list_of_tables] 
set table_nbrs "" 
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [truevalue]} {continue}   
    
  if {$table_nbrs != ""} then {
    set table_nbrs "$table_nbrs, "
  }
  set table_nbrs "$table_nbrs[getTableNbr $table]"
} 

set sql_handle [sql_execute "select t.nbr, t.name from ev_table t where t.nbr not in ($table_nbrs)"]
set sql_result 0
set sql_record [sql_fetch $sql_handle sql_result]
if {$sql_result == 1} then {
>> "/* Delete removed tables */"
>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE nbr INTEGER;"
>> "BEGIN"  
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"
  set in_exec_block [truevalue]
} else {
  set in_exec_block [falsevalue]
}

while {$sql_result == 1} {  
  set removed_nbr [lindex $sql_record 0]
  
>> "  /* Delete removed table [lindex $sql_record 1] */"  
  if {[lindex [sql_execute_direct "SELECT COUNT(*) FROM ev_field WHERE ev_table_nbr = $removed_nbr AND field_type = 'B'"] 0] > 0} then {
>> "  FOR SELECT CAST (fld.old_value AS INTEGER)"
>> "      FROM ev_field_change fld, ev_field f"
>> "      WHERE f.ev_table_nbr = $removed_nbr AND"
>> "            f.field_type = 'B' AND"
>> "            f.nbr = fld.ev_field_nbr AND"
>> "            fld.old_value IS NOT NULL"
>> "      INTO :nbr"
>> "  DO"
>> "    DELETE FROM ev_field_change_blob WHERE nbr = :nbr;"
>> ""  
  }
>> "  FOR SELECT t.nbr FROM ev_table_change t WHERE t.ev_table_nbr = $removed_nbr INTO :nbr"
>> "  DO"
>> "    DELETE FROM ev_field_change WHERE ev_table_change_nbr = :nbr;"
>> ""
>> "  DELETE FROM ev_table_change WHERE ev_table_nbr = $removed_nbr;"
>> "  DELETE FROM ev_field WHERE ev_table_nbr = $removed_nbr;"
>> "  DELETE FROM ev_table WHERE nbr = $removed_nbr;"  
>> ""
  
  set sql_record [sql_fetch $sql_handle sql_result]
}									
sql_free_handle $sql_handle

if {$in_exec_block == [truevalue]} then {
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"
>> "END"
>> "^"
>> "" 
>> "COMMIT^"
>> "" 
}

# Delete removed fields
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [truevalue]} {continue}   
  
  set table_nbr [getTableNbr $table]   
  
  # Prepare list of removed fields  
  set field_list [$table list_of_fields]     
  set field_nbrs "" 
  foreach field $field_list {   
    if {[$field attr LOGICAL] == [truevalue]} {continue}     
	
    if {$field_nbrs != ""} then {
      set field_nbrs "$field_nbrs, "
    }
    set field_nbrs "$field_nbrs[getFieldNbr $field]"  
  } 

  set sql_handle [sql_execute "SELECT f.nbr, f.name FROM ev_field f WHERE f.ev_table_nbr = $table_nbr AND f.nbr NOT IN ($field_nbrs)"]
  set sql_result 0
  set sql_record [sql_fetch $sql_handle sql_result]

  if {$sql_result == 1} then {
>> "/* Delete removed fields of [$table attr NAME] */"
>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE nbr INTEGER;"
>> "BEGIN"  
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"
    set in_exec_block [truevalue]
  } else {
    set in_exec_block [falsevalue]
  }  
  
  while {$sql_result == 1} {
    set removed_nbr [lindex $sql_record 0]
  
>> "  /* [lindex $sql_record 1] */"  
    if {[lindex [sql_execute_direct "SELECT COUNT(*) FROM ev_field WHERE nbr = $removed_nbr AND field_type = 'B'"] 0] > 0} then {
>> "  FOR SELECT CAST (fld.old_value AS INTEGER) FROM ev_field_change fld WHERE fld.ev_field_nbr = $removed_nbr AND fld.old_value IS NOT NULL INTO :nbr"
>> "  DO"
>> "    DELETE FROM ev_field_change_blob WHERE nbr = :nbr;"
>> ""  
  }
>> "  DELETE FROM ev_field_change WHERE ev_field_nbr = $removed_nbr;"
>> "  DELETE FROM ev_field WHERE nbr = $removed_nbr;"
>> "" 
    set sql_record [sql_fetch $sql_handle sql_result]
  }									
  sql_free_handle $sql_handle 
  
  if {$in_exec_block == [truevalue]} then {
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"  
>> "END"
>> "^"
>> "" 
>> "COMMIT^"
>> "" 
  }  
}
   
 
# Add/update tables
set statements_count 0
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [truevalue]} {continue}   

  set table_name [$table attr NAME]  
  if {[$table attr EV_VERSION] == [truevalue]} then {  
    set versioned Y;
  } else {
    set versioned N;
  }

  set table_nbr [getTableNbr $table] 
  set sql_record [sql_execute_direct "SELECT name, versioned FROM ev_table WHERE nbr = $table_nbr"]
  if {[llength $sql_record] == 0} then {
    set sql "  INSERT INTO ev_table (nbr, name, versioned) VALUES ($table_nbr, '$table_name', '$versioned');"
  } elseif {[lindex [lindex $sql_record 0] 0] != $table_name || [lindex [lindex $sql_record 0] 1] != $versioned} then {  
    set sql "  UPDATE ev_table SET name = '$table_name', versioned = '$versioned' WHERE nbr = $table_nbr;"  
  } else {
    set sql ""
  }
  
  if {$sql != ""} then {
    if {$statements_count == 0} then {
>> "/* Add/Update tables */"          
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"	
	}
>> "$sql"  
    incr statements_count
  }
}

if {$statements_count > 0} then {
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"
>> "END"
>> "^"
>> "" 
>> "COMMIT^"
>> "" 
}

# Add/update fields
set statements_count 0
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [truevalue]} {continue}   

  set table_name [$table attr NAME]  
  set table_nbr [getTableNbr $table]     
  set field_list [$table list_of_fields]         
  foreach field $field_list { 
    if {[$field attr LOGICAL] == [truevalue]} {continue}   
                     
    set field_name [$field attr NAME]        
    set field_nbr [getFieldNbr $field]                             
    set field_type [[$field domain] attr TYPE]
    set field_len ""                                  
    set field_scale ""
		
    if {[$field attr EV_VERSION] == [truevalue]} then {  
      set versioned Y;
    } else {
      set versioned N;
    }

    if {$field_type == "INTEGER"} {
      set field_type "I"                 
    } elseif {$field_type == "SMALLINT"} {
      set field_type "S"  
    } elseif {$field_type == "VARCHAR"} {
      set field_type "V"                      
      set field_len [$field attr LEN]
    } elseif {$field_type == "CHAR"} {
      set field_type "C"
      set field_len [$field attr LEN]
    } elseif {$field_type == "FLOAT"} {
      set field_type "F"
    } elseif {$field_type == "DOUBLE PRECISION"} {
      set field_type "E"
    } elseif {$field_type == "DATE"} {
      set field_type "D"
    } elseif {$field_type == "TIMESTAMP"} {
      set field_type "T"
    } elseif {$field_type == "BLOB"} {
      set field_type "B"
    } elseif {$field_type == "NUMERIC"} {
      set field_type "N"                  
      set field_len [$field attr LEN]
      set field_scale [$field attr DEC]
    } else {
      error "$table_name.$field_name: type '$field_type' is not supported"  
    }

    if {[$field attr NULL] == [truevalue]} {
      set field_required N
    } else {          
      set field_required Y
    }     

    set sql_record [sql_execute_direct "SELECT name, field_type, len, scale, required, versioned FROM ev_field WHERE nbr = $field_nbr"]
    if {[llength $sql_record] == 0} then {
	  if {$field_len == ""} then { set field_len NULL }
      if {$field_scale == ""} then { set field_scale NULL }	        		  	
      set sql "  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES ($table_nbr, $field_nbr, '$field_name', '$field_type', $field_len, $field_scale, '$field_required', '$versioned');"
    } elseif {[lindex [lindex $sql_record 0] 0] != $field_name || [lindex [lindex $sql_record 0] 1] != $field_type || 
              [lindex [lindex $sql_record 0] 2] != $field_len || [lindex [lindex $sql_record 0] 3] != $field_scale ||
  			  [lindex [lindex $sql_record 0] 4] != $field_required || [lindex [lindex $sql_record 0] 5] != $versioned} then {      
	  if {$field_len == ""} then { set field_len NULL }
      if {$field_scale == ""} then { set field_scale NULL }	        		  
      set sql "  UPDATE ev_field SET name = '$field_name', field_type = '$field_type', len = $field_len, scale = $field_scale, required = '$field_required', versioned = '$versioned' WHERE nbr = $field_nbr;"	  
    } else {
	  set sql ""
	}
	
	if {$sql != ""} then {	
	  if {$statements_count == 85} then {
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"	  
>> "END"
>> "^"
>> ""
>> "COMMIT^"
>> ""
	  set statements_count 0
	  }

	  if {$statements_count == 0} then {
>> ""
>> "/* Add/Update fields */"          
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"
	  }	
>>	"$sql"
      incr statements_count  
	}
  }
}   

if {$statements_count > 0} then {
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"	  
>> "END"
>> "^" 
>> ""
>> "COMMIT^"
>> ""
}
