source ./Evolution/Scripts/EvoLib.tcl 

set table_list [dict list_of_tables]

foreach table $table_list {
  set table_name [$table attr name]  
  
  if {[$table attr EV_SYSTEM] || $table_name == "CL_BLOB" || $table_name == "SB_BLOB"} then { continue } 
  
  set there_are_relations [falsevalue]
  set relations [$table out_relations]
  set rel_nbr 0 
  
  foreach relation $relations {       
    if {[$relation attr NORMAL]} then { continue }    

	set rel_nbr [expr $rel_nbr + 1]
    set field_list [$relation list_of_foreign_keys]
    foreach field $field_list {
      if {[$field attr LOGICAL] == [falsevalue]} then {
         set field_name [$field attr NAME]	  
		 if {$there_are_relations == [falsevalue]} then {
           set there_are_relations [truevalue]		 
>> "/* $table_name */"		   
>> "EXECUTE BLOCK"
>> "RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))"
>> "AS"
>> "DECLARE VARIABLE curr_date TIMESTAMP;"
>> "DECLARE VARIABLE effective_date TIMESTAMP;"
>> "BEGIN"
>> "  table_name = '$table_name';"		 
>> "  FOR"
>> "  SELECT $table_name\_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE"
>> "  FROM ("  	 
         } else {
>> "       UNION"
		 }         
>> "       SELECT $field_name $table_name\_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM [[$relation child] attr name] WHERE $field_name IS NOT NULL GROUP BY 1"
      }         
    }           
  }    
  if {$there_are_relations} then {
>> "   )"
>> "  GROUP BY 1"
>> "  INTO :nbr, :effective_date"
>> "  DO"
>> "  BEGIN"
>> "    curr_date = NULL;"
>> "    SELECT MIN(effective_date) FROM $table_name WHERE $table_name\_NBR = :nbr INTO :curr_date;"
>> ""
>> "    IF (curr_date IS NULL) THEN"
>> "    BEGIN"
>> "      issue_text = 'Does not exist!';"
>> "      SUSPEND;"
>> "    END"
>> "    ELSE IF (curr_date > effective_date) THEN"
>> "    BEGIN"
>> "      UPDATE $table_name SET effective_date = :effective_date WHERE $table_name\_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';"
>> "      IF (ROW_COUNT > 0) THEN"
>> "        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');"
>> "      ELSE"
>> "        issue_text = 'Starts as deleted!';"
>> "      SUSPEND;"
>> "    END"
>> "  END"
>> "END"
>> "^"
>> "COMMIT"
>> "^"
>> ""
>> ""
  }
}
