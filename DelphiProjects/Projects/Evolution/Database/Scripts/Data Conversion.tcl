source ./Evolution/Scripts/EvoLib.tcl 

proc getFieldVar {field} {
  set a [[$field ENTITY] attr NAME] 
  if {$a != "CO"} {
    return [string tolower [$field attr NAME]]
  }	  
  
  set a $a\_NBR  
  if {$a == [$field attr NAME]} then {
    return [string tolower [$field attr NAME]]
  } else {
    return f[getFieldNbr $field]
  }	
}

set modelName [string tolower [[dict get_model] attr model_name]]

if {$modelName == "system"} then {
  set req_ver 14.21.2.0
} elseif {$modelName == "bureau"} then {
  set req_ver 14.0.0.0  
} elseif {$modelName == "client"} then {
  set req_ver 14.0.0.7  
} elseif {$modelName == "temp"} then {
  set req_ver 14.0.0.1
}

>> "/* Check current version */"
>> ""
>> "SET TERM ^;"
>> ""
>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE VARIABLE REQ_VER VARCHAR(11);"
>> "DECLARE VARIABLE VER VARCHAR(11);"
>> "BEGIN"
>> "  IF (UPPER(CURRENT_USER) <> 'SYSDBA') THEN"
>> "    execute statement '\"This script must be executed by SYSDBA\"';"
>> ""
>> "  REQ_VER = '$req_ver';"
>> "  VER = 'UNKNOWN';"
>> "  SELECT CAST(MAJOR_VERSION AS VARCHAR(2)) || '.' ||"
>> "         CAST(MINOR_VERSION AS VARCHAR(2)) || '.' ||"
>> "         CAST(PATCH_VERSION AS VARCHAR(2)) || '.' ||"
>> "         CAST(BUILD_VERSION AS VARCHAR(2))"
>> "  FROM VERSION_INFO"
>> "  INTO :VER;"
>> ""
>> "  IF (strcopy(VER, 0, strlen(REQ_VER)) <> REQ_VER) THEN"
>> "    execute statement '\"Database version mismatch! Expected ' || REQ_VER || ' but found ' || VER || '\"';"
>> "END^"
>> ""
>> "COMMIT^"
>> ""
>> "SET TERM ;^"
>> ""
>> ""

set script_fragment ./Evolution/Scripts/BeforeConvert_[[dict get_model] attr MODEL_NAME].sql
if {[file exists $script_fragment] == 1} then {
>> "/*<progress>Preparing for data conversion</progress>*/"
  outFile $script_fragment
>> ""  
>> ""  
}

set script_fragment ./Evolution/Scripts/NewStructure_[[dict get_model] attr MODEL_NAME].sql
if {[file exists $script_fragment] == 1} then {
>> "/* Create a new structure */"
>> "/*<progress>Creating new structure</progress>*/"
  outFile $script_fragment
>> ""  
>> ""  
}

>> "SET TERM ^ ;"  
 source "./Evolution/Scripts/Rebuild Metadata.tcl"
>> "COMMIT ^"  
>> ""  
>> ""    

if {$EV_TEMP_DB == [falsevalue]} then {

>> "/* ================== Move data to EV_TRANSACTION table ================== */"   
>> "/*<progress>Converting EV_TRANSACTION</progress>*/"   
>> "ALTER INDEX I_EV_TRANSACTION_1 INACTIVE^"
>> "ALTER INDEX I_EV_TRANSACTION_2 INACTIVE^"
>> ""
>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE VARIABLE start_time TIMESTAMP;"
>> "DECLARE VARIABLE user_id INTEGER;"
>> "BEGIN"
>> "  FOR SELECT DISTINCT start_time, user_id FROM x_transaction"
>> "      ORDER BY start_time"
>> "      INTO :start_time, :user_id"
>> "  DO"
>> "  BEGIN"
>> "    INSERT INTO ev_transaction (nbr, commit_nbr, start_time, commit_time, user_id)"
>> "    VALUES (NEXT VALUE FOR g_ev_transaction, NEXT VALUE FOR g_ev_transaction_commit, :start_time, :start_time, :user_id);"
>> "  END"
>> "END^"
>> ""
>> "COMMIT^"
>> ""
>> "DROP TABLE x_transaction ^"
>> "COMMIT^"  
>> ""
>> "ALTER INDEX I_EV_TRANSACTION_1 ACTIVE^"
>> "ALTER INDEX I_EV_TRANSACTION_2 ACTIVE^"
>> "COMMIT^"  
>> ""
>> ""

>> "/* Convert existing data */"
>> ""  
>> ""    

>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE VARIABLE stmt VARCHAR(1000);"
>> "BEGIN"
>> "  /* Deactivate Indexes */"
>> "  FOR SELECT 'ALTER INDEX '||rdb\$index_name ||' INACTIVE;'"
>> "      FROM rdb\$indices"
>> "      WHERE (rdb\$system_flag IS NULL OR rdb\$system_flag = 0) AND"
>> "            (rdb\$unique_flag IS NULL OR rdb\$unique_flag = 0) AND"
>> "            rdb\$relation_name NOT STARTS 'EV_'"
>> "      ORDER BY rdb\$foreign_key"
>> "  INTO :stmt"
>> "  DO"
>> "    EXECUTE STATEMENT :stmt;"
>> "END^"
>> "COMMIT^"
>> ""

# Execute data conversion block for each table
  set old_tables {}
  foreach table $table_list { 
    set dest_table_name [string tolower [$table attr NAME]]
    if {[$table attr EV_SYSTEM] == [truevalue]} {continue}
	if {[$table attr NAME] == "CO_QEC_RUN"} {continue}
  
    set src_table_name x[string range $dest_table_name 1 64]
    lappend old_tables $src_table_name 
    set key_field_name $dest_table_name\_nbr  
  
    # Prepare list of data fields
    set field_list [$table list_of_fields]
    set data_fields {}   
    set version_fields {}    
    foreach field $field_list {
      if {[$field attr LOGICAL] == [falsevalue] && [$field attr EV_SYSTEM] == [falsevalue]} then {
	   lappend  data_fields $field 
         if {[isVerTable $table] && [$field attr EV_VERSION]} then {	  
          lappend  version_fields $field 	  
	    }
  	  } 
    }

>> "/* ================== Convert data of [string toupper $dest_table_name] ================== */"   
>> "/*<progress>Converting [string toupper $dest_table_name]</progress>*/"   
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  /* Deactivate integrity check and business logic */"
    set trigger_list [$table list_of_triggers]
    foreach trigger $trigger_list {    
      if {[string match Integrity:* [$trigger attr TITLE]] == 1 || [string match Business:* [$trigger attr TITLE]] == 1} then {
>> "  IF (EXISTS(SELECT 1 FROM RDB\$TRIGGERS WHERE RDB\$TRIGGER_NAME = '[$trigger attr NAME]')) THEN"
>> "    EXECUTE STATEMENT 'ALTER TRIGGER [$trigger attr NAME] INACTIVE';"
      }	  
    }
>> "END^"
>> "COMMIT^"
>> ""

>> "CREATE INDEX I\_$src_table_name ON $src_table_name (creation_date)^"
>> "COMMIT^"
>> ""
>> "EXECUTE BLOCK"
>> "AS"
    foreach field $data_fields {
      set var_type [[$field domain] attr TYPE]  	
      if {$var_type == "CHAR" || $var_type == "VARCHAR"} {
	    append var_type ([$field attr LEN])
  	  } elseif {$var_type == "NUMERIC"} {
	    append var_type ([$field attr LEN], [$field attr DEC])
  	  }
>> "DECLARE VARIABLE [getFieldVar $field] $var_type;"
      if {[lsearch $version_fields $field] != -1} then {
>> "DECLARE VARIABLE p_[getFieldVar $field] $var_type;"	
      }
    }

>> "DECLARE VARIABLE changed_by INTEGER;"
>> "DECLARE VARIABLE creation_date TIMESTAMP;"
>> "DECLARE VARIABLE effective_date DATE;"
>> "DECLARE VARIABLE effective_until DATE;"
>> "DECLARE VARIABLE active_record CHAR(1);"
>> "DECLARE VARIABLE curr_effective_date DATE;"
>> "DECLARE VARIABLE transaction_nbr INTEGER;"
>> "DECLARE VARIABLE rec_version INTEGER;"
>> "DECLARE VARIABLE max_tbl_nbr INTEGER;"
>> "BEGIN"
>> "  curr_effective_date = NULL;"
>> "  transaction_nbr = NULL;"
>> "  max_tbl_nbr = 0;"
>> ""
>>> "  FOR SELECT "    
    foreach field $data_fields {
	  set s [string tolower [getOldFieldName $field]];
      if {$s == "user_id" || $s == "start_time" || $s == "nbr"}	{
>>> "$src_table_name."  	  
	  }
>>> "$s, "  
    }
>> "changed_by, creation_date, effective_date, active_record, t.nbr"  
>> "  FROM $src_table_name, ev_transaction t"
>> "  WHERE t.user_id = changed_by AND t.start_time = creation_date"
>> "  ORDER BY creation_date, effective_date"
>>> "  INTO "
    foreach field $data_fields {
>>> ":[getFieldVar $field], "  
    }
>> ":changed_by, :creation_date, :effective_date, :active_record, :transaction_nbr"    
>> "  DO"                     
>> "  BEGIN"
>> "    rdb\$set_context('USER_TRANSACTION', 'TRN_ID', :transaction_nbr);"
>> ""
>> "    IF (max_tbl_nbr < $key_field_name) THEN" 
>> "      max_tbl_nbr = $key_field_name;"
>> ""
>> "    /* Apply record operation */"
>> "    rec_version = NULL;"

    if {[isVerTable $table]} then {
>>> "    SELECT rec_version, effective_date"
      foreach field $version_fields {  
>>> ", [string tolower [$field attr NAME]]"    
      }
>> ""	
>> "    FROM $dest_table_name "
>> "    WHERE $key_field_name = :$key_field_name and effective_date <= :effective_date"
>> "    ORDER BY effective_date DESC"
>> "    ROWS 1"
>>> "    INTO :rec_version, :curr_effective_date"
      foreach field $version_fields {  
>>> ", :p_[getFieldVar $field]" 
      }
>> ";"		  
	} else {
>> "    SELECT rec_version, effective_date FROM $dest_table_name WHERE $key_field_name = :$key_field_name INTO :rec_version, :curr_effective_date;"		
    }	
	
>> ""
    if {[isVerTable $table]} then {
>>> "    IF ((active_record <> 'N') AND ((rec_version IS NULL) OR (curr_effective_date IS DISTINCT FROM effective_date) AND ("
      set item_count 0
      foreach field $version_fields {  
	    if {$item_count > 0} then {
>>> " OR "
	  }
>>> "(p_[getFieldVar $field] IS DISTINCT FROM [getFieldVar $field])"    
        incr item_count
      }
>> "))) THEN"  	
>> "    BEGIN"
>> "      rec_version = NULL;"
>>> "      SELECT rec_version"
      foreach field $version_fields {  
>>> ", [string tolower [$field attr NAME]]"    
      }
>> ""	
>> "      FROM $dest_table_name"
>> "      WHERE $key_field_name = :$key_field_name and effective_date > :effective_date"
>> "      ORDER BY effective_date"
>> "      ROWS 1"
>>> "      INTO :rec_version"
      foreach field $version_fields {  
>>> ", :p_[getFieldVar $field]" 
      }
>> ";"	
>> ""
>>> "      IF ((rec_version IS NULL) OR (curr_effective_date IS DISTINCT FROM effective_date) AND ("
      set item_count 0
      foreach field $version_fields {  
	    if {$item_count > 0} then {
>>> " OR "
	    } 
>>> "(p_[getFieldVar $field] IS DISTINCT FROM [getFieldVar $field])"    
        incr item_count
      }
>> ")) THEN"
>> "      BEGIN"
>> "        /* Add record version */"
>>> "        INSERT INTO $dest_table_name ("
    foreach field $data_fields {
>>> "[string tolower [$field attr NAME]], "  
    }
>> "effective_date)"
>>> "        VALUES ("
    foreach field $data_fields {
>>> ":[getFieldVar $field], "  
    }
>> ":effective_date);"
>> "      END"
>> "      ELSE"
>> "      BEGIN"
>>> "        UPDATE $dest_table_name SET "
    foreach field $data_fields {
      if {[string tolower [$field attr NAME]] == $key_field_name} then { continue }
>>> "[string tolower [$field attr NAME]] = :[getFieldVar $field], "  
    }
>> "effective_date = :effective_date"
>> "        WHERE rec_version = :rec_version;"
>> "      END"
>> "    END"
  } else {
>> "    IF (rec_version IS NULL) THEN"
>> "    BEGIN"
>> "      /* Add record version */"
>> "      IF (active_record <> 'N') THEN"
>> "      BEGIN"
>> "        effective_until = NULL;"
>>> "      INSERT INTO $dest_table_name ("
    foreach field $data_fields {
>>> "[string tolower [$field attr NAME]], "  
    }
>> "effective_date, effective_until)"
>>> "      VALUES ("
    foreach field $data_fields {
>>> ":[getFieldVar $field], "  
    }
>> ":effective_date, :effective_until);"
>> "      END"
>> "    END"
  }

>> "    ELSE"
>> "    BEGIN"
>> "      /* Update record version */"
>> "      IF (active_record = 'N') THEN"
>> "      BEGIN"
>> "        IF (rec_version IS NOT NULL) THEN"
>> "          EXECUTE PROCEDURE del_[string range $dest_table_name 0 26]($key_field_name);"
>> "      END"
>> "      ELSE"   
>> "      BEGIN" 
    if {[isVerTable $table] == [falsevalue]} then {
>> "        IF (curr_effective_date < effective_date) THEN"
>> "          effective_date = curr_effective_date;" 	
	}
>>> "        UPDATE $dest_table_name SET "
    set item_count 0
    foreach field $data_fields {
      if {[string tolower [$field attr NAME]] == $key_field_name} then { continue }
      if {$item_count > 0} then {
>>> ", "
	  } 	  
>>> "[string tolower [$field attr NAME]] = :[getFieldVar $field]"  
      incr item_count
    }
	if {[isVerTable $table] == [falsevalue]} then {
>>> ", effective_date = :effective_date, effective_until = '$EV_FAR_IN_FUTURE'"	
	}
>> ""	
>> "        WHERE rec_version = :rec_version;"
>> "      END"
>> "    END"
>> "  END"    
>> ""
>> "  EXECUTE STATEMENT 'ALTER SEQUENCE g_$dest_table_name RESTART WITH ' || max_tbl_nbr;"
>> "END^"
>> "COMMIT^"
>> ""
>> "DROP INDEX I\_$src_table_name^"
>> "COMMIT^"
>> ""
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  /* Restore integrity check and business logic */"
    set trigger_list [$table list_of_triggers]
    foreach trigger $trigger_list {    
      if {[string match Integrity:* [$trigger attr TITLE]] == 1 || [string match Business:* [$trigger attr TITLE]] == 1} then {	
>> "  IF (EXISTS(SELECT 1 FROM RDB\$TRIGGERS WHERE RDB\$TRIGGER_NAME = '[$trigger attr NAME]')) THEN"
>> "    EXECUTE STATEMENT 'ALTER TRIGGER [$trigger attr NAME] ACTIVE';"
      }
    }
>> "END^"
>> "COMMIT^"
>> ""
>> ""
  }  
>> "/*<progress>Creating indexes</progress>*/"  
>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE VARIABLE stmt VARCHAR(1000);"
>> "BEGIN"
>> "  /* Activate Indexes */"
>> "  FOR SELECT 'ALTER INDEX '||rdb\$index_name ||' ACTIVE;'"
>> "      FROM rdb\$indices"
>> "      WHERE (rdb\$system_flag IS NULL OR rdb\$system_flag = 0) AND"
>> "            (rdb\$unique_flag IS NULL OR rdb\$unique_flag = 0) AND"
>> "            rdb\$relation_name NOT STARTS 'EV_'"
>> "      ORDER BY rdb\$foreign_key"
>> "  INTO :stmt"
>> "  DO"
>> "    EXECUTE STATEMENT :stmt;"
>> "END^"
>> "COMMIT^"
>> ""
>> ""
>> "SET TERM ; ^"  
>> ""
>> ""

>> "/*<progress>Optimizing data</progress>*/"
>> "/* Delete empty transaction records */"   
>> "DELETE FROM ev_transaction t WHERE NOT EXISTS (SELECT nbr FROM ev_table_change WHERE ev_transaction_nbr = t.nbr);"
>> "COMMIT;"
>> ""
>> "/* Pack transaction records */"   
>> "ALTER TRIGGER T_BU_EV_TRANSACTION_1 INACTIVE;"
>> "ALTER TRIGGER T_BU_EV_TABLE_CHANGE_1 INACTIVE;"
>> "COMMIT;"
>> ""
>> "SET TERM ^;"  
>> ""
>> "EXECUTE BLOCK"
>> "AS"
>> "DECLARE VARIABLE nbr INTEGER;"
>> "DECLARE VARIABLE start_time TIMESTAMP;"
>> "DECLARE VARIABLE commit_time TIMESTAMP;"
>> "DECLARE VARIABLE user_id INTEGER;"
>> "DECLARE VARIABLE last_nbr INTEGER;"
>> "DECLARE VARIABLE last_start_time TIMESTAMP;"
>> "DECLARE VARIABLE last_commit_time TIMESTAMP;"
>> "DECLARE VARIABLE last_user_id INTEGER;"
>> "BEGIN"
>> "  last_start_time = NULL;"
>> "  last_commit_time = NULL;"
>> "  last_user_id = NULL;"
>> ""
>> "  FOR SELECT nbr, start_time, commit_time, user_id"
>> "      FROM ev_transaction"
>> "      ORDER BY start_time, nbr"
>> "      INTO :nbr, :start_time, :commit_time, :user_id"
>> "  DO"
>> "  BEGIN"
>> "    IF ((user_id IS DISTINCT FROM last_user_id) OR (commit_time - last_start_time > 0.003472222)) THEN /* five minutes threshold */"
>> "    BEGIN"
>> "      IF (last_commit_time IS NOT NULL) THEN"
>> "        UPDATE ev_transaction SET commit_time = :last_commit_time WHERE nbr = :last_nbr;"
>> ""
>> "      last_nbr = nbr;"
>> "      last_start_time = start_time;"
>> "      last_commit_time = NULL;"
>> "      last_user_id = user_id;"
>> "    END"
>> "    ELSE"
>> "    BEGIN"
>> "      UPDATE ev_table_change SET ev_transaction_nbr = :last_nbr WHERE ev_transaction_nbr = :nbr;"
>> "      DELETE FROM ev_transaction WHERE nbr = :nbr;"
>> "      last_commit_time = commit_time;"
>> "    END"
>> "  END"
>> ""
>> "  IF (last_commit_time IS NOT NULL) THEN"
>> "    UPDATE ev_transaction SET commit_time = :last_commit_time WHERE nbr = :last_nbr;"
>> "END^"
>> ""
>> "COMMIT^"
>> ""
>> "SET TERM ; ^"  
>> ""
>> "ALTER TRIGGER T_BU_EV_TRANSACTION_1 ACTIVE;"
>> "ALTER TRIGGER T_BU_EV_TABLE_CHANGE_1 ACTIVE;"
>> "COMMIT;"
>> ""

# Drop old tables
>> ""
>> "/* Drop old tables */"   
  foreach src_table_name $old_tables { 
>> "DROP TABLE $src_table_name;"
   } 
>> "COMMIT;"
>> ""
}

set script_fragment ./Evolution/Scripts/AfterConvert_[[dict get_model] attr MODEL_NAME].sql
if {[file exists $script_fragment] == 1} then {
>> "/* Finalize conversion */"
  outFile $script_fragment
>> ""  
>> ""  
}

if {$EV_TEMP_DB == [falsevalue]} then {
>> "SET TERM ^;"
>> ""
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  /* Pack duplicate versions */"   
>> "  EXECUTE PROCEDURE do_after_start_transaction(0, NULL);"
>> "  EXECUTE PROCEDURE pack_all;"
>> "  EXECUTE PROCEDURE do_before_commit_transaction(NULL);"
>> ""
>> "  WHEN ANY DO"
>> "  BEGIN"
>> "  END"
>> "END^"
>> "COMMIT^"
>> ""
>> "EXECUTE PROCEDURE UPDATE_INDEX_STATISTICS^"
>> "COMMIT^"
>> ""
}
>> "SET TERM ;^"
>> ""
