SET TERM ^;

/* PREPARE DB STRUCTURE */

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$trigger_name FROM rdb$triggers
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP TRIGGER ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$procedure_name FROM rdb$procedures
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP PROCEDURE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$generator_name FROM rdb$generators
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP SEQUENCE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$exception_name FROM rdb$exceptions
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP EXCEPTION ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$index_name FROM rdb$indices i WHERE
        (rdb$system_flag = 0 OR rdb$system_flag IS NULL) AND
        NOT EXISTS (SELECT rdb$index_name FROM rdb$relation_constraints
        WHERE rdb$index_name = i.rdb$index_name)
      INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP INDEX ' || s;
  END
END^

COMMIT^

SET TERM ;^


/* DROP TABLES */


DROP TABLE version_info;
DROP TABLE tmp_co_delivery_package;
DROP TABLE tmp_cl;
DROP TABLE tmp_cl_co_consolidation;
DROP TABLE tmp_cl_person;
DROP TABLE tmp_co;
DROP TABLE tmp_co_bank_account_register;
DROP TABLE tmp_co_fed_tax_liabilities;
DROP TABLE tmp_co_local_tax;
DROP TABLE tmp_co_local_tax_liabilities;
DROP TABLE tmp_co_manual_ach;
DROP TABLE tmp_co_pr_ach;
DROP TABLE tmp_co_reports;
DROP TABLE tmp_co_states;
DROP TABLE tmp_co_state_tax_liabilities;
DROP TABLE tmp_co_sui_liabilities;
DROP TABLE tmp_co_tax_deposits;
DROP TABLE tmp_co_tax_payment_ach;
DROP TABLE tmp_co_tax_return_queue;
DROP TABLE tmp_ee;
DROP TABLE tmp_pr;
DROP TABLE tmp_pr_miscellaneous_checks;
DROP TABLE tmp_pr_scheduled_event;
COMMIT;
