proc compare {a b} {
  if {[$a attr name] < [$b attr name]} then {
    return -1
  } elseif {[$a attr name] > [$b attr name]} then {
    return 1
  }
  return 0
}


>> "Table,Field,Type"
set table_list [lsort -command compare [dict list_of_tables]]
foreach table $table_list {
   set field_list [lsort -command compare [$table list_of_fields]]
                     
   set tbl [$table attr NAME]
   foreach field $field_list {
     if {[$field attr NAME] == "ACTIVE_RECORD"} {
       append tbl "   (historical)"
       break
     }
   }
>> "$tbl"

   foreach field $field_list {                   
     if {[$field attr NAME] == "ACTIVE_RECORD" || [$field attr NAME] == "EFFECTIVE_DATE" || [$field attr NAME] == "CREATION_DATE" || [$field attr NAME] == "CHANGED_BY"} {
       continue;
     }

     if {[$field attr LOGICAL] == [falsevalue]} {
>>> "[$table attr NAME],[$field attr NAME],[string tolower [$field attr type]]"

     if {[$field attr LEN] != 0} {
>>> "([$field attr LEN])"
     }                            

     if {[$field attr NULL] == [falsevalue]} {
>>> ",not NULL"
     }                            

>> ""
     }
   }

>> ",,"
>> ",,"
}
