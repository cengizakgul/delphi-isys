set EV_FAR_IN_PAST 1/1/1900
set EV_FAR_IN_FUTURE 12/31/9999

if {[[dict get_model] attr MODEL_NAME] == "Temp"} then {
  set EV_TEMP_DB [truevalue]
} else {
  set EV_TEMP_DB [falsevalue]
}


# Returns EV_TABLE.NBR
proc getTableNbr {table} {  
  set res [$table attr EV_ID]
  if {$res == 0} then {
    [error "Attribute EV_TABLE_NBR is not set for table [$table attr NAME]"]
  }
  return $res;
}


# Returns EV_FIELD.NBR     
proc getFieldNbr {field} {        
  set res [$field attr EV_ID]
  if {$res == 0} then {
    [error "Attribute EV_FIELD_NBR is not set for field [$field attr NAME]"]
  }
  return $res;
}                                           


# Check if table supports versioning 
proc isVerTable {table} {
  return [$table attr EV_VERSION]
}


# Check if field supports versioning 
proc isVerField {field} {
  return [$field attr EV_VERSION]
}


# Check if field is system
proc isSysField {field} {
  return [$field attr EV_SYSTEM]
}


# Check if table contains BLOB fields
proc hasBlobFields {table} {
  set field_list [$table list_of_fields]
  foreach field $field_list {
    if {[[$field domain] attr type] == "BLOB"} then {
      return [truevalue]
    }         
  }
  return [falsevalue]
}


# Check if table contains REC_VERSION field
proc isVerAwareTable {table} {
  set field_list [$table list_of_fields]
  foreach field $field_list {
    if {[$field attr name] == "REC_VERSION"} then {
      return [truevalue]
    }         
  }
  return [falsevalue]
}

# Check if field has DEFAULT_VALUE
proc hasDefaultValue {field} {
  set default_value [$field attr EV_DEFAULT]
  if {[ llength $default_value ]  > 0 } {
         return [truevalue]
	  
    } else {
         return [falsevalue]
    }
}

# Copies a file into output
proc outFile {fileName} {
  set f [open $fileName "r"]
  while {[gets $f line] >= 0} {
    >> $line
  }
  close $f
}


# Returns old field name if it was renamed
proc getOldFieldName {field} {
  set table_name [[$field entity] attr NAME]
  set field_name [$field attr NAME]

  if {$table_name == "SY_QUEUE_PRIORITY" && $field_name == "THREADS"} {
    set field_name PACKAGE_ID
  } elseif {$table_name == "SB_QUEUE_PRIORITY" && $field_name == "THREADS"} {
    set field_name PACKAGE_ID
  } elseif {$table_name == "SY_SEC_TEMPLATES" && $field_name == "DATA"} {
    set field_name SY_PCL_FONT_NBR
  }

  return $field_name
}


# Returns list of logical keys
proc getLKfields {table} {
  set lk_fields {}

  set field_list [$table list_of_fields]    

  set key_len 0
  foreach field $field_list {
    if {[$field attr EV_LK_NBR] > $key_len} then {
      set key_len [$field attr EV_LK_NBR]
    }
  }

  for {set i 1} {$i <= $key_len} {incr i} {    
    foreach field $field_list {
      if {[$field attr EV_LK_NBR] == $i} then {
        lappend lk_fields $field
      }
    }
  }

  return $lk_fields
}


# Check if trigger exists
proc hasTrigger {table trig_name} {
  set trig_list [$table list_of_triggers]
  return [trigInList $trig_list $trig_name]
}


# Check if trigger exists in list
proc trigInList {trig_list trig_name} {
  foreach trig $trig_list {
    if {[$trig attr name] == "$trig_name"} then {
      return [truevalue]
    }         
  }
  return [falsevalue]
}


# Check if procedure exists in list
proc procInList {proc_list proc_name} {
  foreach proc $proc_list {
    if {[$proc attr name] == "$proc_name"} then {
      return [truevalue]
    }         
  }
  return [falsevalue]
}


# Returns list of child fields in PK order
proc getRelationFK {rel} {  
  set pk_ind_list   [$rel  master_fields_names]
  if {[ llength $pk_ind_list ]  > 0 } {
    set fk_list [$rel list_of_foreign_keys]
    set count [llength $fk_list]
    if { $count == 0 } {
	  message "Relationship [$rel attr NAME] cannot be created (No Foreign Keys)"
    } else {
      set pk_list {}
   	  foreach fk $fk_list {
   	    lappend pk_list [[$fk parent $rel] attr NAME]
   	  }
      set fk_ordered_list {}
   	  foreach pk $pk_ind_list {
        set pos [lsearch  $pk_list $pk]
   	    lappend fk_ordered_list [lindex $fk_list $pos]
   	  }
    }
  }
  
  return $fk_ordered_list
}  

proc grantProc {prc} {
>> "GRANT EXECUTE ON PROCEDURE [$prc attr NAME] TO EUSER"
>> "^"
>> ""
}

proc syncEvDatabase {} {
  set table [dict table_by_name EV_DATABASE]
  set fld_version [$table field_by_name VERSION]
  set fld_descr [$table field_by_name DESCRIPTION]

>> "/* Update EV_DATABASE */"
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"
>> "  DELETE FROM ev_database;"
>> "  INSERT INTO ev_database (version, description) VALUES ('[$fld_version attr D_DEFAULT]', '[$fld_descr attr D_DEFAULT]');"
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"
>> "END"
>> "^"
>> ""
>> "COMMIT"
>> "^"
>> ""
}

proc checkConnected {} {
  if {[connected] == [falsevalue]} {
    [error "Please connect to the database"]
  }
}
