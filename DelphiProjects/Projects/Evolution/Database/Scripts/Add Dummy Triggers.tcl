source ./Evolution/Scripts/EvoLib.tcl 

set table_list [dict list_of_tables]
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [falsevalue]} then {
    set table_name [$table attr NAME]
    set trig_name [string range $table_name 0 22]	
	set trig_list [$table list_of_triggers]

    if {[isVerAwareTable $table]} then {	

	  set trig T_AI_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER INSERT POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

	  set trig T_AD_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER DELETE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
	  }
	  
	  set trig T_AU_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER UPDATE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

 	  set trig T_BI_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE INSERT POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

 	  set trig T_BU_$trig_name\_1
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE UPDATE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

  	  set trig T_AUD_$trig_name\_2
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER UPDATE OR DELETE POSITION 2"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }
	  
	  
  	  set trig T_BIU_$trig_name\_2
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE INSERT OR UPDATE POSITION 2"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""   
      }
	  
  	  set trig T_AIU_$trig_name\_3
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER INSERT OR UPDATE POSITION 3"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""   
     }

  	  set trig T_AD_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER DELETE POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }
	 
  	  set trig T_AU_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE AFTER UPDATE POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }

  	  set trig T_BUD_$trig_name\_9
      if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE UPDATE OR DELETE POSITION 9"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
      }	  
    }
		
    set trig T_BIU_$trig_name\_3
    if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE OR ALTER TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE INSERT OR UPDATE POSITION 3"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
    }	
  }
}
