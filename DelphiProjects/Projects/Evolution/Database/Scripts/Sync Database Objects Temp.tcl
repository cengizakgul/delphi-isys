source ./Evolution/Scripts/EvoLib.tcl 

set table_list [dict list_of_tables]

# Logical key indexes
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }
  
  set lk_fields [getLKfields $table] 
  
  if {[llength $lk_fields] > 0} then {
    set child_ref ""
    foreach lk_field $lk_fields {
	  if {$child_ref != ""} then {set child_ref $child_ref,}	
	  set child_ref $child_ref[$lk_field attr NAME]
    }		
>> "CREATE INDEX LK_[$table attr NAME] ON [string tolower [$table attr NAME]] ($child_ref)"	
>> "^" 
  }  
}

# Triggers
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [falsevalue]} then {
    set table_name [$table attr NAME]
    set trig_name [string range $table_name 0 22]	
	set trig_list [$table list_of_triggers]

    set trig T_BD_$trig_name\_1
    if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "CREATE TRIGGER $trig FOR $table_name"
>> "ACTIVE BEFORE DELETE POSITION 1"
>> "AS"
>> "BEGIN"
>> "END"
>> "^"
>> ""
    }
  }
}
