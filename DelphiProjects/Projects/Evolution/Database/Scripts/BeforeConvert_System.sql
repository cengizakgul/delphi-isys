/* FIX DATA INTEGRITY ISSUES */


/* Update empty blobs */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE tbl_name VARCHAR(64);
DECLARE VARIABLE fld_name VARCHAR(64);
DECLARE VARIABLE sql VARCHAR(1024);
BEGIN
  FOR SELECT TRIM(r.rdb$relation_name), TRIM(f.rdb$field_name)
      FROM rdb$relations r, rdb$relation_fields f, rdb$fields dt
      WHERE
        (r.rdb$system_flag = 0 OR r.rdb$system_flag IS NULL) AND
        (f.rdb$system_flag = 0 OR f.rdb$system_flag IS NULL) AND
        f.rdb$relation_name = r.rdb$relation_name AND
        (f.rdb$null_flag = 0 OR f.rdb$null_flag IS NULL) AND
        f.rdb$field_source = dt.rdb$field_name AND
        dt.rdb$field_type = 261
  INTO :tbl_name, :fld_name
  DO
  BEGIN
    sql = 'UPDATE ' || tbl_name || ' SET ' || fld_name || ' = NULL WHERE GetBlobSize(' || fld_name || ') = 0';

    EXECUTE STATEMENT sql;
  END
END^

COMMIT^


/* FIX DATA INTEGRITY ISSUES */
UPDATE SY_GLOBAL_AGENCY SET RECEIVING_ACCOUNT_TYPE = 'C' WHERE RECEIVING_ACCOUNT_TYPE IS NULL^
UPDATE SY_GL_AGENCY_REPORT SET DEPOSIT_FREQUENCY = 'Z' WHERE DEPOSIT_FREQUENCY IS NULL^
UPDATE SY_LOCALS SET MINIMUM_HOURS_WORKED_PER = ' ' WHERE MINIMUM_HOURS_WORKED_PER IS NULL^
UPDATE SY_LOCAL_DEPOSIT_FREQ SET THRD_MNTH_DUE_END_OF_NEXT_MNTH = 'N' WHERE THRD_MNTH_DUE_END_OF_NEXT_MNTH IS NULL^
UPDATE SY_STATES SET INC_SUI_TAX_PAYMENT_CODE = ' ' WHERE INC_SUI_TAX_PAYMENT_CODE IS NULL^
UPDATE SY_STATE_DEPOSIT_FREQ SET INC_TAX_PAYMENT_CODE = 'N' WHERE INC_TAX_PAYMENT_CODE IS NULL^
UPDATE SY_STATE_DEPOSIT_FREQ SET THRD_MNTH_DUE_END_OF_NEXT_MNTH = 'N' WHERE THRD_MNTH_DUE_END_OF_NEXT_MNTH IS NULL^
UPDATE SY_SUI SET USE_MISC_TAX_RETURN_CODE = ' ' WHERE USE_MISC_TAX_RETURN_CODE IS NULL^
COMMIT^


/* SY_COUNTY */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_COUNTY';
  FOR
  SELECT SY_COUNTY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_COUNTY_NBR SY_COUNTY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCALS WHERE SY_COUNTY_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_COUNTY WHERE SY_COUNTY_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_COUNTY SET effective_date = :effective_date WHERE SY_COUNTY_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_DELIVERY_SERVICE */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_DELIVERY_SERVICE';
  FOR
  SELECT SY_DELIVERY_SERVICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_DELIVERY_SERVICE_NBR SY_DELIVERY_SERVICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_DELIVERY_METHOD WHERE SY_DELIVERY_SERVICE_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_DELIVERY_SERVICE WHERE SY_DELIVERY_SERVICE_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_DELIVERY_SERVICE SET effective_date = :effective_date WHERE SY_DELIVERY_SERVICE_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_GLOBAL_AGENCY */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_GLOBAL_AGENCY';
  FOR
  SELECT SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_STATE_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATES WHERE SY_STATE_TAX_PMT_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATE_REPORTING_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATES WHERE SY_STATE_REPORTING_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_LOCAL_REPORTING_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCALS WHERE SY_LOCAL_REPORTING_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_LOCAL_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCALS WHERE SY_LOCAL_TAX_PMT_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_GL_AGENCY_FIELD_OFFICE WHERE SY_GLOBAL_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_FED_REPORTING_AGENCY WHERE SY_GLOBAL_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_FED_TAX_PAYMENT_AGENCY WHERE SY_GLOBAL_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_SUI_REPORTING_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_SUI WHERE SY_SUI_REPORTING_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_SUI_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_SUI WHERE SY_SUI_TAX_PMT_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_AGENCY_DEPOSIT_FREQ WHERE SY_GLOBAL_AGENCY_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_GL_AGENCY_HOLIDAYS WHERE SY_GLOBAL_AGENCY_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_GLOBAL_AGENCY WHERE SY_GLOBAL_AGENCY_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_GLOBAL_AGENCY SET effective_date = :effective_date WHERE SY_GLOBAL_AGENCY_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_GL_AGENCY_FIELD_OFFICE */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_GL_AGENCY_FIELD_OFFICE';
  FOR
  SELECT SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_GL_AGENCY_FIELD_OFFICE_NBR SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_GL_AGENCY_REPORT WHERE SY_GL_AGENCY_FIELD_OFFICE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT TAX_PAYMENT_FIELD_OFFICE_NBR SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_GLOBAL_AGENCY WHERE TAX_PAYMENT_FIELD_OFFICE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GL_AGENCY_FIELD_OFFICE_NBR SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE SY_GL_AGENCY_FIELD_OFFICE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT QE_SY_GL_AGENCY_OFFICE_NBR SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE QE_SY_GL_AGENCY_OFFICE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_GL_AGENCY_FIELD_OFFICE_NBR SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE SY_GL_AGENCY_FIELD_OFFICE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT QE_SY_GL_AGENCY_OFFICE_NBR SY_GL_AGENCY_FIELD_OFFICE_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE QE_SY_GL_AGENCY_OFFICE_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_GL_AGENCY_FIELD_OFFICE WHERE SY_GL_AGENCY_FIELD_OFFICE_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_GL_AGENCY_FIELD_OFFICE SET effective_date = :effective_date WHERE SY_GL_AGENCY_FIELD_OFFICE_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_GL_AGENCY_REPORT */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_GL_AGENCY_REPORT';
  FOR
  SELECT SY_GL_AGENCY_REPORT_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_GL_AGENCY_REPORT_NBR SY_GL_AGENCY_REPORT_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_REPORTS_GROUP WHERE SY_GL_AGENCY_REPORT_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_GL_AGENCY_REPORT WHERE SY_GL_AGENCY_REPORT_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_GL_AGENCY_REPORT SET effective_date = :effective_date WHERE SY_GL_AGENCY_REPORT_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_LOCALS */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_LOCALS';
  FOR
  SELECT SY_LOCALS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_LOCALS_NBR SY_LOCALS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE SY_LOCALS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_LOCALS_NBR SY_LOCALS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_EXEMPTIONS WHERE SY_LOCALS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_LOCALS_NBR SY_LOCALS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_MARITAL_STATUS WHERE SY_LOCALS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_LOCALS_NBR SY_LOCALS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_TAX_CHART WHERE SY_LOCALS_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_LOCALS WHERE SY_LOCALS_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_LOCALS SET effective_date = :effective_date WHERE SY_LOCALS_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_LOCAL_DEPOSIT_FREQ */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_LOCAL_DEPOSIT_FREQ';
  FOR
  SELECT SY_LOCAL_DEPOSIT_FREQ_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT FIRST_THRESHOLD_DEP_FREQ_NBR SY_LOCAL_DEPOSIT_FREQ_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE FIRST_THRESHOLD_DEP_FREQ_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SECOND_THRESHOLD_DEP_FREQ_NBR SY_LOCAL_DEPOSIT_FREQ_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE SECOND_THRESHOLD_DEP_FREQ_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_LOCAL_DEPOSIT_FREQ WHERE SY_LOCAL_DEPOSIT_FREQ_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_LOCAL_DEPOSIT_FREQ SET effective_date = :effective_date WHERE SY_LOCAL_DEPOSIT_FREQ_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_LOCAL_MARITAL_STATUS */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_LOCAL_MARITAL_STATUS';
  FOR
  SELECT SY_LOCAL_MARITAL_STATUS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_LOCAL_MARITAL_STATUS_NBR SY_LOCAL_MARITAL_STATUS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_TAX_CHART WHERE SY_LOCAL_MARITAL_STATUS_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_LOCAL_MARITAL_STATUS WHERE SY_LOCAL_MARITAL_STATUS_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_LOCAL_MARITAL_STATUS SET effective_date = :effective_date WHERE SY_LOCAL_MARITAL_STATUS_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_REPORTS */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_REPORTS';
  FOR
  SELECT SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_TAX_PMT_REPORT_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATES WHERE SY_TAX_PMT_REPORT_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_TAX_PMT_REPORT_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCALS WHERE SY_TAX_PMT_REPORT_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_REPORTS_GROUP WHERE SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT CUSTOM_DEBIT_SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_GLOBAL_AGENCY WHERE CUSTOM_DEBIT_SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_FED_TAX_PAYMENT_AGENCY WHERE SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT TAX_COUPON_SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE TAX_COUPON_SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT QE_TAX_COUPON_SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE QE_TAX_COUPON_SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT TAX_COUPON_SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE TAX_COUPON_SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT QE_TAX_COUPON_SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE QE_TAX_COUPON_SY_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_TAX_PMT_REPORT_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_SUI WHERE SY_TAX_PMT_REPORT_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT TAX_COUPON_SY_REPORTS_NBR SY_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_SUI WHERE TAX_COUPON_SY_REPORTS_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_REPORTS WHERE SY_REPORTS_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_REPORTS SET effective_date = :effective_date WHERE SY_REPORTS_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_REPORTS_GROUP */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_REPORTS_GROUP';
  FOR
  SELECT SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_REPORTS_GROUP_NBR SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_REPORTS WHERE SY_REPORTS_GROUP_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_GROUP_NBR SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_GL_AGENCY_REPORT WHERE SY_REPORTS_GROUP_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_GROUP_NBR SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_DEPOSIT_FREQ WHERE SY_REPORTS_GROUP_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_GROUP_NBR SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE SY_REPORTS_GROUP_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_GROUP_NBR SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_SUI WHERE SY_REPORTS_GROUP_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORTS_GROUP_NBR SY_REPORTS_GROUP_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_AGENCY_DEPOSIT_FREQ WHERE SY_REPORTS_GROUP_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_REPORTS_GROUP WHERE SY_REPORTS_GROUP_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_REPORTS_GROUP SET effective_date = :effective_date WHERE SY_REPORTS_GROUP_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_REPORT_GROUPS */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_REPORT_GROUPS';
  FOR
  SELECT SY_REPORT_GROUPS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_REPORT_GROUPS_NBR SY_REPORT_GROUPS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_REPORT_GROUP_MEMBERS WHERE SY_REPORT_GROUPS_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_REPORT_GROUPS WHERE SY_REPORT_GROUPS_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_REPORT_GROUPS SET effective_date = :effective_date WHERE SY_REPORT_GROUPS_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_REPORT_WRITER_REPORTS */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_REPORT_WRITER_REPORTS';
  FOR
  SELECT SY_REPORT_WRITER_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_REPORT_WRITER_REPORTS_NBR SY_REPORT_WRITER_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_REPORTS WHERE SY_REPORT_WRITER_REPORTS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_REPORT_WRITER_REPORTS_NBR SY_REPORT_WRITER_REPORTS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_REPORT_GROUP_MEMBERS WHERE SY_REPORT_WRITER_REPORTS_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_REPORT_WRITER_REPORTS WHERE SY_REPORT_WRITER_REPORTS_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_REPORT_WRITER_REPORTS SET effective_date = :effective_date WHERE SY_REPORT_WRITER_REPORTS_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_STATES */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_STATES';
  FOR
  SELECT SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_COUNTY WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCALS WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_MARITAL_STATUS WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT MAIN_STATE_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_RECIPROCATED_STATES WHERE MAIN_STATE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT PARTICIPANT_STATE_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_RECIPROCATED_STATES WHERE PARTICIPANT_STATE_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_EXEMPTIONS WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_TAX_CHART WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_SUI WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATES_NBR SY_STATES_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_HR_REFUSAL_REASON WHERE SY_STATES_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_STATES WHERE SY_STATES_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_STATES SET effective_date = :effective_date WHERE SY_STATES_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_STATE_DEPOSIT_FREQ */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_STATE_DEPOSIT_FREQ';
  FOR
  SELECT SY_STATE_DEPOSIT_FREQ_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT THRESHOLD_DEP_FREQUENCY_NUMBER SY_STATE_DEPOSIT_FREQ_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE THRESHOLD_DEP_FREQUENCY_NUMBER IS NOT NULL GROUP BY 1
       UNION
       SELECT SECOND_THRESHOLD_DEP_FREQ_NBR SY_STATE_DEPOSIT_FREQ_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_DEPOSIT_FREQ WHERE SECOND_THRESHOLD_DEP_FREQ_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_STATE_DEPOSIT_FREQ WHERE SY_STATE_DEPOSIT_FREQ_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_STATE_DEPOSIT_FREQ SET effective_date = :effective_date WHERE SY_STATE_DEPOSIT_FREQ_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* SY_STATE_MARITAL_STATUS */
EXECUTE BLOCK
RETURNS (table_name VARCHAR(32), nbr INTEGER, issue_text VARCHAR(255))
AS
DECLARE VARIABLE curr_date TIMESTAMP;
DECLARE VARIABLE effective_date TIMESTAMP;
BEGIN
  table_name = 'SY_STATE_MARITAL_STATUS';
  FOR
  SELECT SY_STATE_MARITAL_STATUS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE
  FROM (
       SELECT SY_STATE_MARITAL_STATUS_NBR SY_STATE_MARITAL_STATUS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_LOCAL_MARITAL_STATUS WHERE SY_STATE_MARITAL_STATUS_NBR IS NOT NULL GROUP BY 1
       UNION
       SELECT SY_STATE_MARITAL_STATUS_NBR SY_STATE_MARITAL_STATUS_NBR, MIN(EFFECTIVE_DATE) EFFECTIVE_DATE FROM SY_STATE_TAX_CHART WHERE SY_STATE_MARITAL_STATUS_NBR IS NOT NULL GROUP BY 1
   )
  GROUP BY 1
  INTO :nbr, :effective_date
  DO
  BEGIN
    curr_date = NULL;
    SELECT MIN(effective_date) FROM SY_STATE_MARITAL_STATUS WHERE SY_STATE_MARITAL_STATUS_NBR = :nbr INTO :curr_date;

    IF (curr_date IS NULL) THEN
    BEGIN
      issue_text = 'Does not exist!';
      SUSPEND;
    END
    ELSE IF (curr_date > effective_date) THEN
    BEGIN
      UPDATE SY_STATE_MARITAL_STATUS SET effective_date = :effective_date WHERE SY_STATE_MARITAL_STATUS_NBR = :nbr AND effective_date = :curr_date AND active_record <> 'N';
      IF (ROW_COUNT > 0) THEN
        issue_text = 'EFFECTIVE DATE corrected   old=' || formatdate(curr_date, 'mm/dd/yyyy hh:nn:ss') || '   new=' || formatdate(effective_date, 'mm/dd/yyyy hh:nn:ss');
      ELSE
        issue_text = 'Starts as deleted!';
      SUSPEND;
    END
  END
END
^
COMMIT
^


/* CREATE SY_REPORTS_VERSIONS */

CREATE TABLE sy_reports_versions (
    sy_reports_versions_nbr       INTEGER NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    changed_by                    INTEGER NOT NULL,
    db_version                    VARCHAR(20),
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    status_by                     INTEGER NOT NULL,
    current_report                CHAR(1) NOT NULL,
    modified_on                   TIMESTAMP NOT NULL,
    ticket                        VARCHAR(20),
    sy_report_writer_reports_nbr  INTEGER NOT NULL,
    report_data                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    class_name                    VARCHAR(40),
    ancestor_class_name           VARCHAR(40),
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80
)^

CREATE GENERATOR sy_reports_versions_gen^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE sy_report_writer_reports_nbr INTEGER;
DECLARE VARIABLE report_data BLOB SUB_TYPE 0 SEGMENT SIZE 80;
DECLARE VARIABLE notes BLOB SUB_TYPE 0 SEGMENT SIZE 80;
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE effective_date TIMESTAMP;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE report_status CHAR(1);
DECLARE VARIABLE class_name VARCHAR(40);
DECLARE VARIABLE ancestor_class_name VARCHAR(40);
DECLARE VARIABLE sy_reports_versions_nbr INTEGER;
DECLARE VARIABLE current_report CHAR(1);
DECLARE VARIABLE status CHAR(1);
DECLARE VARIABLE report_ver INTEGER;
DECLARE VARIABLE status_date TIMESTAMP;
DECLARE VARIABLE status_by INTEGER;
DECLARE VARIABLE ticket VARCHAR(20);
DECLARE VARIABLE db_version_int INTEGER;
DECLARE VARIABLE db_version VARCHAR(20);
DECLARE VARIABLE modified_on TIMESTAMP;
DECLARE VARIABLE p_sy_report_writer_reports_nbr INTEGER;
DECLARE VARIABLE p_report_status CHAR(1);
DECLARE VARIABLE p_effective_date TIMESTAMP;
DECLARE VARIABLE p_creation_date TIMESTAMP;
DECLARE VARIABLE p_report_data BLOB SUB_TYPE 0 SEGMENT SIZE 80;
BEGIN
  p_sy_report_writer_reports_nbr = NULL;
  ticket = NULL;

  FOR SELECT sy_report_writer_reports_nbr, report_file, notes, changed_by, effective_date, creation_date, active_record, class_name, ancestor_class_name, report_status
      FROM sy_report_writer_reports
      ORDER BY sy_report_writer_reports_nbr, effective_date, creation_date
  INTO :sy_report_writer_reports_nbr, :report_data, :notes, :changed_by, :effective_date, :creation_date, :active_record, :class_name, :ancestor_class_name, :report_status
  DO
  BEGIN
    IF ((p_sy_report_writer_reports_nbr IS DISTINCT FROM sy_report_writer_reports_nbr) OR (p_report_status = 'V' AND report_status = 'M')) THEN
      sy_reports_versions_nbr = NEXT VALUE FOR sy_reports_versions_gen;

    IF (p_sy_report_writer_reports_nbr IS DISTINCT FROM sy_report_writer_reports_nbr) THEN
    BEGIN
      db_version_int = 0;
      p_report_status = ' ';
      p_report_data = NULL;
      modified_on = creation_date;
      p_sy_report_writer_reports_nbr = sy_report_writer_reports_nbr;
    END

    IF ((p_sy_report_writer_reports_nbr = sy_report_writer_reports_nbr) AND (p_report_status IS DISTINCT FROM report_status)) THEN
    BEGIN
      IF (report_status = 'V') THEN
      BEGIN
        status = 'P';
        db_version_int = db_version_int + 1;
        db_version = CAST(db_version_int AS VARCHAR(20));
      END
      ELSE
      BEGIN
        status = 'I';
        db_version = NULL;
      END

      status_by = changed_by;
      status_date = creation_date;

      p_report_status = report_status;
    END

    IF ((p_sy_report_writer_reports_nbr = sy_report_writer_reports_nbr) AND (p_report_data IS DISTINCT FROM report_data)) THEN
    BEGIN
      modified_on = creation_date;
      p_report_data = report_data;
    END

    IF (active_record = 'C') THEN
      current_report = 'Y';
    ELSE
      current_report = 'N';

    INSERT INTO sy_reports_versions (sy_reports_versions_nbr, effective_date, creation_date, active_record, changed_by,
                                     db_version, status, status_date, status_by, current_report, modified_on, ticket,
                                     sy_report_writer_reports_nbr, report_data, class_name, ancestor_class_name, notes)
    VALUES (:sy_reports_versions_nbr, :effective_date, :creation_date, :active_record, :changed_by,
            :db_version, :status, :status_date, :status_by, :current_report, :modified_on, :ticket,
            :sy_report_writer_reports_nbr, :report_data, :class_name, :ancestor_class_name, :notes);

  END

  DELETE FROM sy_report_writer_reports WHERE active_record <> 'C';
END^

COMMIT^


/* PREPARE DB STRUCTURE */

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$trigger_name FROM rdb$triggers
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP TRIGGER ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$procedure_name FROM rdb$procedures
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP PROCEDURE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$generator_name FROM rdb$generators
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP SEQUENCE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$exception_name FROM rdb$exceptions
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP EXCEPTION ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$index_name FROM rdb$indices i WHERE
        (rdb$system_flag = 0 OR rdb$system_flag IS NULL) AND
        NOT EXISTS (SELECT rdb$index_name FROM rdb$relation_constraints
        WHERE rdb$index_name = i.rdb$index_name)
      INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP INDEX ' || s;
  END
END^

COMMIT^

SET TERM ;^


/* DROP JUNK TABLES */

DROP TABLE SY_EXCEPTIONS;
DROP TABLE VERSION_INFO;
DROP TABLE CHANGE_LOG;
DROP TABLE TRANSACTION_INFO;
DROP TABLE SY_PATCH_INFO;

COMMIT;


/* Prepare X_TRANSACTION table */

CREATE TABLE x_transaction (
    start_time   TIMESTAMP,
    user_id      INTEGER);
COMMIT;

INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_FED_TAX_TABLE;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_FED_TAX_TABLE_BRACKETS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_HR_OSHA_ANATOMIC_CODES;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_HR_HANDICAPS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_FED_EXEMPTIONS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_HR_EEO;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_HR_INJURY_CODES;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_HR_ETHNICITY;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_REPORTS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_GLOBAL_AGENCY;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_GL_AGENCY_FIELD_OFFICE;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_FED_REPORTING_AGENCY;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_STATES;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_COUNTY;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_LOCALS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_STATE_DEPOSIT_FREQ;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_STATE_MARITAL_STATUS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_SUI;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_LOCAL_DEPOSIT_FREQ;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_STATE_TAX_CHART;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_STATE_EXEMPTIONS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_LOCAL_EXEMPTIONS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_FED_TAX_PAYMENT_AGENCY;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_LOCAL_MARITAL_STATUS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_LOCAL_TAX_CHART;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_GL_AGENCY_REPORT;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_QUEUE_PRIORITY;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_REPORT_WRITER_REPORTS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_REPORTS_GROUP;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_GL_AGENCY_HOLIDAYS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_SEC_TEMPLATES;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_STORAGE;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_DELIVERY_SERVICE;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_MEDIA_TYPE;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_DELIVERY_METHOD;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_RECIPROCATED_STATES;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_REPORT_GROUPS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_REPORT_GROUP_MEMBERS;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_HR_REFUSAL_REASON;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_AGENCY_DEPOSIT_FREQ;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM SY_REPORTS_VERSIONS;
COMMIT;



/* CREATE TEMP TABLES */

CREATE TABLE XY_AGENCY_DEPOSIT_FREQ (
    SY_AGENCY_DEPOSIT_FREQ_NBR   INTEGER NOT NULL,
    CREATION_DATE                TIMESTAMP NOT NULL,
    EFFECTIVE_DATE               TIMESTAMP NOT NULL,
    ACTIVE_RECORD                CHAR(1) NOT NULL,
    CHANGED_BY                   INTEGER NOT NULL,
    DESCRIPTION                  VARCHAR(20) NOT NULL,
    FREQUENCY_TYPE               CHAR(1) NOT NULL,
    NUMBER_OF_DAYS_FOR_WHEN_DUE  INTEGER NOT NULL,
    SY_REPORTS_GROUP_NBR         INTEGER,
    SY_GLOBAL_AGENCY_NBR         INTEGER NOT NULL,
    TAX_PAYMENT_TYPE_CODE        VARCHAR(10),
    WHEN_DUE_TYPE                CHAR(1) NOT NULL
);


CREATE TABLE XY_COUNTY (
    SY_COUNTY_NBR   INTEGER NOT NULL,
    COUNTY_NAME     VARCHAR(40) NOT NULL,
    SY_STATES_NBR   INTEGER NOT NULL,
    CHANGED_BY      INTEGER NOT NULL,
    CREATION_DATE   TIMESTAMP NOT NULL,
    EFFECTIVE_DATE  TIMESTAMP NOT NULL,
    ACTIVE_RECORD   CHAR(1) NOT NULL
);


CREATE TABLE XY_DELIVERY_METHOD (
    SY_DELIVERY_METHOD_NBR   INTEGER NOT NULL,
    SY_DELIVERY_SERVICE_NBR  INTEGER NOT NULL,
    CLASS_NAME               VARCHAR(40) NOT NULL,
    NAME                     VARCHAR(40) NOT NULL,
    EFFECTIVE_DATE           TIMESTAMP NOT NULL,
    CHANGED_BY               INTEGER NOT NULL,
    CREATION_DATE            TIMESTAMP NOT NULL,
    ACTIVE_RECORD            CHAR(1) NOT NULL
);


CREATE TABLE XY_DELIVERY_SERVICE (
    SY_DELIVERY_SERVICE_NBR  INTEGER NOT NULL,
    CLASS_NAME               VARCHAR(40) NOT NULL,
    NAME                     VARCHAR(40) NOT NULL,
    EFFECTIVE_DATE           TIMESTAMP NOT NULL,
    CHANGED_BY               INTEGER NOT NULL,
    CREATION_DATE            TIMESTAMP NOT NULL,
    ACTIVE_RECORD            CHAR(1) NOT NULL
);


CREATE TABLE XY_FED_EXEMPTIONS (
    SY_FED_EXEMPTIONS_NBR     INTEGER NOT NULL,
    E_D_CODE_TYPE             VARCHAR(2) NOT NULL,
    EXEMPT_FEDERAL            CHAR(1) NOT NULL,
    EXEMPT_FUI                CHAR(1) NOT NULL,
    EXEMPT_EMPLOYEE_OASDI     CHAR(1) NOT NULL,
    EXEMPT_EMPLOYER_OASDI     CHAR(1) NOT NULL,
    EXEMPT_EMPLOYEE_MEDICARE  CHAR(1) NOT NULL,
    EXEMPT_EMPLOYER_MEDICARE  CHAR(1) NOT NULL,
    EXEMPT_EMPLOYEE_EIC       CHAR(1) NOT NULL,
    W2_BOX                    VARCHAR(4),
    CHANGED_BY                INTEGER NOT NULL,
    CREATION_DATE             TIMESTAMP NOT NULL,
    EFFECTIVE_DATE            TIMESTAMP NOT NULL,
    ACTIVE_RECORD             CHAR(1) NOT NULL
);


CREATE TABLE XY_FED_REPORTING_AGENCY (
    SY_FED_REPORTING_AGENCY_NBR  INTEGER NOT NULL,
    SY_GLOBAL_AGENCY_NBR         INTEGER NOT NULL,
    CHANGED_BY                   INTEGER NOT NULL,
    CREATION_DATE                TIMESTAMP NOT NULL,
    EFFECTIVE_DATE               TIMESTAMP NOT NULL,
    ACTIVE_RECORD                CHAR(1) NOT NULL
);


CREATE TABLE XY_FED_TAX_PAYMENT_AGENCY (
    SY_FED_TAX_PAYMENT_AGENCY_NBR  INTEGER NOT NULL,
    SY_GLOBAL_AGENCY_NBR           INTEGER NOT NULL,
    SY_REPORTS_NBR                 INTEGER NOT NULL,
    CHANGED_BY                     INTEGER NOT NULL,
    CREATION_DATE                  TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                  CHAR(1) NOT NULL
);


CREATE TABLE XY_FED_TAX_TABLE (
    SY_FED_TAX_TABLE_NBR            INTEGER NOT NULL,
    NFMW_EFFECTIVE_DATE             TIMESTAMP,
    NEXT_FED_TIP_CREDIT_BEGIN_DATE  TIMESTAMP,
    NEXT_SUPPL_TAX_PERCENT_DATE     TIMESTAMP,
    NEXT_OASDI_EFFECTIVE_DATE       TIMESTAMP,
    NEXT_OASDI_WL_EFFECTIVE_DATE    TIMESTAMP,
    NEXT_MEDICARE_RATE_BEGIN_DATE   TIMESTAMP,
    NEXT_MEDICARE_WL_EFF_DATE       TIMESTAMP,
    NEXT_FUTA_RATE_REAL_DATE        TIMESTAMP,
    NEXT_FUTA_RATE_CREDIT_DATE      TIMESTAMP,
    NEXT_FUTA_WL_EFFECTIVE_DATE     TIMESTAMP,
    NEXT_401K_LIMIT_EFFECTIVE_DATE  TIMESTAMP,
    NEXT_403B_LIMIT_EFFECTIVE_DATE  TIMESTAMP,
    NEXT_457_LIMIT_EFFECTIVE_DATE   TIMESTAMP,
    NEXT_501C_LIMIT_EFFECTIVE_DATE  TIMESTAMP,
    NEXT_SIMPLE_LIMIT_EFF_DATE      TIMESTAMP,
    NEXT_SEP_LIMIT_EFFECTIVE_DATE   TIMESTAMP,
    NEXT_DEFERRED_COMP_LIMIT_DATE   TIMESTAMP,
    FILLER                          VARCHAR(512),
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    NEXT_FDFT_AMOUNT_BEGIN_DATE     TIMESTAMP,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    EXEMPTION_AMOUNT                NUMERIC(18,6),
    FEDERAL_MINIMUM_WAGE            NUMERIC(18,6),
    NEXT_FEDERAL_MINIMUM_WAGE       NUMERIC(18,6),
    FEDERAL_TIP_CREDIT              NUMERIC(18,6),
    NEXT_FEDERAL_TIP_CREDIT         NUMERIC(18,6),
    SUPPLEMENTAL_TAX_PERCENTAGE     NUMERIC(18,6),
    NEXT_SUPPLEMENTAL_TAX_PERCENT   NUMERIC(18,6),
    OASDI_RATE                      NUMERIC(18,6),
    NEXT_OASDI_RATE                 NUMERIC(18,6),
    OASDI_WAGE_LIMIT                NUMERIC(18,6),
    NEXT_OASDI_WAGE_LIMIT           NUMERIC(18,6),
    MEDICARE_RATE                   NUMERIC(18,6),
    NEXT_MEDICARE_RATE              NUMERIC(18,6),
    MEDICARE_WAGE_LIMIT             NUMERIC(18,6),
    NEXT_MEDICARE_WAGE_LIMIT        NUMERIC(18,6),
    FUI_RATE_REAL                   NUMERIC(18,6),
    NEXT_FUTA_RATE_REAL             NUMERIC(18,6),
    FUI_RATE_CREDIT                 NUMERIC(18,6),
    NEXT_FUTA_RATE_CREDIT           NUMERIC(18,6),
    FUI_WAGE_LIMIT                  NUMERIC(18,6),
    NEXT_FUTA_WAGE_LIMIT            NUMERIC(18,6),
    SY_401K_LIMIT                   NUMERIC(18,6),
    NEXT_401K_LIMIT                 NUMERIC(18,6),
    SY_403B_LIMIT                   NUMERIC(18,6),
    NEXT_403B_LIMIT                 NUMERIC(18,6),
    SY_457_LIMIT                    NUMERIC(18,6),
    NEXT_457_LIMIT                  NUMERIC(18,6),
    SY_501C_LIMIT                   NUMERIC(18,6),
    NEXT_501C_LIMIT                 NUMERIC(18,6),
    SIMPLE_LIMIT                    NUMERIC(18,6),
    NEXT_SIMPLE_LIMIT               NUMERIC(18,6),
    SEP_LIMIT                       NUMERIC(18,6),
    NEXT_SEP_LIMIT                  NUMERIC(18,6),
    DEFERRED_COMP_LIMIT             NUMERIC(18,6),
    NEXT_DEFERRED_COMP_LIMIT        NUMERIC(18,6),
    FED_DEPOSIT_FREQ_THRESHOLD      NUMERIC(18,6),
    NEXT_FDFT_AMOUNT                NUMERIC(18,6),
    FIRST_EIC_LIMIT                 NUMERIC(18,6),
    FIRST_EIC_PERCENTAGE            NUMERIC(18,6),
    SECOND_EIC_LIMIT                NUMERIC(18,6),
    SECOND_EIC_AMOUNT               NUMERIC(18,6),
    THIRD_EIC_ADDITIONAL_PERCENT    NUMERIC(18,6),
    SY_CATCH_UP_LIMIT               NUMERIC(18,6) NOT NULL,
    SY_DEPENDENT_CARE_LIMIT         NUMERIC(18,6) NOT NULL,
    SY_HSA_SINGLE_LIMIT             NUMERIC(18,6) NOT NULL,
    SY_HSA_FAMILY_LIMIT             NUMERIC(18,6) NOT NULL,
    SY_PENSION_CATCH_UP_LIMIT       NUMERIC(18,6) NOT NULL,
    SY_SIMPLE_CATCH_UP_LIMIT        NUMERIC(18,6) NOT NULL,
    COMPENSATION_LIMIT              NUMERIC(18,6),
    SY_HSA_CATCH_UP_LIMIT           NUMERIC(18,6) NOT NULL,
    S132_PARKING_LIMIT              NUMERIC(18,6),
    ROTH_IRA_LIMIT                  NUMERIC(18,6),
    SY_IRA_CATCHUP_LIMIT            NUMERIC(18,6),
    EE_OASDI_RATE                   NUMERIC(18,6),
    ER_OASDI_RATE                   NUMERIC(18,6),
    EE_MED_TH_LIMIT                 NUMERIC(18,6),
    EE_MED_TH_RATE                  NUMERIC(18,6)
);


CREATE TABLE XY_FED_TAX_TABLE_BRACKETS (
    SY_FED_TAX_TABLE_BRACKETS_NBR  INTEGER NOT NULL,
    NEXT_GREATER_THAN_VALUE_DATE   TIMESTAMP,
    NEXT_LESS_THAN_VALUE_DATE      TIMESTAMP,
    MARITAL_STATUS                 CHAR(1) NOT NULL,
    NEXT_PERCENTAGE_BEGIN_DATE     TIMESTAMP,
    CHANGED_BY                     INTEGER NOT NULL,
    CREATION_DATE                  TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                  CHAR(1) NOT NULL,
    GREATER_THAN_VALUE             NUMERIC(18,6),
    NEXT_GREATER_THAN_VALUE        NUMERIC(18,6),
    LESS_THAN_VALUE                NUMERIC(18,6),
    NEXT_LESS_THAN_VALUE           NUMERIC(18,6),
    PERCENTAGE                     NUMERIC(18,6),
    NEXT_PERCENTAGE                NUMERIC(18,6)
);


CREATE TABLE XY_GL_AGENCY_FIELD_OFFICE (
    SY_GL_AGENCY_FIELD_OFFICE_NBR  INTEGER NOT NULL,
    SY_GLOBAL_AGENCY_NBR           INTEGER NOT NULL,
    ADDITIONAL_NAME                VARCHAR(40),
    ADDRESS1                       VARCHAR(30) NOT NULL,
    ADDRESS2                       VARCHAR(30),
    CITY                           VARCHAR(20) NOT NULL,
    ZIP_CODE                       VARCHAR(10) NOT NULL,
    STATE                          CHAR(2) NOT NULL,
    ATTENTION_NAME                 VARCHAR(40),
    CONTACT1                       VARCHAR(30),
    PHONE1                         VARCHAR(20),
    DESCRIPTION1                   VARCHAR(10),
    CONTACT2                       VARCHAR(30),
    PHONE2                         VARCHAR(20),
    DESCRIPTION2                   VARCHAR(10),
    FAX                            VARCHAR(20),
    FAX_DESCRIPTION                VARCHAR(10),
    FILLER                         VARCHAR(512),
    CHANGED_BY                     INTEGER NOT NULL,
    CREATION_DATE                  TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                  CHAR(1) NOT NULL,
    CATEGORY_TYPE                  CHAR(1) NOT NULL
);


CREATE TABLE XY_GL_AGENCY_HOLIDAYS (
    SY_GL_AGENCY_HOLIDAYS_NBR  INTEGER NOT NULL,
    SY_GLOBAL_AGENCY_NBR       INTEGER NOT NULL,
    HOLIDAY_NAME               VARCHAR(40) NOT NULL,
    CHANGED_BY                 INTEGER NOT NULL,
    CREATION_DATE              TIMESTAMP NOT NULL,
    EFFECTIVE_DATE             TIMESTAMP NOT NULL,
    ACTIVE_RECORD              CHAR(1) NOT NULL,
    CALC_TYPE                  CHAR(1) NOT NULL,
    WEEK_NUMBER                SMALLINT,
    DAY_OF_WEEK                SMALLINT,
    MONTH_NUMBER               SMALLINT,
    DAY_NUMBER                 SMALLINT,
    SATURDAY_OFFSET            SMALLINT,
    SUNDAY_OFFSET              SMALLINT
);


CREATE TABLE XY_GL_AGENCY_REPORT (
    SY_GL_AGENCY_REPORT_NBR         INTEGER NOT NULL,
    SYSTEM_TAX_TYPE                 CHAR(1) NOT NULL,
    DEPOSIT_FREQUENCY               CHAR(1),
    SY_GL_AGENCY_FIELD_OFFICE_NBR   INTEGER NOT NULL,
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    WHEN_DUE_TYPE                   CHAR(1) NOT NULL,
    NUMBER_OF_DAYS_FOR_WHEN_DUE     INTEGER,
    RETURN_FREQUENCY                CHAR(1) NOT NULL,
    ENLIST_AUTOMATICALLY            CHAR(1) NOT NULL,
    PRINT_WHEN                      CHAR(1) NOT NULL,
    FILLER                          VARCHAR(512),
    TAX_SERVICE_FILTER              CHAR(1) NOT NULL,
    CONSOLIDATED_FILTER             CHAR(1) NOT NULL,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    SY_REPORTS_GROUP_NBR            INTEGER,
    TAX_RETURN_ACTIVE               CHAR(1) NOT NULL,
    BEGIN_EFFECTIVE_MONTH           INTEGER,
    END_EFFECTIVE_MONTH             INTEGER,
    PAYMENT_METHOD                  CHAR(1) NOT NULL,
    TAX945_CHECKS                   CHAR(1) NOT NULL,
    TAX943_COMPANY                  CHAR(1) NOT NULL,
    TAX944_COMPANY                  CHAR(1) NOT NULL,
    TAXRETURN940                    CHAR(1) NOT NULL,
    TAX_RETURN_INACTIVE_START_DATE  TIMESTAMP
);


CREATE TABLE XY_GLOBAL_AGENCY (
    SY_GLOBAL_AGENCY_NBR           INTEGER NOT NULL,
    AGENCY_NAME                    VARCHAR(40) NOT NULL,
    ACCEPTS_CHECK                  CHAR(1) NOT NULL,
    ACCEPTS_DEBIT                  CHAR(1) NOT NULL,
    ACCEPTS_CREDIT                 CHAR(1) NOT NULL,
    NEGATIVE_DIRECT_DEP_ALLOWED    CHAR(1) NOT NULL,
    IGNORE_HOLIDAYS                CHAR(1) NOT NULL,
    IGNORE_WEEKENDS                CHAR(1) NOT NULL,
    PAYMENT_FILE_DEST_DIRECT       VARCHAR(64),
    RECEIVING_ABA_NUMBER           VARCHAR(9),
    RECEIVING_ACCOUNT_NUMBER       VARCHAR(20),
    RECEIVING_ACCOUNT_TYPE         CHAR(1),
    MAG_MEDIA                      CHAR(1) NOT NULL,
    FILLER                         VARCHAR(512),
    CHANGED_BY                     INTEGER NOT NULL,
    CREATION_DATE                  TIMESTAMP NOT NULL,
    STATE                          CHAR(2),
    COUNTY                         VARCHAR(40),
    TAX_PAYMENT_FIELD_OFFICE_NBR   INTEGER,
    NOTES                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    EFFECTIVE_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                  CHAR(1) NOT NULL,
    SEND_ZERO_EFT                  CHAR(1) NOT NULL,
    SEND_ZERO_COUPON               CHAR(1) NOT NULL,
    PRINT_ZERO_RETURN              CHAR(1) NOT NULL,
    CUSTOM_DEBIT_SY_REPORTS_NBR    INTEGER,
    CUSTOM_DEBIT_AFTER_STATUS      CHAR(1) NOT NULL,
    CUSTOM_DEBIT_POST_TO_REGISTER  CHAR(1) NOT NULL,
    CHECK_ONE_PAYMENT_TYPE         CHAR(1) NOT NULL,
    EFTP_ONE_PAYMENT_TYPE          CHAR(1) NOT NULL,
    ACH_ONE_PAYMENT_TYPE           CHAR(1) NOT NULL,
    AGENCY_TYPE                    CHAR(1) NOT NULL
);


CREATE TABLE XY_HR_EEO (
    SY_HR_EEO_NBR   INTEGER NOT NULL,
    DESCRIPTION     VARCHAR(40) NOT NULL,
    CHANGED_BY      INTEGER NOT NULL,
    CREATION_DATE   TIMESTAMP NOT NULL,
    EFFECTIVE_DATE  TIMESTAMP NOT NULL,
    ACTIVE_RECORD   CHAR(1) NOT NULL
);


CREATE TABLE XY_HR_ETHNICITY (
    SY_HR_ETHNICITY_NBR  INTEGER NOT NULL,
    DESCRIPTION          VARCHAR(40) NOT NULL,
    CHANGED_BY           INTEGER NOT NULL,
    CREATION_DATE        TIMESTAMP NOT NULL,
    EFFECTIVE_DATE       TIMESTAMP NOT NULL,
    ACTIVE_RECORD        CHAR(1) NOT NULL
);


CREATE TABLE XY_HR_HANDICAPS (
    SY_HR_HANDICAPS_NBR  INTEGER NOT NULL,
    DESCRIPTION          VARCHAR(40) NOT NULL,
    CHANGED_BY           INTEGER NOT NULL,
    CREATION_DATE        TIMESTAMP NOT NULL,
    EFFECTIVE_DATE       TIMESTAMP NOT NULL,
    ACTIVE_RECORD        CHAR(1) NOT NULL
);


CREATE TABLE XY_HR_INJURY_CODES (
    SY_HR_INJURY_CODES_NBR  INTEGER NOT NULL,
    DESCRIPTION             VARCHAR(40) NOT NULL,
    CHANGED_BY              INTEGER NOT NULL,
    CREATION_DATE           TIMESTAMP NOT NULL,
    INJURY_CODE             VARCHAR(6) NOT NULL,
    EFFECTIVE_DATE          TIMESTAMP NOT NULL,
    ACTIVE_RECORD           CHAR(1) NOT NULL
);


CREATE TABLE XY_HR_OSHA_ANATOMIC_CODES (
    SY_HR_OSHA_ANATOMIC_CODES_NBR  INTEGER NOT NULL,
    DESCRIPTION                    VARCHAR(40) NOT NULL,
    CHANGED_BY                     INTEGER NOT NULL,
    CREATION_DATE                  TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                  CHAR(1) NOT NULL
);


CREATE TABLE XY_HR_REFUSAL_REASON (
    SY_HR_REFUSAL_REASON_NBR  INTEGER NOT NULL,
    EFFECTIVE_DATE            TIMESTAMP NOT NULL,
    CREATION_DATE             TIMESTAMP NOT NULL,
    CHANGED_BY                INTEGER NOT NULL,
    ACTIVE_RECORD             CHAR(1) NOT NULL,
    HEALTHCARE_COVERAGE       CHAR(1) NOT NULL,
    SY_STATES_NBR             INTEGER,
    DESCRIPTION               VARCHAR(40) NOT NULL
);


CREATE TABLE XY_LOCAL_DEPOSIT_FREQ (
    SY_LOCAL_DEPOSIT_FREQ_NBR       INTEGER NOT NULL,
    SY_LOCALS_NBR                   INTEGER NOT NULL,
    WHEN_DUE_TYPE                   CHAR(1) NOT NULL,
    NUMBER_OF_DAYS_FOR_WHEN_DUE     INTEGER NOT NULL,
    THRD_MNTH_DUE_END_OF_NEXT_MNTH  CHAR(1),
    DESCRIPTION                     VARCHAR(20) NOT NULL,
    FILLER                          VARCHAR(512),
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    FREQUENCY_TYPE                  CHAR(1) NOT NULL,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    TAX_COUPON_SY_REPORTS_NBR       INTEGER,
    HIDE_FROM_USER                  CHAR(1) NOT NULL,
    TAX_PAYMENT_TYPE_CODE           VARCHAR(10),
    INC_TAX_PAYMENT_CODE            CHAR(1) NOT NULL,
    CHANGE_FREQ_ON_THRESHOLD        CHAR(1) NOT NULL,
    FIRST_THRESHOLD_DEP_FREQ_NBR    INTEGER,
    FIRST_THRESHOLD_PERIOD          CHAR(1) NOT NULL,
    FIRST_THRESHOLD_AMOUNT          NUMERIC(18,6),
    SECOND_THRESHOLD_DEP_FREQ_NBR   INTEGER,
    SECOND_THRESHOLD_PERIOD         CHAR(1) NOT NULL,
    SECOND_THRESHOLD_AMOUNT         NUMERIC(18,6),
    SY_GL_AGENCY_FIELD_OFFICE_NBR   INTEGER,
    QE_SY_GL_AGENCY_OFFICE_NBR      INTEGER,
    QE_TAX_COUPON_SY_REPORTS_NBR    INTEGER,
    PAY_AND_SHIFTBACK               CHAR(1) NOT NULL,
    SY_REPORTS_GROUP_NBR            INTEGER
);


CREATE TABLE XY_LOCAL_EXEMPTIONS (
    SY_LOCAL_EXEMPTIONS_NBR  INTEGER NOT NULL,
    SY_LOCALS_NBR            INTEGER NOT NULL,
    E_D_CODE_TYPE            VARCHAR(2) NOT NULL,
    EXEMPT                   CHAR(1) NOT NULL,
    CHANGED_BY               INTEGER NOT NULL,
    CREATION_DATE            TIMESTAMP NOT NULL,
    EFFECTIVE_DATE           TIMESTAMP NOT NULL,
    ACTIVE_RECORD            CHAR(1) NOT NULL
);


CREATE TABLE XY_LOCAL_MARITAL_STATUS (
    SY_LOCAL_MARITAL_STATUS_NBR    INTEGER NOT NULL,
    SY_LOCALS_NBR                  INTEGER NOT NULL,
    SY_STATE_MARITAL_STATUS_NBR    INTEGER NOT NULL,
    NEXT_SD_AMOUNT_BEGIN_DATE      TIMESTAMP,
    NEXT_STD_EXEMPTION_ALLOW_DATE  TIMESTAMP,
    FILLER                         VARCHAR(512),
    CHANGED_BY                     INTEGER NOT NULL,
    CREATION_DATE                  TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                  CHAR(1) NOT NULL,
    STANDARD_DEDUCTION_AMOUNT      NUMERIC(18,6),
    NEXT_STAND_DEDUCTION_AMOUNT    NUMERIC(18,6),
    STANDARD_EXEMPTION_ALLOW       NUMERIC(18,6),
    NEXT_STANDARD_EXEMPTION_ALLOW  NUMERIC(18,6)
);


CREATE TABLE XY_LOCAL_TAX_CHART (
    SY_LOCAL_TAX_CHART_NBR          INTEGER NOT NULL,
    SY_LOCALS_NBR                   INTEGER NOT NULL,
    SY_LOCAL_MARITAL_STATUS_NBR     INTEGER NOT NULL,
    NEXT_MINIMUM_EFFECTIVE_DATE     TIMESTAMP,
    NEXT_MAXIMUM_EFFECTIVE_DATE     TIMESTAMP,
    NEXT_PERCENTAGE_EFFECTIVE_DATE  TIMESTAMP,
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    "MINIMUM"                       NUMERIC(18,6),
    "MAXIMUM"                       NUMERIC(18,6),
    PERCENTAGE                      NUMERIC(18,6),
    NEXT_MINIMUM                    NUMERIC(18,6),
    NEXT_MAXIMUM                    NUMERIC(18,6),
    NEXT_PERCENTAGE                 NUMERIC(18,6)
);


CREATE TABLE XY_LOCALS (
    SY_LOCALS_NBR                   INTEGER NOT NULL,
    NAME                            VARCHAR(40) NOT NULL,
    LOCAL_TYPE                      CHAR(1) NOT NULL,
    SY_STATES_NBR                   INTEGER NOT NULL,
    ZIP_CODE                        VARCHAR(10),
    SY_COUNTY_NBR                   INTEGER,
    LOCAL_TAX_IDENTIFIER            VARCHAR(20),
    AGENCY_NUMBER                   INTEGER NOT NULL,
    NEXT_TAX_RATE_EFFECTIVE_DATE    TIMESTAMP,
    NEXT_TAX_AMOUNT_EFFECTIVE_DATE  TIMESTAMP,
    NEXT_TAX_MAXIMUM_BEGIN_DATE     TIMESTAMP,
    NEXT_WAGE_MAXIMUM_BEGIN_DATE    TIMESTAMP,
    NEXT_WEEKLY_TAX_CAP_BEGIN_DATE  TIMESTAMP,
    NEXT_MIN_HOURS_WORKED_DATE      TIMESTAMP,
    MINIMUM_HOURS_WORKED_PER        CHAR(1),
    CALCULATION_METHOD              CHAR(1) NOT NULL,
    NEXT_MISC_AMOUNT_BEGIN_DATE     TIMESTAMP,
    PRINT_RETURN_IF_ZERO            CHAR(1) NOT NULL,
    USE_MISC_TAX_RETURN_CODE        CHAR(1) NOT NULL,
    SY_LOCAL_TAX_PMT_AGENCY_NBR     INTEGER,
    SY_TAX_PMT_REPORT_NBR           INTEGER,
    SY_LOCAL_REPORTING_AGENCY_NBR   INTEGER,
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    TAX_DEPOSIT_FOLLOW_STATE        CHAR(1) NOT NULL,
    FILLER                          VARCHAR(512),
    LOCAL_ACTIVE                    CHAR(1) NOT NULL,
    ROUND_TO_NEAREST_DOLLAR         CHAR(1) NOT NULL,
    TAX_TYPE                        CHAR(1) NOT NULL,
    TAX_FREQUENCY                   CHAR(1) NOT NULL,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    LOCAL_MINIMUM_WAGE              NUMERIC(18,6),
    TAX_RATE                        NUMERIC(18,6),
    NEXT_TAX_RATE                   NUMERIC(18,6),
    TAX_AMOUNT                      NUMERIC(18,6),
    NEXT_TAX_AMOUNT                 NUMERIC(18,6),
    TAX_MAXIMUM                     NUMERIC(18,6),
    NEXT_TAX_MAXIMUM                NUMERIC(18,6),
    WAGE_MAXIMUM                    NUMERIC(18,6),
    NEXT_WAGE_MAXIMUM               NUMERIC(18,6),
    WEEKLY_TAX_CAP                  NUMERIC(18,6),
    NEXT_WEEKLY_TAX_CAP             NUMERIC(18,6),
    MINIMUM_HOURS_WORKED            NUMERIC(18,6),
    NEXT_MINIMUM_HOURS_WORKED       NUMERIC(18,6),
    MISCELLANEOUS_AMOUNT            NUMERIC(18,6),
    NEXT_MISCELLANEOUS_AMOUNT       NUMERIC(18,6),
    W2_BOX                          VARCHAR(10),
    PAY_WITH_STATE                  CHAR(1) NOT NULL,
    WAGE_MINIMUM                    NUMERIC(18,6),
    E_D_CODE_TYPE                   VARCHAR(2),
    QUARTERLY_MINIMUM_THRESHOLD     NUMERIC(18,6),
    AGENCY_CODE                     VARCHAR(10),
    COMBINE_FOR_TAX_PAYMENTS        CHAR(1) NOT NULL
);


CREATE TABLE XY_MEDIA_TYPE (
    SY_MEDIA_TYPE_NBR  INTEGER NOT NULL,
    CLASS_NAME         VARCHAR(40) NOT NULL,
    NAME               VARCHAR(40) NOT NULL,
    EFFECTIVE_DATE     TIMESTAMP NOT NULL,
    CHANGED_BY         INTEGER NOT NULL,
    CREATION_DATE      TIMESTAMP NOT NULL,
    ACTIVE_RECORD      CHAR(1) NOT NULL
);


CREATE TABLE XY_QUEUE_PRIORITY (
    SY_QUEUE_PRIORITY_NBR  INTEGER NOT NULL,
    PACKAGE_ID             INTEGER NOT NULL,
    METHOD_NAME            VARCHAR(255) NOT NULL,
    PRIORITY               INTEGER NOT NULL,
    EFFECTIVE_DATE         TIMESTAMP NOT NULL,
    CHANGED_BY             INTEGER NOT NULL,
    CREATION_DATE          TIMESTAMP NOT NULL,
    ACTIVE_RECORD          CHAR(1) NOT NULL
);


CREATE TABLE XY_RECIPROCATED_STATES (
    SY_RECIPROCATED_STATES_NBR  INTEGER NOT NULL,
    MAIN_STATE_NBR              INTEGER NOT NULL,
    PARTICIPANT_STATE_NBR       INTEGER NOT NULL,
    EFFECTIVE_DATE              TIMESTAMP NOT NULL,
    CREATION_DATE               TIMESTAMP NOT NULL,
    ACTIVE_RECORD               CHAR(1) NOT NULL,
    CHANGED_BY                  INTEGER NOT NULL
);


CREATE TABLE XY_REPORT_GROUP_MEMBERS (
    SY_REPORT_GROUP_MEMBERS_NBR   INTEGER NOT NULL,
    SY_REPORT_GROUPS_NBR          INTEGER NOT NULL,
    SY_REPORT_WRITER_REPORTS_NBR  INTEGER NOT NULL,
    EFFECTIVE_DATE                TIMESTAMP NOT NULL,
    CREATION_DATE                 TIMESTAMP NOT NULL,
    ACTIVE_RECORD                 CHAR(1) NOT NULL,
    CHANGED_BY                    INTEGER NOT NULL
);


CREATE TABLE XY_REPORT_GROUPS (
    SY_REPORT_GROUPS_NBR  INTEGER NOT NULL,
    NAME                  VARCHAR(40) NOT NULL,
    EFFECTIVE_DATE        TIMESTAMP NOT NULL,
    CREATION_DATE         TIMESTAMP NOT NULL,
    ACTIVE_RECORD         CHAR(1) NOT NULL,
    CHANGED_BY            INTEGER NOT NULL
);


CREATE TABLE XY_REPORT_WRITER_REPORTS (
    SY_REPORT_WRITER_REPORTS_NBR  INTEGER NOT NULL,
    REPORT_DESCRIPTION            VARCHAR(40) NOT NULL,
    REPORT_TYPE                   CHAR(1) NOT NULL,
    REPORT_FILE                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    NOTES                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    CHANGED_BY                    INTEGER NOT NULL,
    CREATION_DATE                 TIMESTAMP NOT NULL,
    EFFECTIVE_DATE                TIMESTAMP NOT NULL,
    ACTIVE_RECORD                 CHAR(1) NOT NULL,
    REPORT_STATUS                 CHAR(1) NOT NULL,
    MEDIA_TYPE                    CHAR(1) NOT NULL,
    CLASS_NAME                    VARCHAR(40),
    ANCESTOR_CLASS_NAME           VARCHAR(40)
);


CREATE TABLE XY_REPORTS (
    SY_REPORTS_NBR                INTEGER NOT NULL,
    CHANGED_BY                    INTEGER NOT NULL,
    CREATION_DATE                 TIMESTAMP NOT NULL,
    DESCRIPTION                   VARCHAR(40) NOT NULL,
    ACTIVE_REPORT                 CHAR(1) NOT NULL,
    SY_REPORT_WRITER_REPORTS_NBR  INTEGER,
    FILLER                        VARCHAR(512),
    EFFECTIVE_DATE                TIMESTAMP NOT NULL,
    ACTIVE_RECORD                 CHAR(1) NOT NULL,
    ACTIVE_YEAR_FROM              TIMESTAMP,
    ACTIVE_YEAR_TO                TIMESTAMP,
    SY_REPORTS_GROUP_NBR          INTEGER
);


CREATE TABLE XY_REPORTS_GROUP (
    SY_REPORTS_GROUP_NBR     INTEGER NOT NULL,
    SY_REPORTS_NBR           INTEGER,
    SY_GL_AGENCY_REPORT_NBR  INTEGER,
    CHANGED_BY               INTEGER NOT NULL,
    CREATION_DATE            TIMESTAMP NOT NULL,
    FILLER                   VARCHAR(512),
    AGENCY_COPY              CHAR(1) NOT NULL,
    SB_COPY                  CHAR(1) NOT NULL,
    CL_COPY                  CHAR(1) NOT NULL,
    MEDIA_TYPE               CHAR(1) NOT NULL,
    EFFECTIVE_DATE           TIMESTAMP NOT NULL,
    ACTIVE_RECORD            CHAR(1) NOT NULL,
    NAME                     VARCHAR(60)
);


CREATE TABLE XY_SEC_TEMPLATES (
    SY_SEC_TEMPLATES_NBR  INTEGER NOT NULL,
    NAME                  VARCHAR(40) NOT NULL,
    SY_PCL_FONT_NBR       BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    CHANGED_BY            INTEGER NOT NULL,
    CREATION_DATE         TIMESTAMP NOT NULL,
    EFFECTIVE_DATE        TIMESTAMP NOT NULL,
    ACTIVE_RECORD         CHAR(1) NOT NULL
);


CREATE TABLE XY_STATE_DEPOSIT_FREQ (
    SY_STATE_DEPOSIT_FREQ_NBR       INTEGER NOT NULL,
    SY_STATES_NBR                   INTEGER NOT NULL,
    WHEN_DUE_TYPE                   CHAR(1) NOT NULL,
    NUMBER_OF_DAYS_FOR_WHEN_DUE     INTEGER NOT NULL,
    THRESHOLD_PERIOD                CHAR(1) NOT NULL,
    CHANGE_STATUS_ON_THRESHOLD      CHAR(1) NOT NULL,
    THRD_MNTH_DUE_END_OF_NEXT_MNTH  CHAR(1),
    DESCRIPTION                     VARCHAR(20) NOT NULL,
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    THRESHOLD_DEP_FREQUENCY_NUMBER  INTEGER,
    FILLER                          VARCHAR(512),
    SECOND_THRESHOLD_PERIOD         CHAR(1) NOT NULL,
    TAX_PAYMENT_TYPE_CODE           VARCHAR(10),
    INC_TAX_PAYMENT_CODE            CHAR(1),
    SY_GL_AGENCY_FIELD_OFFICE_NBR   INTEGER,
    FREQUENCY_TYPE                  CHAR(1) NOT NULL,
    TAX_COUPON_SY_REPORTS_NBR       INTEGER,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    THRESHOLD_AMOUNT                NUMERIC(18,6),
    SECOND_THRESHOLD_AMOUNT         NUMERIC(18,6),
    QE_TAX_COUPON_SY_REPORTS_NBR    INTEGER,
    QE_SY_GL_AGENCY_OFFICE_NBR      INTEGER,
    HIDE_FROM_USER                  CHAR(1) NOT NULL,
    SECOND_THRESHOLD_DEP_FREQ_NBR   INTEGER,
    PAY_AND_SHIFTBACK               CHAR(1) NOT NULL,
    SY_REPORTS_GROUP_NBR            INTEGER
);


CREATE TABLE XY_STATE_EXEMPTIONS (
    SY_STATE_EXEMPTIONS_NBR  INTEGER NOT NULL,
    SY_STATES_NBR            INTEGER NOT NULL,
    E_D_CODE_TYPE            VARCHAR(2) NOT NULL,
    EXEMPT_STATE             CHAR(1) NOT NULL,
    EXEMPT_EMPLOYEE_SDI      CHAR(1) NOT NULL,
    EXEMPT_EMPLOYER_SDI      CHAR(1) NOT NULL,
    EXEMPT_EMPLOYEE_SUI      CHAR(1) NOT NULL,
    EXEMPT_EMPLOYER_SUI      CHAR(1) NOT NULL,
    CHANGED_BY               INTEGER NOT NULL,
    CREATION_DATE            TIMESTAMP NOT NULL,
    EFFECTIVE_DATE           TIMESTAMP NOT NULL,
    ACTIVE_RECORD            CHAR(1) NOT NULL
);


CREATE TABLE XY_STATE_MARITAL_STATUS (
    SY_STATE_MARITAL_STATUS_NBR     INTEGER NOT NULL,
    SY_STATES_NBR                   INTEGER NOT NULL,
    STATUS_TYPE                     CHAR(2) NOT NULL,
    STATUS_DESCRIPTION              VARCHAR(40) NOT NULL,
    PERSONAL_EXEMPTIONS             INTEGER,
    NEXT_SD_PERCENT_OF_GROSS_DATE   TIMESTAMP,
    NEXT_SD_MINIMUM_AMOUNT_DATE     TIMESTAMP,
    NEXT_SD_MAXIMUM_AMOUNT_DATE     TIMESTAMP,
    NEXT_SD_FLAT_AMOUNT_BEGIN_DATE  TIMESTAMP,
    NEXT_STANDARD_PER_E_A_DATE      TIMESTAMP,
    DEDUCT_FEDERAL                  CHAR(1) NOT NULL,
    NEXT_DEDUCT_FED_MAX_AMT_DATE    TIMESTAMP,
    NEXT_PER_DEPENDENT_ALLOW_DATE   TIMESTAMP,
    NEXT_PERSNL_TAX_CR_AMT_DATE     TIMESTAMP,
    NEXT_TAX_CR_PER_DEPEND_DATE     TIMESTAMP,
    NEXT_TAX_CREDIT_PER_ALLOW_DATE  TIMESTAMP,
    NEXT_HIGH_INCOME_PER_E_A_DATE   TIMESTAMP,
    NEXT_HIGH_INCOME_AMOUNT_DATE    TIMESTAMP,
    NEXT_MINIMUM_TAX_INCOME_DATE    TIMESTAMP,
    NEXT_ADD_E_A_EFFECTIVE_DATE     TIMESTAMP,
    NEXT_ADD_DED_ALLOW_START_DATE   TIMESTAMP,
    DEDUCT_FICA                     CHAR(1) NOT NULL,
    NEXT_DEDUCT_FICA_MAX_AMT_DATE   TIMESTAMP,
    NEXT_BLIND_EXMPT_AMT_BEG_DATE   TIMESTAMP,
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    FILLER                          VARCHAR(512),
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    STATE_PERCENT_OF_FEDERAL        NUMERIC(18,6),
    STANDARD_DEDUCTION_PCNT_GROSS   NUMERIC(18,6),
    NEXT_STAND_DEDUCT_PCNT_GROSS    NUMERIC(18,6),
    STANDARD_DEDUCTION_MIN_AMOUNT   NUMERIC(18,6),
    NEXT_STAND_DEDUCT_MIN_AMOUNT    NUMERIC(18,6),
    STANDARD_DEDUCTION_MAX_AMOUNT   NUMERIC(18,6),
    NEXT_STAND_DEDUCT_MAX_AMOUNT    NUMERIC(18,6),
    STANDARD_DEDUCTION_FLAT_AMOUNT  NUMERIC(18,6),
    NEXT_STAND_DEDUCT_FLAT_AMOUNT   NUMERIC(18,6),
    STANDARD_PER_EXEMPTION_ALLOW    NUMERIC(18,6),
    NEXT_STANDARD_PER_EXEMPT_ALLOW  NUMERIC(18,6),
    DEDUCT_FEDERAL_MAXIMUM_AMOUNT   NUMERIC(18,6),
    NEXT_DEDUCT_FED_MAX_AMOUNT      NUMERIC(18,6),
    PER_DEPENDENT_ALLOWANCE         NUMERIC(18,6),
    NEXT_PER_DEPENDENT_ALLOWANCE    NUMERIC(18,6),
    PERSONAL_TAX_CREDIT_AMOUNT      NUMERIC(18,6),
    NEXT_PERSONAL_TAX_CREDIT_AMT    NUMERIC(18,6),
    TAX_CREDIT_PER_DEPENDENT        NUMERIC(18,6),
    NEXT_TAX_CREDIT_PER_DEPENDENT   NUMERIC(18,6),
    TAX_CREDIT_PER_ALLOWANCE        NUMERIC(18,6),
    NEXT_TAX_CREDIT_PER_ALLOWANCE   NUMERIC(18,6),
    HIGH_INCOME_PER_EXEMPT_ALLOW    NUMERIC(18,6),
    NEXT_HIGH_INCOME_PER_E_A        NUMERIC(18,6),
    DEFINED_HIGH_INCOME_AMOUNT      NUMERIC(18,6),
    NEXT_HIGH_INCOME_AMOUNT         NUMERIC(18,6),
    MINIMUM_TAXABLE_INCOME          NUMERIC(18,6),
    NEXT_MINIMUM_TAXABLE_INCOME     NUMERIC(18,6),
    ADDITIONAL_EXEMPT_ALLOWANCE     NUMERIC(18,6),
    NEXT_ADD_EXEMPT_ALLOWANCE       NUMERIC(18,6),
    ADDITIONAL_DEDUCTION_ALLOWANCE  NUMERIC(18,6),
    NEXT_ADD_DEDUCTION_ALLOWANCE    NUMERIC(18,6),
    DEDUCT_FICA__MAXIMUM_AMOUNT     NUMERIC(18,6),
    NEXT_DEDUCT_FICA_MAX_AMOUNT     NUMERIC(18,6),
    BLIND_EXEMPTION_AMOUNT          NUMERIC(18,6),
    NEXT_BLIND_EXEMPTION_AMOUNT     NUMERIC(18,6)
);


CREATE TABLE XY_STATE_TAX_CHART (
    SY_STATE_TAX_CHART_NBR          INTEGER NOT NULL,
    SY_STATES_NBR                   INTEGER NOT NULL,
    SY_STATE_MARITAL_STATUS_NBR     INTEGER NOT NULL,
    NEXT_MINIMUM_EFFECTIVE_DATE     TIMESTAMP,
    NEXT_MAXIMUM_EFFECTIVE_DATE     TIMESTAMP,
    NEXT_PERCENTAGE_EFFECTIVE_DATE  TIMESTAMP,
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    ENTRY_TYPE                      CHAR(1) NOT NULL,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    "MINIMUM"                       NUMERIC(18,6),
    "MAXIMUM"                       NUMERIC(18,6),
    PERCENTAGE                      NUMERIC(18,6),
    NEXT_MINIMUM                    NUMERIC(18,6),
    NEXT_MAXIMUM                    NUMERIC(18,6),
    NEXT_PERCENTAGE                 NUMERIC(18,6)
);


CREATE TABLE XY_STATES (
    SY_STATES_NBR                   INTEGER NOT NULL,
    STATE                           CHAR(2),
    NAME                            VARCHAR(20) NOT NULL,
    NEXT_STATE_MIN_WAGE_BEGIN_DATE  TIMESTAMP,
    STATE_WITHHOLDING               CHAR(1) NOT NULL,
    NEXT_SUP_TAX_PERCENT_DATE       TIMESTAMP,
    STATE_EFT_TYPE                  CHAR(1) NOT NULL,
    EFT_NOTES                       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    PRINT_STATE_RETURN_IF_ZERO      CHAR(1) NOT NULL,
    SUI_EFT_TYPE                    CHAR(1) NOT NULL,
    SUI_EFT_NOTES                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    SDI_RECIPROCATE                 CHAR(1) NOT NULL,
    SUI_RECIPROCATE                 CHAR(1) NOT NULL,
    NEXT_SDI_TAX_CAP_DATE           TIMESTAMP,
    NEXT_EE_SDI_MAX_WAGE_DATE       TIMESTAMP,
    NEXT_ER_SDI_MAX_WAGE_DATE       TIMESTAMP,
    NEXT_EE_SDI_RATE_BEGIN_DATE     TIMESTAMP,
    NEXT_ER_SDI_RATE_BEGIN_DATE     TIMESTAMP,
    SHOW_S125_IN_GROSS_WAGES_SUI    CHAR(1) NOT NULL,
    PRINT_SUI_RETURN_IF_ZERO        CHAR(1) NOT NULL,
    NEXT_MAX_GARNISH_PERCENT_DATE   TIMESTAMP,
    NEXT_GARNISH_MIN_WAGE_MULT_DT   TIMESTAMP,
    PAYROLL                         VARCHAR(8),
    SALES                           VARCHAR(8),
    CORPORATE                       VARCHAR(8),
    RAILROAD                        VARCHAR(8),
    UNEMPLOYMENT                    VARCHAR(8),
    OTHER                           VARCHAR(8),
    UIFSA_NEW_HIRE_REPORTING        CHAR(1) NOT NULL,
    DD_CHILD_SUPPORT                CHAR(1) NOT NULL,
    USE_STATE_MISC_TAX_RETURN_CODE  CHAR(1) NOT NULL,
    NEXT_SALES_TAX_PERCENT_DATE     TIMESTAMP,
    SY_STATE_TAX_PMT_AGENCY_NBR     INTEGER,
    SY_TAX_PMT_REPORT_NBR           INTEGER,
    SY_STATE_REPORTING_AGENCY_NBR   INTEGER,
    PAY_SDI_WITH                    CHAR(1) NOT NULL,
    FILLER                          VARCHAR(512),
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    TAX_DEPOSIT_FOLLOW_FEDERAL      CHAR(1) NOT NULL,
    ROUND_TO_NEAREST_DOLLAR         CHAR(1) NOT NULL,
    SUI_TAX_PAYMENT_TYPE_CODE       VARCHAR(10),
    INC_SUI_TAX_PAYMENT_CODE        CHAR(1),
    FIPS_CODE                       CHAR(2),
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    STATE_MINIMUM_WAGE              NUMERIC(18,6),
    NEXT_STATE_MINIMUM_WAGE         NUMERIC(18,6),
    STATE_TIP_CREDIT                NUMERIC(18,6),
    SUPPLEMENTAL_WAGES_PERCENTAGE   NUMERIC(18,6),
    NEXT_SUPPLEMENTAL_TAX_PERCENT   NUMERIC(18,6),
    WEEKLY_SDI_TAX_CAP              NUMERIC(18,6),
    NEXT_WEEKLY_SDI_TAX_CAP         NUMERIC(18,6),
    EE_SDI_MAXIMUM_WAGE             NUMERIC(18,6),
    NEXT_EE_SDI_MAX_WAGE            NUMERIC(18,6),
    ER_SDI_MAXIMUM_WAGE             NUMERIC(18,6),
    NEXT_ER_SDI_MAX_WAGE            NUMERIC(18,6),
    EE_SDI_RATE                     NUMERIC(18,6),
    NEXT_EE_SDI_RATE                NUMERIC(18,6),
    ER_SDI_RATE                     NUMERIC(18,6),
    NEXT_ER_SDI_RATE                NUMERIC(18,6),
    MAXIMUM_GARNISHMENT_PERCENT     NUMERIC(18,6),
    NEXT_MAX_GARNISH_PERCENT        NUMERIC(18,6),
    GARNISH_MIN_WAGE_MULTIPLIER     NUMERIC(18,6),
    NEXT_GARNISH_MIN_WAGE_MULT      NUMERIC(18,6),
    SALES_TAX_PERCENTAGE            NUMERIC(18,6),
    NEXT_SALES_TAX_PERCENTAGE       NUMERIC(18,6),
    W2_BOX                          VARCHAR(10),
    RECIPROCITY_TYPE                CHAR(1) NOT NULL,
    PRINT_W2                        CHAR(1) NOT NULL,
    CAP_STATE_TAX_CREDIT            CHAR(1) NOT NULL,
    INACTIVATE_MARITAL_STATUS       CHAR(1) NOT NULL,
    STATE_OT_TIP_CREDIT             NUMERIC(18,6)
);


CREATE TABLE XY_STORAGE (
    SY_STORAGE_NBR  INTEGER NOT NULL,
    TAG             VARCHAR(20) NOT NULL,
    STORAGE_DATA    BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    CHANGED_BY      INTEGER NOT NULL,
    CREATION_DATE   TIMESTAMP NOT NULL,
    EFFECTIVE_DATE  TIMESTAMP NOT NULL,
    ACTIVE_RECORD   CHAR(1) NOT NULL
);


CREATE TABLE XY_SUI (
    SY_SUI_NBR                      INTEGER NOT NULL,
    SY_STATES_NBR                   INTEGER NOT NULL,
    SUI_TAX_NAME                    VARCHAR(40) NOT NULL,
    EE_ER_OR_EE_OTHER_OR_ER_OTHER   CHAR(1) NOT NULL,
    FUTURE_DEFAULT_RATE_BEGIN_DATE  TIMESTAMP,
    FUTURE_MAX_WAGE_BEGIN_DATE      TIMESTAMP,
    USE_MISC_TAX_RETURN_CODE        CHAR(1),
    SY_SUI_TAX_PMT_AGENCY_NBR       INTEGER,
    SY_TAX_PMT_REPORT_NBR           INTEGER,
    SY_SUI_REPORTING_AGENCY_NBR     INTEGER,
    FILLER                          VARCHAR(512),
    CHANGED_BY                      INTEGER NOT NULL,
    CREATION_DATE                   TIMESTAMP NOT NULL,
    ROUND_TO_NEAREST_DOLLAR         CHAR(1) NOT NULL,
    EFFECTIVE_DATE                  TIMESTAMP NOT NULL,
    ACTIVE_RECORD                   CHAR(1) NOT NULL,
    SUI_ACTIVE                      CHAR(1) NOT NULL,
    NEW_COMPANY_DEFAULT_RATE        NUMERIC(18,6),
    FUTURE_DEFAULT_RATE             NUMERIC(18,6),
    MAXIMUM_WAGE                    NUMERIC(18,6),
    FUTURE_MAXIMUM_WAGE             NUMERIC(18,6),
    TAX_COUPON_SY_REPORTS_NBR       INTEGER,
    FREQUENCY_TYPE                  CHAR(1) NOT NULL,
    W2_BOX                          VARCHAR(10),
    PAY_WITH_STATE                  CHAR(1) NOT NULL,
    GLOBAL_RATE                     NUMERIC(18,6),
    E_D_CODE_TYPE                   VARCHAR(2),
    FTE_EXEMPTION                   INTEGER,
    FTE_WEEKLY_HOURS                INTEGER,
    FTE_MULTIPLIER                  INTEGER,
    SY_REPORTS_GROUP_NBR            INTEGER,
    ALTERNATE_TAXABLE_WAGE_BASE     NUMERIC(18,6),
    CO_ADD_ALL_BUTTON		    CHAR(1) NOT NULL			
);

CREATE TABLE xy_reports_versions (
    sy_reports_versions_nbr       INTEGER NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    changed_by                    INTEGER NOT NULL,
    db_version                    VARCHAR(20),
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    status_by                     INTEGER NOT NULL,
    current_report                CHAR(1) NOT NULL,
    modified_on                   TIMESTAMP NOT NULL,
    ticket                        VARCHAR(20),
    sy_report_writer_reports_nbr  INTEGER NOT NULL,
    report_data                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    class_name                    VARCHAR(40),
    ancestor_class_name           VARCHAR(40),
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80
);

COMMIT;


/* MOVE DATA INTO TEMP TABLES */

INSERT into XY_AGENCY_DEPOSIT_FREQ SELECT * from SY_AGENCY_DEPOSIT_FREQ;
COMMIT;
DROP TABLE SY_AGENCY_DEPOSIT_FREQ;
COMMIT;

INSERT into XY_COUNTY SELECT * from SY_COUNTY;
COMMIT;
DROP TABLE SY_COUNTY;
COMMIT;

INSERT into XY_DELIVERY_METHOD SELECT * from SY_DELIVERY_METHOD;
COMMIT;
DROP TABLE SY_DELIVERY_METHOD;
COMMIT;

INSERT into XY_DELIVERY_SERVICE SELECT * from SY_DELIVERY_SERVICE;
COMMIT;
DROP TABLE SY_DELIVERY_SERVICE;
COMMIT;

INSERT into XY_FED_EXEMPTIONS SELECT * from SY_FED_EXEMPTIONS;
COMMIT;
DROP TABLE SY_FED_EXEMPTIONS;
COMMIT;

INSERT into XY_FED_REPORTING_AGENCY SELECT * from SY_FED_REPORTING_AGENCY;
COMMIT;
DROP TABLE SY_FED_REPORTING_AGENCY;
COMMIT;

INSERT into XY_FED_TAX_PAYMENT_AGENCY SELECT * from SY_FED_TAX_PAYMENT_AGENCY;
COMMIT;
DROP TABLE SY_FED_TAX_PAYMENT_AGENCY;
COMMIT;

INSERT into XY_FED_TAX_TABLE SELECT * from SY_FED_TAX_TABLE;
COMMIT;
DROP TABLE SY_FED_TAX_TABLE;
COMMIT;

INSERT into XY_FED_TAX_TABLE_BRACKETS SELECT * from SY_FED_TAX_TABLE_BRACKETS;
COMMIT;
DROP TABLE SY_FED_TAX_TABLE_BRACKETS;
COMMIT;

INSERT into XY_GL_AGENCY_FIELD_OFFICE SELECT * from SY_GL_AGENCY_FIELD_OFFICE;
COMMIT;
DROP TABLE SY_GL_AGENCY_FIELD_OFFICE;
COMMIT;

INSERT into XY_GL_AGENCY_HOLIDAYS SELECT * from SY_GL_AGENCY_HOLIDAYS;
COMMIT;
DROP TABLE SY_GL_AGENCY_HOLIDAYS;
COMMIT;

INSERT into XY_GL_AGENCY_REPORT SELECT * from SY_GL_AGENCY_REPORT;
COMMIT;
DROP TABLE SY_GL_AGENCY_REPORT;
COMMIT;

INSERT into XY_GLOBAL_AGENCY SELECT * from SY_GLOBAL_AGENCY;
COMMIT;
DROP TABLE SY_GLOBAL_AGENCY;
COMMIT;

INSERT into XY_HR_EEO SELECT * from SY_HR_EEO;
COMMIT;
DROP TABLE SY_HR_EEO;
COMMIT;

INSERT into XY_HR_ETHNICITY SELECT * from SY_HR_ETHNICITY;
COMMIT;
DROP TABLE SY_HR_ETHNICITY;
COMMIT;

INSERT into XY_HR_HANDICAPS SELECT * from SY_HR_HANDICAPS;
COMMIT;
DROP TABLE SY_HR_HANDICAPS;
COMMIT;

INSERT into XY_HR_INJURY_CODES SELECT * from SY_HR_INJURY_CODES;
COMMIT;
DROP TABLE SY_HR_INJURY_CODES;
COMMIT;

INSERT into XY_HR_OSHA_ANATOMIC_CODES SELECT * from SY_HR_OSHA_ANATOMIC_CODES;
COMMIT;
DROP TABLE SY_HR_OSHA_ANATOMIC_CODES;
COMMIT;

INSERT into XY_HR_REFUSAL_REASON SELECT * from SY_HR_REFUSAL_REASON;
COMMIT;
DROP TABLE SY_HR_REFUSAL_REASON;
COMMIT;

INSERT into XY_LOCAL_DEPOSIT_FREQ SELECT * from SY_LOCAL_DEPOSIT_FREQ;
COMMIT;
DROP TABLE SY_LOCAL_DEPOSIT_FREQ;
COMMIT;

INSERT into XY_LOCAL_EXEMPTIONS SELECT * from SY_LOCAL_EXEMPTIONS;
COMMIT;
DROP TABLE SY_LOCAL_EXEMPTIONS;
COMMIT;

INSERT into XY_LOCAL_MARITAL_STATUS SELECT * from SY_LOCAL_MARITAL_STATUS;
COMMIT;
DROP TABLE SY_LOCAL_MARITAL_STATUS;
COMMIT;

INSERT into XY_LOCAL_TAX_CHART SELECT * from SY_LOCAL_TAX_CHART;
COMMIT;
DROP TABLE SY_LOCAL_TAX_CHART;
COMMIT;

INSERT into XY_LOCALS SELECT * from SY_LOCALS;
COMMIT;
DROP TABLE SY_LOCALS;
COMMIT;

INSERT into XY_MEDIA_TYPE SELECT * from SY_MEDIA_TYPE;
COMMIT;
DROP TABLE SY_MEDIA_TYPE;
COMMIT;

INSERT into XY_QUEUE_PRIORITY SELECT * from SY_QUEUE_PRIORITY;
COMMIT;
DROP TABLE SY_QUEUE_PRIORITY;
COMMIT;

INSERT into XY_RECIPROCATED_STATES SELECT * from SY_RECIPROCATED_STATES;
COMMIT;
DROP TABLE SY_RECIPROCATED_STATES;
COMMIT;

INSERT into XY_REPORT_GROUP_MEMBERS SELECT * from SY_REPORT_GROUP_MEMBERS;
COMMIT;
DROP TABLE SY_REPORT_GROUP_MEMBERS;
COMMIT;

INSERT into XY_REPORT_GROUPS SELECT * from SY_REPORT_GROUPS;
COMMIT;
DROP TABLE SY_REPORT_GROUPS;
COMMIT;

INSERT into XY_REPORT_WRITER_REPORTS SELECT * from SY_REPORT_WRITER_REPORTS;
COMMIT;
DROP TABLE SY_REPORT_WRITER_REPORTS;
COMMIT;

INSERT into XY_REPORTS SELECT * from SY_REPORTS;
COMMIT;
DROP TABLE SY_REPORTS;
COMMIT;

INSERT into XY_REPORTS_GROUP SELECT * from SY_REPORTS_GROUP;
COMMIT;
DROP TABLE SY_REPORTS_GROUP;
COMMIT;

INSERT into XY_SEC_TEMPLATES SELECT * from SY_SEC_TEMPLATES;
COMMIT;
DROP TABLE SY_SEC_TEMPLATES;
COMMIT;

INSERT into XY_STATE_DEPOSIT_FREQ SELECT * from SY_STATE_DEPOSIT_FREQ;
COMMIT;
DROP TABLE SY_STATE_DEPOSIT_FREQ;
COMMIT;

INSERT into XY_STATE_EXEMPTIONS SELECT * from SY_STATE_EXEMPTIONS;
COMMIT;
DROP TABLE SY_STATE_EXEMPTIONS;
COMMIT;

INSERT into XY_STATE_MARITAL_STATUS SELECT * from SY_STATE_MARITAL_STATUS;
COMMIT;
DROP TABLE SY_STATE_MARITAL_STATUS;
COMMIT;

INSERT into XY_STATE_TAX_CHART SELECT * from SY_STATE_TAX_CHART;
COMMIT;
DROP TABLE SY_STATE_TAX_CHART;
COMMIT;

INSERT into XY_STATES SELECT * from SY_STATES;
COMMIT;
DROP TABLE SY_STATES;
COMMIT;

INSERT into XY_STORAGE SELECT * from SY_STORAGE;
COMMIT;
DROP TABLE SY_STORAGE;
COMMIT;

INSERT into XY_REPORTS_VERSIONS SELECT * from SY_REPORTS_VERSIONS;
COMMIT;
DROP TABLE SY_REPORTS_VERSIONS;
COMMIT;                       

insert into XY_SUI (SY_SUI_NBR, SY_STATES_NBR, SUI_TAX_NAME, EE_ER_OR_EE_OTHER_OR_ER_OTHER,
                    FUTURE_DEFAULT_RATE_BEGIN_DATE, FUTURE_MAX_WAGE_BEGIN_DATE, USE_MISC_TAX_RETURN_CODE,
                    SY_SUI_TAX_PMT_AGENCY_NBR, SY_TAX_PMT_REPORT_NBR, SY_SUI_REPORTING_AGENCY_NBR, FILLER, CHANGED_BY,
                    CREATION_DATE, ROUND_TO_NEAREST_DOLLAR, EFFECTIVE_DATE, ACTIVE_RECORD, SUI_ACTIVE,
                    NEW_COMPANY_DEFAULT_RATE, FUTURE_DEFAULT_RATE, MAXIMUM_WAGE, FUTURE_MAXIMUM_WAGE,
                    TAX_COUPON_SY_REPORTS_NBR, FREQUENCY_TYPE, W2_BOX, PAY_WITH_STATE, GLOBAL_RATE, E_D_CODE_TYPE,
                    FTE_EXEMPTION, FTE_WEEKLY_HOURS, FTE_MULTIPLIER, SY_REPORTS_GROUP_NBR, ALTERNATE_TAXABLE_WAGE_BASE, CO_ADD_ALL_BUTTON)
select SY_SUI_NBR, SY_STATES_NBR, SUI_TAX_NAME, EE_ER_OR_EE_OTHER_OR_ER_OTHER, FUTURE_DEFAULT_RATE_BEGIN_DATE,
       FUTURE_MAX_WAGE_BEGIN_DATE, USE_MISC_TAX_RETURN_CODE, SY_SUI_TAX_PMT_AGENCY_NBR, SY_TAX_PMT_REPORT_NBR,
       SY_SUI_REPORTING_AGENCY_NBR, FILLER, CHANGED_BY, CREATION_DATE, ROUND_TO_NEAREST_DOLLAR, EFFECTIVE_DATE,
       ACTIVE_RECORD, SUI_ACTIVE, NEW_COMPANY_DEFAULT_RATE, FUTURE_DEFAULT_RATE, MAXIMUM_WAGE, FUTURE_MAXIMUM_WAGE,
       TAX_COUPON_SY_REPORTS_NBR, FREQUENCY_TYPE, W2_BOX, PAY_WITH_STATE, GLOBAL_RATE, E_D_CODE_TYPE, FTE_EXEMPTION,
       FTE_WEEKLY_HOURS, FTE_MULTIPLIER, SY_REPORTS_GROUP_NBR, NULL, 'Y'
from SY_SUI;
COMMIT;
DROP TABLE SY_SUI;
COMMIT;

SET TERM ^;
EXECUTE BLOCK
AS
DECLARE VARIABLE sy_sui_nbr INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE btn CHAR(1);
DECLARE VARIABLE amt VARCHAR(10);
DECLARE VARIABLE amt_num NUMERIC(18,6);
DECLARE VARIABLE effective_date TIMESTAMP;
DECLARE VARIABLE creation_date TIMESTAMP;
BEGIN
  FOR SELECT sy_sui_nbr, effective_date, creation_date, filler FROM xy_sui WHERE filler IS NOT NULL AND filler <> ''
      INTO sy_sui_nbr, effective_date, creation_date, filler
  DO
  BEGIN
    btn = TRIM(strcopy(filler, 0, 1));
    amt = TRIM(strcopy(filler, 9, 20));
    IF (amt <> '') THEN
      amt_num = CAST(:amt AS NUMERIC(18,6));
    ELSE
      amt_num = NULL;

      UPDATE xy_sui SET co_add_all_button = :btn, alternate_taxable_wage_base = :amt_num
      WHERE sy_sui_nbr = :sy_sui_nbr AND effective_date = :effective_date AND creation_date = :creation_date;
  END

  UPDATE xy_sui SET filler = NULL;
END^

COMMIT^

SET TERM ;^
