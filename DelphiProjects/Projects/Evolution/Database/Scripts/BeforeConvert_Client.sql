/* CUT OFF AUDIT AND DELETED DATA BEFORE 2003 */

DELETE FROM cl WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_billing WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_common_paymaster WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_pieces WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_3_party_sick_pay_admin WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_e_d_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_skills WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_bank_account WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_co_consolidation WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_timeclock_imports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_delivery_method WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_person WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_states WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_delivery_group WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_agency WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_pension WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_union WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_e_ds WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_check_lines WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_funds WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_person_dependents WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_branch WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_department WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_team WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pr_batch_deflt_ed WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_states WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_bank_account_register WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_billing_history WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_delivery_package WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_division WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_e_d_codes WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_applicant WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_attendance_types WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_car WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_performance_ratings WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_positions WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_property WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_recruiters WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_referrals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_salary_grades WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_supervisors WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_jobs WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_local_tax WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_manual_ach WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pay_group WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pr_ach WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pr_check_templates WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pr_filters WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_services WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_shifts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_sui WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_tax_deposits WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_time_off_accrual WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_unions WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_workers_comp WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_salesperson WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_tax_payment_ach WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_child_support_cases WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_direct_deposit WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_scheduled_e_ds WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_check WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_batch WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_miscellaneous_checks WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_phone WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_pension_fund_splits WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_general_ledger WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_e_d_group_codes WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_reprint_history_detail WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_e_d_local_exmpt_excld WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_e_d_state_exmpt_excld WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_rates WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_piece_work WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_work_shifts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_autolabor_distribution WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_time_off_accrual WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_time_off_accrual_rates WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_scheduled_event WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_hr_property_tracking WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_person_education WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_applicant_interview WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_person_skills WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_hr_attendance WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_hr_injury_occurrence WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_hr_co_provided_educatn WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_person_handicaps WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_union_dues WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_check_line_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_check_sui WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_scheduled_e_ds WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_check_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_check_states WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_services WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_billing_history_detail WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_div_pr_batch_deflt_ed WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_batch_local_or_temps WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_batch_states_or_temps WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_fed_tax_liabilities WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_state_tax_liabilities WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_sui_liabilities WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_local_tax_liabilities WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_reprint_history WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pensions WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_brch_pr_batch_deflt_ed WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_dept_pr_batch_deflt_ed WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_team_pr_batch_deflt_ed WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_salesperson_flat_amt WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_job_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_additional_info_names WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_tax_return_queue WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_additional_info WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_jobs_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_scheduled_event_batch WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_division_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_branch_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_department_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_team_locals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_report_writer_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_emergency_contacts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_benefits WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_hr_car WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_school WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_pr_check_template_e_ds WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM pr_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_time_off_accrual_oper WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_tax_return_runs WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_mail_box_group_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_mail_box_group WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_hr_performance_ratings WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_reason_codes WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_person_documents WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_time_off_accrual_tiers WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_calendar_defaults WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_additional_info_values WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_benefit_payment WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_group WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_storage WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM cl_hr_course WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_group_manager WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_group_member WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_change_request WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_auto_enlist_returns WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_enlist_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_bank_acc_reg_details WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_signature WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefits WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_hr_position_grades WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_additional_info WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_category WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_discount WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_package WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_pkg_asmnt WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_pkg_detail WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_providers WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_rates WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_setup WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_states WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_benefit_subtype WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_user_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_beneficiary WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM ee_benefit_refusal WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM co_locations WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;


/* FIX creation_date order issue */
SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE pr_nbr INTEGER;
DECLARE VARIABLE new_creation_date TIMESTAMP;
DECLARE VARIABLE update_char CHAR(1);
BEGIN
  FOR SELECT p.pr_nbr, MAX(creation_date) FROM pr p GROUP BY 1 INTO pr_nbr, new_creation_date
  DO
  BEGIN
    update_char = NULL;
    SELECT p.active_record FROM pr p WHERE p.pr_nbr = :pr_nbr AND p.active_record <> 'P' ORDER BY p.active_record ROWS 1 INTO update_char;

    IF (update_char IS NOT NULL) THEN
    BEGIN
      new_creation_date = new_creation_date + 0.000011574; -- add 1 second
      UPDATE pr p SET creation_date = :new_creation_date WHERE pr_nbr = :pr_nbr AND active_record = :update_char;
    END
  END
END^
COMMIT^

SET TERM ;^


/* FIX DATA INTEGRITY ISSUES */
UPDATE cl SET termination_code = 'A' WHERE termination_code IS NULL;
UPDATE cl_e_ds SET override_fed_tax_type = 'N' WHERE override_fed_tax_type IS NULL;
UPDATE cl_hr_person_education SET degree_level = 'O' WHERE degree_level IS NULL;
UPDATE co SET business_type = ' ' WHERE business_type IS NULL;
UPDATE co SET eftps_enrollment_status = 'N' WHERE eftps_enrollment_status IS NULL;
UPDATE co SET pay_frequencies = ' ' WHERE pay_frequencies IS NULL;
UPDATE co_fed_tax_liabilities SET adjustment_type = 'Z' WHERE adjustment_type IS NULL;
UPDATE co_hr_applicant SET applicant_contacted = 'N' WHERE applicant_contacted IS NULL;
UPDATE co_hr_applicant SET applicant_status = 'A' WHERE applicant_status IS NULL;
UPDATE co_hr_applicant SET applicant_type = 'N' WHERE applicant_type IS NULL;
UPDATE co_hr_applicant SET desired_regular_or_temporary = 'R' WHERE desired_regular_or_temporary IS NULL;
UPDATE co_hr_applicant SET position_status = 'F' WHERE position_status IS NULL;
UPDATE co_hr_car SET color = 'O' WHERE color IS NULL;
UPDATE co_local_tax SET deduct = 'Y' WHERE deduct IS NULL;
UPDATE co_local_tax SET eft_status = 'N' WHERE eft_status IS NULL;
UPDATE co_local_tax_liabilities SET adjustment_type = 'Z' WHERE adjustment_type IS NULL;
UPDATE co_state_tax_liabilities SET adjustment_type = 'Z' WHERE adjustment_type IS NULL;
UPDATE co_sui_liabilities SET adjustment_type = 'Z' WHERE adjustment_type IS NULL;
UPDATE co_tax_deposits SET deposit_type  = 'M' WHERE deposit_type IS NULL;
UPDATE co_time_off_accrual SET annual_reset_code = '3' WHERE annual_reset_code IS NULL;
UPDATE co_time_off_accrual SET auto_create_on_new_hire = 'N' WHERE auto_create_on_new_hire IS NULL;
UPDATE co_time_off_accrual SET rollover_payroll_of_month = 'N' WHERE rollover_payroll_of_month IS NULL;
UPDATE ee SET eic = 'N' WHERE eic IS NULL;
UPDATE ee SET gov_garnish_prior_child_suppt = 'N' WHERE gov_garnish_prior_child_suppt IS NULL;
UPDATE ee SET last_qual_benefit_event = ' ' WHERE last_qual_benefit_event IS NULL;
UPDATE ee SET NEXT_PAY_FREQUENCY = 'W' WHERE NEXT_PAY_FREQUENCY IS NULL;
UPDATE ee SET position_status = 'N' WHERE position_status IS NULL;
UPDATE ee_hr_attendance SET excused = 'Y' WHERE excused IS NULL;
UPDATE ee_hr_attendance SET warning_issued = 'N' WHERE warning_issued IS NULL;
UPDATE ee_hr_injury_occurrence SET death = 'N' WHERE death IS NULL;
UPDATE ee_hr_injury_occurrence SET first_aid= 'Y' WHERE first_aid IS NULL;
UPDATE ee_scheduled_e_ds SET calculation_type = 'F' WHERE calculation_type IS NULL;
UPDATE ee_scheduled_e_ds SET target_action = 'N' WHERE target_action IS NULL;
UPDATE ee_time_off_accrual SET rollover_freq = 'N' WHERE rollover_freq IS NULL;
UPDATE ee_time_off_accrual SET rollover_payroll_of_month = '4' WHERE rollover_payroll_of_month IS NULL;
UPDATE ee_rates SET rate_amount = 0 WHERE rate_amount IS NULL;
UPDATE pr_check_lines SET agency_status = 'P' WHERE agency_status IS NULL;
UPDATE pr_miscellaneous_checks SET eftps_tax_type = 'N' WHERE eftps_tax_type IS NULL;

UPDATE co c SET home_co_states_nbr = NULL
WHERE
  c.active_record = 'C' AND
  c.home_co_states_nbr IS NOT NULL AND
  NOT EXISTS (SELECT s.co_nbr FROM co_states s WHERE s.co_states_nbr = c.home_co_states_nbr AND s.co_nbr = c.co_nbr AND s.active_record = 'C');

COMMIT;

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE cl_nbr INTEGER;
BEGIN
  cl_nbr = NULL;
  SELECT cl_nbr FROM cl WHERE active_record IN ('C', 'N') ORDER BY creation_date DESC ROWS 1 INTO :cl_nbr;
  IF (cl_nbr IS NOT NULL) THEN
    UPDATE cl SET cl_nbr = :cl_nbr WHERE cl_nbr <> :cl_nbr;
END^


EXECUTE BLOCK
AS
DECLARE VARIABLE co_bank_account_register_nbr INTEGER;
DECLARE VARIABLE pr_miscellaneous_checks_nbr INTEGER;
DECLARE VARIABLE effective_date TIMESTAMP;
DECLARE VARIABLE creation_date TIMESTAMP;
BEGIN
  FOR SELECT b.co_bank_account_register_nbr, b.effective_date, b.creation_date, p.pr_miscellaneous_checks_nbr
      FROM co_bank_account_register b LEFT JOIN pr_miscellaneous_checks p
           ON b.pr_miscellaneous_checks_nbr = p.pr_miscellaneous_checks_nbr
      WHERE b.pr_miscellaneous_checks_nbr IS NOT NULL
  INTO co_bank_account_register_nbr, effective_date, creation_date, pr_miscellaneous_checks_nbr
  DO
   IF (pr_miscellaneous_checks_nbr IS NULL) THEN
     UPDATE co_bank_account_register SET pr_miscellaneous_checks_nbr = NULL
     WHERE co_bank_account_register_nbr = :co_bank_account_register_nbr AND
           effective_date = :effective_date AND
           creation_date = :creation_date;

  FOR SELECT b.co_bank_account_register_nbr, b.effective_date, b.creation_date, p.pr_miscellaneous_checks_nbr
      FROM co_bank_account_register b JOIN pr_miscellaneous_checks p
           ON b.pr_miscellaneous_checks_nbr = p.pr_miscellaneous_checks_nbr AND p.active_record = 'N'
      WHERE b.pr_miscellaneous_checks_nbr IS NOT NULL
  INTO co_bank_account_register_nbr, effective_date, creation_date, pr_miscellaneous_checks_nbr
  DO
   IF (pr_miscellaneous_checks_nbr IS NOT NULL) THEN
     UPDATE co_bank_account_register SET pr_miscellaneous_checks_nbr = NULL
     WHERE co_bank_account_register_nbr = :co_bank_account_register_nbr AND
           effective_date = :effective_date AND
           creation_date = :creation_date;
END^

COMMIT^


/* DELETE LOST BLOBS */

DROP TRIGGER ti_cl_blob^
DROP TRIGGER tu_cl_blob^
DROP TRIGGER td_cl_blob^
DROP TRIGGER ti_co_tax_return_queue^
DROP TRIGGER ti_sb_tax_return_runs^
DROP PROCEDURE co_tax_return_runs_hist^
DROP PROCEDURE co_tax_return_runs_del^
DROP PROCEDURE co_tax_return_queue_del^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE cl_blob_nbr INTEGER;
DECLARE VARIABLE ref1 INTEGER;
DECLARE VARIABLE ref2 INTEGER;
DECLARE VARIABLE ref3 INTEGER;
DECLARE VARIABLE ref4 INTEGER;
DECLARE VARIABLE ref5 INTEGER;
DECLARE VARIABLE ref6 INTEGER;
BEGIN
  FOR SELECT q.co_tax_return_queue_nbr
      FROM co_tax_return_queue q
      WHERE q.active_record = 'N'
      INTO :ref1
      DO
  BEGIN
    UPDATE co_tax_return_queue SET cl_blob_nbr = NULL WHERE co_tax_return_queue_nbr = :ref1;
  END

  FOR SELECT b.cl_blob_nbr, t1.cl_blob_nbr
      FROM  cl_blob b LEFT JOIN co_tax_return_queue t1 ON b.cl_blob_nbr = t1.cl_blob_nbr
      INTO  :cl_blob_nbr, :ref1
  DO
  BEGIN
    IF (ref1 IS NULL) THEN
    BEGIN
      FOR SELECT t2.cl_blob_nbr, t3.logo_cl_blob_nbr, t4.signature_cl_blob_nbr, t5.info_blob, t6.info_blob
          FROM cl_blob b LEFT JOIN co_pr_ach t2 ON b.cl_blob_nbr = t2.cl_blob_nbr
               LEFT JOIN cl_bank_account t3 ON b.cl_blob_nbr = t3.logo_cl_blob_nbr
               LEFT JOIN cl_bank_account t4 ON b.cl_blob_nbr = t4.signature_cl_blob_nbr
               LEFT JOIN co_additional_info t5 ON b.cl_blob_nbr = t5.info_blob
               LEFT JOIN ee_additional_info t6 ON b.cl_blob_nbr = t6.info_blob
          WHERE
            b.cl_blob_nbr = :cl_blob_nbr
          INTO  :ref2, :ref3, :ref4, :ref5, :ref6
      DO
      BEGIN
        IF (ref2 IS NULL AND ref3 IS NULL AND ref4 IS NULL AND ref5 IS NULL AND ref6 IS NULL) THEN
          DELETE FROM cl_blob WHERE cl_blob_nbr = :cl_blob_nbr;
      END
    END
  END
END^

COMMIT^


ALTER TABLE co_tax_return_runs ADD cl_blob_nbr INTEGER NOT NULL^
ALTER TABLE co_tax_return_runs ADD change_date TIMESTAMP NOT NULL^
COMMIT^


/* MOVE co_tax_return_runs BLOBS to cl_blob */

EXECUTE BLOCK
AS
DECLARE VARIABLE co_tax_return_runs_nbr INTEGER;
DECLARE VARIABLE cl_blob_nbr INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE data BLOB;
BEGIN
  FOR SELECT r.co_tax_return_runs_nbr, r.creation_date, r.data
      FROM co_tax_return_runs r
      WHERE r.active_record = 'C' AND data IS NOT NULL
      INTO :co_tax_return_runs_nbr, :creation_date, :data
  DO
  BEGIN
    cl_blob_nbr = NEXT VALUE FOR cl_blob_gen;

    INSERT INTO cl_blob (cl_blob_nbr, data, change_date) VALUES(:cl_blob_nbr, :data, :creation_date);

    UPDATE co_tax_return_runs SET cl_blob_nbr = :cl_blob_nbr, change_date = :creation_date
    WHERE co_tax_return_runs_nbr = :co_tax_return_runs_nbr;
  END

  UPDATE co_tax_return_runs SET cl_blob_nbr = 0, change_date = creation_date WHERE cl_blob_nbr IS NULL;
END^

COMMIT^

ALTER TABLE co_tax_return_runs DROP data^
COMMIT^


/* MOVE co_tax_return_queue.cl_blob_nbr to co_tax_return_runs */

EXECUTE BLOCK
AS
DECLARE VARIABLE co_tax_return_queue_nbr INTEGER;
DECLARE VARIABLE old_co_tax_return_queue_nbr INTEGER;
DECLARE VARIABLE co_tax_return_runs_nbr INTEGER;
DECLARE VARIABLE cl_blob_nbr INTEGER;
DECLARE VARIABLE old_cl_blob_nbr INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE changed_by INTEGER;
BEGIN
  old_co_tax_return_queue_nbr = NULL;

  FOR SELECT q.co_tax_return_queue_nbr, q.creation_date, q.changed_by, q.cl_blob_nbr
      FROM co_tax_return_queue q
      WHERE q.cl_blob_nbr IS NOT NULL
      ORDER BY q.co_tax_return_queue_nbr, q.creation_date
      INTO :co_tax_return_queue_nbr, :creation_date, :changed_by, :cl_blob_nbr
  DO
  BEGIN
    IF (co_tax_return_queue_nbr IS DISTINCT FROM old_co_tax_return_queue_nbr) THEN
    BEGIN
      old_co_tax_return_queue_nbr = co_tax_return_queue_nbr;
      old_cl_blob_nbr = NULL;
    END

    IF (cl_blob_nbr IS DISTINCT FROM old_cl_blob_nbr) THEN
    BEGIN
      co_tax_return_runs_nbr = NEXT VALUE FOR co_tax_return_runs_gen;

      INSERT INTO co_tax_return_runs
        (co_tax_return_runs_nbr, active_record, effective_date,creation_date,
         changed_by, change_date, co_tax_return_queue_nbr, cl_blob_nbr)
       VALUES (:co_tax_return_runs_nbr, 'C', :creation_date, :creation_date,
               :changed_by, :creation_date, :co_tax_return_queue_nbr, :cl_blob_nbr);

       old_cl_blob_nbr = cl_blob_nbr;
     END
  END
END^

COMMIT^


/* Update empty blobs */


EXECUTE BLOCK
AS
DECLARE VARIABLE tbl_name VARCHAR(64);
DECLARE VARIABLE fld_name VARCHAR(64);
DECLARE VARIABLE sql VARCHAR(1024);
BEGIN
  FOR SELECT TRIM(r.rdb$relation_name), TRIM(f.rdb$field_name)
      FROM rdb$relations r, rdb$relation_fields f, rdb$fields dt
      WHERE
        (r.rdb$system_flag = 0 OR r.rdb$system_flag IS NULL) AND
        (f.rdb$system_flag = 0 OR f.rdb$system_flag IS NULL) AND
        f.rdb$relation_name = r.rdb$relation_name AND
        (f.rdb$null_flag = 0 OR f.rdb$null_flag IS NULL) AND
        f.rdb$field_source = dt.rdb$field_name AND
        dt.rdb$field_type = 261
  INTO :tbl_name, :fld_name
  DO
  BEGIN
    sql = 'UPDATE ' || tbl_name || ' SET ' || fld_name || ' = NULL WHERE GetBlobSize(' || fld_name || ') = 0';

    EXECUTE STATEMENT sql;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE nbr INTEGER;
DECLARE VARIABLE bnbr INTEGER;
BEGIN
  FOR SELECT t.co_tax_return_queue_nbr, b.cl_blob_nbr
      FROM co_tax_return_queue t, cl_blob b
      WHERE t.cl_blob_nbr = b.cl_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE co_tax_return_queue SET cl_blob_nbr = NULL WHERE co_tax_return_queue_nbr = : nbr;
    DELETE FROM cl_blob WHERE cl_blob_nbr = :bnbr;
  END

  FOR SELECT t.ee_additional_info_nbr, b.cl_blob_nbr
      FROM ee_additional_info t, cl_blob b
      WHERE t.info_blob = b.cl_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE ee_additional_info SET info_blob = NULL WHERE ee_additional_info_nbr = : nbr;
    DELETE FROM cl_blob WHERE cl_blob_nbr = :bnbr;
  END

  FOR SELECT t.co_additional_info_nbr, b.cl_blob_nbr
      FROM co_additional_info t, cl_blob b
      WHERE t.info_blob = b.cl_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE co_additional_info SET info_blob = NULL WHERE co_additional_info_nbr = : nbr;
    DELETE FROM cl_blob WHERE cl_blob_nbr = :bnbr;
  END

  FOR SELECT t.co_pr_ach_nbr, b.cl_blob_nbr
      FROM co_pr_ach t, cl_blob b
      WHERE t.cl_blob_nbr = b.cl_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE co_pr_ach SET cl_blob_nbr = NULL WHERE co_pr_ach_nbr = : nbr;
    DELETE FROM cl_blob WHERE cl_blob_nbr = :bnbr;
  END

  FOR SELECT t.cl_bank_account_nbr, b.cl_blob_nbr
      FROM cl_bank_account t, cl_blob b
      WHERE t.logo_cl_blob_nbr = b.cl_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE cl_bank_account SET logo_cl_blob_nbr = NULL WHERE cl_bank_account_nbr = : nbr;
    DELETE FROM cl_blob WHERE cl_blob_nbr = :bnbr;
  END

  FOR SELECT t.cl_bank_account_nbr, b.cl_blob_nbr
      FROM cl_bank_account t, cl_blob b
      WHERE t.signature_cl_blob_nbr = b.cl_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE cl_bank_account SET signature_cl_blob_nbr = NULL WHERE cl_bank_account_nbr = : nbr;
    DELETE FROM cl_blob WHERE cl_blob_nbr = :bnbr;
  END
END^

COMMIT^


/* Add prev_status field in liability tables */

ALTER TABLE co_fed_tax_liabilities ADD prev_status CHAR(1) NOT NULL^
ALTER TABLE co_state_tax_liabilities ADD prev_status CHAR(1) NOT NULL^
ALTER TABLE co_sui_liabilities ADD prev_status CHAR(1) NOT NULL^
ALTER TABLE co_local_tax_liabilities ADD prev_status CHAR(1) NOT NULL^
COMMIT^

UPDATE co_fed_tax_liabilities SET prev_status = 'Z'^
COMMIT^
UPDATE co_state_tax_liabilities SET prev_status = 'Z'^
COMMIT^
UPDATE co_sui_liabilities SET prev_status = 'Z'^
COMMIT^
UPDATE co_local_tax_liabilities SET prev_status = 'Z'^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE cotaxdepositsnbr INTEGER;
DECLARE VARIABLE co_liab_nbr INTEGER;
DECLARE VARIABLE p_status CHAR;
DECLARE VARIABLE mineffectivedate TIMESTAMP;
DECLARE VARIABLE beforepaidtime TIMESTAMP;
DECLARE VARIABLE l INTEGER;
BEGIN
  /*  Federal Level */
  FOR SELECT a.co_tax_deposits_nbr, MIN(c.effective_date)
      FROM co_fed_tax_liabilities c, co_tax_deposits a
      WHERE  a.active_record = 'C' AND c.active_record = 'C' AND c.co_tax_deposits_nbr = a.co_tax_deposits_nbr
      GROUP BY a.co_tax_deposits_nbr
      INTO :cotaxdepositsnbr, :mineffectivedate
   DO
   BEGIN
     FOR SELECT z.co_fed_tax_liabilities_nbr  FROM co_fed_tax_liabilities z
         WHERE z.co_tax_deposits_nbr = :cotaxdepositsnbr AND z.active_record = 'C'
         INTO :co_liab_nbr
     DO
     BEGIN
       beforepaidtime = mineffectivedate - ( 1.0000000 / 1440);
       p_status = NULL;

       SELECT m.status FROM co_fed_tax_liabilities m,
         ( SELECT co_fed_tax_liabilities_nbr, MAX(effective_date) effective_date  FROM co_fed_tax_liabilities
           WHERE  co_fed_tax_liabilities_nbr = :co_liab_nbr AND co_tax_deposits_nbr IS NULL
           GROUP BY co_fed_tax_liabilities_nbr  HAVING MAX(effective_date) < :beforepaidtime) o
       WHERE m.effective_date = o.effective_date AND
             m.co_fed_tax_liabilities_nbr = o.co_fed_tax_liabilities_nbr
       ORDER BY m.creation_date DESC
       ROWS 1
       INTO  :p_status;

       IF (p_status IS NOT NULL) THEN
       BEGIN
         /* Replace  'prev_status'  with new Field    */
         UPDATE  co_fed_tax_liabilities SET prev_status = :p_status
         WHERE   co_fed_tax_liabilities_nbr = :co_liab_nbr AND active_record = 'C';
       END
    END
  END

  /*  State Level */
  FOR SELECT a.co_tax_deposits_nbr, MIN(c.effective_date)
      FROM co_state_tax_liabilities c, co_tax_deposits a
      WHERE  a.active_record = 'C' AND c.active_record = 'C' AND c.co_tax_deposits_nbr = a.co_tax_deposits_nbr
      GROUP BY a.co_tax_deposits_nbr
      INTO :cotaxdepositsnbr, :mineffectivedate
   DO
   BEGIN
     FOR SELECT z.co_state_tax_liabilities_nbr  FROM co_state_tax_liabilities z
         WHERE z.co_tax_deposits_nbr = :cotaxdepositsnbr AND z.active_record = 'C'
         INTO :co_liab_nbr
     DO
     BEGIN
       beforepaidtime = mineffectivedate - ( 1.0000000 / 1440);
       p_status = NULL;

       SELECT m.status FROM co_state_tax_liabilities m,
         ( SELECT co_state_tax_liabilities_nbr, MAX(effective_date) effective_date  FROM co_state_tax_liabilities
           WHERE  co_state_tax_liabilities_nbr = :co_liab_nbr AND co_tax_deposits_nbr IS NULL
           GROUP BY co_state_tax_liabilities_nbr  HAVING MAX(effective_date) < :beforepaidtime) o
       WHERE m.effective_date = o.effective_date AND
             m.co_state_tax_liabilities_nbr = o.co_state_tax_liabilities_nbr
       ORDER BY m.creation_date DESC
       ROWS 1
       INTO  :p_status;

       IF (p_status IS NOT NULL) THEN
       BEGIN
         /* Replace  'prev_status'  with new Field    */
         UPDATE  co_state_tax_liabilities SET prev_status = :p_status
         WHERE   co_state_tax_liabilities_nbr = :co_liab_nbr AND active_record = 'C';
       END
    END
  END

  /*  SUI Level */
  FOR SELECT a.co_tax_deposits_nbr, MIN(c.effective_date)
      FROM co_sui_liabilities c, co_tax_deposits a
      WHERE  a.active_record = 'C' AND c.active_record = 'C' AND c.co_tax_deposits_nbr = a.co_tax_deposits_nbr
      GROUP BY a.co_tax_deposits_nbr
      INTO :cotaxdepositsnbr, :mineffectivedate
   DO
   BEGIN
     FOR SELECT z.co_sui_liabilities_nbr  FROM co_sui_liabilities z
         WHERE z.co_tax_deposits_nbr = :cotaxdepositsnbr AND z.active_record = 'C'
         INTO :co_liab_nbr
     DO
     BEGIN
       beforepaidtime = mineffectivedate - ( 1.0000000 / 1440);
       p_status = NULL;

       SELECT m.status FROM co_sui_liabilities m,
         ( SELECT co_sui_liabilities_nbr, MAX(effective_date) effective_date  FROM co_sui_liabilities
           WHERE  co_sui_liabilities_nbr = :co_liab_nbr AND co_tax_deposits_nbr IS NULL
           GROUP BY co_sui_liabilities_nbr  HAVING MAX(effective_date) < :beforepaidtime) o
       WHERE m.effective_date = o.effective_date AND
             m.co_sui_liabilities_nbr = o.co_sui_liabilities_nbr
       ORDER BY m.creation_date DESC
       ROWS 1
       INTO  :p_status;

       IF (p_status IS NOT NULL) THEN
       BEGIN
         /* Replace  'prev_status'  with new Field    */
         UPDATE  co_sui_liabilities SET prev_status = :p_status
         WHERE   co_sui_liabilities_nbr = :co_liab_nbr AND active_record = 'C';
       END
    END
  END


  /*  Local Level */
  FOR SELECT a.co_tax_deposits_nbr, MIN(c.effective_date)
      FROM co_local_tax_liabilities c, co_tax_deposits a
      WHERE  a.active_record = 'C' AND c.active_record = 'C' AND c.co_tax_deposits_nbr = a.co_tax_deposits_nbr
      GROUP BY a.co_tax_deposits_nbr
      INTO :cotaxdepositsnbr, :mineffectivedate
   DO
   BEGIN
     FOR SELECT z.co_local_tax_liabilities_nbr  FROM co_local_tax_liabilities z
         WHERE z.co_tax_deposits_nbr = :cotaxdepositsnbr AND z.active_record = 'C'
         INTO :co_liab_nbr
     DO
     BEGIN
       beforepaidtime = mineffectivedate - ( 1.0000000 / 1440);
       p_status = NULL;

       SELECT m.status FROM co_local_tax_liabilities m,
         ( SELECT co_local_tax_liabilities_nbr, MAX(effective_date) effective_date  FROM co_local_tax_liabilities
           WHERE  co_local_tax_liabilities_nbr = :co_liab_nbr AND co_tax_deposits_nbr IS NULL
           GROUP BY co_local_tax_liabilities_nbr  HAVING MAX(effective_date) < :beforepaidtime) o
       WHERE m.effective_date = o.effective_date AND
             m.co_local_tax_liabilities_nbr = o.co_local_tax_liabilities_nbr
       ORDER BY m.creation_date DESC
       ROWS 1
       INTO  :p_status;

       IF (p_status IS NOT NULL) THEN
       BEGIN
         /* Replace  'prev_status'  with new Field    */
         UPDATE  co_local_tax_liabilities SET prev_status = :p_status
         WHERE   co_local_tax_liabilities_nbr = :co_liab_nbr AND active_record = 'C';
       END
    END
  END
END^

COMMIT^



/* Add payroll approve fields */
ALTER TABLE pr ADD approved_by_finance_nbr INTEGER^
ALTER TABLE pr ADD approved_by_tax_nbr INTEGER^
ALTER TABLE pr ADD approved_by_management_nbr INTEGER^
COMMIT^


EXECUTE BLOCK
AS
DECLARE VARIABLE pr_nbr INTEGER;
DECLARE VARIABLE user_nbr INTEGER;
DECLARE VARIABLE pr_nbr2 INTEGER;
DECLARE VARIABLE approved_f CHAR(1);
DECLARE VARIABLE approved_t CHAR(1);
DECLARE VARIABLE approved_m CHAR(1);
DECLARE VARIABLE approved_fi INTEGER;
DECLARE VARIABLE approved_ti INTEGER;
DECLARE VARIABLE approved_mi INTEGER;
DECLARE VARIABLE approved_f2 CHAR(1);
DECLARE VARIABLE approved_t2 CHAR(1);
DECLARE VARIABLE approved_m2 CHAR(1);
DECLARE VARIABLE effective_date TIMESTAMP;
DECLARE VARIABLE creation_date TIMESTAMP;
BEGIN
  approved_f = '';
  approved_t = '';
  approved_m = '';
  pr_nbr = 0;

  FOR SELECT pr_nbr, changed_by, effective_date, creation_date, approved_by_finance, approved_by_tax, approved_by_management
      FROM pr
      ORDER BY pr_nbr, effective_date, creation_date
      INTO :pr_nbr2, :user_nbr, :effective_date,  :creation_date, :approved_f2, :approved_t2, :approved_m2
  DO
  BEGIN
    IF (pr_nbr2 <> pr_nbr) THEN
    BEGIN
      pr_nbr = pr_nbr2;

      approved_fi=user_nbr;
      approved_f=approved_f2;

      approved_ti=user_nbr;
      approved_t=approved_t2;

      approved_mi=user_nbr;
      approved_m=approved_m2;
    END

    IF (approved_f2 <> approved_f) THEN
    BEGIN
      approved_fi=user_nbr;
      approved_f=approved_f2;
    END

    IF (approved_t2 <> approved_t) THEN
    BEGIN
      approved_ti=user_nbr;
      approved_t=approved_t2;
    END

    IF (approved_m2 <> approved_m) THEN
    BEGIN
      approved_mi=user_nbr;
      approved_m=approved_m2;
    END

    UPDATE pr
    SET approved_by_finance_nbr=:approved_fi,
        approved_by_tax_nbr=:approved_ti,
        approved_by_management_nbr=:approved_mi
    WHERE pr_nbr=:pr_nbr
    AND effective_date=:effective_date
    AND creation_date=:creation_date;
  END
END^

COMMIT^


/* Add in_prenote_date field */
ALTER TABLE ee_direct_deposit ADD in_prenote_date DATE^
COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE nbr INTEGER;
DECLARE VARIABLE in_pren_date DATE;
DECLARE VARIABLE eff_date TIMESTAMP;
DECLARE VARIABLE cre_date TIMESTAMP;
DECLARE VARIABLE in_pren CHAR(1);
DECLARE VARIABLE prev_in_pren CHAR(1);
BEGIN
  FOR SELECT ee_direct_deposit_nbr, MIN(creation_date)
  FROM ee_direct_deposit
  GROUP BY ee_direct_deposit_nbr
  ORDER BY ee_direct_deposit_nbr
  INTO nbr, in_pren_date DO
  BEGIN
    prev_in_pren = '';
    FOR SELECT effective_date, creation_date, in_prenote
    FROM ee_direct_deposit
    WHERE ee_direct_deposit_nbr=:nbr
    ORDER BY effective_date, creation_date
    INTO eff_date, cre_date, in_pren
    DO
    BEGIN
      IF (in_pren <> prev_in_pren) THEN
      IF (in_pren='Y') THEN
        in_pren_date = eff_date;

      UPDATE ee_direct_deposit
      SET in_prenote_date=:in_pren_date
      WHERE ee_direct_deposit_nbr=:nbr
      AND effective_date=:eff_date AND creation_date=:cre_date;
    END
  END
END^
COMMIT^


/* PREPARE DB STRUCTURE */
       
EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$trigger_name FROM rdb$triggers
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP TRIGGER ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$procedure_name FROM rdb$procedures
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP PROCEDURE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$generator_name FROM rdb$generators
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    IF ((s <> 'NEXT_PAYMENT_SERIAL_NBR_GEN') AND (s <> 'CL_BLOB_GEN')) THEN
      EXECUTE STATEMENT 'DROP SEQUENCE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$exception_name FROM rdb$exceptions
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP EXCEPTION ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$index_name FROM rdb$indices i WHERE
        (rdb$system_flag = 0 OR rdb$system_flag IS NULL) AND
        NOT EXISTS (SELECT rdb$index_name FROM rdb$relation_constraints
        WHERE rdb$index_name = i.rdb$index_name)
      INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP INDEX ' || s;
  END
END^

COMMIT^

SET TERM ;^


/* DROP JUNK TABLES */

DROP TABLE version_info;
DROP TABLE change_log;
DROP TABLE transaction_info;
DROP TABLE co_tax_dep_reprint_histry;
DROP TABLE cl_report_groups;
DROP TABLE cl_report_group_members;
DROP TABLE co_event_calendar;
DROP TABLE co_job_group_jobs;
DROP TABLE co_reports_history;
DROP TABLE co_sched_reports_queue;
    

COMMIT;

/* Prepare X_TRANSACTION table */

CREATE TABLE x_transaction (
    start_time   TIMESTAMP,
    user_id      INTEGER);

COMMIT;

INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_billing;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_common_paymaster;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_pieces;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_3_party_sick_pay_admin;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_e_d_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_skills;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_bank_account;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_co_consolidation;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_timeclock_imports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_delivery_method;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_person;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_states;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_delivery_group;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_agency;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_pension;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_union;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_e_ds;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_check_lines;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_funds;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_person_dependents;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_branch;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_department;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_team;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pr_batch_deflt_ed;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_states;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_bank_account_register;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_billing_history;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_delivery_package;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_division;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_e_d_codes;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_applicant;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_attendance_types;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_car;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_performance_ratings;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_positions;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_property;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_recruiters;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_referrals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_salary_grades;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_supervisors;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_jobs;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_local_tax;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_manual_ach;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pay_group;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pr_ach;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pr_check_templates;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pr_filters;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_services;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_shifts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_sui;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_tax_deposits;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_time_off_accrual;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_unions;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_workers_comp;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_salesperson;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_tax_payment_ach;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_child_support_cases;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_direct_deposit;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_scheduled_e_ds;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_check;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_batch;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_miscellaneous_checks;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_phone;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_pension_fund_splits;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_general_ledger;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_e_d_group_codes;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_reprint_history_detail;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_e_d_local_exmpt_excld;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_e_d_state_exmpt_excld;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_rates;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_piece_work;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_work_shifts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_autolabor_distribution;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_time_off_accrual;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_time_off_accrual_rates;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_scheduled_event;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_hr_property_tracking;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_person_education;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_applicant_interview;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_person_skills;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_hr_attendance;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_hr_injury_occurrence;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_hr_co_provided_educatn;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_person_handicaps;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_union_dues;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_check_line_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_check_sui;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_scheduled_e_ds;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_check_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_check_states;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_services;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_billing_history_detail;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_div_pr_batch_deflt_ed;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_batch_local_or_temps;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_batch_states_or_temps;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_fed_tax_liabilities;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_state_tax_liabilities;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_sui_liabilities;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_local_tax_liabilities;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_reprint_history;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pensions;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_brch_pr_batch_deflt_ed;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_dept_pr_batch_deflt_ed;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_team_pr_batch_deflt_ed;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_salesperson_flat_amt;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_job_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_additional_info_names;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_tax_return_queue;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_additional_info;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_scheduled_event_batch;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_division_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_branch_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_department_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_team_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_school;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_report_writer_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_emergency_contacts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_benefits;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT change_date, 0 FROM cl_blob;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_jobs_locals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_hr_car;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_pr_check_template_e_ds;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM pr_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_mail_box_group;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_reason_codes;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_time_off_accrual_oper;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_tax_return_runs;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_mail_box_group_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_hr_performance_ratings;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_person_documents;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_time_off_accrual_tiers;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_calendar_defaults;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_additional_info_values;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_benefit_payment;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_group;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM cl_hr_course;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_storage;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_group_manager;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_group_member;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_change_request;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_auto_enlist_returns;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_enlist_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_bank_acc_reg_details;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_hr_position_grades;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_signature;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefits;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_additional_info;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_category;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_discount;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_package;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_pkg_asmnt;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_pkg_detail;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_providers;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_rates;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_setup;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_states;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_benefit_subtype;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_user_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_beneficiary;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM ee_benefit_refusal;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM co_locations;
COMMIT;


/* CREATE TEMP TABLES */

CREATE TABLE xl (
    cl_nbr                          INTEGER NOT NULL,
    custom_client_number            VARCHAR(20) NOT NULL,
    name                            VARCHAR(40) NOT NULL,
    address1                        VARCHAR(30) NOT NULL,
    address2                        VARCHAR(30),
    city                            VARCHAR(20) NOT NULL,
    state                           CHAR(2) NOT NULL,
    zip_code                        VARCHAR(10) NOT NULL,
    contact1                        VARCHAR(30),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax1                            VARCHAR(20),
    fax1_description                VARCHAR(10),
    fax2                            VARCHAR(20),
    fax2_description                VARCHAR(10),
    e_mail_address1                 VARCHAR(80),
    e_mail_address2                 VARCHAR(80),
    sb_accountant_number            INTEGER,
    accountant_contact              VARCHAR(40),
    print_cpa                       CHAR(1) NOT NULL,
    leasing_company                 CHAR(1) NOT NULL,
    reciprocate_sui                 CHAR(1) NOT NULL,
    security_font                   CHAR(1) NOT NULL,
    auto_save_minutes               INTEGER NOT NULL,
    customer_service_sb_user_nbr    INTEGER,
    csr_sb_team_nbr                 INTEGER,
    start_date                      DATE NOT NULL,
    termination_date                DATE,
    termination_code                CHAR(1),
    termination_notes               BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    enable_hr                       CHAR(1) NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    agency_check_mb_group_nbr       INTEGER,
    pr_check_mb_group_nbr           INTEGER,
    pr_report_mb_group_nbr          INTEGER,
    pr_report_second_mb_group_nbr   INTEGER,
    tax_check_mb_group_nbr          INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    tax_return_mb_group_nbr         INTEGER,
    tax_return_second_mb_group_nbr  INTEGER,
    reciprocate_state               CHAR(1) NOT NULL,
    phone1_ext                      VARCHAR(10),
    phone2_ext                      VARCHAR(10),
    print_client_name               CHAR(1) NOT NULL,
    maintenance_hold                CHAR(1) NOT NULL,
    read_only                       CHAR(1) NOT NULL,
    block_invalid_ssn               CHAR(1) NOT NULL
);


CREATE TABLE xl_3_party_sick_pay_admin (
    cl_3_party_sick_pay_admin_nbr  INTEGER NOT NULL,
    sb_agency_nbr                  INTEGER NOT NULL,
    name                           VARCHAR(40) NOT NULL,
    long_term                      CHAR(1) NOT NULL,
    short_term                     CHAR(1) NOT NULL,
    w2                             CHAR(1) NOT NULL,
    state_tax_in_payroll           CHAR(1) NOT NULL,
    local_tax_in_payroll           CHAR(1) NOT NULL,
    non_tax_in_payroll             CHAR(1) NOT NULL,
    contact                        VARCHAR(30),
    phone                          VARCHAR(20),
    description                    VARCHAR(10),
    fax                            VARCHAR(20),
    fax_description                VARCHAR(10),
    e_mail_address                 VARCHAR(80),
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    non_taxable_percentage         NUMERIC(18,6)
);


CREATE TABLE xl_agency (
    cl_agency_nbr               INTEGER NOT NULL,
    sb_agency_nbr               INTEGER NOT NULL,
    report_method               CHAR(1) NOT NULL,
    frequency                   CHAR(1) NOT NULL,
    month_number                VARCHAR(2),
    cl_bank_account_nbr         INTEGER NOT NULL,
    payment_type                CHAR(1) NOT NULL,
    cl_delivery_group_nbr       INTEGER,
    client_agency_custom_field  VARCHAR(10),
    notes                       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                      VARCHAR(512),
    creation_date               TIMESTAMP NOT NULL,
    changed_by                  INTEGER NOT NULL,
    gl_tag                      VARCHAR(20),
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    gl_offset_tag               VARCHAR(20),
    cl_mail_box_group_nbr       INTEGER,
    check_memo_line_1           VARCHAR(40),
    check_memo_line_2           VARCHAR(40)
);


CREATE TABLE xl_bank_account (
    cl_bank_account_nbr         INTEGER NOT NULL,
    sb_banks_nbr                INTEGER NOT NULL,
    custom_bank_account_number  VARCHAR(20) NOT NULL,
    bank_account_type           CHAR(1) NOT NULL,
    next_check_number           INTEGER NOT NULL,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    logo_cl_blob_nbr            INTEGER,
    signature_cl_blob_nbr       INTEGER,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    beginning_balance           NUMERIC(18,6),
    notes                       BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    description                 VARCHAR(80),
    recurring_wire_number       VARCHAR(20)
);


CREATE TABLE xl_billing (
    cl_billing_nbr      INTEGER NOT NULL,
    name                VARCHAR(40) NOT NULL,
    address1            VARCHAR(30),
    address2            VARCHAR(30),
    city                VARCHAR(20),
    state               CHAR(2),
    zip_code            VARCHAR(10),
    contact             VARCHAR(30) NOT NULL,
    phone               VARCHAR(20),
    description         VARCHAR(10),
    fax                 VARCHAR(20),
    fax_description     VARCHAR(10),
    e_mail_address      VARCHAR(80),
    payment_method      CHAR(1) NOT NULL,
    invoice_separately  CHAR(1) NOT NULL,
    billing_notes       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler              VARCHAR(512),
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);


CREATE TABLE xl_blob (
    cl_blob_nbr  INTEGER NOT NULL,
    data         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by               INTEGER,
    creation_date            TIMESTAMP,
    effective_date           TIMESTAMP,
    active_record            CHAR(1)
);


CREATE TABLE xl_co_consolidation (
    cl_co_consolidation_nbr  INTEGER NOT NULL,
    description              VARCHAR(20) NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    primary_co_nbr           INTEGER,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    consolidation_type    CHAR(1) NOT NULL,
    scope                    CHAR(1) NOT NULL
);


CREATE TABLE xl_common_paymaster (
    cl_common_paymaster_nbr  INTEGER NOT NULL,
    cpm_name                 VARCHAR(40) NOT NULL,
    combine_medicare         CHAR(1) NOT NULL,
    combine_oasdi            CHAR(1) NOT NULL,
    combine_fui              CHAR(1) NOT NULL,
    combine_state_sui        CHAR(1) NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL
);


CREATE TABLE xl_delivery_group (
    cl_delivery_group_nbr           INTEGER NOT NULL,
    delivery_group_description      VARCHAR(20) NOT NULL,
    primary_cl_delivery_method_nbr  INTEGER NOT NULL,
    second_cl_delivery_method_nbr   INTEGER,
    stuff                           CHAR(1) NOT NULL,
    notes                           BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL
);


CREATE TABLE xl_delivery_method (
    cl_delivery_method_nbr        INTEGER NOT NULL,
    sb_delivery_company_svcs_nbr  INTEGER NOT NULL,
    name                          VARCHAR(40) NOT NULL,
    address1                      VARCHAR(30),
    address2                      VARCHAR(30),
    city                          VARCHAR(20),
    state                         VARCHAR(3),
    zip_code                      VARCHAR(10),
    attention                     VARCHAR(40) NOT NULL,
    phone_number                  VARCHAR(20),
    cover_sheet_sb_report_nbr     INTEGER,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL
);


CREATE TABLE xl_e_d_group_codes (
    cl_e_d_group_codes_nbr  INTEGER NOT NULL,
    cl_e_ds_nbr             INTEGER NOT NULL,
    cl_e_d_groups_nbr       INTEGER NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xl_e_d_groups (
    cl_e_d_groups_nbr    INTEGER NOT NULL,
    name                 VARCHAR(40) NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL,
    subtract_deductions  CHAR(1) NOT NULL
);


CREATE TABLE xl_e_d_local_exmpt_excld (
    cl_e_d_local_exmpt_excld_nbr  INTEGER NOT NULL,
    cl_e_ds_nbr                   INTEGER NOT NULL,
    sy_locals_nbr                 INTEGER NOT NULL,
    exempt_exclude                CHAR(1) NOT NULL,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL
);


CREATE TABLE xl_e_d_state_exmpt_excld (
    cl_e_d_state_exmpt_excld_nbr   INTEGER NOT NULL,
    cl_e_ds_nbr                    INTEGER NOT NULL,
    sy_states_nbr                  INTEGER NOT NULL,
    employee_exempt_exclude_state  CHAR(1) NOT NULL,
    employee_exempt_exclude_sdi    CHAR(1) NOT NULL,
    employee_exempt_exclude_sui    CHAR(1) NOT NULL,
    employee_state_or_type         CHAR(1) NOT NULL,
    employer_exempt_exclude_sdi    CHAR(1) NOT NULL,
    employer_exempt_exclude_sui    CHAR(1) NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    employee_state_or_value        NUMERIC(18,6)
);


CREATE TABLE xl_e_ds (
    cl_e_ds_nbr                      INTEGER NOT NULL,
    custom_e_d_code_number           VARCHAR(20) NOT NULL,
    e_d_code_type                    CHAR(2) NOT NULL,
    description                      VARCHAR(40) NOT NULL,
    cl_e_d_groups_nbr                INTEGER,
    skip_hours                       CHAR(1) NOT NULL,
    override_ee_rate_number          INTEGER,
    override_fed_tax_type            CHAR(1),
    update_hours                     CHAR(1) NOT NULL,
    default_cl_agency_nbr            INTEGER,
    ee_exempt_exclude_oasdi          CHAR(1) NOT NULL,
    ee_exempt_exclude_medicare       CHAR(1) NOT NULL,
    ee_exempt_exclude_federal        CHAR(1) NOT NULL,
    ee_exempt_exclude_eic            CHAR(1) NOT NULL,
    er_exempt_exclude_oasdi          CHAR(1) NOT NULL,
    er_exempt_exclude_medicare       CHAR(1) NOT NULL,
    er_exempt_exclude_fui            CHAR(1) NOT NULL,
    w2_box                           VARCHAR(12),
    min_cl_e_d_groups_nbr            INTEGER,
    max_cl_e_d_groups_nbr            INTEGER,
    cl_union_nbr                     INTEGER,
    cl_pension_nbr                   INTEGER,
    priority_to_exclude              INTEGER,
    make_up_deduction_shortfall      CHAR(1) NOT NULL,
    ot_all_cl_e_d_groups_nbr         INTEGER,
    piece_cl_e_d_groups_nbr          INTEGER,
    pw_min_wage_make_up_method       CHAR(1) NOT NULL,
    tip_minimum_wage_make_up_type    CHAR(1) NOT NULL,
    offset_cl_e_ds_nbr               INTEGER,
    cl_3_party_sick_pay_admin_nbr    INTEGER,
    show_ytd_on_checks               CHAR(1) NOT NULL,
    scheduled_defaults               CHAR(1) NOT NULL,
    sd_frequency                     CHAR(1) NOT NULL,
    sd_effective_start_date          TIMESTAMP,
    sd_calculation_method            CHAR(1) NOT NULL,
    sd_expression                    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    sd_auto                          CHAR(1) NOT NULL,
    sd_round                         CHAR(1) NOT NULL,
    sd_exclude_week_1                CHAR(1) NOT NULL,
    sd_exclude_week_2                CHAR(1) NOT NULL,
    sd_exclude_week_3                CHAR(1) NOT NULL,
    sd_exclude_week_4                CHAR(1) NOT NULL,
    sd_exclude_week_5                CHAR(1) NOT NULL,
    filler                           VARCHAR(512),
    changed_by                       INTEGER NOT NULL,
    creation_date                    TIMESTAMP NOT NULL,
    override_rate_type               CHAR(1) NOT NULL,
    show_on_input_worksheet          CHAR(1) NOT NULL,
    sy_state_nbr                     INTEGER,
    gl_offset_tag                    VARCHAR(20),
    show_ed_on_checks                CHAR(1) NOT NULL,
    effective_date                   TIMESTAMP NOT NULL,
    active_record                    CHAR(1) NOT NULL,
    notes                            BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    override_rate                    NUMERIC(18,6),
    override_fed_tax_value           NUMERIC(18,6),
    pay_period_minimum_amount        NUMERIC(18,6),
    pay_period_minimum_percent_of    NUMERIC(18,6),
    pay_period_maximum_amount        NUMERIC(18,6),
    pay_period_maximum_percent_of    NUMERIC(18,6),
    annual_maximum                   NUMERIC(18,6),
    match_first_percentage           NUMERIC(18,6),
    match_first_amount               NUMERIC(18,6),
    match_second_percentage          NUMERIC(18,6),
    match_second_amount              NUMERIC(18,6),
    flat_match_amount                NUMERIC(18,6),
    regular_ot_rate                  NUMERIC(18,6),
    piecework_minimum_wage           NUMERIC(18,6),
    sd_amount                        NUMERIC(18,6),
    sd_rate                          NUMERIC(18,6),
    apply_before_taxes               CHAR(1) NOT NULL,
    default_hours                    NUMERIC(18,6),
    show_which_balance               CHAR(1) NOT NULL,
    overstate_hours_for_overtime     CHAR(1) NOT NULL,
    which_checks                     CHAR(1) NOT NULL,
    month_number                     CHAR(1) NOT NULL,
    sd_plan_type                     CHAR(1) NOT NULL,
    sd_which_checks                  CHAR(1) NOT NULL,
    sd_priority_number               INTEGER,
    sd_always_pay                    CHAR(1) NOT NULL,
    sd_deductions_to_zero            CHAR(1) NOT NULL,
    sd_max_avg_amt_grp_nbr           INTEGER,
    sd_max_avg_hrs_grp_nbr           INTEGER,
    sd_max_average_hourly_wage_rate  NUMERIC(18,6),
    sd_threshold_e_d_groups_nbr      INTEGER,
    sd_threshold_amount              NUMERIC(18,6),
    sd_use_pension_limit             CHAR(1) NOT NULL,
    sd_deduct_whole_check            CHAR(1) NOT NULL,
    target_description               VARCHAR(6),
    cobra_eligible                   CHAR(1) NOT NULL,
    show_on_report                   CHAR(1) NOT NULL
);


CREATE TABLE xl_funds (
    cl_funds_nbr    INTEGER NOT NULL,
    cl_pension_nbr  INTEGER NOT NULL,
    name            VARCHAR(40) NOT NULL,
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL
);


CREATE TABLE xl_hr_course (
    cl_hr_course_nbr  INTEGER NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    changed_by        INTEGER NOT NULL,
    active_record     CHAR(1) NOT NULL,
    name              VARCHAR(40),
    renewal_status    CHAR(1) NOT NULL
);


CREATE TABLE xl_hr_person_education (
    cl_hr_person_education_nbr  INTEGER NOT NULL,
    cl_person_nbr               INTEGER NOT NULL,
    degree_level                CHAR(1),
    major                       VARCHAR(30),
    start_date                  DATE,
    end_date                    DATE,
    graduation_date             DATE,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    cl_hr_school_nbr            INTEGER NOT NULL,
    notes                       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    gpa                         NUMERIC(18,6)
);


CREATE TABLE xl_hr_person_handicaps (
    cl_hr_person_handicaps_nbr  INTEGER NOT NULL,
    cl_person_nbr               INTEGER NOT NULL,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    sy_hr_handicaps_nbr         INTEGER NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL
);


CREATE TABLE xl_hr_person_skills (
    cl_hr_person_skills_nbr  INTEGER NOT NULL,
    cl_person_nbr            INTEGER NOT NULL,
    cl_hr_skills_nbr         INTEGER NOT NULL,
    notes                    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    years_experience         NUMERIC(18,6)
);


CREATE TABLE xl_hr_reason_codes (
    cl_hr_reason_codes_nbr  INTEGER NOT NULL,
    name                    VARCHAR(40) NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xl_hr_school (
    cl_hr_school_nbr  INTEGER NOT NULL,
    name              VARCHAR(40) NOT NULL,
    address1          VARCHAR(30),
    address2          VARCHAR(30),
    city              VARCHAR(20),
    state             VARCHAR(3),
    zip_code          VARCHAR(10),
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL
);


CREATE TABLE xl_hr_skills (
    cl_hr_skills_nbr  INTEGER NOT NULL,
    description       VARCHAR(40) NOT NULL,
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL
);


CREATE TABLE xl_mail_box_group (
    cl_mail_box_group_nbr        INTEGER NOT NULL,
    description                  VARCHAR(40) NOT NULL,
    required                     CHAR(1) NOT NULL,
    notification_email           VARCHAR(80),
    sb_cover_letter_report_nbr   INTEGER,
    sy_cover_letter_report_nbr   INTEGER,
    sec_method_active_from       DATE,
    sec_method_active_to         DATE,
    prim_sb_delivery_method_nbr  INTEGER,
    sec_sb_delivery_method_nbr   INTEGER,
    prim_sb_media_type_nbr       INTEGER,
    sec_sb_media_type_nbr        INTEGER,
    up_level_mail_box_group_nbr  INTEGER,
    auto_release_type            CHAR(1) NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    note                         VARCHAR(512)
);


CREATE TABLE xl_mail_box_group_option (
    cl_mail_box_group_option_nbr  INTEGER NOT NULL,
    cl_mail_box_group_nbr         INTEGER NOT NULL,
    option_tag                    VARCHAR(40) NOT NULL,
    option_integer_value          INTEGER,
    option_string_value           VARCHAR(512),
    effective_date                TIMESTAMP NOT NULL,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL
);


CREATE TABLE xl_pension (
    cl_pension_nbr            INTEGER NOT NULL,
    plan_id                   VARCHAR(40),
    policy_id                 VARCHAR(40),
    name                      VARCHAR(40) NOT NULL,
    cl_agency_nbr             INTEGER,
    stop_when_ee_reaches_max  CHAR(1) NOT NULL,
    mark_pension_on_w2        CHAR(1) NOT NULL,
    probation_period_days     INTEGER,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    cl_e_d_groups_nbr         INTEGER,
    pension_type              CHAR(1) NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL,
    max_allowed_percentage    NUMERIC(18,6),
    minimum_age               NUMERIC(18,6)
);


CREATE TABLE xl_person (
    cl_person_nbr                   INTEGER NOT NULL,
    social_security_number          VARCHAR(11) NOT NULL,
    ein_or_social_security_number   CHAR(1) NOT NULL,
    first_name                      VARCHAR(20) NOT NULL,
    middle_initial                  CHAR(1),
    last_name                       VARCHAR(30) NOT NULL,
    w2_first_name                   VARCHAR(20),
    w2_middle_name                  VARCHAR(20),
    w2_last_name                    VARCHAR(30),
    w2_name_suffix                  VARCHAR(10),
    address1                        VARCHAR(30) NOT NULL,
    address2                        VARCHAR(30),
    city                            VARCHAR(20) NOT NULL,
    state                           CHAR(2) NOT NULL,
    zip_code                        VARCHAR(10) NOT NULL,
    county                          VARCHAR(10),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    phone3                          VARCHAR(20),
    description3                    VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    birth_date                      DATE,
    gender                          CHAR(1) NOT NULL,
    ethnicity                       CHAR(1) NOT NULL,
    smoker                          CHAR(1) NOT NULL,
    drivers_license_number          VARCHAR(20),
    drivers_license_exp_date        DATE,
    auto_insurance_carrier          VARCHAR(30),
    auto_insurance_policy_number    VARCHAR(30),
    auto_insurance_policy_exp_date  DATE,
    visa_number                     VARCHAR(15),
    veteran                         CHAR(1) NOT NULL,
    veteran_discharge_date          DATE,
    vietnam_veteran                 CHAR(1) NOT NULL,
    disabled_veteran                CHAR(1) NOT NULL,
    military_reserve                CHAR(1) NOT NULL,
    i9_on_file                      CHAR(1) NOT NULL,
    picture                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    notes                           BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    country                         VARCHAR(40),
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    sy_hr_ethnicity_nbr             INTEGER,
    citizenship                     VARCHAR(30),
    visa_type                       CHAR(1) NOT NULL,
    visa_expiration_date            DATE,
    web_password                    VARCHAR(20),
    residential_state_nbr           INTEGER NOT NULL,
    tribe                           VARCHAR(15),
    reliable_car                    CHAR(1) NOT NULL,
    service_medal_veteran           CHAR(1) NOT NULL,
    other_protected_veteran         CHAR(1) NOT NULL,
    native_language                 CHAR(1) NOT NULL
);


CREATE TABLE xl_person_dependents (
    cl_person_dependents_nbr  INTEGER NOT NULL,
    cl_person_nbr             INTEGER NOT NULL,
    last_name                 VARCHAR(30) NOT NULL,
    first_name                VARCHAR(20) NOT NULL,
    middle_initial            CHAR(1),
    social_security_number    VARCHAR(11),
    relation_type             CHAR(1) NOT NULL,
    gender                    CHAR(1) NOT NULL,
    date_of_birth             DATE,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL,
    address1                  VARCHAR(30),
    address2                  VARCHAR(30),
    city                      VARCHAR(20),
    state                     CHAR(2),
    zip_code                  VARCHAR(10),
    full_time_student         CHAR(1) NOT NULL,
    existing_patient          CHAR(1) NOT NULL,
    pcp                       VARCHAR(40),
    person_type               CHAR(1) NOT NULL
);


CREATE TABLE xl_person_documents (
    cl_person_documents_nbr  INTEGER NOT NULL,
    cl_person_nbr            INTEGER NOT NULL,
    name                     VARCHAR(40) NOT NULL,
    document                 BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    file_name                VARCHAR(40),
    begin_date               TIMESTAMP,
    end_date                 TIMESTAMP
);


CREATE TABLE xl_pieces (
    cl_pieces_nbr     INTEGER NOT NULL,
    name              VARCHAR(40),
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL,
    default_rate      NUMERIC(18,6),
    default_quantity  NUMERIC(18,6)
);


CREATE TABLE xl_report_writer_reports (
    cl_report_writer_reports_nbr  INTEGER NOT NULL,
    report_description            VARCHAR(40) NOT NULL,
    report_type                   CHAR(1) NOT NULL,
    report_file                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    media_type                    CHAR(1) NOT NULL,
    class_name                    VARCHAR(40),
    ancestor_class_name           VARCHAR(40)
);


CREATE TABLE xl_timeclock_imports (
    cl_timeclock_imports_nbr  INTEGER NOT NULL,
    description               VARCHAR(20) NOT NULL,
    timeclock_type            CHAR(1) NOT NULL,
    hardware_type             CHAR(1) NOT NULL,
    connection_type           CHAR(1) NOT NULL,
    sb_supported              CHAR(1) NOT NULL,
    scheduled_install_date    DATE,
    actual_install_date       DATE,
    installer                 VARCHAR(20),
    date_lease_ends           DATE,
    payroll_import_format     CHAR(1) NOT NULL,
    payroll_export_format     CHAR(1) NOT NULL,
    notes                     BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    export_file_path          VARCHAR(80),
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xl_union (
    cl_union_nbr        INTEGER NOT NULL,
    cl_agency_nbr       INTEGER NOT NULL,
    name                VARCHAR(40) NOT NULL,
    address1            VARCHAR(30),
    address2            VARCHAR(30),
    city                VARCHAR(20),
    state               CHAR(2),
    zip_code            VARCHAR(10),
    phone               VARCHAR(20),
    contact             VARCHAR(30),
    flat_or_percentage  CHAR(1) NOT NULL,
    cl_e_d_groups_nbr   INTEGER,
    frequency           CHAR(1) NOT NULL,
    exclude_extra_week  CHAR(1) NOT NULL,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL,
    cap_amount          NUMERIC(18,6)
);


CREATE TABLE xl_union_dues (
    cl_union_dues_nbr  INTEGER NOT NULL,
    cl_union_nbr       INTEGER NOT NULL,
    next_begin_date    TIMESTAMP,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL,
    minimum_rate       NUMERIC(18,6),
    next_minimum_rate  NUMERIC(18,6),
    maximum_rate       NUMERIC(18,6),
    next_maximum_rate  NUMERIC(18,6),
    due_amount         NUMERIC(18,6),
    next_due_amount    NUMERIC(18,6)
);


CREATE TABLE xo (
    co_nbr                          INTEGER NOT NULL,
    custom_company_number           VARCHAR(20) NOT NULL,
    name                            VARCHAR(40) NOT NULL,
    dba                             VARCHAR(40),
    address1                        VARCHAR(30) NOT NULL,
    address2                        VARCHAR(30),
    city                            VARCHAR(20) NOT NULL,
    state                           CHAR(2) NOT NULL,
    zip_code                        VARCHAR(10) NOT NULL,
    county                          VARCHAR(10),
    legal_name                      VARCHAR(40),
    legal_address1                  VARCHAR(30),
    legal_address2                  VARCHAR(30),
    legal_city                      VARCHAR(20),
    legal_state                     CHAR(2),
    legal_zip_code                  VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    referred_by                     VARCHAR(40),
    sb_referrals_nbr                INTEGER,
    start_date                      DATE NOT NULL,
    termination_date                DATE,
    termination_code                CHAR(1) NOT NULL,
    termination_notes               BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    sb_accountant_nbr               INTEGER,
    accountant_contact              VARCHAR(40),
    print_cpa                       CHAR(1) NOT NULL,
    union_cl_e_ds_nbr               INTEGER,
    cl_common_paymaster_nbr         INTEGER,
    cl_co_consolidation_nbr         INTEGER,
    fein                            VARCHAR(9) NOT NULL,
    remote                          CHAR(1) NOT NULL,
    business_start_date             DATE NOT NULL,
    business_type                   CHAR(1),
    corporation_type                CHAR(1) NOT NULL,
    nature_of_business              BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    restaurant                      CHAR(1) NOT NULL,
    days_open                       INTEGER,
    time_open                       TIMESTAMP,
    time_close                      TIMESTAMP,
    company_notes                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    successor_company               CHAR(1) NOT NULL,
    medical_plan                    CHAR(1) NOT NULL,
    retirement_age                  INTEGER,
    pay_frequencies                 CHAR(1),
    general_ledger_format_string    VARCHAR(30),
    cl_billing_nbr                  INTEGER NOT NULL,
    dbdt_level                      CHAR(1) NOT NULL,
    billing_level                   CHAR(1) NOT NULL,
    bank_account_level              CHAR(1) NOT NULL,
    billing_sb_bank_account_nbr     INTEGER,
    tax_sb_bank_account_nbr         INTEGER,
    payroll_cl_bank_account_nbr     INTEGER NOT NULL,
    debit_number_days_prior_pr      INTEGER NOT NULL,
    tax_cl_bank_account_nbr         INTEGER NOT NULL,
    debit_number_of_days_prior_tax  INTEGER,
    billing_cl_bank_account_nbr     INTEGER NOT NULL,
    debit_number_days_prior_bill    INTEGER,
    dd_cl_bank_account_nbr          INTEGER NOT NULL,
    debit_number_of_days_prior_dd   INTEGER,
    home_co_states_nbr              INTEGER,
    home_sdi_co_states_nbr          INTEGER,
    home_sui_co_states_nbr          INTEGER,
    tax_service                     CHAR(1) NOT NULL,
    tax_service_start_date          DATE,
    tax_service_end_date            DATE,
    use_dba_on_tax_return           CHAR(1) NOT NULL,
    workers_comp_cl_agency_nbr      INTEGER,
    workers_comp_policy_id          VARCHAR(40),
    w_comp_cl_bank_account_nbr      INTEGER,
    debit_number_days_prior_wc      INTEGER,
    w_comp_sb_bank_account_nbr      INTEGER,
    eftps_enrollment_status         CHAR(1),
    eftps_enrollment_date           TIMESTAMP,
    eftps_sequence_number           VARCHAR(4),
    eftps_enrollment_number         VARCHAR(4),
    eftps_name                      VARCHAR(40),
    pin_number                      VARCHAR(10),
    ach_sb_bank_account_nbr         INTEGER,
    trust_service                   CHAR(1) NOT NULL,
    trust_sb_account_nbr            INTEGER,
    trust_service_start_date        DATE,
    trust_service_end_date          DATE,
    obc                             CHAR(1) NOT NULL,
    sb_obc_account_nbr              INTEGER,
    obc_start_date                  DATE,
    obc_end_date                    DATE,
    sy_fed_reporting_agency_nbr     INTEGER,
    sy_fed_tax_payment_agency_nbr   INTEGER,
    federal_tax_deposit_frequency   CHAR(1) NOT NULL,
    federal_tax_transfer_method     CHAR(1) NOT NULL,
    federal_tax_payment_method      CHAR(1) NOT NULL,
    fui_sy_tax_payment_agency       INTEGER,
    fui_sy_tax_report_agency        INTEGER,
    external_tax_export             CHAR(1) NOT NULL,
    non_profit                      CHAR(1) NOT NULL,
    federal_tax_exempt_status       CHAR(1) NOT NULL,
    fed_tax_exempt_ee_oasdi         CHAR(1) NOT NULL,
    fed_tax_exempt_er_oasdi         CHAR(1) NOT NULL,
    fed_tax_exempt_ee_medicare      CHAR(1) NOT NULL,
    fed_tax_exempt_er_medicare      CHAR(1) NOT NULL,
    fui_tax_deposit_frequency       CHAR(1) NOT NULL,
    fui_tax_exempt                  CHAR(1) NOT NULL,
    primary_sort_field              CHAR(1) NOT NULL,
    secondary_sort_field            CHAR(1) NOT NULL,
    fiscal_year_end                 INTEGER,
    third_party_in_gross_pr_report  CHAR(1) NOT NULL,
    sic_code                        INTEGER,
    deductions_to_zero              CHAR(1) NOT NULL,
    autopay_company                 CHAR(1) NOT NULL,
    pay_frequency_hourly_default    CHAR(1) NOT NULL,
    pay_frequency_salary_default    CHAR(1) NOT NULL,
    co_check_primary_sort           CHAR(1) NOT NULL,
    co_check_secondary_sort         CHAR(1) NOT NULL,
    cl_delivery_group_nbr           INTEGER,
    annual_cl_delivery_group_nbr    INTEGER,
    quarter_cl_delivery_group_nbr   INTEGER,
    smoker_default                  CHAR(1) NOT NULL,
    auto_labor_dist_show_deducts    CHAR(1) NOT NULL,
    auto_labor_dist_level           CHAR(1) NOT NULL,
    distribute_deductions_default   CHAR(1) NOT NULL,
    co_workers_comp_nbr             INTEGER,
    withholding_default             CHAR(1) NOT NULL,
    apply_misc_limit_to_1099        CHAR(1) NOT NULL,
    check_type                      CHAR(1) NOT NULL,
    make_up_tax_deduct_shortfalls   CHAR(1) NOT NULL,
    show_shifts_on_check            CHAR(1) NOT NULL,
    show_ytd_on_check               CHAR(1) NOT NULL,
    show_ein_number_on_check        CHAR(1) NOT NULL,
    show_ss_number_on_check         CHAR(1) NOT NULL,
    time_off_accrual                CHAR(1) NOT NULL,
    check_form                      CHAR(1) NOT NULL,
    print_manual_check_stubs        CHAR(1) NOT NULL,
    credit_hold                     CHAR(1) NOT NULL,
    cl_timeclock_imports_nbr        INTEGER,
    payroll_password                VARCHAR(10),
    remote_of_client                CHAR(1) NOT NULL,
    hardware_o_s                    CHAR(1) NOT NULL,
    network                         CHAR(1) NOT NULL,
    modem_speed                     CHAR(1) NOT NULL,
    modem_connection_type           CHAR(1) NOT NULL,
    network_administrator           VARCHAR(40),
    network_administrator_phone     VARCHAR(20),
    network_admin_phone_type        CHAR(1) NOT NULL,
    external_network_administrator  CHAR(1) NOT NULL,
    transmission_destination        INTEGER,
    last_call_in_date               TIMESTAMP,
    setup_completed                 CHAR(1) NOT NULL,
    first_monthly_payroll_day       INTEGER,
    second_monthly_payroll_day      INTEGER,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    collate_checks                  CHAR(1) NOT NULL,
    process_priority                INTEGER NOT NULL,
    check_message                   VARCHAR(40),
    customer_service_sb_user_nbr    INTEGER,
    invoice_notes                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    tax_cover_letter_notes          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    final_tax_return                CHAR(1) NOT NULL,
    general_ledger_tag              VARCHAR(20),
    billing_general_ledger_tag      VARCHAR(20),
    federal_general_ledger_tag      VARCHAR(20),
    ee_oasdi_general_ledger_tag     VARCHAR(20),
    er_oasdi_general_ledger_tag     VARCHAR(20),
    ee_medicare_general_ledger_tag  VARCHAR(20),
    er_medicare_general_ledger_tag  VARCHAR(20),
    fui_general_ledger_tag          VARCHAR(20),
    eic_general_ledger_tag          VARCHAR(20),
    backup_w_general_ledger_tag     VARCHAR(20),
    net_pay_general_ledger_tag      VARCHAR(20),
    tax_imp_general_ledger_tag      VARCHAR(20),
    trust_imp_general_ledger_tag    VARCHAR(20),
    thrd_p_tax_general_ledger_tag   VARCHAR(20),
    thrd_p_chk_general_ledger_tag   VARCHAR(20),
    trust_chk_general_ledger_tag    VARCHAR(20),
    federal_offset_gl_tag           VARCHAR(20),
    ee_oasdi_offset_gl_tag          VARCHAR(20),
    er_oasdi_offset_gl_tag          VARCHAR(20),
    ee_medicare_offset_gl_tag       VARCHAR(20),
    er_medicare_offset_gl_tag       VARCHAR(20),
    fui_offset_gl_tag               VARCHAR(20),
    eic_offset_gl_tag               VARCHAR(20),
    backup_w_offset_gl_tag          VARCHAR(20),
    trust_imp_offset_gl_tag         VARCHAR(20),
    billing_exp_gl_tag              VARCHAR(20),
    er_oasdi_exp_gl_tag             VARCHAR(20),
    er_medicare_exp_gl_tag          VARCHAR(20),
    auto_enlist                     CHAR(1) NOT NULL,
    calculate_locals_first          CHAR(1) NOT NULL,
    weekend_action                  CHAR(1) NOT NULL,
    last_fui_correction             TIMESTAMP,
    last_sui_correction             TIMESTAMP,
    last_quarter_end_correction     TIMESTAMP,
    last_preprocess                 TIMESTAMP,
    last_preprocess_message         VARCHAR(80),
    charge_cobra_admin_fee          CHAR(1) NOT NULL,
    cobra_fee_day_of_month_due      INTEGER,
    cobra_notification_days         INTEGER,
    cobra_eligibility_confirm_days  INTEGER,
    show_rates_on_checks            CHAR(1) NOT NULL,
    show_dir_dep_nbr_on_checks      CHAR(1) NOT NULL,
    fed_943_tax_deposit_frequency   CHAR(1) NOT NULL,
    fed_945_tax_deposit_frequency   CHAR(1) NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    impound_workers_comp            CHAR(1) NOT NULL,
    reverse_check_printing          CHAR(1) NOT NULL,
    lock_date                       DATE,
    fui_rate_override               NUMERIC(18,6),
    auto_increment                  NUMERIC(18,6),
    maximum_group_term_life         NUMERIC(18,6),
    payrate_precision               NUMERIC(18,6),
    average_hours                   NUMERIC(18,6),
    maximum_hours_on_check          NUMERIC(18,6),
    maximum_dollars_on_check        NUMERIC(18,6),
    discount_rate                   NUMERIC(18,6),
    mod_rate                        NUMERIC(18,6),
    minimum_tax_threshold           NUMERIC(18,6),
    ss_disability_admin_fee         NUMERIC(18,6),
    check_time_off_avail            CHAR(1) NOT NULL,
    agency_check_mb_group_nbr       INTEGER,
    pr_check_mb_group_nbr           INTEGER,
    pr_report_mb_group_nbr          INTEGER,
    pr_report_second_mb_group_nbr   INTEGER,
    tax_check_mb_group_nbr          INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    tax_return_mb_group_nbr         INTEGER,
    tax_return_second_mb_group_nbr  INTEGER,
    name_on_invoice                 CHAR(1) NOT NULL,
    misc_check_form                 CHAR(1) NOT NULL,
    hold_return_queue               CHAR(1) NOT NULL,
    number_of_invoice_copies        INTEGER,
    prorate_flat_fee_for_dbdt       CHAR(1) NOT NULL,
    initial_effective_date          DATE NOT NULL,
    sb_other_service_nbr            INTEGER,
    break_checks_by_dbdt            CHAR(1) NOT NULL,
    summarize_sui                   CHAR(1) NOT NULL,
    show_shortfall_check            CHAR(1) NOT NULL,
    trust_chk_offset_gl_tag         VARCHAR(20),
    manual_cl_bank_account_nbr      INTEGER,
    wells_fargo_ach_flag            VARCHAR(4),
    billing_check_cpa               CHAR(1) NOT NULL,
    sales_tax_percentage            NUMERIC(18,6),
    exclude_r_c_b_0r_n              CHAR(1) NOT NULL,
    calculate_states_first          CHAR(1) NOT NULL,
    invoice_discount                NUMERIC(18,6),
    discount_start_date             DATE,
    discount_end_date               DATE,
    show_time_clock_punch           CHAR(1) NOT NULL,
    auto_rd_dflt_cl_e_d_groups_nbr  INTEGER,
    auto_reduction_default_ees      CHAR(1) NOT NULL,
    vt_healthcare_gl_tag            VARCHAR(20),
    vt_healthcare_offset_gl_tag     VARCHAR(20),
    ui_rounding_gl_tag              VARCHAR(20),
    ui_rounding_offset_gl_tag       VARCHAR(20),
    reprint_to_balance              CHAR(1) NOT NULL,
    show_manual_checks_in_ess       CHAR(1) NOT NULL,
    payroll_requires_mgr_approval   CHAR(1) NOT NULL,
    days_prior_to_check_date        INTEGER,
    wc_offs_bank_account_nbr        INTEGER,
    qtr_lock_for_tax_pmts           CHAR(1) NOT NULL,
    co_max_amount_for_payroll       NUMERIC(18,6),
    co_max_amount_for_tax_impound   NUMERIC(18,6),
    co_max_amount_for_dd            NUMERIC(18,6),
    co_payroll_process_limitations  CHAR(1) NOT NULL,
    co_ach_process_limitations      CHAR(1) NOT NULL,
    trust_impound                   CHAR(1) NOT NULL,
    tax_impound                     CHAR(1) NOT NULL,
    dd_impound                      CHAR(1) NOT NULL,
    billing_impound                 CHAR(1) NOT NULL,
    wc_impound                      CHAR(1) NOT NULL,
    cobra_credit_gl_tag             VARCHAR(20),
    co_exception_payment_type       CHAR(1) NOT NULL,
    last_tax_return                 CHAR(1) NOT NULL,
    enable_hr                       CHAR(1) NOT NULL,
    enable_ess                      CHAR(1) NOT NULL,
    duns_and_bradstreet             VARCHAR(10),
    bank_account_register_name      VARCHAR(15),
    enable_benefits                 CHAR(1) NOT NULL,
    enable_time_off                 CHAR(1) NOT NULL,
    employer_type                   CHAR(1) NOT NULL,
    wc_fiscal_year_begin            DATE,
    show_paystubs_ess_days          INTEGER,
    co_locations_nbr                INTEGER
);


CREATE TABLE xo_additional_info (
    co_additional_info_nbr         INTEGER NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    changed_by                     INTEGER NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    co_nbr                         INTEGER NOT NULL,
    co_additional_info_names_nbr   INTEGER NOT NULL,
    info_string                    VARCHAR(40),
    info_date                      DATE,
    info_amount                    NUMERIC(18,6),
    co_additional_info_values_nbr  INTEGER,
    info_blob                      INTEGER
);


CREATE TABLE xo_additional_info_names (
    co_additional_info_names_nbr  INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    name                          VARCHAR(40) NOT NULL,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    data_type                     CHAR(1) NOT NULL,
    category                      CHAR(1) NOT NULL,
    seq_num                       SMALLINT NOT NULL
);


CREATE TABLE xo_additional_info_values (
    co_additional_info_values_nbr  INTEGER NOT NULL,
    co_additional_info_names_nbr   INTEGER NOT NULL,
    co_nbr                         INTEGER NOT NULL,
    info_value                     VARCHAR(40) NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL
);


CREATE TABLE xo_auto_enlist_returns (
    co_auto_enlist_returns_nbr  INTEGER NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    changed_by                  INTEGER NOT NULL,
    active_record               CHAR(1) NOT NULL,
    co_nbr                      INTEGER NOT NULL,
    sy_gl_agency_report_nbr     INTEGER NOT NULL,
    process_type                CHAR(1) NOT NULL
);


CREATE TABLE xo_bank_acc_reg_details (
    co_bank_acc_reg_details_nbr   INTEGER NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    changed_by                    INTEGER NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    co_bank_account_register_nbr  INTEGER NOT NULL,
    cl_nbr                        INTEGER NOT NULL,
    client_co_bank_acc_reg_nbr    INTEGER NOT NULL
);


CREATE TABLE xo_bank_account_register (
    co_bank_account_register_nbr  INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    sb_bank_accounts_nbr          INTEGER NOT NULL,
    pr_check_nbr                  INTEGER,
    check_serial_number           INTEGER,
    check_date                    DATE NOT NULL,
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    manual_type                   CHAR(1) NOT NULL,
    co_manual_ach_nbr             INTEGER,
    co_pr_ach_nbr                 INTEGER,
    co_tax_payment_ach_nbr        INTEGER,
    process_date                  TIMESTAMP,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                        VARCHAR(512),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    pr_miscellaneous_checks_nbr   INTEGER,
    close_date                    DATE,
    co_tax_deposits_nbr           INTEGER,
    register_type                 CHAR(1) NOT NULL,
    cleared_date                  DATE,
    transaction_effective_date    TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    amount                        NUMERIC(18,6),
    cleared_amount                NUMERIC(18,6),
    pr_nbr                        INTEGER,
    group_identifier              VARCHAR(15)
);


CREATE TABLE xo_batch_local_or_temps (
    co_batch_local_or_temps_nbr  INTEGER NOT NULL,
    co_local_tax_nbr             INTEGER NOT NULL,
    exclude_local                CHAR(1) NOT NULL,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    co_pr_check_templates_nbr    INTEGER NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    override_amount              NUMERIC(18,6)
);


CREATE TABLE xo_batch_states_or_temps (
    co_batch_states_or_temps_nbr  INTEGER NOT NULL,
    co_states_nbr                 INTEGER NOT NULL,
    tax_at_supplemental_rate      CHAR(1) NOT NULL,
    exclude_state                 CHAR(1) NOT NULL,
    exclude_sdi                   CHAR(1) NOT NULL,
    exclude_sui                   CHAR(1) NOT NULL,
    state_override_type           CHAR(1) NOT NULL,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    co_pr_check_templates_nbr     INTEGER NOT NULL,
    exclude_additional_state      CHAR(1) NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    state_override_value          NUMERIC(18,6)
);


CREATE TABLE xo_benefit_category (
    co_benefit_category_nbr      INTEGER NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    changed_by                   INTEGER NOT NULL,
    active_record                CHAR(1) NOT NULL,
    category_type                CHAR(1) NOT NULL,
    enrollment_notify_hr_days    SMALLINT,
    enrollment_notify_ee_days    SMALLINT,
    enrollment_reminder_hr_days  SMALLINT,
    enrollment_reminder_ee_days  SMALLINT,
    remind_hr_days               SMALLINT,
    remind_ee_days               SMALLINT,
    notes                        BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    co_nbr                       INTEGER NOT NULL,
    enrollment_frequency         CHAR(1) NOT NULL,
    enrollment_start_date        DATE NOT NULL,
    enrollment_end_date          DATE NOT NULL,
    read_only                    CHAR(1) NOT NULL
);


CREATE TABLE xo_benefit_discount (
    co_benefit_discount_nbr  INTEGER NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    active_record            CHAR(1) NOT NULL,
    co_nbr                   INTEGER NOT NULL,
    description              VARCHAR(40) NOT NULL,
    amount                   NUMERIC(18,6) NOT NULL,
    amount_type              CHAR(1) NOT NULL
);


CREATE TABLE xo_benefit_package (
    co_benefit_package_nbr  INTEGER NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    changed_by              INTEGER NOT NULL,
    active_record           CHAR(1) NOT NULL,
    co_nbr                  INTEGER NOT NULL,
    name                    VARCHAR(40),
    cafeteria_limit         NUMERIC(18,6),
    cafeteria_frequency     CHAR(1) NOT NULL,
    payout_flag             CHAR(1) NOT NULL,
    payout_ed_code_nbr      INTEGER
);


CREATE TABLE xo_benefit_pkg_asmnt (
    co_benefit_pkg_asmnt_nbr  INTEGER NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    changed_by                INTEGER NOT NULL,
    active_record             CHAR(1) NOT NULL,
    co_benefit_package_nbr    INTEGER NOT NULL,
    co_division_nbr           INTEGER,
    co_branch_nbr             INTEGER,
    co_department_nbr         INTEGER,
    co_team_nbr               INTEGER,
    co_group_nbr              INTEGER
);


CREATE TABLE xo_benefit_pkg_detail (
    co_benefit_pkg_detail_nbr  INTEGER NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    changed_by                 INTEGER NOT NULL,
    active_record              CHAR(1) NOT NULL,
    co_benefit_package_nbr     INTEGER NOT NULL,
    co_benefits_nbr            INTEGER NOT NULL
);


CREATE TABLE xo_benefit_providers (
    co_benefit_providers_nbr  INTEGER NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    changed_by                INTEGER NOT NULL,
    active_record             CHAR(1) NOT NULL,
    co_nbr                    INTEGER NOT NULL,
    cl_agency_nbr             INTEGER,
    name                      VARCHAR(40) NOT NULL,
    description               BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    website                   VARCHAR(80)
);


CREATE TABLE xo_benefit_rates (
    co_benefit_rates_nbr    INTEGER NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    changed_by              INTEGER NOT NULL,
    active_record           CHAR(1) NOT NULL,
    co_benefit_subtype_nbr  INTEGER NOT NULL,
    ee_rate                 NUMERIC(18,6),
    er_rate                 NUMERIC(18,6),
    cobra_rate              NUMERIC(18,6),
    rate_type               CHAR(1) NOT NULL,
    cl_e_d_groups_nbr       INTEGER,
    max_dependents          SMALLINT,
    period_begin            DATE NOT NULL,
    period_end              DATE NOT NULL
);


CREATE TABLE xo_benefit_setup (
    co_benefit_setup_nbr       INTEGER NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    changed_by                 INTEGER NOT NULL,
    active_record              CHAR(1) NOT NULL,
    co_nbr                     INTEGER NOT NULL,
    ee_enroll_begin_subject    VARCHAR(40),
    ee_enroll_begin_text       BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    ee_enroll_summary_subject  VARCHAR(40),
    ee_enroll_summary_text     BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    ee_enroll_end_subject      VARCHAR(40),
    ee_enroll_end_text         BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    ee_enroll_event_subject    VARCHAR(40),
    ee_enroll_event_text       BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    ee_reminder_subject        VARCHAR(40),
    ee_reminder_text           BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    hr_enroll_begin_subject    VARCHAR(40),
    hr_enroll_begin_text       BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    hr_reminder_subject        VARCHAR(40),
    hr_reminder_text           BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    enroll_instructions        BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    enroll_certification       BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    providers_text             BLOB SUB_TYPE 1 SEGMENT SIZE 80
);


CREATE TABLE xo_benefit_states (
    co_benefit_states_nbr  INTEGER NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    changed_by             INTEGER NOT NULL,
    active_record          CHAR(1) NOT NULL,
    sy_states_nbr          INTEGER NOT NULL,
    co_benefits_nbr        INTEGER
);


CREATE TABLE xo_benefit_subtype (
    co_benefit_subtype_nbr  INTEGER NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    changed_by              INTEGER NOT NULL,
    active_record           CHAR(1) NOT NULL,
    co_benefits_nbr         INTEGER NOT NULL,
    description             VARCHAR(40)
);


CREATE TABLE xo_benefits (
    co_benefits_nbr             INTEGER NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    changed_by                  INTEGER NOT NULL,
    active_record               CHAR(1) NOT NULL,
    benefit_name                VARCHAR(40) NOT NULL,
    benefit_type                CHAR(1) NOT NULL,
    certificate_number          VARCHAR(40),
    group_number                VARCHAR(40),
    policy_number               VARCHAR(40),
    frequency                   CHAR(1) NOT NULL,
    taxable                     CHAR(1) NOT NULL,
    employee_type               CHAR(1) NOT NULL,
    round                       CHAR(1) NOT NULL,
    eligibility_waiting_period  INTEGER,
    expiration_date             DATE,
    minimum_amount              NUMERIC(18,6),
    maximum_amount              NUMERIC(18,6),
    minimum_age_requirement     NUMERIC(18,6),
    cl_agency_nbr               INTEGER,
    co_benefit_category_nbr     INTEGER,
    ee_deduction_nbr            INTEGER,
    er_deduction_nbr            INTEGER,
    ee_cobra_nbr                INTEGER,
    er_cobra_nbr                INTEGER,
    w2_flag                     CHAR(1) NOT NULL,
    co_nbr                      INTEGER NOT NULL,
    read_only                   CHAR(1) NOT NULL,
    allow_hsa                   CHAR(1) NOT NULL,
    requires_dob                CHAR(1) NOT NULL,
    requires_ssn                CHAR(1) NOT NULL,
    requires_pcp                CHAR(1) NOT NULL,
    show_rates                  CHAR(1) NOT NULL,
    apply_discount              CHAR(1) NOT NULL,
    display_cost_period         CHAR(1) NOT NULL,
    allow_ee_contribution       CHAR(1) NOT NULL,
    show_discount_rating        CHAR(1) NOT NULL,
    qual_event_eligible         CHAR(1) NOT NULL,
    qual_event_enroll_duration  SMALLINT,
    additional_info_title1      VARCHAR(40),
    additional_info_title2      VARCHAR(40),
    additional_info_title3      VARCHAR(40),
    ee_benefit                  CHAR(1) NOT NULL,
    requires_beneficiaries      CHAR(1) NOT NULL,
    cafeteria_plan              CHAR(1) NOT NULL,
    decline_amount              NUMERIC(18,6)
);


CREATE TABLE xo_billing_history (
    co_billing_history_nbr       INTEGER NOT NULL,
    invoice_number               INTEGER NOT NULL,
    co_nbr                       INTEGER NOT NULL,
    invoice_date                 TIMESTAMP NOT NULL,
    pr_nbr                       INTEGER,
    exported_to_a_r              CHAR(1) NOT NULL,
    check_serial_number          INTEGER,
    billing_co_bank_account_nbr  INTEGER,
    notes                        BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    status                       CHAR(1) NOT NULL,
    status_date                  TIMESTAMP NOT NULL,
    check_date                   DATE,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    sales_tax_amount             NUMERIC(18,6),
    co_division_nbr              INTEGER,
    co_branch_nbr                INTEGER,
    co_department_nbr            INTEGER,
    co_team_nbr                  INTEGER,
    filler                       VARCHAR(512)
);


CREATE TABLE xo_billing_history_detail (
    co_billing_history_detail_nbr  INTEGER NOT NULL,
    co_billing_history_nbr         INTEGER NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    sb_services_nbr                INTEGER,
    co_services_nbr                INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    quantity                       NUMERIC(18,6),
    amount                         NUMERIC(18,6),
    tax                            NUMERIC(18,6),
    discount                       NUMERIC(18,6)
);


CREATE TABLE xo_branch (
    co_branch_nbr                   INTEGER NOT NULL,
    co_division_nbr                 INTEGER NOT NULL,
    custom_branch_number            VARCHAR(20) NOT NULL,
    name                            VARCHAR(40),
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    contact1                        VARCHAR(30),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax                             VARCHAR(20),
    fax_description                 VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    home_co_states_nbr              INTEGER NOT NULL,
    cl_timeclock_imports_nbr        INTEGER,
    pay_frequency_hourly            CHAR(1) NOT NULL,
    pay_frequency_salary            CHAR(1) NOT NULL,
    print_branch_address_on_check   CHAR(1) NOT NULL,
    payroll_cl_bank_account_nbr     INTEGER,
    cl_billing_nbr                  INTEGER,
    billing_cl_bank_account_nbr     INTEGER,
    tax_cl_bank_account_nbr         INTEGER,
    dd_cl_bank_account_nbr          INTEGER,
    co_workers_comp_nbr             INTEGER,
    union_cl_e_ds_nbr               INTEGER,
    override_ee_rate_number         INTEGER,
    branch_notes                    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    home_state_type                 CHAR(1) NOT NULL,
    general_ledger_tag              VARCHAR(20),
    cl_delivery_group_nbr           INTEGER,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    override_pay_rate               NUMERIC(18,6),
    pr_check_mb_group_nbr           INTEGER,
    pr_ee_report_mb_group_nbr       INTEGER,
    pr_ee_report_sec_mb_group_nbr   INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    pr_dbdt_report_mb_group_nbr     INTEGER,
    pr_dbdt_report_sc_mb_group_nbr  INTEGER,
    new_hire_co_local_tax_nbr       INTEGER,
    worksite_id                     VARCHAR(20),
    co_locations_nbr                INTEGER
);


CREATE TABLE xo_branch_locals (
    co_branch_locals_nbr  INTEGER NOT NULL,
    co_branch_nbr         INTEGER NOT NULL,
    co_local_tax_nbr      INTEGER NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xo_brch_pr_batch_deflt_ed (
    co_brch_pr_batch_deflt_ed_nbr  INTEGER NOT NULL,
    co_branch_nbr                  INTEGER NOT NULL,
    co_e_d_codes_nbr               INTEGER NOT NULL,
    salary_hourly_or_both          CHAR(1) NOT NULL,
    override_ee_rate_number        INTEGER,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    override_line_item_date        DATE,
    filler                         VARCHAR(512),
    co_jobs_nbr                    INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    override_rate                  NUMERIC(18,6),
    override_hours                 NUMERIC(18,6),
    override_amount                NUMERIC(18,6)
);


CREATE TABLE xo_calendar_defaults (
    co_calendar_defaults_nbr        INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    last_real_scheduled_check_date  DATE NOT NULL,
    call_in_time                    TIMESTAMP NOT NULL,
    delivery_time                   TIMESTAMP NOT NULL,
    number_of_days_prior            INTEGER NOT NULL,
    number_of_days_after            INTEGER NOT NULL,
    frequency                       VARCHAR(2) NOT NULL,
    period_begin_date               DATE NOT NULL,
    period_end_date                 DATE NOT NULL,
    changed_by                      INTEGER NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    filler                          VARCHAR(512),
    active_record                   CHAR(1) NOT NULL,
    last_real_sched_check_date2     DATE,
    call_in_time2                   TIMESTAMP,
    delivery_time2                  TIMESTAMP,
    number_of_days_prior2           INTEGER,
    number_of_days_after2           INTEGER,
    period_begin_date2              DATE,
    period_end_date2                DATE,
    move_check_date                 INTEGER NOT NULL,
    move_callin_date                INTEGER NOT NULL,
    move_delivery_date              INTEGER NOT NULL,
    number_of_months                INTEGER NOT NULL
);


CREATE TABLE xo_department (
    co_department_nbr               INTEGER NOT NULL,
    co_branch_nbr                   INTEGER NOT NULL,
    custom_department_number        VARCHAR(20) NOT NULL,
    name                            VARCHAR(40),
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    contact1                        VARCHAR(30),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax                             VARCHAR(20),
    fax_description                 VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    home_co_states_nbr              INTEGER,
    pay_frequency_hourly            CHAR(1) NOT NULL,
    pay_frequency_salary            CHAR(1) NOT NULL,
    cl_timeclock_imports_nbr        INTEGER,
    print_dept_address_on_checks    CHAR(1) NOT NULL,
    payroll_cl_bank_account_nbr     INTEGER,
    cl_billing_nbr                  INTEGER,
    billing_cl_bank_account_nbr     INTEGER,
    tax_cl_bank_account_nbr         INTEGER,
    dd_cl_bank_account_nbr          INTEGER,
    co_workers_comp_nbr             INTEGER,
    union_cl_e_ds_nbr               INTEGER,
    override_ee_rate_number         INTEGER,
    department_notes                BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    home_state_type                 CHAR(1) NOT NULL,
    general_ledger_tag              VARCHAR(20),
    cl_delivery_group_nbr           INTEGER,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    override_pay_rate               NUMERIC(18,6),
    pr_check_mb_group_nbr           INTEGER,
    pr_ee_report_mb_group_nbr       INTEGER,
    pr_ee_report_sec_mb_group_nbr   INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    pr_dbdt_report_mb_group_nbr     INTEGER,
    pr_dbdt_report_sc_mb_group_nbr  INTEGER,
    new_hire_co_local_tax_nbr       INTEGER,
    worksite_id                     VARCHAR(20),
    co_locations_nbr                INTEGER
);


CREATE TABLE xo_department_locals (
    co_department_locals_nbr  INTEGER NOT NULL,
    co_department_nbr         INTEGER NOT NULL,
    co_local_tax_nbr          INTEGER NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xo_dept_pr_batch_deflt_ed (
    co_dept_pr_batch_deflt_ed_nbr  INTEGER NOT NULL,
    co_department_nbr              INTEGER NOT NULL,
    co_e_d_codes_nbr               INTEGER NOT NULL,
    salary_hourly_or_both          CHAR(1) NOT NULL,
    override_ee_rate_number        INTEGER,
    override_line_item_date        DATE,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    filler                         VARCHAR(512),
    co_jobs_nbr                    INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    override_rate                  NUMERIC(18,6),
    override_hours                 NUMERIC(18,6),
    override_amount                NUMERIC(18,6)
);


CREATE TABLE xo_div_pr_batch_deflt_ed (
    co_div_pr_batch_deflt_ed_nbr  INTEGER NOT NULL,
    co_division_nbr               INTEGER NOT NULL,
    co_e_d_codes_nbr              INTEGER NOT NULL,
    salary_hourly_or_both         CHAR(1) NOT NULL,
    override_ee_rate_number       INTEGER,
    override_line_item_date       DATE,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    filler                        VARCHAR(512),
    co_jobs_nbr                   INTEGER,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    override_rate                 NUMERIC(18,6),
    override_hours                NUMERIC(18,6),
    override_amount               NUMERIC(18,6)
);


CREATE TABLE xo_division (
    co_division_nbr                 INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    custom_division_number          VARCHAR(20) NOT NULL,
    name                            VARCHAR(40),
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    contact1                        VARCHAR(30),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax                             VARCHAR(20),
    fax_description                 VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    home_co_states_nbr              INTEGER,
    cl_timeclock_imports_nbr        INTEGER,
    pay_frequency_hourly            CHAR(1) NOT NULL,
    pay_frequency_salary            CHAR(1) NOT NULL,
    print_div_address_on_checks     CHAR(1) NOT NULL,
    payroll_cl_bank_account_nbr     INTEGER,
    cl_billing_nbr                  INTEGER,
    billing_cl_bank_account_nbr     INTEGER,
    tax_cl_bank_account_nbr         INTEGER,
    dd_cl_bank_account_nbr          INTEGER,
    co_workers_comp_nbr             INTEGER,
    override_ee_rate_number         INTEGER,
    union_cl_e_ds_nbr               INTEGER,
    division_notes                  BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    home_state_type                 CHAR(1) NOT NULL,
    general_ledger_tag              VARCHAR(20),
    cl_delivery_group_nbr           INTEGER,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    override_pay_rate               NUMERIC(18,6),
    pr_check_mb_group_nbr           INTEGER,
    pr_ee_report_mb_group_nbr       INTEGER,
    pr_ee_report_sec_mb_group_nbr   INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    pr_dbdt_report_mb_group_nbr     INTEGER,
    pr_dbdt_report_sc_mb_group_nbr  INTEGER,
    new_hire_co_local_tax_nbr       INTEGER,
    worksite_id                     VARCHAR(20),
    co_locations_nbr                INTEGER
);


CREATE TABLE xo_division_locals (
    co_division_locals_nbr  INTEGER NOT NULL,
    co_division_nbr         INTEGER NOT NULL,
    co_local_tax_nbr        INTEGER NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xo_e_d_codes (
    co_e_d_codes_nbr        INTEGER NOT NULL,
    cl_e_ds_nbr             INTEGER NOT NULL,
    co_nbr                  INTEGER NOT NULL,
    distribute              CHAR(1) NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    general_ledger_tag      VARCHAR(20),
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL,
    gl_offset               VARCHAR(15),
    co_benefits_nbr         INTEGER,
    co_benefit_subtype_nbr  INTEGER,
    ee_or_er_benefit        CHAR(1) NOT NULL,
    used_as_benefit         CHAR(1) NOT NULL
);


CREATE TABLE xo_enlist_groups (
    co_enlist_groups_nbr  INTEGER NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    changed_by            INTEGER NOT NULL,
    active_record         CHAR(1) NOT NULL,
    co_nbr                INTEGER NOT NULL,
    sy_report_groups_nbr  INTEGER NOT NULL,
    media_type            CHAR(1) NOT NULL,
    process_type          CHAR(1) NOT NULL
);


CREATE TABLE xo_fed_tax_liabilities (
    co_fed_tax_liabilities_nbr    INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    due_date                      DATE NOT NULL,
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    adjustment_type               CHAR(1),
    pr_nbr                        INTEGER,
    co_tax_deposits_nbr           INTEGER,
    impound_co_bank_acct_reg_nbr  INTEGER,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                        VARCHAR(512),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    third_party                   CHAR(1) NOT NULL,
    check_date                    DATE,
    tax_type                      CHAR(1) NOT NULL,
    impounded                     CHAR(1) NOT NULL,
    ach_key                       VARCHAR(11),
    period_begin_date             DATE,
    period_end_date               DATE,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    amount                        NUMERIC(18,6),
    prev_status              CHAR(1) NOT NULL
);


CREATE TABLE xo_general_ledger (
    co_general_ledger_nbr  INTEGER NOT NULL,
    co_nbr                 INTEGER NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    level_type             CHAR(1) NOT NULL,
    level_nbr              INTEGER,
    data_type              CHAR(1) NOT NULL,
    data_nbr               INTEGER,
    general_ledger_format  VARCHAR(30) NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xo_group (
    co_group_nbr    INTEGER NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    changed_by      INTEGER NOT NULL,
    active_record   CHAR(1) NOT NULL,
    co_nbr          INTEGER NOT NULL,
    group_type      CHAR(1) NOT NULL,
    group_name      VARCHAR(50)
);


CREATE TABLE xo_group_manager (
    co_group_manager_nbr  INTEGER NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    changed_by            INTEGER NOT NULL,
    active_record         CHAR(1) NOT NULL,
    co_group_nbr          INTEGER NOT NULL,
    ee_nbr                INTEGER NOT NULL,
    send_email            CHAR(1) NOT NULL
);


CREATE TABLE xo_group_member (
    co_group_member_nbr  INTEGER NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    changed_by           INTEGER NOT NULL,
    active_record        CHAR(1) NOT NULL,
    co_group_nbr         INTEGER NOT NULL,
    ee_nbr               INTEGER NOT NULL
);


CREATE TABLE xo_hr_applicant (
    co_hr_applicant_nbr           INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    cl_person_nbr                 INTEGER NOT NULL,
    application_date              DATE NOT NULL,
    co_hr_car_nbr                 INTEGER,
    ged                           CHAR(1),
    ged_date                      DATE,
    co_hr_recruiters_nbr          INTEGER,
    co_hr_referrals_nbr           INTEGER,
    co_division_nbr               INTEGER,
    co_branch_nbr                 INTEGER,
    co_department_nbr             INTEGER,
    co_team_nbr                   INTEGER,
    co_jobs_nbr                   INTEGER,
    desired_regular_or_temporary  CHAR(1),
    position_status               CHAR(1),
    desired_shift                 INTEGER,
    security_clearance            VARCHAR(20),
    applicant_status              CHAR(1),
    applicant_contacted           CHAR(1),
    applicant_contacted_by        VARCHAR(10),
    applicant_contacted_date      TIMESTAMP,
    date_position_offered         TIMESTAMP,
    date_position_accepted        TIMESTAMP,
    start_date                    DATE,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    co_hr_supervisors_nbr         INTEGER,
    applicant_type                CHAR(1) NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    desired_rate                  NUMERIC(18,6),
    desired_salary                NUMERIC(18,6),
    co_hr_positions_nbr           INTEGER
);


CREATE TABLE xo_hr_applicant_interview (
    co_hr_applicant_interview_nbr  INTEGER NOT NULL,
    co_hr_applicant_nbr            INTEGER NOT NULL,
    interview_date                 DATE,
    scheduled_by                   VARCHAR(10),
    interviewed_by                 BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    call_back                      CHAR(1) NOT NULL,
    scheduled_return_call_date     TIMESTAMP,
    actual_call_back_date          TIMESTAMP,
    results                        BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    results_date                   DATE,
    notes                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_attendance_types (
    co_hr_attendance_types_nbr  INTEGER NOT NULL,
    co_nbr                      INTEGER NOT NULL,
    attendance_description      VARCHAR(40) NOT NULL,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_car (
    co_hr_car_nbr        INTEGER NOT NULL,
    co_nbr               INTEGER NOT NULL,
    make                 VARCHAR(20),
    model                VARCHAR(20),
    color                CHAR(1),
    license_plate        VARCHAR(10),
    state                VARCHAR(3),
    vin_number           VARCHAR(20),
    personal_or_company  CHAR(1) NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    year_made            VARCHAR(4),
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_performance_ratings (
    co_hr_performance_ratings_nbr  INTEGER NOT NULL,
    co_nbr                         INTEGER NOT NULL,
    description                    VARCHAR(40) NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_position_grades (
    co_hr_position_grades_nbr  INTEGER NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    changed_by                 INTEGER NOT NULL,
    active_record              CHAR(1) NOT NULL,
    co_hr_positions_nbr        INTEGER NOT NULL,
    co_hr_salary_grades_nbr    INTEGER NOT NULL
);


CREATE TABLE xo_hr_positions (
    co_hr_positions_nbr  INTEGER NOT NULL,
    co_nbr               INTEGER NOT NULL,
    description          VARCHAR(40) NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_property (
    co_hr_property_nbr    INTEGER NOT NULL,
    co_nbr                INTEGER NOT NULL,
    property_description  VARCHAR(40) NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_recruiters (
    co_hr_recruiters_nbr  INTEGER NOT NULL,
    co_nbr                INTEGER NOT NULL,
    name                  VARCHAR(40) NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_referrals (
    co_hr_referrals_nbr  INTEGER NOT NULL,
    co_nbr               INTEGER NOT NULL,
    description          VARCHAR(40) NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL
);


CREATE TABLE xo_hr_salary_grades (
    co_hr_salary_grades_nbr  INTEGER NOT NULL,
    co_nbr                   INTEGER NOT NULL,
    description              VARCHAR(40) NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    minimum_pay              NUMERIC(18,6),
    mid_pay                  NUMERIC(18,6),
    maximum_pay              NUMERIC(18,6)
);


CREATE TABLE xo_hr_supervisors (
    co_hr_supervisors_nbr  INTEGER NOT NULL,
    co_nbr                 INTEGER NOT NULL,
    supervisor_name        VARCHAR(40) NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xo_job_groups (
    co_job_groups_nbr  INTEGER NOT NULL,
    co_nbr             INTEGER NOT NULL,
    name               VARCHAR(40) NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xo_jobs (
    co_jobs_nbr          INTEGER NOT NULL,
    co_nbr               INTEGER NOT NULL,
    description          VARCHAR(40) NOT NULL,
    job_active           CHAR(1) NOT NULL,
    certified            CHAR(1) NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    filler               VARCHAR(512),
    co_workers_comp_nbr  INTEGER,
    general_ledger_tag   VARCHAR(20),
    true_description     VARCHAR(40) NOT NULL,
    state_certified      CHAR(1) NOT NULL,
    misc_job_code        VARCHAR(40),
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL,
    rate_per_hour        NUMERIC(18,6),
    address1             VARCHAR(30),
    address2             VARCHAR(30),
    city                 VARCHAR(20),
    state                CHAR(2),
    zip_code             VARCHAR(10),
    co_locations_nbr     INTEGER
);


CREATE TABLE xo_jobs_locals (
    co_jobs_locals_nbr  INTEGER NOT NULL,
    co_jobs_nbr         INTEGER NOT NULL,
    co_local_tax_nbr    INTEGER NOT NULL,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);


CREATE TABLE xo_local_tax (
    co_local_tax_nbr                INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    sy_locals_nbr                   INTEGER NOT NULL,
    exempt                          CHAR(1) NOT NULL,
    local_ein_number                VARCHAR(15) NOT NULL,
    tax_return_code                 VARCHAR(20),
    sy_local_deposit_freq_nbr       INTEGER NOT NULL,
    payment_method                  CHAR(1) NOT NULL,
    final_tax_return                CHAR(1) NOT NULL,
    eft_name                        VARCHAR(40),
    eft_pin_number                  VARCHAR(10),
    eft_status                      CHAR(1),
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    general_ledger_tag              VARCHAR(20),
    offset_gl_tag                   VARCHAR(20),
    local_active                    CHAR(1) NOT NULL,
    self_adjust_tax                 CHAR(1) NOT NULL,
    autocreate_on_new_hire          CHAR(1) NOT NULL,
    sy_states_nbr                   INTEGER NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    tax_rate                        NUMERIC(18,6),
    tax_amount                      NUMERIC(18,6),
    miscellaneous_amount            NUMERIC(18,6),
    applied_for                     CHAR(1) NOT NULL,
    cl_e_ds_nbr                     INTEGER,
    last_tax_return                 CHAR(1) NOT NULL,
    deduct                          CHAR(1)
);


CREATE TABLE xo_local_tax_liabilities (
    co_local_tax_liabilities_nbr  INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    co_local_tax_nbr              INTEGER NOT NULL,
    due_date                      DATE NOT NULL,
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    adjustment_type               CHAR(1),
    pr_nbr                        INTEGER,
    co_tax_deposits_nbr           INTEGER,
    impound_co_bank_acct_reg_nbr  INTEGER,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                        VARCHAR(512),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    third_party                   CHAR(1) NOT NULL,
    check_date                    DATE,
    impounded                     CHAR(1) NOT NULL,
    ach_key                       VARCHAR(11),
    period_begin_date             DATE,
    period_end_date               DATE,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    amount                        NUMERIC(18,6),
    co_locations_nbr              INTEGER,
    nonres_co_local_tax_nbr       INTEGER,
    prev_status              CHAR(1) NOT NULL
);


CREATE TABLE xo_locations (
    co_locations_nbr  INTEGER NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL,
    changed_by        INTEGER NOT NULL,
    co_nbr            INTEGER NOT NULL,
    account_number    VARCHAR(30) NOT NULL,
    address1          VARCHAR(30),
    address2          VARCHAR(30),
    city              VARCHAR(20),
    state             CHAR(2),
    zip_code          VARCHAR(10)
);


CREATE TABLE xo_manual_ach (
    co_manual_ach_nbr           INTEGER NOT NULL,
    co_nbr                      INTEGER NOT NULL,
    ee_nbr                      INTEGER,
    debit_date                  DATE NOT NULL,
    credit_date                 DATE NOT NULL,
    status                      CHAR(1) NOT NULL,
    purpose_notes               BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    from_co_nbr                 INTEGER,
    from_ee_nbr                 INTEGER,
    from_sb_bank_accounts_nbr   INTEGER,
    from_cl_bank_account_nbr    INTEGER,
    from_ee_direct_deposit_nbr  INTEGER,
    to_co_nbr                   INTEGER,
    to_ee_nbr                   INTEGER,
    to_sb_bank_accounts_nbr     INTEGER,
    to_cl_bank_account_nbr      INTEGER,
    to_ee_direct_deposit_nbr    INTEGER,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    status_date                 TIMESTAMP NOT NULL,
    filler                      VARCHAR(512),
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    amount                      NUMERIC(18,6)
);


CREATE TABLE xo_pay_group (
    co_pay_group_nbr               INTEGER NOT NULL,
    co_nbr                         INTEGER NOT NULL,
    description                    VARCHAR(40) NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    pr_check_mb_group_nbr          INTEGER,
    pr_ee_report_mb_group_nbr      INTEGER,
    pr_ee_report_sec_mb_group_nbr  INTEGER,
    tax_ee_return_mb_group_nbr     INTEGER
);


CREATE TABLE xo_pensions (
    co_pensions_nbr  INTEGER NOT NULL,
    co_nbr           INTEGER NOT NULL,
    cl_pension_nbr   INTEGER NOT NULL,
    filler           VARCHAR(512),
    changed_by       INTEGER NOT NULL,
    creation_date    TIMESTAMP NOT NULL,
    effective_date   TIMESTAMP NOT NULL,
    active_record    CHAR(1) NOT NULL
);


CREATE TABLE xo_phone (
    co_phone_nbr    INTEGER NOT NULL,
    co_nbr          INTEGER NOT NULL,
    contact         VARCHAR(30),
    phone_number    VARCHAR(20) NOT NULL,
    phone_type      CHAR(1) NOT NULL,
    description     VARCHAR(40),
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL,
    email_address   VARCHAR(80),
    phone_ext       VARCHAR(10),
    seq_num         SMALLINT,
    title           VARCHAR(40)
);


CREATE TABLE xo_pr_ach (
    co_pr_ach_nbr    INTEGER NOT NULL,
    co_nbr           INTEGER NOT NULL,
    ach_date         TIMESTAMP NOT NULL,
    status           CHAR(1) NOT NULL,
    pr_nbr           INTEGER NOT NULL,
    notes            BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler           VARCHAR(512),
    changed_by       INTEGER NOT NULL,
    creation_date    TIMESTAMP NOT NULL,
    status_date      TIMESTAMP NOT NULL,
    effective_date   TIMESTAMP NOT NULL,
    active_record    CHAR(1) NOT NULL,
    amount           NUMERIC(18,6),
    trust_impound    CHAR(1) NOT NULL,
    tax_impound      CHAR(1) NOT NULL,
    dd_impound       CHAR(1) NOT NULL,
    billing_impound  CHAR(1) NOT NULL,
    wc_impound       CHAR(1) NOT NULL,
    cl_blob_nbr      INTEGER
);


CREATE TABLE xo_pr_batch_deflt_ed (
    co_pr_batch_deflt_ed_nbr  INTEGER NOT NULL,
    co_nbr                    INTEGER NOT NULL,
    co_e_d_codes_nbr          INTEGER NOT NULL,
    salary_hourly_or_both     CHAR(1) NOT NULL,
    override_ee_rate_number   INTEGER,
    override_line_item_date   TIMESTAMP,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    filler                    VARCHAR(512),
    co_jobs_nbr               INTEGER,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL,
    override_rate             NUMERIC(18,6),
    override_hours            NUMERIC(18,6),
    override_amount           NUMERIC(18,6)
);


CREATE TABLE xo_pr_check_template_e_ds (
    co_pr_check_template_e_ds_nbr  INTEGER NOT NULL,
    co_pr_check_templates_nbr      INTEGER NOT NULL,
    cl_e_ds_nbr                    INTEGER NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL
);


CREATE TABLE xo_pr_check_templates (
    co_pr_check_templates_nbr       INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    name                            VARCHAR(40) NOT NULL,
    exclude_dd                      CHAR(1) NOT NULL,
    exclude_dd_except_net           CHAR(1) NOT NULL,
    exclude_time_off_accural        CHAR(1) NOT NULL,
    exclude_auto_distribution       CHAR(1) NOT NULL,
    exclude_all_sched_e_d_codes     CHAR(1) NOT NULL,
    exclude_sch_e_d_from_agcy_chk   CHAR(1) NOT NULL,
    exclude_sch_e_d_except_pension  CHAR(1) NOT NULL,
    prorate_scheduled_e_ds          CHAR(1) NOT NULL,
    exclude_federal                 CHAR(1) NOT NULL,
    exclude_additional_federal      CHAR(1) NOT NULL,
    exclude_employee_oasdi          CHAR(1) NOT NULL,
    exclude_employer_oasdi          CHAR(1) NOT NULL,
    exclude_employee_medicare       CHAR(1) NOT NULL,
    exclude_employer_medicare       CHAR(1) NOT NULL,
    exclude_employee_eic            CHAR(1) NOT NULL,
    exclude_employer_fui            CHAR(1) NOT NULL,
    tax_at_supplemental_rate        CHAR(1) NOT NULL,
    federal_override_type           CHAR(1) NOT NULL,
    override_frequency              CHAR(1) NOT NULL,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    federal_override_value          NUMERIC(18,6)
);


CREATE TABLE xo_pr_filters (
    co_pr_filters_nbr  INTEGER NOT NULL,
    co_nbr             INTEGER NOT NULL,
    name               VARCHAR(40) NOT NULL,
    co_division_nbr    INTEGER,
    co_branch_nbr      INTEGER,
    co_department_nbr  INTEGER,
    co_team_nbr        INTEGER,
    co_pay_group_nbr   INTEGER,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xo_reports (
    co_reports_nbr             INTEGER NOT NULL,
    co_nbr                     INTEGER NOT NULL,
    custom_name                VARCHAR(40) NOT NULL,
    changed_by                 INTEGER NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    active_record              CHAR(1) NOT NULL,
    report_writer_reports_nbr  INTEGER NOT NULL,
    report_level               CHAR(1) NOT NULL,
    run_params                 BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    input_params               BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    override_mb_group_nbr      INTEGER,
    favorite                   CHAR(1) NOT NULL
);


CREATE TABLE xo_salesperson (
    co_salesperson_nbr            INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    sb_user_nbr                   INTEGER NOT NULL,
    next_commission_percent_date  TIMESTAMP,
    last_visit_date               DATE,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    salesperson_type              CHAR(1) NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    commission_percentage         NUMERIC(18,6),
    next_commission_percentage    NUMERIC(18,6),
    chargeback                    NUMERIC(18,6),
    projected_annual_commission   NUMERIC(18,6)
);


CREATE TABLE xo_salesperson_flat_amt (
    co_salesperson_flat_amt_nbr  INTEGER NOT NULL,
    co_salesperson_nbr           INTEGER NOT NULL,
    flat_amount_date             TIMESTAMP NOT NULL,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    flat_amount                  NUMERIC(18,6)
);


CREATE TABLE xo_services (
    co_services_nbr           INTEGER NOT NULL,
    co_nbr                    INTEGER NOT NULL,
    sb_services_nbr           INTEGER NOT NULL,
    name                      VARCHAR(40) NOT NULL,
    effective_start_date      DATE,
    effective_end_date        DATE,
    sales_tax                 CHAR(1) NOT NULL,
    discount_type             CHAR(1) NOT NULL,
    discount_start_date       DATE,
    filler                    VARCHAR(512),
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    frequency                 CHAR(1) NOT NULL,
    week_number               CHAR(1),
    month_number              VARCHAR(2),
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL,
    override_flat_fee         NUMERIC(18,6),
    discount                  NUMERIC(18,6),
    discount_end_date         DATE,
    co_e_d_codes_nbr          INTEGER,
    cl_e_d_groups_nbr         INTEGER,
    co_workers_comp_nbr       INTEGER,
    override_tax_rate         NUMERIC(18,6)
);


CREATE TABLE xo_shifts (
    co_shifts_nbr            INTEGER NOT NULL,
    co_nbr                   INTEGER NOT NULL,
    name                     VARCHAR(40) NOT NULL,
    cl_e_d_groups_nbr        INTEGER,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    default_amount           NUMERIC(18,6),
    default_percentage       NUMERIC(18,6),
    auto_create_on_new_hire  CHAR(1) NOT NULL
);


CREATE TABLE xo_state_tax_liabilities (
    co_state_tax_liabilities_nbr  INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    co_states_nbr                 INTEGER NOT NULL,
    due_date                      DATE NOT NULL,
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    adjustment_type               CHAR(1),
    pr_nbr                        INTEGER,
    co_tax_deposits_nbr           INTEGER,
    impound_co_bank_acct_reg_nbr  INTEGER,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                        VARCHAR(512),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    third_party                   CHAR(1) NOT NULL,
    check_date                    DATE,
    tax_type                      CHAR(1) NOT NULL,
    impounded                     CHAR(1) NOT NULL,
    ach_key                       VARCHAR(11),
    sy_state_deposit_freq_nbr     INTEGER,
    period_begin_date             DATE,
    period_end_date               DATE,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    amount                        NUMERIC(18,6),
    prev_status              CHAR(1) NOT NULL
);


CREATE TABLE xo_states (
    co_states_nbr                   INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    state                           CHAR(2) NOT NULL,
    tax_return_code                 VARCHAR(20),
    use_dba_on_tax_return           CHAR(1) NOT NULL,
    state_tax_deposit_method        CHAR(1) NOT NULL,
    sui_tax_deposit_method          CHAR(1) NOT NULL,
    sy_state_deposit_freq_nbr       INTEGER NOT NULL,
    ignore_state_tax_dep_threshold  CHAR(1) NOT NULL,
    sui_tax_deposit_frequency       CHAR(1) NOT NULL,
    state_non_profit                CHAR(1) NOT NULL,
    state_exempt                    CHAR(1) NOT NULL,
    sui_exempt                      CHAR(1) NOT NULL,
    ee_sdi_exempt                   CHAR(1) NOT NULL,
    state_eft_name                  VARCHAR(40),
    state_eft_pin_number            VARCHAR(10),
    state_eft_enrollment_status     CHAR(1) NOT NULL,
    sui_eft_name                    VARCHAR(40),
    sui_eft_pin_number              VARCHAR(10),
    sui_eft_enrollment_status       CHAR(1) NOT NULL,
    use_state_for_sui               CHAR(1) NOT NULL,
    notes                           BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    state_ein                       VARCHAR(30) NOT NULL,
    sui_ein                         VARCHAR(30) NOT NULL,
    state_sdi_ein                   VARCHAR(30) NOT NULL,
    final_tax_return                CHAR(1) NOT NULL,
    state_general_ledger_tag        VARCHAR(20),
    ee_sdi_general_ledger_tag       VARCHAR(20),
    er_sdi_general_ledger_tag       VARCHAR(20),
    state_offset_gl_tag             VARCHAR(20),
    ee_sdi_offset_gl_tag            VARCHAR(20),
    er_sdi_offset_gl_tag            VARCHAR(20),
    state_eft_ein                   VARCHAR(30),
    state_sdi_eft_ein               VARCHAR(30),
    sui_eft_ein                     VARCHAR(30),
    sy_states_nbr                   INTEGER NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    mo_tax_credit_active            CHAR(1) NOT NULL,
    er_sdi_exempt                   CHAR(1) NOT NULL,
    applied_for                     CHAR(1) NOT NULL,
    company_paid_health_insurance   CHAR(1) NOT NULL,
    last_tax_return                 CHAR(1) NOT NULL,
    tcd_deposit_frequency_nbr       INTEGER,
    tcd_payment_method              CHAR(1) NOT NULL
);


CREATE TABLE xo_storage (
    co_storage_nbr  INTEGER NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    changed_by      INTEGER NOT NULL,
    active_record   CHAR(1) NOT NULL,
    co_nbr          INTEGER NOT NULL,
    tag             VARCHAR(50),
    storage_data    BLOB SUB_TYPE 0 SEGMENT SIZE 80
);


CREATE TABLE xo_sui (
    co_sui_nbr                  INTEGER NOT NULL,
    co_nbr                      INTEGER NOT NULL,
    co_states_nbr               INTEGER NOT NULL,
    sy_sui_nbr                  INTEGER NOT NULL,
    tax_return_code             VARCHAR(20),
    description                 VARCHAR(40) NOT NULL,
    payment_method              CHAR(1) NOT NULL,
    filler                      VARCHAR(512),
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    final_tax_return            CHAR(1) NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    gl_tag                      VARCHAR(20),
    sui_active                  CHAR(1) NOT NULL,
    rate                        NUMERIC(18,8),
    gl_offset_tag               VARCHAR(20),
    applied_for                 CHAR(1) NOT NULL,
    last_tax_return             CHAR(1) NOT NULL,
    sui_reimburser              CHAR(1) NOT NULL,
    alternate_wage              CHAR(1) NOT NULL

);


CREATE TABLE xo_sui_liabilities (
    co_sui_liabilities_nbr        INTEGER NOT NULL,
    co_nbr                        INTEGER NOT NULL,
    co_sui_nbr                    INTEGER NOT NULL,
    due_date                      DATE NOT NULL,
    status                        CHAR(1) NOT NULL,
    status_date                   TIMESTAMP NOT NULL,
    adjustment_type               CHAR(1),
    pr_nbr                        INTEGER,
    co_tax_deposits_nbr           INTEGER,
    impound_co_bank_acct_reg_nbr  INTEGER,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                        VARCHAR(512),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    third_party                   CHAR(1) NOT NULL,
    check_date                    DATE,
    impounded                     CHAR(1) NOT NULL,
    ach_key                       VARCHAR(11),
    period_begin_date             DATE,
    period_end_date               DATE,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    amount                        NUMERIC(18,6),
    prev_status              CHAR(1) NOT NULL
);


CREATE TABLE xo_tax_deposits (
    co_tax_deposits_nbr            INTEGER NOT NULL,
    co_nbr                         INTEGER NOT NULL,
    tax_payment_reference_number   VARCHAR(20),
    status                         CHAR(1) NOT NULL,
    pr_nbr                         INTEGER,
    co_delivery_package_nbr        INTEGER,
    filler                         VARCHAR(512),
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    sy_gl_agency_field_office_nbr  INTEGER,
    status_date                    TIMESTAMP NOT NULL,
    deposit_type                   CHAR(1),
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    trace_number                   VARCHAR(15),
    sb_tax_payment_nbr             INTEGER
);


CREATE TABLE xo_tax_payment_ach (
    co_tax_payment_ach_nbr  INTEGER NOT NULL,
    ach_date                TIMESTAMP NOT NULL,
    status                  CHAR(1) NOT NULL,
    co_tax_deposits_nbr     INTEGER NOT NULL,
    notes                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                  VARCHAR(512),
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    status_date             TIMESTAMP NOT NULL,
    co_nbr                  INTEGER NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL,
    amount                  NUMERIC(18,6)
);


CREATE TABLE xo_tax_return_queue (
    co_tax_return_queue_nbr  INTEGER NOT NULL,
    co_nbr                   INTEGER NOT NULL,
    co_delivery_package_nbr  INTEGER,
    status                   CHAR(1) NOT NULL,
    status_date              TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    sb_copy_printed          CHAR(1) NOT NULL,
    client_copy_printed      CHAR(1) NOT NULL,
    agency_copy_printed      CHAR(1) NOT NULL,
    due_date                 DATE NOT NULL,
    period_end_date          DATE NOT NULL,
    filler                   VARCHAR(512),
    sy_reports_group_nbr     INTEGER NOT NULL,
    cl_co_consolidation_nbr  INTEGER,
    produce_ascii_file       CHAR(1) NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    sy_gl_agency_report_nbr  INTEGER,
    active_record            CHAR(1) NOT NULL
);


CREATE TABLE xo_tax_return_runs (
    co_tax_return_runs_nbr   INTEGER NOT NULL,
    co_tax_return_queue_nbr  INTEGER NOT NULL,
    ee_nbr                   INTEGER,     
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    cl_blob_nbr              INTEGER NOT NULL,
    change_date              TIMESTAMP NOT NULL
);


CREATE TABLE xo_team (
    co_team_nbr                     INTEGER NOT NULL,
    co_department_nbr               INTEGER NOT NULL,
    custom_team_number              VARCHAR(20) NOT NULL,
    name                            VARCHAR(40),
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    contact1                        VARCHAR(30),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax                             VARCHAR(20),
    fax_description                 VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    home_co_states_nbr              INTEGER NOT NULL,
    pay_frequency_hourly            CHAR(1) NOT NULL,
    pay_frequency_salary            CHAR(1) NOT NULL,
    cl_timeclock_imports_nbr        INTEGER,
    print_group_address_on_check    CHAR(1) NOT NULL,
    payroll_cl_bank_account_nbr     INTEGER,
    cl_billing_nbr                  INTEGER,
    billing_cl_bank_account_nbr     INTEGER,
    tax_cl_bank_account_nbr         INTEGER,
    dd_cl_bank_account_nbr          INTEGER,
    union_cl_e_ds_nbr               INTEGER,
    override_ee_rate_number         INTEGER,
    co_workers_comp_nbr             INTEGER,
    team_notes                      BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    home_state_type                 CHAR(1) NOT NULL,
    general_ledger_tag              VARCHAR(20),
    cl_delivery_group_nbr           INTEGER,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    override_pay_rate               NUMERIC(18,6),
    pr_check_mb_group_nbr           INTEGER,
    pr_ee_report_mb_group_nbr       INTEGER,
    pr_ee_report_sec_mb_group_nbr   INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    pr_dbdt_report_mb_group_nbr     INTEGER,
    pr_dbdt_report_sc_mb_group_nbr  INTEGER,
    filler                          VARCHAR(512),
    new_hire_co_local_tax_nbr       INTEGER,
    worksite_id                     VARCHAR(20),
    co_locations_nbr                INTEGER
);


CREATE TABLE xo_team_locals (
    co_team_locals_nbr  INTEGER NOT NULL,
    co_team_nbr         INTEGER NOT NULL,
    co_local_tax_nbr    INTEGER NOT NULL,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);


CREATE TABLE xo_team_pr_batch_deflt_ed (
    co_team_pr_batch_deflt_ed_nbr  INTEGER NOT NULL,
    co_team_nbr                    INTEGER NOT NULL,
    co_e_d_codes_nbr               INTEGER NOT NULL,
    salary_hourly_or_both          CHAR(1) NOT NULL,
    override_ee_rate_number        INTEGER,
    override_line_item_date        DATE,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    filler                         VARCHAR(512),
    co_jobs_nbr                    INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    override_rate                  NUMERIC(18,6),
    override_hours                 NUMERIC(18,6),
    override_amount                NUMERIC(18,6)
);


CREATE TABLE xo_time_off_accrual (
    co_time_off_accrual_nbr         INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    description                     VARCHAR(40) NOT NULL,
    cl_e_ds_nbr                     INTEGER NOT NULL,
    cl_e_d_groups_nbr               INTEGER,
    frequency                       CHAR(1) NOT NULL,
    accrual_month_number            VARCHAR(2),
    payroll_of_the_month_to_accrue  CHAR(1) NOT NULL,
    calculation_method              CHAR(1) NOT NULL,
    accrual_active                  CHAR(1) NOT NULL,
    auto_create_on_new_hire         CHAR(1),
    show_used_balance_on_check      CHAR(1) NOT NULL,
    annual_reset_code               CHAR(1),
    reset_date                      DATE,
    divisor_description             VARCHAR(6),
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    rllovr_co_time_off_accrual_nbr  INTEGER,
    rollover_date                   DATE,
    annual_maximum_hours_to_accrue  NUMERIC(18,6),
    minimum_hours_before_accrual    NUMERIC(18,6),
    probation_period_months         NUMERIC(18,6),
    divisor                         NUMERIC(18,6),
    used_cl_e_d_groups_nbr          INTEGER,
    rollover_freq                   CHAR(1) NOT NULL,
    rollover_month_number           INTEGER,
    rollover_payroll_of_month       CHAR(1),
    rollover_offset                 INTEGER,
    reset_month_number              INTEGER,
    reset_payroll_of_month          CHAR(1) NOT NULL,
    reset_offset                    INTEGER,
    maximum_hours_to_accrue_on      NUMERIC(18,6),
    check_time_off_avail            CHAR(1) NOT NULL,
    process_order                   CHAR(1) NOT NULL,
    co_hr_attendance_types_nbr      INTEGER,
    auto_create_for_statuses        VARCHAR(20),
    accrue_on_standard_hours        CHAR(1) NOT NULL,
    show_ess                        CHAR(1) NOT NULL
);


CREATE TABLE xo_time_off_accrual_rates (
    co_time_off_accrual_rates_nbr  INTEGER NOT NULL,
    co_time_off_accrual_nbr        INTEGER NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    minimum_month_number           NUMERIC(18,6),
    maximum_month_number           NUMERIC(18,6),
    hours                          NUMERIC(18,6),
    maximum_carryover              NUMERIC(18,6),
    annual_accrual_maximum         NUMERIC(18,6)
);


CREATE TABLE xo_time_off_accrual_tiers (
    co_time_off_accrual_tiers_nbr  INTEGER NOT NULL,
    co_time_off_accrual_nbr        INTEGER NOT NULL,
    minimum_hours                  NUMERIC(18,6) NOT NULL,
    maximum_hours                  NUMERIC(18,6) NOT NULL,
    minimum_month_number           INTEGER NOT NULL,
    maximum_month_number           INTEGER NOT NULL,
    rate                           NUMERIC(18,6) NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    changed_by                     INTEGER NOT NULL,
    active_record                  CHAR(1) NOT NULL
);


CREATE TABLE xo_unions (
    co_unions_nbr   INTEGER NOT NULL,
    co_nbr          INTEGER NOT NULL,
    cl_union_nbr    INTEGER NOT NULL,
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL
);


CREATE TABLE xo_user_reports (
    co_user_reports_nbr  INTEGER NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    changed_by           INTEGER NOT NULL,
    active_record        CHAR(1) NOT NULL,
    co_nbr               INTEGER NOT NULL,
    sb_user_nbr          INTEGER NOT NULL,
    co_reports_nbr       INTEGER
);


CREATE TABLE xo_workers_comp (
    co_workers_comp_nbr  INTEGER NOT NULL,
    workers_comp_code    VARCHAR(8) NOT NULL,
    co_nbr               INTEGER NOT NULL,
    co_states_nbr        INTEGER NOT NULL,
    description          VARCHAR(40) NOT NULL,
    cl_e_d_groups_nbr    INTEGER,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL,
    gl_tag               VARCHAR(20),
    rate                 NUMERIC(18,6),
    experience_rating    NUMERIC(18,6),
    gl_offset_tag        VARCHAR(20),
    subtract_premium     CHAR(1) NOT NULL
);


CREATE TABLE xe (
    ee_nbr                          INTEGER NOT NULL,
    cl_person_nbr                   INTEGER NOT NULL,
    co_nbr                          INTEGER NOT NULL,
    co_division_nbr                 INTEGER,
    co_branch_nbr                   INTEGER,
    co_department_nbr               INTEGER,
    co_team_nbr                     INTEGER,
    custom_employee_number          VARCHAR(9) NOT NULL,
    co_hr_applicant_nbr             INTEGER,
    co_hr_recruiters_nbr            INTEGER,
    co_hr_referrals_nbr             INTEGER,
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    county                          VARCHAR(10),
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    phone3                          VARCHAR(20),
    description3                    VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    time_clock_number               VARCHAR(9),
    original_hire_date              DATE,
    current_hire_date               DATE,
    new_hire_report_sent            CHAR(1) NOT NULL,
    current_termination_date        DATE,
    current_termination_code        CHAR(1) NOT NULL,
    co_hr_supervisors_nbr           INTEGER,
    autopay_co_shifts_nbr           INTEGER,
    co_hr_positions_nbr             INTEGER,
    position_effective_date         DATE,
    position_status                 CHAR(1),
    co_hr_performance_ratings_nbr   INTEGER,
    review_date                     DATE,
    next_review_date                DATE,
    pay_frequency                   CHAR(1) NOT NULL,
    next_raise_date                 DATE,
    next_pay_frequency              CHAR(1),
    tipped_directly                 CHAR(1) NOT NULL,
    ald_cl_e_d_groups_nbr           INTEGER,
    distribute_taxes                CHAR(1) NOT NULL,
    co_pay_group_nbr                INTEGER,
    cl_delivery_group_nbr           INTEGER,
    co_unions_nbr                   INTEGER,
    eic                             CHAR(1),
    flsa_exempt                     CHAR(1) NOT NULL,
    w2_type                         CHAR(1) NOT NULL,
    w2_pension                      CHAR(1) NOT NULL,
    w2_deferred_comp                CHAR(1) NOT NULL,
    w2_deceased                     CHAR(1) NOT NULL,
    w2_statutory_employee           CHAR(1) NOT NULL,
    w2_legal_rep                    CHAR(1) NOT NULL,
    home_tax_ee_states_nbr          INTEGER,
    exempt_employee_oasdi           CHAR(1) NOT NULL,
    exempt_employee_medicare        CHAR(1) NOT NULL,
    exempt_exclude_ee_fed           CHAR(1) NOT NULL,
    exempt_employer_oasdi           CHAR(1) NOT NULL,
    exempt_employer_medicare        CHAR(1) NOT NULL,
    exempt_employer_fui             CHAR(1) NOT NULL,
    override_fed_tax_type           CHAR(1) NOT NULL,
    gov_garnish_prior_child_suppt   CHAR(1),
    security_clearance              VARCHAR(20),
    badge_id                        VARCHAR(15),
    on_call_from                    TIMESTAMP,
    on_call_to                      TIMESTAMP,
    base_returns_on_this_ee         CHAR(1) NOT NULL,
    notes                           BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    general_ledger_tag              VARCHAR(20),
    company_or_individual_name      CHAR(1) NOT NULL,
    federal_marital_status          CHAR(1) NOT NULL,
    number_of_dependents            INTEGER NOT NULL,
    distribution_code_1099r         VARCHAR(4),
    tax_amt_determined_1099r        CHAR(1) NOT NULL,
    total_distribution_1099r        CHAR(1) NOT NULL,
    pension_plan_1099r              CHAR(1) NOT NULL,
    makeup_fica_on_cleanup_pr       CHAR(1) NOT NULL,
    sy_hr_eeo_nbr                   INTEGER,
    secondary_notes                 BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    co_pr_check_templates_nbr       INTEGER,
    generate_second_check           CHAR(1) NOT NULL,
    salary_amount                   NUMERIC(18,6),
    standard_hours                  NUMERIC(18,6),
    next_raise_amount               NUMERIC(18,6),
    next_raise_percentage           NUMERIC(18,6),
    next_raise_rate                 NUMERIC(18,6),
    workers_comp_wage_limit         NUMERIC(18,6),
    group_term_policy_amount        NUMERIC(18,6),
    override_fed_tax_value          NUMERIC(18,6),
    calculated_salary               NUMERIC(18,6),
    pr_check_mb_group_nbr           INTEGER,
    pr_ee_report_mb_group_nbr       INTEGER,
    pr_ee_report_sec_mb_group_nbr   INTEGER,
    tax_ee_return_mb_group_nbr      INTEGER,
    highly_compensated              CHAR(1) NOT NULL,
    corporate_officer               CHAR(1) NOT NULL,
    co_jobs_nbr                     INTEGER,
    co_workers_comp_nbr             INTEGER,
    ee_enabled                      CHAR(1) NOT NULL,
    gtl_number_of_hours             INTEGER,
    gtl_rate                        NUMERIC(18,6),
    auto_update_rates               CHAR(1) NOT NULL,
    eligible_for_rehire             CHAR(1) NOT NULL,
    selfserve_enabled               CHAR(1) NOT NULL,
    selfserve_username              VARCHAR(64),
    selfserve_password              VARCHAR(64),
    selfserve_last_login            TIMESTAMP,
    print_voucher                   CHAR(1) NOT NULL,
    healthcare_coverage             CHAR(1) NOT NULL,
    fui_rate_credit_override        NUMERIC(18,6),
    override_federal_minimum_wage   NUMERIC(18,6),
    wc_wage_limit_frequency         CHAR(1) NOT NULL,
    login_question1                 INTEGER,
    login_answer1                   VARCHAR(80),
    login_question2                 INTEGER,
    login_answer2                   VARCHAR(80),
    login_question3                 INTEGER,
    login_answer3                   VARCHAR(80),
    co_benefit_discount_nbr         INTEGER,
    benefits_enabled                CHAR(1) NOT NULL,
    existing_patient                CHAR(1) NOT NULL,
    pcp                             VARCHAR(40),
    time_off_enabled                CHAR(1) NOT NULL,
    dependent_benefits_available    CHAR(1) NOT NULL,
    dependent_benefits_avail_date   DATE,
    benefit_enrollment_complete     TIMESTAMP,
    last_qual_benefit_event         CHAR(1),
    last_qual_benefit_event_date    DATE,
    w2                              CHAR(1) NOT NULL,
    w2_form_on_file                 CHAR(1) NOT NULL,
    benefit_e_mail_address          VARCHAR(80),
    co_hr_position_grades_nbr       INTEGER,
    sec_question1                   INTEGER,
    sec_answer1                     VARCHAR(80),
    sec_question2                   INTEGER,
    sec_answer2                     VARCHAR(80)
);


CREATE TABLE xe_additional_info (
    ee_additional_info_nbr         INTEGER NOT NULL,
    ee_nbr                         INTEGER NOT NULL,
    co_additional_info_names_nbr   INTEGER NOT NULL,
    info_string                    VARCHAR(40),
    info_date                      DATE,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    info_amount                    NUMERIC(18,6),
    co_additional_info_values_nbr  INTEGER,
    info_blob                      INTEGER
);


CREATE TABLE xe_autolabor_distribution (
    ee_autolabor_distribution_nbr  INTEGER NOT NULL,
    co_nbr                         INTEGER NOT NULL,
    co_division_nbr                INTEGER,
    co_branch_nbr                  INTEGER,
    co_department_nbr              INTEGER,
    co_team_nbr                    INTEGER,
    ee_nbr                         INTEGER NOT NULL,
    co_jobs_nbr                    INTEGER,
    co_workers_comp_nbr            INTEGER,
    cl_e_d_groups_nbr              INTEGER,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    percentage                     NUMERIC(18,6)
);


CREATE TABLE xe_beneficiary (
    ee_beneficiary_nbr        INTEGER NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    changed_by                INTEGER NOT NULL,
    active_record             CHAR(1) NOT NULL,
    ee_benefits_nbr           INTEGER NOT NULL,
    cl_person_dependents_nbr  INTEGER,
    percentage                SMALLINT,
    primary_beneficiary       CHAR(1) NOT NULL
);


CREATE TABLE xe_benefit_payment (
    ee_benefit_payment_nbr  INTEGER NOT NULL,
    ee_nbr                  INTEGER NOT NULL,
    payment_date            DATE NOT NULL,
    period_start_date       DATE NOT NULL,
    period_end_date         DATE NOT NULL,
    void                    CHAR(1) NOT NULL,
    reason                  VARCHAR(255),
    remarks                 VARCHAR(255),
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL,
    payment_amount          NUMERIC(18,6),
    payment_number          INTEGER,
    coupon_sent_date        DATE,
    letter_sent_date        DATE,
    payment_type            CHAR(1) NOT NULL,
    benefit_deduction_type  CHAR(1) NOT NULL,
    reconciled              CHAR(1) NOT NULL,
    cl_agency_nbr           INTEGER,
    benefit_name            VARCHAR(40),
    benefit_subtype         VARCHAR(40)
);


CREATE TABLE xe_benefit_refusal (
    ee_benefit_refusal_nbr    INTEGER NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    changed_by                INTEGER NOT NULL,
    active_record             CHAR(1) NOT NULL,
    ee_nbr                    INTEGER NOT NULL,
    co_benefit_category_nbr   INTEGER NOT NULL,
    sy_hr_refusal_reason_nbr  INTEGER NOT NULL,
    home_state_nbr            INTEGER,
    sui_state_nbr             INTEGER
);


CREATE TABLE xe_benefits (
    ee_benefits_nbr              INTEGER NOT NULL,
    ee_nbr                       INTEGER NOT NULL,
    co_benefits_nbr              INTEGER,
    deduction_frequency          CHAR(1) NOT NULL,
    enrollment_status            CHAR(1) NOT NULL,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    benefit_effective_date       DATE,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    total_premium_amount         NUMERIC(18,6),
    cobra_status                 CHAR(1) NOT NULL,
    benefit_type                 CHAR(1) NOT NULL,
    co_benefit_subtype_nbr       INTEGER,
    cobra_termination_event      CHAR(1) NOT NULL,
    cobra_ss_disability          CHAR(1) NOT NULL,
    cobra_medical_participant    CHAR(1) NOT NULL,
    cobra_qualifying_event_date  DATE,
    cobra_reason_for_refusal     CHAR(1) NOT NULL,
    ee_rate                      NUMERIC(18,6),
    er_rate                      NUMERIC(18,6),
    cobra_rate                   NUMERIC(18,6),
    additional_info1             VARCHAR(40),
    additional_info2             VARCHAR(40),
    additional_info3             VARCHAR(40),
    expiration_date              TIMESTAMP,
    rate_type                    CHAR(1) NOT NULL,
    cl_e_d_groups_nbr            INTEGER
);


CREATE TABLE xe_change_request (
    ee_change_request_nbr  INTEGER NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    changed_by             INTEGER NOT NULL,
    active_record          CHAR(1) NOT NULL,
    ee_nbr                 INTEGER NOT NULL,
    request_type           CHAR(1) NOT NULL,
    request_data           BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    signature_nbr          INTEGER,
    description            VARCHAR(40)
);


CREATE TABLE xe_child_support_cases (
    ee_child_support_cases_nbr    INTEGER NOT NULL,
    ee_nbr                        INTEGER NOT NULL,
    priority_number               INTEGER NOT NULL,
    custom_case_number            VARCHAR(32),
    origination_state             CHAR(2),
    cl_agency_nbr                 INTEGER,
    arrears                       CHAR(1) NOT NULL,
    fips_code                     VARCHAR(7),
    custom_field                  VARCHAR(80),
    filler                        VARCHAR(512),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    il_med_ins_eligible           CHAR(1) NOT NULL,
    arrears_amount                NUMERIC(18,6),
    dependent_health_cl_e_ds_nbr  INTEGER
);


CREATE TABLE xe_direct_deposit (
    ee_direct_deposit_nbr   INTEGER NOT NULL,
    ee_nbr                  INTEGER NOT NULL,
    ee_bank_account_number  VARCHAR(20) NOT NULL,
    ee_aba_number           CHAR(9) NOT NULL,
    in_prenote              CHAR(1) NOT NULL,
    addenda                 VARCHAR(15),
    filler                  VARCHAR(512),
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    ee_bank_account_type    CHAR(1) NOT NULL,
    branch_identifier       VARCHAR(9),
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL,
    allow_hyphens           CHAR(1) NOT NULL,
    form_on_file            CHAR(1) NOT NULL,
    in_prenote_date         DATE
);


CREATE TABLE xe_emergency_contacts (
    ee_emergency_contacts_nbr  INTEGER NOT NULL,
    ee_nbr                     INTEGER NOT NULL,
    contact_name               VARCHAR(40) NOT NULL,
    contact_priority           INTEGER NOT NULL,
    primary_phone              VARCHAR(20) NOT NULL,
    secondary_phone            VARCHAR(20),
    pager                      VARCHAR(20),
    fax                        VARCHAR(20),
    email_address              VARCHAR(80),
    additional_info            VARCHAR(80),
    changed_by                 INTEGER NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    active_record              CHAR(1) NOT NULL
);


CREATE TABLE xe_hr_attendance (
    ee_hr_attendance_nbr        INTEGER NOT NULL,
    ee_nbr                      INTEGER NOT NULL,
    co_hr_attendance_types_nbr  INTEGER NOT NULL,
    attendance_type_number      INTEGER NOT NULL,
    period_from                 DATE,
    period_to                   DATE,
    warning_issued              CHAR(1),
    excused                     CHAR(1),
    notes                       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    hours_used                  NUMERIC(18,6)
);


CREATE TABLE xe_hr_car (
    ee_hr_car_nbr   INTEGER NOT NULL,
    ee_nbr          INTEGER NOT NULL,
    co_hr_car_nbr   INTEGER NOT NULL,
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL
);


CREATE TABLE xe_hr_co_provided_educatn (
    ee_hr_co_provided_educatn_nbr  INTEGER NOT NULL,
    ee_nbr                         INTEGER NOT NULL,
    course                         VARCHAR(40),
    date_start                     DATE,
    date_finish                    DATE,
    completed                      CHAR(1) NOT NULL,
    check_number                   INTEGER,
    date_issued                    DATE,
    taxable                        CHAR(1) NOT NULL,
    notes                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    check_issued_by                VARCHAR(40),
    cl_hr_school_nbr               INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    company_cost                   NUMERIC(18,6),
    renewal_status                 CHAR(1) NOT NULL,
    last_review_date               DATE,
    cl_hr_course_nbr               INTEGER,
    renewal_date                   DATE,
    credit_hours                   INTEGER
);


CREATE TABLE xe_hr_injury_occurrence (
    ee_hr_injury_occurrence_nbr    INTEGER NOT NULL,
    ee_nbr                         INTEGER NOT NULL,
    case_number                    VARCHAR(15) NOT NULL,
    case_status                    CHAR(1) NOT NULL,
    date_occurred                  TIMESTAMP,
    date_filed                     DATE,
    wc_claim_number                VARCHAR(10),
    co_workers_comp_nbr            INTEGER,
    first_aid                      CHAR(1),
    days_away                      INTEGER,
    days_restricted                INTEGER,
    restrictions                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    death                          CHAR(1),
    cause                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    special_notes                  BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    sy_hr_injury_codes_nbr         INTEGER,
    sy_hr_osha_anatomic_codes_nbr  INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    medical_cost                   NUMERIC(18,6),
    labor_cost                     NUMERIC(18,6),
    physician_name                 VARCHAR(40),
    hospital_name                  VARCHAR(40),
    address1                       VARCHAR(30),
    address2                       VARCHAR(30),
    city                           VARCHAR(20),
    state                          CHAR(2),
    zip_code                       VARCHAR(10),
    emergency_room                 CHAR(1) NOT NULL,
    overnight_hospitalization      CHAR(1) NOT NULL,
    employee_activity              VARCHAR(80),
    object_description             VARCHAR(80),
    date_of_death                  DATE,
    where_occured                  VARCHAR(30),
    privacy_case                   CHAR(1) NOT NULL
);


CREATE TABLE xe_hr_performance_ratings (
    ee_hr_performance_ratings_nbr  INTEGER NOT NULL,
    ee_nbr                         INTEGER NOT NULL,
    co_hr_performance_ratings_nbr  INTEGER NOT NULL,
    overall_rating                 INTEGER,
    review_date                    DATE NOT NULL,
    co_hr_supervisors_nbr          INTEGER,
    notes                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    cl_hr_reason_codes_nbr         INTEGER,
    increase_amount                NUMERIC(18,6),
    increase_type                  CHAR(1) NOT NULL,
    increase_effective_date        DATE,
    effective_date                 TIMESTAMP NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    rate_number                    INTEGER
);


CREATE TABLE xe_hr_property_tracking (
    ee_hr_property_tracking_nbr  INTEGER NOT NULL,
    ee_nbr                       INTEGER NOT NULL,
    co_hr_property_nbr           INTEGER NOT NULL,
    issued_date                  DATE,
    returned_date                DATE,
    lost_date                    DATE,
    renewal_date                 DATE,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    serial_number                VARCHAR(80)
);


CREATE TABLE xe_locals (
    ee_locals_nbr             INTEGER NOT NULL,
    ee_nbr                    INTEGER NOT NULL,
    co_local_tax_nbr          INTEGER NOT NULL,
    exempt_exclude            CHAR(1) NOT NULL,
    filler                    VARCHAR(512),
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    ee_county                 VARCHAR(20),
    ee_tax_code               VARCHAR(20),
    deduct                    CHAR(1) NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL,
    miscellaneous_amount      NUMERIC(18,6),
    percentage_of_tax_wages   NUMERIC(18,6),
    local_enabled             CHAR(1) NOT NULL,
    override_local_tax_type   CHAR(1) NOT NULL,
    override_local_tax_value  NUMERIC(18,6),
    include_in_pretax         CHAR(1) NOT NULL,
    work_address1             VARCHAR(30),
    work_address2             VARCHAR(30),
    work_city                 VARCHAR(20),
    work_state                CHAR(2),
    work_zip_code             VARCHAR(10),
    co_locations_nbr          INTEGER,
    work_address_ovr          CHAR(1) NOT NULL
);


CREATE TABLE xe_pension_fund_splits (
    ee_pension_fund_splits_nbr  INTEGER NOT NULL,
    ee_nbr                      INTEGER NOT NULL,
    cl_funds_nbr                INTEGER NOT NULL,
    cl_e_ds_nbr                 INTEGER NOT NULL,
    employee_or_employer        CHAR(1) NOT NULL,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    percentage                  NUMERIC(18,6)
);


CREATE TABLE xe_piece_work (
    ee_piece_work_nbr  INTEGER NOT NULL,
    ee_nbr             INTEGER NOT NULL,
    cl_pieces_nbr      INTEGER NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL,
    rate_quantity      NUMERIC(18,6),
    rate_amount        NUMERIC(18,6)
);


CREATE TABLE xe_rates (
    ee_rates_nbr               INTEGER NOT NULL,
    co_division_nbr            INTEGER,
    co_branch_nbr              INTEGER,
    co_department_nbr          INTEGER,
    co_team_nbr                INTEGER,
    ee_nbr                     INTEGER NOT NULL,
    rate_number                INTEGER NOT NULL,
    primary_rate               CHAR(1) NOT NULL,
    co_jobs_nbr                INTEGER,
    co_workers_comp_nbr        INTEGER,
    changed_by                 INTEGER NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    active_record              CHAR(1) NOT NULL,
    rate_amount                NUMERIC(18,6),
    co_hr_positions_nbr        INTEGER,
    co_hr_position_grades_nbr  INTEGER
);


CREATE TABLE xe_scheduled_e_ds (
    ee_scheduled_e_ds_nbr          INTEGER NOT NULL,
    ee_nbr                         INTEGER NOT NULL,
    cl_agency_nbr                  INTEGER,
    cl_e_ds_nbr                    INTEGER NOT NULL,
    cl_e_d_groups_nbr              INTEGER,
    ee_direct_deposit_nbr          INTEGER,
    plan_type                      CHAR(1) NOT NULL,
    frequency                      CHAR(1) NOT NULL,
    exclude_week_1                 CHAR(1) NOT NULL,
    exclude_week_2                 CHAR(1) NOT NULL,
    exclude_week_3                 CHAR(1) NOT NULL,
    exclude_week_4                 CHAR(1) NOT NULL,
    exclude_week_5                 CHAR(1) NOT NULL,
    effective_start_date           DATE NOT NULL,
    effective_end_date             DATE,
    min_ppp_cl_e_d_groups_nbr      INTEGER,
    max_ppp_cl_e_d_groups_nbr      INTEGER,
    target_action                  CHAR(1),
    number_of_targets_remaining    INTEGER,
    which_checks                   CHAR(1) NOT NULL,
    calculation_type               CHAR(1),
    ee_child_support_cases_nbr     INTEGER,
    child_support_state            CHAR(2),
    deduct_whole_check             CHAR(1) NOT NULL,
    expression                     BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    always_pay                     CHAR(1) NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    scheduled_e_d_groups_nbr       INTEGER,
    priority_number                INTEGER,
    garnisnment_id                 VARCHAR(30),
    threshold_e_d_groups_nbr       INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    benefit_amount_type            CHAR(1) NOT NULL,
    amount                         NUMERIC(18,6),
    percentage                     NUMERIC(18,6),
    minimum_wage_multiplier        NUMERIC(18,6),
    target_amount                  NUMERIC(18,6),
    minimum_pay_period_amount      NUMERIC(18,6),
    minimum_pay_period_percentage  NUMERIC(18,6),
    maximum_pay_period_amount      NUMERIC(18,6),
    maximum_pay_period_percentage  NUMERIC(18,6),
    annual_maximum_amount          NUMERIC(18,6),
    maximum_garnish_percent        NUMERIC(18,6),
    take_home_pay                  NUMERIC(18,6),
    balance                        NUMERIC(18,6),
    threshold_amount               NUMERIC(18,6),
    scheduled_e_d_enabled          CHAR(1) NOT NULL,
    cap_state_tax_credit           CHAR(1) NOT NULL,
    max_avg_amt_cl_e_d_groups_nbr  INTEGER,
    max_avg_hrs_cl_e_d_groups_nbr  INTEGER,
    max_average_hourly_wage_rate   NUMERIC(18,6),
    deductions_to_zero             CHAR(1) NOT NULL,
    use_pension_limit              CHAR(1) NOT NULL,
    ee_benefits_nbr                INTEGER,
    co_benefit_subtype_nbr         INTEGER
);


CREATE TABLE xe_signature (
    ee_signature_nbr  INTEGER NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    changed_by        INTEGER NOT NULL,
    active_record     CHAR(1) NOT NULL,
    ee_nbr            INTEGER NOT NULL,
    data              VARCHAR(40),
    document          BLOB SUB_TYPE 1 SEGMENT SIZE 80
);


CREATE TABLE xe_states (
    ee_states_nbr                   INTEGER NOT NULL,
    ee_nbr                          INTEGER NOT NULL,
    co_states_nbr                   INTEGER NOT NULL,
    state_number_withholding_allow  INTEGER,
    state_exempt_exclude            CHAR(1) NOT NULL,
    override_state_tax_type         CHAR(1) NOT NULL,
    sdi_apply_co_states_nbr         INTEGER,
    sui_apply_co_states_nbr         INTEGER,
    ee_sdi_exempt_exclude           CHAR(1) NOT NULL,
    er_sdi_exempt_exclude           CHAR(1) NOT NULL,
    ee_sui_exempt_exclude           CHAR(1) NOT NULL,
    er_sui_exempt_exclude           CHAR(1) NOT NULL,
    reciprocal_method               CHAR(1) NOT NULL,
    reciprocal_co_states_nbr        INTEGER,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    ee_county                       VARCHAR(20),
    ee_tax_code                     VARCHAR(20),
    imported_marital_status         CHAR(1) NOT NULL,
    calculate_taxable_wages_1099    CHAR(1) NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    override_state_tax_value        NUMERIC(18,6),
    reciprocal_amount_percentage    NUMERIC(18,6),
    sy_state_marital_status_nbr     INTEGER NOT NULL,
    state_marital_status            CHAR(2) NOT NULL,
    override_state_minimum_wage     NUMERIC(18,6),
    salary_type                     CHAR(1) NOT NULL
);


CREATE TABLE xe_time_off_accrual (
    ee_time_off_accrual_nbr         INTEGER NOT NULL,
    ee_nbr                          INTEGER NOT NULL,
    co_time_off_accrual_nbr         INTEGER NOT NULL,
    effective_accrual_date          DATE,
    next_accrue_date                DATE,
    rllovr_co_time_off_accrual_nbr  INTEGER,
    rollover_date                   DATE,
    next_reset_date                 DATE,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    current_accrued                 NUMERIC(18,6),
    current_used                    NUMERIC(18,6),
    annual_accrual_maximum          NUMERIC(18,6),
    override_rate                   NUMERIC(18,6),
    just_reset                      CHAR(1) NOT NULL,
    marked_accrued                  NUMERIC(18,6) NOT NULL,
    marked_used                     NUMERIC(18,6) NOT NULL,
    status                          CHAR(1) NOT NULL,
    rollover_freq                   CHAR(1),
    rollover_month_number           INTEGER,
    rollover_payroll_of_month       CHAR(1),
    rollover_offset                 INTEGER
);


CREATE TABLE xe_time_off_accrual_oper (
    ee_time_off_accrual_oper_nbr  INTEGER NOT NULL,
    ee_time_off_accrual_nbr       INTEGER NOT NULL,
    accrual_date                  DATE NOT NULL,
    accrued                       NUMERIC(18,6) NOT NULL,
    accrued_capped                NUMERIC(18,6),
    used                          NUMERIC(18,6) NOT NULL,
    note                          VARCHAR(40) NOT NULL,
    operation_code                CHAR(1) NOT NULL,
    adjusted_ee_toa_oper_nbr      INTEGER,
    connected_ee_toa_oper_nbr     INTEGER,
    pr_nbr                        INTEGER,
    pr_batch_nbr                  INTEGER,
    pr_check_nbr                  INTEGER,
    effective_date                TIMESTAMP NOT NULL,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    request_date                  DATE,
    cl_e_ds_nbr                   INTEGER
);


CREATE TABLE xe_work_shifts (
    ee_work_shifts_nbr  INTEGER NOT NULL,
    ee_nbr              INTEGER NOT NULL,
    co_shifts_nbr       INTEGER NOT NULL,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL,
    shift_rate          NUMERIC(18,6),
    shift_percentage    NUMERIC(18,6)
);


CREATE TABLE xr (
    pr_nbr                      INTEGER NOT NULL,
    co_nbr                      INTEGER NOT NULL,
    run_number                  INTEGER NOT NULL,
    check_date                  DATE NOT NULL,
    check_date_status           CHAR(1) NOT NULL,
    scheduled                   CHAR(1) NOT NULL,
    status                      CHAR(1) NOT NULL,
    status_date                 TIMESTAMP NOT NULL,
    exclude_ach                 CHAR(1) NOT NULL,
    exclude_tax_deposits        CHAR(1) NOT NULL,
    exclude_agency              CHAR(1) NOT NULL,
    mark_liabs_paid_default     CHAR(1) NOT NULL,
    exclude_billing             CHAR(1) NOT NULL,
    exclude_r_c_b_0r_n          CHAR(1) NOT NULL,
    scheduled_call_in_date      TIMESTAMP NOT NULL,
    actual_call_in_date         TIMESTAMP,
    scheduled_process_date      TIMESTAMP NOT NULL,
    scheduled_delivery_date     TIMESTAMP NOT NULL,
    scheduled_check_date        TIMESTAMP,
    invoice_printed             CHAR(1) NOT NULL,
    payroll_comments            BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                      VARCHAR(512),
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    process_date                TIMESTAMP,
    payroll_type                CHAR(1) NOT NULL,
    exclude_time_off            CHAR(1) NOT NULL,
    effective_date              TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL,
    approved_by_finance         CHAR(1) NOT NULL,
    approved_by_tax             CHAR(1) NOT NULL,
    approved_by_management      CHAR(1) NOT NULL,
    cr_notes                    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    same_day_pull_and_replace   CHAR(1) NOT NULL,
    miscellaneous_instructions  BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    combine_runs                CHAR(1) NOT NULL,
    combine_from_run            INTEGER,
    combine_to_run              INTEGER,
    print_all_reports           CHAR(1) NOT NULL,
    cr_approve_sb_user_nbr      INTEGER,
    trust_impound               CHAR(1) NOT NULL,
    tax_impound                 CHAR(1) NOT NULL,
    dd_impound                  CHAR(1) NOT NULL,
    billing_impound             CHAR(1) NOT NULL,
    wc_impound                  CHAR(1) NOT NULL,
    unlocked                    CHAR(1) NOT NULL,
    approved_by_finance_nbr    INTEGER,
    approved_by_tax_nbr        INTEGER,
    approved_by_management_nbr    INTEGER
);


CREATE TABLE xr_batch (
    pr_batch_nbr               INTEGER NOT NULL,
    pr_nbr                     INTEGER NOT NULL,
    co_pr_check_templates_nbr  INTEGER,
    co_pr_filters_nbr          INTEGER,
    period_begin_date          DATE NOT NULL,
    period_begin_date_status   CHAR(1) NOT NULL,
    period_end_date            DATE NOT NULL,
    period_end_date_status     CHAR(1) NOT NULL,
    frequency                  CHAR(1) NOT NULL,
    pay_salary                 CHAR(1) NOT NULL,
    pay_standard_hours         CHAR(1) NOT NULL,
    load_dbdt_defaults         CHAR(1) NOT NULL,
    filler                     VARCHAR(512),
    changed_by                 INTEGER NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    exclude_time_off           CHAR(1) NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    active_record              CHAR(1) NOT NULL
);


CREATE TABLE xr_check (
    pr_check_nbr                    INTEGER NOT NULL,
    ee_nbr                          INTEGER NOT NULL,
    pr_nbr                          INTEGER NOT NULL,
    pr_batch_nbr                    INTEGER NOT NULL,
    custom_pr_bank_acct_number      VARCHAR(20) NOT NULL,
    payment_serial_number           INTEGER NOT NULL,
    check_type                      CHAR(1) NOT NULL,
    co_delivery_package_nbr         INTEGER,
    exclude_dd                      CHAR(1) NOT NULL,
    exclude_dd_except_net           CHAR(1) NOT NULL,
    exclude_time_off_accural        CHAR(1) NOT NULL,
    exclude_auto_distribution       CHAR(1) NOT NULL,
    exclude_all_sched_e_d_codes     CHAR(1) NOT NULL,
    exclude_sch_e_d_from_agcy_chk   CHAR(1) NOT NULL,
    exclude_sch_e_d_except_pension  CHAR(1) NOT NULL,
    prorate_scheduled_e_ds          CHAR(1) NOT NULL,
    or_check_federal_type           CHAR(1) NOT NULL,
    exclude_federal                 CHAR(1) NOT NULL,
    exclude_additional_federal      CHAR(1) NOT NULL,
    exclude_employee_oasdi          CHAR(1) NOT NULL,
    exclude_employer_oasdi          CHAR(1) NOT NULL,
    exclude_employee_medicare       CHAR(1) NOT NULL,
    exclude_employer_medicare       CHAR(1) NOT NULL,
    exclude_employee_eic            CHAR(1) NOT NULL,
    exclude_employer_fui            CHAR(1) NOT NULL,
    tax_at_supplemental_rate        CHAR(1) NOT NULL,
    tax_frequency                   CHAR(1) NOT NULL,
    data_nbr                        INTEGER,
    check_status                    CHAR(1) NOT NULL,
    status_change_date              TIMESTAMP NOT NULL,
    exclude_from_agency             CHAR(1) NOT NULL,
    notes_nbr                       INTEGER,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    check_type_945                  CHAR(1) NOT NULL,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    salary                          NUMERIC(18,6),
    or_check_federal_value          NUMERIC(18,6),
    or_check_oasdi                  NUMERIC(18,6),
    or_check_medicare               NUMERIC(18,6),
    or_check_eic                    NUMERIC(18,6),
    or_check_back_up_withholding    NUMERIC(18,6),
    gross_wages                     NUMERIC(18,6),
    net_wages                       NUMERIC(18,6),
    federal_taxable_wages           NUMERIC(18,6),
    federal_tax                     NUMERIC(18,6),
    ee_oasdi_taxable_wages          NUMERIC(18,6),
    ee_oasdi_taxable_tips           NUMERIC(18,6),
    ee_oasdi_tax                    NUMERIC(18,6),
    ee_medicare_taxable_wages       NUMERIC(18,6),
    ee_medicare_tax                 NUMERIC(18,6),
    ee_eic_tax                      NUMERIC(18,6),
    er_oasdi_taxable_wages          NUMERIC(18,6),
    er_oasdi_taxable_tips           NUMERIC(18,6),
    er_oasdi_tax                    NUMERIC(18,6),
    er_medicare_taxable_wages       NUMERIC(18,6),
    er_medicare_tax                 NUMERIC(18,6),
    er_fui_taxable_wages            NUMERIC(18,6),
    er_fui_gross_wages              NUMERIC(18,6),
    er_fui_tax                      NUMERIC(18,6),
    federal_shortfall               NUMERIC(18,6),
    federal_gross_wages             NUMERIC(18,6),
    ee_oasdi_gross_wages            NUMERIC(18,6),
    er_oasdi_gross_wages            NUMERIC(18,6),
    ee_medicare_gross_wages         NUMERIC(18,6),
    er_medicare_gross_wages         NUMERIC(18,6),
    calculate_override_taxes        CHAR(1) NOT NULL,
    reciprocate_sui                 CHAR(1) NOT NULL,
    user_grouping                   INTEGER,
    disable_shortfalls              CHAR(1) NOT NULL,
    aba_number                      CHAR(9),
    update_balance                  CHAR(1) NOT NULL
);


CREATE TABLE xr_check_line_locals (
    pr_check_line_locals_nbr  INTEGER NOT NULL,
    pr_check_lines_nbr        INTEGER NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    ee_locals_nbr             INTEGER NOT NULL,
    exempt_exclude            CHAR(1) NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xr_check_lines (
    pr_check_lines_nbr             INTEGER NOT NULL,
    pr_check_nbr                   INTEGER NOT NULL,
    line_type                      CHAR(1) NOT NULL,
    line_item_date                 DATE,
    co_division_nbr                INTEGER,
    co_branch_nbr                  INTEGER,
    co_department_nbr              INTEGER,
    co_team_nbr                    INTEGER,
    co_jobs_nbr                    INTEGER,
    co_workers_comp_nbr            INTEGER,
    cl_agency_nbr                  INTEGER,
    agency_status                  CHAR(1),
    co_shifts_nbr                  INTEGER,
    cl_e_ds_nbr                    INTEGER NOT NULL,
    cl_pieces_nbr                  INTEGER,
    ee_scheduled_e_ds_nbr          INTEGER,
    filler                         VARCHAR(512),
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    ee_states_nbr                  INTEGER,
    ee_sui_states_nbr              INTEGER,
    rate_number                    INTEGER,
    pr_miscellaneous_checks_nbr    INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    rate_of_pay                    NUMERIC(18,6),
    hours_or_pieces                NUMERIC(18,6),
    amount                         NUMERIC(18,6),
    e_d_differential_amount        NUMERIC(18,6),
    deduction_shortfall_carryover  NUMERIC(18,6),
    pr_nbr                         INTEGER NOT NULL,
    user_override                  CHAR(1) NOT NULL,
    line_item_end_date             DATE,
    user_grouping                  INTEGER,
    punch_in                       TIMESTAMP,
    punch_out                      TIMESTAMP,
    reducing_cl_e_d_groups_nbr     INTEGER,
    reduced_hours                  CHAR(1) NOT NULL,
    work_address_ovr               CHAR(1) NOT NULL
);


CREATE TABLE xr_check_locals (
    pr_check_locals_nbr   INTEGER NOT NULL,
    pr_check_nbr          INTEGER NOT NULL,
    ee_locals_nbr         INTEGER NOT NULL,
    exclude_local         CHAR(1) NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL,
    local_taxable_wage    NUMERIC(18,6),
    local_tax             NUMERIC(18,6),
    override_amount       NUMERIC(18,6),
    local_shortfall       NUMERIC(18,6),
    pr_nbr                INTEGER NOT NULL,
    local_gross_wages     NUMERIC(18,6),
    co_locations_nbr      INTEGER,
    nonres_ee_locals_nbr  INTEGER,
    reportable            CHAR(1) NOT NULL
);


CREATE TABLE xr_check_states (
    pr_check_states_nbr       INTEGER NOT NULL,
    pr_check_nbr              INTEGER NOT NULL,
    ee_states_nbr             INTEGER NOT NULL,
    tax_at_supplemental_rate  CHAR(1) NOT NULL,
    exclude_state             CHAR(1) NOT NULL,
    exclude_sdi               CHAR(1) NOT NULL,
    exclude_sui               CHAR(1) NOT NULL,
    exclude_additional_state  CHAR(1) NOT NULL,
    state_override_type       CHAR(1) NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL,
    state_override_value      NUMERIC(18,6),
    ee_sdi_override           NUMERIC(18,6),
    state_shortfall           NUMERIC(18,6),
    state_taxable_wages       NUMERIC(18,6),
    state_tax                 NUMERIC(18,6),
    ee_sdi_taxable_wages      NUMERIC(18,6),
    ee_sdi_tax                NUMERIC(18,6),
    er_sdi_taxable_wages      NUMERIC(18,6),
    er_sdi_tax                NUMERIC(18,6),
    ee_sdi_shortfall          NUMERIC(18,6),
    pr_nbr                    INTEGER NOT NULL,
    state_gross_wages         NUMERIC(18,6),
    ee_sdi_gross_wages        NUMERIC(18,6),
    er_sdi_gross_wages        NUMERIC(18,6)
);


CREATE TABLE xr_check_sui (
    pr_check_sui_nbr   INTEGER NOT NULL,
    pr_check_nbr       INTEGER NOT NULL,
    co_sui_nbr         INTEGER NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL,
    sui_taxable_wages  NUMERIC(18,6),
    sui_gross_wages    NUMERIC(18,6),
    sui_tax            NUMERIC(18,6),
    override_amount    NUMERIC(18,6),
    pr_nbr             INTEGER NOT NULL
);


CREATE TABLE xr_miscellaneous_checks (
    pr_miscellaneous_checks_nbr    INTEGER NOT NULL,
    pr_nbr                         INTEGER NOT NULL,
    miscellaneous_check_type       CHAR(1) NOT NULL,
    custom_pr_bank_acct_number     VARCHAR(20) NOT NULL,
    payment_serial_number          INTEGER NOT NULL,
    check_status                   CHAR(1) NOT NULL,
    status_change_date             TIMESTAMP NOT NULL,
    eftps_tax_type                 CHAR(1),
    filler                         VARCHAR(512),
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    cl_agency_nbr                  INTEGER,
    override_information           BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    co_tax_deposits_nbr            INTEGER,
    sy_global_agency_nbr           INTEGER,
    sy_gl_agency_field_office_nbr  INTEGER,
    tax_coupon_sy_reports_nbr      INTEGER,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    miscellaneous_check_amount     NUMERIC(18,6),
    aba_number                     CHAR(9)
);


CREATE TABLE xr_reports (
    pr_reports_nbr  INTEGER NOT NULL,
    pr_nbr          INTEGER NOT NULL,
    co_reports_nbr  INTEGER NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL
);


CREATE TABLE xr_reprint_history (
    pr_reprint_history_nbr  INTEGER NOT NULL,
    pr_nbr                  INTEGER NOT NULL,
    reprint_date            TIMESTAMP NOT NULL,
    reason                  BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xr_reprint_history_detail (
    pr_reprint_history_detail_nbr  INTEGER NOT NULL,
    pr_reprint_history_nbr         INTEGER NOT NULL,
    begin_check_number             INTEGER NOT NULL,
    end_check_number               INTEGER NOT NULL,
    miscellaneous_check            CHAR(1) NOT NULL,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL
);


CREATE TABLE xr_scheduled_e_ds (
    pr_scheduled_e_ds_nbr  INTEGER NOT NULL,
    pr_nbr                 INTEGER NOT NULL,
    co_e_d_codes_nbr       INTEGER NOT NULL,
    exclude                CHAR(1) NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xr_scheduled_event (
    pr_scheduled_event_nbr   INTEGER NOT NULL,
    co_nbr                   INTEGER NOT NULL,
    pr_nbr                   INTEGER,
    status                   CHAR(1) NOT NULL,
    event_date               TIMESTAMP NOT NULL,
    scheduled_process_date   TIMESTAMP NOT NULL,
    actual_process_date      TIMESTAMP,
    scheduled_delivery_date  TIMESTAMP NOT NULL,
    actual_delivery_date     TIMESTAMP,
    scheduled_check_date     DATE,
    normal_check_date        DATE,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    notes                    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                   VARCHAR(512),
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL
);


CREATE TABLE xr_scheduled_event_batch (
    pr_scheduled_event_batch_nbr  INTEGER NOT NULL,
    pr_scheduled_event_nbr        INTEGER NOT NULL,
    period_begin_date             DATE NOT NULL,
    period_end_date               DATE NOT NULL,
    scheduled_call_in_date        TIMESTAMP NOT NULL,
    actual_call_in_date           TIMESTAMP,
    frequency                     CHAR(1) NOT NULL,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL
);


CREATE TABLE xr_services (
    pr_services_nbr  INTEGER NOT NULL,
    pr_nbr           INTEGER NOT NULL,
    co_services_nbr  INTEGER NOT NULL,
    exclude          CHAR(1) NOT NULL,
    changed_by       INTEGER NOT NULL,
    creation_date    TIMESTAMP NOT NULL,
    effective_date   TIMESTAMP NOT NULL,
    active_record    CHAR(1) NOT NULL
);


COMMIT;


/* MOVE DATA INTO TEMP TABLES */

INSERT INTO xe (ee_nbr, cl_person_nbr, co_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr,
                custom_employee_number, co_hr_applicant_nbr, co_hr_recruiters_nbr, co_hr_referrals_nbr, address1,
                address2, city, state, zip_code, county, phone1, description1, phone2, description2, phone3,
                description3, e_mail_address, time_clock_number, original_hire_date, current_hire_date,
                new_hire_report_sent, current_termination_date, current_termination_code, co_hr_supervisors_nbr,
                autopay_co_shifts_nbr, co_hr_positions_nbr, position_effective_date, position_status,
                co_hr_performance_ratings_nbr, review_date, next_review_date, pay_frequency, next_raise_date,
                next_pay_frequency, tipped_directly, ald_cl_e_d_groups_nbr, distribute_taxes, co_pay_group_nbr,
                cl_delivery_group_nbr, co_unions_nbr, eic, flsa_exempt, w2_type, w2_pension, w2_deferred_comp,
                w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi,
                exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare,
                exempt_employer_fui, override_fed_tax_type, gov_garnish_prior_child_suppt, security_clearance, badge_id,
                on_call_from, on_call_to, base_returns_on_this_ee, notes, filler, changed_by, creation_date,
                general_ledger_tag, company_or_individual_name, federal_marital_status, number_of_dependents,
                distribution_code_1099r, tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r,
                makeup_fica_on_cleanup_pr, sy_hr_eeo_nbr, secondary_notes, effective_date, active_record,
                co_pr_check_templates_nbr, generate_second_check, salary_amount, standard_hours, next_raise_amount,
                next_raise_percentage, next_raise_rate, workers_comp_wage_limit, group_term_policy_amount,
                override_fed_tax_value, calculated_salary, pr_check_mb_group_nbr, pr_ee_report_mb_group_nbr,
                pr_ee_report_sec_mb_group_nbr, tax_ee_return_mb_group_nbr, highly_compensated, corporate_officer,
                co_jobs_nbr, co_workers_comp_nbr, ee_enabled, gtl_number_of_hours, gtl_rate, auto_update_rates,
                eligible_for_rehire, selfserve_enabled, selfserve_username, selfserve_password, selfserve_last_login,
                print_voucher, healthcare_coverage, fui_rate_credit_override, override_federal_minimum_wage,
                wc_wage_limit_frequency, login_question1, login_answer1, login_question2, login_answer2,
                login_question3, login_answer3, co_benefit_discount_nbr, benefits_enabled, existing_patient, pcp,
                time_off_enabled, dependent_benefits_available, dependent_benefits_avail_date,
                benefit_enrollment_complete, last_qual_benefit_event, last_qual_benefit_event_date, w2, w2_form_on_file,
                benefit_e_mail_address, co_hr_position_grades_nbr, sec_question1, sec_answer1, sec_question2,
                sec_answer2)
SELECT ee_nbr, cl_person_nbr, co_nbr, co_division_nbr, co_branch_nbr, co_department_nbr, co_team_nbr,
       custom_employee_number, co_hr_applicant_nbr, co_hr_recruiters_nbr, co_hr_referrals_nbr, address1, address2, city,
       state, zip_code, county, phone1, description1, phone2, description2, phone3, description3, e_mail_address,
       time_clock_number, original_hire_date, current_hire_date, new_hire_report_sent, current_termination_date,
       current_termination_code, co_hr_supervisors_nbr, autopay_co_shifts_nbr, co_hr_positions_nbr,
       position_effective_date, position_status, co_hr_performance_ratings_nbr, review_date, next_review_date,
       pay_frequency, next_raise_date, next_pay_frequency, tipped_directly, ald_cl_e_d_groups_nbr, distribute_taxes,
       co_pay_group_nbr, cl_delivery_group_nbr, co_unions_nbr, eic, flsa_exempt, w2_type, w2_pension, w2_deferred_comp,
       w2_deceased, w2_statutory_employee, w2_legal_rep, home_tax_ee_states_nbr, exempt_employee_oasdi,
       exempt_employee_medicare, exempt_exclude_ee_fed, exempt_employer_oasdi, exempt_employer_medicare,
       exempt_employer_fui, override_fed_tax_type, gov_garnish_prior_child_suppt, security_clearance, badge_id,
       on_call_from, on_call_to, base_returns_on_this_ee, notes, filler, changed_by, creation_date, general_ledger_tag,
       company_or_individual_name, federal_marital_status, number_of_dependents, distribution_code_1099r,
       tax_amt_determined_1099r, total_distribution_1099r, pension_plan_1099r, makeup_fica_on_cleanup_pr, sy_hr_eeo_nbr,
       secondary_notes, effective_date, active_record, co_pr_check_templates_nbr, generate_second_check, salary_amount,
       standard_hours, next_raise_amount, next_raise_percentage, next_raise_rate, workers_comp_wage_limit,
       group_term_policy_amount, override_fed_tax_value, calculated_salary, pr_check_mb_group_nbr,
       pr_ee_report_mb_group_nbr, pr_ee_report_sec_mb_group_nbr, tax_ee_return_mb_group_nbr, highly_compensated,
       corporate_officer, co_jobs_nbr, co_workers_comp_nbr, ee_enabled, gtl_number_of_hours, gtl_rate,
       auto_update_rates, eligible_for_rehire, selfserve_enabled, selfserve_username, selfserve_password,
       selfserve_last_login, print_voucher, healthcare_coverage, fui_rate_credit_override,
       override_federal_minimum_wage, wc_wage_limit_frequency, login_question1, login_answer1, login_question2,
       login_answer2, login_question3, login_answer3, co_benefit_discount_nbr, benefits_enabled, existing_patient, pcp,
       time_off_enabled, dependent_benefits_available, dependent_benefits_avail_date, benefit_enrollment_complete,
       last_qual_benefit_event, last_qual_benefit_event_date, w2, w2_form_on_file, benefit_e_mail_address,
       co_hr_position_grades_nbr, sec_question1, sec_answer1, sec_question2, sec_answer2
FROM ee;
COMMIT;
DROP TABLE ee;
COMMIT;

INSERT INTO xe_additional_info SELECT * FROM ee_additional_info;
COMMIT;
DROP TABLE ee_additional_info;
COMMIT;

INSERT INTO xe_autolabor_distribution SELECT * FROM ee_autolabor_distribution;
COMMIT;
DROP TABLE ee_autolabor_distribution;
COMMIT;

INSERT INTO xe_beneficiary SELECT * FROM ee_beneficiary;
COMMIT;
DROP TABLE ee_beneficiary;
COMMIT;

INSERT INTO xe_benefits SELECT * FROM ee_benefits;
COMMIT;
DROP TABLE ee_benefits;
COMMIT;

INSERT INTO xe_benefit_payment SELECT * FROM ee_benefit_payment;
COMMIT;
DROP TABLE ee_benefit_payment;
COMMIT;

INSERT INTO xe_benefit_refusal SELECT * FROM ee_benefit_refusal;
COMMIT;
DROP TABLE ee_benefit_refusal;
COMMIT;

INSERT INTO xe_change_request SELECT * FROM ee_change_request;
COMMIT;
DROP TABLE ee_change_request;
COMMIT;

INSERT INTO xe_child_support_cases SELECT * FROM ee_child_support_cases;
COMMIT;
DROP TABLE ee_child_support_cases;
COMMIT;

INSERT INTO xe_direct_deposit SELECT * FROM ee_direct_deposit;
COMMIT;
DROP TABLE ee_direct_deposit;
COMMIT;

INSERT INTO xe_emergency_contacts SELECT * FROM ee_emergency_contacts;
COMMIT;
DROP TABLE ee_emergency_contacts;
COMMIT;

INSERT INTO xe_hr_attendance SELECT * FROM ee_hr_attendance;
COMMIT;
DROP TABLE ee_hr_attendance;
COMMIT;

INSERT INTO xe_hr_car SELECT * FROM ee_hr_car;
COMMIT;
DROP TABLE ee_hr_car;
COMMIT;

INSERT INTO xe_hr_co_provided_educatn SELECT * FROM ee_hr_co_provided_educatn;
COMMIT;
DROP TABLE ee_hr_co_provided_educatn;
COMMIT;

INSERT INTO xe_hr_injury_occurrence SELECT * FROM ee_hr_injury_occurrence;
COMMIT;
DROP TABLE ee_hr_injury_occurrence;
COMMIT;

INSERT INTO xe_hr_performance_ratings SELECT * FROM ee_hr_performance_ratings;
COMMIT;
DROP TABLE ee_hr_performance_ratings;
COMMIT;

INSERT INTO xe_hr_property_tracking SELECT * FROM ee_hr_property_tracking;
COMMIT;
DROP TABLE ee_hr_property_tracking;
COMMIT;

INSERT INTO xe_locals SELECT * FROM ee_locals;
COMMIT;
DROP TABLE ee_locals;
COMMIT;

INSERT INTO xe_pension_fund_splits SELECT * FROM ee_pension_fund_splits;
COMMIT;
DROP TABLE ee_pension_fund_splits;
COMMIT;

INSERT INTO xe_piece_work SELECT * FROM ee_piece_work;
COMMIT;
DROP TABLE ee_piece_work;
COMMIT;

INSERT INTO xe_rates SELECT * FROM ee_rates;
COMMIT;
DROP TABLE ee_rates;
COMMIT;

INSERT INTO xe_scheduled_e_ds SELECT * FROM ee_scheduled_e_ds;
COMMIT;
DROP TABLE ee_scheduled_e_ds;
COMMIT;

INSERT INTO xe_signature SELECT * FROM ee_signature;
COMMIT;
DROP TABLE ee_signature;
COMMIT;

INSERT INTO xe_states SELECT * FROM ee_states;
COMMIT;
DROP TABLE ee_states;
COMMIT;

INSERT INTO xe_time_off_accrual (ee_time_off_accrual_nbr, ee_nbr, co_time_off_accrual_nbr, effective_accrual_date,
                                 next_accrue_date, rllovr_co_time_off_accrual_nbr, rollover_date, next_reset_date,
                                 changed_by, creation_date, effective_date, active_record, current_accrued,
                                 current_used, annual_accrual_maximum, override_rate, just_reset, marked_accrued,
                                 marked_used, status, rollover_freq, rollover_month_number, rollover_payroll_of_month,
                                 rollover_offset)
SELECT ee_time_off_accrual_nbr, ee_nbr, co_time_off_accrual_nbr, effective_accrual_date, next_accrue_date,
       rllovr_co_time_off_accrual_nbr, rollover_date, next_reset_date, changed_by, creation_date, effective_date,
       active_record, current_accrued, current_used, annual_accrual_maximum, override_rate, just_reset, marked_accrued,
       marked_used, status, rollover_freq, rollover_month_number, rollover_payroll_of_month, rollover_offset
FROM ee_time_off_accrual;
COMMIT;
DROP TABLE ee_time_off_accrual;
COMMIT;

INSERT INTO xe_time_off_accrual_oper SELECT * FROM ee_time_off_accrual_oper;
COMMIT;
DROP TABLE ee_time_off_accrual_oper;
COMMIT;

INSERT INTO xe_work_shifts SELECT * FROM ee_work_shifts;
COMMIT;
DROP TABLE ee_work_shifts;
COMMIT;

INSERT INTO xl (cl_nbr, custom_client_number, name, address1, address2, city, state, zip_code, contact1, phone1,
                description1, contact2, phone2, description2, fax1, fax1_description, fax2, fax2_description,
                e_mail_address1, e_mail_address2, sb_accountant_number, accountant_contact, print_cpa, leasing_company,
                reciprocate_sui, security_font, auto_save_minutes, customer_service_sb_user_nbr, csr_sb_team_nbr,
                start_date, termination_date, termination_code, termination_notes, filler, changed_by, creation_date,
                enable_hr, effective_date, active_record, agency_check_mb_group_nbr, pr_check_mb_group_nbr,
                pr_report_mb_group_nbr, pr_report_second_mb_group_nbr, tax_check_mb_group_nbr,
                tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr, tax_return_second_mb_group_nbr, reciprocate_state,
                phone1_ext, phone2_ext, print_client_name, maintenance_hold, read_only, block_invalid_ssn)
SELECT cl_nbr, custom_client_number, name, address1, address2, city, state, zip_code, contact1, phone1, description1,
       contact2, phone2, description2, fax1, fax1_description, fax2, fax2_description, e_mail_address1, e_mail_address2,
       sb_accountant_number, accountant_contact, print_cpa, leasing_company, reciprocate_sui, security_font,
       auto_save_minutes, customer_service_sb_user_nbr, csr_sb_team_nbr, start_date, termination_date, termination_code,
       termination_notes, filler, changed_by, creation_date, enable_hr, effective_date, active_record,
       agency_check_mb_group_nbr, pr_check_mb_group_nbr, pr_report_mb_group_nbr, pr_report_second_mb_group_nbr,
       tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr, tax_return_second_mb_group_nbr,
       reciprocate_state, phone1_ext, phone2_ext, print_client_name, maintenance_hold, read_only, block_invalid_ssn
FROM cl;
COMMIT;
DROP TABLE cl;
COMMIT;

INSERT INTO xl_3_party_sick_pay_admin SELECT * FROM cl_3_party_sick_pay_admin;
COMMIT;
DROP TABLE cl_3_party_sick_pay_admin;
COMMIT;

INSERT INTO xl_agency (cl_agency_nbr, sb_agency_nbr, report_method, frequency, month_number, cl_bank_account_nbr,
                       payment_type, cl_delivery_group_nbr, client_agency_custom_field, notes, filler, creation_date,
                       changed_by, gl_tag, effective_date, active_record, gl_offset_tag, cl_mail_box_group_nbr,
                       check_memo_line_1, check_memo_line_2)
SELECT cl_agency_nbr, sb_agency_nbr, report_method, frequency, month_number, cl_bank_account_nbr, payment_type,
       cl_delivery_group_nbr, client_agency_custom_field, notes, filler, creation_date, changed_by, gl_tag,
       effective_date, active_record, gl_offset_tag, cl_mail_box_group_nbr, check_memo_line_1, check_memo_line_2
FROM cl_agency;
COMMIT;
DROP TABLE cl_agency;
COMMIT;

INSERT INTO xl_bank_account SELECT * FROM cl_bank_account;
COMMIT;
DROP TABLE cl_bank_account;
COMMIT;

INSERT INTO xl_billing SELECT * FROM cl_billing;
COMMIT;
DROP TABLE cl_billing;
COMMIT;

INSERT INTO xl_blob (cl_blob_nbr, data, changed_by, creation_date, effective_date, active_record) SELECT cl_blob_nbr, data, 0, change_date, CAST('1/1/2000' AS TIMESTAMP), 'C' FROM cl_blob;
COMMIT;
DROP TABLE cl_blob;
COMMIT;

INSERT INTO xl_common_paymaster SELECT * FROM cl_common_paymaster;
COMMIT;
DROP TABLE cl_common_paymaster;
COMMIT;

INSERT INTO xl_co_consolidation (cl_co_consolidation_nbr, description, changed_by, creation_date, primary_co_nbr,
                                 effective_date, active_record, consolidation_type, scope)
SELECT cl_co_consolidation_nbr, description, changed_by, creation_date, primary_co_nbr, effective_date, active_record,
       consolidation_type, scope
FROM cl_co_consolidation;
COMMIT;
DROP TABLE cl_co_consolidation;
COMMIT;

INSERT INTO xl_delivery_group SELECT * FROM cl_delivery_group;
COMMIT;
DROP TABLE cl_delivery_group;
COMMIT;

INSERT INTO xl_delivery_method SELECT * FROM cl_delivery_method;
COMMIT;
DROP TABLE cl_delivery_method;
COMMIT;

INSERT INTO xl_e_ds (cl_e_ds_nbr, custom_e_d_code_number, e_d_code_type, description, cl_e_d_groups_nbr, skip_hours,
                     override_ee_rate_number, override_fed_tax_type, update_hours, default_cl_agency_nbr,
                     ee_exempt_exclude_oasdi, ee_exempt_exclude_medicare, ee_exempt_exclude_federal,
                     ee_exempt_exclude_eic, er_exempt_exclude_oasdi, er_exempt_exclude_medicare, er_exempt_exclude_fui,
                     w2_box, min_cl_e_d_groups_nbr, max_cl_e_d_groups_nbr, cl_union_nbr, cl_pension_nbr,
                     priority_to_exclude, make_up_deduction_shortfall, ot_all_cl_e_d_groups_nbr,
                     piece_cl_e_d_groups_nbr, pw_min_wage_make_up_method, tip_minimum_wage_make_up_type,
                     offset_cl_e_ds_nbr, cl_3_party_sick_pay_admin_nbr, show_ytd_on_checks, scheduled_defaults,
                     sd_frequency, sd_effective_start_date, sd_calculation_method, sd_expression, sd_auto, sd_round,
                     sd_exclude_week_1, sd_exclude_week_2, sd_exclude_week_3, sd_exclude_week_4, sd_exclude_week_5,
                     filler, changed_by, creation_date, override_rate_type, show_on_input_worksheet, sy_state_nbr,
                     gl_offset_tag, show_ed_on_checks, effective_date, active_record, notes, override_rate,
                     override_fed_tax_value, pay_period_minimum_amount, pay_period_minimum_percent_of,
                     pay_period_maximum_amount, pay_period_maximum_percent_of, annual_maximum, match_first_percentage,
                     match_first_amount, match_second_percentage, match_second_amount, flat_match_amount,
                     regular_ot_rate, piecework_minimum_wage, sd_amount, sd_rate, apply_before_taxes, default_hours,
                     show_which_balance, overstate_hours_for_overtime, which_checks, month_number, sd_plan_type,
                     sd_which_checks, sd_priority_number, sd_always_pay, sd_deductions_to_zero, sd_max_avg_amt_grp_nbr,
                     sd_max_avg_hrs_grp_nbr, sd_max_average_hourly_wage_rate, sd_threshold_e_d_groups_nbr,
                     sd_threshold_amount, sd_use_pension_limit, sd_deduct_whole_check, target_description,
                     cobra_eligible, show_on_report)
SELECT cl_e_ds_nbr, custom_e_d_code_number, e_d_code_type, description, cl_e_d_groups_nbr, skip_hours,
       override_ee_rate_number, override_fed_tax_type, update_hours, default_cl_agency_nbr, ee_exempt_exclude_oasdi,
       ee_exempt_exclude_medicare, ee_exempt_exclude_federal, ee_exempt_exclude_eic, er_exempt_exclude_oasdi,
       er_exempt_exclude_medicare, er_exempt_exclude_fui, w2_box, min_cl_e_d_groups_nbr, max_cl_e_d_groups_nbr,
       cl_union_nbr, cl_pension_nbr, priority_to_exclude, make_up_deduction_shortfall, ot_all_cl_e_d_groups_nbr,
       piece_cl_e_d_groups_nbr, pw_min_wage_make_up_method, tip_minimum_wage_make_up_type, offset_cl_e_ds_nbr,
       cl_3_party_sick_pay_admin_nbr, show_ytd_on_checks, scheduled_defaults, sd_frequency, sd_effective_start_date,
       sd_calculation_method, sd_expression, sd_auto, sd_round, sd_exclude_week_1, sd_exclude_week_2, sd_exclude_week_3,
       sd_exclude_week_4, sd_exclude_week_5, filler, changed_by, creation_date, override_rate_type,
       show_on_input_worksheet, sy_state_nbr, gl_offset_tag, show_ed_on_checks, effective_date, active_record, notes,
       override_rate, override_fed_tax_value, pay_period_minimum_amount, pay_period_minimum_percent_of,
       pay_period_maximum_amount, pay_period_maximum_percent_of, annual_maximum, match_first_percentage,
       match_first_amount, match_second_percentage, match_second_amount, flat_match_amount, regular_ot_rate,
       piecework_minimum_wage, sd_amount, sd_rate, apply_before_taxes, default_hours, show_which_balance,
       overstate_hours_for_overtime, which_checks, month_number, sd_plan_type, sd_which_checks, sd_priority_number,
       sd_always_pay, sd_deductions_to_zero, sd_max_avg_amt_grp_nbr, sd_max_avg_hrs_grp_nbr,
       sd_max_average_hourly_wage_rate, sd_threshold_e_d_groups_nbr, sd_threshold_amount, sd_use_pension_limit,
       sd_deduct_whole_check, target_description, cobra_eligible, show_on_report
FROM cl_e_ds;
COMMIT;
DROP TABLE cl_e_ds;
COMMIT;

INSERT INTO xl_e_d_groups SELECT * FROM cl_e_d_groups;
COMMIT;
DROP TABLE cl_e_d_groups;
COMMIT;

INSERT INTO xl_e_d_group_codes SELECT * FROM cl_e_d_group_codes;
COMMIT;
DROP TABLE cl_e_d_group_codes;
COMMIT;

INSERT INTO xl_e_d_local_exmpt_excld SELECT * FROM cl_e_d_local_exmpt_excld;
COMMIT;
DROP TABLE cl_e_d_local_exmpt_excld;
COMMIT;

INSERT INTO xl_e_d_state_exmpt_excld SELECT * FROM cl_e_d_state_exmpt_excld;
COMMIT;
DROP TABLE cl_e_d_state_exmpt_excld;
COMMIT;

INSERT INTO xl_funds SELECT * FROM cl_funds;
COMMIT;
DROP TABLE cl_funds;
COMMIT;

INSERT INTO xl_hr_course SELECT * FROM cl_hr_course;
COMMIT;
DROP TABLE cl_hr_course;
COMMIT;

INSERT INTO xl_hr_person_education SELECT * FROM cl_hr_person_education;
COMMIT;
DROP TABLE cl_hr_person_education;
COMMIT;

INSERT INTO xl_hr_person_handicaps SELECT * FROM cl_hr_person_handicaps;
COMMIT;
DROP TABLE cl_hr_person_handicaps;
COMMIT;

INSERT INTO xl_hr_person_skills SELECT * FROM cl_hr_person_skills;
COMMIT;
DROP TABLE cl_hr_person_skills;
COMMIT;

INSERT INTO xl_hr_reason_codes SELECT * FROM cl_hr_reason_codes;
COMMIT;
DROP TABLE cl_hr_reason_codes;
COMMIT;

INSERT INTO xl_hr_school SELECT * FROM cl_hr_school;
COMMIT;
DROP TABLE cl_hr_school;
COMMIT;

INSERT INTO xl_hr_skills SELECT * FROM cl_hr_skills;
COMMIT;
DROP TABLE cl_hr_skills;
COMMIT;

INSERT INTO xl_mail_box_group SELECT * FROM cl_mail_box_group;
COMMIT;
DROP TABLE cl_mail_box_group;
COMMIT;

INSERT INTO xl_mail_box_group_option SELECT * FROM cl_mail_box_group_option;
COMMIT;
DROP TABLE cl_mail_box_group_option;
COMMIT;

INSERT INTO xl_pension SELECT * FROM cl_pension;
COMMIT;
DROP TABLE cl_pension;
COMMIT;

INSERT INTO xl_person SELECT * FROM cl_person;
COMMIT;
DROP TABLE cl_person;
COMMIT;

INSERT INTO xl_person_dependents SELECT * FROM cl_person_dependents;
COMMIT;
DROP TABLE cl_person_dependents;
COMMIT;

INSERT INTO xl_person_documents SELECT * FROM cl_person_documents;
COMMIT;
DROP TABLE cl_person_documents;
COMMIT;

INSERT INTO xl_pieces SELECT * FROM cl_pieces;
COMMIT;
DROP TABLE cl_pieces;
COMMIT;

INSERT INTO xl_report_writer_reports SELECT * FROM cl_report_writer_reports;
COMMIT;
DROP TABLE cl_report_writer_reports;
COMMIT;

INSERT INTO xl_timeclock_imports SELECT * FROM cl_timeclock_imports;
COMMIT;
DROP TABLE cl_timeclock_imports;
COMMIT;

INSERT INTO xl_union SELECT * FROM cl_union;
COMMIT;
DROP TABLE cl_union;
COMMIT;

INSERT INTO xl_union_dues SELECT * FROM cl_union_dues;
COMMIT;
DROP TABLE cl_union_dues;
COMMIT;


INSERT INTO xo (co_nbr, custom_company_number, name, dba, address1, address2, city, state, zip_code, county, legal_name,
                legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, e_mail_address, referred_by,
                sb_referrals_nbr, start_date, termination_date, termination_code, termination_notes, sb_accountant_nbr,
                accountant_contact, print_cpa, union_cl_e_ds_nbr, cl_common_paymaster_nbr, cl_co_consolidation_nbr,
                fein, remote, business_start_date, business_type, corporation_type, nature_of_business, restaurant,
                days_open, time_open, time_close, company_notes, successor_company, medical_plan, retirement_age,
                pay_frequencies, general_ledger_format_string, cl_billing_nbr, dbdt_level, billing_level,
                bank_account_level, billing_sb_bank_account_nbr, tax_sb_bank_account_nbr, payroll_cl_bank_account_nbr,
                debit_number_days_prior_pr, tax_cl_bank_account_nbr, debit_number_of_days_prior_tax,
                billing_cl_bank_account_nbr, debit_number_days_prior_bill, dd_cl_bank_account_nbr,
                debit_number_of_days_prior_dd, home_co_states_nbr, home_sdi_co_states_nbr, home_sui_co_states_nbr,
                tax_service, tax_service_start_date, tax_service_end_date, use_dba_on_tax_return,
                workers_comp_cl_agency_nbr, workers_comp_policy_id, w_comp_cl_bank_account_nbr,
                debit_number_days_prior_wc, w_comp_sb_bank_account_nbr, eftps_enrollment_status, eftps_enrollment_date,
                eftps_sequence_number, eftps_enrollment_number, eftps_name, pin_number, ach_sb_bank_account_nbr,
                trust_service, trust_sb_account_nbr, trust_service_start_date, trust_service_end_date, obc,
                sb_obc_account_nbr, obc_start_date, obc_end_date, sy_fed_reporting_agency_nbr,
                sy_fed_tax_payment_agency_nbr, federal_tax_deposit_frequency, federal_tax_transfer_method,
                federal_tax_payment_method, fui_sy_tax_payment_agency, fui_sy_tax_report_agency, external_tax_export, non_profit,
                federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi, fed_tax_exempt_ee_medicare,
                fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt, primary_sort_field,
                secondary_sort_field, fiscal_year_end, third_party_in_gross_pr_report, sic_code, deductions_to_zero,
                autopay_company, pay_frequency_hourly_default, pay_frequency_salary_default, co_check_primary_sort,
                co_check_secondary_sort, cl_delivery_group_nbr, annual_cl_delivery_group_nbr,
                quarter_cl_delivery_group_nbr, smoker_default, auto_labor_dist_show_deducts, auto_labor_dist_level,
                distribute_deductions_default, co_workers_comp_nbr, withholding_default, apply_misc_limit_to_1099,
                check_type, make_up_tax_deduct_shortfalls, show_shifts_on_check, show_ytd_on_check,
                show_ein_number_on_check, show_ss_number_on_check, time_off_accrual, check_form,
                print_manual_check_stubs, credit_hold, cl_timeclock_imports_nbr, payroll_password, remote_of_client,
                hardware_o_s, network, modem_speed, modem_connection_type, network_administrator,
                network_administrator_phone, network_admin_phone_type, external_network_administrator,
                transmission_destination, last_call_in_date, setup_completed,
                first_monthly_payroll_day, second_monthly_payroll_day, filler, changed_by, creation_date,
                collate_checks, process_priority, check_message, customer_service_sb_user_nbr, invoice_notes,
                tax_cover_letter_notes, final_tax_return, general_ledger_tag, billing_general_ledger_tag,
                federal_general_ledger_tag, ee_oasdi_general_ledger_tag, er_oasdi_general_ledger_tag,
                ee_medicare_general_ledger_tag, er_medicare_general_ledger_tag, fui_general_ledger_tag,
                eic_general_ledger_tag, backup_w_general_ledger_tag, net_pay_general_ledger_tag,
                tax_imp_general_ledger_tag, trust_imp_general_ledger_tag, thrd_p_tax_general_ledger_tag,
                thrd_p_chk_general_ledger_tag, trust_chk_general_ledger_tag, federal_offset_gl_tag,
                ee_oasdi_offset_gl_tag, er_oasdi_offset_gl_tag, ee_medicare_offset_gl_tag, er_medicare_offset_gl_tag,
                fui_offset_gl_tag, eic_offset_gl_tag, backup_w_offset_gl_tag, trust_imp_offset_gl_tag,
                billing_exp_gl_tag, er_oasdi_exp_gl_tag, er_medicare_exp_gl_tag, auto_enlist, calculate_locals_first,
                weekend_action, last_fui_correction, last_sui_correction, last_quarter_end_correction, last_preprocess,
                last_preprocess_message, charge_cobra_admin_fee, cobra_fee_day_of_month_due, cobra_notification_days,
                cobra_eligibility_confirm_days, show_rates_on_checks, show_dir_dep_nbr_on_checks,
                fed_943_tax_deposit_frequency, fed_945_tax_deposit_frequency, effective_date, active_record,
                impound_workers_comp, reverse_check_printing, lock_date, fui_rate_override, auto_increment,
                maximum_group_term_life, payrate_precision, average_hours, maximum_hours_on_check,
                maximum_dollars_on_check, discount_rate, mod_rate, minimum_tax_threshold, ss_disability_admin_fee,
                check_time_off_avail, agency_check_mb_group_nbr, pr_check_mb_group_nbr, pr_report_mb_group_nbr,
                pr_report_second_mb_group_nbr, tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr,
                tax_return_mb_group_nbr, tax_return_second_mb_group_nbr, name_on_invoice, misc_check_form,
                hold_return_queue, number_of_invoice_copies, prorate_flat_fee_for_dbdt, initial_effective_date,
                sb_other_service_nbr, break_checks_by_dbdt, summarize_sui, show_shortfall_check,
                trust_chk_offset_gl_tag, manual_cl_bank_account_nbr, wells_fargo_ach_flag, billing_check_cpa,
                sales_tax_percentage, exclude_r_c_b_0r_n, calculate_states_first, invoice_discount, discount_start_date,
                discount_end_date, show_time_clock_punch, auto_rd_dflt_cl_e_d_groups_nbr, auto_reduction_default_ees,
                vt_healthcare_gl_tag, vt_healthcare_offset_gl_tag, ui_rounding_gl_tag, ui_rounding_offset_gl_tag,
                reprint_to_balance, show_manual_checks_in_ess, payroll_requires_mgr_approval, days_prior_to_check_date,
                wc_offs_bank_account_nbr, qtr_lock_for_tax_pmts, co_max_amount_for_payroll,
                co_max_amount_for_tax_impound, co_max_amount_for_dd, co_payroll_process_limitations,
                co_ach_process_limitations, trust_impound, tax_impound, dd_impound, billing_impound, wc_impound,
                cobra_credit_gl_tag, co_exception_payment_type, last_tax_return, enable_hr, enable_ess,
                duns_and_bradstreet, bank_account_register_name, enable_benefits, enable_time_off, employer_type,
                wc_fiscal_year_begin, show_paystubs_ess_days, co_locations_nbr)
SELECT co_nbr, custom_company_number, name, dba, address1, address2, city, state, zip_code, county, legal_name,
       legal_address1, legal_address2, legal_city, legal_state, legal_zip_code, e_mail_address, referred_by,
       sb_referrals_nbr, start_date, termination_date, termination_code, termination_notes, sb_accountant_nbr,
       accountant_contact, print_cpa, union_cl_e_ds_nbr, cl_common_paymaster_nbr, cl_co_consolidation_nbr, fein, remote,
       business_start_date, business_type, corporation_type, nature_of_business, restaurant, days_open, time_open,
       time_close, company_notes, successor_company, medical_plan, retirement_age, pay_frequencies,
       general_ledger_format_string, cl_billing_nbr, dbdt_level, billing_level, bank_account_level,
       billing_sb_bank_account_nbr, tax_sb_bank_account_nbr, payroll_cl_bank_account_nbr, debit_number_days_prior_pr,
       tax_cl_bank_account_nbr, debit_number_of_days_prior_tax, billing_cl_bank_account_nbr,
       debit_number_days_prior_bill, dd_cl_bank_account_nbr, debit_number_of_days_prior_dd, home_co_states_nbr,
       home_sdi_co_states_nbr, home_sui_co_states_nbr, tax_service, tax_service_start_date, tax_service_end_date,
       use_dba_on_tax_return, workers_comp_cl_agency_nbr, workers_comp_policy_id, w_comp_cl_bank_account_nbr,
       debit_number_days_prior_wc, w_comp_sb_bank_account_nbr, eftps_enrollment_status, eftps_enrollment_date,
       eftps_sequence_number, eftps_enrollment_number, eftps_name, pin_number, ach_sb_bank_account_nbr, trust_service,
       trust_sb_account_nbr, trust_service_start_date, trust_service_end_date, obc, sb_obc_account_nbr, obc_start_date,
       obc_end_date, sy_fed_reporting_agency_nbr, sy_fed_tax_payment_agency_nbr, federal_tax_deposit_frequency,
       federal_tax_transfer_method, federal_tax_payment_method, fui_sy_tax_payment_agency, fui_sy_tax_report_agency, external_tax_export,
       non_profit, federal_tax_exempt_status, fed_tax_exempt_ee_oasdi, fed_tax_exempt_er_oasdi,
       fed_tax_exempt_ee_medicare, fed_tax_exempt_er_medicare, fui_tax_deposit_frequency, fui_tax_exempt,
       primary_sort_field, secondary_sort_field, fiscal_year_end, third_party_in_gross_pr_report, sic_code,
       deductions_to_zero, autopay_company, pay_frequency_hourly_default, pay_frequency_salary_default,
       co_check_primary_sort, co_check_secondary_sort, cl_delivery_group_nbr, annual_cl_delivery_group_nbr,
       quarter_cl_delivery_group_nbr, smoker_default, auto_labor_dist_show_deducts, auto_labor_dist_level,
       distribute_deductions_default, co_workers_comp_nbr, withholding_default, apply_misc_limit_to_1099, check_type,
       make_up_tax_deduct_shortfalls, show_shifts_on_check, show_ytd_on_check, show_ein_number_on_check,
       show_ss_number_on_check, time_off_accrual, check_form, print_manual_check_stubs, credit_hold,
       cl_timeclock_imports_nbr, payroll_password, remote_of_client, hardware_o_s, network, modem_speed,
       modem_connection_type, network_administrator, network_administrator_phone, network_admin_phone_type,
       external_network_administrator, transmission_destination,  last_call_in_date,
       setup_completed, first_monthly_payroll_day, second_monthly_payroll_day, filler, changed_by, creation_date,
       collate_checks, process_priority, check_message, customer_service_sb_user_nbr, invoice_notes,
       tax_cover_letter_notes, final_tax_return, general_ledger_tag, billing_general_ledger_tag,
       federal_general_ledger_tag, ee_oasdi_general_ledger_tag, er_oasdi_general_ledger_tag,
       ee_medicare_general_ledger_tag, er_medicare_general_ledger_tag, fui_general_ledger_tag, eic_general_ledger_tag,
       backup_w_general_ledger_tag, net_pay_general_ledger_tag, tax_imp_general_ledger_tag,
       trust_imp_general_ledger_tag, thrd_p_tax_general_ledger_tag, thrd_p_chk_general_ledger_tag,
       trust_chk_general_ledger_tag, federal_offset_gl_tag, ee_oasdi_offset_gl_tag, er_oasdi_offset_gl_tag,
       ee_medicare_offset_gl_tag, er_medicare_offset_gl_tag, fui_offset_gl_tag, eic_offset_gl_tag,
       backup_w_offset_gl_tag, trust_imp_offset_gl_tag, billing_exp_gl_tag, er_oasdi_exp_gl_tag, er_medicare_exp_gl_tag,
       auto_enlist, calculate_locals_first, weekend_action, last_fui_correction, last_sui_correction,
       last_quarter_end_correction, last_preprocess, last_preprocess_message, charge_cobra_admin_fee,
       cobra_fee_day_of_month_due, cobra_notification_days, cobra_eligibility_confirm_days, show_rates_on_checks,
       show_dir_dep_nbr_on_checks, fed_943_tax_deposit_frequency, fed_945_tax_deposit_frequency, effective_date,
       active_record, impound_workers_comp, reverse_check_printing, lock_date, fui_rate_override, auto_increment,
       maximum_group_term_life, payrate_precision, average_hours, maximum_hours_on_check, maximum_dollars_on_check,
       discount_rate, mod_rate, minimum_tax_threshold, ss_disability_admin_fee, check_time_off_avail,
       agency_check_mb_group_nbr, pr_check_mb_group_nbr, pr_report_mb_group_nbr, pr_report_second_mb_group_nbr,
       tax_check_mb_group_nbr, tax_ee_return_mb_group_nbr, tax_return_mb_group_nbr, tax_return_second_mb_group_nbr,
       name_on_invoice, misc_check_form, hold_return_queue, number_of_invoice_copies, prorate_flat_fee_for_dbdt,
       initial_effective_date, sb_other_service_nbr, break_checks_by_dbdt, summarize_sui, show_shortfall_check,
       trust_chk_offset_gl_tag, manual_cl_bank_account_nbr, wells_fargo_ach_flag, billing_check_cpa,
       sales_tax_percentage, exclude_r_c_b_0r_n, calculate_states_first, invoice_discount, discount_start_date,
       discount_end_date, show_time_clock_punch, auto_rd_dflt_cl_e_d_groups_nbr, auto_reduction_default_ees,
       vt_healthcare_gl_tag, vt_healthcare_offset_gl_tag, ui_rounding_gl_tag, ui_rounding_offset_gl_tag,
       reprint_to_balance, show_manual_checks_in_ess, payroll_requires_mgr_approval, days_prior_to_check_date,
       wc_offs_bank_account_nbr, qtr_lock_for_tax_pmts, co_max_amount_for_payroll, co_max_amount_for_tax_impound,
       co_max_amount_for_dd, co_payroll_process_limitations, co_ach_process_limitations, trust_impound, tax_impound,
       dd_impound, billing_impound, wc_impound, cobra_credit_gl_tag, co_exception_payment_type, last_tax_return,
       enable_hr, enable_ess, duns_and_bradstreet, bank_account_register_name, enable_benefits, enable_time_off,
       employer_type, wc_fiscal_year_begin, show_paystubs_ess_days, co_locations_nbr
FROM co;
COMMIT;
DROP TABLE co;
COMMIT;

INSERT INTO xo_additional_info SELECT * FROM co_additional_info;
COMMIT;
DROP TABLE co_additional_info;
COMMIT;

INSERT INTO xo_additional_info_names SELECT * FROM co_additional_info_names;
COMMIT;
DROP TABLE co_additional_info_names;
COMMIT;

INSERT INTO xo_additional_info_values SELECT * FROM co_additional_info_values;
COMMIT;
DROP TABLE co_additional_info_values;
COMMIT;

INSERT INTO xo_auto_enlist_returns SELECT * FROM co_auto_enlist_returns;
COMMIT;
DROP TABLE co_auto_enlist_returns;
COMMIT;

INSERT INTO xo_bank_account_register (co_bank_account_register_nbr, co_nbr, sb_bank_accounts_nbr, pr_check_nbr,
                                      check_serial_number, check_date, status, status_date, manual_type,
                                      co_manual_ach_nbr, co_pr_ach_nbr, co_tax_payment_ach_nbr, process_date, notes,
                                      filler, changed_by, creation_date, pr_miscellaneous_checks_nbr, close_date,
                                      co_tax_deposits_nbr, register_type, cleared_date, transaction_effective_date,
                                      effective_date, active_record, amount, cleared_amount, pr_nbr, group_identifier)
SELECT co_bank_account_register_nbr, co_nbr, sb_bank_accounts_nbr, pr_check_nbr, check_serial_number, check_date,
       status, status_date, manual_type, co_manual_ach_nbr, co_pr_ach_nbr, co_tax_payment_ach_nbr, process_date, notes,
       filler, changed_by, creation_date, pr_miscellaneous_checks_nbr, close_date, co_tax_deposits_nbr, register_type,
       cleared_date, transaction_effective_date, effective_date, active_record, amount, cleared_amount, pr_nbr,
       group_identifier
FROM co_bank_account_register;
COMMIT;
DROP TABLE co_bank_account_register;
COMMIT;

INSERT INTO xo_bank_acc_reg_details SELECT * FROM co_bank_acc_reg_details;
COMMIT;
DROP TABLE co_bank_acc_reg_details;
COMMIT;

INSERT INTO xo_batch_local_or_temps SELECT * FROM co_batch_local_or_temps;
COMMIT;
DROP TABLE co_batch_local_or_temps;
COMMIT;

INSERT INTO xo_batch_states_or_temps SELECT * FROM co_batch_states_or_temps;
COMMIT;
DROP TABLE co_batch_states_or_temps;
COMMIT;

INSERT INTO xo_benefits SELECT * FROM co_benefits;
COMMIT;
DROP TABLE co_benefits;
COMMIT;

INSERT INTO xo_benefit_category SELECT * FROM co_benefit_category;
COMMIT;
DROP TABLE co_benefit_category;
COMMIT;

INSERT INTO xo_benefit_discount SELECT * FROM co_benefit_discount;
COMMIT;
DROP TABLE co_benefit_discount;
COMMIT;

INSERT INTO xo_benefit_package SELECT * FROM co_benefit_package;
COMMIT;
DROP TABLE co_benefit_package;
COMMIT;

INSERT INTO xo_benefit_pkg_asmnt SELECT * FROM co_benefit_pkg_asmnt;
COMMIT;
DROP TABLE co_benefit_pkg_asmnt;
COMMIT;

INSERT INTO xo_benefit_pkg_detail SELECT * FROM co_benefit_pkg_detail;
COMMIT;
DROP TABLE co_benefit_pkg_detail;
COMMIT;

INSERT INTO xo_benefit_providers SELECT * FROM co_benefit_providers;
COMMIT;
DROP TABLE co_benefit_providers;
COMMIT;

INSERT INTO xo_benefit_rates SELECT * FROM co_benefit_rates;
COMMIT;
DROP TABLE co_benefit_rates;
COMMIT;

INSERT INTO xo_benefit_setup SELECT * FROM co_benefit_setup;
COMMIT;
DROP TABLE co_benefit_setup;
COMMIT;

INSERT INTO xo_benefit_states SELECT * FROM co_benefit_states;
COMMIT;
DROP TABLE co_benefit_states;
COMMIT;

INSERT INTO xo_benefit_subtype SELECT * FROM co_benefit_subtype;
COMMIT;
DROP TABLE co_benefit_subtype;
COMMIT;

INSERT INTO xo_billing_history SELECT * FROM co_billing_history;
COMMIT;
DROP TABLE co_billing_history;
COMMIT;

INSERT INTO xo_billing_history_detail SELECT * FROM co_billing_history_detail;
COMMIT;
DROP TABLE co_billing_history_detail;
COMMIT;

INSERT INTO xo_branch SELECT * FROM co_branch;
COMMIT;
DROP TABLE co_branch;
COMMIT;

INSERT INTO xo_branch_locals SELECT * FROM co_branch_locals;
COMMIT;
DROP TABLE co_branch_locals;
COMMIT;

INSERT INTO xo_brch_pr_batch_deflt_ed SELECT * FROM co_brch_pr_batch_deflt_ed;
COMMIT;
DROP TABLE co_brch_pr_batch_deflt_ed;
COMMIT;

INSERT INTO xo_calendar_defaults SELECT * FROM co_calendar_defaults;
COMMIT;
DROP TABLE co_calendar_defaults;
COMMIT;

INSERT INTO xo_department SELECT * FROM co_department;
COMMIT;
DROP TABLE co_department;
COMMIT;

INSERT INTO xo_department_locals SELECT * FROM co_department_locals;
COMMIT;
DROP TABLE co_department_locals;
COMMIT;

INSERT INTO xo_dept_pr_batch_deflt_ed SELECT * FROM co_dept_pr_batch_deflt_ed;
COMMIT;
DROP TABLE co_dept_pr_batch_deflt_ed;
COMMIT;

INSERT INTO xo_division SELECT * FROM co_division;
COMMIT;
DROP TABLE co_division;
COMMIT;

INSERT INTO xo_division_locals SELECT * FROM co_division_locals;
COMMIT;
DROP TABLE co_division_locals;
COMMIT;

INSERT INTO xo_div_pr_batch_deflt_ed SELECT * FROM co_div_pr_batch_deflt_ed;
COMMIT;
DROP TABLE co_div_pr_batch_deflt_ed;
COMMIT;

INSERT INTO xo_enlist_groups SELECT * FROM co_enlist_groups;
COMMIT;
DROP TABLE co_enlist_groups;
COMMIT;

INSERT INTO xo_e_d_codes SELECT * FROM co_e_d_codes;
COMMIT;
DROP TABLE co_e_d_codes;
COMMIT;

INSERT INTO xo_fed_tax_liabilities SELECT * FROM co_fed_tax_liabilities;
COMMIT;
DROP TABLE co_fed_tax_liabilities;
COMMIT;

INSERT INTO xo_general_ledger SELECT * FROM co_general_ledger;
COMMIT;
DROP TABLE co_general_ledger;
COMMIT;

INSERT INTO xo_group SELECT * FROM co_group;
COMMIT;
DROP TABLE co_group;
COMMIT;

INSERT INTO xo_group_manager SELECT * FROM co_group_manager;
COMMIT;
DROP TABLE co_group_manager;
COMMIT;

INSERT INTO xo_group_member SELECT * FROM co_group_member;
COMMIT;
DROP TABLE co_group_member;
COMMIT;

INSERT INTO xo_hr_applicant SELECT * FROM co_hr_applicant;
COMMIT;
DROP TABLE co_hr_applicant;
COMMIT;

INSERT INTO xo_hr_applicant_interview SELECT * FROM co_hr_applicant_interview;
COMMIT;
DROP TABLE co_hr_applicant_interview;
COMMIT;

INSERT INTO xo_hr_attendance_types SELECT * FROM co_hr_attendance_types;
COMMIT;
DROP TABLE co_hr_attendance_types;
COMMIT;

INSERT INTO xo_hr_car SELECT * FROM co_hr_car;
COMMIT;
DROP TABLE co_hr_car;
COMMIT;

INSERT INTO xo_hr_performance_ratings SELECT * FROM co_hr_performance_ratings;
COMMIT;
DROP TABLE co_hr_performance_ratings;
COMMIT;

INSERT INTO xo_hr_positions SELECT * FROM co_hr_positions;
COMMIT;
DROP TABLE co_hr_positions;
COMMIT;

INSERT INTO xo_hr_position_grades SELECT * FROM co_hr_position_grades;
COMMIT;
DROP TABLE co_hr_position_grades;
COMMIT;

INSERT INTO xo_hr_property SELECT * FROM co_hr_property;
COMMIT;
DROP TABLE co_hr_property;
COMMIT;

INSERT INTO xo_hr_recruiters SELECT * FROM co_hr_recruiters;
COMMIT;
DROP TABLE co_hr_recruiters;
COMMIT;

INSERT INTO xo_hr_referrals SELECT * FROM co_hr_referrals;
COMMIT;
DROP TABLE co_hr_referrals;
COMMIT;

INSERT INTO xo_hr_salary_grades SELECT * FROM co_hr_salary_grades;
COMMIT;
DROP TABLE co_hr_salary_grades;
COMMIT;

INSERT INTO xo_hr_supervisors SELECT * FROM co_hr_supervisors;
COMMIT;
DROP TABLE co_hr_supervisors;
COMMIT;

INSERT INTO xo_jobs (co_jobs_nbr, co_nbr, description, job_active, certified, changed_by, creation_date, filler,
                     co_workers_comp_nbr, general_ledger_tag, true_description, state_certified, misc_job_code,
                     effective_date, active_record, rate_per_hour, address1, address2, city, state, zip_code,
                     co_locations_nbr)
SELECT co_jobs_nbr, co_nbr, description, job_active, certified, changed_by, creation_date, filler, co_workers_comp_nbr,
       general_ledger_tag, true_description, state_certified, misc_job_code, effective_date, active_record,
       rate_per_hour, address1, address2, city, state, zip_code, co_locations_nbr
FROM co_jobs;
COMMIT;
DROP TABLE co_jobs;
COMMIT;

INSERT INTO xo_jobs_locals SELECT * FROM co_jobs_locals;
COMMIT;
DROP TABLE co_jobs_locals;
COMMIT;

INSERT INTO xo_job_groups SELECT * FROM co_job_groups;
COMMIT;
DROP TABLE co_job_groups;
COMMIT;

INSERT INTO xo_local_tax (co_local_tax_nbr, co_nbr, sy_locals_nbr, exempt, local_ein_number, tax_return_code,
                          sy_local_deposit_freq_nbr, payment_method, final_tax_return, eft_name, eft_pin_number,
                          eft_status, filler, changed_by, creation_date, general_ledger_tag, offset_gl_tag,
                          local_active, self_adjust_tax, autocreate_on_new_hire, sy_states_nbr, effective_date,
                          active_record, tax_rate, tax_amount, miscellaneous_amount, applied_for, cl_e_ds_nbr,
                          last_tax_return, deduct)
SELECT co_local_tax_nbr, co_nbr, sy_locals_nbr, exempt, local_ein_number, tax_return_code, sy_local_deposit_freq_nbr,
       payment_method, final_tax_return, eft_name, eft_pin_number, eft_status, filler, changed_by, creation_date,
       general_ledger_tag, offset_gl_tag, local_active, self_adjust_tax, autocreate_on_new_hire, sy_states_nbr,
       effective_date, active_record, tax_rate, tax_amount, miscellaneous_amount, applied_for, cl_e_ds_nbr,
       last_tax_return, deduct
FROM co_local_tax;
COMMIT;
DROP TABLE co_local_tax;
COMMIT;

INSERT INTO xo_local_tax_liabilities SELECT * FROM co_local_tax_liabilities;
COMMIT;
DROP TABLE co_local_tax_liabilities;
COMMIT;

INSERT INTO xo_manual_ach SELECT * FROM co_manual_ach;
COMMIT;
DROP TABLE co_manual_ach;
COMMIT;

INSERT INTO xo_pay_group SELECT * FROM co_pay_group;
COMMIT;
DROP TABLE co_pay_group;
COMMIT;

INSERT INTO xo_pensions SELECT * FROM co_pensions;
COMMIT;
DROP TABLE co_pensions;
COMMIT;

INSERT INTO xo_phone SELECT * FROM co_phone;
COMMIT;
DROP TABLE co_phone;
COMMIT;

INSERT INTO xo_pr_ach SELECT * FROM co_pr_ach;
COMMIT;
DROP TABLE co_pr_ach;
COMMIT;

INSERT INTO xo_pr_batch_deflt_ed SELECT * FROM co_pr_batch_deflt_ed;
COMMIT;
DROP TABLE co_pr_batch_deflt_ed;
COMMIT;

INSERT INTO xo_pr_check_templates SELECT * FROM co_pr_check_templates;
COMMIT;
DROP TABLE co_pr_check_templates;
COMMIT;

INSERT INTO xo_pr_check_template_e_ds SELECT * FROM co_pr_check_template_e_ds;
COMMIT;
DROP TABLE co_pr_check_template_e_ds;
COMMIT;

INSERT INTO xo_pr_filters SELECT * FROM co_pr_filters;
COMMIT;
DROP TABLE co_pr_filters;
COMMIT;

INSERT INTO xo_reports SELECT * FROM co_reports;
COMMIT;
DROP TABLE co_reports;
COMMIT;

INSERT INTO xo_salesperson SELECT * FROM co_salesperson;
COMMIT;
DROP TABLE co_salesperson;
COMMIT;

INSERT INTO xo_salesperson_flat_amt SELECT * FROM co_salesperson_flat_amt;
COMMIT;
DROP TABLE co_salesperson_flat_amt;
COMMIT;

INSERT INTO xo_services (co_services_nbr, co_nbr, sb_services_nbr, name, effective_start_date, effective_end_date,
                         sales_tax, discount_type, discount_start_date,
                         filler, changed_by, creation_date, frequency, week_number, month_number, effective_date,
                         active_record, override_flat_fee, discount, discount_end_date, co_e_d_codes_nbr,
                         cl_e_d_groups_nbr, co_workers_comp_nbr, override_tax_rate)
SELECT co_services_nbr, co_nbr, sb_services_nbr, name, effective_start_date, effective_end_date, sales_tax,
       discount_type, discount_start_date, filler, changed_by,
       creation_date, frequency, week_number, month_number, effective_date, active_record, override_flat_fee, discount,
       discount_end_date, co_e_d_codes_nbr, cl_e_d_groups_nbr, co_workers_comp_nbr, override_tax_rate
FROM co_services;
COMMIT;
DROP TABLE co_services;
COMMIT;

INSERT INTO xo_shifts SELECT * FROM co_shifts;
COMMIT;
DROP TABLE co_shifts;
COMMIT;

INSERT INTO xo_states (co_states_nbr, co_nbr, state, tax_return_code, use_dba_on_tax_return, state_tax_deposit_method,
                       sui_tax_deposit_method, sy_state_deposit_freq_nbr, ignore_state_tax_dep_threshold,
                       sui_tax_deposit_frequency, state_non_profit, state_exempt, sui_exempt, ee_sdi_exempt,
                       state_eft_name, state_eft_pin_number, state_eft_enrollment_status, sui_eft_name,
                       sui_eft_pin_number, sui_eft_enrollment_status, use_state_for_sui, notes, filler, changed_by,
                       creation_date, state_ein, sui_ein, state_sdi_ein, final_tax_return, state_general_ledger_tag,
                       ee_sdi_general_ledger_tag, er_sdi_general_ledger_tag, state_offset_gl_tag, ee_sdi_offset_gl_tag,
                       er_sdi_offset_gl_tag, state_eft_ein, state_sdi_eft_ein, sui_eft_ein, sy_states_nbr,
                       effective_date, active_record, mo_tax_credit_active, er_sdi_exempt, applied_for,
                       company_paid_health_insurance, last_tax_return, tcd_deposit_frequency_nbr, tcd_payment_method)
SELECT co_states_nbr, co_nbr, state, tax_return_code, use_dba_on_tax_return, state_tax_deposit_method,
       sui_tax_deposit_method, sy_state_deposit_freq_nbr, ignore_state_tax_dep_threshold, sui_tax_deposit_frequency,
       state_non_profit, state_exempt, sui_exempt, ee_sdi_exempt, state_eft_name, state_eft_pin_number,
       state_eft_enrollment_status, sui_eft_name, sui_eft_pin_number, sui_eft_enrollment_status, use_state_for_sui,
       notes, filler, changed_by, creation_date, state_ein, sui_ein, state_sdi_ein, final_tax_return,
       state_general_ledger_tag, ee_sdi_general_ledger_tag, er_sdi_general_ledger_tag, state_offset_gl_tag,
       ee_sdi_offset_gl_tag, er_sdi_offset_gl_tag, state_eft_ein, state_sdi_eft_ein, sui_eft_ein, sy_states_nbr,
       effective_date, active_record, mo_tax_credit_active, er_sdi_exempt, applied_for, company_paid_health_insurance,
       last_tax_return, tcd_deposit_frequency_nbr, tcd_payment_method
FROM co_states;
COMMIT;
DROP TABLE co_states;
COMMIT;

INSERT INTO xo_state_tax_liabilities (co_state_tax_liabilities_nbr, co_nbr, co_states_nbr, due_date, status,
                                      status_date, adjustment_type, pr_nbr, co_tax_deposits_nbr,
                                      impound_co_bank_acct_reg_nbr, notes, filler, changed_by, creation_date,
                                      third_party, check_date, tax_type, impounded, ach_key, sy_state_deposit_freq_nbr,
                                      period_begin_date, period_end_date, effective_date, active_record, amount,
                                      prev_status)
SELECT co_state_tax_liabilities_nbr, co_nbr, co_states_nbr, due_date, status, status_date, adjustment_type, pr_nbr,
       co_tax_deposits_nbr, impound_co_bank_acct_reg_nbr, notes, filler, changed_by, creation_date, third_party,
       check_date, tax_type, impounded, ach_key, sy_state_deposit_freq_nbr, period_begin_date, period_end_date,
       effective_date, active_record, amount, prev_status
FROM co_state_tax_liabilities;
COMMIT;
DROP TABLE co_state_tax_liabilities;
COMMIT;

INSERT INTO xo_storage SELECT * FROM co_storage;
COMMIT;
DROP TABLE co_storage;
COMMIT;

INSERT INTO xo_sui (co_sui_nbr, co_nbr, co_states_nbr, sy_sui_nbr, tax_return_code, description, payment_method, filler,
                    changed_by, creation_date, final_tax_return, effective_date, active_record, gl_tag, sui_active,
                    rate, gl_offset_tag, applied_for, last_tax_return, sui_reimburser, alternate_wage)
SELECT co_sui_nbr, co_nbr, co_states_nbr, sy_sui_nbr, tax_return_code, description, payment_method, filler, changed_by,
       creation_date, final_tax_return, effective_date, active_record, gl_tag, sui_active, rate, gl_offset_tag,
       applied_for, last_tax_return, sui_reimburser, 'N'
FROM co_sui;
COMMIT;
DROP TABLE co_sui;
COMMIT;


SET TERM ^;
EXECUTE BLOCK
AS
DECLARE VARIABLE co_sui_nbr INTEGER;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE fl CHAR(1);
DECLARE VARIABLE effective_date TIMESTAMP;
DECLARE VARIABLE creation_date TIMESTAMP;
BEGIN
  FOR SELECT co_sui_nbr, effective_date, creation_date, filler FROM xo_sui WHERE filler IS NOT NULL AND filler <> ''
      INTO co_sui_nbr, effective_date, creation_date, filler
  DO
  BEGIN
    fl = TRIM(strcopy(filler, 19, 1));
    IF (fl <> '') THEN
      UPDATE xo_sui SET alternate_wage = :fl
      WHERE co_sui_nbr = :co_sui_nbr AND effective_date = :effective_date AND creation_date = :creation_date;
  END
END^

COMMIT^
SET TERM ;^


INSERT INTO xo_sui_liabilities SELECT * FROM co_sui_liabilities;
COMMIT;
DROP TABLE co_sui_liabilities;
COMMIT;

INSERT INTO xo_tax_deposits (co_tax_deposits_nbr, co_nbr, tax_payment_reference_number, status, pr_nbr,
                             co_delivery_package_nbr, filler, changed_by, creation_date, sy_gl_agency_field_office_nbr,
                             status_date, deposit_type, effective_date, active_record, trace_number, sb_tax_payment_nbr)
SELECT co_tax_deposits_nbr, co_nbr, tax_payment_reference_number, status, pr_nbr, co_delivery_package_nbr, filler,
       changed_by, creation_date, sy_gl_agency_field_office_nbr, status_date, deposit_type, effective_date,
       active_record, trace_number, sb_tax_payment_nbr
FROM co_tax_deposits;
COMMIT;
DROP TABLE co_tax_deposits;
COMMIT;

INSERT INTO xo_tax_payment_ach SELECT * FROM co_tax_payment_ach;
COMMIT;
DROP TABLE co_tax_payment_ach;
COMMIT;


INSERT INTO xo_tax_return_queue (co_tax_return_queue_nbr, co_nbr, co_delivery_package_nbr, status, status_date,
                                 changed_by, creation_date, sb_copy_printed, client_copy_printed, agency_copy_printed,
                                 due_date, period_end_date, filler, sy_reports_group_nbr, cl_co_consolidation_nbr,
                                 produce_ascii_file, effective_date, sy_gl_agency_report_nbr,
                                 active_record)
SELECT co_tax_return_queue_nbr, co_nbr, co_delivery_package_nbr, status, status_date, changed_by, creation_date,
       sb_copy_printed, client_copy_printed, agency_copy_printed, due_date, period_end_date, filler,
       sy_reports_group_nbr, cl_co_consolidation_nbr, produce_ascii_file, effective_date,
       sy_gl_agency_report_nbr, active_record
FROM co_tax_return_queue;
COMMIT;
DROP TABLE co_tax_return_queue;
COMMIT;

INSERT INTO xo_tax_return_runs SELECT * FROM co_tax_return_runs;
COMMIT;
DROP TABLE co_tax_return_runs;
COMMIT;

INSERT INTO xo_team SELECT * FROM co_team;
COMMIT;
DROP TABLE co_team;
COMMIT;

INSERT INTO xo_team_locals SELECT * FROM co_team_locals;
COMMIT;
DROP TABLE co_team_locals;
COMMIT;

INSERT INTO xo_team_pr_batch_deflt_ed SELECT * FROM co_team_pr_batch_deflt_ed;
COMMIT;
DROP TABLE co_team_pr_batch_deflt_ed;
COMMIT;

INSERT INTO xo_time_off_accrual (co_time_off_accrual_nbr, co_nbr, description, cl_e_ds_nbr, cl_e_d_groups_nbr,
                                 frequency, accrual_month_number, payroll_of_the_month_to_accrue, calculation_method,
                                 accrual_active, auto_create_on_new_hire, show_used_balance_on_check, annual_reset_code,
                                 reset_date, divisor_description, filler, changed_by, creation_date, effective_date,
                                 active_record, rllovr_co_time_off_accrual_nbr, rollover_date,
                                 annual_maximum_hours_to_accrue, minimum_hours_before_accrual, probation_period_months,
                                 divisor, used_cl_e_d_groups_nbr, rollover_freq, rollover_month_number,
                                 rollover_payroll_of_month, rollover_offset, reset_month_number, reset_payroll_of_month,
                                 reset_offset, maximum_hours_to_accrue_on, check_time_off_avail, process_order,
                                 co_hr_attendance_types_nbr, auto_create_for_statuses, accrue_on_standard_hours,
                                 show_ess)
SELECT co_time_off_accrual_nbr, co_nbr, description, cl_e_ds_nbr, cl_e_d_groups_nbr, frequency, accrual_month_number,
       payroll_of_the_month_to_accrue, calculation_method, accrual_active, auto_create_on_new_hire,
       show_used_balance_on_check, annual_reset_code, reset_date, divisor_description, filler, changed_by,
       creation_date, effective_date, active_record, rllovr_co_time_off_accrual_nbr, rollover_date,
       annual_maximum_hours_to_accrue, minimum_hours_before_accrual, probation_period_months, divisor,
       used_cl_e_d_groups_nbr, rollover_freq, rollover_month_number, rollover_payroll_of_month, rollover_offset,
       reset_month_number, reset_payroll_of_month, reset_offset, maximum_hours_to_accrue_on, check_time_off_avail,
       process_order, co_hr_attendance_types_nbr, auto_create_for_statuses, accrue_on_standard_hours, show_ess
FROM co_time_off_accrual;
COMMIT;
DROP TABLE co_time_off_accrual;
COMMIT;

INSERT INTO xo_time_off_accrual_rates (co_time_off_accrual_rates_nbr, co_time_off_accrual_nbr, changed_by,
                                       creation_date, effective_date, active_record, minimum_month_number,
                                       maximum_month_number, hours, maximum_carryover, annual_accrual_maximum)
SELECT co_time_off_accrual_rates_nbr, co_time_off_accrual_nbr, changed_by, creation_date, effective_date, active_record,
       minimum_month_number, maximum_month_number, hours, maximum_carryover, annual_accrual_maximum
FROM co_time_off_accrual_rates;
COMMIT;
DROP TABLE co_time_off_accrual_rates;
COMMIT;

INSERT INTO xo_time_off_accrual_tiers SELECT * FROM co_time_off_accrual_tiers;
COMMIT;
DROP TABLE co_time_off_accrual_tiers;
COMMIT;

INSERT INTO xo_unions SELECT * FROM co_unions;
COMMIT;
DROP TABLE co_unions;
COMMIT;

INSERT INTO xo_user_reports SELECT * FROM co_user_reports;
COMMIT;
DROP TABLE co_user_reports;
COMMIT;

INSERT INTO xo_workers_comp SELECT * FROM co_workers_comp;
COMMIT;
DROP TABLE co_workers_comp;
COMMIT;

INSERT INTO xr (pr_nbr, co_nbr, run_number, check_date, check_date_status, scheduled, status, status_date, exclude_ach, exclude_tax_deposits, exclude_agency, mark_liabs_paid_default, exclude_billing, exclude_r_c_b_0r_n, scheduled_call_in_date, actual_call_in_date, scheduled_process_date, scheduled_delivery_date, scheduled_check_date, invoice_printed, payroll_comments, filler, changed_by, creation_date, process_date, payroll_type, exclude_time_off, effective_date, active_record, approved_by_finance, approved_by_tax, approved_by_management, cr_notes, same_day_pull_and_replace, miscellaneous_instructions, combine_runs, combine_from_run, combine_to_run, print_all_reports, cr_approve_sb_user_nbr, trust_impound, tax_impound, dd_impound, billing_impound, wc_impound, unlocked, approved_by_finance_nbr, approved_by_tax_nbr, approved_by_management_nbr)
SELECT pr_nbr, co_nbr, run_number, check_date, check_date_status, scheduled, status, status_date, exclude_ach, exclude_tax_deposits, exclude_agency, mark_liabs_paid_default, exclude_billing, exclude_r_c_b_0r_n, scheduled_call_in_date, actual_call_in_date, scheduled_process_date, scheduled_delivery_date, scheduled_check_date, invoice_printed, payroll_comments, filler, changed_by, creation_date, process_date, payroll_type, exclude_time_off, effective_date, active_record, approved_by_finance, approved_by_tax, approved_by_management, cr_notes, same_day_pull_and_replace, miscellaneous_instructions, combine_runs, combine_from_run, combine_to_run, print_all_reports, cr_approve_sb_user_nbr, trust_impound, tax_impound, dd_impound, billing_impound, wc_impound, strcopy(filler, 9, 1), approved_by_finance_nbr, approved_by_tax_nbr, approved_by_management_nbr FROM pr;
COMMIT;
DROP TABLE pr;
COMMIT;

INSERT INTO xr_batch (pr_batch_nbr, pr_nbr, co_pr_check_templates_nbr, co_pr_filters_nbr, period_begin_date,
                      period_begin_date_status, period_end_date, period_end_date_status, frequency, pay_salary,
                      pay_standard_hours, load_dbdt_defaults, filler, changed_by, creation_date, exclude_time_off,
                      effective_date, active_record)
SELECT pr_batch_nbr, pr_nbr, co_pr_check_templates_nbr, co_pr_filters_nbr, period_begin_date, period_begin_date_status,
       period_end_date, period_end_date_status, frequency, pay_salary, pay_standard_hours, load_dbdt_defaults, filler,
       changed_by, creation_date, exclude_time_off, effective_date, active_record
FROM pr_batch;
COMMIT;
DROP TABLE pr_batch;
COMMIT;

INSERT INTO xo_locations SELECT * FROM co_locations;
COMMIT;
DROP TABLE co_locations;
COMMIT;


/* Move PR_CHECK blobs in CL_BLOB */
SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE pr_check_nbr INTEGER;
DECLARE VARIABLE ee_nbr INTEGER;
DECLARE VARIABLE pr_nbr INTEGER;
DECLARE VARIABLE pr_batch_nbr INTEGER;
DECLARE VARIABLE custom_pr_bank_acct_number VARCHAR(20);
DECLARE VARIABLE payment_serial_number INTEGER;
DECLARE VARIABLE check_type CHAR(1);
DECLARE VARIABLE co_delivery_package_nbr INTEGER;
DECLARE VARIABLE exclude_dd CHAR(1);
DECLARE VARIABLE exclude_dd_except_net CHAR(1);
DECLARE VARIABLE exclude_time_off_accural CHAR(1);
DECLARE VARIABLE exclude_auto_distribution CHAR(1);
DECLARE VARIABLE exclude_all_sched_e_d_codes CHAR(1);
DECLARE VARIABLE exclude_sch_e_d_from_agcy_chk CHAR(1);
DECLARE VARIABLE exclude_sch_e_d_except_pension CHAR(1);
DECLARE VARIABLE prorate_scheduled_e_ds CHAR(1);
DECLARE VARIABLE or_check_federal_type CHAR(1);
DECLARE VARIABLE exclude_federal CHAR(1);
DECLARE VARIABLE exclude_additional_federal CHAR(1);
DECLARE VARIABLE exclude_employee_oasdi CHAR(1);
DECLARE VARIABLE exclude_employer_oasdi CHAR(1);
DECLARE VARIABLE exclude_employee_medicare CHAR(1);
DECLARE VARIABLE exclude_employer_medicare CHAR(1);
DECLARE VARIABLE exclude_employee_eic CHAR(1);
DECLARE VARIABLE exclude_employer_fui CHAR(1);
DECLARE VARIABLE tax_at_supplemental_rate CHAR(1);
DECLARE VARIABLE tax_frequency CHAR(1);
DECLARE VARIABLE override_check_notes BLOB SUB_TYPE binary;
DECLARE VARIABLE check_status CHAR(1);
DECLARE VARIABLE status_change_date TIMESTAMP;
DECLARE VARIABLE exclude_from_agency CHAR(1);
DECLARE VARIABLE check_comments BLOB SUB_TYPE binary;
DECLARE VARIABLE filler VARCHAR(512);
DECLARE VARIABLE changed_by INTEGER;
DECLARE VARIABLE creation_date TIMESTAMP;
DECLARE VARIABLE check_type_945 CHAR(1);
DECLARE VARIABLE effective_date TIMESTAMP;
DECLARE VARIABLE active_record CHAR(1);
DECLARE VARIABLE salary NUMERIC(18,6);
DECLARE VARIABLE or_check_federal_value NUMERIC(18,6);
DECLARE VARIABLE or_check_oasdi NUMERIC(18,6);
DECLARE VARIABLE or_check_medicare NUMERIC(18,6);
DECLARE VARIABLE or_check_eic NUMERIC(18,6);
DECLARE VARIABLE or_check_back_up_withholding NUMERIC(18,6);
DECLARE VARIABLE gross_wages NUMERIC(18,6);
DECLARE VARIABLE net_wages NUMERIC(18,6);
DECLARE VARIABLE federal_taxable_wages NUMERIC(18,6);
DECLARE VARIABLE federal_tax NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_taxable_wages NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_taxable_tips NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_tax NUMERIC(18,6);
DECLARE VARIABLE ee_medicare_taxable_wages NUMERIC(18,6);
DECLARE VARIABLE ee_medicare_tax NUMERIC(18,6);
DECLARE VARIABLE ee_eic_tax NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_taxable_wages NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_taxable_tips NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_tax NUMERIC(18,6);
DECLARE VARIABLE er_medicare_taxable_wages NUMERIC(18,6);
DECLARE VARIABLE er_medicare_tax NUMERIC(18,6);
DECLARE VARIABLE er_fui_taxable_wages NUMERIC(18,6);
DECLARE VARIABLE er_fui_gross_wages NUMERIC(18,6);
DECLARE VARIABLE er_fui_tax NUMERIC(18,6);
DECLARE VARIABLE federal_shortfall NUMERIC(18,6);
DECLARE VARIABLE federal_gross_wages NUMERIC(18,6);
DECLARE VARIABLE ee_oasdi_gross_wages NUMERIC(18,6);
DECLARE VARIABLE er_oasdi_gross_wages NUMERIC(18,6);
DECLARE VARIABLE ee_medicare_gross_wages NUMERIC(18,6);
DECLARE VARIABLE er_medicare_gross_wages NUMERIC(18,6);
DECLARE VARIABLE calculate_override_taxes CHAR(1);
DECLARE VARIABLE reciprocate_sui CHAR(1);
DECLARE VARIABLE user_grouping INTEGER;
DECLARE VARIABLE disable_shortfalls CHAR(1);
DECLARE VARIABLE aba_number CHAR(9);
DECLARE VARIABLE update_balance CHAR(1);
DECLARE VARIABLE data_nbr INTEGER;
DECLARE VARIABLE notes_nbr INTEGER;
BEGIN
  FOR SELECT pr_check_nbr, ee_nbr, pr_nbr, pr_batch_nbr, custom_pr_bank_acct_number, payment_serial_number, check_type,
           co_delivery_package_nbr, exclude_dd, exclude_dd_except_net, exclude_time_off_accural,
           exclude_auto_distribution, exclude_all_sched_e_d_codes, exclude_sch_e_d_from_agcy_chk,
           exclude_sch_e_d_except_pension, prorate_scheduled_e_ds, or_check_federal_type, exclude_federal,
           exclude_additional_federal, exclude_employee_oasdi, exclude_employer_oasdi, exclude_employee_medicare,
           exclude_employer_medicare, exclude_employee_eic, exclude_employer_fui, tax_at_supplemental_rate,
           tax_frequency, override_check_notes, check_status, status_change_date, exclude_from_agency, check_comments,
           filler, changed_by, creation_date, check_type_945, effective_date, active_record, salary,
           or_check_federal_value, or_check_oasdi, or_check_medicare, or_check_eic, or_check_back_up_withholding,
           gross_wages, net_wages, federal_taxable_wages, federal_tax, ee_oasdi_taxable_wages, ee_oasdi_taxable_tips,
           ee_oasdi_tax, ee_medicare_taxable_wages, ee_medicare_tax, ee_eic_tax, er_oasdi_taxable_wages,
           er_oasdi_taxable_tips, er_oasdi_tax, er_medicare_taxable_wages, er_medicare_tax, er_fui_taxable_wages,
           er_fui_gross_wages, er_fui_tax, federal_shortfall, federal_gross_wages, ee_oasdi_gross_wages,
           er_oasdi_gross_wages, ee_medicare_gross_wages, er_medicare_gross_wages, calculate_override_taxes,
           reciprocate_sui, user_grouping, disable_shortfalls, aba_number, update_balance
    FROM pr_check
    INTO :pr_check_nbr, :ee_nbr, :pr_nbr, :pr_batch_nbr, :custom_pr_bank_acct_number, :payment_serial_number,
         :check_type, :co_delivery_package_nbr, :exclude_dd, :exclude_dd_except_net, :exclude_time_off_accural,
         :exclude_auto_distribution, :exclude_all_sched_e_d_codes, :exclude_sch_e_d_from_agcy_chk,
         :exclude_sch_e_d_except_pension, :prorate_scheduled_e_ds, :or_check_federal_type, :exclude_federal,
         :exclude_additional_federal, :exclude_employee_oasdi, :exclude_employer_oasdi, :exclude_employee_medicare,
         :exclude_employer_medicare, :exclude_employee_eic, :exclude_employer_fui, :tax_at_supplemental_rate,
         :tax_frequency, :override_check_notes, :check_status, :status_change_date, :exclude_from_agency,
         :check_comments, :filler, :changed_by, :creation_date, :check_type_945, :effective_date, :active_record,
         :salary, :or_check_federal_value, :or_check_oasdi, :or_check_medicare, :or_check_eic,
         :or_check_back_up_withholding, :gross_wages, :net_wages, :federal_taxable_wages, :federal_tax,
         :ee_oasdi_taxable_wages, :ee_oasdi_taxable_tips, :ee_oasdi_tax, :ee_medicare_taxable_wages, :ee_medicare_tax,
         :ee_eic_tax, :er_oasdi_taxable_wages, :er_oasdi_taxable_tips, :er_oasdi_tax, :er_medicare_taxable_wages,
         :er_medicare_tax, :er_fui_taxable_wages, :er_fui_gross_wages, :er_fui_tax, :federal_shortfall,
         :federal_gross_wages, :ee_oasdi_gross_wages, :er_oasdi_gross_wages, :ee_medicare_gross_wages,
         :er_medicare_gross_wages, :calculate_override_taxes, :reciprocate_sui, :user_grouping, :disable_shortfalls,
         :aba_number, :update_balance
  DO
  BEGIN
    IF (override_check_notes IS NOT NULL) THEN
    BEGIN
      data_nbr = NEXT VALUE FOR cl_blob_gen;
      INSERT INTO xl_blob (cl_blob_nbr, data, changed_by, creation_date, effective_date, active_record)
         VALUES (:data_nbr, :override_check_notes, :changed_by, :creation_date, :effective_date, 'C');
    END
    ELSE
      data_nbr = NULL;

    IF (check_comments IS NOT NULL) THEN
    BEGIN
      notes_nbr = NEXT VALUE FOR cl_blob_gen;
      INSERT INTO xl_blob (cl_blob_nbr, data, changed_by, creation_date, effective_date, active_record)
         VALUES (:notes_nbr, :check_comments, :changed_by, :creation_date, :effective_date, 'C');
    END
    ELSE
      notes_nbr = NULL;


    INSERT INTO xr_check (pr_check_nbr, ee_nbr, pr_nbr, pr_batch_nbr, custom_pr_bank_acct_number, payment_serial_number,
                          check_type, co_delivery_package_nbr, exclude_dd, exclude_dd_except_net, exclude_time_off_accural,
                          exclude_auto_distribution, exclude_all_sched_e_d_codes, exclude_sch_e_d_from_agcy_chk,
                          exclude_sch_e_d_except_pension, prorate_scheduled_e_ds, or_check_federal_type, exclude_federal,
                          exclude_additional_federal, exclude_employee_oasdi, exclude_employer_oasdi,
                          exclude_employee_medicare, exclude_employer_medicare, exclude_employee_eic, exclude_employer_fui,
                          tax_at_supplemental_rate, tax_frequency, data_nbr, check_status, status_change_date,
                          exclude_from_agency, notes_nbr, filler, changed_by, creation_date, check_type_945,
                          effective_date, active_record, salary, or_check_federal_value, or_check_oasdi, or_check_medicare,
                          or_check_eic, or_check_back_up_withholding, gross_wages, net_wages, federal_taxable_wages,
                          federal_tax, ee_oasdi_taxable_wages, ee_oasdi_taxable_tips, ee_oasdi_tax,
                          ee_medicare_taxable_wages, ee_medicare_tax, ee_eic_tax, er_oasdi_taxable_wages,
                          er_oasdi_taxable_tips, er_oasdi_tax, er_medicare_taxable_wages, er_medicare_tax,
                          er_fui_taxable_wages, er_fui_gross_wages, er_fui_tax, federal_shortfall, federal_gross_wages,
                          ee_oasdi_gross_wages, er_oasdi_gross_wages, ee_medicare_gross_wages, er_medicare_gross_wages,
                          calculate_override_taxes, reciprocate_sui, user_grouping, disable_shortfalls, aba_number,
                          update_balance)
    VALUES (:pr_check_nbr, :ee_nbr, :pr_nbr, :pr_batch_nbr, :custom_pr_bank_acct_number, :payment_serial_number,
            :check_type, :co_delivery_package_nbr, :exclude_dd, :exclude_dd_except_net, :exclude_time_off_accural,
            :exclude_auto_distribution, :exclude_all_sched_e_d_codes, :exclude_sch_e_d_from_agcy_chk,
            :exclude_sch_e_d_except_pension, :prorate_scheduled_e_ds, :or_check_federal_type, :exclude_federal,
            :exclude_additional_federal, :exclude_employee_oasdi, :exclude_employer_oasdi, :exclude_employee_medicare,
            :exclude_employer_medicare, :exclude_employee_eic, :exclude_employer_fui, :tax_at_supplemental_rate,
            :tax_frequency, :data_nbr, :check_status, :status_change_date, :exclude_from_agency,
            :notes_nbr, :filler, :changed_by, :creation_date, :check_type_945, :effective_date, :active_record,
            :salary, :or_check_federal_value, :or_check_oasdi, :or_check_medicare, :or_check_eic,
            :or_check_back_up_withholding, :gross_wages, :net_wages, :federal_taxable_wages, :federal_tax,
            :ee_oasdi_taxable_wages, :ee_oasdi_taxable_tips, :ee_oasdi_tax, :ee_medicare_taxable_wages, :ee_medicare_tax,
            :ee_eic_tax, :er_oasdi_taxable_wages, :er_oasdi_taxable_tips, :er_oasdi_tax, :er_medicare_taxable_wages,
            :er_medicare_tax, :er_fui_taxable_wages, :er_fui_gross_wages, :er_fui_tax, :federal_shortfall,
            :federal_gross_wages, :ee_oasdi_gross_wages, :er_oasdi_gross_wages, :ee_medicare_gross_wages,
            :er_medicare_gross_wages, :calculate_override_taxes, :reciprocate_sui, :user_grouping, :disable_shortfalls,
            :aba_number, :update_balance);
  END
END^

SET TERM ;^

COMMIT;
DROP TABLE pr_check;
COMMIT;

INSERT INTO xr_check_lines SELECT * FROM pr_check_lines;
COMMIT;
DROP TABLE pr_check_lines;
COMMIT;

INSERT INTO xr_check_line_locals SELECT * FROM pr_check_line_locals;
COMMIT;
DROP TABLE pr_check_line_locals;
COMMIT;

INSERT INTO xr_check_locals SELECT * FROM pr_check_locals;
COMMIT;
DROP TABLE pr_check_locals;
COMMIT;

INSERT INTO xr_check_states SELECT * FROM pr_check_states;
COMMIT;
DROP TABLE pr_check_states;
COMMIT;

INSERT INTO xr_check_sui SELECT * FROM pr_check_sui;
COMMIT;
DROP TABLE pr_check_sui;
COMMIT;

INSERT INTO xr_miscellaneous_checks SELECT * FROM pr_miscellaneous_checks;
COMMIT;
DROP TABLE pr_miscellaneous_checks;
COMMIT;

INSERT INTO xr_reports SELECT * FROM pr_reports;
COMMIT;
DROP TABLE pr_reports;
COMMIT;

INSERT INTO xr_reprint_history SELECT * FROM pr_reprint_history;
COMMIT;
DROP TABLE pr_reprint_history;
COMMIT;

INSERT INTO xr_reprint_history_detail SELECT * FROM pr_reprint_history_detail;
COMMIT;
DROP TABLE pr_reprint_history_detail;
COMMIT;

INSERT INTO xr_scheduled_event SELECT * FROM pr_scheduled_event;
COMMIT;
DROP TABLE pr_scheduled_event;
COMMIT;

INSERT INTO xr_scheduled_event_batch SELECT * FROM pr_scheduled_event_batch;
COMMIT;
DROP TABLE pr_scheduled_event_batch;
COMMIT;

INSERT INTO xr_scheduled_e_ds SELECT * FROM pr_scheduled_e_ds;
COMMIT;
DROP TABLE pr_scheduled_e_ds;
COMMIT;

INSERT INTO xr_services (pr_services_nbr, pr_nbr, co_services_nbr, exclude, changed_by, creation_date, effective_date, active_record) SELECT pr_services_nbr, pr_nbr, co_services_nbr, exclude_, changed_by, creation_date, effective_date, active_record FROM pr_services;
COMMIT;
DROP TABLE pr_services;
COMMIT;


/* Move EE_LOGIN data to EE */

ALTER TABLE xe ADD login_attempts INTEGER;
ALTER TABLE xe ADD login_date TIMESTAMP;
COMMIT;

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE ee_nbr INTEGER;
DECLARE VARIABLE login_attempts INTEGER;
DECLARE VARIABLE login_date TIMESTAMP;
BEGIN
  FOR SELECT ee_nbr, login_attempts, login_date FROM ee_login
  INTO :ee_nbr, :login_attempts, :login_date
  DO
    UPDATE xe SET login_attempts = :login_attempts, login_date = :login_date
    WHERE ee_nbr = :ee_nbr AND active_record = 'C';
END^

COMMIT^

SET TERM ;^

DROP TABLE ee_login;
COMMIT;

DROP SEQUENCE cl_blob_gen;
COMMIT;

DROP TABLE co_delivery_package;
COMMIT;