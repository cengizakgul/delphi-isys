/* Delete some unwanted records */

DELETE FROM sb_option WHERE active_record IN ('P','N'); 
DELETE FROM sb_mail_box_option WHERE active_record IN ('P','N');
DELETE FROM sb_mail_box_content WHERE active_record IN ('P','N'); 
DELETE FROM sb_mail_box WHERE active_record IN ('P','N');
COMMIT;


/* CUT OFF AUDIT AND DELETED DATA BEFORE 2003 */

DELETE FROM sb_accountant WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_holidays WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sales_tax_states WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_referrals WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_global_agency_contacts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_company WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_banks WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_team WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_user WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_agency WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_bank_accounts WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_services WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_company_svcs WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_agency_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_services_calculations WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_team_members WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_group_members WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_rights WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_report_writer_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_row_filters WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_templates WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_sec_clients WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_task WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_queue_priority WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_task_runs WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_method WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_mail_box_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_service_opt WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_paper_info WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_multiclient_reports WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_media_type WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_delivery_service WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_mail_box WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_mail_box_content WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_media_type_option WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_other_service WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_enlist_groups WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_tax_payment WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;
DELETE FROM sb_storage WHERE creation_date < '1/1/2003' AND active_record <> 'C';
COMMIT;

/* Fix integrity issues */

UPDATE sb SET eftps_bank_format = 'B' WHERE eftps_bank_format IS NULL;
UPDATE sb_agency SET account_type = 'C' WHERE account_type IS NULL;
UPDATE sb_delivery_company SET delivery_contact_phone_type = 'P' WHERE delivery_contact_phone_type IS NULL;
UPDATE sb_delivery_company SET supplies_contact_phone_type = 'P' WHERE supplies_contact_phone_type IS NULL;
UPDATE sb_services SET week_number = ' ' WHERE week_number IS NULL;
COMMIT;



/* Update empty blobs */

SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE tbl_name VARCHAR(64);
DECLARE VARIABLE fld_name VARCHAR(64);
DECLARE VARIABLE sql VARCHAR(1024);
BEGIN
  FOR SELECT TRIM(r.rdb$relation_name), TRIM(f.rdb$field_name)
      FROM rdb$relations r, rdb$relation_fields f, rdb$fields dt
      WHERE
        (r.rdb$system_flag = 0 OR r.rdb$system_flag IS NULL) AND
        (f.rdb$system_flag = 0 OR f.rdb$system_flag IS NULL) AND
        f.rdb$relation_name = r.rdb$relation_name AND
        (f.rdb$null_flag = 0 OR f.rdb$null_flag IS NULL) AND
        f.rdb$field_source = dt.rdb$field_name AND
        dt.rdb$field_type = 261
  INTO :tbl_name, :fld_name
  DO
  BEGIN
    sql = 'UPDATE ' || tbl_name || ' SET ' || fld_name || ' = NULL WHERE GetBlobSize(' || fld_name || ') = 0';

    EXECUTE STATEMENT sql;
  END
END^

COMMIT^

ALTER TRIGGER tu_sb_blob INACTIVE^

ALTER TRIGGER td_sb_blob INACTIVE^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE nbr INTEGER;
DECLARE VARIABLE bnbr INTEGER;
BEGIN
  FOR SELECT t.sb_bank_accounts_nbr, b.sb_blob_nbr
      FROM sb_bank_accounts t, sb_blob b
      WHERE t.logo_sb_blob_nbr = b.sb_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE sb_bank_accounts SET logo_sb_blob_nbr = NULL WHERE sb_bank_accounts_nbr = :nbr;
    DELETE FROM sb_blob WHERE sb_blob_nbr = :bnbr;
  END

  FOR SELECT t.sb_bank_accounts_nbr, b.sb_blob_nbr
      FROM sb_bank_accounts t, sb_blob b
      WHERE t.signature_sb_blob_nbr = b.sb_blob_nbr AND b.data IS NULL
  INTO :nbr, :bnbr
  DO
  BEGIN
    UPDATE sb_bank_accounts SET signature_sb_blob_nbr = NULL WHERE sb_bank_accounts_nbr = :nbr;
    DELETE FROM sb_blob WHERE sb_blob_nbr = :bnbr;
  END
END^

/* PREPARE DB STRUCTURE */

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$trigger_name FROM rdb$triggers
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP TRIGGER ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$procedure_name FROM rdb$procedures
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP PROCEDURE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$generator_name FROM rdb$generators
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    IF ((s <> 'NEXT_INVOICE_NUMBER_GEN') AND (s <> 'CL_GEN')) THEN
      EXECUTE STATEMENT 'DROP SEQUENCE ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$exception_name FROM rdb$exceptions
      WHERE rdb$system_flag = 0 OR rdb$system_flag IS NULL INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP EXCEPTION ' || s;
  END
END^

COMMIT^

EXECUTE BLOCK
AS
DECLARE VARIABLE s VARCHAR(32);
BEGIN
  FOR SELECT rdb$index_name FROM rdb$indices i WHERE
        (rdb$system_flag = 0 OR rdb$system_flag IS NULL) AND
        NOT EXISTS (SELECT rdb$index_name FROM rdb$relation_constraints
        WHERE rdb$index_name = i.rdb$index_name)
      INTO :s
  DO
  BEGIN
    EXECUTE STATEMENT 'DROP INDEX ' || s;
  END
END^

COMMIT^

SET TERM ;^


/* DROP JUNK TABLES */

DROP TABLE sb_report_groups;
DROP TABLE sb_report_group_members;
DROP TABLE sb_sched_event_history;
DROP TABLE sb_scheduled_events;
DROP TABLE sb_mail_box_job;
DROP TABLE sb_bank_accounts_phone;
DROP TABLE sb_tax_return_runs;
DROP TABLE sb_exceptions;
DROP TABLE version_info;
DROP TABLE change_log;
DROP TABLE transaction_info;

COMMIT;

/* Prepare X_TRANSACTION table */

CREATE TABLE x_transaction (
    start_time   TIMESTAMP,
    user_id      INTEGER);

COMMIT;

INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_accountant;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_holidays;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sales_tax_states;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_referrals;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_global_agency_contacts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_company;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_banks;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_team;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_user;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_agency;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_bank_accounts;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_services;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_company_svcs;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_agency_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_services_calculations;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_team_members;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_group_members;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_rights;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_report_writer_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_row_filters;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_templates;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_sec_clients;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT change_date, 0 FROM sb_blob;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_task;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_queue_priority;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_task_runs;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_paper_info;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_multiclient_reports;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_method;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_media_type;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_service;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_mail_box;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_mail_box_content;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_mail_box_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_delivery_service_opt;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_media_type_option;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_other_service;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT CAST('1/1/2000' AS TIMESTAMP), 0 FROM sb_user_notice;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_enlist_groups;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_tax_payment;
COMMIT;
INSERT INTO x_transaction (start_time, user_id) SELECT DISTINCT creation_date, changed_by FROM sb_storage;
COMMIT;


/* CREATE TEMP TABLES */

CREATE TABLE xb (
    sb_nbr                          INTEGER NOT NULL,
    sb_name                         VARCHAR(60) NOT NULL,
    address1                        VARCHAR(30) NOT NULL,
    address2                        VARCHAR(30),
    city                            VARCHAR(20) NOT NULL,
    state                           CHAR(2) NOT NULL,
    zip_code                        VARCHAR(10) NOT NULL,
    e_mail_address                  VARCHAR(80),
    parent_sb_number                INTEGER,
    parent_sb_modem_number          VARCHAR(20),
    development_modem_number        VARCHAR(20) NOT NULL,
    development_ftp_site            VARCHAR(80),
    development_ftp_password        VARCHAR(128),
    ein_number                      VARCHAR(9) NOT NULL,
    eftps_tin_number                VARCHAR(9),
    eftps_bank_format               CHAR(1),
    batch_or_edi_filing             CHAR(1),
    use_prenote                     CHAR(1) NOT NULL,
    impound_trust_monies_as_receiv  CHAR(1) NOT NULL,
    pay_tax_from_payables           CHAR(1) NOT NULL,
    tax_export_format               CHAR(1) NOT NULL,
    ar_export_format                CHAR(1) NOT NULL,
    default_check_format            CHAR(1) NOT NULL,
    micr_font                       CHAR(1) NOT NULL,
    micr_horizontal_adjustment      INTEGER NOT NULL,
    auto_save_minutes               INTEGER NOT NULL,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    phone                           VARCHAR(20) NOT NULL,
    fax                             VARCHAR(20) NOT NULL,
    cover_letter_notes              BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    invoice_notes                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    tax_cover_letter_notes          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    ar_import_directory             VARCHAR(80),
    ach_directory                   VARCHAR(80),
    sb_url                          VARCHAR(80),
    days_in_prenote                 INTEGER,
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    sb_logo                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    non_tax_cover_letter_notes      BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    tax_recon_cover_letter_notes    BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    defferred_comp_limit            NUMERIC(18,6),
    user_password_duration_in_days  NUMERIC(18,6),
    dummy_tax_cl_nbr                INTEGER,
    dummy_tax_co_nbr                INTEGER,
    error_screen                    CHAR(1) NOT NULL,
    pswd_min_length                 INTEGER,
    pswd_force_mixed                CHAR(1) NOT NULL,
    misc_check_form                 CHAR(1) NOT NULL,
    vmr_confidencial_notes          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    mark_liabs_paid_default         CHAR(1) NOT NULL,
    trust_impound                   CHAR(1) NOT NULL,
    tax_impound                     CHAR(1) NOT NULL,
    dd_impound                      CHAR(1) NOT NULL,
    billing_impound                 CHAR(1) NOT NULL,
    wc_impound                      CHAR(1) NOT NULL,
    days_prior_to_check_date        INTEGER,
    sb_exception_payment_type       CHAR(1) NOT NULL,
    sb_max_ach_file_total           NUMERIC(18,6),
    sb_ach_file_limitations         CHAR(1) NOT NULL,
    sb_cl_nbr                       INTEGER,
    dashboard_msg                   VARCHAR(200),
    ee_login_type                   CHAR(1) NOT NULL,
    ess_terms_of_use                BLOB SUB_TYPE 1 SEGMENT SIZE 80,
    wc_terms_of_use                 BLOB SUB_TYPE 1 SEGMENT SIZE 80
);


CREATE TABLE xb_accountant (
    sb_accountant_nbr  INTEGER NOT NULL,
    name               VARCHAR(40) NOT NULL,
    address1           VARCHAR(30),
    address2           VARCHAR(30),
    city               VARCHAR(20),
    state              VARCHAR(3),
    zip_code           VARCHAR(10),
    contact1           VARCHAR(30) NOT NULL,
    phone1             VARCHAR(20),
    description1       VARCHAR(10),
    contact2           VARCHAR(30),
    phone2             VARCHAR(20),
    description2       VARCHAR(10),
    fax                VARCHAR(20),
    fax_description    VARCHAR(10),
    e_mail_address     VARCHAR(80),
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    print_name         VARCHAR(40) NOT NULL,
    title              VARCHAR(30),
    signature          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_agency (
    sb_agency_nbr                INTEGER NOT NULL,
    name                         VARCHAR(40) NOT NULL,
    address1                     VARCHAR(30),
    address2                     VARCHAR(30),
    city                         VARCHAR(20),
    state                        CHAR(2),
    zip_code                     VARCHAR(10),
    contact1                     VARCHAR(30),
    phone1                       VARCHAR(20),
    description1                 VARCHAR(10),
    contact2                     VARCHAR(30),
    phone2                       VARCHAR(20),
    description2                 VARCHAR(10),
    fax                          VARCHAR(20),
    fax_description              VARCHAR(10),
    e_mail_address               VARCHAR(80),
    agency_type                  CHAR(1) NOT NULL,
    sb_banks_nbr                 INTEGER,
    account_number               VARCHAR(20),
    account_type                 CHAR(1),
    negative_direct_dep_allowed  CHAR(1) NOT NULL,
    method                       CHAR(1),
    modem_number                 VARCHAR(20),
    transfer_protocol            CHAR(1),
    notes                        BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    filler                       VARCHAR(512),
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    print_name                   VARCHAR(40) NOT NULL,
    county                       VARCHAR(20),
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    bulk_filer_number            VARCHAR(20)
);


CREATE TABLE xb_agency_reports (
    sb_agency_reports_nbr  INTEGER NOT NULL,
    sb_agency_nbr          INTEGER NOT NULL,
    sb_reports_nbr         INTEGER NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    effective_date         TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xb_bank_accounts (
    sb_bank_accounts_nbr         INTEGER NOT NULL,
    sb_banks_nbr                 INTEGER NOT NULL,
    custom_bank_account_number   VARCHAR(20) NOT NULL,
    bank_account_type            CHAR(1) NOT NULL,
    name_description             VARCHAR(40),
    ach_origin_sb_banks_nbr      INTEGER NOT NULL,
    check_form_default           CHAR(1),
    bank_returns                 CHAR(1) NOT NULL,
    custom_header_record         VARCHAR(80),
    next_available_check_number  INTEGER NOT NULL,
    end_check_number             INTEGER,
    next_begin_check_number      INTEGER,
    next_end_check_number        INTEGER,
    filler                       VARCHAR(512),
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    suppress_offset_account      CHAR(1) NOT NULL,
    block_negative_checks        CHAR(1) NOT NULL,
    block_trust_impound          CHAR(1) NOT NULL,
    block_tax_impound            CHAR(1) NOT NULL,
    block_billing_impound        CHAR(1) NOT NULL,
    batch_filer_id               VARCHAR(9),
    master_inquiry_pin           VARCHAR(10),
    logo_sb_blob_nbr             INTEGER,
    signature_sb_blob_nbr        INTEGER,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL,
    block_debit_ach              CHAR(1) NOT NULL,
    beginning_balance            NUMERIC(18,6),
    operating_account            CHAR(1) NOT NULL,
    billing_account              CHAR(1) NOT NULL,
    ach_account                  CHAR(1) NOT NULL,
    trust_account                CHAR(1) NOT NULL,
    tax_account                  CHAR(1) NOT NULL,
    obc_account                  CHAR(1) NOT NULL,
    workers_comp_account         CHAR(1) NOT NULL,
    reccuring_wire_number        INTEGER,
    bank_check                   CHAR(1) NOT NULL
);


CREATE TABLE xb_banks (
    sb_banks_nbr                    INTEGER NOT NULL,
    name                            VARCHAR(40) NOT NULL,
    address1                        VARCHAR(30),
    address2                        VARCHAR(30),
    city                            VARCHAR(20),
    state                           CHAR(2),
    zip_code                        VARCHAR(10),
    contact1                        VARCHAR(30) NOT NULL,
    phone1                          VARCHAR(20),
    description1                    VARCHAR(10),
    contact2                        VARCHAR(30),
    phone2                          VARCHAR(20),
    description2                    VARCHAR(10),
    fax                             VARCHAR(20),
    fax_description                 VARCHAR(10),
    e_mail_address                  VARCHAR(80),
    aba_number                      CHAR(9) NOT NULL,
    top_aba_number                  VARCHAR(10) NOT NULL,
    bottom_aba_number               VARCHAR(10) NOT NULL,
    addenda                         VARCHAR(12) NOT NULL,
    check_template                  BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    use_check_template              CHAR(1) NOT NULL,
    micr_account_start_position     INTEGER,
    micr_check_number_start_positn  INTEGER,
    filler                          VARCHAR(512),
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    print_name                      VARCHAR(40) NOT NULL,
    branch_identifier               VARCHAR(9),
    effective_date                  TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    allow_hyphens                   CHAR(1) NOT NULL
);


CREATE TABLE xb_blob (
    sb_blob_nbr  INTEGER NOT NULL,
    data         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_company (
    sb_delivery_company_nbr      INTEGER NOT NULL,
    name                         VARCHAR(40) NOT NULL,
    delivery_contact             VARCHAR(40),
    delivery_contact_phone       VARCHAR(20),
    delivery_contact_phone_type  CHAR(1),
    supplies_contact             VARCHAR(40),
    supplies_contact_phone       VARCHAR(20),
    supplies_contact_phone_type  CHAR(1),
    web_site                     VARCHAR(40),
    delivery_company_notes       BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    effective_date               TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_company_svcs (
    sb_delivery_company_svcs_nbr  INTEGER NOT NULL,
    sb_delivery_company_nbr       INTEGER NOT NULL,
    description                   VARCHAR(40),
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    reference_fee                 NUMERIC(18,6)
);


CREATE TABLE xb_delivery_method (
    sb_delivery_method_nbr  INTEGER NOT NULL,
    sy_delivery_method_nbr  INTEGER NOT NULL,
    effective_date          TIMESTAMP NOT NULL,
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_service (
    sb_delivery_service_nbr  INTEGER NOT NULL,
    sy_delivery_service_nbr  INTEGER NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL
);


CREATE TABLE xb_delivery_service_opt (
    sb_delivery_service_opt_nbr  INTEGER NOT NULL,
    sb_delivery_service_nbr      INTEGER NOT NULL,
    sb_delivery_method_nbr       INTEGER,
    option_tag                   VARCHAR(40) NOT NULL,
    option_integer_value         INTEGER,
    option_string_value          VARCHAR(512),
    effective_date               TIMESTAMP NOT NULL,
    changed_by                   INTEGER NOT NULL,
    creation_date                TIMESTAMP NOT NULL,
    active_record                CHAR(1) NOT NULL
);


CREATE TABLE xb_enlist_groups (
    sb_enlist_groups_nbr  INTEGER NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    changed_by            INTEGER NOT NULL,
    active_record         CHAR(1) NOT NULL,
    sy_report_groups_nbr  INTEGER NOT NULL,
    media_type            CHAR(1) NOT NULL,
    process_type          CHAR(1) NOT NULL
);


CREATE TABLE xb_global_agency_contacts (
    sb_global_agency_contacts_nbr  INTEGER NOT NULL,
    sy_global_agency_nbr           INTEGER NOT NULL,
    contact_name                   VARCHAR(30) NOT NULL,
    phone                          VARCHAR(20),
    fax                            VARCHAR(20),
    e_mail_address                 VARCHAR(80),
    notes                          BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    tax_return_pin                 VARCHAR(20)
);


CREATE TABLE xb_holidays (
    sb_holidays_nbr  INTEGER NOT NULL,
    holiday_date     DATE NOT NULL,
    holiday_name     VARCHAR(40) NOT NULL,
    used_by          CHAR(1) NOT NULL,
    changed_by       INTEGER NOT NULL,
    creation_date    TIMESTAMP NOT NULL,
    effective_date   TIMESTAMP NOT NULL,
    active_record    CHAR(1) NOT NULL
);


CREATE TABLE xb_mail_box (
    sb_mail_box_nbr                 INTEGER NOT NULL,
    cl_nbr                          INTEGER,
    cl_mail_box_group_nbr           INTEGER,
    cost                            NUMERIC(18,6),
    required                        CHAR(1) NOT NULL,
    notification_email              VARCHAR(80),
    pr_nbr                          INTEGER,
    released_time                   TIMESTAMP,
    printed_time                    TIMESTAMP,
    sb_cover_letter_report_nbr      INTEGER,
    scanned_time                    TIMESTAMP,
    shipped_time                    TIMESTAMP,
    sy_cover_letter_report_nbr      INTEGER,
    note                            VARCHAR(512),
    up_level_mail_box_nbr           INTEGER,
    tracking_info                   VARCHAR(40),
    auto_release_type               CHAR(1) NOT NULL,
    barcode                         VARCHAR(40) NOT NULL,
    created_time                    TIMESTAMP NOT NULL,
    dispose_content_after_shipping  CHAR(1) NOT NULL,
    addressee                       VARCHAR(512) NOT NULL,
    description                     VARCHAR(40) NOT NULL,
    sb_delivery_method_nbr          INTEGER NOT NULL,
    sb_media_type_nbr               INTEGER NOT NULL,
    changed_by                      INTEGER NOT NULL,
    creation_date                   TIMESTAMP NOT NULL,
    active_record                   CHAR(1) NOT NULL,
    effective_date                  TIMESTAMP NOT NULL
);


CREATE TABLE xb_mail_box_content (
    sb_mail_box_content_nbr  INTEGER NOT NULL,
    sb_mail_box_nbr          INTEGER NOT NULL,
    description              VARCHAR(512) NOT NULL,
    file_name                VARCHAR(40) NOT NULL,
    media_type               CHAR(1) NOT NULL,
    page_count               INTEGER NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    sb_mail_box_job_nbr      INTEGER
);


CREATE TABLE xb_mail_box_option (
    sb_mail_box_option_nbr   INTEGER NOT NULL,
    sb_mail_box_nbr          INTEGER NOT NULL,
    sb_mail_box_content_nbr  INTEGER,
    option_tag               VARCHAR(80) NOT NULL,
    option_integer_value     INTEGER,
    option_string_value      VARCHAR(512),
    effective_date           TIMESTAMP NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    sb_mail_box_job_nbr      INTEGER
);


CREATE TABLE xb_media_type (
    sb_media_type_nbr  INTEGER NOT NULL,
    sy_media_type_nbr  INTEGER NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_media_type_option (
    sb_media_type_option_nbr  INTEGER NOT NULL,
    sb_media_type_nbr         INTEGER NOT NULL,
    option_tag                VARCHAR(40) NOT NULL,
    option_integer_value      INTEGER,
    option_string_value       VARCHAR(512),
    effective_date            TIMESTAMP NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xb_multiclient_reports (
    sb_multiclient_reports_nbr  INTEGER NOT NULL,
    description                 VARCHAR(40) NOT NULL,
    report_level                CHAR(1) NOT NULL,
    report_nbr                  INTEGER NOT NULL,
    input_params                BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    effective_date              TIMESTAMP NOT NULL,
    changed_by                  INTEGER NOT NULL,
    creation_date               TIMESTAMP NOT NULL,
    active_record               CHAR(1) NOT NULL
);


CREATE TABLE xb_option (
    sb_option_nbr         INTEGER NOT NULL,
    option_tag            VARCHAR(80) NOT NULL,
    option_integer_value  INTEGER,
    option_string_value   VARCHAR(512),
    effective_date        TIMESTAMP NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xb_other_service (
    sb_other_service_nbr  INTEGER NOT NULL,
    name                  VARCHAR(60) NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL,
    changed_by            INTEGER NOT NULL
);


CREATE TABLE xb_paper_info (
    sb_paper_info_nbr  INTEGER NOT NULL,
    description        VARCHAR(40) NOT NULL,
    height             NUMERIC(18,6) NOT NULL,
    width              NUMERIC(18,6) NOT NULL,
    weight             NUMERIC(18,6) NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL,
    media_type         CHAR(1) NOT NULL
);


CREATE TABLE xb_queue_priority (
    sb_queue_priority_nbr  INTEGER NOT NULL,
    package_id             INTEGER NOT NULL,
    method_name            VARCHAR(255) NOT NULL,
    priority               INTEGER NOT NULL,
    sb_user_nbr            INTEGER,
    sb_sec_groups_nbr      INTEGER,
    effective_date         TIMESTAMP NOT NULL,
    changed_by             INTEGER NOT NULL,
    creation_date          TIMESTAMP NOT NULL,
    active_record          CHAR(1) NOT NULL
);


CREATE TABLE xb_referrals (
    sb_referrals_nbr  INTEGER NOT NULL,
    name              VARCHAR(40) NOT NULL,
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL
);


CREATE TABLE xb_report_writer_reports (
    sb_report_writer_reports_nbr  INTEGER NOT NULL,
    report_description            VARCHAR(40) NOT NULL,
    report_type                   CHAR(1) NOT NULL,
    report_file                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    notes                         BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    media_type                    CHAR(1) NOT NULL,
    class_name                    VARCHAR(40),
    ancestor_class_name           VARCHAR(40)
);


CREATE TABLE xb_reports (
    sb_reports_nbr             INTEGER NOT NULL,
    description                VARCHAR(40) NOT NULL,
    comments                   BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    changed_by                 INTEGER NOT NULL,
    creation_date              TIMESTAMP NOT NULL,
    effective_date             TIMESTAMP NOT NULL,
    active_record              CHAR(1) NOT NULL,
    report_writer_reports_nbr  INTEGER NOT NULL,
    report_level               CHAR(1) NOT NULL,
    input_params               BLOB SUB_TYPE 0 SEGMENT SIZE 80
);


CREATE TABLE xb_sales_tax_states (
    sb_sales_tax_states_nbr  INTEGER NOT NULL,
    state                    CHAR(2) NOT NULL,
    state_tax_id             VARCHAR(19) NOT NULL,
    changed_by               INTEGER NOT NULL,
    creation_date            TIMESTAMP NOT NULL,
    effective_date           TIMESTAMP NOT NULL,
    active_record            CHAR(1) NOT NULL,
    sales_tax_percentage     NUMERIC(18,6)
);


CREATE TABLE xb_sec_clients (
    sb_sec_clients_nbr  INTEGER NOT NULL,
    cl_nbr              INTEGER NOT NULL,
    sb_sec_groups_nbr   INTEGER,
    sb_user_nbr         INTEGER,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_group_members (
    sb_sec_group_members_nbr  INTEGER NOT NULL,
    sb_sec_groups_nbr         INTEGER NOT NULL,
    sb_user_nbr               INTEGER NOT NULL,
    changed_by                INTEGER NOT NULL,
    creation_date             TIMESTAMP NOT NULL,
    effective_date            TIMESTAMP NOT NULL,
    active_record             CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_groups (
    sb_sec_groups_nbr  INTEGER NOT NULL,
    name               VARCHAR(40) NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_rights (
    sb_sec_rights_nbr  INTEGER NOT NULL,
    sb_sec_groups_nbr  INTEGER,
    sb_user_nbr        INTEGER,
    tag                VARCHAR(128) NOT NULL,
    context            VARCHAR(128) NOT NULL,
    changed_by         INTEGER NOT NULL,
    creation_date      TIMESTAMP NOT NULL,
    effective_date     TIMESTAMP NOT NULL,
    active_record      CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_row_filters (
    sb_sec_row_filters_nbr  INTEGER NOT NULL,
    sb_sec_groups_nbr       INTEGER,
    sb_user_nbr             INTEGER,
    database_type           CHAR(1) NOT NULL,
    table_name              VARCHAR(40) NOT NULL,
    filter_type             CHAR(1) NOT NULL,
    custom_expr             VARCHAR(255),
    changed_by              INTEGER NOT NULL,
    creation_date           TIMESTAMP NOT NULL,
    cl_nbr                  INTEGER,
    effective_date          TIMESTAMP NOT NULL,
    active_record           CHAR(1) NOT NULL
);


CREATE TABLE xb_sec_templates (
    sb_sec_templates_nbr  INTEGER NOT NULL,
    name                  VARCHAR(40) NOT NULL,
    template              BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL
);


CREATE TABLE xb_services (
    sb_services_nbr                INTEGER NOT NULL,
    service_name                   VARCHAR(40) NOT NULL,
    frequency                      CHAR(1) NOT NULL,
    month_number                   VARCHAR(2),
    based_on_type                  CHAR(1) NOT NULL,
    next_based_on_type             CHAR(1),
    next_based_on_type_begin_date  TIMESTAMP,
    sb_reports_nbr                 INTEGER,
    commission                     CHAR(1) NOT NULL,
    sales_taxable                  CHAR(1) NOT NULL,
    filler                         VARCHAR(512),
    changed_by                     INTEGER NOT NULL,
    creation_date                  TIMESTAMP NOT NULL,
    product_code                   VARCHAR(6),
    week_number                    CHAR(1),
    service_type                   CHAR(1) NOT NULL,
    effective_date                 TIMESTAMP NOT NULL,
    active_record                  CHAR(1) NOT NULL,
    minimum_amount                 NUMERIC(18,6),
    maximum_amount                 NUMERIC(18,6),
    sb_delivery_method_nbr         INTEGER,
    tax_type                       CHAR(1) NOT NULL
);


CREATE TABLE xb_services_calculations (
    sb_services_calculations_nbr  INTEGER NOT NULL,
    sb_services_nbr               INTEGER NOT NULL,
    next_min_quantity_begin_date  TIMESTAMP,
    next_max_quantity_begin_date  TIMESTAMP,
    next_per_item_begin_date      TIMESTAMP,
    next_flat_amount_begin_date   TIMESTAMP,
    changed_by                    INTEGER NOT NULL,
    creation_date                 TIMESTAMP NOT NULL,
    effective_date                TIMESTAMP NOT NULL,
    active_record                 CHAR(1) NOT NULL,
    minimum_quantity              NUMERIC(18,6),
    next_minimum_quantity         NUMERIC(18,6),
    maximum_quantity              NUMERIC(18,6),
    next_maximum_quantity         NUMERIC(18,6),
    per_item_rate                 NUMERIC(18,6),
    next_per_item_rate            NUMERIC(18,6),
    flat_amount                   NUMERIC(18,6),
    next_flat_amount              NUMERIC(18,6)
);


CREATE TABLE xb_storage (
    sb_storage_nbr  INTEGER NOT NULL,
    effective_date  TIMESTAMP NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL,
    changed_by      INTEGER NOT NULL,
    tag             VARCHAR(20) NOT NULL,
    storage_data    BLOB SUB_TYPE 1 SEGMENT SIZE 80 NOT NULL
);


CREATE TABLE xb_task (
    sb_task_nbr     INTEGER NOT NULL,
    sb_user_nbr     INTEGER NOT NULL,
    schedule        VARCHAR(255) NOT NULL,
    description     VARCHAR(255) NOT NULL,
    task            BLOB SUB_TYPE 0 SEGMENT SIZE 80 NOT NULL,
    last_run        TIMESTAMP,
    effective_date  TIMESTAMP NOT NULL,
    changed_by      INTEGER NOT NULL,
    creation_date   TIMESTAMP NOT NULL,
    active_record   CHAR(1) NOT NULL
);


CREATE TABLE xb_tax_payment (
    sb_tax_payment_nbr  INTEGER NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    changed_by          INTEGER NOT NULL,
    active_record       CHAR(1) NOT NULL,
    description         VARCHAR(80),
    status              CHAR(1) NOT NULL,
    status_date         TIMESTAMP
);


CREATE TABLE xb_team (
    sb_team_nbr       INTEGER NOT NULL,
    team_description  VARCHAR(40) NOT NULL,
    changed_by        INTEGER NOT NULL,
    creation_date     TIMESTAMP NOT NULL,
    effective_date    TIMESTAMP NOT NULL,
    active_record     CHAR(1) NOT NULL,
    cr_category       CHAR(1) NOT NULL
);


CREATE TABLE xb_team_members (
    sb_team_members_nbr  INTEGER NOT NULL,
    sb_team_nbr          INTEGER NOT NULL,
    sb_user_nbr          INTEGER NOT NULL,
    changed_by           INTEGER NOT NULL,
    creation_date        TIMESTAMP NOT NULL,
    effective_date       TIMESTAMP NOT NULL,
    active_record        CHAR(1) NOT NULL
);


CREATE TABLE xb_user (
    sb_user_nbr           INTEGER NOT NULL,
    user_id               VARCHAR(128) NOT NULL,
    user_signature        BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    last_name             VARCHAR(30) NOT NULL,
    first_name            VARCHAR(20) NOT NULL,
    middle_initial        CHAR(1),
    active_user           CHAR(1) NOT NULL,
    department            CHAR(1) NOT NULL,
    password_change_date  TIMESTAMP NOT NULL,
    security_level        CHAR(1) NOT NULL,
    user_update_options   VARCHAR(512) NOT NULL,
    user_functions        VARCHAR(512) NOT NULL,
    changed_by            INTEGER NOT NULL,
    creation_date         TIMESTAMP NOT NULL,
    user_password         VARCHAR(32) NOT NULL,
    email_address         VARCHAR(80),
    user_gui_settings     BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    cl_nbr                INTEGER,
    effective_date        TIMESTAMP NOT NULL,
    active_record         CHAR(1) NOT NULL,
    sb_accountant_nbr     INTEGER,
    wrong_pswd_attempts   INTEGER,
    links_data            VARCHAR(128),
    login_question1       INTEGER,
    login_answer1         VARCHAR(80),
    login_question2       INTEGER,
    login_answer2         VARCHAR(80),
    login_question3       INTEGER,
    login_answer3         VARCHAR(80),
    hr_personnel          CHAR(1) NOT NULL,
    sec_question1         INTEGER,
    sec_answer1           VARCHAR(80),
    sec_question2         INTEGER,
    sec_answer2           VARCHAR(80)
);


CREATE TABLE xb_user_notice (
    sb_user_notice_nbr  INTEGER NOT NULL,
    sb_user_nbr         INTEGER NOT NULL,
    name                VARCHAR(128) NOT NULL,
    notes               BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    task                BLOB SUB_TYPE 0 SEGMENT SIZE 80,
    last_dismiss        TIMESTAMP,
    next_reminder       TIMESTAMP,
    changed_by          INTEGER NOT NULL,
    creation_date       TIMESTAMP NOT NULL,
    effective_date      TIMESTAMP NOT NULL,
    active_record       CHAR(1) NOT NULL
);

COMMIT;


/* MOVE DATA INTO TEMP TABLES */


INSERT INTO xb_banks SELECT * FROM sb_banks;
COMMIT;
DROP TABLE sb_banks;
COMMIT;

INSERT INTO xb_blob (sb_blob_nbr, data, changed_by, creation_date, effective_date, active_record) SELECT sb_blob_nbr, data, 0, change_date, CAST('1/1/2000' AS TIMESTAMP), 'C' FROM sb_blob;
COMMIT;
DROP TABLE sb_blob;
COMMIT; 

INSERT INTO xb_delivery_company SELECT * FROM sb_delivery_company;
COMMIT;
DROP TABLE sb_delivery_company;
COMMIT;

INSERT INTO xb_delivery_company_svcs SELECT * FROM sb_delivery_company_svcs;
COMMIT;
DROP TABLE sb_delivery_company_svcs;
COMMIT;

INSERT INTO xb_delivery_method SELECT * FROM sb_delivery_method;
COMMIT;
DROP TABLE sb_delivery_method;
COMMIT;

INSERT INTO xb_delivery_service SELECT * FROM sb_delivery_service;
COMMIT;
DROP TABLE sb_delivery_service;
COMMIT;

INSERT INTO xb_delivery_service_opt SELECT * FROM sb_delivery_service_opt;
COMMIT;
DROP TABLE sb_delivery_service_opt;
COMMIT;

INSERT INTO xb_enlist_groups SELECT * FROM sb_enlist_groups;
COMMIT;
DROP TABLE sb_enlist_groups;
COMMIT;

INSERT INTO xb_global_agency_contacts SELECT * FROM sb_global_agency_contacts;
COMMIT;
DROP TABLE sb_global_agency_contacts;
COMMIT;

INSERT INTO xb_holidays SELECT * FROM sb_holidays;
COMMIT;
DROP TABLE sb_holidays;
COMMIT;

INSERT INTO xb_mail_box SELECT * FROM sb_mail_box;
COMMIT;
DROP TABLE sb_mail_box;
COMMIT;

INSERT INTO xb_mail_box_content SELECT * FROM sb_mail_box_content;
COMMIT;
DROP TABLE sb_mail_box_content;
COMMIT;

INSERT INTO xb_mail_box_option SELECT * FROM sb_mail_box_option;
COMMIT;
DROP TABLE sb_mail_box_option;
COMMIT;

INSERT INTO xb_media_type SELECT * FROM sb_media_type;
COMMIT;
DROP TABLE sb_media_type;
COMMIT;

INSERT INTO xb_media_type_option SELECT * FROM sb_media_type_option;
COMMIT;
DROP TABLE sb_media_type_option;
COMMIT;

INSERT INTO xb SELECT * FROM sb;
COMMIT;
DROP TABLE sb;
COMMIT;

INSERT INTO xb_accountant SELECT * FROM sb_accountant;
COMMIT;
DROP TABLE sb_accountant;
COMMIT;

INSERT INTO xb_agency SELECT * FROM sb_agency;
COMMIT;
DROP TABLE sb_agency;
COMMIT;

INSERT INTO xb_agency_reports SELECT * FROM sb_agency_reports;
COMMIT;
DROP TABLE sb_agency_reports;
COMMIT;

INSERT INTO xb_bank_accounts SELECT * FROM sb_bank_accounts;
COMMIT;
DROP TABLE sb_bank_accounts;
COMMIT;

INSERT INTO xb_multiclient_reports SELECT * FROM sb_multiclient_reports;
COMMIT;
DROP TABLE sb_multiclient_reports;
COMMIT;

INSERT INTO xb_option SELECT * FROM sb_option;
COMMIT;
DROP TABLE sb_option;
COMMIT;

INSERT INTO xb_other_service SELECT * FROM sb_other_service;
COMMIT;
DROP TABLE sb_other_service;
COMMIT;

INSERT INTO xb_paper_info SELECT * FROM sb_paper_info;
COMMIT;
DROP TABLE sb_paper_info;
COMMIT;

INSERT INTO xb_queue_priority SELECT * FROM sb_queue_priority;
COMMIT;
DROP TABLE sb_queue_priority;
COMMIT;

INSERT INTO xb_referrals SELECT * FROM sb_referrals;
COMMIT;
DROP TABLE sb_referrals;
COMMIT;

INSERT INTO xb_report_writer_reports SELECT * FROM sb_report_writer_reports;
COMMIT;
DROP TABLE sb_report_writer_reports;
COMMIT;

INSERT INTO xb_reports SELECT * FROM sb_reports;
COMMIT;
DROP TABLE sb_reports;
COMMIT;

INSERT INTO xb_sales_tax_states SELECT * FROM sb_sales_tax_states;
COMMIT;
DROP TABLE sb_sales_tax_states;
COMMIT;


INSERT INTO xb_sec_clients SELECT * FROM sb_sec_clients;
COMMIT;
DROP TABLE sb_sec_clients;
COMMIT;

INSERT INTO xb_sec_group_members SELECT * FROM sb_sec_group_members;
COMMIT;
DROP TABLE sb_sec_group_members;
COMMIT;

INSERT INTO xb_sec_groups SELECT * FROM sb_sec_groups;
COMMIT;
DROP TABLE sb_sec_groups;
COMMIT;

INSERT INTO xb_sec_rights SELECT * FROM sb_sec_rights;
COMMIT;
DROP TABLE sb_sec_rights;
COMMIT;

INSERT INTO xb_sec_row_filters SELECT * FROM sb_sec_row_filters;
COMMIT;
DROP TABLE sb_sec_row_filters;
COMMIT;

INSERT INTO xb_sec_templates SELECT * FROM sb_sec_templates;
COMMIT;
DROP TABLE sb_sec_templates;
COMMIT;

INSERT INTO xb_services SELECT * FROM sb_services;
COMMIT;
DROP TABLE sb_services;
COMMIT;

INSERT INTO xb_services_calculations SELECT * FROM sb_services_calculations;
COMMIT;
DROP TABLE sb_services_calculations;
COMMIT;

INSERT INTO xb_task 
  (sb_task_nbr, sb_user_nbr, schedule, description, task, effective_date, changed_by, creation_date, active_record) 
  SELECT sb_task_nbr, sb_user_nbr, schedule, description, task, effective_date, changed_by, creation_date, active_record FROM sb_task;
COMMIT;
DROP TABLE sb_task;
COMMIT;

INSERT INTO xb_storage SELECT * FROM sb_storage;
COMMIT;
DROP TABLE sb_storage;
COMMIT;


SET TERM ^;

EXECUTE BLOCK
AS
DECLARE VARIABLE sb_task_nbr INTEGER;
DECLARE VARIABLE last_run TIMESTAMP;
BEGIN
  FOR SELECT sb_task_nbr, MAX(creation_date) last_run
      FROM sb_task_runs
      WHERE  active_record = 'C'
      GROUP BY sb_task_nbr
      INTO :sb_task_nbr, :last_run
  DO
  BEGIN
    UPDATE xb_task SET last_run = :last_run WHERE sb_task_nbr = :sb_task_nbr AND active_record = 'C';
  END
END^

SET TERM ;^

COMMIT;
DROP TABLE sb_task_runs;
COMMIT;

INSERT INTO xb_tax_payment SELECT * FROM sb_tax_payment;
COMMIT;
DROP TABLE sb_tax_payment;
COMMIT;

INSERT INTO xb_team SELECT * FROM sb_team;
COMMIT;
DROP TABLE sb_team;
COMMIT;

INSERT INTO xb_team_members SELECT * FROM sb_team_members;
COMMIT;
DROP TABLE sb_team_members;
COMMIT;

INSERT INTO xb_user SELECT * FROM sb_user;
COMMIT;
DROP TABLE sb_user;
COMMIT;

INSERT INTO xb_user_notice (sb_user_notice_nbr, sb_user_nbr, name, notes, task, last_dismiss, 
  next_reminder, changed_by, creation_date, effective_date, active_record) 
  SELECT sb_user_notice_nbr, sb_user_nbr, name, notes, task, last_dismiss, 
  next_reminder,
  0, CAST('1/1/2000' AS TIMESTAMP), CAST('1/1/2000' AS TIMESTAMP), 'C'  FROM sb_user_notice;
COMMIT;
DROP TABLE sb_user_notice;
COMMIT;
