1. Synchronize schema with working database (user drop list = EUSER)
2. Extract metadata from the database (using Workbench or IBExpert) and put it into NewStructure_*.sql
3. Execute "Data Conversion" script 

The result is transition database script.




********** How to add a new table ************

1. Create a new Entity <TABLE_NAME> and its structure. Populate EV_TABLE_NBR and EV_FIELD_NBR attributes!
2. Create fields:
    REC_VERSION                     INTEGER NOT NULL
    <TABLE_NAME>_NBR                INTEGER NOT NULL
    EFFECTIVE_DATE                  DATE NOT NULL
    EFFECTIVE_UNTIL                 DATE NOT NULL
3. Set "Evo System Field" field attrivutes to "True" for REC_VERSION, EFFECTIVE_DATE, EFFECTIVE_UNTIL
4. Mark REC_VERSION field as a primary key
5. Create alternative key AK_<TABLE_NAME>_1(<TABLE_NAME>_NBR, EFFECTIVE_DATE)
6. Create alternative key AK_<TABLE_NAME>_2(<TABLE_NAME>_NBR, EFFECTIVE_UNTIL)
7. Create constraint C_<TABLE_NAME>_1 for entire table as CHECK (EFFECTIVE_DATE < EFFECTIVE_UNTIL)
8. Declare Log Key ("Evo Log Key #" attribute) if there is any.
9. Define versioning behaviour for entire table and fields ("Evo Versioning" attribute on table and field levels)
10.Switch to "Triggers" tab. Mark all ato-generated triggers as Ignore ("Action" attribute) and clear "Title" column.
11.Add entity <TABLE_NAME> to diagram called "ALL"
12.Run custom script "Check Model". It will show list of *_NBR fields which may require to create a relation for. Use it for creating relations.
   For all *_NBR fields which refer to external databases (e.g. CL -> SB) set field flag "Evo External Reference" to "True"

  How to create a relation:

  12.1. First create a non-identifying relationship using corresponding toolbar button (start from parent table). Select REC_VERSION field to match
  12.2. Right-click on the relationship and select "Set parent key"
  12.3. Select the alternative key defined as: (<TABLE_NAME>_NBR, EFFECTIVE_DATE)
  12.4. Match <TABLE_NAME>_NBR field and click "Auto-Create FK" for EFFECTIVE_DATE field. The dialog will close.
  12.5. Double-click on the child table and select "Fields" tab
  12.6. Scroll down to the last field called EFFECTIVE_DATE<NUMBER>.
  12.7. Uncheck PK attribute and check VIRTUAL attribute.
  12.8. Switch to "Incoming Relationship" tab and rename Title attribute of this relationship as the field name
  12.9. Create FK_<TABLE_NAME>_# index for referencing field. Add EFFECTIVE_UNTIL if the table is versioned.

 
13.Create DB structure from template for new tables

  !!!POTENTIAL SCHEMA BREAKAGE!!! All FK numeration of existing keys must have no "holes".

  13.1. Run custom script "Sync Database Objects" and apply it to DB (Ignore errors)
  13.2. Synchronize database. Select IMPORT option to bring new items into the model.
  13.3. Mark newly created index LK_<TABLE_NAME> "Evo Unique" attribute as True.
  13.4. Open the table editor and switch to "Triggers" tab. Edit trigger attributes according to this structure: 

NAME                    TITLE                                           ACTION                  TEMPLATE NAME

T_BIU_<TABLE>_3		Integrity: Fields and Records                   Epand Template          Constraints:  Before Insert or Update
T_BUD_<TABLE>_9		Audit: Log BLOB change operation		Epand Template		Audit: Before Update Delete BLOB
T_AU_<TABLE>_9		Audit: Log UPDATE operation	                Epand Template          Audit: After Update
T_AD_<TABLE>_9          Audit: Log DELETE operation                     Epand Template          Audit: After Delete
T_AIU_<TABLE>_3		Versioning: Handle non-versioning fields        Epand Template          Versioning: After Insert or Update
T_BIU_<TABLE>_2		Integrity: Outgoing Refs                        Epand Template          Integrity: Before Insert or Update
T_AUD_<TABLE>_2		Integrity: Incoming Refs                        Epand Template          Integrity: After Update or Delete
T_BU_<TABLE>_1		Versioning: Change EFFECTIVE_DATE               Epand Template          Versioning: Before Update
T_BI_<TABLE>_1		Versioning: Add a New Version                   Epand Template          Versioning: Before Insert
T_AU_<TABLE>_1		Versioning: Correct EFFECTIVE_UNTIL             Epand Template          Versioning: After Update
T_AD_<TABLE>_1		Versioning: Correct EFFECTIVE_UNTIL             Epand Template          Versioning: After Delete
T_AI_<TABLE>_9          Audit: Log INSERT operation                     Epand Template          Audit: After Insert

T_BIU_<TABLE>_1         Default: Fields values                          Expand Template         Default:  Before Insert or Update


14.Stored Procedures definition. Open the table editor and switch to "Procedures" tab. Add the following procedures:

NAME                    TITLE                                           ACTION                  TEMPLATE NAME

PACK_<TABLE>            Versioning: Eliminate duplicate records         Epand Template          Pack Table
DEL_<TABLE>             Versioning: Delete NBR records                  Epand Template          Delete NBR

15.Run custom script "Check Model" again and see that it has no errors.

16.Synchronize model with database. Select ALTER option to commit changes to database. 



********** How to edit table ************

If incoming relationships get deleted you need to take care of FK indexes (because of naming convention FK_<TABLE>_#).

1. Delete all FK_* indexes from child table.
2. Synchronize database. Select ALTER option.
3. Close xCase and copy files DDTRG.cdx, DDTRG.dbf, DDTRG.fpt in safe place.
4. Run custom script "Sync Database Objects" and apply it to DB (Ignore errors)
5. Synchronize database. Select IMPORT option to bring new items into the model
6. Close xCase and copy preserved files DDTRG.cdx, DDTRG.dbf, DDTRG.fpt back in in place.


********** Good practices ************

If there are no discrepancies between model and database the following is true:

1. Try to synchronize model with database. It should be in sync.
2. Run custom script "Check Model". It should not contain errors.
3. Run custom script "Sync Database Objects" and apply it to DB (Ignore errors)
4. Try to synchronize model with database. It should stay in sync.



****************  EXCEPTIONS OF RULES ****************

Table CO has too many fields(300+) but triggers have a limitation of 255 context variables (OLD.*, NEW.*).
Also size of a trigger/procedure cannot be more than 64Kb. 
So, some triggers/procedures are split on two/three pieces and implemented as a custom code.
Pay attention to "# Workaround for Firebird limitation" comments in template scripts.


DROP TRIGGER T_AU_CO_8
^




****************  KNOWN ISSUES ****************
1. xCase does not detect discrepancy if a stored procedures in the database and the model do not match in their input and    output parameters. It is strongly recommended to recreate all stored procedures at the end of work 
   (MM: \DataBase\Create/Drop Objects From DataBase…). This issue is related to xCase  and  Firebird 2.0. Starting from    FB 2.1 there is a workaround as updating   XC_TEMP_PROC_PROCEDURES  in DDL.DBF file.



Running backup through service manager:

gbak -SE spock:service_mgr -B -USER euser -PASSWORD pps97 /db/new/CL_BASE.gdb /db/new/12345.gbk

