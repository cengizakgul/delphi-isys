source ./Evolution/Scripts/EvoLib.tcl 

syncEvDatabase

if {$EV_TEMP_DB} then { return }

>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"
>> "  DELETE FROM ev_field;"
>> "  DELETE FROM ev_table;"

set table_list [dict list_of_tables] 
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [truevalue]} {continue}   
  
  set table_name [$table attr NAME]  
  if {[$table attr EV_VERSION] == [truevalue]} then {  
    set versioned Y;
  } else {
    set versioned N;
  }

  set table_nbr [getTableNbr $table]   
>> "  INSERT INTO ev_table (nbr, name, versioned) VALUES ($table_nbr, '$table_name', '$versioned');"
}
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"
>> "END"
>> "^"
>> ""

set statements_count 0
foreach table $table_list {
  if {[$table attr EV_SYSTEM] == [truevalue]} {continue}   
  
  set table_nbr [getTableNbr $table]     
  set table_name [$table attr NAME]  
  set field_list [$table list_of_fields]     
    
  foreach field $field_list { 
    if {[$field attr LOGICAL] == [truevalue]} {continue}   
                     
    set field_name [$field attr NAME]        
    set field_nbr [getFieldNbr $field]                             
    set field_type [[$field domain] attr TYPE]
    set field_len NULL                                  
    set field_scale NULL
		
    if {[$field attr EV_VERSION] == [truevalue]} then {  
      set versioned Y;
    } else {
      set versioned N;
    }

    if {$field_type == "INTEGER"} {
      set field_type "I"                 
    } elseif {$field_type == "SMALLINT"} {
      set field_type "S"  
    } elseif {$field_type == "VARCHAR"} {
      set field_type "V"                      
      set field_len [$field attr LEN]
    } elseif {$field_type == "CHAR"} {
      set field_type "C"
      set field_len [$field attr LEN]
    } elseif {$field_type == "FLOAT"} {
      set field_type "F"
    } elseif {$field_type == "DOUBLE PRECISION"} {
      set field_type "E"
    } elseif {$field_type == "DATE"} {
      set field_type "D"
    } elseif {$field_type == "TIMESTAMP"} {
      set field_type "T"
    } elseif {$field_type == "BLOB"} {
      set field_type "B"
    } elseif {$field_type == "NUMERIC"} {
      set field_type "N"                  
      set field_len [$field attr LEN]
      set field_scale [$field attr DEC]
    } else {
      error "$table_name.$field_name: type '$field_type' is not supported"  
    }

    if {[$field attr NULL] == [truevalue]} {
      set field_required N
    } else {          
      set field_required Y
    }     
    
	if {$statements_count == 200} then {
>> "END"
>> "^"
>> ""

	  set statements_count 0
	}
	
	if {$statements_count == 0} then {
>> "EXECUTE BLOCK"
>> "AS"
>> "BEGIN"
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', 1);"
	}	
>> "  INSERT INTO ev_field (ev_table_nbr, nbr, name, field_type, len, scale, required, versioned) VALUES ($table_nbr, $field_nbr, '$field_name', '$field_type', $field_len, $field_scale, '$field_required', '$versioned');"
	incr statements_count  
  }  
}  
>> "  rdb\$set_context('USER_TRANSACTION', 'MAINTENANCE', NULL);"  
>> "END"
>> "^"
>> ""
>> "COMMIT"
>> "^"
>> ""

    