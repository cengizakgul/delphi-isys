source ./Evolution/Scripts/EvoLib.tcl 

proc compare {a b} {
  if {[$a attr name] < [$b attr name]} then {
    return -1
  } elseif {[$a attr name] > [$b attr name]} then {
    return 1
  }
  return 0
}

set table_list [lsort -command compare [dict list_of_tables]]
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }
  if {[$table attr EV_VERSION] == [falsevalue]} then { continue }

  if {[$table attr EV_VERSION]} {
	  set tver "Y"
	} else {
	  set tver ""
	}  
>> "[$table attr NAME];;$tver"
  
  set fields [lsort -command compare [$table list_of_fields]] 
  foreach field $fields {       
    if {[$field attr EV_SYSTEM]} then { continue }    
	if {[$field attr EV_VERSION] == [falsevalue]} then { continue }
	
	if {[$field attr EV_VERSION]} {
	  set fver "Y"
	} else {
	  set fver ""
	}
>> ";[$field attr NAME];$fver"
  }  
>> ""  
>> ""  
}
