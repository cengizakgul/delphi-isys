source ./Evolution/Scripts/EvoLib.tcl 

set table_list [dict list_of_tables]

# Check missing relations
>> "========= Missing relations ========="
set modelName [string toupper [[dict get_model] attr model_name]]
if {$modelName == "SYSTEM"} then {
  set ignore_prif {SB_ CL_ CO_ PR_ EE_}
} elseif {$modelName == "BUREAU"} then {
  set ignore_prif {SY_ CL_ CO_ PR_ EE_}
} elseif {$modelName == "CLIENT"} then {
  set ignore_prif {SY_ SB_}
}

foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }
  
  set table_PK_field [$table attr name]\_NBR
  
  set field_list [$table list_of_fields]
  set relations [$table in_relations]  
  
  foreach field $field_list { 
    if {[$field attr ev_ext_fk] == "" && [string match *_NBR [$field attr name]] == 1 && 
	    $table_PK_field != [$field attr name] && [lsearch $ignore_prif [string range [$field attr name] 0 2]] == -1} then {
	  set found [falsevalue]
  	  set rel_nbr 0
      foreach relation $relations {         
        set rel_nbr [expr $rel_nbr + 1]
        set key_field_list [$relation list_of_foreign_keys]
        foreach key_field $key_field_list {
          if {[$key_field attr LOGICAL] == [falsevalue] && [$field attr name] == [$key_field attr name]} then {
		    set found [truevalue]  
            break
		  }
        }
        if {$found}	then { 
		  break 
		}
      }	
	  
	  if {$found == [falsevalue]} then { 
>> "   [$table attr name].[$field attr name]"
	  }	  
    }           
  } 
}
>> ""
>> ""

>> "========= Missing external relations ========="
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }
  
  set table_PK_field [$table attr name]\_NBR
  
  set field_list [$table list_of_fields]
  set relations [$table in_relations]  
  
  foreach field $field_list { 
    if {[$field attr ev_ext_fk] == "" && [string match *_NBR [$field attr name]] == 1 && 
	    $table_PK_field != [$field attr name] && [lsearch $ignore_prif [string range [$field attr name] 0 2]] != -1} then {
>> "   [$table attr name].[$field attr name]"	  
	}	  
  } 
}
>> ""
>> ""


# Check duplicate relations
>> "========= Duplicate relations ========="
foreach table $table_list {
  if {[$table attr EV_SYSTEM]} then { continue }

  set relations [$table in_relations]
  set rel_nbr 0
  
  foreach relation $relations {       
    if {[$relation attr NORMAL]} then { continue }    

    set rel_nbr [expr $rel_nbr + 1]
    set field_list [$relation list_of_foreign_keys]
    foreach field $field_list {
      if {[$field attr LOGICAL] == [falsevalue]} then {
        set child_ref [string tolower [$field attr NAME]]
        break
      }         
    }           
    if {[isVerTable $table]} then {
	  append child_ref ", effective_until"
    }
	  
    foreach relation2 $relations {       
      if {[$relation2 attr NORMAL] || [$relation2 attr NAME] == [$relation attr NAME]} then { continue }    

      set rel_nbr [expr $rel_nbr + 1]
      set field_list [$relation2 list_of_foreign_keys]
      foreach field $field_list {
        if {[$field attr LOGICAL] == [falsevalue]} then {
          set child_ref2 [string tolower [$field attr NAME]]
          break
        }         
      }           	  
      if {[isVerTable $table]} then {
        append child_ref2 ", effective_until"
      }
		  
      if {$child_ref == $child_ref2} then {
>> "   [$table attr NAME]: [$relation attr NAME] and [$relation2 attr NAME]"
		break;
      }
    }
  }  
}
>> ""
>> ""

 # Check Triggers
>> "========= Missing triggers ========="
foreach table $table_list {
  if {[$table attr EV_SYSTEM] || [isVerAwareTable $table] == [falsevalue]} then { continue }

  set trig_list [$table list_of_triggers]  
  set table_name [$table attr NAME]
  set trig_name [string range $table_name 0 22]	
	  
  set trig T_BIU_$trig_name\_3  
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }
	  
  set trig T_BUD_$trig_name\_9  
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }

  set trig T_AU_$trig_name\_9  
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }
	  
  set trig T_AD_$trig_name\_9  
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }
	  
  set trig T_AIU_$trig_name\_3 
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }

  set trig T_BIU_$trig_name\_2 
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }
	  
  set trig T_AUD_$trig_name\_2 
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }

  set trig T_BU_$trig_name\_1 
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }
	 
  set trig T_BI_$trig_name\_1
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }

  set trig T_AU_$trig_name\_1
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }

  set trig T_AD_$trig_name\_1
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }
	  
  set trig T_AI_$trig_name\_9
  if {[trigInList $trig_list $trig] == [falsevalue]} then {
>> "$table_name  $trig"
  }	  
  
}	
>> ""	
>> ""  

 # Check procedures
>> "========= Missing procedures ========="
set proc_list [dict list_of_procedures]  
foreach table $table_list {
  if {[$table attr EV_SYSTEM] || [isVerAwareTable $table] == [falsevalue]} then { continue }
 
  set table_name [$table attr NAME]
  set proc_name [string range $table_name 0 26]	
	  
  set proc PACK_$proc_name
  if {[procInList $proc_list $proc] == [falsevalue]} then {
>> "$proc_name  $proc"
  }  
  
  set proc PACK_$proc_name
  if {[procInList $proc_list $proc] == [falsevalue]} then {
>> "$proc_name  $proc"
  }    
}	
>> ""	
>> ""  
