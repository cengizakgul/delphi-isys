source ./Evolution/Scripts/EvoLib.tcl 

proc validTable {tname} {
  return [expr {[string range $tname 0 2] != "EV_"}] 
}

proc validField {fname} {
  return [expr {$fname != "REC_VERSION" && $fname != "EFFECTIVE_DATE" && $fname != "EFFECTIVE_UNTIL"}]   
}


set modelName [string tolower [[dict get_model] attr model_name]]
>> "// Copyright � 2000-2012 iSystems LLC. All rights reserved."
>> "// This is automatically generated file"
>> "// Do not modify it!"
>> ""
>> "unit SDataDict$modelName;"
>> ""
>> "interface"
>> ""
>> "uses"
>> "  SDDClasses, db, classes, EvTypes;"
>> ""
>> "type"
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]  
  if {[validTable $tname]} {
    if {[string range $tname 0 1] == "SY"} {
      set sType "TddSYTable"
    } elseif {[string range $tname 0 1] == "SB"} {
      set sType "TddSBTable"
    } elseif {[string range $tname 0 2] == "TMP"} {
      set sType "TddTMPTable"	  
    } elseif {[string range $tname 0 1] == "CL"} {
      set sType "TddCLTable"
    } elseif {[string range $tname 0 1] == "CO"} {
      set sType "TddCOTable"
    } elseif {[string range $tname 0 1] == "EE"} {
      set sType "TddEETable"
    } elseif {[string range $tname 0 1] == "PR"} {
      set sType "TddPRTable"
    } else {
      set sType "TddTable"
    }

    >> "  T$tname = class($sType)"
    >> "  public"
    >> "    class function GetClassIndex: Integer; override;"
    >> "    class function GetRequiredFields: TddFieldList; override;"
    >> "    class function GetParentsDesc: TddReferencesDesc; override;"
    >> "    class function GetChildrenDesc: TddReferencesDesc; override;"
    >> "    class function GetFieldSize(FieldName: string): Integer; override;"
    >> "    class function GetFieldPrecision(FieldName: string): Integer; override;"
    >> "    class function GetPrimaryKey: TddFieldList; override;"    
    >> "    class function GetLogicalKeys: TddFieldList; override;"    
    >> "    class function GetEntityName: String; override;"    	
    >> "    class function GetFieldEntityProperty(const FieldName: string): string; override;"    		    
  
  if {$EV_TEMP_DB == [falsevalue]} then {
	>> "    class function IsVersionedTable: Boolean; override;"    	
    >> "    class function IsVersionedField(FieldName: string): Boolean; override;"	
    >> "    class function GetEvTableNbr: Integer; override;"    			
    >> "    class function GetEvFieldNbr(const AFieldName: string): Integer; override;"    		
  }
	
    >> "  published"
    set cnt 1
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [falsevalue]} {
        set ftype [[$field domain] attr type]
        if {$ftype == "INTEGER"} {
          set typename "IntegerField"
        } elseif {$ftype == "SMALLINT"} {
          set typename "IntegerField"
        } elseif {$ftype == "CHAR" || $ftype == "VARCHAR"} {
          set typename "StringField"
        } elseif {$ftype == "TIMESTAMP"} {
          set typename "DateTimeField"
        } elseif {$ftype == "DATE"} {
          set typename "DateField"
        } elseif {$ftype == "NUMERIC"} {
          set typename "FloatField"
        } elseif {$ftype == "FLOAT"} {
          set typename "FloatField"
        } elseif {$ftype == "DOUBLE PRECISION"} {
          set typename "FloatField"       
        } elseif {$ftype == "BLOB"} {
          set typename "BlobField"
        } else {
          error "Type $ftype is unknown"
        }
        >> "    property [$field attr name]: T$typename index $cnt read Get$typename;"
        incr cnt 1
      }
    }
    >> "  end;"
    >> ""
  }
}     
if {$modelName == "client"} {
  >> "  TDM_CLIENT_ = class(TddCLDatabase)"                                               
  set dbtype "TDM_CLIENT_"
} elseif {$modelName == "bureau"} {
  >> "  TDM_BUREAU_ = class(TddDatabase)"                                               
  set dbtype "TDM_BUREAU_"
} elseif {$modelName == "temp"} {
  >> "  TDM_TEMPORARY_ = class(TddDatabase)"                                               
  set dbtype "TDM_TEMPORARY_"
} elseif {$modelName == "system"} {
  >> "  TDM_SYSTEM_ = class(TddDatabase)"
  set dbtype "TDM_SYSTEM_"
}
>>  "  public"
>>  "    class function GetDBType: TevDBType; override;"
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]
  if {[validTable $tname]} {
    >> "  private";
    >> "    function Get$tname: T$tname;"
    >> "  published"
    >> "    property $tname: T$tname read Get$tname;"
  }
}
>> "  end;"
>> ""
>> ""

if {$EV_TEMP_DB} then {

>> "const"

  set cnt 0
  foreach table [dict list_of_tables] {
    if {[$table attr EV_SYSTEM]} then { continue }
	incr cnt [llength [$table in_relations]]    
  }
  incr cnt -1  
>> "  TMP_Constraints: array \[0..$cnt\] of string = ("

  set line_cnt 0
  foreach table [dict list_of_tables] {
    if {[$table attr EV_SYSTEM]} then { continue }
  
    foreach relation [$table in_relations] {         
      if {$line_cnt > 0} then { >> "," }
	  
>>> "    'ALTER TABLE [$table attr name] ADD CONSTRAINT [$relation attr name] FOREIGN KEY ("
      set cnt 0
      foreach key_field [getRelationFK $relation] {
        if {$cnt > 0} then { >>> ", " }
          >>> "[$key_field attr name]"
        incr cnt 1      
      }		  
  	  >>> ") REFERENCES [[$relation parent] attr name] ("		

	  set cnt 0
      foreach key_field [$relation master_fields_names] {	
        if {$cnt > 0} then { >>> ", " }
          >>> "$key_field"
          incr cnt 1      
      }	
      >>> ")'"		
  
	  incr line_cnt 1
    }
  }
>> ""  
>> "  );"  
>> ""

  set cnt 0
  foreach table [dict list_of_tables] {
    if {[$table attr EV_SYSTEM]} then { continue }
	
	foreach index [$table list_of_indexes] {
	  if {[$index attr P_INDEX] != [truevalue]} {
  	    incr cnt 1
      }
	}
  }
  incr cnt -1  
>> "  TMP_Indices: array \[0..$cnt\] of string = ("

  set line_cnt 0
  foreach table [dict list_of_tables] {
    if {[$table attr EV_SYSTEM]} then { continue }
   
	foreach index [$table list_of_indexes] {
	  if {[$index attr P_INDEX]} { continue }
	  
      if {$line_cnt > 0} then { >> "," }
	  
      >>> "    'CREATE " 
      if { [$index attr TYPE] == "U" } {
        >>> "UNIQUE "
      }
      if { [$index attr DESCENDING] == [truevalue] } {
        >>> "DESCENDING "
      }
      >>> "INDEX [$index attr NAME] ON [[$index entity] attr NAME] ("
      set col_list [$index list_of_elements]
      set cnt 0
      foreach col $col_list {
        if { $cnt > 0} { >>> "," }  
        >>> "[[$col FIELD] attr NAME]"
        incr cnt 1
      }
	  >>> ")'"

	  incr line_cnt 1	  	  
    }
  }
>> ""  
>> "  );"  
>> "" 
>> "" 
}

>> "procedure Register;"
>> ""                            
>> "implementation"
>> ""                            
if {$modelName == "client"} {
  set cnt 451
>> "uses SDataDictBureau, SDataDictSystem;"  
>> ""                            
} elseif {$modelName == "bureau"} {
  set cnt 151
>> "uses SDataDictSystem;"  
>> ""                             
} elseif {$modelName == "temp"} {
  set cnt 301
} elseif {$modelName == "system"} {
  set cnt 1
}
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]
  if {[validTable $tname]} {
    >> "{T$tname}"
    >> ""                            
    >> "class function T$tname.GetClassIndex: Integer;"
    >> "begin"
    >> "  Result := $cnt;"
    >> "end;"
    >> ""                         

    if {$EV_TEMP_DB == [falsevalue]} then {	
    >> "class function T$tname.IsVersionedTable: Boolean;"
    >> "begin"
    >>> "  Result := "
      if {[isVerTable $table]} then {
	  >> "True;"
	  } else {
	  >> "False;"	
	  }	
    >> "end;"	
    >> ""
    >> "class function T$tname.GetEvTableNbr: Integer;"
    >> "begin"
    >> "  Result := [getTableNbr $table];"	
    >> "end;"	
    >> ""	
	}
	
    >> "class function T$tname.GetEntityName: String;"
    >> "begin"
    >>> "  Result := "
	if {[$table attr EV_ALIAS] != ""} then {
	  >> "'[$table attr EV_ALIAS]';"
	} else {
	  >> "'';"	
	}	
    >> "end;"		
    >> ""  
	
    if {$EV_TEMP_DB == [falsevalue]} then {		
    >> "class function T$tname.IsVersionedField(FieldName: string): Boolean;"
    >> "begin"                                                      
    >> "  if false then"                                                      
      foreach field [$table list_of_fields] {
        if {[$field attr logical] == [falsevalue] && [validField [$field attr name]] && [isVerField $field]} {
      >> "  else if FieldName = '[$field attr name]' then Result := True"
        }
      }
    >> "  else Result := inherited IsVersionedField(FieldName);"                                                      
    >> "end;"		
    >> ""  	
    >> "class function T$tname.GetEvFieldNbr(const AFieldName: string): Integer;"
    >> "begin"                                                      
    >> "  if false then"                                                      
      foreach field [$table list_of_fields] {
        if {[$field attr logical] == [falsevalue]} {
      >> "  else if AFieldName = '[$field attr name]' then Result := [getFieldNbr $field]"
        }
      }
    >> "  else Result := inherited GetEvFieldNbr(AFieldName);"                                                      
    >> "end;"	
    >> ""  		
	}
	
    >> "class function T$tname.GetFieldEntityProperty(const FieldName: string): String;"
    >> "begin"                                                      
    >> "  if false then"                                                      
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [falsevalue] && [validField [$field attr name]] && [$field attr EV_ALIAS] != ""} {
      >> "  else if FieldName = '[$field attr name]' then Result := '[$field attr EV_ALIAS]'"
      }
    }
    >> "  else Result := inherited GetFieldEntityProperty(FieldName);"                                                      
    >> "end;"		
    >> ""                         
    >> "class function T$tname.GetRequiredFields: TddFieldList;"
    >> "begin"                                                      
    set mcnt 0
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [falsevalue] && [validField [$field attr name]] && [$field attr null] == [falsevalue]} {
        incr mcnt
      }
    } 
    >> "  SetLength(Result, $mcnt);"
    set mcnt 0
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [falsevalue] && [validField [$field attr name]] && [$field attr null] == [falsevalue]} {
        >> "  Result\[$mcnt\] := '[$field attr name]';"
        incr mcnt
      }
    } 
    >> "end;"
    >> ""                            
    >> "class function T$tname.GetFieldSize(FieldName: string): integer;"
    >> "begin"                                                      
    >> "  if false then"                                                      
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [falsevalue] && [validField [$field attr name]]} {
        set ftype [[$field domain] attr type]
        if {$ftype == "CHAR" || $ftype == "VARCHAR"} {
          >> "  else if FieldName = '[$field attr name]' then Result := [$field attr len]"
        }
      }
    }
    >> "  else Result := inherited GetFieldSize(FieldName);"                                                      
    >> "end;"
    >> ""                            
    >> "class function T$tname.GetFieldPrecision(FieldName: string): integer;"
    >> "begin"                                                      
    >> "  if false then"                                                      
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [falsevalue] && [validField [$field attr name]]} {
        set ftype [[$field domain] attr type]
        if {$ftype == "NUMERIC"} {
          >> "  else if FieldName = '[$field attr name]' then Result := [$field attr dec]"
        }
        if {$ftype == "FLOAT"} {
          >> "  else if FieldName = '[$field attr name]' then Result := 7"
        }
        if {$ftype == "DOUBLE PRECISION"} {
          >> "  else if FieldName = '[$field attr name]' then Result := 15"
        }
      }
    }
    >> "  else Result := inherited GetFieldPrecision(FieldName);"                                            
    >> "end;"
    >> ""
    >> "class function T$tname.GetPrimaryKey: TddFieldList;"
    >> "begin"                          
	
	set aks [$table list_of_alternates]	
    if {[llength $aks] > 0} {
	  set pk_fields [[lindex $aks 0] list_of_fields]
	} else {
	  set pk_fields [$table list_of_primary]	
	}
	
	set acnt 0
    foreach field $pk_fields {
      if {[validField [$field attr name]]} {          
	    incr acnt
      }
    }	
    >> "  SetLength(Result, $acnt);"                                            
	set acnt 0
    foreach field $pk_fields {
      if {[validField [$field attr name]]} {          
    >> "  Result\[$acnt\] := '[$field attr name]';"                                            	  
	    incr acnt
      }
    }	
    >> "end;"
    >> ""                      
    
	>> "class function T$tname.GetParentsDesc: TddReferencesDesc;"
    >> "begin"                                                      
    >> "  Result := inherited GetParentsDesc;"
    foreach relation [$table in_relations] {
      set trname [[$relation parent] attr name]
      if {[string range $trname 0 1] == "SY"} {
        set sType "TDM_SYSTEM_"
      } elseif {[string range $trname 0 1] == "SB"} {
        set sType "TDM_BUREAU_"
      } elseif {[string range $trname 0 2] == "TMP"} {
        set sType "TDM_TEMPORARY_"
      } else {
        set sType "TDM_CLIENT_"    
      } 
      >> "  SetLength(Result, Succ(Length(Result)));"
      >> "  Result\[High(Result)\].Database := $sType;"
      >> "  Result\[High(Result)\].Table := T$trname;"
      if {$modelName != "temp"} {
        set fcount 1
      } else {
        set fcount [llength [$relation list_of_foreign_keys]]
      }
      >> "  SetLength(Result\[High(Result)\].SrcFields, $fcount);"
      >> "  SetLength(Result\[High(Result)\].DestFields, $fcount);"   
      set mcnt 0
      foreach field [$relation list_of_foreign_keys] {
        if {[$field attr logical] == [falsevalue]} {
          >> "  Result\[High(Result)\].SrcFields\[$mcnt\] := '[$field attr name]';"
          >> "  Result\[High(Result)\].DestFields\[$mcnt\] := '[[$field parent $relation] attr name]';"
          incr mcnt
        }
      }
      if {$fcount == 2} {
        >> "  Result\[High(Result)\].SrcFields\[1\] := 'CL_NBR';"
        >> "  Result\[High(Result)\].DestFields\[1\] := 'CL_NBR';"
      }
	  
	  if {[$relation attr ev_mstdet]} then {
        >> "  Result\[High(Result)\].MasterDetail := True;"
	  } else {
        >> "  Result\[High(Result)\].MasterDetail := False;"	  
	  }
	  
	  if {[$relation attr ev_alias] != ""} then {
        >> "  Result\[High(Result)\].ParentEntityPropName := '[$relation attr ev_alias]';"
	  } else {
        >> "  Result\[High(Result)\].ParentEntityPropName := '';"	  
	  }	  
    }
	
    foreach field [$table list_of_fields] {
      if {[$field attr logical] == [truevalue] || [validField [$field attr name]] == [falsevalue] || [$field attr ev_ext_fk] == "" || [$field attr ev_ext_fk] == "#"} {        
	    continue		
      }	  
      set trname [$field attr ev_ext_fk]
      if {[string range $trname 0 1] == "SY"} {
        set sType "TDM_SYSTEM_"
      } elseif {[string range $trname 0 1] == "SB"} {
        set sType "TDM_BUREAU_"
      } elseif {[string range $trname 0 2] == "TMP"} {
        set sType "TDM_TEMPORARY_"
      } else {
        set sType "TDM_CLIENT_"    
      } 
      >> "  SetLength(Result, Succ(Length(Result)));"
      >> "  Result\[High(Result)\].Database := $sType;"
      >> "  Result\[High(Result)\].Table := T$trname;"
      >> "  SetLength(Result\[High(Result)\].SrcFields, 1);"
      >> "  SetLength(Result\[High(Result)\].DestFields, 1);"   
      >> "  Result\[High(Result)\].SrcFields\[0\] := '[$field attr name]';"
      >> "  Result\[High(Result)\].DestFields\[0\] := '$trname\_NBR';"  
    } 
	
    >> "end;"
    >> ""    
	
    >> "class function T$tname.GetChildrenDesc: TddReferencesDesc;"
    >> "begin"                                                      
    >> "  Result := inherited GetChildrenDesc;"
    foreach relation [$table out_relations] {
      set trname [[$relation child] attr name]
      if {[string range $trname 0 1] == "SY"} {
        set sType "TDM_SYSTEM_"
      } elseif {[string range $trname 0 1] == "SB"} {
        set sType "TDM_BUREAU_"
      } elseif {[string range $trname 0 2] == "TMP"} {
        set sType "TDM_TEMPORARY_"
      } else {
        set sType "TDM_CLIENT_"    
      } 
      >> "  SetLength(Result, Succ(Length(Result)));"
      >> "  Result\[High(Result)\].Database := $sType;"
      >> "  Result\[High(Result)\].Table := T$trname;"
      if {$modelName != "temp"} {
        set fcount 1
      } else {
        set fcount [llength [$relation list_of_foreign_keys]]
      }
      >> "  SetLength(Result\[High(Result)\].SrcFields, $fcount);"
      >> "  SetLength(Result\[High(Result)\].DestFields, $fcount);"   
      set mcnt 0
      foreach field [$relation list_of_foreign_keys] {
        if {[$field attr logical] == [falsevalue]} {
          >> "  Result\[High(Result)\].SrcFields\[$mcnt\] := '[[$field parent $relation] attr name]';"
          >> "  Result\[High(Result)\].DestFields\[$mcnt\] := '[$field attr name]';"
          incr mcnt
        }
      }
      if {$fcount == 2} {
        >> "  Result\[High(Result)\].SrcFields\[1\] := 'CL_NBR';"
        >> "  Result\[High(Result)\].DestFields\[1\] := 'CL_NBR';"
      }
    }
    >> "end;"
    >> ""                            
	
    set lkFields [getLKfields $table]
    >> "class function T$tname.GetLogicalKeys: TddFieldList;"
    >> "begin"
    >> "  SetLength(Result, [llength $lkFields]);"
    set mcnt 0 	
    foreach field $lkFields {              
      >> "  Result\[$mcnt\] := '[$field attr name]';"
      incr mcnt
    }
    >> "end;"
    >> ""                            
	
    incr cnt
  }
}

>> "{$dbtype}"
>> ""                            
>> "class function $dbtype.GetDBType: TevDBType;"
>> "begin"  
set dbt [string range $dbtype 4 4]
if {$dbt == "S"} {
  set dbt "dbtSystem"
} elseif {$dbt == "B"} {
  set dbt "dbtBureau"
} elseif {$dbt == "C"} {
  set dbt "dbtClient"
} elseif {$dbt == "T"} {
  set dbt "dbtTemporary"
}
>> "  Result := $dbt;"
>> "end;"
>> ""                            
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]
  if {[validTable $tname]} {
    >> "function $dbtype.Get$tname: T$tname;"
    >> "begin"
    >> "  Result := GetDataSet(T$tname) as T$tname;"
    >> "end;"
    >> ""
  }
}            

set mcnt 0
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]
  if {[validTable $tname]} {
    incr mcnt
  }
}

>> "procedure Register;"
>> "begin"
if {$modelName == "client"} {
  >> "  RegisterComponents('EvoDdClient', \["
} elseif {$modelName == "bureau"} {
  >> "  RegisterComponents('EvoDdBureau', \["
} elseif {$modelName == "temp"} {
  >> "  RegisterComponents('EvoDdTemp', \["
} elseif {$modelName == "system"} {
  >> "  RegisterComponents('EvoDdSystem', \["
}
set ccnt 0
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]
  if {[validTable $tname]} {
    >>> "    T$tname"
    incr ccnt    
    if {$ccnt < $mcnt} {
      >> ","
    }
  }
}
>> "\]);"
>> "end;"
>> ""
>> "initialization"
>> "  RegisterClasses(\["
set ccnt 0
foreach table [dict list_of_tables] {
  set tname [$table attr NAME]
  if {[validTable $tname]} {
    >>> "    T$tname"
    incr ccnt    
    if {$ccnt < $mcnt} {
      >> ","
    }
  }
}
>> "\]);"
>> ""
>> "end."
