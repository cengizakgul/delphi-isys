unit SQueueViewFormChoosePr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmChoosePriority = class(TForm)
    lbxPriority: TListBox;
    EnableFiltering: TCheckBox;
    Button1: TButton;
    procedure lbxPriorityDblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }

    procedure Choose(sl:TStringList;var Priority:string;var Filtered:boolean);
  end;

implementation

{$R *.dfm}

{ TfrmChoosePriority }

procedure TfrmChoosePriority.Choose(sl: TStringList;var Priority:string;var Filtered:boolean);
begin
   lbxPriority.Items.CommaText := sl.CommaText;
   lbxPriority.ItemIndex := lbxPriority.Items.IndexOf(Priority);
   EnableFiltering.Checked := Filtered;
   if ShowModal = mrOK then
   begin
     if lbxPriority.ItemIndex <> -1 then
        Priority := lbxPriority.Items[lbxPriority.ItemIndex]
     else
        Priority := '';
     Filtered := EnableFiltering.Checked ;
   end;

end;

procedure TfrmChoosePriority.lbxPriorityDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

end.
