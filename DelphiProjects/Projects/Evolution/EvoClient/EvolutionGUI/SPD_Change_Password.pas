// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Change_Password;

interface

uses Windows, SysUtils, Classes, Forms, Controls, StdCtrls, DateUtils, EvTypes,
     Buttons, ExtCtrls, Dialogs, EvContext, ISBasicClasses,  EvUtils,
     EvBasicUtils, EvMainboard, EvExceptions, EvUIUtils, EvUIComponents;

type
  TChange_Password = class(TForm)
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    Bevel1: TBevel;
    lablNew_Password: TevLabel;
    lablConfirm_New_Password: TevLabel;
    editNew_Password: TEdit;
    editConfirm_New_Password: TEdit;
    Memo1: TMemo;
    lPasswordRules: TevLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  private
    FPasswordExpired: Boolean;
  end;

  function CheckPasswordChange(const AForce: Boolean): Boolean;

implementation


{$R *.DFM}

function CheckPasswordChange(const AForce: Boolean): Boolean;
var
  bExpired: Boolean;
begin
  bExpired := Context.UserAccount.PasswordChangeDate <= Date;  // password has expired
  Result := AForce or bExpired;

  if not Result then  // 7 days reminder
    Result := (Context.UserAccount.PasswordChangeDate <= Date + 7) and
      (EvMessage(
         Format('Your password will expire in %d days (on %s). Would you like to change your password now?',
                [DaysBetween(Date, Context.UserAccount.PasswordChangeDate), DateToStr(Context.UserAccount.PasswordChangeDate)]),
         mtConfirmation, [mbYes,mbNo]) = mrYes);

  if Result then
    with TChange_Password.Create(nil) do
      try
        FPasswordExpired := bExpired;
        Result := ShowModal = mrOk;
      finally
        Free;
      end
  else
    Result := True;
end;

procedure TChange_Password.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  sUser: String;
begin
  if ModalResult = mrOK then
  begin
    CanClose := False;
    if editNew_Password.Text = '' then
      raise EChangePasswordException.CreateHelp('New password can''t be empty. Try again.', IDH_ChangePasswordException);
    if editNew_Password.Text <> editConfirm_New_Password.Text then
      raise EChangePasswordException.CreateHelp('New password has been mistyped. Try again.', IDH_ChangePasswordException);
    if HashPassword(editNew_Password.Text) = Context.UserAccount.Password then
      raise EChangePasswordException.CreateHelp('New password should be different. Try again.', IDH_ChangePasswordException);

    ctx_Security.ChangePassword(editNew_Password.Text);

    // try initialize context using new password
    sUser := EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain);
    Mainboard.ContextManager.CreateThreadContext(sUser, HashPassword(editNew_Password.Text));
    CanClose := True;    
  end

  else if ModalResult = mrCancel then
  begin
    if FPasswordExpired then
      EvMessage('Your password has expired! You will not be able to use Evolution until you update your password.');
  end;
end;

procedure TChange_Password.FormShow(Sender: TObject);
var
  s: String;
begin
  s := ctx_Security.GetPasswordRules.Value['Description'];
  if s <> '' then
    s := 'Password requirements:'#13 + s;
  lPasswordRules.Caption := s;

  ClientHeight := lPasswordRules.BoundsRect.Bottom + 60;
end;

end.
