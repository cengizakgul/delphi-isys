unit mod_EvolutionGUIPlug;

interface

implementation

uses Classes, isBaseClasses, EvCommonInterfaces, EvMainboard, evContext, evTransportInterfaces;

type
  TevEvolutionGUI = class(TisInterfacedObject, IevEvolutionGUI)
  private
    procedure  CreateMainForm;
    procedure  DestroyMainForm;
    procedure  ApplySecurity(const Component: TComponent);
    function   GUIContext: IevContext;
    procedure  AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure  AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure  OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure  CheckTaskQueueStatus;
    procedure  PrintTask(const ATaskID: TisGUID);
    procedure  OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure  OnWaitingServerResponse;
    procedure  OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure  PasswordChangeClose;
  end;

function GetEvolutionGUI: IevEvolutionGUI;
begin
  Result := TevEvolutionGUI.Create;
end;

{ TevEvolutionGUI }

procedure TevEvolutionGUI.AddTaskQueue(const ATask: IevTask;
  const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TevEvolutionGUI.AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TevEvolutionGUI.ApplySecurity(const Component: TComponent);
begin
// a plug
end;

procedure TevEvolutionGUI.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TevEvolutionGUI.CreateMainForm;
begin
// a plug
end;

procedure TevEvolutionGUI.DestroyMainForm;
begin
// a plug
end;

function TevEvolutionGUI.GUIContext: IevContext;
begin
  Result := Context;
end;

procedure TevEvolutionGUI.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
// a plug
end;

procedure TevEvolutionGUI.OnGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TevEvolutionGUI.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TevEvolutionGUI.OnWaitingServerResponse;
begin
// a plug
end;

procedure TevEvolutionGUI.PasswordChangeClose;
begin
// a plug
end;

procedure TevEvolutionGUI.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;


initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEvolutionGUI, IevEvolutionGUI, 'Evolution GUI Plug');

end.