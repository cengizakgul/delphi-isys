object TQueueViewForm: TTQueueViewForm
  Left = 381
  Top = 230
  Width = 813
  Height = 528
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Your queue tasks'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000088888888888888888800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008F0000000000000FF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008F0000000000000FF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008F0000000000000FF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008F0000000000000FF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008F0000000000000FF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008F0000000000000FF8000000000000008FFFFFFFFFFFFFFFF800000000000
    0008FFFFFFFFFFFFFFFF8000000000000008FF44EFFFFFFF8888800000000000
    0008F44E4F4444448FFF8000000000000008FF44FFFFFFFF8FF8000000000000
    0008FFFFFFFFFFFF8F8000000000000000088888888888888800000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFF00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00
    003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00
    003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00003FFE00
    003FFE00003FFE00007FFE0000FFFE0001FFFE0003FFFFFFFFFFFFFFFFFF}
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Tabs: TevTabControl
    Left = 0
    Top = 26
    Width = 797
    Height = 464
    Align = alClient
    MultiLine = True
    TabOrder = 0
    TabPosition = tpRight
    Tabs.Strings = (
      'List View'
      'Split View'
      'Detail View'
      'Full Screen')
    TabIndex = 0
    OnChange = TabsChange
    OnChanging = TabsChanging
    object evSplitter1: TevSplitter
      Left = 4
      Top = 166
      Width = 770
      Height = 3
      Cursor = crVSplit
      Align = alTop
      AutoSnap = False
      MinSize = 80
    end
    object TopListPanel: TevPanel
      Left = 4
      Top = 4
      Width = 770
      Height = 162
      Align = alTop
      TabOrder = 0
      object tListFilter: TevTabControl
        Left = 1
        Top = 1
        Width = 768
        Height = 160
        Align = alClient
        MultiLine = True
        TabOrder = 0
        TabPosition = tpBottom
        Tabs.Strings = (
          'Personal'
          'All')
        TabIndex = 0
        OnChange = tListFilterChange
        object dbgTasks: TevDBGrid
          Left = 4
          Top = 4
          Width = 505
          Height = 134
          DisableThemesInTitle = False
          ControlType.Strings = (
            'Finished;CheckBox;True;False')
          Selected.Strings = (
            'UserName'#9'15'#9'User'#9'T'
            'Finished'#9'5'#9'Finished'#9'T'
            'Priority'#9'5'#9'Priority'#9'F'
            'Nbr'#9'10'#9'Id'#9'F'
            'TimeStamp'#9'18'#9'Updated'#9'T'
            'Status'#9'17'#9'Status'#9'T'
            'TaskType'#9'20'#9'Description'#9'F'
            'Caption'#9'30'#9'Caption'#9'T')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TTQueueViewForm\dbgTasks'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DataSource
          KeyOptions = [dgEnterToTab]
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          OnCalcCellColors = dbgTasksCalcCellColors
          OnDblClick = dbgTasksDblClick
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object StGrid: TStringGrid
          Left = 509
          Top = 4
          Width = 255
          Height = 134
          Cursor = crHandPoint
          TabStop = False
          Align = alRight
          BiDiMode = bdRightToLeft
          Color = clBtnFace
          ColCount = 4
          Ctl3D = False
          DefaultColWidth = 100
          DefaultRowHeight = 15
          DefaultDrawing = False
          FixedCols = 0
          RowCount = 6
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -8
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goVertLine, goHorzLine]
          ParentBiDiMode = False
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssNone
          TabOrder = 1
          OnDblClick = StGridDblClick
          OnDrawCell = StGridDrawCell
          ColWidths = (
            83
            35
            91
            36)
        end
      end
    end
    object DetailPanel: TevPanel
      Left = 4
      Top = 255
      Width = 770
      Height = 205
      Align = alClient
      Caption = 'Double-click to open task details'
      TabOrder = 1
      OnDblClick = DetailPanelDblClick
      inline TaskViewer: TTaskViewer
        Left = 1
        Top = 1
        Width = 768
        Height = 203
        Align = alClient
        TabOrder = 0
      end
    end
    object BriefInfoPanel: TevPanel
      Left = 4
      Top = 169
      Width = 770
      Height = 86
      Align = alTop
      TabOrder = 2
      object evDBText1: TevDBText
        Left = 76
        Top = 4
        Width = 403
        Height = 17
        DataField = 'TaskType'
        DataSource = DataSource
        Transparent = True
      end
      object evDBText2: TevDBText
        Left = 76
        Top = 19
        Width = 404
        Height = 17
        DataField = 'Caption'
        DataSource = DataSource
        Transparent = True
      end
      object evDBText3: TevDBText
        Left = 76
        Top = 34
        Width = 269
        Height = 17
        DataField = 'Status'
        DataSource = DataSource
        Transparent = True
      end
      object evLabel1: TevLabel
        Left = 8
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Description:'
        Transparent = True
      end
      object evLabel2: TevLabel
        Left = 8
        Top = 19
        Width = 39
        Height = 13
        Caption = 'Caption:'
        Transparent = True
      end
      object evLabel3: TevLabel
        Left = 8
        Top = 34
        Width = 33
        Height = 13
        Caption = 'Status:'
        Transparent = True
      end
      object evLabel4: TevLabel
        Left = 359
        Top = 34
        Width = 46
        Height = 13
        Caption = 'Changed:'
        Transparent = True
      end
      object evDBText4: TevDBText
        Left = 415
        Top = 34
        Width = 241
        Height = 17
        DataField = 'TimeStamp'
        DataSource = DataSource
        Transparent = True
      end
      object evLabel5: TevLabel
        Left = 8
        Top = 49
        Width = 46
        Height = 13
        Caption = 'Message:'
        Transparent = True
      end
      object evDBText5: TevDBText
        Left = 76
        Top = 49
        Width = 597
        Height = 17
        DataField = 'Message'
        DataSource = DataSource
        Transparent = True
      end
      object sbSetEmail: TevSpeedButton
        Tag = 4
        Left = 376
        Top = 59
        Width = 25
        Height = 25
        Hint = 'Change Email Address'
        Caption = 'NI'
        HideHint = True
        AutoSize = False
        OnClick = sbSetEmailClick
        NumGlyphs = 2
        Flat = True
        ParentColor = False
        ShortCut = 115
      end
      object cbUseEmail: TevDBCheckBox
        Left = 8
        Top = 64
        Width = 361
        Height = 17
        Caption = 'Send A Note When Finished (EMail):'
        DataField = 'UseEmail'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'True'
        ValueUnchecked = 'False'
        OnExit = cbUseEmailExit
      end
      object chbShowTaskDetails: TevCheckBox
        Left = 484
        Top = 64
        Width = 137
        Height = 17
        Caption = 'Auto show task details'
        TabOrder = 1
        OnClick = chbShowTaskDetailsClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 797
    Height = 26
    Align = alTop
    TabOrder = 1
    object btnNavDelete: TevSpeedButton
      Tag = 6
      Left = 221
      Top = 0
      Width = 40
      Height = 25
      Hint = 'Delete current record (Alt+Del)'
      Enabled = False
      HideHint = True
      ParentShowHint = False
      ShowHint = True
      AutoSize = False
      OnClick = btnNavDeleteClick
      NumGlyphs = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDEE66688
        DDDDDDDDF8888888FDDDDDDEEEE66A668DDDDDDF8DD88F888DDDDDDEEEE6AAA6
        68DDDDF8DDD8FDF88DFDDDDEEEE6666A688DDDF8DDD8888F8D8FDDEEEEE6A666
        6888DD8DDDD8F8888D88DDEEEEE66AAA6DDDDD8DD8888FFF8DDDDDEEEFFF66A6
        6DDDDD888DDD88D88DDDDDE888FF76666DDDDD8DDD8DD8888DDDDDD888877776
        6DDDDDD8DDD8DDD88DDD1111888777771111888D88D8DD88D888119999877799
        999188DDDD8888DDDDD8119999999999999188DDDDDDDDDDDDD8119999999999
        999188DDDDDDDDDDDDD8119999999999999188DDDDDDDDDDDDD8111111111111
        11118888888888888888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      AllowAllUp = True
      Flat = True
      ParentColor = False
      ShortCut = 32814
    end
    object btnNavLast: TevSpeedButton
      Tag = 4
      Left = 79
      Top = 0
      Width = 25
      Height = 25
      Hint = 'Last Record (Alt+End)'
      Enabled = False
      HideHint = True
      ParentShowHint = False
      ShowHint = True
      AutoSize = False
      OnClick = btnNavLastClick
      NumGlyphs = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDD0000000000000000888888888888888800EEEEEEEEEE
        EEE088DDDDDDDDDDDDD800EEEEEE76EEEEE088DDDDDDDFDDDDD800EEEEE7E66E
        EEE088DDDDDD8FFDDDD800EEEE7EEE66EEE088DDDDD888FFDDD8000007EEEEE6
        600088888D88888FFD88DDDD7EEEEEEE66DDDDDDD8888888FFDDDDD7EFFEEEEE
        E66DDDDD888888888FFDDD7EEEEFEEEEEE7DDDD88888888888DDDDD777EEEEE6
        67DDDDDDDD88888FFDDDDDDD7EEEEEEE66DDDDDDD8888888FFDDDDD7EFFFEEEE
        E66DDDDD888888888FFDDD7EEEEFFFEEEE7DDDD88888888888DDDDD777EEEEE7
        77DDDDDDDD88888DDDDDDDDDDD77777DDDDDDDDDDDDDDDDDDDDD}
      Flat = True
      ParentColor = False
      ShortCut = 32803
    end
    object btnNavNext: TevSpeedButton
      Tag = 3
      Left = 54
      Top = 0
      Width = 25
      Height = 25
      Hint = 'Next record (Alt+Right)'
      Enabled = False
      HideHint = True
      ParentShowHint = False
      ShowHint = True
      AutoSize = False
      OnClick = btnNavNextClick
      NumGlyphs = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD76DD
        DDDDDDDDDDDDDFDDDDDDDDDDDDD7E66DDDDDDDDDDDDD8FFDDDDDDDDDDD7EEE66
        DDDDDDDDDDD888FFDDDDDDDDD7EEEEE66DDDDDDDDD88888FFDDDDDDD7EEEEEEE
        66DDDDDDD8888888FFDDDDD7EFFEEEEEE66DDDDD888888888FFDDD7EEEEFEEEE
        EE7DDDD88888888888DDDDD777EFEEE677DDDDDDDD88888FDDDDDDDDD7EFEEE6
        DDDDDDDDDD88888FDDDD000007EFEEE6000088888D88888FD88800FFF7EFFFE6
        FFF088DDDD88888FDDD800FFF7EEEEE7FFF088DDDD88888DDDD800FFFF77777F
        FFF088DDDDDDDDDDDDD800FFFFFFFFFFFFF088DDDDDDDDDDDDD8000000000000
        00008888888888888888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      Flat = True
      ParentColor = False
      ShortCut = 32807
    end
    object btnNavPrevious: TevSpeedButton
      Tag = 2
      Left = 29
      Top = 0
      Width = 25
      Height = 25
      Hint = 'Prior record (Alt+Left)'
      Enabled = False
      HideHint = True
      ParentShowHint = False
      ShowHint = True
      AutoSize = False
      OnClick = btnNavPreviousClick
      NumGlyphs = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDD0000000000000000888888888888888800FFFFFFFFFF
        FFF088DDDDDDDDDDDDD800FFFF766666FFF088DDDDDFFFFFDDD800FFF7EEEEE6
        FFF088DDDD88888FDDD800FFF7EFEEE6FFF088DDDD88888FDDD8000007EFEEE6
        000088888D88888FD888DDDDD7EFEEE6DDDDDDDDDD88888FDDDDDDD766EFEEE6
        666DDDDDFF88888FFFFDDD7EEEEFEEEEEE7DDDD88888888888DDDDD7EFFEEEEE
        E7DDDDDD888888888DDDDDDD7EFFEEEE7DDDDDDDD8888888DDDDDDDDD7EFFEE7
        DDDDDDDDDD88888DDDDDDDDDDD7EFE7DDDDDDDDDDDD888DDDDDDDDDDDDD7E7DD
        DDDDDDDDDDDD8DDDDDDDDDDDDDDD7DDDDDDDDDDDDDDDDDDDDDDD}
      Flat = True
      ParentColor = False
      ShortCut = 32805
    end
    object btnNavFirst: TevSpeedButton
      Tag = 1
      Left = 4
      Top = 0
      Width = 25
      Height = 25
      Hint = 'First record (Alt+Home)'
      Enabled = False
      HideHint = True
      ParentShowHint = False
      ShowHint = True
      AutoSize = False
      OnClick = btnNavFirstClick
      NumGlyphs = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD766666
        DDDDDDDDDDDFFFFFDDDDDDD766EEEEE6666DDDDDFF88888FFFFDDD7EEEEEEEEE
        EE7DDDD88888888888DDDDD7EFFFEEEEE7DDDDDD888888888DDDDDDD7EFFEEEE
        7DDDDDDDD8888888DDDDDDD766EFFEE6666DDDDDFF88888FFFFDDD7EEEEEFEEE
        EE7DDDD88888888888DDDDD7EFFEEEEEE7DDDDDD888888888DDDDDDD7EFFEEEE
        7DDDDDDDD8888888DDDD000007EFFEE7000088888D88888D888800EEEE7EFE7E
        EEE088DDDDD888DDDDD800EEEEE7E7EEEEE088DDDDDD8DDDDDD800EEEEEE7EEE
        EEE088DDDDDDDDDDDDD800EEEEEEEEEEEEE088DDDDDDDDDDDDD8000000000000
        00008888888888888888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      Flat = True
      ParentColor = False
      ShortCut = 32804
    end
    object evSpeedButton1: TevSpeedButton
      Tag = 4
      Left = 120
      Top = 0
      Width = 25
      Height = 25
      Hint = 'Refresh (F5)'
      HideHint = True
      ParentShowHint = False
      ShowHint = True
      AutoSize = False
      OnClick = evSpeedButton1Click
      NumGlyphs = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD4444444DDDDDDDDD8FFFFFFDDDDDDDD484EEEEE4D
        DDDDDD8D888888FDDDDDD47774EEEEE4DDDDD8DDD888888FDDDDD47774444444
        DDDDD8DDD888888FDDDDD4777778DDDDDDDDD8DDDDDDDDDDDDDDD4777778D4DD
        DDDDD8DDDDD8DFDDDDDDD47777784E4DDDDDD8DDDDDDF8FDDDDDD4777774EEE4
        DDDDD8DDDDDF888FDDDDD477774EEEEE4DDDD8DDDDF88888FDDDD47774EEEEEE
        E4DDD8DDDF8888888FDDD477444EEEEE444DD8DDF888888888FDD477774EEEEE
        4DDDD8DDDD888888DDDDDD4884EEEEE4DDDDDD8DD8888888DDDDDDD44444444D
        DDDDDDD88888888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      Flat = True
      ParentColor = False
      ShortCut = 116
    end
  end
  object dsTaskList: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Id'
        DataType = ftInteger
      end
      item
        Name = 'TaskType'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 128
      end
      item
        Name = 'Caption'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Status'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Finished'
        DataType = ftBoolean
      end
      item
        Name = 'Seen'
        DataType = ftBoolean
      end
      item
        Name = 'TimeStamp'
        DataType = ftDateTime
      end
      item
        Name = 'Priority'
        DataType = ftInteger
      end
      item
        Name = 'Requests'
        DataType = ftMemo
      end
      item
        Name = 'UseEmail'
        DataType = ftBoolean
      end
      item
        Name = 'EMail'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Message'
        DataType = ftString
        Size = 256
      end
      item
        Name = 'UserName'
        DataType = ftString
        Size = 128
      end>
    IndexDefs = <
      item
        Name = 'SORT'
        Fields = 'Nbr'
        Options = [ixDescending]
      end>
    AfterDelete = dsTaskListAfterDelete
    Left = 532
    Top = 14
    object dsTaskListID: TStringField
      DisplayWidth = 32
      FieldName = 'ID'
      Size = 32
    end
    object dsTaskListNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Nbr'
    end
    object dsTaskListClassName: TStringField
      DisplayWidth = 255
      FieldName = 'TaskType'
      Size = 255
    end
    object dsTaskListCaption: TStringField
      DisplayWidth = 255
      FieldName = 'Caption'
      Size = 255
    end
    object dsTaskListStatus: TStringField
      DisplayWidth = 255
      FieldName = 'Status'
      Size = 255
    end
    object dsTaskListFinished: TBooleanField
      DisplayWidth = 5
      FieldName = 'Finished'
    end
    object dsTaskListSeen: TBooleanField
      DisplayWidth = 5
      FieldName = 'Seen'
    end
    object dsTaskListTimeStamp: TDateTimeField
      DisplayWidth = 18
      FieldName = 'TimeStamp'
    end
    object dsTaskListPriority: TIntegerField
      DisplayWidth = 10
      FieldName = 'Priority'
    end
    object dsTaskListRequests: TMemoField
      DisplayWidth = 10
      FieldName = 'Requests'
      BlobType = ftMemo
    end
    object dsTaskListUseEmail: TBooleanField
      DisplayWidth = 5
      FieldName = 'UseEmail'
    end
    object dsTaskListEMail: TStringField
      DisplayWidth = 100
      FieldName = 'EMail'
      Size = 100
    end
    object dsTaskListMessage: TStringField
      DisplayWidth = 256
      FieldName = 'Message'
      Size = 256
    end
    object dsTaskListUserName: TStringField
      DisplayWidth = 128
      FieldName = 'UserName'
      Size = 128
    end
    object dsTaskListNewTask: TBooleanField
      DisplayWidth = 5
      FieldName = 'NewTask'
    end
    object dsTaskListWaitForRes: TBooleanField
      DisplayWidth = 5
      FieldName = 'WaitForResTask'
    end
    object dsTaskListExecTask: TBooleanField
      DisplayWidth = 5
      FieldName = 'ExecTask'
    end
    object dsTaskListFinishedSucs: TBooleanField
      DisplayWidth = 5
      FieldName = 'FinishedSucs'
    end
    object dsTaskListFinishedExcep: TBooleanField
      DisplayWidth = 5
      FieldName = 'FinishedExcep'
    end
    object dsTaskListFinishedWarn: TBooleanField
      DisplayWidth = 5
      FieldName = 'FinishedWarn'
    end
    object dsTaskListFailedExcep: TBooleanField
      DisplayWidth = 5
      FieldName = 'FailedExcep'
    end
    object dsTaskListUserNameHide: TStringField
      DisplayWidth = 128
      FieldName = 'UserNameHide'
      Size = 128
    end
  end
  object DataSource: TevDataSource
    DataSet = dsTaskList
    OnDataChange = DataSourceDataChange
    Left = 524
    Top = 38
  end
  object Timer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = TimerTimer
    Left = 581
    Top = 13
  end
end
