// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SWaitingObjectForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, isBaseClasses,
  isBasicUtils, ComCtrls, StdCtrls, isThreadManager;

type
  IevProgressInfo = interface
  ['{1142C2AA-3C3C-41DF-AD7D-8BCF48892A49}']
    function  GetProgressCurr: Integer;
    function  GetProgressMax: Integer;
    function  GetProgressText: String;
    procedure SetProgressCurr(const AValue: Integer);
    procedure SetProgressMax(const AValue: Integer);
    procedure SetProgressText(const AValue: String);
    property  ProgressMax: Integer read GetProgressMax write SetProgressMax;
    property  ProgressCurr: Integer read GetProgressCurr write SetProgressCurr;
    property  ProgressText: String read GetProgressText write SetProgressText;
  end;

  TDummyWait = class
  private
    FStates: IisList;
    FBeginTime: TDateTime;
    FHidden:  Boolean;
    function  CurrentState: IevProgressInfo;
    function  ProgressIsOn: Boolean;
    function  GetTextToDisplay: String;
    procedure DoOnStatusChange; virtual;
  public
    procedure StartWait(const Text: string = ''; const Full: Integer = -1); virtual;
    procedure UpdateWait(const Text: string = ''; const Pos: Integer = -1; const Full: Integer = -1); virtual;
    procedure EndWait; virtual;
    procedure Hide; virtual;
    procedure KillWait;
    procedure ShowIfHidden;
    constructor Create; virtual;
  end;


  TCursorWait = class(TDummyWait)
  private
    FNoWaitCursor: TCursor;
    FCurrentCursor: TCursor;
    procedure DoOnStatusChange; override;
  public
    procedure StartWait(const Text: string = ''; const Full: Integer = -1); override;
    procedure EndWait; override;
    procedure Hide; override;
  end;

  TWaitingObjectForm = class;

  TAdvancedWait = class(TCursorWait)
  private
    FForm: TWaitingObjectForm;
    FStartedTimeStamp: TDateTime;
    procedure HideForm;
    procedure DoOnStatusChange; override;
  public
    destructor Destroy; override;
    procedure  EndWait; override;
    procedure  Hide; override;
    procedure  UpdateTimer;
  end;


  TWaitingObjectForm = class(TForm)
    AnimatedPicture: TAnimate;
    TimerLabel: TLabel;
    PB: TProgressBar;
    lText: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    FBeginTime: TDateTime;
    FDefaultFormHeight: Integer;
  public
    procedure UpdateTimer;
  end;


implementation

{$R *.DFM}

type
  TevProgressInfo = class(TisInterfacedObject, IevProgressInfo)
  private
    FProgressMax: Integer;
    FProgressCurr: Integer;
    FProgressText: String;
    function  GetProgressCurr: Integer;
    function  GetProgressMax: Integer;
    function  GetProgressText: String;
    procedure SetProgressCurr(const AValue: Integer);
    procedure SetProgressMax(const AValue: Integer);
    procedure SetProgressText(const AValue: String);
  end;


{ TevProgressInfo }

function TevProgressInfo.GetProgressCurr: Integer;
begin
  Result := FProgressCurr;
end;

function TevProgressInfo.GetProgressMax: Integer;
begin
  Result := FProgressMax;
end;

function TevProgressInfo.GetProgressText: String;
begin
  Result := FProgressText;
end;

procedure TevProgressInfo.SetProgressCurr(const AValue: Integer);
begin
  FProgressCurr := AValue;
end;

procedure TevProgressInfo.SetProgressMax(const AValue: Integer);
begin
  FProgressMax := AValue;
end;

procedure TevProgressInfo.SetProgressText(const AValue: String);
begin
  FProgressText := AValue;
end;


{ TAdvancedWait }

procedure TWaitingObjectForm.UpdateTimer;
var
  t: TDateTime;
begin
  t := Now - FBeginTime;
  if t > 1/24/60/60*2 then
    TimerLabel.Caption := 'Elapsed time: '+ FormatDateTime('h:nn:ss', t);
  Update;
end;

destructor TAdvancedWait.Destroy;
begin
  FForm.Free;
  inherited;
end;

procedure TAdvancedWait.DoOnStatusChange;
var
  sText: String;
begin
  if FHidden or not IsMultiThread then
    Exit;

  sText := GetTextToDisplay;

  if not Assigned(FForm) then
  begin
    if sText <> '' then
    begin
      FForm := TWaitingObjectForm.Create(nil);
      FForm.TimerLabel.Caption := '';
      FForm.PB.Min := 0;
      FForm.FBeginTime := FBeginTime;
      FForm.Show;
      FStartedTimeStamp := Now;
    end;
  end
  else
    FForm.FBeginTime := FBeginTime;

  if Assigned(FForm) then
  begin
    FForm.lText.Autosize := True;
    FForm.lText.Caption := sText;
    FForm.lText.Autosize := False;
    FForm.lText.Width := FForm.ClientWidth - FForm.lText.Left * 2;

    if FForm.TimerLabel.Top - FForm.lText.BoundsRect.Bottom < 8 then
      FForm.ClientHeight := FForm.ClientHeight + (8 -(FForm.TimerLabel.Top - FForm.lText.BoundsRect.Bottom));

    if CurrentState.ProgressMax > 0 then
    begin
      FForm.PB.Max := CurrentState.ProgressMax;
      FForm.PB.Position := CurrentState.ProgressCurr;
      FForm.PB.Show;
    end
    else if not ProgressIsOn then
      FForm.PB.Hide;

    FForm.UpdateTimer;

    if (FForm.WindowState <> wsMinimized) and Application.Active then
      BringWindowToTop(FForm.Handle);
  end;

  inherited;
end;

procedure TAdvancedWait.EndWait;
begin
  inherited;
  if FStates.Count = 0 then
  begin
    if Assigned(FForm) then
      FForm.FBeginTime := 0;
    HideForm;
  end
end;

procedure TAdvancedWait.Hide;
begin
  inherited;
  HideForm;
end;

procedure TAdvancedWait.HideForm;
begin
  FreeAndNil(FForm);
end;

procedure TWaitingObjectForm.FormCreate(Sender: TObject);
begin
  if not Assigned(Application.MainForm) or
     not Application.MainForm.Visible or
     (fsShowing in Application.MainForm.FormState) then
    Position := poScreenCenter;
  FDefaultFormHeight := ClientHeight;
end;


procedure TAdvancedWait.UpdateTimer;
begin
  if Assigned(FForm) then
    FForm.UpdateTimer;
end;

{ TCursorWait }

procedure TCursorWait.EndWait;
begin
  if FStates.Count = 1 then
  begin
    FCurrentCursor := FNoWaitCursor;
    FNoWaitCursor := crDefault;
  end;
  
  inherited;
end;

procedure TCursorWait.Hide;
begin
  inherited;
  if FStates.Count > 0 then
    Screen.Cursor := crDefault;
end;

procedure TCursorWait.StartWait(const Text: string = ''; const Full: Integer = -1);
begin
  if FStates.Count = 0 then
    FNoWaitCursor := Screen.Cursor;
  FCurrentCursor := crHourGlass;
  inherited;
end;

procedure TCursorWait.DoOnStatusChange;
begin
  inherited;
  if not FHidden then
    Screen.Cursor := FCurrentCursor;

  if Application.MainForm <> nil then
    Application.MainForm.Update;
end;

{ TDummyWait }

constructor TDummyWait.Create;
begin
  FStates := TisList.Create;
  FStates.Capacity := 10;
end;

function TDummyWait.CurrentState: IevProgressInfo;
begin
  if FStates.Count > 0 then
    Result := FStates[FStates.Count - 1] as IevProgressInfo
  else
    Result := TevProgressInfo.Create;
end;

procedure TDummyWait.DoOnStatusChange;
begin
end;

procedure TDummyWait.EndWait;
begin
  if FStates.Count > 0 then
  begin
    FStates.Delete(FStates.Count - 1);
    if FStates.Count = 0 then
      FBeginTime := 0;
    DoOnStatusChange;
  end;
end;

function TDummyWait.GetTextToDisplay: String;
var
  i: Integer;
begin
  Result := '';
  for i := 0  to FStates.Count - 1 do
    if (FStates[i] as IevProgressInfo).ProgressText <> '' then
      AddStrValue(Result, (FStates[i] as IevProgressInfo).ProgressText, #13);
end;

procedure TDummyWait.Hide;
begin
  if FStates.Count > 0 then
    FHidden := True;
end;

procedure TDummyWait.KillWait;
begin
  while FStates.Count > 0 do
    EndWait;
end;

function TDummyWait.ProgressIsOn: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to FStates.Count - 1 do
    if (FStates[i] as IevProgressInfo).ProgressMax > 0 then
    begin
      Result := True;
      break;
    end;
end;

procedure TDummyWait.ShowIfHidden;
begin
  if FStates.Count > 0 then
  begin
    FHidden := False;
    DoOnStatusChange;
  end;
end;

procedure TDummyWait.StartWait(const Text: string; const Full: Integer);
var
  ProgressInfo: IevProgressInfo;
begin
  ProgressInfo := TevProgressInfo.Create;
  ProgressInfo.ProgressMax := Full;
  ProgressInfo.ProgressText := Text;

  FStates.Add(ProgressInfo);

  if FStates.Count = 1 then
    FBeginTime := Now;

  DoOnStatusChange;    
end;

procedure TDummyWait.UpdateWait(const Text: string; const Pos: Integer; const Full: Integer);
var
  ProgressInfo: IevProgressInfo;
begin
  ProgressInfo := CurrentState;
  ProgressInfo.ProgressText := Text;
  ProgressInfo.ProgressCurr := Pos;
  if Full <> -1 then
    ProgressInfo.ProgressMax := Full;

  DoOnStatusChange;
end;


end.
