unit EvSecurityQuestionsFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ISBasicClasses,  ExtCtrls,
  isBaseClasses, isBasicUtils, EvExceptions, EvUtils, evContext, EvUIComponents;

type
  TEvSecurityQuestions = class(TForm)
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    Label1: TLabel;
    pnlQuestions: TevPanel;
    lQuestion1: TevLabel;
    edAnswer1: TevEdit;
    lQuestion2: TevLabel;
    edAnswer2: TevEdit;
    lQuestion3: TevLabel;
    edAnswer3: TevEdit;
    pnlPassword: TevPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    edPassword: TevEdit;
    edConfirmPassword: TevEdit;
    lPasswordRules: TevLabel;
    procedure edAnswer1Exit(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  private
    procedure Initialize(const AQuestions: IisStringList);
    function  GetAnswers: IisListOfValues;
  public
  end;

  function AskSecurityQuestions(const AQuestions: IisStringList; out AQuestionAnswerList: IisListOfValues; out APassword: String): Boolean;

implementation

{$R *.dfm}

function AskSecurityQuestions(const AQuestions: IisStringList; out AQuestionAnswerList: IisListOfValues; out APassword: String): Boolean;
var
  Frm: TEvSecurityQuestions;
begin
  Frm := TEvSecurityQuestions.Create(Application);
  try
    Frm.Initialize(AQuestions);
    Result := Frm.ShowModal = mrOk;
    if Result then
    begin
      AQuestionAnswerList := Frm.GetAnswers;
      APassword := Frm.edPassword.Text;
    end;
  finally
    Frm.Free;
  end;
end;

{ TEvSecurityQuestions }

procedure TEvSecurityQuestions.Initialize(const AQuestions: IisStringList);

  procedure SetQuestion(const ANbr: Integer);
  var
    L: TevLabel;
  begin
    L := FindComponent('lQuestion' + IntToStr(ANbr)) as TevLabel;
    L.Caption := AQuestions[ANbr - 1];
  end;

begin
  SetQuestion(1);
  SetQuestion(2);
  SetQuestion(3);
end;

function TEvSecurityQuestions.GetAnswers: IisListOfValues;

  procedure AddQuestion(const ANbr: Integer);
  var
    L: TevLabel;
    EB: TevEdit;
  begin
    L := FindComponent('lQuestion' + IntToStr(ANbr)) as TevLabel;
    EB := FindComponent('edAnswer' + IntToStr(ANbr)) as TevEdit;
    Result.AddValue(L.Caption, EB.Text);
  end;

begin
  Result := TisListOfValues.Create;
  AddQuestion(1);
  AddQuestion(2);
  AddQuestion(3);
end;

procedure TEvSecurityQuestions.edAnswer1Exit(Sender: TObject);
begin
  (Sender as TevEdit).Text := Trim((Sender as TevEdit).Text);
end;

procedure TEvSecurityQuestions.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then
  begin
    CanClose := False;
    if edPassword.Text = '' then
      raise EChangePasswordException.CreateHelp('New password can''t be empty. Try again.', IDH_ChangePasswordException);
    if edPassword.Text <> edConfirmPassword.Text then
      raise EChangePasswordException.CreateHelp('New password has been mistyped. Try again.', IDH_ChangePasswordException);
    CanClose := True;
  end;
end;

procedure TEvSecurityQuestions.FormShow(Sender: TObject);
var
  s: String;
begin
  s := ctx_Security.GetPasswordRules.Value['Description'];
  if s <> '' then
    s := 'Password requirements:'#13 + s;
  lPasswordRules.Caption := s;

  pnlPassword.Height := lPasswordRules.BoundsRect.Bottom + 15;
  ClientHeight := pnlPassword.BoundsRect.Bottom + 60;
end;

end.
