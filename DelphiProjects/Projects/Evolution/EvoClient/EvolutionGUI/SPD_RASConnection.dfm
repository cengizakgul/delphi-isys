object RASConnectionForm: TRASConnectionForm
  Left = 492
  Top = 247
  AutoScroll = False
  BorderIcons = []
  Caption = 'Remote Access Settings'
  ClientHeight = 249
  ClientWidth = 337
  Color = clBtnFace
  Constraints.MaxHeight = 307
  Constraints.MinHeight = 295
  Constraints.MinWidth = 287
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Menu = MainMenu
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainStb: TStatusBar
    Left = 0
    Top = 230
    Width = 337
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end>
  end
  object Panel1: TevPanel
    Left = 0
    Top = 0
    Width = 337
    Height = 7
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
  end
  object Panel2: TevPanel
    Left = 0
    Top = 194
    Width = 337
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel7: TevPanel
      Left = 8
      Top = 0
      Width = 329
      Height = 36
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtnOK: TevBitBtn
        Left = 80
        Top = 7
        Width = 81
        Height = 25
        Caption = 'OK'
        Default = True
        ModalResult = 1
        TabOrder = 0
        OnClick = BtnOKClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
          DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
          DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
          DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
          DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
          DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
          2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
          A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
          AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
        NumGlyphs = 2
        Margin = 0
      end
      object DialBtn: TevBitBtn
        Left = 168
        Top = 7
        Width = 81
        Height = 25
        Caption = 'Connect'
        TabOrder = 1
        OnClick = DialBtnClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD8000000
          0008DDDDDDFFFFFFFFFDDDD8848888888880DDDDDD888888888FDD8CC4891919
          1980DDD88D8D8D8D8D8FD8CC648888888880DD88DD888888888FDC66C4777777
          7770D8DD8D8DDDDDDD8F8CCC647777777770D888DD8DDDDDDD8FC6C66C677777
          77708D8DD8D8DDDDDD8FCC6E6C677777777088DDD8D8DDDDDD8FC6C6ECC47777
          77788D8DD88D8888888DCECCCCEE664464488D8888DDDDDDDDDDCECEEECEECE6
          CEC88D8DDD8DD8DD8D8D8CECFECFECEECC8DD8D8DD8DD8DD88DDDCEECCCCCCFC
          6C8DD8DD888888D8D8DDD8CFCFEFEFCEC8DDDD8D8DDDDD8D8DDDDD8CCEFFFECC
          8DDDDDD88DDDDD88DDDDDDDD8CCCCC8DDDDDDDDDD88888DDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
    end
  end
  object Panel3: TevPanel
    Left = 330
    Top = 7
    Width = 7
    Height = 187
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
  end
  object Panel4: TevPanel
    Left = 0
    Top = 7
    Width = 7
    Height = 187
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 4
  end
  object Panel5: TevPanel
    Left = 7
    Top = 7
    Width = 323
    Height = 187
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 5
    object Panel6: TevPanel
      Left = 0
      Top = 0
      Width = 323
      Height = 0
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
    end
    object PageControl1: TevPageControl
      Left = 0
      Top = 0
      Width = 323
      Height = 187
      ActivePage = tsConnSettings
      Align = alClient
      TabOrder = 1
      object tsConnSettings: TTabSheet
        Caption = 'Evolution Server'
        ImageIndex = 2
        object Label2: TLabel
          Left = 14
          Top = 19
          Width = 100
          Height = 13
          Caption = 'Evolution Server Port'
        end
        object chbEncryption: TCheckBox
          Left = 16
          Top = 51
          Width = 273
          Height = 17
          Caption = 'Secure Connection'
          TabOrder = 0
        end
        object edPort: TComboBox
          Left = 122
          Top = 16
          Width = 77
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          OnChange = edPortChange
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Main Connection'
        object GroupBox2: TevGroupBox
          Left = 7
          Top = 5
          Width = 301
          Height = 146
          Align = alClient
          Caption = 'Access'
          TabOrder = 0
          object MainConnectionLbl: TevLabel
            Left = 16
            Top = 16
            Width = 116
            Height = 13
            Caption = 'Main dial-up connection:'
          end
          object MainConnectionUserLbl: TevLabel
            Left = 16
            Top = 56
            Width = 100
            Height = 13
            Caption = 'User name (optional):'
          end
          object MainConnectionPasswordLbl: TevLabel
            Left = 16
            Top = 96
            Width = 95
            Height = 13
            Caption = 'Password (optional):'
          end
          object MainConnectionCmb: TevComboBox
            Left = 16
            Top = 32
            Width = 258
            Height = 21
            Style = csDropDownList
            ItemHeight = 0
            TabOrder = 0
            OnChange = ComboBoxChange
          end
          object MainConnectionUserEdit: TevEdit
            Left = 16
            Top = 72
            Width = 121
            Height = 21
            TabOrder = 1
          end
          object MainConnectionPasswordEdit: TevEdit
            Left = 16
            Top = 112
            Width = 121
            Height = 21
            PasswordChar = '*'
            TabOrder = 2
          end
        end
        object Panel8: TevPanel
          Left = 0
          Top = 151
          Width = 315
          Height = 8
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
        end
        object Panel9: TevPanel
          Left = 0
          Top = 5
          Width = 7
          Height = 146
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
        end
        object Panel10: TevPanel
          Left = 308
          Top = 5
          Width = 7
          Height = 146
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
        end
        object Panel11: TevPanel
          Left = 0
          Top = 0
          Width = 315
          Height = 5
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Alternative Connection'
        ImageIndex = 2
        object Panel16: TevPanel
          Left = 0
          Top = 152
          Width = 315
          Height = 7
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
        end
        object Panel17: TevPanel
          Left = 0
          Top = 5
          Width = 7
          Height = 147
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
        end
        object Panel18: TevPanel
          Left = 308
          Top = 5
          Width = 7
          Height = 147
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
        end
        object Panel19: TevPanel
          Left = 0
          Top = 0
          Width = 315
          Height = 5
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
        end
        object GroupBox3: TevGroupBox
          Left = 7
          Top = 5
          Width = 301
          Height = 147
          Align = alClient
          Caption = 'Access'
          TabOrder = 4
          object AltConnectionLbl: TevLabel
            Left = 16
            Top = 16
            Width = 143
            Height = 13
            Caption = 'Alternative dial-up connection:'
            Enabled = False
          end
          object AltConnectionUserLbl: TevLabel
            Left = 16
            Top = 56
            Width = 100
            Height = 13
            Caption = 'User name (optional):'
            Enabled = False
          end
          object AltConnectionPasswordLbl: TevLabel
            Left = 16
            Top = 96
            Width = 95
            Height = 13
            Caption = 'Password (optional):'
            Enabled = False
          end
          object Label1: TevLabel
            Left = 145
            Top = 88
            Width = 87
            Height = 13
            Caption = 'dial-up connection'
          end
          object AltConnectionCmb: TevComboBox
            Left = 16
            Top = 34
            Width = 258
            Height = 21
            Style = csDropDownList
            Enabled = False
            ItemHeight = 0
            TabOrder = 0
            OnChange = ComboBoxChange
          end
          object MainAltConCmb: TevCheckBox
            Left = 144
            Top = 72
            Width = 89
            Height = 17
            Caption = 'Use alternative'
            TabOrder = 1
            OnClick = MainAltConCmbClick
          end
          object AltConnectionUserEdit: TevEdit
            Left = 16
            Top = 72
            Width = 121
            Height = 21
            Enabled = False
            TabOrder = 2
          end
          object AltConnectionPasswordEdit: TevEdit
            Left = 16
            Top = 112
            Width = 121
            Height = 21
            Enabled = False
            PasswordChar = '*'
            TabOrder = 3
          end
        end
      end
    end
  end
  object MainMenu: TMainMenu
    Left = 259
    Top = 31
    object File1: TMenuItem
      Caption = '&File'
      object Exit1: TMenuItem
        Caption = '&Exit'
        ShortCut = 27
        OnClick = Exit1Click
      end
    end
    object Connection1: TMenuItem
      Caption = '&Connection'
      object Connect1: TMenuItem
        Caption = '&Connect'
        OnClick = DialBtnClick
      end
    end
  end
end
