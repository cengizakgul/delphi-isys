unit EvSecurityQuestionsSetupFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ISBasicClasses,  ExtCtrls,
  isBaseClasses, EvUIComponents;

type
  TEvSecurityQuestionsSetup = class(TForm)
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    Label1: TLabel;
    pnlQuestions: TevPanel;
    evLabel1: TevLabel;
    cbQuestion1: TevComboBox;
    evLabel2: TevLabel;
    edAnswer1: TevEdit;
    Lable3: TevLabel;
    cbQuestion2: TevComboBox;
    evLabel4: TevLabel;
    edAnswer2: TevEdit;
    evLabel5: TevLabel;
    cbQuestion3: TevComboBox;
    evLabel6: TevLabel;
    edAnswer3: TevEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure edAnswer1Exit(Sender: TObject);
  private
    procedure Initialize(const AAvailableQuestions: IisStringList);
    function  GetResult: IisListOfValues;
    procedure Validate;
  public
  end;

  function SecurityQuestionSetup(const AAvailableQuestions: IisStringList; out AQuestionAnswerList: IisListOfValues): Boolean;

implementation

{$R *.dfm}

function SecurityQuestionSetup(const AAvailableQuestions: IisStringList; out AQuestionAnswerList: IisListOfValues): Boolean;
var
  Frm: TEvSecurityQuestionsSetup;
begin
  Frm := TEvSecurityQuestionsSetup.Create(Application);
  try
    Frm.Initialize(AAvailableQuestions);
    Result := Frm.ShowModal = mrOk;
    if Result then
      AQuestionAnswerList := Frm.GetResult;
  finally
    Frm.Free;
  end;
end;

{ TEvSecurityQuestionsSetup }

procedure TEvSecurityQuestionsSetup.Initialize(const AAvailableQuestions: IisStringList);

  procedure SetQuestion(const ANbr: Integer);
  var
    CB: TevComboBox;
    EB: TevEdit;
  begin
    CB := FindComponent('cbQuestion' + IntToStr(ANbr)) as TevComboBox;
    EB := FindComponent('edAnswer' + IntToStr(ANbr)) as TevEdit;

    CB.Items.Text := AAvailableQuestions.Text;
    CB.ItemIndex := -1;
    EB.Text := '';
  end;

begin
  SetQuestion(1);
  SetQuestion(2);
  SetQuestion(3);
end;

function TEvSecurityQuestionsSetup.GetResult: IisListOfValues;

  procedure AddQuestion(const ANbr: Integer);
  var
    CB: TevComboBox;
    EB: TevEdit;
  begin
    CB := FindComponent('cbQuestion' + IntToStr(ANbr)) as TevComboBox;
    EB := FindComponent('edAnswer' + IntToStr(ANbr)) as TevEdit;
    Result.AddValue(CB.Text, EB.Text);
  end;

begin
  Result := TisListOfValues.Create;
  AddQuestion(1);
  AddQuestion(2);
  AddQuestion(3);
end;

procedure TEvSecurityQuestionsSetup.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOk then
    Validate;
end;

procedure TEvSecurityQuestionsSetup.Validate;
begin
  if (cbQuestion1.ItemIndex = -1) or (cbQuestion2.ItemIndex = -1) or (cbQuestion3.ItemIndex = -1) or
     (edAnswer1.Text = '') or (edAnswer2.Text = '') or (edAnswer3.Text = '') then
    raise Exception.Create('All questions and answers must be provided');

  if (cbQuestion1.ItemIndex = cbQuestion2.ItemIndex) or
     (cbQuestion1.ItemIndex = cbQuestion3.ItemIndex) or
     (cbQuestion2.ItemIndex = cbQuestion3.ItemIndex) then
    raise Exception.Create('Questions must be unique');
end;

procedure TEvSecurityQuestionsSetup.edAnswer1Exit(Sender: TObject);
begin
  (Sender as TevEdit).Text := Trim((Sender as TevEdit).Text);
end;

end.
