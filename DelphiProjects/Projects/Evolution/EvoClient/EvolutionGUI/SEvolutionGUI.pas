
// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SEvolutionGUI;

interface

uses Classes, Windows, Messages, SysUtils, Forms, isBaseClasses, EvCommonInterfaces, EvMainboard,
     EvTypes, Controls, sSecurityClassDefs, SSecurityInterface, EvStreamUtils,
     SWaitingObjectForm, SMainPackageForm, IsUtils, EvUtils, EvContext, isZippingRoutines,
     EvConsts, EvBasicUtils, EvTransportInterfaces, isThreadManager,isBasicUtils, EvUIUtils,
     EvUIComponents, EvClientDataset, DB;

implementation

uses TypInfo;

type
  TevEvolutionGUI = class(TisInterfacedObject, IevEvolutionGUI, IevContextCallbacks)
  private
    FWaitDialog: TAdvancedWait;
    FMainForm: TMainPackageForm;
    FGUIContext: IevContext;
    FPendingTaskList: IisStringList;
    procedure OnException(Sender: TObject; E: Exception);
    procedure OnShowModalDialog(Sender: TObject);
    procedure OnHideModalDialog(Sender: TObject);
    procedure CheckSessionStatus;
  protected
    procedure DoOnConstruction; override;
    procedure StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure EndWait;
    procedure AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
    procedure CreateMainForm;
    procedure DestroyMainForm;
    procedure ApplySecurity(const Component: TComponent);
    function  GUIContext: IevContext;
    procedure AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure CheckTaskQueueStatus;
    procedure PrintTask(const ATaskID: TisGUID);
    procedure OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure OnWaitingServerResponse;
    procedure OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure PasswordChangeClose;
    procedure UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
  public
    destructor Destroy; override;
  end;


var
  vEvolutionGUI: IevEvolutionGUI;

function GetEvolutionGUI: IevEvolutionGUI;
begin
  if not Assigned(vEvolutionGUI) then
    vEvolutionGUI := TevEvolutionGUI.Create as IevEvolutionGUI;
  Result := vEvolutionGUI;
end;


function GetContextCallbacks: IevContextCallbacks;
begin
  Result := GetEvolutionGUI as IevContextCallbacks;
end;



{ TEvolutionGUI }

procedure TevEvolutionGUI.ApplySecurity(const Component: TComponent);
var
  SecurityElement: ISecurityElement;
  i: Integer;
  State: TevSecElementState;
  DBFieldControl: IevDBFieldControl;
  s: String;
begin
  Component.GetInterface(ISecurityElement, SecurityElement);
  if Assigned(SecurityElement) and (SecurityElement.SGetTag <> '') then
  begin
    State := ctx_AccountRights.GetElementState(SecurityElement.SGetType[1], SecurityElement.SGetTag);

    if State = ' ' then
      SecurityElement.SSetCurrentSecurityState(stDisabled)
    else
      SecurityElement.SSetCurrentSecurityState(State);

    SecurityElement := nil;
  end;

  if IsPublishedProp(Component, 'SecurityRO') and
     ((ctx_AccountRights.Functions.GetState('UPDATE_AS_OF') <> stEnabled) or
     (ctx_AccountRights.Functions.GetState('USER_CAN_EDIT_TAX_EXEMPTIONS') <> stEnabled) or
     (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled)) then
  begin
    Component.GetInterface(IevDBFieldControl, DBFieldControl);
    if Assigned(DBFieldControl) then
    begin
      if Assigned(DBFieldControl.DataSource) and Assigned(DBFieldControl.DataSource.DataSet) and
         (DBFieldControl.DataSource.DataSet is TevClientDataSet) and
         (DBFieldControl.DataSource.DataSet.State <> dsInsert) then
      begin
        s := TevClientDataSet(DBFieldControl.DataSource.DataSet).TableName + '.' + DBFieldControl.FieldName;

        if Contains(UPDATE_AS_OF, s + ',') and
          (ctx_AccountRights.Functions.GetState('UPDATE_AS_OF') <> stEnabled) then
          SetOrdProp(Component, 'SecurityRO', Ord(True))
        else
        if Contains(USER_CAN_EDIT_TAX_EXEMPTIONS, s + ',') and
          (ctx_AccountRights.Functions.GetState('USER_CAN_EDIT_TAX_EXEMPTIONS') <> stEnabled) then
          SetOrdProp(Component, 'SecurityRO', Ord(True))
        else
        if Contains(UPDATE_ACA_AS_OF, s + ',') and
          (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled) then
          SetOrdProp(Component, 'SecurityRO', Ord(True))
      end;
    end;
    DBFieldControl := nil;
  end;


  for i := 0 to Component.ComponentCount-1 do
    ApplySecurity(Component.Components[i]);
end;

procedure TevEvolutionGUI.AsyncCallReturn(const ACallID: TisGUID; const AResults: IisListOfValues);
begin
end;

function TevEvolutionGUI.GUIContext: IevContext;
begin
  Result := FGUIContext;
end;

procedure TevEvolutionGUI.CreateMainForm;
var
  Frm: TForm;
begin
  if not Assigned(FMainForm) then
  begin
    FGUIContext := Mainboard.ContextManager.GetThreadContext;

    if FileExists(GUIContext.Statistics.FileName) then
      FGUIContext.Statistics.Enabled := mb_AppSettings.AsBoolean['Settings\CollectStatistics']
    else
      mb_AppSettings.AsBoolean['Settings\CollectStatistics'] := False;
    FGUIContext.Statistics.DumpIntoFile := True;

    Mainboard.GlobalCallbacks.Subscribe('', cetTaskQueueEvent,
      EncodeUserAtDomain(FGUIContext.UserAccount.User, FGUIContext.UserAccount.Domain));

    Application.ShowMainForm := False;
    Application.CreateForm(TMainPackageForm, Frm);
    FMainForm := TMainPackageForm(Frm);
    FMainForm.Initialize;
    FMainForm.Show;
  end;
end;

destructor TevEvolutionGUI.Destroy;
begin
  Application.OnModalBegin := nil;
  Application.OnModalEnd := nil;
  Application.OnException := nil;

  FreeandNil(FWaitDialog);
  inherited;
end;

procedure TevEvolutionGUI.DestroyMainForm;
begin
  FreeAndNil(FMainForm);
  FGUIContext := nil;
end;

procedure TevEvolutionGUI.DoOnConstruction;
begin
  inherited;
  Application.OnModalBegin := OnShowModalDialog;
  Application.OnModalEnd := OnHideModalDialog;
  Application.OnException := OnException;
  
  FWaitDialog := TAdvancedWait.Create;
  FPendingTaskList := TisStringList.CreateAsIndex(True);
end;

procedure TevEvolutionGUI.EndWait;
begin
  if UnAttended and
     not (Assigned(FGUIContext) and (Context <> nil) and (FGUIContext.GetID = Context.GetID)) then
    Exit;

  if UnAttended then
    FMainForm.WaitObjectOperation(woEnd, '', 0, 0)
  else
    FWaitDialog.EndWait;
end;

procedure TevEvolutionGUI.StartWait(const AText: string; AMaxProgress: Integer);
begin
  if UnAttended and
     not (Assigned(FGUIContext) and (Context <> nil) and (FGUIContext.GetID = Context.GetID)) then
    Exit;

  if UnAttended then
    FMainForm.WaitObjectOperation(woStart, AText, 0, AMaxProgress)
  else
    FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TevEvolutionGUI.UpdateWait(const AText: string; ACurrentProgress: Integer; AMaxProgress: Integer);
begin
  if UnAttended and
     not (Assigned(FGUIContext) and (Context <> nil) and (FGUIContext.GetID = Context.GetID)) then
    Exit;

  if UnAttended then
    FMainForm.WaitObjectOperation(woUpdate, AText, ACurrentProgress, AMaxProgress)
  else
    FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

procedure TevEvolutionGUI.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True);
var
  i: Integer;
begin
  if Supports(ATask, IevPrintableTask) then
    i := FPendingTaskList.Add(ATask.ID)
  else
    i := -1;

  try
    mb_TaskQueue.AddTask(ATask);
  except
    if i <> -1 then
      FPendingTaskList.Delete(i);
    raise;  
  end;

  if AShowComfirmation then
    EvMessage('Your task has been added to the queue');
end;

procedure TevEvolutionGUI.CheckSessionStatus;
begin
  if Assigned(FMainForm) then
    FMainForm.CheckSessionStatus;
end;

procedure TevEvolutionGUI.OnException(Sender: TObject; E: Exception);
begin
  FWaitDialog.KillWait;
  EvExceptMessage(E);
end;

procedure TevEvolutionGUI.OnHideModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.ShowIfHidden;
end;

procedure TevEvolutionGUI.OnShowModalDialog(Sender: TObject);
begin
  if not UnAttended then
    FWaitDialog.Hide;

  DispatchAppPaint;
end;

{ TevGUIGlobalCallbacks }

procedure TevEvolutionGUI.CheckTaskQueueStatus;
begin
  if Assigned(FMainForm) then
    FMainForm.UpdateTaskQueueStatus;
end;

procedure TevEvolutionGUI.PrintTask(const ATaskID: TisGUID);
var
  i: Integer;
  Ctx: IevContext;
begin
  FPendingTaskList.Lock;
  try
    i := FPendingTaskList.IndexOf(ATaskID);
    if i <> -1 then
      FPendingTaskList.Delete(i);
  finally
    FPendingTaskList.UnLock;
  end;

  if i <> -1 then
  begin
    // Current context is not necesserely logged user context! Most likely it's a Guest.
    Mainboard.ContextManager.StoreThreadContext;
    try
      // It needs to create new context in here because of IevRWLocalEngine
      Ctx := Mainboard.ContextManager.CopyContext(FGUIContext);
      Mainboard.ContextManager.SetThreadContext(Ctx);
      ctx_RWLocalEngine.Print(ATaskID);
    finally
      Mainboard.ContextManager.RestoreThreadContext;
    end;
  end;
end;

procedure TevEvolutionGUI.OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
  if Assigned(FMainForm) then
    FMainForm.NotifyGlobalFlagChanges(AFlagInfo, AOperation);
end;

procedure TevEvolutionGUI.OnWaitingServerResponse;
var
  Msg: TMsg;
begin
  if not UnAttended then
  begin
    try
      while PeekMessage(Msg, 0, CM_WAIT_OBJECT_OPER, CM_WAIT_OBJECT_OPER, PM_REMOVE) do
        DispatchMessage(Msg);

      FWaitDialog.UpdateTimer;

      while PeekMessage(Msg, 0, WM_PAINT, WM_PAINT, PM_REMOVE) do
        DispatchMessage(Msg);
    except
    end;
  end;
end;

procedure TevEvolutionGUI.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
  if Assigned(FMainForm) then
    FMainForm.ShowPopupMessage(aText, aFromUser, aToUsers);
end;

procedure TevEvolutionGUI.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);

  procedure SubscribeForNotifications(const Params: TTaskParamList);
  begin
    Snooze(500); // to make sure session is initialized
    Mainboard.GlobalCallbacks.Subscribe('', cetTaskQueueEvent,
      EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
  end;

begin
  CheckSessionStatus;

  if (AStatus = ssActive) and Assigned(FGUIContext) then
    FGUIContext.RunParallelTask(@SubscribeForNotifications, MakeTaskParams([]), 'Subscribe for events');
end;

procedure TevEvolutionGUI.AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean);
var
  i, j: Integer;
  T: IevTask;
begin
  if ATaskList.Count = 1 then
  begin
    AddTaskQueue(ATaskList[0] as IevTask, AShowComfirmation);
    exit;
  end;

  for i := 0 to ATaskList.Count - 1 do
  begin
    T := ATaskList[i] as IevTask;
    if Supports(T, IevPrintableTask) then
      FPendingTaskList.Add(T.ID);
  end;

  try
    mb_TaskQueue.AddTasks(ATaskList);
  except
    for i := 0 to ATaskList.Count - 1 do
    begin
      T := ATaskList[i] as IevTask;
      if Supports(T, IevPrintableTask) then
      begin
        j := FPendingTaskList.IndexOf(T.ID);
        if j <> - 1 then
          FPendingTaskList.Delete(j);
      end;
    end;
    raise;
  end;

  if AShowComfirmation then
    EvMessage('Your ' + IntToStr(ATaskList.Count) +  ' tasks have been added to the queue');
end;

procedure TevEvolutionGUI.UserSchedulerEvent(const AEventID, ASubject: String;
  const AScheduledTime: TDateTime);
begin
  if Assigned(FMainForm) then
    FMainForm.DispatchUserSchedulerEvent(AEventID, ASubject, AScheduledTime);
end;

procedure TevEvolutionGUI.PasswordChangeClose;
begin
  if Assigned(FMainForm) then
    FMainForm.PasswordChangeClose;
end;

initialization
  DisableProcessWindowsGhosting;
  Mainboard.ModuleRegister.RegisterModule(@GetEvolutionGUI, IevEvolutionGUI, 'Evolution GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetContextCallbacks, IevContextCallbacks, 'Context Callbacks');
end.


