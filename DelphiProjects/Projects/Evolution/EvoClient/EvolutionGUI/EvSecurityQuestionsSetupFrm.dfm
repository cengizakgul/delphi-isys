object EvSecurityQuestionsSetup: TEvSecurityQuestionsSetup
  Left = 391
  Top = 96
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Security Setup'
  ClientHeight = 355
  ClientWidth = 516
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    516
    355)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 17
    Width = 244
    Height = 13
    Caption = 'Please set up three questions and answers'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object OKBtn: TevBitBtn
    Left = 328
    Top = 314
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
  end
  object CancelBtn: TevBitBtn
    Left = 424
    Top = 314
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
  end
  object pnlQuestions: TevPanel
    Left = 16
    Top = 42
    Width = 483
    Height = 251
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 16
      Top = 23
      Width = 81
      Height = 13
      AutoSize = False
      Caption = '~Question 1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 16
      Top = 53
      Width = 66
      Height = 13
      AutoSize = False
      Caption = '~Answer 1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Lable3: TevLabel
      Left = 16
      Top = 103
      Width = 81
      Height = 13
      AutoSize = False
      Caption = '~Question 2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel4: TevLabel
      Left = 16
      Top = 134
      Width = 74
      Height = 13
      AutoSize = False
      Caption = '~Answer 2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel5: TevLabel
      Left = 16
      Top = 182
      Width = 83
      Height = 13
      AutoSize = False
      Caption = '~Question 3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel6: TevLabel
      Left = 16
      Top = 211
      Width = 66
      Height = 13
      AutoSize = False
      Caption = '~Answer 3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cbQuestion1: TevComboBox
      Left = 103
      Top = 20
      Width = 362
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object edAnswer1: TevEdit
      Left = 103
      Top = 50
      Width = 362
      Height = 21
      MaxLength = 80
      TabOrder = 1
      OnExit = edAnswer1Exit
    end
    object cbQuestion2: TevComboBox
      Left = 103
      Top = 100
      Width = 362
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
    end
    object edAnswer2: TevEdit
      Left = 103
      Top = 130
      Width = 362
      Height = 21
      MaxLength = 80
      TabOrder = 3
      OnExit = edAnswer1Exit
    end
    object cbQuestion3: TevComboBox
      Left = 103
      Top = 179
      Width = 362
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
    end
    object edAnswer3: TevEdit
      Left = 103
      Top = 208
      Width = 362
      Height = 21
      MaxLength = 80
      TabOrder = 5
      OnExit = edAnswer1Exit
    end
  end
end
