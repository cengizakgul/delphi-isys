object WaitingObjectForm: TWaitingObjectForm
  Left = 372
  Top = 246
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Current status'
  ClientHeight = 154
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    384
    154)
  PixelsPerInch = 96
  TextHeight = 13
  object TimerLabel: TLabel
    Left = 8
    Top = 55
    Width = 26
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Timer'
  end
  object lText: TLabel
    Left = 8
    Top = 8
    Width = 369
    Height = 39
    AutoSize = False
    Caption = '1'#13'2'#13'3'
  end
  object AnimatedPicture: TAnimate
    Left = 8
    Top = 87
    Width = 272
    Height = 60
    Active = True
    Anchors = [akLeft, akBottom]
    CommonAVI = aviCopyFiles
    StopFrame = 31
  end
  object PB: TProgressBar
    Left = 8
    Top = 71
    Width = 369
    Height = 16
    Anchors = [akLeft, akBottom]
    Smooth = True
    TabOrder = 1
  end
end
