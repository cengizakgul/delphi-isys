// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_RASConnection;

interface

uses
  Windows,  Menus, StdCtrls, Controls, ComCtrls, ExtCtrls, Classes, Forms,
  SysUtils, EvUtils, Buttons, ISBasicClasses, SEncryptionRoutines, EvMainboard, Dialogs,
  EvCommonInterfaces, EvConsts, isBasicUtils, EvUIUtils, EvUIComponents,
  isUIEdit, LMDCustomButton, LMDButton, isUILMDButton;

type
  TRASConnectionForm = class(TForm, IevRASAccessGUI)
    MainStb: TStatusBar;
    Panel1: TevPanel;
    Panel2: TevPanel;
    Panel3: TevPanel;
    Panel4: TevPanel;
    Panel5: TevPanel;
    Panel6: TevPanel;
    Panel7: TevPanel;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    GroupBox2: TevGroupBox;
    MainConnectionLbl: TevLabel;
    MainConnectionCmb: TevComboBox;
    Panel8: TevPanel;
    Panel9: TevPanel;
    Panel10: TevPanel;
    Panel11: TevPanel;
    MainMenu: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Connection1: TMenuItem;
    Connect1: TMenuItem;
    TabSheet3: TTabSheet;
    Panel16: TevPanel;
    Panel17: TevPanel;
    Panel18: TevPanel;
    Panel19: TevPanel;
    GroupBox3: TevGroupBox;
    AltConnectionLbl: TevLabel;
    AltConnectionCmb: TevComboBox;
    MainAltConCmb: TevCheckBox;
    MainConnectionUserLbl: TevLabel;
    MainConnectionPasswordLbl: TevLabel;
    MainConnectionUserEdit: TevEdit;
    MainConnectionPasswordEdit: TevEdit;
    AltConnectionUserLbl: TevLabel;
    AltConnectionPasswordLbl: TevLabel;
    AltConnectionUserEdit: TevEdit;
    AltConnectionPasswordEdit: TevEdit;
    Label1: TevLabel;
    BtnOK: TevBitBtn;
    DialBtn: TevBitBtn;
    tsConnSettings: TTabSheet;
    chbEncryption: TCheckBox;
    Label2: TLabel;
    edPort: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure DialBtnClick(Sender: TObject);
    procedure ComboBoxChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MainAltConCmbClick(Sender: TObject);
    procedure RefreshBtnClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edPortChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReadRegistry;
    procedure WriteRegistry;
    procedure RefreshConnectionStatus(ConnectionName: string);
    function  GetConnectionName: string;
    function  GetConnectionUser: string;
    function  GetConnectionPassword: string;
    function  GetPort: string;
    procedure SetPort(const AValue: string);
    function  GetEncryption: Boolean;
    procedure SetEncryption(const AValue: Boolean);
  protected
    procedure UpdateRASStatus(const AStatus: String);
    procedure UpdateRASDetails(const ADetails: String);
    procedure ShowMessage(const AText: String);
    procedure OnBusyConnection(const AConnectionName: String; var ADisconnect: Boolean);
  public
    property  Port: string read GetPort write SetPort;
    property  Encryption: Boolean read GetEncryption write SetEncryption;
  end;

const
//  _LAN = 'LAN (Direct connection)';
  _CONNECT = 'Connect';
  _DISCONNECT = 'Disconnect';

implementation

{$R *.DFM}

procedure TRASConnectionForm.FormCreate(Sender: TObject);
var
  s: string;
begin
  edPort.Items.Clear;
  edPort.Items.Add(IntToStr(TCPClient_Port));
  edPort.Items.Add(IntToStr(TCPClient_PortSSL1));
  edPort.Items.Add(IntToStr(TCPClient_PortSSL2));
  edPort.Items.Add(IntToStr(TCPClient_PortSSL3));
  edPort.Items.Add(IntToStr(TCPClient_PortHTTPS));

  ReadRegistry;
  s := Mainboard.RASAccess.GetRASEntries;

  MainConnectionCmb.Items.Text := s;
  AltConnectionCmb.Items.Text := s;
  Mainboard.RASAccess.SetGUICallback(Self);
  if (MainConnectionCmb.Text <> '') then
    RefreshConnectionStatus(MainConnectionCmb.Text)
  else
    Mainboard.RASAccess.BecomeIdle;
end;

procedure TRASConnectionForm.DialBtnClick(Sender: TObject);
begin
{  if (MainConnectionCmb.Text = _LAN) or (AltConnectionCmb.Text = _LAN) then
  begin
    Close;
    Exit;
  end;
 }
  PageControl1.Enabled := False;
  if (DialBtn.Caption = _CONNECT) then
  begin
    Mainboard.RASAccess.DialRasConnection(GetConnectionName, GetConnectionUser, GetConnectionPassword);
    DialBtn.Caption := _DISCONNECT;
  end
  else
  begin
    Mainboard.RASAccess.HangupRasConnection(GetConnectionName);
    DialBtn.Caption := _CONNECT;
  end;
  PageControl1.Enabled := True;
end;

procedure TRASConnectionForm.ComboBoxChange(Sender: TObject);
begin
  RefreshConnectionStatus(TevComboBox(Sender).Text);
end;

procedure TRASConnectionForm.ReadRegistry;
var
  s: string;
begin
  MainConnectionCmb.Text := mb_AppSettings['Primary RAS'];
  AltConnectionCmb.Text := mb_AppSettings['Secondary RAS'];
  MainConnectionUserEdit.Text := mb_AppSettings['Primary RAS User'];
  s := mb_AppSettings['Primary RAS Password'];
  if s <> '' then
  begin
    try
      MainConnectionPasswordEdit.Text := DecryptString(s, Mainboard.MachineInfo.CipherKey);
    except
      MainConnectionPasswordEdit.Text := '';
    end;
  end
  else
    MainConnectionPasswordEdit.Text := '';
  AltConnectionUserEdit.Text := mb_AppSettings['Secondary RAS User'];
  s := mb_AppSettings['Secondary RAS Password'];
  if s <> '' then
  begin
    try
      AltConnectionPasswordEdit.Text := DecryptString(s, Mainboard.MachineInfo.CipherKey);
    except
      AltConnectionPasswordEdit.Text := '';
    end;
  end
  else
    AltConnectionPasswordEdit.Text := '';
end;

procedure TRASConnectionForm.WriteRegistry;
begin
  mb_AppSettings['Primary RAS'] :=  MainConnectionCmb.Text;
  mb_AppSettings['Secondary RAS'] := AltConnectionCmb.Text;
  mb_AppSettings['Primary RAS User'] := MainConnectionUserEdit.Text;
  mb_AppSettings['Primary RAS Password'] := EncryptString(MainConnectionPasswordEdit.Text, Mainboard.MachineInfo.CipherKey);
  mb_AppSettings['Secondary RAS User'] := AltConnectionUserEdit.Text;
  mb_AppSettings['Secondary RAS Password'] := EncryptString(AltConnectionPasswordEdit.Text, Mainboard.MachineInfo.CipherKey);
end;

procedure TRASConnectionForm.FormDestroy(Sender: TObject);
begin
  Mainboard.RASAccess.SetGUICallback(nil);
  WriteRegistry;
end;

procedure TRASConnectionForm.MainAltConCmbClick(Sender: TObject);
begin
  MainConnectionCmb.Enabled := not MainAltConCmb.Checked;
  MainConnectionLbl.Enabled := not MainAltConCmb.Checked;
  MainConnectionUserLbl.Enabled := not MainAltConCmb.Checked;
  MainConnectionPasswordLbl.Enabled := not MainAltConCmb.Checked;
  MainConnectionUserEdit.Enabled := not MainAltConCmb.Checked;
  MainConnectionPasswordEdit.Enabled := not MainAltConCmb.Checked;

  AltConnectionCmb.Enabled := MainAltConCmb.Checked;
  AltConnectionLbl.Enabled := MainAltConCmb.Checked;
  AltConnectionUserLbl.Enabled := MainAltConCmb.Checked;
  AltConnectionPasswordLbl.Enabled := MainAltConCmb.Checked;
  AltConnectionUserEdit.Enabled := MainAltConCmb.Checked;
  AltConnectionPasswordEdit.Enabled := MainAltConCmb.Checked;
end;

procedure TRASConnectionForm.RefreshBtnClick(Sender: TObject);
begin
  RefreshConnectionStatus(GetConnectionName);
end;

procedure TRASConnectionForm.RefreshConnectionStatus(ConnectionName: string);
begin
  Mainboard.RASAccess.SetConnection(ConnectionName);
  if (ConnectionName = Mainboard.RASAccess.GetFirstConnectionName) then
    DialBtn.Caption := _DISCONNECT
  else
    DialBtn.Caption := _CONNECT;
  Mainboard.RASAccess.RefreshStatus;
end;

function TRASConnectionForm.GetConnectionName: string;
begin
  if MainAltConCmb.Checked then
    Result := AltConnectionCmb.Text
  else
    Result := MainConnectionCmb.Text;
end;

procedure TRASConnectionForm.BtnOKClick(Sender: TObject);
begin
  if not DialBtn.Focused then
    ModalResult := mrOK
  else
    DialBtn.Click;
end;

procedure TRASConnectionForm.Exit1Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

function TRASConnectionForm.GetConnectionPassword: string;
begin
  if MainAltConCmb.Checked then
    Result := AltConnectionPasswordEdit.Text
  else
    Result := MainConnectionPasswordEdit.Text;
end;

function TRASConnectionForm.GetConnectionUser: string;
begin
  if MainAltConCmb.Checked then
    Result := AltConnectionUserEdit.Text
  else
    Result := MainConnectionUserEdit.Text;
end;

procedure TRASConnectionForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

procedure TRASConnectionForm.UpdateRASDetails(const ADetails: String);
begin
  MainStb.Panels[1].Text := ADetails;
  Update;  
end;

procedure TRASConnectionForm.UpdateRASStatus(const AStatus: String);
begin
  MainStb.Panels[0].Text := AStatus;
  Update;
end;

procedure TRASConnectionForm.ShowMessage(const AText: String);
begin
  EvMessage(AText);
end;

procedure TRASConnectionForm.OnBusyConnection(const AConnectionName: String; var ADisconnect: Boolean);
begin
  ADisconnect := EvMessage('The device is used by connection ' + AConnectionName +
                      '. Would you like to disconnect it and continue?', mtConfirmation, [mbYes, mbNo]) = mrYes;
end;

function TRASConnectionForm.GetEncryption: Boolean;
begin
  Result := chbEncryption.Checked;
end;

function TRASConnectionForm.GetPort: string;
begin
  Result := edPort.Text;
end;

procedure TRASConnectionForm.SetEncryption(const AValue: Boolean);
begin
  chbEncryption.Checked := AValue;
end;

procedure TRASConnectionForm.SetPort(const AValue: string);
begin
  if edPort.Items.IndexOf(AValue) = -1 then
    edPort.Items.Add(AValue);

  edPort.Text := AValue;
end;

procedure TRASConnectionForm.edPortChange(Sender: TObject);
var
  Port: Integer;
begin
  Port := StrToIntDef(edPort.Text, 0);
  if Port <> TCPClient_Port then
  begin
    chbEncryption.Caption := 'Secure Connection (SSL method)';
    chbEncryption.Checked := True;
    chbEncryption.Enabled := False;
  end
  else
  begin
    chbEncryption.Caption := 'Secure Connection (Proprietary method)';
    chbEncryption.Enabled := True;
  end;
end;

procedure TRASConnectionForm.FormShow(Sender: TObject);
begin
  edPortChange(nil);
end;

end.
