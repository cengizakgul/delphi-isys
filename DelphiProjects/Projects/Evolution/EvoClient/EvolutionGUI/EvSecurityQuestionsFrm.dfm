object EvSecurityQuestions: TEvSecurityQuestions
  Left = 414
  Top = 141
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Password Reset'
  ClientHeight = 421
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    447
    421)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 11
    Width = 335
    Height = 13
    Caption = 'Please answer these questions and set up a new password'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object OKBtn: TevBitBtn
    Left = 259
    Top = 380
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
  end
  object CancelBtn: TevBitBtn
    Left = 355
    Top = 380
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
  end
  object pnlQuestions: TevPanel
    Left = 16
    Top = 36
    Width = 415
    Height = 201
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object lQuestion1: TevLabel
      Left = 16
      Top = 16
      Width = 51
      Height = 13
      Caption = 'Question 1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lQuestion2: TevLabel
      Left = 16
      Top = 74
      Width = 51
      Height = 13
      Caption = 'Question 2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lQuestion3: TevLabel
      Left = 16
      Top = 137
      Width = 51
      Height = 13
      Caption = 'Question 3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edAnswer1: TevEdit
      Left = 16
      Top = 34
      Width = 377
      Height = 21
      MaxLength = 80
      TabOrder = 0
      OnExit = edAnswer1Exit
    end
    object edAnswer2: TevEdit
      Left = 16
      Top = 92
      Width = 377
      Height = 21
      MaxLength = 80
      TabOrder = 1
      OnExit = edAnswer1Exit
    end
    object edAnswer3: TevEdit
      Left = 16
      Top = 155
      Width = 377
      Height = 21
      MaxLength = 80
      TabOrder = 2
      OnExit = edAnswer1Exit
    end
  end
  object pnlPassword: TevPanel
    Left = 16
    Top = 261
    Width = 415
    Height = 99
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object evLabel1: TevLabel
      Left = 16
      Top = 16
      Width = 71
      Height = 13
      Caption = 'New Password'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 192
      Top = 16
      Width = 109
      Height = 13
      Caption = 'Confirm New Password'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lPasswordRules: TevLabel
      Left = 16
      Top = 69
      Width = 75
      Height = 13
      Caption = 'lPasswordRules'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edPassword: TevEdit
      Left = 16
      Top = 34
      Width = 146
      Height = 21
      MaxLength = 40
      PasswordChar = '*'
      TabOrder = 0
    end
    object edConfirmPassword: TevEdit
      Left = 193
      Top = 34
      Width = 146
      Height = 21
      MaxLength = 40
      PasswordChar = '*'
      TabOrder = 1
    end
  end
end
