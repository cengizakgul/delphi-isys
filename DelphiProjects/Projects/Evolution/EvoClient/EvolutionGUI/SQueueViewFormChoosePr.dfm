object frmChoosePriority: TfrmChoosePriority
  Left = 655
  Top = 316
  BorderStyle = bsToolWindow
  Caption = 'Choose Priority'
  ClientHeight = 134
  ClientWidth = 236
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbxPriority: TListBox
    Left = 0
    Top = 0
    Width = 236
    Height = 105
    Align = alTop
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = lbxPriorityDblClick
  end
  object EnableFiltering: TCheckBox
    Left = 8
    Top = 112
    Width = 153
    Height = 17
    Caption = 'Filter by priority'
    TabOrder = 1
  end
  object Button1: TButton
    Left = 174
    Top = 108
    Width = 51
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
end
