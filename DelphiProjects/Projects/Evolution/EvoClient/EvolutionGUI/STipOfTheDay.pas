// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STipOfTheDay;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  StdCtrls, Buttons,  ExtCtrls, ISBasicClasses, EvContext,
  EvCommonInterfaces, EvMainboard, EvDataAccessComponents, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TTipOfTheDay = class(TForm)
    cbHideTip: TevCheckBox;
    evPanel1: TevPanel;
    evImage1: TevImage;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evLabel1: TevLabel;
    lablTip: TevLabel;
    evLabel2: TevLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure evBitBtn2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    TipsList: TStringList;
    LastTipNbr: Integer;
    procedure GetTip;
    procedure InitIfNeeds;
  public
    function ShowOnStartup: Boolean;
  end;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TTipOfTheDay.FormCreate(Sender: TObject);
begin
  cbHideTip.Checked := mb_AppSettings['HideTipOfTheDay'] = 'Y';
end;

procedure TTipOfTheDay.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if cbHideTip.Checked then
    mb_AppSettings['HideTipOfTheDay'] :=  'Y'
  else
    mb_AppSettings['HideTipOfTheDay'] := 'N';
  mb_AppSettings['LastTipNbr'] := IntToStr(LastTipNbr);
end;

procedure TTipOfTheDay.evBitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TTipOfTheDay.FormDestroy(Sender: TObject);
begin
  TipsList.Free;
end;

procedure TTipOfTheDay.GetTip;
begin
  if TipsList.Count > 0 then
  begin
    Inc(LastTipNbr);
    if LastTipNbr > TipsList.Count then
      LastTipNbr := 1;
    lablTip.Caption := TipsList.Strings[LastTipNbr - 1];
  end;
end;

procedure TTipOfTheDay.evBitBtn1Click(Sender: TObject);
begin
  GetTip;
end;

procedure TTipOfTheDay.InitIfNeeds;
var
  I: Integer;
  S: String;
begin
  if Assigned(TipsList) then
    exit;

  TipsList := TStringList.Create;
  LastTipNbr := mb_AppSettings.AsInteger['LastTipNbr'];
  if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
    ctx_DataAccess.SY_CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('Columns', 'STORAGE_DATA');
    SetMacro('TableName', 'SY_STORAGE');
    SetMacro('Condition', 'TAG=''TIP_OF_THE_DAY''');
    ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.SY_CUSTOM_VIEW.Open;
    if ctx_DataAccess.SY_CUSTOM_VIEW.RecordCount > 0 then
      TipsList.Text := ctx_DataAccess.SY_CUSTOM_VIEW.FieldByName('STORAGE_DATA').AsString;
    ctx_DataAccess.SY_CUSTOM_VIEW.Close;
  end;

  I := 0;
  while I < TipsList.Count do
  begin
    S := TipsList.Strings[I];
    if Length(S) = 0 then
      TipsList.Delete(I)
    else
    if S[Length(S)] = 'R' then
    begin
      if Context.UserAccount.AccountType = uatRemote then
      begin
        Delete(S, Length(S), 1);
        TipsList.Strings[I] := S;
        Inc(I);
      end
      else
        TipsList.Delete(I)
    end
    else
    if S[Length(S)] = 'B' then
    begin
      if Context.UserAccount.AccountType <> uatRemote then
      begin
        Delete(S, Length(S), 1);
        TipsList.Strings[I] := S;
        Inc(I);
      end
      else
        TipsList.Delete(I)
    end
    else
      Inc(I);
  end;
  GetTip;
end;

function TTipOfTheDay.ShowOnStartup: Boolean;
begin
  Result := not cbHideTip.Checked;
end;

procedure TTipOfTheDay.FormShow(Sender: TObject);
begin
  InitIfNeeds;
end;

end.
