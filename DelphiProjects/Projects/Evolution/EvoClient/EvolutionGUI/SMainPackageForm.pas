// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SMainPackageForm;

interface

uses
  Dialogs, Math, DB,
   ComCtrls, Controls, Buttons, Classes, isBaseClasses, isThreadManager,
  ExtCtrls, Forms, Windows, Sysutils, EvUtils, EvTypes, Messages, Menus, EvBasicUtils,
  EvSecurityUtils, EvSecElement, SSecurityInterface, IniFiles, EvInitApp,
  ImgList, graphics, SPackageEntry, EvStreamUtils, SFrameEntry, EvTransportInterfaces,
  StdCtrls, ShellAPI, ISBasicClasses, ISUtils, SEncryptionRoutines, ISErrorUtils,
  EvMainboard, EvCommonInterfaces, isBasicUtils, EvContext, EvConsts, SQueueViewForm, STipOfTheDay,
  EvDataset, StoHtmlHelp, EvUIUtils, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, isMessenger,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, Registry, isExceptions, EvDashboardFrm;

const
  CM_REFRESH_QUEUE_STATUS = WM_USER + 1;
  CM_SHOW_TIPS = WM_USER + 2;
  CM_GLOBAL_FLAG_CHANGE = WM_USER + 3;
  CM_REPAINT_STATUS_BAR = WM_USER + 4;
  CM_SHOW_POPUP_MESSAGE = WM_USER + 5;
  CM_DISPATCH_USER_SCHEDULER_EVENT = WM_USER + 6;

type
  TControlItem = record
    PackageID: String;
    PClass: string;
    Path: string;
    case IsMenuItem: Boolean of
      False: (TabIndex: Integer; Menu: TevPopupMenu);
      True: (MenuItem: TMenuItem);
  end;

  TAControlItem = array of TControlItem;
  TevWaitObjectOperType = (woStart, woUpdate, woEnd);

  TevWaitObjectOperInfo = record
    Oper:     TevWaitObjectOperType;
    Text:     String;
    Progress: Integer;
    ProgressMax: Integer;
  end;

  PevWaitObjectOperInfo = ^TevWaitObjectOperInfo;

  TMainPackageForm = class(TForm, IisMessageRecipient)
    FunctionKeyPanel: TEvPanel;
    TabControl: TEvTabControl;
    MainWorkPanel: TevPanel;
    ButtonImageList: TevImageList;
    StatusBar: TevStatusBar;
    evPanel1: TevPanel;
    sbtnTipOfTheDay: TevSpeedButton;
    btnExit: TevSpeedButton;
    imLockIcon: TImage;
    imConnectedIcon: TImage;
    imDisconnectedIcon: TImage;
    sbtnDumpTraffic: TevSpeedButton;
    btnReminderWnd: TevSpeedButton;
    btnDashboard: TevSpeedButton;
    procedure btnExitClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TabControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure TabControlChange(Sender: TObject);
    procedure TabControlMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TabControlGetImageIndex(Sender: TObject; TabIndex: Integer;
      var ImageIndex: Integer);
    procedure sbtnTipOfTheDayClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure FormResize(Sender: TObject);
    procedure StatusBarMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StatusBarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StatusBarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure sbtnDumpTrafficClick(Sender: TObject);
    procedure btnReminderWndClick(Sender: TObject);
    procedure btnDashboardClick(Sender: TObject);
  private
    FCaption: string;
    FSysDbVer: string;
    IDList: TAControlItem;
    iNewTab: Integer;
    bTabWasChanged: Boolean;
    CurrentPackageID: String;
    FIndexMap: array of integer; //gdy13
    FTipOfTheDay: TTipOfTheDay;
    FPasswordChangeClose: Boolean;
    FDashboard: TevDashboard;
    function  MakeCaption: string;
    function  GetSysDbVersion: string;
    procedure SecElementCallback(const Sender: TComponent);
    procedure ActivateFrame(var Message: TMessage); message WM_ACTIVATE_FRAME;
    procedure ActivateFrameAs(var Message: TMessage); message WM_ACTIVATE_FRAME_PARAMS;
    procedure ActivatePackage(var Message: TMessage); message WM_ACTIVATE_PACKAGE;
    procedure RecreateMenus(var Message: TMessage); message WM_RECREATE_MENUS;
    procedure CreateMenus;
    procedure DoAction(TabNumber: Integer; P: TPoint);
    function  ActivateScreen(ID: String; PClass: string; Path: string): Boolean;
    procedure MenuOnClick(Sender: TObject);
    function  CheckCommited: Boolean;
    procedure CreateMenuStructure;
    procedure CMRefreshQueueStatus(var Message: TMessage); message CM_REFRESH_QUEUE_STATUS;
    procedure CMShowTips(var Message: TMessage); message CM_SHOW_TIPS;
    procedure CMGlobalFlagChange(var Message: TMessage); message CM_GLOBAL_FLAG_CHANGE;
    procedure CMWaitObjectOper(var Message: TMessage); message CM_WAIT_OBJECT_OPER;
    procedure CMRepaintStatusBar(var Message: TMessage); message CM_REPAINT_STATUS_BAR;
    procedure CMShowPopupMessage(var Message: TMessage); message CM_SHOW_POPUP_MESSAGE;
    procedure CMDispatchUserSchedulerEvent(var Message: TMessage); message CM_DISPATCH_USER_SCHEDULER_EVENT;
    function  ActiveScreen: TFrameEntry;
    procedure MarkNewFinishedAsRead;
    function  GetCurrentScreenPackage: IevScreenPackage;
    procedure ShowDashboard;

    // isMessenger
    procedure Notify(const AMessage: String; const AParams: IisListOfValues);
    function  Destroying: Boolean;
  public
    procedure   Initialize;
    procedure   UpdateTaskQueueStatus;
    procedure   NotifyGlobalFlagChanges(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure   WaitObjectOperation(const AOper: TevWaitObjectOperType; const AText: String;
                                    const AProgress, AProgressMax: Integer);
    procedure   ShowPopupMessage(const aText, aFromUser, aToUser: String);
    procedure   DispatchUserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
    procedure   CheckSessionStatus;
    procedure   PasswordChangeClose;
    constructor Create( Owner: TComponent ); override; //gdy13
    destructor  Destroy; override; //gdy13
  protected
    procedure Loaded; override;
  end;

var
  MainPackageForm: TMainPackageForm;

implementation

uses
  SWaitingObjectForm, SShowLogo, SDataStructure;

{$R *.DFM}

procedure TMainPackageForm.CreateMenus;
var
  f: TStringList;
  i, j: Integer;
  s: string;
  iLevel: Integer;
  PClass: string;
  ID: String;
  sName: string;
  pm: TevPopupMenu;
  ti: Integer;
  mi: TMenuItem;
  idi: Integer;
  bDefault: Boolean;
  Bitmap: graphics.TBitmap;
  c: ISecurityElement;


  function GetLevel(iLevel: Integer; Item: TStrItem): string;
  var
    i: Integer;
    s: string;
  begin
    Result := '';
    s := Copy(Item.Path, 2, Length(Item.Path));
    i := 0;
    repeat
      Inc(i);
      if s[i] = '\' then
        Dec(iLevel);
    until (iLevel = 0) or (i = Length(s));

    //if Item.Bitmap <> nil then
      //ODS('Item.Bitmap <> nil');
    if iLevel = 0 then
      Result := #255#1 + Copy(s, 1, Pred(i)) + #1'0'#1''#1'0'#1'0'//gdy13
    else if iLevel = 1 then
      Result := Item.Position + #1 + s + #1 + Item.PackageID + #1 + Item.PClass + #1 + IntTOStr(Integer(Item.Default)) + #1 + IntTOStr(Integer(Item.Bitmap))//gdy13
    else
      Result := '';
  end;

  Procedure SetMenuParentTrue(Parent: TMenuItem);
  begin
    if Assigned(Parent) then
    begin
      Parent.Visible := True;
      if Assigned(Parent.Parent) and (not Parent.Parent.Visible) then
          SetMenuParentTrue(Parent.Parent)
    end;
  end;

  function GetMenuItem(Path: string; Parent: TMenuItem): TMenuItem;
  var
    s: string;
  begin
    if Pos('\', Path) > 0 then
      s := Copy(Path, 1, Pred(Pos('\', Path)))
    else
      s := Path;

    Result := Parent.Find(s);
    if not Assigned(Result) then
    begin
      Result := TMenuItem.Create(Self);
      Result.Caption := s;
      Result.AutoHotkeys := maAutomatic;
      Parent.Add(Result);
    end;

    if Pos('\', Path) > 0 then
      Result := GetMenuItem(Copy(Path, Succ(Pos('\', Path)), Length(Path)), Result);
  end;
label
  skip, skip1;
begin
  f := TStringList.Create;
  f.Sorted := True;
  iLevel := 0;
  try
    repeat
      f.Clear;
      Inc(iLevel);
      for i := 0 to Pred(Length(Structure)) do
      begin
        s := GetLevel(iLevel, Structure[i]);
        if s <> '' then
          f.Add(s);
      end;

      for i := 0 to Pred(f.Count) do
      begin
        s := f[i];
        s := Copy(s, Succ(Pos(#1, s)), Length(s));
        sName := Copy(s, 1, Pred(Pos(#1, s)));
        s := Copy(s, Succ(Pos(#1, s)), Length(s));
        ID := Copy(s, 1, Pred(Pos(#1, s)));
        s := Copy(s, Succ(Pos(#1, s)), Length(s));
        PClass := Copy(s, 1, Pred(Pos(#1, s)));
        s := Copy(s, Succ(Pos(#1, s)), Length(s)); //gdy13
        bDefault := Boolean(StrToInt( Copy(s, 1, Pred(Pos(#1, s))) )); //gd13
        Bitmap := graphics.TBitmap(Pointer(StrToInt(Copy(s, Succ(Pos(#1, s)), Length(s)))));

        s := StringReplace(sName, '&', '', [rfReplaceAll]);
        if iLevel = 1 then
        begin
          for j := 0 to Pred(TabControl.Tabs.Count) do
            if StringReplace(TabControl.Tabs[j], '&', '', [rfReplaceAll]) = s then
              goto skip;

          c := FindSecurityElement(FindMenuSecItem(s, Self), s);
          Assert(Assigned(c));
          if c.SecurityState <> stDisabled then
          begin
//begin gdy13
            SetLength( FIndexMap, Length(FIndexMap) + 1 );
            if assigned( Bitmap ) then
              try
                FIndexMap[Length(FIndexMap)-1] := ButtonImageList.AddMasked( Bitmap , Bitmap.TransparentColor )
              except
              end
            else
              FIndexMap[Length(FIndexMap)-1] := -1;
//end gdy13
            TabControl.Tabs.Add(sName);

            SetLength(IDList, Succ(Length(IDList)));
            IDList[High(IDList)].PackageID := ID;
            IDList[High(IDList)].PClass := PClass;
            IDList[High(IDList)].Path := sName;
            IDList[High(IDList)].IsMenuItem := False;
            IDList[High(IDList)].TabIndex := Pred(TabControl.Tabs.Count);
            IDList[High(IDList)].Menu := nil;
          end;
          skip:
        end
        else
        begin
          Assert(Pos('\', s) > 0);
          s := Copy(s, 1, Pred(Pos('\', s)));

          ti := -1;
          for j := 0 to Pred(TabControl.Tabs.Count) do
            if StringReplace(TabControl.Tabs[j], '&', '', [rfReplaceAll]) = s then
            begin
              ti := j;
              Break;
            end;

          if ti >= 0 then
          begin
            pm := nil;
            idi := -1;
            for j := 0 to High(IDList) do
              if not IDList[j].IsMenuItem and (IDList[j].TabIndex = ti) then
              begin
                pm := IDList[j].Menu;
                idi := j;
                Break;
              end;

            Assert(idi >= 0);
            if not Assigned(pm) then
            begin
              pm := TevPopupMenu.Create(Self);
              pm.Images := ButtonImageList;  //gdy13: trick: makes left spaces equal for all menu items
                                             // look at TMenuItem.MeasureItem for details and try to comment this line
                                             // to see effect of not doing it. I don't know what look is better
              IDList[idi].Menu := pm;
            end;

            s := Copy(sName, Succ(Pos('\', sName)), Length(sName));
            mi := GetMenuItem(s, pm.Items);
            for j := 0 to High(IDList) do
              if IDList[j].IsMenuItem and (IDList[j].MenuItem = mi) then
                goto skip1;
            if PClass <> '' then
            begin
              SetLength(IDList, Succ(Length(IDList)));
              IDList[High(IDList)].PackageID := ID;
              IDList[High(IDList)].PClass := PClass;
              IDList[High(IDList)].Path := sName;
              IDList[High(IDList)].IsMenuItem := True;
              IDList[High(IDList)].MenuItem := mi;
              mi.OnClick := MenuOnClick;
              mi.Default := bDefault;
              mi.Bitmap := Bitmap;
              if Assigned(mi.Parent) and
                 (not mi.Parent.Visible) then
                 SetMenuParentTrue(mi.Parent);
            end
            else if mi.Count = 0 then
            begin
              mi.Visible := False;
              if Assigned(mi.Parent) then
              begin
                mi.Parent.Visible := False;
                for j := 0 to Pred(mi.Parent.Count) do
                  if mi.Parent.Items[j].Visible then
                  begin
                    mi.Parent.Visible := True;
                    Break;
                  end;
              end;
            end;
          end;
          skip1:
        end;
      end;
    until f.Count = 0;
  finally
    f.Free;
  end;
end;

procedure TMainPackageForm.btnExitClick(Sender: TObject);
begin
  btnExit.Down := False;
  Close;
end;

procedure TMainPackageForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  TaskList: IisListOfValues;
  QueueStatus: IisListOfValues;
  TInf: IevTaskInfo;
  i: Integer;
begin
  CanClose := CheckCommited;

  if CanClose then
  begin
    if FPasswordChangeClose then
      Exit;

    // Check Task Queue status before closing
    if Mainboard.ModuleRegister.IsProxyModule(mb_TaskQueue) then
    begin
      try
        // Remote queue
        TaskList := mb_TaskQueue.GetUserTaskInfoList;
        for i := 0 to TaskList.Count - 1 do
        begin
          Tinf := IInterface(TaskList[i].Value) as IevTaskInfo;
          if (Tinf.State <> tsFinished) and AnsiSameText(Tinf.User, Context.UserAccount.User) and
             ((Tinf.TaskType = QUEUE_TASK_RUN_REPORT) or (Tinf.TaskType = QUEUE_TASK_PROCESS_PAYROLL)) then
          begin
            CanClose := False;
            Break;
          end;
        end;

        if not CanClose then
          CanClose := (EvMessage('You have tasks in a queue which may be required to print through your workstation. ' +
                       'If you close Evolution, you will have to reprint them manually. Do you still want to exit?', mtWarning, mbOKCancel, mbCancel) = mrOK)
                       and
                       (EvMessage('Are you sure you want to exit and lose your print jobs?', mtConfirmation, mbOKCancel, mbCancel) = mrOK);
      except
        // incase if connection was lost and cannot be restored
        on E: Exception do
          EvErrMessage(E.Message);
      end;
    end

    else
    begin
      // Local queue
      QueueStatus := mb_TaskQueue.GetUserTaskStatus;
      CanClose := (QueueStatus.Value['UserActiveTasks'] + QueueStatus.Value['OtherActiveTasks']) = 0;

      if not CanClose then
        CanClose := (EvMessage('You have tasks in a queue running on this local machine.' +
                               'If you close Evolution, you will terminate execution of all these tasks. Do you still want to exit?',
                                mtWarning, mbOKCancel, mbCancel) = mrOK) and
                    (EvMessage('Are you sure you want to terminate all running tasks?', mtConfirmation, mbOKCancel, mbCancel) = mrOK);
    end;
  end;
end;

procedure TMainPackageForm.TabControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if (TabControl.TabIndex >= 0) and
     (TabControl.TabIndex <> iNewTab) then
  begin
    AllowChange := CheckCommited;
    if AllowChange and (GetCurrentScreenPackage <> nil) then
      AllowChange := GetCurrentScreenPackage.DeactivatePackage;
    if AllowChange then
    begin
      CurrentPackageID := '';
      Caption := MakeCaption;
    end;
  end;
end;

procedure TMainPackageForm.TabControlChange(Sender: TObject);
var
  P: TPoint;
  iTab: Integer;
  R: TRect;
begin
  P := TabControl.ScreenToClient(Mouse.CursorPos);
  iTab := TabControl.IndexOfTabAt(P.x, P.y);
  if (iTab > -1) and (iTab = TabControl.TabIndex) then
    P := Mouse.CursorPos
  else
  begin
    R := TabControl.TabRect(TabControl.TabIndex);
    P := TabControl.ClientToScreen(Point(R.Left + (R.Right - R.Left) div 2, R.Top + (R.Bottom - R.Top) div 2));
  end;
  DoAction(TabControl.TabIndex, P);
end;

procedure TMainPackageForm.TabControlMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  iTab: Integer;
begin
  if Button = mbLeft then
  begin
    iTab := TabControl.IndexOfTabAt(x, y);
    iNewTab := iTab;
    bTabWasChanged := iTab <> TabControl.TabIndex;
    if (iTab > -1) and (iNewTab = TabControl.TabIndex) then
      DoAction(iTab, TabControl.ClientToScreen(Point(x, y)));
  end;
end;

procedure TMainPackageForm.Loaded;
begin
  inherited;
  OldCreateOrder := False;
end;

procedure TMainPackageForm.ActivatePackage(var Message: TMessage);
var
  i, j: Integer;
  s: string;
begin
  if Pos(' - ', PChar(Message.WParam)) > 0 then
    s := Copy(PChar(Message.WParam), 1, Pred(Pos(' - ', PChar(Message.WParam))))
  else
    s := PChar(Message.WParam);
  for i := 0 to Pred(TabControl.Tabs.Count) do
    if StringReplace(TabControl.Tabs[i], '&', '', [rfReplaceAll]) = s then
    begin
      if (GetCurrentScreenPackage <> nil) and (TabControl.TabIndex <> i) then
      begin
        if CheckCommited and GetCurrentScreenPackage.DeactivatePackage then
          CurrentPackageID := ''
        else
          Exit;
        TabControl.TabIndex := i;
      end;
      for j := 0 to High(IDList) do
      begin
        s := StringReplace(IDList[j].Path, '&', '', [rfReplaceAll]);
        s := StringReplace(s, '\', ' - ', [rfReplaceAll]);
        if PChar(Message.WParam) = s then
        begin
          if ActivateScreen(IDList[j].PackageID, IDList[j].PClass, s) then
            Message.Result := 1;
          Exit;
        end;
      end;
    end;
end;

procedure TMainPackageForm.DoAction(TabNumber: Integer; P: TPoint);
var
  j: Integer;
  pm: TevPopupMenu;
  PClass: string;
  Path: string;
  ID: String;
begin
  pm := nil;
  PClass := '';
  ID := '';
  for j := 0 to High(IDList) do
    if not IDList[j].IsMenuItem and (IDList[j].TabIndex = TabNumber) then
    begin
      pm := IDList[j].Menu;
      PClass := IDList[j].PClass;
      Path := IDList[j].Path;
      ID := IDList[j].PackageID;
      Break;
    end;

  if PClass = '' then
  begin
    if bTabWasChanged then
      for j := 0 to Pred(pm.Items.Count) do
        if pm.Items[j].Default then
        begin
          pm.Items[j].Click;
          Exit;
        end;
    Self.PopupMenu := pm;
    try
      pm.Popup(P.x, P.y);
    finally
      Self.PopupMenu := nil;
    end;
  end
  else
    ActivateScreen(ID, PClass, Path);
end;

procedure TMainPackageForm.MenuOnClick(Sender: TObject);
var
  j: Integer;
  PClass: string;
  Path: string;
  ID: String;
begin
  Update;
  PClass := '';
  ID := '';
  for j := 0 to High(IDList) do
    if IDList[j].IsMenuItem and (IDList[j].MenuItem = Sender) then
    begin
      PClass := IDList[j].PClass;
      ID := IDList[j].PackageID;
      Path := IDList[j].Path;
      Break;
    end;
  if PClass <> '' then
    if Assigned(GetClass(PClass)) then
      ActivateScreen(ID, PClass, Path)
    else
    begin
      ShellExecute(0, 'open', PAnsiChar(PClass), nil, nil, SW_SHOWNORMAL);
    end;
end;

function TMainPackageForm.ActivateScreen(ID: String; PClass: string; Path: string): Boolean;
begin
  Result := False;

  if CurrentPackageID <> ID then
  begin
    if (GetCurrentScreenPackage <> nil) and not GetCurrentScreenPackage.DeactivatePackage then
      Exit;

    CurrentPackageID := '';
    Caption := MakeCaption;

    if ID <> '' then
    begin
      FreeAndNil(FDashboard);
      if (Mainboard.ModuleRegister.CreateModuleInstance(StringToGUID(ID)) as IevScreenPackage).ActivatePackage(MainWorkPanel) then
        CurrentPackageID := ID;
    end;
  end;

  if ID = '' then
  begin
    if PClass = 'Dashboard' then
      ShowDashboard;
    Result := True;
    Exit;
  end;

  Path := StringReplace(Path, '&', '', [rfReplaceAll]);
  Path := StringReplace(Path, '\', ' - ', [rfReplaceAll]);
  LastPathOpened := Path;
  if (CurrentPackageID <> '') and (Mainboard.ModuleRegister.CreateModuleInstance(StringToGUID(ID)) as IevScreenPackage).ActivateFrame(GetClass(PClass), Path) then
  begin
    Caption := MakeCaption + ' - ' + Path;

    Realign;  // We need to realign all controls because a screen may have its own size constraints

    Result := True;
  end;
end;

procedure TMainPackageForm.ActivateFrame(var Message: TMessage);
var
  j: Integer;
  ID: String;
  s: String;
begin
  if Message.WParam = -1 then
    ID := CurrentPackageID
  else
    ID := IntToStr(Message.WParam);

  for j := 0 to High(IDList) do
    if ((Message.WParam = -2) or (IDList[j].PackageID = ID)) and (GetClass(IDList[j].PClass) = Pointer(Message.LParam)) then
    begin
      s := StringReplace(IDList[j].Path, '&', '', [rfReplaceAll]);
      s := StringReplace(s, '\', ' - ', [rfReplaceAll]);
      Message.WParam := Integer(PChar(s + #0));
      Message.LParam := 0;
      ActivatePackage(Message);
      Exit;
    end;
end;

function TMainPackageForm.CheckCommited: Boolean;
begin
  if GetCurrentScreenPackage <> nil then
    Result := GetCurrentScreenPackage.AskCommitChanges
  else
    Result := True;
end;

procedure TMainPackageForm.SecElementCallback(const Sender: TComponent);
begin
  if Sender is TevSecElement then
    with TevSecElement(Sender) do
      if SecurityState = stDisabled then
      begin
        Structure[Tag].PClass := '';
      end;
end;

//begin gdy13
constructor TMainPackageForm.Create(Owner: TComponent);
begin
  inherited;
  SetLength( FIndexMap, 0 );
end;

destructor TMainPackageForm.Destroy;
begin
  SetLength( FIndexMap, 0 );
  inherited;
end;

procedure TMainPackageForm.TabControlGetImageIndex(Sender: TObject;
  TabIndex: Integer; var ImageIndex: Integer);
begin
  if TabIndex < Length(FIndexMap) then //paranoia
    ImageIndex := FIndexMap[ TabIndex ]
  else
    ImageIndex := -1;
end;


procedure TMainPackageForm.sbtnTipOfTheDayClick(Sender: TObject);
begin
  FTipOfTheDay.ShowModal;
end;

procedure TMainPackageForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (GetCurrentScreenPackage <> nil) and not GetCurrentScreenPackage.CanClose then
  begin
    Action:= caNone;
    exit;
  end;

  ctx_StartWait;
  try
    ActivateScreen('', '', '');

    if WindowState = wsMaximized then
      mb_AppSettings['RunMaximized'] := 'Y'
    else
      mb_AppSettings['RunMaximized'] := 'N';

    if Assigned(Mainboard.TCPClient) then
    begin
      ctx_UpdateWait('Logging off the server. Please wait...');
      Mainboard.TCPClient.Logoff;
    end;
  finally
    ctx_EndWait;
  end;

  Action := caHide;
end;

function TMainPackageForm.MakeCaption: string;
var
  s: PChar;
  Buffer: array [1..1024] of Char;
  PInfo: Pointer;
  L: Cardinal;
  ver: string;
begin
  ver := '';
  GetMem(s, 2048);
  try
    if (GetModuleFileName(HInstance, s, 2048) <> 0) and
       GetFileVersionInfo(s, 0, 1024, @Buffer) and
       VerQueryValue(@Buffer, '\', PInfo, L) then
    begin
      ver := IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS div $10000) + '.' +
             PadLeft(IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionMS mod $10000),'0',2) + '.' +
             PadLeft(IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS div $10000),'0',2) + '.' +
             PadLeft(IntToStr(VS_FIXEDFILEINFO(PInfo^).dwFileVersionLS mod $10000),'0',2);
    end;
  finally
    FreeMem(s);
  end;

  Result := FCaption + ' ' + ver;

  if not IsStandalone then
    if Mainboard.TCPClient.ConnectedHost <> '' then
      Result := Result + FSysDbVer + ' [' + Mainboard.TCPClient.ConnectedHost + ']';
end;

procedure TMainPackageForm.StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
var
  s: string;

  procedure DrawSessionState;
  var
    S: IevSession;
    bActive: Boolean;
    Pict: TGraphic;
  begin
    Pict := nil;

    if not IsStandalone then
    begin
      S := Mainboard.TCPClient.Session;
      bActive := Assigned(S) and (S.State = ssActive);

      if bActive then
        if S.EncryptionType > etNone then
          Pict := imLockIcon.Picture.Graphic
        else
          Pict := imConnectedIcon.Picture.Graphic
      else
        Pict := imDisconnectedIcon.Picture.Graphic;
    end;

    if Assigned(Pict) then
      StatusBar.Canvas.Draw(Rect.Left, Rect.Top, Pict)
    else
      StatusBar.Canvas.FillRect(Rect);
  end;

begin
  case Panel.ID of
  3: DrawSessionState;
  1: begin
      s := StatusBar.Panels[1].Text;
      if StartsWith(s, '*') then
      begin
        StatusBar.Canvas.Font.Style := StatusBar.Canvas.Font.Style + [fsBold];
        Delete(s, 1, 1);
      end;
      StatusBar.Canvas.TextRect(Rect, Rect.Left + 3, Rect.Bottom - Abs(StatusBar.Canvas.Font.Height) - 3, 'Task Queue: '+ s);
    end;
  end;
end;

procedure TMainPackageForm.FormResize(Sender: TObject);
begin
  if not Assigned(FDashboard) then
    LogoPresenter.AlignLogo;
end;

procedure TMainPackageForm.StatusBarMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) and
     (X >= 0) and (X <= StatusBar.Panels[0].Width) and
     (Y >= 0) and (Y <= StatusBar.Height) then
  begin
    StatusBar.Panels[0].Bevel := pbLowered;
    StatusBar.Update;
  end;
end;

procedure TMainPackageForm.StatusBarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (StatusBar.Panels[0].Bevel = pbLowered) and (Button = mbLeft) then
  begin
    StatusBar.Panels[0].Bevel := pbRaised;
    StatusBar.Update;

    if (X >= 0) and (X <= StatusBar.Panels[0].Width) and (Y >= 0) and (Y <= StatusBar.Height) then
      with TTQueueViewForm.Create(nil) do
        try
          Show('');
        finally
          Free;
        end;
  end;
  // ask user "Mark New Finished as Read?"
  if (Button = mbLeft) and (X > StatusBar.Panels[0].Width) and (X <= StatusBar.Panels[1].Width)
    and (Y >= 0) and (Y <= StatusBar.Height)
    and (Pos('New Finished', StatusBar.Panels[1].Text) > 0) then

    if EvMessage('Would you like to mark "New Finished" tasks as read?', mtConfirmation, [mbYes, mbNo]) = mrYes then
      MarkNewFinishedAsRead;
end;

procedure TMainPackageForm.StatusBarMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if GetCapture = StatusBar.Handle then
  begin
    if not((X >= 0) and (X <= StatusBar.Panels[0].Width) and
         (Y >= 0) and (Y <= StatusBar.Height)) then
      StatusBar.Panels[0].Bevel := pbRaised
    else
      StatusBar.Panels[0].Bevel := pbLowered;
    StatusBar.Update;
  end;
end;


procedure TMainPackageForm.ActivateFrameAs(var Message: TMessage);
var
  F: TFrame;
  Par: PTFrameActivateParamsRec;
begin
  Par := PTFrameActivateParamsRec(Message.WParam);
  Message.WParam := Integer(PChar(Par^.FramePath + #0));
  Message.Result := 0;

  ActivatePackage(Message);

  if Message.Result = 1 then
  begin
    F := (MainWorkPanel.Controls[0] as TFramePackageTmpl).ActiveFrame;
    if Assigned(F) then
      PostMessage(F.Handle, WM_ACTIVATE_FRAME_PARAMS, Integer(Par), 0);
  end;
end;

procedure TMainPackageForm.CreateMenuStructure;
var
  i: Integer;
  ma: TMenuSecAtom;
  st: string;
  tIDList: TAControlItem;
  ModuleList: IInterfaceList;

  procedure AddScreenInfoFromModule(const aModule: IevModuleDescriptor);
  var
    i: Integer;
    l: Integer;
    ModuleInst: IevScreenPackage;
  begin
    if StartsWith(aModule.ModuleName, 'Screens', True) then
    begin
      ModuleInst := Mainboard.ModuleRegister.CreateModuleInstance(aModule.InstanceType) as IevScreenPackage;

      if not ModuleInst.InitPackage then
        Exit;

      l := Length(Structure);
      if ModuleInst.PackageCaption <> '' then
      begin
        SetLength(Structure, Succ(Length(Structure)));
        Structure[High(Structure)].Path := '\' + ModuleInst.PackageCaption;
        if ModuleInst.PackageSortPosition <> '' then
          Structure[High(Structure)].Position := ModuleInst.PackageSortPosition
        else
          Structure[High(Structure)].Position := #255;
        Structure[High(Structure)].PClass := '';
        Structure[High(Structure)].Default := False;
        Structure[High(Structure)].Bitmap := ModuleInst.PackageBitmap; //gdy13
      end;
      ModuleInst.UserPackageStructure;

      for i := l to High(Structure)  do
        Structure[i].PackageID := GUIDtoString(aModule.InstanceType);
    end;
  end;

begin
  SetLength(tIDList, 0);
  for i := 0 to High(IDList) do
    if not IDList[i].IsMenuItem then
    begin
      FreeAndNil(IDList[i].Menu);
      SetLength(tIDList, Succ(Length(tIDList)));
      tIDList[High(tIDList)] := IDList[i];
    end;
  IDList := tIDList;

  SetLength(Structure, 0);

  ModuleList := Mainboard.ModuleRegister.GetModules;
  for i := 0 to ModuleList.Count - 1 do
    AddScreenInfoFromModule(ModuleList[i] as IevModuleDescriptor);

  for i := Low(Structure) to High(Structure) do
  begin
    st := Copy(Structure[i].Path, 2, Length(Structure[i].Path));
    if Pos('\', st) > 0 then
      st := Copy(st, 1, Pred(Pos('\', st)));
    if Pos('|', st) > 0 then
      st := Copy(st, 1, Pred(Pos('|', st)));
    st := StringReplace(st, '&', '', [rfReplaceAll]);
    ma := FindMenuSecItem(st, Self);

    if (Structure[i].PClass <> '') and (GetClass(Structure[i].PClass) <> nil) then
      with TevSecElement.Create(ma) do
      begin
        AtomComponent := ma;
        ElementType := otScreen;
        ElementTag := Structure[i].PClass;
        Tag := i;
        OnSetState := SecElementCallback;
      end;
  end;

  ctx_EvolutionGUI.ApplySecurity(Self);
  CreateMenus;
end;

procedure TMainPackageForm.RecreateMenus(var Message: TMessage);
begin
  CreateMenuStructure;
end;

procedure TMainPackageForm.UpdateTaskQueueStatus;
begin
  if HandleAllocated then
    PostMessage(Handle, CM_REFRESH_QUEUE_STATUS, 0, 0);
end;

procedure TMainPackageForm.CMRefreshQueueStatus(var Message: TMessage);
var
  TaskQueueStatus: IisListOfValues;
  Total: Integer;
begin
  TaskQueueStatus := mb_TaskQueue.GetUserTaskStatus;


  Total := Integer(TaskQueueStatus.Value['UserActiveTasks']) + Integer(TaskQueueStatus.Value['UserFinishedTasks']);
  StatusBar.Panels[1].Text := 'Total: ' + IntToStr(Total) + '  Finished: ' + String(TaskQueueStatus.Value['UserFinishedTasks']);
  if TaskQueueStatus.Value['UserNotSeenTasks'] > 0 then
    StatusBar.Panels[1].Text := '*' + ' New Finished: ' + IntToStr(TaskQueueStatus.Value['UserNotSeenTasks']) + '  ' + StatusBar.Panels[1].Text;
end;

procedure TMainPackageForm.CMShowTips(var Message: TMessage);
begin
  FTipOfTheDay.ShowModal;
end;

procedure TMainPackageForm.NotifyGlobalFlagChanges(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
  if HandleAllocated then
  begin
    AFlagInfo._AddRef;
    PostMessage(Handle, CM_GLOBAL_FLAG_CHANGE, Integer(Pointer(AFlagInfo)), Byte(AOperation));
  end;
end;

function TMainPackageForm.ActiveScreen: TFrameEntry;
begin
  if (MainWorkPanel.ControlCount > 0) and (MainWorkPanel.Controls[0] is TFramePackageTmpl) then
    Result := TFramePackageTmpl(MainWorkPanel.Controls[0]).ActiveFrame
  else
    Result := nil;  
end;

procedure TMainPackageForm.CMGlobalFlagChange(var Message: TMessage);
var
  F: IevGlobalFlagInfo;
  O: Char;
begin
  F := IevGlobalFlagInfo(Pointer(Message.WParam));
  O := Char(Message.LParam);
  if ActiveScreen <> nil then
    ActiveScreen.OnGlobalFlagChange(F, O);
  F._Release;
end;

procedure TMainPackageForm.WaitObjectOperation(const AOper: TevWaitObjectOperType; const AText: String;
  const AProgress, AProgressMax: Integer);
var
  OperInfo: PevWaitObjectOperInfo;
begin
  if HandleAllocated then
  begin
    New(OperInfo);
    OperInfo.Oper := AOper;
    OperInfo.Text := AText;
    OperInfo.Progress := AProgress;
    OperInfo.Progress := AProgressMax;    
    PostMessage(Handle, CM_WAIT_OBJECT_OPER, Integer(Pointer(OperInfo)), 0);
  end;
end;

procedure TMainPackageForm.CMWaitObjectOper(var Message: TMessage);
var
  OperInfo: PevWaitObjectOperInfo;
begin
  OperInfo := PevWaitObjectOperInfo(Message.wParam);
  try
    case OperInfo.Oper of
    woStart:   Context.Callbacks.StartWait(OperInfo.Text, OperInfo.ProgressMax);
    woUpdate:  Context.Callbacks.UpdateWait(OperInfo.Text, OperInfo.Progress, OperInfo.ProgressMax);
    woEnd:     Context.Callbacks.EndWait;
    end;
  finally
    Dispose(OperInfo);
  end;
end;

procedure TMainPackageForm.CheckSessionStatus;
begin
  PostMessage(Handle, CM_REPAINT_STATUS_BAR, 0, 0);
end;

procedure TMainPackageForm.CMRepaintStatusBar(var Message: TMessage);
var
  s: String;
begin
  s := Caption;
  GetNextStrValue(s, ' - ');
  Caption := MakeCaption;
  if s <> '' then
    Caption := Caption + ' - ' + s;
  StatusBar.Invalidate;
  StatusBar.Update;  
end;

procedure TMainPackageForm.sbtnDumpTrafficClick(Sender: TObject);
var
  Dlg: TSaveDialog;
begin
  if not (Mainboard.TCPClient as IevTrafficDumping).GetDumpTraffic then
  begin
    if AreYouSure('Do you want to START client requests recording?') then
    begin
      Dlg := TSaveDialog.Create(nil);
      try
        Dlg.Title := 'New Client Requests file';
        Dlg.Filter := 'DAT files (*.dat)|*.dat';
        Dlg.FileName := (Mainboard.TCPClient as IevTrafficDumping).GetDumpFileName;
        if Dlg.FileName = '' then
          Dlg.FileName := 'EvClientRequests.dat';
        if Dlg.Execute then
        begin
          (Mainboard.TCPClient as IevTrafficDumping).StartDumping(Dlg.FileName);
          EvMessage('Client requests recording has started!');
        end;
      finally
        Dlg.Free;
      end;
    end;
  end

  else
  begin
    if AreYouSure('Do you want to STOP client requests recording?') then
    begin
      (Mainboard.TCPClient as IevTrafficDumping).StopDumping;
      EvMessage(Format('Client requests recording has stopped.'#13'Record file: %s',
                       [(Mainboard.TCPClient as IevTrafficDumping).GetDumpFileName]));
    end;
  end;

  sbtnDumpTraffic.Down := (Mainboard.TCPClient as IevTrafficDumping).GetDumpTraffic;

  if sbtnDumpTraffic.Down then
    sbtnDumpTraffic.Hint := Format('STOP recording (%s)', [(Mainboard.TCPClient as IevTrafficDumping).GetDumpFileName])
  else
    sbtnDumpTraffic.Hint := 'Client requests recorder';
end;

procedure TMainPackageForm.ShowPopupMessage(const aText, aFromUser, aToUser: String);
var
  Par: IisListOfValues;
begin
  if HandleAllocated then
  begin
    Par := TisListOfValues.Create;
    Par.AddValue('Text', aText);
    Par.AddValue('FromUser', aFromUser);
    Par.AddValue('ToUser', aToUser);
    Par._AddRef;
    PostMessage(Handle, CM_SHOW_POPUP_MESSAGE, Integer(Pointer(Par)), 0);
  end;
end;

procedure TMainPackageForm.CMShowPopupMessage(var Message: TMessage);
var
  Par: IisListOfValues;
  c, t: String;
begin
  Par := IisListOfValues(Pointer(Message.WParam));
  try
    if AnsiSameText(Par.Value['ToUser'],
         EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain)) then
      c := 'Message from ' + Par.Value['FromUser']
    else
      c := 'Broadcast message from ' + Par.Value['FromUser'];
    t := Par.Value['Text'];
  finally
    Par._Release;
  end;

  Application.MessageBox(PAnsiChar(t), PAnsiChar(c));
end;

procedure TMainPackageForm.MarkNewFinishedAsRead;
var
  TaskList: IisListOfValues;
  TaskInf: IevTaskInfo;
  i: integer;
begin
  TaskList := mb_TaskQueue.GetUserTaskInfoList;
  for i := 0 to TaskList.Count - 1 do
  begin
    TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
    if AnsiSameText(TaskInf.User, Context.UserAccount.User) and (TaskInf.State = tsFinished) and not TaskInf.SeenByUser then
      mb_TaskQueue.ModifyTaskProperty( TaskInf.ID, 'SeenByUser', True );
  end;    
end;

procedure TMainPackageForm.DispatchUserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
var
  Par: IisListOfValues;
begin
  if HandleAllocated then
  begin
    Par := TisListOfValues.Create;
    Par.AddValue('AEventID', AEventID);
    Par.AddValue('ASubject', ASubject);
    Par.AddValue('AScheduledTime', AScheduledTime);
    Par._AddRef;
    PostMessage(Handle, CM_DISPATCH_USER_SCHEDULER_EVENT, Integer(Pointer(Par)), 0);
  end;
end;

procedure TMainPackageForm.CMDispatchUserSchedulerEvent(var Message: TMessage);
var
  Par: IisListOfValues;
  UserSchedScreens: IevUserSchedulerScreens;
begin
  Par := IisListOfValues(Pointer(Message.WParam));
  try
    UserSchedScreens := Context.GetScreenPackage(IevUserSchedulerScreens) as IevUserSchedulerScreens;
    if Assigned(UserSchedScreens) then
      UserSchedScreens.ShowRemindersWindow(Par.Value['AEventID']);
  finally
    Par._Release;
  end;
end;

procedure TMainPackageForm.btnReminderWndClick(Sender: TObject);
var
  UserSchedScreens: IevUserSchedulerScreens;
begin
  UserSchedScreens := Context.GetScreenPackage(IevUserSchedulerScreens) as IevUserSchedulerScreens;
  if Assigned(UserSchedScreens) then
    UserSchedScreens.ShowRemindersWindow('*');
end;

procedure TMainPackageForm.PasswordChangeClose;
begin
  FPasswordChangeClose := True;
  Close;
end;

function TMainPackageForm.GetCurrentScreenPackage: IevScreenPackage;
begin
  if CurrentPackageID <> '' then
    Result := Mainboard.ModuleRegister.CreateModuleInstance(StringToGUID(CurrentPackageID)) as IevScreenPackage
  else
    Result := nil;  
end;

procedure TMainPackageForm.Initialize;
var
  DeskRect : TRect;
  UserSchedScreens: IevUserSchedulerScreens;
begin
  if FileExists(AppDir + 'Remotehelp.chm') then
    Application.HelpFile := AppDir + 'Remotehelp.chm'
  else
    Application.HelpFile := AppDir + 'Evolhelp.chm';

  DM_SERVICE_BUREAU.SB.Open;
  mb_AppSettings.AsBoolean['Settings\SendErrorScreen'] := DM_SERVICE_BUREAU.SB.ERROR_SCREEN.Value = 'Y';

  DM_CLIENT.CL.Open; // This will check version of CL_BASE

  FTipOfTheDay := TTipOfTheDay.Create(Self);

  AddScreenShotHotKey(Self);

  SystemParametersInfo(SPI_GETWORKAREA, 0, @DeskRect, 0);
  Left := Max((DeskRect.Right - DeskRect.Left - Width) div 2 + DeskRect.Left, 0);
  Top := Max(((DeskRect.Bottom - DeskRect.Top - Height) div 2) + DeskRect.Top, 0);

  bTabWasChanged := False;
  CurrentPackageID := '';

  StatusBar.Panels[2].Text := ' UserID: ' + ctx_EvolutionGUI.GUIContext.UserAccount.User;

  UpdateTaskQueueStatus;
  Application.HintHidePause := 1500;


  Height := 1;

  if ClientHeight < 546 then
    Constraints.MinHeight := Constraints.MinHeight + 546 - ClientHeight;

  sbtnDumpTraffic.AllowAllUp := True;

  FCaption := Application.Title;
  FSysDbVer := GetSysDbVersion;

  ShowDashboard;
  Caption := MakeCaption;
  CreateMenuStructure;

  if FTipOfTheDay.ShowOnStartup then
    PostMessage(Handle, CM_SHOW_TIPS, 0, 0);

  TabControl.TabIndex := -1;
  sbtnDumpTraffic.Visible := IsDebugMode;

  if mb_AppSettings['RunMaximized'] = 'Y' then
     WindowState := wsMaximized;

  if Mainboard.ModuleRegister.GetModuleInfo(IevUserSchedulerScreens) <> nil then
  begin
    UserSchedScreens := Context.GetScreenPackage(IevUserSchedulerScreens) as IevUserSchedulerScreens;
    if Assigned(UserSchedScreens) then
    begin
      btnReminderWnd.Enabled := true;
      Context.UserScheduler.Active := True;
    end
    else
      btnReminderWnd.Enabled := false;
  end
  else
    btnReminderWnd.Visible := false;

  btnDashboard.Visible := ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_TO_DASHBOARD') = stEnabled;

  //GUI 2.0 NEW STANDARD SIZE IS 1024X768 VS. 800X600.
  //Don't fire if wsMaximized was read from the registry.
  if Self.WindowState <> wsMaximized then
  begin
    Self.Height := 768;
    Self.Width := 1024;
  end;
end;

function TMainPackageForm.GetSysDbVersion: string;
var
  Q: IevQuery;

  function PadLeftZeros(const aVer: string): string;
  var
    i, l: integer;
  begin
    Result := '';
    i := Length(aVer);
    l := 0;
    while i > 0 do
    begin
      if (aVer[i] = '.') then
      begin
        if (l < 2) then
          Result := '0' + Result;
        l := 0;
      end
      else
        Inc(l);
      Result := aVer[i] + Result;
      Dec(i);
    end;
    Result := '  ' + Result;
  end;
begin
  // System Database Version
  Q := TevQuery.Create('SELECT t.tag FROM sy_storage t WHERE t.tag LIKE ''VER %''');
  Result := Q.Result.Fields[0].AsString;
  System.Delete(Result, 1, 4);
  Result := PadLeftZeros( Result );
end;

procedure TMainPackageForm.ShowDashboard;
begin
  if ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_TO_DASHBOARD') <> stEnabled then
  begin
    btnDashboard.Hint := 'Splash Screen';
    LogoPresenter.ShowLogo(MainWorkPanel);
  end
  else
  begin
    btnDashboard.Hint := 'Dashboard';

    if not Assigned(FDashboard) then
      FDashboard := TevDashboard.Create(Self);

    FDashboard.Align := alClient;
    FDashboard.Parent := MainWorkPanel;
  end;
end;

procedure TMainPackageForm.btnDashboardClick(Sender: TObject);
begin
  ActivateScreen('', 'Dashboard', '');
end;

function TMainPackageForm.Destroying: Boolean;
begin
  Result := csDestroying in ComponentState;
end;

procedure TMainPackageForm.Notify(const AMessage: String; const AParams: IisListOfValues);
begin
end;

end.


