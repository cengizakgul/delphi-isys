// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQueueViewForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls,  ExtCtrls, DBCtrls, Db, IsBaseClasses,
  Grids, Wwdbigrd, Wwdbgrid, Buttons, StdCtrls, ISBasicClasses, EvConsts,
  EvTaskResultFrame, Mask, wwdbedit, ActnList, Variants, EvStreamUtils,
  kbmMemTable, ISKbmMemDataSet, ISUtils, EvUtils, ISDataAccessComponents, EvTypes,
  EvDataAccessComponents, Wwdatsrc, EvCommonInterfaces, EvContext, EvMainboard,
  EvTaskViewerFrm,SQueueViewFormChoosePr, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

const
  CM_CURRENT_TASK_CHANGE = WM_USER + 1;

type
  TTQueueViewForm = class(TForm)
    Tabs: TevTabControl;
    TopListPanel: TevPanel;
    evSplitter1: TevSplitter;
    BriefInfoPanel: TevPanel;
    DetailPanel: TevPanel;
    dsTaskList: TevClientDataSet;
    DataSource: TevDataSource;
    dsTaskListClassName: TStringField;
    dsTaskListCaption: TStringField;
    dsTaskListStatus: TStringField;
    dsTaskListSeen: TBooleanField;
    dsTaskListTimeStamp: TDateTimeField;
    dsTaskListFinished: TBooleanField;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evDBText3: TevDBText;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    dsTaskListPriority: TIntegerField;
    dsTaskListRequests: TMemoField;
    dsTaskListUseEmail: TBooleanField;
    dsTaskListEMail: TStringField;
    evLabel5: TevLabel;
    evDBText5: TevDBText;
    dsTaskListMessage: TStringField;
    dsTaskListUserName: TStringField;
    tListFilter: TevTabControl;
    cbUseEmail: TevDBCheckBox;
    sbSetEmail: TevSpeedButton;
    dsTaskListNbr: TIntegerField;
    dsTaskListID: TStringField;
    TaskViewer: TTaskViewer;
    Timer: TTimer;
    dbgTasks: TevDBGrid;
    dsTaskListNewTask: TBooleanField;
    dsTaskListWaitForRes: TBooleanField;
    dsTaskListExecTask: TBooleanField;
    dsTaskListFinishedSucs: TBooleanField;
    dsTaskListFinishedExcep: TBooleanField;
    dsTaskListFinishedWarn: TBooleanField;
    dsTaskListFailedExcep: TBooleanField;
    StGrid: TStringGrid;
    Panel1: TPanel;
    btnNavDelete: TevSpeedButton;
    btnNavLast: TevSpeedButton;
    btnNavNext: TevSpeedButton;
    btnNavPrevious: TevSpeedButton;
    btnNavFirst: TevSpeedButton;
    evSpeedButton1: TevSpeedButton;
    dsTaskListUserNameHide: TStringField;
    chbShowTaskDetails: TevCheckBox;
    procedure TabsChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnNavDeleteClick(Sender: TObject);
    procedure btnNavFirstClick(Sender: TObject);
    procedure btnNavPreviousClick(Sender: TObject);
    procedure btnNavNextClick(Sender: TObject);
    procedure btnNavLastClick(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure sbUpdateEmailInfoClick(Sender: TObject);
    procedure cbUseEmailExit(Sender: TObject);
    procedure edEmailKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tListFilterChange(Sender: TObject);
    procedure sbSetEmailClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TabsChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbgTasksDblClick(Sender: TObject);
    procedure dbgTasksCalcCellColors(Sender: TObject; Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure TimerTimer(Sender: TObject);
    procedure StGridDblClick(Sender: TObject);
    procedure StGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure dsTaskListAfterDelete(DataSet: TDataSet);
    procedure DetailPanelDblClick(Sender: TObject);
    procedure chbShowTaskDetailsClick(Sender: TObject);
  private
    FListHeight: Integer;
    FTaskCache: IisStringList;

    FTotalTasks:integer;
    FNewTasks:integer;
    FWaitingTasks:integer;
    FExecTasks:integer;
    FFinishedTasks:integer;
    FFinSucTasks:integer;
    FFinExTasks:integer;
    FFinWarnTasks:integer;
    FFailedTasks:integer;

    FStatFilter:string;
    FPriorityList: TStringList;

    procedure CMCurrentTaskChange(var Message: TMessage); message CM_CURRENT_TASK_CHANGE;
    procedure Load;
    procedure DispatchShortCut(var Msg: TWMKey; var Handled: Boolean);
    function  TaskListIsEmpty: Boolean;
    procedure ShowTaskDetails;

    procedure UpdateStatFilter;
    procedure UpdateStatistics;
    procedure CalcStatistics;
    procedure ApplyStatistics;

    procedure ClearStatistics;
    procedure SetdsTaskListFilterWithFStatFilter;

    procedure DoOnShow;
  public
    procedure Show(const ATaskID: TisGUID); reintroduce;
  end;

implementation

uses isSettings;


{$R *.DFM}
type
 TStatType = class(TObject)
   private
    selected:boolean;
    FieldName:string;
    FieldValue:string;
 end;

procedure TTQueueViewForm.TabsChange(Sender: TObject);
begin
  if Tabs.TabIndex <> 0 then
  begin
    TopListPanel.Align := alTop;
    TopListPanel.Height := FListHeight;
    PostMessage(Handle, CM_CURRENT_TASK_CHANGE, 0, 0);
  end;
  TopListPanel.Visible := Tabs.TabIndex in [0, 1];
  BriefInfoPanel.Visible := Tabs.TabIndex in [1, 2];
  evSplitter1.Visible := Tabs.TabIndex in [1];
  DetailPanel.Visible := Tabs.TabIndex in [1, 2, 3];
  if Tabs.TabIndex = 0 then
    TopListPanel.Align := alClient;

  TopListPanel.Top := 0;
  evSplitter1.Top := FListHeight;
  BriefInfoPanel.Top := evSplitter1.Top + evSplitter1.Height;
end;

procedure TTQueueViewForm.DataSourceDataChange(Sender: TObject; Field: TField);

  procedure UpdateEmailCaption;
  begin
    cbUseEmail.Caption := dsTaskList['Email'];
    if cbUseEmail.Caption = '' then
      cbUseEmail.Caption := '* Not defined *';
    cbUseEmail.Caption := 'Send Updates to '+ cbUseEmail.Caption;
  end;

begin
  if dsTaskList.Tag <> 0 then
    Exit;

  if not TaskListIsEmpty and Assigned(Field) and (Field.FullName = 'Priority') then
  begin
    mb_TaskQueue.ModifyTaskProperty(dsTaskList['Id'], 'Priority', Field.AsInteger);
    tListFilterChange(Sender);
  end

  else if not TaskListIsEmpty and Assigned(Field) and (Field.FullName = 'EMail') then
  begin
    mb_TaskQueue.ModifyTaskProperty(dsTaskList['Id'], 'NotificationEmail', Field.AsString);
    UpdateEmailCaption;
  end

  else if not TaskListIsEmpty and Assigned(Field) and (Field.FullName = 'UseEmail') then
  begin
    mb_TaskQueue.ModifyTaskProperty(dsTaskList['Id'], 'SendEmailNotification', Field.AsBoolean);
    UpdateEmailCaption;
  end

  else if not Assigned(Field) then
  begin
    if TaskListIsEmpty then
       btnNavDelete.Enabled := false
    else
      if (dsTaskList['Status'] = 'Executing') then
        btnNavDelete.Enabled := Context.UserAccount.AccountType = uatSBAdmin
      else
        btnNavDelete.Enabled := true;
    btnNavLast.Enabled := not TaskListIsEmpty and not dsTaskList.Eof;
    btnNavFirst.Enabled := not TaskListIsEmpty and not dsTaskList.Bof;
    btnNavNext.Enabled := btnNavLast.Enabled;
    btnNavPrevious.Enabled := btnNavFirst.Enabled;

    if not TaskListIsEmpty then
    begin
      if dsTaskList['Finished'] then
        evDBText3.Font.Style := evDBText3.Font.Style + [fsBold]
      else
        evDBText3.Font.Style := evDBText3.Font.Style;
      UpdateEmailCaption;
    end;

    PostMessage(Handle, CM_CURRENT_TASK_CHANGE, 0, 0);
  end;
end;

procedure TTQueueViewForm.btnNavDeleteClick(Sender: TObject);
var
  i: Integer;
begin
  if not TaskListIsEmpty then
  begin
    if EvMessage('Do you want delete selected task?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
     mb_TaskQueue.RemoveTask(dsTaskList['Id']);
     i := FTaskCache.IndexOf(dsTaskList['Id']);
     if i <> - 1 then
       FTaskCache.Delete(i);
     dsTaskList.Delete;
    end;
  end;
end;

procedure TTQueueViewForm.Load;
var
  TaskList: IisListOfValues;
  TaskInf: IevTaskInfo;
  FSavedTaskId: String;
  i: Integer;
  function GetTaskState: String;
  begin
    case TaskInf.State of
      tsInactive:  Result := 'Queued';
      tsWaiting:   Result := 'Waiting for resources';
      tsExecuting: Result := 'Executing';
      tsFinished:  Result := TaskInf.ProgressText;
    end;
  end;

  function GetTaskProgressText: String;
  begin
    if TaskInf.State = tsFinished then
      Result := ''
    else
      Result := TaskInf.ProgressText;
  end;
  function GetStatFieldValue(fn:string):variant;
  begin
    result := false;
    if fn = 'NewTask' then
    begin
       result := Iff(TaskInf.State = tsInactive,true,false);
    end else
    if fn ='WaitForResTask' then
    begin
       result := Iff(TaskInf.State = tsWaiting,true,false);
    end else
    if fn = 'ExecTask' then
    begin
       result := Iff(TaskInf.State = tsExecuting,true,false);
    end else
    if fn = 'FinishedSucs' then
    begin
       result := Iff((TaskInf.State = tsFinished) and (pos('SUCCESS',UpperCase(TaskInf.ProgressText))>0),true,false);
    end else
    if fn ='FinishedExcep' then
    begin
       result := Iff((TaskInf.State = tsFinished) and (pos('EXCEPTION',UpperCase(TaskInf.ProgressText))>0),true,false);
    end else
    if fn ='FinishedWarn' then
    begin
      result := Iff((TaskInf.State = tsFinished) and (pos('WARNING',UpperCase(TaskInf.ProgressText))>0),true,false);
    end else
    if fn = 'FailedExcep' then
    begin
    end;
  end;
var
 sUserNameHide, sUserName : String;

begin
  ctx_StartWait;
  try
    // Check security
    if ctx_AccountRights.Functions.GetState('ALLOW_ADMIN_QUEUE') = stEnabled then
    begin
      dbgTasks.ReadOnly := False;
      tListFilter.Visible := True;
      dbgTasks.Parent := tListFilter;
    end
    else
    begin
      dbgTasks.ReadOnly := True;
      dbgTasks.Parent := tListFilter.Parent;
      tListFilter.Visible := False;
    end;
    // Ask for task list
    TaskList := mb_TaskQueue.GetUserTaskInfoList;

    // Fill up dataset
    dsTaskList.DisableControls;
    try
      if not dsTaskList.Active then
      begin
        FSavedTaskId := '';
        dsTaskList.CreateDataSet;
      end
      else
      begin
        // store active record
        FSavedTaskId := dsTaskList.FieldByName('Id').AsString;
        dsTaskList.EmptyDataSet;
        dsTasklist.Filter   := '';
        dsTasklist.Filtered := False;
      end;

      ClearStatistics;
      sUserName := LowerCase(Context.UserAccount.User);
      for i := 0 to TaskList.Count - 1 do
      begin
        TaskInf := IInterface(TaskList[i].Value) as IevTaskInfo;
        with TaskInf do
        begin
           sUserNameHide := LowerCase(User);
           dsTaskList.AppendRecord([ID, Nbr, TaskType, Caption, GetTaskState, State = tsFinished,  SeenByUser,
                                   LastUpdate, Priority, 'RequestsCommaText', SendEmailNotification, NotificationEmail,
                                   GetTaskProgressText, User,
                                   GetStatFieldValue('NewTask'),
                                   GetStatFieldValue('WaitForResTask'),
                                   GetStatFieldValue('ExecTask'),
                                   GetStatFieldValue('FinishedSucs'),
                                   GetStatFieldValue('FinishedExcep'),
                                   GetStatFieldValue('FinishedWarn'),
                                   GetStatFieldValue('FailedExcep'),
                                   sUserNameHide
                                   ]);

            if (sUserName = sUserNameHide) or
                 (tListFilter.TabIndex = 1) then UpdateStatistics();
        end;
      end;

      // Clean up cache
      for i := FTaskCache.Count - 1 downto 0 do
         if Supports(FTaskCache.Objects[i], IisListOfValues) then
           FTaskCache.Delete(i);

    finally
      UpdateStatFilter;
      SetdsTaskListFilterWithFStatFilter;
      ApplyStatistics;

      // restore active record
      if not VarIsNull(FSavedTaskId) then
        dsTaskList.Locate('Id', FSavedTaskId, []);

      dsTaskList.EnableControls;
    end;

  finally
    ctx_EndWait;
  end;
end;

Procedure TTQueueViewForm.SetdsTaskListFilterWithFStatFilter;
var
 sUserName : String;
Begin
  if tListFilter.TabIndex = 1 then
    dsTaskList.Filter := FStatFilter
  else
  begin
    sUserName :=LowerCase(Context.UserAccount.User);
    dsTaskList.Filter := 'UserNameHide = '+ QuotedStr(sUserName);
    if FStatFilter <> '' then
      dsTaskList.Filter := dsTaskList.Filter + ' and '+ FStatFilter;
  end;
  if Length(Trim(dsTaskList.Filter)) > 0 then
       dsTaskList.Filtered := True
  else
       dsTaskList.Filtered := False;
End;


procedure TTQueueViewForm.btnNavFirstClick(Sender: TObject);
begin
  dsTaskList.First;
end;

procedure TTQueueViewForm.btnNavPreviousClick(Sender: TObject);
begin
  dsTaskList.Prior;
end;

procedure TTQueueViewForm.btnNavNextClick(Sender: TObject);
begin
  dsTaskList.Next;
end;

procedure TTQueueViewForm.btnNavLastClick(Sender: TObject);
begin
  dsTaskList.Last;
end;

procedure TTQueueViewForm.evSpeedButton1Click(Sender: TObject);
begin
  Load;
end;

procedure TTQueueViewForm.sbUpdateEmailInfoClick(Sender: TObject);
begin
  dsTaskList.UpdateRecord;
end;

procedure TTQueueViewForm.cbUseEmailExit(Sender: TObject);
begin
  if dsTaskList.State = dsEdit then
    dsTaskList.UpdateRecord;
end;

procedure TTQueueViewForm.edEmailKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    if dsTaskList.State = dsEdit then
      dsTaskList.UpdateRecord;
end;

procedure TTQueueViewForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

procedure TTQueueViewForm.tListFilterChange(Sender: TObject);
var
 sUserName : String;
begin
  if tListFilter.TabIndex = 1 then
  begin
    dsTaskList.Filter := '';
    dsTaskList.Filtered := False;
  end
  else
  begin
    dsTaskList.Filtered := True;
    sUserName :=LowerCase(Context.UserAccount.User);
    dsTaskList.Filter := 'UserNameHide = '+ QuotedStr(sUserName);
  end;
  CalcStatistics;
  if FStatFilter <> '' then
  begin
       if tListFilter.TabIndex <> 1  then
         dsTaskList.Filter := dsTaskList.Filter + ' and '+ FStatFilter
       else
         dsTaskList.Filter := FStatFilter;
       dsTaskList.Filtered := True;
  end;
end;

procedure TTQueueViewForm.sbSetEmailClick(Sender: TObject);
var
  s: string;
begin
  s := InputBox('Update Notification Settings', 'Email Address:', ConvertNull(dsTaskList['Email'], ''));
  if s <> ConvertNull(dsTaskList['Email'], '') then
  begin
    dsTaskList.Edit;
    dsTaskList['Email'] := s;
    dsTaskList.UpdateRecord;
  end;
end;

procedure TTQueueViewForm.FormCreate(Sender: TObject);
var
  i:integer;
begin
  chbShowTaskDetails.Tag := 1;
  chbShowTaskDetails.Checked := mb_AppSettings.GetValue('Settings\AutoShowTaskDetails', '1') = '1';
  chbShowTaskDetails.Tag := 0;  

  AddScreenShotHotKey(Self);
  FListHeight := TopListPanel.Height;
  OnShortCut := DispatchShortCut;
  FTaskCache := TisStringList.CreateAsIndex;
  for i := 0 to 4 do
  begin
     StGrid.Objects[0,i] := TStatType.Create;
     StGrid.Objects[2,i] := TStatType.Create;
  end;
  StGrid.Objects[1,4] := TStatType.Create;

  (StGrid.Objects[0,0] as TStatType).FieldName := 'NewTask';
  (StGrid.Objects[0,1] as TStatType).FieldName := 'WaitForResTask';
  (StGrid.Objects[0,2] as TStatType).FieldName := 'ExecTask';
  (StGrid.Objects[0,3] as TStatType).FieldName := 'FailedExcep';
  (StGrid.Objects[0,4] as TStatType).FieldName := 'Priority';


  (StGrid.Objects[2,0] as TStatType).FieldName := 'Finished';
  (StGrid.Objects[2,1] as TStatType).FieldName := 'FinishedSucs';
  (StGrid.Objects[2,2] as TStatType).FieldName := 'FinishedExcep';
  (StGrid.Objects[2,3] as TStatType).FieldName := 'FinishedWarn';

   FPriorityList := TStringList.Create;
   FPriorityList.Sorted := true;
   FPriorityList.Duplicates := dupIgnore;

end;

procedure TTQueueViewForm.TabsChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if Tabs.TabIndex <> 0 then
    FListHeight := TopListPanel.Height;
end;

procedure TTQueueViewForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  i: Integer;  
begin
  if dsTaskList.State = dsEdit then
    dsTaskList.UpdateRecord;

  for i := 0 to 4 do
  begin
     (StGrid.Objects[0,i] as TStatType).Free;
     (StGrid.Objects[2,i] as TStatType).Free;
  end;
  (StGrid.Objects[1,4] as TStatType).Free;

  FPriorityList.Free;
end;

procedure TTQueueViewForm.dbgTasksDblClick(Sender: TObject);
begin
  if Tabs.TabIndex = 0 then
  begin
    Tabs.TabIndex := 1;
    TabsChange(Sender);
  end
  else
    ShowTaskDetails;
end;

procedure TTQueueViewForm.dbgTasksCalcCellColors(Sender: TObject; Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  if Assigned(Field) then
  begin
    if AnsiSameText(Field.FieldName, 'Nbr') and dsTaskList.FieldByName('Finished').AsBoolean and not dsTaskList.FieldByName('Seen').AsBoolean then
      ABrush.Color := ShadeColor(ABrush.Color, 5);

    if not (gdFocused in State) and AnsiSameText(Field.FieldName, 'Status') then
      if dsTaskList.FieldByName('FinishedExcep').AsBoolean then
        AFont.Color := clRed
      else if dsTaskList.FieldByName('FinishedWarn').AsBoolean then
        AFont.Color := clBlue
      else if dsTaskList.FieldByName('ExecTask').AsBoolean then
        AFont.Color := clGreen;
  end;
end;

procedure TTQueueViewForm.DispatchShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
//todo?  TaskViewer.OnShortCut(Msg, Handled);
end;

procedure TTQueueViewForm.CMCurrentTaskChange(var Message: TMessage);
begin
  Timer.Tag := Message.WParam;
  Timer.Enabled := False;
  Timer.Enabled := True;
end;

procedure TTQueueViewForm.TimerTimer(Sender: TObject);
begin
  Timer.Enabled := False;
  try
    if (Tabs.TabIndex <> 0) and (chbShowTaskDetails.Checked or (Timer.Tag <> 0)) then
      ShowTaskDetails
    else
    begin
      TaskViewer.CloseTaskInfo;
      if not chbShowTaskDetails.Checked then
        TaskViewer.Hide;
    end;
  finally
    Timer.Tag := 0;
  end;
end;

function TTQueueViewForm.TaskListIsEmpty: Boolean;
begin
  Result := dsTaskList.RecordCount = 0;
end;

procedure TTQueueViewForm.StGridDblClick(Sender: TObject);
var
x,y:integer;
s:TStatType;
ChPr :TfrmChoosePriority;
begin
  y := StGrid.Selection.Top;
  x := StGrid.Selection.Left;
  s := (StGrid.Objects[x,y] as TStatType);
  case y of
     0..3:
     if x in [0,2] then
     begin
       s.selected := not s.selected;
     end;
     4:
     begin
       if x = 0 then
       begin
         ChPr := TfrmChoosePriority.Create(nil);
         try
           ChPr.Choose(FPriorityList,s.FieldValue,s.selected);
         finally
           ChPr.Free;
         end;
       end;
     end;
  end;
  UpdateStatFilter;
  tListFilterChange(nil);
end;

procedure TTQueueViewForm.StGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  StGrid.Canvas.Brush.Color := StGrid.Color;
  StGrid.Canvas.FillRect(Rect);
  StGrid.Canvas.Font.Color:=  clBlack;
//  if ((ACol in [0,2]) and (aRow < 4)) or () then
  if assigned(StGrid.Objects[ACol,ARow]) then
  begin
  if (StGrid.Objects[ACol,ARow] as TStatType).selected then
     StGrid.Canvas.Font.Color:=  clRed;
  end;
  StGrid.Canvas.TextRect(Rect,Rect.Left + 1,Rect.Top + 1,StGrid.Cells[ACol,ARow]);
end;

procedure TTQueueViewForm.ApplyStatistics;
var SelPr:string;
    i:integer;
begin
   with StGrid do
   begin
     Cells[0,0]  := 'Queued';
     Cells[0,1]  := 'Waiting for Res.';
     Cells[0,2]  := 'Executing';
     Cells[0,3]  := 'Failed';

     Cells[0,5]  := 'Total tasks';

     Cells[2,0]  := 'Finished';
     Cells[2,1]  := 'Finished succ.';
     Cells[2,2]  := 'Finished w/excep.';
     Cells[2,3]  := 'Finished w/warn.';
     SelPr := (Objects[0,4] as TStatType).FieldValue;
     Cells[0,4]  := 'Priority('+ SelPr +')';

     i := FPriorityList.IndexOf(SelPr);
     Cells[1,0]  := iff(FNewTasks=0,'',IntTostr(FNewTasks));
     Cells[1,1]  := iff(FWaitingTasks =0, '',IntToStr(FWaitingTasks));
     Cells[1,2]  := iff(FExecTasks=0,'',IntToStr(FExecTasks));
     Cells[1,3]  := iff(FFailedTasks=0,'',IntToStr(FFailedTasks));
     if i <> -1 then
        Cells[1,4]  := (FPriorityList.Objects[i] as TStatType).FieldValue
     else
        Cells[1,4] := '';

     Cells[1,5]  := IntToStr(FTotalTasks);

     Cells[3,0]  := iff(FFinishedTasks=0,'',IntToStr(FFinishedTasks));
     Cells[3,1]  := iff(FFinSucTasks=0,'',IntToStr(FFinSucTasks));
     Cells[3,2]  := iff(FFinExTasks=0,'',IntToStr(FFinExTasks));
     Cells[3,3]  := iff(FFinWarnTasks=0,'',IntToStr(FFinWarnTasks));
   end;
end;

procedure TTQueueViewForm.CalcStatistics;
var
   FSavedTaskId : Variant;
begin
  FSavedTaskId := dsTaskList['Id'];
  with dsTaskList do
  begin
    try
      DisableControls;
      ClearStatistics;
      First;
      while not EOF do
      begin
        UpdateStatistics();
        Next;
      end;
      ApplyStatistics;
      if not VarIsNull(FSavedTaskId) then
         dsTaskList.Locate('Id', FSavedTaskId, []);
    finally
      EnableControls;
    end;
  end;
end;

procedure TTQueueViewForm.ClearStatistics;
begin
   FTotalTasks := 0;
   FNewTasks := 0;
   FWaitingTasks :=0;
   FExecTasks :=0;
   FFinishedTasks :=0;
   FFinSucTasks :=0;
   FFinExTasks :=0;
   FFinWarnTasks :=0;
   FFailedTasks :=0;

   FPriorityList.Clear;

end;

procedure TTQueueViewForm.UpdateStatFilter;
var s:string;
    i:integer;
begin
   FStatFilter := ' (';
   s :='';
   for i := 0 to 3 do
   begin
     with (StGrid.Objects[0,i] as TStatType) do
     if selected then
     begin
        FStatFilter := FStatFilter + s +  ' '+FieldName+' ';
        s :=' or ';
     end;
     with (StGrid.Objects[2,i] as TStatType) do
     if selected then
     begin
        FStatFilter := FStatFilter + s +  ' '+FieldName+' ';
        s :=' or ';
     end;
   end;
   if FStatFilter = ' (' then
      FStatFilter := ''
   else
      FStatFilter := FStatFilter +' )';
   with (StGrid.Objects[0,4] as TStatType) do
   if selected then
   begin
        if FStatFilter <> '' then FStatFilter := FStatFilter + ' and ';
        FStatFilter := FStatFilter +   '('+FieldName+' = ''' + FieldValue +''')';
   end;
end;

procedure TTQueueViewForm.UpdateStatistics;
var
 status :string;
 i: integer;
begin
  Inc(FTotalTasks);

  status := dsTaskListStatus.AsString;
  if dsTaskListFinished.AsBoolean then
  begin
     Inc(FFinishedTasks);
     if dsTaskListFinishedExcep.AsBoolean then Inc(FFinExTasks)
     else  if dsTaskListFinishedSucs.AsBoolean then Inc(FFinSucTasks)
     else  if dsTaskListFinishedWarn.AsBoolean then Inc(FFinWarnTasks);

  end else
  begin
    if dsTaskListExecTask.AsBoolean then
    begin
      Inc(FExecTasks);
      FPriorityList.Add(dsTaskListPriority.AsString);
      i := FPriorityList.IndexOf(dsTaskListPriority.AsString);
      if not Assigned(FPriorityList.Objects[i]) then
      begin
         FPriorityList.Objects[i] := TStatType.Create;
         (FPriorityList.Objects[i] as TStatType).FieldValue := '1';
      end else
        with  (FPriorityList.Objects[i] as TStatType) do
           FieldValue := IntToStr(StrToInt(FieldValue)  + 1);
    end
    else if dsTaskListWaitForRes.AsBoolean then Inc(FWaitingTasks)
    else if dsTaskListNewTask.AsBoolean then Inc(FNewTasks);
  end
end;

procedure TTQueueViewForm.dsTaskListAfterDelete(DataSet: TDataSet);
begin
   CalcStatistics;
end;

procedure TTQueueViewForm.ShowTaskDetails;
var
  T: IevTask;
  ReqList: IisListOfValues;
  i: Integer;
  bMarkAsSeen: Boolean;
begin
  dsTaskList.Tag := 1;
  try
    if ConvertNull(dsTaskList['Finished'], False) then
    begin
      // Task Info
      ctx_StartWait;
      try
        i := FTaskCache.IndexOf(dsTaskList['Id']);
        if (i <> -1) then
          if Supports(FTaskCache.Objects[i], IevTask) then
            T := FTaskCache.Objects[i] as IevTask
          else
            FTaskCache.Delete(i)
        else
          T := nil;

        if not Assigned(T) then
        begin
          bMarkAsSeen := AnsiSameText(dsTaskList['Username'], Context.UserAccount.User);
          T := mb_TaskQueue.GetTask(dsTaskList['Id']);

          if Assigned(T) then
          begin
            FTaskCache.AddObject(T.ID, T);

            if not dsTaskList['Seen'] then
            begin
              if bMarkAsSeen then
              Begin
                mb_TaskQueue.ModifyTaskProperty(T.ID, 'SeenByUser', True);

                dsTaskList.DisableControls;
                try
                  dsTaskList.Edit;
                  dsTaskList['Seen'] := True;
                  dsTaskList.Post;
                finally
                  dsTaskList.EnableControls;
                end
              end;
            end;
          end;
        end;

        if Assigned(T) and (TaskViewer.AssignedItem <> T) then
          TaskViewer.ShowTaskResults(T);
        cbUseEmail.Enabled := False;
        sbSetEmail.Enabled := False;
      finally
        ctx_EndWait;
      end;
    end

    else
    begin
      if not TaskListIsEmpty then
      begin
        // Request info
        ReqList := nil;
        i := FTaskCache.IndexOf(dsTaskList['Id']);
        if (i <> -1) then
          if Supports(FTaskCache.Objects[i], IisListOfValues) then
            ReqList := FTaskCache.Objects[i] as IisListOfValues
          else
            FTaskCache.Delete(i);

        if not Assigned(ReqList) then
        begin
          ReqList := mb_TaskQueue.GetTaskRequests(dsTaskList['Id']);
          FTaskCache.AddObject(dsTaskList['Id'], ReqList);
        end;
      end
      else
        ReqList := nil;

      if TaskViewer.AssignedItem <> ReqList then
        TaskViewer.ShowTaskRequests(ReqList);
      cbUseEmail.Enabled := True;
      sbSetEmail.Enabled := True;
    end;

    TaskViewer.Show;
  finally
    dsTaskList.Tag := 0;  
  end;
end;

procedure TTQueueViewForm.DetailPanelDblClick(Sender: TObject);
begin
  Timer.Enabled := False;
  ShowTaskDetails;
end;

procedure TTQueueViewForm.chbShowTaskDetailsClick(Sender: TObject);
begin
  if chbShowTaskDetails.Tag = 0 then
  begin
    if chbShowTaskDetails.Checked then
      ShowTaskDetails;
    if chbShowTaskDetails.Checked then
      mb_AppSettings.AsString['Settings\AutoShowTaskDetails'] := '1'
    else
      mb_AppSettings.AsString['Settings\AutoShowTaskDetails'] := '0';
  end;
end;

procedure TTQueueViewForm.Show(const ATaskID: TisGUID);
begin
  DoOnShow;

  if ATaskID <> '' then
    if dsTaskList.Locate('ID', ATaskID, []) then
      PostMessage(Handle, CM_CURRENT_TASK_CHANGE, 1, 0);
        
  ShowModal;
end;

procedure TTQueueViewForm.DoOnShow;
var
 sUserName : String;
begin
  Tabs.TabIndex := 1;
  TabsChange(nil);
  Load;
  dsTaskList.Filtered := True;
  sUserName := LowerCase(Context.UserAccount.User);
  dsTaskList.Filter := 'UserNameHide = '+ QuotedStr(sUserName);
end;

end.
