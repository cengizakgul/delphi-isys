unit EvoRWAdapterDM;

interface

uses
  SysUtils, Classes, Windows, Forms, DB, Variants, IniFiles, isUtils, rwCallback,
  rwEngineTypes, evQBTlbCompaniesFrm, EvTypes, SDataDictClient, isAppIDs, EvInitApp,
  ISZippingRoutines, isBasicUtils, EvContext, EvCommonInterfaces, EvMainboard, EvClientStart,
  Controls, EvDataset, isTypes, SWaitingObjectForm, isBaseClasses, isErrorUtils, evTransportInterfaces,
  isThreadManager, evExceptions, ISDataAccessComponents, rwPCLCanvas, Graphics,
  EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet, isDataSet
  ,StrUtils, Math;

type
  TrwExternalFunctType = (rwEFUnknown,rwEFOpenClient, rwEFSetConsolidationID, rwEFEvoConst, rwEFEDTypes,
                          rwEFStringToArray, rwEFGetOpenedClient, rwEFGetConsolidationID, rwEFGetConsolidationMainCompany,
                          rwEFSetCurrentCompany, rwEFGetCurrentCompany, rwEFAdjustedDateIfHoliday, rwEFEvoTranslateField,
                          rwEFGetSecurityScheme, rwEFArrayToString, rwEFCompressData, rwEFDecompressData, rwEFGetEvoUserName,
                          rwEFTestClient, rwEFTextToEMF, rwEFGetSecurityElement, rwEFGetEvoUserAccountInfo,
                          rwEFGetAuditData, rwEFGetAuditNBRCreationData, rwEFRunEvoNativeQuery
                          // rwEFRunEvoNativeQuery kept for enum order and compatibility, but is not functional any more - taken off ticket 102173
                          // ticket 103154
                          , rwEFSetACAConsolidationID, rwEFConsolidationKind
                          // ticket 105183
                          , rwEFGetAuditChanges//, rwEFGetAuditBlob
                          );

  TrwEvAdapterStateType = (rwASCleaningClientDataCache);
  TrwEvAdapterState = set of TrwEvAdapterStateType;

  TevRWReportStatusType =(rsClientOpened, rsCompanyOpened, rsConsolidationMode);
  TevRWReportStatus = set of TevRWReportStatusType;

  TrwEvConsolType = (ctNone, ctNoMerge, ctSimpleMerge, ctMergeNoChanges, ctMergeWithChanges);

  TrwConsolidationInfoRec = record
    TableName: String;
    ConsolType: TrwEvConsolType;
    ConsFieldToChange: String;
  end;


  TlqConsolTable = class(TCollectionItem)
  private
    FTableExtName: String;
    FSBTable: String;
  public
    destructor Destroy; override;
  end;


  TlqConsolTables = class(TOwnedCollection)
  private
    function GetItem(Index: Integer): TlqConsolTable;
  private
    property  Items[Index: Integer]: TlqConsolTable read GetItem; default;

    function  AddTable(const ATable: string): TlqConsolTable;
    function  FindTable(const ATable: string): TlqConsolTable;
  end;


  TEvRWAdapter = class(TDataModule, IevEvolutionGUI, IevContextCallbacks)
    RWCallBack: TrwAppAdapter;
    procedure RWCallBackAfterCacheTable(
      const CachedTableImage: IrwCachedTableImage);
    procedure RWCallBackBeforeCacheTable(const CachedTable: IrwCachedTable;
      const Params: array of TrwParamRec);
    procedure RWCallBackDBCacheCleaning(
      const DataDictExternalTableName: String; var Accept: Boolean);
    procedure RWCallBackEndDesignerProgressBar;
    procedure RWCallBackExecuteExternalFunction(const FunctionID: Integer;
      var Params: array of Variant; out Result: Variant);
    procedure RWCallBackGetCurrentDateTime(var ADateTime: TDateTime);
    procedure RWCallBackGetDDFieldInfo(const ATable, AField: String;
      out FieldInfo: TrwDDFieldInfoRec);
    procedure RWCallBackGetDDFields(const ATable: String; out Fields: String);
    procedure RWCallBackGetDDParamInfo(const ATable, Param: String;
      out ParmInfo: TrwDDParamInfoRec);
    procedure RWCallBackGetDDTableInfo(const ATable: String;
      out TableInfo: TrwDDTableInfoRec);
    procedure RWCallBackGetDDTables(out Tables: String);
    procedure RWCallBackGetExternalFunctionDeclarations(
      const FunctionHeaders: TStringList);
    procedure RWCallBackGetLicencePassword(const ALicenceID: String;
      var APassword: String);
    procedure RWCallBackGetTempDir(out TempDir: String);
    procedure RWCallBackLoadDataDictionary(Data: TStream);
    procedure RWCallBackLoadLibraryComponents(Data: TStream);
    procedure RWCallBackLoadLibraryFunctions(Data: TStream);
    procedure RWCallBackLoadQBTemplates(Data: TStream);
    procedure RWCallBackLoadSQLConstants(Data: TStream);
    procedure RWCallBackStartDesignerProgressBar(const Title: String;
      MaxValue: Integer);
    procedure RWCallBackSubstDefaultClassName(
      var ADefaultClassName: String);
    procedure RWCallBackSubstituteValue(var AValue: Variant;
      const SubstID: String);
    procedure RWCallBackTableDataRequest(const CachedTable: IrwCachedTable;
      const sbTable: IrwSBTable; const Params: array of TrwParamRec);
    procedure RWCallBackUnloadDataDictionary(Data: TStream);
    procedure RWCallBackUnloadLibraryComponents(Data: TStream);
    procedure RWCallBackUnloadLibraryFunctions(Data: TStream);
    procedure RWCallBackUnloadQBTemplates(Data: TStream);
    procedure RWCallBackUnloadSQLConstants(Data: TStream);
    procedure RWCallBackUpdateDesignerProgressBar(
      const ProgressText: String; CurrentValue: Integer);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure RWCallBackReadReportParamsFromStream(const AData: TStream;
      var AParamList: TrwReportParamList);
    procedure RWCallBackGetSecurityDescriptor(
      var ASecurityDescriptor: TrwDesignerSecurityRec);
    procedure RWCallBackDispatchCustomFunction(const FunctionName: String;
      const Params: Variant; var Result: Variant);
    procedure RWCallBackWriteReportParamsToStream(
      var AParamList: TrwReportParamList; const Data: TStream);
    procedure RWCallBackGetCustomControl(const Destination: String;
      var AContainer: TForm);
    procedure RWCallBackException(var ErrorMessage: String);
    procedure RWCallBackDeactivate(Sender: TObject);
    procedure RWCallBackSetAppCustomData(var AData: Variant);
    procedure RWCallBackTerminate(Sender: TObject);
  private
    // ticket 103154
    FConsolidationKind: integer;
    FEvoConstList: TStringList;
    FAdapterState: TrwEvAdapterState;
    FDataDictionaryReadVersion: TDateTime;
    FQBTemplatesReadVersion: TDateTime;
    FLibComponentsReadVersion: TDateTime;
    FLibFunctionsReadVersion: TDateTime;
    FQBConstantsReadVersion: TDateTime;

    FConsolTables: TlqConsolTables;
    FCompanyNBR: Integer;
    FConsolidationNBR: Integer;
    FConsolMainCoNbr: Integer;
    FConsolChldCoStr: String;
    FConsolChldCo: TList;
    FSubstValues: TStringList;
    FReportStatus: TevRWReportStatus;
    FUnattended: Boolean;
    FQBTlbCompanies: TevQBTlbCompanies;

    FCachedCTSLicense: Boolean;

    FProgressBarCounter: Integer;
    FWaitDialog: TAdvancedWait;

    procedure InitConstList;
    function  TableOfClientDB(const ATableName: String): Boolean;
    procedure CleanUpContextTables;
    procedure FreeConsolTables;
    function  GetConsolInfo(const ATableName: String): TrwConsolidationInfoRec;
    procedure CacheReportLicenceInfo;
    procedure ClearDBCache;
    function  FineName(AName: String; RemoveUnderscore: Boolean = True): String;
    function  ConvertDBTypeToRWOne(const AFieldType: TFieldType): TrwDDType;
    procedure OnException(Sender: TObject; E: Exception);
    procedure OnShowModalDialog(Sender: TObject);
    procedure OnHideModalDialog(Sender: TObject);

    procedure ExtProcOpenClient(const Cl_Nbr: Integer);
    function  ExtProcTestClient(const Cl_Nbr: Integer; var AError: String): Boolean;
    // ticket 103154
    procedure ExtProcSetConsolidationNBR(const AConsolNBR: Integer; AConsolidationKind:Integer);
    function  ExtFunctGetConsolidationKind: Integer;

    function  ExtFunctEvoConst(const AConstName: String): String;
    function  ExtFunctEDTypes(const AEDTypeName: String): String;
    function  ExtFunctStringToArray(const AString: String): Variant;
    function  ExtFunctArrayToString(const AArray: Variant): String;
    function  ExtFunctGetOpenedClient: Integer;
    function  ExtFunctGetConsolidationNBR: Integer;
    function  ExtFunctGetConsolidationMainCompany: Integer;
    procedure ExtProcOpenCompany(const Co_Nbr: Integer);
    function  ExtFunctGetOpenedCompany: Integer;
    function  ExtFunctAdjustedDateIfHoliday(const ADate: TDateTime; const ANbrOfDays, SY_GLOBAL_AGENCY_NBR: Integer): TDateTime;
    function  ExtFunctEvoTranslateField(const ATable: String; const AField: String; const AFieldValue: Variant): Variant;
    function  ExtFunctGetSecurityScheme: Variant;
    function  ExtFunctCompressData(const AData: Variant): Variant;
    function  ExtFunctDecompressData(const AData: Variant): Variant;
    function  ExtFunctGetEvoUserName: Variant;
    function  ExtFunctGetEvoUserAccountInfo: Variant;
    function  ExtFunctTextToEMF(const AText: String; const AFontInfo: String): Variant;
    function  ExtFunctGetSecurityElement(const AType, AName: String): Variant;
    function  ExtFunctGetAuditData(const AFields: String; const ABeginDate: TDateTime): Variant;
    function  ExtFunctGetAuditNBRCreationData(const ATables: String): Variant;
//  ticket 102173 - security concern
{
    function  ExtFunctRunEvoNativeQuery(const ASQL: String; const AParamNames: Variant; const AParamValues: Variant): Variant;
}

    procedure OpenClient(const Cl_Nbr: Integer; Co_Nbr: Integer = 0);
    procedure OnChangeCompanyCB(Sender: TObject; Cl_Nbr: Integer; Co_Nbr: Integer);

    function   GUIContext: IevContext;
    procedure  CreateMainForm;
    procedure  DestroyMainForm;
    procedure  StartWait(const AText: string = ''; AMaxProgress: Integer = 0);
    procedure  UpdateWait(const AText: string = ''; ACurrentProgress: Integer = 0; AMaxProgress: Integer = -1);
    procedure  EndWait;
    procedure  AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);

    procedure  ApplySecurity(const Component: TComponent);
    procedure  AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean = True); overload;
    procedure  AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean = True); overload;
    procedure  OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
    procedure  CheckTaskQueueStatus;
    procedure  PrintTask(const ATaskID: TisGUID);
    procedure  OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
    procedure  OnWaitingServerResponse;
    procedure  OnPopupMessage(const aText, aFromUser, aToUsers: String);
    procedure  UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
    procedure  PasswordChangeClose;
    function ExtFunctGetAuditChanges(const AFields: String;
      const ABeginDate: Variant): Variant;
//    function ExtFunctGetAuditBlob(ATable, AField: string;
//      ARefValue: integer): Variant;
  end;

var
  EvRWAdapter: TEvRWAdapter;

implementation

{$R *.dfm}
{$R EvPCLFonts.res}

uses EvConsts, rwEvUtils, SDDClasses, SDataStructure,  SFieldCodeValues, EvUtils, EvBasicUtils,
     EvStreamUtils, EvRWTranslator,  SReportSettings, sSecurityClassDefs;

const
  cDynamicFieldsTable = 'Ev_Dynamic_Fields';

function GetRwAdapterGUI: IevEvolutionGUI;
begin
  Result := EvRWAdapter;
end;

function GetRwAdapterContextCallbacks: IevContextCallbacks;
begin
  Result := EvRWAdapter as IevContextCallbacks;
end;

procedure Initialize; stdcall;
begin
  InitializeEvoApp;
  SetUnhandledErrorSettings(uesLogAllShowNothing);
  DisableProcessWindowsGhosting;
  EvRWAdapter := TEvRWAdapter.Create(Application);
  Mainboard.ModuleRegister.RegisterModule(@GetRwAdapterGUI, IevEvolutionGUI, 'RW Adapter GUI');
  Mainboard.ModuleRegister.RegisterModule(@GetRwAdapterContextCallbacks, IevContextCallbacks, 'Context Callbacks');
end;

procedure Finalize; stdcall;
begin
  UninitializeEvoApp;
  FreeAndNil(EvRWAdapter);
end;

function ReportWriterAppAdapter: IrwAppAdapter; stdcall;
begin
  Result := EvRWAdapter.RWCallBack;
end;

exports Initialize;
exports Finalize;
exports ReportWriterAppAdapter;


const
  ResErrNoLogKey = 'Data consolidation error! Table "%s" does not have logical key';
  ResErrNoKey = 'Data consolidation error! Table "%s" does not have primary key';
  ResErrWrongLogKeyType = 'Data consolidation error! Type is not applicable for operation';
  ResErrAccessDenied = 'You do not have rights to access to this report';


  aConsolTable: array [0..19] of TrwConsolidationInfoRec = (
   (TableName:          'CO';
    ConsolType:         ctNoMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_ADDITIONAL_INFO_NAMES';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_DIVISION';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_BRANCH';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_DEPARTMENT';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_TEAM';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_E_D_CODES';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_FED_TAX_LIABILITIES';
    ConsolType:         ctSimpleMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_JOBS';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_LOCAL_TAX';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_LOCAL_TAX_LIABILITIES';
    ConsolType:         ctSimpleMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_PHONE';
    ConsolType:         ctSimpleMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_REPORTS';
    ConsolType:         ctSimpleMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_STATE_TAX_LIABILITIES';
    ConsolType:         ctSimpleMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_STATES';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_SUI';
    ConsolType:         ctMergeNoChanges;
    ConsFieldToChange:  ''),

   (TableName:          'CO_SUI_LIABILITIES';
    ConsolType:         ctSimpleMerge;
    ConsFieldToChange:  ''),

   (TableName:          'CO_WORKERS_COMP';
    ConsolType:         ctMergeWithChanges;
    ConsFieldToChange:  'WORKERS_COMP_CODE'),

   (TableName:          'EE';
    ConsolType:         ctMergeWithChanges;
    ConsFieldToChange:  'CUSTOM_EMPLOYEE_NUMBER'),

   (TableName:          'PR';
    ConsolType:         ctMergeWithChanges;
    ConsFieldToChange:  'RUN_NUMBER')
  );



procedure TEvRWAdapter.InitConstList;
begin
  FEvoConstList := TStringList.Create;

  with FEvoConstList do
  begin
            // Initialization of Constants List
    Add('ED_ST_DEFERRED_COMP=' + ED_ST_DEFERRED_COMP);
    Add('ED_ST_MASS_RETIREMENT=' + ED_ST_MASS_RETIREMENT);
    Add('ED_ST_PENSION=' + ED_ST_PENSION);
    Add('ED_ST_401K=' + ED_ST_401K);
    Add('ED_ST_403B=' + ED_ST_403B);
    Add('ED_ST_457=' + ED_ST_457);
    Add('ED_ST_501C=' + ED_ST_501C);
    Add('ED_ST_SIMPLE=' + ED_ST_SIMPLE);
    Add('ED_ST_SEP=' + ED_ST_SEP);
    Add('ED_ST_CATCH_UP=' + ED_ST_CATCH_UP);
    Add('ED_ST_HSA_SINGLE=' + ED_ST_HSA_SINGLE);
    Add('ED_ST_HSA_FAMILY=' + ED_ST_HSA_FAMILY);
    Add('ED_ST_TIAA_CREFF=' + ED_ST_TIAA_CREFF);
    Add('ED_ST_PRETAX_INS=' + ED_ST_PRETAX_INS);
    Add('ED_ST_SB_PREAX_INS=' + ED_ST_SB_PREAX_INS);
    Add('ED_ST_DISCRIMINARY_S125=' + ED_ST_DISCRIMINARY_S125);
    Add('ED_ST_STATE_PENSION=' + ED_ST_STATE_PENSION);
    Add('ED_ST_SP_TAXED_DEDUCTION=' + ED_ST_SP_TAXED_DEDUCTION);
    Add('ED_ST_DEPENDENT_CARE=' + ED_ST_DEPENDENT_CARE);
    Add('ED_ST_NON_QUAL_BELOW=' + ED_ST_NON_QUAL_BELOW);
    Add('ED_ST_NON_QUAL_UP=' + ED_ST_NON_QUAL_UP);
    Add('ED_ST_INCENTIVE_STOCK=' + ED_ST_INCENTIVE_STOCK);
    Add('ED_ST_EXEMPT_EARNINGS=' + ED_ST_EXEMPT_EARNINGS);
//    Add('ED_ST_HEALTHCARE=' + ED_ST_HEALTHCARE);
    Add('ED_ST_SP_TAXED_1099_EARNINGS='+ED_ST_SP_TAXED_1099_EARNINGS);
    Add('ED_ST_1099_R=' + ED_ST_1099_R);
    Add('ED_ST_1099_R_SP_TAXED=' + ED_ST_1099_R_SP_TAXED);
    Add('ED_ST_1099_DIV=' + ED_ST_1099_DIV);
    Add('ED_ST_INTEREST=' + ED_ST_INTEREST);
    Add('ED_ST_SP_TAXED_EARNING=' + ED_ST_SP_TAXED_EARNING);
    Add('ED_3RD_SHORT=' + ED_3RD_SHORT);
    Add('ED_3RD_LONG=' + ED_3RD_LONG);
    Add('ED_3RD_NON_TAX=' + ED_3RD_NON_TAX);
    Add('ED_MEMO=' + ED_MEMO_SIMPLE);
    Add('ED_MEMO_ER_INSURANCE_PREMIUM=' + ED_MEMO_ER_INSURANCE_PREMIUM);
    Add('ED_MEMO_TIPPED_SALES=' + ED_MEMO_TIPPED_SALES);
    Add('ED_MEMO_PENSION=' + ED_MEMO_PENSION);
    Add('ED_EWOD_MEAL_CREDIT=' + ED_EWOD_MEAL_CREDIT);
    Add('ED_MEMO_PENSION_SUI=' + ED_MEMO_PENSION_SUI);
    Add('ED_MEMO_LINE_9=' + ED_MEMO_LINE_9);
    Add('ED_MEMO_WASHINGTON_L_I=' + ED_MEMO_WASHINGTON_L_I);
    Add('ED_MU_TIPPED_CREDIT_MAKEUP=' + ED_MU_TIPPED_CREDIT_MAKEUP);
    Add('ED_MU_MIN_WAGE_MAKEUP=' + ED_MU_MIN_WAGE_MAKEUP);
    Add('ED_EWOD_2PERC_SHAREHOLDER=' + ED_EWOD_2PERC_SHAREHOLDER);
    Add('ED_EWOD_GROUP_TERM_LIFE=' + ED_EWOD_GROUP_TERM_LIFE);
    Add('ED_EWOD_MOVING_EXP_TAXABLE=' + ED_EWOD_MOVING_EXP_TAXABLE);
    Add('ED_EWOD_MOVING_EXP_NON_TAX=' + ED_EWOD_MOVING_EXP_NON_TAX);
    Add('ED_EWOD_TIPS=' + ED_EWOD_TIPS);
    Add('ED_EWOD_CHARGE_TIPS=' + ED_EWOD_CHARGE_TIPS);
    Add('ED_EWOD_SPC_TAXED_EARNINGS=' + ED_EWOD_SPC_TAXED_EARNINGS);
    Add('ED_EWOD_TAXABLE_AUTO=' + ED_EWOD_TAXABLE_AUTO);
    Add('ED_DIRECT_DEPOSIT=' + ED_DIRECT_DEPOSIT);
    Add('ED_DED_STANDARD=' + ED_DED_STANDARD);
    Add('ED_DED_CHILD_SUPPORT=' + ED_DED_CHILD_SUPPORT);
    Add('ED_DED_GARNISH=' + ED_DED_GARNISH);
    Add('ED_DED_LEVY=' + ED_DED_LEVY);
    Add('ED_DED_REIMBURSEMENT=' + ED_DED_REIMBURSEMENT);
    Add('ED_DED_SB_COBRA=' + ED_DED_SB_COBRA);
    Add('ED_DED_BANKRUPTCY=' + ED_DED_BANKRUPTCY);
    Add('ED_DED_SPECIAL_SDI=' + ED_DED_SPECIAL_SDI);
    Add('ED_DED_PENSION_LOAN=' + ED_DED_PENSION_LOAN);
    Add('ED_DED_WASHINGTON_L_I=' + ED_DED_WASHINGTON_L_I);
    Add('ED_DED_PERCENT_OF_TAX=' + ED_DED_PERCENT_OF_TAX);
    Add('ED_DED_AFTER_TAX_PENSION=' + ED_DED_AFTER_TAX_PENSION);
    Add('ED_OEARN_OVERTIME=' + ED_OEARN_OVERTIME);
    Add('ED_OEARN_WAITSTAFF_OVERTIME=' + ED_OEARN_WAITSTAFF_OVERTIME);
    Add('ED_OEARN_WEIGHTED_HALF_TIME_OT=' + ED_OEARN_WEIGHTED_HALF_TIME_OT);
    Add('ED_OEARN_WEIGHTED_3_HALF_TIME_OT=' + ED_OEARN_WEIGHTED_3_HALF_TIME_OT);
    Add('ED_OEARN_AVG_OVERTIME=' + ED_OEARN_AVG_OVERTIME);
    Add('ED_OEARN_AVG_MULT_OVERTIME=' + ED_OEARN_AVG_OVERTIME);
    Add('ED_OEARN_BANQUET_TIPS=' + ED_OEARN_BANQUET_TIPS);
    Add('ED_OEARN_CHARGE_TIPS=' + ED_OEARN_CHARGE_TIPS);
    Add('ED_OEARN_SUPP_CHARGE_TIPS=' + ED_OEARN_SUPP_CHARGE_TIPS);
    Add('ED_OEARN_PIECEWORK=' + ED_OEARN_PIECEWORK);
    Add('ED_OEARN_REGULAR=' + ED_OEARN_REGULAR);
    Add('ED_OEARN_SALARY=' + ED_OEARN_SALARY);
    Add('ED_OEARN_STD_EARNINGS=' + ED_OEARN_STD_EARNINGS);
    Add('ED_OEARN_SEVERANCE_PAY=' + ED_OEARN_SEVERANCE_PAY);
    Add('ED_OEARN_SICK=' + ED_OEARN_SICK);
    Add('ED_OERAN_VACATION=' + ED_OERAN_VACATION);
    Add('ED_EWOD_MEAL_CREDIT=' + ED_EWOD_MEAL_CREDIT);
    Add('ED_EWOD_TAXABLE_ER_HSA_SINGLE=' + ED_EWOD_TAXABLE_ER_HSA_SINGLE);
    Add('ED_EWOD_TAXABLE_ER_HSA_FAMILY=' + ED_EWOD_TAXABLE_ER_HSA_FAMILY);

    Add('PAYROLL_STATUS_PENDING=' + PAYROLL_STATUS_PENDING);
    Add('PAYROLL_STATUS_PROCESSED=' + PAYROLL_STATUS_PROCESSED);
    Add('PAYROLL_STATUS_COMPLETED=' + PAYROLL_STATUS_COMPLETED);
    Add('PAYROLL_STATUS_VOIDED=' + PAYROLL_STATUS_VOIDED);
    Add('PAYROLL_STATUS_DELETED=' + PAYROLL_STATUS_DELETED);
    Add('PAYROLL_STATUS_HOLD=' + PAYROLL_STATUS_HOLD);
    Add('PAYROLL_STATUS_SB_REVIEW=' + PAYROLL_STATUS_SB_REVIEW);
    Add('PAYROLL_STATUS_PROCESSING=' + PAYROLL_STATUS_PROCESSING);

    Add('GL_DATA_NONE=' + GL_DATA_NONE);
    Add('GL_DATA_ED=' + GL_DATA_ED);
    Add('GL_DATA_ED_OFFSET=' + GL_DATA_ED_OFFSET);
    Add('GL_DATA_FEDERAL=' + GL_DATA_FEDERAL);
    Add('GL_DATA_EE_OASDI=' + GL_DATA_EE_OASDI);
    Add('GL_DATA_ER_OASDI=' + GL_DATA_ER_OASDI);
    Add('GL_DATA_EE_MEDICARE=' + GL_DATA_EE_MEDICARE);
    Add('GL_DATA_ER_MEDICARE=' + GL_DATA_ER_MEDICARE);
    Add('GL_DATA_FUI=' + GL_DATA_FUI);
    Add('GL_DATA_EIC=' + GL_DATA_EIC);
    Add('GL_DATA_BACKUP=' + GL_DATA_BACKUP);
    Add('GL_DATA_STATE=' + GL_DATA_STATE);
    Add('GL_DATA_EE_SDI=' + GL_DATA_EE_SDI);
    Add('GL_DATA_ER_SDI=' + GL_DATA_ER_SDI);
    Add('GL_DATA_LOCAL=' + GL_DATA_LOCAL);
    Add('GL_DATA_EE_SUI=' + GL_DATA_EE_SUI);
    Add('GL_DATA_ER_SUI=' + GL_DATA_ER_SUI);
    Add('GL_DATA_BILLING=' + GL_DATA_BILLING);
    Add('GL_DATA_AGENCY_CHECK=' + GL_DATA_AGENCY_CHECK);
    Add('GL_DATA_BILLING_OFFSET=' + GL_DATA_BILLING_OFFSET);
    Add('GL_DATA_AGENCY_CHECK_OFFSET=' + GL_DATA_AGENCY_CHECK_OFFSET);
    Add('GL_DATA_ER_MATCH_OFFSET=' + GL_DATA_ER_MATCH_OFFSET);
    Add('GL_DATA_TAX_IMPOUND_OFFSET=' + GL_DATA_TAX_IMPOUND_OFFSET);
    Add('GL_DATA_TAX_CHECK=' + GL_DATA_TAX_CHECK);
    Add('GL_DATA_TAX_CHECK_OFFSET=' + GL_DATA_TAX_CHECK_OFFSET);
    Add('GL_DATA_UI_ROUNDING=' + GL_DATA_UI_ROUNDING);
    Add('GL_DATA_VT_HEALTHCARE=' + GL_DATA_VT_HEALTHCARE);
    Add('GL_DATA_NET_PAY=' + GL_DATA_NET_PAY);
    Add('GL_DATA_TAX_IMPOUND=' + GL_DATA_TAX_IMPOUND);
    Add('GL_DATA_TRUST_IMPOUND=' + GL_DATA_TRUST_IMPOUND);
    Add('GL_DATA_TRUST_IMPOUND_OFFSET=' + GL_DATA_TRUST_IMPOUND_OFFSET);
    Add('GL_DATA_WC_IMPOUND=' + GL_DATA_WC_IMPOUND);
    Add('GL_DATA_WC_IMPOUND_OFFSET=' + GL_DATA_WC_IMPOUND_OFFSET);
    Add('GL_DATA_3P_CHECK=' + GL_DATA_3P_CHECK);
    Add('GL_DATA_3P_TAX=' + GL_DATA_3P_TAX);
    Add('GL_DATA_FEDERAL_OFFSET=' + GL_DATA_FEDERAL_OFFSET);
    Add('GL_DATA_EE_OASDI_OFFSET=' + GL_DATA_EE_OASDI_OFFSET);
    Add('GL_DATA_ER_OASDI_OFFSET=' + GL_DATA_ER_OASDI_OFFSET);
    Add('GL_DATA_EE_MEDICARE_OFFSET=' + GL_DATA_EE_MEDICARE_OFFSET);
    Add('GL_DATA_ER_MEDICARE_OFFSET=' + GL_DATA_ER_MEDICARE_OFFSET);
    Add('GL_DATA_FUI_OFFSET=' + GL_DATA_FUI_OFFSET);
    Add('GL_DATA_EIC_OFFSET=' + GL_DATA_EIC_OFFSET);
    Add('GL_DATA_BACKUP_OFFSET=' + GL_DATA_BACKUP_OFFSET);
    Add('GL_DATA_STATE_OFFSET=' + GL_DATA_STATE_OFFSET);
    Add('GL_DATA_EE_SDI_OFFSET=' + GL_DATA_EE_SDI_OFFSET);
    Add('GL_DATA_ER_SDI_OFFSET=' + GL_DATA_ER_SDI_OFFSET);
    Add('GL_DATA_LOCAL_OFFSET=' + GL_DATA_LOCAL_OFFSET);
    Add('GL_DATA_EE_SUI_OFFSET=' + GL_DATA_EE_SUI_OFFSET);
    Add('GL_DATA_ER_SUI_OFFSET=' + GL_DATA_ER_SUI_OFFSET);

    Add('GL_LEVEL_NONE=' + GL_LEVEL_NONE);
    Add('GL_LEVEL_DIVISION=' + GL_LEVEL_DIVISION);
    Add('GL_LEVEL_BRANCH=' + GL_LEVEL_BRANCH);
    Add('GL_LEVEL_DEPT=' + GL_LEVEL_DEPT);
    Add('GL_LEVEL_TEAM=' + GL_LEVEL_TEAM);
    Add('GL_LEVEL_EMPLOYEE=' + GL_LEVEL_EMPLOYEE);
    Add('GL_LEVEL_JOB=' + GL_LEVEL_JOB);

    Add('GROUP_BOX_EE=' + GROUP_BOX_EE);
    Add('GROUP_BOX_ER=' + GROUP_BOX_ER);
    Add('GROUP_BOX_EE_OTHER=' + GROUP_BOX_EE_OTHER);
    Add('GROUP_BOX_ER_OTHER=' + GROUP_BOX_ER_OTHER);

    Add('MISC_CHECK_TYPE_AGENCY=' + MISC_CHECK_TYPE_AGENCY);
    Add('MISC_CHECK_TYPE_TAX=' + MISC_CHECK_TYPE_TAX);
    Add('MISC_CHECK_TYPE_BILLING=' + MISC_CHECK_TYPE_BILLING);
    Add('MISC_CHECK_TYPE_AGENCY_VOID=' + MISC_CHECK_TYPE_AGENCY_VOID);
    Add('MISC_CHECK_TYPE_TAX_VOID=' + MISC_CHECK_TYPE_TAX_VOID);
    Add('MISC_CHECK_TYPE_BILLING_VOID=' + MISC_CHECK_TYPE_BILLING_VOID);

    Add('Payment_Type_Check=' + Payment_Type_Check);
    Add('Payment_Type_DirectDeposit=' + Payment_Type_DirectDeposit);
    Add('Payment_Type_None=' + Payment_Type_None);

    Add('GROUP_BOX_YES=' + GROUP_BOX_YES);
    Add('GROUP_BOX_NO=' + GROUP_BOX_NO);

    Add('TRUST_SERVICE_ALL=' + TRUST_SERVICE_ALL);
    Add('TRUST_SERVICE_AGENCY=' + TRUST_SERVICE_AGENCY);
    Add('TRUST_SERVICE_NO=' + TRUST_SERVICE_NO);

    Add('CHECK_TYPE2_REGULAR=' + CHECK_TYPE2_REGULAR);
    Add('CHECK_TYPE2_VOID=' + CHECK_TYPE2_VOID);
    Add('CHECK_TYPE2_3RD_PARTY=' + CHECK_TYPE2_3RD_PARTY);
    Add('CHECK_TYPE2_MANUAL=' + CHECK_TYPE2_MANUAL);
    Add('CHECK_TYPE2_VOUCHER=' + CHECK_TYPE2_VOUCHER);
    Add('CHECK_TYPE2_YTD=' + CHECK_TYPE2_YTD);
    Add('CHECK_TYPE2_QTD=' + CHECK_TYPE2_QTD);

    Add('GROUP_BOX_COMPANY=' + GROUP_BOX_COMPANY);
    Add('GROUP_BOX_INDIVIDUAL=' + GROUP_BOX_INDIVIDUAL);

    Add('TAX_LIABILITY_TYPE_FEDERAL=' + TAX_LIABILITY_TYPE_FEDERAL);
    Add('TAX_LIABILITY_TYPE_EE_OASDI=' + TAX_LIABILITY_TYPE_EE_OASDI);
    Add('TAX_LIABILITY_TYPE_EE_MEDICARE=' + TAX_LIABILITY_TYPE_EE_MEDICARE);
    Add('TAX_LIABILITY_TYPE_EE_EIC=' + TAX_LIABILITY_TYPE_EE_EIC);
    Add('TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING=' + TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING);
    Add('TAX_LIABILITY_TYPE_ER_OASDI=' + TAX_LIABILITY_TYPE_ER_OASDI);
    Add('TAX_LIABILITY_TYPE_ER_MEDICARE=' + TAX_LIABILITY_TYPE_ER_MEDICARE);
    Add('TAX_LIABILITY_TYPE_ER_FUI=' + TAX_LIABILITY_TYPE_ER_FUI);
    Add('TAX_LIABILITY_TYPE_FEDERAL_945=' + TAX_LIABILITY_TYPE_FEDERAL_945);
    Add('TAX_LIABILITY_TYPE_STATE=' + TAX_LIABILITY_TYPE_STATE);
    Add('TAX_LIABILITY_TYPE_EE_SDI=' + TAX_LIABILITY_TYPE_EE_SDI);
    Add('TAX_LIABILITY_TYPE_ER_SDI=' + TAX_LIABILITY_TYPE_ER_SDI);

    Add('BILLING_PAYMENT_METHOD_CLIENT=' + BILLING_PAYMENT_METHOD_CLIENT);
    Add('BILLING_PAYMENT_METHOD_SB=' + BILLING_PAYMENT_METHOD_SB);
    Add('BILLING_PAYMENT_METHOD_DEBIT=' + BILLING_PAYMENT_METHOD_DEBIT);
    Add('BILLING_PAYMENT_METHOD_INVOICE=' + BILLING_PAYMENT_METHOD_INVOICE);

    Add('TAX_DEPOSIT_STATUS_PENDING=' + TAX_DEPOSIT_STATUS_PENDING);
    Add('TAX_DEPOSIT_STATUS_PAID=' + TAX_DEPOSIT_STATUS_PAID);
    Add('TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE=' + TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE);
    Add('TAX_DEPOSIT_STATUS_NSF=' + TAX_DEPOSIT_STATUS_NSF);
    Add('TAX_DEPOSIT_STATUS_VOIDED=' + TAX_DEPOSIT_STATUS_VOIDED);
    Add('TAX_DEPOSIT_STATUS_DELETED=' + TAX_DEPOSIT_STATUS_DELETED);
    Add('TAX_DEPOSIT_STATUS_SEND_NOTICE=' + TAX_DEPOSIT_STATUS_SEND_NOTICE);
    Add('TAX_DEPOSIT_STATUS_NOTICE_SENT=' + TAX_DEPOSIT_STATUS_NOTICE_SENT);
    Add('TAX_DEPOSIT_STATUS_IMPOUNDED=' + TAX_DEPOSIT_STATUS_IMPOUNDED);
    Add('TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED=' + TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED);
    Add('TAX_DEPOSIT_STATUS_PAID_WO_FUNDS=' + TAX_DEPOSIT_STATUS_PAID_WO_FUNDS);

    Add('CO_BILLING_STATUS_IGNORE=' + CO_BILLING_STATUS_IGNORE);
    Add('CO_BILLING_STATUS_CHECK=' + CO_BILLING_STATUS_CHECK);
    Add('CO_BILLING_STATUS_READY_FOR_ACH=' + CO_BILLING_STATUS_READY_FOR_ACH);
    Add('CO_BILLING_STATUS_ACH_SENT=' + CO_BILLING_STATUS_ACH_SENT);

    Add('BANK_REGISTER_STATUS_Cleared=' + BANK_REGISTER_STATUS_Cleared);
    Add('BANK_REGISTER_STATUS_Returned=' + BANK_REGISTER_STATUS_Returned);
    Add('BANK_REGISTER_STATUS_Issued=' + BANK_REGISTER_STATUS_Issued);
    Add('BANK_REGISTER_STATUS_Outstanding=' + BANK_REGISTER_STATUS_Outstanding);
    Add('BANK_REGISTER_STATUS_Stop_payment=' + BANK_REGISTER_STATUS_Stop_payment);
    Add('BANK_REGISTER_STATUS_Dead=' + BANK_REGISTER_STATUS_Dead);
    Add('BANK_REGISTER_STATUS_NSF=' + BANK_REGISTER_STATUS_NSF);
    Add('BANK_REGISTER_STATUS_Paid_NSF=' + BANK_REGISTER_STATUS_Paid_NSF);

    Add('BANK_REGISTER_MANUALTYPE_Interest=' + BANK_REGISTER_MANUALTYPE_Interest);
    Add('BANK_REGISTER_MANUALTYPE_Fees=' + BANK_REGISTER_MANUALTYPE_Fees);
    Add('BANK_REGISTER_MANUALTYPE_ManualDeposit=' + BANK_REGISTER_MANUALTYPE_ManualDeposit);
    Add('BANK_REGISTER_MANUALTYPE_NonSystemCheck=' + BANK_REGISTER_MANUALTYPE_NonSystemCheck);
    Add('BANK_REGISTER_MANUALTYPE_Misc=' + BANK_REGISTER_MANUALTYPE_Misc);
    Add('BANK_REGISTER_MANUALTYPE_Sweep=' + BANK_REGISTER_MANUALTYPE_Sweep);
    Add('BANK_REGISTER_MANUALTYPE_Adjustment=' + BANK_REGISTER_MANUALTYPE_Adjustment);
    Add('BANK_REGISTER_MANUALTYPE_None=' + BANK_REGISTER_MANUALTYPE_None);
    Add('BANK_REGISTER_MANUALTYPE_MadeByMistake_Void=' + BANK_REGISTER_MANUALTYPE_MadeByMistake_Void);
  end;
  FEvoConstList.Sorted := True;
end;


procedure TEvRWAdapter.RWCallBackAfterCacheTable(const CachedTableImage: IrwCachedTableImage);

  procedure MakeLogKeyArr(ATable: IrwDDTable; var L: TrwStrList);
  var
    h: String;
    i: Integer;
  begin
    SetLength(L, 20);
    h := ATable.GetLogicalKey;
    i := 0;
    while h <> '' do
    begin
      L[i] := GetNextStrValue(h, ',');
      Inc(i);
    end;
    SetLength(L, i);
  end;

  function CheckLogicalKey(const ATable: IrwDDTable): TrwEvConsolType;
  var
    h, h1: String;
    R: TrwConsolidationInfoRec;
    T: IrwDDTable;
  begin
    Result := ctNone;
    h := ATable.GetLogicalKey;
    if h = '' then
      Exit;

    h1 := ATable.GetForeignKeysByField(h);
    if h1 = '' then
      Exit;

    h1 := GetNextStrValue(h1, '=');
    T := RWCallBack.RWEngine.DataDictionary.GetTable(h1);
    R := GetConsolInfo(T.GetTableExternalName);

    Result := R.ConsolType;

    if Result = ctNone then
      Result := CheckLogicalKey(T)
    else if Result = ctSimpleMerge then
      Result := ctNone;
  end;


  procedure CacheRefTable(const ATable: IrwDDTable);
  var
    h: String;
    CTI: IrwCachedTableImage;
    CT: IrwCachedTable;
    P: TrwParamList;
  begin
    CT := RWCallBack.RWEngine.DBCache.GetCachedTable(ATable.GetTableName);
    if Assigned(CT) then
    begin
      CTI := CT.GetTableImageByParams(CachedTableImage.GetParams);
      if Assigned(CTI) then
      begin
        if Assigned(CTI.GetSBTable) then
          Exit;
      end;
    end;

    h := 'SELECT ' + ATable.GetPrimaryKey + ', ' + ATable.GetLogicalKey +
         ' FROM ' + ATable.GetTableName + '(:Effective_Date)';
    SetLength(P, 1);
    P[0] := CachedTableImage.GetParams[0];
    RWCallBack.RWEngine.OpenQuery(h, P, nil);
  end;


  procedure UpdateLookupFields(const AFields: TrwStrList);
  var
    i, j: Integer;
    h: String;
    s, f, w, fld: String;
    CT: TlqConsolTable;
    sbTbl: IrwSBTable;
    ddT: IrwDDTable;
    R: TrwConsolidationInfoRec;
    imTblFileName: String;
    Flds: TrwStrList;
    fl: Boolean;
  begin
    Flds := nil;
    // updates all lookup fields with a new key values modified by consolidation process

    imTblFileName := CachedTableImage.GetSBTable.TableFileName;

    s := '';
    f := '''' + imTblFileName + ''' t';
    w := '';

    for i := Low(AFields) to High(AFields) do
    begin
      h := CachedTableImage.CachedTable.ddTable.GetForeignKeysByField(AFields[i]);

      if h <> '' then
      begin
        ddT := RWCallBack.RWEngine.DataDictionary.GetTable(GetNextStrValue(h, '='));
        R := GetConsolInfo(ddT.GetTableExternalName);
        if not AnsiSameText(ddT.GetTableName, 'CO') and ((R.ConsolType <> ctNone) or (CheckLogicalKey(ddT) <> ctNone)) then
          CacheRefTable(ddT);

        if AnsiSameText(ddT.GetTableName, 'CO') then
          CT := nil
        else
          CT := FConsolTables.FindTable(ddT.GetTableExternalName);

        if Assigned(CT) then
        begin
          fld := 't' + IntToStr(i) + '.new_key ' + AFields[i];
          f := f + ', ''' + CT.FSBTable + ''' ' + 't' + IntToStr(i);
          if Length(w) > 0 then
            w := w + ' AND ';
          w := w + 't.' + AFields[i] + ' =+ t' + IntToStr(i) + '.old_key';
        end
        else
          fld := 't.' + AFields[i];
      end
      else
        fld := 't.' + AFields[i];

      if Length(s) > 0 then
        s := s + ', ';
      s := s + fld;
    end;

    if Length(w) > 0 then
    begin
      Flds := CachedTableImage.CachedTable.GetFields;

      for i := Low(Flds) to High(Flds) do
      begin
        fl := False;
        for j := Low(AFields) to High(AFields) do
        begin
          if AnsiSameText(AFields[j], Flds[i]) then
          begin
            fl := True;
            break;
          end;
        end;

        if not fl then
          s := s + ', t.' + Flds[i];
      end;

      h := 'SELECT ' + s + ' FROM ' + f +' WHERE ' + w;
      sbTbl := RWCallBack.RWEngine.OpenSBSQL(h, []);
      RWCallBack.RWEngine.CopySBTable(sbTbl, imTblFileName);
      sbTbl := nil;
    end;
  end;


  procedure ConsolNoMerge;
  var
    MapTbl: TlqConsolTable;
    s, tbl: String;
  begin
    if Assigned(FConsolTables.FindTable(CachedTableImage.CachedTable.ddTable.GetTableExternalName)) then
      Exit;

    if Length(FConsolChldCOStr) = 0 then
      Exit;

    MapTbl := FConsolTables.AddTable(CachedTableImage.CachedTable.ddTable.GetTableName);
    tbl := CachedTableImage.GetSBTable.TableFileName;

    s := 'INSERT INTO ''' + MapTbl.FSBTable + ''' (old_key, new_key) SELECT ' +
      CachedTableImage.CachedTable.ddTable.GetPrimaryKey + ', ' + CachedTableImage.CachedTable.ddTable.GetPrimaryKey +
      ' FROM ''' + tbl + ''' WHERE CO_NBR = ' + IntToStr(FConsolMainCoNbr);
    RWCallBack.RWEngine.ExecSBSQL(s, []);

    s := 'INSERT INTO ''' + MapTbl.FSBTable + ''' (old_key, new_key) SELECT ' +
      CachedTableImage.CachedTable.ddTable.GetPrimaryKey + ', 0 FROM ''' + tbl +
      ''' WHERE CO_NBR IN (' + FConsolChldCOStr + ')';
    RWCallBack.RWEngine.ExecSBSQL(s, []);

    s := 'DELETE FROM ''' + tbl + ''' WHERE CO_NBR IN (' + FConsolChldCOStr + ')';
    RWCallBack.RWEngine.ExecSBSQL(s, []);
  end;


  procedure ConsolSimpleMerge;
  var
    i, j: Integer;
    s: String;
    Flds, ExtFlds: TrwStrList;
    evT: TddTableClass;
    RL: TddReferencesDesc;
    RI: TddReferenceDesc;
  begin
    Flds := nil;
    ExtFlds := nil;
    RL := nil;

    if FConsolChldCOStr = '' then
      Exit;

    Flds := CachedTableImage.CachedTable.GetFields;
    ExtFlds := CachedTableImage.CachedTable.GetExternalFields;

    evT := GetddTableClassByName(CachedTableImage.CachedTable.ddTable.GetTableExternalName);
    RL := evT.GetParentsDesc;
    for i := Low(RL) to High(RL) do
    begin
      RI := RL[i];
      if RI.Table.ClassInfo = TCO.ClassInfo then
      begin
        for j := Low(Flds) to High(Flds) do
          if AnsiSameText(RI.SrcFields[0], ExtFlds[j]) then
          begin
            s := 'UPDATE ''' + CachedTableImage.GetSBTable.TableFileName + ''' SET ' + Flds[j] + ' = ' + IntToStr(FConsolMainCoNbr) +
                 ' WHERE ' + Flds[j] + ' IN (' + FConsolChldCOStr + ')';

            RWCallBack.RWEngine.ExecSBSQL(s, []);

            break;
          end;
      end;
    end;
  end;


  procedure ConsolMergeNoChanges;
  var
    MapTbl: TlqConsolTable;
    Tbl1, Tbl2: IrwSBTable;
    s, f: String;
    i, j: Integer;
    V: array of String;
    NewVal: Integer;
    cofld: String;
    Flds, ExtFlds: TrwStrList;
    tbl: String;
    LogKey: TrwStrList;
    sbFlds1, sbFlds2: TrwSBFieldList;
  begin
    sbFlds1 := nil;
    sbFlds2 := nil;
    Flds := nil;
    ExtFlds := nil;

    if Length(FConsolChldCOStr) = 0 then
      Exit;

    MakeLogKeyArr(CachedTableImage.CachedTable.ddTable, Flds);
    UpdateLookupFields(Flds);

    if Assigned(FConsolTables.FindTable(CachedTableImage.CachedTable.ddTable.GetTableExternalName)) then
      Exit;

    if CachedTableImage.CachedTable.ddTable.GetPrimaryKey = '' then
      raise ErwException.CreateFmt(ResErrNoKey, [CachedTableImage.CachedTable.ddTable.GetTableName]);

    if CachedTableImage.CachedTable.ddTable.GetLogicalKey = '' then
      raise ErwException.CreateFmt(ResErrNoLogKey, [CachedTableImage.CachedTable.ddTable.GetTableName]);

    tbl := CachedTableImage.GetSBTable.TableFileName;

    ExtFlds := CachedTableImage.CachedTable.GetExternalFields;
    Flds := CachedTableImage.CachedTable.GetFields;
    cofld := '';
    for i := Low(ExtFlds) to High(ExtFlds) do
      if AnsiSameStr(ExtFlds[i], 'CO_NBR') then
      begin
        cofld := Flds[i];
        break;
      end;

    if cofld <> '' then
    begin
      s := 'UPDATE ''' + tbl + ''' SET ' + cofld + ' = -1  WHERE ' + cofld + ' = ' + IntToStr(FConsolMainCoNbr);
     RWCallBack.RWEngine.ExecSBSQL(s, []);
    end;


    MapTbl := FConsolTables.AddTable(CachedTableImage.CachedTable.ddTable.GetTableName);

    MakeLogKeyArr(CachedTableImage.CachedTable.ddTable, LogKey);
    SetLength(V, Length(LogKey));

    Tbl2 := RWCallBack.RWEngine.OpenSBTable(MapTbl.FSBTable, False);
    try
      f := CachedTableImage.CachedTable.ddTable.GetLogicalKey;

      if cofld <> '' then
        f := f + ', ' + cofld;

      s := 'SELECT ' + CachedTableImage.CachedTable.ddTable.GetPrimaryKey + ', ' + f + ' FROM ''' + tbl + '''';
      if cofld <> '' then
        s := s + ' WHERE ' + cofld + ' IN (-1, ' + FConsolChldCOStr + ')';
      s := s + ' ORDER BY ' + f;

      Tbl1 := RWCallBack.RWEngine.OpenSBSQL(s, []);

      NewVal := 0;
      sbFlds1 := Tbl1.GetFields;
      sbFlds2 := Tbl2.GetFields;
      Tbl1.First;
      while not Tbl1.IsEOF do
      begin
        for i := Low(LogKey) to High(LogKey) do
          if not AnsiSameStr(V[i], sbFlds1[i + 1].GetAsString) then
          begin
            for j := Low(LogKey) to High(LogKey) do
              V[j] := sbFlds1[j + 1].GetAsString;
            NewVal := sbFlds1[0].GetAsInteger;
            break;
          end;

        Tbl2.Append;
        sbFlds2[0].SetAsInteger(sbFlds1[0].GetAsInteger);
        sbFlds2[1].SetAsInteger(NewVal);
        Tbl2.Post;

        Tbl1.Next;
      end;

      sbFlds1 := nil;
      sbFlds2 := nil;
      Tbl1 := nil;
      Tbl2 := nil;

      s := 'SELECT t1.* FROM ''' + tbl + ''' t1, ' +
           '(SELECT DISTINCT new_key FROM ''' + MapTbl.FSBTable + ''') t2 ' +
           'WHERE t1.' + CachedTableImage.CachedTable.ddTable.GetPrimaryKey + ' = t2.new_key';

      Tbl1 := RWCallBack.RWEngine.OpenSBSQL(s, []);

      if cofld <> '' then
      begin
        s := 'SELECT * FROM ''' + tbl + ''' WHERE CO_NBR NOT IN (-1, ' + FConsolChldCOStr + ')';
        Tbl2 := RWCallBack.RWEngine.OpenSBSQL(s, []);

        sbFlds1 := Tbl1.GetFields;
        sbFlds2 := Tbl2.GetFields;

        while not Tbl2.IsEOF do
        begin
          Tbl1.Append;
          for i := Low(sbFlds2) to High(sbFlds2) do
            sbFlds1[i].SetValue(sbFlds2[i].GetValue);
          Tbl1.Post;
          Tbl2.Next;
        end;
      end;

      sbFlds1 := nil;
      sbFlds2 := nil;

      RWCallBack.RWEngine.CopySBTable(Tbl1, tbl);

    finally
      V := nil;
      Tbl1 := nil;
      Tbl2 := nil;
    end;

    if cofld <> '' then
    begin
      s := 'UPDATE ''' + tbl + ''' SET ' + cofld + ' = ' + IntToStr(FConsolMainCoNbr) +
           ' WHERE ' + cofld + ' IN (-1, ' + FConsolChldCOStr + ')';
      RWCallBack.RWEngine.ExecSBSQL(s, []);
    end;
  end;


  procedure ConsolMergeWithChanges;
  var
    i: Integer;
    s: String;
    Tbl1: IrwSBTable;
    fCo, fKey: IrwSBField;
    tbl: String;
    ConsRec: TrwConsolidationInfoRec;
    fdt: TrwDDType;
    Flds: TrwStrList;
  begin
    if Length(FConsolChldCOStr) = 0 then
      Exit;

    MakeLogKeyArr(CachedTableImage.CachedTable.ddTable, Flds);
    UpdateLookupFields(Flds);

    ConsRec := GetConsolInfo(CachedTableImage.CachedTable.ddTable.GetTableExternalName);

    if Assigned(FConsolTables.FindTable(CachedTableImage.CachedTable.ddTable.GetTableExternalName)) or
       (ConsRec.ConsFieldToChange = '') then
      Exit;

    tbl := CachedTableImage.GetSBTable.TableFileName;

    Tbl1 := RWCallBack.RWEngine.OpenSBTable(tbl, False);
    try
      Tbl1.SetFilter('CO_NBR in (' + FConsolChldCOStr + ')');
      Tbl1.SetFiltered(True);
      Tbl1.First;
      fCo := Tbl1.GetFieldByName('CO_NBR');
      fKey := Tbl1.GetFieldByName(ConsRec.ConsFieldToChange);
      fdt := fKey.GetDataType;
      while not Tbl1.IsEOF do
      begin
        i := FConsolChldCo.IndexOf(Pointer(fCo.GetAsInteger));

        Tbl1.Edit;

        if fdt = ddtString then
          fKey.SetAsString(fKey.GetAsString + Chr(Ord('A') + i))
        else if fdt = ddtInteger then
          fKey.SetAsInteger(fKey.GetAsInteger + (i + 1) * 100)
        else
          raise ErwException.Create(ResErrWrongLogKeyType);

        Tbl1.Post;

        Tbl1.Next;
      end;
    finally
      fCo := nil;
      fKey := nil;
      Tbl1 := nil;
    end;

    s := 'UPDATE ''' + tbl + ''' SET CO_NBR = ' + IntToStr(FConsolMainCoNbr) +
         ' WHERE CO_NBR IN (' + FConsolChldCOStr + ')';
    RWCallBack.RWEngine.ExecSBSQL(s, []);
  end;

var
  T: TddTableClass;
  ConsolInfo: TrwConsolidationInfoRec;

begin
  if AnsiSameText(CachedTableImage.CachedTable.ddTable.GetTableExternalName, cDynamicFieldsTable) then
    Exit;

//                              Consolidation

  T := GetddTableClassByName(CachedTableImage.CachedTable.ddTable.GetTableExternalName);
  if not T.GetIsHistorical or (FConsolidationNbr = 0) then
    Exit;

  ConsolInfo := GetConsolInfo(T.GetTableName);

  case ConsolInfo.ConsolType of
    ctNoMerge:           ConsolNoMerge;
    ctSimpleMerge:       ConsolSimpleMerge;
    ctMergeNoChanges:    ConsolMergeNoChanges;
    ctMergeWithChanges:  ConsolMergeWithChanges;
  end;

  UpdateLookupFields(CachedTableImage.CachedTable.GetFields);

  if ConsolInfo.ConsolType = ctNone then
    case CheckLogicalKey(CachedTableImage.CachedTable.ddTable) of
      ctNoMerge:           ConsolNoMerge;
      ctMergeNoChanges:    ConsolMergeNoChanges;
      ctMergeWithChanges:  ConsolMergeWithChanges;
    end;
end;

procedure TEvRWAdapter.RWCallBackBeforeCacheTable(const CachedTable: IrwCachedTable; const Params: array of TrwParamRec);
var
  F: TrwStrList;
  i: Integer;
  Fld: IrwDDField;
  fl: Boolean;
begin
  // Force to cache CO_NBR field
  Fld := CachedTable.ddTable.GetField('CO_NBR');

  if Assigned(Fld) then
  begin
    fl := False;
    F := CachedTable.GetExternalFields;
    for i := Low(F) to High(F) do
      if F[i] = 'CO_NBR' then
      begin
        fl := True;
        break;
      end;

    if not fl then
    begin
      F := CachedTable.GetFields;
      SetLength(F, High(F) - Low(F) + 2);
      F[High(F)] := Fld.GetFieldName;
      CachedTable.SetFields(F);
    end;
  end;
end;

procedure TEvRWAdapter.RWCallBackDBCacheCleaning(const DataDictExternalTableName: String; var Accept: Boolean);
begin
  if AnsiSameText(DataDictExternalTableName, cDynamicFieldsTable) then
  begin
    Accept := False;
    Exit;
  end;

  if rwASCleaningClientDataCache in FAdapterState then
    Accept := TableOfClientDB(DataDictExternalTableName)
  else
    Accept := True;
end;

procedure TEvRWAdapter.RWCallBackEndDesignerProgressBar;
begin
  if not FUnAttended then
    if FProgressBarCounter > 0 then
    begin
      Dec(FProgressBarCounter);
      EndWait;
      EndWait;
      if FProgressBarCounter = 0 then
        FWaitDialog.Hide;
    end;
end;

procedure TEvRWAdapter.RWCallBackExecuteExternalFunction(const FunctionID: Integer; var Params: array of Variant; out Result: Variant);
var
  sP: String;
begin
  Result := varUnknown;

  case TrwExternalFunctType(FunctionID) of
    rwEFOpenClient:         ExtProcOpenClient(Params[0]);
    rwEFTestClient:
    begin
      sP := '';
      Result := ExtProcTestClient(Params[0], sP);
      PVariant(Pointer(Integer(Params[1])))^ := sP;
    end;

    // ticket 103154 - legacy call means passing 0 as ConsolidationKind
    rwEFSetConsolidationID: ExtProcSetConsolidationNBR(Params[0], 0);

    rwEFEvoConst:           Result := ExtFunctEvoConst(Params[0]);
    rwEFEDTypes:            Result := ExtFunctEDTypes(Params[0]);
    rwEFStringToArray:      Result := ExtFunctStringToArray(Params[0]);
    rwEFArrayToString:      Result := ExtFunctArrayToString(Params[0]);
    rwEFGetOpenedClient:    Result := ExtFunctGetOpenedClient;
    rwEFGetConsolidationID: Result := ExtFunctGetConsolidationNBR;
    rwEFGetConsolidationMainCompany: Result := ExtFunctGetConsolidationMainCompany;
    rwEFSetCurrentCompany:  ExtProcOpenCompany(Params[0]);
    rwEFGetCurrentCompany:  Result := ExtFunctGetOpenedCompany;
    rwEFAdjustedDateIfHoliday: Result := ExtFunctAdjustedDateIfHoliday(Params[0], Params[1], Params[2]);
    rwEFEvoTranslateField:  Result := ExtFunctEvoTranslateField(Params[0], Params[1], Params[2]);
    rwEFGetSecurityScheme:  Result := ExtFunctGetSecurityScheme;
    rwEFCompressData:       Result := ExtFunctCompressData(Params[0]);
    rwEFDecompressData:     Result := ExtFunctDecompressData(Params[0]);
    rwEFGetEvoUserName:     Result := ExtFunctGetEvoUserName;
    rwEFTextToEMF:          Result := ExtFunctTextToEMF(Params[0], Params[1]);
    rwEFGetSecurityElement: Result := ExtFunctGetSecurityElement(Params[0], Params[1]);
    rwEFGetEvoUserAccountInfo: Result := ExtFunctGetEvoUserAccountInfo;
    rwEFGetAuditData:       Result := ExtFunctGetAuditData(Params[0], Params[1]);
    rwEFGetAuditNBRCreationData: Result := ExtFunctGetAuditNBRCreationData(Params[0]);
    {
    rwEFRunEvoNativeQuery:     Result := ExtFunctRunEvoNativeQuery(Params[0], Params[1], Params[2]);
    }
    rwEFRunEvoNativeQuery:     Result := null; //ExtFunctRunEvoNativeQuery(Params[0], Params[1], Params[2]); //ticket 102173 - security concern
    // ticket 103154 - new consolidation means passing non-Zero Consolidation Kind
    rwEFSetACAConsolidationID: ExtProcSetConsolidationNBR(Params[0], 1);

    rwEFConsolidationKind: Result := ExtFunctGetConsolidationKind;
    // ticket 105183 - audit optimization
    rwEFGetAuditChanges: Result := ExtFunctGetAuditChanges(Params[0], Params[1]);
    //rwEFGetAuditBlob: Result := ExtFunctGetAuditBlob( Params[0], Params[1], Params[2]);  // const ATable, AField: String; const ARefValue: Integer): IisStream;
  end;

end;

procedure TEvRWAdapter.RWCallBackGetCurrentDateTime(var ADateTime: TDateTime);
begin
  ADateTime := SysTime;
end;

procedure TEvRWAdapter.RWCallBackGetDDFieldInfo(const ATable, AField: String; out FieldInfo: TrwDDFieldInfoRec);
var
  T: TddTableClass;
  V: TevFieldValues;
  FV: TevFieldValuesRec;
  i: Integer;
begin
  if AnsiSameText(ATable, cDynamicFieldsTable) then
  begin
    if AnsiSameText(AField, 'Table_Name') then
    begin
      FieldInfo.Name := AField;
      FieldInfo.DisplayName := 'Evolution Table Name';
      FieldInfo.FieldType := ddtString;
      FieldInfo.Size := 32;
      FieldInfo.FieldValues := '';
    end
    else if AnsiSameText(AField, 'Field_Name') then
    begin
      FieldInfo.Name := AField;
      FieldInfo.DisplayName := 'Evolution Field Name';
      FieldInfo.FieldType := ddtString;
      FieldInfo.Size := 32;
      FieldInfo.FieldValues := '';
    end;

    Exit;
  end;

  T := GetddTableClassByName(ATable);

  if T.GetIsHistorical then
  begin
    FieldInfo.Name := '';
    if AField = 'EFFECTIVE_DATE' then
    begin
      FieldInfo.Name := 'Effective_Date';
      FieldInfo.DisplayName := 'Effective period start date';
      FieldInfo.FieldType := ddtDateTime;
      FieldInfo.Size := 0;
      FieldInfo.FieldValues := '';
    end
    else if AField = 'EFFECTIVE_UNTIL' then
    begin
      FieldInfo.Name := 'Effective_Until';
      FieldInfo.DisplayName := 'Effective period end date';
      FieldInfo.FieldType := ddtDateTime;
      FieldInfo.Size := 0;
      FieldInfo.FieldValues := '';
    end
    else if AField = 'REC_VERSION' then
    begin
      FieldInfo.Name := 'Rec_Version';
      FieldInfo.DisplayName := 'Record version surrogate key';
      FieldInfo.FieldType := ddtInteger;
      FieldInfo.Size := 0;
      FieldInfo.FieldValues := '';
    end;

    if FieldInfo.Name <> '' then
      Exit;
  end;

  FieldInfo.Name := FineName(AField, False);
  FieldInfo.DisplayName := T.GetFieldDisplayLabel(AField);
  if FieldInfo.DisplayName = '' then
    FieldInfo.DisplayName := StringReplace(FieldInfo.Name, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
  FieldInfo.Size := T.GetFieldSize(AField);

  // Exception for consolidated tables
  if SameText(FieldInfo.Name, 'CUSTOM_EMPLOYEE_NUMBER') then
    FieldInfo.Size := FieldInfo.Size + 1;

  V := FieldCodeValues.FindFields(T);
  if Assigned(V) then
  begin
    i := V.FieldIndex(AField);
    if i <> -1 then
    begin
      FV := V[i];
      FieldInfo.FieldValues := GetConstChoisesByField(ATable + '_' + AField);
    end;
  end;

  FieldInfo.FieldType := ConvertDBTypeToRWOne(T.GetFieldType(AField));
  if (FieldInfo.FieldType = ddtString) and (FieldInfo.Size > 255) then
  begin
    FieldInfo.FieldType := ddtMemo;
    FieldInfo.Size := 0;
  end;
end;

procedure TEvRWAdapter.RWCallBackGetDDFields(const ATable: String; out Fields: String);
var
  T: TddTableClass;
  F: TddFieldList;
  i: Integer;
  RestrictedFields: IisStringList;
begin
  Fields := '';

  if AnsiSameText(ATable, cDynamicFieldsTable) then
  begin
    Fields := 'Table_Name,Field_Name';
    Exit;
  end;

  T := GetddTableClassByName(ATable);
  F := T.GetFieldList;

  RestrictedFields := GetSecuredFields;
//  RestrictedFields.Add('Sb_User.Password_Change_Date'); // reso #104796
  RestrictedFields.Add('Sb_User.Wrong_Pswd_Attempts');

  for i := Low(F) to High(F) do
    if RestrictedFields.IndexOf(ATable + '.' + F[i]) = -1 then
      Fields := Fields + ',' + F[i];

  if T.GetIsHistorical then
    Fields := Fields + ',EFFECTIVE_DATE,EFFECTIVE_UNTIL,REC_VERSION';

  if Fields <> '' then
    Delete(Fields, 1, 1);
end;

procedure TEvRWAdapter.RWCallBackGetDDParamInfo(const ATable, Param: String; out ParmInfo: TrwDDParamInfoRec);
var
 T: TddTableClass;
 P: TddParamList;
 i: Integer;
begin
  T := GetddTableClassByName(ATable);

  P := T.GetParameters;
  for i := Low(P) to High(P) do
    if AnsiSameText(P[i].Name, Param) then
    begin
      ParmInfo.Name := FineName(Param, False);
      ParmInfo.ParamType := ConvertDBTypeToRWOne(P[i].ParamType);

      if AnsiSameText(ParmInfo.Name, 'EFFECTIVE_DATE') then
      begin
        ParmInfo.DisplayName := 'As Of Date';
        ParmInfo.ParamValues := 'null=Historical Data' + #13 +
                                'now=Curent Data';
        ParmInfo.DefaultValue := 'now';
      end;
      break;
    end;
end;

procedure TEvRWAdapter.RWCallBackGetDDTableInfo(const ATable: String; out TableInfo: TrwDDTableInfoRec);
var
  T: TddTableClass;
  F: TddFieldList;
  i, j: Integer;
  h: String;
  RL: TddReferencesDesc;
  RI: TddReferenceDesc;
  P: TddParamList;

  procedure ExceptionalFK;
  begin
    if T = TCO then
    begin
      TableInfo.ForeignKey :=  TableInfo.ForeignKey + #13 + 'CO_STATES=HOME_CO_STATES_NBR' + #13 +
                                                            'CO_STATES=HOME_TAX_EE_STATES_NBR' + #13 +
                                                            'CO_STATES=HOME_SUI_CO_STATES_NBR';
    end
    else if T = TEE then
    begin
      TableInfo.ForeignKey :=  TableInfo.ForeignKey + #13 + 'EE_STATES=HOME_TAX_EE_STATES_NBR';
    end;

    if T.GetIsHistorical then
    begin
      if TableInfo.ForeignKey <> '' then
        TableInfo.ForeignKey := TableInfo.ForeignKey + #13;
      TableInfo.ForeignKey := TableInfo.ForeignKey + 'SB_USER=CHANGED_BY';
    end;
  end;

begin
  if AnsiSameText(ATable, cDynamicFieldsTable) then
  begin
    TableInfo.Name := ATable;
    TableInfo.DisplayName := 'Dynamic Fields';
    TableInfo.PrimaryKey := '';
    TableInfo.LogicalKey := '';
    TableInfo.ForeignKey := '';
    TableInfo.Params := '';
    Exit;        
  end;

  T := GetddTableClassByName(ATable);

  TableInfo.Name := FineName(ATable, False);
  TableInfo.DisplayName := StringReplace(TableInfo.Name, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
  TableInfo.PrimaryKey := ATable + '_NBR';

  F := T.GetLogicalKeys;
  h := '';
  for i := Low(F) to High(F) do
    h := h + ',' + F[i];
  if h <> '' then
    Delete(h, 1, 1);
  TableInfo.LogicalKey := h;

  TableInfo.ForeignKey := '';
  RL := T.GetParentsDesc;
  for i := Low(RL) to High(RL) do
  begin
    RI := RL[i];
    h := RI.Table.GetTableName + '=';

    for j := Low(RI.SrcFields) to High(RI.SrcFields) do
    begin
      if j > Low(RI.SrcFields) then
        h := h + ',';
      h := h + RI.SrcFields[j];
    end;

    if i > Low(RL) then
      TableInfo.ForeignKey := TableInfo.ForeignKey + #13;

    TableInfo.ForeignKey := TableInfo.ForeignKey + h;
  end;

  ExceptionalFK;

  TableInfo.Params := '';
  P := T.GetParameters;
  for i := Low(P) to High(P) do
  begin
    if i > Low(P) then
      TableInfo.Params := TableInfo.Params + ',';
    TableInfo.Params := TableInfo.Params + P[i].Name;
  end;
end;

procedure TEvRWAdapter.RWCallBackGetDDTables(out Tables: String);
var
  T: TddTableList;

  procedure AddTablesToResult;
  var
    i: Integer;
  begin
    for i := Low(T) to High(T) do
      AddStrValue(Tables, T[i], ',');
  end;

begin
  Tables := '';

  T := TDM_SYSTEM.GetTableList;
  AddTablesToResult;

  T := TDM_BUREAU.GetTableList;
  AddTablesToResult;

  T := TDM_CLIENT.GetTableList;
  AddTablesToResult;

  T := TDM_TEMPORARY.GetTableList;
  AddTablesToResult;

  AddStrValue(Tables, cDynamicFieldsTable, ',');
end;

procedure TEvRWAdapter.RWCallBackGetExternalFunctionDeclarations(const FunctionHeaders: TStringList);
begin
  with FunctionHeaders do
  begin
    AddObject('function GetSecurityScheme: Variant', Pointer(Ord(rwEFGetSecurityScheme)));
    AddObject('procedure OpenClient (AClientIntNBR: Integer)', Pointer(Ord(rwEFOpenClient)));
    AddObject('function TestClient (AClientIntNBR: Integer; var AError: String): Boolean', Pointer(Ord(rwEFTestClient)));
    AddObject('procedure SetConsolidationNBR (AConsolidationNBR : Integer)', Pointer(Ord(rwEFSetConsolidationID)));
    AddObject('function EvoConstant (AConstantName: String): String', Pointer(Ord(rwEFEvoConst)));
    AddObject('function EDTypes(AType: string): String', Pointer(Ord(rwEFEDTypes)));
    AddObject('function StringToArray(AString: String): Array', Pointer(Ord(rwEFStringToArray)));
    AddObject('function GetOpenedClient: Integer', Pointer(Ord(rwEFGetOpenedClient)));
    AddObject('function GetConsolidationNBR: Integer', Pointer(Ord(rwEFGetConsolidationID)));
    AddObject('function GetConsolidationMainCompany: Integer', Pointer(Ord(rwEFGetConsolidationMainCompany)));
    AddObject('procedure SetCurrentCompany(ACompanyIntNBR: Integer)', Pointer(Ord(rwEFSetCurrentCompany)));
    AddObject('function GetCurrentCompany: Integer', Pointer(Ord(rwEFGetCurrentCompany)));
    AddObject('function AdjustedDateIfHoliday(ADate: Date; ANbrOfDays: Integer; SY_GLOBAL_AGENCY_NBR: Integer): Date', Pointer(Ord(rwEFAdjustedDateIfHoliday)));
    AddObject('function EvoTranslateField(ATable: String; AField: String; AFieldValue: Variant): Array', Pointer(Ord(rwEFEvoTranslateField)));
    AddObject('function ArrayToString(AArray: Array): String', Pointer(Ord(rwEFArrayToString)));
    AddObject('function CompressData(AArray: Array): Array', Pointer(Ord(rwEFCompressData)));
    AddObject('function DecompressData(AArray: Array): Array', Pointer(Ord(rwEFDecompressData)));
    AddObject('function GetEvoUserName: Variant', Pointer(Ord(rwEFGetEvoUserName)));
    AddObject('function TextToEMF(AText: String; AFontInfo: String): Array', Pointer(Ord(rwEFTextToEMF)));
    AddObject('function GetSecurityElement(AType: String; AName: String): String', Pointer(Ord(rwEFGetSecurityElement)));
    AddObject('function GetEvoUserAccountInfo: Array', Pointer(Ord(rwEFGetEvoUserAccountInfo)));
    AddObject('function GetAuditData(AFields: String; ABeginDate: Date): Array', Pointer(Ord(rwEFGetAuditData)));
    AddObject('function GetAuditNbrCreationData(ATables: String): Variant', Pointer(Ord(rwEFGetAuditNBRCreationData)));
    {
    AddObject('function RunEvoNativeQuery(ASQL: String; AParamNames: Array; AParamValues: Array): Variant', Pointer(Ord(rwEFRunEvoNativeQuery))); //// ticket 102173 - security concern
    }
// Ticket 103154 - consolidation by ACA
    AddObject('procedure SetACAConsolidationNBR (AConsolidationNBR : Integer)', Pointer(Ord(rwEFSetACAConsolidationID)));
    AddObject('function GetConsolidationKind:integer', Pointer(Ord(rwEFConsolidationKind)));
    AddObject('function GetAuditChanges(AFields: String; ABeginDate: Variant): Array', Pointer(Ord(rwEFGetAuditChanges)));
  end;
end;

procedure TEvRWAdapter.RWCallBackGetLicencePassword(const ALicenceID: String; var APassword: String);
const
  Passwords: array [1..3, 1..2] of string = (('CTS1', 'zcLs"''4Bp.!Pt4>,&Hw^uU%=@bzj Ys#'),
                                             ('CTS2', '!.zjtk] "4K$77/V*$:j%3FD\iZRAt&A'),
                                             ('CTS3', 'E3FFY8&Ku"nUhSj&x]K">NIVSnBl #S'''));
var
  i: Integer;
begin
  APassword := '';
  for i := Low(Passwords) to High(Passwords) do
    if Passwords[i][1] = ALicenceID then
    begin
      if FCachedCTSLicense then
        APassword := Passwords[i][2];
      break;
    end;

  if (APassword = '') and IsDebugMode and not FUnAttended then
    EvDialog('Password', 'Password', APassword, True);

  if APassword = '' then
    raise ErwException.Create(ResErrAccessDenied);
end;

procedure TEvRWAdapter.RWCallBackGetTempDir(out TempDir: String);
begin
  TempDir := AppTempFolder;
end;

procedure TEvRWAdapter.RWCallBackLoadDataDictionary(Data: TStream);
begin
  FDataDictionaryReadVersion := LoadStreamFromStorage(Data, RW_DATA_DICTIONARY);
end;

procedure TEvRWAdapter.RWCallBackLoadLibraryComponents(Data: TStream);
begin
  FLibComponentsReadVersion := LoadStreamFromStorage(Data, RW_LIB_COMPONENTS);
end;

procedure TEvRWAdapter.RWCallBackLoadLibraryFunctions(Data: TStream);
begin
  FLibFunctionsReadVersion := LoadStreamFromStorage(Data, RW_LIB_FUNCTIONS);
end;

procedure TEvRWAdapter.RWCallBackLoadQBTemplates(Data: TStream);
begin
  FQBTemplatesReadVersion := LoadStreamFromStorage(Data, RW_QB_TEMPLATES);
end;

procedure TEvRWAdapter.RWCallBackLoadSQLConstants(Data: TStream);
begin
  FQBConstantsReadVersion := LoadStreamFromStorage(Data, RW_QB_CONSTANTS);
end;

procedure TEvRWAdapter.RWCallBackStartDesignerProgressBar(const Title: String; MaxValue: Integer);
begin
  if not FUnAttended then
  begin
    Inc(FProgressBarCounter);
    StartWait(Title);
    StartWait(' ', MaxValue);
  end;
end;

procedure TEvRWAdapter.RWCallBackSubstDefaultClassName(var ADefaultClassName: String);
const
  Subst = 'TrmReport=TrmlRCustom,'+

          'TrmPrintForm=TrmlCustomPrintForm,'+
          'TrmPrintText=TrmlCustomPrintText,'+
          'TrmPrintPanel=TrmlCustomPrintPanel,'+
          'TrmPrintImage=TrmlCustomPrintImage,'+
          'TrmTable=TrmlCustomTable,'+
          'TrmTableCell=TrmlCustomTableCell,'+
          'TrmDBTable=TrmlCustomDBTable,'+

          'TrwInputFormContainer=TrmlCustomInputForm,'+
          'TrwText=TrmlCustomLabel,'+
          'TrwPanel=TrmlCustomPanel,'+
          'TrwEdit=TrmlCustomEditBox,'+
          'TrwDateEdit=TrmlCustomDateTimeComboBox,'+
          'TrwComboBox=TrmlCustomComboBox,'+
          'TrwDBComboBox=TrmlCustomDBComboBox,'+
          'TrwCheckBox=TrmlCustomCheckBox,'+
          'TrwGroupBox=TrmlCustomGroupBox,'+
          'TrwDBGrid=TrmlCustomDBGrid,'+
          'TrwRadioGroup=TrmlCustomRadioGroup,'+
          'TrwPageControl=TrmlCustomPageControl,'+
          'TrwButton=TrmlCustomButton,'+

          'TrmASCIIForm=TrmlCustomASCIIForm,'+
          'TrmASCIIRecord=TrmlCustomASCIIRecord,' +
          'TrmASCIIField=TrmlCustomASCIIField,' +
          'TrmXMLForm = TrmlCustomXMLForm';
var
  L: TStringList;
  h: String;
begin
  L := TStringList.Create;
  L.CommaText := Subst;
  h := L.Values[ADefaultClassName];
  if h <> '' then
    ADefaultClassName := h;
end;

procedure TEvRWAdapter.RWCallBackSubstituteValue(var AValue: Variant; const SubstID: String);
begin
  FSubstValues.Text := GetConstChoisesByField(SubstID);
  AValue := FSubstValues.Values[VarToStr(AValue)];
end;

procedure TEvRWAdapter.RWCallBackTableDataRequest(const CachedTable: IrwCachedTable; const sbTable: IrwSBTable; const Params: array of TrwParamRec);
var
  i: Integer;
  DBType: TevDBType;
  Q: IevQuery;
  FieldValues: TisDynVariantArray;

  function CreatePhysicalQuery: string;
  var
    hs, hf, whereCond: string;
    i: Integer;
    flds: TrwStrList;
    fltr: String;
    CoNbrExists: Boolean;
    ClDBTable: Boolean;
  begin
    flds := CachedTable.GetExternalFields;
    fltr := CachedTable.GetFilter;
    ClDBTable := TableOfClientDB(CachedTable.ddTable.GetTableExternalName);

    hs := '';
    CoNbrExists := False;
    for i := Low(flds) to High(flds) do
    begin
      if i > Low(flds) then
        hs := hs + ',';
      hs := hs + flds[i];

      if ClDBTable and (FCompanyNBR > 0) then
        CoNbrExists := CoNbrExists or SameText(flds[i], 'CO_NBR');
    end;

    if CoNbrExists then
    begin
      if Fltr <> '' then
        Fltr := '(' + Fltr + ') AND ';
      Fltr := Fltr + 'CO_NBR = ' + IntToStr(FCompanyNBR);
    end;

    hf := CachedTable.ddTable.GetTableExternalName;
    whereCond := '';
    if Length(Params) > 0 then
    begin
      if GetddTableClassByName(hf).GetIsHistorical then
      begin
        if not VarIsNull(Params[0].Value) then
          whereCond := '{GenericAsOfDate<:' + Params[0].Name + ',' + hf +'>}';
      end
      else
      begin
        hf := hf + '(';
        for i := Low(Params) to High(Params) do
        begin
          if i > Low(Params) then
            hf := hf + ',';
          hf := hf + ':' + Params[i].Name;
        end;
        hf := hf + ')';
      end;
    end;

    if (Fltr <> '') and not (ClDBTable and (FConsolidationNbr <> 0)) then
      if whereCond = '' then
        whereCond := Fltr
      else
        AddStrValue(whereCond, '(' + Fltr + ')', ' AND ');

    if whereCond <> '' then
      whereCond := 'WHERE ' + whereCond;

    Result := 'SELECT ' + hs + ' FROM ' + hf + ' ' + whereCond;
  end;


  procedure CreateVersionedFieldsDataResult;
  var
    T: TddTableList;
    flds: TrwStrList;

    procedure AddTablesToResult;
    var
      i, j, k: Integer;
      TblClass: TddTableClass;
      L: IisStringList;
    begin
      for i := Low(T) to High(T) do
      begin
        TblClass := GetddTableClassByName(T[i]);
        if TblClass.IsVersionedTable then
        begin
          L := TblClass.GetFieldNameList(fmVersioned);
          for j := 0 to L.Count - 1 do
          begin
            for k := Low(flds) to High(flds) do
              if AnsiSameText(flds[k], 'Table_Name') then
                FieldValues[k] := TblClass.GetTableName
              else if AnsiSameText(flds[k], 'Field_Name') then
                FieldValues[k] := L[j];

            sbTable.AppendRecord(FieldValues);
          end;
        end;
      end;
    end;

  begin
    flds := CachedTable.GetExternalFields;
    SetLength(FieldValues, Length(flds));

    T := TDM_SYSTEM.GetTableList;
    AddTablesToResult;

    T := TDM_BUREAU.GetTableList;
    AddTablesToResult;

    T := TDM_CLIENT.GetTableList;
    AddTablesToResult;
  end;

begin
  if AnsiSameText(CachedTable.ddTable.GetTableExternalName, cDynamicFieldsTable) then
  begin
    CreateVersionedFieldsDataResult;
    Exit;
  end;

  DBType := GetDBTypeByTableName(CachedTable.ddTable.GetTableExternalName);

  if DBType = dbtClient then
  begin
    if not (rsClientOpened in FReportStatus) then
      raise ErwException.Create('Client context is not defined');

    if not (rsCompanyOpened in FReportStatus) and (FCompanyNBR <> 0) or not (rsConsolidationMode in FReportStatus) and (FConsolidationNBR <> 0) then
      CleanUpContextTables;
  end;


  //Execure query
  Q := TevQuery.Create(CreatePhysicalQuery);

  for i := Low(Params) to High(Params) do
    if not VarIsArray(Params[i].Value) then
      Q.Params.AddValue(Params[i].Name, Params[i].Value);

  Q.Execute;
  SetLength(FieldValues, Q.Result.Fields.Count);

  while not Q.Result.Eof do
  begin
    for i := 0 to Q.Result.Fields.Count - 1 do
      FieldValues[i] := Q.Result.Fields[i].Value;

    sbTable.AppendRecord(FieldValues);
    Q.Result.Next;
  end;
end;

procedure TEvRWAdapter.RWCallBackUnloadDataDictionary(Data: TStream);
begin
  SaveStreamToStorage(Data, RW_DATA_DICTIONARY, FDataDictionaryReadVersion);
end;

procedure TEvRWAdapter.RWCallBackUnloadLibraryComponents(Data: TStream);
begin
  SaveStreamToStorage(Data, RW_LIB_COMPONENTS, FLibComponentsReadVersion);
end;

procedure TEvRWAdapter.RWCallBackUnloadLibraryFunctions(Data: TStream);
begin
  SaveStreamToStorage(Data, RW_LIB_FUNCTIONS, FLibFunctionsReadVersion);
end;

procedure TEvRWAdapter.RWCallBackUnloadQBTemplates(Data: TStream);
begin
  SaveStreamToStorage(Data, RW_QB_TEMPLATES, FQBTemplatesReadVersion);
end;

procedure TEvRWAdapter.RWCallBackUnloadSQLConstants(Data: TStream);
begin
  SaveStreamToStorage(Data, RW_QB_CONSTANTS, FQBConstantsReadVersion);
end;

procedure TEvRWAdapter.RWCallBackUpdateDesignerProgressBar(const ProgressText: String; CurrentValue: Integer);
begin
  if not FUnAttended then
    UpdateWait(ProgressText, CurrentValue);
end;


 {TlqConsolTable }

destructor TlqConsolTable.Destroy;
begin
  try
    TEvRWAdapter(TOwnedCollection(Collection).Owner).RWCallBack.RWEngine.ExecSBSQL('DROP TABLE ''' + FSBTable + '''', []);
  finally
    inherited;
  end;
end;


{ TlqConsolTables }

function TlqConsolTables.AddTable(const ATable: string): TlqConsolTable;
begin
  Result := TlqConsolTable(Add);
  Result.FTableExtName := AnsiUpperCase(ATable);

  rwCriticalSection.Enter;
  try
    Result.FSBTable := GetUniqueFileName(TEvRWAdapter(Owner).RWCallBack.RWEngine.DBCache.GetCachePath, 's', '.sbt');
    TEvRWAdapter(Owner).RWCallBack.RWEngine.ExecSBSQL('CREATE TABLE ''' + Result.FSBTable + ''' (old_key INTEGER, new_key INTEGER)', []);
  finally
    rwCriticalSection.Leave;
  end;
end;


function TlqConsolTables.FindTable(const ATable: string): TlqConsolTable;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count -1 do
    if AnsiSameText(ATable, Items[i].FTableExtName) then
    begin
      Result := Items[i];
      break;
    end;
end;

function TlqConsolTables.GetItem(Index: Integer): TlqConsolTable;
begin
  Result := TlqConsolTable(inherited Items[Index]);
end;



procedure TEvRWAdapter.DataModuleCreate(Sender: TObject);
begin
  Application.OnModalBegin := OnShowModalDialog;
  Application.OnModalEnd := OnHideModalDialog;
  Application.OnException := OnException;

  FUnattended := not AnsiSameText(ExtractFileName(Application.ExeName), 'isRWDesigner.exe');

  if not FUnattended then
    FWaitDialog := TAdvancedWait.Create;

  FSubstValues := TStringList.Create;
  FCompanyNBR := 0;
  FConsolidationNBR := 0;
  InitConstList;

  ActivateMainboard;
end;

procedure TEvRWAdapter.DataModuleDestroy(Sender: TObject);
begin
  Application.OnModalBegin := nil;
  Application.OnModalEnd := nil;
  Application.OnException := nil;

  DeactivateMainboard;
  FreeAndNil(FSubstValues);
  FreeAndNil(FEvoConstList);
  FreeandNil(FWaitDialog);
end;

function TEvRWAdapter.TableOfClientDB(const ATableName: String): Boolean;
begin
  Result := GetDBTypeByTableName(ATableName) = dbtClient;
end;

function TEvRWAdapter.ExtFunctAdjustedDateIfHoliday(const ADate: TDateTime;
  const ANbrOfDays, SY_GLOBAL_AGENCY_NBR: Integer): TDateTime;
begin
  if ANbrOfDays > 0 then
  begin
    if ADate <> CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, 0, ADate) then
      Result := CheckForNDaysRule(SY_GLOBAL_AGENCY_NBR, ANbrOfDays, ADate)
    else
      Result := ADate;
  end

  else if ANbrOfDays < 0 then
  begin
    if ADate <> CheckForReversedNDaysRule(SY_GLOBAL_AGENCY_NBR, 0, ADate) then
      Result := CheckForReversedNDaysRule(SY_GLOBAL_AGENCY_NBR, -ANbrOfDays, ADate)
    else
      Result := ADate;
  end

  else
    raise ErwException.Create('Function "AdjustedDateIfHoliday": ANbrOfDays = 0 in not valid parameter value');
end;

function TEvRWAdapter.ExtFunctEDTypes(const AEDTypeName: String): String;
begin
  if AnsiSameText(AEDTypeName, 'TaxedDed') then
    Result := ListSpecTaxedDedTypes
  else if AnsiSameText(AEDTypeName, 'Pension') then
    Result := ListPensionTypes
  else if AnsiSameText(AEDTypeName, '3rdParty') then
    Result := List3rdPartyTypes
  else if AnsiSameText(AEDTypeName, 'TaxableMemo') then
    Result := ListTaxableMemoTypes + ',''ER'''
  else if AnsiSameText(AEDTypeName, 'Tips') then
    Result := ListTipsTypes
  else
    Result := '';
end;

function TEvRWAdapter.ExtFunctEvoConst(const AConstName: String): String;
begin
  Result := FEvoConstList.Values[AnsiUpperCase(AConstName)];
end;

function TEvRWAdapter.ExtFunctEvoTranslateField(const ATable,
  AField: String; const AFieldValue: Variant): Variant;
begin
  Result := TRWTranslator.Translate(ATable, AField, AFieldValue);
end;

function TEvRWAdapter.ExtFunctGetConsolidationMainCompany: Integer;
begin
  Result := FConsolMainCoNbr;
end;

function TEvRWAdapter.ExtFunctGetConsolidationNBR: Integer;
begin
  Result := FConsolidationNBR;
end;

// ticket 103154
function TEvRWAdapter.ExtFunctGetConsolidationKind: Integer;
begin
  Result := FConsolidationKind;
end;


function TEvRWAdapter.ExtFunctGetOpenedClient: Integer;
begin
  Result := ctx_DataAccess.ClientID;
end;

function TEvRWAdapter.ExtFunctGetSecurityScheme: Variant;
var
  SecStruct: IevSecurityStructure;
  Atom: TSecurityAtom;
  DS: TisBasicClientDataSet;
  i, j: integer;
begin
  Result := VarArrayCreate([0, 1], varVariant);

  SecStruct := ctx_Security.GetSecStructure;

  // Atoms
  DS := TisBasicClientDataSet.Create(nil);
  try
    DS.FieldDefs.Add('AtomTag', ftString, 80, False);
    DS.FieldDefs.Add('Caption', ftString, 255, False);
    DS.FieldDefs.Add('Type', ftString, 1, False);
    DS.CreateDataSet;
    DS.Open;

    for i := 0 to SecStruct.Obj.AtomCount- 1 do
      DS.AppendRecord([SecStruct.Obj.Atoms[i].Tag,
                       SecStruct.Obj.Atoms[i].Caption,
                       SecStruct.Obj.Atoms[i].AtomType]);

    Result[0] := DS.Data;
  finally
    DS.Free;
  end;

  // Contexts
  DS := TisBasicClientDataSet.Create(nil);
  try
    DS.FieldDefs.Add('AtomTag', ftString, 80, False);
    DS.FieldDefs.Add('ContextTag', ftString, 80, False);
    DS.FieldDefs.Add('Caption', ftString, 255, False);
    DS.FieldDefs.Add('Priority', ftInteger, 0, False);
    DS.CreateDataSet;
    DS.Open;

    for i := 0 to SecStruct.Obj.AtomCount- 1 do
    begin
      Atom := SecStruct.Obj.Atoms[i];
      for j := 0 to Atom.ContextCount - 1 do
        DS.AppendRecord([Atom.Tag,
                         Atom.Contexts[j].Tag,
                         Atom.Contexts[j].Caption,
                         Atom.Contexts[j].Priority]);
    end;
    Result[1] := DS.Data;
  finally
    DS.Free;
  end;
end;

function TEvRWAdapter.ExtFunctGetOpenedCompany: Integer;
begin
  if FConsolidationNBR = 0 then
    Result := FCompanyNBR
  else
    Result := FConsolMainCoNbr;
end;

function TEvRWAdapter.ExtFunctStringToArray(const AString: String): Variant;
var
  P: PChar;
  StrLen: Integer;
begin
  StrLen := Length(AString);
  if StrLen > 0 then
  begin
    Result := VarArrayCreate([0, StrLen-1], varByte);
    P := VarArrayLock(Result);
    try
      Move(AString[1], p^, StrLen);
    finally
      VarArrayUnlock(Result);
    end;
  end
  else
    Result := VarArrayCreate([0, -1], varByte);
end;

procedure TEvRWAdapter.ExtProcOpenClient(const Cl_Nbr: Integer);
begin
  if ctx_DataAccess.ClientID <> Cl_Nbr then
  begin
    CleanUpContextTables;
    ctx_DataAccess.OpenClient(Cl_Nbr);
  end;

  FReportStatus := FReportStatus + [rsClientOpened];
end;

procedure TEvRWAdapter.ExtProcOpenCompany(const Co_Nbr: Integer);
begin
  if FCompanyNBR <> Co_Nbr then
  begin
    CleanUpContextTables;
    try
      // Check existance of such company
    finally
      FCompanyNBR := Co_Nbr;
      if FCompanyNBR > 0 then
        FReportStatus := FReportStatus + [rsCompanyOpened];
    end;
  end;
end;

procedure TEvRWAdapter.ExtProcSetConsolidationNBR(const AConsolNBR: Integer; AConsolidationKind:Integer);
var
  DS: TevClientDataSet;
begin
  if AConsolNBR <> FConsolidationNBR then
  begin
    CleanUpContextTables;

// ticket 103154
    FConsolidationKind := AConsolidationKind;

    FConsolidationNBR := AConsolNBR;

    if FConsolidationNBR = 0 then
      Exit;

    FConsolChldCo := TList.Create;

    DS := TevClientDataSet.Create(nil);
    try
      DS.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;

      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', 'PRIMARY_CO_NBR');
        SetMacro('TableName', 'CL_CO_CONSOLIDATION');
        // primary consolidation company got defined the same way no matter what kind of consolidation
        SetMacro('Condition', 'CL_CO_CONSOLIDATION_NBR = ' + IntToStr(FConsolidationNBR)
                    );
        DS.DataRequest(AsVariant);
      end;
      DS.Open;

      if DS.Fields[0].IsNull then
        raise ErwException.Create('Data consolidation error! Primary company has been set up incorrectly');
      FConsolMainCoNbr := DS.Fields[0].AsInteger;
      DS.Close;


      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', 'CO_NBR');
        SetMacro('TableName', 'CO');
        // ticket 103154 - search by different field in case of ACA consolidation
        case AConsolidationKind of
        0 : SetMacro('Condition', 'CL_CO_CONSOLIDATION_NBR = ' + IntToStr(FConsolidationNBR) + ' AND CO_NBR <> ' + IntToStr(FConsolMainCoNbr));
        1 : SetMacro('Condition', 'ACA_CL_CO_CONSOLIDATION_NBR = ' + IntToStr(FConsolidationNBR) + ' AND CO_NBR <> ' + IntToStr(FConsolMainCoNbr));
        else
          raise ErwException.Create('Data consolidation error! Wrong consolidaion kind : '+ IntToStr(AConsolidationKind));
        end;
        DS.DataRequest(AsVariant);
      end;
      DS.Open;

      FConsolChldCo.Count := Ds.RecordCount;
      while not DS.Eof do
      begin
        if DS.RecNo > 1 then
          FConsolChldCOStr := FConsolChldCOStr + ', ';
        FConsolChldCOStr := FConsolChldCOStr + DS.Fields[0].AsString;
        FConsolChldCo[DS.RecNo - 1] := Pointer(DS.Fields[0].AsInteger);
        DS.Next;
      end;
    finally
      DS.Free;
    end;

    FConsolTables := TlqConsolTables.Create(Self, TlqConsolTable);
    FReportStatus := FReportStatus + [rsConsolidationMode];
  end;
end;


procedure TEvRWAdapter.OpenClient(const Cl_Nbr: Integer; Co_Nbr: Integer);
begin
  ExtProcOpenClient(Cl_Nbr);
  ExtProcOpenCompany(Co_Nbr);
end;

procedure TEvRWAdapter.CleanUpContextTables;
begin
  Include(FAdapterState, rwASCleaningClientDataCache);
  try
    RWCallBack.RWEngine.DBCache.EmptyCachedTables;
  finally
    Exclude(FAdapterState, rwASCleaningClientDataCache);
  end;

  FCompanyNBR := 0;

  // clean up consolidation
  FConsolidationNBR := 0;
  FConsolChldCOStr := '';
  FConsolMainCoNbr := 0;
// ticket 103154
  FConsolidationKind := 0;
  FreeConsolTables;

  FReportStatus := FReportStatus - [rsCompanyOpened, rsConsolidationMode];
end;

procedure TEvRWAdapter.FreeConsolTables;
begin
  try
    FreeAndNil(FConsolTables);
  finally
    FreeAndNil(FConsolChldCo);
  end;
end;

function TEvRWAdapter.GetConsolInfo(const ATableName: String): TrwConsolidationInfoRec;
var
  i: Integer;
begin
  Result.TableName := ATableName;
  Result.ConsolType := ctNone;
  Result.ConsFieldToChange := '';

  for i := Low(aConsolTable) to High(aConsolTable) do
    if aConsolTable[i].TableName = ATableName then
    begin
      Result := aConsolTable[i];
      break;
    end;
end;

procedure TEvRWAdapter.CacheReportLicenceInfo;
begin
  FCachedCTSLicense := Context.License.CTSReports;
end;

procedure TEvRWAdapter.ClearDBCache;
begin
  if Assigned(RWCallBack.RWEngine) and Assigned(RWCallBack.RWEngine.DBCache) then
    RWCallBack.RWEngine.DBCache.ClearCache;
  ctx_DataAccess.OpenClient(0);
  FCompanyNBR := 0;
  FReportStatus := [];
  ExtProcSetConsolidationNBR(0,0);
end;

function TEvRWAdapter.FineName(AName: String; RemoveUnderscore: Boolean): String;
var
  i: Integer;
begin
  Result := '';
  i := Pos('_', AName);
  while i > 0 do
  begin
    Result := Result + AnsiUpperCase(AName[1]) + AnsiLowerCase(Copy(AName, 2, i - 1));
    Delete(AName, 1, i);
    i := Pos('_', AName);
  end;

  if AName <> '' then
    Result := Result + AnsiUpperCase(AName[1]) + AnsiLowerCase(Copy(AName, 2, Length(AName) - 1));

  if RemoveUnderscore then
    Result := StringReplace(Result, '_', ' ', [rfReplaceAll, rfIgnoreCase]);
end;

function TEvRWAdapter.ConvertDBTypeToRWOne(const AFieldType: TFieldType): TrwDDType;
begin
  case AFieldType of
    ftString:    Result := ddtString;

    ftDateTime,
    ftDate:      Result := ddtDateTime;

    ftInteger:   Result := ddtInteger;

    ftFloat:     Result := ddtFloat;

    ftBlob:      Result := ddtBLOB;

    ftMemo:      Result := ddtMemo;

    ftGraphic:   Result := ddtGraphic;

    ftCurrency:  Result := ddtCurrency;
  else
    Result := ddtUnknown;
  end;
end;

procedure TEvRWAdapter.RWCallBackReadReportParamsFromStream(const AData: TStream; var AParamList: TrwReportParamList);
var
  P: TrwReportParams;
begin
  P := TrwReportParams.Create;
  try
    AData.Position := 0;
    P.LoadFromStream(AData);
    AParamList := EvRepParamsToParamList(P);
  finally
    P.Free;
  end;
end;

procedure TEvRWAdapter.RWCallBackGetSecurityDescriptor(var ASecurityDescriptor: TrwDesignerSecurityRec);
begin
  ASecurityDescriptor.DataDictionary.Modify := ctx_AccountRights.Functions.GetState('ABILITY_EDIT_DD') = stEnabled;
  ASecurityDescriptor.QBTemplates.Modify := ASecurityDescriptor.DataDictionary.Modify;
  ASecurityDescriptor.QBConstants.Modify := ASecurityDescriptor.DataDictionary.Modify;
  ASecurityDescriptor.LibComponents.Modify := ctx_AccountRights.Functions.GetState('ABILITY_EDIT_RWLIB') = stEnabled;
  ASecurityDescriptor.LibFunctions.Modify := ASecurityDescriptor.LibComponents.Modify;
  ASecurityDescriptor.LicencedReports.Modify := IsDebugMode;
end;


procedure TEvRWAdapter.RWCallBackDispatchCustomFunction(const FunctionName: String; const Params: Variant; var Result: Variant);
var
  V: Variant;
begin
  if AnsiSameText(FunctionName, 'SetClientCompany') then
    OpenClient(Params[0], Params[1])

  else if AnsiSameText(FunctionName, 'StartSession') then
  begin
    V := Params;
    RWCallBackSetAppCustomData(V);
    CacheReportLicenceInfo;
    FReportStatus := [];
  end

  else if AnsiSameText(FunctionName, 'EndSession') then
  begin
    ClearDBCache;
    Mainboard.ContextManager.DestroyThreadContext;
    Mainboard.ContextManager.CreateThreadContext(sGuestUserName, '');
  end

  else if AnsiSameText(FunctionName, 'BeforeRunReport') then
    FReportStatus := []

  else
    raise ErwException.Create('Unknown RW adapter function "' + FunctionName + '"');
end;

procedure TEvRWAdapter.RWCallBackWriteReportParamsToStream(var AParamList: TrwReportParamList; const Data: TStream);
var
  EvParams: TrwReportParams;
begin
  EvParams := TrwReportParams.Create;
  try
    ParamListToEvRepParams(AParamList, EvParams);
    Data.Position := 0;
    EvParams.SaveToStream(Data);
  finally
    FreeAndNil(EvParams);
  end;
end;

procedure TEvRWAdapter.RWCallBackGetCustomControl(const Destination: String; var AContainer: TForm);
begin
  if Destination = 'Query Builder Toolbar' then
  begin
    if not Assigned(FQBTlbCompanies) then
    begin
      FQBTlbCompanies := TevQBTlbCompanies.Create(Self);
      FQBTlbCompanies.OnChange := OnChangeCompanyCB;
      FQBTlbCompanies.SetCompany(ctx_DataAccess.ClientID, FCompanyNBR);
    end;

    AContainer := FQBTlbCompanies;
  end;
end;

procedure TEvRWAdapter.OnChangeCompanyCB(Sender: TObject; Cl_Nbr, Co_Nbr: Integer);
begin
  OpenClient(Cl_Nbr, Co_Nbr);
end;

procedure TEvRWAdapter.RWCallBackException(var ErrorMessage: String);
begin
  ErrorMessage := ErrorMessage + #13 + 'Client: ' + IntToStr(ctx_DataAccess.ClientID) + '   Company: ' + IntToStr(FCompanyNBR) + '   Consolidation: ' + IntToStr(FConsolidationNBR);
end;

function TEvRWAdapter.ExtFunctDecompressData(const AData: Variant): Variant;
var
  Src, Dest: IEvDualStream;
  P: PChar;
begin
  Src := TEvDualStreamHolder.CreateFromVariant(AData);
  Src.Position := 0;
  Dest := TEvDualStreamHolder.CreateInMemory;

  if Src.Size > 0 then
  begin
    InflateStream(Src.RealStream, Dest.RealStream);

    Result := VarArrayCreate([0, Dest.Size - 1], varByte);

    P := VarArrayLock(Result);
    try
      Dest.Position := 0;
      Dest.ReadBuffer(p, Dest.Size);
    finally
      VarArrayUnlock(Result);
    end
  end
  else
    Result := VarArrayCreate([0, -1], varByte);  
end;

function TEvRWAdapter.ExtFunctArrayToString(const AArray: Variant): String;
var
  P: PChar;
  StrLen: Integer;
begin
  StrLen := VarArrayHighBound(AArray, 1) - VarArrayLowBound(AArray, 1) + 1;
  if StrLen > 0 then
  begin
    SetLength(Result, StrLen);
    P := VarArrayLock(AArray);
    try
      Move(p^, Result[1], StrLen);
    finally
      VarArrayUnlock(AArray);
    end;
  end
  else
    Result := '';
end;

function TEvRWAdapter.ExtFunctCompressData(const AData: Variant): Variant;
var
  Src, Dest: IEvDualStream;
  P: PChar;
begin
  Src := TEvDualStreamHolder.CreateFromVariant(AData);
  Src.Position := 0;
  Dest := TEvDualStreamHolder.CreateInMemory;

  if Src.Size > 0 then
  begin
    DeflateStream(Src.RealStream, Dest.RealStream);
    Result := VarArrayCreate([0, Dest.Size - 1], varByte);
    P := VarArrayLock(Result);
    try
      Dest.Position := 0;
      Dest.ReadBuffer(p, Dest.Size);
    finally
      VarArrayUnlock(Result);
    end;
  end

  else
    Result := VarArrayCreate([0, - 1], varByte);  
end;

procedure TEvRWAdapter.ApplySecurity(const Component: TComponent);
begin
end;

procedure TEvRWAdapter.CreateMainForm;
begin
end;

procedure TEvRWAdapter.DestroyMainForm;
begin
end;

procedure TEvRWAdapter.EndWait;
begin
  if not FUnAttended then
    FWaitDialog.EndWait;
end;

procedure TEvRWAdapter.StartWait(const AText: string; AMaxProgress: Integer);
begin
  if not FUnAttended then
    FWaitDialog.StartWait(AText, AMaxProgress);
end;

procedure TEvRWAdapter.UpdateWait(const AText: string; ACurrentProgress, AMaxProgress: Integer);
begin
  if not FUnAttended then
    FWaitDialog.UpdateWait(AText, ACurrentProgress, AMaxProgress);
end;

procedure TEvRWAdapter.AsyncCallReturn(const ACallID: String; const AResults: IisListOfValues);
begin
  // a plug
end;

function TEvRWAdapter.GUIContext: IevContext;
begin
  Result := Mainboard.ContextManager.GetThreadContext;
end;

function TEvRWAdapter.ExtFunctGetEvoUserName: Variant;
begin
  result := Context.UserAccount.User;
end;

procedure TEvRWAdapter.AddTaskQueue(const ATask: IevTask; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TEvRWAdapter.OnException(Sender: TObject; E: Exception);
begin
  FWaitDialog.KillWait;
end;

procedure TEvRWAdapter.OnHideModalDialog(Sender: TObject);
begin
  if not FUnAttended then
    FWaitDialog.ShowIfHidden;
end;

procedure TEvRWAdapter.OnShowModalDialog(Sender: TObject);
begin
  if not FUnAttended then
    FWaitDialog.Hide;
end;

procedure TEvRWAdapter.CheckTaskQueueStatus;
begin
// a plug
end;

procedure TEvRWAdapter.OnGlobalFlagChange(
  const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
begin
// a plug
end;

procedure TEvRWAdapter.PrintTask(const ATaskID: TisGUID);
begin
// a plug
end;

procedure TEvRWAdapter.OnWaitingServerResponse;
begin
// a plug
end;

procedure TEvRWAdapter.OnPopupMessage(const aText, aFromUser, aToUsers: String);
begin
// a plug
end;

procedure TEvRWAdapter.OnChangeSessionStatus(const AStatus, APreviousStatus: TevSessionState);
begin
// a plug
end;

procedure TEvRWAdapter.RWCallBackDeactivate(Sender: TObject);
begin
  if not IsStandalone then
  begin
    if Context = nil then
      Mainboard.ContextManager.CreateThreadContext(EncodeUserAtDomain(sGuestUserName, sDefaultDomain), '');
    Mainboard.TCPClient.Logoff;
  end;
end;

procedure TEvRWAdapter.RWCallBackSetAppCustomData(var AData: Variant);
var
  tmpGlobalSettings: IevDualStream;
  AppData: IisListOfValues;
begin
  if VarType(AData) = varUnknown then
    AppData := IInterface(AData) as IisListOfValues;

  if AppData = nil then
  begin
    CheckCondition(InitGUIContext, 'Connection to server has failed');
    AppData := TisListOfValues.Create;
    if not IsStandalone then
    begin
      AppData.AddValue('Host', Mainboard.TCPClient.Host);
      AppData.AddValue('Port', Mainboard.TCPClient.Port);
      AppData.AddValue('EncryptionType', Mainboard.TCPClient.EncryptionType);
      AppData.AddValue('CompressionLevel', Mainboard.TCPClient.CompressionLevel);
    end
    else
      AppData.AddValue('GlobalSettings', (mb_GlobalSettings as IisInterfacedObject).AsStream);
    AppData.AddValue('User', EncodeUserAtDomain(Context.UserAccount.User, Context.UserAccount.Domain));
    AppData.AddValue('Password', Context.UserAccount.Password);
    AData := AppData;
  end

  else
  begin
    if not IsStandalone then
    begin
      Mainboard.TCPClient.Host := AppData.Value['Host'];
      Mainboard.TCPClient.Port := AppData.Value['Port'];
      Mainboard.TCPClient.EncryptionType := AppData.Value['EncryptionType'];
      Mainboard.TCPClient.CompressionLevel := AppData.Value['CompressionLevel'];
    end
    else
    begin
      tmpGlobalSettings := IInterface(AppData.Value['GlobalSettings']) as IevDualStream;
      (Mainboard.GlobalSettings as IisInterfacedObject).AsStream := tmpGlobalSettings;
    end;

    Mainboard.ContextManager.CreateThreadContext(AppData.Value['User'], AppData.Value['Password']);
  end;
end;

procedure TEvRWAdapter.RWCallBackTerminate(Sender: TObject);
begin
  SetAppTerminatingFlag;
end;

function TEvRWAdapter.ExtProcTestClient(const Cl_Nbr: Integer; var AError: String): Boolean;
var
  OldCl: Integer;
  DS: IevDataSet;
begin
  Result := False;
  OldCl := ctx_DBAccess.CurrentClientNbr;
  try
    AError := '';
    ctx_DBAccess.CurrentClientNbr := Cl_Nbr;
    DS := ctx_DBAccess.OpenQuery('SELECT CL_NBR FROM CL WHERE {AsOfNow<CL>}', nil, nil);
    CheckCondition(DS.Fields[0].AsInteger = Cl_Nbr, 'Client database ' + IntToStr(Cl_Nbr) + ' is invalid',
                   EevException);
    ctx_DBAccess.CurrentClientNbr := OldCl;
    Result := True;
  except
    on E: Exception do
    begin
      AError := E.Message;
      ctx_DBAccess.CurrentClientNbr := OldCl;
    end;
  end;
end;

procedure TEvRWAdapter.AddTaskQueue(const ATaskList: IisList; const AShowComfirmation: Boolean);
begin
// a plug
end;

procedure TEvRWAdapter.UserSchedulerEvent(const AEventID, ASubject: String; const AScheduledTime: TDateTime);
begin
// a plug
end;

function TEvRWAdapter.ExtFunctTextToEMF(const AText, AFontInfo: String): Variant;
const
  cVirtualCanvasRes = 600;
  cScreenCanvasRes = 96;
var
  Picture: TMetafile;
  C: TrwPCLCanvas;
  Canvas: TCanvas;
  S: IisStream;
  R:TRect;
  PCLFontData: String;

  P: TPoint;
  fID, h: String;
  i: Integer;

  function GetPCLFont(const AFontName: String): String;
  var
    HR: HRSRC;
    SR: Cardinal;
    PR: Pointer;
  begin
    HR := FindResource(HINSTANCE, PAnsiChar(AnsiUpperCase(AFontName)), 'PCLFONT');
    SR := SizeofResource(HINSTANCE, HR);
    PR := Pointer(LoadResource(HINSTANCE, HR));
    SetLength(Result, SR);
    CopyMemory(@(Result[1]), PR, SR);
  end;

begin
  Picture := TMetafile.Create;
  S := TisStream.CreateInMemory;
  try
    PCLFontData := GetPCLFont(AFontInfo);
    if (PCLFontData = '') or (AText = '') then
      Exit;

    C := TrwPCLCanvas.Create;
    Canvas := TCanvas.Create;
    try
      if Copy(PCLFontData, 1, 3) <> #27'*c' then
      begin
        fID := '21002';
        h := #27'*c' + fID + 'D';
        S.WriteBuffer(h[1], Length(h));

      end
      else
      begin
        fID := '';
        i := Pos('D', PCLFontData);
        fID := Copy(PCLFontData, 4, i - 4);
      end;

      C.NoMargins := True;
      S.WriteBuffer(PCLFontData[1], Length(PCLFontData));
      h := #27'*c' + fID + 'D'#27'*c5F'#27'(' + fID + 'X';
      S.WriteBuffer(h[1], Length(h));
      S.WriteBuffer(AText[1], Length(AText));

      R := C.CalcTextRect(S.RealStream, Canvas, cVirtualCanvasRes);
      Picture.Width := R.Right - R.Left;
      Picture.Height := R.Bottom - R.Top;

      case C.PrimaryFont.SoftFont.Header.Orientation of
        0, 2: P := Point(0, Picture.Height);
        1, 3: P := Point(Picture.Width, Picture.Height);
      end;

      C.PaintTo(S.RealStream, Picture, P, cVirtualCanvasRes);
    finally
      Canvas.Free;
      C.Free;
    end;

  finally
    try
      S.Clear;
      Picture.SaveToStream(S.RealStream);
      Result := S.AsVarArrayOfBytes;
    finally
      Picture.Free;
    end;
  end;
end;

function TEvRWAdapter.ExtFunctGetSecurityElement(const AType, AName: String): Variant;
begin
  Result := ctx_AccountRights.GetElementState(AType[1], AName);
end;

function TEvRWAdapter.ExtFunctGetEvoUserAccountInfo: Variant;
begin
  Result := VarArrayCreate([0, 5], varVariant);
  Result[0] := Context.Security.GetAuthorization.Domain;
  Result[1] := Context.Security.GetAuthorization.User;
  Result[2] := Context.Security.GetAuthorization.FirstName;
  Result[3] := Context.Security.GetAuthorization.LastName;
  Result[4] := Context.Security.GetAuthorization.Email;
  Result[5] := Ord(Context.Security.GetAuthorization.AccountType);
end;

procedure TEvRWAdapter.PasswordChangeClose;
begin
// a plug
end;

function TEvRWAdapter.ExtFunctGetAuditData(const AFields: String; const ABeginDate: TDateTime): Variant;
const
  NonVerResStruct: array [0..10] of TDSFieldDef = (
    (FieldName: 'change_date';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'user_name';  DataType: ftString;  Size: 30;  Required: False),
    (FieldName: 'change_type';  DataType: ftString;  Size: 1;  Required: False),
    (FieldName: 'table_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'field_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'old_value';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'new_value';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'old_value_text';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'new_value_text';  DataType: ftString;  Size: 255;  Required: False));

  VerResStruct: array [0..10] of TDSFieldDef = (
    (FieldName: 'table_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'field_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'change_date';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'user_name';  DataType: ftString;  Size: 30;  Required: False),
    (FieldName: 'group_id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'begin_date';  DataType: ftDate;  Size: 0;  Required: False),
    (FieldName: 'end_date';  DataType: ftDate;  Size: 0;  Required: False),
    (FieldName: 'value';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'text_value';  DataType: ftString;  Size: 255;  Required: False));

  procedure ProcessNonVersionedFields(const Params: TTaskParamList);
  var
    DS, ResultDS: IevDataSet;
    tbl, flds: String;
    dat_b: TDateTime;
    Errors: IisStringList;
  begin
    tbl := Params[0];
    flds := Params[1];
    dat_b := Params[2];
    ResultDS := IevDataSet(Pointer(Integer(Params[3])));
    ctx_DBAccess.CurrentClientNbr := Params[4];
    Errors := IisStringList(Pointer(Integer(Params[5])));

    try
      DS := ctx_DBAccess.GetRecordAudit(tbl, flds, 0, dat_b);

      if Errors.Count > 0 then Exit;

      if Assigned(DS) then
      begin
        (ResultDS as IisInterfacedObject).Lock;
        try
          DS.First;
          while not DS.Eof do
          begin
            ResultDS.Append;
            ResultDS.Fields[0].Value := DS.Fields[0].Value;
            ResultDS.Fields[1].Value := DS.Fields[1].Value;
            ResultDS.Fields[2].Value := DS.Fields[2].Value;
            ResultDS.Fields[3].Value := DS.Fields[3].Value;
            ResultDS.Fields[4].Value := tbl;
            ResultDS.Fields[5].Value := DS.Fields[4].Value;
            ResultDS.Fields[6].Value := DS.Fields[9].Value;
            ResultDS.Fields[7].Value := DS.Fields[5].Value;
            ResultDS.Fields[8].Value := DS.Fields[6].Value;
            ResultDS.Fields[9].Value := DS.Fields[7].Value;
            ResultDS.Fields[10].Value := DS.Fields[8].Value;
            ResultDS.Post;
            DS.Next;
          end;
        finally
          (ResultDS as IisInterfacedObject).Unlock;
        end;
      end;
    except
      on E: Exception do
        Errors.Add('Table ' + tbl + #10#13 + E.Message);
    end;
  end;

  procedure ProcessVersionedField(const Params: TTaskParamList);
  var
    DS, ResultDS: IevDataSet;
    tbl, fld: String;
    dat_b: TDateTime;
    VerFldAudit: IisListOfValues;
    NbrVerAudit: IisParamsCollection;
    UserName: String;
    UserId: Integer;
    ChangeDate: TDateTime;
    GrpId, Nbr, i, k: Integer;
    Errors: IisStringList;
  begin
    tbl := Params[0];
    fld := Params[1];
    dat_b := Params[2];
    ResultDS := IevDataSet(Pointer(Integer(Params[3])));
    ctx_DBAccess.CurrentClientNbr := Params[4];
    Errors := IisStringList(Pointer(Integer(Params[5])));

    try
      VerFldAudit := ctx_DBAccess.GetVersionFieldAudit(tbl, fld, 0, dat_b);

      if Errors.Count > 0 then Exit;

      if Assigned(VerFldAudit) then
      begin
        GrpId := 0;
        (ResultDS as IisInterfacedObject).Lock;
        try
          for i := 0 to VerFldAudit.Count - 1 do
          begin
            NbrVerAudit := IInterface(VerFldAudit[i].Value) as IisParamsCollection;
            for k := NbrVerAudit.Count - 1 downto 0 do
            begin
              ChangeDate := NbrVerAudit[k].Value['ChangeTime'];
              UserId := NbrVerAudit[k].Value['UserID'];
              UserName := NbrVerAudit[k].Value['UserName'];
              Nbr := NbrVerAudit[k].Value['Nbr'];

              Inc(GrpId);
              DS := IInterface(NbrVerAudit[k].Value['Data']) as IevDataSet;
              DS.First;
              repeat
                ResultDS.Append;
                ResultDS.Fields[0].Value := tbl;
                ResultDS.Fields[1].Value := fld;
                ResultDS.Fields[2].Value := Nbr;
                ResultDS.Fields[3].Value := ChangeDate;
                ResultDS.Fields[4].Value := UserId;
                ResultDS.Fields[5].Value := UserName;
                ResultDS.Fields[6].Value := GrpId;
                ResultDS.Fields[7].Value := DS.Fields[1].Value;
                ResultDS.Fields[8].Value := DS.Fields[2].Value;
                ResultDS.Fields[9].Value := DS.Fields[3].Value;
                ResultDS.Fields[10].Value := DS.Fields[4].Value;
                ResultDS.Post;

                DS.Next;
              until DS.Eof;
            end;
          end;
        finally
          (ResultDS as IisInterfacedObject).Unlock;
        end;
      end;
    except
      on E: Exception do
        Errors.Add('Field ' + tbl + '.' + fld + #10#13 + E.Message);
    end;
  end;

var
  s, tbl_fld, tbl, fld, fld_lst: String;
  Flds: IisListOfValues;
  LV: IisNamedValue;
  i: Integer;
  NonVerResultDS, VerResultDS: IevDataSet;
  TblClass: TddTableClass;
  TM: IThreadManager;
  Errors: IisStringList;
begin
  // Group fields by tables
  Flds := TisListOfValues.Create;
  s := AnsiUpperCase(AFields);
  TblClass := nil;
  while s <> '' do
  begin
    tbl_fld := GetNextStrValue(s, ',');
    tbl := Trim(GetNextStrValue(tbl_fld, '.'));
    tbl_fld := Trim(tbl_fld);

    if not Assigned(TblClass) or not AnsiSameText(TblClass.GetTableName, tbl) then
    begin
      TblClass := GetddTableClassByName(tbl);
      if not Assigned(TblClass) then
        raise ErwException.CreateFmt('Table %s does not exist', [tbl]);
    end;

    if TblClass.IsVersionedField(tbl_fld) then
      tbl := '*' + tbl;

    LV := Flds.FindValue(tbl);
    if not Assigned(LV) then
    begin
      i := Flds.AddValue(tbl, '');
      LV := Flds[i];
    end;
    fld_lst := LV.Value;
    AddStrValue(fld_lst, tbl_fld, ',');
    LV.Value := fld_lst;
  end;

  NonVerResultDS := TevDataSet.Create(NonVerResStruct, True);
  VerResultDS := TevDataSet.Create(VerResStruct, True);

  TM := TThreadManager.Create('ExtFunctGetAuditData TM');
  i := HowManyProcessors;
  if i >= 4 then
   TM.Capacity := 4
  else
   TM.Capacity := 2;

  Errors := TisStringList.Create(nil, True);

  for i := 0 to Flds.Count - 1 do
  begin
    if Errors.Count > 0 then Break;
    LV := Flds[i];
    tbl := LV.Name;
    fld_lst := LV.Value;
    if StartsWith(tbl, '*') then
    begin
      Delete(tbl, 1, 1);
      while fld_lst <> '' do
      begin
        fld := GetNextStrValue(fld_lst, ',');
        Context.RunParallelTask(@ProcessVersionedField, MakeTaskParams([tbl, fld, ABeginDate, Pointer(VerResultDS), ctx_DataAccess.ClientID, Pointer(Errors)]),
                                'ProcessVersionedField for ' + tbl + '.' + fld, tpNormal, TM);
      end;
    end
    else
      Context.RunParallelTask(@ProcessNonVersionedFields, MakeTaskParams([tbl, fld_lst, ABeginDate, Pointer(NonVerResultDS), ctx_DataAccess.ClientID, Pointer(Errors)]),
                              'ProcessNonVersionedFields for ' + tbl, tpNormal, TM);
  end;

  TM.WaitForTaskEnd(0);

  if Errors.Count > 0 then
    raise ErwException.Create(Errors.Text);

  Result := VarArrayCreate([0, 1], varVariant);
  Result[0] := NonVerResultDS.Data;
  Result[1] := VerResultDS.Data;
end;

function TEvRWAdapter.ExtFunctGetAuditNBRCreationData(const ATables: String): Variant;
var
  DS: IevDataSet;
begin
  DS := ctx_DBAccess.GetInitialCreationNBRAudit(ATables);

  if Assigned(DS) then
    Result := DS.Data
  else
    Result := Null;
end;

//  ticket 102173 - security concern
{
function TEvRWAdapter.ExtFunctRunEvoNativeQuery(const ASQL: String; const AParamNames, AParamValues: Variant): Variant;
var
  Q: IevQuery;
  i: Integer;
begin
  ErrorIf(VarArrayHighBound(AParamNames, 1) <> VarArrayHighBound(AParamValues, 1), 'Arrays AParamNames and AParamValues have no equal length');

  Q := TevQuery.Create(ASQL, True);
  for i := 0 to VarArrayHighBound(AParamNames, 1) do
    Q.Params.AddValue(AParamNames[i], AParamValues[i]);

  Result := Q.Result.Data;
end;
}

procedure  BlobTextChangeCheck(aFixBas: IevDataset; table_name:string);
var
   sBefore, sAfter:string;
   aBefore, aAfter: iIsStringList;
   jOld,JNew, jerr: integer;
   j: integer;
   jjOld,jjNew: iIsStream;
   sTextOld, sTextNew: string;
   sWordOld, sWordNew: string;
   sFieldName:string;
begin
// display changed piece of text BLOB changed, instead of simple (Blob)
// aFixbas supposed to be in Edit state
  if (afixBas.FieldByName('OLD_VALUE_TEXT').AsString='(Blob)')
     and
      (afixBas.FieldByName('NEW_VALUE_TEXT').AsString='(Blob)')
     and
       ( AnsiStartsStr('-', afixBas.FieldByName('OLD_VALUE').AsString) or AnsiStartsStr('(Blob)', afixBas.FieldByName('OLD_VALUE').AsString) )
     and
       (AnsiStartsStr('-', afixBas.FieldByName('NEW_VALUE').AsString) or AnsiStartsStr('(Blob)', afixBas.FieldByName('NEW_VALUE').AsString))
     and
     AnsiContainsText(afixBas.FieldByName('field_name').AsString, 'NOTE')
  then  // Text BLOB
  begin

    if AnsiStartsStr('-', afixBas.FieldByName('OLD_VALUE').AsString) then
    begin
      Val( afixBas.FieldByName('OLD_VALUE').AsString, jOld, jerr);
      if jerr <> 0 then Exit;
    end
    else
      jOld := afixBas.FieldByName('NBR').AsInteger; // positive amount retrieves value from table, not from blob history

    if AnsiStartsStr('-', afixBas.FieldByName('NEW_VALUE').AsString) then
    begin
    Val( afixBas.FieldByName('NEW_VALUE').AsString, jNew, jerr);
    if jerr <> 0 then Exit;
    end
    else
      jNew := afixBas.FieldByName('NBR').AsInteger; // positive amount retrieves value from table, not from blob history

    // simultaneous appearance of (Blob) on Old and New values considered hardly probable

    sFieldName := afixBas.FieldByName('field_name').AsString;
    jjOld  := ctx_DBAccess.GetBlobAuditData(table_name, sFieldName  ,jOld);
    jjNew  := ctx_DBAccess.GetBlobAuditData(table_name, sFieldName  ,jNew);
    sBefore := '';  sAfter := '';
    if Assigned(jjOld) then
      sBefore := jjOld.AsString;
    if Assigned(jjNew) then
      sAfter := jjNew.AsString;

    if CompareStr(sBefore,sAfter) = 0 then Exit;

    aBefore := TisStringList.Create;
    aAfter := Tisstringlist.Create;

//    aBefore.Delimiter := ' ';
//    aAfter.Delimiter := ' ';
    aBefore.Text := sBefore;
    aAfter.Text := sAfter;

    sTextOld := '...';
    sTextNew := '...';
    for j := 0 to Max(aBefore.Count, aAfter.Count ) - 1 do
    begin
      if j < aBefore.Count then
           sWordOld := aBefore[j]
      else
           sWordOld := '';

      if j < aAfter.Count then
           sWordNew := aAfter[j]
      else
           sWordNew := '';

      if CompareStr(sWordOld, sWordNew) <> 0 then
      begin
        sTextOld := sTextOld + 'Line[' + IntToStr(j)+']' + sWordOld + '...';
        sTextNew := sTextNew + 'Line[' + IntToStr(j)+']' + sWordNew + '...';
        afixBas.Edit;
        afixBas.FieldByName('OLD_VALUE_TEXT').AsString := '(Blob)' + sTextOld;
        afixBas.FieldByName('NEW_VALUE_TEXT').AsString := '(Blob)' + sTextNew;
        afixBas.Post;
        Exit;
      end
      else
      begin
        afixBas.Edit;
        afixBas.FieldByName('OLD_VALUE_TEXT').AsString := '(Blob)' + sTextOld;
        afixBas.FieldByName('NEW_VALUE_TEXT').AsString := '(Blob)' + sTextNew;
        afixBas.Post;
        Exit;
      end;
    end;
    // here we come if strings different, but words all are the same; different length
    sTextOld := '(Length='+IntToStr(Length(sBefore))+')';
    sTextNew := '(Length='+IntToStr(Length(sAfter))+')';
    afixBas.Edit;
    afixBas.FieldByName('OLD_VALUE_TEXT').AsString := '(Blob)' + sTextOld;
    afixBas.FieldByName('NEW_VALUE_TEXT').AsString := '(Blob)' + sTextNew;
    afixBas.Post;

  end;
end;

function TEvRWAdapter.ExtFunctGetAuditChanges(const AFields: String; const ABeginDate: Variant): Variant;
const
  NonVerResStruct: array [0..11] of TDSFieldDef = (
    (FieldName: 'change_date';  DataType: ftDateTime;  Size: 0;  Required: False),
    (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'user_name';  DataType: ftString;  Size: 30;  Required: False),
    (FieldName: 'change_type';  DataType: ftString;  Size: 1;  Required: False),
    (FieldName: 'table_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'field_name';  DataType: ftString;  Size: 64;  Required: False),
    (FieldName: 'nbr';  DataType: ftInteger;  Size: 0;  Required: False),
    (FieldName: 'old_value';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'new_value';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'old_value_text';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'new_value_text';  DataType: ftString;  Size: 255;  Required: False),
    (FieldName: 'field_nbr';  DataType: ftInteger;  Size: 0;  Required: False));  // new one - 106058


  VerResChangeStruct: array [0..22] of TDSFieldDef = (
    (FieldName: 'table_name';  DataType: ftString;  Size: 32;  Required: False),
    (FieldName: 'field_name';  DataType: ftString;  Size: 32;  Required: False),

    (FieldName: 'idx';  DataType: ftinteger;  Size: 0;  Required: False),             //1
    (FieldName: 'field_nbr';  DataType: ftinteger;  Size: 0;  Required: False),       //2
    (FieldName: 'rec_nbr';  DataType: ftInteger;  Size: 0;  Required: False),         //3
    (FieldName: 'rec_version';  DataType: ftInteger;  Size: 0;  Required: False),     //4
    (FieldName: 'field_kind';  DataType: ftString;  Size: 1;  Required: False),       //5
    (FieldName: 'change_type';  DataType: ftString;  Size: 1;  Required: False),      //6
    (FieldName: 'change_datetime';  DataType: ftDateTime;  Size: 0;  Required: False),//7
    (FieldName: 'old_value';  DataType: ftString;  Size: 255;  Required: False),      //8
    (FieldName: 'new_value';  DataType: ftString;  Size: 255;  Required: False),      //9
    (FieldName: 'user_id';  DataType: ftInteger;  Size: 0;  Required: False),         //10
    (FieldName: 'live_nbr';  DataType: ftInteger;  Size: 0;  Required: False),        //11
    (FieldName: 'begin_date';  DataType: ftDateTime;  Size: 0;  Required: False),     //12
    (FieldName: 'end_date';  DataType: ftDateTime;  Size: 0;  Required: False),       //13
    (FieldName: 'effective_date';  DataType: ftDateTime;  Size: 0;  Required: False), //14
    (FieldName: 'effective_until';  DataType: ftDateTime;  Size: 0;  Required: False),//15
    (FieldName: 'old_text'; DataType: ftString;  Size: 255;  Required: False),        //16
    (FieldName: 'new_text'; DataType: ftString;  Size: 255;  Required: False),         //17

    (FieldName: 'old_effective';  DataType: ftDateTime;  Size: 0;  Required: False),  //18
    (FieldName: 'new_effective';  DataType: ftDateTime;  Size: 0;  Required: False),  //19
    (FieldName: 'old_until';  DataType: ftDateTime;  Size: 0;  Required: False),      //20
    (FieldName: 'new_until';  DataType: ftDateTime;  Size: 0;  Required: False)       //21
                                                     ); // end of VerResChangeStruct; marked for better visibility

  procedure ProcessNonVersionedFields(const Params: TTaskParamList);
  var
    DS, ResultDS: IevDataSet;
    tbl, flds, nbrs, sfld: String;
    dat_b: TDateTime;
    Errors, NbrList: IisStringList;
  begin
    NbrList := TisStringList.Create;
    tbl := Params[0];
    flds := Params[1];
    dat_b := Params[2];
    ResultDS := IevDataSet(Pointer(Integer(Params[3])));
    ctx_DBAccess.CurrentClientNbr := Params[4];
    Errors := IisStringList(Pointer(Integer(Params[5])));
    nbrs := Params[6];

    sfld := flds;
    while Length(nbrs) > 0 do // numbers of fields listed in flds
       NbrList.Add(GetNextStrValue(sfld,',') + '=' + GetNextStrValue(nbrs,','));

    try
      DS := ctx_DBAccess.GetRecordAudit(tbl, flds, 0, dat_b);
      DS.First;
      while NOT DS.EOF do
      begin
        BlobTextChangeCheck(DS, tbl);
        DS.Next;
      end;

      if Errors.Count > 0 then Exit;

      if Assigned(DS) then
      begin
        (ResultDS as IisInterfacedObject).Lock;
        try
          DS.First;
          while not DS.Eof do
          begin
            ResultDS.Append;
            ResultDS.Fields[0].Value := DS.Fields[0].Value;
            ResultDS.Fields[1].Value := DS.Fields[1].Value;
            ResultDS.Fields[2].Value := DS.Fields[2].Value;
            ResultDS.Fields[3].Value := DS.Fields[3].Value;
            ResultDS.Fields[4].Value := tbl;
            ResultDS.Fields[5].Value := DS.Fields[4].Value;
            ResultDS.Fields[6].Value := DS.Fields[9].Value;
            ResultDS.Fields[7].Value := DS.Fields[5].Value;
            ResultDS.Fields[8].Value := DS.Fields[6].Value;
            ResultDS.Fields[9].Value := DS.Fields[7].Value;
            ResultDS.Fields[10].Value := DS.Fields[8].Value;
            ResultDS.Fields[11].Value := StrToInt(NbrList.Values[string(DS.Fields[4].Value)]);
            ResultDS.Post;
            DS.Next;
          end;
        finally
          (ResultDS as IisInterfacedObject).Unlock;
        end;
      end;
    except
      on E: Exception do
        Errors.Add('Table ' + tbl + #10#13 + E.Message);
    end;
  end;

  procedure ProcessVersionedField(const Params: TTaskParamList); // here go changes: returned are single dataset and not collection of values
  var
    ResultDS: IevDataSet;
    tbl, fld: String;
    dat_b: TDateTime;
    VerFldAuditChange: IevDataset;
    Errors: IisStringList;
    dat_e: TDateTime;
  begin
    tbl := Params[0];
    fld := Params[1];
    dat_b := Params[2];
    dat_e := Params[3];
    ResultDS := IevDataSet(Pointer(Integer(Params[4])));
    ctx_DBAccess.CurrentClientNbr := Params[5];
    Errors := IisStringList(Pointer(Integer(Params[6])));

    try
      VerFldAuditChange := ctx_DBAccess.GetVersionFieldAuditChange(tbl, fld, {0,} dat_b);//returns ready dataset to be merged into output

      if Errors.Count > 0 then Exit;

      if Assigned(VerFldAuditChange) then
      begin
        //GrpId := 0;
        (ResultDS as IisInterfacedObject).Lock;
        try
          while NOT VerFldAuditChange.EOF do
          begin
            try
            if (Not(VerFldAuditChange.Fields[7].isNull))
               and
               (VerFldAuditChange.Fields[7].AsFloat <= Double(dat_e))
            then
             ResultDS.AppendRecord([
               tbl, fld,
               VerFldAuditChange.Fields[0].Value, // idxx -> idx
               VerFldAuditChange.Fields[2].Value, // field_nbr
               VerFldAuditChange.Fields[3].Value, // rec_nbr
               VerFldAuditChange.Fields[4].Value, // rec_version
               VerFldAuditChange.Fields[5].Value, // field_kind
               VerFldAuditChange.Fields[6].Value, // change-type
               VerFldAuditChange.Fields[7].Value, // change_datetime
               VerFldAuditChange.Fields[8].Value, // old_value
               VerFldAuditChange.Fields[9].Value, // new_value
               VerFldAuditChange.Fields[10].Value,// user_id
               VerFldAuditChange.Fields[11].Value,// live_nbr
               VerFldAuditChange.Fields[12].Value,// begin_date
               VerFldAuditChange.Fields[13].Value,// end_date
               VerFldAuditChange.Fields[14].Value,// effective_date
               VerFldAuditChange.Fields[15].Value,// effective_until
               VerFldAuditChange.Fields[16].Value,// old_text
               VerFldAuditChange.Fields[17].Value, // new_text
               VerFldAuditChange.Fields[19].Value, // old_effective
               VerFldAuditChange.Fields[20].Value, // new_effective
               VerFldAuditChange.Fields[21].Value, // old_until
               VerFldAuditChange.Fields[22].Value // new_until

             // there are also some internal fields not to transfer to output, so they are skipped here
                                  ]);
            except
              Assert(VerFldAuditChange.Fields[0].Value > 0);
            end;
            VerFldAuditChange.Next;
          end;
        finally
          (ResultDS as IisInterfacedObject).Unlock;
        end;
      end;
    except
      on E: Exception do
        Errors.Add('Field ' + tbl + '.' + fld + #10#13 + E.Message);
    end;
  end;

var
  s, tbl_fld, tbl, fld, fld_lst, nbr_lst: String;
  Flds: IisListOfValues;
  FldsNumbers:IisListOfValues;
  jFldNbr: integer;

  LV, LVNbrFlds: IisNamedValue;
  i: Integer;
  NonVerResultDS, VerResultDS: IevDataSet;
  TblClass: TddTableClass;
  TM: IThreadManager;
  Errors: IisStringList;
  DBeginDate:TDateTime;
  DEndDate: TDateTime;
begin
  DEndDate := Now();
  DBeginDate := Now();
  if VarIsArray(ABeginDate) then
  begin
     if VarArrayHighBound(ABeginDate,1) > 0 then
     begin
       DBeginDate := ABeginDate[0];
       DEndDate := ABeginDate[1];
     end;
     if VarArrayHighBound(ABeginDate,1) = 0 then
     begin
       DBeginDate := ABeginDate[0];
     end;
  end
  else
    DBeginDate := ABeginDate;

  // Group fields by tables
  Flds := TisListOfValues.Create;
  FldsNumbers := TisListOfValues.Create;
  s := AnsiUpperCase(AFields);
  TblClass := nil;
  while s <> '' do
  begin
    tbl_fld := GetNextStrValue(s, ',');
    tbl := Trim(GetNextStrValue(tbl_fld, '.'));
    tbl_fld := Trim(tbl_fld);

    if not Assigned(TblClass) or not AnsiSameText(TblClass.GetTableName, tbl) then
    begin
      TblClass := GetddTableClassByName(tbl);
      if not Assigned(TblClass) then
        raise ErwException.CreateFmt('Table %s does not exist', [tbl]);
    end;

    if TblClass.IsVersionedField(tbl_fld) then
      tbl := '*' + tbl;

    jFldNbr := TblClass.GetEvFieldNbr(tbl_fld);

    LV := Flds.FindValue(tbl);
    if not Assigned(LV) then
    begin
      i := Flds.AddValue(tbl, '');
      LV := Flds[i];
    end;
    LVNbrFlds := FldsNumbers.FindValue(tbl);
    if NOT Assigned( LVNbrFlds) then
    begin
      i :=  FldsNumbers.AddValue(tbl, '');
      LVNbrFlds := FldsNumbers[i];
    end;

    fld_lst := LV.Value;
    AddStrValue(fld_lst, tbl_fld, ',');
    LV.Value := fld_lst;

    nbr_lst := LVNbrFlds.Value;
    AddStrValue(nbr_lst, IntToStr(jFldNbr), ',');
    LVNbrFlds.Value := nbr_lst;
  end;

  NonVerResultDS := TevDataSet.Create(NonVerResStruct, True);
  VerResultDS := TevDataSet.Create(VerResChangeStruct, True);
  VerResultDS.LogChanges := False;

  TM := TThreadManager.Create('ExtFunctGetAuditDataChange TM');
  i := HowManyProcessors;
  if i >= 4 then
   TM.Capacity := 4
  else
   TM.Capacity := 2;

  Errors := TisStringList.Create(nil, True);

  for i := 0 to Flds.Count - 1 do
  begin
    if Errors.Count > 0 then Break;
    LV := Flds[i];
    tbl := LV.Name;
    fld_lst := LV.Value;
    nbr_lst := LVNbrFlds.Value;
    if StartsWith(tbl, '*') then
    begin
      Delete(tbl, 1, 1);
      while fld_lst <> '' do
      begin
        fld := GetNextStrValue(fld_lst, ',');
        Context.RunParallelTask(@ProcessVersionedField, MakeTaskParams([tbl, fld, DBeginDate, DEndDate, Pointer(VerResultDS), ctx_DataAccess.ClientID, Pointer(Errors)]),
                                'ProcessVersionedFieldChange for ' + tbl + '.' + fld, tpNormal, TM);
      end;
    end
    else
    begin
      Context.RunParallelTask(@ProcessNonVersionedFields, MakeTaskParams([tbl, fld_lst, DBeginDate, Pointer(NonVerResultDS), ctx_DataAccess.ClientID, Pointer(Errors),nbr_lst]),
                              'ProcessNonVersionedFieldChange for ' + tbl, tpNormal, TM);
    end;
  end;

  TM.WaitForTaskEnd(0);

  if Errors.Count > 0 then
    raise ErwException.Create(Errors.Text);

  Result := VarArrayCreate([0, 1], varVariant);
  Result[0] := NonVerResultDS.Data; // leave intact for static fields
  Result[1] := VerResultDS.Data;
end;

{
function TEvRWAdapter.ExtFunctGetAuditBlob( ATable: String; AField: string; ARefValue: integer): Variant;// const ATable, AField: String; const ARefValue: Integer): IisStream;
begin
  Result := ctx_DBAccess.GetBlobAuditData(ATable, AField, ARefValue);
end;
}

end.
