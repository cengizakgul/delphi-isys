object evQBTlbCompanies: TevQBTlbCompanies
  Left = 486
  Top = 308
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'Evolution Companies'
  ClientHeight = 21
  ClientWidth = 365
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 0
    Top = 4
    Width = 44
    Height = 13
    Caption = 'Company'
  end
  object cbCo: TevDBLookupCombo
    Left = 52
    Top = 0
    Width = 313
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'CUSTOM_COMPANY_NUMBER'#9'20'#9'Company #'#9'F'
      'NAME'#9'40'#9'Company Name'#9'F')
    LookupField = 'NAME'
    Style = csDropDownList
    DropDownCount = 20
    TabOrder = 0
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = cbCoChange
  end
end
