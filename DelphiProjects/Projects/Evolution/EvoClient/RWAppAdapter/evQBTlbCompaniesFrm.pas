unit evQBTlbCompaniesFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  wwdblook, DB, EvDataSet, EvCommonInterfaces, ISBasicClasses, EvUIComponents;

type
  TevOnCompanyChangeEvent = procedure (Sender: TObject; Cl_Nbr: Integer; Co_Nbr: Integer) of object;

  TevQBTlbCompanies = class(TForm)
    cbCo: TevDBLookupCombo;
    evLabel1: TevLabel;
    procedure cbCoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FOnChange: TevOnCompanyChangeEvent;
    FCoList: IevDataSet;
  public
    property  OnChange: TevOnCompanyChangeEvent read FOnChange write FOnChange;
    procedure SetCompany(Cl_Nbr, Co_Nbr: Integer);
  end;

implementation


{$R *.dfm}

procedure TevQBTlbCompanies.cbCoChange(Sender: TObject);
begin
 if Assigned(FOnChange) and (cbCo.Tag = 0) then
   FOnChange(Self, FCoList.FieldByName('CL_NBR').AsInteger, FCoList.FieldByName('CO_NBR').AsInteger);
end;

procedure TevQBTlbCompanies.SetCompany(Cl_Nbr, Co_Nbr: Integer);
begin
  cbCo.Tag := 1;
  try
    FCoList.Locate('CL_NBR;CO_NBR', VarArrayOf([Cl_Nbr, Co_Nbr]), [loCaseInsensitive]);
  finally
    cbCo.Tag := 0;
  end;
end;

procedure TevQBTlbCompanies.FormCreate(Sender: TObject);
var
  Q: IevQuery;
begin
  Left := 1000000;
  Top := 1000000;
  
  cbCo.Tag := 1;
  try
    Q := TevQuery.Create('SELECT CL_NBR, CO_NBR, NAME, CUSTOM_COMPANY_NUMBER FROM TMP_CO ORDER BY 3');
    FCoList := Q.Result;
  finally
    cbCo.Tag := 0;
  end;

  cbCo.LookupTable := FCoList.vclDataSet;
end;

end.
