unit EvConnectorMain;

interface

uses SysUtils, isBaseClasses, isBasicUtils,
     EvStreamUtils, EvTransportShared, EvTransportDatagrams, evTransportInterfaces,
     EvBasicUtils, EvConsts, isLogFile, isSocket;

type
  IevContext = interface
  ['{468C6B5E-5A71-4BC6-87D9-3E0855838394}']
    function ID: TisGUID;
    function AppID: String;
    function AnonymousUser: String;
    function ConnectionsPerSession: Integer;
    function Host: String;
    function HostIP: String;    
    function Port: String;
    function User: String;
    function Password: String;
  end;


  IevConnector = interface
  ['{A304EA82-94E4-45F6-9CC9-CDB690E66A67}']
    function  CreateContext(const aAppID, aAnonymousUser: String; const aConnectionsPerSession: Integer;
                            const aHost, aPort, aUser, aPassword: String): IevContext;
    procedure DestroyContext(const aContext: IevContext);
    function  SendRequest(const aContext: IevContext; const aMethod: String; const aParams: IisStream): IisStream;
  end;

  function CreateEvConnector: IevConnector;

implementation


type
  IevTCPClient = interface
  ['{AD70A610-D30D-4814-A4F7-D31FA16CAAF0}']
    function  SendRequest(const AContext: IevContext; const ADatagram: IevDatagram): IevDatagram;
  end;

  TevConnector = class (TisInterfacedObject, IevConnector)
  private
    FTCPClient: IevTCPClient;
  protected
    function  CreateContext(const aAppID, aAnonymousUser: String; const aConnectionsPerSession: Integer;
                            const aHost, aPort, aUser, aPassword: String): IevContext;
    procedure DestroyContext(const aContext: IevContext);
    function  SendRequest(const aContext: IevContext; const aMethod: String; const aParams: IisStream): IisStream;
  public
    constructor Create; reintroduce;
  end;


  TevTCPClient = class(TevCustomTCPClient, IevTCPClient)
  private
    FSessions: IisListOfValues;
    function  GetSession(const AContext: IevContext; const ADatagram: IevDatagram): IevSession;
  protected
    procedure DoOnConstruction; override;
    function  CreateDispatcher: IevDatagramDispatcher; override;
    function  SendRequest(const AContext: IevContext; const ADatagram: IevDatagram): IevDatagram;
  end;


  TevClientDispatcher = class(TevDatagramDispatcher)
  protected
    procedure BeforeDispatchIncomingData(const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
                                         const AStream: IEvDualStream; var Handled: Boolean); override;
  end;


  TevContext = class (TisInterfacedObject, IevContext)
  private
    FID: TisGUID;
    FAppID: String;
    FAnonymousUser: String;
    FConnectionsPerSession: Integer;
    FHost: String;
    FHostIP: String;
    FPort: String;
    FUser: String;
    FPassword: String;
  protected
    function ID: TisGUID;
    function AppID: String;
    function AnonymousUser: String;
    function ConnectionsPerSession: Integer;
    function Host: String;
    function HostIP: String;
    function Port: String;
    function User: String;
    function Password: String;
  public
    constructor Create(const aAppID, aAnonymousUser: String; const aConnectionsPerSession: Integer;
                       const aHost, aPort, aUser, aPassword: String); reintroduce;
  end;


function CreateEvConnector: IevConnector;
begin
  Result := TevConnector.Create;
end;

{ TevConnector }

constructor TevConnector.Create;
begin
  inherited Create;
  FTCPClient := TevTCPClient.Create;
end;

function TevConnector.CreateContext(const aAppID, aAnonymousUser: String; const aConnectionsPerSession: Integer;
  const aHost, aPort, aUser, aPassword: String): IevContext;
begin
  Result := TevContext.Create(aAppID, aAnonymousUser, aConnectionsPerSession, aHost, aPort, aUser, aPassword);
  CheckCondition(Result.HostIP <> '0.0.0.0', 'Cannot resolve host name: ' + aHost);
end;

procedure TevConnector.DestroyContext(const aContext: IevContext);
begin
  // Future use: removing the object from some global list of contexts
end;

function TevConnector.SendRequest(const aContext: IevContext; const aMethod: String; const aParams: IisStream): IisStream;
var
  DM, ResDM: IevDatagram;
  i: Integer;
begin
  DM := TevDatagram.Create(aMethod);
  DM.Header.ContextID := aContext.ID;
  DM.Header.User := aContext.User;
  DM.Header.Password := aContext.Password;
  DM.SetParamData(aParams);

  for i := 1 to 3 do
  begin
    try
      ResDM := FTCPClient.SendRequest(aContext, DM);
      DM := nil;
      break;
    except
      on E: EevConnection do
      begin
        GlobalLogFile.AddEvent(etError, 'TCP Client', 'Communication failed',
          E.Message + #13 + 'Host: ' + aContext.Host + #13 + 'Port: ' + aContext.Port + #13 + 'User: ' + aContext.User);
        if i = 3 then
          raise;
      end
      else
        raise;
    end;
  end;

  Result := ResDM.GetParamData;
  Result.Position := 0;
end;

{ TevTCPClient }

function TevTCPClient.CreateDispatcher: IevDatagramDispatcher;
begin
  Result := TevClientDispatcher.Create;
end;

procedure TevTCPClient.DoOnConstruction;
begin
  inherited;
  FSessions := TisListOfValues.Create;
end;

function TevTCPClient.GetSession(const AContext: IevContext; const ADatagram: IevDatagram): IevSession;
var
  HostID: String;
  SessionID: TisGUID;
  Session: IevSession;
  i: Integer;
  user, password: String;
begin
  HostID := AnsiUpperCase(AContext.Host + ':' + AContext.Port + ' ' + AContext.AppID);
  Lock;
  try
    SessionID := FSessions.TryGetValue(HostID, '');
    Session := FindSession(SessionID);
    if Assigned(Session) then

    if Assigned(Session) and (Session.State = ssActive) then
    begin
      Result := Session;
      Exit;
    end;

    if not Assigned(Session) or (Session.State <> ssActive) then
    begin
      if Assigned(Session) and (Session.State <> ssInactive) then
        Disconnect(SessionID);

      if (AContext.AnonymousUser <> '') or not Assigned(ADatagram) then
      begin
        user := AContext.AnonymousUser;
        password := '';
      end
      else
      begin
        user := ADatagram.Header.User;
        password := ADatagram.Header.Password;
      end;

      SessionID := CreateConnection(AContext.Host, AContext.Port, user, password, '', '', '', AContext.AppID);
      for i := 1 to AContext.ConnectionsPerSession - 1 do
        CreateConnection(AContext.Host, AContext.Port, user, password, SessionID, '', '', AContext.AppID);

      Session := FindSession(SessionID);
      FSessions.AddValue(HostID, SessionID);
      Result := Session;
    end;
  finally
    UnLock;
  end;
end;

function TevTCPClient.SendRequest(const AContext: IevContext; const ADatagram: IevDatagram): IevDatagram;
var
  S: IevSession;
  Request: IisStream;
  SentBytes, ReceivedBytes, TransmissionType: Cardinal;
begin
  if ADatagram.Header.User = '' then
  begin
    ADatagram.Header.User := AContext.AnonymousUser;
    ADatagram.Header.Password := '';
  end;

  S := GetSession(AContext, ADatagram);

  if not Assigned(S) then
    raise EevConnection.Create('Session  not found');

  ADatagram.Header.SessionID := S.SessionID;

  Request := TisStream.Create;
  (ADatagram as IisInterfacedObject).WriteToStream(Request);
  Request.Position := 0;

  Request := S.SendRequest(Request, ADatagram.Header.SequenceNbr, True, SentBytes, ReceivedBytes, TransmissionType);

  Request.Position := 0;
  Result := TevDatagram.CreateFromStream(Request, True);
end;


{ TevClientDispatcher }

procedure TevClientDispatcher.BeforeDispatchIncomingData(
  const ASession: IevSession; const ADatagramHeader: IevDatagramHeader;
  const AStream: IEvDualStream; var Handled: Boolean);
begin
  inherited;
  if (ASession.State = ssActive) and not IsTransportDatagram(ADatagramHeader) and not ADatagramHeader.IsResponse then
    Handled := True;  // suppress any callbacks
end;

{ TevContext }

function TevContext.AnonymousUser: String;
begin
  Result := FAnonymousUser;
end;

function TevContext.AppID: String;
begin
  Result := FAppID;
end;

function TevContext.ConnectionsPerSession: Integer;
begin
  Result := FConnectionsPerSession;
end;

constructor TevContext.Create(const aAppID, aAnonymousUser: String; const aConnectionsPerSession: Integer;
  const aHost, aPort, aUser, aPassword: String);
begin
  inherited Create;
  FID := GetUniqueID;
  FAppID := aAppID;
  FAnonymousUser := aAnonymousUser;
  FConnectionsPerSession := aConnectionsPerSession; 
  FHost := aHost;
  FHostIP := SocketLib.LookupHostAddr(FHost);
  FPort := aPort;
  FUser := aUser;
  FPassword := aPassword;
end;

function TevContext.Host: String;
begin
  Result := FHost;
end;

function TevContext.HostIP: String;
begin
  Result := FHostIP;
end;

function TevContext.ID: TisGUID;
begin
  Result := FID;
end;

function TevContext.Password: String;
begin
  Result := FPassword;
end;

function TevContext.Port: String;
begin
  Result := FPort;
end;

function TevContext.User: String;
begin
  Result := FUser;
end;

end.
