unit EvConnectorInt;

interface

uses SysUtils, Variants, Windows;

implementation

uses EvConnectorMain, isLogFile, isBasicUtils, isExceptions, IsErrorUtils, EvStreamUtils,
     isThreadManager, EvBasicUtils;

const
  RET_OK = 0;
  RET_ERROR = 1;

type
  ObjRef = Pointer;
  ErrObjRef = ObjRef;
  ResCode = Integer;

var
  Connector: IevConnector;


procedure LogError(const AException: Exception);
var
  Details: String;
begin
  try
    Details :=GetErrorDetailsStack(AException);
    if Details = '' then
      Details := GetStack(AException);

    GlobalLogFile.AddEvent(etError, 'Stdcall', AException.Message, Details);
  except
  end;
end;


procedure LogInfo(const AText: String);
begin
  try
    GlobalLogFile.AddEvent(etInformation, 'Main', AText, '');
  except
  end;
end;


function MakeRef(const Obj: IInterface): ObjRef;
begin
  if Assigned(Obj) then
    Obj._AddRef;
  Result := Pointer(Obj as IInterface);
end;


function MakeError(const E: Exception): ErrObjRef;
var
  StackedException: Exception;
begin
  StackedException := EnsureCallStack(E);
  try
    Result := MakeRef(SnapshotException(StackedException));
  finally
    if StackedException <> E then
      StackedException.Free;
  end;
end;


function GetErrorText(Error: ErrObjRef): LPTSTR; stdcall;
begin
  Result := LPTSTR((IInterface(Error) as IisException).Message);
end;


function Initialize(): ErrObjRef; stdcall;
var
  s: IisStream;
begin
  s := nil;

  try
    if GlobalLogFile = nil then
    begin
      OpenGlobalLogFile(ChangeFileExt(ModuleFileName, '.log'));

//TODO: Figure out why it does not work in .Net
//      CheckIfTampered;

      LogInfo('Initialized');
    end;

    Connector := CreateEvConnector();
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function Uninitialize: ErrObjRef; stdcall;
begin
  try
    Connector := nil;
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;

  LogInfo('Uninitialized');
  CloseGlobalLogFile;

  GlobalThreadManager.TerminateTask;
  GlobalThreadManager.WaitForTaskEnd;
end;


function ReleaseObject(Obj: ObjRef): ErrObjRef; stdcall;
begin
  try
    IInterface(Obj)._Release;
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function NewStream(out Stream: ObjRef): ErrObjRef; stdcall;
begin
  try
    Stream := MakeRef(TisStream.Create);
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function WriteToStream(Stream: ObjRef; Buffer: Pointer; BufferSize: Integer): ErrObjRef; stdcall;
var
  S: IisStream;
begin
  try
    S := IInterface(Stream) as IisStream;
    S.WriteBuffer(Buffer^, BufferSize);
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function ReadFromStream(Stream: ObjRef; Buffer: Pointer; BufferSize: Integer; out ReadBytes: Integer): ErrObjRef; stdcall;
var
  S: IisStream;
begin
  try
    S := IInterface(Stream) as IisStream;
    ReadBytes := S.Read(Buffer^, BufferSize);
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function GetStreamPosition(Stream: ObjRef; out Position: Integer): ErrObjRef; stdcall;
var
  S: IisStream;
begin
  try
    S := IInterface(Stream) as IisStream;
    Position := S.Position;
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function SetStreamPosition(Stream: ObjRef; Position: Integer): ErrObjRef; stdcall;
var
  S: IisStream;
begin
  try
    S := IInterface(Stream) as IisStream;
    S.Position := Position;
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function GetStreamSize(Stream: ObjRef; out Size: Integer): ErrObjRef; stdcall;
var
  S: IisStream;
begin
  try
    S := IInterface(Stream) as IisStream;
    Size := S.Size;
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function SetStreamSize(Stream: ObjRef; Size: Integer): ErrObjRef; stdcall;
var
  S: IisStream;
begin
  try
    S := IInterface(Stream) as IisStream;
    S.Size := Size;
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;

function NewContext(AppID, AnonymousUser: LPTSTR; ConnectionsPerSession: Integer; Host, Port, User, Password: LPTSTR; out Context: ObjRef): ErrObjRef; stdcall;
begin
  try
    Context := MakeRef(Connector.CreateContext(AppID, AnonymousUser, ConnectionsPerSession, Host, Port, User, Password));
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function SendRequest(Context: ObjRef; Method: LPTSTR; ParamsStream: ObjRef; out ResultStream: ObjRef): ErrObjRef; stdcall;
var
  C: IevContext;
  Par: IisStream;
begin
  try
    C := IInterface(Context) as IevContext;
    Par := IInterface(ParamsStream) as IisStream;

    ResultStream := MakeRef(Connector.SendRequest(C, Method, Par));
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;


function HashPassword(Password: LPTSTR; HashedPassword: LPTSTR): ErrObjRef; stdcall;
var
  s: String;
begin
  try
    s := EvBasicUtils.HashPassword(Password);
    StrPCopy(HashedPassword, s);
    Result := nil;
  except
    on E: Exception do
    begin
      Result := MakeError(E);
      LogError(E);
    end;
  end;
end;



exports GetErrorText;
exports ReleaseObject;

exports Initialize;
exports Uninitialize;

exports NewStream;
exports WriteToStream;
exports ReadFromStream;
exports GetStreamPosition;
exports SetStreamPosition;
exports GetStreamSize;
exports SetStreamSize;

exports NewContext;
exports SendRequest;

exports HashPassword;

end.