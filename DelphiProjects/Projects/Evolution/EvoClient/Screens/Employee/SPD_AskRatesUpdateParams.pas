// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_AskRatesUpdateParams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_AskGlobalUpdateParamsBase, ActnList,  ExtCtrls,
  StdCtrls, Buttons, ISBasicClasses, EvUIComponents, isUIEdit,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TUpdateTarget = ( utRates, utSalaries );
  TRatesUpdateMode = ( rumOnAmount, rumOnPercentage, rumToAmount );

  TRatesUpdateParamsRec = record
    Target: TUpdateTarget;
    ForHourlyOnly: boolean;
    RangeLow: Variant;
    RangeHigh: Variant;
    UpdateMode: TRatesUpdateMode;
    Value: Variant;
    RoundTo2Decimal: boolean;
  end;

  TAskRatesUpdateParams = class(TAskGlobalUpdateParamsBase)
    evGroupBox1: TevGroupBox;
    rbOnAmount: TevRadioButton;
    rbOnPercent: TevRadioButton;
    edOnAmount: TevEdit;
    edOnPercent: TevEdit;
    evGroupBox2: TevGroupBox;
    edRangeLow: TevEdit;
    edRangeHigh: TevEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    rbToAmount: TevRadioButton;
    edToAmount: TevEdit;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evGroupBox3: TevGroupBox;
    rbSalaries: TevRadioButton;
    rbRates: TevRadioButton;
    cbHourlyOnly: TevCheckBox;
    HourlyOnly: TAction;
    GroupBox1: TGroupBox;
    StaticText1: TStaticText;
    evcbDecimal: TevCheckBox;
    procedure OKUpdate(Sender: TObject);
    procedure edOnAmountChange(Sender: TObject);
    procedure edOnPercentChange(Sender: TObject);
    procedure edToAmountChange(Sender: TObject);
    procedure HourlyOnlyUpdate(Sender: TObject);
    procedure rbOnPercentClick(Sender: TObject);
  private
    function UpdateMode: TRatesUpdateMode;
  public
    function Params: TRatesUpdateParamsRec;
  end;

implementation

{$R *.dfm}

function TAskRatesUpdateParams.Params: TRatesUpdateParamsRec;
begin
  if rbSalaries.Checked then
    Result.Target := utSalaries
  else if rbRates.Checked then
    Result.Target := utRates
  else
    Assert(false);

  Result.ForHourlyOnly := cbHourlyOnly.Checked;
  Result.RoundTo2Decimal := evcbDecimal.Checked;

  Result.UpdateMode := UpdateMode;

  case UpdateMode of
    rumOnAmount: Result.Value := strtofloat( edOnAmount.Text );
    rumOnPercentage: Result.Value := strtofloat( edOnPercent.Text );
    rumToAmount: Result.Value := strtofloat( edToAmount.Text );
  else
    Assert( false );
  end;

  if trim(edRangeHigh.Text) <> '' then
    Result.RangeHigh := edRangeHigh.Text
  else
    Result.RangeHigh := Null;

  if trim(edRangeLow.Text) <> '' then
    Result.RangeLow := edRangeLow.Text
  else
    Result.RangeLow := Null;
end;

function TAskRatesUpdateParams.UpdateMode: TRatesUpdateMode;
begin
  if rbOnAmount.Checked then
    Result := rumOnAmount
  else if rbOnPercent.Checked then
    Result := rumOnPercentage
  else if rbToAmount.Checked then
    Result := rumToAmount
  else
  begin
    assert( false );
    Result := rumToAmount;
  end
end;

procedure TAskRatesUpdateParams.OKUpdate(Sender: TObject);
var
  ok: boolean;
begin
  ok := true;
  case UpdateMode of
    rumOnAmount: ok := IsFloat( edOnAmount.Text );
    rumOnPercentage: ok := IsFloat( edOnPercent.Text );
    rumToAmount: ok := IsFloat( edToAmount.Text );
  else
    Assert( false );
  end;
  ok := ok and ( (trim(edRangeLow.Text) = '') or IsFloat( edRangeLow.Text ) )
           and ( (trim(edRangeHigh.Text) = '') or IsFloat( edRangeHigh.Text ) );
  (Sender as TCustomAction).Enabled := ok;
end;

procedure TAskRatesUpdateParams.edOnAmountChange(Sender: TObject);
begin
  rbOnAmount.Checked := true;
end;

procedure TAskRatesUpdateParams.edOnPercentChange(Sender: TObject);
begin
  rbOnPercent.Checked := true;
end;

procedure TAskRatesUpdateParams.edToAmountChange(Sender: TObject);
begin
  rbToAmount.Checked := true;
end;

procedure TAskRatesUpdateParams.HourlyOnlyUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := rbRates.Checked;
  if not rbRates.Checked then
    (Sender as TCustomAction).Checked := false;
end;

procedure TAskRatesUpdateParams.rbOnPercentClick(Sender: TObject);
begin
  inherited;
    evcbDecimal.Enabled := rbOnPercent.Checked;
    if not evcbDecimal.Enabled then
      evcbDecimal.Checked := False;
end;

end.
