// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_POPUP_EDIT_EE_LOCALS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SDataStructure,
  ActnList, ISBasicClasses,  SDDClasses, DB,
  Wwdatsrc, StdCtrls, ExtCtrls, DBCtrls, Mask, wwdbedit, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, EvUtils, Forms, SDataDictclient, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel;

type
  TPOPUP_EDIT_EE_LOCALS = class(TForm)
    evPanel1: TevPanel;
    evPanel3: TevPanel;
    DM_COMPANY: TDM_COMPANY;
    evActionList1: TevActionList;
    Add: TAction;
    Delete: TAction;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    evDataSource1: TevDataSource;
    evDataSource2: TevDataSource;
    fpCompanLocalTax: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    fpEmployeeLocals: TisUIFashionPanel;
    evPanel5: TevPanel;
    evPanel2: TevPanel;
    evSpeedButton1: TevSpeedButton;
    SpeedButton2: TevSpeedButton;
    evCheckBox1: TevCheckBox;
    wwdgLocal_Taxes: TevDBGrid;
    evPanel4: TevPanel;
    evLabel1: TevLabel;
    lablCounty: TevLabel;
    edPercOfTaxW: TevDBEdit;
    dedtCounty: TevDBEdit;
    rgrpAlwaysDeduct: TevDBRadioGroup;
    evBitBtn1: TevBitBtn;
    procedure evSpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure AddUpdate(Sender: TObject);
    procedure DeleteUpdate(Sender: TObject);
    procedure evCheckBox1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FOldIndexName: string;
    FOldIndexFieldNames: string;

    { Private declarations }
  public
    function ShowModal: Integer; override;
    procedure SetDataSources(dsEELocals, dsCoLocalTax: TEvDataSource);
    { Public declarations }
  end;

implementation

uses
  Dialogs, EvConsts;
{$R *.DFM}

{ TTPOPUP_EDIT_EE_LOCALS }

procedure TPOPUP_EDIT_EE_LOCALS.SetDataSources(dsEELocals, dsCoLocalTax: TEvDataSource);
begin
  evDbGrid1.DataSource := dsCoLocalTax;
  wwdgLocal_Taxes.DataSource := dsEELocals;
  dedtCounty.DataSource := dsEELocals;
  edPercOfTaxW.DataSource := dsEELocals;
  rgrpAlwaysDeduct.DataSource := dsEELocals;
end;

procedure TPOPUP_EDIT_EE_LOCALS.evSpeedButton1Click(Sender: TObject);
var
  i: integer;
  Pos: TBookmark;
  cotax: TDataSet;

  procedure DoTransfer;
  var
    locals: TDataSet;
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', cotax.fieldByName('LocalState').Value, []) then
     if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
     begin
       EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
       exit;
     end;
    DM_EMPLOYEE.EE_STATES.DisableControls;
    try
      locals := wwdgLocal_Taxes.DataSource.DataSet;
      locals.Insert;
      locals.FieldByName('CO_LOCAL_TAX_NBR').Value := cotax.fieldByName('CO_LOCAL_TAX_NBR').Value;
      if DM_EMPLOYEE.EE['COMPANY_OR_INDIVIDUAL_NAME'] <> 'C' then
        locals.FieldByName('EXEMPT_EXCLUDE').Value := GROUP_BOX_INCLUDE
      else
        locals.FieldByName('EXEMPT_EXCLUDE').Value := GROUP_BOX_EXEMPT;

      locals.FieldByName('MISCELLANEOUS_AMOUNT').Value := cotax.fieldByName('MISCELLANEOUS_AMOUNT').Value;
      locals.FieldByName('FILLER').Value := cotax.fieldByName('FILLER').Value;
      locals.FieldByName('EE_TAX_CODE').Value := cotax.fieldByName('TAX_RETURN_CODE').Value;
      locals.FieldByName('DEDUCT').AsString := GetCoLocalTaxDeduct(cotax.fieldByName('DEDUCT').AsString, cotax.fieldByName('LOCAL_TYPE').AsString );

      if Locals.fieldbyname('CO_LOCAL_TAX_NBR').ASSTRING <> '' then
      begin
        DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', Locals.fieldbyname('CO_LOCAL_TAX_NBR').Value, []);
        if DM_SYSTEM.SY_LOCALS.Locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX.FIELDBYNAME('SY_LOCALS_NBR').AsString, []) then
        begin
          locals.FieldByName('EE_COUNTY').AsString := DM_SYSTEM.SY_COUNTY.Lookup('SY_COUNTY_NBR', DM_SYSTEM.SY_LOCALS.FIELDBYNAME('SY_COUNTY_NBR').AsString, 'COUNTY_NAME');
        end;
      end;
      locals.Post;
    finally
      DM_EMPLOYEE.EE_STATES.EnableControls;
    end;
  end;

begin
  inherited;
  cotax := evDbgrid1.DataSource.DataSet;

  if wwdgLocal_Taxes.DataSource.DataSet.State in [dsInsert, dsEdit] then
    wwdgLocal_Taxes.DataSource.DataSet.Post;
  if wwdgLocal_Taxes.DataSource.DataSet.State in [dsBrowse] then
    if evDbgrid1.SelectedList.Count > 0 then
    begin
      Pos := cotax.GetBookMark;
      try
        cotax.DisableControls;
        for i := 0 to evDbgrid1.SelectedList.Count - 1 do
        begin
          cotax.GotoBookmark(evDbgrid1.SelectedList.items[i]);
          if not wwdgLocal_Taxes.DataSource.DataSet.Locate('CO_LOCAL_TAX_NBR', cotax.FieldByName('CO_LOCAL_TAX_NBR').Value, []) then
            DoTransfer;
              //    EvMessage(FieldByName(��).asString);
        end;
        cotax.gotoBookMark(Pos);
        cotax.EnableControls;
      finally
        cotax.FreeBookMark(Pos);
      end;
    end
    else
      if not wwdgLocal_Taxes.DataSource.DataSet.Locate('CO_LOCAL_TAX_NBR', cotax.FieldByName('CO_LOCAL_TAX_NBR').Value, []) then
        DoTransfer;
end;

procedure TPOPUP_EDIT_EE_LOCALS.SpeedButton2Click(Sender: TObject);
begin
  if (wwdgLocal_Taxes.DataSource.DataSet.State in [dsBrowse]) and (wwdgLocal_Taxes.DataSource.DataSet.RecordCount > 0) then
    if EvMessage('Delete record.  Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes then
      wwdgLocal_Taxes.DataSource.DataSet.Delete;
end;

procedure TPOPUP_EDIT_EE_LOCALS.AddUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := evDbgrid1.DataSource.DataSet.RecordCount > 0;
end;

procedure TPOPUP_EDIT_EE_LOCALS.DeleteUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (wwdgLocal_Taxes.DataSource.DataSet.State in [dsBrowse]) and (wwdgLocal_Taxes.DataSource.DataSet.RecordCount > 0);
end;

procedure TPOPUP_EDIT_EE_LOCALS.evCheckBox1Click(Sender: TObject);
begin
  if (Sender as TevCheckBox).checked then
    (wwdgLocal_Taxes.DataSource.DataSet as TEvClientDataSet).IndexFieldNames := 'EE_COUNTY;Local_Name_Lookup'
  else
    (wwdgLocal_Taxes.DataSource.DataSet as TEvClientDataSet).IndexFieldNames := '';
end;

procedure TPOPUP_EDIT_EE_LOCALS.FormShow(Sender: TObject);
begin
  FOldIndexFieldNames := (wwdgLocal_Taxes.DataSource.DataSet as TEvClientDataSet).IndexFieldNames;
  FOldIndexName := (wwdgLocal_Taxes.DataSource.DataSet as TEvClientDataSet).IndexName;
  evCheckBox1.Checked := false;
end;

procedure TPOPUP_EDIT_EE_LOCALS.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  (wwdgLocal_Taxes.DataSource.DataSet as TEvClientDataSet).IndexFieldNames := FOldIndexFieldNames;
  (wwdgLocal_Taxes.DataSource.DataSet as TEvClientDataSet).IndexName := FOldIndexName;
end;

function TPOPUP_EDIT_EE_LOCALS.ShowModal: Integer;
begin
  DM_COMPANY.CO_LOCAL_TAX.Filter := 'LOCAL_ACTIVE = ''' + GROUP_BOX_YES + '''';;
  DM_COMPANY.CO_LOCAL_TAX.Filtered := True;
  DM_EMPLOYEE.EE_LOCALS.Filter := 'LOCAL_ENABLED <> ''' + GROUP_BOX_NO + '''';
  DM_EMPLOYEE.EE_LOCALS.Filtered := true;
  try
    Result := inherited ShowModal;
  finally
    DM_EMPLOYEE.EE_LOCALS.Filter := '';
    DM_EMPLOYEE.EE_LOCALS.Filtered := false;
    DM_COMPANY.CO_LOCAL_TAX.Filter := '';;
    DM_COMPANY.CO_LOCAL_TAX.Filtered := false;
  end;
end;

end.

