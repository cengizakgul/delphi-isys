object EDChangeForm: TEDChangeForm
  Left = 347
  Top = 316
  Width = 553
  Height = 289
  Caption = 'Refresh check lines'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    537
    250)
  PixelsPerInch = 96
  TextHeight = 13
  object RefreshGrid: TevDBGrid
    Left = 12
    Top = 12
    Width = 519
    Height = 199
    DisableThemesInTitle = False
    ControlType.Strings = (
      'CHECKED;CheckBox;Y;')
    Selected.Strings = (
      'CHECKED'#9'2'#9' '#9'F'
      'CHECK_DATE'#9'18'#9'Check Date'#9'T'
      'RUN_NUMBER'#9'10'#9'Run #'#9'T'
      'CUSTOM_EMPLOYEE_NUMBER'#9'9'#9'Ee #'#9'T'
      'EE_NAME'#9'30'#9'Ee Name'#9'T'
      'CO_NAME'#9'30'#9'Company Name'#9'T'
      'CL_NAME'#9'30'#9'Client Name'#9'T')
    IniAttributes.Enabled = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDChangeForm\RefreshGrid'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm, ecoDisableDateTimePicker]
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = RDSource
    KeyOptions = [dgEnterToTab]
    ReadOnly = False
    TabOrder = 3
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    UseTFields = True
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = '-'
    NoFire = False
  end
  object btnRefresh: TevButton
    Left = 372
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Refresh All'
    ModalResult = 1
    TabOrder = 1
    OnClick = btnRefreshClick
    Margin = 0
  end
  object btnCancel: TevButton
    Left = 453
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    Margin = 0
  end
  object btnSelectAll: TevButton
    Left = 17
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Select All'
    TabOrder = 4
    OnClick = btnSelectAllClick
    Margin = 0
  end
  object btnUnselectAll: TevButton
    Left = 98
    Top = 225
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Unselect All'
    TabOrder = 5
    OnClick = btnUnselectAllClick
    Margin = 0
  end
  object evButton1: TevButton
    Left = 224
    Top = 225
    Width = 141
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Refresh All Except Manual'
    ModalResult = 1
    TabOrder = 0
    OnClick = btnRefreshClick
    Margin = 0
  end
  object RDSet: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'RUN_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_EMPLOYEE_NUMBER'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'CO_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CL_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FIRST_NAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'MIDDLE_INITIAL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LAST_NAME'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHECKED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EE_NAME'
        DataType = ftString
        Size = 40
      end>
    OnCalcFields = RDSetCalcFields
    Left = 279
    Top = 138
    object RDSetCHECKED: TStringField
      DisplayLabel = ' '
      DisplayWidth = 2
      FieldKind = fkInternalCalc
      FieldName = 'CHECKED'
      Size = 1
    end
    object RDSetRUN_NUMBER: TIntegerField
      DisplayLabel = 'Run #'
      DisplayWidth = 10
      FieldName = 'RUN_NUMBER'
    end
    object RDSetCUSTOM_EMPLOYEE_NUMBER: TStringField
      DisplayLabel = 'Ee #'
      DisplayWidth = 9
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object RDSetEE_NAME: TStringField
      DisplayLabel = 'Ee Name'
      DisplayWidth = 30
      FieldKind = fkInternalCalc
      FieldName = 'EE_NAME'
      Size = 40
    end
    object RDSetCO_NAME: TStringField
      DisplayLabel = 'Company Name'
      DisplayWidth = 30
      FieldName = 'CO_NAME'
      Size = 40
    end
    object RDSetCL_NAME: TStringField
      DisplayLabel = 'Client Name'
      DisplayWidth = 30
      FieldName = 'CL_NAME'
      Size = 40
    end
    object RDSetCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object RDSetCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object RDSetEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Visible = False
    end
    object RDSetFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Visible = False
    end
    object RDSetMIDDLE_INITIAL: TStringField
      FieldName = 'MIDDLE_INITIAL'
      Visible = False
      Size = 1
    end
    object RDSetLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Visible = False
      Size = 30
    end
    object RDSetPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Visible = False
    end
    object RDSetCHECK_DATE: TDateField
      DisplayLabel = 'Check Date'
      FieldName = 'CHECK_DATE'
    end
  end
  object RDSource: TevDataSource
    DataSet = RDSet
    Left = 315
    Top = 138
  end
end
