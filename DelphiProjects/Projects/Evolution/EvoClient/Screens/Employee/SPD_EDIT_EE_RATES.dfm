inherited EDIT_EE_RATES: TEDIT_EE_RATES
  inherited PageControl1: TevPageControl
    HelpContext = 19002
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 167
                IniAttributes.SectionName = 'TEDIT_EE_RATES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 167
                IniAttributes.SectionName = 'TEDIT_EE_RATES\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtRates: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbRates: TScrollBox
        Left = 0
        Top = 0
        Width = 435
        Height = 161
        VertScrollBar.Position = 162
        Align = alClient
        TabOrder = 0
        object pnlPayRateInfo: TisUIFashionPanel
          Left = 8
          Top = -154
          Width = 339
          Height = 170
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Salary Information'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablSalary_Amount: TevLabel
            Left = 12
            Top = 35
            Width = 68
            Height = 13
            Caption = 'Salary Amount'
            FocusControl = dedtSalary_Amount
          end
          object lablCalculated_Salary: TevLabel
            Left = 12
            Top = 74
            Width = 84
            Height = 13
            Caption = 'Annualized Salary'
          end
          object lablStandard_Hours: TevLabel
            Left = 110
            Top = 35
            Width = 74
            Height = 13
            Caption = 'Standard Hours'
          end
          object lablPay_Frequency: TevLabel
            Left = 110
            Top = 74
            Width = 80
            Height = 16
            Caption = '~Pay Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 113
            Width = 71
            Height = 13
            Caption = 'Average Hours'
          end
          object evLabel2: TevLabel
            Left = 110
            Top = 113
            Width = 89
            Height = 13
            Caption = 'Calc Annual Salary'
          end
          object lablWorkers_Comp_Wage_Limit: TevLabel
            Left = 208
            Top = 35
            Width = 53
            Height = 13
            Caption = 'Wage Limit'
            FocusControl = dedtWorkers_Comp_Wage_Limit
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel
            Left = 208
            Top = 74
            Width = 106
            Height = 13
            Caption = 'Wage Limit Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtSalary_Amount: TevDBEdit
            Left = 12
            Top = 50
            Width = 90
            Height = 21
            HelpContext = 19002
            DataField = 'SALARY_AMOUNT'
            DataSource = wwdsSubMaster
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtCalculated_Salary: TevDBEdit
            Left = 12
            Top = 89
            Width = 90
            Height = 21
            HelpContext = 19002
            DataField = 'CALCULATED_SALARY'
            DataSource = wwdsSubMaster
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtStandard_Hours: TevDBEdit
            Left = 110
            Top = 50
            Width = 90
            Height = 21
            HelpContext = 19002
            DataField = 'STANDARD_HOURS'
            DataSource = wwdsSubMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbPay_Frequency: TevDBComboBox
            Left = 110
            Top = 89
            Width = 90
            Height = 21
            HelpContext = 19002
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PAY_FREQUENCY'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'W'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'S'
              'Monthly'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object edtAvHours: TevDBEdit
            Left = 12
            Top = 128
            Width = 90
            Height = 21
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = edtAvHoursExit
            Glowing = False
          end
          object eCASalary: TevDBEdit
            Left = 110
            Top = 128
            Width = 90
            Height = 21
            HelpContext = 19002
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtWorkers_Comp_Wage_Limit: TevDBEdit
            Left = 208
            Top = 50
            Width = 107
            Height = 21
            HelpContext = 19002
            DataField = 'WORKERS_COMP_WAGE_LIMIT'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox1: TevDBComboBox
            Left = 208
            Top = 89
            Width = 107
            Height = 21
            HelpContext = 19002
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'WC_WAGE_LIMIT_FREQUENCY'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 0
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
        end
        object fpOverride: TisUIFashionPanel
          Left = 8
          Top = 24
          Width = 591
          Height = 334
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Override Information'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablHome_Division: TevLabel
            Left = 309
            Top = 67
            Width = 37
            Height = 13
            Caption = 'Division'
          end
          object lablHome_Branch: TevLabel
            Left = 309
            Top = 92
            Width = 34
            Height = 13
            Caption = 'Branch'
          end
          object lablHome_Department: TevLabel
            Left = 309
            Top = 117
            Width = 55
            Height = 13
            Caption = 'Department'
          end
          object lablHome_Team: TevLabel
            Left = 309
            Top = 143
            Width = 27
            Height = 13
            Caption = 'Team'
          end
          object lablJobs_Number: TevLabel
            Left = 309
            Top = 171
            Width = 62
            Height = 13
            Caption = 'Jobs Number'
          end
          object SpeedButton1: TevSpeedButton
            Left = 372
            Top = 38
            Width = 188
            Height = 21
            Caption = 'Assign D/B/D/T'
            HideHint = True
            AutoSize = False
            OnClick = SpeedButton1Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
              77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
              7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
              8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
              7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
              AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
              2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
              7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
            ParentColor = False
            ShortCut = 115
          end
          object lablRate_Number: TevLabel
            Left = 115
            Top = 171
            Width = 72
            Height = 16
            Alignment = taRightJustify
            Caption = '~Rate Number'
            FocusControl = dedtRate_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablRate_Amount: TevLabel
            Left = 206
            Top = 171
            Width = 71
            Height = 16
            Alignment = taRightJustify
            Caption = '~Rate Amount'
            FocusControl = dedtRate_Amount
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablHR_Salary_Grades_Number: TevLabel
            Left = 309
            Top = 264
            Width = 50
            Height = 13
            Caption = 'Pay Grade'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 309
            Top = 225
            Width = 105
            Height = 13
            Caption = 'Position for Pay Grade'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cbDivisionName: TevDBLookupCombo
            Left = 470
            Top = 63
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 8
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbBranchName: TevDBLookupCombo
            Left = 470
            Top = 88
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 10
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbTeamName: TevDBLookupCombo
            Left = 470
            Top = 139
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 14
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbDepartmentName: TevDBLookupCombo
            Left = 470
            Top = 113
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 12
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcJobs_Number: TevDBLookupCombo
            Left = 309
            Top = 186
            Width = 251
            Height = 21
            HelpContext = 19002
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Code'#9'F'
              'TRUE_DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'CO_JOBS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_JOBS.CO_JOBS
            LookupField = 'CO_JOBS_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evGroupBox1: TevGroupBox
            Left = 311
            Top = 800
            Width = 232
            Height = 104
            Caption = 'Combined Position '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
          end
          object evGroupBox2: TevGroupBox
            Left = 12
            Top = 210
            Width = 277
            Height = 104
            Caption = 'Workers Comp'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            object Label6: TevLabel
              Left = 12
              Top = 15
              Width = 25
              Height = 13
              Caption = 'Code'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label1: TevLabel
              Left = 12
              Top = 54
              Width = 53
              Height = 13
              Caption = 'Description'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label27: TevLabel
              Left = 183
              Top = 54
              Width = 25
              Height = 13
              Caption = 'State'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object wwDBLookupCombo1: TevDBLookupCombo
              Left = 12
              Top = 30
              Width = 250
              Height = 21
              HelpContext = 19002
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
                'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F'
                'Co_State_Lookup'#9'2'#9'Co_State_Lookup'#9'F')
              DataField = 'CO_WORKERS_COMP_NBR'
              DataSource = wwdsDetail
              LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
              LookupField = 'CO_WORKERS_COMP_NBR'
              Style = csDropDownList
              ParentFont = False
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
            object wwDBLookupCombo2: TevDBLookupCombo
              Left = 12
              Top = 68
              Width = 161
              Height = 21
              HelpContext = 19002
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
              DataField = 'CO_WORKERS_COMP_NBR'
              DataSource = wwdsDetail
              LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
              LookupField = 'CO_WORKERS_COMP_NBR'
              Style = csDropDownList
              Enabled = False
              ParentFont = False
              TabOrder = 1
              AutoDropDown = True
              ShowButton = False
              PreciseEditRegion = False
              AllowClearKey = False
            end
            object wwDBLookupCombo4: TevDBLookupCombo
              Left = 183
              Top = 68
              Width = 79
              Height = 21
              HelpContext = 19002
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'Co_State_Lookup'#9'2'#9#9'F'
                'StateName'#9'20'#9'State'#9'F')
              DataField = 'CO_WORKERS_COMP_NBR'
              DataSource = wwdsDetail
              LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
              LookupField = 'CO_WORKERS_COMP_NBR'
              Style = csDropDownList
              Enabled = False
              ParentFont = False
              TabOrder = 2
              AutoDropDown = True
              ShowButton = False
              PreciseEditRegion = False
              AllowClearKey = False
            end
          end
          object wwDBGrid1: TevDBGrid
            Left = 12
            Top = 40
            Width = 277
            Height = 118
            HelpContext = 19002
            DisableThemesInTitle = False
            Selected.Strings = (
              'RATE_NUMBER'#9'20'#9'Rate Number'#9'F'
              'RATE_AMOUNT'#9'19'#9'Rate Amount'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_RATES\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object cbDivisionNbr: TevDBLookupCombo
            Left = 372
            Top = 63
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DIVISION_NUMBER'#9'20'#9'CUSTOM_DIVISION_NUMBER')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 7
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbBranchNbr: TevDBLookupCombo
            Left = 372
            Top = 88
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BRANCH_NUMBER'#9'20'#9'CUSTOM_BRANCH_NUMBER')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 9
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbDepartmentNbr: TevDBLookupCombo
            Left = 372
            Top = 113
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DEPARTMENT_NUMBER'#9'20'#9'CUSTOM_DEPARTMENT_NUMBER')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 11
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbTeamNbr: TevDBLookupCombo
            Left = 372
            Top = 139
            Width = 90
            Height = 21
            HelpContext = 19002
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_TEAM_NUMBER'#9'20'#9'CUSTOM_TEAM_NUMBER')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 13
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object drgpPrimary_Rate: TevDBRadioGroup
            Left = 16
            Top = 171
            Width = 92
            Height = 36
            HelpContext = 19002
            Caption = '~Primary Rate'
            Columns = 2
            DataField = 'PRIMARY_RATE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object dedtRate_Number: TevDBEdit
            Left = 115
            Top = 186
            Width = 83
            Height = 21
            HelpContext = 19002
            DataField = 'RATE_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtRate_Amount: TevDBEdit
            Left = 206
            Top = 186
            Width = 83
            Height = 21
            HelpContext = 19002
            DataField = 'RATE_AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '[-]*12[#][.*4[#]]'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = dedtRate_AmountExit
            Glowing = False
          end
          object evcbPosition: TevDBLookupCombo
            Left = 309
            Top = 240
            Width = 251
            Height = 21
            HelpContext = 19002
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_HR_POSITIONS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_POSITIONS.CO_HR_POSITIONS
            LookupField = 'CO_HR_POSITIONS_NBR'
            Style = csDropDownList
            ParentFont = False
            PopupMenu = pmPosition
            TabOrder = 15
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = evcbPositionChange
            OnCloseUp = evcbPositionCloseUp
          end
          object wwlcHR_Salary_Grades_Number: TevDBLookupCombo
            Left = 309
            Top = 278
            Width = 251
            Height = 21
            HelpContext = 19002
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'30'#9'Pay Grade'#9'F'
              'MINIMUM_PAY'#9'10'#9'Minimum Pay'#9'F'
              'MID_PAY'#9'10'#9'Mid Pay'#9'F'
              'MAXIMUM_PAY'#9'10'#9'Maximum Pay'#9'F')
            DataField = 'CO_HR_POSITION_GRADES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_POSITION_GRADES.CO_HR_POSITION_GRADES
            LookupField = 'CO_HR_POSITION_GRADES_NBR'
            Options = [loTitles]
            Style = csDropDownList
            ParentFont = False
            TabOrder = 16
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
        object fpFutureUpdates: TisUIFashionPanel
          Left = 355
          Top = -154
          Width = 244
          Height = 170
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Future Updates (Informational only)'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablFuture_Raise_Date: TevLabel
            Left = 12
            Top = 35
            Width = 53
            Height = 13
            Caption = 'Raise Date'
          end
          object lablFuture_Raise_Amount: TevLabel
            Left = 12
            Top = 74
            Width = 66
            Height = 13
            Caption = 'Raise Amount'
          end
          object lablFuture_Raise_Percentage: TevLabel
            Left = 12
            Top = 113
            Width = 38
            Height = 13
            Caption = 'Raise %'
          end
          object lablFuture_Pay_Frequency: TevLabel
            Left = 117
            Top = 74
            Width = 71
            Height = 13
            Caption = 'Pay Frequency'
          end
          object lablFuture_Raise_Rate: TevLabel
            Left = 117
            Top = 35
            Width = 53
            Height = 13
            Caption = 'Raise Rate'
          end
          object wwDBDateTimePicker1: TevDBDateTimePicker
            Left = 12
            Top = 50
            Width = 97
            Height = 21
            HelpContext = 19002
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'NEXT_RAISE_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 0
          end
          object dedtFuture_Raise_Amount: TevDBEdit
            Left = 12
            Top = 89
            Width = 97
            Height = 21
            HelpContext = 19002
            DataField = 'NEXT_RAISE_AMOUNT'
            DataSource = wwdsSubMaster
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtFuture_Raise_Percentage: TevDBEdit
            Left = 12
            Top = 128
            Width = 97
            Height = 21
            HelpContext = 19002
            DataField = 'NEXT_RAISE_PERCENTAGE'
            DataSource = wwdsSubMaster
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbFuture_Pay_Frequency: TevDBComboBox
            Left = 117
            Top = 89
            Width = 97
            Height = 21
            HelpContext = 19002
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'NEXT_PAY_FREQUENCY'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'W'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'S'
              'Monthly'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object DBEdit1: TevDBEdit
            Left = 117
            Top = 50
            Width = 97
            Height = 21
            HelpContext = 19002
            DataField = 'NEXT_RAISE_RATE'
            DataSource = wwdsSubMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 117
            Top = 113
            Width = 97
            Height = 36
            HelpContext = 19002
            Caption = 'Auto update rates'
            Columns = 2
            DataField = 'AUTO_UPDATE_RATES'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    inherited dblkupEE_NBR: TevDBLookupCombo
      Top = 7
    end
    inherited dbtxtname: TevDBLookupCombo
      Top = 7
    end
    inherited butnPrior: TevBitBtn
      Top = 4
    end
    inherited butnNext: TevBitBtn
      Top = 4
    end
    object evBitBtn1: TevBitBtn
      Left = 389
      Top = 4
      Width = 129
      Height = 25
      Action = UpdateRates
      TabOrder = 4
      Color = clBlack
      NumGlyphs = 2
      Layout = blGlyphRight
      Margin = 0
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_RATES.EE_RATES
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    Left = 198
    Top = 18
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 23
  end
  inherited PageControlImages: TevImageList
    Left = 584
    Top = 56
  end
  inherited dsCL: TevDataSource
    Left = 251
    Top = 27
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 340
    Top = 19
  end
  inherited DM_COMPANY: TDM_COMPANY
    Top = 18
  end
  inherited wwdsEmployee: TevDataSource
    Left = 646
    Top = 56
  end
  inherited DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 412
    Top = 86
  end
  inherited wwdsSubMaster: TevDataSource
    OnDataChange = wwdsSubMasterDataChange
    Top = 85
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 505
    Top = 82
  end
  inherited ActionList: TevActionList
    Left = 375
    Top = 99
  end
  object evProxyDataSet1: TevProxyDataSet
    Dataset = DM_EE_RATES.EE_RATES
    BeforeEdit = evProxyDataSet1BeforeEdit
    BeforePost = evProxyDataSet1BeforePost
    AfterPost = evProxyDataSet1AfterPost
    Left = 584
    Top = 82
  end
  object evActionList1: TevActionList
    Left = 616
    Top = 80
    object UpdateRates: TAction
      Caption = 'Update Rates or Salaries'
      OnExecute = UpdateRatesExecute
      OnUpdate = UpdateRatesUpdate
    end
    object aClearDBDT: TAction
      Caption = 'Clear DBDT'
      ShortCut = 8307
      OnExecute = aClearDBDTExecute
    end
  end
  object evPopupMenu1: TevPopupMenu
    Left = 616
    Top = 56
    object FocusMain1: TMenuItem
      Action = UpdateRates
    end
  end
  object pmPosition: TPopupMenu
    Left = 560
    Top = 83
    object mnPositionCopy: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnPositionCopyClick
    end
  end
end
