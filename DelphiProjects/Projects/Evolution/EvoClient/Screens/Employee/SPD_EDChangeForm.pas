// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDChangeForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, Wwdbigrd, Wwdbgrid,  Db, Wwdatsrc, 
  StdCtrls, EvUtils, SDataStructure, EvConsts, Variants, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, EvDataAccessComponents, EvContext,
  ISDataAccessComponents, EvUIComponents, EvClientDataSet, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TEDChangeForm = class(TForm)
    RefreshGrid: TevDBGrid;
    RDSet: TevClientDataSet;
    RDSource: TevDataSource;
    btnRefresh: TevButton;
    btnCancel: TevButton;
    btnSelectAll: TevButton;
    btnUnselectAll: TevButton;
    RDSetCHECKED: TStringField;
    RDSetCL_NAME: TStringField;
    RDSetCL_NBR: TIntegerField;
    RDSetCO_NAME: TStringField;
    RDSetCO_NBR: TIntegerField;
    RDSetEE_NBR: TIntegerField;
    RDSetCUSTOM_EMPLOYEE_NUMBER: TStringField;
    RDSetFIRST_NAME: TStringField;
    RDSetMIDDLE_INITIAL: TStringField;
    RDSetLAST_NAME: TStringField;
    RDSetPR_NBR: TIntegerField;
    RDSetRUN_NUMBER: TIntegerField;
    RDSetEE_NAME: TStringField;
    RDSetCHECK_DATE: TDateField;
    evButton1: TevButton;
    procedure RDSetCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnSelectAllClick(Sender: TObject);
    procedure btnUnselectAllClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
  private
    procedure RDSetFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ChangeValue(Value: string);
  public
    ChangeList: TevClientDataSet;
  end;

implementation

{$R *.DFM}

procedure TEDChangeForm.ChangeValue(Value: string);
var
  Mark: TBookmarkStr;
begin
  Mark := RDSet.Bookmark;
  RDSet.DisableControls;
  try
    RDSet.First;
    while not RDSet.Eof do
    begin
      RDSet.Edit;
      RDSet['Checked'] := Value;
      RDSet.Post;
      RDSet.Next;
    end;
  finally
    RDSet.Bookmark := Mark;
    RDSet.EnableControls;
  end;
end;

procedure TEDChangeForm.RDSetCalcFields(DataSet: TDataSet);
begin
  DataSet['EE_NAME'] := DataSet.FieldByName('LAST_NAME').AsString + ' ' + DataSet.FieldByName('FIRST_NAME').AsString;
end;

procedure TEDChangeForm.FormShow(Sender: TObject);
begin
  ChangeValue('Y');
end;

procedure TEDChangeForm.btnSelectAllClick(Sender: TObject);
begin
  ChangeValue('Y');
end;

procedure TEDChangeForm.btnUnselectAllClick(Sender: TObject);
begin
  ChangeValue('');
end;

procedure TEDChangeForm.btnRefreshClick(Sender: TObject);
var
  i: Integer;
  PrCheckNbr: Integer;
  bPaySalary: Boolean;
  MY_EE_SCHEDULED_E_DS: TevClientDataset;
begin
  try
    ctx_StartWait;
    RDSet.DisableControls;

    try
      RDSet.First;
      while not RDSet.Eof do
      begin
        if RDSet.FieldByName('Checked').AsString = 'Y' then
        begin

          ctx_DataAccess.OpenClient(RDSet['Cl_Nbr']);

          DM_COMPANY.CO.DataRequired('CO_NBR = ' + RDSet.FieldByName('Co_Nbr').AsString);
          DM_EMPLOYEE.EE.DataRequired('CO_NBR = ' + RDSet.FieldByName('Co_Nbr').AsString);
          DM_PAYROLL.PR.DataRequired('CO_NBR = ' + RDSet.FieldByName('Co_Nbr').AsString);
          DM_PAYROLL.PR.Locate('PR_NBR', RDSet['Pr_Nbr'], []);
          ctx_PayrollCalculation.OpenDetailPayroll(RDSet['Pr_Nbr']);

          PrCheckNbr := DM_PAYROLL.PR_CHECK.FieldbyName('PR_CHECK_NBR').AsInteger;
          DM_PAYROLL.PR_CHECK.Filter := 'EE_NBR = ' + RDSet.FieldByName('EE_NBR').AsString;
          DM_PAYROLL.PR_CHECK.Filtered := True;
          DM_PAYROLL.PR_CHECK_LINES.Filtered := True;
          try
            DM_PAYROLL.PR_CHECK.First;
            while not DM_PAYROLL.PR_CHECK.Eof do
            begin
              if (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_VOID) and
                 ((Sender = btnRefresh) or (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_MANUAL)) then
              begin
                DM_PAYROLL.PR_CHECK_LINES.Filter := 'PR_CHECK_NBR = ' + IntToStr(DM_PAYROLL.PR_CHECK.FieldbyName('PR_CHECK_NBR').AsInteger);
                with DM_PAYROLL do
                begin
                  PR_CHECK_LINES.First;
                  while not PR_CHECK_LINES.EOF do
                  begin
                    if (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED) and
                       (not Assigned(ChangeList) or ChangeList.Locate('ClNbr;EeNbr;CUSTOM_E_D_CODE_NUMBER', VarArrayOf([RDSet['Cl_Nbr'], PR_CHECK['EE_NBR'], PR_CHECK_LINES['E_D_Code_Lookup']]), [])) then
                    begin
                      PR_CHECK_LINES.Delete;
                      i := PR_CHECK_LINES.RecNo;
                      PR_CHECK_LINES.ReSync([]);
                      PR_CHECK_LINES.RecNo := i;
                    end
                    else
                      PR_CHECK_LINES.Next;
                  end;

                  DM_EMPLOYEE.EE_SCHEDULED_E_DS.Filter := '';
                  DM_EMPLOYEE.EE_SCHEDULED_E_DS.Filtered := False;
                  DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired();
                  DM_EMPLOYEE.EE_SCHEDULED_E_DS.OnFilterRecord := RDSetFilterRecord;
                  DM_EMPLOYEE.EE_SCHEDULED_E_DS.Filtered := True;
                  MY_EE_SCHEDULED_E_DS := TevClientDataset.Create(nil);
                  MY_EE_SCHEDULED_E_DS.Data := DM_EMPLOYEE.EE_SCHEDULED_E_DS.GetDataStream(False, False);
                  try
                    bPaySalary := PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y';
                    if bPaySalary then
                    begin
                      if not DM_CLIENT.CL_E_DS.Active then
                        DM_CLIENT.CL_E_DS.DataRequired();
                      if DM_CLIENT.CL_E_DS.Locate('E_D_CODE_TYPE', ED_OEARN_SALARY, []) then
                        bPaySalary := not Assigned(ChangeList) or ChangeList.Locate('ClNbr;EeNbr;CUSTOM_E_D_CODE_NUMBER', VarArrayOf([RDSet['Cl_Nbr'], PR_CHECK['EE_NBR'], DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER']]), []);
                    end;

                    ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
                      PR,
                      PR_BATCH,
                      PR_CHECK,
                      PR_CHECK_LINES,
                      PR_CHECK_STATES,
                      PR_CHECK_SUI,
                      PR_CHECK_LOCALS,
                      PR_CHECK_LINE_LOCALS,
                      PR_SCHEDULED_E_DS,
                      DM_CLIENT.CL_E_DS,
                      DM_CLIENT.CL_PERSON,
                      DM_CLIENT.CL_PENSION,
                      DM_COMPANY.CO,
                      DM_COMPANY.CO_E_D_CODES,
                      DM_COMPANY.CO_STATES,
                      DM_EMPLOYEE.EE,
                      MY_EE_SCHEDULED_E_DS,
                      DM_EMPLOYEE.EE_STATES,
                      DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
                      DM_SYSTEM_STATE.SY_STATES,
                      False,
                      bPaySalary,
                      (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
                      True
                    );
                  finally
                    MY_EE_SCHEDULED_E_DS.Free;
                    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Filtered := False;
                    DM_EMPLOYEE.EE_SCHEDULED_E_DS.OnFilterRecord := nil;
                  end;

                end;
              end;
              DM_PAYROLL.PR_CHECK.Next;
            end;
          finally
            DM_PAYROLL.PR_CHECK.Filtered := False;
            DM_PAYROLL.PR_CHECK.Filter := '';
            DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', PrCheckNbr, []);
            DM_PAYROLL.PR_CHECK_LINES.Filtered := False;
          end;
        end;
        RDSet.Next;
        ctx_DataAccess.PostDataSets([
                DM_PAYROLL.PR_CHECK,
                DM_PAYROLL.PR_CHECK_LINES,
                DM_PAYROLL.PR_CHECK_STATES,
                DM_PAYROLL.PR_CHECK_SUI,
                DM_PAYROLL.PR_CHECK_LOCALS,
                DM_PAYROLL.PR_CHECK_LINE_LOCALS]);
      end;
    finally
      RDSet.EnableControls;
      ctx_EndWait;
    end;
  except
    ModalResult := mrNone;
    raise;
  end;
end;

procedure TEDChangeForm.RDSetFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  if Assigned(ChangeList) then
  begin
    ChangeList.First;
    Accept := ChangeList.Locate('ClNbr;EeNbr;CUSTOM_E_D_CODE_NUMBER', VarArrayOf([TevClientDataSet(DataSet).ClientID,
      DataSet['EE_NBR'], DataSet['CUSTOM_E_D_CODE_NUMBER']]), []);
  end
  else
    Accept := True;
end;

end.
