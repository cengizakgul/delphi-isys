inherited EDIT_EE_WORK_SHIFTS: TEDIT_EE_WORK_SHIFTS
  inherited PageControl1: TevPageControl
    HelpContext = 39503
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 143
                IniAttributes.SectionName = 'TEDIT_EE_WORK_SHIFTS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 143
                IniAttributes.SectionName = 'TEDIT_EE_WORK_SHIFTS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtWork_Shifts: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbShifts: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 149
        Align = alClient
        TabOrder = 0
        object fpShiftSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 325
          Height = 251
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablAuto_Shift: TevLabel
            Left = 12
            Top = 35
            Width = 46
            Height = 13
            Caption = 'Auto-Shift'
            FocusControl = wwlcAuto_Shift
          end
          object wwdgWork_Shifts: TevDBGrid
            Left = 12
            Top = 84
            Width = 289
            Height = 145
            HelpContext = 39503
            DisableThemesInTitle = False
            Selected.Strings = (
              'NameLKUP'#9'16'#9'Shift Name'#9'F'
              'SHIFT_RATE'#9'11'#9'Shift Rate'#9'F'
              'SHIFT_PERCENTAGE'#9'13'#9'Shift %'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_WORK_SHIFTS\wwdgWork_Shifts'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object wwlcAuto_Shift: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 185
            Height = 21
            HelpContext = 39503
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NameLKUP'#9'40'#9'NameLKUP'#9'F')
            DataField = 'AUTOPAY_CO_SHIFTS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_EE_WORK_SHIFTS.EE_WORK_SHIFTS
            LookupField = 'CO_SHIFTS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            LookupTableEditorEnabled = False
          end
        end
        object fpShiftDetail: TisUIFashionPanel
          Left = 8
          Top = 267
          Width = 325
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Shift Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablShift: TevLabel
            Left = 12
            Top = 35
            Width = 36
            Height = 13
            Caption = '~Shift'
            FocusControl = wwlcShift
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablRate: TevLabel
            Left = 125
            Top = 35
            Width = 23
            Height = 13
            Caption = 'Rate'
            FocusControl = dedtRate
          end
          object lablShift_Percentage: TevLabel
            Left = 218
            Top = 35
            Width = 8
            Height = 13
            Caption = '%'
            FocusControl = dedtShift_Percentage
          end
          object wwlcShift: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 109
            Height = 21
            HelpContext = 39503
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'38'#9'NAME')
            DataField = 'CO_SHIFTS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_SHIFTS.CO_SHIFTS
            LookupField = 'CO_SHIFTS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object dedtRate: TevDBEdit
            Left = 125
            Top = 50
            Width = 85
            Height = 21
            HelpContext = 39503
            DataField = 'SHIFT_RATE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '[-]*12[#][.*6[#]]'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object dedtShift_Percentage: TevDBEdit
            Left = 218
            Top = 50
            Width = 85
            Height = 21
            HelpContext = 39503
            DataField = 'SHIFT_PERCENTAGE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '[-]*12[#][.*6[#]]'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_WORK_SHIFTS.EE_WORK_SHIFTS
    OnDataChange = wwdsDetailDataChange
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 366
    Top = 13
  end
  object EEStatesDS: TevClientDataSet
    Left = 475
    Top = 91
  end
end
