// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_AUTOLABOR_DISTRIBUTION;

interface

uses
   EvUtils, SDataStructure, wwdbedit, SPD_EDIT_EE_BASE, wwdblook,
  StdCtrls, Mask, Buttons, ExtCtrls, DBActns, Classes, ActnList, Db,
  Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, DBCtrls, Controls,
  EvSecElement, Wwdotdot, Wwdbcomb, SPackageEntry, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, SDDClasses, isVCLBugFix, 
  ISDataAccessComponents, EvDataAccessComponents, EvConsts,
  SDataDictclient, SDataDicttemp, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIwwDBEdit, ImgList, isUIwwDBLookupCombo, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_AUTOLABOR_DISTRIBUTION = class(TEDIT_EE_BASE)
    tshtAuto_Labor_Distribution: TTabSheet;
    pdsEmployee: TevProxyDataSet;
    pnlInformation: TisUIFashionPanel;
    pnlListOfDistrubutions: TisUIFashionPanel;
    lablE_D_Group: TevLabel;
    lblTotal: TevLabel;
    wwdgAuto_Labor_Distribution: TevDBGrid;
    wwlcE_D_Group: TevDBLookupCombo;
    evBitBtn1: TevBitBtn;
    edTotalAllocation: TevEdit;
    lablJob: TevLabel;
    lablWorkers_Comp_Code: TevLabel;
    lablPercentage: TevLabel;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    SpeedButton1: TevSpeedButton;
    dedtPercentage: TevDBEdit;
    wwlcDivision: TevDBLookupCombo;
    wwlcBranch: TevDBLookupCombo;
    wwlcDepartment: TevDBLookupCombo;
    wwlcTeam: TevDBLookupCombo;
    wwlcJob: TevDBLookupCombo;
    wwlcWorkers_Comp_Code: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwDBLookupCombo2: TevDBLookupCombo;
    wwDBLookupCombo3: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    sbLaborDist: TScrollBox; //SEG
    procedure SpeedButton1Click(Sender: TObject);
    procedure EENextExecute(Sender: TObject);
    procedure EEPriorExecute(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure evBitBtn1Click(Sender: TObject);
    procedure wwdsEmployeeDataChange(Sender: TObject; Field: TField); //SEG
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  private
    function GetPercTotal: double; //SEG
  public
    procedure Scroll;
    function GetInsertControl: TWinControl; override;
    function TryFixPerc: boolean; //SEG
    property PercTotal: double read GetPercTotal;
    function CanClose: Boolean; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

uses
  SPD_DBDTSelectionListFiltr, Dialogs, SysUtils,
  SPD_EDIT_Open_BASE, SFrameEntry;

const
  eps: double = 1e-3; //gdy

  {$R *.DFM}

function TEDIT_EE_AUTOLABOR_DISTRIBUTION.GetDataSetConditions(
  sName: string): string;
begin
  {if (sName = 'CO_BRANCH') or
     (sName = 'CO_DEPARTMENT') or
     (sName = 'CO_TEAM') then
    Result := ''
  else}
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  inherited;
end;

function TEDIT_EE_AUTOLABOR_DISTRIBUTION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION;
end;

function TEDIT_EE_AUTOLABOR_DISTRIBUTION.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo1;
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.SpeedButton1Click(
  Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  inherited;
  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DM_PAYROLL, DBDTSelectionList do
    begin
      DBDTSelectionList.Setup(CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value
        , wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value, wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value
        , wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
          begin
            if wwdsDetail.DataSet.RecordCount = 0 then
              wwdsDetail.DataSet.Insert
            else
              wwdsDetail.DataSet.Edit;
          end;
          wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

function TEDIT_EE_AUTOLABOR_DISTRIBUTION.GetPercTotal;
var
  nbr: Integer;
begin
  Result := 0.0;
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
    with (wwdsDetail.DataSet as TevClientDataset) do
    begin
      nbr := DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.RecNo;
      DisableControls;
      try
        First;
        while not EOF do
        begin
          Result := Result + RoundAll(FieldByName('PERCENTAGE').AsFloat, 6);
          Next;
        end;
      finally
        DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION.RecNo := nbr;
        EnableControls;
      end;
    end;
end;

function TEDIT_EE_AUTOLABOR_DISTRIBUTION.TryFixPerc: boolean; //SEG
begin
  {if} //EvMessage('Percentage should equal 0% or 100%!' + #13 +
  //'Add ' + FloatToStr(RoundAll(100.0 - PercTotal, 6)) + '%', mtWarning, [mbOk{, mbCancel}]);{ = mrOk then}
  //  result := true
//  else
//    result := false;
  EvMessage('Percentage should equal 100% or be larger than 0%!' + #13 +
  'Add ' + FloatToStr(RoundAll(100.0 - PercTotal, 6)) + '%', mtWarning, [mbOk{, mbCancel}]);{ = mrOk then}
   result := true
end;

function TEDIT_EE_AUTOLABOR_DISTRIBUTION.CanClose: Boolean;
begin
  Result := inherited CanClose;
  if Result then
  begin
    with DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION do
      if State in [dsEdit, dsInsert] then
        Post;

    if (abs(PercTotal) > eps) and (abs(PercTotal - 100.0) > eps) then //gdy
    begin
      if TryFixPerc then
      begin
        PageControl1.ActivePage:=tshtAuto_Labor_Distribution;
        if (wwdgAuto_Labor_Distribution.Visible and wwdgAuto_Labor_Distribution.Enabled) then
          wwdgAuto_Labor_Distribution.SetFocus;
        Result := False;
      end
    end;
  end;
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE, EditDataSets);
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
  OverrideRate: Boolean;
begin
  inherited;
  if Kind = NavOK then
  begin
    if (dedtPercentage.Text<>'') and (StrTofloat(dedtPercentage.Text)=0) then
    begin
      Handled := True;
      ShowMessage('Field "Percentage"  must be great than 0.00%.');
      Exit;
    end;
    if DM_EMPLOYEE.EE.FieldByName('ALD_CL_E_D_GROUPS_NBR').AsString = '' then
    begin
      Handled := True;
      ShowMessage('You must have an E/D Group.');
      Exit;
    end
    else
      Handled := False;

      OverrideRate := ((DM_COMPANY.CO_DIVISION.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and (not DM_COMPANY.CO_DIVISION.FieldByName('OVERRIDE_PAY_RATE').IsNull)) or
                      ((DM_COMPANY.CO_BRANCH.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and (not DM_COMPANY.CO_BRANCH.FieldByName('OVERRIDE_PAY_RATE').IsNull)) or
                      ((DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and (not DM_COMPANY.CO_DEPARTMENT.FieldByName('OVERRIDE_PAY_RATE').IsNull)) or
                      ((DM_COMPANY.CO_TEAM.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and (not DM_COMPANY.CO_TEAM.FieldByName('OVERRIDE_PAY_RATE').IsNull));
      if OverrideRate then
        EvMessage('Auto Labor Distribution will NOT use the override rate set up at this DBDT!!!', mtWarning, [mbOK]);

  end;

end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavOK] then
   edTotalAllocation.Text := FloatToStr(RoundAll(PercTotal, 6));
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.wwdsEmployeeDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and (wwdsDetail.DataSet.State =dsbrowse) then
     edTotalAllocation.Text := FloatToStr(RoundAll(PercTotal, 6));
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.EENextExecute(Sender: TObject);
begin
  Scroll;
  inherited;
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.Scroll;
begin
  with DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION do
    if State in [dsEdit, dsInsert] then
      Post;

  if Visible and (abs(PercTotal) > eps) and (abs(PercTotal - 100.0) > eps) then //gdy
  begin
    if TryFixPerc then
    begin
      PageControl1.ActivePage:=tshtAuto_Labor_Distribution;
      if (wwdgAuto_Labor_Distribution.Visible and wwdgAuto_Labor_Distribution.Enabled) then
        wwdgAuto_Labor_Distribution.SetFocus;
      AbortEx;
    end
  end
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.EEPriorExecute(Sender: TObject);
begin
  Scroll;
  inherited;
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  if PageControl1.ActivePage = tshtAuto_Labor_Distribution then
  try
    Scroll;
  except
    AllowChange := False;
  end;
  inherited;
end;

procedure TEDIT_EE_AUTOLABOR_DISTRIBUTION.evBitBtn1Click(Sender: TObject);
begin
  inherited;

  if EvMessage('Remove all Auto Labor Distribution setup from this employee?', mtConfirmation, [mbOK, mbCancel]) = mrOk then
  begin
    wwdsDetail.DataSet.DisableControls;
    try
      wwdsDetail.DataSet.First;
      while not wwdsDetail.DataSet.Eof do
      begin
        wwdsDetail.DataSet.Delete;
      end;
      wwdsEmployee.DataSet.Edit;
      wwdsEmployee.DataSet.FieldByName('ALD_CL_E_D_GROUPS_NBR').Clear;
      wwdsEmployee.DataSet.Post;
      ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION, DM_EMPLOYEE.EE]);
    finally
      wwdsDetail.DataSet.EnableControls;
    end;
  end;

end;



initialization
  RegisterClass(TEDIT_EE_AUTOLABOR_DISTRIBUTION);

end.
