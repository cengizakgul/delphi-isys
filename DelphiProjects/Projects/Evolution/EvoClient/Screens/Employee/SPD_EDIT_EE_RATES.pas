// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_RATES;

interface

uses
  SDataStructure,  SPD_EDIT_EE_BASE, wwdbdatetimepicker,
  StdCtrls, ExtCtrls, DBCtrls, wwdbedit, Wwdotdot, Wwdbcomb, wwdblook,
  Mask, Buttons, DBActns, Classes, ActnList, Db, Wwdatsrc, Grids, Wwdbigrd, Variants,
  Wwdbgrid, ComCtrls, Controls, EvConsts,  Graphics,
  Menus, SPD_EDIT_COPY,  EvContext,
  SPD_AskRatesUpdateParams, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  SDDClasses, ISDataAccessComponents, EvLegacy, isVCLBugFix,
  EvDataAccessComponents, SDataDictclient, SDataDicttemp, evTypes, EvStreamUtils,
  EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvDataset;

type
  TEDIT_EE_RATES = class(TEDIT_EE_BASE)
    tshtRates: TTabSheet;
    evProxyDataSet1: TevProxyDataSet;
    evActionList1: TevActionList;
    UpdateRates: TAction;
    evPopupMenu1: TevPopupMenu;
    FocusMain1: TMenuItem;
    evBitBtn1: TevBitBtn;
    aClearDBDT: TAction;
    pmPosition: TPopupMenu;
    mnPositionCopy: TMenuItem;
    sbRates: TScrollBox;
    pnlPayRateInfo: TisUIFashionPanel;
    lablSalary_Amount: TevLabel;
    lablCalculated_Salary: TevLabel;
    lablStandard_Hours: TevLabel;
    lablPay_Frequency: TevLabel;                    
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    dedtSalary_Amount: TevDBEdit;
    dedtCalculated_Salary: TevDBEdit;
    dedtStandard_Hours: TevDBEdit;
    wwcbPay_Frequency: TevDBComboBox;
    edtAvHours: TevDBEdit;
    eCASalary: TevDBEdit;
    fpOverride: TisUIFashionPanel;
    lablHome_Division: TevLabel;
    lablHome_Branch: TevLabel;
    lablHome_Department: TevLabel;
    lablHome_Team: TevLabel;
    lablJobs_Number: TevLabel;
    SpeedButton1: TevSpeedButton;
    cbDivisionName: TevDBLookupCombo;
    cbBranchName: TevDBLookupCombo;
    cbTeamName: TevDBLookupCombo;
    cbDepartmentName: TevDBLookupCombo;
    wwlcJobs_Number: TevDBLookupCombo;
    evGroupBox1: TevGroupBox;
    lablHR_Salary_Grades_Number: TevLabel;
    evLabel4: TevLabel;
    evcbPosition: TevDBLookupCombo;
    wwlcHR_Salary_Grades_Number: TevDBLookupCombo;
    fpFutureUpdates: TisUIFashionPanel;
    lablFuture_Raise_Date: TevLabel;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    lablFuture_Raise_Amount: TevLabel;
    dedtFuture_Raise_Amount: TevDBEdit;
    lablFuture_Raise_Percentage: TevLabel;
    dedtFuture_Raise_Percentage: TevDBEdit;
    lablFuture_Pay_Frequency: TevLabel;
    wwcbFuture_Pay_Frequency: TevDBComboBox;
    lablFuture_Raise_Rate: TevLabel;
    DBEdit1: TevDBEdit;
    evDBRadioGroup1: TevDBRadioGroup;
    evGroupBox2: TevGroupBox;
    Label6: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    Label1: TevLabel;
    wwDBLookupCombo2: TevDBLookupCombo;
    Label27: TevLabel;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBGrid1: TevDBGrid;
    cbDivisionNbr: TevDBLookupCombo;
    cbBranchNbr: TevDBLookupCombo;
    cbDepartmentNbr: TevDBLookupCombo;
    cbTeamNbr: TevDBLookupCombo;
    lablRate_Number: TevLabel;
    lablRate_Amount: TevLabel;
    drgpPrimary_Rate: TevDBRadioGroup;
    dedtRate_Number: TevDBEdit;
    dedtRate_Amount: TevDBEdit;
    lablWorkers_Comp_Wage_Limit: TevLabel;
    evLabel5: TevLabel;
    dedtWorkers_Comp_Wage_Limit: TevDBEdit;
    evDBComboBox1: TevDBComboBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure evProxyDataSet1BeforePost(DataSet: TDataSet);
    procedure evProxyDataSet1BeforeEdit(DataSet: TDataSet);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure edtAvHoursExit(Sender: TObject);
    procedure dedtRate_AmountExit(Sender: TObject);
    procedure UpdateRatesUpdate(Sender: TObject);
    procedure UpdateRatesExecute(Sender: TObject);
    procedure aClearDBDTExecute(Sender: TObject);
    procedure evProxyDataSet1AfterPost(DataSet: TDataSet);
    procedure EENextExecute(Sender: TObject);
    procedure EEPriorExecute(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure evcbPositionCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evcbPositionChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure SetCO_HR_POSITION_GRADESFilter;
    procedure mnPositionCopyClick(Sender: TObject);
    procedure cbDivisionNbrSetLookupText(Sender: TObject;
      var AText: String);
    procedure wwdsDetailStateChange(Sender: TObject);
  private
    FParams: TRatesUpdateParamsRec;
    FPositionGrades : Boolean;
    WasPrimary: Boolean;
    OldPrimaryRate, NewPrimaryRate: Variant;
    FOldEeRatesBeforePost: TDataSetNotifyEvent;
    procedure UpdateAverageHours;
    procedure CheckHRSalary;
    procedure EeRatesBeforePost(DataSet: TDataSet);
    function CanUpdateRates: boolean;
    procedure updateRate;
    procedure updateSalary;
    function CalcNewAmt( amt: Variant ): Variant;
    function HasPrimaryRate(DontShowMessage: boolean = False): boolean;
  protected
    procedure SetReadOnly(Value: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure DeActivate; override;
    function GetInsertControl: TWinControl; override;
    function CanClose: Boolean; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

uses
  SPD_DBDTSelectionListFiltr, SysUtils, Dialogs, evutils, sPackageEntry,
  EvCommonInterfaces;

{$R *.DFM}

function TEDIT_EE_RATES.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_HR_POSITION_GRADES') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;


procedure TEDIT_EE_RATES.ButtonClicked(Kind: Integer; var Handled: Boolean);

begin
  inherited;
  if (Kind in [NavOK,NavCommit]) then
  begin
    if (trim(evcbPosition.Text) <> '') then
      if (Trim(wwlcHR_Salary_Grades_Number.Text) = '') then
        raise EUpdateError.CreateHelp('Please select a Pay Grade!', IDH_ConsistencyViolation);

    if drgpPrimary_Rate.ItemIndex = 0 then
    begin
      if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
         DM_CLIENT.EE_RATES.UpdateRecord;
      if CheckPrimaryRateFlag(true,sysdate) then
        raise EUpdateError.CreateHelp('Employee has a primary rate set up!', IDH_ConsistencyViolation);
    end;
  end;
end;

procedure TEDIT_EE_RATES.SpeedButton1Click(Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DBDTSelectionList do
    begin
      DBDTSelectionList.Setup(CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value
        , wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value, wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value
        , wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
          begin
            if wwdsDetail.DataSet.RecordCount = 0 then
              wwdsDetail.DataSet.Insert
            else
              wwdsDetail.DataSet.Edit;
          end;
          wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;


procedure TEDIT_EE_RATES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_HR_SALARY_GRADES, aDS);
  DM_COMPANY.CO_HR_POSITION_GRADES.Close;  
  AddDS(DM_COMPANY.CO_HR_POSITION_GRADES, aDS);
  AddDS(DM_COMPANY.CO_HR_POSITIONS, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  inherited;
  WasPrimary := False;
end;

function TEDIT_EE_RATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_RATES;
end;

function TEDIT_EE_RATES.GetInsertControl: TWinControl;
begin
  Result := drgpPrimary_Rate;
end;

procedure TEDIT_EE_RATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE, EditDataSets);
end;

procedure TEDIT_EE_RATES.evProxyDataSet1BeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DataSet.FindField('PrRateMax').AsString = 'Y') and (drgpPrimary_Rate.Value = 'Y') and (not WasPrimary) then
    if EvMessage('Primary Rate already exists!', mtWarning, [mbOk]) = mrOk then
    begin
      DataSet.FindField('PRIMARY_RATE').AsString := GROUP_BOX_NO;
      AbortEx;
    end;
  WasPrimary := False;
end;

procedure TEDIT_EE_RATES.evProxyDataSet1BeforeEdit(DataSet: TDataSet);
begin
  inherited;
  WasPrimary := (DataSet.FieldByName('PRIMARY_RATE').AsString = 'Y');
  if (DM_EMPLOYEE.EE['AUTO_UPDATE_RATES'] = GROUP_BOX_YES) and
    (DM_EMPLOYEE.EE_RATES['PRIMARY_RATE'] = GROUP_BOX_YES) then
      OldPrimaryRate := DM_EMPLOYEE.EE_RATES['RATE_AMOUNT']; //Saves old primary rate amount
end;

procedure TEDIT_EE_RATES.wwdsDetailDataChange(Sender: TObject; Field: TField);

  procedure HideDBDTInfo(const CB: TevDBLookupCombo);
  begin
    CB.Tag := 1;
    CB.DisplayValue := '';
  end;

  procedure UnHideDBDTInfo(const CB: TevDBLookupCombo);
  begin
    CB.Tag := 0;
  end;

begin
  inherited;
  if not DM_COMPANY.CO_DIVISION.Active or
     not DM_COMPANY.CO_BRANCH.Active or
     not DM_COMPANY.CO_DEPARTMENT.Active or
     not DM_COMPANY.CO_TEAM.Active then
    Exit;

  if DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', wwdsDetail.DataSet['CO_DIVISION_NBR'], 'PRINT_DIV_ADDRESS_ON_CHECKS') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbDivisionNbr);
    HideDBDTInfo(cbDivisionName);
  end
  else
  begin
    UnHideDBDTInfo(cbDivisionNbr);
    UnHideDBDTInfo(cbDivisionName);
  end;

  if DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', wwdsDetail.DataSet['CO_BRANCH_NBR'], 'PRINT_BRANCH_ADDRESS_ON_CHECK') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbBranchNbr);
    HideDBDTInfo(cbBranchName);
  end
  else
  begin
    UnHideDBDTInfo(cbBranchNbr);
    UnHideDBDTInfo(cbBranchName);
  end;

  if DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', wwdsDetail.DataSet['CO_DEPARTMENT_NBR'], 'PRINT_DEPT_ADDRESS_ON_CHECKS') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbDepartmentNbr);
    HideDBDTInfo(cbDepartmentName);
  end
  else
  begin
    UnHideDBDTInfo(cbDepartmentNbr);
    UnHideDBDTInfo(cbDepartmentName);
  end;

  if DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', wwdsDetail.DataSet['CO_TEAM_NBR'], 'PRINT_GROUP_ADDRESS_ON_CHECK') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbTeamNbr);
    HideDBDTInfo(cbTeamName);
  end
  else
  begin
    UnHideDBDTInfo(cbTeamNbr);
    UnHideDBDTInfo(cbTeamName);
  end;

  UpdateAverageHours;

  if wwdsDetail.DataSet.State = dsBrowse then
     SetCO_HR_POSITION_GRADESFilter;
end;

procedure TEDIT_EE_RATES.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  UpdateAverageHours;

end;

procedure TEDIT_EE_RATES.UpdateAverageHours;
var
  v: Double;
begin
  if (TevClientDataSet(DM_EMPLOYEE.EE_RATES).State = dsBrowse) and (TevClientDataSet(DM_EMPLOYEE.EE).State = dsBrowse) then
  begin
    v := ConvertNull(DM_EMPLOYEE.EE_RATES.Lookup( 'EE_NBR;PRIMARY_RATE',VarArrayOf([DM_EMPLOYEE.EE['EE_NBR'],GROUP_BOX_YES]),'RATE_AMOUNT'), 0);
    if v = 0 then
      edtAvHours.Text := '0.00'
    else
      edtAvHours.Text := Format( '%.2f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat / v] );

    if DM_EMPLOYEE.EE.SALARY_AMOUNT.IsNull then
      eCASalary.Text := FormatFloat('#,##0.00', v * GetTaxFreq(DM_EMPLOYEE.EE.PAY_FREQUENCY.Value) * DM_EMPLOYEE.EE.STANDARD_HOURS.Value)
    else
      eCASalary.Text := FormatFloat('#,##0.00', DM_EMPLOYEE.EE.SALARY_AMOUNT.Value * GetTaxFreq(DM_EMPLOYEE.EE.PAY_FREQUENCY.Value));
  end;
//  else
//    edtAvHours.Text := '0.00';
end;

procedure TEDIT_EE_RATES.edtAvHoursExit(Sender: TObject);
var
  new_rate: Real;
begin
  inherited;
  if (DM_EMPLOYEE.EE_RATES.FieldByName('PRIMARY_RATE').AsString = GROUP_BOX_YES) and
     (dedtSalary_Amount.Text <> '') and (edtAvHours.Text <> '') {and (StrToFloat(edtAvHours.Text) <> 0 ) and
     (StrToFloat(dedtRate_Amount.Text) <> StrToFloat(Format('%.4f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat/StrToFloat(edtAvHours.Text)])))} then
    begin
      if StrToFloat(edtAvHours.Text) <> 0 then
        new_rate := StrToFloat(Format('%.4f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat/StrToFloat(edtAvHours.Text)]))
      else
        new_rate := 0;
      if StrToFloat(Format('%.4f', [DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat])) = new_rate then exit;
      if EvMessage('Rate will change to $'+Format('%.4f', [new_rate])+'.', mtConfirmation, [mbYes, mbNo]) = mrYes then
      begin
        if DM_EMPLOYEE.EE_RATES.State = dsBrowse then
          DM_EMPLOYEE.EE_RATES.Edit;
        DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat := new_rate;
      end
      else
        UpdateAverageHours;
    end;
end;

procedure TEDIT_EE_RATES.dedtRate_AmountExit(Sender: TObject);
begin
  inherited;
  if (DM_EMPLOYEE.EE_RATES.State <> dsBrowse) and
     (DM_EMPLOYEE.EE_RATES.FieldByName('PRIMARY_RATE').AsString = GROUP_BOX_YES) then
  begin
    if DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat <> 0 then
      edtAvHours.Text := Format( '%.2f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat / DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat] )
    else
      edtAvHours.Text := '0.00';
  end;
end;

procedure TEDIT_EE_RATES.Activate;
begin
  inherited;
  FPositionGrades := True;
  FOldEeRatesBeforePost := DM_EMPLOYEE.EE_RATES.BeforePost;
  DM_EMPLOYEE.EE_RATES.BeforePost := EeRatesBeforePost;
  wwlcHR_Salary_Grades_Number.Enabled := Context.License.HR;

end;

procedure TEDIT_EE_RATES.CheckHRSalary;
var
  s: Extended;
  n: Integer;
begin

  if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_HR_POSITION_GRADES_NBR').IsNull and not DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').IsNull then
  begin
    case DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').AsString[1] of
      FREQUENCY_TYPE_DAILY:         n := 365;
      FREQUENCY_TYPE_WEEKLY:        n := 52;
      FREQUENCY_TYPE_BIWEEKLY:      n := 26;
      FREQUENCY_TYPE_SEMI_MONTHLY:  n := 24;
      FREQUENCY_TYPE_MONTHLY:       n := 12;
      FREQUENCY_TYPE_QUARTERLY:     n := 4;
    else
      n := 0;
    end;

    s := DM_EMPLOYEE.EE.FieldByName('STANDARD_HOURS').AsFloat * DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat * n;


    if (s < DM_COMPANY.CO_HR_POSITION_GRADES.FieldByName('MINIMUM_PAY').Value) or
       (s > DM_COMPANY.CO_HR_POSITION_GRADES.FieldByName('MAXIMUM_PAY').Value) then
      EvMessage('Annual salary is out of the HR salary grade!', mtWarning, [mbOk]);
  end;
end;

procedure TEDIT_EE_RATES.EeRatesBeforePost(DataSet: TDataSet);
begin
  if Assigned(FOldEeRatesBeforePost) then
    FOldEeRatesBeforePost(DataSet);
  CheckHRSalary;
end;

procedure TEDIT_EE_RATES.DeActivate;
begin
  FPositionGrades := false;
  DM_EMPLOYEE.EE_RATES.BeforePost := FOldEeRatesBeforePost;

  DM_COMPANY.CO_HR_POSITION_GRADES.Filter:= '';
  DM_COMPANY.CO_HR_POSITION_GRADES.Filtered:= false;

  inherited;
end;

function TEDIT_EE_RATES.CalcNewAmt( amt: Variant ): Variant;
begin
  case FParams.UpdateMode of
    rumOnAmount: Result := amt + FParams.Value;
    rumOnPercentage: if not VarIsNull(amt) then
                     begin
                       Result := amt + amt * FParams.Value / 100;
                       if FParams.RoundTo2Decimal then
                           Result :=  RoundAll(Result, 2);
                     end
                     else
                       Result := Null;

    rumToAmount: Result := FParams.Value;
  else
    Assert(false);
    Result := Null;
  end;
end;

procedure SetDSValues( ds: TevClientDataSet; fieldnames: string; values: Variant );
begin
  ds.Edit;
  try
    ds[fieldnames] := values;
    ds.Post;
  except
    ds.Cancel;
    raise;
  end;
end;

procedure TEDIT_EE_RATES.updateSalary;
var
  amt: Variant;
  newamt: Variant;
begin
  Assert( FParams.Target = utSalaries );
  amt := DM_EMPLOYEE.EE['SALARY_AMOUNT'];
  if ( VarIsNull(FParams.RangeLow) or (FParams.RangeLow <= amt) ) and
     ( VarIsNull(FParams.RangeHigh) or (amt <= FParams.RangeHigh) ) then
  begin
    newamt := CalcNewAmt( amt );
    if newamt <> amt then
      SetDSValues( DM_EMPLOYEE.EE, 'SALARY_AMOUNT', newamt );
  end;
end;

procedure TEDIT_EE_RATES.updateRate;
var
  amt, salamt: Variant;
  newamt: Variant;
begin
  Assert( FParams.Target = utRates );

  amt := DM_EMPLOYEE.EE_RATES['RATE_AMOUNT'];
  salamt := DM_EMPLOYEE.EE.Lookup('EE_NBR', DM_EMPLOYEE.EE_RATES['EE_NBR'], 'SALARY_AMOUNT');
  if ( VarIsNull(FParams.RangeLow) or (FParams.RangeLow <= amt) ) and
     ( VarIsNull(FParams.RangeHigh) or (amt <= FParams.RangeHigh) ) and
     not ( FParams.ForHourlyOnly and not VarIsNull(salamt) and (salamt <> 0) ) then
  begin
    newamt := CalcNewAmt(amt);
    if newamt <> amt then
      SetDSValues( DM_EMPLOYEE.EE_RATES, 'RATE_AMOUNT', newamt );
  end;
end;


procedure TEDIT_EE_RATES.UpdateRatesUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := CanUpdateRates;
end;

function TEDIT_EE_RATES.CanUpdateRates: boolean;
begin
  Result := DSIsActive(DM_EMPLOYEE.EE)
        and DSIsActive(DM_EMPLOYEE.EE_RATES)
        and (TevClientDataSet(DM_EMPLOYEE.EE).State = dsBrowse)
        and (TevClientDataSet(DM_EMPLOYEE.EE_RATES).State = dsBrowse)
        and (DM_EMPLOYEE.EE.RecordCount > 0);
end;

procedure TEDIT_EE_RATES.UpdateRatesExecute(Sender: TObject);
var
  ParamsForm: TAskRatesUpdateParams;
  CopyForm: TEDIT_COPY;
begin
  ParamsForm := TAskRatesUpdateParams.Create( Self );
  try
    if ParamsForm.ShowModal = mrOk then
    begin
      FParams := ParamsForm.Params;
      CopyForm := TEDIT_COPY.Create(Self);
      try
        CopyForm.CopyFrom := 'EE';
        CopyForm.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;

        if FParams.Target = utRates then
        begin
          CopyForm.CopyTo := 'EERates';
          CopyForm.CustomEDFunction := updateRate;
          DM_EMPLOYEE.EE_RATES.Cancel;
          DM_EMPLOYEE.EE_RATES.DisableControls;
          try
            CopyForm.ShowModal;
          finally
            DM_EMPLOYEE.EE_RATES.EnableControls;
          end;
        end
        else if FParams.Target = utSalaries then
        begin
          CopyForm.CopyTo := 'EEs';
          CopyForm.CustomEDFunction := updateSalary;
          DM_EMPLOYEE.EE.Cancel;
          DM_EMPLOYEE.EE.DisableControls;
          try
            CopyForm.ShowModal;
          finally
            DM_EMPLOYEE.EE.EnableControls;
          end;
        end
        else
          Assert(false);
      finally
        FreeAndNil( CopyForm );
      end;
    end;
  finally
    FreeAndNil( ParamsForm );
  end;
end;

procedure TEDIT_EE_RATES.aClearDBDTExecute(Sender: TObject);
begin
  inherited;
  if DM_EMPLOYEE.EE_RATES.State = dsBrowse then
    DM_EMPLOYEE.EE_RATES.Edit;
  wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Clear;
  wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Clear;
  wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Clear;
  wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Clear;
end;

procedure TEDIT_EE_RATES.SetReadOnly(Value: Boolean);
begin
  inherited;
  eCASalary.SecurityRO := True;
end;

procedure TEDIT_EE_RATES.evProxyDataSet1AfterPost(DataSet: TDataSet);
var
  bmk: TBookmark;
begin
  if DM_EMPLOYEE.EE['AUTO_UPDATE_RATES'] = GROUP_BOX_YES then
    with DM_EMPLOYEE do
      if EE_RATES['PRIMARY_RATE'] = GROUP_BOX_YES then //this check prevents recursion when RATE_AMOUNT is posted below
      begin
        NewPrimaryRate := EE_RATES['RATE_AMOUNT'];
        EE_RATES.DisableControls;
        try
          bmk := EE_RATES.GetBookmark;
          try
            try
              EE_RATES.First;
              while not EE_RATES.Eof do      //updates all rates that match the primary rate,
              begin                          //when primary rate changed
                if ((EE_RATES['RATE_AMOUNT'] = OldPrimaryRate) and
                    (OldPrimaryRate <> NewPrimaryRate)) then
                  SetDSValues( EE_RATES, 'RATE_AMOUNT', NewPrimaryRate );
                EE_RATES.Next;
              end
            finally
              EE_RATES.GotoBookmark( bmk );
            end;
          finally
            EE_RATES.FreeBookmark( bmk );
          end;
        finally
          EE_RATES.EnableControls;
        end;
      end
end;

function TEDIT_EE_RATES.CanClose: Boolean;
begin
  Result := inherited CanClose;
  if Result then
    Result := HasPrimaryRate;
end;


function TEDIT_EE_RATES.HasPrimaryRate(DontShowMessage: boolean = False): boolean;
var
  bmk: TBookmark;
begin
  if DM_EMPLOYEE.EE_RATES.Active then
    Result := DM_EMPLOYEE.EE_RATES.RecordCount = 0
  else
    Result := True;  

  if not Result then
  with DM_EMPLOYEE do
  begin
    EE_RATES.DisableControls;
    try
      bmk := EE_RATES.GetBookmark;
      try
        try
          EE_RATES.First;
          while not EE_RATES.Eof and not Result do
          begin
            Result := EE_RATES['PRIMARY_RATE'] = GROUP_BOX_YES;
            EE_RATES.Next;
          end
        finally
          EE_RATES.GotoBookmark( bmk );
        end;
      finally
        EE_RATES.FreeBookmark( bmk );
      end;
    finally
      EE_RATES.EnableControls;
    end;

    if (not Result) then
      result:= CheckPrimaryRateFlag(false,sysdate);

    if (not Result) and (not DontShowMessage) then
      EvMessage('Employees must have a primary rate set up!', mtWarning, [mbOk]);
  end;
end;

procedure TEDIT_EE_RATES.EENextExecute(Sender: TObject);
begin
  if HasPrimaryRate then
    inherited;
end;

procedure TEDIT_EE_RATES.EEPriorExecute(Sender: TObject);
begin
  if HasPrimaryRate then
    inherited;
end;

procedure TEDIT_EE_RATES.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if PageControl1.ActivePage = tshtRates then
    AllowChange := HasPrimaryRate;
  inherited;
end;

procedure TEDIT_EE_RATES.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavOK then
  begin
    dblkupEE_NBR.Enabled := HasPrimaryRate(True);
    dbtxtname.Enabled := dblkupEE_NBR.Enabled;
  end;
end;

procedure TEDIT_EE_RATES.SetCO_HR_POSITION_GRADESFilter;
begin
  if FPositionGrades then
  begin
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
       wwdsDetail.DataSet.UpdateRecord;
    DM_COMPANY.CO_HR_POSITION_GRADES.Filter:= 'CO_HR_POSITIONS_NBR =  ' + IntToStr(wwdsDetail.DataSet.FieldByName('CO_HR_POSITIONS_NBR').AsInteger);
    DM_COMPANY.CO_HR_POSITION_GRADES.Filtered:= True;
    DM_COMPANY.CO_HR_POSITION_GRADES.Locate('CO_HR_POSITIONS_NBR',wwdsDetail.DataSet.FieldByName('CO_HR_POSITIONS_NBR').Value,[]);
  end;
end;

procedure TEDIT_EE_RATES.evcbPositionCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Assigned(wwlcHR_Salary_Grades_Number) and
    (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
  begin
      SetCO_HR_POSITION_GRADESFilter;
      if  (evcbPosition.Value <> '') and (DM_COMPANY.CO_HR_POSITION_GRADES.RecordCount = 1) then
          wwdsDetail.DataSet['CO_HR_POSITION_GRADES_NBR'] := DM_COMPANY.CO_HR_POSITION_GRADES['CO_HR_POSITION_GRADES_NBR']
      else
          wwdsDetail.DataSet['CO_HR_POSITION_GRADES_NBR'] := null;
  end;
end;


procedure TEDIT_EE_RATES.evcbPositionChange(Sender: TObject);
begin
  inherited;
  evcbPositionCloseUp(Sender,nil,nil,true);
end;

procedure TEDIT_EE_RATES.PageControl1Change(Sender: TObject);
begin
  inherited;

  if (PageControl1.ActivePage = tshtRates) then
  begin
    SetCO_HR_POSITION_GRADESFilter;
    evcbPosition.RefreshDisplay;
  end;
end;

procedure TEDIT_EE_RATES.mnPositionCopyClick(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) then
  begin
     CopyPositionAndPayGradeTo(false);
     evcbPosition.RefreshDisplay;
     wwlcHR_Salary_Grades_Number.RefreshDisplay;
  end;
end;

procedure TEDIT_EE_RATES.cbDivisionNbrSetLookupText(Sender: TObject; var AText: String);
begin
  if (Sender as TevDBLookupCombo).Tag = 1 then
    AText := '';
end;

procedure TEDIT_EE_RATES.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  wwDBGrid1.Enabled := wwdsDetail.DataSet.State = dsBrowse;

end;

initialization
  RegisterClass(TEDIT_EE_RATES);

end.
