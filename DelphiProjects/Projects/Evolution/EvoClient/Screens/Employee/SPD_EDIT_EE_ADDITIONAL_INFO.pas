// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_ADDITIONAL_INFO;

interface

uses
   SDataStructure, wwdbedit, SPD_EDIT_EE_BASE,
  wwdbdatetimepicker, Mask, StdCtrls, wwdblook, DBActns, Classes, ActnList,
  Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls,
  DBCtrls, Controls, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBEdit, ImgList, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel, Forms,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_ADDITIONAL_INFO = class(TEDIT_EE_BASE)
    TabSheet2: TTabSheet;
    fpSummary: TisUIFashionPanel;
    fpDetails: TisUIFashionPanel;
    wwdgValues: TevDBGrid;
    Label2: TevLabel;
    lablBirth_Date: TevLabel;
    lablSalary_Amount: TevLabel;
    evLabel1: TevLabel;
    StringEdit: TevDBEdit;
    DatePicker: TevDBDateTimePicker;
    AmounTevEdit: TevDBEdit;
    evDBLookupComboValue: TevDBLookupCombo;
    fpCompany: TisUIFashionPanel;
    Label1: TevLabel;
    wwlcField: TevDBLookupCombo;
    sbAdditional: TScrollBox;
    procedure evDBLookupComboValueBeforeDropDown(Sender: TObject);
    procedure evDBLookupComboValueCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_EE_ADDITIONAL_INFO }

procedure TEDIT_EE_ADDITIONAL_INFO.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_ADDITIONAL_INFO_NAMES, aDS);
  AddDS(DM_COMPANY.CO_ADDITIONAL_INFO_VALUES, aDS);
  inherited;
end;

function TEDIT_EE_ADDITIONAL_INFO.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_ADDITIONAL_INFO;
end;

function TEDIT_EE_ADDITIONAL_INFO.GetInsertControl: TWinControl;
begin
  Result := wwlcField;
end;

procedure TEDIT_EE_ADDITIONAL_INFO.evDBLookupComboValueBeforeDropDown(
  Sender: TObject);
begin
  inherited;
     DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.Filter :=
         'CO_ADDITIONAL_INFO_NAMES_NBR ='+ DM_CLIENT.CO_ADDITIONAL_INFO_NAMES.CO_ADDITIONAL_INFO_NAMES_NBR.AsString;
     DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.Filtered := true;
end;

procedure TEDIT_EE_ADDITIONAL_INFO.evDBLookupComboValueCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.Filtered := false;
  DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.Filter := '';
end;

procedure TEDIT_EE_ADDITIONAL_INFO.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;
  if (Field = nil) or (Field.FieldName = 'CO_ADDITIONAL_INFO_NAMES_NBR') then
  begin
    evDBLookupComboValue.Enabled := not DM_CLIENT.EE_ADDITIONAL_INFO.CO_ADDITIONAL_INFO_NAMES_NBR.IsNull;
  end;
end;

initialization
  RegisterClass(TEDIT_EE_ADDITIONAL_INFO);

end.
