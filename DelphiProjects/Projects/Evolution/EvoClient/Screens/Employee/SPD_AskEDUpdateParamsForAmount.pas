// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_AskEDUpdateParamsForAmount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_AskGlobalUpdateParamsBase, ActnList,  ExtCtrls,
  StdCtrls, Buttons,EvUtils, ISBasicClasses, EvUIUtils, EvUIComponents,
  isUIEdit, LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDAmountUpdateMode = ( aumFixed, aumByPercent );

  TAskEDUpdateParamsForAmount = class(TAskGlobalUpdateParamsBase)
    evGroupBox1: TevGroupBox;
    evRadioButton1: TevRadioButton;
    evRadioButton2: TevRadioButton;
    evEdit1: TevEdit;
    evEdit2: TevEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    procedure rgClick(Sender: TObject);
    procedure OKUpdate(Sender: TObject);
  private
    { Private declarations }
    procedure SetAmount(const Value: Variant);
    function GetAmount: Variant;
    function GetAmountChangePercentage: Variant;
    function GetUpdateMode: TEDAmountUpdateMode;
    procedure SetUpdateMode(const Value: TEDAmountUpdateMode);
  public
    { Public declarations }
    property Amount: Variant read GetAmount write SetAmount;
    property AmountChangePercentage: Variant read GetAmountChangePercentage;
    property UpdateMode: TEDAmountUpdateMode read GetUpdateMode write SetUpdateMode;
  end;

implementation

{$R *.dfm}

function TAskEDUpdateParamsForAmount.GetAmount: Variant;
begin
  Result := evEdit1.Text;
end;

function TAskEDUpdateParamsForAmount.GetAmountChangePercentage: Variant;
begin
  Result := evEdit2.Text;
end;

function TAskEDUpdateParamsForAmount.GetUpdateMode: TEDAmountUpdateMode;
begin
  if evRadioButton1.Checked then
    Result := aumFixed
  else if evRadioButton2.Checked then
    Result := aumByPercent
  else
  begin
    assert( false );
    Result := aumFixed;
  end
end;

procedure TAskEDUpdateParamsForAmount.rgClick(Sender: TObject);
begin
  GrayOutControl( evEdit1, not evRadioButton1.Checked );
  GrayOutControl( evEdit2, not evRadioButton2.Checked );
end;

procedure TAskEDUpdateParamsForAmount.SetAmount(const Value: Variant);
begin
  evEdit1.Text := VarToStr( Value );
  UpdateMode := aumFixed;
end;

procedure TAskEDUpdateParamsForAmount.SetUpdateMode(
  const Value: TEDAmountUpdateMode);
begin
  case Value of
    aumFixed: evRadioButton1.Checked := true;
    aumByPercent: evRadioButton2.Checked := true;
  else
    Assert( false )
  end;
  rgClick( nil );
end;

procedure TAskEDUpdateParamsForAmount.OKUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := (UpdateMode = aumFixed) and IsFloat(evEdit1.Text)
                                    or (UpdateMode = aumByPercent) and IsFloat(evEdit2.Text);
end;

end.
