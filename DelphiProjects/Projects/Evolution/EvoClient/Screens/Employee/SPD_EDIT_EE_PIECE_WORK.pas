// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_PIECE_WORK;

interface

uses
   SDataStructure, wwdbedit, SPD_EDIT_EE_BASE, wwdblook,
  StdCtrls, Mask, DBActns, Classes, ActnList, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Controls, 
   SysUtils, EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBEdit,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, SDataDictclient, ImgList,
  SDataDicttemp, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_PIECE_WORK = class(TEDIT_EE_BASE)
    tshtPiece_Work: TTabSheet;
    pnlList: TisUIFashionPanel;
    wwdgPiece_Work: TevDBGrid;
    pnlInfo: TisUIFashionPanel;
    lablPiece: TevLabel;
    lablRate_Quantity: TevLabel;
    lablRate_Amount: TevLabel;
    dedtRate_Quantity: TevDBEdit;
    dedtRate_Amount: TevDBEdit;
    wwlcPiece: TevDBLookupCombo;
    sbPiecework: TScrollBox;
    procedure wwlcPieceCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_EE_PIECE_WORK.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_PIECES, aDS);
  inherited;
end;

function TEDIT_EE_PIECE_WORK.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_PIECE_WORK;
end;

function TEDIT_EE_PIECE_WORK.GetInsertControl: TWinControl;
begin
  Result := wwlcPiece;
end;

procedure TEDIT_EE_PIECE_WORK.wwlcPieceCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Trim(wwlcPiece.Text) <> '' then
  begin
    if Assigned(wwlcPiece.DataSource.DataSet) and
    wwlcPiece.DataSource.DataSet.Active and
    (wwlcPiece.DataSource.DataSet.State in [dsEdit, dsInsert]) then
    begin
      wwlcPiece.DataSource.DataSet.FieldByName('RATE_AMOUNT').Value := DM_CLIENT.CL_PIECES.Lookup('CL_PIECES_NBR', wwlcPiece.DataSource.DataSet.FieldByName('CL_PIECES_NBR').Value, 'DEFAULT_RATE');
      wwlcPiece.DataSource.DataSet.FieldByName('RATE_QUANTITY').Value := DM_CLIENT.CL_PIECES.Lookup('CL_PIECES_NBR', wwlcPiece.DataSource.DataSet.FieldByName('CL_PIECES_NBR').Value, 'DEFAULT_QUANTITY');
    end;
  end
  else
  begin
    if Assigned(wwlcPiece.DataSource.DataSet) and
    wwlcPiece.DataSource.DataSet.Active and
    (wwlcPiece.DataSource.DataSet.State in [dsEdit, dsInsert]) then
    begin
      wwlcPiece.DataSource.DataSet.FieldByName('RATE_AMOUNT').Clear;
      wwlcPiece.DataSource.DataSet.FieldByName('RATE_QUANTITY').Clear;
    end;
  end;
end;

initialization
  RegisterClass(TEDIT_EE_PIECE_WORK);

end.
