inherited EDIT_EE_LOCALS: TEDIT_EE_LOCALS
  Width = 978
  Height = 715
  object evLabel5: TevLabel [0]
    Left = 8
    Top = 28
    Width = 47
    Height = 13
    Caption = 'Address 1'
  end
  inherited Panel1: TevPanel
    Width = 978
    inherited pnlFavoriteReport: TevPanel
      Left = 826
    end
    inherited pnlTopLeft: TevPanel
      Width = 826
    end
  end
  inherited PageControl1: TevPageControl
    Width = 978
    Height = 628
    HelpContext = 18001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 970
        Height = 599
        inherited pnlBorder: TevPanel
          Width = 966
          Height = 595
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 966
          Height = 595
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 918
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 918
            Height = 488
            inherited Splitter1: TevSplitter
              Height = 484
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 484
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 404
                IniAttributes.SectionName = 'TEDIT_EE_LOCALS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 596
              Height = 484
              inherited wwDBGrid4: TevDBGrid
                Width = 546
                Height = 404
                IniAttributes.SectionName = 'TEDIT_EE_LOCALS\wwDBGrid4'
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              end
            end
          end
        end
      end
    end
    object tshtLocal_Taxes: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbLocals: TScrollBox
        Left = 0
        Top = 0
        Width = 970
        Height = 599
        VertScrollBar.Position = 34
        Align = alClient
        TabOrder = 0
        object fpAdditional: TisUIFashionPanel
          Left = 343
          Top = 192
          Width = 306
          Height = 403
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Additional Local Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablTax_Amount: TevLabel
            Left = 153
            Top = 301
            Width = 91
            Height = 13
            Caption = 'Override Tax Value'
            FocusControl = dedtTax_Amount
          end
          object lablMiscellaneous_Amount: TevLabel
            Left = 12
            Top = 344
            Width = 61
            Height = 13
            Caption = 'Misc Amount'
          end
          object evLabel1: TevLabel
            Left = 153
            Top = 344
            Width = 100
            Height = 13
            Caption = '% Of Taxable Wages'
            FocusControl = edPercOfTaxW
          end
          object evLabel7: TevLabel
            Left = 12
            Top = 35
            Width = 255
            Height = 13
            Caption = 'Use Effective Dates to Change Tax Statuses'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lablTax_Rate: TevLabel
            Left = 12
            Top = 301
            Width = 88
            Height = 13
            Caption = 'Override Tax Type'
            FocusControl = dedtTax_Rate
          end
          object bvAddlSplit: TEvBevel
            Left = 12
            Top = 284
            Width = 274
            Height = 3
            Shape = bsTopLine
          end
          object dedtTax_Amount: TevDBEdit
            Left = 153
            Top = 316
            Width = 130
            Height = 21
            DataField = 'OVERRIDE_LOCAL_TAX_VALUE'
            DataSource = wwdsDetail
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtMiscellaneous_Amount: TevDBEdit
            Left = 12
            Top = 359
            Width = 133
            Height = 21
            DataField = 'MISCELLANEOUS_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edPercOfTaxW: TevDBEdit
            Left = 153
            Top = 359
            Width = 130
            Height = 21
            DataField = 'PERCENTAGE_OF_TAX_WAGES'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object rdDeduct: TevDBRadioGroup
            Left = 12
            Top = 154
            Width = 271
            Height = 61
            Caption = '~Deduct'
            Columns = 2
            DataField = 'DEDUCT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Always'
              'No Overrides'
              'Never')
            ParentFont = False
            PopupMenu = pmAlwaysDeduct
            TabOrder = 2
            Values.Strings = (
              'Y'
              'D'
              'N')
            AutoPopupEffectiveDateDialog = True
          end
          object rgLocalEnabled: TevDBRadioGroup
            Left = 12
            Top = 104
            Width = 271
            Height = 45
            Caption = '~Local Enabled'
            Columns = 2
            DataField = 'LOCAL_ENABLED'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
          end
          object evDBRadioGroup3: TevDBRadioGroup
            Left = 12
            Top = 220
            Width = 271
            Height = 45
            Caption = '~Include pretax deductions in tax wages'
            Columns = 2
            DataField = 'INCLUDE_IN_PRETAX'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmINCLUDE_IN_PRETAX
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpExempt_Exclude: TevDBRadioGroup
            Left = 12
            Top = 53
            Width = 271
            Height = 45
            Caption = '~Exempt or Block'
            Columns = 3
            DataField = 'EXEMPT_EXCLUDE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpExempt_ExcludeChange
            AutoPopupEffectiveDateDialog = True
          end
          object dedtTax_Rate: TevDBComboBox
            Left = 12
            Top = 316
            Width = 133
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'OVERRIDE_LOCAL_TAX_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
        end
        object fpLocal: TisUIFashionPanel
          Left = 8
          Top = 192
          Width = 327
          Height = 403
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Local Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablLocal: TevLabel
            Left = 12
            Top = 35
            Width = 35
            Height = 16
            Caption = '~Local'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCounty: TevLabel
            Left = 12
            Top = 74
            Width = 33
            Height = 13
            Caption = 'County'
          end
          object lablTaxCode: TevLabel
            Left = 12
            Top = 113
            Width = 46
            Height = 13
            Caption = 'Tax Code'
          end
          object evLabel2: TevLabel
            Left = 213
            Top = 35
            Width = 90
            Height = 13
            Caption = 'Standard Tax Rate'
          end
          object wwlcLocal: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 193
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'LocalName'#9'40'#9'LocalName'#9'F'
              'localState'#9'6'#9'localState'#9'F'
              'PSDCode'#9'20'#9'PSD Code'#9'F')
            DataField = 'CO_LOCAL_TAX_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
            LookupField = 'CO_LOCAL_TAX_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = wwlcLocalChange
            OnBeforeDropDown = wwlcLocalBeforeDropDown
            OnCloseUp = wwlcLocalCloseUp
          end
          object dedtCounty: TevDBEdit
            Left = 12
            Top = 89
            Width = 294
            Height = 21
            DataField = 'EE_COUNTY'
            DataSource = wwdsDetail
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*[&[*?]*[{#, ,.,;,,;;,:,;[,;],(,),}]]'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtTaxCode: TevDBEdit
            Left = 12
            Top = 128
            Width = 294
            Height = 21
            DataField = 'EE_TAX_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object edStandartTaxRate: TevDBEdit
            Left = 213
            Top = 50
            Width = 92
            Height = 21
            Color = clBtnFace
            DataField = 'companyRate'
            DataSource = dsCO_LOCAL_TAX
            Enabled = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object grWorkAddress: TevGroupBox
            Left = 12
            Top = 191
            Width = 294
            Height = 190
            Caption = 'Work Address  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            object evLabel3: TevLabel
              Left = 12
              Top = 23
              Width = 47
              Height = 13
              Caption = 'Address 1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel4: TevLabel
              Left = 12
              Top = 62
              Width = 47
              Height = 13
              Caption = 'Address 2'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel6: TevLabel
              Left = 12
              Top = 101
              Width = 26
              Height = 13
              Caption = 'City   '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel8: TevLabel
              Left = 208
              Top = 101
              Width = 15
              Height = 13
              Caption = 'Zip'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel9: TevLabel
              Left = 157
              Top = 101
              Width = 28
              Height = 13
              Caption = 'State '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel53: TevLabel
              Left = 12
              Top = 140
              Width = 41
              Height = 13
              Caption = 'Location'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object edAddress1: TevDBEdit
              Left = 12
              Top = 38
              Width = 268
              Height = 21
              DataField = 'WORK_ADDRESS1'
              DataSource = wwdsDetail
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edAddress2: TevDBEdit
              Left = 12
              Top = 77
              Width = 268
              Height = 21
              DataField = 'WORK_ADDRESS2'
              DataSource = wwdsDetail
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edCity: TevDBEdit
              Left = 12
              Top = 116
              Width = 137
              Height = 21
              DataField = 'WORK_CITY'
              DataSource = wwdsDetail
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edZipCode: TevDBEdit
              Left = 208
              Top = 116
              Width = 72
              Height = 21
              DataField = 'WORK_ZIP_CODE'
              DataSource = wwdsDetail
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 4
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edState: TevDBEdit
              Left = 157
              Top = 116
              Width = 43
              Height = 21
              DataField = 'WORK_STATE'
              DataSource = wwdsDetail
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&,@}'
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object evBtnCopyEEAddress: TevBitBtn
              Left = 120
              Top = 12
              Width = 160
              Height = 21
              Caption = 'Copy EE Address'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 6
              OnClick = evBtnCopyEEAddressClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFCFCFCFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
                CCCCCCCCCCCCCCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCFCFCFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFBB8B36B68124B57F20B57F1FB57F1FB57F1FB57F
                1FB57F20B68124BB8B36FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF82828279
                7979777676777676777676777676777676777676797979828282FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFB68225FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFB68124FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7979FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979CFCFCFCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCB58022FFFFFFEADDBAEADCB8E8D9B3E7D7B1E6D6
                AFE6D6AEFFFFFFB57F1FCFCFCFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC787877FF
                FFFFD8D8D8D7D6D6D3D3D3D2D2D1D0D0D0D0D0D0FFFFFF777676BB8B36B68124
                B57F20B57F1FB57F1FB47E1EB37B1AB07712B07612BB8A32EADCB8E7D7B1E6D6
                AFE5D5ACFFFFFFB47E1E8282827979797776767776767776767676757473736F
                6F6F6E6E6E828181D7D6D6D2D2D1D0D0D0CFCFCFFFFFFF767675B68124FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB07610FCFBF4F9F4E9F8F2
                E6F8F2E5FFFFFFB47E1E797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF6E6E6EFBFBFBF3F3F3F1F1F1F1F1F1FFFFFF767675B57F1FFFFFFF
                E6D6AEE6D6AFE6D7B0E6D7B0E6D6AFE6D6AEFFFFFFAE740EF8F5E7F4EEDCF3EC
                D9F3ECD8FFFFFFB47E1E777676FFFFFFD0D0D0D0D0D0D1D1D1D1D1D1D0D0D0D0
                D0D0FFFFFF6C6C6CF3F3F3ECEBEBEAEAEAE9E9E9FFFFFF767675B47E1EFFFFFF
                E5D5ACE6D6AFE6D6AFE6D6AFE6D6AFE5D5ACFFFFFFAE740EF6F0DEF1E8D1F0E6
                CDF0E7CDFFFFFFB47E1E767675FFFFFFCFCFCFD0D0D0D0D0D0D0D0D0D0D0D0CF
                CFCFFFFFFF6C6C6CEEEEEEE5E5E5E3E3E2E4E3E3FFFFFF767675B47E1EFFFFFF
                F8F2E5F8F2E6F8F3E7F8F3E7F8F2E6F8F2E5FFFFFFAE740EF1EAD3F7F1E4FFFF
                FFFFFFFFFFFFFFB47F1E767675FFFFFFF1F1F1F1F1F1F2F2F1F2F2F1F1F1F1F1
                F1F1FFFFFF6C6C6CE7E7E6F0F0F0FFFFFFFFFFFFFFFFFF777676B47E1EFFFFFF
                F3ECD9F3ECD9F3ECDAF3ECD9F3ECD9F3ECD8FFFFFFAE740EEFE4C7FFFFFFCFAB
                6DAC7005FFFFFFB58022767675FFFFFFEAEAEAEAEAEAEAEAEAEAEAEAEAEAEAE9
                E9E9FFFFFF6C6C6CE0DFDFFFFFFFA3A3A3696969FFFFFF787877B47E1EFFFFFF
                F1E8CFF1E8D0F1E8D0F0E7CFF0E6CDF0E7CDFFFFFFAE740EEBDEBCFFFFFFAC70
                05FFFFFFECDCC4CFAC6D767675FFFFFFE4E4E4E5E5E5E5E5E5E4E3E3E3E3E2E4
                E3E3FFFFFF6C6C6CD9D9D9FFFFFF696969FFFFFFD9D9D9A3A3A3B47E1EFFFFFF
                EDE3C7EDE3C8EDE3C7F6F0E1FFFFFFFFFFFFFFFFFFB07611FFFFFFFFFFFFFFFF
                FFECDCC4CFAC6EFFFFFF767675FFFFFFDFDFDEE0DFDFDFDFDEEFEEEEFFFFFFFF
                FFFFFFFFFF6E6E6EFFFFFFFFFFFFFFFFFFD9D9D9A4A4A4FFFFFFB47E1EFFFFFF
                EBDDBCEBDDBDEBDCBBFFFFFFCFAB6DAC7005FFFFFFB37D1CB58021B57F20B580
                22CFAC6DFFFFFFFFFFFF767675FFFFFFD8D8D8D8D8D8D7D7D7FFFFFFA3A3A369
                6969FFFFFF757474787877777676787877A3A3A3FFFFFFFFFFFFB57F1FFFFFFF
                E7D8B1E7D8B1E7D7B0FFFFFFAC7005FFFFFFECDCC3D0AF73FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF777676FFFFFFD3D2D2D3D2D2D2D2D1FFFFFF696969FF
                FFFFD9D9D9A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB68124FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECDCC4CFAC6EFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
                D9D9A4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBE8F3AB68124
                B57F1FB47E1EB47E1EB47F1FB58022C3984AFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFF8686867979797776767676757676757776767878778E
                8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object evDBLookupCombo8: TevDBLookupCombo
              Left = 12
              Top = 155
              Width = 267
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
                'ADDRESS1'#9'30'#9'Address1'#9'F'
                'CITY'#9'20'#9'City'#9'F'
                'ZIP_CODE'#9'10'#9'Zip Code'#9'F'
                'STATE'#9'2'#9'State'#9'F')
              DataField = 'CO_LOCATIONS_NBR'
              DataSource = wwdsDetail
              LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
              LookupField = 'CO_LOCATIONS_NBR'
              Style = csDropDownList
              ParentFont = False
              PopupMenu = pmLocation
              TabOrder = 5
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = False
            end
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 294
            Height = 36
            Caption = 'Work Address Override'
            Columns = 2
            DataField = 'WORK_ADDRESS_OVR'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = -26
          Width = 641
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          OnResize = fpSummaryResize
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object pnlFPLocalsBody: TevPanel
            Left = 12
            Top = 35
            Width = 605
            Height = 153
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object evDBGrid1: TevDBGrid
              Left = 0
              Top = 0
              Width = 605
              Height = 153
              DisableThemesInTitle = False
              Selected.Strings = (
                'Local_Name_Lookup'#9'32'#9'Local'#9'F'
                'EE_COUNTY'#9'28'#9'County'#9'F'
                'State'#9'2'#9'State'#9'F'
                'LOCALTYPEDESC'#9'32'#9'Local Type'#9'F'
                'TaxTypeDesc'#9'20'#9'Override Tax Type'#9'F'
                'OVERRIDE_LOCAL_TAX_VALUE'#9'10'#9'Value'#9'F'
                'EXEMPT_EXCLUDE'#9'1'#9'Exempt Exclude'#9'F'
                'MISCELLANEOUS_AMOUNT'#9'10'#9'Misc Amount'#9'F'
                'LOCAL_ENABLED'#9'1'#9'Enabled'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_EE_LOCALS\evDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsDetail
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 978
    inherited dblkupEE_NBR: TevDBLookupCombo
      Top = 7
    end
    inherited dbtxtname: TevDBLookupCombo
      Top = 7
    end
    inherited butnPrior: TevBitBtn
      Top = 4
    end
    inherited butnNext: TevBitBtn
      Top = 4
    end
  end
  inline HideRecordHelper: THideRecordHelper [4]
    Left = 371
    Top = 57
    Width = 104
    Height = 27
    AutoScroll = False
    TabOrder = 3
    inherited butnNext: TevBitBtn
      Top = 1
      Width = 89
    end
    inherited evActionList1: TevActionList
      Left = 88
      Top = 65532
      inherited HideCurRecord: TAction
        Caption = 'Hide this Local'
      end
    end
    inherited dsHelper: TevDataSource
      Left = 120
      Top = 65532
    end
  end
  inherited wwdsMaster: TevDataSource [5]
  end
  inherited wwdsDetail: TevDataSource [6]
    DataSet = DM_EE_LOCALS.EE_LOCALS
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    Left = 734
  end
  inherited wwdsList: TevDataSource [7]
    Left = 106
    Top = 26
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY [8]
    Left = 593
    Top = 63
  end
  inherited PageControlImages: TevImageList [9]
    Left = 504
    Top = 72
  end
  inherited dsCL: TevDataSource [10]
  end
  inherited DM_CLIENT: TDM_CLIENT [11]
  end
  inherited DM_COMPANY: TDM_COMPANY [12]
  end
  inherited wwdsEmployee: TevDataSource [13]
    Left = 654
    Top = 64
  end
  inherited DM_EMPLOYEE: TDM_EMPLOYEE [14]
    Left = 220
    Top = 66
  end
  inherited wwdsSubMaster: TevDataSource [15]
    Left = 638
    Top = 21
  end
  inherited wwdsSubMaster2: TevDataSource [16]
  end
  inherited ActionList: TevActionList [17]
  end
  inherited EEClone: TevClientDataSet
    Left = 570
  end
  object evSecElement1: TevSecElement
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 384
    Top = 8
  end
  object dsCO_LOCAL_TAX: TevDataSource
    DataSet = CO_LOCAL_TAXClone
    Left = 688
    Top = 96
  end
  object pmAlwaysDeduct: TevPopupMenu
    Left = 764
    Top = 105
    object CopyTo1: TMenuItem
      Action = CopyAD
    end
  end
  object evActionList1: TevActionList
    Left = 628
    Top = 116
    object CopyAD: TAction
      Caption = 'Copy To...'
      OnExecute = CopyADExecute
      OnUpdate = CopyADUpdate
    end
  end
  object CO_LOCAL_TAXClone: TevClientDataSet
    Filtered = True
    Left = 738
    Top = 90
  end
  object pmLocation: TevPopupMenu
    Left = 772
    Top = 185
    object mnCopyLocation: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnCopyLocationClick
    end
  end
  object pmINCLUDE_IN_PRETAX: TevPopupMenu
    Left = 772
    Top = 233
    object mnIncludePretax: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnIncludePretaxClick
    end
  end
end
