// Screens of "Employee"
unit scr_Employee;

interface

uses
  SEmployeeScr,
  SPD_EDIT_EE_RATES,
  SPD_EDIT_EE_STATES,
  SPD_EDIT_EE_LOCALS,
  SPD_EDIT_EE_SCHEDULED_E_DS,
  SPD_EDIT_EE_TIME_OFF_ACCRUAL,
  SPD_EDIT_EE_WORK_SHIFTS,
  SPD_EDIT_EE_AUTOLABOR_DISTRIBUTION,
  SPD_EDIT_EE_PENSION_FUND_SPLITS,
  SPD_EDIT_EE_PIECE_WORK,
  SPD_EDIT_EE_ADDITIONAL_INFO,
  SPD_EDIT_EE_AND_CL_PERSON,
  SPD_EDChangeForm,
  SPD_POPUP_EDIT_EE_LOCALS,
  SPD_EmployeeWithLocalsChoiceQuery,
  SPD_AskEDUpdateParamsForAmount,
  SPD_AskEDUpdateParamsForPercent,
  SPD_AskRatesUpdateParams,
  SPD_EE_QUICK_ENTRY,
  SDBDT,
  SPD_RehirePromptForm;

implementation
end.
