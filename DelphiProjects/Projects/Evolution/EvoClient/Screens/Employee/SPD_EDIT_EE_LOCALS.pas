// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_EE_LOCALS;

interface

uses
   SDataStructure, wwdbedit, SPD_EDIT_EE_BASE,
  wwdbdatetimepicker, wwdblook, StdCtrls, ExtCtrls, DBCtrls, Mask, DBActns,
  Classes, ActnList, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, EvTypes,
  ComCtrls, Controls, EvConsts, Wwdotdot, SPackageEntry,
  Wwdbcomb, EvSecElement, Menus, Variants, Forms, SPD_HideRecordHelper,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, DateUtils,
  ISDataAccessComponents, EvDataAccessComponents, 
  SDataDictclient, SDataDicttemp, EvExceptions, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBComboBox, ImgList, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_LOCALS = class(TEDIT_EE_BASE)
    tshtLocal_Taxes: TTabSheet;
    evSecElement1: TevSecElement;
    dsCO_LOCAL_TAX: TevDataSource;
    pmAlwaysDeduct: TevPopupMenu;
    CopyTo1: TMenuItem;
    evActionList1: TevActionList;
    CopyAD: TAction;
    HideRecordHelper: THideRecordHelper;
    CO_LOCAL_TAXClone: TevClientDataSet;
    evLabel5: TevLabel;
    sbLocals: TScrollBox;
    fpSummary: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    fpLocal: TisUIFashionPanel;
    lablLocal: TevLabel;
    lablCounty: TevLabel;
    lablTaxCode: TevLabel;
    evLabel2: TevLabel;
    wwlcLocal: TevDBLookupCombo;
    dedtCounty: TevDBEdit;
    dedtTaxCode: TevDBEdit;
    edStandartTaxRate: TevDBEdit;
    grWorkAddress: TevGroupBox;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel6: TevLabel;
    evLabel8: TevLabel;
    evLabel9: TevLabel;
    edAddress1: TevDBEdit;
    edAddress2: TevDBEdit;
    edCity: TevDBEdit;
    edZipCode: TevDBEdit;
    edState: TevDBEdit;
    fpAdditional: TisUIFashionPanel;
    lablTax_Amount: TevLabel;
    lablMiscellaneous_Amount: TevLabel;
    evLabel1: TevLabel;
    dedtTax_Amount: TevDBEdit;
    dedtMiscellaneous_Amount: TevDBEdit;
    edPercOfTaxW: TevDBEdit;
    rdDeduct: TevDBRadioGroup;
    rgLocalEnabled: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    pnlFPLocalsBody: TevPanel;
    evBtnCopyEEAddress: TevBitBtn;
    evLabel7: TevLabel;
    drgpExempt_Exclude: TevDBRadioGroup;
    lablTax_Rate: TevLabel;
    dedtTax_Rate: TevDBComboBox;
    evDBRadioGroup1: TevDBRadioGroup;
    evLabel53: TevLabel;
    evDBLookupCombo8: TevDBLookupCombo;
    bvAddlSplit: TEvBevel;
    pmLocation: TevPopupMenu;
    mnCopyLocation: TMenuItem;
    pmINCLUDE_IN_PRETAX: TevPopupMenu;
    mnIncludePretax: TMenuItem;
    procedure drgpExempt_ExcludeChange(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure CopyADExecute(Sender: TObject);
    procedure CopyADUpdate(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwlcLocalChange(Sender: TObject);
    procedure wwlcLocalCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcLocalBeforeDropDown(Sender: TObject);
    procedure evBtnCopyEEAddressClick(Sender: TObject);
    procedure fpSummaryResize(Sender: TObject);
    procedure mnCopyLocationClick(Sender: TObject);
    procedure mnIncludePretaxClick(Sender: TObject);
  private
    FCoLocalNbr: Variant;
    FAlwaysDeduct: Variant;
    procedure CopyLocation;
    procedure CopyAlwaysDeduct;
    function CopyAlwaysDeductTo(ee: TEvClientDataSet): boolean;
    function CanChangeOrDelete: Boolean;
    procedure CopyPretax;    
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    procedure DoOnBroadcastTableChange(const ATable: String; const ANbr: Integer; const AChangeType: TevTableChangeType); override;

  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;    
    constructor Create(AOwner: TComponent); override;
    procedure Activate; override;
    procedure DeActivate; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;

  end;

implementation

{$R *.DFM}

uses
  Dialogs, SSecurityInterface, EvUtils, sysutils, SPD_EmployeeWithLocalsChoiceQuery, 
  SFrameEntry, SPD_EmployeeChoiceQuery, SPD_SafeChoiceFromListQuery;

const
  CurrentFilter: string = 'LOCAL_ENABLED like ''%Y%''';

procedure TEDIT_EE_LOCALS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);

  if DM_EMPLOYEE.EE_LOCALS.Active then
    DM_EMPLOYEE.EE_LOCALS.Close;

  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  inherited;

  //To avoid Evo freezing
  wwlcLocal.LookupTable := DM_CLIENT.CO_LOCAL_TAX;
end;

function TEDIT_EE_LOCALS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_LOCALS;
end;

function TEDIT_EE_LOCALS.GetInsertControl: TWinControl;
begin
  //added the button enable here because there appears to be a multi-call sync
  //issue here with regard to this control being enabled due to the dataset state
  //changing.
  wwlcLocal.Enabled := True;
  Result := wwlcLocal;
end;

procedure TEDIT_EE_LOCALS.drgpExempt_ExcludeChange(Sender: TObject);
var
  OldVal: string;
begin
  inherited;

  with (Sender as TevDBRadioGroup) do
    if Assigned(Field) and Assigned(Field.DataSet) and
      (Field.DataSet.State in [dsEdit]) then
    begin
      OldVal := VarToStr(Field.Value);
      if (DM_EMPLOYEE.EE['EXEMPT_EXCLUDE_EE_FED'] = GROUP_BOX_EXEMPT) and
        (Value <> GROUP_BOX_EXEMPT) and (OldVal = GROUP_BOX_EXEMPT) then
        if EvMessage('Do You Want to Change the Tax Status for this 1099?',
          mtConfirmation, [mbYes, mbNo]) <> mrYes then
          Field.DataSet.Cancel;
    end

end;

procedure TEDIT_EE_LOCALS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  wwlcLocal.Enabled := wwdsDetail.DataSet.State = dsInsert;
  mnCopyLocation.Enabled := assigned(DM_EMPLOYEE.EE_LOCALS) and (DM_EMPLOYEE.EE_LOCALS.State = dsBrowse) and (DM_EMPLOYEE.EE_LOCALS.RecordCount > 0);
end;

constructor TEDIT_EE_LOCALS.Create(AOwner: TComponent);
begin
  inherited;
  evSecElement1.AtomComponent := nil;
end;

procedure TEDIT_EE_LOCALS.CopyAlwaysDeduct;
  procedure DoCopyAlwaysDeduct;
  var
    saveEE: TBookmark;
  begin
    with TEmployeeWithLocalsChoiceQuery.Create(Self) do
    try
      FCoLocalNbr := DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'];
      FAlwaysDeduct := DM_EMPLOYEE.EE_LOCALS['DEDUCT'];

      Setup(DM_EMPLOYEE.EE_LOCALS['EE_NBR'], DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR']);
      Caption := 'Select the employees you want the Always Deduct setting copied to';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &All';
      ConfirmAllMessage := 'This operation will copy the Always Deduct setting to all employees in this company. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count > 0) then
      begin
        saveEE := DM_EMPLOYEE.EE.GetBookmark;
        try
          try
            ForSelected(dgChoiceList, CopyAlwaysDeductTo);
          except
            ctx_DataAccess.CancelDataSets([DM_EMPLOYEE.EE_LOCALS]);
            raise;
          end;
          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_LOCALS]);
        finally
          DM_EMPLOYEE.EE.GotoBookmark(saveEE);
          DM_EMPLOYEE.EE.FreeBookmark(saveEE);
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  ctx_DataAccess.CancelDataSets([DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_LOCALS]);
  DM_EMPLOYEE.EE_LOCALS.SaveState;
  try
    DoCopyAlwaysDeduct;
  finally
    DM_EMPLOYEE.EE_LOCALS.LoadState;
  end;
end;

function TEDIT_EE_LOCALS.CopyAlwaysDeductTo(ee: TEvClientDataSet): boolean;
begin
  if DM_EMPLOYEE.EE.Locate('EE_NBR', ee['EE_NBR'], [])
    and DM_EMPLOYEE.EE_LOCALS.Locate('CO_LOCAL_TAX_NBR', FCoLocalNbr, []) then
  begin
    DM_EMPLOYEE.EE_LOCALS.Edit;
    DM_EMPLOYEE.EE_LOCALS['DEDUCT'] := FAlwaysDeduct;
    DM_EMPLOYEE.EE_LOCALS.Post;
  end
  else
    Assert(false);
  Result := true;
end;

procedure TEDIT_EE_LOCALS.CopyADExecute(Sender: TObject);
begin
  CopyAlwaysDeduct;
end;

procedure TEDIT_EE_LOCALS.CopyADUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := assigned(DM_EMPLOYEE.EE_LOCALS) and (DM_EMPLOYEE.EE_LOCALS.State = dsBrowse) and (DM_EMPLOYEE.EE_LOCALS.RecordCount > 0);
end;

procedure TEDIT_EE_LOCALS.CopyPretax;

  procedure DoCopyPretax;
  var
    saveEE: TBookmark;
    tmpEENbr, tmpField: Variant;
    k: integer;
  begin
    tmpEENbr := DM_EMPLOYEE.EE_LOCALS['EE_NBR'];
    FCoLocalNbr := DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'];
    tmpField := DM_EMPLOYEE.EE_LOCALS['INCLUDE_IN_PRETAX'];


    with TEmployeeWithLocalsChoiceQuery.Create(Self) do
    try
      FCoLocalNbr := DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'];

      Setup(DM_EMPLOYEE.EE_LOCALS['EE_NBR'], DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR']);
      ConfirmAllMessage := 'This operation will copy the Include pretax deductions in tax wages to all employees in this company. Are you sure you want to do this?';
      pnlEffectiveDate.Visible := true;
      Caption := 'Select the employees you want the Include pretax deductions in tax wages setting copied to';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &All';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count > 0) then
      begin
        ctx_StartWait('Processing...');

        saveEE := DM_EMPLOYEE.EE.GetBookmark;
        try

            DM_EMPLOYEE.EE_LOCALS.DisableControls;
            try
              for k := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
               cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
               DM_EMPLOYEE.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);
               if DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsVariant <> tmpEENbr then
                 if DM_EMPLOYEE.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsVariant, FCoLocalNbr]) , []) then
                 begin
                   ctx_DBAccess.StartTransaction([dbtClient]);
                   try
                     ctx_DBAccess.UpdateFieldValue('EE_LOCALS', 'INCLUDE_IN_PRETAX', DM_EMPLOYEE.EE_LOCALS.EE_LOCALS_NBR.Value, dtBeginEffective.Date, DayBeforeEndOfTime, tmpField);
                     ctx_DBAccess.CommitTransaction;
                     DM_EMPLOYEE.EE_LOCALS.RefreshRecord(DM_EMPLOYEE.EE_LOCALS.EE_LOCALS_NBR.Value);
                   except
                     ctx_DBAccess.RollbackTransaction;
                     raise;
                   end;
                 end;
              end;
            finally
              ctx_EndWait;
              DM_EMPLOYEE.EE_LOCALS.EnableControls;
              DM_EMPLOYEE.EE.Locate('EE_NBR',tmpEENbr, []);
              DM_EMPLOYEE.EE_LOCALS.Locate('CO_LOCAL_TAX_NBR', FCoLocalNbr, []);
            end;

        finally
          DM_EMPLOYEE.EE.GotoBookmark(saveEE);
          DM_EMPLOYEE.EE.FreeBookmark(saveEE);
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_EMPLOYEE.EE_LOCALS.SaveState;
  try
    DoCopyPretax;
  finally
    DM_EMPLOYEE.EE_LOCALS.LoadState;
  end;
end;


procedure TEDIT_EE_LOCALS.Activate;
var
  r: Integer;
begin
  inherited;
  HideRecordHelper.SetDataSet(DM_EMPLOYEE.EE_LOCALS, 'LOCAL_ENABLED');
  HideRecordHelper.OnCanHideRecord := HideRecordHelper.CanHideAny;
  DM_EMPLOYEE.EE_LOCALS.DisableControls;
  try
    r := DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger;
    DM_EMPLOYEE.EE_LOCALS.FilterOptions := DM_EMPLOYEE.EE_LOCALS.FilterOptions + [foCaseInsensitive];
    DM_EMPLOYEE.EE_LOCALS.UserFilter := CurrentFilter;
    DM_EMPLOYEE.EE_LOCALS.UserFiltered := True;
    DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', r, []);
  finally
    DM_EMPLOYEE.EE_LOCALS.EnableControls;
  end;
end;

procedure TEDIT_EE_LOCALS.DeActivate;
var
  r: Integer;
begin
  DM_EMPLOYEE.EE_LOCALS.DisableControls;
  try
    GetDefaultDataSet.Filter := '';
    GetDefaultDataSet.Filtered := false;
    r := 0;
    if DM_EMPLOYEE.EE_LOCALS.Active then
      r := DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger;

    DM_EMPLOYEE.EE_LOCALS.UserFiltered := False;
    DM_EMPLOYEE.EE_LOCALS.UserFilter := '';
    if DM_EMPLOYEE.EE_LOCALS.Active then
      DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', r, []);
  finally
    DM_EMPLOYEE.EE_LOCALS.EnableControls;
  end;
  inherited;
end;

procedure TEDIT_EE_LOCALS.AfterDataSetsReopen;
begin
  inherited;
  with TisProvider.Create(nil) do
  try
    DataSet := DM_COMPANY.CO_LOCAL_TAX;
    DM_COMPANY.CO_LOCAL_TAX.First;
    CO_LOCAL_TAXClone.Data := Data;
    TFloatField(CO_LOCAL_TAXClone.FieldByName('companyRate')).DisplayFormat := '#,##0.00######';
  finally
    Free;
  end;
  wwdsDetailDataChange(nil, nil);

end;

procedure TEDIT_EE_LOCALS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if not CO_LOCAL_TAXClone.Active or not CO_LOCAL_TAXClone.Locate('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'], []) then
    edStandartTaxRate.Text := '';
  if rgLocalEnabled.Value = GROUP_BOX_NO then
    rdDeduct.Enabled := False
  else
    rdDeduct.Enabled := True;

  if assigned(grWorkAddress) then
     grWorkAddress.Enabled :=  assigned(wwdsDetail.DataSet) and
                            ((DM_EMPLOYEE.EE_LOCALS.FieldByName('LOCAL_TYPE').AsString = LOCAL_TYPE_INC_NON_RESIDENTIAL) or
                             (DM_EMPLOYEE.EE_LOCALS.FieldByName('LOCAL_TYPE').AsString = LOCAL_TYPE_OCCUPATIONAL)) and
                             (DM_EMPLOYEE.EE_LOCALS.FieldByName('SY_STATES_NBR').AsInteger = 41);

end;

function TEDIT_EE_LOCALS.CanChangeOrDelete: Boolean;
  function CheckLocal(TableName: string): Integer;
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('TableName', TableName);
      SetMacro('Columns', 'count(*)');
      SetMacro('NbrField', 'EE_LOCALS');
      SetParam('RecordNbr', DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      Result := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger;
    end;
  end;
begin
  Result := (CheckLocal('PR_CHECK_LINE_LOCALS') = 0) and (CheckLocal('PR_CHECK_LOCALS') = 0);
end;

procedure TEDIT_EE_LOCALS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  case Kind of
    NavDelete:
      begin
        if not CanChangeOrDelete then
        begin
          DM_EMPLOYEE.EE_LOCALS.Cancel;
          raise EUpdateError.CreateHelp('Cannot delete this local tax because it''s in use!', IDH_ConsistencyViolation);
        end;
      end;
  end;
end;

procedure TEDIT_EE_LOCALS.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort] then
     ApplySecurity;
end;

procedure TEDIT_EE_LOCALS.wwlcLocalChange(Sender: TObject);
begin
  if wwlcLocal.Focused and (DM_EMPLOYEE.EE_LOCALS.State <> dsBrowse) and (DM_EMPLOYEE.EE_LOCALS.State <> dsInsert) then
    if not CanChangeOrDelete then
    begin
      DM_EMPLOYEE.EE_LOCALS.Cancel;
      raise EUpdateError.CreateHelp('Cannot change this local tax because it''s in use!', IDH_ConsistencyViolation);
    end;
end;

procedure TEDIT_EE_LOCALS.wwlcLocalCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
var
  State: String;
begin
  inherited;
  DM_CLIENT.CO_LOCAL_TAX.Filter := '';
  DM_CLIENT.CO_LOCAL_TAX.Filtered := False;
  if wwlcLocal.LookupValue <> '' then
  begin
    DM_CLIENT.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', wwlcLocal.LookupValue, []);
    DM_SYSTEM.SY_LOCALS.Locate('SY_LOCALS_NBR', DM_CLIENT.CO_LOCAL_TAX.FIELDBYNAME('SY_LOCALS_NBR').AsString, []);

    DM_CLIENT.EE_LOCALS.FieldByName('EE_COUNTY').AsString := DM_SYSTEM.SY_COUNTY.Lookup('SY_COUNTY_NBR', DM_SYSTEM.SY_LOCALS.FIELDBYNAME('SY_COUNTY_NBR').AsString, 'COUNTY_NAME');

    if (DM_EMPLOYEE.EE_LOCALS.State = dsInsert) then
        DM_EMPLOYEE.EE_LOCALS.FieldByName('DEDUCT').AsString := GetCoLocalTaxDeduct(DM_CLIENT.CO_LOCAL_TAX.FieldByName('DEDUCT').AsString,DM_CLIENT.CO_LOCAL_TAX.FieldByName('LOCAL_TYPE').AsString);

    // Disallow for all users to select inactive States;
    State := DM_CLIENT.CO_LOCAL_TAX['LocalState'];
    if (wwdsDetail.DataSet.State = dsInsert) and (State <> '') then
    begin
      DM_CLIENT.CO_STATES.DataRequired('CO_NBR = ' + wwdsList.DataSet.FieldByName('CO_NBR').AsString);
      if DM_CLIENT.CO_STATES.Locate('STATE', State, []) then
        if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
        begin
          EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
          wwlcLocal.Clear;
          DM_CLIENT.EE_LOCALS['CO_LOCAL_TAX_NBR'] := Null;
        end;
    end;
  end;
end;

procedure TEDIT_EE_LOCALS.wwlcLocalBeforeDropDown(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CO_LOCAL_TAX.Filter := 'LOCAL_ACTIVE =''' + GROUP_BOX_YES+'''';
  DM_CLIENT.CO_LOCAL_TAX.Filtered := True;
end;

procedure TEDIT_EE_LOCALS.evBtnCopyEEAddressClick(Sender: TObject);
begin
  inherited;

  if (wwdsDetail.DataSet.FieldByName('EE_NBR').AsInteger > 0) or
      (wwdsDetail.DataSet.State in [dsEdit,dsInsert]) then
  begin
   if wwdsDetail.DataSet.State = dsBrowse then
      wwdsDetail.DataSet.Edit;

   wwdsDetail.DataSet.FieldByName('WORK_ADDRESS1').Value := DM_CLIENT.CL_PERSON.FieldByName('ADDRESS1').Value;
   wwdsDetail.DataSet.FieldByName('WORK_ADDRESS2').Value := DM_CLIENT.CL_PERSON.FieldByName('ADDRESS2').Value;
   wwdsDetail.DataSet.FieldByName('WORK_CITY').Value := DM_CLIENT.CL_PERSON.FieldByName('CITY').Value;
   wwdsDetail.DataSet.FieldByName('WORK_STATE').Value := DM_CLIENT.CL_PERSON.FieldByName('STATE').Value;
   wwdsDetail.DataSet.FieldByName('WORK_ZIP_CODE').Value := DM_CLIENT.CL_PERSON.FieldByName('ZIP_CODE').Value;
  end;

end;

procedure TEDIT_EE_LOCALS.fpSummaryResize(Sender: TObject);
begin
  inherited;
  pnlFPLocalsBody.Width := fpSummary.Width - 40;
  pnlFPLocalsBody.Height := fpSummary.Height - 65;
end;

procedure TEDIT_EE_LOCALS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_COMPANY.CO_LOCAL_TAX, SupportDataSets, '');
end;

procedure TEDIT_EE_LOCALS.DoOnBroadcastTableChange(const ATable: String;
  const ANbr: Integer; const AChangeType: TevTableChangeType);
begin
  inherited;
  if AnsiSameText(ATable, 'EE_LOCALS') then
    DM_EMPLOYEE.EE_LOCALS.RefreshRecord(ANbr);
end;

procedure TEDIT_EE_LOCALS.CopyLocation;
  procedure DoCopyLocation;
  var
   k: integer;
   tmpEENbr, tmpField: Variant;

  begin
    tmpEENbr := DM_EMPLOYEE.EE_LOCALS['EE_NBR'];
    FCoLocalNbr := DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'];
    tmpField := DM_EMPLOYEE.EE_LOCALS['CO_LOCATIONS_NBR'];

    with TEmployeeChoiceQuery.Create(Self) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      pnlEffectiveDate.Visible := true;
      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      ConfirmAllMessage := 'This operation will copy the Location to all employees in this company. Are you sure you want to do this?';

      Caption := 'Select the employees you want the Location copied to';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');

        DM_EMPLOYEE.EE_LOCALS.DisableControls;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
           cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
           DM_EMPLOYEE.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);
           if DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsVariant <> tmpEENbr then
             if DM_EMPLOYEE.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsVariant, FCoLocalNbr]) , []) then
             begin
               ctx_DBAccess.StartTransaction([dbtClient]);
               try
                 ctx_DBAccess.UpdateFieldValue('EE_LOCALS', 'CO_LOCATIONS_NBR', DM_EMPLOYEE.EE_LOCALS.EE_LOCALS_NBR.Value, dtBeginEffective.Date, DayBeforeEndOfTime, tmpField);
                 ctx_DBAccess.CommitTransaction;
                 DM_EMPLOYEE.EE_LOCALS.RefreshRecord(DM_EMPLOYEE.EE_LOCALS.EE_LOCALS_NBR.Value);
               except
                 ctx_DBAccess.RollbackTransaction;
                 raise;
               end;
             end;
          end;
        finally
          ctx_EndWait;
          DM_EMPLOYEE.EE_LOCALS.EnableControls;
          DM_EMPLOYEE.EE.Locate('EE_NBR',tmpEENbr, []);
          DM_EMPLOYEE.EE_LOCALS.Locate('CO_LOCAL_TAX_NBR', FCoLocalNbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  ctx_DataAccess.CancelDataSets([DM_EMPLOYEE.EE_LOCALS]);
  DoCopyLocation;
end;

procedure TEDIT_EE_LOCALS.mnCopyLocationClick(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) then
     CopyLocation;;
end;


procedure TEDIT_EE_LOCALS.mnIncludePretaxClick(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) then
      CopyPretax;
end;

initialization
  RegisterClass(TEDIT_EE_LOCALS);

end.



