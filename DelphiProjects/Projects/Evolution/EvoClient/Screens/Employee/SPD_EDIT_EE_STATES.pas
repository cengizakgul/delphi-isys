// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_STATES;

interface

uses
   SDataStructure, SPD_EDIT_EE_BASE, StdCtrls, ExtCtrls,
  DBCtrls, Wwdotdot, Wwdbcomb, wwdblook, Mask, wwdbedit, DBActns, Classes,
  ActnList, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls,  SPackageEntry, SysUtils, EvConsts,
  EvSecElement, EvTypes, Variants, Menus, SDDClasses,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDataDictsystem,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel, Forms,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;


type
  TEDIT_EE_STATES = class(TEDIT_EE_BASE)
    tshtStates: TTabSheet;
    tshtTax_Overrides: TTabSheet;
    EEStatesDS: TevClientDataSet;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    SYStatesSrc: TevDataSource;
    SYMaritalSrc: TevDataSource;
    evSecElement1: TevSecElement;
    pmOverrideStateMinWage: TevPopupMenu;
    StateMinWageCopyTo: TMenuItem;
    ScrollBox1: TScrollBox;
    pnlListofStates: TisUIFashionPanel;
    lablHome_State: TevLabel;
    wwlcHome_State: TevDBLookupCombo;
    wwdgStates: TevDBGrid;
    pnlStateInformation: TisUIFashionPanel;
    lablState: TevLabel;
    lablMarital_Status: TevLabel;
    lablDependants_Group_Marital_Status: TevLabel;
    lablSDI: TevLabel;
    lablSUI: TevLabel;
    lablReciprocal_Method: TevLabel;
    lablReciprocal_State: TevLabel;
    lablReciprocal_Amount_Percentage: TevLabel;
    lablCounty: TevLabel;
    lablTaxCode: TevLabel;
    evLabel2: TevLabel;
    dedtDependants_Group_Marital_Status: TevDBEdit;
    dedtReciprocal_Amount_Percentage: TevDBEdit;
    wwlcState: TevDBLookupCombo;
    wwlcSDI: TevDBLookupCombo;
    wwlcSUI: TevDBLookupCombo;
    wwlcReciprocal_State: TevDBLookupCombo;
    wwcbReciprocal_Method: TevDBComboBox;
    wwlcMarital_Status: TevDBLookupCombo;
    dedtCounty: TevDBEdit;
    dedtTaxCode: TevDBEdit;
    evDBRadioGroup1: TevDBRadioGroup;
    edSalaryType: TevDBComboBox;
    sbOverrides: TScrollBox;
    fpOverrides: TisUIFashionPanel;
    lablOverride_State_Tax_Type: TevLabel;
    wwdbOverride_State_Tax_Type: TevDBComboBox;
    pfSettings: TisUIFashionPanel;
    evLabel7: TevLabel;
    drgpEE_SDI_Exempt_Exclude: TevDBRadioGroup;
    drgpEE_SUI_Exempt_Exclude: TevDBRadioGroup;
    drgpER_SDI_Exempt_Exclude: TevDBRadioGroup;
    drgpER_SUI_Exempt_Exclude: TevDBRadioGroup;
    drgpState_Exempt_Exclude: TevDBRadioGroup;
    lablOverride_State_Tax_Value: TevLabel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    dedtOverride_State_Tax_Value: TevDBEdit;
    fp1099: TisUIFashionPanel;
    evDBRadioGroup2: TevDBRadioGroup;
    lablSocCode: TevLabel;
    dedtSocCode: TevDBEdit;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure PageControl1Change(Sender: TObject);
    procedure drgpState_Exempt_ExcludeChange(Sender: TObject);
    procedure wwlcStateCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcHome_StateCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure StateMinWageCopyToClick(Sender: TObject);
    procedure wwlcStateChange(Sender: TObject);
    procedure wwdgStatesDblClick(Sender: TObject);
    procedure wwlcMarital_StatusBeforeDropDown(Sender: TObject);
    procedure wwlcMarital_StatusCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  private
    procedure CopyEEStateFieldTo(aFieldCaption, aFieldName: string);
  protected
    procedure SetReadOnly(Value: Boolean); Override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Deactivate; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    function CanClose: Boolean; override;
    procedure DoBeforeCommit; override;    
  end;

implementation

{$R *.DFM}
uses Dialogs,windows, EvUtils, SSecurityInterface, SPD_EmployeeChoiceQuery, EvContext;

procedure TEDIT_EE_STATES.wwdsDetailDataChange(Sender: TObject; Field: TField);
begin
  if DM_COMPANY.CO_STATES.Active and DM_SYSTEM_STATE.SY_STATES.Active then
  begin
    DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR',
      wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Value, 'STATE'), []);
  end;
//  if DM_EMPLOYEE.EE_STATES.RetrieveCondition <> '' then
//    EEStatesDS.CloneCursor(DM_EMPLOYEE.EE_STATES, False);
  if EEStatesDS.Active then
    EEStatesDS.RetrieveCondition := DM_EMPLOYEE.EE_STATES.RetrieveCondition;
end;

procedure TEDIT_EE_STATES.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  case Kind of
  NavInsert:
  begin
     wwlcState.Enabled := True;
     wwlcSDI.Enabled   := True;
  end;
  NavOk:
  begin
     if wwlcSUI.Text <> '' then
     begin
       wwlcState.Enabled := False;
       wwlcState.Enabled := False;
//           wwlcSDI.Enabled   := False;
     end
     else
       raise EUpdateError.CreateHelp('SUI cannot be left blank!', IDH_ConsistencyViolation);
  end;
  NavCancel:
  begin
     wwlcState.Enabled := False;
//         wwlcSDI.Enabled   := False;
  end;
  NavRefresh:
  begin
    EEStatesDS.RetrieveCondition := '';
    EEStatesDS.Close;
  end;
  end;
end;

procedure TEDIT_EE_STATES.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort] then
     ApplySecurity;
end;

function TEDIT_EE_STATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_STATES;
end;

procedure TEDIT_EE_STATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE, EditDataSets);
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
end;

procedure TEDIT_EE_STATES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDSWithCheck(DM_COMPANY.CO_STATES, aDS, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  EEStatesDS.RetrieveCondition := '';
  EEStatesDS.Close;
  DM_EMPLOYEE.EE_STATES.Close;
  inherited;
end;

function TEDIT_EE_STATES.GetInsertControl: TWinControl;
begin
  Result := wwlcState;
end;

procedure TEDIT_EE_STATES.PageControl1Change(Sender: TObject);
begin
  inherited;
  if not EEStatesDS.Active or (EEStatesDS.RecordCount = 0) then
    wwlcHome_State.Text := '';

  if wwlcState.Text <> '' then
    wwlcState.Enabled := False
  else
    wwlcState.Enabled := True;

  //GUI 2.0 FIXED COLUMN WIDTHS
  wwdgStates.Columns[0].DisplayWidth := 17;
  wwdgStates.Columns[1].DisplayWidth := 28;
  wwdgStates.Columns[2].DisplayWidth := 21;
end;

procedure TEDIT_EE_STATES.SetReadOnly(Value: Boolean);
begin
  inherited;
  if wwlcState.Text <> '' then
    wwlcState.Enabled := False
  else
    wwlcState.Enabled := True;
end;

procedure TEDIT_EE_STATES.drgpState_Exempt_ExcludeChange(Sender: TObject);
var
  OldVal: string;
begin
  inherited;
//SEG BEGIN
  with (Sender as TevDBRadioGroup) do
    if Assigned(Field) and Assigned(Field.DataSet) and
       (Field.DataSet.State in [dsEdit]) then
      begin
        OldVal:=VarToStr(Field.Value);
        if (DM_EMPLOYEE.EE['EXEMPT_EXCLUDE_EE_FED'] = GROUP_BOX_EXEMPT) and
          (Value <> GROUP_BOX_EXEMPT) and (OldVal = GROUP_BOX_EXEMPT) then
          if EvMessage('Do You Want to Change the Tax Status for this 1099?',
             mtConfirmation, [mbYes, mbNo]) <> mrYes then
            Field.DataSet.Cancel;
          end;
//SEG END
end;

//begin gdy

function TEDIT_EE_STATES.CanClose: Boolean;
begin
  Result := inherited CanClose;
  if Result and assigned(wwdsSubMaster.DataSet) and assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
    if (wwdsDetail.DataSet.Recordcount > 0) and (trim(wwlcHome_State.Text)=''){ugly}{( wwdsSubMaster.DataSet.FieldByNAme('HOME_TAX_EE_STATES_NBR').IsNull or (trim(wwdsSubMaster.DataSet.FieldByNAme('HOME_TAX_EE_STATES_NBR').AsString) = '')) } then
    begin
      Result := GetIfReadOnly;
      EvMessage('You should select Home State');
    end;
end;

//end gdy

procedure TEDIT_EE_STATES.wwlcStateCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and (wwdsDetail.DataSet.State = dsInsert) and (wwlcState.Value <> '') then
  begin
    DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.DataRequired('SY_STATES_NBR='+DM_COMPANY.CO_STATES.FieldByName('SY_STATES_NBR').AsString);
    if DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RecordCount = 1 then
    begin
       wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Assign(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE'));
       wwdsDetail.DataSet.FieldByName('SY_STATE_MARITAL_STATUS_NBR').Assign(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.FieldByName('SY_STATE_MARITAL_STATUS_NBR'));
    end;
  end;

  // Disallow for all users to select inactive states; Reso#49434, Andrew.
  if (wwdsDetail.DataSet.State = dsInsert) and (wwlcState.Value <> '') then
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', wwlcState.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        wwlcState.Clear;
        DM_CLIENT.EE_STATES['CO_STATES_NBR'] := Null;
      end;
  end;
end;

constructor TEDIT_EE_STATES.Create(AOwner: TComponent);
begin
  inherited;
  evSecElement1.AtomComponent := nil;
end;

procedure TEDIT_EE_STATES.AfterDataSetsReopen;
begin
  inherited;
  EEStatesDS.CloneCursor(DM_EMPLOYEE.EE_STATES, True);
  EEStatesDS.RetrieveCondition := DM_EMPLOYEE.EE_STATES.RetrieveCondition;
end;

procedure TEDIT_EE_STATES.Deactivate;
begin
  EEStatesDS.Close;
  inherited;
end;

procedure TEDIT_EE_STATES.wwlcHome_StateCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (modified) and (wwlcHome_State.Value = 'PR') then
  begin
    DM_EMPLOYEE.EE.Edit;
    DM_EMPLOYEE.EE.W2_TYPE.AsString := W2_TYPE_PUERTO_RICO;
  end;
end;

procedure TEDIT_EE_STATES.CopyEEStateFieldTo(aFieldCaption,
  aFieldName: string);
var
  k: integer;
  sEEnbr, sCoStateNbr: variant;
  lFieldValue: variant;
begin
  sEEnbr := DM_CLIENT.EE_STATES.FieldByName('EE_NBR').Value;
  sCoStateNbr := DM_CLIENT.EE_STATES.FieldByName('CO_STATES_NBR').Value;
  lFieldValue := DM_CLIENT.EE_STATES.FieldByName( aFieldName ).Value;
  if DM_CLIENT.EE_STATES.FieldByName('EE_NBR').AsString <> '' then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy the '+ aFieldCaption +' for all employees. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_CLIENT.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);
            if DM_CLIENT.EE_STATES.Locate('CO_STATES_NBR', sCoStateNbr, []) then
            begin
              DM_CLIENT.EE_STATES.Edit;
              DM_CLIENT.EE_STATES[ aFieldName ] := lFieldValue;
              DM_CLIENT.EE_STATES.Post;
            end;
          end;
        finally
          ctx_EndWait;
          DM_CLIENT.EE.Locate('EE_NBR',sEEnbr, []);
          DM_CLIENT.EE_STATES.Locate('CO_STATES_NBR', sCoStateNbr, [loCaseInsensitive]);
        end;
      end;
    finally
      Free;
    end;
  end;
end;

procedure TEDIT_EE_STATES.StateMinWageCopyToClick(Sender: TObject);
begin
  inherited;
  CopyEEStateFieldTo('Override State Minimum Wage', 'OVERRIDE_STATE_MINIMUM_WAGE');
end;

procedure TEDIT_EE_STATES.wwlcStateChange(Sender: TObject);
var
  COStateNBR: Integer;
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsInsert) and
     (not wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').IsNull)
  then begin
    COStateNBR := DM_CLIENT.CO_STATES.CO_STATES_NBR.Value;
    wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').clear;
    wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').value := COStateNBR;
  end;
end;

procedure TEDIT_EE_STATES.wwdgStatesDblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 2;
end;


procedure TEDIT_EE_STATES.DoBeforeCommit;
begin
  inherited;
  (Owner as TFramePackageTmpl).SetPostingDataSetLast(DM_EMPLOYEE.EE);
end;

procedure TEDIT_EE_STATES.wwlcMarital_StatusBeforeDropDown(
  Sender: TObject);
begin
  inherited;
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFilter := 'ACTIVE_STATUS = ''Y'' ';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFiltered := true;
end;

procedure TEDIT_EE_STATES.wwlcMarital_StatusCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFilter := '';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFiltered := false;
end;

initialization
  classes.RegisterClass(TEDIT_EE_STATES);

end.
