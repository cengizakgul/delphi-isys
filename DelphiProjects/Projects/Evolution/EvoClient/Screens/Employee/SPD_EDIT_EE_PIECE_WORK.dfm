inherited EDIT_EE_PIECE_WORK: TEDIT_EE_PIECE_WORK
  inherited PageControl1: TevPageControl
    HelpContext = 41001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 104
                IniAttributes.SectionName = 'TEDIT_EE_PIECE_WORK\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 104
                IniAttributes.SectionName = 'TEDIT_EE_PIECE_WORK\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtPiece_Work: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPiecework: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 149
        Align = alClient
        TabOrder = 0
        object pnlList: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 499
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgPiece_Work: TevDBGrid
            Left = 12
            Top = 35
            Width = 463
            Height = 145
            HelpContext = 41001
            DisableThemesInTitle = False
            Selected.Strings = (
              'NAME'#9'48'#9'Name'#9'F'
              'RATE_QUANTITY'#9'10'#9'Rate Quantity'#9'F'
              'RATE_AMOUNT'#9'10'#9'Rate Amount'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_PIECE_WORK\wwdgPiece_Work'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object pnlInfo: TisUIFashionPanel
          Left = 8
          Top = 219
          Width = 499
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlInfo'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Piece Work Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPiece: TevLabel
            Left = 12
            Top = 35
            Width = 33
            Height = 13
            Caption = '~Piece'
            FocusControl = wwlcPiece
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablRate_Quantity: TevLabel
            Left = 270
            Top = 35
            Width = 65
            Height = 13
            Caption = 'Rate Quantity'
            FocusControl = dedtRate_Quantity
          end
          object lablRate_Amount: TevLabel
            Left = 379
            Top = 35
            Width = 62
            Height = 13
            Caption = 'Rate Amount'
            FocusControl = dedtRate_Amount
          end
          object dedtRate_Quantity: TevDBEdit
            Left = 270
            Top = 50
            Width = 100
            Height = 21
            HelpContext = 41001
            DataField = 'RATE_QUANTITY'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object dedtRate_Amount: TevDBEdit
            Left = 379
            Top = 50
            Width = 98
            Height = 21
            HelpContext = 41001
            DataField = 'RATE_AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '[-]*12[#][.*6[#]]'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwlcPiece: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 250
            Height = 21
            HelpContext = 41001
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME'
              'DEFAULT_RATE'#9'10'#9'DEFAULT_RATE'
              'DEFAULT_QUANTITY'#9'10'#9'DEFAULT_QUANTITY')
            DataField = 'CL_PIECES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_PIECES.CL_PIECES
            LookupField = 'CL_PIECES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = wwlcPieceCloseUp
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_PIECE_WORK.EE_PIECE_WORK
  end
end
