inherited AskRatesUpdateParams: TAskRatesUpdateParams
  Left = 527
  Top = 201
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Update Rates or Salaries'
  ClientHeight = 470
  ClientWidth = 327
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Width = 327
    Height = 419
    object evGroupBox1: TevGroupBox
      Left = 8
      Top = 170
      Width = 313
      Height = 157
      Caption = 'Mode'
      TabOrder = 1
      object evLabel3: TevLabel
        Left = 173
        Top = 21
        Width = 6
        Height = 13
        Alignment = taRightJustify
        Caption = '$'
      end
      object evLabel4: TevLabel
        Left = 172
        Top = 63
        Width = 8
        Height = 13
        Alignment = taRightJustify
        Caption = '%'
      end
      object evLabel5: TevLabel
        Left = 172
        Top = 118
        Width = 6
        Height = 13
        Alignment = taRightJustify
        Caption = '$'
      end
      object rbOnAmount: TevRadioButton
        Left = 8
        Top = 20
        Width = 153
        Height = 29
        Caption = 'Change by a Fixed Amount (e.g. add .50)'
        Checked = True
        TabOrder = 0
        TabStop = True
        WordWrap = True
        OnClick = rbOnPercentClick
      end
      object rbOnPercent: TevRadioButton
        Left = 8
        Top = 60
        Width = 129
        Height = 29
        Caption = 'Change by a Percent   (e.g. 10)'
        TabOrder = 2
        TabStop = True
        WordWrap = True
        OnClick = rbOnPercentClick
      end
      object edOnAmount: TevEdit
        Left = 184
        Top = 18
        Width = 113
        Height = 21
        TabOrder = 1
        OnChange = edOnAmountChange
        OnClick = edOnAmountChange
      end
      object edOnPercent: TevEdit
        Left = 184
        Top = 58
        Width = 113
        Height = 21
        TabOrder = 3
        OnChange = edOnPercentChange
        OnClick = edOnPercentChange
      end
      object rbToAmount: TevRadioButton
        Left = 8
        Top = 116
        Width = 145
        Height = 29
        Caption = 'Change to an Amount (e.g. change to 6.15)'
        TabOrder = 4
        TabStop = True
        WordWrap = True
        OnClick = rbOnPercentClick
      end
      object edToAmount: TevEdit
        Left = 184
        Top = 114
        Width = 113
        Height = 21
        TabOrder = 5
        OnChange = edToAmountChange
        OnClick = edToAmountChange
      end
      object evcbDecimal: TevCheckBox
        Left = 64
        Top = 88
        Width = 153
        Height = 17
        Caption = 'Round to 2 decimal places'
        Enabled = False
        TabOrder = 6
      end
    end
    object evGroupBox2: TevGroupBox
      Left = 8
      Top = 99
      Width = 313
      Height = 62
      Caption = 'Wage Range to Be Modified'
      TabOrder = 0
      object evLabel1: TevLabel
        Left = 8
        Top = 32
        Width = 29
        Height = 13
        Caption = 'from $'
      end
      object evLabel2: TevLabel
        Left = 160
        Top = 32
        Width = 18
        Height = 13
        Caption = 'to $'
      end
      object edRangeLow: TevEdit
        Left = 40
        Top = 24
        Width = 113
        Height = 21
        TabOrder = 0
      end
      object edRangeHigh: TevEdit
        Left = 184
        Top = 24
        Width = 113
        Height = 21
        TabOrder = 1
      end
    end
    object evGroupBox3: TevGroupBox
      Left = 8
      Top = 8
      Width = 313
      Height = 84
      Caption = 'Update'
      TabOrder = 2
      object rbSalaries: TevRadioButton
        Left = 8
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Salaries'
        TabOrder = 0
      end
      object rbRates: TevRadioButton
        Left = 8
        Top = 39
        Width = 113
        Height = 17
        Caption = 'Rates'
        Checked = True
        TabOrder = 1
        TabStop = True
      end
      object cbHourlyOnly: TevCheckBox
        Left = 27
        Top = 59
        Width = 175
        Height = 17
        Action = HourlyOnly
        TabOrder = 2
      end
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 333
      Width = 313
      Height = 81
      Caption = 'NOTE'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      object StaticText1: TStaticText
        Left = 8
        Top = 15
        Width = 297
        Height = 58
        AutoSize = False
        Caption = 
          'If a waiting payroll has been preprocessed and check lines have ' +
          'been distributed via Auto Labor Distribution, those check lines ' +
          'will not be updated with the new rate.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  inherited evPanel2: TevPanel
    Top = 431
    Width = 327
    inherited evPanel4: TevPanel
      Left = 110
      Width = 217
      inherited evBitBtn2: TevBitBtn
        Left = 0
        Width = 121
        Caption = 'Select Employees'
        ModalResult = 1
      end
      inherited evBitBtn1: TevBitBtn
        Left = 128
      end
    end
  end
  inherited evPanel3: TevPanel
    Top = 419
    Width = 327
    inherited EvBevel1: TEvBevel
      Width = 310
    end
  end
  inherited evActionList1: TevActionList
    Left = 224
    Top = 48
    inherited OK: TAction
      OnUpdate = OKUpdate
    end
    object HourlyOnly: TAction
      AutoCheck = True
      Caption = 'For Hourly Employees Only'
      OnUpdate = HourlyOnlyUpdate
    end
  end
end
