// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_SCHEDULED_E_DS;

interface

uses
   SDataStructure, SPD_EDIT_EE_BASE, Menus, DBCtrls, ExtCtrls,
  wwdbdatetimepicker, wwdbedit, Mask, Wwdotdot, Wwdbcomb, StdCtrls,
  wwdblook, DBActns, Classes, ActnList, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Controls, SysUtils, SPD_EDIT_COPY,
   Wwdbspin, SPackageEntry, EvTypes, EvConsts, SFieldCodeValues,
  Graphics, Dialogs, messages, Variants, Forms, SPD_HideRecordHelper,
  SPD_AskEDUpdateParamsForAmount, kbmMemTable, SEmployeeScr,
  ISKbmMemDataSet, ISBasicClasses, SDDClasses, typinfo,
  EvDataAccessComponents, ISDataAccessComponents, SPD_MiniNavigationFrame,
  SDataDictclient, SDataDicttemp, EvBasicUtils, DateUtils, EvUtils, EvExceptions,
  EvUIUtils, EvUIComponents, EvClientDataSet, isUIEdit,
  isUIwwDBDateTimePicker, isUIwwDBEdit, isUIwwDBComboBox, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvCommonInterfaces, EvDataset;

type
  TSetOfChar = set of char;
  TUpdateEDEvent = procedure of object;

  TEDIT_EE_SCHEDULED_E_DS = class(TEDIT_EE_BASE)
    tshtBrowse_Scheduled_E_DS: TTabSheet;
    tshtDetail_1: TTabSheet;
    tshtDetail_2: TTabSheet;
    Label3: TevLabel;
    Label4: TevLabel;
    DBText5: TevDBText;
    DBText6: TevDBText;
    PopupMenu1: TevPopupMenu;
    Copy1: TMenuItem;
    ChildSupportSrc: TevDataSource;
    DDSrc: TevDataSource;
    UpdateAgencies1: TMenuItem;
    UpdateEDs1: TMenuItem;
    evLabel8: TevLabel;
    evDBText1: TevDBText;
    dsCL_E_DS: TevDataSource;
    HideRecordHelper: THideRecordHelper;
    FocusMain1: TMenuItem;
    UpdatePercent1: TMenuItem;
    evdsCLBenefitSubtype: TevDataSource;
    tshtDirectDeposits: TTabSheet;
    mnavDirectDeposit: TMiniNavigationFrame;
    tshtChildSupport: TTabSheet;
    dsCLBenefits: TevDataSource;
    sbBrowseEDs: TScrollBox;
    pnlBrowseEDBody: TevPanel;
    pnlFashionBrowseEDs: TisUIFashionPanel;
    pnlBrowseAlign: TevPanel;
    sbDetails1: TScrollBox;
    pnlEDInfo: TisUIFashionPanel;
    lablE_D: TevLabel;
    lablPercentage: TevLabel;
    EDLabel: TevLabel;
    wwlcE_D: TevDBLookupCombo;
    fpDestinationDetails: TisUIFashionPanel;
    labl_Child_Support: TevLabel;
    labl_Garnishment_ID: TevLabel;
    wwlcChildSupport: TevDBLookupCombo;
    dedt_Garnishment_ID: TevDBEdit;
    pnlActions: TisUIFashionPanel;
    pnlMinimums: TisUIFashionPanel;
    pnlThreshholds: TisUIFashionPanel;
    lablTarget_Action: TevLabel;
    lablNumber_Targets_Remaining: TevLabel;
    lablTarget_Amount: TevLabel;
    lablBalance: TevLabel;
    dedtNumber_Targets_Remaining: TevDBEdit;
    dedtTarget_Amount: TevDBEdit;
    wwcbTarget_Action: TevDBComboBox;
    wwdeBalance: TevDBEdit;
    lablMaximum_Garnish_Percentage: TevLabel;
    lablMinimum_Wage_Multiplier: TevLabel;
    dedtMaximum_Garnish_Percentage: TevDBEdit;
    dedtMinimum_Wage_Multiplier: TevDBEdit;
    labl_Threshold_E_D_Group: TevLabel;
    labl_Threshold_Amount: TevLabel;
    dedtThreshold_Amount: TevDBEdit;
    cbUsePensionLimit: TevDBCheckBox;
    sbDD: TScrollBox;
    fpDirectDepositSummary: TisUIFashionPanel;
    wwdgDirect_Deposit: TevDBGrid;
    fpDirectDepositDetail: TisUIFashionPanel;
    lablABA_Number: TevLabel;
    Label1: TevLabel;
    lablBranchIdentifier: TevLabel;
    lablBank_Account_Nbr: TevLabel;
    lablBank_Acct_Type: TevLabel;
    dedtABA_Number: TevDBEdit;
    DBEdit1: TevDBEdit;
    dedtBranchIdentifier: TevDBEdit;
    dedtBank_Account_Nbr: TevDBEdit;
    wwDBComboBox1: TevDBComboBox;
    drgpIn_Prenote: TevDBRadioGroup;
    drgpForm_on_File: TevDBRadioGroup;
    pnlListOfAgencies: TisUIFashionPanel;
    pnlAgencyInfo: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    lablPriority_Number: TevLabel;
    lablCase_Number: TevLabel;
    lablOrigination_State: TevLabel;
    lablAgency_Number: TevLabel;
    lablFIPS_code: TevLabel;
    dedtPriority_Number: TevDBEdit;
    dedtCase_Number: TevDBEdit;
    dedtOrigination_State: TevDBEdit;
    evDBLookupCombo1: TevDBLookupCombo;
    drgpARREARS: TevDBRadioGroup;
    rgILElig: TevDBRadioGroup;
    dedtFIPS_code: TevDBEdit;
    dedtCustom_Field2: TevDBEdit;
    sbChildSupport: TScrollBox;
    fpCalculation: TisUIFashionPanel;
    lablCalculation_Type: TevLabel;
    lablAmount: TevLabel;
    wwcbCalculation_Type: TevDBComboBox;
    dedtAmount: TevDBEdit;
    dedtPercentage: TevDBEdit;
    pnlBenefitInfo: TPanel;
    edcAMOUNT: TevEdit;
    edcPERCENTAGE: TevEdit;
    fpFrequency: TisUIFashionPanel;
    lablFrequency: TevLabel;
    evLabel4: TevLabel;
    evLabel9: TevLabel;
    wwcbFrequency: TevDBComboBox;
    evDBComboBox1: TevDBComboBox;
    evDBComboBox4: TevDBComboBox;
    lablEffective_Start_Date: TevLabel;
    lablEffective_End_Date: TevLabel;
    dEffectiveStartDate: TevDBDateTimePicker;
    dEffectiveEndDate: TevDBDateTimePicker;
    lablE_D_Group: TevLabel;
    wwlcE_D_Group: TevDBLookupCombo;
    lablAgency: TevLabel;
    wwlcAgency: TevDBLookupCombo;
    labl_EE_direct_Deposit: TevLabel;
    lablTake_Home_Pay: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
    dedtTake_Home_Pay: TevDBEdit;
    lblPercentage: TevLabel;
    evLabel5: TevLabel;
    evDBComboBox2: TevDBComboBox;
    evDBRadioGroup1: TevDBRadioGroup;
    lablScheduledEDGroup: TevLabel;
    lablPriority: TevLabel;
    wwlcScheduledEDGroup: TevDBLookupCombo;
    dbsePriority: TevDBSpinEdit;
    evLabel7: TevLabel;
    evBtnEEBenefit: TevSpeedButton;
    lablBenefitReference: TevLabel;
    evLabel1: TevLabel;
    evcbEEBenefitReference: TevDBLookupCombo;

    cdsDependentHealthCode: TevClientDataSet;
    cdsDependentHealthCodecustom_e_d_code_number: TStringField;
    cdsDependentHealthCodeCL_E_D_NBR: TIntegerField;
    pnlNYChildSupport: TevPanel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evDBEdit1: TevDBEdit;
    cbDependentHealthCode: TevDBLookupCombo;

    evcbBenefitSubType: TevDBLookupCombo;
    evcbBenefitReference: TevDBComboBox;
    cdEEScheduledView: TevClientDataSet;
    cdEEScheduledViewEE_SCHEDULED_E_DS_NBR: TIntegerField;
    cdEEScheduledViewEE_NBR: TIntegerField;
    cdEEScheduledViewCUSTOM_E_D_CODE_NUMBER: TStringField;
    cdEEScheduledViewDESCRIPTION: TStringField;
    cdEEScheduledViewCALCULATION_TYPE: TStringField;
    cdEEScheduledViewAMOUNT: TCurrencyField;
    cdEEScheduledViewPERCENTAGE: TCurrencyField;
    cdEEScheduledViewFREQUENCY: TStringField;
    cdEEScheduledViewEFFECTIVE_START_DATE: TDateTimeField;
    cdEEScheduledViewEFFECTIVE_END_DATE: TDateTimeField;
    dsEEScheduledView: TevDataSource;
    grdEESchedView: TevDBGrid;
    cdEEBenefits: TevClientDataSet;
    cdEEBenefitsEE_BENEFITS_NBR: TIntegerField;
    cdEEBenefitsBenefit: TStringField;
    cdEEBenefitsBenefitAmountType: TStringField;
    cdEEBenefitsTOTAL_PREMIUM_AMOUNT: TCurrencyField;
    cdEEBenefitsEE_RATE: TCurrencyField;
    cdEEBenefitsER_RATE: TCurrencyField;
    cdEEBenefitsCOBRA_RATE: TCurrencyField;
    cdEEBenefitsBENEFIT_EFFECTIVE_DATE: TDateTimeField;
    cdEEBenefitsEXPIRATION_DATE: TDateTimeField;
    cdEEBenefitsReferenceType: TStringField;

    bevelDestination: TEvBevel;
    fpAnnualMax: TisUIFashionPanel;
    fpLimits: TisUIFashionPanel;
    lablMinimum_E_D_Group: TevLabel;
    wwlcMinimum_E_D_Group: TevDBLookupCombo;
    lablMinimum_Pay_Period_Percentage: TevLabel;
    dedtMinimum_Pay_Period_Percentage: TevDBEdit;
    lablMinimum_Pay_Period_Amount: TevLabel;
    dedtMinimum_Pay_Period_Amount: TevDBEdit;
    bvlLimits: TEvBevel;
    fpMaxAvg: TisUIFashionPanel;
    lablAnnual_Maximum_Amount: TevLabel;
    lablClient_Annual_Maximum_Amount: TevLabel;
    dedtAnnual_Maximum_Amount: TevDBEdit;
    dedtClient_Annual_Maximum_Amount: TevDBEdit;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel6: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBEdit3: TevDBEdit;
    lablMaximum_Pay_Period_Percentage: TevLabel;
    lablMaximum_Pay_Period_Amount: TevLabel;
    lablMaximum_E_D_Group: TevLabel;
    dedtMaximum_Pay_Period_Percentage: TevDBEdit;
    dedtMaximum_Pay_Period_Amount: TevDBEdit;
    wwlcMaximum_E_D_Group: TevDBLookupCombo;
    sbAdvanced: TScrollBox;
    drgpExclude_Week_1: TevDBRadioGroup;
    drgpExclude_Week_2: TevDBRadioGroup;
    drgpExclude_Week_3: TevDBRadioGroup;
    drgpExclude_Week_4: TevDBRadioGroup;
    drgpExclude_Week_5: TevDBRadioGroup;
    bvFreq: TEvBevel;
    fpNavigation: TisUIFashionPanel;
    evBitBtn1: TevBitBtn;
    btnEmployeeBenefit: TevBitBtn;
    EvBevel1: TEvBevel;
    mnavChildSupport: TMiniNavigationFrame;
    wwlcThreshold_E_D_Group: TevDBLookupCombo;
    evrgShowinEP: TevDBRadioGroup;
    rbReadOnlyRemotes: TevDBRadioGroup;
    rbAllowHyphens: TevDBRadioGroup;
    cbFundingCode: TevDBLookupCombo;
    cbExcessCode: TevDBLookupCombo;

    procedure Copy1Click(Sender: TObject);
    procedure wwlcE_DCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcE_DChange(Sender: TObject);
    procedure UpdateAgencies1Click(Sender: TObject);
    procedure UpdateEDs1Click(Sender: TObject);
    procedure wwlcScheduledEDGroupChange(Sender: TObject);
    procedure wwlcMinimum_E_D_GroupChange(Sender: TObject);
    procedure wwlcMaximum_E_D_GroupChange(Sender: TObject);
    procedure wwcbTarget_ActionChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure dedtMinimum_Wage_MultiplierChange(Sender: TObject);
    procedure dedtMaximum_Garnish_PercentageChange(Sender: TObject);
    procedure wwcbCalculation_TypeChange(Sender: TObject);
    procedure dedtTarget_AmountChange(Sender: TObject);
    procedure DBRadioGroup1Change(Sender: TObject);
    procedure dedtMinimum_Pay_Period_PercentageChange(Sender: TObject);
    procedure dedtMaximum_Pay_Period_PercentageChange(Sender: TObject);
    procedure wwlcThreshold_E_D_GroupChange(Sender: TObject);
    procedure dedtThreshold_AmountChange(Sender: TObject);
    procedure wwcbCalculation_TypeCloseUp(Sender: TwwDBComboBox;
      Select: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure evcbBenefitAmountTypeChange(Sender: TObject);
    procedure UpdateAmountExecute(Sender: TObject);
    procedure UpdatePercentExecute(Sender: TObject);
    procedure evcbBenefitSubTypeChange(Sender: TObject);
    procedure dsCL_E_DSDataChange(Sender: TObject; Field: TField);
    procedure dsCL_E_DSDataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure dsCLBenefitsDataChange(Sender: TObject; Field: TField);
    procedure mnavDirectDepositInsertRecordExecute(Sender: TObject);
    procedure DDSrcUpdateData(Sender: TObject);
    procedure mnavDirectDepositSpeedButton2Click(Sender: TObject);
    procedure mnavChildSupportSpeedButton1Click(Sender: TObject);
    procedure mnavChildSupportSpeedButton2Click(Sender: TObject);
    procedure rgILEligChange(Sender: TObject);
    procedure DDSrcDataChange(Sender: TObject; Field: TField);
    procedure ChildSupportSrcDataChange(Sender: TObject; Field: TField);
    procedure evBitBtn1Click(Sender: TObject);
    procedure dedtBank_Account_NbrChange(Sender: TObject);
    procedure cbUsePensionLimitClick(Sender: TObject);
    procedure tshtDirectDepositsShow(Sender: TObject);
    procedure tshtChildSupportShow(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure wwDBGrid4RowChanged(Sender: TObject);
    procedure ResultFIELDS_EE_BANK_Function;
    procedure PageControl1Change(Sender: TObject);
    procedure dEffectiveStartDateChange(Sender: TObject);
    procedure dEffectiveStartDateExit(Sender: TObject);
    procedure dEffectiveStartDateKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnEmployeeBenefitClick(Sender: TObject);
    procedure wwdsEmployeeDataChange(Sender: TObject; Field: TField);
    procedure evBtnEEBenefitClick(Sender: TObject);
    procedure evcbBenefitReferenceCloseUp(Sender: TwwDBComboBox;
      Select: Boolean);
    procedure evcbBenefitReferenceChange(Sender: TObject);
    procedure pnlFashionBrowseEDsResize(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure dsEEScheduledViewDataChange(Sender: TObject; Field: TField);
    procedure grdEESchedViewTitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure grdEESchedViewDblClick(Sender: TObject);
    procedure edcAMOUNTExit(Sender: TObject);
    procedure HideRecordHelperbutnNextClick(Sender: TObject);
  private
    FFilteredDS : IevDataSet;
    csCobenefit : IevDataSetHolder;
    FCalcTypeCodes: TSetOfChar;
    FBenefitAmountTypeCodes: TSetOfChar;
    FActivated: boolean;
    FWasInsert: boolean;
    FWasEdit: boolean;
    FWasNewDD: Boolean;
    FWasNewCS: Boolean;
    FTmpEE: integer;
    RequiredList: TStringList;

    // used by Update*Execute and HandleUpdateEDField only
    FValueToUpdateTo: Variant;
    FFieldToUpdate: string;
    FidxBenefitReference : integer;


    // used by HandleUpdateEDAmount and UpdateAmountExecute only
    FAmountUpdateMode: TEDAmountUpdateMode;
    FAmount: Variant;
    FAmountChangePercent: Variant;
    FAfterPost: TDataSetNotifyEvent;

    procedure HandleUpdateEDPercentage;
    procedure HandleUpdateEDAmount;
    procedure ScreenStateChanged(fname: string; aDeductWholeCheck: string);
    procedure CheckRequiredFields;
    procedure CheckConsistency;
    procedure AddNewEDForPension;
    procedure CheckRequired(aRequired: boolean; aFieldName: string; aLabel: TevLabel);
    function CanUpdateED: boolean;

    //MR added function for reso# 37818
    function CalculateEDStartDate: TDate;
    function CheckMissedCSDD: boolean;

    procedure CalcEffectiveEndDate;
    Procedure detailBenefitAmount;
    Procedure SetBenefitReference;
    procedure LoadCOBenefits;

    Procedure FillcdEEScheduledView(const EEScheduledNbr:integer);
    Procedure FillcdEEBenefits;
    procedure FindBenefitAmount(const cdEEScheduled: TevClientDataSet; var amount: Variant; var RateType: String);
    procedure setReadOnlyRemotes;
    function CanHideOrUnhideSchedEE( DataSet: TevClientDataSet): boolean;
    function MayHideOrUnhideSchedEE(DataSet: TevClientDataSet): boolean;
  protected
    function GetDataSetConditions(sName: string): string; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;

    procedure AfterPost(Dataset: TDataset);
    procedure BeforeScroll(Dataset: TDataset);

  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function CanClose: boolean; override;
  end;

implementation

uses
  windows, EvContext,
  SFrameEntry,
  SPD_AskGlobalUpdateParamsBase, SPD_AskEDUpdateParamsForPercent,
  SPD_EDIT_Open_BASE, SPD_EDFields, SPD_SCEDFields;
{$R *.DFM}

const
  currentfilter = 'SCHEDULED_E_D_ENABLED like ''%Y%''';

procedure TEDIT_EE_SCHEDULED_E_DS.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;
  if AContext = 1 then
  begin
    DM_CLIENT.EE.Locate('EE_NBR', AParams[0], []);
    FillcdEEScheduledView(DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);
    PageControl1.ActivePage := tshtBrowse_Scheduled_E_DS;
  end;
end;

function TEDIT_EE_SCHEDULED_E_DS.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BENEFIT_SUBTYPE') or (sName = 'CO_BENEFIT_RATES') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.Copy1Click(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  i: Integer;
  s: string;
begin
  inherited;
  CopyForm := TEDIT_COPY.Create(Self);
  try
    CopyForm.CopyTo := 'EEScheduledEDS';
    CopyForm.CopyFrom := 'EE';
    CopyForm.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
    CopyForm.EdNbr := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
    s := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
    CopyForm.UpdateEDs := False;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
    CopyForm.ShowModal;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;

    with Owner as TEmployeeFrm do
      for i := 0 to Pred(CopyForm.EdList.Count) do
        EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, CopyForm.EdList[i], s]);
  finally
    CopyForm.Free;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_COMPANY.CO_BENEFITS, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_SUBTYPE, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_EMPLOYEE.EE_DIRECT_DEPOSIT, aDS);
  AddDS(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES, aDS);
end;

function TEDIT_EE_SCHEDULED_E_DS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_SCHEDULED_E_DS;
end;

function TEDIT_EE_SCHEDULED_E_DS.GetInsertControl: TWinControl;
begin
  Result := wwlcE_D;

  if (PageControl1.ActivePage <> tshtDetail_1) then
  begin
     FillcdEEBenefits;
     SetBenefitReference;
  end;

  if wwlcE_D.SecurityRO then
  begin
    SetControlsReccuring(tshtDetail_1, 'SecurityRO', Integer(False));
    SetControlsReccuring(tshtDetail_2, 'SecurityRO', Integer(False));
    SetControlsReccuring(tshtChildSupport, 'SecurityRO', Integer(False));
    SetControlsReccuring(tshtDirectDeposits, 'SecurityRO', Integer(False));
  end;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwlcE_DCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
var

  tmpFilter : String;
  tmpFiltered : Boolean;
  recNo : Integer;
begin
  inherited;
  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    wwdsDetail.DataSet.UpdateRecord;
  ScreenStateChanged( '', '' );
  if Trim(wwlcE_D.Text) <> '' then
  begin
    if Assigned(wwlcE_D.DataSource.DataSet) and
       wwlcE_D.DataSource.DataSet.Active and
       (wwlcE_D.DataSource.DataSet.State in [dsEdit, dsInsert]) and
       DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', wwlcE_D.LookupTable['CL_E_DS_NBR'], []) then
    begin
      if (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwlcE_D.LookupTable['CL_E_DS_NBR'],'E_D_CODE_TYPE') = 'D^') or
        (DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwlcE_D.LookupTable['CL_E_DS_NBR'],'E_D_CODE_TYPE') = 'M0') then
      begin
        DM_CLIENT.EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString := '3';
        DM_CLIENT.EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString := 'L';
      end
      else
      begin
        DM_CLIENT.EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString := 'N';
        DM_CLIENT.EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString := 'A';
      end;
      if DM_CLIENT.CL_E_DS.FieldByName('SCHEDULED_DEFAULTS').AsString = 'Y' then
      begin
        wwlcE_D.DataSource.DataSet.FieldByName('AMOUNT').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_AMOUNT').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('PERCENTAGE').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_RATE').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('FREQUENCY').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_FREQUENCY').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_1').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_EXCLUDE_WEEK_1').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_2').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_EXCLUDE_WEEK_2').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_3').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_EXCLUDE_WEEK_3').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_4').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_EXCLUDE_WEEK_4').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_5').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_EXCLUDE_WEEK_5').Value;

        wwlcE_D.DataSource.DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime := CalculateEDStartDate;

        wwlcE_D.DataSource.DataSet.FieldByName('CALCULATION_TYPE').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_CALCULATION_METHOD').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('EXPRESSION').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_EXPRESSION').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_DS.FieldByName('CL_E_D_GROUPS_NBR').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('CL_AGENCY_NBR').Value := DM_CLIENT.CL_E_DS.FieldByName('DEFAULT_CL_AGENCY_NBR').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('CL_E_DS_NBR').Value := DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').Value;


        wwlcE_D.DataSource.DataSet.FieldByName('WHICH_CHECKS').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_WHICH_CHECKS').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('PLAN_TYPE').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_PLAN_TYPE').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('PRIORITY_NUMBER').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_PRIORITY_NUMBER').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('ALWAYS_PAY').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_ALWAYS_PAY').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('MAX_AVG_AMT_CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_MAX_AVG_AMT_GRP_NBR').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('MAX_AVG_HRS_CL_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_MAX_AVG_HRS_GRP_NBR').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('MAX_AVERAGE_HOURLY_WAGE_RATE').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_MAX_AVERAGE_HOURLY_WAGE_RATE').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('THRESHOLD_E_D_GROUPS_NBR').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_THRESHOLD_E_D_GROUPS_NBR').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('THRESHOLD_AMOUNT').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_THRESHOLD_AMOUNT').Value;

        wwlcE_D.DataSource.DataSet.FieldByName('DEDUCTIONS_TO_ZERO').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_DEDUCTIONS_TO_ZERO').Value;
        wwlcE_D.DataSource.DataSet.FieldByName('USE_PENSION_LIMIT').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_USE_PENSION_LIMIT').Value;

        wwlcE_D.DataSource.DataSet.FieldByName('DEDUCT_WHOLE_CHECK').Value := DM_CLIENT.CL_E_DS.FieldByName('SD_DEDUCT_WHOLE_CHECK').Value;

        tmpFilter := DM_COMPANY.CO_E_D_CODES.Filter;
        tmpFiltered := DM_COMPANY.CO_E_D_CODES.Filtered;
        recNo := DM_COMPANY.CO_E_D_CODES.RecNo;
        try
          DM_COMPANY.CO_E_D_CODES.Filter := 'CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
          DM_COMPANY.CO_E_D_CODES.Filtered := True;
          if DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').Value, []) then
          begin
            if (DM_COMPANY.CO_E_D_CODES.FieldByName('USED_AS_BENEFIT').AsString <> USEDASBENEFIT_EE) or  (not Context.License.HR) then
            begin
               wwlcE_D.DataSource.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value := DM_COMPANY.CO_E_D_CODES.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value;
               SetBenefitReference;
               ScreenStateChanged( '', '' );
            end;
          end;
        finally
          DM_COMPANY.CO_E_D_CODES.Filter := tmpFilter;
          DM_COMPANY.CO_E_D_CODES.Filtered := tmpFiltered;
          DM_COMPANY.CO_E_D_CODES.RecNo := recNo;
        end;

        if not Context.License.HR then
        Begin
          evcbEEBenefitReference.Enabled := False;
          evBtnEEBenefit.Enabled := False;
        end;
      end;
      CalcEffectiveEndDate;
    end;
  end
  else
  begin
    if Assigned(wwlcE_D.DataSource.DataSet) and
    wwlcE_D.DataSource.DataSet.Active and
    (wwlcE_D.DataSource.DataSet.State in [dsEdit, dsInsert]) then
    begin
      wwlcE_D.DataSource.DataSet.FieldByName('AMOUNT').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('FREQUENCY').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_1').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_2').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_3').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_4').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EXCLUDE_WEEK_5').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EFFECTIVE_START_DATE').AsDateTime := Date;
      wwlcE_D.DataSource.DataSet.FieldByName('CALCULATION_TYPE').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('EXPRESSION').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('CL_E_D_GROUPS_NBR').Clear;
      wwlcE_D.DataSource.DataSet.FieldByName('CL_AGENCY_NBR').Clear;
    end;
  end;
end;

//MR added function for reso# 37818
// The ED effective date should be set to the latest Date of whatever specified of:
// Jan 1st of yr when employee turns 50, Date when EE reaches Minimum age, or
// End of EE probation period, or Client ED effective date
function TEDIT_EE_SCHEDULED_E_DS.CalculateEDStartDate: TDate;
var
  aEDCodeType: string;
  dEDStartDate, dBirthDate, dProbationDate: TDate;
  dMinimumAgeDate, d50Date, dClientEDDate: TDate;
  iProbationDays, iMinimumAge: integer;
  bCatchupED, bPensionED: boolean;
  Q: IevQuery;
begin

  // By default set to current date
  dEDStartDate := Date;
  bCatchupED := false;
  bPensionED := false;

  // Set Start Date to later date of current or Client ED Effective date
  dClientEDDate := DM_CLIENT.CL_E_DS.FieldByName('SD_EFFECTIVE_START_DATE').AsDateTime;
  if CompareDate(dClientEDDate, dEDStartDate) > 0 then
     dEDStartDate := dClientEDDate;

  // Check if selected E/D code type is for catchup or pension ED
  if DM_EMPLOYEE.CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', wwlcE_D.Text, []) then
    aEDCodeType := DM_EMPLOYEE.CL_E_DS.FieldByName('E_D_CODE_TYPE').AsString;
  if (aEDCodeType = 'D!') or (aEDCodeType = 'D#') or (aEDCodeType = 'D%') or
     (aEDCodeType = 'D$') or (aEDCodeType = 'D*') or (aEDCodeType = 'D2') or
     (aEDCodeType = 'D@') then
    bCatchupED := true
  else if (aEDCodeType = 'D(') or (aEDCodeType = 'D)') or (aEDCodeType = 'D3') or
     (aEDCodeType = 'D4') or (aEDCodeType = 'DA') or (aEDCodeType = 'DB') or
     (aEDCodeType = 'DC') or (aEDCodeType = 'DD') or (aEDCodeType = 'DE') or
     (aEDCodeType = 'DF') or (aEDCodeType = 'DG') or (aEDCodeType = 'DH') or
     (aEDCodeType = 'DI') or (aEDCodeType = 'DK') then
    bPensionED := true;

  // Check if EE DOB entered
  dBirthDate := DM_EMPLOYEE.CL_PERSON.FieldByName('BIRTH_DATE').AsDateTime;
  if dBirthDate = 0 then
  begin
    // popup a warning message for chatch-up ED's
    if bCatchupED or (aEDCodeType = ED_ST_ROTH_IRA) then
    begin
      Q := TevQuery.Create('select DESCRIPTION from CL_E_DS where E_D_CODE_TYPE='''+aEDCodeType+''' and {AsOfNow<CL_E_DS>}');
      evMessage('The Date of Birth for this employee is blank. '+Q.Result.FieldByName('DESCRIPTION').AsString+' can not be calculated.');
    end;
  end

  // If DOB is specified increase the E/D effective date for catchup or pension ED's
   else if bCatchupED or bPensionED then
  begin
    // Find the Client Pension linked to the Scheduled E/D
    if DM_EMPLOYEE.CL_PENSION.Locate('CL_PENSION_NBR', DM_EMPLOYEE.CL_E_DS.FieldByName('CL_PENSION_NBR').AsInteger, []) then
    begin
      // Compute Date employee reaches minimum age
      iMinimumAge := DM_EMPLOYEE.CL_PENSION.FieldByName('MINIMUM_AGE').AsInteger;
      if iMinimumAge > 0 then
      begin
        dMinimumAgeDate := EncodeDate( GetYear(dBirthDate) + iMinimumAge, GetMonth(dBirthDate), GetDay(dBirthDate));
        // For Pension and Catchup ED's use later date
        if CompareDate(dMinimumAgeDate, dEDStartDate) > 0 then
          dEDStartDate := dMinimumAgeDate;
      end;

      // Compute Date of 1st day after EE's probation period
      iProbationDays := DM_EMPLOYEE.CL_PENSION.FieldByName('PROBATION_PERIOD_DAYS').AsInteger;
      if iProbationDays > 0 then
      begin
        dProbationDate := IncDay(DM_EMPLOYEE.EE.FieldByName('CURRENT_HIRE_DATE').AsDateTime, iProbationDays);
        // For Pension and Catchup ED's use later date
        if CompareDate(dProbationDate, dEDStartDate) > 0 then
          dEDStartDate := dProbationDate;
      end;
    end;

    // For Catchup ED's also take 50 year date into account
    if bCatchupED then
    begin
      // Jan 1st of year when employee reaches 50
      d50Date := EncodeDate( GetYear(dBirthDate) + 50, 1, 1);
      // Use later date
      if CompareDate(d50Date, dEDStartDate) > 0 then
        dEDStartDate := d50Date;
    end;
  end;

  Result := dEDStartDate;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwlcE_DChange(Sender: TObject);
begin
  if not wwlcE_D.Grid.Visible then
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
      wwdsDetail.DataSet.UpdateRecord;

  if Trim(wwlcE_D.Text) <> '' then
  begin
    if wwlcE_D.LookupTable.Active and
       (UpperCase(wwlcE_D.LookupTable.FieldByName('ED_Lookup').AsString) <> UpperCase(Trim(wwlcE_D.Text))) then
      wwlcE_D.LookupTable.Locate('ED_Lookup', Trim(wwlcE_D.Text), [loCaseInsensitive]);
    EDLabel.Caption := wwlcE_D.LookupTable.FieldByName('CodeDescription').AsString;
  end
  else
    EDLabel.Caption := '';
end;

//now duplicates functionality of EDUpdateTemplate -- convert someday?
procedure TEDIT_EE_SCHEDULED_E_DS.UpdateAgencies1Click(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  i: Integer;
  s: string;
  t: TStringList;

  procedure AddFields(Tab: TisUIFashionPanel; clb: TevCheckListBox);
  var
    i: Integer;
  begin
    for i := 0 to Pred(Tab.ControlCount) do
      if (Tab.Controls[i] is TevLabel) and
         Assigned(TevLabel(Tab.Controls[i]).FocusControl) and
         (Tab.Controls[i] <> lablE_D) and
         (Tab.Controls[i] <> labl_Child_Support) and
         (Tab.Controls[i] <> labl_Garnishment_ID) and
         (Tab.Controls[i] <> labl_EE_direct_Deposit) and
         (Tab.Controls[i] <> lablClient_Annual_Maximum_Amount) and
         (Tab.Controls[i] <> lablAmount) and
         (Tab.Controls[i] <> lablPercentage) then
        with TevLabel(Tab.Controls[i]) do
        begin
          clb.Items.Add(StringReplace(Caption, '~', '', [rfReplaceAll]));
          t.Add(GetStrProp(FocusControl, 'DataField'));
        end
      else if Tab.Controls[i] is TevDBRadioGroup then
        with TevDBRadioGroup(Tab.Controls[i]) do
        begin
          clb.Items.Add(StringReplace(Caption, '~', '', [rfReplaceAll]));
          t.Add(DataField);
        end;
  end;
begin
  inherited;
  t := TStringList.Create;
  with TSCEDFields.Create( Self ) do
  try
   //original code when fields resided on the tab sheets
   { AddFields(tshtDetail_1, clbFields);
    AddFields(tshtDetail_2, clbFields);  }
    
    //new code must reference each fashion panel
    AddFields(fpCalculation,clbFields);
    AddFields(fpFrequency,clbFields);
    AddFields(fpDestinationDetails,clbFields);
    AddFields(fpAnnualMax,clbFields);
    AddFields(fpLimits,clbFields);
    AddFields(pnlActions,clbFields);
    AddFields(pnlThreshholds,clbFields);
    AddFields(fpMaxAvg,clbFields);
    AddFields(pnlMinimums,clbFields);

    evStartDate.Date := Now;
    evdrStartDate.Date := Now;
    evdrEndDate.Date := Now;

    if ShowModal = mrOk then
    begin
      FFieldToUpdate := '';
      for i := 0 to Pred(clbFields.Items.Count) do
        if clbFields.Checked[i] then
          FFieldToUpdate := FFieldToUpdate + t[i] + ';';
      Delete(FFieldToUpdate, Length(FFieldToUpdate), 1);

      FValueToUpdateTo := DM_EMPLOYEE.EE_SCHEDULED_E_DS[FFieldToUpdate];

      CopyForm := TEDIT_COPY.Create(Self);
      try
        CopyForm.CopyTo := 'EEScheduledEDS';
        CopyForm.CopyFrom := 'EE';
        CopyForm.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
        CopyForm.EdNbr := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
        s := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
        CopyForm.UpdateEDs := True;
        if not evcbStartDate.Checked then
        begin
          if evrgStartDate.Checked then
             CopyForm.StartDate := evStartDate.Date
          else
          begin
             CopyForm.StartDate := evdrStartDate.Date;
             CopyForm.EndDate := evdrEndDate.Date;
          end;
        end;
        CopyForm.CustomEDFunction := HandleUpdateEDPercentage;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
        CopyForm.ShowModal;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;

        with Self.Owner as TEmployeeFrm do
          for i := 0 to Pred(CopyForm.EdList.Count) do
            EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, CopyForm.EdList[i], s]);
      finally
        CopyForm.Free;
      end;
    end;
  finally
    t.Free;
    Free;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.UpdateEDs1Click(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  i: Integer;
  s: string;
begin
  inherited;
  CopyForm := TEDIT_COPY.Create(Self);
  try
    CopyForm.CopyTo := 'EEScheduledEDS';
    CopyForm.CopyFrom := 'EE';
    CopyForm.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
    CopyForm.EdNbr := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
    s := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
    CopyForm.UpdateEDs := True;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
    CopyForm.ShowModal;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;

    with Owner as TEmployeeFrm do
      for i := 0 to Pred(CopyForm.EdList.Count) do
        EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, CopyForm.EdList[i], s]);
  finally
    CopyForm.Free;
  end;
end;

procedure IPItemsToSet( items: string; var codes: TSetOfChar );
var
  p: integer;
  token: string;
  isLastToken: boolean;
begin
  codes := [];
  p := 1;
  isLastToken := false;
  while not isLastToken do
  begin
    if GetToken( items, p, token ) then
      break;
    isLastToken := GetToken( items, p, token );
    Include( Codes, token[1] );
  end;
end;

function SetToIPItems( const originalItems: string; const codes: TSetOfChar ): string;
var
  p: integer;
  token1, token2: string;
  isLastToken: boolean;
begin
  p := 1;
  Result := '';
  isLastToken := false;
  while not isLastToken do
  begin
    if GetToken( originalItems, p, token1 )  then
      break;
    isLastToken := GetToken( originalItems, p, token2 );
    if token2[1] in codes then
      Result := Result + #13 + token1 + #9 + token2;
  end;
  if Result <> '' then
    Result := copy(Result, 2, length(Result)-1);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.Activate;
begin
  RequiredList :=  TStringList.Create;
  RequiredList.Sorted := True;
  RequiredList.Duplicates := dupIgnore;

  IPItemsToSet( ScheduledCalcMethod_ComboBoxChoices, FCalcTypeCodes );
  IPItemsToSet( BenefitAmountType_ComboChoices, FBenefitAmountTypeCodes );

  FActivated := true;

  inherited;

  HideRecordHelper.SetDataSet( DM_EMPLOYEE.EE_SCHEDULED_E_DS, 'SCHEDULED_E_D_ENABLED' );
  HideRecordHelper.OnCanHideRecord := CanHideOrUnhideSchedEE;
  HideRecordHelper.OnMayHideRecord := MayHideOrUnhideSchedEE;
  HideRecordHelper.sHideAction := 'Remove';
  HideRecordHelper.OnCanUnhideRecord := CanHideOrUnhideSchedEE;
  HideRecordHelper.OnMayUnhideRecord := MayHideOrUnhideSchedEE;
  HideRecordHelper.ScheduledEEFlag := True;

  HideRecordHelper.butnNext.SecurityRO := false;
  HideRecordHelper.evBitBtn1.SecurityRO := false;

  if (wwdsList.DataSet.RecordCount > 0) then
    cdEEScheduledView.UserFilter := currentfilter;

  ScreenStateChanged( '', '' );
  GrayOut(dedtClient_Annual_Maximum_Amount, true);

  mnavDirectDeposit.DataSource := DDSrc;
  mnavDirectDeposit.InsertFocusControl := dedtABA_Number;

  mnavChildSupport.DataSource := ChildSupportSrc;
  mnavChildSupport.InsertFocusControl := dedtPriority_Number;
  CheckRequired(true, 'CALCULATION_TYPE', lablCalculation_Type);

  //GUI 2.0
  pnlFashionBrowseEDs.OnResize(self);

  FAfterPost := DM_CLIENT.EE_SCHEDULED_E_DS.AfterPost;
  DM_CLIENT.EE_SCHEDULED_E_DS.AfterPost := AfterPost;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.DeActivate;
begin
  DM_CLIENT.EE_SCHEDULED_E_DS.AfterPost := FAfterPost;
  GetDefaultDataSet.Filter := '';
  GetDefaultDataSet.Filtered := false;
  FActivated := false;
  inherited;

  RequiredList.Free;
  FFilteredDS := nil;  
end;

procedure TEDIT_EE_SCHEDULED_E_DS.CheckRequiredFields;
var
  I: Integer;
  ErrorFound: Boolean;
  Required: String;
begin
  ErrorFound := False;
  Required := '';
  wwdsDetail.DataSet.UpdateRecord;
  if Trim(wwdsDetail.DataSet.FieldByName('CALCULATION_TYPE').AsString) = '' then
  begin
     wwdsDetail.DataSet.FieldByName('CALCULATION_TYPE').Clear;
  end;
  for I := 0 to RequiredList.Count - 1 do
  begin
    if wwdsDetail.DataSet.FieldByName(RequiredList.Names[I]).IsNull then
    begin
      ErrorFound := true;
      Required := Required+', '+RequiredList.Values[RequiredList.Names[I]];
    end;
  end;

  if ErrorFound then
  begin
    if Length(Required) > 0 then
      if Required[1] = ',' then
        Delete(Required, 1, 1);
    Required :=  Trim(Required);
    if Required = 'EE Direct Deposit' then
    begin
      PageControl1.ActivatePage(tshtDirectDeposits);
      raise EUpdateError.CreateHelp('You must attach a Direct Deposit!  You have been automatically taken to the Direct Deposit tab at this time.', IDH_ConsistencyViolation);
    end;
    if Required = 'Child Support Case' then
    begin
      PageControl1.ActivatePage(tshtChildSupport);
      raise EUpdateError.CreateHelp('You must attach a Child Support Case!  You have been automatically taken to the Child Support Case tab at this time.', IDH_ConsistencyViolation);
    end;
    raise EUpdateError.CreateHelp('The following fields must have a value '+Required+'.', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);

  function CheckEffectiveDateLimits:Boolean;
  var
   sTmp : String;
  begin
        Result := false;
        sTmp := '';
        if wwdsDetail.State in [dsEdit] then
          sTmp:=' and EE_SCHEDULED_E_DS_NBR <> '+ DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsString;

        if FFilteredDS.vclDataSet.IndexDefs.IndexOf('Idx')=-1 then
           FFilteredDS.vclDataSet.AddIndex('Idx','EFFECTIVE_START_DATE',[ixDescending]);
        FFilteredDS.Filter:= ' CL_E_DS_NBR=' + DM_EMPLOYEE.EE_SCHEDULED_E_DS.CL_E_DS_NBR.AsString + sTmp;
        FFilteredDS.Filtered := true;

        if FFilteredDS.RecordCount > 0 then
        begin
          if FFilteredDS.Locate('EFFECTIVE_START_DATE', dEffectiveStartDate.Date, []) then
             result := True
          else
          begin
            FFilteredDS.First;
            while not FFilteredDS.Eof do
            begin
               if ((( dEffectiveStartDate.Date>=FFilteredDS.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                    ( dEffectiveStartDate.Date<=FFilteredDS.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
                   or
                   ((dEffectiveStartDate.Date>=FFilteredDS.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                    (FFilteredDS.FieldByName('EFFECTIVE_END_DATE').IsNull)))  then // EXCEPTION WRONG_EFFECTIVE_START_DATE;
               begin
                  result := True;
                  break;
               end;
               if (((dEffectiveEndDate.Date>=FFilteredDS.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                    (dEffectiveEndDate.Date<=FFilteredDS.FieldByName('EFFECTIVE_END_DATE').AsDateTime))
                   or
                   ((dEffectiveEndDate.Date>=FFilteredDS.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                    (FFilteredDS.FieldByName('EFFECTIVE_END_DATE').IsNull))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
               begin
                  result := True;
                  break;
               end;
               if ((dEffectiveStartDate.Date<FFilteredDS.FieldByName('EFFECTIVE_START_DATE').AsDateTime) and
                   (((dEffectiveEndDate.Date>FFilteredDS.FieldByName('EFFECTIVE_END_DATE').AsDateTime) and (FFilteredDS.FieldByName('EFFECTIVE_END_DATE').AsDateTime <> 0)) or
                    (dEffectiveEndDate.Date = 0))) then  // EXCEPTION WRONG_EFFECTIVE_END_DATE;
               begin
                  result := True;
                  break;
               end;

               FFilteredDS.Next;
            end;
          end;
        end;
  end;

begin

  if Kind in [NavOK, NavCommit]  then
    if (wwdsDetail.State in [dsEdit, dsInsert]) and  CheckEffectiveDateLimits then
    begin
         wwdsDetail.DataSet.Cancel;
         raise EUpdateError.CreateHelp('Please check Effective Start Date and Effective End Date limits.  The same scheduled E/D can not have effective dates that overlap.', IDH_ConsistencyViolation);
    end;

  if Kind = NavOK then
  begin
    if (dEffectiveEndDate.Text <>'') and (dEffectiveStartDate.Text <>'') then
      if dEffectiveStartDate.Date > dEffectiveEndDate.Date then
        raise EUpdateError.CreateHelp('You can not have an Effective End Date less than the Effective Start Date.', IDH_ConsistencyViolation);

    if (wwdsDetail.State = dsEdit) and (wwdsDetail.DataSet.FieldByName('CL_E_DS_NBR').OldValue <> wwdsDetail.DataSet.FieldByName('CL_E_DS_NBR').NewValue) then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelect2CurrentNbrWithCondition') do
      begin
        SetMacro('Columns', 'count(*)');
        SetMacro('Table1', 'pr_check');
        SetMacro('Table2', 'pr_check_lines');
        SetMacro('JoinField', 'pr_check');
        SetMacro('NbrField', 'ee_scheduled_e_ds');
        SetParam('RecordNbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR']);
        SetMacro('Condition', 't1.ee_nbr = :EeNbr');
        SetParam('EeNbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR']);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      if (ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0) then
      begin
        Handled := True;
        ctx_DataAccess.CUSTOM_VIEW.Close;
        raise EUpdateError.CreateHelp('You can not change the ED TYPE because its attached to payroll(s). You should end date the ED and create a new one with the correct type!', IDH_ConsistencyViolation);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Close;
    end;

    if (wwdsDetail.State in [dsInsert, dsEdit]) and
       ((wwdsDetail.DataSet.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').Value <> 0) or
       (not VarIsNull(wwdsDetail.DataSet.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').Value))) then
    begin
      ctx_DataAccess.CUSTOM_VIEW.Close;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', 'count(*)');
        SetMacro('TableName', 'EE_SCHEDULED_E_DS');
        SetMacro('Condition', 'ee_nbr = :EeNbr and ee_child_support_cases_nbr = :EeChild ');
        SetParam('EeNbr', DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR']);
        SetParam('EeChild', DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_CHILD_SUPPORT_CASES_NBR']);
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      if ((ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 1) and (wwdsDetail.State = dsEdit) ) or
         ((ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0) and (wwdsDetail.State = dsInsert)) then
      begin
        Handled := True;
        ctx_DataAccess.CUSTOM_VIEW.Close;
        raise EUpdateError.CreateHelp('A child support case can only be attached to one Scheduled E/D.  Please attach a different case.!', IDH_ConsistencyViolation);
      end;
      ctx_DataAccess.CUSTOM_VIEW.Close;
    end;

    if (wwdsDetail.State = dsInsert) or (wwdsDetail.State = dsEdit) then
    begin
      FWasInsert := wwdsDetail.State = dsInsert;
      FWasEdit := wwdsDetail.State = dsEdit;

      CheckRequiredFields;
      CheckConsistency;
    end;

    if (DDSrc.State = dsInsert) or (DDSrc.State = dsEdit) then
    begin
      DDSrc.DataSet.UpdateRecord;
      DDsrc.DataSet.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString := Trim(DDsrc.DataSet.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString);
      if ValidateABANumber(DDSrc.DataSet['EE_ABA_NUMBER']) <> 0 then DDSrc.DataSet.Cancel;

      if VarCompareValue( DDsrc.DataSet.FieldByName('EE_BANK_ACCOUNT_NUMBER').OldValue,
        DDsrc.DataSet.FieldByName('EE_BANK_ACCOUNT_NUMBER').NewValue ) <> vrEqual then
        EvMessage('Be sure to obtain a new direct deposit authorization form'+ #10#13 +'from the employee with the new account number!');
    end;

    if (DDSrc.State = dsInsert) or (ChildSupportSrc.State = dsInsert) then
    begin
      FWasNewDD := (DDSrc.State = dsInsert);
      FWasNewCS := (ChildSupportSrc.State = dsInsert);
      FTmpEE := wwdsEmployee.DataSet.FieldByName('EE_NBR').AsInteger;
    end;


    if ((wwdsDetail.State = dsInsert) or (wwdsDetail.State = dsEdit)) and
      (wwdsDetail.DataSet['DEDUCT_WHOLE_CHECK']='Y') and
      not (VarIsNull( wwdsDetail.DataSet['MAXIMUM_PAY_PERIOD_PERCENTAGE'] ) and VarIsNull( wwdsDetail.DataSet['MAXIMUM_PAY_PERIOD_AMOUNT'] )) then
    begin
      wwdsDetail.DataSet['MAXIMUM_PAY_PERIOD_PERCENTAGE']:=Null;
      wwdsDetail.DataSet['MAXIMUM_PAY_PERIOD_AMOUNT']:=Null;
      wwlcMaximum_E_D_Group.DataSource.DataSet.FieldByName(wwlcMaximum_E_D_Group.DataField).Value := Null;
      EvMessage('Deduct whole check on Detail 1 tab has been checked, so Maximum Pay Period % or Amount on Detail 2 tab will be removed!');
    end;
  end;
  inherited;

  if Kind in [NavCancel, NavAbort, NavInsert, NavDelete] then
    FidxBenefitReference :=-1;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.AfterClick(Kind: Integer);
var
  nbr: integer;
begin
  inherited;
  if Kind = NavOK then
  begin
    if FWasInsert or FWasEdit then
    begin
      AddNewEDForPension;
      FWasInsert := False;
      FWasEdit := False;
    end;
  end;

  if Kind in [NavOK,NavCommit] then
  begin
    nbr:= DM_COMPANY.EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DS_NBR.AsInteger;
    try
      FFilteredDS := nil;
      FFilteredDS := TevDataSet.Create(DM_COMPANY.EE_SCHEDULED_E_DS);
    finally
      DM_COMPANY.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR',nbr,[]);
      FillcdEEScheduledView(DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);
    end;
  end;

  if Kind in [NavCancel, NavAbort, NavInsert, NavDelete] then
     SetBenefitReference;

  if Kind in [NavCancel, NavAbort] then
  begin
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.CancelUpdates;
    FillcdEEScheduledView(DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);
  end;

end;


procedure TEDIT_EE_SCHEDULED_E_DS.CheckRequired( aRequired: boolean; aFieldName: string; aLabel: TevLabel );
var
  idx: integer;
  aDisplayLabel: string;
begin
  if Assigned(RequiredList) then
  begin
    if Pos('~', aLabel.Caption) = 1 then
      aDisplayLabel := Copy(aLabel.Caption, 2, Length(aLabel.Caption)-1)
    else
      aDisplayLabel := aLabel.Caption;
    if aRequired then
    begin
      RequiredList.Add( aFieldName + '=' + aDisplayLabel );
      aLabel.Caption := '~' + aDisplayLabel;
    end
    else
    begin
      idx := RequiredList.IndexOfName( aFieldName );
      if idx <> - 1 then
        RequiredList.Delete(idx);
      aLabel.Caption := aDisplayLabel;
    end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwlcScheduledEDGroupChange(
  Sender: TObject);
begin
  CheckRequired( wwlcScheduledEDGroup.Text <> '', 'PRIORITY_NUMBER', lablPriority );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwlcMinimum_E_D_GroupChange(
  Sender: TObject);
begin
  CheckRequired( wwlcMinimum_E_D_Group.Text <> '', 'MINIMUM_PAY_PERIOD_PERCENTAGE', lablMinimum_Pay_Period_Percentage );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwlcMaximum_E_D_GroupChange(
  Sender: TObject);
begin
  CheckRequired( wwlcMaximum_E_D_Group.Text <> '', 'MAXIMUM_PAY_PERIOD_PERCENTAGE', lablMaximum_Pay_Period_Percentage );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwcbTarget_ActionChange(Sender: TObject);
begin
  CheckRequired( (wwcbTarget_Action.Text <> '') and (wwcbTarget_Action.Text <> 'None'), 'TARGET_AMOUNT', lablTarget_Amount );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwdsDetailDataChange(Sender: TObject; Field: TField);
var
  i: Integer;
  b: Boolean;
begin
  if csLoading in ComponentState then
    Exit;
  if not assigned(Field) or (Field.FieldName = 'CL_E_DS_NBR') then
    if DSIsActive( DM_CLIENT.CL_E_DS) and DSIsActive( wwdsDetail ) then
      DM_CLIENT.CL_E_DS.Locate( 'CL_E_DS_NBR', wwdsDetail.DataSet['CL_E_DS_NBR'], [] );
  inherited;

  if FActivated and (PageControl1.ActivePage = tshtBrowse_Scheduled_E_DS) and cdEEScheduledView.Active and
      (wwdsDetail.DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger > 0) and
       (cdEEScheduledView.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger <> wwdsDetail.DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger) then
    cdEEScheduledView.Locate('EE_SCHEDULED_E_DS_NBR', wwdsDetail.DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger,[]);

  if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
     SetBenefitReference;

  if assigned(Field) then
    ScreenStateChanged( Field.FieldName, '' )
  else
    ScreenStateChanged( '', '' );
  b := CanUpdateED;
  with PopupMenu1 do
    for i := 0 to Pred(PopupMenu1.Items.Count) do
      PopupMenu1.Items[i].Enabled := b;
  if (wwdsDetail.DataSet['DEDUCT_WHOLE_CHECK']='Y') then
  begin
    dedtMaximum_Pay_Period_Percentage.Enabled:=false;
    dedtMaximum_Pay_Period_Percentage.color:=clBtnFace ;
    wwlcMaximum_E_D_Group.Enabled:=false;
    wwlcMaximum_E_D_Group.color:=clBtnFace ;
    dedtMaximum_Pay_Period_Amount.Enabled:=false;
    dedtMaximum_Pay_Period_Amount.Color:=clBtnFace;
  end
  else
  begin
    dedtMaximum_Pay_Period_Percentage.Enabled:=true;
    dedtMaximum_Pay_Period_Percentage.color:=clWindow;
    wwlcMaximum_E_D_Group.Enabled:=true;
    wwlcMaximum_E_D_Group.Color:=clWindow;
    dedtMaximum_Pay_Period_Amount.Enabled:=true;
    dedtMaximum_Pay_Period_Amount.Color:=clWindow;
  end;
  if (Field = nil) or (Field.FieldName = 'USE_PENSION_LIMIT') then
  begin
     cbUsePensionLimitClick(Sender);
  end;

  if FActivated then
   if not assigned(Field) or (Field.FieldName = 'CL_E_DS_NBR') then
    if DSIsActive( DM_CLIENT.CL_E_DS) and DSIsActive( wwdsDetail ) then
     if DM_CLIENT.CL_E_DS.Locate( 'CL_E_DS_NBR', wwdsDetail.DataSet['CL_E_DS_NBR'], [] ) then
   begin
      if not Context.License.HR then
      Begin
       evBtnEEBenefit.Enabled := False;
       evcbEEBenefitReference.Enabled := False;
      End;
   end;

  ResultFIELDS_EE_BANK_Function;
  evcbBenefitSubType.Enabled := FidxBenefitReference <> -1;

  if not evcbBenefitSubType.Enabled then
     evcbEEBenefitReference.RefreshDisplay;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.LoadCOBenefits;
var
 sl : TStringList;
begin
  if FidxBenefitReference <> -1 then
     Exit;

  if (DM_COMPANY.CO.FieldByName('CO_NBR').asInteger > 0) then
  begin
    evcbBenefitReference.ItemIndex := -1;
    evcbBenefitReference.Items.CommaText := '';

    csCobenefit := TEvDataSetHolder.CreateCloneAndRetrieveData( DM_COMPANY.CO_BENEFITS,'CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    csCobenefit.DataSet.IndexFieldNames := 'BENEFIT_NAME';

    if csCobenefit.DataSet.RecordCount > 0 then
    begin
      evcbBenefitReference.Enabled := True;
      sl := csCobenefit.DataSet.GetFieldValueList('BENEFIT_NAME',True);
      try
        evcbBenefitReference.Items.CommaText := sl.CommaText;
      finally
         FreeandNil(sl);
      end;
    end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.evcbBenefitReferenceCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  inherited;
  if (FidxBenefitReference <> evcbBenefitReference.ItemIndex) and
      not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
     wwdsDetail.DataSet.Edit;

  if (FidxBenefitReference <> evcbBenefitReference.ItemIndex) then
  begin
     FidxBenefitReference := evcbBenefitReference.ItemIndex;
     dsCLBenefits.DataSet.Locate('BENEFIT_NAME',evcbBenefitReference.Text,[]);

     wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Clear;
     ScreenStateChanged( 'CO_BENEFIT_SUBTYPE_NBR', '' );
     ScreenStateChanged( 'EE_BENEFITS_NBR', '' );
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.evcbBenefitReferenceChange(
  Sender: TObject);
begin
  inherited;
  if (Trim(evcbBenefitReference.Text) = '' ) and
     (FidxBenefitReference <> -1) then
  begin
    if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
       wwdsDetail.DataSet.Edit;

    FidxBenefitReference := -1;
    dsCLBenefits.DataSet.Locate('BENEFIT_NAME',evcbBenefitReference.Text,[]);

    wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Clear;
    ScreenStateChanged( 'CO_BENEFIT_SUBTYPE_NBR', '' );
    ScreenStateChanged( 'EE_BENEFITS_NBR', '' );
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.SetBenefitReference;
begin
    FidxBenefitReference := -1;
    if (not wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull) and
       assigned(csCobenefit) then
    begin
        if ( csCobenefit.DataSet.RecordCount > 0 ) then
        begin
          DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR',wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger,[]);
          DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR',DM_COMPANY.CO_BENEFIT_SUBTYPE.FieldByName('CO_BENEFITS_NBR').AsInteger,[]);
          evcbBenefitReference.ItemIndex := evcbBenefitReference.Items.IndexOf(DM_COMPANY.CO_BENEFITS.FieldByName('BENEFIT_NAME').asString)
        end
        else
          evcbBenefitReference.ItemIndex := -1;
    end
    else
      evcbBenefitReference.ItemIndex := -1;

    FidxBenefitReference := evcbBenefitReference.ItemIndex;
    evcbBenefitSubType.Enabled := FidxBenefitReference <> -1;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dedtMinimum_Wage_MultiplierChange(
  Sender: TObject);
begin
  CheckRequired( dedtMinimum_Wage_Multiplier.Text <> '', 'MAXIMUM_GARNISH_PERCENT', lablMaximum_Garnish_Percentage );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dedtMaximum_Garnish_PercentageChange(
  Sender: TObject);
begin
  CheckRequired( dedtMaximum_Garnish_Percentage.Text <> '', 'MINIMUM_WAGE_MULTIPLIER',  lablMinimum_Wage_Multiplier );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwcbCalculation_TypeChange(
  Sender: TObject);
begin
  CheckRequired( (wwcbCalculation_Type.Text = '% of Earn Code Group') , 'CL_E_D_GROUPS_NBR',  lablE_D_Group );
end;


procedure TEDIT_EE_SCHEDULED_E_DS.dedtTarget_AmountChange(Sender: TObject);
begin
  CheckRequired( dedtTarget_Amount.Text <> '' , 'TARGET_ACTION', lablTarget_Action );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.DBRadioGroup1Change(Sender: TObject);
begin
  inherited;
 // dbradiogroup changes state of dataset to dsEdit after calling of OnClick and Onchange events
  if DBRadioGroup1.ItemIndex <> -1 then
    ScreenStateChanged( '', DBRadioGroup1.Values[DBRadioGroup1.ItemIndex] );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dedtMinimum_Pay_Period_PercentageChange(
  Sender: TObject);
begin
  CheckRequired( dedtMinimum_Pay_Period_Percentage.Text <> '' , 'MIN_PPP_CL_E_D_GROUPS_NBR', lablMinimum_E_D_Group );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dedtMaximum_Pay_Period_PercentageChange(
  Sender: TObject);
begin
  CheckRequired( dedtMaximum_Pay_Period_Percentage.Text <> '' , 'MAX_PPP_CL_E_D_GROUPS_NBR', lablMaximum_E_D_Group );
end;

function IsDA_DK( aCodeType: string ): boolean;
begin
  Result := ( Length(aCodeType) = 2 ) and ( aCodeType[1] = 'D' ) and ( aCodeType[2] in ['A'..'K', '2', '4','!', '@', '#', '$', '%', '(', ')', '*']);
end;

function IsDA_DFDK( aCodeType: string ): boolean;
begin
  Result := ( Length(aCodeType) = 2 ) and ( aCodeType[1] = 'D' ) and ( aCodeType[2] in ['A'..'F','K', '2', '!', '@', '#', '$', '%', '(', ')', '*']);
end;

procedure SetEvCbText( evcb: TevDBComboBox; aText: string );
var
  cbText: string;
begin
  if aText <> evcb.Items.Text then
  begin
    evcb.Items.Text := aText;
    cbText := evcb.GetComboDisplay( evcb.DataSource.DataSet.FieldByName(evcb.DataField).AsString );
    if evcb.Text <> cbText then
      evcb.Text := cbText;
  end;
end;



procedure TEDIT_EE_SCHEDULED_E_DS.ScreenStateChanged(fname: string; aDeductWholeCheck: string );
  procedure SyncBenefitInfo;
  begin
    if (not DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull) or
       (not DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EE_BENEFITS_NBR').IsNull)  then
    begin

      detailBenefitAmount;

      pnlBenefitInfo.Visible := True;
      edcAMOUNT.SecurityRO := True;
      edcPERCENTAGE.SecurityRO := True;
      pnlBenefitInfo.BringToFront;

    end
    else
    begin
      pnlBenefitInfo.Visible := False;
      wwcbCalculation_Type.Enabled := True;
    end;
    GrayOut( wwcbCalculation_Type, not wwcbCalculation_Type.Enabled or wwcbCalculation_Type.SecurityRO);
  end;
  procedure DoScreenStateChanged;
  var
    calcTypeCodes: TSetOfChar;
    procedure GrayOutCalcType( aCalcType: TSetOfChar; grayout: boolean );
    begin
      if grayout then
        calcTypeCodes := calcTypeCodes - aCalcType
      else
        calcTypeCodes := calcTypeCodes + aCalcType;
    end;
  var
    CodeType: string;
    CalculationType: string;
    DeductWholeCheck: boolean;
    IsCompleted: boolean;
    IsCodeCompleted: boolean;
    PercentageGrayedOut: boolean;
    AmountGrayedOut: boolean;
    shouldGrayOut: boolean;
  begin
    CodeType := AnsiUpperCase(trim(VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwdsDetail.DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE'))));
    CalculationType := trim(VarToStr(wwdsDetail.DataSet['CALCULATION_TYPE']));
    if aDeductWholeCheck = '' then
      DeductWholeCheck := trim(VarToStr(wwdsDetail.DataSet['DEDUCT_WHOLE_CHECK'])) = GROUP_BOX_YES
    else
      DeductWholeCheck := (Length(aDeductWholeCheck) = 1) and ( aDeductWholeCheck[1] = GROUP_BOX_YES );
    IsCompleted := (CalculationType <> '') and (CodeType <> '');
    IsCodeCompleted := (CodeType <> '');
    PercentageGrayedOut := false;
    AmountGrayedOut := false;

    calcTypeCodes := FCalcTypeCodes;

    shouldGrayOut := not IsDA_DK(CodeType) and (CalculationType = CALC_METHOD_FIXED) and IsCompleted;
    GrayOutControl( wwlcE_D_Group, shouldGrayOut or wwlcE_D_Group.SecurityRO);

    PercentageGrayedOut := PercentageGrayedOut or shouldGrayOut;

    shouldGrayOut := (CodeType <> 'DR') and IsCodeCompleted;
    GrayOutCalcType( [CALC_METHOD_GARNISHMENT, CALC_METHOD_GARNISHMENT_GROSS], shouldGrayOut );
    GrayOutControl( dedtMinimum_Wage_Multiplier, shouldGrayOut or dedtMinimum_Wage_Multiplier.SecurityRO);
    GrayOutControl( dedtMaximum_Garnish_Percentage, shouldGrayOut or dedtMaximum_Garnish_Percentage.SecurityRO);

    shouldGrayOut := (CodeType = 'DZ') and IsCodeCompleted;
    GrayOutCalcType( calcTypeCodes - [CALC_METHOD_NY_SDI] {?? - gdy}, shouldGrayOut );

    shouldGrayOut := (CodeType = 'D1') and (CalculationType = CALC_METHOD_FIXED) and IsCompleted;
    PercentageGrayedOut := PercentageGrayedOut or shouldGrayOut;

    shouldGrayOut := (CodeType = 'D1') and (CalculationType <> CALC_METHOD_FIXED) and IsCompleted;
    AmountGrayedOut := AmountGrayedOut or shouldGrayOut;

    shouldGrayOut := (CodeType = 'D1') and not DeductWholeCheck and IsCodeCompleted;
    GrayOutControl( dedtTake_Home_Pay, shouldGrayOut or dedtTake_Home_Pay.SecurityRO );

    shouldGrayOut := (CodeType = 'D1') and DeductWholeCheck and IsCodeCompleted;
    PercentageGrayedOut := PercentageGrayedOut or shouldGrayOut;
    AmountGrayedOut := AmountGrayedOut or shouldGrayOut;

    CheckRequired( CodeType = 'D1', 'EE_DIRECT_DEPOSIT_NBR', labl_EE_direct_Deposit );
    CheckRequired( CodeType = 'DR', 'GARNISNMENT_ID', labl_Garnishment_ID );
    CheckRequired( CodeType = 'DQ', 'EE_CHILD_SUPPORT_CASES_NBR', labl_Child_Support );
    CheckRequired( CodeType = 'DS', 'TAKE_HOME_PAY', lablTake_Home_Pay);


    shouldGrayOut := wwdsDetail.DataSet.FieldByName('CL_AGENCY_NBR').IsNull xor
                     wwdsDetail.DataSet.FieldByName('EE_DIRECT_DEPOSIT_NBR').IsNull;
    GrayOutControl( wwDBLookupCombo1, (shouldGrayOut and (CodeType <> 'D1') and
        not wwdsDetail.DataSet.FieldByName('CL_AGENCY_NBR').IsNull) or wwDBLookupCombo1.SecurityRO);
    wwDBLookupCombo1.SecurityRO := wwDBLookupCombo1.SecurityRO;

    GrayOutControl( wwlcAgency, ( shouldGrayOut and not wwdsDetail.DataSet.FieldByName('EE_DIRECT_DEPOSIT_NBR').IsNull ) or wwlcAgency.SecurityRO);

    shouldGrayOut := (FidxBenefitReference = -1)
                       xor
                     wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').IsNull;

    GrayOutControl(evcbBenefitReference, (shouldGrayOut and not wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').IsNull) or evcbBenefitReference.SecurityRO);

    GrayOutControl( evcbBenefitSubType, (FidxBenefitReference = -1) or evcbBenefitSubType.SecurityRO);

    GrayOutControl( evcbEEBenefitReference, (shouldGrayOut and (FidxBenefitReference <> -1)) or evcbEEBenefitReference.SecurityRO);
    GrayOutControl( evBtnEEBenefit, (shouldGrayOut and (FidxBenefitReference <> -1)) or evBtnEEBenefit.SecurityRO);

    SetEvCbText( wwcbCalculation_Type, SetToIPItems(ScheduledCalcMethod_ComboBoxChoices, calcTypeCodes) );
    wwcbCalculation_Type.AllowClearKey := False;
    GrayOutControl( dedtPercentage, PercentageGrayedOut or dedtPercentage.SecurityRO);
    GrayOutControl( dedtAmount, AmountGrayedOut or dedtAmount.SecurityRO );
  end;
begin
  if FActivated and DSIsActive(DM_CLIENT.CL_E_DS) and DSIsActive(wwdsDetail) then
  begin
    if (fname = '') or (fname = 'CL_E_DS_NBR') or (fname = 'CALCULATION_TYPE') or (fname = 'DEDUCT_WHOLE_CHECK')
       or (fname = 'CL_AGENCY_NBR') or (fname = 'EE_DIRECT_DEPOSIT_NBR') or
       (fname = 'CO_BENEFITS_NBR') or
       (fname = 'EE_BENEFITS_NBR') then
      DoScreenStateChanged;
    if (fname = '') or (fname = 'CO_BENEFITS_NBR') or
         (fname = 'CO_BENEFIT_SUBTYPE_NBR') or (fname = 'EE_BENEFITS_NBR') then
      SyncBenefitInfo;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.CheckConsistency;
var
  CodeType: string;
  CalculationType: string;
  DeductWholeCheck: boolean;
  value: string;
  v: Variant;
begin
  CodeType := AnsiUpperCase(trim(VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwdsDetail.DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE'))));
  CalculationType := trim(VarToStr(wwdsDetail.DataSet['CALCULATION_TYPE']));
  DeductWholeCheck := trim(VarToStr(wwdsDetail.DataSet['DEDUCT_WHOLE_CHECK'])) = GROUP_BOX_YES;

  if (CodeType = 'D1') and
     (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FREQUENCY.Value = SCHED_FREQ_USER_ENTERED) then
  begin
    if DM_EMPLOYEE.EE_SCHEDULED_E_DS.DEDUCT_WHOLE_CHECK.Value = GROUP_BOX_YES then
      EvMessage('Deduct whole check can not be used with this frequency. You must either set an amount in the scheduled E/D, or manually key it in the payroll');
    if not DM_EMPLOYEE.EE_SCHEDULED_E_DS.EE_DIRECT_DEPOSIT_NBR.IsNull then
    begin
      Assert(DM_EMPLOYEE.EE_DIRECT_DEPOSIT.Locate('EE_DIRECT_DEPOSIT_NBR', DM_EMPLOYEE.EE_SCHEDULED_E_DS.EE_DIRECT_DEPOSIT_NBR.Value, []));
      if DM_EMPLOYEE.EE_DIRECT_DEPOSIT.IN_PRENOTE.Value = GROUP_BOX_YES then
        EvMessage('This direct deposit is in prenote. You must either manually send a prenote, or change the status to prenote "no" in order to use this deduction');
    end;
  end;

  if (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('BENEFIT_AMOUNT_TYPE').AsString[1] in
     [BENEFIT_AMOUNT_TYPE_SINGLE_PERCENT, BENEFIT_AMOUNT_TYPE_DOUBLE_PERCENT,
      BENEFIT_AMOUNT_TYPE_FAMILY_PERCENT]) and
    (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString <> CALC_METHOD_GROSS) and
    (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString <> CALC_METHOD_NET) and
    (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString <> CALC_METHOD_DISP_EARN) and
    (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString <> CALC_METHOD_TAX_WAGES) and
    (DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString <> CALC_METHOD_PERC) then
    raise EUpdateError.CreateHelp('Calculation type must be percentage based for this benefit', IDH_ConsistencyViolation);

  if (wwdsDetail.DataSet.FieldByName('TARGET_AMOUNT').AsFloat <> 0) and
     ((Trim(wwdsDetail.DataSet.FieldByName('TARGET_ACTION').AsString) = '') or
      (wwdsDetail.DataSet.FieldByName('TARGET_ACTION').AsString = SCHED_ED_TARGET_NONE)) then
  begin
    wwcbTarget_Action.Show;
    if wwcbTarget_Action.CanFocus then
      wwcbTarget_Action.SetFocus;
    raise EUpdateError.CreateHelp('Target action must be setup if target amount is not zero', IDH_ConsistencyViolation);
  end;

  if (CodeType = 'DO') and (wwdsDetail.DataSet.FieldByName('TARGET_AMOUNT').AsFloat > 0) then
  begin
    dedtTarget_Amount.Show;
    if dedtTarget_Amount.CanFocus then
      dedtTarget_Amount.SetFocus;
    raise EUpdateError.CreateHelp('Target amount must be negative for this E/D type', IDH_ConsistencyViolation);
  end;

  if (CodeType = 'D1') and DeductWholeCheck and (CalculationType <> CALC_METHOD_NONE) then
  begin
    wwcbCalculation_Type.Show;
    if wwcbCalculation_Type.CanFocus then
      wwcbCalculation_Type.SetFocus;
    raise EUpdateError.CreateHelp('Due to selecting Deduct Whole Check ''Yes'' Calculation Method must be ''None''.', IDH_ConsistencyViolation);
  end;

  if IsDA_DFDK(CodeType) and wwdsDetail.DataSet.fieldByName('CL_AGENCY_NBR').IsNull then
    if evMessage('Do you want to attach agency?',mtConfirmation	,[mbYes, mbNo], mbYes) <> mrNo then
    begin
      wwlcAgency.Show;
      if wwlcAgency.CanFocus then
        wwlcAgency.SetFocus;
      Abort;
    end;

  if IsDA_DFDK(CodeType) and wwdsDetail.DataSet.fieldByName('PERCENTAGE').IsNull and (CalculationType = CALC_METHOD_FIXED) then
    if EvMessage('Is there an ER MATCH associated with this deduction?', mtConfirmation, [mbYes, mbNo]) = mrYes then
      while true do
        if EvDialog('% Match', 'Please enter the % for er match.', Value) then
        try
          wwdsDetail.DataSet.Edit;
          wwdsDetail.DataSet.FieldByName('PERCENTAGE').AsFloat := StrToFloat(Value);
          break;
        except
          EvMessage('You must enter a valid number. Please try again.');
        end
        else
          break;


  if (CalculationType = CALC_METHOD_PENSION) and
     not wwdsDetail.DataSet.fieldByName('MAX_PPP_CL_E_D_GROUPS_NBR').IsNull then
  begin
    v := DM_CLIENT.CL_E_DS.Lookup( 'E_D_CODE_TYPE', ED_ST_401K,
      'MATCH_FIRST_PERCENTAGE;MATCH_SECOND_PERCENTAGE;MATCH_FIRST_AMOUNT;MATCH_SECOND_AMOUNT');
    if not VarIsNull(v) then
    begin
      if VarIsNull(v[0]) or VarIsNull(v[1]) or VarIsNull(v[2]) or VarIsNull(v[3]) then
        EvMessage('You have selected the Pension Match calculation method and used the Maximum E/D Group.'#13#10+
        'You need to set up the percentages for the 401k E/D on Pension and Benefits tab on the Client E/Ds screen.',mtInformation, [mbOK]	);
    end
  end

end;

procedure TEDIT_EE_SCHEDULED_E_DS.AddNewEDForPension;
var
  CodeType: string;
  CalculationType: string;
  dsClientED: TEvClientDataSet;
  effDate: TDate;
begin
  CodeType := AnsiUpperCase(trim(VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwdsDetail.DataSet['CL_E_DS_NBR'], 'E_D_CODE_TYPE'))));
  CalculationType := trim(VarToStr(wwdsDetail.DataSet['CALCULATION_TYPE']));
  if IsDA_DFDK(CodeType) then
  begin
    dsClientED := TEvClientDataSet.Create( nil );
    try
      dsClientED.CloneCursor( DM_CLIENT.CL_E_DS, true );
      dsClientED.Filter := 'E_D_CODE_TYPE=''M3'' or E_D_CODE_TYPE=''M5''';
      dsClientED.Filtered := True;
      dsClientED.First;
      while not dsClientED.Eof do
      begin
        if not VarIsNull( DM_COMPANY.CO_E_D_CODES.Lookup('CL_E_DS_NBR', dsClientED.FieldByName('CL_E_DS_NBR').Value, 'CL_E_DS_NBR') )
           and VarIsNull( DM_EMPLOYEE.EE_SCHEDULED_E_DS.Lookup('CL_E_DS_NBR', dsClientED.FieldByName('CL_E_DS_NBR').Value, 'CL_E_DS_NBR') ) then

          if EvMessage('Would you like to setup a match using '+dsClientED.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString+' ('+dsClientED.FieldByName('DESCRIPTION').AsString+').',
                         mtConfirmation, [mbYes, mbNo]) = mrYes then
          begin
            effDate := dsClientED.fieldByName('SD_EFFECTIVE_START_DATE').AsDateTime;
            while effDate = 0 do
              if not EvDateDialog( 'Setup a match using '+dsClientED.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString+' ('+dsClientED.FieldByName('DESCRIPTION').AsString+')', 'Please, enter Effective Start Date for this match', effDate ) then
                Break;
            if effDate <> 0 then
            begin
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.Insert;
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR'] := DM_EMPLOYEE.EE['EE_NBR'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'] := dsClientED['CL_E_D_GROUPS_NBR'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['FREQUENCY'] := dsClientED['SD_FREQUENCY'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EFFECTIVE_START_DATE'] := effDate;
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['AMOUNT'] := dsClientED['SD_AMOUNT'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['PERCENTAGE'] := dsClientED['SD_RATE'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_AGENCY_NBR'] := dsClientED['DEFAULT_CL_AGENCY_NBR'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['CALCULATION_TYPE'] := dsClientED['SD_CALCULATION_METHOD'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_1'] := dsClientED['SD_EXCLUDE_WEEK_1'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_2'] := dsClientED['SD_EXCLUDE_WEEK_2'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_3'] := dsClientED['SD_EXCLUDE_WEEK_3'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_4'] := dsClientED['SD_EXCLUDE_WEEK_4'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['EXCLUDE_WEEK_5'] := dsClientED['SD_EXCLUDE_WEEK_5'];
              DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_DS_NBR'] := dsClientED['CL_E_DS_NBR'];
              // Added for reso#45721 by Andrew
              if (DM_CLIENT.CL_E_DS.FieldByName('SCHEDULED_DEFAULTS').AsString = 'Y') then
                DM_EMPLOYEE.EE_SCHEDULED_E_DS['PRIORITY_NUMBER'] := dsClientED['SD_PRIORITY_NUMBER'];

              DM_EMPLOYEE.EE_SCHEDULED_E_DS['CALCULATION_TYPE'] := dsClientED['SD_CALCULATION_METHOD'];//CALC_METHOD_PENSION;
              if CalculationType = CALC_METHOD_FIXED then
              begin
                DM_EMPLOYEE.EE_SCHEDULED_E_DS['MAXIMUM_PAY_PERIOD_PERCENTAGE'] := 100;
                DM_EMPLOYEE.EE_SCHEDULED_E_DS['MAX_PPP_CL_E_D_GROUPS_NBR'] := DM_EMPLOYEE.EE_SCHEDULED_E_DS['CL_E_D_GROUPS_NBR'];
              end;

              (Owner as TFramePackageTmpl).ClickButton(NavOK);
            end;
          end;
        dsClientED.Next;
      end;
    finally
      FreeAndNil(dsClientED);
    end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwlcThreshold_E_D_GroupChange(Sender: TObject);
begin
  CheckRequired( (wwlcThreshold_E_D_Group.Text <> '') and (not cbUsePensionLimit.Checked), 'THRESHOLD_AMOUNT', labl_Threshold_Amount );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dedtThreshold_AmountChange(Sender: TObject);
begin
  CheckRequired( (dedtThreshold_Amount.Text <> '') , 'THRESHOLD_E_D_GROUPS_NBR', labl_Threshold_E_D_Group );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwcbCalculation_TypeCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    wwdsDetail.DataSet.UpdateRecord;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.BitBtn1Click(Sender: TObject);
begin

  inherited;
  dedtClient_Annual_Maximum_Amount.SecurityRO := true; //gdy - any better way to gray out control?
  ScreenStateChanged( '', '' );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.evcbBenefitAmountTypeChange(Sender: TObject);
begin

  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    wwdsDetail.DataSet.UpdateRecord;
  ScreenStateChanged( 'BENEFIT_AMOUNT_TYPE', '' ); //gdy - SyncBenefitInfo was here
end;

function TEDIT_EE_SCHEDULED_E_DS.CanHideOrUnhideSchedEE( DataSet: TevClientDataSet): boolean;
begin   //Actually Show Remove Button, UnHide
  with DataSet.FieldByName('EFFECTIVE_END_DATE') do
    Result := not IsNull and (AsDateTime < Now);
end;

//!! navigates WITHOUT disabling of change notifications so I can inspect Enabled property for db aware controls
//!! actually I should factor out and call the same subroutine as ScreenStateChanged does instead of inspecting db aware controls
function TEDIT_EE_SCHEDULED_E_DS.CanUpdateED: boolean;
begin
  Result := DSIsActive(DM_EMPLOYEE.EE_SCHEDULED_E_DS)
        and DSIsActive(DM_EMPLOYEE.EE)
        and (TevClientDataSet(DM_EMPLOYEE.EE).State = dsBrowse)
        and (DM_EMPLOYEE.EE_SCHEDULED_E_DS.State = dsBrowse)
        and (DM_EMPLOYEE.EE_SCHEDULED_E_DS.RecordCount > 0);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.UpdateAmountExecute(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  i: Integer;
  s: string;
begin
  with TAskEDUpdateParamsForAmount.Create( Self ) do
  try
    Amount := DM_EMPLOYEE.EE_SCHEDULED_E_DS['AMOUNT'];
    if ShowModal = mrOk then
    begin
      FAmountUpdateMode := UpdateMode;
      FAmount := Amount;
      FAmountChangePercent := AmountChangePercentage;

      CopyForm := TEDIT_COPY.Create(Self);
      try
        CopyForm.CopyTo := 'EEScheduledEDS';
        CopyForm.CopyFrom := 'EE';
        CopyForm.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
        CopyForm.EdNbr := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
        s := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
        CopyForm.UpdateEDs := True;
        CopyForm.CustomEDFunction := HandleUpdateEDamount;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
        CopyForm.ShowModal;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;

        with Self.Owner as TEmployeeFrm do
          for i := 0 to Pred(CopyForm.EdList.Count) do
            EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, CopyForm.EdList[i], s]);
      finally
        CopyForm.Free;
      end;
    end
  finally
    Free;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.HandleUpdateEDamount;
var
  amt, newamt: Variant;
begin
//  if dedtAmount.Enabled then //!! look at EDUpdateTemplate - it navigates WITHOUT disabling of change notifications
  begin
    amt := DM_EMPLOYEE.EE_SCHEDULED_E_DS['AMOUNT'];
    case FAmountUpdateMode of
      aumFixed: newamt := FAmount;
      aumByPercent: if not VarIsNull( amt ) then
                      newamt := amt + amt * FAmountChangePercent / 100
                    else
                      newamt := Null;
    else
      Assert( false );
    end;
    DM_EMPLOYEE.EE_SCHEDULED_E_DS['AMOUNT'] := newamt;
  end
end;

procedure TEDIT_EE_SCHEDULED_E_DS.UpdatePercentExecute(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  i: Integer;
  s: string;
begin
  with TAskEDUpdateParamsForPercent.Create( Self ) do
  try
    Percent := DM_EMPLOYEE.EE_SCHEDULED_E_DS['PERCENTAGE'];
    if ShowModal = mrOk then
    begin
      FFieldToUpdate := 'PERCENTAGE';
      FValueToUpdateTo := Percent;

      CopyForm := TEDIT_COPY.Create(Self);
      try
        CopyForm.CopyTo := 'EEScheduledEDS';
        CopyForm.CopyFrom := 'EE';
        CopyForm.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;
        CopyForm.EdNbr := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
        s := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
        CopyForm.UpdateEDs := True;
        CopyForm.CustomEDFunction := HandleUpdateEDPercentage;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.Cancel;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
        CopyForm.ShowModal;
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;

        with Self.Owner as TEmployeeFrm do
          for i := 0 to Pred(CopyForm.EdList.Count) do
            EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, CopyForm.EdList[i], s]);
      finally
        CopyForm.Free;
      end;
    end
  finally
    Free;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.HandleUpdateEDPercentage;
begin
//  if dedtPercentage.Enabled then //!! look at EDUpdateTemplate - it navigates WITHOUT disabling of change notifications
    DM_EMPLOYEE.EE_SCHEDULED_E_DS[FFieldToUpdate] := FValueToUpdateTo;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE_DIRECT_DEPOSIT, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES, EditDataSets);
  AddDS(DM_CLIENT.CL_E_DS, SupportDataSets);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.evcbBenefitSubTypeChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    wwdsDetail.DataSet.UpdateRecord;
   ScreenStateChanged( 'CO_BENEFIT_SUBTYPE_NBR', '' );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dsCL_E_DSDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  ;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dsCL_E_DSDataChangeBeforeChildren(
  Sender: TObject; Field: TField);
begin
  inherited;
  ;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dsCLBenefitsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  ;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.AfterDataSetsReopen;
begin
  inherited;
  evdsCLBenefitSubtype.DataSet := CreateLookupDataset(DM_CLIENT.CO_BENEFIT_SUBTYPE, Self);
  evcbBenefitSubType.LookupTable := evdsCLBenefitSubtype.DataSet;
  LoadCOBenefits;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.mnavDirectDepositInsertRecordExecute(
  Sender: TObject);
begin
  inherited;
  mnavDirectDeposit.InsertRecordExecute(Sender);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.DDSrcUpdateData(Sender: TObject);
{
var
  L : integer;
}
begin
  inherited;
{
  with TevDataSource(Sender) do
  begin
    L := Length(VarToStr(DataSet['EE_ABA_NUMBER']));
    if (L <> 9) and (L <> 0) then
      raise EUpdateError.CreateHelp('The ABA Number must have 9 digits', IDH_ConsistencyViolation);
  end;

  with TevDataSource(Sender) do
    if (VarToStr(DataSet['EE_ABA_NUMBER']) <> '') then
    begin
      if HashTotalABAOk(VarToStr(DataSet['EE_ABA_NUMBER'])) <> 0 then
        raise EInvalidHashTotal.CreateHelp('Invalid Hash Total of EE_ABA_NUMBER', IDH_ConsistencyViolation);
      if VarToStr(DataSet['EE_ABA_NUMBER'])[1] = '5' then
        raise EInvalidHashTotal.CreateHelp('ABA Number is not allowed to begin with 5', IDH_ConsistencyViolation);
    end
}    
end;

procedure TEDIT_EE_SCHEDULED_E_DS.mnavDirectDepositSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  mnavDirectDeposit.DeleteRecordExecute(Sender);

end;

procedure TEDIT_EE_SCHEDULED_E_DS.mnavChildSupportSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  mnavChildSupport.InsertRecordExecute(Sender);

end;

procedure TEDIT_EE_SCHEDULED_E_DS.mnavChildSupportSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  mnavChildSupport.DeleteRecordExecute(Sender);

end;

procedure TEDIT_EE_SCHEDULED_E_DS.rgILEligChange(Sender: TObject);
//var
//  b: Boolean;
//  i: Integer;
begin
  inherited;
{  b := False;
  for i := 0 to Pred(rgILElig.ControlCount) do
    b := b or TWinControl(rgILElig.Controls[i]).Focused;
  if b and (DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.State <> dsBrowse) and
     (rgILElig.Value <> GROUP_BOX_NOT_APPLICATIVE) then
    EvMessage('This option is only for IL child support');}
end;

procedure TEDIT_EE_SCHEDULED_E_DS.setReadOnlyRemotes;
begin
 if (Context.UserAccount.AccountType = uatRemote) then
 begin
   if DDSrc.DataSet.FieldByName('READ_ONLY_REMOTES').AsString = GROUP_BOX_YES then
   begin
      SetControlsReccuring(tshtDirectDeposits, 'SecurityRO', Integer(True));
      mnavDirectDeposit.SecurityRO := GetIfReadOnly;
      mnavDirectDeposit.SpeedButton1.SecurityRO:= mnavDirectDeposit.SecurityRO;
   end
   else
      SetControlsReccuring(tshtDirectDeposits, 'SecurityRO', Integer(GetIfReadOnly));
 end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.DDSrcDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if wwdsDetail.DataSet['EE_DIRECT_DEPOSIT_NBR'] <> DDSrc.DataSet['EE_DIRECT_DEPOSIT_NBR'] then
    if ((wwdsDetail.DataSet.State = dsInsert) or (wwdsDetail.DataSet.State = dsEdit)) and (PageControl1.ActivePage = tshtDirectDeposits)
       and wwdsDetail.DataSet.FieldByName('CL_AGENCY_NBR').IsNull  then
    begin
      wwdsDetail.DataSet['EE_DIRECT_DEPOSIT_NBR'] := DDSrc.DataSet['EE_DIRECT_DEPOSIT_NBR'];
      evMessage('You have attached the currently selected Direct Deposit!  Please verify this is correct!');
    end;

  setReadOnlyRemotes;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.ChildSupportSrcDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if wwdsDetail.DataSet['EE_CHILD_SUPPORT_CASES_NBR'] <> ChildSupportSrc.DataSet['EE_CHILD_SUPPORT_CASES_NBR'] then
    if ((wwdsDetail.DataSet.State = dsInsert) or (wwdsDetail.DataSet.State = dsEdit)) and (PageControl1.ActivePage = tshtChildSupport) then
    begin
      wwdsDetail.DataSet['EE_CHILD_SUPPORT_CASES_NBR'] := ChildSupportSrc.DataSet['EE_CHILD_SUPPORT_CASES_NBR'];
      evMessage('You have attached the currently selected Child Support Case!  Please verify this is correct!');
    end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.evBitBtn1Click(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_EE_CL_PERSON');
end;

procedure TEDIT_EE_SCHEDULED_E_DS.btnEmployeeBenefitClick(Sender: TObject);
begin
  inherited;
  ActivateFrameAs('HR Module - Employee - Benefits', [wwdsEmployee.DataSet.FieldByName('EE_NBR').AsInteger], 1);
end;


procedure TEDIT_EE_SCHEDULED_E_DS.dedtBank_Account_NbrChange(
  Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet['EE_DIRECT_DEPOSIT_NBR'] = DDSrc.DataSet['EE_DIRECT_DEPOSIT_NBR'] then
    if wwDBLookupCombo1.Text <> ''  then
      wwDBLookupCombo1.Text := dedtBank_Account_Nbr.Text;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.cbUsePensionLimitClick(Sender: TObject);
var Gray:boolean;
begin
  inherited;
  Gray :=  cbUsePensionLimit.Checked;
  GrayOutControl(dedtThreshold_Amount,Gray or dedtThreshold_Amount.SecurityRO );

  CheckRequired( Gray, 'THRESHOLD_E_D_GROUPS_NBR',labl_Threshold_E_D_Group);
  CheckRequired( (wwlcThreshold_E_D_Group.Text <> '') and (not Gray), 'THRESHOLD_AMOUNT', labl_Threshold_Amount );
end;

procedure TEDIT_EE_SCHEDULED_E_DS.tshtDirectDepositsShow(Sender: TObject);
begin
  inherited;
  dedtABA_Number.SetFocus;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.tshtChildSupportShow(Sender: TObject);
begin
  inherited;
  dedtPriority_Number.SetFocus;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
var
 R:integer;
begin
  inherited;
  if (wwdsList.DataSet.RecordCount <= 0) then
  begin
    AllowChange := False;
    Exit;
  end;

  AllowChange := true;

  if ((Sender as TPageControl).ActivePage = tshtBrowse_Scheduled_E_DS) then
    FillcdEEScheduledView(DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);

  if ((Sender as TPageControl).ActivePage = tshtDirectDeposits) then
  begin
    if (DDSrc.State = dsInsert) or (DDSrc.State = dsEdit) then
    begin
      DDSrc.DataSet.UpdateRecord;
      R := GetValidateABAResult(DDSrc.DataSet['EE_ABA_NUMBER']);
      AllowChange := R=0;
      if R>0 then
        EvMessage(ValidateABA_MSGS[R]);
    end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.PageControl1Change(Sender: TObject);
begin
  inherited;
  if (PageControl1.ActivePage = tshtDirectDeposits) or
     (PageControl1.ActivePage = tshtDetail_1)  then
     ResultFIELDS_EE_BANK_Function
  else if (PageControl1.ActivePage = tshtChildSupport) then
  begin
    if (wwdsDetail.DataSet.Active) and (wwdsDetail.DataSet.RecordCount > 0) then
      pnlNYChildSupport.Visible := (wwdsDetail.DataSet['CALCULATION_TYPE'] = CALC_METHOD_NYCHILDSUPPORT);
  end;

  if (PageControl1.ActivePage = tshtBrowse_Scheduled_E_DS) then
    FillcdEEScheduledView(DM_EMPLOYEE.EE_SCHEDULED_E_DS.fieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger);

    if (PageControl1.ActivePage = tshtDetail_1) then
    begin
       FillcdEEBenefits;
       SetBenefitReference;
    end;
    
    if (PageControl1.ActivePage = tshtDirectDeposits) then
      setReadOnlyRemotes;

  //For some reason the following field was getting set to 19 tall.
  wwlcThreshold_E_D_Group.Height := 21;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwDBGrid4RowChanged(Sender: TObject);
begin
// The code below looks for Child Support Cases and Direct Deposits
// that are not attached to any shceduled E/D and if such was found
// let to user choice to fix it or keep it.
// Reso #29416. Andrew
  inherited;

  if FWasNewCS or FWasNewDD then
    if CheckMissedCSDD then
      if EvMessage('Either a Child Support Case or a Direct Deposit account has been set up but not linked to a Scheduled E/D.'#13#10 +
        'Do you want to fix this now?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then begin
        wwdsEmployee.DataSet.DisableControls;
        wwdsEmployee.DataSet.Locate('EE_NBR', FTmpEE, []);
        wwdsEmployee.DataSet.EnableControls;
        PageControl1.ActivatePage(tshtBrowse_Scheduled_E_DS);
      end;
end;

function TEDIT_EE_SCHEDULED_E_DS.CheckMissedCSDD: boolean;
var dsED, dsCSDD: TevClientDataSet;
    KeyField: String;
begin
  Result := False;
  if (wwdsDetail.State = dsBrowse) and (DDSrc.State = dsBrowse) and (ChildSupportSrc.State = dsBrowse) then begin
    dsED := TEvClientDataSet.Create(Self);
    dsCSDD := TEvClientDataSet.Create(Self);
    try
      dsED.CloneCursor(DM_CLIENT.EE_SCHEDULED_E_DS, True);
      if FWasNewCS then begin
        dsCSDD.CloneCursor(DM_CLIENT.EE_CHILD_SUPPORT_CASES, True);
        KeyField := 'EE_CHILD_SUPPORT_CASES_NBR';
      end
      else begin
        dsCSDD.CloneCursor(DM_CLIENT.EE_DIRECT_DEPOSIT, True);
        KeyField :='EE_DIRECT_DEPOSIT_NBR';
      end;
      dsED.Filtered := False;
      dsED.Filter := 'EE_NBR=' + IntToStr(FTmpEE);
      dsED.Filtered := True;
      dsCSDD.Filtered := False;
      dsCSDD.Filter := 'EE_NBR=' + IntToStr(FTmpEE);
      dsCSDD.Filtered := True;
      dsCSDD.First;
      while (dsED.Locate(KeyField, dsCSDD.FieldByName(KeyField).Value, [])) and not(dsCSDD.Eof) do dsCSDD.Next;
      if not(dsCSDD.Eof) then
        Result := True;
    finally
      dsED.Free;
      dsCSDD.Free;
    end;
    FWasNewDD := False;
    FWasNewCS := False;
  end;
end;

function TEDIT_EE_SCHEDULED_E_DS.CanClose: boolean;
begin
  Result := inherited CanClose;
  if Result then begin
    if FWasNewCS or FWasNewDD then
      if CheckMissedCSDD then
        if EvMessage('Either a Child Support Case or a Direct Deposit account has been set up but not linked to a Scheduled E/D.'#13#10 +
          'Do you want to fix this now?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then begin
          PageControl1.ActivatePage(tshtBrowse_Scheduled_E_DS);
          Result := False;
        end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.ResultFIELDS_EE_BANK_Function;
Begin
  if csLoading in ComponentState then
    Exit;
  if ctx_AccountRights.Functions.GetState('FIELDS_EE_BANK_') <> stEnabled then
   Begin
    dedtBank_Account_Nbr.Enabled := False;
    mnavDirectDeposit.SpeedButton1.Enabled := false;
    mnavDirectDeposit.SpeedButton2.Enabled := false;
    wwDBLookupCombo1.Enabled := False;
   end;
End;

procedure TEDIT_EE_SCHEDULED_E_DS.CalcEffectiveEndDate;
var
  EDT: String;
begin
  if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) and DM_CLIENT.CL_E_DS.Active and (Trim(wwlcE_D.Text) <> '') then
  begin
    EDT := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwlcE_D.LookupTable['CL_E_DS_NBR'],'E_D_CODE_TYPE');
    if ((EDT = 'MC') or (((EDT = 'M1') or (EDT = 'M4')) and
      ( Pos('COBRA', UpperCase(VarToStr(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', wwlcE_D.LookupTable['CL_E_DS_NBR'],'DESCRIPTION')))) > 0 ))) then
    begin
      if (dEffectiveStartDate.Text <> '') then
        wwdsDetail.DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime := IncMonth(dEffectiveStartDate.Date, 15)
      else
        wwdsDetail.DataSet.FieldByName('EFFECTIVE_END_DATE').AsDateTime := IncMonth(Date, 15);
    end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dEffectiveStartDateChange(
  Sender: TObject);
begin
  if csLoading in ComponentState then
    Exit;
  inherited;
  CalcEffectiveEndDate;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dEffectiveStartDateExit(Sender: TObject);
begin
  inherited;
  CalcEffectiveEndDate;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dEffectiveStartDateKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  CalcEffectiveEndDate;
end;


procedure TEDIT_EE_SCHEDULED_E_DS.wwdsEmployeeDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;  

  if FActivated and  Assigned(evcbEEBenefitReference) then
     FidxBenefitReference := -1;

  if FActivated and (PageControl1.ActivePage = tshtBrowse_Scheduled_E_DS) and cdEEScheduledView.Active then
     FillcdEEScheduledView(-1);

  if FActivated and (PageControl1.ActivePage = tshtDetail_1) and cdEEBenefits.Active then
     FillcdEEBenefits

end;

procedure TEDIT_EE_SCHEDULED_E_DS.evBtnEEBenefitClick(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).StoreEditDSForswitchScreen(GetFrameClassForTable('EE_BENEFITS'));
  ActivateFrameAs('HR Module - Employee - Benefits', [wwdsEmployee.DataSet.FieldByName('EE_NBR').AsInteger], 2);

end;


procedure TEDIT_EE_SCHEDULED_E_DS.pnlFashionBrowseEDsResize(
  Sender: TObject);
begin
  inherited;
//  pnlBrowseAlign.Width := pnlFashionBrowse.Width - 40;
//  pnlBrowseAlign.Height := pnlFashionBrowse.Height - 65;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 2;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.dsEEScheduledViewDataChange(
  Sender: TObject; Field: TField);
var
  b:boolean;
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  if (cdEEScheduledView.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger > 0) and
     (cdEEScheduledView.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger <> wwdsDetail.DataSet.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger) then
    wwdsDetail.DataSet.Locate('EE_SCHEDULED_E_DS_NBR',cdEEScheduledView.FieldByName('EE_SCHEDULED_E_DS_NBR').AsInteger,[]);

  if wwdsDetail.DataSet.RecordCount > 0 then
  begin
    b := wwdsDetail.DataSet.FieldByName('SCHEDULED_E_D_ENABLED').asString[1] = GROUP_BOX_NO;
    SetControlsReccuring(tshtDetail_1, 'SecurityRO', Integer(b));
    SetControlsReccuring(tshtDetail_2, 'SecurityRO', Integer(b));
    SetControlsReccuring(tshtChildSupport, 'SecurityRO', Integer(b));
    SetControlsReccuring(tshtDirectDeposits, 'SecurityRO', Integer(b));
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.grdEESchedViewTitleButtonClick(
  Sender: TObject; AFieldName: String);
var
 i: integer;
begin
  inherited;
  if cdEEScheduledView.IndexName = 'SORT' then
  begin
    i := cdEEScheduledView.IndexDefs.IndexOf('SORT');
    if i <> -1 then
    begin
      if (wwdsDetail.DataSet as TevClientDataset).IndexDefs.IndexOf('SORT') > -1 then
         (wwdsDetail.DataSet as TevClientDataset).DeleteIndex('SORT');

      (wwdsDetail.DataSet as TevClientDataset).AddIndex('SORT',
         cdEEScheduledView.IndexDefs[i].Fields,
          cdEEScheduledView.IndexDefs[i].Options,
            cdEEScheduledView.IndexDefs[i].DescFields);
      (wwdsDetail.DataSet as TevClientDataset).indexName := 'SORT';
    end;
  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  if (wwdsSubMaster.DataSet.Active) and (wwdsSubMaster.DataSet.State = dsBrowse)
     and not assigned(Field) then
  begin
    if cdsDependentHealthCode.Active then cdsDependentHealthCode.close;
    cdsDependentHealthCode.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
    with TExecDSWrapper.Create('select c.custom_e_d_code_number, c.cl_e_ds_nbr  from ee_scheduled_e_ds e, cl_e_ds c '#13 +
                               'where e.cl_e_ds_nbr = c.cl_e_ds_nbr '#13 +
                               'and ( (c.e_d_code_type starting with ''D'') or (c.e_d_code_type starting with ''M'') ) '#13 +
                               'and e.EE_nbr = :EE_nbr and {AsOfNow<e>} and {AsOfNow<c>} ') do
    begin
      SetParam('EE_nbr', ConvertNull(wwdsSubMaster.DataSet['EE_NBR'], 0));
      cdsDependentHealthCode.DataRequest(AsVariant);
    end;
    cdsDependentHealthCode.Open;

    FFilteredDS := nil;
    if DM_COMPANY.EE_SCHEDULED_E_DS.Active then
       FFilteredDS := TevDataSet.Create(DM_COMPANY.EE_SCHEDULED_E_DS);

  end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.detailBenefitAmount;
var
   pAmount: variant;
   RateType: String;
begin
  edcAMOUNT.Text:= '';
  edcPERCENTAGE.Text:= '';
  FindBenefitAmount((DM_EMPLOYEE.EE_SCHEDULED_E_DS as TevClientDataset), pAmount, RateType);

  if not VarIsNull(pAmount) then
  begin
    edcAMOUNT.Text :=  FloatToStrF(pAmount, ffCurrency, 10, 2);
    edcAMOUNT.Text := FormatFloat('#######0.00', pAmount);
    edcAMOUNT.Text := Format('%13s', [edcAMOUNT.Text]);
  end;

  if RateType = BENEFIT_RATES_RATE_TYPE_PERCENT then
  begin
      edcPERCENTAGE.Text:= edcAMOUNT.Text;
      edcAMOUNT.Text:= '';
      wwcbCalculation_Type.Enabled := True
  end
  else
  begin
      wwcbCalculation_Type.Enabled := False;
      if DM_EMPLOYEE.EE_SCHEDULED_E_DS.State in [dsInsert, dsEdit] then
        if DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString <> CALC_METHOD_FIXED then
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString := CALC_METHOD_FIXED;
  end;

end;

procedure TEDIT_EE_SCHEDULED_E_DS.FillcdEEBenefits;
begin
      if not DM_EMPLOYEE.EE_BENEFITS.Active then
         DM_EMPLOYEE.EE_BENEFITS.DataRequired('ALL');

      if not DM_COMPANY.CO_BENEFIT_RATES.Active then
         DM_COMPANY.CO_BENEFIT_RATES.DataRequired('ALL');

      DM_COMPANY.CO_BENEFITS.IndexFieldNames := 'CO_BENEFITS_NBR';
      DM_COMPANY.CO_BENEFIT_SUBTYPE.IndexFieldNames := 'CO_BENEFIT_SUBTYPE_NBR';

      cdEEBenefits.DisableControls;
      try
          cdEEBenefits.EmptyDataSet;
          if cdEEBenefits.FieldDefs.Count = 0 then
             cdEEBenefits.CreateDataSet;

          DM_COMPANY.EE_BENEFITS.IndexFieldNames := 'EE_NBR';
          DM_COMPANY.EE_BENEFITS.SetRange([DM_CLIENT.EE['EE_NBR']], [DM_CLIENT.EE['EE_NBR']]);
          DM_EMPLOYEE.EE_BENEFITS.First;
          while not DM_EMPLOYEE.EE_BENEFITS.Eof do
          begin
             cdEEBenefits.Append;
             cdEEBenefits['EE_BENEFITS_NBR'] := DM_EMPLOYEE.EE_BENEFITS['EE_BENEFITS_NBR'];

             cdEEBenefits['BENEFIT_EFFECTIVE_DATE'] := DM_EMPLOYEE.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE'];
             cdEEBenefits['EXPIRATION_DATE'] := DM_EMPLOYEE.EE_BENEFITS['EXPIRATION_DATE'];

             if not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['CO_BENEFITS_NBR']) then
               if DM_COMPANY.CO_BENEFITS.FindKey([DM_EMPLOYEE.EE_BENEFITS['CO_BENEFITS_NBR']]) then
                   cdEEBenefits['Benefit'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];

             if not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR']) then
                if DM_COMPANY.CO_BENEFIT_SUBTYPE.FindKey([DM_EMPLOYEE.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR']]) then
                  cdEEBenefits['BenefitAmountType'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['DESCRIPTION'];

             if  (not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'])) and
                   GetCoBenefitRateNumber(DM_EMPLOYEE.EE_BENEFITS['CO_BENEFIT_SUBTYPE_NBR'],
                        ConvertNull(DM_EMPLOYEE.EE_BENEFITS['BENEFIT_EFFECTIVE_DATE'],Now), ConvertNull(DM_EMPLOYEE.EE_BENEFITS['EXPIRATION_DATE'], Now))  then
             begin

                if (VarIsNull(DM_EMPLOYEE.EE_BENEFITS['EE_RATE'])) and
                   (VarIsNull(DM_EMPLOYEE.EE_BENEFITS['ER_RATE'])) and
                   (VarIsNull(DM_EMPLOYEE.EE_BENEFITS['COBRA_RATE'])) and
                   (VarIsNull(DM_EMPLOYEE.EE_BENEFITS['TOTAL_PREMIUM_AMOUNT'])) then
                begin
                  cdEEBenefits['EE_RATE']    := DM_COMPANY.CO_BENEFIT_RATES['EE_RATE'];
                  cdEEBenefits['ER_RATE']    := DM_COMPANY.CO_BENEFIT_RATES['ER_RATE'];
                  cdEEBenefits['COBRA_RATE'] := DM_COMPANY.CO_BENEFIT_RATES['COBRA_RATE'];
                  cdEEBenefits.FieldByName('TOTAL_PREMIUM_AMOUNT').AsFloat := cdEEBenefits.fieldByName('EE_RATE').AsFloat + cdEEBenefits.fieldByName('ER_RATE').AsFloat;
                  cdEEBenefits['ReferenceType'] := 'Client';
                end;
             end;

             if (not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['EE_RATE'])) or
                (not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['ER_RATE'])) or
                (not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['COBRA_RATE'])) or
                (not VarIsNull(DM_EMPLOYEE.EE_BENEFITS['TOTAL_PREMIUM_AMOUNT'])) then
             begin
               cdEEBenefits['EE_RATE'] := DM_EMPLOYEE.EE_BENEFITS['EE_RATE'];
               cdEEBenefits['ER_RATE'] := DM_EMPLOYEE.EE_BENEFITS['ER_RATE'];
               cdEEBenefits['COBRA_RATE'] := DM_EMPLOYEE.EE_BENEFITS['COBRA_RATE'];
               cdEEBenefits['TOTAL_PREMIUM_AMOUNT'] := DM_EMPLOYEE.EE_BENEFITS['TOTAL_PREMIUM_AMOUNT'];
               cdEEBenefits['ReferenceType'] := 'Employee';
             end;
             cdEEBenefits.Post;

             DM_EMPLOYEE.EE_BENEFITS.Next;
          end;
      finally
       DM_EMPLOYEE.EE_BENEFITS.CancelRange;
       DM_COMPANY.CO_BENEFITS.IndexFieldNames := '';
       DM_COMPANY.CO_BENEFIT_RATES.IndexFieldNames := '';
       cdEEBenefits.EnableControls;
       if not wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').IsNull then
          cdEEBenefits.Locate('EE_BENEFITS_NBR',wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').Value,[])
       else
          cdEEBenefits.First;
      end;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.FindBenefitAmount(const cdEEScheduled: TevClientDataSet; var amount: Variant; var RateType: String);
begin
  RateType := '';

  if (not VarIsNull(cdEEScheduled['CO_BENEFIT_SUBTYPE_NBR'])) then
  begin
    ctx_RemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(
      cdEEScheduled.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger,
      cdEEScheduled.FieldByName('CL_E_DS_NBR').AsInteger,
      ConvertNull(cdEEScheduled['EFFECTIVE_START_DATE'], 0),
      ConvertNull(cdEEScheduled['EFFECTIVE_END_DATE'], 0), RateType, amount);

     if amount = Null then
       amount := cdEEScheduled.FieldByName('AMOUNT').Value;
  end
  else
  if (cdEEScheduled.FieldByName('EE_BENEFITS_NBR').AsInteger > 0) and
      (cdEEScheduled.FieldByName('CO_NBR').AsInteger > 0) then
    ctx_RemoteMiscRoutines.CalcEEScheduled_E_DS_Rate(
        cdEEScheduled.FieldByName('EE_BENEFITS_NBR').AsInteger,
        cdEEScheduled.FieldByName('CL_E_DS_NBR').AsInteger,
        ConvertNull(cdEEScheduled['EFFECTIVE_START_DATE'], 0),
        ConvertNull(cdEEScheduled['EFFECTIVE_END_DATE'], 0),
        cdEEScheduled.FieldByName('AMOUNT').Value,
        cdEEScheduled.FieldByName('ANNUAL_MAXIMUM_AMOUNT').Value,
        cdEEScheduled.FieldByName('TARGET_AMOUNT').Value,
        cdEEScheduled.FieldByName('FREQUENCY').asString,
        RateType, amount);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.FillcdEEScheduledView(const EEScheduledNbr: integer);
var
  cd: TevClientDataSet;
  pAmount: Variant;
  RateType: String;
  i: Integer;
begin

  cdEEScheduledView.DisableControls;
  cdEEScheduledView.BeforeScroll := nil;
  try
    cdEEScheduledView.EmptyDataSet;
    if cdEEScheduledView.FieldDefs.Count = 0 then
       cdEEScheduledView.CreateDataSet;

    cd := TevClientDataSet.Create(nil);
    try
      cd.CloneCursor(DM_EMPLOYEE.EE_SCHEDULED_E_DS, True);
      if DM_CLIENT.EE_SCHEDULED_E_DS.State = dsEdit then
      begin
        if cd.Locate('EE_SCHEDULED_E_DS_NBR', DM_CLIENT.EE_SCHEDULED_E_DS['EE_SCHEDULED_E_DS_NBR'], []) then
        begin
          cd.Edit;
          for i := 0 to cd.FieldCount - 1 do
          begin
            if cd.Fields[i].FieldName <> 'EE_SCHEDULED_E_DS_NBR' then
              cd.Fields[i].Value := DM_CLIENT.EE_SCHEDULED_E_DS.Fields[i].Value;
          end;
          cd.Post;
        end;
      end;
      cd.IndexFieldNames:= 'EE_NBR';
      cd.SetRange([DM_CLIENT.EE.FieldByName('EE_NBR').AsInteger], [DM_CLIENT.EE.FieldByName('EE_NBR').AsInteger]);
      cd.First;
      while not cd.Eof do
      begin
        cdEEScheduledView.Append;
        cdEEScheduledView['EE_SCHEDULED_E_DS_NBR'] := cd['EE_SCHEDULED_E_DS_NBR'];
        cdEEScheduledView['EE_NBR'] := cd['EE_NBR'];
        cdEEScheduledView['CUSTOM_E_D_CODE_NUMBER'] := cd['CUSTOM_E_D_CODE_NUMBER'];
        cdEEScheduledView['DESCRIPTION'] := cd['DESCRIPTION'];
        cdEEScheduledView['CALCULATION_TYPE'] := cd['CALCULATION_TYPE'];
        cdEEScheduledView['SCHEDULED_E_D_ENABLED'] := cd['SCHEDULED_E_D_ENABLED'];

        if (not cd.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull) or
           (not cd.FieldByName('EE_BENEFITS_NBR').IsNull)  then
        begin
          FindBenefitAmount(cd,pAmount, RateType);
          if not varisnull(pAmount) then
          begin
            if RateType = BENEFIT_RATES_RATE_TYPE_PERCENT then
              cdEEScheduledView['PERCENTAGE'] := pAmount
            else
              cdEEScheduledView['AMOUNT'] := pAmount;
          end
        end
        else
        begin
          cdEEScheduledView['AMOUNT'] := cd['cAMOUNT'];
          cdEEScheduledView['PERCENTAGE'] := cd['cPERCENTAGE'];
        end;
        cdEEScheduledView['FREQUENCY'] := cd['FREQUENCY'];
        cdEEScheduledView['EFFECTIVE_START_DATE'] := cd['EFFECTIVE_START_DATE'];
        cdEEScheduledView['EFFECTIVE_END_DATE'] := cd['EFFECTIVE_END_DATE'];
        cdEEScheduledView.Post;
        cd.Next;
      end;
    finally
      cd.Free;
    end;
  finally
    cdEEScheduledView.UserFiltered := True;
    cdEEScheduledView.EnableControls;
    if EEScheduledNbr > 0 then
      cdEEScheduledView.Locate('EE_SCHEDULED_E_DS_NBR',EEScheduledNbr,[])
    else
      cdEEScheduledView.First;
    cdEEScheduledView.BeforeScroll := BeforeScroll;
  end;
end;


procedure TEDIT_EE_SCHEDULED_E_DS.grdEESchedViewDblClick(Sender: TObject);
begin
  inherited;
  FillcdEEBenefits;
  SetBenefitReference;
  PageControl1.ActivePageIndex := 2;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.edcAMOUNTExit(Sender: TObject);
begin
  if DM_CLIENT.EE_SCHEDULED_E_DS.State = dsEdit then
    DM_CLIENT.EE_SCHEDULED_E_DS.Post;
end;

procedure TEDIT_EE_SCHEDULED_E_DS.AfterPost(Dataset: TDataset);
begin
  if not (Owner as TEmployeeFrm).EDChangeList.Locate('ClNbr;EeNbr;CUSTOM_E_D_CODE_NUMBER', VarArrayOf([DM_COMPANY.CO.ClientID,
    DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR'], DM_CLIENT.EE_SCHEDULED_E_DS['CUSTOM_E_D_CODE_NUMBER']]), []) then
    (Owner as TEmployeeFrm).EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, DM_EMPLOYEE.EE_SCHEDULED_E_DS['EE_NBR'], DM_CLIENT.EE_SCHEDULED_E_DS['CUSTOM_E_D_CODE_NUMBER']]);
  if Assigned(FAfterPost) then
    FAfterPost(Dataset);
end;

procedure TEDIT_EE_SCHEDULED_E_DS.BeforeScroll(Dataset: TDataset);
begin
  with Owner as TFramePackageTmpl do
    if btnNavOk.Enabled or btnNavCancel.Enabled then
       raise EevException.Create('You must post or cancel changes.');
end;

function TEDIT_EE_SCHEDULED_E_DS.MayHideOrUnhideSchedEE(DataSet: TevClientDataSet): boolean;
begin
  Result := not (Owner as TFramePackageTmpl).IsModified;
end;
procedure TEDIT_EE_SCHEDULED_E_DS.HideRecordHelperbutnNextClick(
  Sender: TObject);
begin
  inherited;
  cdEEScheduledView.BeforeScroll := nil;
  try
    HideRecordHelper.HideCurRecordExecute(Sender);
  finally
    cdEEScheduledView.BeforeScroll := BeforeScroll;
  end;

end;
initialization
  classes.RegisterClass(TEDIT_EE_SCHEDULED_E_DS);

end.

