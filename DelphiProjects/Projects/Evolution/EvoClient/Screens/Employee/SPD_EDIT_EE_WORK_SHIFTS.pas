// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_WORK_SHIFTS;

interface

uses
   SDataStructure, wwdbedit, SPD_EDIT_EE_BASE, wwdblook,
  StdCtrls, Mask, ExtCtrls, DBActns, Classes, ActnList, Db, Wwdatsrc,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, DBCtrls, Controls,
   SPackageEntry, SDDClasses, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBEdit,
  ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_WORK_SHIFTS = class(TEDIT_EE_BASE)
    tshtWork_Shifts: TTabSheet;
    EEStatesDS: TevClientDataSet;
    fpShiftSummary: TisUIFashionPanel;
    lablAuto_Shift: TevLabel;
    wwdgWork_Shifts: TevDBGrid;
    wwlcAuto_Shift: TevDBLookupCombo;
    sbShifts: TScrollBox;
    fpShiftDetail: TisUIFashionPanel;
    lablShift: TevLabel;
    wwlcShift: TevDBLookupCombo;
    lablRate: TevLabel;
    dedtRate: TevDBEdit;
    dedtShift_Percentage: TevDBEdit;
    lablShift_Percentage: TevLabel;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure PageControl1Change(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                           var InsertDataSets, EditDataSets: TArrayDS;
                           var DeleteDataSet: TevClientDataSet;
                           var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_EE_WORK_SHIFTS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  if Kind = NavRefresh then
  begin
    EEStatesDS.RetrieveCondition := '';
    EEStatesDS.Close;
  end;
end;

procedure TEDIT_EE_WORK_SHIFTS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_SHIFTS, aDS);
  EEStatesDS.RetrieveCondition := '';
  EEStatesDS.Close;
  DM_EMPLOYEE.EE_WORK_SHIFTS.Close;
  inherited;
end;

function TEDIT_EE_WORK_SHIFTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_WORK_SHIFTS;
end;
         
function TEDIT_EE_WORK_SHIFTS.GetInsertControl: TWinControl;
begin
  Result := wwlcShift;
end;

procedure TEDIT_EE_WORK_SHIFTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE, EditDataSets);
end;

procedure TEDIT_EE_WORK_SHIFTS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if EEStatesDS.Active then
    EEStatesDS.RetrieveCondition := DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition;
end;

procedure TEDIT_EE_WORK_SHIFTS.PageControl1Change(Sender: TObject);
begin
  inherited;
  if not EEStatesDS.Active or (EEStatesDS.RecordCount = 0) then
    wwlcAuto_Shift.Text := '';
end;

procedure TEDIT_EE_WORK_SHIFTS.AfterDataSetsReopen;
begin
  inherited;
  EEStatesDS.CloneCursor(DM_EMPLOYEE.EE_WORK_SHIFTS, True);
  EEStatesDS.RetrieveCondition := DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition;
end;

initialization
  RegisterClass(TEDIT_EE_WORK_SHIFTS);

end.
