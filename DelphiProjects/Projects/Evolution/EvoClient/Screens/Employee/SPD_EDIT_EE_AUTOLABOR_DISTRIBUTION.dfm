inherited EDIT_EE_AUTOLABOR_DISTRIBUTION: TEDIT_EE_AUTOLABOR_DISTRIBUTION
  Width = 803
  Height = 499
  inherited Panel1: TevPanel
    Width = 803
    inherited pnlFavoriteReport: TevPanel
      Left = 651
    end
    inherited pnlTopLeft: TevPanel
      Width = 651
    end
  end
  inherited PageControl1: TevPageControl
    Width = 803
    Height = 412
    HelpContext = 16501
    ActivePage = tshtAuto_Labor_Distribution
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 795
        Height = 383
        inherited pnlBorder: TevPanel
          Width = 791
          Height = 379
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 791
          Height = 379
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 743
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 743
            Height = 272
            inherited Splitter1: TevSplitter
              Height = 268
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 268
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 188
                IniAttributes.SectionName = 'TEDIT_EE_AUTOLABOR_DISTRIBUTION\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 421
              Height = 268
              inherited wwDBGrid4: TevDBGrid
                Width = 371
                Height = 188
                IniAttributes.SectionName = 'TEDIT_EE_AUTOLABOR_DISTRIBUTION\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtAuto_Labor_Distribution: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbLaborDist: TScrollBox
        Left = 0
        Top = 0
        Width = 795
        Height = 383
        VertScrollBar.Position = 66
        Align = alClient
        TabOrder = 0
        object pnlListOfDistrubutions: TisUIFashionPanel
          Left = 8
          Top = -58
          Width = 550
          Height = 251
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlListOfDistrubutions'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablE_D_Group: TevLabel
            Left = 12
            Top = 35
            Width = 61
            Height = 16
            Caption = '~E/D Group'
            FocusControl = wwlcE_D_Group
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblTotal: TevLabel
            Left = 451
            Top = 35
            Width = 76
            Height = 13
            Caption = 'Total Allocation '
          end
          object wwdgAuto_Labor_Distribution: TevDBGrid
            Left = 12
            Top = 84
            Width = 515
            Height = 145
            HelpContext = 16501
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_DIVISION_NUMBER'#9'15'#9'Division Code'
              'CUSTOM_BRANCH_NUMBER'#9'10'#9'Branch Code'
              'CUSTOM_DEPARTMENT_NUMBER'#9'10'#9'Department Code'
              'CUSTOM_TEAM_NUMBER'#9'18'#9'Team Code'
              'JobDescription'#9'8'#9'Job'
              'PERCENTAGE'#9'10'#9'Percentage'#9'F'#9
              'WorkersComp'#9'17'#9'Workers Comp ')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_AUTOLABOR_DISTRIBUTION\wwdgAuto_Labor_Distribution'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object wwlcE_D_Group: TevDBLookupCombo
            Left = 10
            Top = 50
            Width = 193
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'ALD_CL_E_D_GROUPS_NBR'
            DataSource = wwdsEmployee
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evBitBtn1: TevBitBtn
            Left = 217
            Top = 50
            Width = 75
            Height = 21
            Caption = 'Clear Dist.'
            TabOrder = 2
            OnClick = evBitBtn1Click
            Color = clBlack
            Margin = 0
          end
          object edTotalAllocation: TevEdit
            Left = 451
            Top = 50
            Width = 76
            Height = 21
            Enabled = False
            TabOrder = 3
          end
        end
        object pnlInformation: TisUIFashionPanel
          Left = 8
          Top = 201
          Width = 550
          Height = 178
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Auto Labor Distribution Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablJob: TevLabel
            Left = 309
            Top = 43
            Width = 17
            Height = 13
            Caption = 'Job'
            FocusControl = wwlcJob
          end
          object lablWorkers_Comp_Code: TevLabel
            Left = 309
            Top = 82
            Width = 98
            Height = 13
            Caption = 'Workers Comp Code'
            FocusControl = wwlcWorkers_Comp_Code
          end
          object lablPercentage: TevLabel
            Left = 309
            Top = 121
            Width = 17
            Height = 16
            Caption = '~%'
            FocusControl = dedtPercentage
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TevLabel
            Left = 12
            Top = 67
            Width = 35
            Height = 13
            Caption = 'Divison'
          end
          object Label2: TevLabel
            Left = 12
            Top = 91
            Width = 34
            Height = 13
            Caption = 'Branch'
          end
          object Label3: TevLabel
            Left = 12
            Top = 115
            Width = 55
            Height = 13
            Caption = 'Department'
          end
          object Label4: TevLabel
            Left = 12
            Top = 139
            Width = 27
            Height = 13
            Caption = 'Team'
          end
          object SpeedButton1: TevSpeedButton
            Left = 88
            Top = 35
            Width = 212
            Height = 21
            Caption = 'Assign D/B/D/T'
            HideHint = True
            AutoSize = False
            OnClick = SpeedButton1Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
              77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
              7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
              8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
              7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
              AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
              2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
              7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
            ParentColor = False
            ShortCut = 115
          end
          object dedtPercentage: TevDBEdit
            Left = 309
            Top = 136
            Width = 64
            Height = 21
            HelpContext = 16501
            DataField = 'PERCENTAGE'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcDivision: TevDBLookupCombo
            Left = 198
            Top = 64
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 1
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcBranch: TevDBLookupCombo
            Left = 198
            Top = 88
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 2
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcDepartment: TevDBLookupCombo
            Left = 198
            Top = 112
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcTeam: TevDBLookupCombo
            Left = 198
            Top = 136
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 4
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcJob: TevDBLookupCombo
            Left = 309
            Top = 58
            Width = 219
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_JOBS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_JOBS.CO_JOBS
            LookupField = 'CO_JOBS_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcWorkers_Comp_Code: TevDBLookupCombo
            Left = 309
            Top = 97
            Width = 219
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'
              'WORKERS_COMP_CODE'#9'5'#9'WORKERS_COMP_CODE'
              'Co_State_Lookup'#9'2'#9'Co_State_Lookup'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 88
            Top = 64
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DIVISION_NUMBER'#9'10'#9'CUSTOM_DIVISION_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 7
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwDBLookupCombo2: TevDBLookupCombo
            Left = 88
            Top = 88
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BRANCH_NUMBER'#9'10'#9'CUSTOM_BRANCH_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 8
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwDBLookupCombo3: TevDBLookupCombo
            Left = 88
            Top = 112
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DEPARTMENT_NUMBER'#9'10'#9'CUSTOM_DEPARTMENT_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 9
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwDBLookupCombo4: TevDBLookupCombo
            Left = 88
            Top = 136
            Width = 102
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_TEAM_NUMBER'#9'10'#9'CUSTOM_TEAM_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 10
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 803
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_AUTOLABOR_DISTRIBUTION.EE_AUTOLABOR_DISTRIBUTION
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 321
    Top = 79
  end
  inherited wwdsEmployee: TevDataSource
    Left = 158
  end
  object pdsEmployee: TevProxyDataSet
    Dataset = DM_EE.EE
    Left = 172
    Top = 98
  end
end
