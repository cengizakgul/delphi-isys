inherited EDIT_EE_PENSION_FUND_SPLITS: TEDIT_EE_PENSION_FUND_SPLITS
  inherited PageControl1: TevPageControl
    HelpContext = 18502
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 209
                IniAttributes.SectionName = 'TEDIT_EE_PENSION_FUND_SPLITS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 209
                IniAttributes.SectionName = 'TEDIT_EE_PENSION_FUND_SPLITS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtPension_Fund_Splits: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensionSplits: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 149
        Align = alClient
        TabOrder = 0
        object pnlList: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 498
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid1: TevDBGrid
            Left = 12
            Top = 35
            Width = 462
            Height = 145
            DisableThemesInTitle = False
            Selected.Strings = (
              'Funds_lookup'#9'20'#9'Funds'#9'F'
              'E_D_Code_lookup'#9'20'#9'E/D Code'#9'F'
              'PERCENTAGE'#9'10'#9'Percentage'#9'F'
              'EMPLOYEE_OR_EMPLOYER'#9'18'#9'Employee or Employer'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_PENSION_FUND_SPLITS\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object pnlInfo: TisUIFashionPanel
          Left = 8
          Top = 219
          Width = 498
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Pension Fund Split Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablFund: TevLabel
            Left = 16
            Top = 35
            Width = 40
            Height = 13
            Caption = '~Fund'
            FocusControl = wwlcFund
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablE_D_Code: TevLabel
            Left = 137
            Top = 35
            Width = 57
            Height = 13
            Caption = '~E/D Code'
            FocusControl = wwlcE_D_Code
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPercentage: TevLabel
            Left = 259
            Top = 35
            Width = 46
            Height = 13
            Caption = '~%'
            FocusControl = dedtPercentage
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtPercentage: TevDBEdit
            Left = 259
            Top = 50
            Width = 64
            Height = 21
            DataField = 'PERCENTAGE'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwlcFund: TevDBLookupCombo
            Left = 16
            Top = 50
            Width = 110
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'FUND_NAME')
            DataField = 'CL_FUNDS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_FUNDS.CL_FUNDS
            LookupField = 'CL_FUNDS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwlcE_D_Code: TevDBLookupCombo
            Left = 137
            Top = 50
            Width = 110
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_E_D_CODE_NUMBER'#9'10'#9'CUSTOM_E_D_CODE_NUMBER'
              'DESCRIPTION'#9'40'#9'DESCRIPTION'
              'E_D_CODE_TYPE'#9'2'#9'E_D_CODE_TYPE')
            DataField = 'CL_E_DS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_DS.CL_E_DS
            LookupField = 'CL_E_DS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object drgpEE_ER: TevDBRadioGroup
            Left = 335
            Top = 35
            Width = 141
            Height = 38
            Caption = '~EE or ER'
            Columns = 2
            DataField = 'EMPLOYEE_OR_EMPLOYER'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Employee'
              'Employer')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'E'
              'R')
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_PENSION_FUND_SPLITS.EE_PENSION_FUND_SPLITS
  end
end
