inherited AskEDUpdateParamsForPercent: TAskEDUpdateParamsForPercent
  Left = 494
  Top = 422
  Width = 261
  Height = 121
  Caption = 'Update E/D Percentage'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Width = 245
    Height = 31
    object evLabel1: TevLabel
      Left = 8
      Top = 16
      Width = 109
      Height = 13
      Caption = 'Change to a Percent %'
    end
    object evEdit2: TevEdit
      Left = 120
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 0
    end
  end
  inherited evPanel2: TevPanel
    Top = 43
    Width = 245
    inherited evPanel4: TevPanel
      Left = 59
      inherited evBitBtn2: TevBitBtn
        ModalResult = 1
      end
    end
  end
  inherited evPanel3: TevPanel
    Top = 31
    Width = 245
    inherited EvBevel1: TEvBevel
      Width = 236
    end
  end
  inherited evActionList1: TevActionList
    Left = 24
    Top = 48
    inherited OK: TAction
      OnUpdate = OKUpdate
    end
  end
end
