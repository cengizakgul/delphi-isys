inherited EDIT_EE_ADDITIONAL_INFO: TEDIT_EE_ADDITIONAL_INFO
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 280
                IniAttributes.SectionName = 'TEDIT_EE_ADDITIONAL_INFO\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 280
                IniAttributes.SectionName = 'TEDIT_EE_ADDITIONAL_INFO\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbAdditional: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 149
        Align = alClient
        TabOrder = 0
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 516
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgValues: TevDBGrid
            Left = 12
            Top = 35
            Width = 480
            Height = 139
            HelpContext = 17001
            DisableThemesInTitle = False
            Selected.Strings = (
              'NAME'#9'30'#9'Name'
              'INFO_STRING'#9'30'#9'String'
              'INFO_Date'#9'12'#9'Date'
              'INFO_Amount'#9'12'#9'Amount'
              'CO_ADDITIONAL_INFO_VALUES_NBR'#9'40'#9'Internal Value #'#9'F'
              'Value'#9'40'#9'Value'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_ADDITIONAL_INFO\wwdgValues'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpDetails: TisUIFashionPanel
          Left = 221
          Top = 219
          Width = 304
          Height = 132
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDetails'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Employee Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label2: TevLabel
            Left = 12
            Top = 35
            Width = 27
            Height = 13
            Caption = 'String'
            FocusControl = StringEdit
          end
          object lablBirth_Date: TevLabel
            Left = 151
            Top = 35
            Width = 23
            Height = 13
            Caption = 'Date'
          end
          object lablSalary_Amount: TevLabel
            Left = 151
            Top = 74
            Width = 36
            Height = 13
            Caption = 'Amount'
            FocusControl = AmounTevEdit
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 74
            Width = 27
            Height = 13
            Caption = 'Value'
            FocusControl = evDBLookupComboValue
          end
          object StringEdit: TevDBEdit
            Left = 12
            Top = 50
            Width = 129
            Height = 21
            HelpContext = 17001
            DataField = 'INFO_STRING'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object DatePicker: TevDBDateTimePicker
            Left = 151
            Top = 50
            Width = 129
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'INFO_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object AmounTevEdit: TevDBEdit
            Left = 151
            Top = 89
            Width = 129
            Height = 21
            HelpContext = 19002
            DataField = 'INFO_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBLookupComboValue: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 129
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'INFO_VALUE'#9'40'#9'INFO_VALUE')
            DataField = 'CO_ADDITIONAL_INFO_VALUES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_ADDITIONAL_INFO_VALUES.CO_ADDITIONAL_INFO_VALUES
            LookupField = 'CO_ADDITIONAL_INFO_VALUES_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnBeforeDropDown = evDBLookupComboValueBeforeDropDown
            OnCloseUp = evDBLookupComboValueCloseUp
          end
        end
        object fpCompany: TisUIFashionPanel
          Left = 12
          Top = 219
          Width = 200
          Height = 132
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCompany'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Company Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label1: TevLabel
            Left = 12
            Top = 35
            Width = 38
            Height = 13
            Caption = '~Field'
            FocusControl = wwlcField
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwlcField: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 165
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_ADDITIONAL_INFO_NAMES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_ADDITIONAL_INFO_NAMES.CO_ADDITIONAL_INFO_NAMES
            LookupField = 'CO_ADDITIONAL_INFO_NAMES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_ADDITIONAL_INFO.EE_ADDITIONAL_INFO
    OnDataChange = wwdsDetailDataChange
  end
end
