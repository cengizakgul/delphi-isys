//Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_AND_CL_PERSON;

interface

uses
   SDataStructure, SPD_EDIT_EE_BASE, Dialogs, DBCtrls, EvBasicUtils,
  StdCtrls, wwdbdatetimepicker, wwdblook, ExtCtrls, Wwdotdot, Wwdbcomb,
  Mask, wwdbedit, Buttons, DBActns, Classes, ActnList, Db, Wwdatsrc, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Controls, SPD_DBDTSelectionListFiltr, EvConsts,
  SPackageEntry, SysUtils, ExtDlgs, SPD_YTD_Totals,  Variants,
  Windows, Messages, SPD_POPUP_EDIT_EE_LOCALS, EvSecElement, ShellAPI,
  EvStreamUtils, SPD_ChoiceFromListQuery, EvSendMail, ISZippingRoutines,
  Forms, SPD_HideRecordHelper, Menus, EvContext, isVCLBugFix,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvLegacy,
  EvDataAccessComponents, ISDataAccessComponents, SPD_PersonDocuments,
  SDataDictsystem, SDataDictclient, SDataDicttemp, SPD_EmployeeChoiceQuery,
  EvCommonInterfaces, EvDataSet, EvExceptions, EvMainboard, isBaseClasses, Graphics, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIDBMemo, isUIwwDBDateTimePicker, isUIwwDBComboBox,
  isUIwwDBEdit, ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIDBImage, Wwdbspin;

type
  TEDIT_EE_CL_PERSON = class(TEDIT_EE_BASE)
    tshtEmployee: TTabSheet;
    tshtHR: TTabSheet;
    tshtW2: TTabSheet;
    tshtAddress: TTabSheet;
    Notes: TTabSheet;
    tshtHR_Applicant: TTabSheet;
    memoHR_Applicant: TMemo;
    tshtFederal_Tax: TTabSheet;
    dmemNotes: TEvDBMemo;
    tshtQuick: TTabSheet;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    SYStatesSrc: TevDataSource;
    SYMaritalSrc: TevDataSource;
    wwdsSubMaster3: TevDataSource;
    wwdsSubMaster4: TevDataSource;
    wwdsCoLocalTax: TevDataSource;
    wwdsSubMasterEELocals: TevDataSource;
    evLabel3: TevLabel;
    EvDBMemo1: TEvDBMemo;
    evSecElement1: TevSecElement;
    DM_SYSTEM_HR: TDM_SYSTEM_HR;
    evLabel1: TevLabel;
    tsVMR: TTabSheet;
    evLabel20: TevLabel;
    evLabel25: TevLabel;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    evLabel13: TevLabel;
    evLabel14: TevLabel;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    tsDocuments: TTabSheet;
    HideRecordHelper: THideRecordHelper;
    TPersonDocuments1: TPersonDocuments;
    pmAccessLevel: TPopupMenu;
    imAccessLevelCopyTo: TMenuItem;
    pmOverrideFedMinWage: TevPopupMenu;
    FedMinWageCopyTo: TMenuItem;
    tsESS: TTabSheet;
    dsAvaliableGroups: TEvClientDataSet;
    dsAvaliableGroupsCO_GROUP_NBR: TIntegerField;
    dsAvaliableGroupsGROUP_NAME: TStringField;
    dsAvaliableGroupsGROUP_TYPE: TStringField;
    dsAvaliableGroupsGroupTypeName: TStringField;
    dsAssignedGroups: TEvClientDataSet;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    AvaliableGroupsDatasource: TevDataSource;
    AssignedGroupsDataSource: TevDataSource;
    pmW2FormatCopyTo: TevPopupMenu;
    miW2FormatCopy: TMenuItem;
    pmDBDTCopy: TPopupMenu;
    mnDBDTCopy: TMenuItem;
    pmPosition: TPopupMenu;
    mnPositionCopy: TMenuItem;
    ScrollBox1: TScrollBox;
    pnlEmployeeInfo: TisUIFashionPanel;
    Label45: TevLabel;
    Label46: TevLabel;
    Label47: TevLabel;
    Label48: TevLabel;
    Label62: TevLabel;
    Label63: TevLabel;
    Label64: TevLabel;
    Label65: TevLabel;
    Label66: TevLabel;
    Label67: TevLabel;
    Label68: TevLabel;
    Label69: TevLabel;
    Label44: TevLabel;
    DBEdit20: TevDBEdit;
    DBEdit21: TevDBEdit;
    DBEdit22: TevDBEdit;
    DBEdit23: TevDBEdit;
    DBEdit24: TevDBEdit;
    DBEdit27: TevDBEdit;
    DBEdit28: TevDBEdit;
    DBEdit29: TevDBEdit;
    DBEdit30: TevDBEdit;
    DBEdit31: TevDBEdit;
    DBEdit32: TevDBEdit;
    DBEdit33: TevDBEdit;
    DBEdit34: TevDBEdit;
    fpLaborDefaults: TisUIFashionPanel;
    Label75: TevLabel;
    Label76: TevLabel;
    Label77: TevLabel;
    Label78: TevLabel;
    btnDBDTEdit: TevSpeedButton;
    cbDivisionName: TevDBLookupCombo;
    cbBranchName: TevDBLookupCombo;
    cbDepartmentName: TevDBLookupCombo;
    cbTeamName: TevDBLookupCombo;
    cbDivisionNbr: TevDBLookupCombo;
    cbBranchNbr: TevDBLookupCombo;
    cbDepartmentNbr: TevDBLookupCombo;
    cbTeamNbr: TevDBLookupCombo;
    evSpeedButton1: TevSpeedButton;
    evSpeedButton2: TevSpeedButton;
    fpPay: TisUIFashionPanel;
    Label70: TevLabel;
    Label71: TevLabel;
    Label72: TevLabel;
    Label73: TevLabel;
    Label74: TevLabel;
    evLabel5: TevLabel;
    DBEdit35: TevDBEdit;
    DBEdit36: TevDBEdit;
    wwDBComboBox8: TevDBComboBox;
    DBEdit37: TevDBEdit;
    DBEdit38: TevDBEdit;
    edtAvHours: TevDBEdit;
    fpPosition: TisUIFashionPanel;
    fpTaxationDetails: TisUIFashionPanel;
    Label54: TevLabel;
    Label56: TevLabel;
    Label57: TevLabel;
    Label58: TevLabel;
    Label59: TevLabel;
    Label60: TevLabel;
    DBRadioGroup5: TevDBRadioGroup;
    DBEdit25: TevDBEdit;
    wwlcState: TevDBLookupCombo;
    wwlcMarital_Status: TevDBLookupCombo;
    wwlcSDI: TevDBLookupCombo;
    wwlcSUI: TevDBLookupCombo;
    DBEdit26: TevDBEdit;
    pnlAdditionalDetails: TisUIFashionPanel;
    lablPosition_Effective_Date: TevLabel;
    lablTime_Clock_Number: TevLabel;
    evLabel22: TevLabel;
    wwlcPosition: TevDBLookupCombo;
    wwDBDateTimePicker8: TevDBDateTimePicker;
    drgpFLSA_Exempt: TevDBRadioGroup;
    evDBEdit2: TevDBEdit;
    evDBComboBox7: TevDBComboBox;
    evDBRadioGroup5: TevDBRadioGroup;
    evDBRadioGroup6: TevDBRadioGroup;
    lablPosition: TevLabel;
    pnlDistribution: TisUIFashionPanel;
    lablLabor_Distribution_Options: TevLabel;
    lablAuto_Labor_Distribution_E_D_Group: TevLabel;
    lablPay_Group: TevLabel;
    lablCall_From: TevLabel;
    lablCall_To: TevLabel;
    Label35: TevLabel;
    lablGroup_Term_Policy_Amount: TevLabel;
    evLabel9: TevLabel;
    evLabel19: TevLabel;
    evLabel26: TevLabel;
    wwcbLabor_Distribution_Options: TevDBComboBox;
    wwlcAuto_Labor_Distribution_E_D_Group: TevDBLookupCombo;
    wwlcPay_Group: TevDBLookupCombo;
    wwDBDateTimePicker14: TevDBDateTimePicker;
    wwDBDateTimePicker15: TevDBDateTimePicker;
    DBEdit1: TevDBEdit;
    dedtGroup_Term_Policy_Amount: TevDBEdit;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBEdit5: TevDBEdit;
    evDBEdit6: TevDBEdit;
    evLabel48: TevLabel;
    pnlSecondaryDetails: TisUIFashionPanel;
    YTDBtn: TevSpeedButton;
    evLabel12: TevLabel;
    evLabel18: TevLabel;
    drgpTipped_Directly: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup1: TevDBRadioGroup;
    evDBRadioGroup4: TevDBRadioGroup;
    wwlcCheck_Template: TevDBLookupCombo;
    evDBComboBox4: TevDBComboBox;
    evBitBtn2: TevBitBtn;
    pnlStandardAddress: TisUIFashionPanel;
    lablAddress_1: TevLabel;
    lablThird_Phone: TevLabel;
    lablSecondary_Phone: TevLabel;
    lablPrimary_Phone: TevLabel;
    lablCounty: TevLabel;
    lablZip_Code: TevLabel;
    lablState: TevLabel;
    lablCity: TevLabel;
    lablAddress_2: TevLabel;
    evLabel8: TevLabel;
    dedtAddress_1: TevDBEdit;
    dedtAddress_2: TevDBEdit;
    dedtCity: TevDBEdit;
    dedtState: TevDBEdit;
    dedtZip_Code: TevDBEdit;
    dedtCounty: TevDBEdit;
    dedtPrimary_Phone: TevDBEdit;
    dedtSecondary_Phone: TevDBEdit;
    dedtThird_Phone: TevDBEdit;
    dedtThird_Phone_Description: TevDBEdit;
    dedtPrimary_Phone_Description: TevDBEdit;
    dedtSecondary_Phone_Description: TevDBEdit;
    edCountry: TevDBEdit;
    pnlAlternateAddress: TisUIFashionPanel;
    dedtAlternate_Address_1: TevDBEdit;
    dedtAlternate_Address_2: TevDBEdit;
    dedtAlternate_City: TevDBEdit;
    dedtAlternate_State: TevDBEdit;
    dedtAlternate_Zip_Code: TevDBEdit;
    dedtAlternate_County: TevDBEdit;
    dedtAlternate_Primary_Phone: TevDBEdit;
    dedtAlternate_Secondary_Phone: TevDBEdit;
    dedtAlternate_Third_Phone: TevDBEdit;
    dedtAlternate_Third_Phone_Description: TevDBEdit;
    dedtAlternate_Secondary_Phone_Description: TevDBEdit;
    dedtAlternate_Primary_Phone_Description: TevDBEdit;
    pnlPrimary: TisUIFashionPanel;
    lablW2_First_Name: TevLabel;
    lablW2_Middle_Name: TevLabel;
    lablW2_Last_Name: TevLabel;
    lablW2_Name_Suffix: TevLabel;
    lablW2_Social_Security_Number: TevLabel;
    dedtW2_First_Name: TevDBEdit;
    dedtW2_Middle_Name: TevDBEdit;
    dedtW2_Last_Name: TevDBEdit;
    dedtW2_Name_Suffix: TevDBEdit;
    dedtW2_Social_Security_Number: TevDBEdit;
    drgpTreat_SS_EIN: TevDBRadioGroup;
    drgpTreat_Company_Individual: TevDBRadioGroup;
    pnlPrimaryFederal: TisUIFashionPanel;
    lablOverride_Fed_Tax_Type: TevLabel;
    lablOverride_Fed_Tax_Value: TevLabel;
    Label55: TevLabel;
    evLabel31: TevLabel;
    evLabel34: TevLabel;
    wwcbOverride_Fed_Tax_Type: TevDBComboBox;
    dedtOverride_Fed_Tax_Value: TevDBEdit;
    wwDBComboBox7: TevDBComboBox;
    dedtFUIRateCreditOverride: TevDBEdit;
    dedtOverrideFedMinWage: TevDBEdit;
    lablDependents: TevLabel;
    lablEIC: TevLabel;
    drgpFederal_Marital_Status: TevDBRadioGroup;
    dedtDependents: TevDBEdit;
    wwcbEIC: TevDBComboBox;
    pnlEffectiveDateFederal: TisUIFashionPanel;
    drgpER_OASDI: TevDBRadioGroup;
    drgpEmployee_OASDI: TevDBRadioGroup;
    drgpER_Medicare: TevDBRadioGroup;
    drgpEmployee_Medicare: TevDBRadioGroup;
    drgpEmployer_FUI: TevDBRadioGroup;
    drgpEE_Federal: TevDBRadioGroup;
    sbNotes: TScrollBox;
    sbFederal: TScrollBox;
    sbW2: TScrollBox;
    sbAddress: TScrollBox;
    sbDetails: TScrollBox;
    pnlPayrollNotes: TisUIFashionPanel;
    pnlGeneralNotes: TisUIFashionPanel;
    pnlPicture: TisUIFashionPanel;
    lablPicture: TevLabel;
    dimgPicture: TevDBImage;
    bbtnLoad: TevBitBtn;
    fpVisa: TisUIFashionPanel;
    sbHR: TScrollBox;
    sbDocs: TScrollBox;
    sbMailRoom: TScrollBox;
    isUIFashionPanel1: TisUIFashionPanel;
    sbSelfServe: TScrollBox;
    pnlSSBorder: TevPanel;
    pnlAccessSettings: TisUIFashionPanel;
    evLabel151: TevLabel;
    evLabel321: TevLabel;
    evGroupBox51: TevGroupBox;
    dgrpSS_Enabled: TevDBRadioGroup;
    dgrpTimeOff_Enabled: TevDBRadioGroup;
    dgrpBenefits_Enabled: TevDBRadioGroup;
    dedtSS_UserName: TevDBEdit;
    editPassword: TevEdit;
    btnUnblockAcc: TevButton;
    pnlGroupAssignments: TisUIFashionPanel;
    wwDBDateTimePicker16: TevDBDateTimePicker;
    Label79: TevLabel;
    evDBComboBox1: TevDBComboBox;
    Label53: TevLabel;
    wwDBComboBox6: TevDBComboBox;
    evDBEdit1: TevDBEdit;
    lblTribe: TevLabel;
    fpHireStatus: TisUIFashionPanel;
    Label49: TevLabel;
    Label50: TevLabel;
    Label51: TevLabel;
    Label61: TevLabel;
    evLabel35: TevLabel;
    wwDBComboBox5: TevDBComboBox;
    wwDBDateTimePicker17: TevDBDateTimePicker;
    wwDBDateTimePicker18: TevDBDateTimePicker;
    wwDBDateTimePicker19: TevDBDateTimePicker;
    evDBComboBox5: TevDBComboBox;
    evLabel33: TevLabel;
    cmbHealthCare: TevDBComboBox;
    evLabel38: TevLabel;
    evLabel45: TevLabel;
    edDependentBenefitDate: TevDBDateTimePicker;
    edBenefitAvailable: TevDBComboBox;
    evLabel21: TevLabel;
    evLabel23: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo6: TevDBLookupCombo;
    bvlTaxDetails1: TEvBevel;
    EvBevel1: TEvBevel;
    lablReciprocal_Method: TevLabel;
    lablReciprocal_State: TevLabel;
    wwcbReciprocal_Method: TevDBComboBox;
    btnLocals: TevBitBtn;
    wwlcReciprocal_State: TevDBLookupCombo;
    lablW2_Type: TevLabel;
    wwcbW2_Type: TevDBComboBox;
    evLabel391: TevLabel;
    AvaliableGroupsGrid: TevDBGrid;
    sbAddESSGroup: TevSpeedButton;
    sbDelESSGroup: TevSpeedButton;
    evLabel401: TevLabel;
    AssignedGroupsGrid: TevDBGrid;
    lblExtension1: TevLabel;
    lblExtension2: TevLabel;
    lblExtension3: TevLabel;
    evLabel2: TevLabel;
    evLabel4: TevLabel;
    evLabel7: TevLabel;
    evLabel49: TevLabel;
    evLabel50: TevLabel;
    evLabel51: TevLabel;
    evLabel52: TevLabel;
    evLabel53: TevLabel;
    evLabel54: TevLabel;
    evLabel55: TevLabel;
    evLabel56: TevLabel;
    evLabel57: TevLabel;
    fpSecurity: TisUIFashionPanel;
    lablSecurity_Clearance: TevLabel;
    lablSupervisor: TevLabel;
    dedtSecurity_Clearance: TevDBEdit;
    wwlcSupervisor: TevDBLookupCombo;
    pnlLifeEvent: TisUIFashionPanel;
    evLabel40: TevLabel;
    evcbLastQualBenefitEvent: TevDBComboBox;
    evLabel42: TevLabel;
    evdtLastQualBenefitEventDate: TevDBDateTimePicker;
    pnlPosition: TisUIFashionPanel;
    lablRecruiter: TevLabel;
    lablReferral: TevLabel;
    lablPerformance_Rating: TevLabel;
    lablReview_Date: TevLabel;
    lablNext_Review_Date: TevLabel;
    evLabel24: TevLabel;
    evLabel15: TevLabel;
    wwlcRecruiter: TevDBLookupCombo;
    wwlcReferral: TevDBLookupCombo;
    wwlcPerformance_Rating: TevDBLookupCombo;
    wwDBDateTimePicker11: TevDBDateTimePicker;
    wwDBDateTimePicker12: TevDBDateTimePicker;
    evDBEdit4: TevDBEdit;
    evDBComboBox8: TevDBComboBox;
    fpAutomobile: TisUIFashionPanel;
    lablAuto_Insurance_Carrier: TevLabel;
    lablAuto_Insurance_Policy_Number: TevLabel;
    lablDrivers_License_Number: TevLabel;
    lablDrivers_License_Exp_Date: TevLabel;
    Label4: TevLabel;
    dedtAuto_Insurance_Carrier: TevDBEdit;
    dedtAuto_Insurance_Policy_Number: TevDBEdit;
    dedtDrivers_License_Number: TevDBEdit;
    drgpReliable_Car: TevDBRadioGroup;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    fpService: TisUIFashionPanel;
    drgpVeteran: TevDBRadioGroup;
    lablVeteran_Discharge_Date: TevLabel;
    wwDBDateTimePicker13: TevDBDateTimePicker;
    drgpVietnam_Veteran: TevDBRadioGroup;
    drgpDisabled_Veteran: TevDBRadioGroup;
    drgpMilitary_Reserve: TevDBRadioGroup;
    evRGServiceMedalVeteran: TevDBRadioGroup;
    evRGOtherProtectedVeteran: TevDBRadioGroup;
    lablVisa_Number: TevLabel;
    evLabel16: TevLabel;
    evLabel17: TevLabel;
    dedtVisa_Number: TevDBEdit;
    evDBComboBox2: TevDBComboBox;
    evDBDateTimePicker3: TevDBDateTimePicker;
    fpBenefits: TisUIFashionPanel;
    evLabel43: TevLabel;
    evLabel32: TevLabel;
    evLabel39: TevLabel;
    evDBLookupCombo10: TevDBLookupCombo;
    dtLastElection: TevDBDateTimePicker;
    fp: TisUIFashionPanel;
    evDBRadioGroup71: TevDBRadioGroup;
    isUIFashionPanel3: TisUIFashionPanel;
    drgpW2_Deceased: TevDBRadioGroup;
    drgpW2_Statutory_Employee: TevDBRadioGroup;
    drgpW2_Legal_Rep: TevDBRadioGroup;
    drgpW2_Deferred_Comp: TevDBRadioGroup;
    drgpW2_Pension: TevDBRadioGroup;
    evGroupBox1: TevGroupBox;
    drgpI9_File: TevDBRadioGroup;
    cbResidentalState: TevDBLookupCombo;
    evPanel1: TevPanel;
    evBitBtn1: TevBitBtn;
    isUIFashionPanel4: TisUIFashionPanel;
    gbAnnualReturns: TevGroupBox;
    evLabel46: TevLabel;
    rgW2onfile: TevDBRadioGroup;
    cbW2Format: TevDBComboBox;
    evLabel381: TevLabel;
    dedtEmail_Address1: TevDBEdit;
    evLabel44: TevLabel;
    evDBEdit8: TevDBEdit;
    wwlcEEOCode: TevDBLookupCombo;
    evLabel6: TevLabel;
    drgpSmoker: TevDBRadioGroup;
    evLabel27: TevLabel;
    lablEmail_Address: TevLabel;
    evLabel28: TevLabel;
    dedtEmail_Address: TevDBEdit;
    edWebPassword: TevDBEdit;
    lablBadge_ID: TevLabel;
    dedtBadge_ID: TevDBEdit;
    evLabel41: TevLabel;
    fp1099R: TisUIFashionPanel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evDBEdit3: TevDBEdit;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    evDBComboBox3: TevDBComboBox;
    cbCoGroupMember_benefit: TevComboBox;
    lblUnion: TevLabel;
    cmbUnion: TevDBLookupCombo;
    lablPosition_Status: TevLabel;
    wwcbPosition_Status: TevDBComboBox;
    evLabel37: TevLabel;
    evcbPosition: TevDBLookupCombo;
    evLabel36: TevLabel;
    wwlcHR_Position_Grades_Number: TevDBLookupCombo;
    pmAcaStatus: TPopupMenu;
    mnAcaStatus: TMenuItem;
    pmAcaCoverage: TPopupMenu;
    mnAcaCoverage: TMenuItem;
    wwlcUnion: TevDBLookupCombo;
    lablUnion: TevLabel;
    pmAcaLowestCost: TPopupMenu;
    mnAcaLowestCost: TMenuItem;
    pmAcaPolicyOrigin: TPopupMenu;
    mnAcaPolicyOrigin: TMenuItem;
    pmAcaSafeHarbor: TPopupMenu;
    mnAcaSafeHarbor: TMenuItem;
    cdCOBenefits: TevClientDataSet;
    cdCOBenefitsCO_BENEFITS_NBR: TIntegerField;
    cdCOBenefitsBENEFIT_NAME: TStringField;
    cdCOBenefitSubType: TevClientDataSet;
    IntegerField2: TIntegerField;
    cdCOBenefitSubTypeCO_BENEFIT_SUBTYPE_NBR: TIntegerField;
    cdCOBenefitSubTypeDESCRIPTION: TStringField;
    btnApplyForNewPosition: TevBitBtn;
    rgIncludeAnalytics: TevDBRadioGroup;
    pmEnableAnalytics: TevPopupMenu;
    mnEnableAnalytics: TMenuItem;
    tsACA: TTabSheet;
    isUIFashionPanel2: TisUIFashionPanel;
    cmbACAStatus: TevDBComboBox;
    evLabel29: TevLabel;
    edtACAStandardHours: TevDBEdit;
    evLabel30: TevLabel;
    evLabel58: TevLabel;
    evLowestCoBenefits: TevDBComboBox;
    evcbBenefitSubType: TevDBLookupCombo;
    evLabel61: TevLabel;
    evLabel60: TevLabel;
    evDBComboBox10: TevDBComboBox;
    rgBenefitsEligible: TevDBRadioGroup;
    pmHealthCare: TPopupMenu;
    mnHealthCare: TMenuItem;
    isUIFashionPanel5: TisUIFashionPanel;
    evLabel64: TevLabel;
    evLabel66: TevLabel;
    cbAcaFormat: TevDBComboBox;
    cbFormType: TevDBComboBox;
    evDBRadioGroup7: TevDBRadioGroup;
    pmAcaFormat: TPopupMenu;
    mnAcaFormat: TMenuItem;
    pmAcaFormType: TPopupMenu;
    mnAcaFormType: TMenuItem;
    dgrpDD_Enabled: TevDBRadioGroup;
    pmSSN: TPopupMenu;
    mnSSN: TMenuItem;
    pmBenefitsEligible: TPopupMenu;
    mnBenefitsEligible: TMenuItem;
    isUIFashionPanel6: TisUIFashionPanel;
    lblPBJWorks: TevLabel;
    edtPBJDays: TevDBSpinEdit;
    pmPBJWorks: TPopupMenu;
    mnPBJWorks: TMenuItem;
    isUIFashionPanel7: TisUIFashionPanel;
    lblACA: TevLabel;
    evLabel59: TevLabel;
    grACAHistory: TevDBGrid;
    cbMonth: TevDBComboBox;
    dsACAHistory: TevDataSource;
    AcaHistoryYear: TevComboBox;
    btnACAHistory: TevBitBtn;
    evLabel62: TevLabel;
    edACADOB: TevDBDateTimePicker;
    evLabel63: TevLabel;
    edACAHireDAte: TevDBDateTimePicker;
    evLabel65: TevLabel;
    edACATerm: TevDBDateTimePicker;
    btnNavCancel: TevSpeedButton;
    btnACAPost: TevSpeedButton;
    lbACAInitial: TevLabel;
    lblStability: TevLabel;
    lblACAMeasurementData: TevLabel;
    lblACAStabilityData: TevLabel;
    rgWorkAtHome: TevDBRadioGroup;
    cbACASafeHarborType: TevDBComboBox;
    evLabel67: TevLabel;
    cbACACovarage: TevDBLookupCombo;
    cbACAReliefCode: TevDBLookupCombo;
    cbACA_COVERAGE_OFFER: TevDBComboBox;
    cbEE_ACA_SAFE_HARBOR: TevDBComboBox;
    Label52: TevLabel;
    pmW2FormOnFile: TevPopupMenu;
    mnW2FormOnFile: TMenuItem;
    procedure ChangeSubMaster(Sender: TObject; Field: TField);
    procedure bbtnLoadClick(Sender: TObject);
    procedure dedtSocial_Security_NumberExit(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwlcMarital_StatusChange(Sender: TObject);
    procedure wwlcStateCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure YTDBtnClick(Sender: TObject);
    procedure drgpTreat_SS_EINChange(Sender: TObject);
    procedure drgpEE_FederalChange(Sender: TObject);
    procedure drgpTreat_Company_IndividualChange(Sender: TObject);
    procedure btnLocalsClick(Sender: TObject);
    procedure drgpEmployee_OASDIChange(Sender: TObject);
    procedure wwlcStateDropDown(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure evSpeedButton2Click(Sender: TObject);
    procedure edtAvHoursExit(Sender: TObject);
    procedure wwdsSubMaster4DataChange(Sender: TObject; Field: TField);
    procedure DBEdit21Change(Sender: TObject);
    procedure DBEdit25Exit(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure DBEdit21KeyPress(Sender: TObject; var Key: Char);
    procedure evBitBtn1Click(Sender: TObject);
    procedure wwdsSubMaster2DataChange(Sender: TObject; Field: TField);
    procedure DBRadioGroup1Change(Sender: TObject);
    procedure DBEdit30Exit(Sender: TObject);
    procedure wwdsSubMaster2UpdateData(Sender: TObject);
    procedure wwDBComboBox6CloseUp(Sender: TwwDBComboBox; Select: Boolean);
    procedure evBitBtn2Click(Sender: TObject);
    procedure wwlcMarital_StatusCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBComboBox2Exit(Sender: TObject);
    procedure wwlcMarital_StatusBeforeDropDown(Sender: TObject);
    procedure btnApplyForNewPositionClick(Sender: TObject);
    procedure wwlcSDICloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcSUICloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure ResultFieldSSNFunction;
    procedure dgrpSS_EnabledChange(Sender: TObject);
    procedure imAccessLevelCopyToClick(Sender: TObject);
    procedure FedMinWageCopyToClick(Sender: TObject);
    procedure evcbPositionCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure SetCO_HR_POSITION_GRADESFilter;
    procedure PageControl1Change(Sender: TObject);
    procedure evcbPositionChange(Sender: TObject);
    procedure wwdsEmployeeDataChange(Sender: TObject; Field: TField);
    procedure btnUnblockAccClick(Sender: TObject);
    procedure wwdsEmployeeUpdateData(Sender: TObject);
    procedure editPasswordChange(Sender: TObject);
    procedure dsAvaliableGroupsCalcFields(DataSet: TDataSet);
    procedure sbAddESSGroupClick(Sender: TObject);
    procedure sbDelESSGroupClick(Sender: TObject);
    procedure cbCoGroupMember_benefitCloseUp(Sender: TObject);
    procedure cbCoGroupMember_benefitChange(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure rgW2onfileChange(Sender: TObject);
    procedure miW2FormatCopyClick(Sender: TObject);
    procedure cbW2FormatCloseUp(Sender: TwwDBComboBox; Select: Boolean);
    procedure cbW2FormatDropDown(Sender: TObject);
    procedure mnDBDTCopyClick(Sender: TObject);
    procedure CopyEEFDBDTCopy;
    procedure mnPositionCopyClick(Sender: TObject);
    procedure evDBRadioGroup5Exit(Sender: TObject);
    procedure cbDivisionNbrSetLookupText(Sender: TObject;
      var AText: String);
    procedure mnAcaStatusClick(Sender: TObject);
    procedure wwdsSubMaster2StateChange(Sender: TObject);
    procedure mnAcaCoverageClick(Sender: TObject);
    procedure LoadACACOBenefits;
    procedure evLowestCoBenefitsChange(Sender: TObject);
    procedure evLowestCoBenefitsCloseUp(Sender: TwwDBComboBox;
      Select: Boolean);
    procedure mnAcaPolicyOriginClick(Sender: TObject);
    procedure mnAcaLowestCostClick(Sender: TObject);
    procedure mnAcaSafeHarborClick(Sender: TObject);
    procedure mnEnableAnalyticsClick(Sender: TObject);
    procedure mnHealthCareClick(Sender: TObject);
    procedure mnAcaFormatClick(Sender: TObject);
    procedure mnAcaFormTypeClick(Sender: TObject);
    procedure mnSSNClick(Sender: TObject);
    procedure mnBenefitsEligibleClick(Sender: TObject);
    procedure mnPBJWorksClick(Sender: TObject);
    procedure btnACAHistoryClick(Sender: TObject);
    procedure dsACAHistoryDataChange(Sender: TObject; Field: TField);
    procedure btnACAPostClick(Sender: TObject);
    procedure cbACA_COVERAGE_OFFERChange(Sender: TObject);
    procedure cbACA_COVERAGE_OFFERDropDown(Sender: TObject);
    procedure cbEE_ACA_SAFE_HARBORDropDown(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure cbACA_COVERAGE_OFFERExit(Sender: TObject);
    procedure cbEE_ACA_SAFE_HARBORExit(Sender: TObject);
    procedure mnW2FormOnFileClick(Sender: TObject);
  private
    FidxBenefitReference : integer;
    csCoGroup_benefit, csCoGroupMember_benefit : IevDataSetHolder;
    FSSChangedUsers : IisListOfValues;
    DMClientEEAfterPostHook : TDataSetNotifyEvent;
    MartitalStatusNeedsChanging: Boolean;
    FLocalsForm: TPOPUP_EDIT_EE_LOCALS;
    FOldwwlcStateValue: string;
    FRehirePrompt: boolean;
    SSHandled: Boolean;
    FApplicantCL_PERSON_NBR: Variant;
    FStateChanged: boolean;
    FHiring: Boolean;
    FGroupManagerEENBR : Integer;
    FToCheckIsGroupManager : Boolean;
    FESSGroupChanging: boolean;
    FPositionGrades : Boolean;
    FCheckLocalTaxAskOption : Boolean;
    FLoaded: Boolean;
    cdACAHistory: IevDataSet;
    cdACAOffer: IEvDataSet;
    cdACARelief: IEvDataSet;
    ACAHistoryState: Boolean;

    Procedure SetACABenefitReference;
    procedure DoCleanGroupManager(aEEnbr : integer);
    procedure UpdateAverageHours;
    procedure LoadCOGroupBenefits;
    procedure CheckHRSalary;
    function  ValidateSSN(const bSSNorEIN : Boolean = true): String;
    function CanHideOrUnhideEE( DataSet: TevClientDataSet): boolean;
    function MayHideOrUnhideEE( DataSet: TevClientDataSet): boolean;
    function CheckBeforeHidingEE( DataSet: TevClientDataSet): boolean;
    procedure CheckIfStateChanged;
    procedure CheckMartitalStatus;
    function SelectApplicantForHiring:boolean;
    procedure DMClientEEAfterPost(DataSet: TDataSet);
    procedure CopyEEFieldTo(aFieldCaption, aFieldName: string);
    procedure CopyACAStatusTo(const AFieldName: string);
    procedure CopyW2FormatEEFieldTo(aFieldCaption, aFieldName: string);
    function CheckIfESSEnabled: boolean;
    procedure FillUpESSGroups;
    procedure UpdateCoGroupMember_benefit;
    procedure SetW2formatComboBox(const EE_W2Format : string);
    function CheckLocalTaxAskOption: Boolean;
    function  CheckEmployeeChilds(pEE_NBR : Integer;Check : Char):boolean;
    function getCodeText( const fTable, fCode, fDesc: String):string;
    procedure PostACAHistory;
    procedure ACAHistory;
  protected
    procedure SetReadOnly(Value: Boolean); Override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function  GetDataSetConditions(sName: string): string; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;
    procedure ShowRehirePrompt;
  public
    constructor Create(AOwner: TComponent); override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    procedure DoBeforeDelete; override;
  end;

type
  TStrFunc = function(const S, SubStr: string):string;

function DelSubstr(const S, SubStr: string) : string;
procedure ChangeMask(C:TCustomEdit; PictMask: string; SF: TStrFunc);

implementation

uses
  EvUtils, EvTypes, SSecurityInterface, SPD_TaxCalculator, SPD_CheckFinder,
  SFrameEntry, SPD_RehirePromptForm, SEmployeeScr, DateUtils, SFieldCodeValues,
  SPD_POPUP_SSN, isDataSet;

type
  SSNMaskType = (SSN, EIN);

const
  aSSNMask: array[SSNMaskType] of string = ('*3{#}-*2{#}-*4{#}', '*2{#}-*7{#}');


{$R *.DFM}


procedure TEDIT_EE_CL_PERSON.ChangeSubMaster(Sender: TObject; Field: TField);

  procedure HideDBDTInfo(const CB: TevDBLookupCombo);
  begin
    CB.Tag := 1;
    CB.DisplayValue := '';
  end;

  procedure UnHideDBDTInfo(const CB: TevDBLookupCombo);
  begin
    CB.Tag := 0;
  end;

var
  coStateNbr :variant;
begin
  if DM_EMPLOYEE.EE_STATES.Active and (DM_EMPLOYEE.EE_STATES.State = dsBrowse) then
  begin
    if not DM_COMPANY.CO_STATES.Active then
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString);

    coStateNbr := DM_COMPANY.CO_STATES.Lookup('STATE', 'VT', 'CO_STATES_NBR');
    cmbHealthCare.Enabled := DM_EMPLOYEE.EE_STATES.Locate('EE_NBR;SUI_APPLY_CO_STATES_NBR',
                                         VarArrayOf([wwdsSubMaster.DataSet['EE_NBR'], coStateNbr]), []);
    DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', wwdsSubMaster.DataSet['HOME_TAX_EE_STATES_NBR'], []);

  end;

  if DM_EMPLOYEE.EE_RATES.Active and (DM_EMPLOYEE.EE_RATES.State = dsBrowse) then
    DM_EMPLOYEE.EE_RATES.Locate('EE_NBR; PRIMARY_RATE', VarArrayOf([wwdsSubMaster.DataSet['EE_NBR'], 'Y']), []);

  if not DM_COMPANY.CO_DIVISION.Active or
     not DM_COMPANY.CO_BRANCH.Active or
     not DM_COMPANY.CO_DEPARTMENT.Active or
     not DM_COMPANY.CO_TEAM.Active then
    Exit;

  if DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', wwdsSubMaster.DataSet['CO_DIVISION_NBR'], 'PRINT_DIV_ADDRESS_ON_CHECKS') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbDivisionNbr);
    HideDBDTInfo(cbDivisionName);
  end
  else
  begin
    UnHideDBDTInfo(cbDivisionNbr);
    UnHideDBDTInfo(cbDivisionName);
  end;
  if DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', wwdsSubMaster.DataSet['CO_BRANCH_NBR'], 'PRINT_BRANCH_ADDRESS_ON_CHECK') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbBranchNbr);
    HideDBDTInfo(cbBranchName);
  end
  else
  begin
    UnHideDBDTInfo(cbBranchNbr);
    UnHideDBDTInfo(cbBranchName);
  end;
  if DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', wwdsSubMaster.DataSet['CO_DEPARTMENT_NBR'], 'PRINT_DEPT_ADDRESS_ON_CHECKS') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbDepartmentNbr);
    HideDBDTInfo(cbDepartmentName);
  end
  else
  begin
    UnHideDBDTInfo(cbDepartmentNbr);
    UnHideDBDTInfo(cbDepartmentName);
  end;
  if DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', wwdsSubMaster.DataSet['CO_TEAM_NBR'], 'PRINT_GROUP_ADDRESS_ON_CHECK') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
  begin
    HideDBDTInfo(cbTeamNbr);
    HideDBDTInfo(cbTeamName);
  end
  else
  begin
    UnHideDBDTInfo(cbTeamNbr);
    UnHideDBDTInfo(cbTeamName);
  end;

  UpdateAverageHours;

  if not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) then
     SetACABenefitReference;

  if not btnACAHistory.Enabled then
  begin
    if not ACAHistoryState then
    begin
      cdACAHistory:= nil;
      btnACAHistory.Enabled:= True;
    end;
  end
  else
    ACAHistoryState:= false;

end;

procedure TEDIT_EE_CL_PERSON.UpdateAverageHours;
var
  v: Variant;
begin
  if (TevClientDataSet(DM_EMPLOYEE.EE_RATES).State = dsBrowse) and (TevClientDataSet(DM_EMPLOYEE.EE).State = dsBrowse) then
  begin
    v := DM_EMPLOYEE.EE_RATES.Lookup( 'EE_NBR;PRIMARY_RATE',VarArrayOf([DM_EMPLOYEE.EE['EE_NBR'],GROUP_BOX_YES]),'RATE_AMOUNT');
    if  (VarIsNull(v)) or ( double(v) = 0 ) then
      edtAvHours.Text := '0.00'
    else
      edtAvHours.Text := Format( '%.2f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat / double(v)] );
  end
//  else
//    edtAvHours.Text := '0.00';
end;


procedure TEDIT_EE_CL_PERSON.bbtnLoadClick(Sender: TObject);
begin
  dimgPicture.ContexMenu_LoadFromFile;
end;

procedure TEDIT_EE_CL_PERSON.dedtSocial_Security_NumberExit(Sender: TObject);
var
  TempSSN: string;
  t: TevClientDataSet;
  sError: String;
begin
  inherited;
  with DM_CLIENT.CL_PERSON do
    if TevClientDataSet(DM_CLIENT.CL_PERSON).State = dsInsert then
    begin
      DisableControls;
      t := TevClientDataSet.Create(Self);
      try
        t.CloneCursor(DM_CLIENT.CL_PERSON, True);
        t.Filtered := False;
        TempSSN := FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
        if t.Locate('SOCIAL_SECURITY_NUMBER', TempSSN, []) then
        begin
          Cancel;
          Filtered := False;
          Locate('SOCIAL_SECURITY_NUMBER', TempSSN, []);
          Edit;

          if (TevClientDataSet(DM_EMPLOYEE.EE).State in [dsInsert]) then
          begin
            DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').Assign(FieldByName('CL_PERSON_NBR'));
            DM_COMPANY.CO_HR_APPLICANT.DisableControls;
            try

              if ((not(FHiring) and SelectApplicantForHiring) or FHiring) then
              begin

                if FHiring or DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull then
                  DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_DIVISION_NBR').Value;
                if FHiring or DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull then
                  DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_BRANCH_NBR').Value;
                if FHiring or DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull then
                  DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_DEPARTMENT_NBR').Value;
                if FHiring or DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull then
                  DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_TEAM_NBR').Value;
                if FHiring or DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').IsNull then
                   DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_JOBS_NBR').Value;
                if FHiring or DM_EMPLOYEE.EE.FieldByName('POSITION_STATUS').IsNull then
                   DM_EMPLOYEE.EE.FieldByName('POSITION_STATUS').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('POSITION_STATUS').Value;

                if FHiring or DM_EMPLOYEE.EE.FieldByName('CO_HR_APPLICANT_NBR').IsNull then begin
                   DM_EMPLOYEE.EE.FieldByName('CO_HR_APPLICANT_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').Value;
                end;

                if FHiring or DM_EMPLOYEE.EE.CO_HR_SUPERVISORS_NBR.IsNull then
                   TField(DM_EMPLOYEE.EE.CO_HR_SUPERVISORS_NBR).Value := TField(DM_COMPANY.CO_HR_APPLICANT.CO_HR_SUPERVISORS_NBR).Value;
                if FHiring or DM_EMPLOYEE.EE.SECURITY_CLEARANCE.IsNull then
                   TField(DM_EMPLOYEE.EE.SECURITY_CLEARANCE).Value := TField(DM_COMPANY.CO_HR_APPLICANT.SECURITY_CLEARANCE).Value;
                if FHiring or DM_EMPLOYEE.EE.CO_HR_RECRUITERS_NBR.IsNull then
                   TField(DM_EMPLOYEE.EE.CO_HR_RECRUITERS_NBR).Value := TField(DM_COMPANY.CO_HR_APPLICANT.CO_HR_RECRUITERS_NBR).Value;
                if FHiring or DM_EMPLOYEE.EE.CO_HR_REFERRALS_NBR.IsNull then
                   TField(DM_EMPLOYEE.EE.CO_HR_REFERRALS_NBR).Value := TField(DM_COMPANY.CO_HR_APPLICANT.CO_HR_REFERRALS_NBR).Value;
                if FHiring or DM_EMPLOYEE.EE.POSITION_STATUS.IsNull then
                   TField(DM_EMPLOYEE.EE.POSITION_STATUS).Value := TField(DM_COMPANY.CO_HR_APPLICANT.POSITION_STATUS).Value;
                if FHiring or DM_EMPLOYEE.EE.CO_HR_POSITIONS_NBR.IsNull then
                   TField(DM_EMPLOYEE.EE.CO_HR_POSITIONS_NBR).Value := TField(DM_COMPANY.CO_HR_APPLICANT.CO_HR_POSITIONS_NBR).Value;
              end;
            finally
              DM_COMPANY.CO_HR_APPLICANT.EnableControls;
            end;
          end;
        end;
      finally
        EnableControls;
        t.Free;
      end;
    end;

  // If applicant is hired and existent employee's data should reflect the applicant's data (reso #35361).

  if ((FHiring) and (TevClientDataSet(DM_EMPLOYEE.EE).State in [dsEdit])) then
  begin
    DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_DIVISION_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_BRANCH_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_DEPARTMENT_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_TEAM_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_JOBS_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_JOBS_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('POSITION_STATUS').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('POSITION_STATUS').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_HR_APPLICANT_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_HR_SUPERVISORS_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_SUPERVISORS_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('SECURITY_CLEARANCE').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('SECURITY_CLEARANCE').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_HR_RECRUITERS_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_RECRUITERS_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_HR_REFERRALS_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_REFERRALS_NBR').Value;
    DM_EMPLOYEE.EE.FieldByName('CO_HR_POSITIONS_NBR').Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_POSITIONS_NBR').Value;
  end;

  if Assigned(Sender) then
    if (DM_CLIENT.CL_PERSON['EIN_OR_SOCIAL_SECURITY_NUMBER'] = GROUP_BOX_SSN) then
    begin
      sError := ValidateSSN;
      if sError <> '' then
      begin
        if DBEdit21.CanFocus then
          DBEdit21.SetFocus;
        raise EevException.Create(sError);
      end;
    end;
end;

procedure TEDIT_EE_CL_PERSON.SpeedButton3Click(Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  if (ctx_Security.AccountRights.Functions.GetState('UPDATE_AS_OF') = stEnabled) AND
     not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) and (wwdsSubMaster.DataSet.RecordCount > 0) then
  begin
    ShowVersionedFieldEditor(wwdsSubMaster.DataSet as TevClientDataSet, 'CO_DIVISION_NBR');
    Exit;
  end;

  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DM_PAYROLL, DBDTSelectionList do
    begin
      DBDTSelectionList.Setup(CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value
        , wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value, wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value
        , wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) then
          begin
            if wwdsSubMaster.DataSet.RecordCount = 0 then
              wwdsSubMaster.DataSet.Insert
            else
              wwdsSubMaster.DataSet.Edit;
          end;
          wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;

        if Assigned(wwdsDetail.DataSet) and (wwdsDetail.DataSet.State = dsInsert) then
        begin
          case DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1] of
          CLIENT_LEVEL_DIVISION:
            if DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value, []) and
               (DM_COMPANY.CO_DIVISION.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            begin
              wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Assign(DM_COMPANY.CO_DIVISION.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SDI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_DIVISION.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_DIVISION.FieldByName('HOME_CO_STATES_NBR'));
              wwdsSubMaster.DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_DIVISION.FieldByName('CO_WORKERS_COMP_NBR').Value;
              if DM_EMPLOYEE.EE_RATES.State = dsInsert then
              begin
                if not DM_COMPANY.CO_DIVISION.OVERRIDE_EE_RATE_NUMBER.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_NUMBER.Assign(DM_COMPANY.CO_DIVISION.OVERRIDE_EE_RATE_NUMBER);
                if not DM_COMPANY.CO_DIVISION.OVERRIDE_PAY_RATE.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_AMOUNT.Assign(DM_COMPANY.CO_DIVISION.OVERRIDE_PAY_RATE);
              end;
              FOldwwlcStateValue := wwlcState.Value;
              wwlcStateCloseUp(nil, nil, nil, False);
            end;
          CLIENT_LEVEL_BRANCH:
            if DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value, []) and
               (DM_COMPANY.CO_BRANCH.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            begin
              wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Assign(DM_COMPANY.CO_BRANCH.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SDI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_BRANCH.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_BRANCH.FieldByName('HOME_CO_STATES_NBR'));
              wwdsSubMaster.DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_BRANCH.FieldByName('CO_WORKERS_COMP_NBR').Value;
              if DM_EMPLOYEE.EE_RATES.State = dsInsert then
              begin
                if not DM_COMPANY.CO_BRANCH.OVERRIDE_EE_RATE_NUMBER.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_NUMBER.Assign(DM_COMPANY.CO_BRANCH.OVERRIDE_EE_RATE_NUMBER);
                if not DM_COMPANY.CO_BRANCH.OVERRIDE_PAY_RATE.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_AMOUNT.Assign(DM_COMPANY.CO_BRANCH.OVERRIDE_PAY_RATE);
              end;
              FOldwwlcStateValue := wwlcState.Value;
              wwlcStateCloseUp(nil, nil, nil, False);
            end;
          CLIENT_LEVEL_DEPT:
            if DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value, []) and
               (DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            begin
              wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Assign(DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SDI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_CO_STATES_NBR'));
              wwdsSubMaster.DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_DEPARTMENT.FieldByName('CO_WORKERS_COMP_NBR').Value;
              if DM_EMPLOYEE.EE_RATES.State = dsInsert then
              begin
                if not DM_COMPANY.CO_DEPARTMENT.OVERRIDE_EE_RATE_NUMBER.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_NUMBER.Assign(DM_COMPANY.CO_DEPARTMENT.OVERRIDE_EE_RATE_NUMBER);
                if not DM_COMPANY.CO_DEPARTMENT.OVERRIDE_PAY_RATE.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_AMOUNT.Assign(DM_COMPANY.CO_DEPARTMENT.OVERRIDE_PAY_RATE);
              end;
              FOldwwlcStateValue := wwlcState.Value;
              wwlcStateCloseUp(nil, nil, nil, False);
            end;
          CLIENT_LEVEL_TEAM:
            if DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value, []) and
               (DM_COMPANY.CO_TEAM.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_DEFAULT) then
            begin
              wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Assign(DM_COMPANY.CO_TEAM.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SDI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_TEAM.FieldByName('HOME_CO_STATES_NBR'));
              wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Assign(DM_COMPANY.CO_TEAM.FieldByName('HOME_CO_STATES_NBR'));
              wwdsSubMaster.DataSet.FieldByName('CO_WORKERS_COMP_NBR').Value := DM_COMPANY.CO_TEAM.FieldByName('CO_WORKERS_COMP_NBR').Value;
              if DM_EMPLOYEE.EE_RATES.State = dsInsert then
              begin
                if not DM_COMPANY.CO_TEAM.OVERRIDE_EE_RATE_NUMBER.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_NUMBER.Assign(DM_COMPANY.CO_TEAM.OVERRIDE_EE_RATE_NUMBER);
                if not DM_COMPANY.CO_TEAM.OVERRIDE_PAY_RATE.IsNull then
                  DM_EMPLOYEE.EE_RATES.RATE_AMOUNT.Assign(DM_COMPANY.CO_TEAM.OVERRIDE_PAY_RATE);
              end;
              FOldwwlcStateValue := wwlcState.Value;
              wwlcStateCloseUp(nil, nil, nil, False);
            end;
          end;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TEDIT_EE_CL_PERSON.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and (wwdsDetail.DataSet.State = dsInsert) then
  begin
    wwdsDetail.DataSet.FieldByName('RECIPROCAL_METHOD').AsString := STATE_RECIPROCAL_TYPE_NONE;
    wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Assign(wwdsMaster.DataSet.FieldByName('HOME_CO_STATES_NBR'));
    wwdsDetail.DataSet.FieldByName('SDI_APPLY_CO_STATES_NBR').Assign(wwdsMaster.DataSet.FieldByName('HOME_SDI_CO_STATES_NBR'));
    wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Assign(wwdsMaster.DataSet.FieldByName('HOME_SUI_CO_STATES_NBR'));
    cmbHealthCare.Enabled := (DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Value, 'STATE') = 'VT');
  end;
  btnLocals.Enabled := wwdsDetail.DataSet.State <> dsInsert;
end;

procedure TEDIT_EE_CL_PERSON.ButtonClicked(Kind: Integer; var Handled: Boolean);
  function isNameExist:boolean;
  var
    Q: IevQuery;
    s: string;
  begin
    Result := false;
    if (DM_COMPANY.CL_PERSON_DOCUMENTS.State in [dsInsert]) then
    begin
      DM_COMPANY.CL_PERSON_DOCUMENTS.UpdateRecord;
      if (not DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').IsNull) then
         Result := true;
    end;

    if (DM_COMPANY.CL_PERSON_DOCUMENTS.State in [dsEdit]) then
    begin
      DM_COMPANY.CL_PERSON_DOCUMENTS.UpdateRecord;
      if (not DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').IsNull) and
         (DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').OldValue <>
          DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').NewValue) then
           Result := true;
    end;

    if result then
    begin
      s:= 'SELECT count(*) FROM cl_person_documents WHERE name = :pName AND CL_PERSON_DOCUMENTS_NBR <> :pClPersonDocumentsNbr ' +
              ' and  {AsOfNow<cl_person_documents>} ';

      Q := TevQuery.Create(s);
      Q.Params.AddValue('pName', DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').asString);
      Q.Params.AddValue('pClPersonDocumentsNbr', DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('CL_PERSON_DOCUMENTS_NBR').AsInteger);
      Q.Execute;
      Result := Q.Result.Fields[0].AsInteger > 0;
    end;
  end;

begin
  if ACAHistoryState then
  begin
    Handled:= True;
    if (Kind in [NavOK]) then
       if EvMessage('Do you want to save changes?',  mtConfirmation, [mbYes, mbNo]) = mrYes then
          PostACAHistory;
    if (Kind in [NavCancel,NavAbort]) then
       ACAHistory;
  end
  else
  begin
    inherited;

    if (Kind in [NavOK,NavCommit]) then
    begin

      if (trim(evcbPosition.Text) <> '') then
        if (Trim(wwlcHR_Position_Grades_Number.Text) = '') then
          raise EUpdateError.CreateHelp('Please select a Pay Grade!', IDH_ConsistencyViolation);
    end;

    if (Kind = NavDelete) then
    Begin
     if CheckEmployeeChilds(DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger,'P') then
      raise EUpdateError.CreateHelp('Cannot delete Employee because the employee has payroll history!', IDH_ConsistencyViolation);
     if CheckEmployeeChilds(DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger,'R') then
      raise EUpdateError.CreateHelp('Cannot delete Employee because the employee has Payrate history!', IDH_ConsistencyViolation);
     if CheckEmployeeChilds(DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger,'D') then
      raise EUpdateError.CreateHelp('Cannot delete Employee because the employee has Scheduled E/D history!', IDH_ConsistencyViolation);
    end;

    if (Kind = NavOK) then
    begin
      if DM_CLIENT.EE_RATES.State in [dsEdit,dsInsert] then
        DM_CLIENT.EE_RATES.UpdateRecord;
      if DM_CLIENT.EE_RATES.State in [dsEdit,dsInsert] then
        CheckHRSalary;

      if isNameExist then
         raise EUpdateError.CreateHelp('File name is currently in use. Please create a unique name and save.', IDH_ConsistencyViolation);

      if (Trim(DBEdit20.Text) = '') then
          raise EUpdateError.CreateHelp('Custom Employee Number can''t be empty', IDH_ConsistencyViolation);

      if (wwDBDateTimePicker18.Text = '') then
          raise EUpdateError.CreateHelp('Current Hire Date can''t be empty', IDH_ConsistencyViolation);

      if (wwlcState.Text = '') then
          raise EUpdateError.CreateHelp('State cannot be empty', IDH_ConsistencyViolation);

      if (wwlcSDI.Text = '') then
          raise EUpdateError.CreateHelp('SDI cannot be empty', IDH_ConsistencyViolation);

      if (wwlcSUI.Text = '') then
          raise EUpdateError.CreateHelp('SUI cannot be empty', IDH_ConsistencyViolation);

      if MartitalStatusNeedsChanging then
          raise EUpdateError.CreateHelp('You must re-set the state marital status any time the state is changed!', IDH_ConsistencyViolation);

      if (wwdsDetail.DataSet.State = dsInsert) and
        wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').IsNull then
          raise EUpdateError.CreateHelp('State marital status can''t be empty', IDH_ConsistencyViolation);

      if (wwDBDateTimePicker16.Text = '') and (drgpTreat_Company_Individual.Value <> GROUP_BOX_COMPANY)
          and (DM_COMPANY.CO.FieldByName('ENFORCE_EE_DOB').AsString = GROUP_BOX_YES) then
          raise EUpdateError.CreateHelp('Based on your company''s criteria, a valid DOB must be entered for all W2 type employees. Please enter a valid date of birth and try again.', IDH_ConsistencyViolation);

      if (evcbLastQualBenefitEvent.ItemIndex >= 0)
          and (evdtLastQualBenefitEventDate.Date <= 0) then
          raise EUpdateError.CreateHelp('Life Qualifying Event Date can''t be empty', IDH_ConsistencyViolation);

      if (Trim(DBEdit36.Text) = '') then
          raise EUpdateError.CreateHelp('Rate Number can''t be empty', IDH_ConsistencyViolation);

      if (Trim(DBEdit38.Text) = '') then
          raise EUpdateError.CreateHelp('Rate Amount can''t be empty', IDH_ConsistencyViolation);

      if (rgW2onfile.Value = GROUP_BOX_NO) then
      begin
        if wwdsSubMaster.DataSet.State in [dsEdit, dsInsert] then
        begin
          wwdsSubMaster.DataSet.UpdateRecord;
          if wwdsSubMaster.DataSet.FieldByName('W2').AsString = EE_REPORT_RESULT_TYPE_FILE then
            raise EUpdateError.CreateHelp('W2 Format can''t be electronic.', IDH_ConsistencyViolation);
        end;
      end;

      if (TevClientDataSet(DM_CLIENT.EE).State = dsInsert) and (evDBComboBox1.Value = GROUP_BOX_UNKOWN) then
          if EvMessage('Do you want to assign a Gender to this employee?', mtConfirmation, [mbYes, mbNo]) = mrYes then
          begin
             Handled := True;
             Exit;
          end;

      if (wwdsSubMaster.DataSet.State in [dsEdit, dsInsert]) and (wwDBDateTimePicker19.DateTime > 0) then
      begin
          wwdsSubMaster.DataSet.UpdateRecord;
          if (wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_DATE').OldValue <> wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_DATE').NewValue) and
             (wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_CODE').OldValue = wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_CODE').NewValue)  then
              EvMessage('Don''t forget to change the Current Status Code.', mtError, [mbOK]);
      end;

      if (rgIncludeAnalytics.Value = GROUP_BOX_NO) then
      begin
        if wwdsSubMaster.DataSet.State in [dsEdit, dsInsert] then
        begin
          wwdsSubMaster.DataSet.UpdateRecord;
          if VarToStr(wwdsSubMaster.DataSet.FieldByName('ENABLE_ANALYTICS').OldValue) = GROUP_BOX_YES then
          if EvMessage('Removing this Employee from Analytics will cause them to be removed from ALL Analytics Dashboards.  Are you sure you want to proceed?', mtConfirmation, [mbOK, mbCancel]) <> mrOk then
          begin
             Handled := True;
             Exit;
          end;
        end;
      end;
    end;

    inherited;

    case Kind of
      NavInsert:
        begin
           wwlcState.Enabled := True;
           wwlcSDI.Enabled   := True;
        end;
      NavOk:
        begin
            wwlcState.Enabled := False;
          wwlcSDI.Enabled   := False;
          SSHandled := False;
          FApplicantCL_PERSON_NBR := Null;
          if DM_EMPLOYEE.EE_RATES.State = dsInsert then
            DM_EMPLOYEE.EE_RATES['PRIMARY_RATE'] := GROUP_BOX_YES;
          CheckIfStateChanged;

          if DM_COMPANY.CO_HR_APPLICANT.State <> dsBrowse then
            DM_COMPANY.CO_HR_APPLICANT.Cancel;
          DM_COMPANY.CO_HR_APPLICANT.DisableControls;
          try
            if DM_COMPANY.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', DM_EMPLOYEE.EE.FieldByName('CO_HR_APPLICANT_NBR').AsInteger, []) then
            begin
              if DM_COMPANY.CO_HR_APPLICANT.FieldByName('APPLICANT_STATUS').AsString <> APPLICANT_STATUS_HIRED then
              begin
                DM_COMPANY.CO_HR_APPLICANT.Edit;
                DM_COMPANY.CO_HR_APPLICANT.FieldByName('APPLICANT_STATUS').AsString := APPLICANT_STATUS_HIRED;
              end;
            end;
          finally
            DM_COMPANY.CO_HR_APPLICANT.EnableControls;
          end;

          FRehirePrompt := (VarToStr(wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_CODE').OldValue) <> 'A') and
            ((VarToStr(wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_CODE').NewValue) = 'A') or (LowerCase(wwDBComboBox5.Text) = 'active')) and
            (EvMessage('Is this a rehire?', mtConfirmation, [mbYes, mbNo]) = mrYes);
          if FRehirePrompt then
          begin
            if (wwdsSubMaster.DataSet.FieldByName('ORIGINAL_HIRE_DATE').IsNull) then
            begin
              if not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) then
                wwdsSubMaster.DataSet.Edit;
              wwdsSubMaster.DataSet.FieldByName('ORIGINAL_HIRE_DATE').Value := wwdsSubMaster.DataSet.FieldByName('CURRENT_HIRE_DATE').oldValue;
            end;

            if (wwdsSubMaster.DataSet.FieldByName('NEW_HIRE_REPORT_SENT').AsString <> 'P') then
            begin
              if not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) then
                wwdsSubMaster.DataSet.Edit;
              wwdsSubMaster.DataSet.FieldByName('NEW_HIRE_REPORT_SENT').Value := NEW_HIRE_REPORT_PENDING;
            end;
          end;

          FToCheckIsGroupManager := (DM_COMPANY.CO.FieldByName('ENABLE_ESS').asString <> GROUP_BOX_NO )and
          (VarToStr(wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_CODE').OldValue) = EE_TERM_ACTIVE) and
            ((VarToStr(wwdsSubMaster.DataSet.FieldByName('CURRENT_TERMINATION_CODE').NewValue) <> EE_TERM_ACTIVE) or (LowerCase(wwDBComboBox5.Text) <> 'active'));
          if FToCheckIsGroupManager
          then FGroupManagerEENBR := wwdsSubMaster.DataSet.FieldByName('EE_NBR').AsInteger
          else FGroupManagerEENBR := 0;

          if Assigned(csCoGroupMember_benefit)
             and (csCoGroupMember_benefit.DataSet.State in [dsEdit, dsInsert]) then
             UpdateCoGroupMember_benefit;

          FCheckLocalTaxAskOption := false;
          if wwdsSubMaster.DataSet.State in [dsInsert] then
               FCheckLocalTaxAskOption := True;

        end;
      NavCancel:
        begin
           wwlcState.Enabled := False;
           wwlcSDI.Enabled   := False;
           SSHandled := False;
           FApplicantCL_PERSON_NBR := Null;
           FStateChanged := false;

           if Assigned(csCoGroupMember_benefit)
              and (csCoGroupMember_benefit.DataSet.State in [dsEdit, dsInsert]) then
              csCoGroupMember_benefit.DataSet.Cancel;

           SetW2formatComboBox(EE_W2FormatType_ComboChoices2);
        end;
    end;

    if Kind in [NavCancel, NavAbort, NavInsert, NavDelete] then
       FidxBenefitReference :=-1;
  end;
end;

procedure TEDIT_EE_CL_PERSON.wwlcMarital_StatusChange(Sender: TObject);
begin
  inherited;
  MartitalStatusNeedsChanging := False;
end;

procedure TEDIT_EE_CL_PERSON.wwlcStateCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  CheckMartitalStatus;

  if (wwdsSubMaster.DataSet.State = dsInsert) and (wwlcState.Text = 'PR') then
    wwdsSubMaster.DataSet.FieldByName('W2_TYPE').AsString := W2_TYPE_PUERTO_RICO;

  if (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) and (wwlcState.Value <> '') then
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', wwlcState.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        wwlcState.Clear;
        DM_CLIENT.EE_STATES['CO_STATES_NBR'] := Null;
      end;
  end;

end;

procedure TEDIT_EE_CL_PERSON.wwlcSDICloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) and (wwlcSDI.Value <> '') then
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', wwlcSDI.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        wwlcSDI.Clear;
        DM_CLIENT.EE_STATES['SDI_APPLY_CO_STATES_NBR'] := Null;
      end;
  end;

end;

procedure TEDIT_EE_CL_PERSON.wwlcSUICloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) and (wwlcSUI.Value <> '') then
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', wwlcSUI.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        wwlcSUI.Clear;
        DM_CLIENT.EE_STATES['SUI_APPLY_CO_STATES_NBR'] := Null;
      end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);
  AddDS(DM_COMPANY.CO_HR_POSITIONS, aDS);
  AddDS(DM_COMPANY.CO_HR_SUPERVISORS, aDS);
  AddDS(DM_COMPANY.CO_UNIONS, aDS);
  AddDS(DM_COMPANY.CO_HR_RECRUITERS, aDS);
  AddDS(DM_COMPANY.CO_HR_REFERRALS, aDS);
  AddDS(DM_COMPANY.CO_HR_PERFORMANCE_RATINGS, aDS);
  AddDS(DM_COMPANY.CO_HR_CAR, aDS);
  AddDS(DM_CLIENT.CL_DELIVERY_GROUP, aDS);
  AddDS(DM_COMPANY.CO_PAY_GROUP, aDS);
  AddDS(DM_COMPANY.CO_PR_CHECK_TEMPLATES, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_CLIENT.CL_PERSON_DEPENDENTS, aDS);
  AddDS(DM_EMPLOYEE.EE_STATES, aDS);
  AddDS(DM_EMPLOYEE.EE_RATES, aDS);
  AddDS(DM_EMPLOYEE.EE_WORK_SHIFTS, aDS);
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL, aDS);
  AddDS(DM_EMPLOYEE.EE_SCHEDULED_E_DS, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_EMPLOYEE.EE_LOCALS, aDS);
  AddDS(DM_COMPANY.CO_HR_SALARY_GRADES, aDS);
  AddDS(DM_CLIENT.CL_PERSON_DOCUMENTS, aDS);
  AddDS(DM_COMPANY.CO_HR_APPLICANT, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_DISCOUNT, aDS);
  AddDS(DM_COMPANY.CO_GROUP_MEMBER, aDS);
  AddDS(DM_COMPANY.CO_GROUP, aDS);
  DM_COMPANY.CO_HR_POSITION_GRADES.Close;
  AddDS(DM_COMPANY.CO_HR_POSITION_GRADES, aDS);
  inherited;
end;

function TEDIT_EE_CL_PERSON.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE;
end;

function TEDIT_EE_CL_PERSON.GetInsertControl: TWinControl;
begin
  Result := dbEdit21;
end;

function TEDIT_EE_CL_PERSON.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_GROUP_MEMBER') or (sName = 'CO_HR_POSITION_GRADES') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_EE_CL_PERSON.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  DM_COMPANY.CO_DIVISION.Close;
  DM_COMPANY.CO_TEAM.Close;
  DM_COMPANY.CO_BRANCH.Close;
  DM_COMPANY.CO_DEPARTMENT.Close;

  AddDS(DM_CLIENT.CL_PERSON, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON, InsertDataSets);
  AddDS(DM_COMPANY.CO, EditDataSets);
  inherited;
  AddDS(DM_EMPLOYEE.EE_STATES, InsertDataSets);
  AddDS(DM_EMPLOYEE.EE_STATES, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON_DEPENDENTS, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON_DOCUMENTS, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_RATES, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_RATES, InsertDataSets);
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_SCHEDULED_E_DS, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_WORK_SHIFTS, EditDataSets);
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
  AddDS(DM_EMPLOYEE.EE_LOCALS, EditDataSets);
  AddDSWithCheck(DM_SYSTEM_HR.SY_HR_EEO, SupportDataSets, '');
  AddDS(DM_COMPANY.CO_HR_APPLICANT, EditDataSets);
  AddDSWithCheck(DM_COMPANY.CO_BENEFIT_DISCOUNT, SupportDataSets,'');
  AddDS(DM_COMPANY.CO_GROUP_MEMBER, EditDataSets);
  AddDSWithCheck(DM_COMPANY.CO_HR_POSITION_GRADES, SupportDataSets,'');

  AddDS(DM_SYSTEM_HR.SY_FED_ACA_OFFER_CODES, SupportDataSets);
  AddDS(DM_SYSTEM_HR.SY_FED_ACA_RELIEF_CODES, SupportDataSets);

end;

procedure TEDIT_EE_CL_PERSON.YTDBtnClick(Sender: TObject);
begin
  with CreateCoYTD do
  begin
    ClNbr := wwdsList.DataSet['cl_nbr'];
    CoNbr := wwdsList.DataSet['co_nbr'];
    EeNbr := wwdsSubmaster.DataSet['ee_nbr'];
    ShowModal;
  end;
end;

procedure TEDIT_EE_CL_PERSON.DeActivate;
begin
  FidxBenefitReference := -1;
  FPositionGrades := false;
  DM_CLIENT.EE.AfterPost := DMClientEEAfterPostHook;
  DMClientEEAfterPostHook := nil;

  DM_COMPANY.CO_HR_POSITION_GRADES.Filter:= '';
  DM_COMPANY.CO_HR_POSITION_GRADES.Filtered:= false;

  DM_EMPLOYEE.EE.AfterCancel := nil;
  FLoaded := False;
  FreeCoYTD;
  inherited;
end;

procedure TEDIT_EE_CL_PERSON.SetReadOnly(Value: Boolean);
begin
  inherited;

  evSpeedButton2.SecurityRO := false;
  YTDBtn.SecurityRO := false;

  dedtW2_Social_Security_Number.SecurityRO := True;
  tsDocuments.TabVisible := ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_HR_DOCUMENTS') = stEnabled;

  if wwdsSubMaster.DataSet.State = dsInsert then
     evLowestCoBenefits.SecurityRO:= GetIfReadOnly
  else
     evLowestCoBenefits.SecurityRO:= GetIfReadOnly or
                                 (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled);

  edACADOB.Enabled:= false;
  edACAHireDAte.Enabled := false;
  edACATerm.Enabled := false;

  grACAHistory.SecurityRO := GetIfReadOnly or (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled);
  cbACA_COVERAGE_OFFER.SecurityRO:= grACAHistory.SecurityRO;
  cbEE_ACA_SAFE_HARBOR.SecurityRO:= grACAHistory.SecurityRO;
end;

function DelSubstr(const S, SubStr: string) : string;
begin
  result := S;
  while pos(SubStr, result) > 0 do
    Delete(result, pos(SubStr, result), Length(SubStr));
end;

procedure ChangeMask(C: TCustomEdit; PictMask: string; SF: TStrFunc);
var
  S :string;
  i:integer;
  chEv :TNotifyEvent;
begin
  if (C is TwwDBEdit) then
  begin
    with TwwDBEdit(C) do
      if Assigned(DataSource) and Assigned(DataSource.DataSet) and
          Assigned(Field) and DataSource.DataSet.Active then
      begin

        S:= Field.AsString;
        S := SF(S, '-');
        chEv := OnChange;
        OnChange := nil;
        Clear;
        Picture.PictureMask := PictMask;
        for i:= 1 to length(S) do
        begin
          SendMessage(Handle, WM_Char, word(S[i]), 0);
        end;
        OnChange := chEv;
        if Modified then
          OnChange(nil);
        if DataSource.DataSet.State in [dsEdit,dsInsert] then
        begin
          OnChange := nil;
          Field.Value := Text;
          OnChange := chEv;
        end
      end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.drgpTreat_SS_EINChange(Sender: TObject);
var ev:TNotifyEvent;
begin
  inherited;
  ev := drgpTreat_SS_EIN.OnChange;
 try
   with (Sender as TevDBRadioGroup) do
    if Assigned(Field) and Assigned(Field.DataSet) and
       (Field.DataSet.State in [dsEdit, dsInsert]) then
    begin
      with (Sender as TevDBRadioGroup)  do
      begin

        if Value = GROUP_BOX_SSN then
        begin
          ChangeMask(DBEdit21, aSSNMask[SSN], DelSubstr);
          exit;
        end;
        if Value = GROUP_BOX_EIN then
        begin
          ChangeMask(DBEdit21, aSSNMask[EIN], DelSubstr);
          exit;
        end;
      end
    end else
    begin
        if Value = GROUP_BOX_SSN then
        begin
          DBEdit21.Picture.PictureMask := aSSNMask[SSN];
        end;
        if Value = GROUP_BOX_EIN then
        begin
          DBEdit21.Picture.PictureMask := aSSNMask[EIN];
        end;
    end;


  finally
  end;
end;

procedure TEDIT_EE_CL_PERSON.drgpEE_FederalChange(Sender: TObject);
var
  OldVal: string;
begin
  inherited;
  with (Sender as TevDBRadioGroup) do
    if Assigned(Field) and Assigned(Field.DataSet) and
       (Field.DataSet.State in [dsEdit]) then
      begin
        OldVal:=VarToStr(Field.Value);
        if (drgpTreat_Company_Individual.Value = GROUP_BOX_COMPANY) and
           (Value <> GROUP_BOX_EXEMPT) and (OldVal = GROUP_BOX_EXEMPT) then
          if EvMessage('Do You Want to Change the Tax Status for this 1099?',
             mtConfirmation, [mbYes, mbNo]) <> mrYes then
            Field.DataSet.Cancel;
          end;
end;


procedure TEDIT_EE_CL_PERSON.Activate;
begin
  inherited;

  FPositionGrades := True;
  DMClientEEAfterPostHook := DM_CLIENT.EE.AfterPost;
  DM_CLIENT.EE.AfterPost := DMClientEEAfterPost;

  SYMaritalSrc.MasterDataSource := SYStatesSrc;
  wwDBDateTimePicker16.Epoch := GetYear(Date) - 95;
  wwDBDateTimePicker16.CalendarAttributes.PopupYearOptions.StartYear := wwDBDateTimePicker16.Epoch;

  FLocalsForm := TPOPUP_EDIT_EE_LOCALS.Create( Self );
  FLocalsForm.SetDataSources( wwdsSubMasterEELocals, wwdsCoLocalTax );
  DM_EMPLOYEE.EE_RATES.CustomBeforeInsert := nil;
  DM_EMPLOYEE.EE_STATES.CustomBeforeInsert := nil;

  PageControl1.ActivePageIndex := 0;

  wwlcRecruiter.Enabled := Context.License.HR;
  wwlcPerformance_Rating.Enabled := wwlcRecruiter.Enabled;
  wwlcReferral.Enabled := wwlcRecruiter.Enabled;
  wwlcSupervisor.Enabled := wwlcRecruiter.Enabled;
  wwlcHR_Position_Grades_Number.Enabled := wwlcRecruiter.Enabled;
  evcbPosition.Enabled := wwlcRecruiter.Enabled;
  rgIncludeAnalytics.Enabled := DM_SERVICE_BUREAU.SB.ANALYTICS_LICENSE.Value = 'Y';

  HideRecordHelper.SetDataSet( GetDefaultDataSet, 'EE_ENABLED' );
  HideRecordHelper.OnCanHideRecord := CanHideOrUnhideEE;
  HideRecordHelper.OnMayHideRecord := MayHideOrUnhideEE;
  HideRecordHelper.sHideAction := 'Remove';
  HideRecordHelper.OnCheckBeforeHidingRecord := CheckBeforeHidingEE;
  HideRecordHelper.OnCanUnhideRecord := CanHideOrUnhideEE;
  HideRecordHelper.OnMayUnhideRecord := MayHideOrUnhideEE;
  FStateChanged := false;
  ResultFieldSSNFunction;
  FLoaded := True;
end;

procedure TEDIT_EE_CL_PERSON.drgpTreat_Company_IndividualChange(
  Sender: TObject);
begin
  inherited;
  with (Sender as TevDBRadioGroup) do
  begin
    if Assigned(DataSource) and Assigned(DataSource.DataSet) then
      with (DataSource.DataSet as TevClientDataSet) do
      begin
        if State in [dsInsert] then
        begin
          if drgpTreat_Company_Individual.Value = GROUP_BOX_COMPANY then //if EE marked 1099
          begin
            drgpEE_Federal.Field.Value := GROUP_BOX_EXEMPT;     //default on EE->Exempt
            drgpEmployee_OASDI.Field.Value := GROUP_BOX_YES;    //default->Exempt
            drgpEmployee_Medicare.Field.Value := GROUP_BOX_YES; //default->Exempt
            drgpER_OASDI.Field.Value := GROUP_BOX_YES;          //default->Exempt
            drgpER_Medicare.Field.Value := GROUP_BOX_YES;       //default->Exempt
            drgpEmployer_FUI.Field.Value := GROUP_BOX_YES;      //default->Exempt
            FieldByName('W2_TYPE').AsString := W2_TYPE_FOREIGN;
          end;
        end;

        if not DM_SERVICE_BUREAU.SB.Active then
          DM_SERVICE_BUREAU.SB.Open;

        if (drgpTreat_Company_Individual.Value = GROUP_BOX_COMPANY) or
           (DM_CLIENT.CO.FieldByName('ENFORCE_EE_DOB').AsString <> GROUP_BOX_YES) then //if EE marked 1099  or SB level flag is false
           Label52.Caption := 'Date of Birth '
        else
           Label52.Caption := '~Date of Birth';

        Label52.Left:= Label53.Left;

      end;
  end;

end;

procedure TEDIT_EE_CL_PERSON.btnLocalsClick(Sender: TObject);
begin
  inherited;
  with Owner as TFramePackageTmpl do
    if ButtonEnabled(NavOK) then
      ClickButton(NavOK);
  FLocalsForm.ShowModal;
end;

procedure TEDIT_EE_CL_PERSON.drgpEmployee_OASDIChange(Sender: TObject);
var
  OldVal: string;
begin
  inherited;
  with (Sender as TevDBRadioGroup) do
    if Assigned(Field) and Assigned(Field.DataSet) and
       (Field.DataSet.State in [dsEdit]) then
      begin
        OldVal:=VarToStr(Field.Value);
        if (drgpTreat_Company_Individual.Value = GROUP_BOX_COMPANY) and
           (Value = GROUP_BOX_NO) and (OldVal = GROUP_BOX_YES) then
          if EvMessage('Do You Want to Change the Tax Status for this 1099?',
             mtConfirmation, [mbYes, mbNo]) = mrYes then
            Field.Value := GROUP_BOX_NO
          else
            Field.DataSet.Cancel;
          end;
end;

procedure TEDIT_EE_CL_PERSON.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;

  if Kind = NavOK then
    if Value then
    begin
      (Owner as TFramePackageTmpl).NavigationDataSet := nil;
      TabSheet1.Enabled := False;
    end
    else
    begin
      (Owner as TFramePackageTmpl).NavigationDataSet := DM_EMPLOYEE.EE;
      TabSheet1.Enabled := True;
    end;

end;

procedure TEDIT_EE_CL_PERSON.wwlcStateDropDown(Sender: TObject);
begin
  inherited;
  FOldwwlcStateValue := wwlcState.Value;
end;

procedure TEDIT_EE_CL_PERSON.evSpeedButton1Click(Sender: TObject);
var
  b: Boolean;
begin
  inherited;
  if BitBtn1Enabled then
    Exit;
  TaxCalculator := TTaxCalculator.Create(Nil);
  b := ctx_DataAccess.RecalcLineBeforePost;
  ctx_DataAccess.RecalcLineBeforePost := True;
  try
    TaxCalculator.ShowModal;
  finally
    ctx_DataAccess.RecalcLineBeforePost := b;
    TaxCalculator.Free;
  end;
  DM_EMPLOYEE.EE_STATES.DataRequired('ALL');
  DM_PAYROLL.PR.Close;
  ResultFieldSSNFunction;
end;

constructor TEDIT_EE_CL_PERSON.Create(AOwner: TComponent);
begin
  inherited;
  FLoaded := False;
  FHiring := False;
  FApplicantCL_PERSON_NBR := Null;
  evSecElement1.AtomComponent := nil;
  FSSChangedUsers := TisListOfValues.Create;
  //GUI 2.0 functionality
end;

procedure TEDIT_EE_CL_PERSON.evSpeedButton2Click(Sender: TObject);
begin
  inherited;
  if BitBtn1Enabled then
    Exit;
  CheckFinder := TCheckFinder.Create(Self);
  try
    CheckFinder.editEENbr.Text := Trim(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
    CheckFinder.DisableOKButton := True;
    CheckFinder.ShowModal;
  finally
    CheckFinder.Free;
  end;
end;

procedure TEDIT_EE_CL_PERSON.edtAvHoursExit(Sender: TObject);
begin
  inherited;
  if (DBEdit38.Text <> '') and (edtAvHours.Text <> '') and (StrToFloat(edtAvHours.Text) <> 0) and
  (StrToFloat(DBEdit38.Text) <> StrToFloat(Format('%.4f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat / StrToFloat(edtAvHours.Text)]))) then
  begin
    if EvMessage('Rate will change to $' + Format('%.4f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat / StrToFloat(edtAvHours.Text)])
    + '.', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      if DM_EMPLOYEE.EE_RATES.State = dsBrowse then
        DM_EMPLOYEE.EE_RATES.Edit;
      wwdsSubMaster4.DataSet.FieldByName('RATE_AMOUNT').AsFloat := StrToFloat(Format('%.4f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat/StrToFloat(edtAvHours.Text)]));
    end
    else
      UpdateAverageHours;
  end;
  if (StrToFloat(edtAvHours.Text) = 0) then
  begin
    if EvMessage('Rate will change to $0.00.', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      if DM_EMPLOYEE.EE_RATES.State = dsBrowse then
        DM_EMPLOYEE.EE_RATES.Edit;
      wwdsSubMaster4.DataSet.FieldByName('RATE_AMOUNT').AsFloat := 0;
    end
    else
      UpdateAverageHours;
  end;
end;


procedure TEDIT_EE_CL_PERSON.wwdsSubMaster4DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  UpdateAverageHours;
end;

function TEDIT_EE_CL_PERSON.CheckLocalTaxAskOption:Boolean;
var
    HomeState : integer;
    SuiState : integer;
Begin
    result := false;

    if DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', DM_EMPLOYEE.EE['HOME_TAX_EE_STATES_NBR'], []) and
       (not DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').IsNull) then
    begin
      HomeState := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR;CO_NBR', VarArrayOf([DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').Value, DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'SY_STATES_NBR');
      SuiState := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR;CO_NBR', VarArrayOf([DM_EMPLOYEE.EE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').Value, DM_COMPANY.CO.FieldByName('CO_NBR').Value]), 'SY_STATES_NBR');
      DM_COMPANY.CO_LOCAL_TAX.First;
      while not DM_COMPANY.CO_LOCAL_TAX.EOF do
      begin
          if (DM_COMPANY.CO_LOCAL_TAX.FieldByName('LOCAL_ACTIVE').AsString = GROUP_BOX_YES) and
             ((HomeState = DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_STATES_NBR').AsInteger) or
              (SuiState = DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_STATES_NBR').AsInteger) ) and
             (DM_COMPANY.CO_LOCAL_TAX.FieldByName('AUTOCREATE_ON_NEW_HIRE').AsString = GROUP_BOX_ASK) then
             begin
              result := true;
              break;
             end;

        DM_COMPANY.CO_LOCAL_TAX.Next;
      end;
    end;
end;

procedure TEDIT_EE_CL_PERSON.AfterClick(Kind: Integer);
var
  tmpEENbr: integer;

  procedure SendSecurityNotification;
  var
    i : integer;
  begin
    try
      for i := 0 to FSSChangedUsers.Count - 1 do
        Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain('EE.' + FSSChangedUsers.Values[i].Name, Context.UserAccount.Domain));
    finally
      FSSChangedUsers.Clear;
    end;
  end;
begin
  inherited;
  if Kind = NavOK then
  begin
    if dblkupEE_NBR.Text = '' then
      dblkupEE_NBR.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
    if dbtxtname.Text = '' then
      dbtxtname.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
    if FRehirePrompt then
      ShowRehirePrompt;

    if FToCheckIsGroupManager then
       DoCleanGroupManager (FGroupManagerEENBR);

    if FCheckLocalTaxAskOption and CheckLocalTaxAskOption then
       FLocalsForm.ShowModal;

  end;
  ResultFieldSSNFunction;

  // Notify security cache to refresh this account
  // TODO!!!
  // This is a workaround! This notification should be generated in DB Access on RP side
  if Kind = NavCommit then
    SendSecurityNotification;
  if Kind = NavAbort then
    FSSChangedUsers.Clear;

  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort, NavRefresh] then
     ApplySecurity;
  if Kind in [NavCancel, NavAbort, NavOK, NavCommit] then
  begin
     tmpEENbr:=DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
     if Kind in [NavAbort] then
     begin
      DM_EMPLOYEE.EE.RefreshRecord(tmpEENbr);
      DM_EMPLOYEE.EE.Locate('EE_NBR', tmpEENbr, []);
     end;

     RefreshEeClone;
     dblkupEE_NBR.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
     dbtxtname.Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
  end;
end;

procedure TEDIT_EE_CL_PERSON.DBEdit21Change(Sender: TObject);
var
  s: string;
  isApplicant: boolean;

begin
  inherited;

  if SSHandled then
    Exit;

  isApplicant := FApplicantCL_PERSON_NBR = DM_CLIENT.CL_PERSON.CL_PERSON_NBR.AsVariant;

  if not isApplicant and
     (wwdsSubMaster2.DataSet.State = dsEdit) and
     (wwdsSubMaster2.DataSet.Modified or
      DM_EMPLOYEE.EE.Modified or
      DM_EMPLOYEE.EE_RATES.Modified or
      DM_EMPLOYEE.EE_STATES.Modified) then
  begin
    SSHandled := True;
    try
      DBEdit21.Text := DBEdit21.DataSource.DataSet[DBEdit21.DataField];
      drgpTreat_SS_EIN.Value := drgpTreat_SS_EIN.DataSource.DataSet[drgpTreat_SS_EIN.DataField];
      EvErrMessage('Please, post or cancel any changes you already made before changing SSN');
      Exit;
    finally
      SSHandled := False;
    end;
  end
  else
  begin
   if not isApplicant then
   begin
     if (wwdsSubMaster2.DataSet.State = dsEdit) then
     begin
      EvErrMessage('Changes to SSNs must be done via the Effective Period Editor.');
      wwdsSubMaster2.DataSet.Cancel;
      wwdsSubMaster.DataSet.Cancel;
      Exit;
     end;
   end;

  end;

  if (wwdsSubMaster2.DataSet.State <> dsEdit) or (ValidateSSN(DM_CLIENT.CL_PERSON['EIN_OR_SOCIAL_SECURITY_NUMBER'] = GROUP_BOX_SSN) <> '') then
    Exit;

  if DBEdit21.Modified then
  begin
    SSHandled := True;
    if DM_CLIENT.CL_PERSON.ShadowDataSet.Locate('SOCIAL_SECURITY_NUMBER', DBEdit21.Text, []) then
    begin
      s :=' ('+ DM_CLIENT.CL_PERSON.ShadowDataSet.fieldByName('FIRST_NAME').AsString + ' ' + DM_CLIENT.CL_PERSON.ShadowDataSet.fieldByName('LAST_NAME').AsString + ')';
      if isApplicant then
      begin
        SSHandled := false;
        EvErrMessage('Another person with same SSN already exists.');
      end
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.DBEdit25Exit(Sender: TObject);
var
  field: TField;
begin
  if (TevClientDataSet(DM_EMPLOYEE.EE).State = dsInsert) and (DM_EMPLOYEE.EE_STATES.State = dsInsert) then
  begin
    field := DM_EMPLOYEE.EE_STATES.FieldByNAme('STATE_NUMBER_WITHHOLDING_ALLOW');
    if field.IsNull then
      field.Value := TevDBEdit(Sender).DataLink.Field.Value;
  end;
end;

procedure TEDIT_EE_CL_PERSON.DBEdit38Exit(Sender: TObject);
begin
  inherited;
  if (DM_EMPLOYEE.EE_RATES.State <> dsBrowse) and
     (DM_EMPLOYEE.EE_RATES.FieldByName('PRIMARY_RATE').AsString = GROUP_BOX_YES) then
  begin
    if DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat <> 0 then
      edtAvHours.Text := Format( '%.2f', [DM_EMPLOYEE.EE.fieldByName('SALARY_AMOUNT').AsFloat / DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat] )
    else
      edtAvHours.Text := '0.00';
  end;
end;

procedure TEDIT_EE_CL_PERSON.AfterDataSetsReopen;
var
 sl : TStringList;
begin
  inherited;
  AssignVMRClientLookups(tsVMR);

  DM_COMPANY.CO_GROUP.Filter := ' GROUP_TYPE <> ''' + ESS_GROUPTYPE_BENEFIT + '''';
  DM_COMPANY.CO_GROUP.Filtered := True;
  sl := DM_COMPANY.CO_GROUP.GetFieldValueList('CO_GROUP_NBR',True);
  try
    if sl.Count > 0 then
      DM_COMPANY.CO_GROUP_Member.DataRequired(' CO_GROUP_NBR in ('+ sl.CommaText +')');
  finally
     FreeandNil(sl);
  end;


  FidxBenefitReference := -1;
  LoadACACOBenefits;
  evcbBenefitSubType.LookupTable := cdCOBenefitSubType;
  evcbBenefitSubType.LookupField:= 'CO_BENEFIT_SUBTYPE_NBR';


  cbAcaFormat.AllowClearKey:= false;
  cbFormType.AllowClearKey:= false;


  cdACAOffer:= ctx_RemoteMiscRoutines.GetACAOfferCodes(DM_COMPANY.CO.CO_NBR.AsInteger, Now);
  cdACARelief:= ctx_RemoteMiscRoutines.GetACAReliefCodes(DM_COMPANY.CO.CO_NBR.AsInteger, Now);

  cbACACovarage.LookupTable := cdACAOffer.vclDataSet;
  cbACAReliefCode.LookupTable := cdACARelief.vclDataSet;

  if not DM_COMPANY.CO.FieldByName('ACA_INITIAL_PERIOD_FROM').IsNull and
     not DM_COMPANY.CO.FieldByName('ACA_INITIAL_PERIOD_TO').IsNull then
    lblACAMeasurementData.Caption:=
        IntToStr(GetMonth(DM_COMPANY.CO.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime)) + '/' +
         IntToStr(GetYear(DM_COMPANY.CO.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime)) + ' - ' +
        IntToStr(GetMonth(DM_COMPANY.CO.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime)) + '/' +
         IntToStr(GetYear(DM_COMPANY.CO.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime));

  if not DM_COMPANY.CO.FieldByName('ACA_STABILITY_PERIOD_FROM').IsNull and
     not DM_COMPANY.CO.FieldByName('ACA_STABILITY_PERIOD_TO').IsNull then
    lblACAStabilityData.Caption:=
        IntToStr(GetMonth(DM_COMPANY.CO.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime)) + '/' +
         IntToStr(GetYear(DM_COMPANY.CO.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime)) + ' - ' +
        IntToStr(GetMonth(DM_COMPANY.CO.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime)) + '/' +
         IntToStr(GetYear(DM_COMPANY.CO.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime));

end;

procedure TEDIT_EE_CL_PERSON.CheckHRSalary;
var
  s: Extended;
  n: Integer;
begin
  if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_HR_POSITION_GRADES_NBR').IsNull and not DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').IsNull then
  begin
    case DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').AsString[1] of
      FREQUENCY_TYPE_DAILY:         n := 365;
      FREQUENCY_TYPE_WEEKLY:        n := 52;
      FREQUENCY_TYPE_BIWEEKLY:      n := 26;
      FREQUENCY_TYPE_SEMI_MONTHLY:  n := 24;
      FREQUENCY_TYPE_MONTHLY:       n := 12;
      FREQUENCY_TYPE_QUARTERLY:     n := 4;
    else
      n := 0;
    end;

    s := DM_EMPLOYEE.EE.FieldByName('STANDARD_HOURS').AsFloat * DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat * n;

    if (s < DM_COMPANY.CO_HR_POSITION_GRADES.FieldByName('MINIMUM_PAY').Value) or
       (s > DM_COMPANY.CO_HR_POSITION_GRADES.FieldByName('MAXIMUM_PAY').Value) then
      EvMessage('Annual salary is out of the HR salary grade!', mtWarning, [mbOk]);
  end;
end;


procedure TEDIT_EE_CL_PERSON.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;

  if AContext = 3 then                  // Preparation for adding new employee
  begin
    wwdsList.DataSet.Locate('CL_NBR;CO_NBR',VarArrayOf(AParams),[]);
    BitBtn1Click(BitBtn1);
  end;
  if Self.bPayrollsInQueue then EvErrMessage('You can not hire an employee until the payroll currently on the queue has finished processing.');
  if AContext = 1 then              //Hire applicant
    if TFramePackageTmpl(Owner).btnNavInsert.Enabled then
    begin
       TFramePackageTmpl(Owner).ClickButton(NavInsert);
       PageControl1.ActivePage := tshtQuick;
       DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := AParams[0];


       //  added By CA..
       //   if user enter more then one record for same applicant we have to use
       //   CO_HR_APPLICANT_NBR  field to reach real information for applicant information...
       // AParams[1]   is coming from SPD_EDIT_CO_HR_APPLICANT file...

       DM_CLIENT.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', AParams[1], []);


       FHiring := True;
       try
         DBEdit21.OnExit(nil);
         DBEdit21.SetFocus;
       finally
         FHiring := False;
       end;
       //RE 29161
       if ValidateSSN <> '' then
         FApplicantCL_PERSON_NBR := DM_CLIENT.CL_PERSON.CL_PERSON_NBR.AsVariant;
    end;
  if AContext = 2 then  begin           //  Hire applicant if employee already exist (reso# 35361)
    DM_CLIENT.EE.Locate('EE_NBR', AParams[0], []);
    DM_EMPLOYEE.EE.Edit;
    PageControl1.ActivePage := tshtQuick;
    DM_CLIENT.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', AParams[1], []);
    FHiring := True;
    try
      DBEdit21.OnExit(nil);
      DBEdit21.SetFocus;
    finally
      FHiring := False;
    end;
  end;
  if AContext = 3 then                  // Add new employee
    if TFramePackageTmpl(Owner).btnNavInsert.Enabled then
    begin
      TFramePackageTmpl(Owner).ClickButton(NavInsert);
      PageControl1.ActivePage := tshtQuick;
    end;
end;

procedure TEDIT_EE_CL_PERSON.ShowRehirePrompt;
var
  RehirePromptFrm: TRehirePrompt;
begin
  if (EvMessage('Do you want to see a report of which Employee details are set up that should be reviewed?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
  begin
    RehirePromptFrm := TRehirePrompt.Create( Self );
    try
      RehirePromptFrm.GetEERehirePrompts(wwdsSubMaster.DataSet.FieldByName('EE_NBR').AsInteger,
        DBEdit23.Text + ' ' + DBEdit22.Text,
        Trim(wwdsEmployee.DataSet.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString),
        Trim(wwdsMaster.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString),
        Trim(wwdsMaster.DataSet.FieldByName('NAME').AsString));
      RehirePromptFrm.ShowModal;
    finally
      RehirePromptFrm.Free;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.DBEdit21KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    DBEdit20.SetFocus;
    DBEdit21.SetFocus;
  end;
end;

function TEDIT_EE_CL_PERSON.ValidateSSN(const bSSNorEIN : Boolean = true): String;
var
  h: String;
  i: Integer;
begin
  Result := '';
  h := DBEdit21.Text;
  if bSSNorEIN then
  begin
    if Length(h) <> 11  then
    begin
      Result := 'Social Security Number is invalid';
      Exit;
    end;

    for i := 1 to Length(h) do
    begin
      if not (h[i] in ['0'..'9']) then
        if not((h[i] = '-') and (i in [4, 7])) then
        begin
          Result := 'Social Security Number is invalid';
          break;
        end;
    end;
    if DM_CLIENT.CL['BLOCK_INVALID_SSN'] = 'Y' then
    try
      CheckSSN(h);
    except
      on E: Exception do
        Result := E.Message;
    end;
  end
  else
  begin
    if Length(h) <> 10  then
    begin
      Result :='EIN is invalid';
      Exit;
    end;

    for i := 1 to Length(h) do
    begin
      if not (h[i] in ['0'..'9']) then
        if not((h[i] = '-') and (i in [3])) then
        begin
          Result :='EIN is invalid';
          break;
        end;
    end;
  end;

end;


procedure TEDIT_EE_CL_PERSON.evBitBtn1Click(Sender: TObject);
var
  Frm: TChoiceFromListQuery;
  DS: TevClientDataSet;
  i, Empt: Integer;
  EM: TStringList;
begin
  DS := TevClientDataSet.Create(Self);
  EM := TStringList.Create;
  Frm := TChoiceFromListQuery.Create(nil);
  try
    Frm.Caption := 'E-mail';
    DS.CloneCursor(DM_EMPLOYEE.EE, True);
    Frm.dgChoiceList.Selected.Clear;
    Frm.dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'EE Code'#9'F');
    Frm.dgChoiceList.Selected.Add('Employee_Name_Calculate'#9'20'#9'EE Name'#9'F');
    Frm.dgChoiceList.Selected.Add('E_MAIL_ADDRESS_CALC'#9'30'#9'eMail'#9'F');
    Frm.dgChoiceList.Selected.Add('CURRENT_TERMINATION_CODE'#9'30'#9'Status'#9'F');
    Frm.dgChoiceList.ApplySelected;
    i := DS.IndexDefs.IndexOf('SORT');
    if i <> -1 then
      DS.IndexDefs.Delete(i);
    Frm.dgChoiceList.DefaultSort := 'CUSTOM_EMPLOYEE_NUMBER';
    Frm.dgChoiceList.Sorting := True;
    DS.UserFiltered := False;
    DS.UserFilter := 'CURRENT_TERMINATION_CODE=''A''';
    DS.UserFiltered := True;
    Frm.dsChoiceList.DataSet := DS;
    DS.Locate('EE_NBR', DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger, []);
    Frm.Width := 450;
    if Frm.ShowModal = mrYes then
    begin
      Empt := 0;
      DS.DisableControls;
      for i := 0 to Frm.dgChoiceList.SelectedList.Count - 1 do
      begin
        DS.GotoBookmark(Frm.dgChoiceList.SelectedList[i]);
        if DS.FieldByName('E_MAIL_ADDRESS_CALC').AsString <> '' then
          EM.Add(DS.FieldByName('E_MAIL_ADDRESS_CALC').AsString)
        else
          Inc(Empt);
      end;
      DS.EnableControls;

      if (Empt > 0) and
         (EvMessage('There are ' + IntToStr(Empt) + ' Employees who do not have emails entered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], mbYes) <> mrYes) then
        Exit;

      MailTo(EM, nil, nil, nil, '', '');
    end;
  finally
    Frm.Free;
    DS.Free;
    EM.Free;
  end;
end;

function TEDIT_EE_CL_PERSON.CanHideOrUnhideEE(DataSet: TevClientDataSet): boolean;
begin
  with DataSet.FieldByName('CURRENT_TERMINATION_CODE') do
    Result := (Length(AsString) = 1) and (AsString[1] in [EE_TERM_TERMINATED,
                                                          EE_TERM_DEATH,
                                                          EE_TERM_TRANSFER,
                                                          EE_TERM_RESIGN,
                                                          EE_TERM_INV_LAYOFF,
                                                          EE_TERM_LAYOFF,
                                                          EE_TERM_RELEASE,
                                                          EE_TERM_RETIREMENT,
                                                          EE_TERM_SUSPENDED] );
end;

function TEDIT_EE_CL_PERSON.MayHideOrUnhideEE(DataSet: TevClientDataSet): boolean;
begin
  Result := not (Owner as TFramePackageTmpl).IsModified;
end;

function GetNumberOfChecksInThisYear(EeNbr: integer): Integer;
begin
  result := 0;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('CountThisYearChecksForEE') do
  begin
    SetParam('EeNbr', EeNbr);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0 then
    result := ctx_DataAccess.CUSTOM_VIEW.FieldByName('Result').AsInteger;
end;

function TEDIT_EE_CL_PERSON.CheckBeforeHidingEE(DataSet: TevClientDataSet): boolean;
begin
  Result := GetNumberOfChecksInThisYear( DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger ) = 0;
  if not Result then
    EvMessage( 'Cannot hide this employee. This employee has checks in the current year.', mtWarning, [mbOk] );
end;

procedure TEDIT_EE_CL_PERSON.CheckIfStateChanged;
var
  state: string;
  co_state: Variant;
  ee_state: Variant;
begin
  if TevClientDataSet(DM_CLIENT.CL_PERSON).State in [dsEdit, dsInsert] then
    DM_CLIENT.CL_PERSON.UpdateRecord;
  DBEdit30Exit(nil);
  if FStateChanged then
  begin
    FStateChanged := false;
    if DSIsActive(DM_CLIENT.CL_PERSON) and DSIsActive(DM_COMPANY.CO_STATES) and DSIsActive(DM_EMPLOYEE.EE_STATES)  then
    begin
      state := AnsiUpperCase(DM_CLIENT.CL_PERSON.FieldByName('STATE').AsString);
      co_state := DM_COMPANY.CO_STATES.Lookup( 'STATE', state, 'CO_STATES_NBR');
      if not VarIsNull(co_state) and
         (EvMessage( 'Do you want to change Home State to '+state+'?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
      begin
        ee_state := DM_EMPLOYEE.EE_STATES.Lookup( 'CO_STATES_NBR', co_state, 'EE_STATES_NBR' );
        if not VarIsNull(ee_state) then
        begin
          if ee_state <> DM_EMPLOYEE.EE['HOME_TAX_EE_STATES_NBR'] then
          begin
            if TevClientDataSet(DM_EMPLOYEE.EE).State = dsBrowse then
              DM_EMPLOYEE.EE.Edit;
            DM_EMPLOYEE.EE['HOME_TAX_EE_STATES_NBR'] := ee_state;
            if State = 'PR' then
              DM_EMPLOYEE.EE.W2_TYPE.AsString := W2_TYPE_PUERTO_RICO;
            CheckMartitalStatus;
          end
        end
        else
          PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Employee - States' + #0)), 0);
      end;
    end
  end;
end;

procedure TEDIT_EE_CL_PERSON.wwdsSubMaster2DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if assigned(Field) and (Field.FieldName = 'STATE') and
     (Field.DataSet.State = dsEdit) and
     (TevClientDataSet(DM_EMPLOYEE.EE).State <> dsInsert) then
    FStateChanged := true;

  if wwDBComboBox6.Text = 'American Indian or Alaska Native' then
  begin
    evDBEdit1.Enabled := True;
    evDBEdit1.Color := clWindow;
  end
  else
  begin
    evDBEdit1.Enabled := False;
    evDBEdit1.Color := clBtnFace;
  end;
end;


procedure TEDIT_EE_CL_PERSON.DBRadioGroup1Change(Sender: TObject);
begin
  inherited;
  if TWinControl(DBRadioGroup1.Controls[1]).Focused and
     (DBRadioGroup1.ItemIndex = 1) then
    EvMessage('Separate W-2''s will be produced for this employee');
end;

procedure TEDIT_EE_CL_PERSON.DBEdit30Exit(Sender: TObject);
var
  V: Variant;
begin
  if ((TDataSet(DM_CLIENT.CL_PERSON).State = dsInsert) or
      ((TDataSet(DM_CLIENT.CL_PERSON).State = dsEdit) and (DM_CLIENT.CL_PERSON.STATE.OldValue <> DM_CLIENT.CL_PERSON.STATE.NewValue)))
        and
     (DM_CLIENT.CL_PERSON.RESIDENTIAL_STATE_NBR.IsNull or
      (DM_CLIENT.CL_PERSON.STATE.Value <> DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', DM_CLIENT.CL_PERSON.RESIDENTIAL_STATE_NBR.Value, 'STATE')) and
      (EvMessage('Do you want to change residential state too?', mtConfirmation, [mbYes, mbNo]) = mrYes)) and
     not DM_CLIENT.CL_PERSON.STATE.IsNull then
  begin
    if DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CL_PERSON.STATE.AsString, []) then
      DM_CLIENT.CL_PERSON.RESIDENTIAL_STATE_NBR.AsInteger :=  DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.AsInteger;
  end;

  if DBEdit30.Text <> wwlcState.Value then
  begin
    V := DM_SYSTEM_STATE.SY_STATES.Lookup('state', wwlcState.Value, 'SY_STATES_NBR');
    if not VarIsNull(V) then
    begin
      DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition := 'SY_STATES_NBR=' + QuotedStr(V);
      ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS]);
      DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR',V ,[]);
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.wwdsSubMaster2UpdateData(Sender: TObject);
begin
  inherited;
  if Pos(';', ConvertNull(wwdsSubMaster.DataSet['E_MAIL_ADDRESS'], '')) <> 0 then
    raise EInvalidParameters.CreateHelp('Alternative E-mail contains illegal characters', IDH_InvalidParameters);
  if Pos(';', ConvertNull(wwdsSubMaster2.DataSet['E_MAIL_ADDRESS'], '')) <> 0 then
    raise EInvalidParameters.CreateHelp('Standard E-mail contains illegal characters', IDH_InvalidParameters);
  if Pos(';', ConvertNull(wwdsSubMaster2.DataSet['WEB_PASSWORD'], '')) <> 0 then
    raise EInvalidParameters.CreateHelp('Standard password contains illegal characters', IDH_InvalidParameters);
end;

procedure TEDIT_EE_CL_PERSON.wwDBComboBox6CloseUp(Sender: TwwDBComboBox;
  Select: Boolean);
begin
  inherited;
  if Sender.Text = 'American Indian or Alaska Native' then
  begin
    evDBEdit1.Enabled := True;
    evDBEdit1.Color := clWindow;
  end
  else
  begin
    evDBEdit1.Clear;
    evDBEdit1.Enabled := False;
    evDBEdit1.Color := clBtnFace;
  end;
end;

procedure TEDIT_EE_CL_PERSON.evBitBtn2Click(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_EE_SCHEDULED_E_DS');
end;

procedure TEDIT_EE_CL_PERSON.wwlcMarital_StatusCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
var
  V: Variant;
begin
  inherited;
  //TS:  08/15/2006 issue 39575 Set field default for non nullable field.
  if (DM_EMPLOYEE.EE_STATES.State in [dsInsert, dsEdit]) then
  begin
    V := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DM_EMPLOYEE.EE_STATES['CO_STATES_NBR'], 'SY_STATES_NBR');
    if not VarIsNull(V) then
    begin
      V := DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([V, DM_EMPLOYEE.EE_STATES['STATE_MARITAL_STATUS']]), 'SY_STATE_MARITAL_STATUS_NBR');
      if not VarIsNull(V) then
        DM_EMPLOYEE.EE_STATES['SY_STATE_MARITAL_STATUS_NBR'] := V;
    end;
  end;

  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFilter := '';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFiltered := false;
end;

procedure TEDIT_EE_CL_PERSON.evDBComboBox2Exit(Sender: TObject);
begin
  inherited;
  if not DM_CLIENT.CL_PERSON.FieldByName('VISA_NUMBER').IsNull
  and (DM_CLIENT.CL_PERSON.FieldByName('VISA_TYPE').Value <> VISA_TYPE_NONE)
  and (DM_CLIENT.CL_PERSON.FieldByName('VISA_TYPE').Value <> VISA_TYPE_H1B_RES) then
    ShowMessage('By selecting this visa type, if this EE/Company is not setup to be exempt from federal tax, this will activate the non-resident alien withholding tables to be used.');
end;

procedure TEDIT_EE_CL_PERSON.wwlcMarital_StatusBeforeDropDown(
  Sender: TObject);
begin
  inherited;

  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFilter := 'ACTIVE_STATUS = ''Y'' ';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFiltered := true;

  if(wwlcState.Value <> DM_SYSTEM_STATE.SY_STATES.STATE.Value) then
      wwlcStateCloseUp(Sender,nil,nil,true);
end;


procedure TEDIT_EE_CL_PERSON.btnApplyForNewPositionClick(Sender: TObject);
begin
  inherited;
  ActivateFrameAs('HR Module - Employee - HR Applicant - Applicants', [DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString], 1);
end;

function TEDIT_EE_CL_PERSON.SelectApplicantForHiring:boolean;
var
   DS: TevClientDataSet;
   Frm: TChoiceFromListQuery;
   SSN: string;
begin
  inherited;

  Result := False;
  DS := TevClientDataSet.Create(Self);
  SSN := DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
  try
    DS.CloneCursor(DM_ClIENT.CO_HR_APPLICANT, True);
    //DS.Locate('SSNLookup', SSN, []);
    DS.UserFiltered := False;
    DS.UserFilter := 'SSNLookup='+SSN+' and '+'(APPLICANT_STATUS<>''' + APPLICANT_STATUS_HIRED + ''''+ ' or '+'APPLICANT_STATUS=NULL)';
    DS.UserFiltered := True;
    DS.Locate('SSNLookup', SSN, []);
    if DS.RecordCount > 0 then begin
      if EvMessage('An Applicant already exists with that SSN.  Choose OK to either hire the applicant or create a new employee.', mtConfirmation, [mbOk]) <> mrOk then begin
        Exit;
      end;
      Frm := TChoiceFromListQuery.Create(nil);
      Frm.Caption := DS.FieldByName('Name_Calculate').AsString + '             SSN ' + SSN;
      Frm.dgChoiceList.Selected.Clear;
      Frm.dgChoiceList.Selected.Add('Division_Name'#9'15'#9'Division'#9'F');
      Frm.dgChoiceList.Selected.Add('Branch_Name'#9'15'#9'Branch'#9'F');
      Frm.dgChoiceList.Selected.Add('Department_Name'#9'15'#9'Department'#9'F');
      Frm.dgChoiceList.Selected.Add('Team_Name'#9'15'#9'Team'#9'F');
      Frm.dgChoiceList.Selected.Add('Jobs_Desc'#9'15'#9'Job'#9'F');
      Frm.dgChoiceList.ApplySelected;
      Frm.dgChoiceList.Options := Frm.dgChoiceList.Options - [dgMultiSelect];
      Frm.btnYes.Caption := 'Hire selected';
      Frm.btnNo.Caption := 'Do not hire';
      //Frm.btnNo.Visible := False;
      Frm.btnYes.Glyph := nil;
      Frm.btnNo.Glyph := nil;
      Frm.btnYes.Width := 150;
      Frm.btnNo.Width := 150;
      Frm.btnYes.Left := 18;
      Frm.btnNo.Left := 186;
      Frm.dsChoiceList.DataSet := DS;
      if Frm.ShowModal = mrYes then begin
        DM_ClIENT.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', DS.FieldByName('CO_HR_APPLICANT_NBR').AsString, []);
        Result := True;
      end
    end;
  finally
    DS.Free;
  end;


end;

procedure TEDIT_EE_CL_PERSON.BitBtn1Click(Sender: TObject);
Begin
  inherited;
  ResultFieldSSNFunction;
  grACAHistory.SecurityRO := GetIfReadOnly or (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled);
  cbACA_COVERAGE_OFFER.SecurityRO:= grACAHistory.SecurityRO;
  cbEE_ACA_SAFE_HARBOR.SecurityRO:= grACAHistory.SecurityRO;
End;

procedure TEDIT_EE_CL_PERSON.ResultFieldSSNFunction;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_SSN') <> stEnabled then
   Begin
    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
    DBEdit21.Enabled := False;
    Label44.Caption := 'SSN access restricted';
   end;
End;

procedure TEDIT_EE_CL_PERSON.DMClientEEAfterPost(DataSet: TDataSet);
var
  sUserName : String;
begin
  if Assigned(DMClientEEAfterPostHook) then
    DMClientEEAfterPostHook(DataSet);
  if not ((DataSet.FieldByName('SELFSERVE_ENABLED').oldValue = DataSet.FieldByName('SELFSERVE_ENABLED').NewValue) and
    (DataSet.FieldByName('SELFSERVE_USERNAME').oldValue = DataSet.FieldByName('SELFSERVE_USERNAME').NewValue) and
    (DataSet.FieldByName('SELFSERVE_PASSWORD').oldValue = DataSet.FieldByName('SELFSERVE_PASSWORD').NewValue)) then
    if (DataSet.FieldByName('SELFSERVE_USERNAME').oldValue <> null) then
    begin
      sUserName := DataSet.FieldByName('SELFSERVE_USERNAME').oldValue;
      if not FSSChangedUsers.ValueExists(sUserName) then
        FSSChangedUsers.AddValue(sUserName, '');
    end;
end;

procedure TEDIT_EE_CL_PERSON.CheckMartitalStatus;
begin
  MartitalStatusNeedsChanging := MartitalStatusNeedsChanging or (wwlcState.Value <> FOldwwlcStateValue);
  if Assigned(wwdsSubMaster.DataSet) {and (wwdsSubMaster.DataSet.State = dsInsert)} and (wwlcState.Value <> '') then
  begin
    DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition := 'SY_STATES_NBR=' + VarToStr( DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.EE_STATES.FieldByName('CO_STATES_NBR').Value, 'SY_STATES_NBR'));
    DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR',DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', DM_COMPANY.EE_STATES.FieldByName('CO_STATES_NBR').Value, 'SY_STATES_NBR') ,[]);

    if wwdsDetail.DataSet.State = dsBrowse then wwdsDetail.DataSet.Edit;

    if DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RecordCount = 1 then
    begin
      wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Value := DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').Value;
      MartitalStatusNeedsChanging := False;
    end
    else if VarIsNull(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Lookup('STATUS_TYPE', wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Value, 'SY_STATE_MARITAL_STATUS_NBR')) then
      wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Clear;
  end;
end;

procedure TEDIT_EE_CL_PERSON.dgrpSS_EnabledChange(Sender: TObject);
begin
  inherited;
  dedtSS_UserName.Enabled := (dgrpSS_Enabled.ItemIndex <> 0) or
                             (dgrpTimeOff_Enabled.ItemIndex <> 0) or
                             (dgrpBenefits_Enabled.ItemIndex <> 0);

  editPassword.Enabled :=    (dgrpSS_Enabled.ItemIndex <> 0) or
                             (dgrpTimeOff_Enabled.ItemIndex <> 0) or
                             (dgrpBenefits_Enabled.ItemIndex <> 0);
end;

procedure TEDIT_EE_CL_PERSON.imAccessLevelCopyToClick(Sender: TObject);
var
  k: integer;
  sSelfServeEnabled, sTimeOffEnabled, sBenefitsEnabled, sDdEnabled, sEEnbr: string;
begin
  sEEnbr := DM_CLIENT.EE.FieldByName('EE_NBR').AsString;
  sSelfServeEnabled := Trim(DM_CLIENT.EE.FieldByName('SELFSERVE_ENABLED').AsString);
  sTimeOffEnabled := Trim(DM_CLIENT.EE.FieldByName('TIME_OFF_ENABLED').AsString);
  sBenefitsEnabled := Trim(DM_CLIENT.EE.FieldByName('BENEFITS_ENABLED').AsString);
  sDdEnabled := Trim(DM_CLIENT.EE.FieldByName('DIRECT_DEPOSIT_ENABLED').AsString);
  if sEEnbr <> '' then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy the Self Serve Access Level for all employees. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_CLIENT.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);
            DM_CLIENT.EE.Edit;
            DM_CLIENT.EE['SELFSERVE_ENABLED'] := sSelfServeEnabled;
            DM_CLIENT.EE['TIME_OFF_ENABLED'] := sTimeOffEnabled;
            DM_CLIENT.EE['BENEFITS_ENABLED'] := sBenefitsEnabled;
            DM_CLIENT.EE['DIRECT_DEPOSIT_ENABLED'] := sDdEnabled;
            DM_CLIENT.EE.Post;
          end;
        finally
          DM_CLIENT.EE.Locate('EE_NBR',sEEnbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.FedMinWageCopyToClick(Sender: TObject);
begin
  inherited;
  CopyEEFieldTo('Override Fedaral Minimum Wage', 'OVERRIDE_FEDERAL_MINIMUM_WAGE');
end;

procedure TEDIT_EE_CL_PERSON.mnEnableAnalyticsClick(Sender: TObject);
begin
  inherited;
  CopyEEFieldTo('Include in Analytics', 'ENABLE_ANALYTICS');
end;

procedure TEDIT_EE_CL_PERSON.CopyEEFieldTo(aFieldCaption, aFieldName: string);
var
  k: integer;
  sEEnbr: string;
  lFieldValue: variant;
begin
  sEEnbr := DM_CLIENT.EE.FieldByName('EE_NBR').AsString;
  lFieldValue := DM_CLIENT.EE.FieldByName( aFieldName ).Value;
  if sEEnbr <> '' then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy the '+ aFieldCaption +' for all employees. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');
        DM_CLIENT.EE.DisableControls;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_CLIENT.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);
            DM_CLIENT.EE.Edit;
            DM_CLIENT.EE[ aFieldName ] := lFieldValue;
            DM_CLIENT.EE.Post;
          end;
        finally
          DM_CLIENT.EE.EnableControls;
          ctx_EndWait;
          DM_CLIENT.EE.Locate('EE_NBR',sEEnbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
end;

//Ticket#72373 -Begin
procedure TEDIT_EE_CL_PERSON.DoCleanGroupManager(aEEnbr : integer);
     function PopulateGroupList(aList : tStrings) : String;
       var i : integer;
     begin
       Result := '';
       for i := 0 to aList.Count -1 do
           Result := Result + aList[i] + ',';

       if Length(Result) > 0 then
          Delete(Result, Length(Result), 1);
     end;

var sList: tStrings;

begin
  sList := tStringlist.create;
  sList.Clear;
  try
    ctx_DataAccess.CUSTOM_VIEW.Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
    begin
      SetMacro('Columns', 'T1.GROUP_NAME');
      SetMacro('TABLE1', 'CO_GROUP');
      SetMacro('TABLE2', 'CO_GROUP_MANAGER');
      SetMacro('NBRFIELD', 'EE');
      SetMacro('JOINFIELD', 'CO_GROUP');
      SetParam('RecordNbr', aEEnbr);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0 then
    begin
      with ctx_DataAccess.CUSTOM_VIEW.GetFieldValueList('GROUP_NAME', True) do
      begin
        sList.CommaText := CommaText;
        Free;
      end;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;

    if ( sList.Count > 0 ) and
       ( EvMessage('Do you want to remove the employee from the Manager Assignment screen?', mtConfirmation, [mbYes, mbNo]) = mrYes)
    then begin
       DM_COMPANY.CO_GROUP_MANAGER.DataRequired('EE_NBR='+ IntTostr(aEEnbr));
       try
         while not DM_COMPANY.CO_GROUP_MANAGER.Eof do
            DM_COMPANY.CO_GROUP_MANAGER.Delete;

         ctx_DataAccess.PostDataSets([DM_COMPANY.CO_GROUP_MANAGER]);
       finally
         EvMessage('This employee was successfully removed from ' + PopulateGroupList(sList) + ' Group(s)', mtInformation, [mbOK]);
       end;
    end;
  finally
    sList.Free;
  end;
end;
//Ticket#72373 -End


procedure TEDIT_EE_CL_PERSON.SetCO_HR_POSITION_GRADESFilter;
begin

    if  FPositionGrades then
    begin
      if wwdsSubMaster4.DataSet.State in [dsInsert, dsEdit] then
         wwdsSubMaster4.DataSet.UpdateRecord
      else
      if DM_EMPLOYEE.EE_RATES.Active and (DM_EMPLOYEE.EE_RATES.State = dsBrowse) then
         DM_EMPLOYEE.EE_RATES.Locate('EE_NBR; PRIMARY_RATE', VarArrayOf([wwdsSubMaster.DataSet['EE_NBR'], 'Y']), []);

      DM_COMPANY.CO_HR_POSITION_GRADES.Filter:= 'CO_HR_POSITIONS_NBR =  ' + IntToStr(wwdsSubMaster4.DataSet.FieldByName('CO_HR_POSITIONS_NBR').AsInteger);
      DM_COMPANY.CO_HR_POSITION_GRADES.Filtered:= True;
      DM_COMPANY.CO_HR_POSITION_GRADES.Locate('CO_HR_POSITIONS_NBR',wwdsSubMaster4.DataSet.FieldByName('CO_HR_POSITIONS_NBR').Value,[]);
    end;
end;

procedure TEDIT_EE_CL_PERSON.evcbPositionCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Assigned(wwlcHR_Position_Grades_Number) and
     (wwdsSubMaster4.DataSet.State in [dsInsert, dsEdit]) then
  begin
      SetCO_HR_POSITION_GRADESFilter;
      if  (evcbPosition.Value <> '') and (DM_COMPANY.CO_HR_POSITION_GRADES.RecordCount = 1) then
          wwdsSubMaster4.DataSet['CO_HR_POSITION_GRADES_NBR'] := DM_COMPANY.CO_HR_POSITION_GRADES['CO_HR_POSITION_GRADES_NBR']
      else
          wwdsSubMaster4.DataSet['CO_HR_POSITION_GRADES_NBR'] := null;
  end;
end;

procedure TEDIT_EE_CL_PERSON.PageControl1Change(Sender: TObject);
begin
  inherited;
  if (PageControl1.ActivePage = tshtQuick) then
  begin
     SetCO_HR_POSITION_GRADESFilter;
  end
  else if (PageControl1.ActivePage = tsDocuments)
           and Assigned(TPersonDocuments1.dsEditDoc.DataSet)
  then TPersonDocuments1.dsEditDoc.DataSet.First//refresh
  else if (PageControl1.ActivePage = tshtHR) then
  begin
    LoadCOGroupBenefits;
    SetACABenefitReference;
  end
  else if (PageControl1.ActivePage = tsESS) then FillUpESSGroups
  else if (PageControl1.ActivePage = tsACA) then
        AcaHistoryYear.ItemIndex := YearOf(SysTime)-2014;
end;

procedure TEDIT_EE_CL_PERSON.evcbPositionChange(Sender: TObject);
begin
  inherited;
  evcbPositionCloseUp(Sender,nil,nil,true);
end;

procedure TEDIT_EE_CL_PERSON.wwdsEmployeeDataChange(Sender: TObject; Field: TField);
begin
  inherited;

  if TDataSet(DM_EMPLOYEE.EE).State = dsBrowse then
  begin
    SetW2formatComboBox(EE_W2FormatType_ComboChoices2);
    btnUnblockAcc.Enabled := DM_CLIENT.EE.LOGIN_ATTEMPTS.AsInteger >= 3;
    editPassword.Text := DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.AsString;
    SetCO_HR_POSITION_GRADESFilter;
    FillUpESSGroups;
    LoadCOGroupBenefits;
  end;

  if TDataSet(DM_EMPLOYEE.EE).State = dsInsert then
    editPassword.Text := DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.AsString;
end;

procedure TEDIT_EE_CL_PERSON.btnUnblockAccClick(Sender: TObject);
var
  sPassword: String;
begin
  inherited;
  sPassword := ctx_Security.GenerateNewPassword;
  if EvDialog('Password', 'Please type a temporary password', sPassword) then
  begin
    if TDataSet(DM_EMPLOYEE.EE).State = dsBrowse then
      DM_EMPLOYEE.EE.Edit;

    DM_CLIENT.EE.LOGIN_ATTEMPTS.AsInteger := 0;
    DM_CLIENT.EE.LOGIN_DATE.AsDateTime := SysTime;

    DM_EMPLOYEE.EE.SELFSERVE_LAST_LOGIN.AsDateTime := DateOf(SysTime);

    DM_EMPLOYEE.EE.LOGIN_QUESTION1.Clear;
    DM_EMPLOYEE.EE.LOGIN_ANSWER1.Clear;
    DM_EMPLOYEE.EE.LOGIN_QUESTION2.Clear;
    DM_EMPLOYEE.EE.LOGIN_ANSWER2.Clear;
    DM_EMPLOYEE.EE.LOGIN_QUESTION3.Clear;
    DM_EMPLOYEE.EE.LOGIN_ANSWER3.Clear;

    DM_EMPLOYEE.EE.SEC_QUESTION1.Clear;
    DM_EMPLOYEE.EE.SEC_ANSWER1.Clear;
    DM_EMPLOYEE.EE.SEC_QUESTION2.Clear;
    DM_EMPLOYEE.EE.SEC_ANSWER2.Clear;

    editPassword.Text := sPassword;

    btnUnblockAcc.Enabled := False;
  end;
end;

procedure TEDIT_EE_CL_PERSON.wwdsEmployeeUpdateData(Sender: TObject);
begin
  inherited;

  if (DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.AsString <> editPassword.Text) then
  begin
    if dedtSS_UserName.Text <> '' then
      try
        ctx_Security.ValidatePassword(editPassword.Text);
      except
        on E: ESecurity do
        begin
          E.Details := 'Password requirements:'#13#13 + ctx_Security.GetPasswordRules.Value['Description'];
          raise
        end
        else
          raise;
      end;

    if editPassword.Text <> '' then
      DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.AsString := HashPassword(editPassword.Text)
    else
      DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.Clear;

    editPassword.Text := DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.AsString;

    DM_CLIENT.EE.SELFSERVE_LAST_LOGIN.AsDateTime := Date + Trunc(DM_SERVICE_BUREAU.SB.USER_PASSWORD_DURATION_IN_DAYS.Value);
  end;
end;

procedure TEDIT_EE_CL_PERSON.editPasswordChange(Sender: TObject);
begin
  inherited;
  if DM_EMPLOYEE.EE.SELFSERVE_PASSWORD.AsString <> editPassword.Text then
  begin
    if not (TDataSet(DM_EMPLOYEE.EE).State in [dsInsert, dsEdit]) then
      DM_EMPLOYEE.EE.Edit;
  end;
end;

procedure TEDIT_EE_CL_PERSON.dsAvaliableGroupsCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if DataSet['GROUP_TYPE'] = ESS_GROUPTYPE_PERSONALINFO then
    DataSet['GroupTypeName'] := 'Personal Info'
  else if DataSet['GROUP_TYPE'] = ESS_GROUPTYPE_TIMEOFF then
    DataSet['GroupTypeName'] := 'Time Off'
  else
    DataSet['GroupTypeName'] := 'Unknown type';
end;

procedure TEDIT_EE_CL_PERSON.sbAddESSGroupClick(Sender: TObject);
begin
  inherited;

  if Assigned(csCoGroupMember_benefit)
     and (csCoGroupMember_benefit.DataSet.State in [dsEdit, dsInsert]) then
     UpdateCoGroupMember_benefit;


  FESSGroupChanging := True;
  try
    if (dsAvaliableGroups.RecordCount > 0) and (dsAvaliableGroups['CO_GROUP_NBR'] <> null) then
    begin
      if  not DM_COMPANY.CO_GROUP_MEMBER.Locate('CO_GROUP_NBR; EE_NBR', VarArrayOf([dsAvaliableGroups['CO_GROUP_NBR'], DM_EMPLOYEE.EE.EE_NBR.AsInteger]), []) then
      begin
        DM_COMPANY.CO_GROUP_MEMBER.Append;
        try
          DM_COMPANY.CO_GROUP_MEMBER['CO_GROUP_NBR'] := dsAvaliableGroups['CO_GROUP_NBR'];
          DM_COMPANY.CO_GROUP_MEMBER['EE_NBR'] := DM_EMPLOYEE.EE.EE_NBR.AsInteger;
          DM_COMPANY.CO_GROUP_MEMBER.Post;
        except
          DM_COMPANY.CO_GROUP_MEMBER.Cancel;
          raise;
        end;
      end
      else
        raise Exception.Create('Record already exists in CO_GROUP_MEMBER table');

      dsAssignedGroups.Append;
      try
        dsAssignedGroups['GROUP_NAME'] := dsAvaliableGroups['GROUP_NAME'];
        dsAssignedGroups['GROUP_TYPE'] := dsAvaliableGroups['GROUP_TYPE'];
        dsAssignedGroups['CO_GROUP_NBR'] := dsAvaliableGroups['CO_GROUP_NBR'];
        dsAssignedGroups.Post;
      except
        dsAssignedGroups.Cancel;
        raise;
      end;

      dsAvaliableGroups.Delete;
    end;
    sbAddESSGroup.Enabled := dsAvaliableGroups.RecordCount > 0;
    sbDelESSGroup.Enabled := dsAssignedGroups.RecordCount > 0;
  finally
    FESSGroupChanging := False;
  end;
end;

function TEDIT_EE_CL_PERSON.CheckIfESSEnabled: boolean;
begin
  if (DM_COMPANY.CO.ENABLE_ESS.AsString <> GROUP_BOX_NO) or
    (DM_COMPANY.CO.ENABLE_BENEFITS.AsString <> GROUP_BOX_NO) or
    (DM_COMPANY.CO.ENABLE_TIME_OFF.AsString <> GROUP_BOX_NO) then
    Result := True
  else
    Result := False;
end;

procedure TEDIT_EE_CL_PERSON.FillUpESSGroups;
var
  Q: IevQuery;
begin
  if FESSGroupChanging then
    Exit;

  sbAddESSGroup.Enabled := False;
  sbDelESSGroup.Enabled := False;

  if DM_EMPLOYEE.EE.EE_NBR.AsString = '' then
    exit;

  if csLoading in dsAvaliableGroups.ComponentState then
    Exit;    
  try
    dsAvaliableGroups.DisableControls;
    dsAvaliableGroups.Close;
    dsAvaliableGroups.CreateDataSet;

    Q := TevQuery.Create('GenericSelectWithCondition');
    Q.Macros.AddValue('TABLENAME', 'CO_GROUP');
    Q.Macros.AddValue('COLUMNS', 'GROUP_NAME, GROUP_TYPE, CO_GROUP_NBR');
    Q.Macros.AddValue('CONDITION', '{AsOfNow<CO_GROUP>} and CO_NBR=' + DM_CLIENT.CO.CO_NBR.AsString +
      ' and GROUP_TYPE <>''' + ESS_GROUPTYPE_BENEFIT + '''');
    Q.Execute;

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      dsAvaliableGroups.Append;
      try
        dsAvaliableGroups['GROUP_NAME'] := Q.Result['GROUP_NAME'];
        dsAvaliableGroups['GROUP_TYPE'] := Q.Result['GROUP_TYPE'];
        dsAvaliableGroups['CO_GROUP_NBR'] := Q.Result['CO_GROUP_NBR'];
        dsAvaliableGroups.Post;
      except
        dsAvaliableGroups.Cancel;
        raise;
      end;
      Q.Result.Next;
    end;

    Q := TevQuery.Create('GenericSelectCurrentWithCondition');
    Q.Macros.AddValue('TABLENAME', 'CO_GROUP_MEMBER');
    Q.Macros.AddValue('COLUMNS', 'CO_GROUP_NBR');
    Q.Macros.AddValue('CONDITION', 'EE_NBR= ' + DM_EMPLOYEE.EE.EE_NBR.AsString +
      ' and CO_GROUP_NBR not in (select CO_GROUP_NBR from CO_GROUP t where {AsOfNow<t>} and GROUP_TYPE =''' + ESS_GROUPTYPE_BENEFIT + ''')');
    Q.Execute;

    try
      dsAssignedGroups.DisableControls;
      dsAssignedGroups.Close;
      dsAssignedGroups.CreateDataSet;

      Q.Result.First;
      while not Q.Result.Eof do
      begin
        if  dsAvaliableGroups.Locate('CO_GROUP_NBR', Q.Result['CO_GROUP_NBR'], []) then
        begin
          dsAssignedGroups.Append;
          try
            dsAssignedGroups['GROUP_NAME'] := dsAvaliableGroups['GROUP_NAME'];
            dsAssignedGroups['GROUP_TYPE'] := dsAvaliableGroups['GROUP_TYPE'];
            dsAssignedGroups['CO_GROUP_NBR'] := dsAvaliableGroups['CO_GROUP_NBR'];
            dsAssignedGroups.Post;
          except
            dsAssignedGroups.Cancel;
            raise;
          end;
          dsAvaliableGroups.Delete;
        end;

        Q.Result.next;
      end;

    finally
      dsAssignedGroups.EnableControls;
    end;
  finally
    dsAvaliableGroups.EnableControls;
  end;

  sbAddESSGroup.Enabled := dsAvaliableGroups.RecordCount > 0;
  sbDelESSGroup.Enabled := dsAssignedGroups.RecordCount > 0;
end;

procedure TEDIT_EE_CL_PERSON.sbDelESSGroupClick(Sender: TObject);
begin
  inherited;
  if Assigned(csCoGroupMember_benefit)
     and (csCoGroupMember_benefit.DataSet.State in [dsEdit, dsInsert]) then
     UpdateCoGroupMember_benefit;


  FESSGroupChanging := True;
  try
    if (dsAssignedGroups.RecordCount > 0) and (dsAssignedGroups['CO_GROUP_NBR'] <> null) then
    begin
      dsAvaliableGroups.Append;
      try
        dsAvaliableGroups['GROUP_NAME'] := dsAssignedGroups['GROUP_NAME'];
        dsAvaliableGroups['GROUP_TYPE'] := dsAssignedGroups['GROUP_TYPE'];
        dsAvaliableGroups['CO_GROUP_NBR'] := dsAssignedGroups['CO_GROUP_NBR'];
        dsAvaliableGroups.Post;
      except
        dsAvaliableGroups.Cancel;
        raise;
      end;
      if DM_COMPANY.CO_GROUP_MEMBER.Locate('CO_GROUP_NBR;EE_NBR', VarArrayOf([dsAssignedGroups['CO_GROUP_NBR'], DM_EMPLOYEE.EE.EE_NBR.AsInteger]), []) then
      begin
        try
          DM_COMPANY.CO_GROUP_MEMBER.Delete;
        except
          DM_COMPANY.CO_GROUP_MEMBER.cancel;
          raise;
        end;
      end
      else
        raise Exception.Create('Cannot find record in CO_GROUP_MEMBER table.');

      dsAssignedGroups.Delete;
    end;
    sbAddESSGroup.Enabled := dsAvaliableGroups.RecordCount > 0;
    sbDelESSGroup.Enabled := dsAssignedGroups.RecordCount > 0;
  finally
    FESSGroupChanging := False;
  end;
end;

procedure TEDIT_EE_CL_PERSON.LoadCOGroupBenefits;
var
 sl : TStringList;
begin
  if FESSGroupChanging then
    Exit;
    
  if DM_EMPLOYEE.EE.Active
     and (TDataSet(DM_EMPLOYEE.EE).State = dsBrowse)
     and (DM_EMPLOYEE.EE.FieldByName('CO_NBR').asInteger > 0) then
  begin
    cbCoGroupMember_benefit.Enabled := False;
    cbCoGroupMember_benefit.ItemIndex := -1;
    cbCoGroupMember_benefit.Items.CommaText := '';

    csCoGroup_benefit := TevDataSetHolder.CreateCloneAndRetrieveData( DM_COMPANY.CO_GROUP,
                         ' GROUP_TYPE = ''' + ESS_GROUPTYPE_BENEFIT + ''' and CO_NBR = ' + DM_EMPLOYEE.EE.FieldByName('CO_NBR').AsString);

    if csCoGroup_benefit.DataSet.RecordCount > 0 then
    begin
      cbCoGroupMember_benefit.Enabled := True;
      sl := csCoGroup_benefit.DataSet.GetFieldValueList('GROUP_NAME',True);
      try
        cbCoGroupMember_benefit.Items.CommaText := sl.CommaText;
      finally
         FreeandNil(sl);
      end;

      sl := csCoGroup_benefit.DataSet.GetFieldValueList('CO_GROUP_NBR',True);
      try
         csCoGroupMember_benefit := TevDataSetHolder.CreateCloneAndRetrieveData( DM_COMPANY.CO_GROUP_MEMBER,
                                 ' EE_NBR = ' +  DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString +
                                 ' and CO_GROUP_NBR in ('+ sl.CommaText +')' );
      finally
         FreeandNil(sl);
      end;

      if ( ( csCoGroupMember_benefit.DataSet.RecordCount > 0 )
           and csCoGroup_benefit.DataSet.Locate('CO_GROUP_NBR', csCoGroupMember_benefit.DataSet.FieldByName('CO_GROUP_NBR').AsInteger, []))
      then
        cbCoGroupMember_benefit.ItemIndex := cbCoGroupMember_benefit.Items.IndexOf(csCoGroup_benefit.DataSet.FieldByName('Group_Name').asString)
      else
        cbCoGroupMember_benefit.ItemIndex := -1;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.cbCoGroupMember_benefitCloseUp(
  Sender: TObject);
begin
  inherited;
  if Assigned(csCoGroup_benefit.DataSet)
     and (csCoGroup_benefit.DataSet.RecordCount > 0 )
  then begin
    if cbCoGroupMember_benefit.ItemIndex >= 0 then
    begin
      if (TDataSet(DM_EMPLOYEE.EE).State = dsBrowse) then DM_EMPLOYEE.EE.edit;
      if (csCoGroupMember_benefit.DataSet.State = dsBrowse) then csCoGroupMember_benefit.DataSet.Edit;

      csCoGroupMember_benefit.DataSet.FieldByName('EE_NBR').AsInteger :=  DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
      try
        if csCoGroup_benefit.DataSet.Locate('GROUP_NAME', cbCoGroupMember_benefit.Items[cbCoGroupMember_benefit.ItemIndex], [])
        then csCoGroupMember_benefit.DataSet.FieldByName('CO_GROUP_NBR').AsInteger := csCoGroup_benefit.DataSet.FieldByName('CO_GROUP_NBR').AsInteger
        else raise EevException.Create('Not Found Benefit Group');
      except
        DM_EMPLOYEE.EE.cancel;
        csCoGroupMember_benefit.DataSet.cancel;
      end;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.UpdateCoGroupMember_benefit;
begin
  if cbCoGroupMember_benefit.ItemIndex < 0 then
    csCoGroupMember_benefit.DataSet.Delete
  else
    csCoGroupMember_benefit.DataSet.post;

  ctx_DataAccess.PostDataSets([csCoGroupMember_benefit.DataSet]);
end;

procedure TEDIT_EE_CL_PERSON.cbCoGroupMember_benefitChange(
  Sender: TObject);
begin
  inherited;
  if (cbCoGroupMember_benefit.ItemIndex < 0)
     and Assigned(csCoGroupMember_benefit.DataSet)
     and (not csCoGroupMember_benefit.DataSet.IsEmpty) then
  begin
    if (TDataSet(DM_EMPLOYEE.EE).State = dsBrowse) then DM_EMPLOYEE.EE.edit;
    if (csCoGroupMember_benefit.DataSet.State = dsBrowse) then csCoGroupMember_benefit.DataSet.Edit;
  end;
end;

procedure TEDIT_EE_CL_PERSON.wwdsListDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  tsVMR.TabVisible := not BitBtn1Enabled;
  tsESS.TabVisible := not BitBtn1Enabled;
  if not BitBtn1Enabled then
  begin
    tsESS.TabVisible := CheckIfESSEnabled;
    tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  end;
end;

procedure TEDIT_EE_CL_PERSON.CopyW2FormatEEFieldTo(aFieldCaption, aFieldName: string);
var
  k: integer;
  sEEnbr: string;
  lFieldValue: variant;
  bUpdate : boolean;
begin
  sEEnbr := DM_CLIENT.EE.FieldByName('EE_NBR').AsString;
  lFieldValue := DM_CLIENT.EE.FieldByName( aFieldName ).Value;
  if sEEnbr <> '' then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy the '+ aFieldCaption +' for all employees. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');
        wwdsSubMaster.DataSet.DisableControls;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_CLIENT.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);

            bUpdate :=True;
            if aFieldName = 'W2' then
              if (VarToStr(lFieldValue) = EE_REPORT_RESULT_TYPE_FILE) and
                 (DM_CLIENT.EE.FieldByName('W2_FORM_ON_FILE').AsString = GROUP_BOX_NO) then
                  bUpdate :=false;
            if bUpdate then
            begin
              DM_CLIENT.EE.Edit;
              DM_CLIENT.EE[ aFieldName ] := lFieldValue;
              DM_CLIENT.EE.Post;
            end;
          end;
        finally
          ctx_EndWait;
          wwdsSubMaster.DataSet.EnableControls;
          ctx_DataAccess.PostDataSets([DM_CLIENT.EE]);
          DM_CLIENT.EE.Locate('EE_NBR',sEEnbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
end;


procedure TEDIT_EE_CL_PERSON.miW2FormatCopyClick(Sender: TObject);
begin
  inherited;
  CopyW2FormatEEFieldTo('W2', 'W2');
end;

procedure TEDIT_EE_CL_PERSON.mnW2FormOnFileClick(Sender: TObject);
begin
  inherited;
  CopyW2FormatEEFieldTo('Form On File', 'W2_FORM_ON_FILE');
end;

procedure TEDIT_EE_CL_PERSON.SetW2formatComboBox(const EE_W2Format : string);
var
 i : integer;
 s : String;
begin
    s := EE_W2Format;
    if rgW2onfile.ItemIndex = 0 then
      s := EE_ReportResultType_ComboChoices;
    cbW2Format.Items.Text := s;
    cbW2Format.ApplyList;
    cbW2Format.ItemIndex := -1;

    if assigned(cbW2Format.Field) and (not cbW2Format.Field.IsNull) then
    begin
      for i:=0 to cbW2Format.Items.Count - 1 do
      begin
         s := cbW2Format.Items[i];
         if s[Length(s)] = cbW2Format.Field.AsString[1] then
         begin
            cbW2Format.ItemIndex := i;
            break;
         end;
      end;
      if cbW2Format.ItemIndex = -1 then
      begin
        if (cbW2Format.datasource.State in [dsEdit,dsInsert]) and
           (cbW2Format.datasource.dataset.fieldbyname('W2').oldvalue<>'P') then
           cbW2Format.Field.Value := cbW2Format.datasource.dataset.fieldbyname('W2').oldvalue
        else
           cbW2Format.ItemIndex := 0;
      end;
    end;
end;

procedure TEDIT_EE_CL_PERSON.rgW2onfileChange(Sender: TObject);
begin
  inherited;
  SetW2formatComboBox(EE_W2FormatType_ComboChoices2);
end;

procedure TEDIT_EE_CL_PERSON.cbW2FormatCloseUp(Sender: TwwDBComboBox;
  Select: Boolean);
begin
  inherited;
  if wwdsSubMaster.DataSet.State = dsBrowse then
     SetW2formatComboBox(EE_W2FormatType_ComboChoices2);
end;

procedure TEDIT_EE_CL_PERSON.cbW2FormatDropDown(Sender: TObject);
begin
  inherited;
  SetW2formatComboBox(EE_W2FormatType_ComboChoices);
end;

procedure TEDIT_EE_CL_PERSON.mnDBDTCopyClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyEEFDBDTCopy;
end;

procedure TEDIT_EE_CL_PERSON.CopyEEFDBDTCopy;
var
  k: integer;
  sEEnbr: string;
  vDivNbr, vBranchNbr, vDepNbr, vTeamNbr : Variant;
begin
  sEEnbr := DM_CLIENT.EE.FieldByName('EE_NBR').AsString;
  if sEEnbr <> '' then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      vDivNbr:= wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value;
      vBranchNbr := wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value;
      vDepNbr := wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value;
      vTeamNbr := wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value;

      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy DBDT for all employees. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            if DM_CLIENT.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []) then
            begin
               DM_CLIENT.EE.Edit;
               DM_CLIENT.EE['CO_DIVISION_NBR'] := vDivNbr;
               DM_CLIENT.EE['CO_BRANCH_NBR'] := vBranchNbr;
               DM_CLIENT.EE['CO_DEPARTMENT_NBR'] := vDepNbr;
               DM_CLIENT.EE['CO_TEAM_NBR'] := vTeamNbr;
               DM_CLIENT.EE.Post;
            end;
          end;
        finally
          ctx_EndWait;
          ctx_DataAccess.PostDataSets([DM_CLIENT.EE]);
          DM_CLIENT.EE.Locate('EE_NBR',sEEnbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.mnPositionCopyClick(Sender: TObject);
begin
  inherited;

  if (wwdsSubMaster4.DataSet.State = dsBrowse) then
  begin
     CopyPositionAndPayGradeTo(True);
     evcbPosition.RefreshDisplay;
     wwlcHR_Position_Grades_Number.RefreshDisplay;
  end;
end;

procedure TEDIT_EE_CL_PERSON.CopyACAStatusTo(const AFieldName: string);
var
  k: integer;

  lFieldValue: variant;
  sEEnbr, clPersonNbr, coNbr: string;
  bRefresh: boolean;
begin
  sEEnbr := DM_CLIENT.EE.FieldByName('EE_NBR').AsString;
  lFieldValue := DM_CLIENT.EE.FieldByName(AFieldName).Value;
  if sEEnbr <> '' then
  begin
    with TEmployeeChoiceQuery.Create( Self ) do
    try
      dgChoiceList.Selected.Clear;
      dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      dgChoiceList.Selected.Add('CURRENT_HIRE_DATE'#9'21'#9'Hire Date'#9'F');
      if AFieldName = 'ACA_TYPE' then
       dgChoiceList.Selected.Add('ACA_STATUS_desc'#9'25'#9'ACA Status'#9'F');

      pnlEffectiveDate.Visible := true;

      dgChoiceList.ApplySelected;
      SetFilter( 'CUSTOM_EMPLOYEE_NUMBER <>'''+ DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString+'''' );
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'Employees';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      if AFieldName = 'HEALTHCARE_COVERAGE' then
         ConfirmAllMessage := 'This operation will copy the Healthcare Coverage for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'ACA_STATUS' then
         ConfirmAllMessage := 'This operation will copy the ACA Status for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'ACA_FORMAT' then
         ConfirmAllMessage := 'This operation will copy the ACA Format for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'ACA_TYPE' then
         ConfirmAllMessage := 'This operation will copy the ACA Form Type for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'ACA_CO_BENEFIT_SUBTYPE_NBR' then
         ConfirmAllMessage := 'This operation will copy the Lowest Cost Benefit for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'SY_FED_ACA_RELIEF_CODES_NBR' then
         ConfirmAllMessage := 'This operation will copy the Applicable Section 4980H for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'ACA_POLICY_ORIGIN' then
         ConfirmAllMessage := 'This operation will copy the ACA Policy Origin for all employees. Are you sure you want to do this?'
      else
      if AFieldName = 'BENEFITS_ELIGIBLE' then
         ConfirmAllMessage := 'This operation will copy the Benefits Eligible for all employees. Are you sure you want to do this?'
      else
         ConfirmAllMessage := 'This operation will copy the ACA Coverage Offer for all employees. Are you sure you want to do this?';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');
        coNbr:= DM_EMPLOYEE.EE.CO_NBR.AsString;
        sEEnbr:= DM_EMPLOYEE.EE.EE_NBR.AsString;
        clPersonNbr:= DM_CLIENT.CL_PERSON.CL_PERSON_NBR.AsString;
        bRefresh:= false;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_EMPLOYEE.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []);

            ctx_DBAccess.StartTransaction([dbtClient]);
            try
              ctx_DBAccess.UpdateFieldValue('EE', AFieldName, DM_EMPLOYEE.EE.EE_NBR.Value, dtBeginEffective.Date, DayBeforeEndOfTime, lFieldValue);
              ctx_DBAccess.CommitTransaction;
              bRefresh:= true;
            except
              ctx_DBAccess.RollbackTransaction;
              raise;
            end;
          end;
        finally
          ctx_EndWait;
          if bRefresh then
          begin
            DM_EMPLOYEE.EE.DisableControls;
            try
              DM_EMPLOYEE.EE.Close;
              DM_EMPLOYEE.EE.DataRequired('CO_NBR =' + coNbr);
            finally
              DM_EMPLOYEE.EE.UserFiltered:= True; 
              DM_EMPLOYEE.EE.Locate('EE_NBR', sEEnbr, []);
              DM_EMPLOYEE.EE.EnableControls;
            end;
           end;
           DM_CLIENT.EE.Locate('EE_NBR',sEEnbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.mnAcaStatusClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('ACA_STATUS');
end;

procedure TEDIT_EE_CL_PERSON.mnAcaFormatClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('ACA_FORMAT');
end;

procedure TEDIT_EE_CL_PERSON.mnAcaFormTypeClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('ACA_TYPE');
end;

procedure TEDIT_EE_CL_PERSON.evDBRadioGroup5Exit(Sender: TObject);
begin
  inherited;
  //GUI 2.0. AFTER LEAVING THIS FIELD, TAB ORDER GETS LOST???
  wwcbLabor_Distribution_Options.SetFocus;
end;

function TEDIT_EE_CL_PERSON.CheckEmployeeChilds(pEE_NBR: Integer; Check: Char): boolean;
var
  Q: IevQuery;
begin
  Result := False;
  Case Check of
  'D' :
    Q := TevQuery.Create('SELECT Count(*) FROM EE_SCHEDULED_E_DS t WHERE {AsOfNow<t>} and EE_NBR = :EE_NBR ');
  'P' :
    Q := TevQuery.Create('SELECT Count(*) FROM PR_CHECK t WHERE {AsOfNow<t>} and EE_NBR = :EE_NBR ');
  else
    Q := TevQuery.Create('SELECT Count(*) FROM EE_RATES t WHERE {AsOfNow<t>} and EE_NBR = :EE_NBR ');
  end;
  Q.Params.AddValue('EE_NBR', pEE_NBR);
  Q.Execute;
  if Q.Result.Fields[0].AsInteger > 0 then Result := True;
end;

procedure TEDIT_EE_CL_PERSON.DoBeforeDelete;
begin
  inherited;

  // Delete EE_STATES children
  DM_EMPLOYEE.EE_STATES.SaveState;
  try
    DM_EMPLOYEE.EE_STATES.Filtered := False;
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_NBR = ' + DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsString;
    DM_EMPLOYEE.EE_STATES.Filtered := True;

    DM_EMPLOYEE.EE_STATES.First;
    while not DM_EMPLOYEE.EE_STATES.Eof do
      DM_EMPLOYEE.EE_STATES.Delete;
  finally
    DM_EMPLOYEE.EE_STATES.LoadState;
  end;
end;

procedure TEDIT_EE_CL_PERSON.cbDivisionNbrSetLookupText(Sender: TObject; var AText: String);
begin
  if (Sender as TevDBLookupCombo).Tag = 1 then
    AText := '';
end;

procedure TEDIT_EE_CL_PERSON.wwdsSubMaster2StateChange(Sender: TObject);
begin
  inherited;
  if TevClientDataSet(DM_CLIENT.CL_PERSON).State = dsEdit then
     DM_EMPLOYEE.EE.Edit;
end;

procedure TEDIT_EE_CL_PERSON.LoadACACOBenefits;
var
 sl : TStringList;
Begin
  if (DM_COMPANY.CO.FieldByName('CO_NBR').asInteger > 0) then
  begin
    evLowestCoBenefits.ItemIndex := -1;
    evLowestCoBenefits.Items.CommaText := '';

    cdCOBenefitSubType.Close;
    with TExecDSWrapper.Create('select a.CO_BENEFITS_NBR, b.CO_BENEFIT_SUBTYPE_NBR, b.DESCRIPTION ' +
                          ' from CO_BENEFITS a,   CO_BENEFIT_SUBTYPE b ' +
                          ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and  a.CO_NBR = :coNbr and ' +
                          ' b.ACA_LOWEST_COST = ''Y'' ' +
                          ' order by b.DESCRIPTION') do
    begin
      SetParam('coNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.GetCustomData(cdCOBenefitSubType, AsVariant);
    end;
    cdCOBenefitSubType.Open;
    cdCOBenefitSubType.IndexFieldNames := 'CO_BENEFITS_NBR';

    if cdCOBenefitSubType.RecordCount > 0 then
    begin
      cdCOBenefits.Close;
      with TExecDSWrapper.Create('select DISTINCT  a.CO_BENEFITS_NBR, a.BENEFIT_NAME ' +
                              ' from CO_BENEFITS a,   CO_BENEFIT_SUBTYPE b ' +
                              ' where {AsOfNow<a>} and {AsOfNow<b>} and a.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and ' +
                              ' a.CO_NBR = :coNbr and b.ACA_LOWEST_COST = ''Y'' ' +
                              ' order by a.BENEFIT_NAME') do
      begin
        SetParam('coNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
        ctx_DataAccess.GetCustomData(cdCOBenefits, AsVariant);
      end;
      cdCOBenefits.Open;
      cdCOBenefits.IndexFieldNames := 'CO_BENEFITS_NBR';

      if cdCOBenefits.RecordCount > 0 then
      begin
        sl := cdCOBenefits.GetFieldValueList('BENEFIT_NAME',True);
        try
          evLowestCoBenefits.Items.CommaText := sl.CommaText;
        finally
          FreeandNil(sl);
        end;
      end;
    end;
  end;
end;

procedure TEDIT_EE_CL_PERSON.SetACABenefitReference;
begin
   if wwdsSubMaster.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').IsNull then
   begin
      FidxBenefitReference := -1;
      evLowestCoBenefits.ItemIndex := -1;
      exit;
   end;

   if ((cdCOBenefitSubType.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger <>
              wwdsSubMaster.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').AsInteger) or
           (evLowestCoBenefits.ItemIndex = -1)) then
   begin
    FidxBenefitReference := -1;
    if ( evLowestCoBenefits.Items.Count > 0) then
    begin
      cdCOBenefitSubType.Filter:= '';
      cdCOBenefitSubType.Filtered:= False;
      cdCOBenefitSubType.Locate('CO_BENEFIT_SUBTYPE_NBR',wwdsSubMaster.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').AsInteger,[]);
      cdCOBenefits.Locate('CO_BENEFITS_NBR', cdCOBenefitSubType.FieldByName('CO_BENEFITS_NBR').AsInteger,[]);
      cdCOBenefitSubType.Filter:= 'CO_BENEFITS_NBR = ' + cdCOBenefits.fieldByName('CO_BENEFITS_NBR').AsString;
      cdCOBenefitSubType.Filtered:= True;
      evLowestCoBenefits.ItemIndex := evLowestCoBenefits.Items.IndexOf(cdCOBenefits.FieldByName('BENEFIT_NAME').asString);
    end;
    FidxBenefitReference := evLowestCoBenefits.ItemIndex;
   end;
end;

procedure TEDIT_EE_CL_PERSON.evLowestCoBenefitsChange(Sender: TObject);
begin
  inherited;
  if cdCOBenefits.Active and (Trim(evLowestCoBenefits.Text) = '' ) and (FidxBenefitReference <> -1) then
  begin
    if not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) then
       wwdsSubMaster.DataSet.Edit;

    FidxBenefitReference := -1;
    cdCOBenefits.Locate('BENEFIT_NAME',evLowestCoBenefits.Text,[]);

    wwdsSubMaster.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').Clear;
  end;
end;

procedure TEDIT_EE_CL_PERSON.evLowestCoBenefitsCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  inherited;
  if (FidxBenefitReference <> evLowestCoBenefits.ItemIndex) then
  begin
     if not (wwdsSubMaster.DataSet.State in [dsInsert, dsEdit]) then
        wwdsSubMaster.DataSet.Edit;

     FidxBenefitReference := evLowestCoBenefits.ItemIndex;
     cdCOBenefits.Locate('BENEFIT_NAME',evLowestCoBenefits.Text,[]);

     cdCOBenefitSubType.Filter:= 'CO_BENEFITS_NBR = ' + cdCOBenefits.fieldByName('CO_BENEFITS_NBR').AsString;
     cdCOBenefitSubType.Filtered:= True;

     wwdsSubMaster.DataSet.FieldByName('ACA_CO_BENEFIT_SUBTYPE_NBR').Clear;
  end;
end;

procedure TEDIT_EE_CL_PERSON.mnAcaLowestCostClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('ACA_CO_BENEFIT_SUBTYPE_NBR');
end;

procedure TEDIT_EE_CL_PERSON.mnAcaPolicyOriginClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('ACA_POLICY_ORIGIN');
end;

procedure TEDIT_EE_CL_PERSON.mnAcaCoverageClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('SY_FED_ACA_OFFER_CODES_NBR');
end;

procedure TEDIT_EE_CL_PERSON.mnAcaSafeHarborClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('SY_FED_ACA_RELIEF_CODES_NBR');
end;

procedure TEDIT_EE_CL_PERSON.mnHealthCareClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('HEALTHCARE_COVERAGE');
end;

procedure TEDIT_EE_CL_PERSON.mnSSNClick(Sender: TObject);
var
 FSSN: TPOPUP_SSN;
begin
 inherited;

  if ctx_AccountRights.Functions.GetState('BLOCK_CHANGING_OF_SSN') <> stEnabled then
  begin
    if MessageDlg('Are you sure you want to change this social security number?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      FSSN := TPOPUP_SSN.Create( Self );
      try
        if FSSN.ShowModal = mrOK then
        begin
          ctx_DBAccess.StartTransaction([dbtClient]);
          try
            ctx_DBAccess.UpdateFieldValue('EE', 'CL_PERSON_NBR', DM_EMPLOYEE.EE.EE_NBR.Value, BeginOfTime, DayBeforeEndOfTime, FSSN.cdClPerson.FieldByName('CL_PERSON_NBR').asInteger);
            ctx_DBAccess.CommitTransaction;

            DM_EMPLOYEE.EE.RefreshRecord(DM_EMPLOYEE.EE.EE_NBR.Value);
            DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE.CL_PERSON_NBR.Value, []);
          except
            ctx_DBAccess.RollbackTransaction;
            raise;
          end;
        end;
      finally
        FSSN.Free;
      end;
    end
  end  
  else
    EvMessage('You are not authorized to change Social Security Numbers.', mtError, [mbOK]);
end;

procedure TEDIT_EE_CL_PERSON.mnBenefitsEligibleClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyACAStatusTo('BENEFITS_ELIGIBLE');
end;

procedure TEDIT_EE_CL_PERSON.mnPBJWorksClick(Sender: TObject);
begin
  inherited;
  if (wwdsSubMaster.DataSet.State = dsBrowse) then
     CopyEEFieldTo('Number of days after Hire Date work starts', 'PBJ_ORIENTATION_DAYS');
end;

function TEDIT_EE_CL_PERSON.getCodeText( const fTable, fCode, fDesc: String):string;
var
 Q: IEvQuery;
begin
  result:= '';
  Q := TEvQuery.Create('Select ' + fTable + '_NBR, ' +  fCode + ',' + fDesc + ' from ' + fTable  +
       ' where {AsOfNow<' + fTable + '>} order by ' + fCode);
  Q.Execute;
  Q.Result.First;
  while not Q.Result.Eof do
  begin
    if result <> '' then
      result := result + #13;
    result:= result + Q.Result.FieldByName(fCode).AsString + '-' + Q.Result.FieldByName(fDesc).AsString + #9 + Q.Result.FieldByName(fTable + '_NBR').AsString;
    Q.Result.Next;
  end;
  result:= result + #13 + 'Multiple Values Selected'#9'9999';
end;


procedure TEDIT_EE_CL_PERSON.ACAHistory;
begin

  cbACA_COVERAGE_OFFER.Items.Text := getCodeText('SY_FED_ACA_OFFER_CODES','ACA_OFFER_CODE', 'ACA_OFFER_CODE_DESCRIPTION');
  cbEE_ACA_SAFE_HARBOR.Items.Text := getCodeText('SY_FED_ACA_RELIEF_CODES','ACA_RELIEF_CODE', 'ACA_RELIEF_CODE_DESCRIPTION');

  cdACAHistory:= ctx_RemoteMiscRoutines.GetACAHistory(DM_EMPLOYEE.EE.EE_NBR.AsInteger, StrToInt(AcaHistoryYear.Text));
  cdACAHistory.First;
  grACAHistory.DataSource.DataSet:= cdACAHistory.vclDataSet;
  btnACAHistory.Enabled:= false;

  wwdsSubMaster.DataSet.Cancel;

  btnACAPost.Enabled:= False;
  btnNavCancel.Enabled:= btnACAPost.Enabled;
  AcaHistoryYear.Enabled:= not btnACAPost.Enabled;
  ACAHistoryState:= false;
  if (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') = stEnabled) then
    ClearReadOnly;
end;

procedure TEDIT_EE_CL_PERSON.btnACAHistoryClick(Sender: TObject);
begin
  inherited;
  ACAHistory;
end;

procedure TEDIT_EE_CL_PERSON.dsACAHistoryDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  if dsACAHistory.DataSet.FieldByName('SY_FED_ACA_OFFER_CODES_NBR').AsInteger = 9999 then
     cbACA_COVERAGE_OFFER.ReadOnly:= True
  else
     cbACA_COVERAGE_OFFER.ReadOnly:= false;

  if dsACAHistory.DataSet.FieldByName('SY_FED_ACA_RELIEF_CODES_NBR').AsInteger = 9999 then
     cbEE_ACA_SAFE_HARBOR.ReadOnly:= True
  else
     cbEE_ACA_SAFE_HARBOR.ReadOnly:= false;
end;

procedure TEDIT_EE_CL_PERSON.PostACAHistory;
begin
   grACAHistory.DataSource.DataSet.DisableControls;
   try

    grACAHistory.DataSource.DataSet.First;
    ctx_RemoteMiscRoutines.PostACAHistory(DM_EMPLOYEE.EE.EE_NBR.AsInteger, StrToInt(AcaHistoryYear.Text), cdACAHistory);
    btnACAHistory.Enabled:= True;

    cdACAHistory:= ctx_RemoteMiscRoutines.GetACAHistory(DM_EMPLOYEE.EE.EE_NBR.AsInteger, StrToInt(AcaHistoryYear.Text));
    cdACAHistory.First;
    grACAHistory.DataSource.DataSet:= cdACAHistory.vclDataSet;
    btnACAHistory.Enabled:= false;

    btnACAPost.Enabled:= False;
    btnNavCancel.Enabled:= btnACAPost.Enabled;
    AcaHistoryYear.Enabled:= not btnACAPost.Enabled;
    wwdsSubMaster.DataSet.Cancel;
    DM_EMPLOYEE.EE.RefreshFieldValue('SY_FED_ACA_OFFER_CODES_NBR');
    DM_EMPLOYEE.EE.RefreshFieldValue('SY_FED_ACA_RELIEF_CODES_NBR');
    ACAHistoryState:= false;
   finally
     if (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') = stEnabled) then
         ClearReadOnly;
     grACAHistory.DataSource.DataSet.EnableControls;
   end;
end;

procedure TEDIT_EE_CL_PERSON.btnACAPostClick(Sender: TObject);
begin
  inherited;
  if EvMessage('Do you want to save changes?',  mtConfirmation, [mbYes, mbNo]) = mrYes then
    PostACAHistory;
end;

procedure TEDIT_EE_CL_PERSON.cbACA_COVERAGE_OFFERChange(Sender: TObject);
var
    oldAca, newAca : variant;
    sField: String;

    procedure UpdateNextOnes;
    begin
      while not cdACAHistory.vclDataSet.Eof do
      begin
          if (oldAca = cdACAHistory.vclDataSet.FieldByName(sField).Value) then
          begin
           if dsACAHistory.State = dsbrowse then
             cdACAHistory.vclDataSet.Edit;
           cdACAHistory.vclDataSet.FieldByName(sField).Value:= newAca;
          end
          else
          begin
           cdACAHistory.vclDataSet.Prior;
           break;
          end;
          cdACAHistory.vclDataSet.Next;
      end;
    end;

begin
  inherited;
  if dsACAHistory.State = dsEdit then
  begin

    if not ACAHistoryState then
    begin
      SetReadOnly(True);
      grACAHistory.SecurityRO := GetIfReadOnly or (ctx_AccountRights.Functions.GetState('UPDATE_ACA_AS_OF') <> stEnabled);
      cbACA_COVERAGE_OFFER.SecurityRO:= grACAHistory.SecurityRO;
      cbEE_ACA_SAFE_HARBOR.SecurityRO:= grACAHistory.SecurityRO;
      btnACAPost.SecurityRO := false;
      btnNavCancel.SecurityRO := false;
    end;

    ACAHistoryState:= True;
    wwdsSubMaster.DataSet.Edit;

    btnACAPost.Enabled:= True;
    btnNavCancel.Enabled:= True;
    AcaHistoryYear.Enabled:= false;

    sField:= TevDBComboBox(Sender).DataField;

    oldAca:= cdACAHistory.vclDataSet.FieldByName(sField).Value;
    newAca:= TevDBComboBox(Sender).Value;
    UpdateNextOnes;

  end;
end;

procedure TEDIT_EE_CL_PERSON.cbACA_COVERAGE_OFFERDropDown(Sender: TObject);
begin
  inherited;
  cbACA_COVERAGE_OFFER.Items.Text := getCodeText('SY_FED_ACA_OFFER_CODES','ACA_OFFER_CODE', 'ACA_OFFER_CODE_DESCRIPTION');

  if cdACAHistory.vclDataSet.FieldByName('SY_FED_ACA_OFFER_CODES_NBR').Value <> 9999 then
    with TwwDBComboBox(Sender).Items do
      delete(TwwDBComboBox(Sender).Items.Count -1);
end;

procedure TEDIT_EE_CL_PERSON.cbEE_ACA_SAFE_HARBORDropDown(Sender: TObject);
begin
  inherited;

  cbEE_ACA_SAFE_HARBOR.Items.Text := getCodeText('SY_FED_ACA_RELIEF_CODES','ACA_RELIEF_CODE', 'ACA_RELIEF_CODE_DESCRIPTION');
  if cdACAHistory.vclDataSet.FieldByName('SY_FED_ACA_RELIEF_CODES_NBR').Value <> 9999 then
    with TwwDBComboBox(Sender).Items do
      delete(TwwDBComboBox(Sender).Items.Count -1);
end;

procedure TEDIT_EE_CL_PERSON.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = tsACA then
    AllowChange := not btnACAPost.Enabled;

  if not AllowChange then
     EvErrMessage('You must post or cancel changes.');
end;

procedure TEDIT_EE_CL_PERSON.cbACA_COVERAGE_OFFERExit(Sender: TObject);
begin
  inherited;
  cbACA_COVERAGE_OFFER.Items.Text := getCodeText('SY_FED_ACA_OFFER_CODES','ACA_OFFER_CODE', 'ACA_OFFER_CODE_DESCRIPTION');
end;

procedure TEDIT_EE_CL_PERSON.cbEE_ACA_SAFE_HARBORExit(Sender: TObject);
begin
  inherited;
  cbEE_ACA_SAFE_HARBOR.Items.Text := getCodeText('SY_FED_ACA_RELIEF_CODES','ACA_RELIEF_CODE', 'ACA_RELIEF_CODE_DESCRIPTION');
end;



initialization
  Classes.RegisterClass(TEDIT_EE_CL_PERSON);

end.

