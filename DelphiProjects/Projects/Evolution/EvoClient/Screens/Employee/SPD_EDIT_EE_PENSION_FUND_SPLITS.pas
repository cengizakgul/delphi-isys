// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_PENSION_FUND_SPLITS;

interface

uses
   SDataStructure, wwdbedit, SPD_EDIT_EE_BASE, StdCtrls,
  ExtCtrls, DBCtrls, wwdblook, Mask, DBActns, Classes, ActnList, Db,
  Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, EvUIComponents, EvUtils, EvClientDataSet,
  ISBasicClasses, isUIwwDBEdit, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel, Forms,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_PENSION_FUND_SPLITS = class(TEDIT_EE_BASE)
    tshtPension_Fund_Splits: TTabSheet;
    pnlInfo: TisUIFashionPanel;
    pnlList: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    lablFund: TevLabel;
    lablE_D_Code: TevLabel;
    lablPercentage: TevLabel;
    dedtPercentage: TevDBEdit;
    wwlcFund: TevDBLookupCombo;
    wwlcE_D_Code: TevDBLookupCombo;
    drgpEE_ER: TevDBRadioGroup;
    sbPensionSplits: TScrollBox;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_EE_PENSION_FUND_SPLITS.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_CLIENT.CL_FUNDS, aDS);
  inherited;
end;

function TEDIT_EE_PENSION_FUND_SPLITS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_PENSION_FUND_SPLITS;
end;

function TEDIT_EE_PENSION_FUND_SPLITS.GetInsertControl: TWinControl;
begin
  Result := wwlcFund;
end;

initialization
  RegisterClass(TEDIT_EE_PENSION_FUND_SPLITS);

end.
