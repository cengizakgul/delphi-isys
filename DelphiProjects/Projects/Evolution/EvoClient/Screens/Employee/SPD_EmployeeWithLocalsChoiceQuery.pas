// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EmployeeWithLocalsChoiceQuery;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, SPD_EmployeeChoiceQuery,
  SDataStructure, ActnList, Db,   Wwdatsrc,
  Grids, Wwdbigrd, Wwdbgrid, StdCtrls, Buttons, ExtCtrls, Variants,
  SDDClasses, SDataDictclient, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, ISBasicClasses, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEmployeeWithLocalsChoiceQuery = class(TEmployeeChoiceQuery)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Setup( eeNbr, coLocalNbr: Variant );
  end;

implementation

uses
  evutils;
{$R *.DFM}

{ TEmployeeWithLocalsChoiceQuery }

procedure CreateDS( cds: TevClientDataSet; src: TDataSet );
  procedure AddField( aSrcField: TField );
  var
    aField: TField;
  type
    TFieldClass = class of TField;
  begin
    aField := TFieldClass(aSrcField.ClassType).Create(cds);
    with aField do
    begin
      FieldKind := fkData;
      Visible := aSrcField.Visible;
      Size := aSrcField.Size;
      FieldName := aSrcField.FieldName;
      Required := aSrcField.Required;
      DataSet := cds;
      DisplayLabel := aSrcField.DisplayLabel;
    end;
    if aSrcField is TBCDField then
      TBCDField(aField).Precision := TBCDField(aSrcField).Precision;
    if aSrcField.DataType = ftFixedChar then
      TStringField(aField).FixedChar := True;
  end;
var
  i: integer;
begin
  for i := 0 to src.FieldCount-1 do
    AddField( src.Fields[i] );
  cds.CreateDataSet;
end;

procedure AddRec( cds: TDataSet; src: TDataSet );
var
  i: integer;
begin
  cds.Append;
  try
    for i := 0 to src.FieldCount-1 do
      cds[ src.Fields[i].FieldName ] := src.Fields[i].Value;
  except
    cds.Cancel;
    raise;
  end;
  cds.Post;
end;

procedure TEmployeeWithLocalsChoiceQuery.Setup(eeNbr, coLocalNbr: Variant);
var
  saveEE: TBookmark;
begin
  saveEE := DM_EMPLOYEE.EE.GetBookmark;
  try
    dgChoiceList.DataSource := nil;
    cdsChoiceList.Close;
    CreateDS( cdsChoiceList, DM_EMPLOYEE.EE );
    cdsChoiceList.Open;
  //  SetSelectedFromFields( dgChoiceList, cdsChoiceList, []);

    DM_EMPLOYEE.EE.First;
    while not DM_EMPLOYEE.EE.Eof do
    begin
      if ( DM_EMPLOYEE.EE['EE_NBR'] <> eeNbr ) and
         not VarIsNull( DM_EMPLOYEE.EE_LOCALS.Lookup( 'CO_LOCAL_TAX_NBR', coLocalNbr, 'CO_LOCAL_TAX_NBR') ) then
         AddRec( cdsChoiceList, DM_EMPLOYEE.EE );
      DM_EMPLOYEE.EE.Next;
    end;
    cdsChoiceList.First;
    dgChoiceList.DataSource := dsChoiceList;
  finally
    DM_EMPLOYEE.EE.GotoBookmark( saveEE );
    DM_EMPLOYEE.EE.FreeBookmark( saveEE );
  end;
end;

end.
