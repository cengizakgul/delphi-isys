inherited EDIT_EE_SCHEDULED_E_DS: TEDIT_EE_SCHEDULED_E_DS
  Width = 749
  Height = 590
  inherited Panel1: TevPanel
    Width = 749
    inherited pnlFavoriteReport: TevPanel
      Left = 597
    end
    inherited pnlTopLeft: TevPanel
      Width = 597
    end
  end
  inherited PageControl1: TevPageControl
    Top = 89
    Width = 749
    Height = 501
    HelpContext = 19565
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 741
        Height = 472
        inherited pnlBorder: TevPanel
          Width = 737
          Height = 468
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 737
          Height = 468
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 0
              Width = 800
              BevelOuter = bvNone
              ParentColor = True
            end
            inherited pnlSubbrowse: TevPanel
              Left = 404
              Top = 132
            end
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 689
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 689
            Height = 361
            inherited Splitter1: TevSplitter
              Height = 357
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 357
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 277
                IniAttributes.SectionName = 'TEDIT_EE_SCHEDULED_E_DS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 367
              Height = 357
              inherited wwDBGrid4: TevDBGrid
                Width = 317
                Height = 277
                IniAttributes.SectionName = 'TEDIT_EE_SCHEDULED_E_DS\wwDBGrid4'
                OnRowChanged = wwDBGrid4RowChanged
              end
            end
          end
        end
      end
    end
    object tshtBrowse_Scheduled_E_DS: TTabSheet
      Caption = 'Browse Scheduled E/Ds'
      ImageIndex = 14
      object sbBrowseEDs: TScrollBox
        Left = 0
        Top = 0
        Width = 681
        Height = 366
        Align = alClient
        TabOrder = 0
        object pnlBrowseEDBody: TevPanel
          Left = 0
          Top = 0
          Width = 677
          Height = 362
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlFashionBrowseEDs: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 661
            Height = 346
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            OnResize = pnlFashionBrowseEDsResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Browse Scheduled Earnings and Deductions'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlBrowseAlign: TevPanel
              Left = 12
              Top = 36
              Width = 629
              Height = 290
              Align = alClient
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object grdEESchedView: TevDBGrid
                Left = 0
                Top = 0
                Width = 629
                Height = 290
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_E_D_CODE_NUMBER'#9'10'#9'E/D Code'#9'F'
                  'DESCRIPTION'#9'28'#9'Description'#9'F'
                  'CALCULATION_TYPE'#9'10'#9'Calculation Type'#9'F'
                  'AMOUNT'#9'10'#9'Amount'#9'F'
                  'PERCENTAGE'#9'10'#9'Percentage'#9'F'
                  'FREQUENCY'#9'10'#9'Frequency'#9'F'
                  'EFFECTIVE_START_DATE'#9'10'#9'Eff Start Date'#9'F'
                  'EFFECTIVE_END_DATE'#9'10'#9'Eff End Date'#9'F'
                  'SCHEDULED_E_D_ENABLED'#9'10'#9'Enabled'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_EE_SCHEDULED_E_DS\grdEESchedView'
                IniAttributes.Delimiter = ';;'
                IniAttributes.CheckNewFields = False
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsEEScheduledView
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                PopupMenu = PopupMenu1
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnTitleButtonClick = grdEESchedViewTitleButtonClick
                OnDblClick = grdEESchedViewDblClick
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtDetail_1: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbDetails1: TScrollBox
        Left = 0
        Top = 0
        Width = 741
        Height = 472
        Align = alClient
        TabOrder = 0
        object pnlEDInfo: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 330
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Earning / Deduction Code'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablE_D: TevLabel
            Left = 12
            Top = 35
            Width = 57
            Height = 16
            Caption = '~E/D Code'
            FocusControl = wwlcE_D
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPercentage: TevLabel
            Left = 100
            Top = 1143
            Width = 8
            Height = 13
            Caption = '%'
          end
          object EDLabel: TevLabel
            Left = 80
            Top = 54
            Width = 3
            Height = 13
          end
          object wwlcE_D: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 60
            Height = 21
            HelpContext = 19571
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ED_Lookup'#9'20'#9'ED_Lookup'#9'F'
              'CodeDescription'#9'40'#9'CodeDescription'#9'F')
            DataField = 'CL_E_DS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
            LookupField = 'CL_E_DS_NBR'
            Style = csDropDownList
            AutoSelect = False
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
            OnChange = wwlcE_DChange
            OnCloseUp = wwlcE_DCloseUp
          end
        end
        object fpDestinationDetails: TisUIFashionPanel
          Left = 695
          Top = 8
          Width = 208
          Height = 334
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Destination Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object labl_Child_Support: TevLabel
            Left = 12
            Top = 74
            Width = 177
            Height = 13
            AutoSize = False
            Caption = 'Child Support Case'
            FocusControl = wwlcChildSupport
          end
          object labl_Garnishment_ID: TevLabel
            Left = 12
            Top = 113
            Width = 169
            Height = 13
            AutoSize = False
            Caption = 'Garnishment/Loan ID'
            FocusControl = dedt_Garnishment_ID
          end
          object lablAgency: TevLabel
            Left = 12
            Top = 35
            Width = 36
            Height = 13
            Caption = 'Agency'
            FocusControl = wwlcAgency
          end
          object labl_EE_direct_Deposit: TevLabel
            Left = 12
            Top = 188
            Width = 170
            Height = 13
            AutoSize = False
            Caption = 'EE Direct Deposit'
            FocusControl = wwDBLookupCombo1
          end
          object lablTake_Home_Pay: TevLabel
            Left = 12
            Top = 277
            Width = 125
            Height = 13
            AutoSize = False
            Caption = 'Take Home Pay'
            FocusControl = dedtTake_Home_Pay
          end
          object bevelDestination: TEvBevel
            Left = 12
            Top = 170
            Width = 175
            Height = 3
            Shape = bsTopLine
          end
          object wwlcChildSupport: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 175
            Height = 21
            HelpContext = 19568
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_CASE_NUMBER'#9'40'#9'CUSTOM CASE NUMBER')
            DataField = 'EE_CHILD_SUPPORT_CASES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_EE_CHILD_SUPPORT_CASES.EE_CHILD_SUPPORT_CASES
            LookupField = 'EE_CHILD_SUPPORT_CASES_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object dedt_Garnishment_ID: TevDBEdit
            Left = 12
            Top = 128
            Width = 175
            Height = 21
            HelpContext = 19583
            DataField = 'GARNISNMENT_ID'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcAgency: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 175
            Height = 21
            HelpContext = 19570
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
            DataField = 'CL_AGENCY_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_AGENCY.CL_AGENCY
            LookupField = 'CL_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 204
            Width = 175
            Height = 21
            HelpContext = 19596
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'EE_BANK_ACCOUNT_NUMBER'#9'20'#9'EE_BANK_ACCOUNT_NUMBER'
              'EE_BANK_ACCOUNT_TYPE'#9'1'#9'EE_BANK_ACCOUNT_TYPE'#9'F'
              'EE_ABA_NUMBER'#9'9'#9'EE_ABA_NUMBER'
              'IN_PRENOTE'#9'1'#9'IN_PRENOTE'
              'ADDENDA'#9'12'#9'ADDENDA')
            DataField = 'EE_DIRECT_DEPOSIT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_EE_DIRECT_DEPOSIT.EE_DIRECT_DEPOSIT
            LookupField = 'EE_DIRECT_DEPOSIT_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object DBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 231
            Width = 175
            Height = 39
            HelpContext = 19597
            Caption = '~Deduct Whole Check'
            Columns = 2
            DataField = 'DEDUCT_WHOLE_CHECK'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
            OnChange = DBRadioGroup1Change
          end
          object dedtTake_Home_Pay: TevDBEdit
            Left = 12
            Top = 293
            Width = 175
            Height = 21
            HelpContext = 19587
            DataField = 'TAKE_HOME_PAY'
            DataSource = wwdsDetail
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpCalculation: TisUIFashionPanel
          Left = 8
          Top = 109
          Width = 330
          Height = 348
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCalculation'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Calculation'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCalculation_Type: TevLabel
            Left = 12
            Top = 35
            Width = 153
            Height = 13
            AutoSize = False
            Caption = 'Calculation Method'
            FocusControl = wwcbCalculation_Type
          end
          object lablAmount: TevLabel
            Left = 12
            Top = 74
            Width = 36
            Height = 13
            Caption = 'Amount'
            FocusControl = dedtAmount
          end
          object lablE_D_Group: TevLabel
            Left = 165
            Top = 35
            Width = 73
            Height = 13
            AutoSize = False
            Caption = 'E/D Group'
            FocusControl = wwlcE_D_Group
          end
          object lblPercentage: TevLabel
            Left = 165
            Top = 74
            Width = 66
            Height = 13
            Caption = '% Percentage'
            FocusControl = dedtAmount
          end
          object evLabel5: TevLabel
            Left = 12
            Top = 113
            Width = 103
            Height = 16
            Caption = '~Always Pay/Deduct'
            FocusControl = evDBComboBox2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablScheduledEDGroup: TevLabel
            Left = 165
            Top = 152
            Width = 145
            Height = 13
            Caption = 'Multiple Scheduled E/D Group'
            FocusControl = wwlcScheduledEDGroup
          end
          object lablPriority: TevLabel
            Left = 12
            Top = 152
            Width = 49
            Height = 13
            AutoSize = False
            Caption = 'Priority'
            FocusControl = dbsePriority
          end
          object evLabel7: TevLabel
            Left = 12
            Top = 279
            Width = 142
            Height = 13
            AutoSize = False
            Caption = 'Employee Benefit'
            FocusControl = evcbEEBenefitReference
          end
          object evBtnEEBenefit: TevSpeedButton
            Left = 251
            Top = 294
            Width = 59
            Height = 21
            Caption = 'Create'
            HideHint = True
            AutoSize = False
            OnClick = evBtnEEBenefitClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
              CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
              E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
              7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
              82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
              9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
              55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
              5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
              CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
              0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
              76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
              FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
              16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
              8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
              FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
              38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
              9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
              FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
              51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
              9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
              66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
              9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
              76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
              9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
              FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
              59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
              9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
              FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
              0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
              87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
              FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
              55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
              556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
              B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
              88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
              E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
              78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentColor = False
            ShortCut = 0
          end
          object lablBenefitReference: TevLabel
            Left = 12
            Top = 240
            Width = 142
            Height = 13
            AutoSize = False
            Caption = 'Company Benefit'
          end
          object evLabel1: TevLabel
            Left = 165
            Top = 240
            Width = 125
            Height = 13
            AutoSize = False
            Caption = 'Company Benefit Rate'
            FocusControl = evcbBenefitSubType
          end
          object EvBevel1: TEvBevel
            Left = 12
            Top = 216
            Width = 295
            Height = 3
            Shape = bsTopLine
          end
          object wwcbCalculation_Type: TevDBComboBox
            Left = 12
            Top = 50
            Width = 145
            Height = 21
            HelpContext = 19582
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CALCULATION_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnChange = wwcbCalculation_TypeChange
            OnCloseUp = wwcbCalculation_TypeCloseUp
          end
          object dedtAmount: TevDBEdit
            Left = 12
            Top = 89
            Width = 145
            Height = 21
            HelpContext = 19583
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtPercentage: TevDBEdit
            Left = 165
            Top = 89
            Width = 145
            Height = 21
            HelpContext = 19584
            DataField = 'PERCENTAGE'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object pnlBenefitInfo: TPanel
            Left = 12
            Top = 89
            Width = 297
            Height = 24
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 3
            object edcAMOUNT: TevEdit
              Left = 0
              Top = 0
              Width = 145
              Height = 21
              TabOrder = 0
            end
            object edcPERCENTAGE: TevEdit
              Left = 153
              Top = 0
              Width = 145
              Height = 21
              TabOrder = 1
            end
          end
          object wwlcE_D_Group: TevDBLookupCombo
            Left = 165
            Top = 50
            Width = 145
            Height = 21
            HelpContext = 19572
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evDBComboBox2: TevDBComboBox
            Left = 12
            Top = 128
            Width = 145
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ALWAYS_PAY'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 165
            Top = 113
            Width = 145
            Height = 36
            HelpContext = 19597
            Caption = '~Deductions to zero'
            Columns = 2
            DataField = 'DEDUCTIONS_TO_ZERO'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object wwlcScheduledEDGroup: TevDBLookupCombo
            Left = 165
            Top = 167
            Width = 145
            Height = 21
            HelpContext = 19572
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'SCHEDULED_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 8
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwlcScheduledEDGroupChange
          end
          object dbsePriority: TevDBSpinEdit
            Left = 12
            Top = 167
            Width = 145
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 999.000000000000000000
            MinValue = 1.000000000000000000
            Value = 1.000000000000000000
            DataField = 'PRIORITY_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
          end
          object evcbEEBenefitReference: TevDBLookupCombo
            Left = 12
            Top = 294
            Width = 236
            Height = 21
            HelpContext = 19591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Benefit'#9'25'#9'HR Benefit '#9'F'
              'BENEFIT_EFFECTIVE_DATE'#9'10'#9'Effective Start Date'#9'F'
              'EXPIRATION_DATE'#9'10'#9'Effective End Date'#9'F'
              'ReferenceType'#9'10'#9'Reference Type'#9'F'
              'EE_RATE'#9'10'#9'EE Amount'#9'F'
              'ER_RATE'#9'10'#9'ER Amount'#9'F')
            DataField = 'EE_BENEFITS_NBR'
            DataSource = wwdsDetail
            LookupTable = cdEEBenefits
            LookupField = 'EE_BENEFITS_NBR'
            Options = [loTitles]
            Style = csDropDownList
            TabOrder = 11
            AutoDropDown = True
            ShowButton = True
            SearchDelay = 400
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evcbBenefitSubType: TevDBLookupCombo
            Left = 165
            Top = 255
            Width = 145
            Height = 21
            HelpContext = 19591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Description'#9'40'#9'Description')
            DataField = 'CO_BENEFIT_SUBTYPE_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE
            LookupField = 'CO_BENEFIT_SUBTYPE_NBR'
            Style = csDropDownList
            TabOrder = 9
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evcbBenefitReference: TevDBComboBox
            Left = 12
            Top = 255
            Width = 145
            Height = 21
            ShowButton = True
            Style = csDropDown
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            DropDownWidth = 370
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
            OnChange = evcbBenefitReferenceChange
            OnCloseUp = evcbBenefitReferenceCloseUp
          end
        end
        object fpFrequency: TisUIFashionPanel
          Left = 346
          Top = 8
          Width = 341
          Height = 450
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpFrequency'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Frequency'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablFrequency: TevLabel
            Left = 12
            Top = 35
            Width = 59
            Height = 16
            Caption = '~Frequency'
            FocusControl = wwcbFrequency
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 74
            Width = 49
            Height = 16
            Caption = '~Month #'
            FocusControl = evDBComboBox1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel9: TevLabel
            Left = 170
            Top = 74
            Width = 79
            Height = 16
            Caption = '~Which Payrolls'
            FocusControl = evDBComboBox4
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablEffective_Start_Date: TevLabel
            Left = 12
            Top = 113
            Width = 102
            Height = 16
            Caption = '~Effective Start Date'
            FocusControl = dEffectiveStartDate
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablEffective_End_Date: TevLabel
            Left = 170
            Top = 113
            Width = 90
            Height = 13
            Caption = 'Effective End Date'
            FocusControl = dEffectiveEndDate
          end
          object bvFreq: TEvBevel
            Left = 12
            Top = 170
            Width = 308
            Height = 3
            Shape = bsTopLine
          end
          object wwcbFrequency: TevDBComboBox
            Left = 12
            Top = 50
            Width = 150
            Height = 21
            HelpContext = 19573
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'FREQUENCY'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Every Pay'#9'D'
              'Every Scheduled Pay'#9'P'
              'Every Other Scheduled Pay'#9'O'
              'First Scheduled of Month'#9'F'
              'Last Scheduled of Month'#9'L'
              'Closest Scheduled to 15th of Month'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDBComboBox1: TevDBComboBox
            Left = 12
            Top = 89
            Width = 150
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PLAN_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evDBComboBox4: TevDBComboBox
            Left = 170
            Top = 89
            Width = 150
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'WHICH_CHECKS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object dEffectiveStartDate: TevDBDateTimePicker
            Left = 12
            Top = 128
            Width = 150
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'EFFECTIVE_START_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
            UnboundDataType = wwDTEdtDate
            OnChange = dEffectiveStartDateChange
            OnExit = dEffectiveStartDateExit
            OnKeyUp = dEffectiveStartDateKeyUp
          end
          object dEffectiveEndDate: TevDBDateTimePicker
            Left = 170
            Top = 128
            Width = 150
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'EFFECTIVE_END_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object drgpExclude_Week_1: TevDBRadioGroup
            Left = 12
            Top = 189
            Width = 308
            Height = 36
            HelpContext = 19577
            Caption = '~Block Week 1'
            Columns = 2
            DataField = 'EXCLUDE_WEEK_1'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpExclude_Week_2: TevDBRadioGroup
            Left = 12
            Top = 237
            Width = 308
            Height = 36
            HelpContext = 19577
            Caption = '~Block Week 2'
            Columns = 2
            DataField = 'EXCLUDE_WEEK_2'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpExclude_Week_3: TevDBRadioGroup
            Left = 12
            Top = 286
            Width = 308
            Height = 36
            HelpContext = 19577
            Caption = '~Block Week 3'
            Columns = 2
            DataField = 'EXCLUDE_WEEK_3'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpExclude_Week_4: TevDBRadioGroup
            Left = 12
            Top = 335
            Width = 308
            Height = 36
            HelpContext = 19577
            Caption = '~Block Week 4'
            Columns = 2
            DataField = 'EXCLUDE_WEEK_4'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpExclude_Week_5: TevDBRadioGroup
            Left = 12
            Top = 384
            Width = 308
            Height = 36
            HelpContext = 19577
            Caption = '~Block Week 5'
            Columns = 2
            DataField = 'EXCLUDE_WEEK_5'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 9
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpNavigation: TisUIFashionPanel
          Left = 695
          Top = 352
          Width = 208
          Height = 107
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpNavigation'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Navigation'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evBitBtn1: TevBitBtn
            Left = 12
            Top = 35
            Width = 174
            Height = 25
            Caption = 'Employee'
            TabOrder = 0
            OnClick = evBitBtn1Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
          object btnEmployeeBenefit: TevBitBtn
            Left = 12
            Top = 65
            Width = 174
            Height = 25
            Caption = 'HR Employee Benefits'
            TabOrder = 1
            OnClick = btnEmployeeBenefitClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
        end
      end
    end
    object tshtDetail_2: TTabSheet
      Caption = 'Advanced'
      ImageIndex = 37
      object sbAdvanced: TScrollBox
        Left = 0
        Top = 0
        Width = 741
        Height = 472
        Align = alClient
        TabOrder = 0
        object fpAnnualMax: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 214
          Height = 130
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Annual Maximum Amounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablAnnual_Maximum_Amount: TevLabel
            Left = 12
            Top = 35
            Width = 46
            Height = 13
            Caption = 'Employee'
            FocusControl = dedtAnnual_Maximum_Amount
          end
          object lablClient_Annual_Maximum_Amount: TevLabel
            Left = 12
            Top = 74
            Width = 26
            Height = 13
            Caption = 'Client'
            FocusControl = dedtClient_Annual_Maximum_Amount
          end
          object dedtAnnual_Maximum_Amount: TevDBEdit
            Left = 12
            Top = 50
            Width = 180
            Height = 21
            HelpContext = 19585
            DataField = 'ANNUAL_MAXIMUM_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtClient_Annual_Maximum_Amount: TevDBEdit
            Left = 12
            Top = 89
            Width = 180
            Height = 21
            HelpContext = 19585
            DataField = 'ANNUAL_MAXIMUM'
            DataSource = dsCL_E_DS
            ParentColor = True
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpLimits: TisUIFashionPanel
          Left = 8
          Top = 146
          Width = 214
          Height = 455
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Limits'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablMinimum_E_D_Group: TevLabel
            Left = 12
            Top = 35
            Width = 140
            Height = 13
            AutoSize = False
            Caption = 'Minimum E/D Group'
            FocusControl = wwlcMinimum_E_D_Group
          end
          object lablMinimum_Pay_Period_Percentage: TevLabel
            Left = 12
            Top = 74
            Width = 126
            Height = 13
            AutoSize = False
            Caption = 'Minimum Pay Period %'
            FocusControl = dedtMinimum_Pay_Period_Percentage
          end
          object lablMinimum_Pay_Period_Amount: TevLabel
            Left = 12
            Top = 113
            Width = 134
            Height = 13
            Caption = 'Minimum Pay Period Amount'
            FocusControl = dedtMinimum_Pay_Period_Amount
          end
          object bvlLimits: TEvBevel
            Left = 12
            Top = 204
            Width = 180
            Height = 3
            Shape = bsTopLine
          end
          object lablMaximum_Pay_Period_Percentage: TevLabel
            Left = 12
            Top = 260
            Width = 134
            Height = 13
            AutoSize = False
            Caption = 'Maximum Pay Period %'
            FocusControl = dedtMaximum_Pay_Period_Percentage
          end
          object lablMaximum_Pay_Period_Amount: TevLabel
            Left = 12
            Top = 299
            Width = 137
            Height = 13
            Caption = 'Maximum Pay Period Amount'
            FocusControl = dedtMaximum_Pay_Period_Amount
          end
          object lablMaximum_E_D_Group: TevLabel
            Left = 12
            Top = 221
            Width = 142
            Height = 13
            AutoSize = False
            Caption = 'Maximum E/D Group'
            FocusControl = wwlcMaximum_E_D_Group
          end
          object evLabel12: TevLabel
            Left = 12
            Top = 153
            Width = 75
            Height = 13
            Caption = 'Minimum Hours '
            FocusControl = edtMinHours
          end
          object evLabel13: TevLabel
            Left = 12
            Top = 339
            Width = 78
            Height = 13
            Caption = 'Maximum Hours '
            FocusControl = edtMaxHours
          end
          object wwlcMinimum_E_D_Group: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 180
            Height = 21
            HelpContext = 19590
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'MIN_PPP_CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwlcMinimum_E_D_GroupChange
          end
          object dedtMinimum_Pay_Period_Percentage: TevDBEdit
            Left = 12
            Top = 89
            Width = 180
            Height = 21
            HelpContext = 19590
            DataField = 'MINIMUM_PAY_PERIOD_PERCENTAGE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtMinimum_Pay_Period_PercentageChange
            Glowing = False
          end
          object dedtMinimum_Pay_Period_Amount: TevDBEdit
            Left = 12
            Top = 128
            Width = 180
            Height = 21
            HelpContext = 19590
            DataField = 'MINIMUM_PAY_PERIOD_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtMaximum_Pay_Period_Percentage: TevDBEdit
            Left = 12
            Top = 275
            Width = 180
            Height = 21
            HelpContext = 19591
            DataField = 'MAXIMUM_PAY_PERIOD_PERCENTAGE'
            DataSource = wwdsDetail
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtMaximum_Pay_Period_PercentageChange
            Glowing = False
          end
          object dedtMaximum_Pay_Period_Amount: TevDBEdit
            Left = 12
            Top = 314
            Width = 180
            Height = 21
            HelpContext = 19591
            DataField = 'MAXIMUM_PAY_PERIOD_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcMaximum_E_D_Group: TevDBLookupCombo
            Left = 12
            Top = 236
            Width = 180
            Height = 21
            HelpContext = 19591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'MAX_PPP_CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwlcMaximum_E_D_GroupChange
          end
          object edtMinHours: TevDBEdit
            Left = 12
            Top = 168
            Width = 180
            Height = 21
            HelpContext = 19590
            DataField = 'MINIMUM_HW_HOURS'
            DataSource = wwdsDetail
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edtMaxHours: TevDBEdit
            Left = 12
            Top = 354
            Width = 180
            Height = 21
            HelpContext = 19591
            DataField = 'MAXIMUM_HW_HOURS'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpMaxAvg: TisUIFashionPanel
          Left = 230
          Top = 272
          Width = 379
          Height = 130
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Maximum Average'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel2: TevLabel
            Left = 12
            Top = 35
            Width = 91
            Height = 13
            Caption = 'Amount E/D Group'
            FocusControl = evDBLookupCombo2
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 74
            Width = 83
            Height = 13
            Caption = 'Hours E/D Group'
            FocusControl = evDBLookupCombo3
          end
          object evLabel6: TevLabel
            Left = 195
            Top = 35
            Width = 88
            Height = 13
            Caption = 'Hourly Wage Rate'
            FocusControl = evDBEdit3
          end
          object evDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 175
            Height = 21
            HelpContext = 19591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwlcMaximum_E_D_GroupChange
          end
          object evDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 175
            Height = 21
            HelpContext = 19591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwlcMaximum_E_D_GroupChange
          end
          object evDBEdit3: TevDBEdit
            Left = 195
            Top = 50
            Width = 163
            Height = 21
            HelpContext = 19591
            DataField = 'MAX_AVERAGE_HOURLY_WAGE_RATE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object pnlActions: TisUIFashionPanel
          Left = 230
          Top = 8
          Width = 379
          Height = 130
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Target Actions'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablTarget_Action: TevLabel
            Left = 12
            Top = 35
            Width = 64
            Height = 13
            Caption = 'Target Action'
            FocusControl = wwcbTarget_Action
          end
          object lablNumber_Targets_Remaining: TevLabel
            Left = 244
            Top = 35
            Width = 111
            Height = 13
            Caption = '# of Targets Remaining'
            FocusControl = dedtNumber_Targets_Remaining
          end
          object lablTarget_Amount: TevLabel
            Left = 195
            Top = 74
            Width = 70
            Height = 13
            Caption = 'Target Amount'
            FocusControl = dedtTarget_Amount
          end
          object lablBalance: TevLabel
            Left = 12
            Top = 74
            Width = 73
            Height = 13
            Caption = 'Balance Taken'
            FocusControl = wwdeBalance
          end
          object dedtNumber_Targets_Remaining: TevDBEdit
            Left = 244
            Top = 50
            Width = 114
            Height = 21
            HelpContext = 19581
            DataField = 'NUMBER_OF_TARGETS_REMAINING'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtTarget_Amount: TevDBEdit
            Left = 195
            Top = 89
            Width = 163
            Height = 21
            HelpContext = 19586
            DataField = 'TARGET_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtTarget_AmountChange
            Glowing = False
          end
          object wwcbTarget_Action: TevDBComboBox
            Left = 12
            Top = 50
            Width = 224
            Height = 21
            HelpContext = 19580
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'TARGET_ACTION'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Reset Balance'#9'R'
              'Zero Out Balance'#9'Z'
              'Do Not Reset Balance but Leave Line'#9'L'
              'Do Not Reset Balance and Remove Line'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnChange = wwcbTarget_ActionChange
          end
          object wwdeBalance: TevDBEdit
            Left = 12
            Top = 89
            Width = 175
            Height = 21
            HelpContext = 19599
            DataField = 'BALANCE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object pnlMinimums: TisUIFashionPanel
          Left = 230
          Top = 410
          Width = 379
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Garnishment Overrides'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablMaximum_Garnish_Percentage: TevLabel
            Left = 195
            Top = 35
            Width = 123
            Height = 13
            AutoSize = False
            Caption = 'Maximum Garnish %'
            FocusControl = dedtMaximum_Garnish_Percentage
          end
          object lablMinimum_Wage_Multiplier: TevLabel
            Left = 12
            Top = 35
            Width = 139
            Height = 13
            AutoSize = False
            Caption = 'Minimum Wage Multiplier'
            FocusControl = dedtMinimum_Wage_Multiplier
          end
          object dedtMaximum_Garnish_Percentage: TevDBEdit
            Left = 195
            Top = 50
            Width = 163
            Height = 21
            HelpContext = 19589
            DataField = 'MAXIMUM_GARNISH_PERCENT'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtMaximum_Garnish_PercentageChange
            Glowing = False
          end
          object dedtMinimum_Wage_Multiplier: TevDBEdit
            Left = 12
            Top = 50
            Width = 175
            Height = 21
            HelpContext = 19588
            DataField = 'MINIMUM_WAGE_MULTIPLIER'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtMinimum_Wage_MultiplierChange
            Glowing = False
          end
        end
        object pnlThreshholds: TisUIFashionPanel
          Left = 230
          Top = 146
          Width = 379
          Height = 118
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlThreshholds'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Thresholds'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object labl_Threshold_E_D_Group: TevLabel
            Left = 12
            Top = 62
            Width = 102
            Height = 13
            Caption = 'Threshold E/D Group'
          end
          object labl_Threshold_Amount: TevLabel
            Left = 195
            Top = 62
            Width = 86
            Height = 13
            Caption = 'Threshold Amount'
            FocusControl = dedtThreshold_Amount
          end
          object dedtThreshold_Amount: TevDBEdit
            Left = 195
            Top = 77
            Width = 163
            Height = 21
            HelpContext = 19591
            DataField = 'THRESHOLD_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtThreshold_AmountChange
            Glowing = False
          end
          object cbUsePensionLimit: TevDBCheckBox
            Left = 12
            Top = 35
            Width = 161
            Height = 17
            Caption = 'Use Pension Limit'
            DataField = 'USE_PENSION_LIMIT'
            DataSource = wwdsDetail
            TabOrder = 0
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
            OnClick = cbUsePensionLimitClick
          end
          object wwlcThreshold_E_D_Group: TevDBLookupCombo
            Left = 12
            Top = 77
            Width = 175
            Height = 21
            HelpContext = 19591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'THRESHOLD_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwlcMaximum_E_D_GroupChange
          end
        end
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 230
          Top = 509
          Width = 379
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 6
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Health and Welfare Benefits '
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblExcessCode: TevLabel
            Left = 195
            Top = 35
            Width = 78
            Height = 13
            AutoSize = False
            Caption = 'Excess Code '
          end
          object edtFundingCode: TevLabel
            Left = 12
            Top = 35
            Width = 77
            Height = 13
            AutoSize = False
            Caption = 'Funding Code '
          end
          object cbFundingCode: TevDBLookupCombo
            Left = 12
            Top = 52
            Width = 170
            Height = 19
            HelpContext = 19591
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'HW_FUNDING_CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Frame.FocusStyle = efsFrameSunken
            Frame.NonFocusStyle = efsFrameSunken
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbExcessCode: TevDBLookupCombo
            Left = 195
            Top = 52
            Width = 165
            Height = 19
            HelpContext = 19591
            BorderStyle = bsNone
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'HW_EXCESS_CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Frame.FocusStyle = efsFrameSunken
            Frame.NonFocusStyle = efsFrameSunken
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
      end
    end
    object tshtDirectDeposits: TTabSheet
      Caption = 'Direct Deposits'
      ImageIndex = 28
      OnShow = tshtDirectDepositsShow
      object sbDD: TScrollBox
        Left = 0
        Top = 0
        Width = 741
        Height = 472
        Align = alClient
        TabOrder = 0
        object fpDirectDepositSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 403
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgDirect_Deposit: TevDBGrid
            Left = 12
            Top = 35
            Width = 361
            Height = 145
            HelpContext = 17001
            DisableThemesInTitle = False
            Selected.Strings = (
              'EE_BANK_ACCOUNT_NUMBER'#9'33'#9'EE Bank Account Number'#9'F'
              'EE_ABA_NUMBER'#9'20'#9'ABA Number'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_SCHEDULED_E_DS\wwdgDirect_Deposit'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = DDSrc
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpDirectDepositDetail: TisUIFashionPanel
          Left = 8
          Top = 219
          Width = 403
          Height = 261
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Direct Deposit Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablABA_Number: TevLabel
            Left = 12
            Top = 35
            Width = 70
            Height = 16
            Caption = '~ABA Number'
            FocusControl = dedtABA_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1: TevLabel
            Left = 136
            Top = 112
            Width = 43
            Height = 13
            Caption = 'Addenda'
            FocusControl = DBEdit1
          end
          object lablBranchIdentifier: TevLabel
            Left = 11
            Top = 112
            Width = 77
            Height = 13
            Caption = 'Branch Identifier'
            FocusControl = DBEdit1
          end
          object lablBank_Account_Nbr: TevLabel
            Left = 12
            Top = 73
            Width = 117
            Height = 16
            Caption = '~Bank Account Number'
            FocusControl = dedtBank_Account_Nbr
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablBank_Acct_Type: TevLabel
            Left = 135
            Top = 73
            Width = 76
            Height = 16
            Caption = '~Account Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtABA_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 237
            Height = 21
            HelpContext = 17001
            DataField = 'EE_ABA_NUMBER'
            DataSource = DDSrc
            TabOrder = 0
            UnboundDataType = wwDefault
            UsePictureMask = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit1: TevDBEdit
            Left = 136
            Top = 127
            Width = 115
            Height = 21
            HelpContext = 17001
            DataField = 'ADDENDA'
            DataSource = DDSrc
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtBranchIdentifier: TevDBEdit
            Left = 11
            Top = 127
            Width = 115
            Height = 21
            HelpContext = 17001
            DataField = 'BRANCH_IDENTIFIER'
            DataSource = DDSrc
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtBank_Account_Nbr: TevDBEdit
            Left = 12
            Top = 89
            Width = 115
            Height = 21
            HelpContext = 17001
            DataField = 'EE_BANK_ACCOUNT_NUMBER'
            DataSource = DDSrc
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = dedtBank_Account_NbrChange
            Glowing = False
          end
          object wwDBComboBox1: TevDBComboBox
            Left = 135
            Top = 89
            Width = 115
            Height = 21
            HelpContext = 17001
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EE_BANK_ACCOUNT_TYPE'
            DataSource = DDSrc
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object drgpIn_Prenote: TevDBRadioGroup
            Left = 257
            Top = 118
            Width = 125
            Height = 36
            HelpContext = 17001
            Caption = '~In Prenote'
            Columns = 2
            DataField = 'IN_PRENOTE'
            DataSource = DDSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpForm_on_File: TevDBRadioGroup
            Left = 257
            Top = 161
            Width = 125
            Height = 36
            HelpContext = 17001
            Caption = '~Form on File'
            Columns = 2
            DataField = 'FORM_ON_FILE'
            DataSource = DDSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'Y'
              'N')
          end
          inline mnavDirectDeposit: TMiniNavigationFrame
            Left = 12
            Top = 211
            Width = 243
            Height = 28
            TabOrder = 10
            inherited SpeedButton1: TevSpeedButton
              Width = 115
              Caption = 'Create'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited SpeedButton2: TevSpeedButton
              Left = 124
              Width = 115
              Caption = 'Delete'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited evActionList1: TevActionList
              Left = 164
            end
          end
          object evrgShowinEP: TevDBRadioGroup
            Left = 257
            Top = 203
            Width = 125
            Height = 36
            HelpContext = 17001
            Caption = '~Show in EE Portal '
            Columns = 2
            DataField = 'SHOW_IN_EE_PORTAL'
            DataSource = DDSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 9
            Values.Strings = (
              'Y'
              'N')
          end
          object rbReadOnlyRemotes: TevDBRadioGroup
            Left = 257
            Top = 35
            Width = 125
            Height = 36
            HelpContext = 17001
            Caption = '~Read Only for Remotes'
            Columns = 2
            DataField = 'READ_ONLY_REMOTES'
            DataSource = DDSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object rbAllowHyphens: TevDBRadioGroup
            Left = 257
            Top = 77
            Width = 125
            Height = 36
            HelpContext = 17001
            Caption = '~Allow Hyphens'
            Columns = 2
            DataField = 'ALLOW_HYPHENS'
            DataSource = DDSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
        end
      end
    end
    object tshtChildSupport: TTabSheet
      Caption = 'Child Support Cases'
      ImageIndex = 12
      OnShow = tshtChildSupportShow
      object sbChildSupport: TScrollBox
        Left = 0
        Top = 0
        Width = 741
        Height = 472
        Align = alClient
        TabOrder = 0
        object pnlListOfAgencies: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 393
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBGrid1: TevDBGrid
            Left = 12
            Top = 35
            Width = 361
            Height = 145
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_CASE_NUMBER'#9'33'#9'Custom case number'#9'F'
              'PRIORITY_NUMBER'#9'20'#9'Priority number'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_SCHEDULED_E_DS\evDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = ChildSupportSrc
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object pnlAgencyInfo: TisUIFashionPanel
          Left = 8
          Top = 219
          Width = 393
          Height = 287
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Child Support Case Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPriority_Number: TevLabel
            Left = 12
            Top = 35
            Width = 80
            Height = 16
            Caption = '~Priority Number'
            FocusControl = dedtPriority_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCase_Number: TevLabel
            Left = 135
            Top = 35
            Width = 64
            Height = 13
            Caption = 'Case Number'
            FocusControl = dedtCase_Number
          end
          object lablOrigination_State: TevLabel
            Left = 258
            Top = 35
            Width = 78
            Height = 13
            Caption = 'Origination State'
            FocusControl = dedtOrigination_State
          end
          object lablAgency_Number: TevLabel
            Left = 12
            Top = 74
            Width = 36
            Height = 13
            Caption = 'Agency'
            FocusControl = evDBLookupCombo1
          end
          object lablFIPS_code: TevLabel
            Left = 12
            Top = 152
            Width = 51
            Height = 13
            Caption = 'FIPS Code'
            FocusControl = dedtFIPS_code
          end
          object dedtPriority_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 115
            Height = 21
            HelpContext = 19568
            DataField = 'PRIORITY_NUMBER'
            DataSource = ChildSupportSrc
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtCase_Number: TevDBEdit
            Left = 135
            Top = 50
            Width = 115
            Height = 21
            HelpContext = 19568
            DataField = 'CUSTOM_CASE_NUMBER'
            DataSource = ChildSupportSrc
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtOrigination_State: TevDBEdit
            Left = 259
            Top = 50
            Width = 115
            Height = 21
            HelpContext = 19568
            DataField = 'ORIGINATION_STATE'
            DataSource = ChildSupportSrc
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 215
            Height = 21
            HelpContext = 19568
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
            DataField = 'CL_AGENCY_NBR'
            DataSource = ChildSupportSrc
            LookupTable = DM_CL_AGENCY.CL_AGENCY
            LookupField = 'CL_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object drgpARREARS: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 215
            Height = 36
            HelpContext = 19568
            Caption = '~Arrears'
            Columns = 2
            DataField = 'ARREARS'
            DataSource = ChildSupportSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object rgILElig: TevDBRadioGroup
            Left = 235
            Top = 75
            Width = 139
            Height = 74
            HelpContext = 19568
            Caption = '~St Medical Ins Eligible'
            DataField = 'IL_MED_INS_ELIGIBLE'
            DataSource = ChildSupportSrc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
            OnChange = rgILEligChange
          end
          object dedtFIPS_code: TevDBEdit
            Left = 12
            Top = 167
            Width = 115
            Height = 21
            HelpContext = 19568
            DataField = 'FIPS_CODE'
            DataSource = ChildSupportSrc
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtCustom_Field2: TevDBEdit
            Left = 135
            Top = 167
            Width = 239
            Height = 21
            HelpContext = 19568
            DataField = 'CUSTOM_FIELD'
            DataSource = ChildSupportSrc
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          inline mnavChildSupport: TMiniNavigationFrame
            Left = 10
            Top = 237
            Width = 369
            Height = 27
            TabOrder = 8
            inherited SpeedButton1: TevSpeedButton
              Left = 2
              Top = 2
              Width = 115
              Caption = 'Create'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited SpeedButton2: TevSpeedButton
              Left = 249
              Top = 2
              Width = 115
              Caption = 'Delete'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited evActionList1: TevActionList
              Left = 60
              Top = 65524
            end
          end
          object pnlNYChildSupport: TevPanel
            Left = 12
            Top = 194
            Width = 368
            Height = 38
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 9
            object evLabel10: TevLabel
              Left = 0
              Top = 0
              Width = 72
              Height = 13
              Caption = 'Arrears Amount'
              FocusControl = evDBEdit1
            end
            object evLabel11: TevLabel
              Left = 123
              Top = 0
              Width = 138
              Height = 13
              Caption = 'Dependent Health E/D Code'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evDBEdit1: TevDBEdit
              Left = 0
              Top = 15
              Width = 115
              Height = 21
              HelpContext = 19568
              DataField = 'ARREARS_AMOUNT'
              DataSource = ChildSupportSrc
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object cbDependentHealthCode: TevDBLookupCombo
              Left = 123
              Top = 15
              Width = 239
              Height = 21
              HelpContext = 19568
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'custom_e_d_code_number'#9'20'#9'E/D Code'#9#9)
              DataField = 'DEPENDENT_HEALTH_CL_E_DS_NBR'
              DataSource = ChildSupportSrc
              LookupTable = cdsDependentHealthCode
              LookupField = 'cl_e_ds_nbr'
              Style = csDropDownList
              TabOrder = 1
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 749
    Height = 35
    object Label3: TevLabel [0]
      Left = 484
      Top = 2
      Width = 54
      Height = 13
      Caption = 'E/D Code: '
    end
    object DBText5: TevDBText [1]
      Left = 541
      Top = 2
      Width = 41
      Height = 15
      DataField = 'CUSTOM_E_D_CODE_NUMBER'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TevLabel [2]
      Left = 484
      Top = 17
      Width = 31
      Height = 13
      Caption = 'Desc: '
    end
    object DBText6: TevDBText [3]
      Left = 541
      Top = 17
      Width = 105
      Height = 17
      DataField = 'description'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel8: TevLabel [4]
      Left = 593
      Top = 2
      Width = 30
      Height = 13
      Caption = 'Type: '
    end
    object evDBText1: TevDBText [5]
      Left = 625
      Top = 2
      Width = 23
      Height = 15
      DataField = 'E_D_CODE_TYPE'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited dblkupEE_NBR: TevDBLookupCombo
      Top = 7
    end
    inherited dbtxtname: TevDBLookupCombo
      Top = 7
    end
    inherited butnPrior: TevBitBtn
      Top = 4
    end
    inherited butnNext: TevBitBtn
      Top = 4
    end
    inline HideRecordHelper: THideRecordHelper
      Left = 370
      Top = 4
      Width = 112
      Height = 28
      AutoScroll = False
      Color = clBtnFace
      ParentColor = False
      TabOrder = 4
      inherited butnNext: TevBitBtn
        Width = 112
      end
      inherited evBitBtn1: TevBitBtn
        Top = 0
        Width = 112
        Caption = 'Unremove E/D'
      end
      inherited evActionList1: TevActionList
        Left = 0
        Top = 15
        inherited HideCurRecord: TAction
          Caption = 'Remove E/D'
        end
        inherited UnhideCurRecord: TAction
          Caption = 'UnRemove E/D'
        end
      end
      inherited dsHelper: TevDataSource
        Left = 65520
        Top = 16
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 128
    Top = 18
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_SCHEDULED_E_DS.EE_SCHEDULED_E_DS
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = wwdsSubMaster
    Left = 214
    Top = 18
  end
  inherited wwdsList: TevDataSource
    Left = 182
    Top = 65518
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 345
    Top = 39
  end
  inherited PageControlImages: TevImageList
    Left = 256
    Top = 64
    Bitmap = {
      494C010126002700040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000A000000001001000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000396739673967DE7B0000000000000000000000000000
      0000000039675A6B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A6B396739673967396739673967
      39673967396739673967095609560956F6620000000000000000000000000000
      00000000256A476A3967DE7B0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FD3EDD3ADD3ADD3ADD3ADD3AFD3A
      FE3AFE3AFE3A1F3BE6596E5EA47E327748627B6F396739673967396739673967
      396739676E5A676E676EF65E39677B6F00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF67BF63BF63BF63DF67D852
      EF3DCE3DCE3DCE3D3142964E136BB57F48625726F605F605F601F601F601F601
      F601F701F701896AD27F686A112A572600000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF67DF73DF73BF5F3142524E
      5A6F9C739C735A6F734E1142BE7377774866F605000000000000000000000000
      00000000000034772D7BB07F677230367B6F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF67DF6FDF73B84E734E9C73
      1D4B3E437E479E579C73734EB65647660000F605000000000000000000000000
      000000000466676EA972AF7FAF7FCA768D6A0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF6B7F5B9F5B10429C73FC46
      3D475E479F4FBF539E579C73113E00000000F6010000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0566B17F8D7F0B77276A476E486A0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF6B7F579F5BEF3DDE7FDC3A
      7E5B7E537E4F9F4F7E47DE7F114200000000F6010000DE7BDE7BDE7BDE7BDE7B
      DE7BFF7B8C6E517B8F7F2A7B486E142200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF6BBF6FDF6FF03DFF7FDC3A
      BE6B7E5B7E535E473D3FFF7F114200000000F6050000BD77BD77BD77BD77BD77
      BD77BE77F16ECA72FB7F497BC9764B5EBD770000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ABF6FBF6BDF6F1142DD7BDC42
      BF73BE6B7E5B3D4B3D4BDD7B324600000000F60500009C739C739C739C739C73
      9C739D73156F676E917F927FB27F696EF46A0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3ADF735F535F53F85AB5560000
      DC42FC42FC421D4B0000B55A984200000000F60500005A6B5A6B5A6B5A6B5A6B
      5A6B7B6B386B276E276E476E486E697269720000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD36DF735F4F5F4FDF6F744ED65A
      FF7F00000000FF7FD65A9452FE3A00000000F605000000000000000000000000
      0000000000000000000000000000F80100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD36DF77BF67BF6B3F4F3F4FF95A
      524A31463146524AF956FF77DD3A00000000F6059E5F9C269C26BC26BC26BC26
      BC26BC26BC26BC26BC269C269F5FF70500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD36FF7B9F679F673F4B3F4BBF67
      BF673F4B3F4BBF67BF67FF7BDD3600000000160A7E575A125A125A125A125A12
      5A125A125A125A125A125A127E57160A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3AFF7FFF7BFF7BFF7FFF7FFF7B
      FF7BFF7FFF7FFF7BFF7BFF7FDD3A00000000170A5D4F3D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F5D4F170A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FD42DD3ADD36DD36DD36DD36DD36
      DD36DD36DD36DD36DD36DD3AFD4200000000992A170A160A160A160A160A160A
      160A160A160A160A160A160A170A992A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000005A6B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000396739677B6F0000000000000000AB3DA941496E2A6A0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000BD77422A00000000000000000000000000000000DE7B396739673967
      39673967396739673967DE7B0000000000000000DE7B39673967396739673967
      39673967202A0043683A7B6F0000000000000B4E905A527FEB7EA74D0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00003967DE7B3967396739673967
      39670000D152402E396739673967FF7F0000000000000000B84A550954055405
      54055405540554055509B84A0000000000000000B84A55095505550556055705
      5705390120268053E042883A7B6F00000000A976937F737F647E627EA74D0000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD7720265563603680368036A036
      602EBD77803EC04680368036602E5967000000000000000076095A2E9C369C36
      9C369C369C369C365B2E7609000000000000000076095A2E9C369D36682E2026
      20262026002260536053E042883A7B6F0000C976917FE87E857E637E637EA74D
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD04EA0425663A046005320536053
      C03A335BE052004F204F40538057873A0000000000000000D819FD467B2E7B2E
      7B2A7B2A7B2E7C2EFD46B7110000000000000000D819FD467C2E7E2E0022A763
      60536053404F404F404F404FE042883A0000FF7FE661297FE77EA57E637E637E
      A74D0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F60320053F152ED4E803A8036A03A
      602E00006032C04680368036404BA036000000000000000019263D57BD36BD36
      FE7FDD7BDD36BD363D57D815000000000000000019263D57BD36DF3E001EAD67
      204F204F204F004B204F204F8C6720220000FF7FFF7FE7612A7FE77EA57E637E
      637EA74D0000FF7FFF7FFF7FFF7FFF7FFF7F60360053A03A5A6BDE7B39670000
      00000000DC77402E0000165FAF4A202600000000000000005A2E7E67FE3EDF6F
      FF7FDE7B7D671F3F9E67F91900000000000000005A2E7E671F3FFF77001EB273
      A96BA96B896BB26F004B8C63C042CA420000FF7FFF7FFF7FE7612A7FE77EA57E
      637E637EA74D0000FF7FFF7FFF7FFF7FFF7F6036E04E20536232F35600220000
      000000000000A83E9C7360320047402A00000000000000005A261D4FBF6B7677
      8B62E7516F5ADF6F1D535A2A00000000000000005A261D4FBF6B777B433A001E
      001E001E001E906B6B63A03ECB4200000000FF7FFF7FFF7FFF7FE7612A7FE77E
      A57E637E637EA74D0000FF7FFF7FFF7FFF7FEC4AE04A004F204B404B545F0000
      0000000000000000AD46E04E4053C03A00000000000000000000FD42BE32175F
      6C5EE84D5246BE36FD42000000000000000000000000FD42BE32185F6C5EE951
      544ADF3600228F6FA03ECB42000000000000FF7FFF7FFF7FFF7FFF7FE7612A7F
      E77EA57E637E627EA6510000FF7FFF7FFF7F3967402E20534053A03600000000
      0000000000000000402E0057404F85360000000000000000000000007B6FAF62
      EF6EAD664C569C73000000000000000000000000000000007B6FAF62EF6EAD6A
      4C569C732022A042CA420000000000000000FF7FFF7FFF7FFF7FFF7FFF7FE761
      2A7FE77EA57E627EAF6AF0390000FF7FFF7F202660328036A03A365F39679C73
      DE7B39673967396739676132A03A0000000000000000000000000000CB49937F
      1077CE6EAD6A0D5200000000000000000000000000000000CB49937F1077CE6E
      AD6A0E520000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      E761297FE67EF26E3242D756EF350000FF7F00000000BD7739678A3E603AAC46
      BA732022C046E042602E9C73B96F0000000000000000000000000000C030D57F
      727F30730F73A02C00000000000000000000000000000000C030D57F727F3073
      0F73812C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FE665977B123E5A673142D659B365FF7F0000000075636036004F40536036
      BD779C73402E204F404BAD4A00000000000000000000000000000000E1348449
      2A5ACF6A6C5EA12C00000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F123EDE7730425966F759F565FF7F000000000000803A0053204F8053
      CF4AAD4AE046204F204BA03A5A6B00000000000000000000000000000239E655
      295EEA552539C230000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F103EFD769A6A376AFF7FFF7F000000000000986BA03EE0428436
      603AA046205300470F53873A422E00000000000000000000000000006741C455
      275EE75524396741000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F386E376AFF7FFF7FFF7F0000000000000000BA6F0E4FA93E
      402A803A8036613200000000000000000000000000000000000000009B774441
      63454341443D9B77000000000000000000000000000000009B77444163454341
      443D9B7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FE7F186318631863FE7F00004539253925394539453945394539
      45394539453945392539253925394539FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      00000000FE7F88420022002200228842FE7F0D4E2E52CA458841884188418841
      8841884188418841AA452E522E520D4EFF7F3967396739673967396739673967
      3967396739673967396739673967396739670000000000000000000000000000
      000000009C73CD5DC454C454C454CD5D9C730000000000000000000000000000
      0000000088428032004390630043803288424F52663D4F56CA45884188458845
      8845A845A84588418841CA4545394F52FF7F5542343E343E343E343E343E343E
      343E343E343E343E343E343E343E343E55420000000000000000000000000000
      00000000AC5DE560877D877D877DE560CC5D7863186318631863186318631863
      1863186300220043004300000043004300224539883D8841A845A945EA4D8E5A
      D56FEB4DA949A949A945884188414539FF7F343E000000000000000000000000
      00000000000000000000000000000000343E7B6F396739673967396739673967
      396739678358887D877D677D877D887DC3541053104310431043104310431043
      1053105300229063000000000000906300222539A945A945A949C949B56F536B
      5267B56F3267C949C949A945CA452539FF7F343E0000734ED55A9352314A9452
      9452945293527352B4569452734E0000343EF152CD46CD46CD46CD46CD46CD46
      CD46EE466254F57E000000000000F57EA3549042786310631053106310631063
      10631863002200530053000000530053002225352D52A949CA4DCA4DEB4D6F5A
      F162B56F956FCA4DCA4DC9494E562535FF7F343E000000000000000000009C73
      00009C7300000000FF7F000000000000343EAD46D96F115BF052325F325F325F
      325F535F6254CB7DCB7DCA7DCB7DEB7DA3549042786388428832786378639063
      1053105388328032805390738053803288324539705A0C52EB4DEB51B66FB673
      3367B05E2D56EC51EB4D0C524E56A945FF7F343E0000774A3B67774E3B67774E
      3B67774E3B67FA5E56465646FA5E0000343EAC42D96F8B3E4936B767B667535B
      115331538851E5640D7E2E7E0D7E0665A84D9042786310537863786378639042
      786378631053084300220022002208439042BC77463DD4622D560C52546BB773
      756F966F336B0C522E56D466663DBC77FF7F343E0000564AFF7F574AFE7B574A
      FE7B574ADE77574AFF7FFF7F564A0000343E8C42D973CE4A966796679667AD46
      96639663EE46EB5D6254625461540D62CD3E8842FE7F90427863883278639042
      7863786390427863904278631053FE7F9042FF7F1667463DF5664F5A2E56B162
      766FD3662E5A4F5AF56A463D1667FF7FFF7F343E0000574AFE7F574ADE7B574A
      DE7B574ABD77574AFE7FFE7F5646FF7F343E8B3EDA73AD4695674A369567AD46
      96639663CE46B6678B36D663EE46FA738C3E8842FE7F90427863786378639042
      7863786390427863786378639042FE7F8842FF7FFF7F1667663DF56A4F5A505E
      AF660E525277166B673D1667FF7FFF7FFF7F343E0000574A1A63774A1A63774A
      1A63774A1A63F95A56465646D95AFF7F343E8B3EDB77AC429463746374638C3E
      966396638C3E946394639563AD42DB778B3E8842FE7F88428842906390631053
      1053104310539063906388428842FE7F8842FF7FFF7FFF7FD462EB4D386F176B
      B47FB062757BCB49D462FF7FFF7FFF7FFF7F343EFF7F9C735B6B9C735B6B9C73
      5B6B9C735B6BBD77BD77BD77BD77FF7F343E6B3ADB776A3A8B3E73637363F052
      EF4ACF46F052736373638B3E6A3ADB776B3A8842FE7F08320832884288428842
      8842884288428842884208320832FE7F8842FF7FFF7FFF7FFF7F673D88412739
      CF66737B0B520D4EFF7FFF7FFF7FFF7FFF7F343E0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000343E6A3AFC7B282E28326B3A6B3A6B3A
      6B3A6B3A6B3A6B3A6B3A2832282EFC7B6A3A8832FE7FFE7FFE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FFE7F8832FF7FFF7FFF7FFF7FFF7F0E4E7377
      947BB47F1567FF7FFF7FFF7FFF7FFF7FFF7FD856343E343E343E343E343E343E
      343E343E343E343E343E343E343E343ED8564A36FC7BDC7BDC7BDC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7BFC7B4A361053105378637863786378637863
      786378637863786378637863786310531053FF7FFF7FFF7FFF7FFF7F463DEB4D
      505AEC51EC45FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000F14E115396679567956795679567
      95679567956795679567956796671153F14E0000105388328832883288328832
      883288328832883288328832883210530000FF7FFF7FFF7FFF7FFF7F596F925A
      0D4E67414639FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D14E4A364936493649364936
      4936493649364936493649364A36D14E0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000396739673967396739673967
      3967396739673967396739673967396739670000000000003967396700000000
      0000000000000000000000000000000000000000000039673967000000000000
      000000009C733967396739679C7300000000075607560756FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB84A55095405550536012576266A
      076A076A076A076A076A076A076A076A276A000000000000666E876E5A6B0000
      00000000000000000000000000000000000000000000666E876E5A6B00000000
      00009C73CA46602E602E602ECA469C730000E755CD6AE755FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F760D7B2EBC3A9C369D32266E487F
      697F697F697F687F697F697F697F497F276E000000000000CC72666E886E3967
      FF7F0000000000000000000000000000000000000000CC72666E886E3967FF7F
      0000CA468036E0426E63E0428036CA460000E755CD6AE655FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF8191D4B9C327C2E7D2A563E4672
      6A7F6A7F6A7F0E1D6A7F6A7F6A7F67728F5A000000000000767B666E0C77666E
      1767FF7F000000000000000000000000000000000000767B666E0C77666E1767
      FF7F602A0047E0420000E0420047602E0000E755CC6AE659FF7FFF7FFF7FFF7F
      FF7FDE7B39670000000000007B6FDE7BFF7F39265E5FDE3AFE3EFF7FDF772B62
      097B8B7F8B7F0C6F8B7F8B7F0A7F276206320000000000000000666E507B917F
      666E1767BD77000000000000000000000000000000000000666E507B917F666E
      396760268E670000000000008E67602E0000E655CC6AE45D7B6FDE7BFF7FFF7F
      FF7FFA4E5C163C123C123C129B2EFA569C733A227E639F67FF77FF7FDF7B7C6B
      276E8C7FAC7F2D21AD7F8C7F066AED5206360000000000000000CD72EA76D57F
      6F7F666EF26ABD7700000000000000000000000000000000CD72EA76D57F6F7F
      68726026204B204700002047204F602E0000E655AC6A3D0EBB32FA5200000000
      00005C163C123C127F477F43FF2A7D1ABB36FF7B9B32FD4215678B62E74D1042
      9552C976CE7F2D21CE7FC9766652725F063A0000000039673967F46AA872D77F
      907F6E7B666EF36A9C730000000000000000000039673967F46AA872D77F907F
      6F7F624AA032404FAE6B404FA03A0D530000E655AB723E0A9F167D163C123C12
      3C123C125D163B0E7F435F3F1F2F3F3F3C12FF7FFF7FBF73B2568C622A56CC35
      354F6A6E8E7F2D21AE7F6A6A935F7363263E00000000676EA972A8728772D77F
      8F7F8F7F6E7B8872D06A7B6F0000000000000000676EA972A8728772D77F8F7F
      8F7F6E7F834A6026602A602E0D5300000000E655AA723D0A3F3BFF261F333F37
      3F3B5F3B5F471B0E7F435F3F1F2B5F473C12FF7FFF7F3867EF720F73AE6A4B5E
      4846894E6872706F68720A4AED528A4AFE7F00000000666ED37F907F8E7F6D7B
      6D7B6D7B8E7F6E7F8872AD6A7B6F000000000000666ED37F907F8E7F6D7B6D7B
      6D7F8E7F6E7F8876AF6E7B6F000000000000E655AA6E3D0A5F43DF221F2F3F3B
      7F475F3B7F4B1B0A7F435F3F1F2B7F533C0EFF7FFF7FE234B47F517BEF6ECE6A
      0539FE7B295E89722862E841BB77FF7FFF7F000000008B6E707B917F6D7B6D7B
      D77FD77FD67FD57FD57FA872AD6E0000000000008B6E707B917F6D7B6D7BD77F
      D77FD67FD57FD57FA872AD6E000000000000E655AA6E3D067F4FDF221F2F3F37
      7F433F377F531B0A7F435F3BFF2A9F5B3C0EFF7FFF7FE134AD66CF6A3177F072
      80285A6B1073CE6ECE6A6B5E3867FF7FFF7F0000000013770D77B37F4B7B4C7B
      C872466E466E466E676E676E686E00000000000013770D77B37F4B7B4C7BC872
      466E466E466E676E676E686E000000000000E655AA6E3D067F57DF1E1F2F3F37
      7F433F379F5B1B0A9F575F3BFF26BF633C0EFF7FFF7F0239C555295AEA51263D
      A12C453DD57F527BF06ECE6AE334FF7FFF7F00000000BB7FA972B67F6C7B4A7B
      B57FCC72D16AFF7F000000000000000000000000BB7FA972B67F6C7B4A7BB57F
      CC72D16AFF7F000000000000000000000000E655AA6E3D069F5FDF1EFF2A3F37
      7F433F37BF631B0ABF6FDF73DF73DF733C0EFF7FFF7F0239E555285EE8552439
      2539E134AD66CF6A31771073A12CFF7FFF7F000000000000676EB57F707B297B
      707B957F476E5A6B0000000000000000000000000000676EB57F707B297B707B
      957F476E5A6B000000000000000000000000E655AB6E3E06FF777F535F4B5F43
      7F4B5F43FF731C0A3C129C269D2A9F673C0EFF7FFF7F9B77223D834943410339
      FF7F0239E555295AEA51463DC230FF7FFF7F0000000000008B6E717BB57F277B
      287BD77FED728B6ADE7B0000000000000000000000008B6E717BB57F277B287B
      D77FED728B6ADE7B00000000000000000000E655CB6E3E06BE2A3F439F5FFF73
      FF77FF737F573C0EFF7FBF6B7E5BBD2E3C12FF7FFF7FFF7FFF7FFF7FDE7BFF7F
      FF7F0239E555285EE8552439C234FF7FFF7F00000000000013730D77FA7FD97F
      D87FFA7FD87F686EF46A00000000000000000000000013730D77FA7FD97FD87F
      FA7FD87F686EF46A00000000000000000000AE62E559D35A5E539D263C0E3C0E
      3C0E3C0E3C121E47FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F9C77223D6349434102399B77FF7FFF7F000000000000BA7B686E476E476E
      476E476E686E686E696E000000000000000000000000BA7B686E476E476E476E
      476E686E686E696E000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      0000000000009C733967396739679C7300000000000039673967396739673967
      3967396739673967396739673967000000006072607240724072407260766076
      607640724072407240724072407260726072000039673967BD779C7339673967
      3967396739670E6A2665276527650E669C730000000000000000000000000000
      000000007B6FCA46602E602E602ECA469C7300000000F231D129B129B129B129
      B129B129B129B129B129D129F231000000006172677B677B667B867BC762C935
      A67F867B667B667B667B667B677B877B617200008876887A136F184A152DF52C
      F52CF6280C51466D667D667D677D47690E667B6F396739673967396739673967
      39673967A9428036E0426E63E0428036CA4600000000D1290000000000000000
      000000000000000000000000D12900000000307BC376687B467B467BA7628A08
      A835667F467B467B467B467B687BC376307B0000A87AB07F3435162D572D772D
      772D7829246D877D877D677D877D887D26655726F605F605F605F605F605F605
      F705F901602E0047E0420000E0420047602E00000000B1290000FF7FFF7F0000
      D515D5150000FF7FFF7F0000B12900000000FF7F4072497B477B467B667F667F
      C910C666467F467B267B467B497B4072FF7F00007677343537297831DA39FC3D
      FC3D1D3A0469F47E000000000000F57E2665F605FE7FDE7FDE7FDE7FDE7FDE7F
      DE7FFF7F402A8E670000000000008E67602E00000000B1290000FF7FFF7FD515
      FE3AFE3AD515FF7FFF7F0000B12900000000FF7F307BA3764A7B06778662C66A
      8835C9100673257F257B4A7BA476307BFF7F0000184A372DB835FB3D3C67FD7F
      B356FE7FC364CB7DCA7DCA7DCA7DEB7D2665F605FE7FBD77BD77BD77BD77BD77
      000000004026204B204700002047204F602E00000000B1290000FE7FD515FD36
      FD3AFD3AFD36D515FE7F0000B12900000000FF7FFF7F61722A7B2877C814A90C
      A90C8908E818E576287B2A7B6172FF7FFF7F0000362DB83DFB3D3B67FB7FFC7F
      FC7FFD7F4F72476D0D7E2E7E2D7E686D5072F605FE7F9C73734E734EBD73F85E
      000000002D53A036404FAE6B404FA03A0D5300000000B1290000D5111E431E43
      1D43FD363E471E43D5110000B12900000000FF7FFF7F757B83766E7F27568808
      A73D457F257F067B4E7F8376757BFF7FFF7F0000162D3A4A1C42FA7FFB7F2A21
      366BFB7FFC7F4E72E4640469256971720000F605FE7F9C739C739C739C739452
      945294529656673A40264026602A4A1E000000000000B1290000983AD619F619
      3D4FBC2EF61DF619983A0000B12900000000FF7FFF7FFF7F61722B7B6C7FE71C
      88082756267F2B7B2C7B6172FF7FFF7FFF7F000016297B521D42704EFA7F566B
      4B251567FA7F714E1F3E7D4E182100000000F605FE7F7B6F7B6F7B6F7B6F7B6F
      7B6F7B6F7C6F7D737D739E730000F905000000000000B1290000BD7BDD7FD615
      7D5B3D53F619DD7FBD7B0000B12900000000FF7FFF7FFF7FB97F6176937F8966
      6704870CA96E937F8276B97FFF7FFF7FFF7F00001629BC5A1D42F87F356B4B29
      3567D97FD87FF87F1D42BD5A172500000000F605FE7F5B6F7B6F7B6F5B6BBD77
      0000000000000000BE775B6FFF7FF705000000000000B12900009C739C77983A
      D615D615983A9C779C730000B12900000000FF7FFF7FFF7FFF7FA5762D7B927F
      E7202600C9140C77A576FF7FFF7FFF7FFF7F7B6F36299C565D4A3A674B293467
      D87FD77FD67F19635D4A9C5636297B6F0000F605FE7F5A6B3246524A5A6B185F
      0000000000000000185F5A6BFE7FF605000000000000B1290000000000000000
      000000000000000000000000B12900000000FF7FFF7FFF7FFF7FB97F8172967F
      4E7F4E7FD87F8276B97FFF7FFF7FFF7FFF7FCE76EF51B939FE663E461967D67F
      0C46D57FF9621D42FE66B939EF51CE760000F605FE7F39673A673A673A679452
      935273527352935293523967FE7FF6050000000000003726381E171E171E171E
      171E171E171E171E171E381E162600000000FF7FFF7FFF7FFF7FFF7FEB760B7B
      977F967F0D7BEB76FF7FFF7FFF7FFF7FFF7FA97A0A7F75313B4A3F6F9E521E42
      1E421E427E523F6F3B4A75310A7FA97A0000F605FE7FDE7FDE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FF60500000000000058225E4FFD36FD3AFD3A
      FD3AFD3AFD3AFD3AFD365E4F582200000000FF7FFF7FFF7FFF7FFF7FFE7F6072
      B97FB97F6072FE7FFF7FFF7FFF7FFF7FFF7FCA7A4C7F4D7F9531DA3D3F6B9F7F
      9F7F9F7F3F6BDA3D95314D7F4C7FCA7A0000F6099E63BB26BB26BB26BB26BB26
      BB26BB26BB26BB26BB26BB269E63F60900000000000058261C4B9A2A9A2A9A2A
      9A2A9A2A9A2A9A2A9A2A1C4B582600000000FF7FFF7FFF7FFF7FFF7FFF7FEA76
      0E7B0E7BEA76FF7FFF7FFF7FFF7FFF7FFF7F987F0B7BCF7FEF7F3052562D5629
      56295629562D3052EF7FCF7F0B7B987F0000170A5D533D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F5D53170A0000000000005826FB46FB46FB46FB46
      FB46FB46FB46FB46FB46FB46582600000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      60726072FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000987FEA7A097F707F00000000
      000000000000707F097FEA7A987F00000000B932170A160A160A160A160A160A
      160A160A160A160A160A160A170AB932000000000000792A5826582658265826
      582658265826582658265826792A000000000000000000000000000000003967
      3967396700000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      00000000000000000000000000000000000000007B6F39673967396739673967
      39673967396739673967396739675A6B0000000000000000000000000000AB3D
      89414A6E3967000000000000000000000000453A453A453A4536433614361436
      14361436143643364536453A453A453AFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D65A734E734E734E734E734E
      734E734E734E734E734E734E734EB55600000000FF7FBD773967396739670B52
      905A527FA64D39673967BD77FF7F000000000532C542C542A43EE229FF77E461
      2766E461FF77E229A43EC542A53E0532FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F734E00000000DE7BD65A314610421142C87A
      917F507F627EA64D5342D65ADE7B00000000DD77EA2D222E4C3E5B63DF73A059
      2866A059DF735B634C3A222EEA2DDD77FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BDE7B3146AD35DE7B
      3146AD35DE7F7C2A7C2ADD7BDE7B734E000000005A6B734EDE77BE777B6B7D6B
      C465087FA57E627EA64D954E5A6B00000000FF7F353ADF77BF6FBF6FDF6F4055
      27664055DF6FBF6FBF6FDF77353AFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BBD77DE7BDE7BDE77
      DE7BDE7BDD7BBD329C2EBD7BDE7B734E00000000F75EB556BD7711425B6B3142
      7D67C561287FA57E627EA551D65A00000000FF7F343ADF779E6BBE6F12323436
      343634361232BE6F9E6BDF77343AFF7FFF7F0000000000000000000000000000
      39673967DE7B0000000000000000000000000000734EDE7BBD733146AD35BD77
      3146AD35BD7BBE32BE32BC77DE7B734E00000000B556F75A5B6B3A6731465A6B
      32465C67E565087FA47ED06AEF3939670000FF7F333ADF77D852F952F9561A57
      1A571A57F956F952D852DF77333AFF7FFF7F0000000000000000000000003967
      8C318C31F75E0000000000000000000000000000734EBE779C739D73BD779D73
      9D77BD779C739C779C779C73BE77734E00000000524A18633967524639675246
      3A6752463B63C465757B3242F85EEE353967FF7F343ADF779E6BF9529E6FF952
      9E6FF9529E6FF9529E6BDF77343AFF7FFF7F0000000000000000000039678C31
      B556B5568C313967000000000000000000000000734EBD777C6F3246AE359C73
      3246AE359C733246AE357C6FBD77734E0000000031463963524A1963524A1963
      524A1963534A3963103EBD733142F759B365FF7F343EDF77D852F952F952F952
      F952F952F952F952D852DF77343EFF7FFF7F000000000000000039678C31F85E
      D65AD65AF85E8C31396700000000000000000000734EBD777B6B7C6B7C6B7C6F
      7C6F7C6F7C6F7C6B7C6B7B6BBD77734E00005A6B104295529452945294529452
      945294529452955295520F3EDC725966F669FF7F543EDF777D67D9529E6BF952
      9E6BF9529E6BD9527D67DF77543EFF7FFF7F00000000000000008C317B6F9D73
      9C739C739D737B6F8C3100000000000000000000734EBD775B67E37D847D7B6B
      3246AE357B6B047E847D5B67BD77734E00003146BD777B6F7B737B6F7B6F7B6B
      7B6B7B6B7B6F7B6F7B739B6F176E186E0000FF7F543EFF7BD84ED952D952D952
      D952D952D952D952D84EFF7B543EFF7FFF7F000000000000000031468C318C31
      8C318C318C318C313146000000000000000000009452DE7BBE77DE77DE77DE77
      DE7BDE7BDE77DE77DE77DE77DE7B945200001042BC77BA32B801186B1863185F
      185F185F1863186BB801BA32BC7730420000FF7F553EFF7B7D67D9527D67D952
      7D67D9527D67D9527D67FF7B553EFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000009452EF3DEF41EF41EF41EF41
      EF41EF41EF41EF41EF41EF41EF3D94520000B556BC77BF577B1AD8019B7B9C73
      9C739C739B7BD8017B1ABF57BC77524A0000FF7F5542FF7BD84ED852D852D852
      D852D852D852D852D84EFF7B5542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000094523F471F3F1F3F1F431F43
      1F431F431F431F431F3F1F3F3F47945200000000B5560F46BF5B7B1AD8010F4A
      10460F4AD8017B1ABF5B0F46B55A00000000FF7F7542FF7F5C63D84E5D63D84E
      5D63D84E5D63D84E5C63FF7F7542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093525F539D329C32BC32BC32
      BC32BC32BC32BC329C329D325F53935200000000000000000000BF5B7B1AD901
      0000D9017B1ABF5B00000000000000000000FF7F7642FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F7642FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093527F5B7F5F7F5F7F5F7F5F
      7F5F7F5F7F5F7F5F7F5F7F5F7F5B9352000000000000000000000000BF5B7B1E
      00007B1EBF5B000000000000000000000000FF7F195B76467646764276427642
      764276427642764276467646195BFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000B55693529352935293529352
      9352935293529352935293529352B55600000000396739673967396739673967
      396739670000FF7FFF7FFF7FFF7FFF7FFF7F00000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F000000000000FF7F000000007B6F3967396739673967
      39673967396739673967396739677B6F00000000000000000000000000000000
      000000000000000000000000000000000000B84A550554055405540554055405
      54055505B84AFF7FFF7FFF7FFF7FFF7FFF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000F75ED656D656D656D656
      B556B556B556B556B556B556B556F75E00000000000000000000000000000000
      000000000000000000000000000000000000760D7B2EBC3ABC3A9C3A9C3ABC3A
      BC3A7B2E960DFF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D65600000000307A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000F8191D4B9C327C2E7C2E7C2E7C2E
      9C321D4FB711FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000B55600000000E6700000
      0000000000000000000000000000B556000000000000000039677B6F00000000
      00000000000000000000000000000000000039265E5FDE3AFE3EFE7FBD7BFE3E
      FE3E7E63D815FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C5FDA46845CDA46
      B846B846B846B846B846B8465C63D55600000000000039672026653600000000
      0000000000000000000000000000000000003A227E639F67FF77FF7FDE7B9E6B
      3D535D5F1A22FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000000007710000
      0000FF7FFF7FFF7FFF7FFF7F0000D55600000000396700226A5B202239673967
      396739673967396739673967396739677B6FFF7B9C32FD4215676B62E7512E46
      F71D1726D952BD77DE7BFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63DA46845CFA46
      B84AB84AB84AB84AB846B8465C63D5560000396700224C5B6053E03E00222022
      20222022202220222022202220222026873AFF7FFF7FBF73B2568C622A560E42
      BA42FF73B93EB84AF95A39673967BD77FF7FF2310000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D5560000FF7F0671FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000D556000020266E5F204F204F204F204F4053
      405340534053405340534053405340532026FF7FFF7F3867EF720F73AD6A4962
      954ADA42FF7FBA42993A16221726D84EBD77F231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000D5565C63DA46845CFA4A
      B84AB84AB84AB84AB84AB8465C63D556000020267067004F004F6C638C678C67
      8C678C678C678C678C678C678C678C6B2026FF7FFF7FE234B47F517BEF72714E
      792E7E5FFF73FF73FF77FF779E637936D8520000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5560000FF7BE670FF7F
      FE7FDE7FDE7FDE7FDE7FDE7B0000D556000000000022936B004FA03E00220022
      20222022202220222022202220222022C942FF7FFF7FE134AD66CF6E307B3826
      9F67DF6FDF6FDF6FDF6BDF6BDF6FBF67372AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C63DA46845CFA4A
      B94AB84AB84AB84AB84AB8465C63D5560000000000000022946F202200000000
      000000000000000000000000000000000000FF7FFF7F0239C555295EC8553922
      FF73BF67BF6BBF6BBF6BBF67BF67FF733826F231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000FF77E66CFF7B
      DE7BDD7BDD7BDD7BBD7BBD7B0000D55600000000000000002026643200000000
      000000000000000000000000000000000000FF7FFF7F0239E555275EC5555A26
      DF739F63BF67BF67BF67BF679F63DF73382A0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63D946845CDA46
      B84AB84AB84AB84AB84AB8465C63D55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F9C77223D63492141392A
      BF67BF679F639F639F639F63BF639F67392AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000B5560000BD73C568BD73
      9C739C739C739C739C739C730000B55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F7E67
      9B3A9E67DF73DF73DF73DF739E679A3A7D6300000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D65600000000317A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      7E637A32592E592E592E592E7A327D63FF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231FF7FFF7F00000000F75AD656D656D656D656
      B556B556B556B556B556B556D656F75A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000396739673967396739670000
      FF7FFF7F00003967396739673967396700000000000000000000000000000000
      0000000000000000000000000000000000000000396739673967396739673967
      39673967396739673967396739675A6BFF7F0000DE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7B7B6FCA46602E602E602ECA469C73524ACE39AE35AD358D318C311042
      FF7FFF7F524ACE39AE35AD358D318C3110423967396739673967396739673967
      3967396739673967396739673967396739678C66476627664766476A466AAB39
      89412A6A466A4666266626662666696AFF7F5A6B195F195F195F195F195F195F
      195F1A5FC9468036E0426E63E0428036CA46EF39F03D734A1142AE354B296C2D
      FF7FFF7FEF39F03D734A1142AE354B296C2D216C006C006C006C006C006C006C
      006C006C006C006C006C006C006C006C216C47666E7F4B7F297BA772CA760C4E
      905A517F8649B07F8F7F8F7F907F2666FF7F993A993279327932793279327932
      79329C32602EE046E0420000E0420047602ECE3939675B6B3A67396318636B2D
      FF7FFF7FCE393A675B6B3A67396318636B2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF7B7B5B27664B7F097BA772E77A2666C976
      917F507F627E86498F7F6E7F907F2666FF7F9932FF7BFF77FF77FF77FF77FF77
      FF77FF7B402A8E670000000000008E67602ECE39D6563963B6565246CE396B2D
      39673967CE39D6563963B6565246CE396B2D0070237DC07CC07CC07CC07CC07C
      C07CC07CC07CC07CC07CC07CC07C237D00704766297BA772E776487F26668E7F
      C661087FA57E637E86458E7F907F2666FF7F7932FF773C539E63DF6FDF6FDF6F
      DF6FFF734026204B004700002047204F602ECE39D6563963B656524ACE396B2D
      EF398C31CE39D6563963B656524ACE396C2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF775B5B4766A772E77A487F6C7F06666D7F
      6D7FC65D097FA57E627E8649B17F466AFF7F7932FF77FF77DB421C4BDF6FDF6F
      DF6FFF730D4BA036404FAE6B404FA0368B32CE39D6563963B6565246CE396B29
      CE396C2DCE39D6563963B6565246CE396B2D0078877D807C807C807C807C807C
      807C807C807C807C807C807C807C877D00704766CB72266626660666886E6C7F
      4C7F6C7FC65D087FA47EB06AD031456EFF7F7932FF77BF6BFF7BDB42DF6FFB46
      582A592EFE4A0D4B402A4026402A0D4F9C36CE397B6FBD777B6F3967F75E4A29
      5A6B3146CE397B6FBD777B6F3967F75E6B2D7401FF73BC01DF63BC01DF63BC01
      DF63BD01FF7BFF77FF77FF77FF77FF775B5B4666AB726C7F6C7F6C7F4C7F4B7F
      4B7F4C7F6C7FC5617677323EF85AEF3539677932FF77BF67BF67FF7F1C4BFB46
      00000000FC461E4F0000DF6BDF6BFF779A32EF39AD358C316C2D6B2D6B2D3246
      5A6B3142744EAD358C316C2D6C2D6C2D524A72019D2A9F677901BF6B7901BF6B
      7901BF63407C407C407C407C407CCC7D00702666B27F4A7F4B7F4B7F4B7F4B7F
      4B7F4B7F4B7F6A7FF2399D733142F759B3657932FF77BF63BF633D4FDB420000
      BF67BF670000DB423D4FBF67BF63FF777932FF7FCE39F75E39679452EF3D4B29
      5A6B3146CE39F75A39679452EF3D6B2DFF7F5201FF7F57019E6B57019E6B5701
      9E6B5801FF7FFF77FF77FF77FF77FF775A5B2666B37F497F4A7F4A7F4A7F4A7F
      4A7F4A7F4A7F4A7F497FF139DC6E5962F6657932FF77BF637E57FB42FF7FBF63
      BF63BF63BF63FF7FFB427E57BF63FF777932FF7FCE39D75A1963944EEF3D6B2D
      CE396C2DCE39F75A1963944EEF3D6C2DFF7F5201DC429E6736019E6B36019E6B
      36019F63007C207C207C207C007C307E00702666B37F287F297F297F297F297F
      297F297F297F297F497F477F1966196AFF7F7932FF7B9F5BDB42FF779F5F9F5F
      9F5F9F5F9F5F9F5FFF77DB429F5BFF7B7932FF7FCF39F75E39679452EF3D6B2D
      EF398C31CF39F75E39679452F03D6C2DFF7F5201000014017E6714017E671401
      7E671501FF7FFF77FF77FF77FF77FF777B5B2666B47F917F917F927F927F927F
      927F927F927F927F917F917FD37F2566FF7F7932FF7BFC46DF739F5B9F5B9F5B
      9F5B9F5B9F5B9F5B9F5BDF73FC46FF7B7932FF7F9452AE358D318C2D6C2D3146
      FF7FFF7F734EAD358D318C318C31524AFF7F72011C5700001C5300001C530000
      1C53FF7FB37EB47EB47EB47EB57ED57E006C2666B57FC676C776C776C776C776
      C776C776C776C776C776C676D57F2666FF7F9932FF7FFF7FFF7BFF7BFF7BFF7B
      FF7BFF7BFF7BFF7BFF7BFF7BFF7FFF7F9932FF7FFF7F3967CF393963734E6C2D
      FF7FFF7FCF393963534A6C2D3967FF7FFF7F5726720152015201520152015201
      720174010074006C006C006C006C006CE7702666D67F267F277F277F277F277F
      277F277F277F277F277F267FD67F2666FF7F1B4F993279327932793279327932
      79327932793279327932793279329932DA42FF7FFF7FEF39324632468C316C2D
      FF7FFF7FCF39314632468D318C2DFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000002666B07F907F907F907F907F907F
      907F907F907F907F907F907FB07F2666FF7F0000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FEF39CE35AD358D316C2D
      FF7FFF7FEF39CE35AD358D318C2DFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000686A266626662666266626662666
      2666266626662666266626662666686AFF7F0000000000000000000000005A6B
      F75EF75E5A6B00000000000000000000000000000000000000000000F75EF75E
      F75EDE7B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B6F
      396739673967396739673967396739677B6F00005A6BF75EF75EF75EF75E734E
      10423146734EF75EF75EF75EF75E5A6B00000000BD771863F75EF75E6B2D4A29
      3146D65A1863BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B6F39673967396739673967CB51
      24412541254125412541254125412541CB4D00001042AD35AD35AD35AD35524A
      B5563967F75E524AAD35AD35AD35104200000000524A0821E71CE71CEF3D3146
      5A6B8C310821524A000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010428C318C318C318C318D312445
      4B5EE455E459C451C451E459E4554B5E2441F75EAD353967CE39AD35DE7BBD77
      1042F75E7B6FF75E104210423146EF3D7B6F00002925EF3D31463146D65A5A6B
      5A6B94528C312925000000000000000000000000000000000000000000000000
      00000000000000009C73F75E0000000000008C31AE35CF39CF398C31AE312441
      0656824D8249413D413D8249824D06562441EF3DF75E2925EF3D8C31BD777B6F
      9C731042F75E5A6BF75E18631863734E945200006B2D945210421042EF3DCE39
      1863B55694528C31F75E00000000000000000000000000000000000000000000
      0000000000000000524A8C31F75E00000000CE39744EEF398C314A294B292445
      275AA24DA24D087F087FA24DA24D275A24410000AD356B2D10428C31BD777B6F
      7B6F7B6FEF3D396739675A6B5A6B5A6B524A0000AD35F75E524A524AFF7FBD77
      CE391863D65A94528C31F75E0000000000007B6FF75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75E8C3118638C31F75E00008C3118638C31CE39C61809210441
      8C62814D814D81498149814D814D8C622441F75EAD35396731468C31BD775A6B
      5A6B7B6FEF3D7B6F7B6FAD35AD359C73524A0000EF3D5A6B9452BD770000DE7B
      5A6BCE391863B556B5568C31F75E0000000010428C318C318C318C318C318C31
      8C318C318C318C31524AF75EF75E8C31F75E1863AD35103E1863324653460341
      8D5E6C5A6C5E6C5A6C5A6C5E6C5E8D5E2441EF3DF75E2925524A8C31BD773967
      DE7B000010425A6B7B6FCE39CE397B6F734E0000CE39D65A9C735A6B524ACE39
      3146BD77EF3D5A6BCE39B5568C31F75E00008C31F75ED65AD65AD65AD65AD65A
      D65AD65AD65AD65AB556B556B55618638C310000FF7FAE35BD7BC614C614CA51
      E23CE23C0341033DE33C033DE24003412D5A0000AD356B2D734E8C31BD773967
      0000B556734E94525A6B5A6B5A6BB5561863000000009452524AD65A3146AD35
      EF3D524AB556AD359C73CE39EF3DCE3900008C315A6B39673967396739673967
      396739673967396739679452945239678C3100000000FF7F366FAD6A4A5A156B
      DE7BBE77E140DF77DF77E13C000019020000F75EAD35396794528C31BD771863
      0000AD35D65A734E524A524A734E104200000000000000005A6B734ED65A734E
      EF3D9C7300000000CE39D65A524A10420000524A8C318C318C318C318C318C31
      8C318C318C318C31314694525A6B8C310000000000005A6F31771073AD6A8C66
      3E0F7F07CC2DC040C040AC250000F7010000EF3DF75E2925B5568C31DE7B1863
      DE7B000000000000DE7B3967DE7BAD3500000000000000008C317B6FF75EB556
      9452CE39000000000000524A3146000000000000000000000000000000000000
      00000000000000008C315A6B8C3100000000000000006741B57F517BEF72CD6E
      48215F235E1F5F1F5F1F5E1B0000F60500000000AD356B2DB5568C31DE7B1863
      186318631863186318631863DE7B8C310000000000000000C618DE7B5A6B1863
      F75E841000000000000000000000000000000000000000000000000000000000
      0000000000000000734E8C3100000000000000000000E134AD6AAE6A1173CE6E
      4028000000000000000000000000F6050000F75EAD35F75EB5568C31FF7FF75E
      18636B2D314631461863F75EFF7F8C310000000000000000E71C4A29EF3DB556
      3146A51400000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000239E555295EEA510439
      802C19639C730000BF577F230000F6050000EF3DF75E0821B5568C31FF7FF75E
      F75EF75EF75EF75EF75EF75EFF7F8C310000000000000000E71CCE391042CE39
      0821A51400000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000705AE455275EE755E234
      AB411963F75ED75E9E43DC020000F60500000000AD35BD779C738C310000FF7F
      FF7F00000000FF7FFF7FFF7F0000AD3500000000000000004A29AD35EF3DAD35
      0821292500000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000004E5A624921456F5E
      0000000000000000000000000000F60500000000524AAD35AD35AD35AD358C31
      8C318C318C318C318C318C31AD35524A00000000000000009C73292529252925
      08219C7300000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DC2E1806
      F705F605F605F605F605F605F605B93200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000039673967
      3967DE7B00000000000000000000000000000000396739673967396739673967
      3967396739673967396739673967396700000000000000000000000000003967
      3967396739670000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DE7B396739673967CA458941
      496E166B3967DE7B000000000000000000005722F605F605F605F605F605F605
      F605F605F605F605F605F605F605F6055722000000000000000000000000EF3D
      9C73D65A10420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000B84A5509550536012A566E5A
      517FA5515701B84A00000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6057B6F396739673967396739671042
      BD77F75A1042396739673967396739677B6F000000000000F75E396700000000
      000000000000000000000000000000000000000076095A2E9C369D32C77E4F7F
      507F627EA555990100000000000000000000F605FF7F744E3146524A5A6BFF7F
      18631863FF7F1863186318631863FF7FF6053146AD358D318D31AD31AD31AE31
      8D318D318D31AD35AD35AD35AD35AD35314600000000F75E8C31EF3D00000000
      0000000000000000000000000000000000000000D819FD467C2E7C2A7E22C365
      077FA57E627EA65139670000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF605AE3539671863185F5A6B954A0060
      954E744E744E744E744E744E744EB552AE350000F75E8C3118638C31F75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E5A6B000019263D57BD36BD36FF7FDF73
      C461087FA57E627EA6513967000000000000F605FF7F534A314231463967FF7F
      F75EF75EFF7FF75EF75EF75EF75EFF7FF605CE391863D65AD65A3967D7520060
      D752B552B552B552B552B552B552F75ECE39F75E8C31F75EF75E524A8C318C31
      8C318C318C318C318C318C318C318C31EF3D00005A2E7E67FE3EDF6FFF7FDF77
      9F5FC465087FA47ED06EEF39396700000000F605FF7FFF7FFF7FFF7FFF7FDE7B
      FF7BFF7BDE7BFF7BFF7FFF7FFF7FFF7FF605CF39185FB556B5523963185B0060
      195BF75AD75AD75AD75AD75AD75A3A67CE398C311863B556B556B556D65AD65A
      D65AD65AD65AD65AD65AD65AD65AF75E8C3100005A261D4FBF6B76778B62E751
      7056DF67E369757B3242F85EEE3539670000F605FF7F524610421142185FDE7B
      D65AD65ADE7BD65AD65AD65AD65AFF7FF605EF3D3967944E744E7B6B5A630064
      5A631963186318631863186319639C6FCF398C31396794529452396739673967
      39673967396739673967396739675A6B8C3100000000FD42BE32175F6C5EE84D
      5246BF321E471042BD733142F759B3650000F605FF7FDE7BDE7BDE7BDE77BD77
      BD77BD77BD77BD77DE77DE77BD77FF7FF605F03D5A6B534A524A7B6B9C670064
      9C675A675A675A675A673A677B6BBD77EF3D00008C315A6B945231468C318C31
      8C318C318C318C318C318C318C318C31524A0000000000007B6FAF62EF6EAD66
      4C569C73000000003042DC725962F5650000F605FF7F3146F03D103EF75ABD73
      B556B556BD73B556B556B556B556FF7FF605103E9C73524A3246DE77DE6F0064
      DE6F9C6F7C6F7C6F7C6F9C6FDE77BD77F03D000000008C315A6B8C3100000000
      000000000000000000000000000000000000000000000000CB49937F1077CE6E
      AD6A0D52000000000000386E376A00000000F605FF7F9C73BD77BD739C739C6F
      9C739C739C6F9C739C739C739C73FF7FF605B656D65A9C73DE7BFF7FFF7B0068
      FF7BDF7BDE7BDE7BDE7BDE7BDE7BB656B6560000000000008C31EF3D00000000
      000000000000000000000000000000000000000000000000C030D57F727F3073
      0F73A02C0000000000000000000000000000F605FF7FF03DCE39CF39B5567B6F
      944E944E7B6F9452945294527352FF7FF6050000534A3146314231425342006C
      7342314231423142314231423142524A00000000000000000000000000000000
      000000000000000000000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6050000000000000000000000000070
      3967396739677B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000F6097E5B7B1A7B1E7B1E7B1E7B1E
      7B1E7B1E7B1E7B1E7B1E7B1E7B1A7E5BF6090000000000000000000000000070
      0070007400740871000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000170A5D4F3D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F3D4F5D4F170A0000000000000000000000000070
      00700068005C0070000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000009B77444163454341
      443D9B770000000000000000000000000000992A170A160A160A160A160A160A
      160A160A160A160A160A160A160A170A992A0000000000000000000000000070
      00700070006C297500000000000000000000424D3E000000000000003E000000
      2800000040000000A00000000100010000000000000500000000000000000000
      000000000000000000000000FFFFFF00FFF0FF9F000000000000FF8700000000
      0000000100000000000000010000000000007FC00000000000017F8000000000
      0003400000000000000340010000000000034000000000000003400000000000
      021340000000000000C37FFD0000000000030001000000000003000100000000
      00030001000000000003000100000000F000FFDFFFFFFF8F0800FF9FE0078007
      04008081E007800302000001E007800101000001E007800100800081E0078001
      00400391E0078001002003C1E0078003001003E1F00FC007000807E1F81FE00F
      00040003F81FE07F0002C003F81FE07F0000C007F81FE07F0000E003F81FE07F
      0000E003F81FE07F0000F01FF81FE07FFFFFFFFEFFFFFFFFFFC10000FFFFFFC1
      FF8000000000FF80FF8000000000FF80000800007FFE0000001C00004002001C
      000800007D6E0000000000004002000000000000400200000000000040000000
      0000000040000000000000000000000000000000400200000000000000000000
      00000000FFFF000080010000FFFF8001E0008000E7FFCF8300000000E3FFC701
      00000000E0FFC10100000000E07FC01100380000F03FE03900000000F01FE011
      07000000C00F800100000000C007800300000000C003800700000000C0038007
      00000000C003800700000000C01F803F00000000E01FC03F00000000E00FC01F
      00000000E00FC01F00000000E00FC01FFFFFFFC1FFC1C00300008000FF80C003
      000080000000DFFB000080000008D24B0000801C001CD00B000080000188D00B
      000080000180D00B000080010001D00B000080030005D00B0000800301E1D00B
      0000000101E1DFFB000000010001C003000000010001C003000000010001C003
      000000010001C003000087C30001C003FC7FFFFEFFFF8001FC3F0000FFFF8001
      80030000FFFF800180030000FFFF800180030000FFFF800180030000FE3F8001
      80010000FC3F800180000000F81F800180000000F00F800100000000F00F8001
      00010000F00F800100010000FFFF800100010000FFFF800180030000FFFF8001
      F11F0000FFFF8001F93F0000FFFF80018040DB6EC001FFFF00000000C001FFFF
      00000000DBFDFFFF00008002DBFDE7FF00000000C001C7FF00000000DB058000
      00008002C001000000005B6CD005000000000000C001000000008002D0058000
      00000000C001C7FF00000000D005E7FF00008002C001FFFF00000000D005FFFF
      0000DB6CDBFDFFFF00000000C001FFFFFFC18241FFFF80008000000000000000
      00000000000000000008000000000000001C0000000000000008000000000000
      0000000000000000000000000000000001900000000000000240000000000000
      00000000000000000000000040000000000000002A0000000000000000000000
      00000000FFFF0000FFFF0000FFFF0000FC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  inherited dsCL: TevDataSource
    Left = 295
    Top = 31
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 440
    Top = 15
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 586
    Top = 18
  end
  inherited wwdsEmployee: TevDataSource
    Top = 16
  end
  inherited DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 692
    Top = 18
  end
  inherited wwdsSubMaster: TevDataSource
    OnDataChange = wwdsSubMasterDataChange
    Left = 638
    Top = 5
  end
  inherited wwdsSubMaster2: TevDataSource
    Top = 18
  end
  inherited ActionList: TevActionList
    Left = 675
    Top = 11
  end
  inherited EEClone: TevClientDataSet
    Left = 538
    Top = 8
  end
  object DDSrc: TevDataSource
    DataSet = DM_EE_DIRECT_DEPOSIT.EE_DIRECT_DEPOSIT
    OnDataChange = DDSrcDataChange
    OnUpdateData = DDSrcUpdateData
    MasterDataSource = wwdsEmployee
    Left = 91
    Top = 69
  end
  object ChildSupportSrc: TevDataSource
    DataSet = DM_EE_CHILD_SUPPORT_CASES.EE_CHILD_SUPPORT_CASES
    OnDataChange = ChildSupportSrcDataChange
    MasterDataSource = wwdsEmployee
    Left = 48
    Top = 71
  end
  object PopupMenu1: TevPopupMenu
    Left = 740
    Top = 65
    object Copy1: TMenuItem
      Caption = 'Copy'
      OnClick = Copy1Click
    end
    object UpdateEDs1: TMenuItem
      Caption = 'Update EDs'
      OnClick = UpdateEDs1Click
    end
    object UpdateAgencies1: TMenuItem
      Caption = 'Update Fields'
      OnClick = UpdateAgencies1Click
    end
    object FocusMain1: TMenuItem
      Caption = 'Update Amount'
      OnClick = UpdateAmountExecute
    end
    object UpdatePercent1: TMenuItem
      Caption = 'Update Percent'
      OnClick = UpdatePercentExecute
    end
  end
  object dsCL_E_DS: TevDataSource
    AutoEdit = False
    DataSet = DM_CL_E_DS.CL_E_DS
    Left = 128
    Top = 72
  end
  object evdsCLBenefitSubtype: TevDataSource
    MasterDataSource = dsCLBenefits
    Left = 152
    Top = 64
  end
  object dsCLBenefits: TevDataSource
    AutoEdit = False
    DataSet = DM_CO_BENEFITS.CO_BENEFITS
    Left = 184
    Top = 64
  end
  object cdsDependentHealthCode: TevClientDataSet
    FieldDefs = <
      item
        Name = 'custom_e_d_code_number'
        DataType = ftString
        Size = 20
      end>
    Left = 306
    Top = 448
    object cdsDependentHealthCodecustom_e_d_code_number: TStringField
      DisplayLabel = 'E/D Code'
      FieldName = 'custom_e_d_code_number'
    end
    object cdsDependentHealthCodeCL_E_D_NBR: TIntegerField
      FieldName = 'cl_e_ds_nbr'
      Visible = False
    end
  end
  object cdEEScheduledView: TevClientDataSet
    Left = 600
    Top = 168
    object cdEEScheduledViewEE_SCHEDULED_E_DS_NBR: TIntegerField
      FieldName = 'EE_SCHEDULED_E_DS_NBR'
    end
    object cdEEScheduledViewEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdEEScheduledViewCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object cdEEScheduledViewDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdEEScheduledViewCALCULATION_TYPE: TStringField
      FieldName = 'CALCULATION_TYPE'
      Size = 1
    end
    object cdEEScheduledViewAMOUNT: TCurrencyField
      FieldName = 'AMOUNT'
    end
    object cdEEScheduledViewPERCENTAGE: TCurrencyField
      FieldName = 'PERCENTAGE'
    end
    object cdEEScheduledViewFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Size = 1
    end
    object cdEEScheduledViewEFFECTIVE_START_DATE: TDateTimeField
      FieldName = 'EFFECTIVE_START_DATE'
    end
    object cdEEScheduledViewEFFECTIVE_END_DATE: TDateTimeField
      FieldName = 'EFFECTIVE_END_DATE'
    end
    object cdEEScheduledViewSCHEDULED_E_D_ENABLED: TStringField
      FieldName = 'SCHEDULED_E_D_ENABLED'
      Size = 1
    end
  end
  object dsEEScheduledView: TevDataSource
    DataSet = cdEEScheduledView
    OnDataChange = dsEEScheduledViewDataChange
    Left = 600
    Top = 128
  end
  object cdEEBenefits: TevClientDataSet
    Left = 544
    Top = 168
    object cdEEBenefitsEE_BENEFITS_NBR: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object cdEEBenefitsBenefit: TStringField
      FieldName = 'Benefit'
      Size = 40
    end
    object cdEEBenefitsBenefitAmountType: TStringField
      FieldName = 'BenefitAmountType'
      Size = 40
    end
    object cdEEBenefitsTOTAL_PREMIUM_AMOUNT: TCurrencyField
      FieldName = 'TOTAL_PREMIUM_AMOUNT'
    end
    object cdEEBenefitsEE_RATE: TCurrencyField
      FieldName = 'EE_RATE'
    end
    object cdEEBenefitsER_RATE: TCurrencyField
      FieldName = 'ER_RATE'
    end
    object cdEEBenefitsCOBRA_RATE: TCurrencyField
      FieldName = 'COBRA_RATE'
    end
    object cdEEBenefitsBENEFIT_EFFECTIVE_DATE: TDateTimeField
      FieldName = 'BENEFIT_EFFECTIVE_DATE'
    end
    object cdEEBenefitsEXPIRATION_DATE: TDateTimeField
      FieldName = 'EXPIRATION_DATE'
    end
    object cdEEBenefitsReferenceType: TStringField
      FieldName = 'ReferenceType'
      Size = 10
    end
  end
end
