//******************************************************************************
//FileName      : SPD_RehirePromptForm
//Creation Date : 14 february 2007
//Project       : Employee Package
//Created by    : Andrey Solovyov
//Description   : Dialog window that shows what screens should be checked after
//                EE is rehired
//******************************************************************************
unit SPD_RehirePromptForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ISBasicClasses,  ExtCtrls,
  Buttons, EvContext, EvDataAccessComponents, EvUIComponents;

type
  TRehirePrompt = class(TForm)
    panelBorder: TevPanel;
    editInfo: TevRichEdit;
    btnClose: TevBitBtn;
    btnPrint: TevBitBtn;
    procedure btnPrintClick(Sender: TObject);
  public
    procedure GetEERehirePrompts(EE_NBR: integer; EEName, EECode, CoNbr, CoName: string);
  end;

implementation

{$R *.dfm}

uses EvUtils;

procedure TRehirePrompt.GetEERehirePrompts(EE_NBR: integer; EEName, EECode, CoNbr, CoName: string);
begin
  editInfo.Lines.Clear;
  Caption := 'Employee details to review for employee ' + EEName;
  ctx_StartWait;
  try
    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('COLUMNS', 'Count(EE_SCHEDULED_E_DS_NBR)');
      SetMacro('TABLENAME', 'EE_SCHEDULED_E_DS');
      SetMacro('NBRFIELD', 'EE');
      SetMacro('Condition', '((EFFECTIVE_END_DATE > :CurrentDate) or (EFFECTIVE_END_DATE is null))');
      SetParam('RecordNbr', EE_NBR);
      SetParam('CurrentDate', SysTime);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Scheduled E/Ds');

    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('COLUMNS', 'COUNT(Ee_Direct_Deposit_Nbr)');
      SetMacro('TABLENAME', 'EE_SCHEDULED_E_DS');
      SetMacro('NBRFIELD', 'EE');
      SetMacro('Condition', '((EFFECTIVE_END_DATE > :CurrentDate) or (EFFECTIVE_END_DATE is null))');
      SetParam('RecordNbr', EE_NBR);
      SetParam('CurrentDate', SysTime);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Direct Deposits');

    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('COLUMNS', 'COUNT(Ee_Child_Support_Cases_Nbr)');
      SetMacro('TABLENAME', 'EE_SCHEDULED_E_DS');
      SetMacro('NBRFIELD', 'EE');
      SetMacro('Condition', '((EFFECTIVE_END_DATE > :CurrentDate) or (EFFECTIVE_END_DATE is null))');
      SetParam('RecordNbr', EE_NBR);
      SetParam('CurrentDate', SysTime);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Child Support Cases');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(EE_RATES_NBR)');
      SetMacro('TABLENAME', 'EE_RATES');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Pay Rates');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(EE_STATES_NBR)');
      SetMacro('TABLENAME', 'EE_STATES');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('States');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(EE_LOCALS_NBR)');
      SetMacro('TABLENAME', 'EE_LOCALS');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Locals');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(Ee_Time_Off_Accrual_NBR)');
      SetMacro('TABLENAME', 'Ee_Time_Off_Accrual');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Time Off Accrual');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(EE_AUTOLABOR_DISTRIBUTION_NBR)');
      SetMacro('TABLENAME', 'Ee_Autolabor_Distribution');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Auto Labor Distribution');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(EE_PENSION_FUND_SPLITS_NBR)');
      SetMacro('TABLENAME', 'Ee_Pension_Fund_Splits');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Pension Fund Splits');

    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'Count(Autopay_Co_Shifts_Nbr)');
      SetMacro('TABLENAME', 'EE');
      SetMacro('NBRFIELD', 'EE');
      SetParam('RecordNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      editInfo.Lines.Add('Auto Shifts');

    ctx_DataAccess.CUSTOM_VIEW.Close;
  finally
    ctx_EndWait;
  end;
  if editInfo.Lines.Count > 0 then
  begin
    editInfo.Lines.Insert(0, 'Employee details to review for employee #'+ EECode + ', ' + EEName + ', Co #' + CoNbr + ', ' + CoName + ':');
    editInfo.Lines.Insert(1, ' ');
    btnPrint.Enabled := True;
  end;
end;

procedure TRehirePrompt.btnPrintClick(Sender: TObject);
begin
  editInfo.Print(Caption);
  Close;
end;

end.
