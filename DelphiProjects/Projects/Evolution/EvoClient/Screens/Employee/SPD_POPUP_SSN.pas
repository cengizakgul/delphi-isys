// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_POPUP_SSN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, SDataStructure,
  ActnList, ISBasicClasses,  SDDClasses, DB,
  Wwdatsrc, StdCtrls, ExtCtrls, DBCtrls, Mask, wwdbedit, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, EvUtils, Forms, SDataDictclient, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents;

type
  TPOPUP_SSN = class(TForm)
    DM_COMPANY: TDM_COMPANY;
    fpEEList: TisUIFashionPanel;
    grdCLPerson: TevDBGrid;
    btnOk: TevBitBtn;
    btnCancel: TevBitBtn;
    cdClPerson: TevClientDataSet;
    dsClPerson: TevDataSource;
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    function ShowModal: Integer; override;
    { Public declarations }
  end;

implementation

uses
  Dialogs, EvConsts;
{$R *.DFM}

{ TPOPUP_SSN }


function TPOPUP_SSN.ShowModal: Integer;
Begin
  cdClPerson.SkipFieldCheck := True;
  cdClPerson.ProviderName := 'CL_PERSON_PROV';
  cdClPerson.Active := false;
  cdClPerson.DataRequired('ALL');

  Result := inherited ShowModal;

end;

procedure TPOPUP_SSN.btnOkClick(Sender: TObject);
begin

  if (EvMessage('ARE YOU SURE? YOU WILL LOSE DATA FOR THIS EMPLOYEE.  Do you want to continue?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
     ModalResult := mrOk;
end;

end.

