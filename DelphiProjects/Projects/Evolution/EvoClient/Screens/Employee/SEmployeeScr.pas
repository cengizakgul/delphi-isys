// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SEmployeeScr;

interface

uses
  Windows, SPackageEntry, ComCtrls,  StdCtrls,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, Db, Variants,
   Dialogs, EvUtils, SDataStructure, ActnList, EvTypes, Graphics, EvCommonInterfaces,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents, EvContext,
  ISDataAccessComponents, EvMainboard, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEmployeeFrm = class(TFramePackageTmpl, IevEmployeeScreens)
    EDChangeList: TevClientDataSet;
  private
    function CheckEDRefresh: Boolean;
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function DeactivatePackage: Boolean; override;
    function  PackageBitmap: TBitmap; override;
    function InitPackage: Boolean; override;

  public
    procedure ClickButton(Kind: Integer); override;
  end;

implementation

uses SPD_EDChangeForm, SPD_EDIT_EE_BASE, evConsts;

var
  EmployeeFrm: TEmployeeFrm;

{$R *.DFM}

function GetEmployeeScreens: IevEmployeeScreens;
begin
  if not Assigned(EmployeeFrm) then
    EmployeeFrm := TEmployeeFrm.Create(nil);
  Result := EmployeeFrm;

end;

{ TSystemFrm }

function TEmployeeFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
  if Result and not EDChangeList.Active then
  begin
    EDChangeList.CreateDataSet;
    SetEESchedEDChanged('');
  end;
end;

function TEmployeeFrm.CheckEDRefresh: Boolean;
var
  ClNbr: Integer;
  s: string;
  b: Boolean;
  ClId, CoId, PrId: Integer;
  i: Integer;
begin
  Result := EDChangeList.Active;
  if not Result then
    Exit;
  if EDChangeList.RecordCount > 0 then
    with TEDChangeForm.Create(Self) do
    begin
      ctx_StartWait;
      try
        EDChangeList.First;
        ctx_DataAccess.CUSTOM_VIEW.Close;
        while not EDChangeList.Eof do
        begin
          ClNbr := EDChangeList['ClNbr'];
          s := '';
          i := 0;
          while (ClNbr = EDChangeList['ClNbr']) and not EDChangeList.Eof and (i < 1000) do
          begin
            s := s + EDChangeList.FieldByName('EeNbr').AsString + ',';
            EDChangeList.Next;
            Inc(i);
          end;
          Delete(s, Length(s), 1);

          with TExecDSWrapper.Create('GetPayrollForEDRefresh'), ctx_DataAccess.CUSTOM_VIEW do
          begin
            SetMacro('EENbr', s);
            ClientID := ClNbr;
            DataRequest(AsVariant);
            Open;
            if not RDSet.Active then
              RDSet.Data := Data
            else
              RDSet.AppendData(Data, False);
            Close;
          end;
        end;
      finally
        ctx_EndWait;
      end;
      if RDSet.Active and (RDSet.RecordCount > 0) then
        case EvMessage('You''ve changed salary or scheduled E/D information for one or more employees.'#13#10 +
                       'These changes will take effect when any open payrolls are processed. '#13#10 +
                       'Do you want to refresh check lines in appropriate waiting payrolls so that you can see your changes now?'#13#10 +
                       'You''ll be able to choose payrolls from the list, if you press "Yes" button.'#13#10 +
                       'If you refresh check lines that have already been distributed, those check lines will remain as they are, '#13#10 +
                       'and a new check line will be created, so that the E/D will be in the check twice.'#13#10 +
                       'Please manually adjust those checks as needed.', mtConfirmation, mbYesNoCancel) of
          mrYes:
          begin
            ChangeList := EDChangeList;
            b := TEDIT_EE_BASE(ActiveFrame).EEClone.Active;
            if Assigned(ActiveFrame) and (ActiveFrame is TEDIT_EE_BASE) and b then
              TEDIT_EE_BASE(ActiveFrame).EEClone.Close;
            try
              ClId := ctx_DataAccess.ClientID;
              CoId := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
              PrId := 0;
              if DM_PAYROLL.PR.Active then
                PrId := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
              Result := ShowModal <> mrCancel;
              DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;              
              if Result then
              begin
//                ctx_DataAccess.OpenClient(0);
                ctx_DataAccess.OpenClient(ClId);

                DM_COMPANY.CO.Close;
                DM_COMPANY.CO.Activate;
                DM_COMPANY.CO.Locate('CO_NBR', CoId, []);

                if PrId <> 0 then
                begin
                  DM_PAYROLL.PR.Close;
                  DM_PAYROLL.PR.Activate;
                  DM_PAYROLL.PR.Locate('PR_NBR', PrId, []);
//                  PayrollCalculationInterface.OpenDetailPayroll(PrId);
                end;
              end;
            finally
              if Assigned(ActiveFrame) and (ActiveFrame is TEDIT_EE_BASE) and b and DM_EMPLOYEE.EE.Active then
              begin
                s := DM_EMPLOYEE.EE.IndexName;
                try
                  DM_EMPLOYEE.EE.IndexName := '';
                  TEDIT_EE_BASE(ActiveFrame).EEClone.AssignDataFrom(DM_EMPLOYEE.EE);
                finally
                  if s <> '' then
                    DM_EMPLOYEE.EE.IndexName := s;
                end;
              end;
            end;
          end;
          mrCancel:
            Result := False;
          mrNo:
          begin
            if not DM_CLIENT.CL_E_DS.Active then
              DM_CLIENT.CL_E_DS.DataRequired();
            if DM_CLIENT.CL_E_DS.Locate('E_D_CODE_TYPE', ED_OEARN_SALARY, []) then
            begin
              if EDChangeList.Locate('CUSTOM_E_D_CODE_NUMBER', DM_CLIENT.CL_E_DS.CUSTOM_E_D_CODE_NUMBER.Value, []) then
       {         EvMessage('Salary information has changed.  All checks for employees'+
                ' affected by these changes will be updated with the most current employee salary information.',
                mtWarning, [mbOk] );  }
                EvMessage('Salary information has changed. No paid checks will be changed.' +
                ' This will affect checks in waiting and new' +
                ' payrolls for employees whose salary information has been altered.',
                mtWarning, [mbOk] ); 
            end
          end
        end;
      Free;
    end;
  if Result then
  begin
    EDChangeList.EmptyDataSet;
    SetEESchedEDChanged('');
  end;
end;

var
  bWasCommited: Boolean;

procedure TEmployeeFrm.ClickButton(Kind: Integer);
begin
  if Kind = NavOK then
    bWasCommited := False;
  inherited;
  if Kind = NavOK then
  begin
    if EESchedEDChanged <> '' then
    begin
      if not EDChangeList.Locate('ClNbr;EeNbr;CUSTOM_E_D_CODE_NUMBER', VarArrayOf([DM_COMPANY.CO.ClientID, DM_EMPLOYEE.EE['EE_NBR'], EESchedEDChanged]), []) then
        EDChangeList.AppendRecord([DM_COMPANY.CO.ClientID, DM_EMPLOYEE.EE['EE_NBR'], EESchedEDChanged]);
      SetEESchedEDChanged('');
    end;
    if bWasCommited then
      EDChangeList.MergeChangeLog;
  end;
  if (Kind = NavCommit) then
  begin
    EDChangeList.MergeChangeLog;
    bWasCommited := True;
  end;
  if Kind = NavAbort then
    EDChangeList.CancelUpdates;
end;

function TEmployeeFrm.DeactivatePackage: Boolean;
begin
  Result := CanClose and (not FActivated or CheckEDRefresh) and inherited DeactivatePackage;
  if Result then
    EDChangeList.Close;
end;

function TEmployeeFrm.InitPackage: Boolean;
begin
  Result := inherited InitPackage;
end;

function TEmployeeFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TEmployeeFrm.PackageCaption: string;
begin
  Result := '&Employee';
end;

function TEmployeeFrm.PackageSortPosition: string;
begin
  Result := '050';
end;

procedure TEmployeeFrm.UserPackageStructure;
begin
  AddStructure('Employee|010', 'TEDIT_EE_CL_PERSON');
  AddStructure('Employee Quick Entry|015', 'TEE_QUICK_ENTRY');
  AddStructure('Pay rate info|020', 'TEDIT_EE_RATES');
  AddStructure('States|030', 'TEDIT_EE_STATES');
  AddStructure('Locals|040', 'TEDIT_EE_LOCALS');
  AddStructure('Scheduled E/Ds|070', 'TEDIT_EE_SCHEDULED_E_DS');
  AddStructure('Time Off Accrual|080', 'TEDIT_EE_TIME_OFF_ACCRUAL');
  AddStructure('Shifts|090', 'TEDIT_EE_WORK_SHIFTS');
  AddStructure('Auto Labor Distribution|100', 'TEDIT_EE_AUTOLABOR_DISTRIBUTION');
  AddStructure('Pension Fund Splits|110', 'TEDIT_EE_PENSION_FUND_SPLITS');
  AddStructure('Piece Work|120', 'TEDIT_EE_PIECE_WORK');
  AddStructure('Additional info|140', 'TEDIT_EE_ADDITIONAL_INFO');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetEmployeeScreens, IevEmployeeScreens, 'Screens Employee');

finalization
  EmployeeFrm.Free;

end.
