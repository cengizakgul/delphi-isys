inherited EDIT_EE_STATES: TEDIT_EE_STATES
  Width = 622
  Height = 571
  inherited Panel1: TevPanel
    Width = 622
    inherited pnlFavoriteReport: TevPanel
      Left = 470
    end
    inherited pnlTopLeft: TevPanel
      Width = 470
    end
  end
  inherited PageControl1: TevPageControl
    Width = 622
    Height = 484
    HelpContext = 20001
    ActivePage = tshtStates
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 614
        Height = 455
        inherited pnlBorder: TevPanel
          Width = 610
          Height = 451
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 610
          Height = 451
          inherited Panel3: TevPanel
            Width = 562
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 562
            Height = 344
            inherited Splitter1: TevSplitter
              Height = 340
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 340
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 260
                IniAttributes.SectionName = 'TEDIT_EE_STATES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 240
              Height = 340
              inherited wwDBGrid4: TevDBGrid
                Width = 190
                Height = 260
                IniAttributes.SectionName = 'TEDIT_EE_STATES\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtStates: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 614
        Height = 455
        VertScrollBar.Position = 124
        Align = alClient
        TabOrder = 0
        object pnlListofStates: TisUIFashionPanel
          Left = 8
          Top = -116
          Width = 482
          Height = 309
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablHome_State: TevLabel
            Left = 12
            Top = 35
            Width = 65
            Height = 16
            Caption = '~Home State'
            FocusControl = wwlcHome_State
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwlcHome_State: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 121
            Height = 21
            HelpContext = 20004
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'State_Lookup'#9'2'#9'State_Lookup')
            DataField = 'HOME_TAX_EE_STATES_NBR'
            DataSource = wwdsSubMaster
            LookupTable = EEStatesDS
            LookupField = 'EE_STATES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = wwlcHome_StateCloseUp
          end
          object wwdgStates: TevDBGrid
            Left = 12
            Top = 78
            Width = 446
            Height = 209
            DisableThemesInTitle = False
            Selected.Strings = (
              'State_Lookup'#9'17'#9'State'#9'F'
              'STATE_MARITAL_STATUS'#9'28'#9'Marital Status'#9'F'
              'STATE_NUMBER_WITHHOLDING_ALLOW'#9'21'#9'Withholding Allowances'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_STATES\wwdgStates'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnDblClick = wwdgStatesDblClick
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object pnlStateInformation: TisUIFashionPanel
          Left = 8
          Top = 201
          Width = 482
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'State Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablState: TevLabel
            Left = 12
            Top = 35
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = wwlcState
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablMarital_Status: TevLabel
            Left = 141
            Top = 74
            Width = 73
            Height = 16
            Caption = '~Marital Status'
            FocusControl = wwlcMarital_Status
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDependants_Group_Marital_Status: TevLabel
            Left = 141
            Top = 113
            Width = 161
            Height = 13
            AutoSize = False
            Caption = 'Withholding Allowances'
            FocusControl = dedtDependants_Group_Marital_Status
          end
          object lablSDI: TevLabel
            Left = 12
            Top = 74
            Width = 18
            Height = 13
            Caption = 'SDI'
            FocusControl = wwlcSDI
          end
          object lablSUI: TevLabel
            Left = 12
            Top = 113
            Width = 27
            Height = 16
            Caption = '~SUI'
            FocusControl = wwlcSUI
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablReciprocal_Method: TevLabel
            Left = 310
            Top = 74
            Width = 99
            Height = 16
            Caption = '~Reciprocal Method'
            FocusControl = wwcbReciprocal_Method
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablReciprocal_State: TevLabel
            Left = 310
            Top = 113
            Width = 79
            Height = 13
            Caption = 'Reciprocal State'
            FocusControl = wwlcReciprocal_State
          end
          object lablReciprocal_Amount_Percentage: TevLabel
            Left = 310
            Top = 35
            Width = 150
            Height = 13
            Caption = 'Reciprocal Amount/Percentage'
            FocusControl = dedtReciprocal_Amount_Percentage
          end
          object lablCounty: TevLabel
            Left = 141
            Top = 35
            Width = 33
            Height = 13
            Caption = 'County'
          end
          object lablTaxCode: TevLabel
            Left = 12
            Top = 152
            Width = 46
            Height = 13
            Caption = 'Tax Code'
          end
          object evLabel2: TevLabel
            Left = 310
            Top = 152
            Width = 121
            Height = 13
            AutoSize = False
            Caption = 'Salary Type'
          end
          object lablSocCode: TevLabel
            Left = 12
            Top = 192
            Width = 50
            Height = 13
            Caption = 'SOC Code'
          end
          object dedtDependants_Group_Marital_Status: TevDBEdit
            Left = 141
            Top = 128
            Width = 161
            Height = 21
            HelpContext = 20016
            DataField = 'STATE_NUMBER_WITHHOLDING_ALLOW'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtReciprocal_Amount_Percentage: TevDBEdit
            Left = 310
            Top = 50
            Width = 151
            Height = 21
            HelpContext = 20008
            DataField = 'RECIPROCAL_AMOUNT_PERCENTAGE'
            DataSource = wwdsDetail
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcState: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 121
            Height = 21
            HelpContext = 20005
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = wwlcStateChange
            OnCloseUp = wwlcStateCloseUp
          end
          object wwlcSDI: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 121
            Height = 21
            HelpContext = 20013
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SDI_APPLY_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcSUI: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 121
            Height = 21
            HelpContext = 20014
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SUI_APPLY_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcReciprocal_State: TevDBLookupCombo
            Left = 310
            Top = 128
            Width = 151
            Height = 21
            HelpContext = 20010
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'RECIPROCAL_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 11
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwcbReciprocal_Method: TevDBComboBox
            Left = 310
            Top = 89
            Width = 151
            Height = 21
            HelpContext = 20009
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'RECIPROCAL_METHOD'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
          end
          object wwlcMarital_Status: TevDBLookupCombo
            Left = 141
            Top = 89
            Width = 161
            Height = 21
            HelpContext = 20006
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATUS_TYPE'#9'6'#9'STATUS_TYPE'#9'F'
              'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION')
            DataField = 'STATE_MARITAL_STATUS'
            DataSource = wwdsDetail
            LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
            LookupField = 'STATUS_TYPE'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnBeforeDropDown = wwlcMarital_StatusBeforeDropDown
            OnCloseUp = wwlcMarital_StatusCloseUp
          end
          object dedtCounty: TevDBEdit
            Left = 141
            Top = 50
            Width = 161
            Height = 21
            HelpContext = 20011
            DataField = 'EE_COUNTY'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*[&[*?]*[{#, ,.,;,,;;,:,;[,;],(,),}]]'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtTaxCode: TevDBEdit
            Left = 12
            Top = 167
            Width = 121
            Height = 21
            HelpContext = 20015
            DataField = 'EE_TAX_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*[{&,#}[*{?,#}]*[{#, ,.,;,,;;,:,;[,;],(,),},-,/,\]]'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 141
            Top = 152
            Width = 161
            Height = 36
            Caption = '~Import Marital Status'
            Columns = 2
            DataField = 'IMPORTED_MARITAL_STATUS'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Single'
              'Married')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'S'
              'M')
          end
          object edSalaryType: TevDBComboBox
            Left = 310
            Top = 167
            Width = 151
            Height = 21
            HelpContext = 20009
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SALARY_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 12
            UnboundDataType = wwDefault
          end
          object dedtSocCode: TevDBEdit
            Left = 12
            Top = 207
            Width = 121
            Height = 21
            HelpContext = 20015
            DataField = 'SOCCodeMask'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*2{#,x}-*4{&,#}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
      end
    end
    object tshtTax_Overrides: TTabSheet
      Caption = 'Overrides'
      ImageIndex = 11
      object sbOverrides: TScrollBox
        Left = 0
        Top = 0
        Width = 614
        Height = 455
        Align = alClient
        TabOrder = 0
        object fpOverrides: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 290
          Height = 170
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Overrides'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablOverride_State_Tax_Type: TevLabel
            Left = 12
            Top = 35
            Width = 125
            Height = 16
            Caption = '~Override State Tax Type'
            FocusControl = wwdbOverride_State_Tax_Type
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablOverride_State_Tax_Value: TevLabel
            Left = 12
            Top = 74
            Width = 119
            Height = 13
            Caption = 'Override State Tax Value'
            FocusControl = dedtOverride_State_Tax_Value
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 113
            Width = 144
            Height = 13
            Caption = 'Override State Minimum Wage'
            FocusControl = evDBEdit1
          end
          object wwdbOverride_State_Tax_Type: TevDBComboBox
            Left = 12
            Top = 50
            Width = 258
            Height = 21
            HelpContext = 20003
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'OVERRIDE_STATE_TAX_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDBEdit1: TevDBEdit
            Left = 12
            Top = 128
            Width = 258
            Height = 21
            HelpContext = 20008
            DataField = 'OVERRIDE_STATE_MINIMUM_WAGE'
            DataSource = wwdsDetail
            PopupMenu = pmOverrideStateMinWage
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtOverride_State_Tax_Value: TevDBEdit
            Left = 12
            Top = 89
            Width = 258
            Height = 21
            HelpContext = 20003
            DataField = 'OVERRIDE_STATE_TAX_VALUE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object pfSettings: TisUIFashionPanel
          Left = 308
          Top = 8
          Width = 290
          Height = 286
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel7: TevLabel
            Left = 12
            Top = 35
            Width = 255
            Height = 13
            Caption = 'Use Effective Dates to Change Tax Statuses'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object drgpEE_SDI_Exempt_Exclude: TevDBRadioGroup
            Left = 12
            Top = 100
            Width = 258
            Height = 39
            HelpContext = 20003
            Caption = '~EE SDI'
            Columns = 3
            DataField = 'EE_SDI_EXEMPT_EXCLUDE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpState_Exempt_ExcludeChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpEE_SUI_Exempt_Exclude: TevDBRadioGroup
            Left = 12
            Top = 142
            Width = 258
            Height = 39
            HelpContext = 20003
            Caption = '~EE SUI'
            Columns = 3
            DataField = 'EE_SUI_EXEMPT_EXCLUDE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpState_Exempt_ExcludeChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpER_SDI_Exempt_Exclude: TevDBRadioGroup
            Left = 12
            Top = 184
            Width = 258
            Height = 39
            HelpContext = 20003
            Caption = '~ER SDI'
            Columns = 3
            DataField = 'ER_SDI_EXEMPT_EXCLUDE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpState_Exempt_ExcludeChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpER_SUI_Exempt_Exclude: TevDBRadioGroup
            Left = 12
            Top = 226
            Width = 258
            Height = 39
            HelpContext = 20003
            Caption = '~ER SUI'
            Columns = 3
            DataField = 'ER_SUI_EXEMPT_EXCLUDE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpState_Exempt_ExcludeChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpState_Exempt_Exclude: TevDBRadioGroup
            Left = 12
            Top = 58
            Width = 258
            Height = 39
            HelpContext = 20003
            Caption = '~State Exempt or Block'
            Columns = 3
            DataField = 'STATE_EXEMPT_EXCLUDE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpState_Exempt_ExcludeChange
            AutoPopupEffectiveDateDialog = True
          end
        end
        object fp1099: TisUIFashionPanel
          Left = 8
          Top = 186
          Width = 290
          Height = 108
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fp1099'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = '1099'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 9
            Top = 35
            Width = 258
            Height = 39
            HelpContext = 20003
            Caption = '~Calculate SUI Taxable Wages'
            Columns = 2
            DataField = 'CALCULATE_TAXABLE_WAGES_1099'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpState_Exempt_ExcludeChange
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 622
  end
  inherited wwdsMaster: TevDataSource
    Left = 936
    Top = 186
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_STATES.EE_STATES
    OnDataChange = wwdsDetailDataChange
    Left = 654
    Top = 242
  end
  inherited wwdsList: TevDataSource
    Left = 826
    Top = 202
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 697
    Top = 159
  end
  inherited PageControlImages: TevImageList
    Left = 400
    Top = 80
  end
  inherited dsCL: TevDataSource
    Left = 631
    Top = 63
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 656
    Top = 15
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 666
    Top = 122
  end
  inherited wwdsEmployee: TevDataSource
    Left = 686
    Top = 152
  end
  inherited DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 588
    Top = 66
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 678
    Top = 189
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 693
    Top = 58
  end
  inherited ActionList: TevActionList
    Left = 723
    Top = 235
  end
  object EEStatesDS: TevClientDataSet
    Left = 331
    Top = 43
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 497
    Top = 43
  end
  object SYStatesSrc: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 879
    Top = 211
  end
  object SYMaritalSrc: TevDataSource
    DataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
    MasterDataSource = SYStatesSrc
    Left = 670
    Top = 112
  end
  object evSecElement1: TevSecElement
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 384
    Top = 8
  end
  object pmOverrideStateMinWage: TevPopupMenu
    Left = 724
    Top = 138
    object StateMinWageCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = StateMinWageCopyToClick
    end
  end
end
