inherited AskEDUpdateParamsForAmount: TAskEDUpdateParamsForAmount
  Left = 521
  Top = 197
  Width = 336
  Height = 189
  Caption = 'Update E/D Amount'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Width = 320
    Height = 99
    object evGroupBox1: TevGroupBox
      Left = 8
      Top = 8
      Width = 313
      Height = 97
      Caption = 'Update Mode'
      TabOrder = 0
      object evLabel1: TevLabel
        Left = 166
        Top = 28
        Width = 6
        Height = 13
        Alignment = taRightJustify
        Caption = '$'
      end
      object evLabel2: TevLabel
        Left = 165
        Top = 61
        Width = 8
        Height = 13
        Alignment = taRightJustify
        Caption = '%'
      end
      object evRadioButton1: TevRadioButton
        Left = 16
        Top = 27
        Width = 129
        Height = 17
        Caption = 'Change to an Amount'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rgClick
      end
      object evRadioButton2: TevRadioButton
        Left = 16
        Top = 59
        Width = 137
        Height = 17
        Caption = 'Change by a Percent'
        TabOrder = 1
        OnClick = rgClick
      end
      object evEdit1: TevEdit
        Left = 176
        Top = 24
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object evEdit2: TevEdit
        Left = 176
        Top = 56
        Width = 121
        Height = 21
        TabOrder = 3
      end
    end
  end
  inherited evPanel2: TevPanel
    Top = 111
    Width = 320
    inherited evPanel4: TevPanel
      Left = 134
      inherited evBitBtn2: TevBitBtn
        ModalResult = 1
      end
    end
  end
  inherited evPanel3: TevPanel
    Top = 99
    Width = 320
    inherited EvBevel1: TEvBevel
      Width = 311
    end
  end
  inherited evActionList1: TevActionList
    inherited OK: TAction
      OnUpdate = OKUpdate
    end
  end
end
