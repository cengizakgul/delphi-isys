unit SDBDT;

interface
uses
  SPD_DBDTSelectionListFiltr,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_EE_BASE, SDataStructure,
  EvSecElement, ExtDlgs, DB, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents,  DBActns,
  ActnList, ISBasicClasses, SDDClasses, Wwdatsrc, SPD_HideRecordHelper,
  StdCtrls, SPD_PersonDocuments, DBCtrls, wwdbdatetimepicker, wwdblook,
  ExtCtrls, Wwdotdot, Wwdbcomb, Mask, wwdbedit, Buttons, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, EvClientDataSet;

type
  TDBDT = class(TObject)
  private
    FIsValidDBDT: boolean;
    FSelectionListFiltr: TDBDTSelectionListFiltr;
    FDivDataSet, FBrchDataSet, FDeptDataSet, FTeamDataSet: TevClientDataSet;
//    FDivValue, FBrchValue, FDeptValue, FTeamValue: Variant;
    FDBDTLevel: Char;
    FEdit: TCustomEdit;
    FOriginalEditOnChange: TNotifyEvent;
    FDBDT: string;
    FDivisionNbr, FBranchNbr, FDepartmentNbr, FTeamNbr: Variant;
  protected
    procedure EditOnChange(Sender: TObject);
  public
    constructor Create(AEdit: TCustomEdit = nil);
    destructor Destroy; override;
    procedure InitSelectionFilter(ADivDataSet, ABrchDataSet, ADeptDataSet,
      ATeamDataSet: TevClientDataSet; ADivValue, ABrchValue, ADeptValue,
      ATeamValue: Variant; ADBDTLevel: Char);
    property SelectionListFiltr: TDBDTSelectionListFiltr read FSelectionListFiltr;
    property Edit: TCustomEdit read FEdit;
    function ShowModal: TModalResult;
    property IsValidDBDT: boolean read FIsValidDBDT;
    property DBDT: string read FDBDT;
    property DivisionNbr: variant read FDivisionNbr;
    property BranchNbr: variant read FBranchNbr;
    property DepartmentNbr: variant read FDepartmentNbr;
    property TeamNbr: Variant read FTeamNbr;
  end;

implementation

uses
  evConsts;

type
  TUnprotectedCustomEdit = class(TCustomEdit)
  protected
    function GetOnChange: TNotifyEvent;
    procedure SetOnChange(const Value: TNotifyEvent);
  public
    property OnChange: TNotifyEvent read GetOnChange write SetOnChange;
  end;

constructor TDBDT.Create(AEdit: TCustomEdit = nil);
begin
  inherited Create;
  FEdit := AEdit;
  if Assigned(FEdit) then
  begin
    FOriginalEditOnChange := TUnprotectedCustomEdit(FEdit).OnChange;
    TUnprotectedCustomEdit(FEdit).OnChange := EditOnChange;
  end
  else
  begin
    FOriginalEditOnChange := nil;
  end;
end;

destructor TDBDT.Destroy;
begin
  if Assigned(FSelectionListFiltr) then
  begin
    FSelectionListFiltr.Release;
    FSelectionListFiltr := nil;
  end;
  inherited Destroy;
end;

procedure TDBDT.InitSelectionFilter(ADivDataSet, ABrchDataSet,
  ADeptDataSet, ATeamDataSet: TevClientDataSet; ADivValue, ABrchValue,
  ADeptValue, ATeamValue: Variant; ADBDTLevel: Char);
begin
  if Assigned(FSelectionListFiltr) then
  begin
    FSelectionListFiltr.Release;
    FSelectionListFiltr := nil;
  end;
  FDivDataSet := ADivDataSet;
  FBrchDataSet := ABrchDataSet;
  FDeptDataSet := ADeptDataSet;
  FTeamDataSet := ATeamDataSet;
  FDBDTLevel := ADBDTLevel;
  FSelectionListFiltr := TDBDTSelectionListFiltr.Create(nil);
  FSelectionListFiltr.Setup(FDivDataSet, FBrchDataSet, FDeptDataSet,
    FTeamDataSet, ADivValue, ABrchValue, ADeptValue, ATeamValue, FDBDTLevel);
end;

function TUnprotectedCustomEdit.GetOnChange: TNotifyEvent;
begin
  Result := inherited OnChange;
end;

procedure TUnprotectedCustomEdit.SetOnChange(const Value: TNotifyEvent);
begin
  inherited OnChange := Value;
end;

// Ways to select dbdt:

function TDBDT.ShowModal: TModalResult;
// 1: user clicks the button
begin
  SelectionListFiltr.edtFiltr.Text := '';
  Result := SelectionListFiltr.ShowModal;
  if Result = mrOK then
  begin
    FIsValidDBDT := True;
    FDBDT := SelectionListFiltr.wwcsTempDBDTDBDT_CUSTOM_NUMBERS.AsString;
    FDivisionNbr := SelectionListFiltr.wwcsTempDBDTDIVISION_NBR.AsVariant;
    FBranchNbr := SelectionListFiltr.wwcsTempDBDTBRANCH_NBR.AsVariant;
    FDepartmentNbr := SelectionListFiltr.wwcsTempDBDTDEPARTMENT_NBR.AsVariant;
    FTeamNbr := SelectionListFiltr.wwcsTempDBDTTEAM_NBR.AsVariant;
    if Assigned(Edit) then
      Edit.Text := DBDT;
  end;
end;

procedure TDBDT.EditOnChange(Sender: TObject);
// 2: user types in the edit box
begin
  SelectionListFiltr.edtFiltr.Text :=
    StringReplace(Edit.Text, ' | ', '', [rfReplaceAll, rfIgnoreCase]);
  if (SelectionListFiltr.wwcsTempDBDT.RecordCount < 1) or
    (Trim(Edit.Text) = '') then
  begin
    // Invalid input
    FIsValidDBDT := False;
    FDBDT := '';
    FDivisionNbr := Null;
    FBranchNbr := Null;
    FDepartmentNbr := Null;
    FTeamNbr := Null;
    Beep;
  end
  else
  begin
    // Select first valid choice
    FIsValidDBDT := True;
    FDBDT := SelectionListFiltr.wwcsTempDBDTDBDT_CUSTOM_NUMBERS.AsString;
    FDivisionNbr := SelectionListFiltr.wwcsTempDBDTDIVISION_NBR.AsVariant;
    FBranchNbr := SelectionListFiltr.wwcsTempDBDTBRANCH_NBR.AsVariant;
    FDepartmentNbr := SelectionListFiltr.wwcsTempDBDTDEPARTMENT_NBR.AsVariant;
    FTeamNbr := SelectionListFiltr.wwcsTempDBDTTEAM_NBR.AsVariant;
  end;
  if Assigned(FOriginalEditOnChange) then
    FOriginalEditOnChange(Sender);
end;

end.

