inherited EDIT_EE_CL_PERSON: TEDIT_EE_CL_PERSON
  Width = 1199
  Height = 750
  AutoScroll = False
  object evLabel3: TevLabel [0]
    Left = 16
    Top = 5
    Width = 3
    Height = 13
    Visible = False
  end
  object evLabel1: TevLabel [1]
    Left = 416
    Top = 472
    Width = 50
    Height = 13
    Caption = 'Pay Group'
  end
  inherited Panel1: TevPanel
    Width = 1199
    Height = 51
    TabOrder = 2
    inherited pnlFavoriteReport: TevPanel
      Left = 1033
      Width = 166
      Height = 51
      inherited spbFavoriteReports: TevSpeedButton
        Left = 62
        HideHint = False
      end
      inherited btnSwipeClock: TevSpeedButton
        Left = 94
        HideHint = False
      end
      inherited BtnMRCLogIn: TevSpeedButton
        Left = 126
        HideHint = False
      end
      inherited evLabel47: TevLabel
        Width = 162
      end
      object evSpeedButton1: TevSpeedButton
        Left = 8
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Tax and Check Line Calculator (F9)'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = evSpeedButton1Click
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFDCDCDC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCD3D3D3FFFFFFFFFFFFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
          CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCD4D4D4FFFFFFFFFFFFB0B0B0
          9F9F9F9E9E9E9E9E9E9E9F9F9E9E9E9E9E9E9E9F9F9E9E9E9D9E9F9D9E9F9D9E
          9E9F9F9FAAAAAAFFFFFFFFFFFFB1B0B0A09F9F9F9F9E9F9F9EA09F9F9F9F9E9F
          9F9EA09F9F9F9F9E9F9F9E9F9F9E9F9F9EA09F9FAAAAAAFFFFFFFFFFFF9F9F9F
          FFFEFEFBFAFAFEFDFDFEFDFDFDFCFCFEFDFDFEFDFEFBFCFDF9FCFFF9FCFFF9FA
          FCFFFEFE9F9F9FFFFFFFFFFFFFA09F9FFFFFFFFCFCFBFEFEFEFEFEFEFEFDFDFE
          FEFEFFFFFFFEFDFDFEFEFEFEFEFEFCFCFBFFFFFFA09F9FFFFFFFFFFFFF9E9E9E
          F7F6F6F2F1F08D8A886D6B68F7F6F58D8B896C6B69F2F6F9E49F55E49F55EEF2
          F5F7F6F79E9E9EFFFFFFFFFFFF9F9F9EF7F7F7F3F2F28A8A8A6A6A6AF7F7F78B
          8B8B6B6B6BF8F8F8999999999999F4F4F4F8F8F89F9F9EFFFFFFFFFFFF9D9E9E
          F4F3F2EDECEBF2F1F0F3F3F2F0F0EFF3F2F1F3F3F3EDF1F6EEAD66E4A25BE9ED
          F2F4F4F39D9E9EFFFFFFFFFFFF9F9F9EF4F4F4EDEDEDF3F2F2F4F4F4F1F1F1F3
          F3F3F4F4F4F3F3F3A7A7A79C9C9CF0EFEFF5F5F59F9F9EFFFFFFFFFFFF9E9E9E
          F3F2F0E9E8E78E8C8A6E6C6AEDECEB8F8D8A6E6C6BE9ECF0F0AC62F0AC62E5E8
          ECF2F2F19E9E9EFFFFFFFFFFFF9F9F9EF3F3F3E9E9E98C8C8C6C6C6CEDEDED8D
          8D8D6C6C6CEEEEEEA6A6A6A6A6A6EAEAEAF3F3F39F9F9EFFFFFFFFFFFF9E9E9E
          F0EEEEE4E2E2E9E7E7EAE8E9E8E6E6E9E7E8EAE8E9E6E5E7E5E6EBE4E6EBE2E2
          E4F0EEEE9E9E9EFFFFFFFFFFFF9F9F9EF0EFEFE4E3E3E8E8E8EAEAEAE8E7E7E8
          E8E8EAEAEAE7E7E6E8E8E8E8E8E8E4E3E3F0EFEF9F9F9EFFFFFFFFFFFF9E9F9F
          EEEDEDE0DEDD918E8A716F6BE5E2E2918F8C706E6CE4E2E2918F8C706E6AE1DF
          DEEEEDEE9E9F9FFFFFFFFFFFFFA09F9FEFEEEEE0DFDF8E8E8E6E6E6EE4E3E38F
          8F8F6D6D6DE4E3E38F8F8F6D6D6DE0E0E0EFEEEEA09F9FFFFFFFFFFFFF9F9F9F
          EEECECDDD9D5E4DED6E5DFD7E0DDD9E0DEDDE1DFDEE1DDD9E5DFD7E5DFD6DDD9
          D5EEEDEC9F9F9FFFFFFFFFFFFFA09F9FEDEDEDDADADADEDEDEDFDFDEDEDEDEE0
          DFDFE0E0E0DEDEDEDFDFDEDFDFDEDADADAEEEEEEA09F9FFFFFFFFFFFFF9F9F9F
          EDECEADAD5CE1F7FFF2266FFDDD8D1928F8C716F6CDED8D22080FF2265FFDAD5
          CEEEECEA9F9F9FFFFFFFFFFFFFA09F9FEDEDEDD5D5D5A6A6A69A9A9AD8D8D88F
          8F8F6E6E6ED8D8D8A6A6A6999999D5D5D5EDEDEDA09F9FFFFFFFFFFFFFA0A1A1
          F1F0F0F1EFEDF6F1ECF6F2ECF2F1EFF1F1F2F2F2F3F2F1EFF6F2ECF6F2ECF1F0
          EDF1F0F0A0A1A1FFFFFFFFFFFFA1A1A1F2F2F1F0F0F0F2F2F1F3F2F2F2F2F1F3
          F2F2F3F3F3F2F2F1F3F2F2F3F2F2F1F1F1F2F2F1A1A1A1FFFFFFFFFFFFA2A3A5
          7C7E7F7B7D807C7E807C7E817B7E817B7E827B7E827B7E817C7E817C7E807B7D
          807C7E7FA2A3A5FFFFFFFFFFFFA4A4A47F7E7E7E7E7D7F7E7E7F7E7E7F7E7E80
          807F80807F7F7E7E7F7E7E7F7E7E7E7E7D7F7E7EA4A4A4FFFFFFFFFFFFA0A3A7
          FFCA8BF9C27EF9C27FF9C280F9C280F9C280F9C280F9C280F9C280F9C27FF9C2
          7EFFCA8BA0A3A7FFFFFFFFFFFFA4A4A4C4C3C3BBBBBBBBBBBBBBBBBBBBBBBBBB
          BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC4C3C3A4A4A4FFFFFFFFFFFF9EA2A6
          F9D0A4E8A760E7A762E7A863E7A863E7A863E7A863E7A863E7A863E7A762E8A7
          60F9D0A49EA2A6FFFFFFFFFFFFA3A3A3CBCBCBA1A0A0A1A0A0A1A1A1A1A1A1A1
          A1A1A1A1A1A1A1A1A1A1A1A1A0A0A1A0A0CBCBCBA3A3A3FFFFFFFFFFFF9FA2A5
          FDDBB6F8DAB8F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DA
          B8FDDBB69FA2A5FFFFFFFFFFFFA3A3A3D7D7D7D7D6D6D7D6D6D7D6D6D7D6D6D7
          D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D7D7A3A3A3FFFFFFFFFFFFA9AAAB
          9FA2A59EA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59EA1
          A59FA2A5A9AAABFFFFFFFFFFFFABABABA3A3A3A2A2A2A2A2A2A2A2A2A2A2A2A2
          A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A3A3A3ABABABFFFFFF}
        ParentColor = False
        ShortCut = 120
      end
      object evSpeedButton2: TevSpeedButton
        Left = 35
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Check Finder (F12)'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = evSpeedButton2Click
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCC
          CCCCCCCCCCCCCCF4F4F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCCDCCCCCDCCCCF6F6F5D0D0D0CCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC4880
          AC4980AA4982ACB0BDC7D1D1D1CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
          CCCCCDCCCCCDCCCCCDCCCCCDCCCC8989898989898B8B8BBFBFBFE9BB7DE9B773
          E9B672E9B672E9B672EBB772EFBA74F2BD76F4BF77F4BF76FFC273377EB6739E
          BF21ACFF94CEEB4591C6B4B4B4B0AFAFAFAFAFAFAFAFAFAFAFB0AFAFB2B2B2B5
          B5B5B8B7B7B7B6B6BABABA8B8B8BA5A5A5C0C0C0D3D3D39D9D9DE9B773FFEDCD
          FFE8C5FFE8C5FFEBC7FFF0CBC0B4A37B7C7E7375797375797777798E8985B0A6
          9E9EC1D7AEE9FF4091C6B0AFAFE9E9E9E4E3E3E4E3E3E6E6E6EBEBEAB2B2B27D
          7C7C777676777676787877898989A6A6A6C5C5C5ECECEC9D9D9DE9B671FFE9CA
          FFF2E1FFF3E2FFE9BE8E8983979798D6D6DAE1E3E7E1E2E6D7D6D99C9A998A85
          82F2ECE7BCDDEA4092CAAFAFAFE5E5E5F0F0F0F2F2F1E3E3E2888888989898D8
          D8D8E5E5E5E4E4E4D8D8D89A9A9A868585ECECECE0DFDF9F9F9EE9B671FFE9CC
          FFF1DFFFF5E2C0AF999B9B9DE6E6E6E8C790F1CC81F7DA8EF5E1A8E7E7E69E9C
          9BB5AEA83891CBFFFFFFAFAFAFE6E6E6F0EFEFF3F3F3ACACAC9C9C9CE8E7E7C0
          C0C0C3C2C2CFCFCFD8D8D8E8E8E89D9D9DAEAEAE9E9E9EFFFFFFE9B671FFECD0
          FFDCB1FFE3B5838182E0E2E5E6BF8BEECA8CF1D48EF9E59EFFEEA5F5E3A9E0E0
          E48B827CFFFFFFFFFFFFAFAFAFE8E8E8D7D7D7DDDDDD828181E4E3E3B9B9B9C2
          C2C1CACACADBDADAE3E3E2DADADAE2E2E2828181FFFFFFFFFFFFE9B671FFECD3
          FFDAACFFE1B17C7D7FF2F5F9E4B675F3DDB7F3DAA3F6DF99F9E59EF6D98DF2F3
          F8888582FFFFFFFFFFFFAFAFAFE9E9E9D5D5D5DBDADA7E7E7DF7F7F7AFAFAFD8
          D8D8D3D2D2D5D5D5DBDADACECECEF6F6F5868585FFFFFFFFFFFFE9B670FFECD5
          FFEDD8FFF4DD807F7FF8FBFFE3B377F7E9D2F4DEB7F3DAA2F2D48FEFCA7FF8FA
          FE8A8785FFFFFFFFFFFFAFAFAFEAEAEAEBEBEAF1F1F180807FFEFDFDACACACE7
          E7E6D9D9D9D2D2D1CACACAC0C0C0FCFCFB878787FFFFFFFFFFFFE9B670FFEFDA
          FFECD5FFF2DA8A8784EEF2F5E3B682FAEFE2F7E8D1F3DCB5EDCB90EDCB94EFF2
          F6928C88FFFFFFFFFFFFAFAFAFEDEDEDEAEAEAF0EFEF878787F4F4F4B1B0B0EE
          EEEEE5E5E5D7D6D6C4C3C3C4C4C4F4F4F48C8C8CFFFFFFFFFFFFE9B670FFF1E1
          FFD3A1FFD7A3C7BCB0AEAEAFFFFFFFE3B782E5BA85E6BE87EAC591FFFFFFAEAF
          B2C1A580FFFFFFFFFFFFAFAFAFF0EFEFCECDCDD1D1D1BBBBBBAFAFAFFFFFFFB2
          B1B1B4B4B4B8B7B7BEBEBEFFFFFFB1B0B0A1A0A0FFFFFFFFFFFFE9B56FFFF3E7
          FFD09BFFD29EFFF1D8A09D9AB1B1B4FAFCFFFFFFFFFFFFFFF9FBFFB1B1B2A1A0
          A0F2BB72FFFFFFFFFFFFADADADF3F2F2CACACACDCCCCEEEEEE9D9D9DB2B2B2FE
          FEFEFFFFFFFFFFFFFEFDFDB2B2B2A1A0A0B3B3B3FFFFFFFFFFFFE9B56FFFF5EA
          FFE8CEFFEAD2FFCF99FFCE9ACABEB19391908C8C8E8C8C8E939190CABBADFFFA
          EDEBB770FFFFFFFFFFFFADADADF5F5F5E5E5E5E8E7E7CAC9C9C9C8C8BDBCBC92
          92928D8D8D8D8D8D929292BABABAF9F9F8B0AFAFFFFFFFFFFFFFE9B56FFFF8F0
          FFE4C8FFE6CCFFCA92FFCB93FFEACEFFECCFFFCF93FFCF93FFEBCEFFE8CAFFF9
          F1E9B56FFFFFFFFFFFFFADADADF8F8F8E1E1E1E4E3E3C4C4C4C5C5C5E7E7E6E8
          E8E8C9C8C8C9C8C8E8E7E7E4E4E4F9F9F8ADADADFFFFFFFFFFFFE9B670FFFDFC
          FFFAF6FFFBF7FFFCF9FFFCF9FFFBF7FFFBF7FFFCF9FFFCF9FFFBF7FFFBF6FFFE
          FCE9B670FFFFFFFFFFFFAFAFAFFEFEFEFBFBFBFCFCFBFDFDFCFDFDFCFCFCFBFC
          FCFBFDFDFCFDFDFCFCFCFBFCFCFBFFFFFFAFAFAFFFFFFFFFFFFFEDBF81E9B670
          E9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B6
          70EDBF81FFFFFFFFFFFFB8B7B7AFAFAFADADADADADADADADADADADADADADADAD
          ADADADADADADADADADADADADADADAFAFAFB8B7B7FFFFFFFFFFFF}
        ParentColor = False
        ShortCut = 123
      end
    end
    inherited pnlTopLeft: TevPanel
      Width = 1033
      Height = 51
      inherited CompanyNameText: TevDBText
        Width = 110
        Height = 15
        AutoSize = True
      end
    end
  end
  inherited PageControl1: TevPageControl
    Top = 84
    Width = 1199
    Height = 666
    HelpContext = 17781
    TabOrder = 0
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1191
        Height = 637
        inherited pnlBorder: TevPanel
          Width = 1187
          Height = 633
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1187
          Height = 633
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 0
              Width = 800
              BevelInner = bvLowered
              BevelOuter = bvLowered
              ParentColor = True
            end
            inherited pnlSubbrowse: TevPanel
              Left = 385
              Top = 76
            end
          end
          inherited Panel3: TevPanel
            Width = 1139
            inherited BitBtn1: TevBitBtn
              Width = 102
            end
            inherited bOpenFilteredByDBDTG: TevBitBtn
              Left = 110
              Width = 147
              TabOrder = 3
            end
            inherited bOpenFilteredByEe: TevBitBtn
              Left = 265
              Width = 147
              TabOrder = 1
            end
            inherited cbShowRemovedEmployees: TevCheckBox
              Left = 420
              Width = 93
              Caption = 'Removed EEs'
              TabOrder = 2
            end
            object evPanel1: TevPanel
              Left = 1032
              Top = 0
              Width = 107
              Height = 35
              Align = alRight
              Alignment = taRightJustify
              BevelOuter = bvNone
              Color = 14737632
              TabOrder = 4
              object evBitBtn1: TevBitBtn
                Left = 13
                Top = 0
                Width = 92
                Height = 25
                Caption = 'Send E-mail'
                TabOrder = 0
                OnClick = evBitBtn1Click
                Color = clBlack
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDD
                  DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFF4F4F4
                  F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F8F8F800884700C583439E
                  73DFDFDFF5F5F5FFFFFFFFFFFFF6F6F5F7F6F6F7F6F6F7F6F6F7F6F6F7F6F6F7
                  F6F6F7F6F6FAF9F97A7979B4B4B4939393E0E0E0F7F6F6FFFFFFD0D0D0CDC4B8
                  CCC4B9CCC4B9CCC4B9CEC4B9CFC4B9D0C4BAD2C6BCD2C7BF00864500E4A500BE
                  804A9F76D3C5BAD0D0D0D1D1D1C3C2C2C4C3C3C4C3C3C4C3C3C4C3C3C4C3C3C4
                  C3C3C6C6C6C7C7C7787877D3D2D2AEAEAE949393C4C4C4D1D1D1CDA671CCA066
                  CA9E63CA9E63CC9E63D8A1673D925300864400864400864400824100D9A100D8
                  A000BB7F4A9253E5AA76A09F9F9999999797979797979797979C9C9C83838378
                  7877787877787877747373C9C8C8C8C8C7ABABAB858484A6A6A6CCA066FFFFF5
                  FFFFEDFFFEEBFFFFECFFFFF300813B3AE7C000D7A000D6A000D59F00D09C00D0
                  9C00D39F00B77E4D9456999999FEFEFEFDFDFCFBFBFBFDFDFCFEFEFE727171DA
                  DADAC7C7C7C6C6C6C5C5C5C1C1C1C1C1C1C4C3C3A8A8A7868686CA9E63FFFEED
                  E6CAA1F7E4C5FFF7E1FFFCE8007E376AE6CE00C89900C89900C89900C79700C8
                  9800CA9A5CE5CC008845979797FCFCFBC5C5C5E0E0E0F4F4F4FAF9F96F6F6FDD
                  DDDDBABABABABABABABABAB8B8B8BABABABCBCBBDCDBDB797979CA9E63FFFBE9
                  FFFBEFDAB684E6C599FFFAE3007E3790ECDF4AE7D24EE7D24DE6D193E8D800C2
                  975BDEC400AF7C4C9355979797F9F9F8FAFAFAB0AFAFBFBFBFF7F7F76F6F6FE6
                  E6E6DEDEDEDEDEDEDDDDDDE2E2E2B5B5B5D4D4D4A1A0A0868585CA9E63FFF9E8
                  FFEFD0FFFDF5DDB684FFF8DD3E9456007E35007E3600803900803880E3D554D8
                  C200A7765AB281E4A46B979797F7F7F7EBEBEAFDFDFCB1B0B0F4F4F48685856E
                  6E6E6E6E6E717070717070DCDCDCCFCFCF999999A5A5A5A09F9FCA9E63FFFAE8
                  FFEBCAFFECCBFFFFFEE9C395E9BC8CFFFFFFFFFFFFFFC29300813A70E1D400A3
                  7655A971FFFFF2D2A166979797F8F8F8E7E7E6E8E7E7FFFFFFBEBDBDB7B6B6FF
                  FFFFFFFFFFBEBEBE727171DBDADA9796969B9B9BFEFDFD9B9B9BCA9E63FFFAEA
                  FFE9C5FFEAC7EBCB9EDCB784FFFFFFFFEFD0FFEFD1FFFFFF007C3300A07852A8
                  6FFFF0CEFFFCEDCC9F63979797F8F8F8E4E4E4E5E5E5C5C5C5B1B0B0FFFFFFEB
                  EBEAECEBEBFFFFFF6C6C6C9493939A9A9AECEBEBFAFAFA989898CA9E63FFFBEC
                  FFE8C1F6D9AFDEBA87FFFFFAFFE9C7FFE9C6FFE9C6FFEBC9FFFFFEF3BE8CFFDD
                  B4FFEAC3FFFCEDCA9E63979797FAF9F9E3E3E2D4D4D4B4B4B4FFFFFFE5E5E5E4
                  E4E4E4E4E4E7E7E6FFFFFFB9B9B9D8D8D8E5E5E5FAFAFA979797CA9E63FFFCF1
                  FEE1B5DEB682FFFBEFFFE6BDFFE6BDFFE6BEFFE6BEFFE6BDFFE6BEFFFCF0DFB7
                  83FFE1B6FFFCF1CA9E63979797FBFBFBDCDBDBB0AFAFFAFAFAE0E0E0E0E0E0E1
                  E1E1E1E1E1E0E0E0E1E1E1FBFBFBB1B0B0DCDBDBFBFBFB979797CA9F63FFFEF4
                  E6BE88FFF5E2FFE1B2FFE0B2FFE1B3FFE1B3FFE1B3FFE1B3FFE0B2FFE1B2FFF5
                  E2E6BE88FFFEF4CA9F63989898FEFDFDB8B7B7F3F3F3DBDADADADADADCDBDBDC
                  DBDBDCDBDBDCDBDBDADADADBDADAF3F3F3B8B7B7FEFDFD989898CCA065FFFFFD
                  FFFFF8FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FFFF
                  F7FFFFF8FFFFFDCCA065999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999DDC099CCA065
                  CA9E63CA9E62CA9E62CA9E62CA9E62CA9E62CA9E62CA9E62CA9E62CA9E62CA9E
                  62CA9E63CCA065D5B180BBBBBB99999997979797979797979797979797979797
                  9797979797979797979797979797979797979797999999ABABABFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                NumGlyphs = 2
                Margin = 0
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 1139
            Height = 526
            inherited Splitter1: TevSplitter
              Height = 522
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 522
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 442
                IniAttributes.SectionName = 'TEDIT_EE_CL_PERSON\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 817
              Height = 522
              inherited wwDBGrid4: TevDBGrid
                Width = 767
                Height = 442
                IniAttributes.SectionName = 'TEDIT_EE_CL_PERSON\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtQuick: TTabSheet
      Caption = 'EE Entry'
      ImageIndex = 1
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlEmployeeInfo: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 298
          Height = 403
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Employee'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label45: TevLabel
            Left = 12
            Top = 35
            Width = 31
            Height = 16
            Caption = '~SSN'
            FocusControl = DBEdit21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label46: TevLabel
            Left = 12
            Top = 74
            Width = 63
            Height = 16
            Caption = '~Last Name '
            FocusControl = DBEdit22
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label47: TevLabel
            Left = 12
            Top = 113
            Width = 59
            Height = 16
            Caption = '~First Name'
            FocusControl = DBEdit23
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label48: TevLabel
            Left = 222
            Top = 113
            Width = 12
            Height = 13
            Caption = 'MI'
            FocusControl = DBEdit24
          end
          object Label62: TevLabel
            Left = 12
            Top = 152
            Width = 56
            Height = 16
            Caption = '~Address 1'
            FocusControl = DBEdit27
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label63: TevLabel
            Left = 12
            Top = 269
            Width = 68
            Height = 13
            Caption = 'Primary Phone'
            FocusControl = DBEdit33
          end
          object Label64: TevLabel
            Left = 150
            Top = 308
            Width = 33
            Height = 13
            Caption = 'County'
            FocusControl = DBEdit32
          end
          object Label65: TevLabel
            Left = 201
            Top = 230
            Width = 24
            Height = 16
            Caption = '~Zip'
            FocusControl = DBEdit31
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label66: TevLabel
            Left = 150
            Top = 230
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = DBEdit30
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label67: TevLabel
            Left = 12
            Top = 230
            Width = 26
            Height = 16
            Caption = '~City'
            FocusControl = DBEdit29
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label68: TevLabel
            Left = 12
            Top = 191
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = DBEdit28
          end
          object Label69: TevLabel
            Left = 150
            Top = 35
            Width = 51
            Height = 16
            Caption = '~EE Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label44: TevLabel
            Left = 128
            Top = 425
            Width = 118
            Height = 13
            Caption = 'Always key SSN first'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
            Visible = False
          end
          object Label79: TevLabel
            Left = 201
            Top = 269
            Width = 44
            Height = 16
            Alignment = taRightJustify
            Caption = '~Gender'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label53: TevLabel
            Left = 12
            Top = 347
            Width = 49
            Height = 16
            Alignment = taRightJustify
            Caption = '~Ethnicity'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblTribe: TevLabel
            Left = 150
            Top = 347
            Width = 24
            Height = 13
            Caption = 'Tribe'
            FocusControl = evDBEdit1
          end
          object evLabel41: TevLabel
            Left = 150
            Top = 269
            Width = 14
            Height = 13
            Caption = 'ext'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label52: TevLabel
            Left = 12
            Top = 308
            Width = 68
            Height = 16
            Alignment = taRightJustify
            Caption = '~Date of Birth'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit20: TevDBEdit
            Left = 150
            Top = 50
            Width = 64
            Height = 21
            HelpContext = 17714
            DataField = 'CUSTOM_EMPLOYEE_NUMBER'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit21: TevDBEdit
            Left = 12
            Top = 50
            Width = 134
            Height = 21
            HelpContext = 17713
            DataField = 'SOCIAL_SECURITY_NUMBER'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*3{#,x}-*2{&,#}-*4{&,#}'
            PopupMenu = pmSSN
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = DBEdit21Change
            OnExit = dedtSocial_Security_NumberExit
            OnKeyPress = DBEdit21KeyPress
            Glowing = False
          end
          object DBEdit22: TevDBEdit
            Left = 12
            Top = 89
            Width = 265
            Height = 21
            HelpContext = 17715
            DataField = 'LAST_NAME'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit23: TevDBEdit
            Left = 12
            Top = 128
            Width = 202
            Height = 21
            HelpContext = 17715
            DataField = 'FIRST_NAME'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit24: TevDBEdit
            Left = 222
            Top = 128
            Width = 55
            Height = 21
            HelpContext = 17715
            DataField = 'MIDDLE_INITIAL'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit27: TevDBEdit
            Left = 12
            Top = 167
            Width = 265
            Height = 21
            HelpContext = 18002
            DataField = 'ADDRESS1'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit28: TevDBEdit
            Left = 12
            Top = 206
            Width = 265
            Height = 21
            HelpContext = 18002
            DataField = 'ADDRESS2'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit29: TevDBEdit
            Left = 12
            Top = 245
            Width = 134
            Height = 21
            HelpContext = 18002
            DataField = 'CITY'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit30: TevDBEdit
            Left = 150
            Top = 245
            Width = 43
            Height = 21
            HelpContext = 18002
            DataField = 'STATE'
            DataSource = wwdsSubMaster2
            Picture.PictureMask = '*{&,@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = DBEdit30Exit
            Glowing = False
          end
          object DBEdit31: TevDBEdit
            Left = 201
            Top = 245
            Width = 76
            Height = 21
            HelpContext = 18002
            DataField = 'ZIP_CODE'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit32: TevDBEdit
            Left = 150
            Top = 323
            Width = 128
            Height = 21
            HelpContext = 18002
            DataField = 'COUNTY'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit33: TevDBEdit
            Left = 12
            Top = 284
            Width = 134
            Height = 21
            HelpContext = 18003
            DataField = 'PHONE1'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit34: TevDBEdit
            Left = 150
            Top = 284
            Width = 43
            Height = 21
            HelpContext = 18003
            DataField = 'DESCRIPTION1'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwDBDateTimePicker16: TevDBDateTimePicker
            Left = 12
            Top = 323
            Width = 134
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.YearsPerColumn = 20
            CalendarAttributes.PopupYearOptions.NumberColumns = 5
            CalendarAttributes.PopupYearOptions.StartYear = 1930
            DataField = 'BIRTH_DATE'
            DataSource = wwdsSubMaster2
            Epoch = 1900
            ShowButton = True
            TabOrder = 13
          end
          object evDBComboBox1: TevDBComboBox
            Left = 201
            Top = 284
            Width = 77
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'GENDER'
            DataSource = wwdsSubMaster2
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Male'#9'M'
              'Female'#9'F'
              'Non-Applicable'#9'U')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 14
            UnboundDataType = wwDefault
          end
          object wwDBComboBox6: TevDBComboBox
            Left = 12
            Top = 362
            Width = 134
            Height = 21
            HelpContext = 17733
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Column1Width = 150
            DataField = 'ETHNICITY'
            DataSource = wwdsSubMaster2
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'African American'#9'N'
              'Hispanic'#9'H'
              'Asian'#9'A'
              'American Indian'#9'I'
              'Caucasian'#9'C'
              'Other'#9'O'
              'Not Applicable'#9'P')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 15
            UnboundDataType = wwDefault
            OnCloseUp = wwDBComboBox6CloseUp
          end
          object evDBEdit1: TevDBEdit
            Left = 150
            Top = 362
            Width = 128
            Height = 21
            DataField = 'TRIBE'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 16
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpLaborDefaults: TisUIFashionPanel
          Left = 314
          Top = 362
          Width = 321
          Height = 301
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'FashionPanel1'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Labor Defaults'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label75: TevLabel
            Left = 12
            Top = 67
            Width = 37
            Height = 13
            Alignment = taRightJustify
            Caption = 'Division'
          end
          object Label76: TevLabel
            Left = 12
            Top = 115
            Width = 55
            Height = 13
            Alignment = taRightJustify
            Caption = 'Department'
          end
          object Label77: TevLabel
            Left = 12
            Top = 139
            Width = 27
            Height = 13
            Alignment = taRightJustify
            Caption = 'Team'
          end
          object Label78: TevLabel
            Left = 12
            Top = 91
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Branch'
          end
          object btnDBDTEdit: TevSpeedButton
            Left = 88
            Top = 35
            Width = 212
            Height = 21
            Hint = 'Select D/B/D/T'
            Caption = 'Assign D/B/D/T'
            HideHint = True
            ParentShowHint = False
            ShowHint = True
            AutoSize = False
            OnClick = SpeedButton3Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
              77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
              7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
              8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
              7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
              AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
              2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
              7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
            PopupMenu = pmDBDTCopy
            ParentColor = False
            ShortCut = 115
          end
          object evLabel21: TevLabel
            Left = 12
            Top = 161
            Width = 55
            Height = 13
            Caption = 'Default WC'
          end
          object evLabel23: TevLabel
            Left = 12
            Top = 200
            Width = 54
            Height = 13
            Caption = 'Default Job'
          end
          object lblUnion: TevLabel
            Left = 12
            Top = 240
            Width = 28
            Height = 13
            Caption = 'Union'
          end
          object cbDivisionName: TevDBLookupCombo
            Left = 198
            Top = 64
            Width = 102
            Height = 21
            HelpContext = 17716
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 1
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbBranchName: TevDBLookupCombo
            Left = 198
            Top = 88
            Width = 102
            Height = 21
            HelpContext = 17717
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 3
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbDepartmentName: TevDBLookupCombo
            Left = 198
            Top = 112
            Width = 102
            Height = 21
            HelpContext = 17718
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 5
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbTeamName: TevDBLookupCombo
            Left = 198
            Top = 136
            Width = 102
            Height = 21
            HelpContext = 17719
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 7
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbDivisionNbr: TevDBLookupCombo
            Left = 88
            Top = 64
            Width = 102
            Height = 21
            HelpContext = 17716
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DIVISION_NUMBER'#9'20'#9'CUSTOM_DIVISION_NUMBER'#9'F')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 0
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbBranchNbr: TevDBLookupCombo
            Left = 88
            Top = 88
            Width = 102
            Height = 21
            HelpContext = 17717
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BRANCH_NUMBER'#9'20'#9'CUSTOM_BRANCH_NUMBER'#9'F')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 2
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbDepartmentNbr: TevDBLookupCombo
            Left = 88
            Top = 112
            Width = 102
            Height = 21
            HelpContext = 17718
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DEPARTMENT_NUMBER'#9'20'#9'CUSTOM_DEPARTMENT_NUMBER'#9'F')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 4
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbTeamNbr: TevDBLookupCombo
            Left = 88
            Top = 136
            Width = 102
            Height = 21
            HelpContext = 17719
            TabStop = False
            OnSetLookupText = cbDivisionNbrSetLookupText
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_TEAM_NUMBER'#9'20'#9'CUSTOM_TEAM_NUMBER'#9'F')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            ReadOnly = True
            TabOrder = 6
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 215
            Width = 289
            Height = 21
            HelpContext = 17760
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Code'#9'F'
              'TRUE_DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'CO_JOBS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_JOBS.CO_JOBS
            LookupField = 'CO_JOBS_NBR'
            Options = [loTitles, loDisplaySelectedFields]
            Style = csDropDownList
            TabOrder = 9
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evDBLookupCombo6: TevDBLookupCombo
            Left = 12
            Top = 176
            Width = 288
            Height = 21
            HelpContext = 19002
            DisplayTextFormat = '[Workers_Comp_Code]   [Description]   [Co_State_Lookup]'
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
              'DESCRIPTION'#9'40'#9'Description'#9'F'
              'Co_State_Lookup'#9'2'#9'State'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Options = [loTitles, loDisplaySelectedFields]
            Style = csDropDownList
            TabOrder = 8
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cmbUnion: TevDBLookupCombo
            Left = 12
            Top = 255
            Width = 289
            Height = 21
            HelpContext = 17760
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Union_Name'#9'40'#9'Union Name'#9'F')
            DataField = 'CO_UNIONS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_UNIONS.CO_UNIONS
            LookupField = 'CO_UNIONS_NBR'
            Options = [loTitles, loDisplaySelectedFields]
            Style = csDropDownList
            TabOrder = 10
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
        object fpPay: TisUIFashionPanel
          Left = 314
          Top = 8
          Width = 321
          Height = 205
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPay'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Pay'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label70: TevLabel
            Left = 12
            Top = 74
            Width = 68
            Height = 13
            Caption = 'Salary Amount'
            FocusControl = DBEdit35
          end
          object Label71: TevLabel
            Left = 12
            Top = 113
            Width = 72
            Height = 16
            Caption = '~Rate Number'
            FocusControl = DBEdit36
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label72: TevLabel
            Left = 12
            Top = 35
            Width = 80
            Height = 16
            Caption = '~Pay Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label73: TevLabel
            Left = 166
            Top = 35
            Width = 74
            Height = 13
            Caption = 'Standard Hours'
          end
          object Label74: TevLabel
            Left = 166
            Top = 113
            Width = 71
            Height = 16
            Caption = '~Rate Amount'
            FocusControl = DBEdit38
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel
            Left = 166
            Top = 74
            Width = 71
            Height = 13
            Caption = 'Average Hours'
          end
          object evLabel37: TevLabel
            Left = 12
            Top = 152
            Width = 105
            Height = 13
            Caption = 'Position for Pay Grade'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel36: TevLabel
            Left = 166
            Top = 152
            Width = 50
            Height = 13
            Caption = 'Pay Grade'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit35: TevDBEdit
            Left = 12
            Top = 89
            Width = 148
            Height = 21
            HelpContext = 18006
            DataField = 'SALARY_AMOUNT'
            DataSource = wwdsSubMaster
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit36: TevDBEdit
            Left = 12
            Top = 128
            Width = 148
            Height = 21
            HelpContext = 18007
            DataField = 'RATE_NUMBER'
            DataSource = wwdsSubMaster4
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwDBComboBox8: TevDBComboBox
            Left = 12
            Top = 50
            Width = 148
            Height = 21
            HelpContext = 18005
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PAY_FREQUENCY'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'W'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'S'
              'Monthly'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object DBEdit37: TevDBEdit
            Left = 166
            Top = 50
            Width = 134
            Height = 21
            HelpContext = 18009
            DataField = 'STANDARD_HOURS'
            DataSource = wwdsSubMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit38: TevDBEdit
            Left = 166
            Top = 128
            Width = 134
            Height = 21
            HelpContext = 18008
            DataField = 'RATE_AMOUNT'
            DataSource = wwdsSubMaster4
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '[-]*12[#][.*4[#]]'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = DBEdit38Exit
            Glowing = False
          end
          object edtAvHours: TevDBEdit
            Left = 166
            Top = 89
            Width = 134
            Height = 21
            EditAlignment = eaRightAlignEditing
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = edtAvHoursExit
            Glowing = False
          end
          object evcbPosition: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 148
            Height = 21
            HelpContext = 19002
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_HR_POSITIONS_NBR'
            DataSource = wwdsSubMaster4
            LookupTable = DM_CO_HR_POSITIONS.CO_HR_POSITIONS
            LookupField = 'CO_HR_POSITIONS_NBR'
            Style = csDropDownList
            ParentFont = False
            PopupMenu = pmPosition
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = evcbPositionChange
            OnCloseUp = evcbPositionCloseUp
          end
          object wwlcHR_Position_Grades_Number: TevDBLookupCombo
            Left = 166
            Top = 167
            Width = 134
            Height = 21
            HelpContext = 19002
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'30'#9'Pay Grade'#9'F'
              'MINIMUM_PAY'#9'10'#9'Minimum Pay'#9'F'
              'MID_PAY'#9'10'#9'Mid Pay'#9'F'
              'MAXIMUM_PAY'#9'10'#9'Maximum Pay'#9'F')
            DataField = 'CO_HR_POSITION_GRADES_NBR'
            DataSource = wwdsSubMaster4
            LookupTable = DM_CO_HR_POSITION_GRADES.CO_HR_POSITION_GRADES
            LookupField = 'CO_HR_POSITION_GRADES_NBR'
            Options = [loTitles]
            Style = csDropDownList
            ParentFont = False
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
        object fpPosition: TisUIFashionPanel
          Left = 314
          Top = 221
          Width = 321
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPosition'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Position'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel33: TevLabel
            Left = 12
            Top = 35
            Width = 110
            Height = 16
            Caption = '~Healthcare Coverage'
            FocusControl = cmbHealthCare
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel38: TevLabel
            Left = 168
            Top = 74
            Width = 89
            Height = 13
            Caption = 'Eligible for Benefits'
          end
          object evLabel45: TevLabel
            Left = 12
            Top = 74
            Width = 148
            Height = 13
            Caption = 'Dependent Coverage Available'
          end
          object cmbHealthCare: TevDBComboBox
            Left = 12
            Top = 50
            Width = 148
            Height = 21
            HelpContext = 20009
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'HEALTHCARE_COVERAGE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'No ER Paid Ins/Not Eligible'#9'N'
              'Eligible/Not Covered'#9'E'
              'Eligible/Covered'#9'I')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmHealthCare
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            AutoPopupEffectiveDateDialog = True
            DontAutoPopupEffectiveDateDialog = True
          end
          object edDependentBenefitDate: TevDBDateTimePicker
            Left = 168
            Top = 89
            Width = 134
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DEPENDENT_BENEFITS_AVAIL_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object edBenefitAvailable: TevDBComboBox
            Left = 12
            Top = 89
            Width = 148
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DEPENDENT_BENEFITS_AVAILABLE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object fpTaxationDetails: TisUIFashionPanel
          Left = 643
          Top = 8
          Width = 224
          Height = 655
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpTaxationDetails'
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Taxation Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label54: TevLabel
            Left = 12
            Top = 74
            Width = 105
            Height = 16
            Caption = '~Federal Dependents'
            FocusControl = DBEdit25
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label56: TevLabel
            Left = 12
            Top = 146
            Width = 34
            Height = 16
            Caption = '~State'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label57: TevLabel
            Left = 12
            Top = 262
            Width = 101
            Height = 16
            Caption = '~State Marital Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label58: TevLabel
            Left = 12
            Top = 185
            Width = 18
            Height = 13
            Caption = 'SDI'
          end
          object Label59: TevLabel
            Left = 12
            Top = 223
            Width = 18
            Height = 13
            Caption = 'SUI'
          end
          object Label60: TevLabel
            Left = 141
            Top = 262
            Width = 60
            Height = 13
            AutoSize = False
            Caption = 'State Dep'
            FocusControl = DBEdit26
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object bvlTaxDetails1: TEvBevel
            Left = 16
            Top = 130
            Width = 189
            Height = 3
            Shape = bsTopLine
          end
          object EvBevel1: TEvBevel
            Left = 12
            Top = 396
            Width = 189
            Height = 3
            Shape = bsTopLine
          end
          object lablReciprocal_Method: TevLabel
            Left = 12
            Top = 410
            Width = 99
            Height = 16
            Caption = '~Reciprocal Method'
            FocusControl = wwcbReciprocal_Method
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablReciprocal_State: TevLabel
            Left = 12
            Top = 449
            Width = 79
            Height = 13
            Caption = 'Reciprocal State'
            FocusControl = wwlcReciprocal_State
          end
          object DBRadioGroup5: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 192
            Height = 33
            HelpContext = 18010
            Caption = '~Federal Marital Status'
            Columns = 2
            DataField = 'FEDERAL_MARITAL_STATUS'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Single'
              'Married')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'S'
              'M')
          end
          object DBEdit25: TevDBEdit
            Left = 12
            Top = 89
            Width = 121
            Height = 21
            HelpContext = 18011
            DataField = 'NUMBER_OF_DEPENDENTS'
            DataSource = wwdsSubMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = DBEdit25Exit
            Glowing = False
          end
          object wwlcState: TevDBLookupCombo
            Left = 12
            Top = 160
            Width = 121
            Height = 21
            HelpContext = 18013
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnDropDown = wwlcStateDropDown
            OnCloseUp = wwlcStateCloseUp
          end
          object wwlcMarital_Status: TevDBLookupCombo
            Left = 12
            Top = 277
            Width = 121
            Height = 21
            HelpContext = 18014
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATUS_TYPE'#9'6'#9'STATUS_TYPE'#9'F'
              'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION')
            DataField = 'STATE_MARITAL_STATUS'
            DataSource = wwdsDetail
            LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
            LookupField = 'STATUS_TYPE'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = wwlcMarital_StatusChange
            OnBeforeDropDown = wwlcMarital_StatusBeforeDropDown
            OnCloseUp = wwlcMarital_StatusCloseUp
          end
          object wwlcSDI: TevDBLookupCombo
            Left = 12
            Top = 199
            Width = 121
            Height = 21
            HelpContext = 18016
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SDI_APPLY_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = wwlcSDICloseUp
          end
          object wwlcSUI: TevDBLookupCombo
            Left = 12
            Top = 238
            Width = 121
            Height = 21
            HelpContext = 18017
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SUI_APPLY_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = wwlcSUICloseUp
          end
          object DBEdit26: TevDBEdit
            Left = 141
            Top = 277
            Width = 63
            Height = 21
            HelpContext = 18015
            DataField = 'STATE_NUMBER_WITHHOLDING_ALLOW'
            DataSource = wwdsDetail
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbReciprocal_Method: TevDBComboBox
            Left = 11
            Top = 426
            Width = 192
            Height = 21
            HelpContext = 20009
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'RECIPROCAL_METHOD'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Take None'#9'N'
              'Take Difference Between States'#9'D'
              'Take a Flat Amount'#9'F'
              'Take a Percentage'#9'P'
              'Take the Full Amount for Each State'#9'U')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 8
            UnboundDataType = wwDefault
          end
          object btnLocals: TevBitBtn
            Left = 12
            Top = 313
            Width = 192
            Height = 21
            Caption = 'Assign Locals'
            TabOrder = 7
            OnClick = btnLocalsClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDD
              DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFFFFCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC008B4700C683469F
              74CCCCCCCCCCCCFFFFFFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCC7C7B7BB5B5B5949393CCCCCCCCCCCCFFFFFF2E92722E9171
              2E90702B906F20906EA7826DA78570AA8673AE8675B8857600894500E4A500BE
              7F128B57349074FFFFFF88888887878786868686868686858581808083838386
              85858686868686867A7979D3D2D2AEAEAE7E7E7D878787FFFFFF2D84632BB281
              29B38323A879157956FFFAF100847600894100894300884400844100D9A100D8
              A000BB7E128450DDDDDD7B7B7BA4A4A4A6A6A69B9B9B6F6F6FFAF9F97E7E7D7A
              79797A7979797979767675C9C8C8C8C8C7ABABAB777676DDDDDDEBF3F051785D
              188B5D61977BDAD6C7FFF0E80082393CE7BF00D7A000D7A000D59F00D09C00D0
              9C00D39F00B98045A175F3F2F27170707F7E7E8F8F8FD4D4D4F0F0F0737373DA
              DADAC7C7C7C7C7C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9969595FFFFFFAC8875
              FFF8EFFFEBDEFEEBDEFFEFE50081386BE7CE00C89900C89900C89900C79700C8
              9800CA9A63E6CD008A47FFFFFF878787F8F8F8EBEBEAEBEBEAF0EFEF727171DE
              DEDEBABABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFA18973
              FEF4EBCAB7A5CFBAA9A27F6B00833A91EEE04BE8D34EE7D34DE6D193E8D800C3
              9760E0C600B28153B184FFFFFF868686F4F4F4B5B5B5B8B8B87E7E7D747373E8
              E7E7DFDFDEDEDEDEDDDDDDE2E2E2B6B6B5D7D6D6A4A4A4A5A5A5FFFFFF9F8B75
              FDF4EBF2E4D5CCB9A7FFECE03C9A6600833A00823A00823B00803880E4D557DA
              C300AD7D5BB388FFFFFFFFFFFF888888F4F4F4E3E3E2B8B7B7ECECEC8D8D8D74
              7373737373737373717070DDDDDDD1D1D1A09F9FA8A8A7FFFFFFFFFFFFA08D77
              FCF5EDC7B6A2C9B9A6CEBAA8D6BDACDCBEAEE1BFB0ECC1B400843B71E1D400A8
              793F8A59FFFFFFFFFFFFFFFFFF8A8A8AF5F5F5B4B4B4B7B6B6B8B8B8BCBCBBBE
              BDBDBFBFBFC3C2C2757474DBDADA9B9B9B7F7E7EFFFFFFFFFFFFFFFFFFA49079
              FDF6EFF0E0D0C9B7A4F3E4D5CBB8A6F5E4D6CFB9A7FFE8DC00823800A37854B0
              81BC9481FFFFFFFFFFFFFFFFFF8D8D8DF7F6F6DFDFDEB5B5B5E3E3E2B7B6B6E4
              E3E3B8B7B7E8E8E8737373979696A3A3A3949393FFFFFFFFFFFFFFFFFFA6927B
              FDF7F2C7B49FC9B7A3C9B7A4CAB7A4C9B7A4CAB7A4CFB8A6D9BAA9DAB8A7FFFB
              F8AD947EFFFFFFFFFFFFFFFFFF8F8F8FF8F8F8B2B1B1B5B5B5B5B5B5B5B5B5B5
              B5B5B5B5B5B7B6B6B9B9B9B8B7B7FCFCFB929292FFFFFFFFFFFFFFFFFFA8947E
              FEFAF4ECDBCBC8B7A3EFDFD0C8B7A4EFDFD0C8B7A4F0DFD0CAB7A4EFDCCCFFFA
              F5A9947EFFFFFFFFFFFFFFFFFF929292FAFAFADADADAB5B5B5DEDEDEB5B5B5DE
              DEDEB5B5B5DEDEDEB5B5B5DBDADAFAFAFA929292FFFFFFFFFFFFFFFFFFAA9680
              FFFCF8C4B19CC6B5A0C7B6A1C7B6A1C7B6A1C7B6A1C7B6A1C6B5A0C4B19CFFFC
              F8AA9680FFFFFFFFFFFFFFFFFF949393FDFDFCAFAFAFB2B2B2B3B3B3B3B3B3B3
              B3B3B3B3B3B3B3B3B2B2B2AFAFAFFDFDFC949393FFFFFFFFFFFFFFFFFFAD9983
              FFFEFBE7D4C1C3B09CE9D7C4C4B19DE9D7C4C4B19DE9D7C4C3B09CE7D4C1FFFE
              FBAD9983FFFFFFFFFFFFFFFFFF979696FFFFFFD3D2D2AEAEAED5D5D5AFAFAFD5
              D5D5AFAFAFD5D5D5AEAEAED3D2D2FFFFFF979696FFFFFFFFFFFFFFFFFFB09D87
              FFFFFCFFFCF8FFFCF9FFFDFAFFFDFAFFFDFAFFFDFAFFFDFAFFFCF9FFFCF8FFFF
              FCB09D87FFFFFFFFFFFFFFFFFF9A9A9AFFFFFFFDFDFCFDFDFCFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFDFDFCFDFDFCFFFFFF9A9A9AFFFFFFFFFFFFFFFFFFCCC0B1
              B29F8AB19E88B19E87B19E87B19E87B19E87B19E87B19E87B19E87B19E88B29F
              8ACCC0B1FFFFFFFFFFFFFFFFFFBEBDBD9D9D9D9B9B9B9B9B9B9B9B9B9B9B9B9B
              9B9B9B9B9B9B9B9B9B9B9B9B9B9B9D9D9DBEBDBDFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object wwlcReciprocal_State: TevDBLookupCombo
            Left = 12
            Top = 465
            Width = 192
            Height = 21
            HelpContext = 20010
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'RECIPROCAL_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 9
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object rgWorkAtHome: TevDBRadioGroup
            Left = 12
            Top = 342
            Width = 192
            Height = 36
            HelpContext = 18010
            Caption = ' Work At Home '
            Columns = 2
            DataField = 'WORKATHOME'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
          end
        end
        object fpHireStatus: TisUIFashionPanel
          Left = 8
          Top = 419
          Width = 298
          Height = 244
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Hire Status'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label49: TevLabel
            Left = 12
            Top = 35
            Width = 91
            Height = 16
            Caption = '~Current Hire Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label50: TevLabel
            Left = 12
            Top = 74
            Width = 87
            Height = 13
            Caption = 'Current Term Date'
          end
          object Label51: TevLabel
            Left = 12
            Top = 113
            Width = 104
            Height = 16
            Caption = '~Current Status Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label61: TevLabel
            Left = 150
            Top = 35
            Width = 83
            Height = 13
            Caption = 'Original Hire Date'
          end
          object evLabel35: TevLabel
            Left = 150
            Top = 74
            Width = 91
            Height = 16
            Caption = '~Eligible for Rehire'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPosition_Status: TevLabel
            Left = 12
            Top = 152
            Width = 79
            Height = 16
            Caption = '~Position Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwDBComboBox5: TevDBComboBox
            Left = 12
            Top = 128
            Width = 263
            Height = 21
            HelpContext = 17726
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CURRENT_TERMINATION_CODE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Active'#9'A'
              'Involuntary Layoff'#9'I'
              'Termination Due to Layoff'#9'L'
              'Release Without Prejudice'#9'F'
              'Voluntary Resignation'#9'R'
              'Termination Due to Retirement'#9'E'
              'Termination Due to Transfer'#9'T'
              'Leave of Absence'#9'V'
              'Seasonal'#9'S'
              'Termination Due to Death'#9'D'
              'Terminated'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object wwDBDateTimePicker17: TevDBDateTimePicker
            Left = 150
            Top = 50
            Width = 125
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'ORIGINAL_HIRE_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object wwDBDateTimePicker18: TevDBDateTimePicker
            Left = 12
            Top = 50
            Width = 134
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CURRENT_HIRE_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 0
          end
          object wwDBDateTimePicker19: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 134
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CURRENT_TERMINATION_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object evDBComboBox5: TevDBComboBox
            Left = 150
            Top = 89
            Width = 125
            Height = 21
            HelpContext = 18005
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ELIGIBLE_FOR_REHIRE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'W'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'S'
              'Monthly'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object wwcbPosition_Status: TevDBComboBox
            Left = 12
            Top = 167
            Width = 148
            Height = 21
            HelpContext = 17730
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'POSITION_STATUS'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Full Time'#9'F'
              'Full Time Temp'#9'U'
              'Part Time'#9'P'
              'Part Time temp'#9'R'
              'Seasonal'#9'S'
              'Student'#9'T'
              '1099'#9'1')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
        end
      end
    end
    object tshtEmployee: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbDetails: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlAdditionalDetails: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 207
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Position'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPosition_Effective_Date: TevLabel
            Left = 12
            Top = 230
            Width = 108
            Height = 13
            Caption = 'Position Effective Date'
          end
          object lablTime_Clock_Number: TevLabel
            Left = 12
            Top = 152
            Width = 93
            Height = 13
            Caption = 'Time Clock Number'
          end
          object evLabel22: TevLabel
            Left = 12
            Top = 35
            Width = 113
            Height = 16
            Caption = '~New Hire Report Sent'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPosition: TevLabel
            Left = 12
            Top = 191
            Width = 37
            Height = 13
            Caption = 'Position'
          end
          object wwlcPosition: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 177
            Height = 21
            HelpContext = 17727
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_HR_POSITIONS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_HR_POSITIONS.CO_HR_POSITIONS
            LookupField = 'CO_HR_POSITIONS_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBDateTimePicker8: TevDBDateTimePicker
            Left = 12
            Top = 245
            Width = 177
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'POSITION_EFFECTIVE_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object drgpFLSA_Exempt: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 178
            Height = 75
            HelpContext = 17767
            Caption = '~FLSA Exempt'
            DataField = 'FLSA_EXEMPT'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Aplicable')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
          object evDBEdit2: TevDBEdit
            Left = 12
            Top = 167
            Width = 177
            Height = 21
            DataField = 'TIME_CLOCK_NUMBER'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox7: TevDBComboBox
            Left = 12
            Top = 50
            Width = 177
            Height = 21
            HelpContext = 17730
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'NEW_HIRE_REPORT_SENT'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup5: TevDBRadioGroup
            Left = 12
            Top = 308
            Width = 177
            Height = 36
            HelpContext = 17787
            Caption = 'Corporate Officer'
            Columns = 2
            DataField = 'CORPORATE_OFFICER'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
            OnExit = evDBRadioGroup5Exit
          end
          object evDBRadioGroup6: TevDBRadioGroup
            Left = 12
            Top = 269
            Width = 177
            Height = 36
            HelpContext = 17787
            Caption = 'Highly Compensated'
            Columns = 2
            DataField = 'HIGHLY_COMPENSATED'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object pnlDistribution: TisUIFashionPanel
          Left = 223
          Top = 8
          Width = 198
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlDistribution'
          Color = 14737632
          TabOrder = 1
          TabStop = True
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payroll'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablLabor_Distribution_Options: TevLabel
            Left = 12
            Top = 35
            Width = 130
            Height = 16
            Caption = '~Labor Distribution Options'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAuto_Labor_Distribution_E_D_Group: TevLabel
            Left = 12
            Top = 74
            Width = 162
            Height = 13
            Caption = 'Auto Labor Distribution E/D Group'
            FocusControl = wwlcAuto_Labor_Distribution_E_D_Group
          end
          object lablPay_Group: TevLabel
            Left = 12
            Top = 113
            Width = 50
            Height = 13
            Caption = 'Pay Group'
          end
          object lablCall_From: TevLabel
            Left = 12
            Top = 269
            Width = 60
            Height = 13
            Caption = 'On Call From'
          end
          object lablCall_To: TevLabel
            Left = 100
            Top = 269
            Width = 50
            Height = 13
            Caption = 'On Call To'
          end
          object Label35: TevLabel
            Left = 48
            Top = 400
            Width = 41
            Height = 13
            Caption = 'G/L Tag'
            FocusControl = DBEdit1
          end
          object lablGroup_Term_Policy_Amount: TevLabel
            Left = 12
            Top = 191
            Width = 126
            Height = 13
            Caption = 'Group Term Policy Amount'
            FocusControl = dedtGroup_Term_Policy_Amount
          end
          object evLabel9: TevLabel
            Left = 12
            Top = 152
            Width = 38
            Height = 13
            Caption = 'Delivery'
          end
          object evLabel19: TevLabel
            Left = 12
            Top = 230
            Width = 52
            Height = 13
            Caption = 'GTL Hours'
            FocusControl = evDBEdit5
          end
          object evLabel26: TevLabel
            Left = 100
            Top = 230
            Width = 47
            Height = 13
            Caption = 'GTL Rate'
            FocusControl = evDBEdit6
          end
          object evLabel48: TevLabel
            Left = 12
            Top = 308
            Width = 41
            Height = 13
            Caption = 'G/L Tag'
          end
          object wwcbLabor_Distribution_Options: TevDBComboBox
            Left = 12
            Top = 50
            Width = 169
            Height = 21
            HelpContext = 18101
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DISTRIBUTE_TAXES'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Distribute Taxes'#9'T'
              'Distribute Deductions'#9'D'
              'Distribute Both'#9'B'
              'Distribute None'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object wwlcAuto_Labor_Distribution_E_D_Group: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 169
            Height = 21
            HelpContext = 18103
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'ALD_CL_E_D_GROUPS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcPay_Group: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 169
            Height = 21
            HelpContext = 17761
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_PAY_GROUP_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_PAY_GROUP.CO_PAY_GROUP
            LookupField = 'CO_PAY_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBDateTimePicker14: TevDBDateTimePicker
            Left = 12
            Top = 284
            Width = 81
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'ON_CALL_FROM'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 7
          end
          object wwDBDateTimePicker15: TevDBDateTimePicker
            Left = 100
            Top = 284
            Width = 81
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'ON_CALL_TO'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 8
          end
          object DBEdit1: TevDBEdit
            Left = 12
            Top = 323
            Width = 169
            Height = 21
            HelpContext = 10668
            DataField = 'GENERAL_LEDGER_TAG'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtGroup_Term_Policy_Amount: TevDBEdit
            Left = 12
            Top = 206
            Width = 169
            Height = 21
            HelpContext = 17762
            DataField = 'GROUP_TERM_POLICY_AMOUNT'
            DataSource = wwdsSubMaster
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 169
            Height = 21
            HelpContext = 17760
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DELIVERY_GROUP_DESCRIPTION'#9'20'#9'DELIVERY_GROUP_DESCRIPTION')
            DataField = 'CL_DELIVERY_GROUP_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CL_DELIVERY_GROUP.CL_DELIVERY_GROUP
            LookupField = 'CL_DELIVERY_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evDBEdit5: TevDBEdit
            Left = 12
            Top = 245
            Width = 81
            Height = 21
            HelpContext = 17762
            DataField = 'GTL_NUMBER_OF_HOURS'
            DataSource = wwdsSubMaster
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit6: TevDBEdit
            Left = 100
            Top = 245
            Width = 81
            Height = 21
            HelpContext = 17762
            DataField = 'GTL_RATE'
            DataSource = wwdsSubMaster
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object pnlSecondaryDetails: TisUIFashionPanel
          Left = 429
          Top = 8
          Width = 233
          Height = 379
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSecondaryDetails'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Reporting'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object YTDBtn: TevSpeedButton
            Left = 12
            Top = 35
            Width = 201
            Height = 21
            Caption = 'YTD (F3)'
            HideHint = True
            AutoSize = False
            OnClick = YTDBtnClick
            NumGlyphs = 2
            ParentColor = False
            ShortCut = 114
          end
          object evLabel12: TevLabel
            Left = 12
            Top = 252
            Width = 118
            Height = 13
            Caption = 'Second Check Template'
          end
          object evLabel18: TevLabel
            Left = 12
            Top = 213
            Width = 129
            Height = 16
            Caption = '~Deductions To Take First'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object drgpTipped_Directly: TevDBRadioGroup
            Left = 12
            Top = 60
            Width = 201
            Height = 36
            HelpContext = 17766
            Caption = '~Tipped Directly'
            Columns = 2
            DataField = 'TIPPED_DIRECTLY'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 98
            Width = 201
            Height = 36
            HelpContext = 17787
            Caption = '~Ignore FICA on Cleanup Payroll'
            Columns = 2
            DataField = 'MAKEUP_FICA_ON_CLEANUP_PR'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 136
            Width = 201
            Height = 36
            HelpContext = 17787
            Caption = '~Combine Returns On This EE'
            Columns = 2
            DataField = 'BASE_RETURNS_ON_THIS_EE'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
            OnChange = DBRadioGroup1Change
          end
          object evDBRadioGroup4: TevDBRadioGroup
            Left = 12
            Top = 174
            Width = 201
            Height = 36
            HelpContext = 17787
            Caption = '~Generate Second Check'
            Columns = 2
            DataField = 'GENERATE_SECOND_CHECK'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object wwlcCheck_Template: TevDBLookupCombo
            Left = 12
            Top = 267
            Width = 201
            Height = 21
            HelpContext = 17761
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_PR_CHECK_TEMPLATES_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_PR_CHECK_TEMPLATES.CO_PR_CHECK_TEMPLATES
            LookupField = 'CO_PR_CHECK_TEMPLATES_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evDBComboBox4: TevDBComboBox
            Left = 12
            Top = 228
            Width = 201
            Height = 21
            HelpContext = 17730
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'GOV_GARNISH_PRIOR_CHILD_SUPPT'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object evBitBtn2: TevBitBtn
            Left = 12
            Top = 338
            Width = 201
            Height = 21
            Caption = 'EE Scheduled ED'#39's'
            TabOrder = 6
            OnClick = evBitBtn2Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
          object rgIncludeAnalytics: TevDBRadioGroup
            Left = 12
            Top = 294
            Width = 201
            Height = 36
            HelpContext = 17787
            Caption = '~Include in Analytics'
            Columns = 2
            DataField = 'ENABLE_ANALYTICS'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmEnableAnalytics
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
        end
      end
    end
    object tshtAddress: TTabSheet
      HelpContext = 17734
      Caption = 'Address'
      ImageIndex = 3
      object sbAddress: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlStandardAddress: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 298
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Standard Address for Mailing W2'#39's'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablAddress_1: TevLabel
            Left = 12
            Top = 35
            Width = 56
            Height = 16
            Caption = '~Address 1'
            FocusControl = dedtAddress_1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablThird_Phone: TevLabel
            Left = 12
            Top = 308
            Width = 69
            Height = 13
            Caption = 'Tertiary Phone'
            FocusControl = dedtThird_Phone
          end
          object lablSecondary_Phone: TevLabel
            Left = 12
            Top = 269
            Width = 85
            Height = 13
            Caption = 'Secondary Phone'
            FocusControl = dedtSecondary_Phone
          end
          object lablPrimary_Phone: TevLabel
            Left = 12
            Top = 230
            Width = 68
            Height = 13
            Caption = 'Primary Phone'
            FocusControl = dedtPrimary_Phone
          end
          object lablCounty: TevLabel
            Left = 12
            Top = 191
            Width = 33
            Height = 13
            Caption = 'County'
            FocusControl = dedtCounty
          end
          object lablZip_Code: TevLabel
            Left = 148
            Top = 152
            Width = 24
            Height = 16
            Caption = '~Zip'
            FocusControl = dedtZip_Code
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState: TevLabel
            Left = 12
            Top = 152
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = dedtState
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCity: TevLabel
            Left = 12
            Top = 113
            Width = 26
            Height = 16
            Caption = '~City'
            FocusControl = dedtCity
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAddress_2: TevLabel
            Left = 12
            Top = 74
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = dedtAddress_2
          end
          object evLabel8: TevLabel
            Left = 148
            Top = 191
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Country'
          end
          object lblExtension1: TevLabel
            Left = 148
            Top = 230
            Width = 45
            Height = 13
            Caption = 'extension'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblExtension2: TevLabel
            Left = 148
            Top = 269
            Width = 45
            Height = 13
            Caption = 'extension'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblExtension3: TevLabel
            Left = 148
            Top = 308
            Width = 45
            Height = 13
            Caption = 'extension'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtAddress_1: TevDBEdit
            Left = 12
            Top = 50
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'ADDRESS1'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAddress_2: TevDBEdit
            Left = 12
            Top = 89
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'ADDRESS2'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtCity: TevDBEdit
            Left = 12
            Top = 128
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'CITY'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtState: TevDBEdit
            Left = 12
            Top = 167
            Width = 129
            Height = 21
            HelpContext = 17734
            CharCase = ecUpperCase
            DataField = 'STATE'
            DataSource = wwdsSubMaster2
            Picture.PictureMask = '*{&,@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = DBEdit30Exit
            Glowing = False
          end
          object dedtZip_Code: TevDBEdit
            Left = 148
            Top = 167
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'ZIP_CODE'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtCounty: TevDBEdit
            Left = 12
            Top = 206
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'COUNTY'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtPrimary_Phone: TevDBEdit
            Left = 12
            Top = 245
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'PHONE1'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtSecondary_Phone: TevDBEdit
            Left = 12
            Top = 284
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'PHONE2'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtThird_Phone: TevDBEdit
            Left = 12
            Top = 323
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'PHONE3'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtThird_Phone_Description: TevDBEdit
            Left = 148
            Top = 323
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'DESCRIPTION3'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtPrimary_Phone_Description: TevDBEdit
            Left = 148
            Top = 245
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'DESCRIPTION1'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtSecondary_Phone_Description: TevDBEdit
            Left = 148
            Top = 284
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'DESCRIPTION2'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edCountry: TevDBEdit
            Left = 148
            Top = 206
            Width = 129
            Height = 21
            DataField = 'COUNTRY'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object pnlAlternateAddress: TisUIFashionPanel
          Left = 314
          Top = 8
          Width = 298
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlAlternateAddress'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Alternate Address for Payroll Checks'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel2: TevLabel
            Left = 12
            Top = 35
            Width = 47
            Height = 13
            Caption = 'Address 1'
            FocusControl = dedtAddress_1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 74
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = dedtAddress_2
          end
          object evLabel7: TevLabel
            Left = 12
            Top = 113
            Width = 17
            Height = 13
            Caption = 'City'
            FocusControl = dedtCity
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel49: TevLabel
            Left = 12
            Top = 152
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = dedtState
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel50: TevLabel
            Left = 148
            Top = 152
            Width = 15
            Height = 13
            Caption = 'Zip'
            FocusControl = dedtZip_Code
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel51: TevLabel
            Left = 12
            Top = 191
            Width = 33
            Height = 13
            Caption = 'County'
            FocusControl = dedtCounty
          end
          object evLabel52: TevLabel
            Left = 12
            Top = 230
            Width = 68
            Height = 13
            Caption = 'Primary Phone'
            FocusControl = dedtPrimary_Phone
          end
          object evLabel53: TevLabel
            Left = 148
            Top = 230
            Width = 45
            Height = 13
            Caption = 'extension'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel54: TevLabel
            Left = 12
            Top = 269
            Width = 85
            Height = 13
            Caption = 'Secondary Phone'
            FocusControl = dedtSecondary_Phone
          end
          object evLabel55: TevLabel
            Left = 148
            Top = 269
            Width = 45
            Height = 13
            Caption = 'extension'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel56: TevLabel
            Left = 12
            Top = 308
            Width = 69
            Height = 13
            Caption = 'Tertiary Phone'
            FocusControl = dedtThird_Phone
          end
          object evLabel57: TevLabel
            Left = 148
            Top = 308
            Width = 45
            Height = 13
            Caption = 'extension'
            FocusControl = dedtPrimary_Phone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8816262
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtAlternate_Address_1: TevDBEdit
            Left = 12
            Top = 50
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'ADDRESS1'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Address_2: TevDBEdit
            Left = 12
            Top = 89
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'ADDRESS2'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_City: TevDBEdit
            Left = 12
            Top = 128
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'CITY'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_State: TevDBEdit
            Left = 12
            Top = 167
            Width = 129
            Height = 21
            HelpContext = 17734
            CharCase = ecUpperCase
            DataField = 'STATE'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&,@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Zip_Code: TevDBEdit
            Left = 148
            Top = 167
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'ZIP_CODE'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_County: TevDBEdit
            Left = 12
            Top = 206
            Width = 265
            Height = 21
            HelpContext = 17734
            DataField = 'COUNTY'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Primary_Phone: TevDBEdit
            Left = 12
            Top = 245
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'PHONE1'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Secondary_Phone: TevDBEdit
            Left = 12
            Top = 284
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'PHONE2'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Third_Phone: TevDBEdit
            Left = 12
            Top = 323
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'PHONE3'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Third_Phone_Description: TevDBEdit
            Left = 148
            Top = 323
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'DESCRIPTION3'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Secondary_Phone_Description: TevDBEdit
            Left = 148
            Top = 284
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'DESCRIPTION2'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAlternate_Primary_Phone_Description: TevDBEdit
            Left = 148
            Top = 245
            Width = 129
            Height = 21
            HelpContext = 17734
            DataField = 'DESCRIPTION1'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fp: TisUIFashionPanel
          Left = 8
          Top = 381
          Width = 612
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Additional Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablEmail_Address: TevLabel
            Left = 148
            Top = 35
            Width = 28
            Height = 13
            Caption = 'E-mail'
            FocusControl = dedtEmail_Address
          end
          object evLabel28: TevLabel
            Left = 148
            Top = 74
            Width = 73
            Height = 13
            Caption = 'VMR Password'
            FocusControl = edWebPassword
          end
          object evDBRadioGroup71: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 129
            Height = 76
            Caption = '~Print Voucher'
            DataField = 'PRINT_VOUCHER'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object dedtEmail_Address: TevDBEdit
            Left = 148
            Top = 50
            Width = 298
            Height = 21
            HelpContext = 17734
            DataField = 'E_MAIL_ADDRESS'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            UsePictureMask = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edWebPassword: TevDBEdit
            Left = 148
            Top = 89
            Width = 298
            Height = 21
            HelpContext = 17734
            DataField = 'WEB_PASSWORD'
            DataSource = wwdsSubMaster2
            PasswordChar = '*'
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            UsePictureMask = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
      end
    end
    object tshtW2: TTabSheet
      HelpContext = 17756
      Caption = 'W2'
      ImageIndex = 13
      object sbW2: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlPrimary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 298
          Height = 324
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'W2 Information'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablW2_First_Name: TevLabel
            Left = 12
            Top = 152
            Width = 50
            Height = 13
            Caption = 'First Name'
            FocusControl = dedtW2_First_Name
          end
          object lablW2_Middle_Name: TevLabel
            Left = 12
            Top = 191
            Width = 62
            Height = 13
            Caption = 'Middle Name'
            FocusControl = dedtW2_Middle_Name
          end
          object lablW2_Last_Name: TevLabel
            Left = 12
            Top = 230
            Width = 51
            Height = 13
            Caption = 'Last Name'
            FocusControl = dedtW2_Last_Name
          end
          object lablW2_Name_Suffix: TevLabel
            Left = 12
            Top = 269
            Width = 57
            Height = 13
            Caption = 'Name Suffix'
            FocusControl = dedtW2_Name_Suffix
          end
          object lablW2_Social_Security_Number: TevLabel
            Left = 12
            Top = 35
            Width = 119
            Height = 16
            Caption = '~Social Security Number'
            FocusControl = dedtW2_Social_Security_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablW2_Type: TevLabel
            Left = 12
            Top = 114
            Width = 95
            Height = 16
            Caption = '~Annual Form Type'
            FocusControl = wwcbW2_Type
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtW2_First_Name: TevDBEdit
            Left = 12
            Top = 167
            Width = 265
            Height = 21
            DataField = 'W2_FIRST_NAME'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtW2_Middle_Name: TevDBEdit
            Left = 12
            Top = 206
            Width = 265
            Height = 21
            DataField = 'W2_MIDDLE_NAME'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtW2_Last_Name: TevDBEdit
            Left = 12
            Top = 245
            Width = 265
            Height = 21
            DataField = 'W2_LAST_NAME'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtW2_Name_Suffix: TevDBEdit
            Left = 12
            Top = 284
            Width = 265
            Height = 21
            DataField = 'W2_NAME_SUFFIX'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtW2_Social_Security_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 265
            Height = 21
            Color = clBtnFace
            DataField = 'SOCIAL_SECURITY_NUMBER'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object drgpTreat_SS_EIN: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 128
            Height = 36
            HelpContext = 17720
            Caption = '~EIN or SSN'
            Columns = 2
            DataField = 'EIN_OR_SOCIAL_SECURITY_NUMBER'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'EIN'
              'SSN')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'E'
              'S')
            OnChange = drgpTreat_SS_EINChange
          end
          object drgpTreat_Company_Individual: TevDBRadioGroup
            Left = 149
            Top = 74
            Width = 128
            Height = 36
            HelpContext = 17721
            Caption = '~1099 or Employee'
            Columns = 2
            DataField = 'COMPANY_OR_INDIVIDUAL_NAME'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              '1099'
              'W-2')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'C'
              'I')
            OnChange = drgpTreat_Company_IndividualChange
          end
          object wwcbW2_Type: TevDBComboBox
            Left = 12
            Top = 129
            Width = 265
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'W2_TYPE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Federal'#9'F'
              'Foreign'#9'O'
              'Puerto Rico'#9'P')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
        end
        object isUIFashionPanel3: TisUIFashionPanel
          Left = 314
          Top = 8
          Width = 200
          Height = 324
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'Settings'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel27: TevLabel
            Left = 12
            Top = 230
            Width = 89
            Height = 16
            Caption = '~Residential State'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object drgpW2_Deceased: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 166
            Height = 36
            Caption = '~Deceased'
            Columns = 2
            DataField = 'W2_DECEASED'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpW2_Statutory_Employee: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 166
            Height = 36
            Caption = '~Statutory Employee'
            Columns = 2
            DataField = 'W2_STATUTORY_EMPLOYEE'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpW2_Legal_Rep: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 166
            Height = 36
            Caption = '~Legal Rep'
            Columns = 2
            DataField = 'W2_LEGAL_REP'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpW2_Deferred_Comp: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 166
            Height = 36
            Caption = '~Deferred Compensation'
            Columns = 2
            DataField = 'W2_DEFERRED_COMP'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpW2_Pension: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 166
            Height = 36
            Caption = '~Pension'
            Columns = 2
            DataField = 'W2_PENSION'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
          object evGroupBox1: TevGroupBox
            Left = 22
            Top = 359
            Width = 166
            Height = 193
            Caption = '1099R'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 7
            Visible = False
          end
          object drgpI9_File: TevDBRadioGroup
            Left = 12
            Top = 269
            Width = 166
            Height = 36
            HelpContext = 17765
            Caption = '~I-9 on File'
            Columns = 2
            DataField = 'I9_ON_FILE'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object cbResidentalState: TevDBLookupCombo
            Left = 12
            Top = 245
            Width = 166
            Height = 21
            HelpContext = 20010
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'RESIDENTIAL_STATE_NBR'
            DataSource = wwdsSubMaster2
            LookupTable = DM_SY_STATES.SY_STATES
            LookupField = 'SY_STATES_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
        object fp1099R: TisUIFashionPanel
          Left = 522
          Top = 8
          Width = 200
          Height = 324
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fp1099R'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = '1099R'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel10: TevLabel
            Left = 12
            Top = 35
            Width = 80
            Height = 13
            Caption = 'Distribution Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel11: TevLabel
            Left = 12
            Top = 152
            Width = 71
            Height = 16
            Caption = '~Pension Plan'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evDBEdit3: TevDBEdit
            Left = 12
            Top = 50
            Width = 166
            Height = 21
            DataField = 'DISTRIBUTION_CODE_1099R'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 166
            Height = 36
            Caption = '~Tax Amount Determined'
            Columns = 2
            DataField = 'TAX_AMT_DETERMINED_1099R'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup3: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 166
            Height = 36
            Caption = '~Total Distribution'
            Columns = 2
            DataField = 'TOTAL_DISTRIBUTION_1099R'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBComboBox3: TevDBComboBox
            Left = 12
            Top = 167
            Width = 166
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PENSION_PLAN_1099R'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'IRA'#9'I'
              'SEP'#9'S'
              'Simple'#9'M')
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
        end
      end
    end
    object tshtFederal_Tax: TTabSheet
      Caption = 'Federal'
      ImageIndex = 10
      object sbFederal: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlPrimaryFederal: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 233
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Federal Taxation'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablOverride_Fed_Tax_Type: TevLabel
            Left = 12
            Top = 35
            Width = 118
            Height = 16
            Caption = '~Override Fed Tax Type'
            FocusControl = wwcbOverride_Fed_Tax_Type
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablOverride_Fed_Tax_Value: TevLabel
            Left = 12
            Top = 74
            Width = 112
            Height = 13
            Caption = 'Override Fed Tax Value'
            FocusControl = dedtOverride_Fed_Tax_Value
          end
          object Label55: TevLabel
            Left = 12
            Top = 113
            Width = 17
            Height = 13
            Caption = 'EIC'
          end
          object evLabel31: TevLabel
            Left = 12
            Top = 152
            Width = 116
            Height = 13
            Caption = 'FUI Rate Credit Override'
            FocusControl = dedtFUIRateCreditOverride
          end
          object evLabel34: TevLabel
            Left = 12
            Top = 191
            Width = 154
            Height = 13
            Caption = 'Override Federal Minimum Wage'
            FocusControl = dedtOverrideFedMinWage
          end
          object lablDependents: TevLabel
            Left = 12
            Top = 269
            Width = 67
            Height = 16
            Caption = '~Dependents'
            FocusControl = dedtDependents
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object lablEIC: TevLabel
            Left = 12
            Top = 308
            Width = 17
            Height = 13
            Caption = 'EIC'
            FocusControl = wwcbEIC
            Visible = False
          end
          object wwcbOverride_Fed_Tax_Type: TevDBComboBox
            Left = 12
            Top = 50
            Width = 200
            Height = 21
            HelpContext = 17773
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'OVERRIDE_FED_TAX_TYPE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Regular Amount'#9'A'
              'Regular Percent'#9'P'
              'Additional Amount'#9'M'
              'Additional Percent'#9'E')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object dedtOverride_Fed_Tax_Value: TevDBEdit
            Left = 12
            Top = 89
            Width = 200
            Height = 21
            HelpContext = 17774
            DataField = 'OVERRIDE_FED_TAX_VALUE'
            DataSource = wwdsSubMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwDBComboBox7: TevDBComboBox
            Left = 12
            Top = 128
            Width = 200
            Height = 21
            HelpContext = 18012
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'EIC'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Single'#9'S'
              'Married'#9'M'
              'Married Both Spouses Filing for EIC'#9'B')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object dedtFUIRateCreditOverride: TevDBEdit
            Left = 12
            Top = 167
            Width = 200
            Height = 21
            HelpContext = 17774
            Color = clBtnFace
            DataField = 'FUI_RATE_CREDIT_OVERRIDE'
            DataSource = wwdsSubMaster
            ReadOnly = True
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
          end
          object dedtOverrideFedMinWage: TevDBEdit
            Left = 12
            Top = 206
            Width = 200
            Height = 21
            HelpContext = 17762
            DataField = 'OVERRIDE_FEDERAL_MINIMUM_WAGE'
            DataSource = wwdsSubMaster
            PopupMenu = pmOverrideFedMinWage
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object drgpFederal_Marital_Status: TevDBRadioGroup
            Left = 12
            Top = 230
            Width = 200
            Height = 36
            HelpContext = 17770
            Caption = '~Federal Marital Status'
            Columns = 2
            DataField = 'FEDERAL_MARITAL_STATUS'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Single'
              'Married')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'S'
              'M')
            Visible = False
          end
          object dedtDependents: TevDBEdit
            Left = 12
            Top = 284
            Width = 64
            Height = 21
            HelpContext = 17771
            DataField = 'NUMBER_OF_DEPENDENTS'
            DataSource = wwdsSubMaster
            TabOrder = 6
            UnboundDataType = wwDefault
            Visible = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbEIC: TevDBComboBox
            Left = 12
            Top = 323
            Width = 200
            Height = 21
            HelpContext = 17772
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'EIC'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Single'#9'S'
              'Married'#9'M'
              'Married Both Spouses Filing for EIC'#9'B')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
            Visible = False
          end
        end
        object pnlEffectiveDateFederal: TisUIFashionPanel
          Left = 249
          Top = 8
          Width = 307
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Use Effective Dates to Change Tax Statuses'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object drgpER_OASDI: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 274
            Height = 36
            HelpContext = 17775
            Caption = '~ER OASDI Exempt'
            Columns = 2
            DataField = 'EXEMPT_EMPLOYER_OASDI'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpEmployee_OASDIChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpER_Medicare: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 274
            Height = 36
            HelpContext = 17778
            Caption = '~ER Medicare Exempt'
            Columns = 2
            DataField = 'EXEMPT_EMPLOYER_MEDICARE'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpEmployee_OASDIChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpEmployer_FUI: TevDBRadioGroup
            Left = 12
            Top = 230
            Width = 274
            Height = 36
            HelpContext = 17779
            Caption = '~ER FUI Exempt'
            Columns = 2
            DataField = 'EXEMPT_EMPLOYER_FUI'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpEmployee_OASDIChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpEmployee_OASDI: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 274
            Height = 36
            HelpContext = 17776
            Caption = '~EE OASDI Exempt'
            Columns = 2
            DataField = 'EXEMPT_EMPLOYEE_OASDI'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpEmployee_OASDIChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpEmployee_Medicare: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 274
            Height = 36
            HelpContext = 17777
            Caption = '~EE Medicare Exempt'
            Columns = 2
            DataField = 'EXEMPT_EMPLOYEE_MEDICARE'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpEmployee_OASDIChange
            AutoPopupEffectiveDateDialog = True
          end
          object drgpEE_Federal: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 274
            Height = 36
            HelpContext = 17780
            Caption = '~EE Federal'
            Columns = 3
            DataField = 'EXEMPT_EXCLUDE_EE_FED'
            DataSource = wwdsSubMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Exempt'
              'Block'
              'Include')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'E'
              'X'
              'I')
            OnChange = drgpEE_FederalChange
            AutoPopupEffectiveDateDialog = True
          end
        end
      end
    end
    object Notes: TTabSheet
      Caption = 'Notes'
      ImageIndex = 11
      object sbNotes: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlPayrollNotes: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 674
          Height = 220
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payroll Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object dmemNotes: TEvDBMemo
            Left = 16
            Top = 35
            Width = 633
            Height = 160
            DataField = 'NOTES'
            DataSource = wwdsSubMaster
            TabOrder = 0
          end
        end
        object pnlGeneralNotes: TisUIFashionPanel
          Left = 8
          Top = 236
          Width = 674
          Height = 220
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'General Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object EvDBMemo1: TEvDBMemo
            Left = 16
            Top = 35
            Width = 633
            Height = 160
            DataField = 'SECONDARY_NOTES'
            DataSource = wwdsSubMaster
            TabOrder = 0
          end
        end
      end
    end
    object tshtHR: TTabSheet
      Caption = 'HR'
      ImageIndex = 7
      object sbHR: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object lablUnion: TevLabel
          Left = 901
          Top = 442
          Width = 28
          Height = 13
          Caption = 'Union'
          Visible = False
        end
        object pnlPicture: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 198
          Height = 325
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Picture'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPicture: TevLabel
            Left = 12
            Top = 74
            Width = 33
            Height = 13
            Caption = 'Picture'
            FocusControl = dimgPicture
          end
          object dimgPicture: TevDBImage
            Left = 12
            Top = 89
            Width = 161
            Height = 169
            HelpContext = 17737
            DataField = 'PICTURE'
            DataSource = wwdsSubMaster2
            Stretch = True
            TabOrder = 0
          end
          object bbtnLoad: TevBitBtn
            Left = 12
            Top = 35
            Width = 161
            Height = 21
            Caption = 'Load Picture'
            TabOrder = 1
            OnClick = bbtnLoadClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCDEDEDEFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC29E77B68550B5834FB5834EB6834FB6834FB6834FB583
              4EB5834FB58551BFA07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99999980807F7F
              7E7E7F7E7E7F7E7E7F7E7E7F7E7E7F7E7E7F7E7E80807F9C9C9CFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFBF8447FFFADEFFF4D5FFF4D6FFF5D6FFF5D7FFF6D6FFF5
              D6FFF4D5FFFADFB58551FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80807FF6F6F5F0
              EFEFF0EFEFF0F0F0F1F1F1F1F1F1F0F0F0F0EFEFF6F6F580807FDEDEDECCCCCC
              CCCCCCCCCCCCCCCCCCC08141FFF6DB4BB48D25A77F29A9802BAA812BAC8127A9
              7F4CB58DFFF6DCB5834EDEDEDECCCCCCCCCCCCCCCCCCCCCCCC7D7C7CF3F2F2A9
              A8A89B9B9B9D9D9D9E9E9EA09F9F9D9D9DAAA9A9F3F2F27F7E7E75ACD14398D2
              4094D03D93D13394DBBF7F3FFFF6DD2DBD9037BF933DC29545C796255D8E42C6
              9530BF91FFF6DFB5834EB3B3B3A5A5A5A1A1A1A1A1A1A5A5A57B7B7BF3F2F2B0
              AFAFB2B2B2B5B5B5B9B9B9686868B8B8B8B2B1B1F3F3F37F7E7E4499D23F94D0
              ABFBFF9AF4FF8AF6FFBB7B3DFFF6E1FFE4AFFFE5B1FFE9B4C4CFA11B8780C3CE
              A0FFE8B1FFF8E4B4834DA6A6A6A1A1A1FAF9F9F3F3F3F5F5F5777676F3F3F3DD
              DDDDDEDEDEE1E1E1C6C6C6828282C5C5C5E0E0E0F6F6F57E7E7D4397D156ACDD
              8EDAF5A1EFFF7BECFFB8793BFFF7E5FFDCA1FFDDA4FFE1A7139E8222A384139E
              81FFE1A5FFF9E9B3824CA4A4A4B6B6B5DEDEDEF0F0F0EDEDED757474F5F5F5D4
              D4D4D6D6D6D9D9D9959594999999959594D9D9D9F7F7F77D7C7C4296D171C4EA
              6CBCE6BBF4FF6FE5FFB8793BFFFBEBFFD790FFD894FFDB9A48B08F22AA8D48B0
              8EFFDA98FFFCF1B3824CA3A3A3CBCBCBC4C4C4F5F5F5E8E7E7757474FAF9F9CE
              CECECFCFCFD3D2D2A6A6A6A09F9FA6A6A6D2D2D1FBFBFB7D7C7C4095D090DDF8
              44A0D8DCFDFFD4FFFFB97A3BFFFFF014B9FF76C5DFFFD58BC6CA9216B09AC4C9
              91FFD38BFFFFFAB3824CA2A2A2E1E1E1ACACACFEFDFDFEFEFE767675FEFDFDC8
              C8C7C9C8C8CCCCCCC0C0C0A7A7A7BFBFBFCACACAFFFFFF7D7C7C3E93CFB2F6FF
              51ACDE348BCD2C8FD7BC7C3CFFFFFB7AC9D51BCAFFFFCD7BFFCE81FFCE82FFCD
              81FFCB80FFFFFFB3824CA1A0A0F6F6F5B6B6B59B9B9BA1A0A0787877FFFFFFCA
              C9C9D3D3D3C4C3C3C5C5C5C5C5C5C4C4C4C3C2C2FFFFFF7D7C7C3D92CFB8F3FF
              77DFFE7AE1FF74E5FFBF7C3BFFF7DFFFF5DAFFF4DBFFF4DFFFF4E2FFF5E2FFF4
              E2FFF5E2FFFAE6B5854FA09F9FF4F4F4E4E3E3E5E5E5E8E7E7787877F4F4F4F1
              F1F1F1F1F1F2F2F1F3F2F2F3F3F3F3F2F2F3F3F3F7F7F780807F3C92CFC0F3FF
              70D9FB73DAFC6FDEFFA1A087BE7B3BBB7B3BBB7B3BBA7B3CB97B3DB87A3DB87B
              3EBC7F40BD8346C2925EA09F9FF4F4F4DEDEDEDFDFDEE3E3E29C9C9C77767677
              76767776767776767776767676757776767B7B7B7E7E7D8D8D8D3B92CFCAF6FF
              69D5F96CD5F969D6FC65D9FF62DBFF60DBFF60DBFF60DBFF60DBFF5EDBFFCFFE
              FF3094DCFFFFFFFFFFFFA09F9FF7F7F7DBDADADBDADADCDCDCDFDFDEE0E0E0E0
              E0E0E0E0E0E0E0E0E0E0E0E0E0E0FEFDFDA5A5A5FFFFFFFFFFFF3B92CFD5F7FF
              60D1F961D0F8B4EBFDD8F7FFD9F9FFD9FAFFD9FAFFD9FAFFD9F9FFD8FAFFDDFE
              FF3B94D3FFFFFFFFFFFFA09F9FF8F8F8D7D7D7D7D6D6EEEEEEF9F9F8FAFAFAFB
              FBFBFBFBFBFBFBFBFAFAFAFAFAFAFEFEFEA2A2A2FFFFFFFFFFFF3D94D0DCFCFF
              D8F7FFD8F7FFDBFAFF358ECD3991CE3A92CF3A92CF3A92CF3A92CF3A92CF3D94
              D086BDE3FFFFFFFFFFFFA1A1A1FDFDFCF9F9F8F9F9F8FBFBFB9D9D9D9F9F9EA0
              9F9FA09F9FA09F9FA09F9FA09F9FA1A1A1C4C4C4FFFFFFFFFFFF84BCE23D94D0
              3A92CF3A92CF3D94D089BEE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFC3C2C2A1A1A1A09F9FA09F9FA1A1A1C5C5C5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpVisa: TisUIFashionPanel
          Left = 8
          Top = 517
          Width = 198
          Height = 130
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Non-Resident Visa'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablVisa_Number: TevLabel
            Left = 12
            Top = 35
            Width = 60
            Height = 13
            Caption = 'Visa Number'
            FocusControl = dedtVisa_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel16: TevLabel
            Left = 12
            Top = 74
            Width = 47
            Height = 13
            Caption = 'Visa Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel17: TevLabel
            Left = 95
            Top = 74
            Width = 72
            Height = 13
            Caption = 'Expiration Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtVisa_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 166
            Height = 21
            HelpContext = 17763
            DataField = 'VISA_NUMBER'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox2: TevDBComboBox
            Left = 12
            Top = 89
            Width = 79
            Height = 21
            HelpContext = 17730
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'VISA_TYPE'
            DataSource = wwdsSubMaster2
            DropDownCount = 8
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 0
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            OnExit = evDBComboBox2Exit
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 95
            Top = 89
            Width = 79
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'VISA_EXPIRATION_DATE'
            DataSource = wwdsSubMaster2
            Epoch = 1950
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 2
          end
        end
        object fpSecurity: TisUIFashionPanel
          Left = 8
          Top = 341
          Width = 198
          Height = 168
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSecurity'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Security'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablSecurity_Clearance: TevLabel
            Left = 12
            Top = 74
            Width = 89
            Height = 13
            Caption = 'Security Clearance'
            FocusControl = dedtSecurity_Clearance
          end
          object lablSupervisor: TevLabel
            Left = 12
            Top = 113
            Width = 50
            Height = 13
            Caption = 'Supervisor'
            FocusControl = wwlcSupervisor
          end
          object lablBadge_ID: TevLabel
            Left = 12
            Top = 35
            Width = 45
            Height = 13
            Caption = 'Badge ID'
            FocusControl = dedtBadge_ID
          end
          object dedtSecurity_Clearance: TevDBEdit
            Left = 12
            Top = 89
            Width = 161
            Height = 21
            HelpContext = 17739
            DataField = 'SECURITY_CLEARANCE'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcSupervisor: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 161
            Height = 21
            HelpContext = 17740
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'SUPERVISOR_NAME'#9'40'#9'SUPERVISOR_NAME')
            DataField = 'CO_HR_SUPERVISORS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_HR_SUPERVISORS.CO_HR_SUPERVISORS
            LookupField = 'CO_HR_SUPERVISORS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object dedtBadge_ID: TevDBEdit
            Left = 12
            Top = 50
            Width = 161
            Height = 21
            HelpContext = 17738
            DataField = 'BADGE_ID'
            DataSource = wwdsSubMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object pnlLifeEvent: TisUIFashionPanel
          Left = 558
          Top = 517
          Width = 313
          Height = 131
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlLifeEvent'
          Color = 14737632
          TabOrder = 7
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Life Event'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel40: TevLabel
            Left = 12
            Top = 35
            Width = 80
            Height = 13
            Caption = 'Qualifying Event '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel42: TevLabel
            Left = 12
            Top = 74
            Width = 103
            Height = 13
            Caption = 'Qualifying Event Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evcbLastQualBenefitEvent: TevDBComboBox
            Left = 12
            Top = 50
            Width = 280
            Height = 21
            HelpContext = 17730
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'LAST_QUAL_BENEFIT_EVENT'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 0
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evdtLastQualBenefitEventDate: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 280
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'LAST_QUAL_BENEFIT_EVENT_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 1
          end
        end
        object pnlPosition: TisUIFashionPanel
          Left = 214
          Top = 8
          Width = 332
          Height = 325
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlPosition'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Position'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablRecruiter: TevLabel
            Left = 12
            Top = 74
            Width = 43
            Height = 13
            Caption = 'Recruiter'
            FocusControl = wwlcRecruiter
          end
          object lablReferral: TevLabel
            Left = 12
            Top = 35
            Width = 37
            Height = 13
            Caption = 'Referral'
            FocusControl = wwlcReferral
          end
          object lablPerformance_Rating: TevLabel
            Left = 12
            Top = 190
            Width = 94
            Height = 13
            Caption = 'Performance Rating'
            FocusControl = wwlcPerformance_Rating
          end
          object lablReview_Date: TevLabel
            Left = 12
            Top = 229
            Width = 62
            Height = 13
            Caption = 'Review Date'
          end
          object lablNext_Review_Date: TevLabel
            Left = 167
            Top = 229
            Width = 87
            Height = 13
            Caption = 'Next Review Date'
          end
          object evLabel24: TevLabel
            Left = 12
            Top = 152
            Width = 50
            Height = 13
            Caption = 'Citizenship'
          end
          object evLabel15: TevLabel
            Left = 167
            Top = 152
            Width = 48
            Height = 13
            Caption = 'Language'
          end
          object evLabel6: TevLabel
            Left = 12
            Top = 113
            Width = 50
            Height = 13
            Caption = 'EEO Code'
          end
          object wwlcRecruiter: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 300
            Height = 21
            HelpContext = 17741
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CO_HR_RECRUITERS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_HR_RECRUITERS.CO_HR_RECRUITERS
            LookupField = 'CO_HR_RECRUITERS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcReferral: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 300
            Height = 21
            HelpContext = 17742
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_HR_REFERRALS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_HR_REFERRALS.CO_HR_REFERRALS
            LookupField = 'CO_HR_REFERRALS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcPerformance_Rating: TevDBLookupCombo
            Left = 12
            Top = 205
            Width = 300
            Height = 21
            HelpContext = 17749
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION')
            DataField = 'CO_HR_PERFORMANCE_RATINGS_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_HR_PERFORMANCE_RATINGS.CO_HR_PERFORMANCE_RATINGS
            LookupField = 'CO_HR_PERFORMANCE_RATINGS_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBDateTimePicker11: TevDBDateTimePicker
            Left = 12
            Top = 244
            Width = 145
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'REVIEW_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 6
          end
          object wwDBDateTimePicker12: TevDBDateTimePicker
            Left = 167
            Top = 244
            Width = 145
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'NEXT_REVIEW_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 7
          end
          object evDBEdit4: TevDBEdit
            Left = 12
            Top = 167
            Width = 145
            Height = 21
            HelpContext = 17763
            DataField = 'CITIZENSHIP'
            DataSource = wwdsSubMaster2
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox8: TevDBComboBox
            Left = 167
            Top = 167
            Width = 145
            Height = 21
            HelpContext = 17733
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Column1Width = 150
            DataField = 'NATIVE_LANGUAGE'
            DataSource = wwdsSubMaster2
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'African American'#9'N'
              'Hispanic'#9'H'
              'Asian'#9'A'
              'American Indian'#9'I'
              'Caucasian'#9'C'
              'Other'#9'O'
              'Not Applicable'#9'P')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
            OnCloseUp = wwDBComboBox6CloseUp
          end
          object wwlcEEOCode: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 300
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'SY_HR_EEO_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_SY_HR_EEO.SY_HR_EEO
            LookupField = 'SY_HR_EEO_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object btnApplyForNewPosition: TevBitBtn
            Left = 12
            Top = 274
            Width = 298
            Height = 21
            Caption = 'Apply for New Position'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnClick = btnApplyForNewPositionClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpAutomobile: TisUIFashionPanel
          Left = 214
          Top = 341
          Width = 332
          Height = 244
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAutomobile'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Automobile'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablAuto_Insurance_Carrier: TevLabel
            Left = 12
            Top = 113
            Width = 80
            Height = 13
            Caption = 'Insurance Carrier'
            FocusControl = dedtAuto_Insurance_Carrier
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAuto_Insurance_Policy_Number: TevLabel
            Left = 12
            Top = 152
            Width = 118
            Height = 13
            Caption = 'Insurance Policy Number'
            FocusControl = dedtAuto_Insurance_Policy_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDrivers_License_Number: TevLabel
            Left = 12
            Top = 35
            Width = 113
            Height = 13
            Caption = 'Drivers License Number'
            FocusControl = dedtDrivers_License_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDrivers_License_Exp_Date: TevLabel
            Left = 12
            Top = 74
            Width = 68
            Height = 13
            Caption = 'D/L Expiration'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TevLabel
            Left = 12
            Top = 191
            Width = 77
            Height = 13
            Caption = 'Policy Expiration'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtAuto_Insurance_Carrier: TevDBEdit
            Left = 12
            Top = 128
            Width = 300
            Height = 21
            HelpContext = 17747
            DataField = 'AUTO_INSURANCE_CARRIER'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAuto_Insurance_Policy_Number: TevDBEdit
            Left = 12
            Top = 167
            Width = 300
            Height = 21
            HelpContext = 17748
            DataField = 'AUTO_INSURANCE_POLICY_NUMBER'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtDrivers_License_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 300
            Height = 21
            HelpContext = 17746
            DataField = 'DRIVERS_LICENSE_NUMBER'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object drgpReliable_Car: TevDBRadioGroup
            Left = 167
            Top = 191
            Width = 145
            Height = 36
            HelpContext = 17744
            Caption = 'Reliable Vehicle'
            Columns = 2
            DataField = 'RELIABLE_CAR'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 300
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DRIVERS_LICENSE_EXP_DATE'
            DataSource = wwdsSubMaster2
            Epoch = 1950
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 1
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 12
            Top = 206
            Width = 145
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'AUTO_INSURANCE_POLICY_EXP_DATE'
            DataSource = wwdsSubMaster2
            Epoch = 1950
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 4
          end
        end
        object fpService: TisUIFashionPanel
          Left = 554
          Top = 8
          Width = 313
          Height = 325
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpService'
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Service'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablVeteran_Discharge_Date: TevLabel
            Left = 12
            Top = 74
            Width = 74
            Height = 13
            Caption = 'Discharge Date'
          end
          object drgpVeteran: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 280
            Height = 36
            HelpContext = 17751
            Caption = '~Veteran'
            Columns = 3
            DataField = 'VETERAN'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Applicable')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
          object wwDBDateTimePicker13: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 137
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'VETERAN_DISCHARGE_DATE'
            DataSource = wwdsSubMaster2
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object drgpVietnam_Veteran: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 280
            Height = 36
            HelpContext = 17753
            Caption = '~Vietnam Veteran'
            Columns = 3
            DataField = 'VIETNAM_VETERAN'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Applicable')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
          object drgpDisabled_Veteran: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 280
            Height = 36
            HelpContext = 17754
            Caption = '~Disabled Veteran'
            Columns = 3
            DataField = 'DISABLED_VETERAN'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Applicable')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
          object drgpMilitary_Reserve: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 280
            Height = 36
            HelpContext = 17755
            Caption = '~Military Reserve'
            Columns = 3
            DataField = 'MILITARY_RESERVE'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Applicable')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
          object evRGServiceMedalVeteran: TevDBRadioGroup
            Left = 12
            Top = 230
            Width = 280
            Height = 36
            HelpContext = 17755
            Caption = '~Service Medal Veteran'
            Columns = 3
            DataField = 'SERVICE_MEDAL_VETERAN'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Applicable')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
          object evRGOtherProtectedVeteran: TevDBRadioGroup
            Left = 12
            Top = 269
            Width = 280
            Height = 36
            HelpContext = 17755
            Caption = '~Other Protected Veteran'
            Columns = 3
            DataField = 'OTHER_PROTECTED_VETERAN'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Not Applicable')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N'
              'A')
          end
        end
        object fpBenefits: TisUIFashionPanel
          Left = 554
          Top = 341
          Width = 313
          Height = 168
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpBenefits'
          Color = 14737632
          TabOrder = 6
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Benefits'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel43: TevLabel
            Left = 157
            Top = 35
            Width = 80
            Height = 13
            Caption = 'Wellness Rating '
            FocusControl = evDBLookupCombo10
          end
          object evLabel32: TevLabel
            Left = 12
            Top = 35
            Width = 61
            Height = 13
            Caption = 'Last Election'
            FocusControl = dtLastElection
          end
          object evLabel39: TevLabel
            Left = 12
            Top = 74
            Width = 65
            Height = 13
            Caption = 'Benefit Group'
            FocusControl = evDBLookupCombo10
          end
          object evDBLookupCombo10: TevDBLookupCombo
            Left = 157
            Top = 50
            Width = 135
            Height = 21
            HelpContext = 17740
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F'
              'AMOUNT'#9'10'#9'Amount'#9'F')
            DataField = 'CO_BENEFIT_DISCOUNT_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_BENEFIT_DISCOUNT.CO_BENEFIT_DISCOUNT
            LookupField = 'CO_BENEFIT_DISCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object dtLastElection: TevDBDateTimePicker
            Left = 12
            Top = 50
            Width = 135
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'BENEFIT_ENROLLMENT_COMPLETE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            ReadOnly = True
            ShowButton = True
            TabOrder = 0
          end
          object drgpSmoker: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 280
            Height = 36
            HelpContext = 17764
            Caption = '~Smoker'
            Columns = 2
            DataField = 'SMOKER'
            DataSource = wwdsSubMaster2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object cbCoGroupMember_benefit: TevComboBox
            Left = 12
            Top = 89
            Width = 135
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 2
            OnChange = cbCoGroupMember_benefitChange
            OnCloseUp = cbCoGroupMember_benefitCloseUp
          end
        end
        object wwlcUnion: TevDBLookupCombo
          Left = 901
          Top = 457
          Width = 135
          Height = 21
          HelpContext = 17743
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'UNION_NAME'#9'27'#9'UNION_NAME')
          DataField = 'CO_UNIONS_NBR'
          DataSource = wwdsSubMaster
          LookupTable = DM_CO_UNIONS.CO_UNIONS
          LookupField = 'CO_UNIONS_NBR'
          Style = csDropDownList
          TabOrder = 8
          Visible = False
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object isUIFashionPanel6: TisUIFashionPanel
          Left = 216
          Top = 590
          Width = 329
          Height = 75
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 9
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'PBJ Orientation Days '
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblPBJWorks: TevLabel
            Left = 62
            Top = 39
            Width = 200
            Height = 13
            Caption = 'Number of days after Hire Date work starts'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edtPBJDays: TevDBSpinEdit
            Left = 12
            Top = 35
            Width = 41
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'PBJ_ORIENTATION_DAYS'
            DataSource = wwdsSubMaster
            MaxLength = 2
            PopupMenu = pmPBJWorks
            TabOrder = 0
            UnboundDataType = wwDefault
          end
        end
      end
    end
    object tsACA: TTabSheet
      Caption = 'ACA'
      ImageIndex = 39
      object isUIFashionPanel2: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 323
        Height = 248
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'ACA'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evLabel29: TevLabel
          Left = 12
          Top = 35
          Width = 63
          Height = 16
          Caption = '~ACA Status'
          FocusControl = cmbACAStatus
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel30: TevLabel
          Left = 168
          Top = 35
          Width = 98
          Height = 13
          Caption = 'ACA Standard Hours'
          FocusControl = edtACAStandardHours
        end
        object evLabel58: TevLabel
          Left = 12
          Top = 73
          Width = 63
          Height = 13
          Caption = 'ACA Benefit  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel61: TevLabel
          Left = 168
          Top = 73
          Width = 100
          Height = 13
          Caption = 'Lowest Cost Benefit  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel60: TevLabel
          Left = 12
          Top = 112
          Width = 94
          Height = 13
          Caption = 'ACA Policy Origin    '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel67: TevLabel
          Left = 12
          Top = 191
          Width = 93
          Height = 16
          Caption = '~Safe Harbor Type'
          FocusControl = cbACASafeHarborType
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cmbACAStatus: TevDBComboBox
          Left = 12
          Top = 50
          Width = 148
          Height = 21
          HelpContext = 20009
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'ACA_STATUS'
          DataSource = wwdsSubMaster
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          PopupMenu = pmAcaStatus
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object edtACAStandardHours: TevDBEdit
          Left = 168
          Top = 50
          Width = 135
          Height = 21
          HelpContext = 18009
          DataField = 'ACA_STANDARD_HOURS'
          DataSource = wwdsSubMaster
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object evLowestCoBenefits: TevDBComboBox
          Left = 13
          Top = 88
          Width = 148
          Height = 21
          HelpContext = 20009
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 2
          UnboundDataType = wwDefault
          OnChange = evLowestCoBenefitsChange
          OnCloseUp = evLowestCoBenefitsCloseUp
        end
        object evcbBenefitSubType: TevDBLookupCombo
          Left = 168
          Top = 88
          Width = 135
          Height = 21
          HelpContext = 19591
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Description'#9'40'#9'Description')
          DataField = 'ACA_CO_BENEFIT_SUBTYPE_NBR'
          DataSource = wwdsSubMaster
          LookupField = 'CO_BENEFIT_SUBTYPE_NBR'
          Style = csDropDownList
          PopupMenu = pmAcaLowestCost
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object evDBComboBox10: TevDBComboBox
          Left = 12
          Top = 127
          Width = 289
          Height = 21
          HelpContext = 20009
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'ACA_POLICY_ORIGIN'
          DataSource = wwdsSubMaster
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          PopupMenu = pmAcaPolicyOrigin
          Sorted = False
          TabOrder = 4
          UnboundDataType = wwDefault
        end
        object rgBenefitsEligible: TevDBRadioGroup
          Left = 12
          Top = 150
          Width = 289
          Height = 36
          HelpContext = 17787
          Caption = '~Benefits Eligible'
          Columns = 2
          DataField = 'BENEFITS_ELIGIBLE'
          DataSource = wwdsSubMaster
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          PopupMenu = pmBenefitsEligible
          TabOrder = 5
          Values.Strings = (
            'Y'
            'N')
        end
        object cbACASafeHarborType: TevDBComboBox
          Left = 12
          Top = 206
          Width = 148
          Height = 21
          HelpContext = 20009
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'ACA_SAFE_HARBOR_TYPE'
          DataSource = wwdsSubMaster
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 6
          UnboundDataType = wwDefault
        end
      end
      object isUIFashionPanel5: TisUIFashionPanel
        Left = 8
        Top = 263
        Width = 323
        Height = 196
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Reporting'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evLabel64: TevLabel
          Left = 13
          Top = 74
          Width = 65
          Height = 16
          Caption = '~ACA Format'
          FocusControl = dtLastElection
        end
        object evLabel66: TevLabel
          Left = 12
          Top = 113
          Width = 59
          Height = 16
          Caption = '~Form Type'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cbAcaFormat: TevDBComboBox
          Left = 13
          Top = 89
          Width = 167
          Height = 21
          HelpContext = 20009
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'ACA_FORMAT'
          DataSource = wwdsSubMaster
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          PopupMenu = pmAcaFormat
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object cbFormType: TevDBComboBox
          Left = 13
          Top = 128
          Width = 167
          Height = 21
          HelpContext = 20009
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'ACA_TYPE'
          DataSource = wwdsSubMaster
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          PopupMenu = pmAcaFormType
          Sorted = False
          TabOrder = 1
          UnboundDataType = wwDefault
          OnChange = evLowestCoBenefitsChange
          OnCloseUp = evLowestCoBenefitsCloseUp
        end
        object evDBRadioGroup7: TevDBRadioGroup
          Left = 12
          Top = 35
          Width = 167
          Height = 35
          HelpContext = 17787
          Caption = '~Form on File'
          Columns = 2
          DataField = 'ACA_FORM_ON_FILE'
          DataSource = wwdsSubMaster
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 2
          Values.Strings = (
            'Y'
            'N')
        end
      end
      object isUIFashionPanel7: TisUIFashionPanel
        Left = 335
        Top = 8
        Width = 497
        Height = 480
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 2
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'ACA History'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lblACA: TevLabel
          Left = 12
          Top = 34
          Width = 96
          Height = 13
          Caption = 'ACA Coverage Offer'
          FocusControl = dtLastElection
        end
        object evLabel59: TevLabel
          Left = 12
          Top = 73
          Width = 79
          Height = 13
          Caption = 'ACA Relief Code'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel62: TevLabel
          Left = 181
          Top = 34
          Width = 23
          Height = 13
          Caption = 'DOB'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel63: TevLabel
          Left = 181
          Top = 73
          Width = 45
          Height = 13
          Caption = 'Hire Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel65: TevLabel
          Left = 181
          Top = 115
          Width = 50
          Height = 13
          Caption = 'Term Date'
        end
        object btnNavCancel: TevSpeedButton
          Tag = 11
          Left = 273
          Top = 433
          Width = 65
          Height = 28
          Hint = 'Cancel changes'
          Caption = 'Cancel'
          Enabled = False
          HideHint = True
          AutoSize = False
          OnClick = btnACAHistoryClick
          NumGlyphs = 2
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDCCCCCCCCCCCCCCCCCCCC
            CCCCCCCCCCCCCCCCCCCCCCEDEDEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            EDEDED9499C82C3CC02B3BBE2B3ABE2B3ABE2B3ABE2B3BBE2C3CC09499C8EDED
            EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDA5A5A56C6C6C6B6B6B6B6B6B6B
            6B6B6B6B6B6B6B6B6C6C6CA5A5A5EDEDEDFFFFFFFFFFFFFFFFFFFFFFFFEDEDED
            969BC92F3EC35F71F9697DFF697CFF697CFF697CFF697DFF5F71F92F3EC3969B
            C9EDEDEDFFFFFFFFFFFFFFFFFFEDEDEDA7A7A76E6E6E9E9E9EA6A6A6A6A6A6A6
            A6A6A6A6A6A6A6A69E9E9E6E6E6EA7A7A7EDEDEDFFFFFFFFFFFFEDEDED969BC9
            2F3EC2586BF65F74FF5D72FE5E72FD5E73FD5E72FD5D72FE5F74FF586BF62F3E
            C2969BC9EDEDEDFFFFFFEDEDEDA7A7A76E6E6E999999A1A1A1A09F9FA09F9FA0
            9F9FA09F9FA09F9FA1A1A19999996E6E6EA7A7A7EDEDEDFFFFFF9499C8303FC2
            5568F3586CFC4E64F94D63F85468F9576BF95468F94D63F84E64F9586CFC5568
            F3303FC29499C8FFFFFFA5A5A56E6E6E9796969C9C9C9797979796969999999A
            9A9A9999999796969797979C9C9C9796966E6E6EA5A5A5FFFFFF2D3DC05367F2
            556BFA4960F7FFFFFFFFFFFF3E56F6475EF63E56F6FFFFFFFFFFFF4960F7556B
            FA5166F22D3DC0FFFFFF6D6D6D9695959B9B9B959594FFFFFFFFFFFF90909093
            9393909090FFFFFFFFFFFF9595949B9B9B9595946D6D6DFFFFFF2B3BBF6276FC
            4D64F64259F4FFFFFFFFFFFFFFFFFF2C46F3FFFFFFFFFFFFFFFFFF4259F44E64
            F65F75FC2C3BBFFFFFFF6B6B6BA1A1A1969595909090FFFFFFFFFFFFFFFFFF88
            8888FFFFFFFFFFFFFFFFFF909090969595A1A0A06B6B6BFFFFFF2A3ABF7386FA
            495FF3435AF36E80F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E80F6435AF3495F
            F36E81FA2B3ABFFFFFFF6B6B6BAAA9A9929292909090A4A4A4FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFA4A4A4909090929292A7A7A76B6B6BFFFFFF2939BF8696FB
            425AF14259F1354EF05B70F2FFFFFFFFFFFFFFFFFF5B70F2354EF04259F1435B
            F17D90F92A39BFFFFFFF6B6B6BB3B3B38F8F8F8F8F8F8989899A9A9AFFFFFFFF
            FFFFFFFFFF9A9A9A8989898F8F8F909090AFAFAF6B6B6BFFFFFF2737BF9AA8FB
            3A55EF3953EE2844EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2844ED3953EE3B55
            EF8E9DFA2838BFFFFFFF6A6A6ABFBFBF8C8C8C8A8A8A858484FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF8584848A8A8A8C8C8CB8B7B76A6A6AFFFFFF2637BF9FABF1
            314CED2B47EBFFFFFFFFFFFFFFFFFF5369EFFFFFFFFFFFFFFFFFFF2C47EB314C
            ED9FABF12737BFFFFFFF6A6A6ABEBDBD878787858484FFFFFFFFFFFFFFFFFF96
            9595FFFFFFFFFFFFFFFFFF858484878787BEBDBD6A6A6AFFFFFF2838C19FABF1
            8091F4213EE8FFFFFFFFFFFF5D72EE2340E85D72EEFFFFFFFFFFFF213EE88091
            F49FABF12838C1FFFFFF6B6B6BBEBDBDAEAEAE818080FFFFFFFFFFFF99999981
            8080999999FFFFFFFFFFFF818080AEAEAEBEBDBD6B6B6BFFFFFFB4BAEA2E3EC3
            97A5EF778AF25B71EE6074EE2643E62C48E72643E66074EE5B71EE778AF297A5
            EF2E3EC3B4BAEAFFFFFFC6C6C66E6E6EB9B9B9A9A8A89999999A9A9A81808083
            83838180809A9A9A999999A9A8A8B9B9B96E6E6EC6C6C6FFFFFFFFFFFFB6BCEA
            2E3EC295A2EE7688F01E3BE42340E52541E52340E51E3BE47688F095A2EE2E3E
            C2B6BCEAFFFFFFFFFFFFFFFFFFC8C8C76E6E6EB7B6B6A7A7A77E7E7D80807F80
            807F80807F7E7E7DA7A7A7B7B6B66E6E6EC8C8C7FFFFFFFFFFFFFFFFFFFFFFFF
            B6BCEA2F3DC394A0EFADB9F8ADB8F7ADB9F7ADB8F7ADB9F894A0EF2F3DC3B6BC
            EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C76E6E6EB6B6B5CAC9C9C9C8C8CA
            C9C9C9C8C8CAC9C9B6B6B56E6E6EC8C8C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFB4BAEA303FC44555CE4454CD4354CD4454CD4555CE303FC4B4BAEAFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C66F6F6F7E7E7D7D7C7C7D
            7C7C7D7C7C7E7E7D6F6F6FC6C6C6FFFFFFFFFFFFFFFFFFFFFFFF}
          Flat = True
          ParentColor = False
          ShortCut = 0
        end
        object btnACAPost: TevSpeedButton
          Tag = 10
          Left = 208
          Top = 433
          Width = 65
          Height = 28
          Hint = 'Post changes'
          Caption = 'Save'
          Enabled = False
          HideHint = True
          AutoSize = False
          OnClick = btnACAPostClick
          NumGlyphs = 2
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFF7F9F8D6E8E0BEDDCEB9D9CAC9E2D6E7F1ECFFFEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAECECECE0
            E0E0DDDDDDE4E4E4F3F3F3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFBFCFCBEDDCF6CBF9A3AB48026B27521B1722FB47A4EB88993CCB2E3EF
            EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE1E1E1C3C3C3B7B7B7B3
            B3B3B2B2B2B6B6B6BBBBBBD0D0D0F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            F3F8F683C7A626AE6D18B8701EC17A23C58024C68022C57E1CBF791AB47049B6
            83C8E3D6FFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CBCBCBB0B0B0B6B6B6BDBDBDC0
            C0C0C2C2C2C0C0C0BCBCBCB3B3B3B9B9B9E5E5E5FFFFFFFFFFFFFFFFFFFAFBFB
            83C5A313A75919B86A20BD7420BE7620BD7721BF7822C07922C07A21C17916B8
            6D3BB176CFE6DAFFFFFFFFFFFFFCFCFCC8C8C8A6A6A6B4B4B4B9B9B9BABABABC
            BCBCBBBBBBBBBBBBBDBDBDBCBCBCB4B4B4B2B2B2E8E8E8FFFFFFFFFFFFBFDDCE
            20A05914AD581CB4651AB56617B05F1AAF5E18B5681FBB711FBB711FBC721FBD
            7213B06363B88EF4F7F6FFFFFFE0E0E0A1A1A1A7A7A7AFAFAFB1B1B1ABABABAA
            AAAAB1B1B1B7B7B7B7B7B7B8B8B8B8B8B8ADADADBDBDBDF8F8F8F6FBF968B78E
            0DA14B18AA5514A85316A3507DC798AFDDBE48B77511AC5A1CB76A1EB76B1EB8
            6A19B66623A762C1DED0FEFCFDB9B9B99C9C9CA4A4A4A3A3A39D9D9DC3C3C3DA
            DADAB2B2B2A7A7A7B2B2B2B2B2B2B2B2B2B1B1B1A8A8A8E1E1E1D7EAE135A96B
            14AB5516A9541A9E4E85C49AF3F9F3F7FBF8DFEFE459B87D12A7531AB2621CB3
            641BB36313AA5B8AC9AAF3F0F1AAA9A9A5A5A5A3A3A3999999C1C1C1F7F7F7FB
            FBFBEFEFEFB5B5B5A2A2A2ADADADAEAEAEAFAFAFA7A7A7CCCCCCBEDDD021AB67
            1AB25E13AD576FC693FFFEFFA8DCBB4DB775E3F3E8EEF5EF67BC8511A24D17AE
            5B1AAF5D11AF5E69C198E7E5E6ACABABADADADA7A7A7C3C3C3FEFEFED9D9D9B2
            B2B2F1F1F1F5F5F5B8B8B89D9D9DA9A9A9AAAAAAACACACC3C3C3B9DBCC1FB170
            1CB66419B5643AC27D84D9AE3BBF7B0BB05857C98BEBF9F1F3F7F477C2911BA9
            5818AD5A11B16062C097E4E3E3B1B0B0B1B1B1B1B1B1BDBDBDD7D7D7BCBCBCAB
            ABABC6C6C6F9F9F9F7F7F7BFBFBFA5A5A5A7A7A7ADADADC3C3C3C9E2D72DB378
            1CBB6D20BD731EC0761ABF761EC17A20C47D18C17669D6A4F2FCF7F8FCF846C4
            8215B25F12AD5B77C5A0ECEAECB5B4B4B7B7B7BABABABCBCBCBBBBBBBEBEBEC0
            C0C0BDBDBDD3D3D3FBFBFBFBFBFBC0C0C0ADADADAAAAAAC8C8C8E7F1EC4DB887
            1ABA7125C48126C88A27CB8F27CD9228CE9426CD9320CB8C81E1BCDAF5E840CF
            921AC37818B066A2D3BBF9F7F8B9B9B9B7B7B7C1C1C1C5C5C5C8C8C8CACACACB
            CBCBCCCCCCC8C8C8DFDFDFF4F4F4CBCBCBBEBEBEAEAEAED7D7D7FEFEFE94CDB3
            1AB67126CA8C2BD09A2DD4A02FD6A630D7A82FD7A92BD7A530D5A362DEB530D1
            9517C47A3DB681DFECE6FFFFFFD0D0D0B5B5B5C7C7C7CDCDCDD0D0D0D3D3D3D5
            D5D5D6D6D6D3D3D3D3D3D3DCDCDCCDCDCDBFBFBFB9B9B9EFEFEFFFFFFFE3EFE9
            4CBD8E1FC88C30D7A833DAAF35DDB437DFB838DFB937DFB833DCB22FDAAC28D4
            9C1FBD7F99D1B8FEFEFEFFFFFFF1F1F1C0C0C0C6C6C6D5D5D5D8D8D8DBDBDBDD
            DDDDDDDDDDDDDDDDDBDBDBD8D8D8D0D0D0BDBDBDD5D5D5FEFEFEFFFFFFFFFFFF
            C8E4D842BF9529D2A637E3BF3DE6C43DE6C63FE6C63EE6C63DE6C332DEB627C7
            977FCAAEF5F8F6FFFFFFFFFFFFFFFFFFE7E7E7C2C2C2D1D1D1E0E0E0E4E4E4E4
            E4E4E4E4E4E4E4E4E4E4E4DDDDDDC8C8C8CFCFCFF9F9F9FFFFFFFFFFFFFFFFFF
            FFFEFFCFE6DC6BC6A83CCBA83CDABC40E3C940E6CC3FE0C33BD3B34CC4A29BD2
            BEF5F7F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9CBCBCBCDCDCDDBDBDBE3
            E3E3E5E5E5E0E0E0D5D5D5C9C9C9D7D7D7F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF4F7F3C3E0D393D1BC79CAB173C8AF84CDB6A7D8C5DEEBE4FFFD
            FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E2E2E2D5D5D5CE
            CECECCCCCCD2D2D2DBDBDBEDEDEDFEFEFEFFFFFFFFFFFFFFFFFF}
          Flat = True
          ParentColor = False
          ShortCut = 0
        end
        object lbACAInitial: TevLabel
          Left = 299
          Top = 34
          Width = 133
          Height = 13
          Caption = 'Initial Measurement Period   '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblStability: TevLabel
          Left = 299
          Top = 115
          Width = 78
          Height = 13
          Caption = 'Stability Period   '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblACAMeasurementData: TevLabel
          Left = 299
          Top = 52
          Width = 54
          Height = 13
          Caption = '                  '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblACAStabilityData: TevLabel
          Left = 299
          Top = 134
          Width = 21
          Height = 13
          Caption = '       '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object grACAHistory: TevDBGrid
          Left = 13
          Top = 161
          Width = 463
          Height = 252
          DisableThemesInTitle = False
          ControlType.Strings = (
            'Month;CustomEdit;cbMonth;T'
            'SY_FED_ACA_OFFER_CODES_NBR;CustomEdit;cbACA_COVERAGE_OFFER;T'
            'SY_FED_ACA_RELIEF_CODES_NBR;CustomEdit;cbEE_ACA_SAFE_HARBOR;T')
          Selected.Strings = (
            'Month'#9'9'#9'Month'#9'T'#9
            'SY_FED_ACA_OFFER_CODES_NBR'#9'30'#9'ACA Coverage Offer'#9'F'
            'SY_FED_ACA_RELIEF_CODES_NBR'#9'30'#9'ACA Relief Code '#9'F')
          IniAttributes.Enabled = False
          IniAttributes.SaveToRegistry = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TevVersionedField\grFieldData'
          IniAttributes.Delimiter = ';;'
          IniAttributes.CheckNewFields = False
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsACAHistory
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          DefaultSort = '-'
          Sorting = False
          NoFire = False
        end
        object cbMonth: TevDBComboBox
          Left = 64
          Top = 256
          Width = 100
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'Month'
          DataSource = dsACAHistory
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'January'#9'1'
            'February'#9'2'
            'March'#9'3'
            'April'#9'4'
            'May'#9'5'
            'June'#9'6'
            'July'#9'7'
            'August'#9'8'
            'September'#9'9'
            'October'#9'10'
            'November'#9'11'
            'December'#9'12')
          Picture.PictureMaskFromDataSet = False
          ReadOnly = True
          Sorted = False
          TabOrder = 1
          UnboundDataType = wwDefault
          Visible = False
        end
        object AcaHistoryYear: TevComboBox
          Left = 12
          Top = 131
          Width = 62
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          OnChange = btnACAHistoryClick
          Items.Strings = (
            '2014'
            '2015'
            '2016'
            '2017'
            '2018'
            '2019'
            '2020'
            '2021'
            '2022'
            '2023'
            '2024'
            '2025')
        end
        object btnACAHistory: TevBitBtn
          Left = 95
          Top = 128
          Width = 68
          Height = 25
          Caption = 'ACA History'
          TabOrder = 3
          OnClick = btnACAHistoryClick
          Color = clBlack
          Margin = 0
        end
        object edACADOB: TevDBDateTimePicker
          Left = 181
          Top = 48
          Width = 100
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.YearsPerColumn = 20
          CalendarAttributes.PopupYearOptions.NumberColumns = 5
          CalendarAttributes.PopupYearOptions.StartYear = 1930
          Color = clBtnFace
          DataField = 'BIRTH_DATE'
          DataSource = wwdsSubMaster2
          Epoch = 1900
          ShowButton = True
          TabOrder = 4
        end
        object edACAHireDAte: TevDBDateTimePicker
          Left = 181
          Top = 88
          Width = 100
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Color = clBtnFace
          DataField = 'CURRENT_HIRE_DATE'
          DataSource = wwdsSubMaster
          Epoch = 1950
          ShowButton = True
          TabOrder = 5
        end
        object edACATerm: TevDBDateTimePicker
          Left = 181
          Top = 130
          Width = 100
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Color = clBtnFace
          DataField = 'CURRENT_TERMINATION_DATE'
          DataSource = wwdsSubMaster
          Epoch = 1950
          ShowButton = True
          TabOrder = 6
        end
        object cbACACovarage: TevDBLookupCombo
          Left = 12
          Top = 48
          Width = 135
          Height = 21
          HelpContext = 19591
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'44'#9'Description'#9'F')
          DataField = 'SY_FED_ACA_OFFER_CODES_NBR'
          DataSource = wwdsSubMaster
          LookupField = 'SY_FED_ACA_OFFER_CODES_NBR'
          Style = csDropDownList
          PopupMenu = pmAcaCoverage
          TabOrder = 7
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object cbACAReliefCode: TevDBLookupCombo
          Left = 12
          Top = 88
          Width = 135
          Height = 21
          HelpContext = 19591
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'44'#9'Description'#9'F')
          DataField = 'SY_FED_ACA_RELIEF_CODES_NBR'
          DataSource = wwdsSubMaster
          LookupField = 'SY_FED_ACA_RELIEF_CODES_NBR'
          Style = csDropDownList
          PopupMenu = pmAcaSafeHarbor
          TabOrder = 8
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
      end
      object cbACA_COVERAGE_OFFER: TevDBComboBox
        Left = 792
        Top = 304
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SY_FED_ACA_OFFER_CODES_NBR'
        DataSource = dsACAHistory
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
        Visible = False
        OnChange = cbACA_COVERAGE_OFFERChange
        OnDropDown = cbACA_COVERAGE_OFFERDropDown
        OnExit = cbACA_COVERAGE_OFFERExit
      end
      object cbEE_ACA_SAFE_HARBOR: TevDBComboBox
        Left = 792
        Top = 336
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SY_FED_ACA_RELIEF_CODES_NBR'
        DataSource = dsACAHistory
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
        Visible = False
        OnChange = cbACA_COVERAGE_OFFERChange
        OnDropDown = cbEE_ACA_SAFE_HARBORDropDown
        OnExit = cbEE_ACA_SAFE_HARBORExit
      end
    end
    object tsDocuments: TTabSheet
      Caption = 'Documents'
      ImageIndex = 14
      TabVisible = False
      object sbDocs: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        inline TPersonDocuments1: TPersonDocuments
          Left = 0
          Top = 0
          Width = 1187
          Height = 633
          Align = alClient
          TabOrder = 0
          inherited pnlDocsBackground: TevPanel
            Height = 633
            inherited evPanel1: TevPanel
              Top = 534
              inherited fpControls: TisUIFashionPanel
                inherited pnlDocs2: TPanel
                  inherited btnSaveDoc: TevBitBtn
                    Glyph.Data = {
                      36060000424D3606000000000000360000002800000020000000100000000100
                      18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFF7F9F8D6E8E0BEDDCEB9D9CAC9E2D6E7F1ECFFFEFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAECECECE0
                      E0E0DDDDDDE4E4E4F3F3F3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFBFCFCBEDDCF6CBF9A3AB48026B27521B1722FB47A4EB88993CCB2E3EF
                      EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE1E1E1C3C3C3B7B7B7B3
                      B3B3B2B2B2B6B6B6BBBBBBD0D0D0F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      F3F8F683C7A626AE6D18B8701EC17A23C58024C68022C57E1CBF791AB47049B6
                      83C8E3D6FFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CBCBCBB0B0B0B6B6B6BDBDBDC0
                      C0C0C2C2C2C0C0C0BCBCBCB3B3B3B9B9B9E5E5E5FFFFFFFFFFFFFFFFFFFAFBFB
                      83C5A313A75919B86A20BD7420BE7620BD7721BF7822C07922C07A21C17916B8
                      6D3BB176CFE6DAFFFFFFFFFFFFFCFCFCC8C8C8A6A6A6B4B4B4B9B9B9BABABABC
                      BCBCBBBBBBBBBBBBBDBDBDBCBCBCB4B4B4B2B2B2E8E8E8FFFFFFFFFFFFBFDDCE
                      20A05914AD581CB4651AB56617B05F1AAF5E18B5681FBB711FBB711FBC721FBD
                      7213B06363B88EF4F7F6FFFFFFE0E0E0A1A1A1A7A7A7AFAFAFB1B1B1ABABABAA
                      AAAAB1B1B1B7B7B7B7B7B7B8B8B8B8B8B8ADADADBDBDBDF8F8F8F6FBF968B78E
                      0DA14B18AA5514A85316A3507DC798AFDDBE48B77511AC5A1CB76A1EB76B1EB8
                      6A19B66623A762C1DED0FEFCFDB9B9B99C9C9CA4A4A4A3A3A39D9D9DC3C3C3DA
                      DADAB2B2B2A7A7A7B2B2B2B2B2B2B2B2B2B1B1B1A8A8A8E1E1E1D7EAE135A96B
                      14AB5516A9541A9E4E85C49AF3F9F3F7FBF8DFEFE459B87D12A7531AB2621CB3
                      641BB36313AA5B8AC9AAF3F0F1AAA9A9A5A5A5A3A3A3999999C1C1C1F7F7F7FB
                      FBFBEFEFEFB5B5B5A2A2A2ADADADAEAEAEAFAFAFA7A7A7CCCCCCBEDDD021AB67
                      1AB25E13AD576FC693FFFEFFA8DCBB4DB775E3F3E8EEF5EF67BC8511A24D17AE
                      5B1AAF5D11AF5E69C198E7E5E6ACABABADADADA7A7A7C3C3C3FEFEFED9D9D9B2
                      B2B2F1F1F1F5F5F5B8B8B89D9D9DA9A9A9AAAAAAACACACC3C3C3B9DBCC1FB170
                      1CB66419B5643AC27D84D9AE3BBF7B0BB05857C98BEBF9F1F3F7F477C2911BA9
                      5818AD5A11B16062C097E4E3E3B1B0B0B1B1B1B1B1B1BDBDBDD7D7D7BCBCBCAB
                      ABABC6C6C6F9F9F9F7F7F7BFBFBFA5A5A5A7A7A7ADADADC3C3C3C9E2D72DB378
                      1CBB6D20BD731EC0761ABF761EC17A20C47D18C17669D6A4F2FCF7F8FCF846C4
                      8215B25F12AD5B77C5A0ECEAECB5B4B4B7B7B7BABABABCBCBCBBBBBBBEBEBEC0
                      C0C0BDBDBDD3D3D3FBFBFBFBFBFBC0C0C0ADADADAAAAAAC8C8C8E7F1EC4DB887
                      1ABA7125C48126C88A27CB8F27CD9228CE9426CD9320CB8C81E1BCDAF5E840CF
                      921AC37818B066A2D3BBF9F7F8B9B9B9B7B7B7C1C1C1C5C5C5C8C8C8CACACACB
                      CBCBCCCCCCC8C8C8DFDFDFF4F4F4CBCBCBBEBEBEAEAEAED7D7D7FEFEFE94CDB3
                      1AB67126CA8C2BD09A2DD4A02FD6A630D7A82FD7A92BD7A530D5A362DEB530D1
                      9517C47A3DB681DFECE6FFFFFFD0D0D0B5B5B5C7C7C7CDCDCDD0D0D0D3D3D3D5
                      D5D5D6D6D6D3D3D3D3D3D3DCDCDCCDCDCDBFBFBFB9B9B9EFEFEFFFFFFFE3EFE9
                      4CBD8E1FC88C30D7A833DAAF35DDB437DFB838DFB937DFB833DCB22FDAAC28D4
                      9C1FBD7F99D1B8FEFEFEFFFFFFF1F1F1C0C0C0C6C6C6D5D5D5D8D8D8DBDBDBDD
                      DDDDDDDDDDDDDDDDDBDBDBD8D8D8D0D0D0BDBDBDD5D5D5FEFEFEFFFFFFFFFFFF
                      C8E4D842BF9529D2A637E3BF3DE6C43DE6C63FE6C63EE6C63DE6C332DEB627C7
                      977FCAAEF5F8F6FFFFFFFFFFFFFFFFFFE7E7E7C2C2C2D1D1D1E0E0E0E4E4E4E4
                      E4E4E4E4E4E4E4E4E4E4E4DDDDDDC8C8C8CFCFCFF9F9F9FFFFFFFFFFFFFFFFFF
                      FFFEFFCFE6DC6BC6A83CCBA83CDABC40E3C940E6CC3FE0C33BD3B34CC4A29BD2
                      BEF5F7F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9CBCBCBCDCDCDDBDBDBE3
                      E3E3E5E5E5E0E0E0D5D5D5C9C9C9D7D7D7F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFF4F7F3C3E0D393D1BC79CAB173C8AF84CDB6A7D8C5DEEBE4FFFD
                      FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E2E2E2D5D5D5CE
                      CECECCCCCCD2D2D2DBDBDBEDEDEDFEFEFEFFFFFFFFFFFFFFFFFF}
                  end
                  inherited btnDeleteDoc: TevBitBtn
                    Glyph.Data = {
                      36060000424D3606000000000000360000002800000020000000100000000100
                      18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFF6F6FAD6D6EEBCBCE5B7B7E3C7C7E8E6E6F4FFFFFEFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAECECECE0
                      E0E0DDDDDDE4E4E4F3F3F3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFBFBFDBDBDE66666CF3333C71D1DC51818C42626C54747C98F8FD9E3E3
                      F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE1E1E1C3C3C3B7B7B7B3
                      B3B3B2B2B2B6B6B6BBBBBBD0D0D0F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      F3F3F98181D41E1EC10D0DC91313D01818D21919D41616D31010CF1010C74343
                      C8C6C6EAFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CBCBCBB0B0B0B6B6B6BDBDBDC0
                      C0C0C2C2C2C0C0C0BCBCBCB3B3B3B9B9B9E5E5E5FFFFFFFFFFFFFFFFFFF9F9FC
                      8080D30B0BBD0F0FCA1515CD1515CE1616CE1717CF1616CF1515CF1515CF0B0B
                      C83434C3CECEECFFFFFFFFFFFFFCFCFCC8C8C8A6A6A6B4B4B4B9B9B9BABABABC
                      BCBCBBBBBBBBBBBBBCBCBCBCBCBCB4B4B4B2B2B2E8E8E8FFFFFFFFFFFFBCBCE5
                      1919B70C0CC11313C61515C41313C11212C61313CB1212C51515C51616CA1616
                      CC0909C35E5EC9F3F3F9FFFFFFE0E0E0A1A1A1A7A7A7AFAFAFADADADA9A9A9AF
                      AFAFB5B5B5AFAFAFAEAEAEB6B6B6B9B9B9ADADADBDBDBDF8F8F8F7F7FB6464C8
                      0707B61010BD0D0DBD5757CDAEAEE44F4FCA0E0EBD8585DAA5A5E42B2BCB1010
                      C71010C81B1BBDBFBFE7FEFEFCB9B9B99C9C9CA4A4A4A3A3A3BABABADADADAB6
                      B6B6A3A3A3CBCBCBDADADAB6B6B6B1B1B1B1B1B1A8A8A8E1E1E1D6D6EF3030BD
                      0C0CBE1010BD0B0BBA2A2ABFD3D3EFCACAEA7474CBFAFAFB9595E11212C31212
                      C51313C50B0BBF8686D6F3F3F0AAAAA9A5A5A5A4A4A49F9F9FA7A7A7E9E9E9E3
                      E3E3B8B8B8F9F9F9D6D6D6ABABABAEAEAEAFAFAFA7A7A7CCCCCCBCBCE61A1AC0
                      1111C51212C31212C10404BA5151CAEFEFF9F7F7F9C8C8EC2323C20B0BBF1212
                      C21212C20909C26464D0E7E7E6ACACABADADADAAAAAAA8A8A89F9F9FB5B5B5F6
                      F6F6F7F7F7E5E5E5AAAAAAA7A7A7AAAAAAAAAAAAACACACC3C3C3B7B7E31616C4
                      1313C81313C81414C80D0DC72323C4D4D4F0FFFFFF9292DA0A0ABD1111C21212
                      C21111C10808C45E5ED0E4E4E3B1B1B0B1B1B1B2B2B2B3B3B3B1B1B1AEAEAEEB
                      EBEBFFFFFFCECECEA5A5A5ACACACAAAAAAA7A7A7ADADADC3C3C3C7C7E92525C5
                      1313CB1616CD1515CF0F0FCB8383DFF4F4FBDADAF6E4E4F64040D20D0DCD1515
                      CD1313C60909C17272D3ECECEAB5B5B4B7B7B7B9B9B9BDBDBDB8B8B8D2D2D2F9
                      F9F9F3F3F3F2F2F2C0C0C0B9B9B9B9B9B9AEAEAEAAAAAAC8C8C8E6E6F44747C9
                      0F0FCB1919D21515D54646DBE1E1F7ABABEF4A4ADFE1E1FAACACEC1D1DD51515
                      D51515D21010C2A0A0DEF9F9F7B9B9B9B7B7B7C1C1C1C4C4C4CDCDCDF4F4F4E9
                      E9E9D2D2D2F7F7F7E5E5E5C4C4C4C4C4C4BFBFBFAEAEAED7D7D7FEFEFE9090D9
                      1010C71A1AD71D1DDA3939E15C5CE93636E31C1CE14949E75858E82727DE1919
                      D90E0ED23636C8DFDFF1FFFFFFD0D0D0B5B5B5C7C7C7CDCDCDD5D5D5DFDFDFD9
                      D9D9D4D4D4DCDCDCDFDFDFD1D1D1CBCBCBC0C0C0B9B9B9EFEFEFFFFFFFE2E2F3
                      4545CD1010D52222E12121E31C1CE32323E62626E61F1FE51E1EE42222E31B1B
                      DE1414CD9595DDFEFEFEFFFFFFF1F1F1C0C0C0C5C5C5D5D5D5D7D7D7D9D9D9DC
                      DCDCDDDDDDDCDCDCD9D9D9D8D8D8D0D0D0BDBDBDD5D5D5FEFEFEFFFFFFFFFFFF
                      C6C6EA3838CE1818DD2525E92A2AEB2A2AEC2A2AEC2B2BED2929EB1F1FE61A1A
                      D47B7BD8F4F4F9FFFFFFFFFFFFFFFFFFE7E7E7C2C2C2D1D1D1E0E0E0E4E4E4E4
                      E4E4E4E4E4E4E4E4E4E4E4DDDDDDC8C8C8CFCFCFF9F9F9FFFFFFFFFFFFFFFFFF
                      FFFFFFCDCDEC6464D42E2ED72727E32929EA2A2AEB2828E82727DE4141D39898
                      DDF5F5FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9CBCBCBCDCDCDDBDBDBE3
                      E3E3E5E5E5E0E0E0D5D5D5C9C9C9D7D7D7F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFF4F4F8C1C1E78E8EDD7171D66A6AD57C7CD9A4A4E1DEDEF0FFFF
                      FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E2E2E2D5D5D5CE
                      CECECCCCCCD2D2D2DBDBDBEDEDEDFEFEFEFFFFFFFFFFFFFFFFFF}
                  end
                end
              end
            end
            inherited pnlDocs1: TPanel
              Height = 534
              ParentColor = True
              inherited pnlGridBorder: TevPanel
                Width = 978
                Height = 422
                inherited fpGridDocuments: TisUIFashionPanel
                  Height = 406
                end
              end
            end
          end
          inherited odDocs: TOpenDialog
            Left = 28
            Top = 40
          end
          inherited sdDocs: TSaveDialog
            Left = 80
            Top = 36
          end
          inherited dsEditDoc: TevDataSource
            DataSet = DM_CL_PERSON_DOCUMENTS.CL_PERSON_DOCUMENTS
            MasterDataSource = wwdsSubMaster2
          end
        end
      end
    end
    object tshtHR_Applicant: TTabSheet
      Caption = 'H/R Applicant'
      TabVisible = False
      object memoHR_Applicant: TMemo
        Left = 144
        Top = 16
        Width = 369
        Height = 89
        HelpContext = 18018
        Lines.Strings = (
          
            'This tab will appear if there is a non-zero - non-null entry in ' +
            'the person'#39's'
          'HR_APPLICANT_NBR column.'
          ''
          
            'The column will be automatically filled in when a new person is ' +
            'added to the'
          'database.')
        TabOrder = 0
      end
    end
    object tsVMR: TTabSheet
      Caption = 'Mail Room'
      ImageIndex = 8
      object sbMailRoom: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 381
          Height = 208
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Mail Room Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel25: TevLabel
            Left = 12
            Top = 152
            Width = 185
            Height = 13
            Caption = 'Override EE Electronic Return Mail Box'
          end
          object evLabel20: TevLabel
            Left = 12
            Top = 35
            Width = 151
            Height = 13
            Caption = 'Override Payroll Check Mail Box'
          end
          object evLabel14: TevLabel
            Left = 12
            Top = 113
            Width = 156
            Height = 13
            Caption = 'Override 2nd EE Report Mail Box'
          end
          object evLabel13: TevLabel
            Left = 12
            Top = 74
            Width = 135
            Height = 13
            Caption = 'Override EE Report Mail Box'
          end
          object evDBLookupCombo7: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 350
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'TAX_EE_RETURN_MB_GROUP_NBR'
            DataSource = wwdsEmployee
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo5: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 350
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
            DataSource = wwdsEmployee
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo4: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 350
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_EE_REPORT_MB_GROUP_NBR'
            DataSource = wwdsEmployee
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 350
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_CHECK_MB_GROUP_NBR'
            DataSource = wwdsEmployee
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
    object tsESS: TTabSheet
      Caption = 'Self Serve'
      ImageIndex = 12
      object sbSelfServe: TScrollBox
        Left = 0
        Top = 0
        Width = 1191
        Height = 637
        Align = alClient
        TabOrder = 0
        object pnlSSBorder: TevPanel
          Left = 0
          Top = 0
          Width = 1187
          Height = 633
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 12
          TabOrder = 0
          object pnlAccessSettings: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 345
            Height = 308
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Access'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evLabel151: TevLabel
              Left = 12
              Top = 222
              Width = 53
              Height = 13
              Caption = 'User Name'
            end
            object evLabel321: TevLabel
              Left = 174
              Top = 222
              Width = 46
              Height = 13
              Caption = 'Password'
            end
            object evGroupBox51: TevGroupBox
              Left = 12
              Top = 35
              Width = 312
              Height = 179
              Caption = '~Access Level'
              PopupMenu = pmAccessLevel
              TabOrder = 0
              object dgrpSS_Enabled: TevDBRadioGroup
                Left = 11
                Top = 18
                Width = 139
                Height = 73
                Caption = '~EE Info'
                DataField = 'SELFSERVE_ENABLED'
                DataSource = wwdsSubMaster
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'No'
                  'Read Only'
                  'Full Access')
                ParentFont = False
                TabOrder = 0
                Values.Strings = (
                  'N'
                  'Y'
                  'F')
                OnChange = dgrpSS_EnabledChange
              end
              object dgrpTimeOff_Enabled: TevDBRadioGroup
                Left = 162
                Top = 18
                Width = 139
                Height = 73
                Caption = '~Time Off'
                DataField = 'TIME_OFF_ENABLED'
                DataSource = wwdsSubMaster
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'No'
                  'Read Only'
                  'Full Access')
                ParentFont = False
                TabOrder = 1
                Values.Strings = (
                  'N'
                  'Y'
                  'F')
                OnChange = dgrpSS_EnabledChange
              end
              object dgrpBenefits_Enabled: TevDBRadioGroup
                Left = 11
                Top = 95
                Width = 139
                Height = 73
                Caption = '~Benefits'
                DataField = 'BENEFITS_ENABLED'
                DataSource = wwdsSubMaster
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'No'
                  'Read Only'
                  'Full Access')
                ParentFont = False
                TabOrder = 2
                Values.Strings = (
                  'N'
                  'Y'
                  'F')
                OnChange = dgrpSS_EnabledChange
              end
              object dgrpDD_Enabled: TevDBRadioGroup
                Left = 162
                Top = 95
                Width = 139
                Height = 73
                Caption = '~Direct Deposit'
                DataField = 'DIRECT_DEPOSIT_ENABLED'
                DataSource = wwdsSubMaster
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'No'
                  'Read Only'
                  'Full Access')
                ParentFont = False
                TabOrder = 3
                Values.Strings = (
                  'N'
                  'Y'
                  'F')
                OnChange = dgrpSS_EnabledChange
              end
            end
            object dedtSS_UserName: TevDBEdit
              Left = 12
              Top = 237
              Width = 150
              Height = 21
              DataField = 'SELFSERVE_USERNAME'
              DataSource = wwdsSubMaster
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object editPassword: TevEdit
              Left = 174
              Top = 237
              Width = 150
              Height = 21
              PasswordChar = '*'
              TabOrder = 2
              OnChange = editPasswordChange
            end
            object btnUnblockAcc: TevButton
              Left = 12
              Top = 264
              Width = 312
              Height = 25
              Caption = 'Unblock account'
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              OnClick = btnUnblockAccClick
              Color = clBlack
              Margin = 0
            end
          end
          object pnlGroupAssignments: TisUIFashionPanel
            Left = 8
            Top = 325
            Width = 698
            Height = 253
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'pnlGroupAssignments'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Group Assignments'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evLabel391: TevLabel
              Left = 12
              Top = 35
              Width = 80
              Height = 13
              Caption = 'Avaliable Groups'
            end
            object sbAddESSGroup: TevSpeedButton
              Left = 306
              Top = 115
              Width = 74
              Height = 22
              Caption = 'Add'
              HideHint = True
              AutoSize = False
              OnClick = sbAddESSGroupClick
              NumGlyphs = 2
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
                DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
                DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
                78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
                8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
                F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
                F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
                DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
                DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
              Layout = blGlyphRight
              ParentColor = False
              ShortCut = 0
            end
            object sbDelESSGroup: TevSpeedButton
              Left = 306
              Top = 148
              Width = 74
              Height = 22
              Caption = 'Remove'
              HideHint = True
              AutoSize = False
              OnClick = sbDelESSGroupClick
              NumGlyphs = 2
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
                DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
                DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
                78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
                448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
                F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
                F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
                44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
                DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
              ParentColor = False
              ShortCut = 0
            end
            object evLabel401: TevLabel
              Left = 396
              Top = 35
              Width = 80
              Height = 13
              Caption = 'Assigned Groups'
            end
            object AvaliableGroupsGrid: TevDBGrid
              Left = 12
              Top = 50
              Width = 285
              Height = 180
              DisableThemesInTitle = False
              Selected.Strings = (
                'GROUP_NAME'#9'25'#9'Group Name'#9'F'
                'GroupTypeName'#9'15'#9'Group Type'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_EE_CL_PERSON\AvaliableGroupsGrid'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = AvaliableGroupsDatasource
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
            object AssignedGroupsGrid: TevDBGrid
              Left = 392
              Top = 50
              Width = 285
              Height = 180
              DisableThemesInTitle = False
              Selected.Strings = (
                'GROUP_NAME'#9'25'#9'Group Name'#9'F'
                'GroupTypeName'#9'15'#9'Group Type'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_EE_CL_PERSON\AssignedGroupsGrid'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = AssignedGroupsDataSource
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 1
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
          object isUIFashionPanel4: TisUIFashionPanel
            Left = 361
            Top = 8
            Width = 343
            Height = 309
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 2
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Settings'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evLabel381: TevLabel
              Left = 12
              Top = 166
              Width = 28
              Height = 13
              Caption = 'E-mail'
            end
            object evLabel44: TevLabel
              Left = 12
              Top = 221
              Width = 69
              Height = 13
              Caption = 'Benefits E-mail'
            end
            object gbAnnualReturns: TevGroupBox
              Left = 12
              Top = 35
              Width = 312
              Height = 101
              Caption = 'Annual Returns '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              object evLabel46: TevLabel
                Left = 12
                Top = 52
                Width = 35
                Height = 13
                Caption = 'Format '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object rgW2onfile: TevDBRadioGroup
                Left = 12
                Top = 18
                Width = 286
                Height = 32
                Caption = 'Form On File'
                Columns = 2
                DataField = 'W2_FORM_ON_FILE'
                DataSource = wwdsSubMaster
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                PopupMenu = pmW2FormOnFile
                TabOrder = 0
                OnChange = rgW2onfileChange
              end
              object cbW2Format: TevDBComboBox
                Left = 12
                Top = 66
                Width = 286
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = False
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'W2'
                DataSource = wwdsSubMaster
                DropDownCount = 8
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ItemHeight = 0
                ParentFont = False
                Picture.PictureMaskFromDataSet = False
                PopupMenu = pmW2FormatCopyTo
                Sorted = False
                TabOrder = 1
                UnboundDataType = wwDefault
                OnDropDown = cbW2FormatDropDown
                OnCloseUp = cbW2FormatCloseUp
              end
            end
            object dedtEmail_Address1: TevDBEdit
              Left = 12
              Top = 181
              Width = 312
              Height = 21
              HelpContext = 17734
              DataField = 'E_MAIL_ADDRESS'
              DataSource = wwdsSubMaster
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 1
              UnboundDataType = wwDefault
              UsePictureMask = False
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object evDBEdit8: TevDBEdit
              Left = 12
              Top = 236
              Width = 312
              Height = 21
              HelpContext = 17734
              DataField = 'BENEFIT_E_MAIL_ADDRESS'
              DataSource = wwdsSubMaster
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              UsePictureMask = False
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Top = 51
    Width = 1199
    inherited dblkupEE_NBR: TevDBLookupCombo
      Top = 7
    end
    inherited dbtxtname: TevDBLookupCombo
      Top = 7
    end
    inherited butnPrior: TevBitBtn
      Top = 4
    end
    inherited butnNext: TevBitBtn
      Top = 4
    end
  end
  inline HideRecordHelper: THideRecordHelper [5]
    Left = 376
    Top = 55
    Width = 145
    Height = 27
    AutoScroll = False
    TabOrder = 3
    inherited butnNext: TevBitBtn
      Width = 121
    end
    inherited evBitBtn1: TevBitBtn
      Top = 0
      Width = 121
    end
    inherited evActionList1: TevActionList
      Left = 64
      Top = 16
      inherited HideCurRecord: TAction
        Caption = 'Remove Employee'
      end
      inherited UnhideCurRecord: TAction
        Caption = 'Unremove Employee'
      end
    end
    inherited dsHelper: TevDataSource
      Left = 104
      Top = 16
    end
  end
  inherited wwdsMaster: TevDataSource [6]
    Left = 712
    Top = 82
  end
  inherited wwdsDetail: TevDataSource [7]
    DataSet = DM_EE_STATES.EE_STATES
    OnStateChange = wwdsDetailStateChange
    Left = 1110
    Top = 314
  end
  inherited wwdsList: TevDataSource [8]
    Left = 850
    Top = 82
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY [9]
    Left = 617
    Top = 55
  end
  inherited PageControlImages: TevImageList [10]
    Left = 672
    Top = 64
    Bitmap = {
      494C010128002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B000000001001000000000000058
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000396739673967DE7B0000000000000000000000007B6F
      396739677B6F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A6B396739673967396739673967
      39673967396739673967095609560956F6620000000000000000000000006D62
      085E075E4C5E7B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      3E5378630000000000000000000000000000FD3EDD3ADD3ADD3ADD3ADD3AFD3A
      FE3AFE3AFE3A1F3BE6596E5EA47E327748620000DE7BDE7BDE7BDE7BFF7F2962
      8A6A8B6A29624C5E7B6FDE7B0000000000003967396739673967396739673967
      3967396739673967396739673967396739670000000000000000000000000000
      183318430000000000000000000000000000DD3ABF67BF63BF63BF63DF67D852
      EF3DCE3DCE3DCE3D3142964E136BB57F48625A6B156716671667166717674A62
      967F6A6A6A6A29624C5E16675A6B000000009452734E524A524A524A524A524A
      524A524A524A524A524A524A524A734E94520000000000000000000000000000
      982290220000000000000000000000000000DD3ABF67DF73DF73BF5F3142524E
      5A6F9C739C735A6F734E1142BE7377774866696A686A486A4766476647662962
      6966967F6A6A6A6A0662676E896E5A6BFF7F734E000000000000000000000000
      00000000000000000000000000000000734E000000000000000000000000FE7F
      98321833FE7F000000000000000000000000DD3ABF67DF6FDF73B84E734E9C73
      1D4B3E437E479E579C73734EB65647660000686A476AF57FD37FD27FD27FD27F
      AC6A4966957FB06AF75E113E113E53461863734EFF7FDE7BDE7FDE7FDE7FDE7F
      DE7FDE7FDE7FDE7FDE7FDE7FDE7BFF7F734E0000000000000000000000001853
      901290221853000000000000000000000000DD3ABF6B7F5B9F5B10429C73FC46
      3D475E479F4FBF539E579C73113E00000000486AAA6E717BB47F907F907F907F
      B17FAC6A8A6A7C6F3A675A6BBD779D733246734EDE7BF529F529F529F529F529
      F529F529F529F529F529F529F529DE7B734E0000000000000000000000001833
      184318431843000000000000000000000000DD3ABF6B7F579F5BEF3DDE7FDC3A
      7E5B7E537E4F9F4F7E47DE7F114200000000486A0E77ED72D77F6E7F6E7F6F7F
      8F7F8F7F8F7FD8567B6F3967324652465346734EDE7BF52D5F5B5F5B5F5F5F5F
      5F5B5F5B3F573F575F575F5BF52DDE7B734E0000000000000000000000009012
      184318439012000000000000000000000000DD3ABF6BBF6FDF6FF03DFF7FDC3A
      BE6B7E5B7E535E473D3FFF7F114200000000486A727F886EFB7FFB7FFB7FFB7F
      6E7F6E7F6E7F54465B6BF85E534A2E7FF36A734EDD7BF52D5F5BF42DF52DF52D
      F42D3F57FF7FDD7B9B73596BF529DD7B734E0000000000000000000000009012
      184318339012000000000000000000000000DD3ABF6FBF6BDF6F1142DD7BDC42
      BF73BE6B7E5B3D4B3D4BDD7B3246000000004766D67FAA6E2666266626662666
      EB766D7F6C7F54467C6FB656744E744A954E734EDD7BF52D5F5B5F5B5F5B5F5B
      5F5B3F57FF7FDD7B9B73596BF529DD7B734E0000000000000000000018539822
      184318539022185300000000000000000000DD3ADF735F535F53F85AB5560000
      DC42FC42FC421D4B0000B55A9842000000004766D77F6E7F8F7F8F7F8F7F8F7F
      AA6EEA76FB7F17673A67DE779C6F3A639356734EDE7BF52D5F5FF52D5F5FF52D
      5F5B5F5B3F573F575F575F5BF52DDE7B734E0000000000000000000098221853
      786318531853982200000000000000000000DD36DF735F4F5F4FDF6F744ED65A
      FF7F00000000FF7FD65A9452FE3A000000004766D87F6E7F6E7F6E7F6E7F6E7F
      6E7FA96E266627667156964EB652935AF26E734EFF7FF529F52DF52DF52DF52D
      F52DF529F529F529F529F529F529FF7F734E7863982210021002100210021002
      100210021002100210021002100298227863DD36DF77BF67BF6B3F4F3F4FF95A
      524A31463146524AF956FF77DD3A000000004766D97F4D7F4D7F4D7F4D7F4D7F
      4D7F6D7F6D7F6D7F6C7FFA7F466A9B7B0000734E000000000000000000000000
      00000000000000000000000000000000734EFE7F786318439822100210021002
      18537863100210021002982218437863FE7FDD36FF7B9F679F673F4B3F4BBF67
      BF673F4B3F4BBF67BF67FF7BDD36000000004766DA7F4C7F4C7FB67FDB7FFB7F
      FB7FFB7FFB7FFB7FFB7FFB7F476ABB7B0000F75E734E734E734E734E734E734E
      734E734E734E734E734E734E734E734EF75E0000000000000000FE7F90129022
      1833184390129822FE7F0000000000000000DD3AFF7FFF7BFF7BFF7FFF7FFF7B
      FF7BFF7FFF7FFF7BFF7BFF7FDD3A00000000476AFB7FDB7FDB7FFB7F26664766
      47664766476647664766476A8A6A000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FD42DD3ADD36DD36DD36DD36DD36
      DD36DD36DD36DD36DD36DD3AFD4200000000696A476A47664766476A8A6ABC7F
      BB7BBB7BBB7BBB7BBB7BBA7B0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000005A6B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000396739677B6F0000000000000000AB3DA941496E2A6A0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000BD77422A00000000000000000000000000000000DE7B396739673967
      39673967396739673967DE7B0000000000000000DE7B39673967396739673967
      39673967202A0043683A7B6F0000000000000B4E905A527FEB7EA74D0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00003967DE7B3967396739673967
      39670000D152402E396739673967FF7F0000000000000000B84A550954055405
      54055405540554055509B84A0000000000000000B84A55095505550556055705
      5705390120268053E042883A7B6F00000000A976937F737F647E627EA74D0000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD7720265563603680368036A036
      602EBD77803EC04680368036602E5967000000000000000076095A2E9C369C36
      9C369C369C369C365B2E7609000000000000000076095A2E9C369D36682E2026
      20262026002260536053E042883A7B6F0000C976917FE87E857E637E637EA74D
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD04EA0425663A046005320536053
      C03A335BE052004F204F40538057873A0000000000000000D819FD467B2E7B2E
      7B2A7B2A7B2E7C2EFD46B7110000000000000000D819FD467C2E7E2E0022A763
      60536053404F404F404F404FE042883A0000FF7FE661297FE77EA57E637E637E
      A74D0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F60320053F152ED4E803A8036A03A
      602E00006032C04680368036404BA036000000000000000019263D57BD36BD36
      FE7FDD7BDD36BD363D57D815000000000000000019263D57BD36DF3E001EAD67
      204F204F204F004B204F204F8C6720220000FF7FFF7FE7612A7FE77EA57E637E
      637EA74D0000FF7FFF7FFF7FFF7FFF7FFF7F60360053A03A5A6BDE7B39670000
      00000000DC77402E0000165FAF4A202600000000000000005A2E7E67FE3EDF6F
      FF7FDE7B7D671F3F9E67F91900000000000000005A2E7E671F3FFF77001EB273
      A96BA96B896BB26F004B8C63C042CA420000FF7FFF7FFF7FE7612A7FE77EA57E
      637E637EA74D0000FF7FFF7FFF7FFF7FFF7F6036E04E20536232F35600220000
      000000000000A83E9C7360320047402A00000000000000005A261D4FBF6B7677
      8B62E7516F5ADF6F1D535A2A00000000000000005A261D4FBF6B777B433A001E
      001E001E001E906B6B63A03ECB4200000000FF7FFF7FFF7FFF7FE7612A7FE77E
      A57E637E637EA74D0000FF7FFF7FFF7FFF7FEC4AE04A004F204B404B545F0000
      0000000000000000AD46E04E4053C03A00000000000000000000FD42BE32175F
      6C5EE84D5246BE36FD42000000000000000000000000FD42BE32185F6C5EE951
      544ADF3600228F6FA03ECB42000000000000FF7FFF7FFF7FFF7FFF7FE7612A7F
      E77EA57E637E627EA6510000FF7FFF7FFF7F3967402E20534053A03600000000
      0000000000000000402E0057404F85360000000000000000000000007B6FAF62
      EF6EAD664C569C73000000000000000000000000000000007B6FAF62EF6EAD6A
      4C569C732022A042CA420000000000000000FF7FFF7FFF7FFF7FFF7FFF7FE761
      2A7FE77EA57E627EAF6AF0390000FF7FFF7F202660328036A03A365F39679C73
      DE7B39673967396739676132A03A0000000000000000000000000000CB49937F
      1077CE6EAD6A0D5200000000000000000000000000000000CB49937F1077CE6E
      AD6A0E520000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      E761297FE67EF26E3242D756EF350000FF7F00000000BD7739678A3E603AAC46
      BA732022C046E042602E9C73B96F0000000000000000000000000000C030D57F
      727F30730F73A02C00000000000000000000000000000000C030D57F727F3073
      0F73812C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FE665977B123E5A673142D659B365FF7F0000000075636036004F40536036
      BD779C73402E204F404BAD4A00000000000000000000000000000000E1348449
      2A5ACF6A6C5EA12C00000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F123EDE7730425966F759F565FF7F000000000000803A0053204F8053
      CF4AAD4AE046204F204BA03A5A6B00000000000000000000000000000239E655
      295EEA552539C230000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F103EFD769A6A376AFF7FFF7F000000000000986BA03EE0428436
      603AA046205300470F53873A422E00000000000000000000000000006741C455
      275EE75524396741000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F386E376AFF7FFF7FFF7F0000000000000000BA6F0E4FA93E
      402A803A8036613200000000000000000000000000000000000000009B774441
      63454341443D9B77000000000000000000000000000000009B77444163454341
      443D9B7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FE7F186318631863FE7F00004539253925394539453945394539
      45394539453945392539253925394539FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      00000000FE7F88420022002200228842FE7F0D4E2E52CA458841884188418841
      8841884188418841AA452E522E520D4EFF7F3967396739673967396739673967
      3967396739673967396739673967396739670000000000000000000000000000
      000000009C73CD5DC454C454C454CD5D9C730000000000000000000000000000
      0000000088428032004390630043803288424F52663D4F56CA45884188458845
      8845A845A84588418841CA4545394F52FF7F5542343E343E343E343E343E343E
      343E343E343E343E343E343E343E343E55420000000000000000000000000000
      00000000AC5DE560877D877D877DE560CC5D7863186318631863186318631863
      1863186300220043004300000043004300224539883D8841A845A945EA4D8E5A
      D56FEB4DA949A949A945884188414539FF7F343E000000000000000000000000
      00000000000000000000000000000000343E7B6F396739673967396739673967
      396739678358887D877D677D877D887DC3541053104310431043104310431043
      1053105300229063000000000000906300222539A945A945A949C949B56F536B
      5267B56F3267C949C949A945CA452539FF7F343E0000734ED55A9352314A9452
      9452945293527352B4569452734E0000343EF152CD46CD46CD46CD46CD46CD46
      CD46EE466254F57E000000000000F57EA3549042786310631053106310631063
      10631863002200530053000000530053002225352D52A949CA4DCA4DEB4D6F5A
      F162B56F956FCA4DCA4DC9494E562535FF7F343E000000000000000000009C73
      00009C7300000000FF7F000000000000343EAD46D96F115BF052325F325F325F
      325F535F6254CB7DCB7DCA7DCB7DEB7DA3549042786388428832786378639063
      1053105388328032805390738053803288324539705A0C52EB4DEB51B66FB673
      3367B05E2D56EC51EB4D0C524E56A945FF7F343E0000774A3B67774E3B67774E
      3B67774E3B67FA5E56465646FA5E0000343EAC42D96F8B3E4936B767B667535B
      115331538851E5640D7E2E7E0D7E0665A84D9042786310537863786378639042
      786378631053084300220022002208439042BC77463DD4622D560C52546BB773
      756F966F336B0C522E56D466663DBC77FF7F343E0000564AFF7F574AFE7B574A
      FE7B574ADE77574AFF7FFF7F564A0000343E8C42D973CE4A966796679667AD46
      96639663EE46EB5D6254625461540D62CD3E8842FE7F90427863883278639042
      7863786390427863904278631053FE7F9042FF7F1667463DF5664F5A2E56B162
      766FD3662E5A4F5AF56A463D1667FF7FFF7F343E0000574AFE7F574ADE7B574A
      DE7B574ABD77574AFE7FFE7F5646FF7F343E8B3EDA73AD4695674A369567AD46
      96639663CE46B6678B36D663EE46FA738C3E8842FE7F90427863786378639042
      7863786390427863786378639042FE7F8842FF7FFF7F1667663DF56A4F5A505E
      AF660E525277166B673D1667FF7FFF7FFF7F343E0000574A1A63774A1A63774A
      1A63774A1A63F95A56465646D95AFF7F343E8B3EDB77AC429463746374638C3E
      966396638C3E946394639563AD42DB778B3E8842FE7F88428842906390631053
      1053104310539063906388428842FE7F8842FF7FFF7FFF7FD462EB4D386F176B
      B47FB062757BCB49D462FF7FFF7FFF7FFF7F343EFF7F9C735B6B9C735B6B9C73
      5B6B9C735B6BBD77BD77BD77BD77FF7F343E6B3ADB776A3A8B3E73637363F052
      EF4ACF46F052736373638B3E6A3ADB776B3A8842FE7F08320832884288428842
      8842884288428842884208320832FE7F8842FF7FFF7FFF7FFF7F673D88412739
      CF66737B0B520D4EFF7FFF7FFF7FFF7FFF7F343E0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000343E6A3AFC7B282E28326B3A6B3A6B3A
      6B3A6B3A6B3A6B3A6B3A2832282EFC7B6A3A8832FE7FFE7FFE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FFE7F8832FF7FFF7FFF7FFF7FFF7F0E4E7377
      947BB47F1567FF7FFF7FFF7FFF7FFF7FFF7FD856343E343E343E343E343E343E
      343E343E343E343E343E343E343E343ED8564A36FC7BDC7BDC7BDC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7BFC7B4A361053105378637863786378637863
      786378637863786378637863786310531053FF7FFF7FFF7FFF7FFF7F463DEB4D
      505AEC51EC45FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000F14E115396679567956795679567
      95679567956795679567956796671153F14E0000105388328832883288328832
      883288328832883288328832883210530000FF7FFF7FFF7FFF7FFF7F596F925A
      0D4E67414639FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D14E4A364936493649364936
      4936493649364936493649364A36D14E0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000396739673967396739673967
      3967396739673967396739673967396739670000000000003967396700000000
      0000000000000000000000000000000000000000000039673967000000000000
      000000009C733967396739679C7300000000075607560756FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB84A55095405550536012576266A
      076A076A076A076A076A076A076A076A276A000000000000666E876E5A6B0000
      00000000000000000000000000000000000000000000666E876E5A6B00000000
      00009C73CA46602E602E602ECA469C730000E755CD6AE755FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F760D7B2EBC3A9C369D32266E487F
      697F697F697F687F697F697F697F497F276E000000000000CC72666E886E3967
      FF7F0000000000000000000000000000000000000000CC72666E886E3967FF7F
      0000CA468036E0426E63E0428036CA460000E755CD6AE655FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF8191D4B9C327C2E7D2A563E4672
      6A7F6A7F6A7F0E1D6A7F6A7F6A7F67728F5A000000000000767B666E0C77666E
      1767FF7F000000000000000000000000000000000000767B666E0C77666E1767
      FF7F602A0047E0420000E0420047602E0000E755CC6AE659FF7FFF7FFF7FFF7F
      FF7FDE7B39670000000000007B6FDE7BFF7F39265E5FDE3AFE3EFF7FDF772B62
      097B8B7F8B7F0C6F8B7F8B7F0A7F276206320000000000000000666E507B917F
      666E1767BD77000000000000000000000000000000000000666E507B917F666E
      396760268E670000000000008E67602E0000E655CC6AE45D7B6FDE7BFF7FFF7F
      FF7FFA4E5C163C123C123C129B2EFA569C733A227E639F67FF77FF7FDF7B7C6B
      276E8C7FAC7F2D21AD7F8C7F066AED5206360000000000000000CD72EA76D57F
      6F7F666EF26ABD7700000000000000000000000000000000CD72EA76D57F6F7F
      68726026204B204700002047204F602E0000E655AC6A3D0EBB32FA5200000000
      00005C163C123C127F477F43FF2A7D1ABB36FF7B9B32FD4215678B62E74D1042
      9552C976CE7F2D21CE7FC9766652725F063A0000000039673967F46AA872D77F
      907F6E7B666EF36A9C730000000000000000000039673967F46AA872D77F907F
      6F7F624AA032404FAE6B404FA03A0D530000E655AB723E0A9F167D163C123C12
      3C123C125D163B0E7F435F3F1F2F3F3F3C12FF7FFF7FBF73B2568C622A56CC35
      354F6A6E8E7F2D21AE7F6A6A935F7363263E00000000676EA972A8728772D77F
      8F7F8F7F6E7B8872D06A7B6F0000000000000000676EA972A8728772D77F8F7F
      8F7F6E7F834A6026602A602E0D5300000000E655AA723D0A3F3BFF261F333F37
      3F3B5F3B5F471B0E7F435F3F1F2B5F473C12FF7FFF7F3867EF720F73AE6A4B5E
      4846894E6872706F68720A4AED528A4AFE7F00000000666ED37F907F8E7F6D7B
      6D7B6D7B8E7F6E7F8872AD6A7B6F000000000000666ED37F907F8E7F6D7B6D7B
      6D7F8E7F6E7F8876AF6E7B6F000000000000E655AA6E3D0A5F43DF221F2F3F3B
      7F475F3B7F4B1B0A7F435F3F1F2B7F533C0EFF7FFF7FE234B47F517BEF6ECE6A
      0539FE7B295E89722862E841BB77FF7FFF7F000000008B6E707B917F6D7B6D7B
      D77FD77FD67FD57FD57FA872AD6E0000000000008B6E707B917F6D7B6D7BD77F
      D77FD67FD57FD57FA872AD6E000000000000E655AA6E3D067F4FDF221F2F3F37
      7F433F377F531B0A7F435F3BFF2A9F5B3C0EFF7FFF7FE134AD66CF6A3177F072
      80285A6B1073CE6ECE6A6B5E3867FF7FFF7F0000000013770D77B37F4B7B4C7B
      C872466E466E466E676E676E686E00000000000013770D77B37F4B7B4C7BC872
      466E466E466E676E676E686E000000000000E655AA6E3D067F57DF1E1F2F3F37
      7F433F379F5B1B0A9F575F3BFF26BF633C0EFF7FFF7F0239C555295AEA51263D
      A12C453DD57F527BF06ECE6AE334FF7FFF7F00000000BB7FA972B67F6C7B4A7B
      B57FCC72D16AFF7F000000000000000000000000BB7FA972B67F6C7B4A7BB57F
      CC72D16AFF7F000000000000000000000000E655AA6E3D069F5FDF1EFF2A3F37
      7F433F37BF631B0ABF6FDF73DF73DF733C0EFF7FFF7F0239E555285EE8552439
      2539E134AD66CF6A31771073A12CFF7FFF7F000000000000676EB57F707B297B
      707B957F476E5A6B0000000000000000000000000000676EB57F707B297B707B
      957F476E5A6B000000000000000000000000E655AB6E3E06FF777F535F4B5F43
      7F4B5F43FF731C0A3C129C269D2A9F673C0EFF7FFF7F9B77223D834943410339
      FF7F0239E555295AEA51463DC230FF7FFF7F0000000000008B6E717BB57F277B
      287BD77FED728B6ADE7B0000000000000000000000008B6E717BB57F277B287B
      D77FED728B6ADE7B00000000000000000000E655CB6E3E06BE2A3F439F5FFF73
      FF77FF737F573C0EFF7FBF6B7E5BBD2E3C12FF7FFF7FFF7FFF7FFF7FDE7BFF7F
      FF7F0239E555285EE8552439C234FF7FFF7F00000000000013730D77FA7FD97F
      D87FFA7FD87F686EF46A00000000000000000000000013730D77FA7FD97FD87F
      FA7FD87F686EF46A00000000000000000000AE62E559D35A5E539D263C0E3C0E
      3C0E3C0E3C121E47FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F9C77223D6349434102399B77FF7FFF7F000000000000BA7B686E476E476E
      476E476E686E686E696E000000000000000000000000BA7B686E476E476E476E
      476E686E686E696E000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      0000000000009C733967396739679C7300000000000039673967396739673967
      3967396739673967396739673967000000006072607240724072407260766076
      607640724072407240724072407260726072000039673967BD779C7339673967
      3967396739670E6A2665276527650E669C730000000000000000000000000000
      000000007B6FCA46602E602E602ECA469C7300000000F231D129B129B129B129
      B129B129B129B129B129D129F231000000006172677B677B667B867BC762C935
      A67F867B667B667B667B667B677B877B617200008876887A136F184A152DF52C
      F52CF6280C51466D667D667D677D47690E667B6F396739673967396739673967
      39673967A9428036E0426E63E0428036CA4600000000D1290000000000000000
      000000000000000000000000D12900000000307BC376687B467B467BA7628A08
      A835667F467B467B467B467B687BC376307B0000A87AB07F3435162D572D772D
      772D7829246D877D877D677D877D887D26655726F605F605F605F605F605F605
      F705F901602E0047E0420000E0420047602E00000000B1290000FF7FFF7F0000
      D515D5150000FF7FFF7F0000B12900000000FF7F4072497B477B467B667F667F
      C910C666467F467B267B467B497B4072FF7F00007677343537297831DA39FC3D
      FC3D1D3A0469F47E000000000000F57E2665F605FE7FDE7FDE7FDE7FDE7FDE7F
      DE7FFF7F402A8E670000000000008E67602E00000000B1290000FF7FFF7FD515
      FE3AFE3AD515FF7FFF7F0000B12900000000FF7F307BA3764A7B06778662C66A
      8835C9100673257F257B4A7BA476307BFF7F0000184A372DB835FB3D3C67FD7F
      B356FE7FC364CB7DCA7DCA7DCA7DEB7D2665F605FE7FBD77BD77BD77BD77BD77
      000000004026204B204700002047204F602E00000000B1290000FE7FD515FD36
      FD3AFD3AFD36D515FE7F0000B12900000000FF7FFF7F61722A7B2877C814A90C
      A90C8908E818E576287B2A7B6172FF7FFF7F0000362DB83DFB3D3B67FB7FFC7F
      FC7FFD7F4F72476D0D7E2E7E2D7E686D5072F605FE7F9C73734E734EBD73F85E
      000000002D53A036404FAE6B404FA03A0D5300000000B1290000D5111E431E43
      1D43FD363E471E43D5110000B12900000000FF7FFF7F757B83766E7F27568808
      A73D457F257F067B4E7F8376757BFF7FFF7F0000162D3A4A1C42FA7FFB7F2A21
      366BFB7FFC7F4E72E4640469256971720000F605FE7F9C739C739C739C739452
      945294529656673A40264026602A4A1E000000000000B1290000983AD619F619
      3D4FBC2EF61DF619983A0000B12900000000FF7FFF7FFF7F61722B7B6C7FE71C
      88082756267F2B7B2C7B6172FF7FFF7FFF7F000016297B521D42704EFA7F566B
      4B251567FA7F714E1F3E7D4E182100000000F605FE7F7B6F7B6F7B6F7B6F7B6F
      7B6F7B6F7C6F7D737D739E730000F905000000000000B1290000BD7BDD7FD615
      7D5B3D53F619DD7FBD7B0000B12900000000FF7FFF7FFF7FB97F6176937F8966
      6704870CA96E937F8276B97FFF7FFF7FFF7F00001629BC5A1D42F87F356B4B29
      3567D97FD87FF87F1D42BD5A172500000000F605FE7F5B6F7B6F7B6F5B6BBD77
      0000000000000000BE775B6FFF7FF705000000000000B12900009C739C77983A
      D615D615983A9C779C730000B12900000000FF7FFF7FFF7FFF7FA5762D7B927F
      E7202600C9140C77A576FF7FFF7FFF7FFF7F7B6F36299C565D4A3A674B293467
      D87FD77FD67F19635D4A9C5636297B6F0000F605FE7F5A6B3246524A5A6B185F
      0000000000000000185F5A6BFE7FF605000000000000B1290000000000000000
      000000000000000000000000B12900000000FF7FFF7FFF7FFF7FB97F8172967F
      4E7F4E7FD87F8276B97FFF7FFF7FFF7FFF7FCE76EF51B939FE663E461967D67F
      0C46D57FF9621D42FE66B939EF51CE760000F605FE7F39673A673A673A679452
      935273527352935293523967FE7FF6050000000000003726381E171E171E171E
      171E171E171E171E171E381E162600000000FF7FFF7FFF7FFF7FFF7FEB760B7B
      977F967F0D7BEB76FF7FFF7FFF7FFF7FFF7FA97A0A7F75313B4A3F6F9E521E42
      1E421E427E523F6F3B4A75310A7FA97A0000F605FE7FDE7FDE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FF60500000000000058225E4FFD36FD3AFD3A
      FD3AFD3AFD3AFD3AFD365E4F582200000000FF7FFF7FFF7FFF7FFF7FFE7F6072
      B97FB97F6072FE7FFF7FFF7FFF7FFF7FFF7FCA7A4C7F4D7F9531DA3D3F6B9F7F
      9F7F9F7F3F6BDA3D95314D7F4C7FCA7A0000F6099E63BB26BB26BB26BB26BB26
      BB26BB26BB26BB26BB26BB269E63F60900000000000058261C4B9A2A9A2A9A2A
      9A2A9A2A9A2A9A2A9A2A1C4B582600000000FF7FFF7FFF7FFF7FFF7FFF7FEA76
      0E7B0E7BEA76FF7FFF7FFF7FFF7FFF7FFF7F987F0B7BCF7FEF7F3052562D5629
      56295629562D3052EF7FCF7F0B7B987F0000170A5D533D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F5D53170A0000000000005826FB46FB46FB46FB46
      FB46FB46FB46FB46FB46FB46582600000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      60726072FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000987FEA7A097F707F00000000
      000000000000707F097FEA7A987F00000000B932170A160A160A160A160A160A
      160A160A160A160A160A160A170AB932000000000000792A5826582658265826
      582658265826582658265826792A000000000000000000000000000000003967
      3967396700000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      00000000000000000000000000000000000000007B6F39673967396739673967
      39673967396739673967396739675A6B0000000000000000000000000000AB3D
      89414A6E3967000000000000000000000000453A453A453A4536433614361436
      14361436143643364536453A453A453AFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D65A734E734E734E734E734E
      734E734E734E734E734E734E734EB55600000000FF7FBD773967396739670B52
      905A527FA64D39673967BD77FF7F000000000532C542C542A43EE229FF77E461
      2766E461FF77E229A43EC542A53E0532FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F734E00000000DE7BD65A314610421142C87A
      917F507F627EA64D5342D65ADE7B00000000DD77EA2D222E4C3E5B63DF73A059
      2866A059DF735B634C3A222EEA2DDD77FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BDE7B3146AD35DE7B
      3146AD35DE7F7C2A7C2ADD7BDE7B734E000000005A6B734EDE77BE777B6B7D6B
      C465087FA57E627EA64D954E5A6B00000000FF7F353ADF77BF6FBF6FDF6F4055
      27664055DF6FBF6FBF6FDF77353AFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BBD77DE7BDE7BDE77
      DE7BDE7BDD7BBD329C2EBD7BDE7B734E00000000F75EB556BD7711425B6B3142
      7D67C561287FA57E627EA551D65A00000000FF7F343ADF779E6BBE6F12323436
      343634361232BE6F9E6BDF77343AFF7FFF7F0000000000000000000000000000
      39673967DE7B0000000000000000000000000000734EDE7BBD733146AD35BD77
      3146AD35BD7BBE32BE32BC77DE7B734E00000000B556F75A5B6B3A6731465A6B
      32465C67E565087FA47ED06AEF3939670000FF7F333ADF77D852F952F9561A57
      1A571A57F956F952D852DF77333AFF7FFF7F0000000000000000000000003967
      8C318C31F75E0000000000000000000000000000734EBE779C739D73BD779D73
      9D77BD779C739C779C779C73BE77734E00000000524A18633967524639675246
      3A6752463B63C465757B3242F85EEE353967FF7F343ADF779E6BF9529E6FF952
      9E6FF9529E6FF9529E6BDF77343AFF7FFF7F0000000000000000000039678C31
      B556B5568C313967000000000000000000000000734EBD777C6F3246AE359C73
      3246AE359C733246AE357C6FBD77734E0000000031463963524A1963524A1963
      524A1963534A3963103EBD733142F759B365FF7F343EDF77D852F952F952F952
      F952F952F952F952D852DF77343EFF7FFF7F000000000000000039678C31F85E
      D65AD65AF85E8C31396700000000000000000000734EBD777B6B7C6B7C6B7C6F
      7C6F7C6F7C6F7C6B7C6B7B6BBD77734E00005A6B104295529452945294529452
      945294529452955295520F3EDC725966F669FF7F543EDF777D67D9529E6BF952
      9E6BF9529E6BD9527D67DF77543EFF7FFF7F00000000000000008C317B6F9D73
      9C739C739D737B6F8C3100000000000000000000734EBD775B67E37D847D7B6B
      3246AE357B6B047E847D5B67BD77734E00003146BD777B6F7B737B6F7B6F7B6B
      7B6B7B6B7B6F7B6F7B739B6F176E186E0000FF7F543EFF7BD84ED952D952D952
      D952D952D952D952D84EFF7B543EFF7FFF7F000000000000000031468C318C31
      8C318C318C318C313146000000000000000000009452DE7BBE77DE77DE77DE77
      DE7BDE7BDE77DE77DE77DE77DE7B945200001042BC77BA32B801186B1863185F
      185F185F1863186BB801BA32BC7730420000FF7F553EFF7B7D67D9527D67D952
      7D67D9527D67D9527D67FF7B553EFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000009452EF3DEF41EF41EF41EF41
      EF41EF41EF41EF41EF41EF41EF3D94520000B556BC77BF577B1AD8019B7B9C73
      9C739C739B7BD8017B1ABF57BC77524A0000FF7F5542FF7BD84ED852D852D852
      D852D852D852D852D84EFF7B5542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000094523F471F3F1F3F1F431F43
      1F431F431F431F431F3F1F3F3F47945200000000B5560F46BF5B7B1AD8010F4A
      10460F4AD8017B1ABF5B0F46B55A00000000FF7F7542FF7F5C63D84E5D63D84E
      5D63D84E5D63D84E5C63FF7F7542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093525F539D329C32BC32BC32
      BC32BC32BC32BC329C329D325F53935200000000000000000000BF5B7B1AD901
      0000D9017B1ABF5B00000000000000000000FF7F7642FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F7642FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093527F5B7F5F7F5F7F5F7F5F
      7F5F7F5F7F5F7F5F7F5F7F5F7F5B9352000000000000000000000000BF5B7B1E
      00007B1EBF5B000000000000000000000000FF7F195B76467646764276427642
      764276427642764276467646195BFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000B55693529352935293529352
      9352935293529352935293529352B55600000000396739673967396739673967
      396739670000FF7FFF7FFF7FFF7FFF7FFF7F00000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F000000000000FF7F000000007B6F3967396739673967
      39673967396739673967396739677B6F00000000000000000000000000000000
      000000000000000000000000000000000000B84A550554055405540554055405
      54055505B84AFF7FFF7FFF7FFF7FFF7FFF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000F75ED656D656D656D656
      B556B556B556B556B556B556B556F75E00000000000000000000000000000000
      000000000000000000000000000000000000760D7B2EBC3ABC3A9C3A9C3ABC3A
      BC3A7B2E960DFF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D65600000000307A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000F8191D4B9C327C2E7C2E7C2E7C2E
      9C321D4FB711FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000B55600000000E6700000
      0000000000000000000000000000B556000000000000000039677B6F00000000
      00000000000000000000000000000000000039265E5FDE3AFE3EFE7FBD7BFE3E
      FE3E7E63D815FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C5FDA46845CDA46
      B846B846B846B846B846B8465C63D55600000000000039672026653600000000
      0000000000000000000000000000000000003A227E639F67FF77FF7FDE7B9E6B
      3D535D5F1A22FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000000007710000
      0000FF7FFF7FFF7FFF7FFF7F0000D55600000000396700226A5B202239673967
      396739673967396739673967396739677B6FFF7B9C32FD4215676B62E7512E46
      F71D1726D952BD77DE7BFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63DA46845CFA46
      B84AB84AB84AB84AB846B8465C63D5560000396700224C5B6053E03E00222022
      20222022202220222022202220222026873AFF7FFF7FBF73B2568C622A560E42
      BA42FF73B93EB84AF95A39673967BD77FF7FF2310000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D5560000FF7F0671FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000D556000020266E5F204F204F204F204F4053
      405340534053405340534053405340532026FF7FFF7F3867EF720F73AD6A4962
      954ADA42FF7FBA42993A16221726D84EBD77F231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000D5565C63DA46845CFA4A
      B84AB84AB84AB84AB84AB8465C63D556000020267067004F004F6C638C678C67
      8C678C678C678C678C678C678C678C6B2026FF7FFF7FE234B47F517BEF72714E
      792E7E5FFF73FF73FF77FF779E637936D8520000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5560000FF7BE670FF7F
      FE7FDE7FDE7FDE7FDE7FDE7B0000D556000000000022936B004FA03E00220022
      20222022202220222022202220222022C942FF7FFF7FE134AD66CF6E307B3826
      9F67DF6FDF6FDF6FDF6BDF6BDF6FBF67372AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C63DA46845CFA4A
      B94AB84AB84AB84AB84AB8465C63D5560000000000000022946F202200000000
      000000000000000000000000000000000000FF7FFF7F0239C555295EC8553922
      FF73BF67BF6BBF6BBF6BBF67BF67FF733826F231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000FF77E66CFF7B
      DE7BDD7BDD7BDD7BBD7BBD7B0000D55600000000000000002026643200000000
      000000000000000000000000000000000000FF7FFF7F0239E555275EC5555A26
      DF739F63BF67BF67BF67BF679F63DF73382A0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63D946845CDA46
      B84AB84AB84AB84AB84AB8465C63D55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F9C77223D63492141392A
      BF67BF679F639F639F639F63BF639F67392AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000B5560000BD73C568BD73
      9C739C739C739C739C739C730000B55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F7E67
      9B3A9E67DF73DF73DF73DF739E679A3A7D6300000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D65600000000317A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      7E637A32592E592E592E592E7A327D63FF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231FF7FFF7F00000000F75AD656D656D656D656
      B556B556B556B556B556B556D656F75A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000396739673967396739670000
      FF7FFF7F00003967396739673967396700000000000000000000000000000000
      0000000000000000000000000000000000000000396739673967396739673967
      39673967396739673967396739675A6BFF7F0000DE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7B7B6FCA46602E602E602ECA469C73524ACE39AE35AD358D318C311042
      FF7FFF7F524ACE39AE35AD358D318C3110423967396739673967396739673967
      3967396739673967396739673967396739678C66476627664766476A466AAB39
      89412A6A466A4666266626662666696AFF7F5A6B195F195F195F195F195F195F
      195F1A5FC9468036E0426E63E0428036CA46EF39F03D734A1142AE354B296C2D
      FF7FFF7FEF39F03D734A1142AE354B296C2D216C006C006C006C006C006C006C
      006C006C006C006C006C006C006C006C216C47666E7F4B7F297BA772CA760C4E
      905A517F8649B07F8F7F8F7F907F2666FF7F993A993279327932793279327932
      79329C32602EE046E0420000E0420047602ECE3939675B6B3A67396318636B2D
      FF7FFF7FCE393A675B6B3A67396318636B2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF7B7B5B27664B7F097BA772E77A2666C976
      917F507F627E86498F7F6E7F907F2666FF7F9932FF7BFF77FF77FF77FF77FF77
      FF77FF7B402A8E670000000000008E67602ECE39D6563963B6565246CE396B2D
      39673967CE39D6563963B6565246CE396B2D0070237DC07CC07CC07CC07CC07C
      C07CC07CC07CC07CC07CC07CC07C237D00704766297BA772E776487F26668E7F
      C661087FA57E637E86458E7F907F2666FF7F7932FF773C539E63DF6FDF6FDF6F
      DF6FFF734026204B004700002047204F602ECE39D6563963B656524ACE396B2D
      EF398C31CE39D6563963B656524ACE396C2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF775B5B4766A772E77A487F6C7F06666D7F
      6D7FC65D097FA57E627E8649B17F466AFF7F7932FF77FF77DB421C4BDF6FDF6F
      DF6FFF730D4BA036404FAE6B404FA0368B32CE39D6563963B6565246CE396B29
      CE396C2DCE39D6563963B6565246CE396B2D0078877D807C807C807C807C807C
      807C807C807C807C807C807C807C877D00704766CB72266626660666886E6C7F
      4C7F6C7FC65D087FA47EB06AD031456EFF7F7932FF77BF6BFF7BDB42DF6FFB46
      582A592EFE4A0D4B402A4026402A0D4F9C36CE397B6FBD777B6F3967F75E4A29
      5A6B3146CE397B6FBD777B6F3967F75E6B2D7401FF73BC01DF63BC01DF63BC01
      DF63BD01FF7BFF77FF77FF77FF77FF775B5B4666AB726C7F6C7F6C7F4C7F4B7F
      4B7F4C7F6C7FC5617677323EF85AEF3539677932FF77BF67BF67FF7F1C4BFB46
      00000000FC461E4F0000DF6BDF6BFF779A32EF39AD358C316C2D6B2D6B2D3246
      5A6B3142744EAD358C316C2D6C2D6C2D524A72019D2A9F677901BF6B7901BF6B
      7901BF63407C407C407C407C407CCC7D00702666B27F4A7F4B7F4B7F4B7F4B7F
      4B7F4B7F4B7F6A7FF2399D733142F759B3657932FF77BF63BF633D4FDB420000
      BF67BF670000DB423D4FBF67BF63FF777932FF7FCE39F75E39679452EF3D4B29
      5A6B3146CE39F75A39679452EF3D6B2DFF7F5201FF7F57019E6B57019E6B5701
      9E6B5801FF7FFF77FF77FF77FF77FF775A5B2666B37F497F4A7F4A7F4A7F4A7F
      4A7F4A7F4A7F4A7F497FF139DC6E5962F6657932FF77BF637E57FB42FF7FBF63
      BF63BF63BF63FF7FFB427E57BF63FF777932FF7FCE39D75A1963944EEF3D6B2D
      CE396C2DCE39F75A1963944EEF3D6C2DFF7F5201DC429E6736019E6B36019E6B
      36019F63007C207C207C207C007C307E00702666B37F287F297F297F297F297F
      297F297F297F297F497F477F1966196AFF7F7932FF7B9F5BDB42FF779F5F9F5F
      9F5F9F5F9F5F9F5FFF77DB429F5BFF7B7932FF7FCF39F75E39679452EF3D6B2D
      EF398C31CF39F75E39679452F03D6C2DFF7F5201000014017E6714017E671401
      7E671501FF7FFF77FF77FF77FF77FF777B5B2666B47F917F917F927F927F927F
      927F927F927F927F917F917FD37F2566FF7F7932FF7BFC46DF739F5B9F5B9F5B
      9F5B9F5B9F5B9F5B9F5BDF73FC46FF7B7932FF7F9452AE358D318C2D6C2D3146
      FF7FFF7F734EAD358D318C318C31524AFF7F72011C5700001C5300001C530000
      1C53FF7FB37EB47EB47EB47EB57ED57E006C2666B57FC676C776C776C776C776
      C776C776C776C776C776C676D57F2666FF7F9932FF7FFF7FFF7BFF7BFF7BFF7B
      FF7BFF7BFF7BFF7BFF7BFF7BFF7FFF7F9932FF7FFF7F3967CF393963734E6C2D
      FF7FFF7FCF393963534A6C2D3967FF7FFF7F5726720152015201520152015201
      720174010074006C006C006C006C006CE7702666D67F267F277F277F277F277F
      277F277F277F277F277F267FD67F2666FF7F1B4F993279327932793279327932
      79327932793279327932793279329932DA42FF7FFF7FEF39324632468C316C2D
      FF7FFF7FCF39314632468D318C2DFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000002666B07F907F907F907F907F907F
      907F907F907F907F907F907FB07F2666FF7F0000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FEF39CE35AD358D316C2D
      FF7FFF7FEF39CE35AD358D318C2DFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000686A266626662666266626662666
      2666266626662666266626662666686AFF7F0000000000000000000000005A6B
      F75EF75E5A6B00000000000000000000000000000000000000000000F75EF75E
      F75EDE7B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B6F
      396739673967396739673967396739677B6F00005A6BF75EF75EF75EF75E734E
      10423146734EF75EF75EF75EF75E5A6B00000000BD771863F75EF75E6B2D4A29
      3146D65A1863BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B6F39673967396739673967CB51
      24412541254125412541254125412541CB4D00001042AD35AD35AD35AD35524A
      B5563967F75E524AAD35AD35AD35104200000000524A0821E71CE71CEF3D3146
      5A6B8C310821524A000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010428C318C318C318C318D312445
      4B5EE455E459C451C451E459E4554B5E2441F75EAD353967CE39AD35DE7BBD77
      1042F75E7B6FF75E104210423146EF3D7B6F00002925EF3D31463146D65A5A6B
      5A6B94528C312925000000000000000000000000000000000000000000000000
      00000000000000009C73F75E0000000000008C31AE35CF39CF398C31AE312441
      0656824D8249413D413D8249824D06562441EF3DF75E2925EF3D8C31BD777B6F
      9C731042F75E5A6BF75E18631863734E945200006B2D945210421042EF3DCE39
      1863B55694528C31F75E00000000000000000000000000000000000000000000
      0000000000000000524A8C31F75E00000000CE39744EEF398C314A294B292445
      275AA24DA24D087F087FA24DA24D275A24410000AD356B2D10428C31BD777B6F
      7B6F7B6FEF3D396739675A6B5A6B5A6B524A0000AD35F75E524A524AFF7FBD77
      CE391863D65A94528C31F75E0000000000007B6FF75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75E8C3118638C31F75E00008C3118638C31CE39C61809210441
      8C62814D814D81498149814D814D8C622441F75EAD35396731468C31BD775A6B
      5A6B7B6FEF3D7B6F7B6FAD35AD359C73524A0000EF3D5A6B9452BD770000DE7B
      5A6BCE391863B556B5568C31F75E0000000010428C318C318C318C318C318C31
      8C318C318C318C31524AF75EF75E8C31F75E1863AD35103E1863324653460341
      8D5E6C5A6C5E6C5A6C5A6C5E6C5E8D5E2441EF3DF75E2925524A8C31BD773967
      DE7B000010425A6B7B6FCE39CE397B6F734E0000CE39D65A9C735A6B524ACE39
      3146BD77EF3D5A6BCE39B5568C31F75E00008C31F75ED65AD65AD65AD65AD65A
      D65AD65AD65AD65AB556B556B55618638C310000FF7FAE35BD7BC614C614CA51
      E23CE23C0341033DE33C033DE24003412D5A0000AD356B2D734E8C31BD773967
      0000B556734E94525A6B5A6B5A6BB5561863000000009452524AD65A3146AD35
      EF3D524AB556AD359C73CE39EF3DCE3900008C315A6B39673967396739673967
      396739673967396739679452945239678C3100000000FF7F366FAD6A4A5A156B
      DE7BBE77E140DF77DF77E13C000019020000F75EAD35396794528C31BD771863
      0000AD35D65A734E524A524A734E104200000000000000005A6B734ED65A734E
      EF3D9C7300000000CE39D65A524A10420000524A8C318C318C318C318C318C31
      8C318C318C318C31314694525A6B8C310000000000005A6F31771073AD6A8C66
      3E0F7F07CC2DC040C040AC250000F7010000EF3DF75E2925B5568C31DE7B1863
      DE7B000000000000DE7B3967DE7BAD3500000000000000008C317B6FF75EB556
      9452CE39000000000000524A3146000000000000000000000000000000000000
      00000000000000008C315A6B8C3100000000000000006741B57F517BEF72CD6E
      48215F235E1F5F1F5F1F5E1B0000F60500000000AD356B2DB5568C31DE7B1863
      186318631863186318631863DE7B8C310000000000000000C618DE7B5A6B1863
      F75E841000000000000000000000000000000000000000000000000000000000
      0000000000000000734E8C3100000000000000000000E134AD6AAE6A1173CE6E
      4028000000000000000000000000F6050000F75EAD35F75EB5568C31FF7FF75E
      18636B2D314631461863F75EFF7F8C310000000000000000E71C4A29EF3DB556
      3146A51400000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000239E555295EEA510439
      802C19639C730000BF577F230000F6050000EF3DF75E0821B5568C31FF7FF75E
      F75EF75EF75EF75EF75EF75EFF7F8C310000000000000000E71CCE391042CE39
      0821A51400000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000705AE455275EE755E234
      AB411963F75ED75E9E43DC020000F60500000000AD35BD779C738C310000FF7F
      FF7F00000000FF7FFF7FFF7F0000AD3500000000000000004A29AD35EF3DAD35
      0821292500000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000004E5A624921456F5E
      0000000000000000000000000000F60500000000524AAD35AD35AD35AD358C31
      8C318C318C318C318C318C31AD35524A00000000000000009C73292529252925
      08219C7300000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DC2E1806
      F705F605F605F605F605F605F605B93200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000039673967
      3967DE7B00000000000000000000000000000000396739673967396739673967
      3967396739673967396739673967396700000000000000000000000000003967
      3967396739670000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DE7B396739673967CA458941
      496E166B3967DE7B000000000000000000005722F605F605F605F605F605F605
      F605F605F605F605F605F605F605F6055722000000000000000000000000EF3D
      9C73D65A10420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000B84A5509550536012A566E5A
      517FA5515701B84A00000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6057B6F396739673967396739671042
      BD77F75A1042396739673967396739677B6F000000000000F75E396700000000
      000000000000000000000000000000000000000076095A2E9C369D32C77E4F7F
      507F627EA555990100000000000000000000F605FF7F744E3146524A5A6BFF7F
      18631863FF7F1863186318631863FF7FF6053146AD358D318D31AD31AD31AE31
      8D318D318D31AD35AD35AD35AD35AD35314600000000F75E8C31EF3D00000000
      0000000000000000000000000000000000000000D819FD467C2E7C2A7E22C365
      077FA57E627EA65139670000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF605AE3539671863185F5A6B954A0060
      954E744E744E744E744E744E744EB552AE350000F75E8C3118638C31F75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E5A6B000019263D57BD36BD36FF7FDF73
      C461087FA57E627EA6513967000000000000F605FF7F534A314231463967FF7F
      F75EF75EFF7FF75EF75EF75EF75EFF7FF605CE391863D65AD65A3967D7520060
      D752B552B552B552B552B552B552F75ECE39F75E8C31F75EF75E524A8C318C31
      8C318C318C318C318C318C318C318C31EF3D00005A2E7E67FE3EDF6FFF7FDF77
      9F5FC465087FA47ED06EEF39396700000000F605FF7FFF7FFF7FFF7FFF7FDE7B
      FF7BFF7BDE7BFF7BFF7FFF7FFF7FFF7FF605CF39185FB556B5523963185B0060
      195BF75AD75AD75AD75AD75AD75A3A67CE398C311863B556B556B556D65AD65A
      D65AD65AD65AD65AD65AD65AD65AF75E8C3100005A261D4FBF6B76778B62E751
      7056DF67E369757B3242F85EEE3539670000F605FF7F524610421142185FDE7B
      D65AD65ADE7BD65AD65AD65AD65AFF7FF605EF3D3967944E744E7B6B5A630064
      5A631963186318631863186319639C6FCF398C31396794529452396739673967
      39673967396739673967396739675A6B8C3100000000FD42BE32175F6C5EE84D
      5246BF321E471042BD733142F759B3650000F605FF7FDE7BDE7BDE7BDE77BD77
      BD77BD77BD77BD77DE77DE77BD77FF7FF605F03D5A6B534A524A7B6B9C670064
      9C675A675A675A675A673A677B6BBD77EF3D00008C315A6B945231468C318C31
      8C318C318C318C318C318C318C318C31524A0000000000007B6FAF62EF6EAD66
      4C569C73000000003042DC725962F5650000F605FF7F3146F03D103EF75ABD73
      B556B556BD73B556B556B556B556FF7FF605103E9C73524A3246DE77DE6F0064
      DE6F9C6F7C6F7C6F7C6F9C6FDE77BD77F03D000000008C315A6B8C3100000000
      000000000000000000000000000000000000000000000000CB49937F1077CE6E
      AD6A0D52000000000000386E376A00000000F605FF7F9C73BD77BD739C739C6F
      9C739C739C6F9C739C739C739C73FF7FF605B656D65A9C73DE7BFF7FFF7B0068
      FF7BDF7BDE7BDE7BDE7BDE7BDE7BB656B6560000000000008C31EF3D00000000
      000000000000000000000000000000000000000000000000C030D57F727F3073
      0F73A02C0000000000000000000000000000F605FF7FF03DCE39CF39B5567B6F
      944E944E7B6F9452945294527352FF7FF6050000534A3146314231425342006C
      7342314231423142314231423142524A00000000000000000000000000000000
      000000000000000000000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6050000000000000000000000000070
      3967396739677B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000F6097E5B7B1A7B1E7B1E7B1E7B1E
      7B1E7B1E7B1E7B1E7B1E7B1E7B1A7E5BF6090000000000000000000000000070
      0070007400740871000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000170A5D4F3D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F3D4F5D4F170A0000000000000000000000000070
      00700068005C0070000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000009B77444163454341
      443D9B770000000000000000000000000000992A170A160A160A160A160A160A
      160A160A160A160A160A160A160A170A992A0000000000000000000000000070
      00700070006C297500000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFF0FC3FFFFFFFFF0000FC1FFFFFFE7F
      000080070000FE7F000000030000FE7F000000007FFEFC3F000100000000FC3F
      000300000000FC3F000300000000FC3F000300000000FC3F000300000000F81F
      021300000000F81F00C3000000000000000300017FFE0000000300010000F00F
      00030003FFFFFFFF00030007FFFFFFFFF000FFDFFFFFFF8F0800FF9FE0078007
      04008081E007800302000001E007800101000001E007800100800081E0078001
      00400391E0078001002003C1E0078003001003E1F00FC007000807E1F81FE00F
      00040003F81FE07F0002C003F81FE07F0000C007F81FE07F0000E003F81FE07F
      0000E003F81FE07F0000F01FF81FE07FFFFFFFFEFFFFFFFFFFC10000FFFFFFC1
      FF8000000000FF80FF8000000000FF80000800007FFE0000001C00004002001C
      000800007D6E0000000000004002000000000000400200000000000040000000
      0000000040000000000000000000000000000000400200000000000000000000
      00000000FFFF000080010000FFFF8001E0008000E7FFCF8300000000E3FFC701
      00000000E0FFC10100000000E07FC01100380000F03FE03900000000F01FE011
      07000000C00F800100000000C007800300000000C003800700000000C0038007
      00000000C003800700000000C01F803F00000000E01FC03F00000000E00FC01F
      00000000E00FC01F00000000E00FC01FFFFFFFC1FFC1C00300008000FF80C003
      000080000000DFFB000080000008D24B0000801C001CD00B000080000188D00B
      000080000180D00B000080010001D00B000080030005D00B0000800301E1D00B
      0000000101E1DFFB000000010001C003000000010001C003000000010001C003
      000000010001C003000087C30001C003FC7FFFFEFFFF8001FC3F0000FFFF8001
      80030000FFFF800180030000FFFF800180030000FFFF800180030000FE3F8001
      80010000FC3F800180000000F81F800180000000F00F800100000000F00F8001
      00010000F00F800100010000FFFF800100010000FFFF800180030000FFFF8001
      F11F0000FFFF8001F93F0000FFFF80018040DB6EC001FFFF00000000C001FFFF
      00000000DBFDFFFF00008002DBFDE7FF00000000C001C7FF00000000DB058000
      00008002C001000000005B6CD005000000000000C001000000008002D0058000
      00000000C001C7FF00000000D005E7FF00008002C001FFFF00000000D005FFFF
      0000DB6CDBFDFFFF00000000C001FFFFFFC18241FFFF80008000000000000000
      00000000000000000008000000000000001C0000000000000008000000000000
      0000000000000000000000000000000001900000000000000240000000000000
      00000000000000000000000040000000000000002A0000000000000000000000
      00000000FFFF0000FFFF0000FFFF0000FC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  inherited dsCL: TevDataSource [11]
    Left = 639
    Top = 71
  end
  inherited DM_CLIENT: TDM_CLIENT [12]
    Left = 900
    Top = 79
  end
  inherited DM_COMPANY: TDM_COMPANY [13]
    Left = 930
    Top = 66
  end
  inherited wwdsEmployee: TevDataSource [14]
    OnUpdateData = wwdsEmployeeUpdateData
    Left = 1070
    Top = 384
  end
  inherited DM_EMPLOYEE: TDM_EMPLOYEE [15]
    Left = 1116
    Top = 346
  end
  inherited wwdsSubMaster: TevDataSource [16]
    OnDataChange = ChangeSubMaster
    Left = 1142
    Top = 309
  end
  inherited wwdsSubMaster2: TevDataSource [17]
    OnStateChange = wwdsSubMaster2StateChange
    OnUpdateData = wwdsSubMaster2UpdateData
    Left = 773
    Top = 66
  end
  inherited ActionList: TevActionList [18]
    Left = 567
    Top = 83
  end
  inherited EEClone: TevClientDataSet
    Left = 1066
    Top = 298
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 1080
    Top = 170
  end
  object SYStatesSrc: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 1007
    Top = 139
  end
  object SYMaritalSrc: TevDataSource
    DataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
    Left = 1110
    Top = 272
  end
  object wwdsSubMaster3: TevDataSource
    DataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
    MasterDataSource = wwdsSubMaster2
    Left = 707
    Top = 51
  end
  object wwdsSubMaster4: TevDataSource
    DataSet = DM_EE_RATES.EE_RATES
    OnDataChange = wwdsSubMaster4DataChange
    MasterDataSource = wwdsEmployee
    Left = 888
    Top = 336
  end
  object wwdsCoLocalTax: TevDataSource
    DataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
    MasterDataSource = wwdsMaster
    Left = 1068
    Top = 272
  end
  object wwdsSubMasterEELocals: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    MasterDataSource = wwdsEmployee
    Left = 924
    Top = 328
  end
  object evSecElement1: TevSecElement
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 560
    Top = 84
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 1148
    Top = 256
  end
  object pmAccessLevel: TPopupMenu
    Left = 64
    Top = 227
    object imAccessLevelCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = imAccessLevelCopyToClick
    end
  end
  object pmOverrideFedMinWage: TevPopupMenu
    Left = 1134
    Top = 461
    object FedMinWageCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = FedMinWageCopyToClick
    end
  end
  object dsAvaliableGroups: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CO_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GROUP_NAME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'GROUP_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GroupTypeName'
        DataType = ftString
        Size = 30
      end>
    OnCalcFields = dsAvaliableGroupsCalcFields
    Left = 1192
    Top = 128
    object dsAvaliableGroupsCO_GROUP_NBR: TIntegerField
      FieldName = 'CO_GROUP_NBR'
      Visible = False
    end
    object dsAvaliableGroupsGROUP_NAME: TStringField
      DisplayLabel = 'Group Name'
      FieldName = 'GROUP_NAME'
      Size = 50
    end
    object dsAvaliableGroupsGROUP_TYPE: TStringField
      FieldName = 'GROUP_TYPE'
      Visible = False
      Size = 1
    end
    object dsAvaliableGroupsGroupTypeName: TStringField
      DisplayLabel = 'Group Type'
      FieldKind = fkCalculated
      FieldName = 'GroupTypeName'
      Size = 30
      Calculated = True
    end
  end
  object dsAssignedGroups: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CO_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_GROUP_MEMBER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GROUP_NAME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'GROUP_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GroupTypeName'
        DataType = ftString
        Size = 30
      end>
    OnCalcFields = dsAvaliableGroupsCalcFields
    Left = 1224
    Top = 176
    object IntegerField1: TIntegerField
      FieldName = 'CO_GROUP_NBR'
      Visible = False
    end
    object StringField1: TStringField
      DisplayLabel = 'Group Name'
      FieldName = 'GROUP_NAME'
      Size = 50
    end
    object StringField2: TStringField
      FieldName = 'GROUP_TYPE'
      Visible = False
      Size = 1
    end
    object StringField3: TStringField
      DisplayLabel = 'Group Type'
      FieldKind = fkCalculated
      FieldName = 'GroupTypeName'
      Size = 30
      Calculated = True
    end
  end
  object AvaliableGroupsDatasource: TevDataSource
    DataSet = dsAvaliableGroups
    Left = 1181
    Top = 114
  end
  object AssignedGroupsDataSource: TevDataSource
    DataSet = dsAssignedGroups
    Left = 1221
    Top = 146
  end
  object pmW2FormatCopyTo: TevPopupMenu
    Left = 1174
    Top = 461
    object miW2FormatCopy: TMenuItem
      Caption = 'Copy To...'
      OnClick = miW2FormatCopyClick
    end
  end
  object pmDBDTCopy: TPopupMenu
    Left = 1088
    Top = 555
    object mnDBDTCopy: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnDBDTCopyClick
    end
  end
  object pmPosition: TPopupMenu
    Left = 1040
    Top = 595
    object mnPositionCopy: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnPositionCopyClick
    end
  end
  object pmAcaStatus: TPopupMenu
    Left = 744
    Top = 483
    object mnAcaStatus: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaStatusClick
    end
  end
  object pmAcaCoverage: TPopupMenu
    Left = 944
    Top = 507
    object mnAcaCoverage: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaCoverageClick
    end
  end
  object pmAcaLowestCost: TPopupMenu
    Left = 904
    Top = 603
    object mnAcaLowestCost: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaLowestCostClick
    end
  end
  object pmAcaPolicyOrigin: TPopupMenu
    Left = 944
    Top = 603
    object mnAcaPolicyOrigin: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaPolicyOriginClick
    end
  end
  object pmAcaSafeHarbor: TPopupMenu
    Left = 984
    Top = 603
    object mnAcaSafeHarbor: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaSafeHarborClick
    end
  end
  object cdCOBenefits: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CO_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_GROUP_MEMBER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GROUP_NAME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'GROUP_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GroupTypeName'
        DataType = ftString
        Size = 30
      end>
    OnCalcFields = dsAvaliableGroupsCalcFields
    Left = 832
    Top = 608
    object cdCOBenefitsCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object cdCOBenefitsBENEFIT_NAME: TStringField
      FieldName = 'BENEFIT_NAME'
      Size = 40
    end
  end
  object cdCOBenefitSubType: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CO_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_GROUP_MEMBER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GROUP_NAME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'GROUP_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GroupTypeName'
        DataType = ftString
        Size = 30
      end>
    OnCalcFields = dsAvaliableGroupsCalcFields
    Left = 800
    Top = 608
    object IntegerField2: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object cdCOBenefitSubTypeCO_BENEFIT_SUBTYPE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object cdCOBenefitSubTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
  end
  object pmEnableAnalytics: TevPopupMenu
    Left = 782
    Top = 397
    object mnEnableAnalytics: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnEnableAnalyticsClick
    end
  end
  object pmHealthCare: TPopupMenu
    Left = 624
    Top = 475
    object mnHealthCare: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnHealthCareClick
    end
  end
  object pmAcaFormat: TPopupMenu
    Left = 744
    Top = 523
    object mnAcaFormat: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaFormatClick
    end
  end
  object pmAcaFormType: TPopupMenu
    Left = 744
    Top = 555
    object mnAcaFormType: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAcaFormTypeClick
    end
  end
  object pmSSN: TPopupMenu
    Left = 920
    Top = 419
    object mnSSN: TMenuItem
      Caption = 'Employee Merge'
      OnClick = mnSSNClick
    end
  end
  object pmBenefitsEligible: TPopupMenu
    Left = 624
    Top = 515
    object mnBenefitsEligible: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnBenefitsEligibleClick
    end
  end
  object pmPBJWorks: TPopupMenu
    Left = 568
    Top = 475
    object mnPBJWorks: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnPBJWorksClick
    end
  end
  object dsACAHistory: TevDataSource
    OnDataChange = dsACAHistoryDataChange
    Left = 440
    Top = 304
  end
  object pmW2FormOnFile: TevPopupMenu
    Left = 1174
    Top = 501
    object mnW2FormOnFile: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnW2FormOnFileClick
    end
  end
end
