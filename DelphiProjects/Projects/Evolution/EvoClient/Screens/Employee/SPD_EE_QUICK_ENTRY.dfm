inherited EE_QUICK_ENTRY: TEE_QUICK_ENTRY
  Width = 706
  Height = 473
  inherited Panel1: TevPanel
    Width = 706
    inherited pnlFavoriteReport: TevPanel
      Left = 554
    end
    inherited pnlTopLeft: TevPanel
      Width = 554
    end
  end
  inherited PageControl1: TevPageControl
    Width = 706
    Height = 386
    ActivePage = tshtEEQuickEntry
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 698
        Height = 357
        inherited pnlBorder: TevPanel
          Width = 694
          Height = 353
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 694
          Height = 353
          inherited pnlFashionBody: TevPanel
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseLeft2: TisUIFashionPanel
                  inherited pnlFPEEBrowseLeftBody2: TevPanel
                    Left = 12
                  end
                end
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  inherited pnlFPEEBrowseRightBody2: TevPanel
                    Left = 12
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 646
            inherited bOpenFilteredByDBDTG: TevBitBtn
              Visible = False
            end
            inherited bOpenFilteredByEe: TevBitBtn
              Visible = False
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 646
            Height = 246
            inherited Splitter1: TevSplitter
              Height = 242
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 242
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 162
                IniAttributes.SectionName = 'TEE_QUICK_ENTRY\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 324
              Height = 242
              inherited wwDBGrid4: TevDBGrid
                Width = 274
                Height = 162
                IniAttributes.SectionName = 'TEE_QUICK_ENTRY\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    object tshtEEQuickEntry: TTabSheet
      Caption = 'EE Quick Entry'
      ImageIndex = 1
      OnShow = tshtEEQuickEntryShow
      object sbQuickEntry: TScrollBox
        Left = 0
        Top = 0
        Width = 698
        Height = 357
        Align = alClient
        TabOrder = 0
        object pnlEmpInfo: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 298
          Height = 389
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Employee Info'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label45: TevLabel
            Left = 12
            Top = 35
            Width = 31
            Height = 16
            Caption = '~SSN'
            FocusControl = EdtSSN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label69: TevLabel
            Left = 154
            Top = 35
            Width = 51
            Height = 16
            Caption = '~EE Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label46: TevLabel
            Left = 12
            Top = 74
            Width = 60
            Height = 16
            Caption = '~Last Name'
            FocusControl = dtLastName
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label47: TevLabel
            Left = 12
            Top = 113
            Width = 59
            Height = 16
            Caption = '~First Name'
            FocusControl = edtFirstName
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label62: TevLabel
            Left = 12
            Top = 152
            Width = 56
            Height = 16
            Caption = '~Address 1'
            FocusControl = edtAddress1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label68: TevLabel
            Left = 12
            Top = 191
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = edtAddress2
          end
          object Label67: TevLabel
            Left = 12
            Top = 230
            Width = 26
            Height = 16
            Caption = '~City'
            FocusControl = edtCity
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label65: TevLabel
            Left = 201
            Top = 230
            Width = 52
            Height = 16
            Caption = '~Zip Code'
            FocusControl = edtZIP
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label63: TevLabel
            Left = 12
            Top = 328
            Width = 68
            Height = 13
            Caption = 'Primary Phone'
            FocusControl = edtPhone
          end
          object Label79: TevLabel
            Left = 154
            Top = 328
            Width = 44
            Height = 16
            Caption = '~Gender'
            FocusControl = edtGender
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel27: TevLabel
            Left = 154
            Top = 230
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = cbResidentalState
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label52: TevLabel
            Left = 154
            Top = 289
            Width = 68
            Height = 16
            Caption = '~Date of Birth'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label53: TevLabel
            Left = 12
            Top = 289
            Width = 49
            Height = 16
            Caption = '~Ethnicity'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EvBevel1: TEvBevel
            Left = 12
            Top = 278
            Width = 265
            Height = 3
            Shape = bsTopLine
          end
          object EdtSSN: TevDBEdit
            Left = 12
            Top = 50
            Width = 134
            Height = 21
            HelpContext = 17713
            Color = clBtnFace
            DataField = 'SOCIAL_SECURITY_NUMBER'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*3{#,x}-*2{&,#}-*4{&,#}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = EdtSSNExit
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtEECode: TevDBEdit
            Left = 154
            Top = 50
            Width = 43
            Height = 21
            HelpContext = 17714
            Color = clBtnFace
            DataField = 'CUSTOM_EMPLOYEE_NUMBER'
            DataSource = wwdsSubMaster
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtFirstName: TevDBEdit
            Left = 12
            Top = 128
            Width = 265
            Height = 21
            HelpContext = 17715
            Color = clBtnFace
            DataField = 'FIRST_NAME'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object dtLastName: TevDBEdit
            Left = 12
            Top = 89
            Width = 265
            Height = 21
            HelpContext = 17715
            Color = clBtnFace
            DataField = 'LAST_NAME'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtAddress1: TevDBEdit
            Left = 12
            Top = 167
            Width = 265
            Height = 21
            HelpContext = 18002
            Color = clBtnFace
            DataField = 'ADDRESS1'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtAddress2: TevDBEdit
            Left = 12
            Top = 206
            Width = 265
            Height = 21
            HelpContext = 18002
            Color = clBtnFace
            DataField = 'ADDRESS2'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtCity: TevDBEdit
            Left = 12
            Top = 245
            Width = 134
            Height = 21
            HelpContext = 18002
            Color = clBtnFace
            DataField = 'CITY'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtZIP: TevDBEdit
            Left = 201
            Top = 245
            Width = 76
            Height = 21
            HelpContext = 18002
            Color = clBtnFace
            DataField = 'ZIP_CODE'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtPhone: TevDBEdit
            Left = 12
            Top = 343
            Width = 134
            Height = 21
            HelpContext = 18003
            Color = clBtnFace
            DataField = 'PHONE1'
            DataSource = wwdsSubMaster2
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtGender: TevDBComboBox
            Left = 154
            Top = 343
            Width = 123
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Color = clBtnFace
            DataField = 'GENDER'
            DataSource = wwdsSubMaster2
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Items.Strings = (
              'Male'#9'M'
              'Female'#9'F'
              'Non-Applicable'#9'U')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 13
            UnboundDataType = wwDefault
            BlockEffectiveDateEditor = True
          end
          object edtState: TevDBEdit
            Left = 159
            Top = 245
            Width = 25
            Height = 21
            HelpContext = 18002
            DataField = 'STATE'
            DataSource = wwdsSubMaster2
            Picture.PictureMask = '*{&,@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            Visible = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edtDateOfBirth: TevDBDateTimePicker
            Left = 154
            Top = 304
            Width = 123
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.YearsPerColumn = 20
            CalendarAttributes.PopupYearOptions.NumberColumns = 5
            CalendarAttributes.PopupYearOptions.StartYear = 1930
            Color = clBtnFace
            DataField = 'BIRTH_DATE'
            DataSource = wwdsSubMaster2
            Epoch = 1900
            Enabled = False
            ShowButton = True
            TabOrder = 11
            BlockEffectiveDateEditor = True
          end
          object cmbEthnicity: TevDBComboBox
            Left = 12
            Top = 304
            Width = 134
            Height = 21
            HelpContext = 17733
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Color = clBtnFace
            DataField = 'ETHNICITY'
            DataSource = wwdsSubMaster2
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Items.Strings = (
              'African American'#9'N'
              'Hispanic'#9'H'
              'Asian'#9'A'
              'American Indian'#9'I'
              'Caucasian'#9'C'
              'Other'#9'O'
              'Not Applicable'#9'P')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
            BlockEffectiveDateEditor = True
          end
          object cbResidentalState: TevDBLookupCombo
            Left = 154
            Top = 245
            Width = 43
            Height = 21
            HelpContext = 20010
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'RESIDENTIAL_STATE_NBR'
            DataSource = wwdsSubMaster2
            LookupTable = DM_SY_STATES.SY_STATES
            LookupField = 'SY_STATES_NBR'
            Style = csDropDownList
            Color = clBtnFace
            Enabled = False
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = cbResidentalStateChange
            BlockEffectiveDateEditor = True
          end
        end
        object pnlAdditionalInfo: TisUIFashionPanel
          Left = 314
          Top = 8
          Width = 282
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Taxation Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label60: TevLabel
            Left = 140
            Top = 152
            Width = 48
            Height = 13
            Caption = 'State Dep'
            FocusControl = edtStateDependents
          end
          object Label57: TevLabel
            Left = 12
            Top = 152
            Width = 101
            Height = 16
            Caption = '~State Marital Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 111
            Height = 16
            Caption = '~Federal Marital Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label54: TevLabel
            Left = 12
            Top = 74
            Width = 105
            Height = 16
            Caption = '~Federal Dependents'
            FocusControl = edtFedDependents
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label59: TevLabel
            Left = 184
            Top = 113
            Width = 27
            Height = 16
            Caption = '~SUI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label56: TevLabel
            Left = 12
            Top = 113
            Width = 34
            Height = 16
            Caption = '~State'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel7: TevLabel
            Left = 98
            Top = 113
            Width = 18
            Height = 13
            Caption = 'SDI'
          end
          object edtStateDependents: TevDBEdit
            Left = 140
            Top = 167
            Width = 120
            Height = 21
            HelpContext = 18015
            Color = clBtnFace
            DataField = 'STATE_NUMBER_WITHHOLDING_ALLOW'
            DataSource = wwdsDetail
            Enabled = False
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtStateMarital: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 120
            Height = 21
            HelpContext = 18014
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATUS_TYPE'#9'4'#9'STATUS_TYPE'
              'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION')
            DataField = 'STATE_MARITAL_STATUS'
            DataSource = wwdsDetail
            LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
            LookupField = 'STATUS_TYPE'
            Style = csDropDownList
            Color = clBtnFace
            Enabled = False
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = edtStateMaritalChange
            OnBeforeDropDown = edtStateMaritalBeforeDropDown
            OnCloseUp = edtStateMaritalCloseUp
            BlockEffectiveDateEditor = True
          end
          object cmbFedMarital: TevDBComboBox
            Left = 12
            Top = 50
            Width = 248
            Height = 21
            HelpContext = 17726
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Color = clBtnFace
            DataField = 'FEDERAL_MARITAL_STATUS'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Items.Strings = (
              'Single'#9'S'
              'Married'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            BlockEffectiveDateEditor = True
          end
          object edtFedDependents: TevDBEdit
            Left = 12
            Top = 89
            Width = 248
            Height = 21
            HelpContext = 18011
            Color = clBtnFace
            DataField = 'NUMBER_OF_DEPENDENTS'
            DataSource = wwdsSubMaster
            Enabled = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtSUI: TevDBLookupCombo
            Left = 184
            Top = 128
            Width = 76
            Height = 21
            HelpContext = 18017
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SUI_APPLY_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            Color = clBtnFace
            Enabled = False
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = edtSUICloseUp
            BlockEffectiveDateEditor = True
          end
          object wwlcState: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 78
            Height = 21
            HelpContext = 18013
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            Color = clBtnFace
            Enabled = False
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnDropDown = wwlcStateDropDown
            OnCloseUp = wwlcStateCloseUp
            BlockEffectiveDateEditor = True
          end
          object edtSDI: TevDBLookupCombo
            Left = 98
            Top = 128
            Width = 78
            Height = 21
            HelpContext = 18017
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SDI_APPLY_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            Color = clBtnFace
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = edtSDICloseUp
            BlockEffectiveDateEditor = True
          end
        end
        object pnlPayInfo: TisUIFashionPanel
          Left = 14
          Top = 405
          Width = 588
          Height = 206
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Pay'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label74: TevLabel
            Left = 196
            Top = 35
            Width = 71
            Height = 16
            Caption = '~Rate Amount'
            FocusControl = edtRateDBDT1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 381
            Top = 35
            Width = 58
            Height = 13
            Caption = 'DBDT Code'
            FocusControl = edtFiltrDBDT1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 381
            Top = 74
            Width = 58
            Height = 13
            Caption = 'DBDT Code'
            FocusControl = edtFiltrDBDT2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 381
            Top = 113
            Width = 58
            Height = 13
            Caption = 'DBDT Code'
            FocusControl = edtFiltrDBDT3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel
            Left = 196
            Top = 74
            Width = 62
            Height = 13
            Caption = 'Rate Amount'
            FocusControl = edtRateDBDT2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel6: TevLabel
            Left = 196
            Top = 113
            Width = 62
            Height = 13
            Caption = 'Rate Amount'
            FocusControl = edtRateDBDT3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label72: TevLabel
            Left = 12
            Top = 35
            Width = 80
            Height = 16
            Caption = '~Pay Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblSalaryCaption: TevLabel
            Left = 12
            Top = 74
            Width = 68
            Height = 13
            Caption = 'Salary Amount'
            FocusControl = edtSalaryAmount
          end
          object evlblWCCode: TevLabel
            Left = 12
            Top = 113
            Width = 55
            Height = 13
            Caption = 'Default WC'
          end
          object btnSelectDBDT1: TevSpeedButton
            Left = 543
            Top = 50
            Width = 23
            Height = 21
            HideHint = True
            AutoSize = False
            Action = SelectDBDT1
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
              77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
              7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
              8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
              7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
              AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
              2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
              7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
            ParentColor = False
            ShortCut = 115
          end
          object btnSelectDBDT2: TevSpeedButton
            Left = 543
            Top = 89
            Width = 23
            Height = 21
            HideHint = True
            AutoSize = False
            Action = SelectDBDT2
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
              77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
              7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
              8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
              7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
              AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
              2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
              7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
            ParentColor = False
            ShortCut = 115
          end
          object btnSelectDBDT3: TevSpeedButton
            Left = 543
            Top = 128
            Width = 23
            Height = 21
            HideHint = True
            AutoSize = False
            Action = SelectDBDT3
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
              77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
              7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
              8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
              7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
              AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
              2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
              7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
            ParentColor = False
            ShortCut = 115
          end
          object edtRateDBDT1: TevDBEdit
            Left = 196
            Top = 50
            Width = 176
            Height = 21
            HelpContext = 18008
            Color = clBtnFace
            DataField = 'RATE_AMOUNT'
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '[-]*12[#][.*4[#]]'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtFiltrDBDT1: TevDBEdit
            Left = 381
            Top = 50
            Width = 162
            Height = 21
            AutoSelect = False
            Color = clBtnFace
            Enabled = False
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = edtFiltrDBDT1Exit
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtFiltrDBDT2: TevDBEdit
            Left = 381
            Top = 89
            Width = 162
            Height = 21
            AutoSelect = False
            Color = clBtnFace
            Enabled = False
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = edtFiltrDBDT2Exit
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtFiltrDBDT3: TevDBEdit
            Left = 381
            Top = 128
            Width = 162
            Height = 21
            AutoSelect = False
            Color = clBtnFace
            Enabled = False
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnExit = edtFiltrDBDT3Exit
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtRateDBDT2: TevDBEdit
            Left = 196
            Top = 89
            Width = 176
            Height = 21
            HelpContext = 18008
            Color = clBtnFace
            DataField = 'RATE_AMOUNT'
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*12[#][.*4[#]]'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtRateDBDT3: TevDBEdit
            Left = 196
            Top = 128
            Width = 176
            Height = 21
            HelpContext = 18008
            Color = clBtnFace
            DataField = 'RATE_AMOUNT'
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*12[#][.*4[#]]'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object cmbPayFrequency: TevDBComboBox
            Left = 12
            Top = 50
            Width = 176
            Height = 21
            HelpContext = 18005
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Color = clBtnFace
            DataField = 'PAY_FREQUENCY'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'W'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'S'
              'Monthly'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            BlockEffectiveDateEditor = True
          end
          object edtSalaryAmount: TevDBEdit
            Left = 12
            Top = 89
            Width = 176
            Height = 21
            HelpContext = 18006
            Color = clBtnFace
            DataField = 'SALARY_AMOUNT'
            DataSource = wwdsSubMaster
            Enabled = False
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
          object edtWCCode: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 176
            Height = 21
            HelpContext = 19002
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F'
              'Co_State_Lookup'#9'2'#9'Co_State_Lookup'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsSubMaster
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            Color = clBtnFace
            Enabled = False
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            BlockEffectiveDateEditor = True
          end
          object btnAddScheduledEDs: TevButton
            Left = 12
            Top = 161
            Width = 176
            Height = 25
            Caption = 'EE Scheduled E/Ds'
            TabOrder = 10
            TabStop = False
            OnClick = btnAddScheduledEDsClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
          object btnOKNext: TevButton
            Left = 381
            Top = 161
            Width = 185
            Height = 25
            Caption = 'Post && Add Next Employee'
            TabOrder = 11
            OnClick = btnOKNextClick
            Color = clBlack
            Margin = 0
          end
          object edQEHiddenField: TEdit
            Left = 2000
            Top = 96
            Width = 121
            Height = 21
            TabOrder = 0
            Text = 'edQEHiddenField'
          end
        end
        object fpPosition: TisUIFashionPanel
          Left = 314
          Top = 226
          Width = 282
          Height = 171
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPosition'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Position'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label49: TevLabel
            Left = 12
            Top = 35
            Width = 91
            Height = 16
            Caption = '~Current Hire Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label50: TevLabel
            Left = 140
            Top = 35
            Width = 118
            Height = 13
            Caption = 'Current Termination Date'
          end
          object evLabel8: TevLabel
            Left = 12
            Top = 74
            Width = 104
            Height = 16
            Caption = '~Current Status Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel33: TevLabel
            Left = 140
            Top = 74
            Width = 110
            Height = 16
            Caption = '~Healthcare Coverage'
            FocusControl = cmbHealthCare
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTime_Clock_Number: TevLabel
            Left = 12
            Top = 113
            Width = 93
            Height = 13
            Caption = 'Time Clock Number'
            FocusControl = edtTimeClockNumber
          end
          object edtHireDate: TevDBDateTimePicker
            Left = 12
            Top = 50
            Width = 120
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            Color = clBtnFace
            DataField = 'CURRENT_HIRE_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            Enabled = False
            ShowButton = True
            TabOrder = 0
            BlockEffectiveDateEditor = True
          end
          object edtTerminationDate: TevDBDateTimePicker
            Left = 140
            Top = 50
            Width = 120
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            Color = clBtnFace
            DataField = 'CURRENT_TERMINATION_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            Enabled = False
            ShowButton = True
            TabOrder = 1
            BlockEffectiveDateEditor = True
          end
          object cmbSTatusCode: TevDBComboBox
            Left = 12
            Top = 89
            Width = 120
            Height = 21
            HelpContext = 17726
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Color = clBtnFace
            DataField = 'CURRENT_TERMINATION_CODE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Items.Strings = (
              'Active'#9'A'
              'Involuntary Layoff'#9'I'
              'Termination Due to Layoff'#9'L'
              'Release Without Prejudice'#9'F'
              'Voluntary Resignation'#9'R'
              'Termination Due to Retirement'#9'E'
              'Termination Due to Transfer'#9'T'
              'Leave of Absence'#9'V'
              'Seasonal'#9'S'
              'Termination Due to Death'#9'D'
              'Terminated'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
            BlockEffectiveDateEditor = True
          end
          object cmbHealthCare: TevDBComboBox
            Left = 140
            Top = 89
            Width = 120
            Height = 21
            HelpContext = 20009
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            Color = clBtnFace
            DataField = 'HEALTHCARE_COVERAGE'
            DataSource = wwdsSubMaster
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Items.Strings = (
              'No ER Paid Ins/Not Eligible'#9'N'
              'Eligible/Not Covered'#9'E'
              'Eligible/Covered'#9'I')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
            BlockEffectiveDateEditor = True
          end
          object edtTimeClockNumber: TevDBEdit
            Left = 12
            Top = 128
            Width = 120
            Height = 21
            Color = clBtnFace
            DataField = 'TIME_CLOCK_NUMBER'
            DataSource = wwdsSubMaster
            Enabled = False
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            BlockEffectiveDateEditor = True
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 706
  end
  inherited wwdsMaster: TevDataSource
    Left = 464
    Top = 42
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_STATES.EE_STATES
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    Left = 494
    Top = 42
  end
  inherited wwdsList: TevDataSource
    Left = 434
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 681
    Top = 47
  end
  inherited dsCL: TevDataSource
    Left = 623
    Top = 39
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 736
    Top = 31
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 706
    Top = 42
  end
  inherited wwdsEmployee: TevDataSource
    Left = 590
    Top = 40
  end
  inherited DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 652
  end
  inherited wwdsSubMaster: TevDataSource
    OnDataChange = wwdsSubMasterDataChange
    Left = 526
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 557
  end
  inherited ActionList: TevActionList
    Left = 371
    Top = 43
    object SelectDBDT1: TAction
      ImageIndex = 2
      OnExecute = SelectDBDT1Execute
    end
    object SelectDBDT2: TAction
      ImageIndex = 2
      OnExecute = SelectDBDT2Execute
    end
    object SelectDBDT3: TAction
      ImageIndex = 2
      OnExecute = SelectDBDT3Execute
    end
  end
  inherited EEClone: TevClientDataSet
    Left = 762
    Top = 26
  end
  object wwdsEERates: TevDataSource
    DataSet = DM_EE_RATES.EE_RATES
    OnDataChange = wwdsEmployeeDataChange
    Left = 430
    Top = 72
  end
  object SYMaritalSrc: TevDataSource
    DataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
    Left = 774
    Top = 112
  end
  object SYStatesSrc: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 775
    Top = 67
  end
end
