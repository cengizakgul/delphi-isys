// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_AskEDUpdateParamsForPercent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_AskGlobalUpdateParamsBase, ActnList,  ExtCtrls,
  StdCtrls, Buttons, EvUIComponents, isUIEdit, ISBasicClasses,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TAskEDUpdateParamsForPercent = class(TAskGlobalUpdateParamsBase)
    evEdit2: TevEdit;
    evLabel1: TevLabel;
    procedure OKUpdate(Sender: TObject);
  private
    procedure SetPercent(const Value: Variant);
    function GetPercent: Variant;
    { Private declarations }
  public
    { Public declarations }
    property Percent: Variant read GetPercent write SetPercent;
  end;

implementation

{$R *.dfm}

function TAskEDUpdateParamsForPercent.GetPercent: Variant;
begin
  Result := evEdit2.Text;
end;

procedure TAskEDUpdateParamsForPercent.SetPercent(const Value: Variant);
begin
  evEdit2.Text := VarToStr( Value );
end;

procedure TAskEDUpdateParamsForPercent.OKUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := IsFloat(evEdit2.Text);
end;

end.
