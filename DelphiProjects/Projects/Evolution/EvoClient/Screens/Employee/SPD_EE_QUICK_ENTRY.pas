unit SPD_EE_QUICK_ENTRY;

interface

uses
  SShowDataset,
  SDBDT, SPD_DBDTSelectionListFiltr,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_EE_BASE, SDataStructure, isVCLBugFix,
  EvSecElement, ExtDlgs, DB, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents,  DBActns,
  ActnList, ISBasicClasses, SDDClasses, Wwdatsrc, SPD_HideRecordHelper,
  StdCtrls, SPD_PersonDocuments, DBCtrls, wwdbdatetimepicker, wwdblook,
  ExtCtrls, Wwdotdot, Wwdbcomb, Mask, wwdbedit, Buttons, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, SDataDictclient, SDataDicttemp, EvContext, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEE_QUICK_ENTRY = class(TEDIT_EE_BASE)
    tshtEEQuickEntry: TTabSheet;
    sbQuickEntry: TScrollBox;
    wwdsEERates: TevDataSource;
    SelectDBDT1: TAction;
    SelectDBDT2: TAction;
    SelectDBDT3: TAction;
    SYMaritalSrc: TevDataSource;
    SYStatesSrc: TevDataSource;
    pnlEmpInfo: TisUIFashionPanel;
    Label45: TevLabel;
    Label69: TevLabel;
    Label46: TevLabel;
    Label47: TevLabel;
    Label62: TevLabel;
    Label68: TevLabel;
    Label67: TevLabel;
    Label65: TevLabel;
    Label63: TevLabel;
    Label79: TevLabel;
    evLabel27: TevLabel;
    EdtSSN: TevDBEdit;
    edtEECode: TevDBEdit;
    edtFirstName: TevDBEdit;
    dtLastName: TevDBEdit;
    edtAddress1: TevDBEdit;
    edtAddress2: TevDBEdit;
    edtCity: TevDBEdit;
    edtZIP: TevDBEdit;
    edtPhone: TevDBEdit;
    edtGender: TevDBComboBox;
    cbResidentalState: TevDBLookupCombo;
    edtState: TevDBEdit;
    pnlAdditionalInfo: TisUIFashionPanel;
    Label60: TevLabel;
    Label57: TevLabel;
    evLabel1: TevLabel;
    Label54: TevLabel;
    Label59: TevLabel;
    Label56: TevLabel;
    evLabel7: TevLabel;
    edtStateDependents: TevDBEdit;
    edtStateMarital: TevDBLookupCombo;
    cmbFedMarital: TevDBComboBox;
    edtFedDependents: TevDBEdit;
    edtSUI: TevDBLookupCombo;
    wwlcState: TevDBLookupCombo;
    edtSDI: TevDBLookupCombo;
    pnlPayInfo: TisUIFashionPanel;
    Label74: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    Label72: TevLabel;
    lblSalaryCaption: TevLabel;
    evlblWCCode: TevLabel;
    edtRateDBDT1: TevDBEdit;
    edtFiltrDBDT1: TevDBEdit;
    edtFiltrDBDT2: TevDBEdit;
    edtFiltrDBDT3: TevDBEdit;
    edtRateDBDT2: TevDBEdit;
    edtRateDBDT3: TevDBEdit;
    cmbPayFrequency: TevDBComboBox;
    edtSalaryAmount: TevDBEdit;
    edtWCCode: TevDBLookupCombo;
    btnSelectDBDT1: TevSpeedButton;
    btnSelectDBDT2: TevSpeedButton;
    btnSelectDBDT3: TevSpeedButton;
    Label52: TevLabel;
    edtDateOfBirth: TevDBDateTimePicker;
    Label53: TevLabel;
    cmbEthnicity: TevDBComboBox;
    btnAddScheduledEDs: TevButton;
    btnOKNext: TevButton;
    fpPosition: TisUIFashionPanel;
    Label49: TevLabel;
    Label50: TevLabel;
    evLabel8: TevLabel;
    evLabel33: TevLabel;
    edtHireDate: TevDBDateTimePicker;
    edtTerminationDate: TevDBDateTimePicker;
    cmbSTatusCode: TevDBComboBox;
    cmbHealthCare: TevDBComboBox;
    lablTime_Clock_Number: TevLabel;
    edtTimeClockNumber: TevDBEdit;
    EvBevel1: TEvBevel;
    edQEHiddenField: TEdit;
    procedure tshtEEQuickEntryShow(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure SelectDBDT1Execute(Sender: TObject);
    procedure SelectDBDT2Execute(Sender: TObject);
    procedure SelectDBDT3Execute(Sender: TObject);
    procedure EENextExecute(Sender: TObject);
    procedure EEPriorExecute(Sender: TObject);
    procedure EdtSSNExit(Sender: TObject);
    procedure edtStateChange(Sender: TObject);
    procedure cbResidentalStateChange(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure btnOKNextClick(Sender: TObject);
    procedure edtFiltrDBDT1Exit(Sender: TObject);
    procedure edtFiltrDBDT2Exit(Sender: TObject);
    procedure edtFiltrDBDT3Exit(Sender: TObject);
    procedure wwlcStateDropDown(Sender: TObject);
    procedure wwlcStateCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure edtStateMaritalChange(Sender: TObject);
    procedure btnAddScheduledEDsClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure edtStateMaritalBeforeDropDown(Sender: TObject);
    procedure edtSDICloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure edtSUICloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwdbgridSelectClientDblClick(Sender: TObject);
    procedure edtStateMaritalCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  private
    FInserting: boolean;
    FMartitalStatusNeedsChanging: boolean;
    FOldwwlcStateValue: string;
    FDBDT1: TDBDT;
    FDBDT2: TDBDT;
    FDBDT3: TDBDT;
    procedure InitGUI;
    procedure InitDBDTSelectionListFiltrs;
    procedure AssignSelectedDBDT1;
    procedure AssignSelectedDBDT2;
    procedure AssignSelectedDBDT3;
//    procedure ReshuffleDataSetsToPost;
    function IsValidSSN(SSN: string): Boolean;
    procedure AssignSelectedDBDT(DBDT: TDBDT; RateNumber: integer;
      RateAmount: Double; IsPrimaryRate: boolean);
    procedure GetRateAndDBDT(DBDT: TDBDT; RateNumber: integer; var Rate: string; var DBDTText: string);
    procedure ToggleEnabledColor;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS;
      var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Activate; override;
    procedure Deactivate; override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet; var SupportDataSets: TArrayDS);
      override;
    property DBDT1: TDBDT read FDBDT1;
    property DBDT2: TDBDT read FDBDT2;
    property DBDT3: TDBDT read FDBDT3;
  end;

resourcestring
  SSNAlreadyExist =
    'The Social Security Number entered already exists.'#13#10 +
    'Please use the Employee screen to edit this employee';
  SSNInvalid = 'Social Security Number is invalid';
  StateMaritalStatusConsistencyViolation =
    'You must re-set the state marital status any time the state is changed!';
  SUIStateMustHaveValue = 'Field ''SUI State'' must have a value!';

implementation
{$R *.dfm}
uses
  evUtils, evConsts, SPackageEntry, evTypes, SEmployeeScr;

constructor TEE_QUICK_ENTRY.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDBDT1 := TDBDT.Create(edtFiltrDBDT1);
  FDBDT2 := TDBDT.Create(edtFiltrDBDT2);
  FDBDT3 := TDBDT.Create(edtFiltrDBDT3);
end;

destructor TEE_QUICK_ENTRY.Destroy;
begin
  FreeAndNil(FDBDT3);
  FreeAndNil(FDBDT2);
  FreeAndNil(FDBDT1);
  inherited Destroy;
end;

procedure TEE_QUICK_ENTRY.Activate;
begin
  inherited Activate;
  InitGUI;
  SYMaritalSrc.MasterDataSource := SYStatesSrc;
end;

procedure TEE_QUICK_ENTRY.Deactivate;
begin
  inherited Deactivate;
end;

function TEE_QUICK_ENTRY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE;
end;

procedure TEE_QUICK_ENTRY.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);
  AddDS(DM_COMPANY.CO_HR_POSITIONS, aDS);
  AddDS(DM_COMPANY.CO_HR_SUPERVISORS, aDS);
  AddDS(DM_COMPANY.CO_UNIONS, aDS);
  AddDS(DM_COMPANY.CO_HR_RECRUITERS, aDS);
  AddDS(DM_COMPANY.CO_HR_REFERRALS, aDS);
  AddDS(DM_COMPANY.CO_HR_PERFORMANCE_RATINGS, aDS);
  AddDS(DM_COMPANY.CO_HR_CAR, aDS);
  AddDS(DM_CLIENT.CL_DELIVERY_GROUP, aDS);
  AddDS(DM_COMPANY.CO_PAY_GROUP, aDS);
  AddDS(DM_COMPANY.CO_PR_CHECK_TEMPLATES, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_CLIENT.CL_PERSON_DEPENDENTS, aDS);
  AddDS(DM_EMPLOYEE.EE_STATES, aDS);
  AddDS(DM_EMPLOYEE.EE_RATES, aDS);
  AddDS(DM_EMPLOYEE.EE_WORK_SHIFTS, aDS);
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL, aDS);
  AddDS(DM_EMPLOYEE.EE_SCHEDULED_E_DS, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_EMPLOYEE.EE_LOCALS, aDS);
  AddDS(DM_COMPANY.CO_HR_SALARY_GRADES, aDS);
  AddDS(DM_CLIENT.CL_PERSON_DOCUMENTS, aDS);
  AddDS(DM_COMPANY.CO_HR_APPLICANT, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  
  inherited;
end;

procedure TEE_QUICK_ENTRY.tshtEEQuickEntryShow(Sender: TObject);
begin
  inherited;
  InitGui;
end;

procedure TEE_QUICK_ENTRY.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
var
  EmployeeFrm: TEmployeeFrm;
begin
  EmployeeFrm := Owner as TEmployeeFrm;
  case PageControl1.ActivePageIndex of
    1:
      begin
        if FInserting then
        begin
          // Only change the page if the user agrees to discard changes
          AllowChange :=
            MessageDlg('Discard changes?', mtConfirmation, mbOKCancel, 0) =
            mrOK;
          if AllowChange and Assigned(EmployeeFrm) then
            EmployeeFrm.btnNavCancel.Click;
        end;
      end;
  end;
  //GUI 2.0 FIX PER QA. MAKE SALARY LEFT ALIGNED TO BETTER MATCH OTHER FIELDS.
  DM_EMPLOYEE.EE.FieldByName('SALARY_AMOUNT').Alignment := taLeftJustify;
  inherited;
end;

procedure TEE_QUICK_ENTRY.PageControl1Change(Sender: TObject);
//var
//  EmployeeFrm: TEmployeeFrm;
begin
  inherited;
//  EmployeeFrm := Owner as TEmployeeFrm;
  case PageControl1.ActivePageIndex of
    0: InitGui;
    1:
      begin
        InitGui;
        if not FInserting then
        begin
          // This piece of code does not work as expected. It generates:
          // exception 8, Can not create employee. Missing division
          // I have commented it out for that reason alone
          // EmployeeFrm.btnNavInsert.Click;
        end;
      end;
  end;
end;

procedure TEE_QUICK_ENTRY.InitGUI;
var
  EmployeeFrm: TEmployeeFrm;
begin
  EmployeeFrm := Owner as TEmployeeFrm;

  // Main form controls
  // Note: iSystems.exe will instantiate the frames without the owner when
  // building security, so this check is absolutely nescessary.
  if Assigned(EmployeeFrm) then
  begin
    EmployeeFrm.btnNavFirst.Enabled := EmployeeFrm.btnNavFirst.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
    EmployeeFrm.btnNavPrevious.Enabled := EmployeeFrm.btnNavPrevious.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
    EmployeeFrm.btnNavNext.Enabled := EmployeeFrm.btnNavNext.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
    EmployeeFrm.btnNavLast.Enabled := EmployeeFrm.btnNavLast.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
    EmployeeFrm.btnNavInsert.Enabled := not FInserting and
      (PageControl1.ActivePage = tshtEEQuickEntry);
    EmployeeFrm.btnNavDelete.Enabled := EmployeeFrm.btnNavDelete.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
    EmployeeFrm.btnNavHistory.Enabled := EmployeeFrm.btnNavHistory.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
    EmployeeFrm.btnRefresh.Enabled := EmployeeFrm.btnRefresh.Enabled and
      (PageControl1.ActivePage <> tshtEEQuickEntry);
  end;

  // Above tabsheet controls
  butnPrior.Enabled := butnPrior.Enabled and
    (PageControl1.ActivePage <> tshtEEQuickEntry);
  butnNext.Enabled := butnNext.Enabled and
    (PageControl1.ActivePage <> tshtEEQuickEntry);
  dblkupEE_NBR.Enabled := dblkupEE_NBR.Enabled and
    (PageControl1.ActivePage <> tshtEEQuickEntry);
  dbtxtname.Enabled := dbtxtname.Enabled and
    (PageControl1.ActivePage <> tshtEEQuickEntry);

  // Quick entree Tabsheet controls
  if PageControl1.ActivePage = tshtEEQuickEntry then
  begin
    EdtSSN.Enabled := FInserting;
    edtEECode.Enabled := FInserting;
    edtFirstName.Enabled := FInserting;
    dtLastName.Enabled := FInserting;
    edtAddress1.Enabled := FInserting;
    edtAddress2.Enabled := FInserting;
    edtCity.Enabled := FInserting;
    cbResidentalState.Enabled := FInserting;
    edtZIP.Enabled := FInserting;
    edtPhone.Enabled := FInserting;
    edtGender.Enabled := FInserting;
    edtTimeClockNumber.Enabled := FInserting;
    edtRateDBDT1.Enabled := FInserting;
    edtFiltrDBDT1.Enabled := FInserting;
    btnSelectDBDT1.Enabled := FInserting;
    edtRateDBDT2.Enabled := FInserting;
    edtFiltrDBDT2.Enabled := FInserting;
    btnSelectDBDT2.Enabled := FInserting;
    edtRateDBDT3.Enabled := FInserting;
    edtFiltrDBDT3.Enabled := FInserting;
    btnSelectDBDT3.Enabled := FInserting;
    cmbEthnicity.Enabled := FInserting;
    cmbFedMarital.Enabled := FInserting;
    edtFedDependents.Enabled := FInserting;
    edtStateMarital.Enabled := FInserting;
    cmbHealthCare.Enabled :=  FInserting and (DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Value, 'STATE') = 'VT');
    edtStateDependents.Enabled := FInserting;
    wwlcState.Enabled := FInserting;
    edtSDI.Enabled := FInserting;
    edtSUI.Enabled := FInserting;
    edtDateOfBirth.Enabled := FInserting;
    edtHireDate.Enabled := FInserting;
    edtTerminationDate.Enabled := FInserting;
    cmbStatusCode.Enabled := FInserting;
    edtState.Enabled := FInserting;
    edtSalaryAmount.Enabled := FInserting;
    cmbPayFrequency.Enabled := FInserting;
    edtWCCode.Enabled := FInserting;
    btnOKNext.Enabled := FInserting;
    btnAddScheduledEDs.Enabled := not FInserting;
    InitDBDTSelectionListFiltrs;
  end;

  if ctx_AccountRights.Functions.GetState('FIELDS_SSN') <> stEnabled then
   Begin
    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
    EdtSSN.Enabled := False;
   end;
end;

procedure TEE_QUICK_ENTRY.InitDBDTSelectionListFiltrs;
var
  rate, dbdttext: string;
begin
  DBDT1.InitSelectionFilter(DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH,
    DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM,
    wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value,
    DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1]);
  DBDT2.InitSelectionFilter(DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH,
    DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM,
    wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value,
    DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1]);
  DBDT3.InitSelectionFilter(DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH,
    DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM,
    wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value,
    wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value,
    DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1]);

  if FInserting then
  begin
    edtRateDBDT1.Text := '';
    edtRateDBDT2.Text := '';
    edtRateDBDT3.Text := '';
    edtFiltrDBDT1.Text := '';
    edtFiltrDBDT2.Text := '';
    edtFiltrDBDT3.Text := '';
  end
  else
  begin
    GetRateAndDBDT(DBDT1, 1, rate, dbdttext);
    edtRateDBDT1.Text := rate;
    edtFiltrDBDT1.Text := dbdttext;

    GetRateAndDBDT(DBDT2, 2, rate, dbdttext);
    edtRateDBDT2.Text := rate;
    edtFiltrDBDT2.Text := dbdttext;

    GetRateAndDBDT(DBDT3, 3, rate, dbdttext);
    edtRateDBDT3.Text := rate;
    edtFiltrDBDT3.Text := dbdttext;
  end;

  if DM_COMPANY.CO.DBDT_LEVEL.AsString <> CLIENT_LEVEL_COMPANY then
    evLabel2.Caption := '~DBDT Code'
  else
    evLabel2.Caption := 'DBDT Code';
end;

procedure TEE_QUICK_ENTRY.SelectDBDT1Execute(Sender: TObject);
begin
  inherited;
  if DBDT1.ShowModal = mrOK then
    AssignSelectedDBDT1;
end;

procedure TEE_QUICK_ENTRY.SelectDBDT2Execute(Sender: TObject);
begin
  inherited;
  if DBDT2.ShowModal = mrOK then
    AssignSelectedDBDT2;
end;

procedure TEE_QUICK_ENTRY.SelectDBDT3Execute(Sender: TObject);
begin
  inherited;
  if DBDT3.ShowModal = mrOK then
    AssignSelectedDBDT3;
end;

procedure TEE_QUICK_ENTRY.edtFiltrDBDT1Exit(Sender: TObject);
begin
  inherited;
  if DBDT1.IsValidDBDT then
    edtFiltrDBDT1.Text := DBDT1.DBDT
  else
    edtFiltrDBDT1.Text := '';
end;

procedure TEE_QUICK_ENTRY.edtFiltrDBDT2Exit(Sender: TObject);
begin
  inherited;
  if DBDT2.IsValidDBDT then
    edtFiltrDBDT2.Text := DBDT2.DBDT
  else
    edtFiltrDBDT2.Text := '';
end;

procedure TEE_QUICK_ENTRY.edtFiltrDBDT3Exit(Sender: TObject);
begin
  inherited;
  if DBDT3.IsValidDBDT then
    edtFiltrDBDT3.Text := DBDT3.DBDT
  else
    edtFiltrDBDT3.Text := '';
end;

procedure TEE_QUICK_ENTRY.AssignSelectedDBDT(DBDT: TDBDT; RateNumber: integer;
  RateAmount: Double; IsPrimaryRate: boolean);
begin
  if (DBDT.SelectionListFiltr.wwcsTempDBDT.RecordCount = 0) or DBDT.IsValidDBDT then
  begin
    ctx_DataAccess.TempCommitDisable := True;
    try
      if DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER',
        VarArrayOf([wwdsEmployee.DataSet.FieldByName('EE_NBR').Value, RateNumber]), []) then
        wwdsEERates.DataSet.Edit
      else
      begin
        wwdsEERates.DataSet.Insert;
        wwdsEERates.DataSet.FieldByName('EE_NBR').Value :=
          wwdsEmployee.DataSet.FieldByName('EE_NBR').Value;
        wwdsEERates.DataSet.FieldByName('RATE_NUMBER').Value := RateNumber;
      end;
      if (DBDT.SelectionListFiltr.wwcsTempDBDT.RecordCount > 0) then
      begin
        wwdsEERates.DataSet.FieldByName('CO_DIVISION_NBR').Value :=
          DBDT.DivisionNbr;
        wwdsEERates.DataSet.FieldByName('CO_BRANCH_NBR').Value :=
          DBDT.BranchNbr;
        wwdsEERates.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value :=
          DBDT.DepartmentNbr;
        wwdsEERates.DataSet.FieldByName('CO_TEAM_NBR').Value := DBDT.TeamNbr;
      end;
      wwdsEERates.DataSet.FieldByName('RATE_AMOUNT').Value := RateAmount;
      if IsPrimaryRate then
        wwdsEERates.DataSet.FieldByName('PRIMARY_RATE').Value := 'Y'
      else
        wwdsEERates.DataSet.FieldByName('PRIMARY_RATE').Value := 'N';
    finally
      ctx_DataAccess.TempCommitDisable := False;
    end;
  end;
end;

procedure TEE_QUICK_ENTRY.AssignSelectedDBDT1;
begin
  if DBDT1.IsValidDBDT then
  begin
    if Trim(edtRateDBDT1.Text) = '' then
      edtRateDBDT1.Text := '0';
//    AssignSelectedDBDT(DBDT1, 1, StrToFloat(edtRateDBDT1.Text), True);
  end;
end;

procedure TEE_QUICK_ENTRY.AssignSelectedDBDT2;
begin
  if DBDT2.IsValidDBDT then
  begin
    if Trim(edtRateDBDT2.Text) = '' then
      edtRateDBDT2.Text := '0';
//    AssignSelectedDBDT(DBDT2, 2, StrToFloat(edtRateDBDT2.Text), False);
  end;
end;

procedure TEE_QUICK_ENTRY.AssignSelectedDBDT3;
begin
  if DBDT3.IsValidDBDT then
  begin
    if Trim(edtRateDBDT3.Text) = '' then
      edtRateDBDT3.Text := '0';
//    AssignSelectedDBDT(DBDT3, 3, StrToFloat(edtRateDBDT3.Text), False);
  end;
end;

procedure TEE_QUICK_ENTRY.wwdsSubMasterDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE_STATES.Active and (DM_EMPLOYEE.EE_STATES.State = dsBrowse) then
  begin
    cmbHealthCare.Enabled := DM_EMPLOYEE.EE_STATES.Locate('EE_NBR;SUI_APPLY_CO_STATES_NBR', VarArrayOf([wwdsSubMaster.DataSet['EE_NBR'], DM_COMPANY.CO_STATES.Lookup('STATE', 'VT', 'CO_STATES_NBR')]), []);
    //the above line is BOGUS. It's overriding the FInserting variable and enabling this when it should not. Turn it back off if not inserting.
    if not FInserting then cmbHealthCare.Enabled := False;
    DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', wwdsSubMaster.DataSet['HOME_TAX_EE_STATES_NBR'], []);
  end;
  if DM_EMPLOYEE.EE_RATES.Active and (DM_EMPLOYEE.EE_RATES.State = dsBrowse) then
    DM_EMPLOYEE.EE_RATES.Locate('EE_NBR; PRIMARY_RATE', VarArrayOf([wwdsSubMaster.DataSet['EE_NBR'], 'Y']), []);
end;

procedure TEE_QUICK_ENTRY.EENextExecute(Sender: TObject);
begin
  inherited;
  InitDBDTSelectionListFiltrs;
end;

procedure TEE_QUICK_ENTRY.EEPriorExecute(Sender: TObject);
begin
  inherited;
  InitDBDTSelectionListFiltrs;
end;

{ This is a BS algorithm  (see Aleksey if you need to uncomment it)
procedure TEE_QUICK_ENTRY.ReshuffleDataSetsToPost;
var
  DataSetsToPost: TArrayDS;
  CLPerson: TevClientDataSet;
  I, J: integer;
begin
  DataSetsToPost := (Owner as TEmployeeFrm).DataSetsToPost;
  if Assigned(DataSetsToPost) then
  begin
    for I := High(DataSetsToPost) downto Low(DataSetsToPost) do
    begin
      if SameText(DataSetsToPost[I].Name, 'CL_PERSON') then // Find CL_PERSON
      begin
        CLPerson := DataSetsToPost[I];
        for J := I - 1 downto Low(DataSetsToPost) do
          // Remove CL_PERSON by moving the rest of the items in the array
          // one position to the right
          DataSetsToPost[J + 1] := DataSetsToPost[J];
        DataSetsToPost[Low(DataSetsToPost)] := CLPerson;
      end;
    end;
  end;
end;
}

procedure TEE_QUICK_ENTRY.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  case Kind of
    NavInsert:
      begin
        FInserting := True;
        edtEECode.Text := '';
        InitGUI;
        EdtSSN.SetFocus;
      end;
    NavCancel: FInserting := False;
    NavOK:
      begin
        if not IsValidSSN(EdtSSN.Text) then
          raise EInconsistentData.Create(SSNInvalid);
        if FMartitalStatusNeedsChanging then
          raise EInconsistentData.Create(StateMaritalStatusConsistencyViolation);
        if trim(edtSUI.Text) = '' then
          raise EInconsistentData.Create(SUIStateMustHaveValue);
        if trim(edtHireDate.Text) = '' then
          raise EUpdateError.CreateHelp('Current Hire Date can''t be empty', IDH_ConsistencyViolation);


        if edtRateDBDT1.Text = '' then
          raise EUpdateError.CreateHelp('Rate Amount can''t be empty.', IDH_ConsistencyViolation);

        if (DM_COMPANY.CO.DBDT_LEVEL.AsString <> CLIENT_LEVEL_COMPANY) and (edtFiltrDBDT1.Text = '') then
          raise EUpdateError.CreateHelp('DBDT can''t be empty', IDH_ConsistencyViolation);
                                            
        if (edtDateOfBirth.Text = '') and ( DM_CLIENT.EE.COMPANY_OR_INDIVIDUAL_NAME.AsString <> GROUP_BOX_COMPANY) then
          raise EUpdateError.CreateHelp('Date of Birth can''t be empty', IDH_ConsistencyViolation);

        if edtRateDBDT1.Text <> '' then
        begin
          try
            AssignSelectedDBDT(DBDT1, 1, StrToFloat(edtRateDBDT1.Text), True);
          except
            raise EUpdateError.CreateHelp('DBDT is not valid', IDH_ConsistencyViolation);
          end;
        end;


        if edtRateDBDT2.Text <> '' then
        begin
          try
            AssignSelectedDBDT(DBDT2, 2, StrToFloat(edtRateDBDT2.Text), False);
          except
            raise EUpdateError.CreateHelp('DBDT2 is not valid', IDH_ConsistencyViolation);
          end;
        end;
        if edtRateDBDT3.Text <> '' then
        begin
          try
            AssignSelectedDBDT(DBDT3, 3, StrToFloat(edtRateDBDT3.Text), False);
          except
            raise EUpdateError.CreateHelp('DBDT3 is not valid', IDH_ConsistencyViolation);
          end;
        end;
        // When creating new Employee, copy DBDTs from Employee Rate #1 to Employee if Employee's DBDTs are empty.
        // Issue #47916. Andrew.
        if wwdsSubMaster.State = dsInsert then
        begin
          if (DM_CLIENT.EE['CO_DIVISION_NBR'] = Null) and (DM_CLIENT.EE['CO_BRANCH_NBR'] = Null)
            and (DM_CLIENT.EE['CO_DEPARTMENT_NBR'] = Null) and (DM_CLIENT.EE['CO_TEAM_NBR'] = Null) then
          begin
            wwdsSubMaster.DataSet.FieldByName('CO_DIVISION_NBR').Value := DBDT1.DivisionNbr;
            wwdsSubMaster.DataSet.FieldByName('CO_BRANCH_NBR').Value := DBDT1.BranchNbr;
            wwdsSubMaster.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value := DBDT1.DepartmentNbr;
            wwdsSubMaster.DataSet.FieldByName('CO_TEAM_NBR').Value := DBDT1.TeamNbr;
          end;
        end;
      end;
    NavCommit:
      begin
//        ReshuffleDataSetsToPost;
        FInserting := False;
      end;
  end;
end;

procedure TEE_QUICK_ENTRY.AfterClick(Kind: Integer);
begin
  inherited;
  InitGUI;
end;

procedure TEE_QUICK_ENTRY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDS(DM_CLIENT.CL_PERSON, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON, InsertDataSets);
  inherited;
  AddDS(DM_CLIENT.CL_PERSON_DEPENDENTS, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON_DOCUMENTS, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_STATES, InsertDataSets);
  AddDS(DM_EMPLOYEE.EE_STATES, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_RATES, EditDataSets);
//  AddDS(DM_EMPLOYEE.EE_RATES, InsertDataSets);
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_SCHEDULED_E_DS, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_WORK_SHIFTS, EditDataSets);
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
  AddDS(DM_EMPLOYEE.EE_LOCALS, EditDataSets);
  AddDSWithCheck(DM_SYSTEM_HR.SY_HR_EEO, SupportDataSets, '');
  AddDS(DM_COMPANY.CO_HR_APPLICANT, EditDataSets);
end;

function TEE_QUICK_ENTRY.GetInsertControl: TWinControl;
begin
  Result := EdtSSN;
end;

function TEE_QUICK_ENTRY.IsValidSSN(SSN: string): Boolean;
var
  I: Integer;
begin
  Result := Length(SSN) = 11;
  if Result then
  begin
    for I := 1 to Length(SSN) do
    begin
      Result := (SSN[I] in ['0'..'9']) or
        ((SSN[I] = '-') and (I in [4, 7]));
      if not Result then
        Exit;
    end;
  end;

  if DM_CLIENT.CL['BLOCK_INVALID_SSN'] = 'Y' then
    CheckSSN(SSN);
end;

procedure TEE_QUICK_ENTRY.EdtSSNExit(Sender: TObject);
var
  TempSSN: string;
  ClientDataSet: TevClientDataSet;
begin
  inherited;
  if FInserting then
  begin
    DM_CLIENT.CL_PERSON.DisableControls;
    try
      ClientDataSet := TevClientDataSet.Create(Self);
      try
        ClientDataSet.CloneCursor(DM_CLIENT.CL_PERSON, True);
        ClientDataSet.Filtered := False;
        TempSSN := DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
        if ClientDataSet.Locate('SOCIAL_SECURITY_NUMBER', TempSSN, []) then
        begin
          EdtSSN.SetFocus;
          raise EInconsistentData.Create(SSNAlreadyExist);
        end;
      finally
        ClientDataSet.Free;
      end;
    finally
      DM_CLIENT.CL_PERSON.EnableControls;
    end;
    if not (DM_CLIENT.CL_PERSON['EIN_OR_SOCIAL_SECURITY_NUMBER'] = GROUP_BOX_SSN) and
      (IsValidSSN(EdtSSN.Text)) then
    begin
      EdtSSN.SetFocus;
      raise EInconsistentData.Create(SSNInvalid);
    end
    else
    begin
      edtEECode.SetFocus;
      edtEECode.Text := '';
    end;
  end;
end;

procedure TEE_QUICK_ENTRY.edtStateChange(Sender: TObject);
begin
  inherited;
  if FInserting then
  begin
    if DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CL_PERSON.STATE.AsString, []) then
      DM_CLIENT.CL_PERSON.RESIDENTIAL_STATE_NBR.AsInteger := DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.AsInteger;
  end;
end;

procedure TEE_QUICK_ENTRY.cbResidentalStateChange(Sender: TObject);
begin
  inherited;
  edtState.Text := cbResidentalState.Text;
end;

procedure TEE_QUICK_ENTRY.btnOKNextClick(Sender: TObject);
var
  EmployeeFrm: TEmployeeFrm;
begin
  inherited;
  if Assigned(Owner) then
  begin
    EmployeeFrm := Owner as TEmployeeFrm;
    EmployeeFrm.btnNavOk.Click;
    EmployeeFrm.btnNavInsert.Click;
  end;
end;

procedure TEE_QUICK_ENTRY.wwlcStateDropDown(Sender: TObject);
begin
  inherited;
  FOldwwlcStateValue := wwlcState.Value;
end;

procedure TEE_QUICK_ENTRY.wwlcStateCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  FMartitalStatusNeedsChanging :=
    FMartitalStatusNeedsChanging or (wwlcState.Value <> FOldwwlcStateValue);
  if Assigned(wwdsSubMaster.DataSet) and
    (wwdsSubMaster.DataSet.State = dsInsert) and (wwlcState.Value <> '') then
  begin
    DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition :=
      'SY_STATES_NBR=' +
      DM_COMPANY.CO_STATES.FieldByName('SY_STATES_NBR').AsString;
    if DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RecordCount = 1 then
    begin
      wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Value :=
        DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.FieldByName('STATUS_TYPE').Value;
      FMartitalStatusNeedsChanging := False;
    end
    else
      if VarIsNull(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Lookup('STATUS_TYPE',
        wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Value,
        'SY_STATE_MARITAL_STATUS_NBR')) then
        wwdsDetail.DataSet.FieldByName('STATE_MARITAL_STATUS').Clear;

    if DM_CLIENT.CO_STATES.Locate('STATE', wwlcState.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        wwlcState.Clear;
        DM_CLIENT.EE_STATES['CO_STATES_NBR'] := Null;
      end;
  end;

end;

procedure TEE_QUICK_ENTRY.edtStateMaritalChange(Sender: TObject);
begin
  inherited;
  FMartitalStatusNeedsChanging := False;
end;

procedure TEE_QUICK_ENTRY.GetRateAndDBDT(DBDT: TDBDT; RateNumber: integer; var Rate,
  DBDTText: string);
begin
  Rate := '';
  DBDTText := '';
  ctx_DataAccess.TempCommitDisable := True;
  try
    if DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER',
      VarArrayOf([wwdsEmployee.DataSet.FieldByName('EE_NBR').Value, RateNumber]), []) then
    begin
      Rate := wwdsEERates.DataSet.FieldByName('RATE_AMOUNT').AsString;
      if DBDT.SelectionListFiltr.wwcsTempDBDT.Locate('DIVISION_NBR;BRANCH_NBR;DEPARTMENT_NBR;TEAM_NBR',
        VarArrayOf([wwdsEERates.DataSet.FieldByName('CO_DIVISION_NBR').Value,
        wwdsEERates.DataSet.FieldByName('CO_BRANCH_NBR').Value,
          wwdsEERates.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value,
          wwdsEERates.DataSet.FieldByName('CO_TEAM_NBR').Value]), []) then
        DBDTText := DBDT.SelectionListFiltr.wwcsTempDBDTDBDT_CUSTOM_NUMBERS.AsString;
    end;
  finally
    ctx_DataAccess.TempCommitDisable := False;
  end;
end;

procedure TEE_QUICK_ENTRY.btnAddScheduledEDsClick(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_EE_SCHEDULED_E_DS');
end;

procedure TEE_QUICK_ENTRY.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and (wwdsDetail.DataSet.State = dsInsert) then
  begin
    wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Assign(wwdsMaster.DataSet.FieldByName('HOME_CO_STATES_NBR'));
    wwdsDetail.DataSet.FieldByName('SDI_APPLY_CO_STATES_NBR').Assign(wwdsMaster.DataSet.FieldByName('HOME_SDI_CO_STATES_NBR'));
    wwdsDetail.DataSet.FieldByName('SUI_APPLY_CO_STATES_NBR').Assign(wwdsMaster.DataSet.FieldByName('HOME_SUI_CO_STATES_NBR'));
  end;
end;

procedure TEE_QUICK_ENTRY.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  if not (csLoading in ComponentState) then
    if DM_COMPANY.CO_STATES.Active and DM_SYSTEM_STATE.SY_STATES.Active then
      DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR',
        wwdsDetail.DataSet.FieldByName('CO_STATES_NBR').Value, 'STATE'), []);

end;

procedure TEE_QUICK_ENTRY.AfterDataSetsReopen;
begin
  inherited;

end;

procedure TEE_QUICK_ENTRY.edtStateMaritalBeforeDropDown(Sender: TObject);
var
  V: Variant;
begin
  inherited;
  if cbResidentalState.Text <> wwlcState.Value then
  begin
    V := DM_SYSTEM_STATE.SY_STATES.Lookup('state', wwlcState.Value, 'SY_STATES_NBR');
    if not VarIsNull(V) then
    begin
      DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.RetrieveCondition := 'SY_STATES_NBR=' + QuotedStr(V);
      ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS]);
    end;
  end;

  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFilter := 'ACTIVE_STATUS = ''Y'' ';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFiltered := true;  
end;

procedure TEE_QUICK_ENTRY.edtSDICloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (wwdsDetail.DataSet.State in [dsInsert]) and (edtSDI.Value <> '') then
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', edtSDI.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        edtSDI.Clear;
        DM_CLIENT.EE_STATES['SDI_APPLY_CO_STATES_NBR'] := Null;
      end;
  end;
end;

procedure TEE_QUICK_ENTRY.edtSUICloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (wwdsDetail.DataSet.State in [dsInsert]) and (edtSUI.Value <> '') then
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', edtSUI.Value, []) then
      if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
      begin
        EvMessage('This state is inactive.  You can not assign it to an Employee.', mtError, [mbOK]);
        edtSUI.Clear;
        DM_CLIENT.EE_STATES['SUI_APPLY_CO_STATES_NBR'] := Null;
      end;
  end;
end;

procedure TEE_QUICK_ENTRY.ToggleEnabledColor;
var
  aColor : TColor;
begin
  //GUI 2.0, ADDED TO GUARANTEE THE PROPER COLOR OF DISABLED FIELDS.
  //WITHOUT IT, DATE COMBOS AND SOME NUMERIC FIELDS LOOK WHITE, INSTEAD OF GRAY
  if FInserting then aColor := clWindow else aColor := clBtnFace;
  //I am NOT a fan of individually editing all fields one by one. It would
  //be better to be in a loop, but alas, copy and paste from above code.

  EdtSSN.Color := aColor;
  edtEECode.Color := aColor;
  edtFirstName.Color := aColor;
  dtLastName.Color := aColor;
  edtAddress1.Color := aColor;
  edtAddress2.Color := aColor;
  edtCity.Color := aColor;
  cbResidentalState.Color := aColor;
  edtZIP.Color := aColor;
  edtPhone.Color := aColor;
  edtGender.Color := aColor;
  edtTimeClockNumber.Color := aColor;
  edtRateDBDT1.Color := aColor;
  edtFiltrDBDT1.Color := aColor;
  btnSelectDBDT1.Color := aColor;
  edtRateDBDT2.Color := aColor;
  edtFiltrDBDT2.Color := aColor;
  btnSelectDBDT2.Color := aColor;
  edtRateDBDT3.Color := aColor;
  edtFiltrDBDT3.Color := aColor;
  btnSelectDBDT3.Color := aColor;
  cmbEthnicity.Color := aColor;
  cmbFedMarital.Color := aColor;
  edtFedDependents.Color := aColor;
  edtStateMarital.Color := aColor;
  cmbHealthCare.Color := aColor;
  edtStateDependents.Color := aColor;
  wwlcState.Color := aColor;
  edtSDI.Color := aColor;
  edtSUI.Color := aColor;
  edtDateOfBirth.Color := aColor;
  edtHireDate.Color := aColor;
  edtTerminationDate.Color := aColor;
  cmbStatusCode.Color := aColor;
  edtState.Color := aColor;
  edtSalaryAmount.Color := aColor;
  cmbPayFrequency.Color := aColor;
  edtWCCode.Color := aColor;
  btnOKNext.Color := aColor;
end;

procedure TEE_QUICK_ENTRY.wwdbgridSelectClientDblClick(Sender: TObject);
begin
  inherited;
  //GUI 2.0, TO SOLVE GRAY VS. WHITE FOR DISABLED FIELDS.
  ToggleEnabledColor;
end;

procedure TEE_QUICK_ENTRY.edtStateMaritalCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFilter := '';
  DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.UserFiltered := false;
end;

initialization
  Classes.RegisterClass(TEE_QUICK_ENTRY);
end.

