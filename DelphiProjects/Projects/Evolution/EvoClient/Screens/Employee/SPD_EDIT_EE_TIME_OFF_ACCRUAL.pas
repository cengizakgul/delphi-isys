// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_TIME_OFF_ACCRUAL;

interface

uses
   SDataStructure, wwdbedit, SPD_EDIT_EE_BASE, StdCtrls,
  wwdbdatetimepicker, wwdblook, Mask, ExtCtrls, DBActns, Classes, ActnList,
  Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, DBCtrls,
  Controls, SysUtils,  SPackageEntry, Dialogs, Variants,
  SDDClasses, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, IsBasicUtils, EvExceptions,
  SReportSettings, EvContext, EvTypes, isVCLBugFix, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIwwDBDateTimePicker, isUIwwDBEdit, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_TIME_OFF_ACCRUAL = class(TEDIT_EE_BASE)
    tshtTime_Off_Accural: TTabSheet;
    cdBalances: TevClientDataSet;
    dsBalances: TevDataSource;
    tsOpers: TTabSheet;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    edOperHistStartDate: TevDateTimePicker;
    dbgrOpers: TevDBGrid;
    evPanel2: TevPanel;
    evGroupBox1: TevGroupBox;
    evSplitter2: TevSplitter;
    evGroupBox2: TevGroupBox;
    evGroupBox3: TevGroupBox;
    evSplitter1: TevSplitter;
    cdOpers: TevClientDataSet;
    dsOpers: TevDataSource;
    cdOpersEE_TIME_OFF_ACCRUAL_OPER_NBR: TIntegerField;
    cdOpersEE_TIME_OFF_ACCRUAL_NBR: TIntegerField;
    cdOpersACCRUAL_DATE: TDateField;
    cdOpersACCRUED: TFloatField;
    cdOpersACCRUED_CAPPED: TFloatField;
    cdOpersUSED: TFloatField;
    cdOpersNOTE: TStringField;
    cdOpersOPERATION_CODE: TStringField;
    cdOpersADJUSTED_EE_TOA_OPER_NBR: TIntegerField;
    cdOpersCONNECTED_EE_TOA_OPER_NBR: TIntegerField;
    cdOpersPR_NBR: TIntegerField;
    cdOpersPR_CHECK_NBR: TIntegerField;
    cdOpersEFFECTIVE_DATE: TDateTimeField;
    cdOpersTO_NAME: TStringField;
    cdOpersPR_CHECK_DATE: TDateField;
    cdOpersPR_PAYMENT_SERIAL_NUMBER: TIntegerField;
    evLabel2: TevLabel;
    edOperHistEndDate: TevDateTimePicker;
    bRefreshOpers: TevBitBtn;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    cdOpersRUN_NUMBER: TIntegerField;
    cdOperAdj: TevClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateField1: TDateField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    DateTimeField1: TDateTimeField;
    StringField4: TStringField;
    DateTimeField3: TDateField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    dsOperAdj: TevDataSource;
    cdOperConn: TevClientDataSet;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    DateField2: TDateField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField12: TIntegerField;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    IntegerField15: TIntegerField;
    DateTimeField4: TDateTimeField;
    StringField8: TStringField;
    DateTimeField6: TDateField;
    IntegerField17: TIntegerField;
    IntegerField18: TIntegerField;
    dsOperConn: TevDataSource;
    dbgrOperAdj: TevDBGrid;
    dbgrOperConn: TevDBGrid;
    evLabel5: TevLabel;
    cbActiveOnly: TevCheckBox;
    pnlPrintBtns: TevPanel;
    bPreviewCheck: TevBitBtn;
    bPrintCheck: TevBitBtn;
    fpTOASummary: TisUIFashionPanel;
    fpTOADetail: TisUIFashionPanel;
    fpTOAOverrides: TisUIFashionPanel;
    lablEffective_Accrual_Date: TevLabel;
    lablAnnual_Accrual_Maximum: TevLabel;
    lablOverride_Rate: TevLabel;
    lablRollover_Type: TevLabel;
    lablRollover_Date: TevLabel;
    dedtAnnual_Accrual_Maximum: TevDBEdit;
    dedtOverride_Rate: TevDBEdit;
    wwlcRollover_Type: TevDBLookupCombo;
    edOverrideHireDate: TevDBDateTimePicker;
    edOverrideRolloverDate: TevDBDateTimePicker;
    bShowOpers: TevBitBtn;
    sbAccruals: TScrollBox;
    wwdgTime_Off_Accural: TevDBGrid;
    lablType: TevLabel;
    lablCurrent_Accrued: TevLabel;
    lablCurrent_Used: TevLabel;
    Label1: TevLabel;
    lReason: TevLabel;
    dedtCurrent_Accrued: TevDBEdit;
    dedtCurrent_Used: TevDBEdit;
    wwlcType: TevDBLookupCombo;
    edBalance: TevDBEdit;
    edActive: TevDBRadioGroup;
    edReason: TevEdit;
    evActive: TevCheckBox;
    lablFuture_Accrue_Date: TevLabel;
    lablFuture_Reset_Date: TevLabel;
    edFutureAccrueDate: TevDBDateTimePicker;
    edFutureResetDate: TevDBDateTimePicker;
    sbTimeOffAccrual: TScrollBox;
    fpSummary: TisUIFashionPanel;
    fpTransactions: TisUIFashionPanel;
    pnlFPTransactions: TevPanel;
    procedure bShowOpersClick(Sender: TObject);
    procedure bRefreshOpersClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure dbgrOpersDblClick(Sender: TObject);
    procedure dsOpersDataChange(Sender: TObject; Field: TField);
    procedure wwdgTime_Off_AccuralDblClick(Sender: TObject);
    procedure ClickRefreshOnEnter(Sender: TObject; var Key: Char);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure cbActiveOnlyClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure evActiveClick(Sender: TObject);
    procedure bPrintCheckClick(Sender: TObject);
    procedure bPreviewCheckClick(Sender: TObject);
    procedure fpTransactionsResize(Sender: TObject);
  private
    FDoNotUpdateFlag: boolean;
    procedure SetDoNotUpdateFlag(const Value: boolean);
  protected
    property DoNotUpdateFlag : boolean read FDoNotUpdateFlag write SetDoNotUpdateFlag;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure SetDataSetsProps(aDS: TArrayDS; ListDataSet: TevClientDataSet);override;
    procedure AfterDataSetsReopen; override;
    procedure SetReadOnly(Value: Boolean); override;
    procedure PrintResult(aPrint: boolean);
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;

implementation

{$R *.DFM}

uses EvUtils, SFrameEntry, EvBasicUtils, SPD_EDIT_Open_BASE, Math, EvConsts;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.Activate;
begin
  cdBalances.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
  edOperHistStartDate.Date := GetBeginQuarter(Date);
  edOperHistEndDate.Date := Date+ 7;
  tsOpers.TabVisible := False;
  inherited;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.SetReadOnly(Value: Boolean);
begin
  inherited;
  bShowOpers.SecurityRO := false;
  bRefreshOpers.SecurityRO := false;
  edOperHistStartDate.SecurityRO := false;
  edOperHistEndDate.SecurityRO := false;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.AfterDataSetsReopen;
var
  d: TEvClientDataSet;
  f: TField;
  i, n: Integer;
  s: string;
begin
  inherited;
  edReason.Clear;
  with DM_COMPANY.CO_TIME_OFF_ACCRUAL do
  begin
    n := -1;
    i := RecNo;
    First;
    while not Eof do
    begin
      n := Max(n, FieldByName('DIGITS_AFTER_DECIMAL').AsInteger);
      Next;
    end;
    RecNo := i;
  end;
  if n >= 2 then
    s := '####0.00'+ StringOfChar('#', n-2)
  else
    s := '####0.'+ StringOfChar('#', n);
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('ACCRUED')).DisplayFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('USED')).DisplayFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('BALANCE')).DisplayFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('ACCRUED')).EditFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('USED')).EditFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('ANNUAL_ACCRUAL_MAXIMUM')).EditFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('OVERRIDE_RATE')).EditFormat := s;
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.AssignDisplayFormatPictureMasks;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.FieldByName('ACCRUED')).DisplayFormat := s;
  TFloatField(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.FieldByName('USED')).DisplayFormat := s;
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.AssignDisplayFormatPictureMasks;
  TFloatField(cdOpers.FieldByName('ACCRUED')).DisplayFormat := s;
  TFloatField(cdOpers.FieldByName('ACCRUED_CAPPED')).DisplayFormat := s;
  TFloatField(cdOpers.FieldByName('USED')).DisplayFormat := s;
  TFloatField(cdOperAdj.FieldByName('ACCRUED')).DisplayFormat := s;
  TFloatField(cdOperAdj.FieldByName('ACCRUED_CAPPED')).DisplayFormat := s;
  TFloatField(cdOperAdj.FieldByName('USED')).DisplayFormat := s;
  TFloatField(cdOperConn.FieldByName('ACCRUED')).DisplayFormat := s;
  TFloatField(cdOperConn.FieldByName('ACCRUED_CAPPED')).DisplayFormat := s;
  TFloatField(cdOperConn.FieldByName('USED')).DisplayFormat := s;

  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
  begin
    d := TEvClientDataSet.Create(nil);
    try
      d.CloneCursor(TevClientDataSet(wwdsDetail.DataSet), True, True);
      d.IndexFieldNames := 'ee_time_off_accrual_nbr';
      TFloatField(d.FieldByName('ACCRUED')).DisplayFormat := s;
      TFloatField(d.FieldByName('USED')).DisplayFormat := s;
      TFloatField(d.FieldByName('BALANCE')).DisplayFormat := s;
      TFloatField(d.FieldByName('ANNUAL_ACCRUAL_MAXIMUM')).DisplayFormat := s;
      TFloatField(d.FieldByName('OVERRIDE_RATE')).DisplayFormat := s;
      //d.AssignDisplayFormatPictureMasks;
      d.PictureMasks.Clear;
      f := d.FieldByName('ee_time_off_accrual_nbr');
      d.First;
      while not d.Eof do
      begin
        if cdBalances.FindKey([f.AsInteger]) then
        begin
          d.Edit;
          d['ACCRUED'] := cdBalances['ACCRUED'];
          d['USED'] := cdBalances['USED'];
          d['BALANCE'] := d['ACCRUED']- d['USED'];
          d.Post;
        end;
        d.Next;
      end;
      d.MergeChangeLog;
    finally
      d.Free;
    end;
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  with TExecDSWrapper.Create('TimeOffBalances') do
       cdBalances.DataRequest(AsVariant);
  AddDS([DM_COMPANY.CO_TIME_OFF_ACCRUAL, cdBalances, DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER], aDS);
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.RetrieveCondition := AlwaysFalseCond;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.SetDataSetsProps(aDS: TArrayDS; ListDataSet: TevClientDataSet);
Var
  i: Integer;
begin
  for i := Low(aDS) to High(aDS) do
    if not aDS[i].Active then
    begin
      aDS[i].ClientID := ListDataSet.FieldByName('CL_NBR').AsInteger;
      aDS[i].AsOfDate := ctx_DataAccess.AsOfDate;

      if aDS[i].TableName = 'EE_TIME_OFF_ACCRUAL_OPER' then
       aDS[i].RetrieveCondition := AlwaysFalseCond
      else
       aDS[i].RetrieveCondition := GetDataSetConditions(aDS[i].TableName);
    end;
end;

function TEDIT_EE_TIME_OFF_ACCRUAL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL;
end;

function TEDIT_EE_TIME_OFF_ACCRUAL.GetInsertControl: TWinControl;
begin
  Result := wwlcType;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, EditDataSets);
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.RetrieveCondition := AlwaysFalseCond;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.bShowOpersClick(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.RecordCount > 0 then
  begin
    evLabel5.Caption := ConvertNull(wwdsDetail.DataSet['DESCRIPTION'], '');
    bRefreshOpers.Click;
    tsOpers.TabVisible := True;
    PageControl1.ActivatePage(tsOpers);
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.bRefreshOpersClick(Sender: TObject);
begin
  inherited;
  with TExecDSWrapper.Create('TimeOffOperations') do
  begin
    SetMacro('COND', 'o.ee_time_off_accrual_nbr= :Nbr and o.accrual_date between :BegDate and :EndDate');
    SetParam('Nbr', Integer(wwdsDetail.DataSet['ee_time_off_accrual_nbr']));
    SetParam('BegDate', Trunc(edOperHistStartDate.DateTime));
    SetParam('EndDate', Trunc(edOperHistEndDate.DateTime));
    ctx_DataAccess.GetCustomData(cdOpers, AsVariant);
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage <> tsOpers then
    tsOpers.TabVisible := False;

  if PageControl1.ActivePage = tshtTime_Off_Accural then
    cbActiveOnlyClick(Sender);

  if not PageControl1.ActivePageIndex = 1 then exit;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.dbgrOpersDblClick(Sender: TObject);
begin
  inherited;
  with TExecDSWrapper.Create('TimeOffOperations') do
  begin
    SetMacro('COND', 'o.ADJUSTED_EE_TOA_OPER_NBR= :Nbr');
    SetParam('Nbr', cdOpersEE_TIME_OFF_ACCRUAL_OPER_NBR.AsInteger);
    ctx_DataAccess.GetCustomData(cdOperAdj, AsVariant);
  end;
  with TExecDSWrapper.Create('TimeOffOperations') do
  begin
    SetMacro('COND', 'o.EE_TIME_OFF_ACCRUAL_OPER_NBR= :Nbr');
    SetParam('Nbr', cdOpersCONNECTED_EE_TOA_OPER_NBR.AsInteger);
    ctx_DataAccess.GetCustomData(cdOperConn, AsVariant);
  end;
  dbgrOperAdj.Visible := True;
  dbgrOperConn.Visible := True;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.dsOpersDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  cdOperAdj.Close;
  cdOperConn.Close;
  dbgrOperAdj.Visible := False;
  dbgrOperConn.Visible := False;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.wwdgTime_Off_AccuralDblClick(
  Sender: TObject);
begin
  inherited;
  bShowOpers.Click;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.ClickRefreshOnEnter(
  Sender: TObject; var Key: Char);
begin
  inherited;
  case Key of
  #13:
    bRefreshOpers.Click;
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  //rearranged to now have these always visible, but enabled/disabled

  edReason.Enabled := wwdsDetail.State in [dsEdit, dsInsert];


  if wwdsDetail.State = dsInsert then
  begin
    DoNotUpdateFlag := true;
    try
      wwdsDetail.DataSet['STATUS'] := 'Y';
      evActive.Checked := true;
    finally
      DoNotUpdateFlag := false;
    end;
  end;

  if wwdsDetail.State in [dsEdit, dsInsert] then
    evActive.Enabled := false
  else
    evActive.Enabled := true;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  wwdsDetail.DataSet['REASON'] := edReason.Text;
  wwdsDetail.DataSet['BALANCE'] := wwdsDetail.DataSet['ACCRUED']- wwdsDetail.DataSet['USED'];
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.cbActiveOnlyClick(Sender: TObject);
begin
  inherited;
  if cbActiveOnly.Checked then begin
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filtered := false;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filter := 'STATUS='+ QuotedStr(GROUP_BOX_YES);
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filtered := true;
  end else begin
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filtered := false;
    DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filter := '';
  end;
//  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filtered := DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Filter <> '';
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  s : String;
  cdEETOA: TevClientDataSet;
begin
  inherited;
  if (Kind in [NavOK,NavCommit]) then
  begin
    s := 'select a.EE_TIME_OFF_ACCRUAL_NBR, a.EE_NBR, a.CO_TIME_OFF_ACCRUAL_NBR ' +
         'from EE_TIME_OFF_ACCRUAL a where {AsOfNow<a>} and a.EE_NBR = :EE_NBR ' +
         'order by a.EE_NBR, a.CO_TIME_OFF_ACCRUAL_NBR ';
    cdEETOA := TEvClientDataSet.Create(nil);
    try
        with TExecDSWrapper.Create(s) do
        begin
          SetParam('EE_NBR', DM_CLIENT.EE.FieldByName('EE_NBR').AsInteger);
          cdEETOA.Close;
          ctx_DataAccess.GetCustomData(cdEETOA, AsVariant);
        end;
        if wwdsDetail.DataSet.State = dsInsert  then
        begin
          if cdEETOA.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_CLIENT.EE.FieldByName('EE_NBR').Value, wwdsDetail.DataSet.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger]), []) then
            raise EUpdateError.CreateHelp('You can not have more than one of the same accrual type attached to an employee!', IDH_ConsistencyViolation);
        end;
        if wwdsDetail.DataSet.State = dsEdit  then
        begin
          if cdEETOA.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_CLIENT.EE.FieldByName('EE_NBR').Value, wwdsDetail.DataSet.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger]), []) then
           if cdEETOA.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger <> wwdsDetail.DataSet.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger then
              raise EUpdateError.CreateHelp('You can not have more than one of the same accrual type attached to an employee!', IDH_ConsistencyViolation);
        end;
    Finally
     cdEETOA.free;
    end;
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavCommit then
    TevClientDataSet(wwdsDetail.DataSet).MergeChangeLog;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if Kind = navOk then
    wwdgTime_Off_Accural.Enabled := not Value;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  DoNotUpdateFlag := true;
  try
    if wwdsDetail.DataSet['STATUS'] <> null then
      if (wwdsDetail.DataSet['STATUS'] = 'Y') then
        evActive.Checked := true
      else
        evActive.Checked := false
    else
      evActive.Checked := false;
  finally
    DoNotUpdateFlag := false;
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.SetDoNotUpdateFlag(
  const Value: boolean);
begin
  FDoNotUpdateFlag := Value;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.evActiveClick(Sender: TObject);
var
  dsClone: TEvClientDataSet;
begin
  if not DoNotUpdateFlag then
  begin
    if not evActive.Checked then
      if EvMessage('Make ''' + ConvertNull(DM_CLIENT.EE_TIME_OFF_ACCRUAL['Description'],'Error: Description not found') +''' record inactive.  Are you sure?', mtConfirmation, [mbYes, mbNo]) <> mrYes then begin
         evActive.Checked := true;
         AbortEx;
    end;
    inherited;
    dsClone := TEvClientDataSet.Create( nil );
    DM_CLIENT.EE_TIME_OFF_ACCRUAL.DisableControls;
    try
      dsClone.CloneCursor(DM_CLIENT.EE_TIME_OFF_ACCRUAL, true, false);
      CheckCondition(dsClone.Locate('EE_TIME_OFF_ACCRUAL_NBR',DM_CLIENT.EE_TIME_OFF_ACCRUAL['EE_TIME_OFF_ACCRUAL_NBR'],[]), 'Record not found in clone of DM_CLIENT.EE_TIME_OFF_ACCRUAL!');
      dsClone.Edit;
      if evActive.Checked then
        dsClone['STATUS'] := 'Y'
      else
        dsClone['STATUS'] := 'N';
      dsClone.Post;
      dsClone.Close;
      ctx_DataAccess.PostDataSets([DM_CLIENT.EE_TIME_OFF_ACCRUAL]);
    finally
      FreeAndNil(dsClone);
      DM_CLIENT.EE_TIME_OFF_ACCRUAL.EnableControls;
    end;
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.PrintResult(aPrint: boolean);
var
  ReportParams: TrwReportParams;
  ReportResult: TrwReportResults;
begin
  ReportParams := TrwReportParams.Create;
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_DESCRIPTION=''Employee Time Off History''');
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Employee Time Off History', [loCaseInsensitive]) then
      raise EMissingReport.CreateHelp('"Employee Time Off History" report does not exist in the database!', IDH_MissingReport);

    ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('Companies', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger]));
    ReportParams.Add('BeginDate', edOperHistStartDate.Date);
    ReportParams.Add('EndDate', edOperHistEndDate.Date);
    ReportParams.Add('Nbr', DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.FieldByName('EE_TIME_OFF_ACCRUAL_NBR').AsInteger);

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
        CH_DATABASE_SYSTEM, ReportParams);
    finally
      ReportResult := TrwReportResults.Create(ctx_RWLocalEngine.EndGroup(rdtNone));
      if aPrint then
        ctx_RWLocalEngine.Print(ReportResult)
      else
        ctx_RWLocalEngine.Preview(ReportResult);
    end;
  finally
    ReportParams.Free;
  end;
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.bPrintCheckClick(Sender: TObject);
begin
  PrintResult(True);
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.bPreviewCheckClick(Sender: TObject);
begin
  PrintResult(False);
end;

procedure TEDIT_EE_TIME_OFF_ACCRUAL.fpTransactionsResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 ROUTINES
  pnlFPTransactions.Width  := fpTransactions.Width - 40;
  pnlFPTransactions.Height := fpTransactions.Height - 65;
end;

initialization
  RegisterClass(TEDIT_EE_TIME_OFF_ACCRUAL);

end.
