object SchedFrame: TSchedFrame
  Left = 0
  Top = 0
  Width = 785
  Height = 504
  TabOrder = 0
  object pFrame: TevPanel
    Left = 0
    Top = 0
    Width = 785
    Height = 464
    Align = alClient
    BevelOuter = bvLowered
    Caption = 'No parameters required'
    TabOrder = 0
  end
  object pButtons: TevPanel
    Left = 0
    Top = 464
    Width = 785
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object lPriority: TevLabel
      Left = 12
      Top = 15
      Width = 46
      Height = 13
      Caption = '~Priority'
      FocusControl = sePriority
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object eEMail: TevEdit
      Left = 243
      Top = 11
      Width = 169
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object cbEMail: TevCheckBox
      Left = 134
      Top = 14
      Width = 104
      Height = 17
      Alignment = taLeftJustify
      Caption = 'E-Mail Notification'
      TabOrder = 0
      OnClick = cbEMailClick
    end
    object cbEmailSendRule: TevComboBox
      Left = 425
      Top = 11
      Width = 187
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        'Always'
        'Successful'
        'Exceptions'
        'Exceptions/Warnings')
    end
    object sePriority: TevEdit
      Left = 56
      Top = 11
      Width = 64
      Height = 21
      TabOrder = 3
    end
  end
end
