inherited Scheduler: TScheduler
  Width = 847
  Height = 661
  object pcMain: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 847
    Height = 661
    ActivePage = tsParams
    Align = alClient
    TabOrder = 0
    OnChange = pcMainChange
    object tsList: TTabSheet
      Caption = 'Scheduled Tasks'
      object Splitter: TevSplitter
        Left = 0
        Top = 520
        Width = 839
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object gTasks: TevDBGrid
        Left = 0
        Top = 0
        Width = 839
        Height = 520
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'30'#9'Task Name'#9'F'
          'TaskCaption'#9'100'#9'Task Caption'#9'F'
          'USER_NAME'#9'30'#9'User'#9'F'
          'SCHED_DESC'#9'512'#9'Schedule'#9'F'
          'NOTES'#9'255'#9'Notes'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TScheduler\gTasks'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = gTasksRowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object gRuns: TevDBGrid
        Left = 0
        Top = 523
        Width = 839
        Height = 110
        DisableThemesInTitle = False
        Selected.Strings = (
          'run_date'#9'18'#9'Runs'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TScheduler\gRuns'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alBottom
        DataSource = wwdsDetail
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tsParams: TTabSheet
      Caption = 'Parameters'
      ImageIndex = 1
      object pTask: TevPanel
        Left = 0
        Top = 0
        Width = 839
        Height = 111
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lTask: TevLabel
          Left = 9
          Top = 27
          Width = 36
          Height = 16
          Caption = '~Task:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lCaption: TevLabel
          Left = 9
          Top = 81
          Width = 39
          Height = 13
          Caption = 'Caption:'
        end
        object lCapt: TevLabel
          Left = 54
          Top = 81
          Width = 604
          Height = 13
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evLabel1: TevLabel
          Left = 9
          Top = 55
          Width = 34
          Height = 13
          Caption = 'Notes: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cbTask: TevDBComboBox
          Left = 54
          Top = 24
          Width = 370
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'DESCRIPTION'
          DataSource = wwdsMaster
          DropDownCount = 9
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
          OnChange = cbTaskChange
        end
        object edtNotes: TevDBEdit
          Left = 54
          Top = 51
          Width = 627
          Height = 21
          DataField = 'NOTES'
          DataSource = wwdsMaster
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      inline SchedFrame: TSchedFrame
        Left = 0
        Top = 111
        Width = 839
        Height = 522
        Align = alClient
        TabOrder = 1
        inherited pFrame: TevPanel
          Width = 839
          Height = 482
        end
        inherited pButtons: TevPanel
          Top = 482
          Width = 839
          inherited sePriority: TevEdit
            Enabled = False
          end
        end
      end
    end
    object tsSchedule: TTabSheet
      Caption = 'Schedule'
      ImageIndex = 2
      object Bevel1: TBevel
        Left = 0
        Top = 380
        Width = 9
        Height = 244
        Align = alLeft
        Shape = bsSpacer
      end
      object Bevel2: TBevel
        Left = 830
        Top = 380
        Width = 9
        Height = 244
        Align = alRight
        Shape = bsSpacer
      end
      object Bevel3: TBevel
        Left = 0
        Top = 624
        Width = 839
        Height = 9
        Align = alBottom
        Shape = bsSpacer
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 839
        Height = 380
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lSchedDesc: TevDBText
          Left = 9
          Top = 27
          Width = 667
          Height = 37
          DataField = 'SCHED_DESC'
          DataSource = wwdsMaster
          WordWrap = True
        end
        object EvBevel1: TEvBevel
          Left = 9
          Top = 72
          Width = 667
          Height = 2
        end
        object lScheduleTask: TevLabel
          Left = 9
          Top = 84
          Width = 75
          Height = 13
          Caption = 'Schedule Task:'
          FocusControl = cbScheduleTask
        end
        object lStartTime: TevLabel
          Left = 156
          Top = 84
          Width = 51
          Height = 13
          Caption = 'Start Time:'
          FocusControl = dtStartTime
        end
        object lStartDate: TevLabel
          Left = 303
          Top = 84
          Width = 51
          Height = 13
          Caption = 'Start Date:'
          FocusControl = dtStartDate
        end
        object lLastRun: TLabel
          Left = 303
          Top = 337
          Width = 46
          Height = 13
          Caption = 'Last Run:'
        end
        object Label1: TLabel
          Left = 9
          Top = 364
          Width = 86
          Height = 13
          Caption = 'Schedule Preview'
        end
        object gbScheduleWeekly: TevGroupBox
          Left = 9
          Top = 139
          Width = 268
          Height = 150
          Caption = 'Schedule Task Weekly'
          TabOrder = 0
          Visible = False
          object lWeeklyWeeks: TevLabel
            Left = 123
            Top = 24
            Width = 55
            Height = 13
            Caption = 'week(s) on:'
          end
          object lWeeklyEvery: TevLabel
            Left = 15
            Top = 24
            Width = 27
            Height = 13
            Caption = 'Every'
          end
          object seWeekly: TevSpinEdit
            Left = 60
            Top = 21
            Width = 52
            Height = 22
            MaxValue = 9999
            MinValue = 1
            TabOrder = 0
            Value = 1
            OnChange = seWeeklyChange
          end
          object cbMon: TevCheckBox
            Left = 111
            Top = 54
            Width = 49
            Height = 17
            Caption = 'Mon'
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = cbJanClick
          end
          object cbTue: TevCheckBox
            Left = 111
            Top = 75
            Width = 49
            Height = 17
            Caption = 'Tue'
            TabOrder = 2
            OnClick = cbJanClick
          end
          object cbWed: TevCheckBox
            Left = 111
            Top = 96
            Width = 49
            Height = 17
            Caption = 'Wed'
            TabOrder = 3
            OnClick = cbJanClick
          end
          object cbThu: TevCheckBox
            Left = 111
            Top = 117
            Width = 49
            Height = 17
            Caption = 'Thu'
            TabOrder = 4
            OnClick = cbJanClick
          end
          object cbFri: TevCheckBox
            Left = 180
            Top = 54
            Width = 49
            Height = 17
            Caption = 'Fri'
            TabOrder = 5
            OnClick = cbJanClick
          end
          object cbSat: TevCheckBox
            Left = 180
            Top = 75
            Width = 49
            Height = 17
            Caption = 'Sat'
            TabOrder = 6
            OnClick = cbJanClick
          end
          object cbSun: TevCheckBox
            Left = 180
            Top = 96
            Width = 49
            Height = 17
            Caption = 'Sun'
            TabOrder = 7
            OnClick = cbJanClick
          end
        end
        object gbScheduleDaily: TevGroupBox
          Left = 9
          Top = 139
          Width = 268
          Height = 57
          Caption = 'Schedule Task Daily'
          TabOrder = 1
          object lDailyEvery: TevLabel
            Left = 15
            Top = 24
            Width = 27
            Height = 13
            Caption = 'Every'
          end
          object lDailyDays: TevLabel
            Left = 123
            Top = 24
            Width = 28
            Height = 13
            Caption = 'day(s)'
          end
          object seDaily: TevSpinEdit
            Left = 60
            Top = 21
            Width = 52
            Height = 22
            MaxValue = 9999
            MinValue = 1
            TabOrder = 0
            Value = 1
            OnChange = seWeeklyChange
          end
        end
        object cbScheduleTask: TevComboBox
          Left = 9
          Top = 102
          Width = 121
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          OnChange = cbScheduleTaskClick
          Items.Strings = (
            'Daily'
            'Weekly'
            'Monthly'
            'Once'
            'At System Startup')
        end
        object dtStartTime: TevDateTimePicker
          Left = 156
          Top = 102
          Width = 121
          Height = 21
          Date = 37435.000000000000000000
          Format = 'h:mm tt'
          Time = 37435.000000000000000000
          DateMode = dmUpDown
          Kind = dtkTime
          TabOrder = 3
          OnChange = dtStartTimeChange
        end
        object cbEndDate: TevCheckBox
          Left = 450
          Top = 84
          Width = 97
          Height = 17
          Caption = 'End Date:'
          TabOrder = 4
          OnClick = cbEndDateClick
        end
        object dtStartDate: TevDateTimePicker
          Left = 303
          Top = 102
          Width = 121
          Height = 21
          Date = 37435.000000000000000000
          Time = 37435.000000000000000000
          TabOrder = 5
          OnChange = dtStartTimeChange
        end
        object dtEndDate: TevDateTimePicker
          Left = 450
          Top = 102
          Width = 121
          Height = 21
          Date = 37435.000000000000000000
          Time = 37435.000000000000000000
          Enabled = False
          TabOrder = 6
          OnChange = dtStartTimeChange
        end
        object pRepeatTask: TevPanel
          Left = 303
          Top = 144
          Width = 268
          Height = 145
          BevelInner = bvRaised
          BevelOuter = bvLowered
          TabOrder = 7
          object lEvery: TevLabel
            Left = 12
            Top = 18
            Width = 30
            Height = 13
            Caption = 'Every:'
            Enabled = False
          end
          object lUntil: TevLabel
            Left = 12
            Top = 48
            Width = 24
            Height = 13
            Caption = 'Until:'
            Enabled = False
          end
          object lHours: TevLabel
            Left = 195
            Top = 78
            Width = 32
            Height = 13
            Caption = 'hour(s)'
            Enabled = False
          end
          object lMinutes: TevLabel
            Left = 195
            Top = 108
            Width = 42
            Height = 13
            Caption = 'minute(s)'
            Enabled = False
          end
          object seRepeatInterval: TevSpinEdit
            Left = 51
            Top = 15
            Width = 55
            Height = 22
            Enabled = False
            MaxValue = 9999
            MinValue = 1
            TabOrder = 0
            Value = 1
            OnChange = seWeeklyChange
          end
          object cbRepeatUnits: TevComboBox
            Left = 123
            Top = 15
            Width = 100
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            Enabled = False
            ItemHeight = 13
            TabOrder = 1
            OnChange = dtStartTimeChange
            Items.Strings = (
              'minutes'
              'hours')
          end
          object rbTime: TevRadioButton
            Left = 51
            Top = 48
            Width = 49
            Height = 17
            Caption = 'Time:'
            Enabled = False
            TabOrder = 2
            OnClick = rbTimeClick
          end
          object dtRepeatEndTime: TevDateTimePicker
            Left = 123
            Top = 45
            Width = 100
            Height = 21
            Date = 37435.000000000000000000
            Format = 'h:mm tt'
            Time = 37435.000000000000000000
            DateMode = dmUpDown
            Enabled = False
            Kind = dtkTime
            TabOrder = 4
            OnChange = dtRepeatEndTimeChange
          end
          object rbDuration: TevRadioButton
            Left = 51
            Top = 78
            Width = 61
            Height = 17
            Caption = 'Duration:'
            Checked = True
            Enabled = False
            TabOrder = 3
            TabStop = True
            OnClick = rbTimeClick
          end
          object seHours: TevSpinEdit
            Left = 123
            Top = 75
            Width = 64
            Height = 22
            Enabled = False
            MaxValue = 23
            MinValue = 0
            TabOrder = 5
            Value = 0
            OnChange = seHoursChange
          end
          object seMinutes: TevSpinEdit
            Left = 123
            Top = 105
            Width = 64
            Height = 22
            Enabled = False
            MaxValue = 59
            MinValue = 0
            TabOrder = 6
            Value = 0
            OnChange = seHoursChange
          end
        end
        object cbRepeatTask: TevCheckBox
          Left = 315
          Top = 136
          Width = 82
          Height = 17
          Caption = 'Repeat Task'
          TabOrder = 8
          OnClick = cbRepeatTaskClick
        end
        object cbRunIfMissed: TevCheckBox
          Left = 303
          Top = 306
          Width = 217
          Height = 17
          Caption = 'Run on next startup if missed'
          TabOrder = 9
          OnClick = cbJanClick
        end
        object gbScheduleMonthly: TevGroupBox
          Left = 9
          Top = 139
          Width = 268
          Height = 201
          Caption = 'Schedule Task Monthly'
          TabOrder = 10
          Visible = False
          object lMonthlyMonths1: TevLabel
            Left = 123
            Top = 24
            Width = 70
            Height = 13
            Caption = 'of the month(s)'
            Enabled = False
          end
          object lMonthlyMonths2: TevLabel
            Left = 171
            Top = 84
            Width = 70
            Height = 13
            Caption = 'of the month(s)'
          end
          object rbMonthlyDay: TevRadioButton
            Left = 15
            Top = 24
            Width = 43
            Height = 17
            Caption = 'Day'
            TabOrder = 0
            OnClick = rbMonthlyDayClick
          end
          object rbMonthlyThe: TevRadioButton
            Left = 15
            Top = 53
            Width = 46
            Height = 17
            Caption = 'The'
            Checked = True
            TabOrder = 1
            TabStop = True
            OnClick = rbMonthlyDayClick
          end
          object seMonthly: TevComboBox
            Left = 60
            Top = 21
            Width = 52
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            Enabled = False
            ItemHeight = 13
            TabOrder = 2
            OnChange = seMonthlyChange
            Items.Strings = (
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9'
              '10'
              '11'
              '12'
              '13'
              '14'
              '15'
              '16'
              '17'
              '18'
              '19'
              '20'
              '21'
              '22'
              '23'
              '24'
              '25'
              '26'
              '27'
              '28'
              '29'
              '30'
              '31'
              'Last')
          end
          object cbMonthlyFirst: TevComboBox
            Left = 60
            Top = 51
            Width = 70
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 3
            OnChange = dtStartTimeChange
            Items.Strings = (
              'first'
              'second'
              'third'
              'fourth'
              'last')
          end
          object cbDayOfWeek: TevComboBox
            Left = 60
            Top = 81
            Width = 103
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 4
            OnChange = dtStartTimeChange
            Items.Strings = (
              'Monday'
              'Tuesday'
              'Wednesday'
              'Thursday'
              'Friday'
              'Saturday'
              'Sunday')
          end
          object cbApr: TevCheckBox
            Left = 15
            Top = 174
            Width = 67
            Height = 17
            Caption = 'April'
            Checked = True
            State = cbChecked
            TabOrder = 8
            OnClick = cbJanClick
          end
          object cbMar: TevCheckBox
            Left = 15
            Top = 153
            Width = 67
            Height = 17
            Caption = 'March'
            Checked = True
            State = cbChecked
            TabOrder = 7
            OnClick = cbJanClick
          end
          object cbFeb: TevCheckBox
            Left = 15
            Top = 132
            Width = 67
            Height = 17
            Caption = 'February'
            Checked = True
            State = cbChecked
            TabOrder = 6
            OnClick = cbJanClick
          end
          object cbJan: TevCheckBox
            Left = 15
            Top = 111
            Width = 67
            Height = 17
            Caption = 'January'
            Checked = True
            State = cbChecked
            TabOrder = 5
            OnClick = cbJanClick
          end
          object cbMay: TevCheckBox
            Left = 98
            Top = 111
            Width = 67
            Height = 17
            Caption = 'May'
            Checked = True
            State = cbChecked
            TabOrder = 9
            OnClick = cbJanClick
          end
          object cbJun: TevCheckBox
            Left = 98
            Top = 132
            Width = 67
            Height = 17
            Caption = 'June'
            Checked = True
            State = cbChecked
            TabOrder = 10
            OnClick = cbJanClick
          end
          object cbJul: TevCheckBox
            Left = 98
            Top = 153
            Width = 67
            Height = 17
            Caption = 'July'
            Checked = True
            State = cbChecked
            TabOrder = 11
            OnClick = cbJanClick
          end
          object cbAug: TevCheckBox
            Left = 98
            Top = 174
            Width = 67
            Height = 17
            Caption = 'August'
            Checked = True
            State = cbChecked
            TabOrder = 12
            OnClick = cbJanClick
          end
          object cbSep: TevCheckBox
            Left = 180
            Top = 111
            Width = 76
            Height = 17
            Caption = 'September'
            Checked = True
            State = cbChecked
            TabOrder = 13
            OnClick = cbJanClick
          end
          object cbOct: TevCheckBox
            Left = 180
            Top = 132
            Width = 67
            Height = 17
            Caption = 'October'
            Checked = True
            State = cbChecked
            TabOrder = 14
            OnClick = cbJanClick
          end
          object cbNov: TevCheckBox
            Left = 180
            Top = 153
            Width = 67
            Height = 17
            Caption = 'November'
            Checked = True
            State = cbChecked
            TabOrder = 15
            OnClick = cbJanClick
          end
          object cbDec: TevCheckBox
            Left = 180
            Top = 174
            Width = 79
            Height = 17
            Caption = 'December'
            Checked = True
            State = cbChecked
            TabOrder = 16
            OnClick = cbJanClick
          end
        end
      end
      object lbSchedule: TListBox
        Left = 9
        Top = 380
        Width = 821
        Height = 244
        Align = alClient
        Columns = 3
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_TASK.SB_TASK
    OnDataChange = wwdsMasterDataChange
  end
  inherited wwdsDetail: TevDataSource
    DataSet = cdsSB_TASK_RUNS
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 370
    Top = 45
  end
  object cdsSB_TASK_RUNS: TevClientDataSet
    ProviderName = 'SB_TASK_RUNS_PROV'
    Left = 328
    Top = 64
  end
end
