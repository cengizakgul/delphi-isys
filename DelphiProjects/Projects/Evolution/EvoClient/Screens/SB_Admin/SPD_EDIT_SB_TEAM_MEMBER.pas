// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_TEAM_MEMBER;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SB_TEAM_MEMBER = class(TFrameEntry)
    pctlSB_TEAM_MEMBER: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtTeam_Member_Detail: TTabSheet;
    wwdgTeam: TevDBGrid;
    wwdgTeamMembers: TevDBGrid;
    dtxtTEAM_DESCRIPTION: TevDBText;
    lablTeam: TevLabel;
    wwDBLookupLast1: TevDBLookupCombo;
    Label2: TevLabel;
    Bevel1: TEvBevel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evDBGrid1: TevDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_TEAM_MEMBER: TEDIT_SB_TEAM_MEMBER;

implementation

{$R *.DFM}

function TEDIT_SB_TEAM_MEMBER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_TEAM_MEMBERS;
end;

function TEDIT_SB_TEAM_MEMBER.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupLast1;
end;

procedure TEDIT_SB_TEAM_MEMBER.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SERVICE_BUREAU.SB_TEAM, SupportDataSets);
  AddDS(DM_SERVICE_BUREAU.SB_USER, SupportDataSets);
end;

initialization
  RegisterClass(TEDIT_SB_TEAM_MEMBER);

end.
