inherited frmMaintenance: TfrmMaintenance
  object PageControl1: TPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    TabStop = False
    object TabSheet1: TTabSheet
      Caption = 'Main'
      object StatusBar1: TStatusBar
        Left = 0
        Top = 230
        Width = 435
        Height = 19
        Panels = <
          item
            Text = 'Idle...'
            Width = 150
          end
          item
            Width = 150
          end
          item
            Text = 'Time Elapsed:'
            Width = 50
          end>
      end
      object evplRebuildTmp: TevPanel
        Left = 0
        Top = 0
        Width = 366
        Height = 230
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Bevel1: TBevel
          Left = 8
          Top = 8
          Width = 347
          Height = 78
        end
        object Label7: TLabel
          Left = 18
          Top = 52
          Width = 91
          Height = 13
          Caption = 'Number of Threads'
        end
        object edtThreads: TEdit
          Left = 122
          Top = 49
          Width = 25
          Height = 21
          TabOrder = 0
          Text = '10'
        end
        object UpDown1: TUpDown
          Left = 147
          Top = 49
          Width = 16
          Height = 21
          Associate = edtThreads
          Min = 1
          Max = 64
          Position = 10
          TabOrder = 1
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 101
          Width = 347
          Height = 129
          Caption = 'Email Notification'
          TabOrder = 2
          object Label6: TLabel
            Left = 16
            Top = 52
            Width = 41
            Height = 13
            Caption = 'Send To'
          end
          object edAddressTo: TEdit
            Left = 69
            Top = 49
            Width = 257
            Height = 21
            TabOrder = 0
          end
          object btnSendTest: TevBitBtn
            Left = 141
            Top = 86
            Width = 83
            Height = 22
            Caption = 'Test'
            TabOrder = 1
            OnClick = btnSendTestClick
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD6666666800
              08DDDD8888888DFFFDDDD666666660BBB0DDD88888888D888FDD666EEEEE70BB
              B0DD888DDDDDDD888FDD66EDDDDDDD0008DD88DDDDDDDDFFFDDD66DD666660BB
              B0DD88DD88888D888FDD66D6666660BBB08D88D888888D888FDD66D66E66760B
              BB0888D88D88D8D888FD66D66D66D660BBB088D88D88D88D888F66D66D60006D
              0BB088D88D8DFFFDD88F66D66660BB000BB088D8888D88FFF88F66DE6660BBBB
              BBB088DD888D8888888D66DDEE7D0BBBBB0D88DDDDDDD88888DD666DDDDD6000
              00DD888DDDDD8DDDDDDDE6666666667DDDDDD888888888DDDDDDDE6666666EDD
              DDDDDD8888888DDDDDDDDDEEEEEEEDDDDDDDDDDDDDDDDDDDDDDD}
            NumGlyphs = 2
            Margin = 0
          end
          object cbSendEmail: TCheckBox
            Left = 16
            Top = 23
            Width = 153
            Height = 17
            Caption = 'Send Email when done'
            TabOrder = 2
          end
        end
        object btnDoRebuild: TevBitBtn
          Left = 179
          Top = 28
          Width = 155
          Height = 41
          Caption = 'Rebuild Temporary Tables'
          TabOrder = 3
          OnClick = btnDoRebuildClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD444444444
            4444DDD88888888888888884FFF4FFF4FFF488D8FFF8FFF8FFF8877444444444
            44448DD888888888888887777774FFF4FFF48DDDDDD8FFF8FFF8877777744444
            44448DDDDDD888888888877788888774FFF48DDD888DDDD8FFF8877877777774
            44448DD8DDDDDDD888888778788888778DDD8DD8D88888DDDDDD877878787877
            8DDD8DD8D8D8D8DD8DDD8778787878778DDD8DD8D8D8D8DD8DDD877878887877
            8DDD8DD8D888D8DD8DDD8778777778778DDD8DD8DDDDD8DD8DDD877788888777
            8DDD8DDD88888DDD8DDD8777777777778DDD8DDDDDDDDDDD8DDD888888888888
            8DDD8888888888888DDD8888888888888DDD8888888888888DDD}
          NumGlyphs = 2
          Margin = 0
        end
        object chbTaskQueue: TCheckBox
          Left = 16
          Top = 22
          Width = 146
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Run through the Queue'
          TabOrder = 4
          OnClick = chbTaskQueueClick
        end
      end
      object evplDeletedDB: TevPanel
        Left = 366
        Top = 0
        Width = 156
        Height = 230
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        object evPanel1: TevPanel
          Left = 0
          Top = 189
          Width = 156
          Height = 41
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object btnList: TevBitBtn
            Left = 2
            Top = 1
            Width = 153
            Height = 40
            Caption = 'List Deleted Databases'
            TabOrder = 0
            OnClick = btnListClick
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D33333333333
              33DDD8888888888888DDD3BBBBBBBBBBB3DDD8DDDDDDDDDDD8DDD3BBBBBBBBBB
              B3DDD8DDDDDDDDDDD8DDD3BBB33333BBB3DDD8DDD88888DDD8DDD3BB3BBBBBBB
              B3DDD8DD8DDDDDDDD8DDD3BB3B33333BB3DDD8DD8D88888DD8DDD3BB3B3B3B3B
              B3DDD8DD8D8D8D8DD8DDD3BB3B3B3B3BB3DDD8DD8D8D8D8DD8DDD3BB3B333B3B
              B3DDD8DD8D888D8DD8DDD3BB3BBBBB3BB3DDD8DD8DDDDD8DD8DDD3BBB33333BB
              B3DDD8DDD88888DDD8DDD3BBBBBBBBBBB3DDD8DDDDDDDDDDD8DDD3BBBBBBBB33
              33DDD8DDDDDDDD8888DDD3BBBBBBBB3BB7DDD8DDDDDDDD8DD8DDD3BBBBBBBB3B
              7DDDD8DDDDDDDD8D8DDDD33333333337DDDDD88888888888DDDD}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object grDeletedClients: TevDBGrid
          Left = 0
          Top = 0
          Width = 156
          Height = 189
          DisableThemesInTitle = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TfrmMaintenance\grDeletedClients'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = evDataSource1
          PopupMenu = pmDeletedClient
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Messages'
      ImageIndex = 2
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 598
        Height = 464
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 157
    Top = 2
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 388
    Top = 176
  end
  object pmDeletedClient: TevPopupMenu
    Left = 432
    Top = 176
    object miUndeleteClient: TMenuItem
      Caption = 'Undelete Client'
      OnClick = miUndeleteClientClick
    end
  end
  object dsDeletedClients: TEvBasicClientDataSet
    Left = 420
    Top = 216
    object dsDeletedClientsName: TStringField
      DisplayLabel = 'Deleted Client'
      FieldName = 'Name'
      Size = 32
    end
    object dsDeletedClientsClientID: TIntegerField
      FieldName = 'ClientID'
      Visible = False
    end
  end
  object evDataSource1: TevDataSource
    DataSet = dsDeletedClients
    Left = 440
    Top = 88
  end
end
