// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_USER_MESSAGES;


interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, ISBasicClasses, isUIwwDBEdit, isUIDBMemo, SShowLogo,
  isUIwwDBLookupCombo, SDDClasses, SDataDictbureau, EvCommonInterfaces, EvDataset;

type
  TEDIT_SB_USER_MESSAGES = class(TFrameEntry)
    pctlEDIT_SB_TEAM: TevPageControl;
    tshtBrowse: TTabSheet;
    dedtDescription: TevDBEdit;
    evLabel1: TevLabel;
    lblData: TevLabel;
    edtUserStereotype: TevDBRadioGroup;
    edtData: TEvDBMemo;
    wwdgBrowse: TevDBGrid;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    edtGridStreoType: TevDBComboBox;
  private
    function GetStereoTypeCount(const aStereoType: string; const aNbr: integer): Integer;
  public
    procedure DoAfterCommit(const AError: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

var
  EDIT_SB_USER_MESSAGES: TEDIT_SB_USER_MESSAGES;

implementation

uses SysUtils, SPackageEntry, EvUIUtils;

{$R *.DFM}

{ TEDIT_SB_USER_MESSAGES }

function TEDIT_SB_USER_MESSAGES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_USER_MESSAGES;
end;

function TEDIT_SB_USER_MESSAGES.GetInsertControl: TWinControl;
begin
  Result := edtUserStereotype;
end;

procedure TEDIT_SB_USER_MESSAGES.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  if (Kind = NavOK) then
  begin
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    begin
      wwdsDetail.DataSet.UpdateRecord;

      if GetStereoTypeCount(wwdsDetail.DataSet.FieldByName('USER_STEREOTYPE').asString,
                      wwdsDetail.DataSet.FieldByName('SB_USER_MESSAGES_NBR').asInteger) > 4 then
      begin
          EvMessage('You can define a maximum of 5 messages per user stereotype.', mtInformation, [mbOK]);
          Handled:=true;
      end;
    end;
  end;
end;

function TEDIT_SB_USER_MESSAGES.GetStereoTypeCount(const aStereoType: string; const aNbr: integer): Integer;
var
      s : String;
      Q : IevQuery;
begin
     result := 0;
     s := 'select USER_STEREOTYPE , count(*) from SB_USER_MESSAGES ' +
        ' where {AsOfNow<SB_USER_MESSAGES>} and USER_STEREOTYPE = :stereotype and SB_USER_MESSAGES_NBR <> :nbr ' +
          ' group by USER_STEREOTYPE ';
     Q := TevQuery.Create(s);
     Q.Params.AddValue('stereotype', aStereoType);
     Q.Params.AddValue('nbr', aNbr);
     Q.Execute;
     if Q.Result.RecordCount > 0 then
      Result := Q.Result.Fields[1].AsInteger;
end;


procedure TEDIT_SB_USER_MESSAGES.DoAfterCommit(const AError: Boolean);
begin
  inherited;
  if not AError then
    LogoPresenter.Refresh;
end;

initialization
  RegisterClass(TEDIT_SB_USER_MESSAGES);

end.
