inherited EDIT_SB_QUEUE_PRIORITY: TEDIT_SB_QUEUE_PRIORITY
  object evDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 270
    DisableThemesInTitle = False
    ControlType.Strings = (
      'METHOD_NAME;CustomEdit;cbTask;F'
      'SB_USER_NBR;CustomEdit;lcUser'
      'SB_SEC_GROUPS_NBR;CustomEdit;lcGroup'
      'UserName;CustomEdit;lcUser;F'
      'GroupName;CustomEdit;lcGroup;F')
    Selected.Strings = (
      'METHOD_NAME'#9'40'#9'Task Name'#9'F'
      'PRIORITY'#9'10'#9'Priority'#9'F'
      'THREADS'#9'10'#9'Max Threads'#9'F'
      'UserName'#9'20'#9'User'#9'F'
      'GroupName'#9'20'#9'Group'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SB_QUEUE_PRIORITY\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsList
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object cbTask: TevDBComboBox [1]
    Left = 64
    Top = 111
    Width = 379
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'METHOD_NAME'
    DataSource = wwdsList
    DropDownCount = 15
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 1
    UnboundDataType = wwDefault
  end
  object lcUser: TevDBLookupCombo [2]
    Left = 129
    Top = 183
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'USER_ID'#9'10'#9'USER_ID'#9'F'
      'LAST_NAME'#9'20'#9'LAST_NAME'#9'F'
      'FIRST_NAME'#9'20'#9'FIRST_NAME'#9'F')
    DataField = 'SB_USER_NBR'
    DataSource = wwdsList
    LookupTable = DM_SB_USER.SB_USER
    LookupField = 'SB_USER_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object lcGroup: TevDBLookupCombo [3]
    Left = 126
    Top = 225
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'NAME'#9'40'#9'NAME'#9'F')
    DataField = 'SB_SEC_GROUPS_NBR'
    DataSource = wwdsList
    LookupTable = DM_SB_SEC_GROUPS.SB_SEC_GROUPS
    LookupField = 'SB_SEC_GROUPS_NBR'
    Style = csDropDownList
    TabOrder = 3
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_SB_QUEUE_PRIORITY.SB_QUEUE_PRIORITY
    OnDataChange = wwdsListDataChange
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 252
    Top = 18
  end
end
