inherited UTIL_REVERS_BANK_REGISTER_DEB_CRE: TUTIL_REVERS_BANK_REGISTER_DEB_CRE
  object Pc: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    ActivePage = ts2
    Align = alClient
    TabOrder = 0
    object ts1: TTabSheet
      Caption = 'Select company'
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 665
        Height = 401
        DisableThemesInTitle = False
        Selected.Strings = (
          'CUSTOM_CLIENT_NUMBER'#9'15'#9'Client #'
          'CLIENT_NAME'#9'30'#9'Client name'
          'CUSTOM_COMPANY_NUMBER'#9'15'#9'Company #'#9'F'
          'COMPANY_NAME'#9'30'#9'Company name'
          'TAX_SERVICE'#9'1'#9'Tax')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TUTIL_REVERS_BANK_REGISTER_DEB_CRE\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsClCo
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object btnGo: TevBitBtn
        Left = 560
        Top = 408
        Width = 107
        Height = 25
        Caption = 'Show register'
        TabOrder = 1
        OnClick = btnGoClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D444444444DD
          DDDDD888888888DDDDDD47FFFFFFF4DDDDDD88DDDDDDD8DDDDDD47FFFFFFF488
          8DDD88DDDDDDD8888DDD4444444444DD8DDD8888888888DD8DDD47F777777488
          888D88DFFFFFFFFFFFFD4444000000000000888F88888888888847F70BBBBBBB
          BBB088DF88888888888844480B33333333B0888F88DDDDDDDD8847F70BBBBBBB
          BBB088DF88888888888844440B33333333B0888F88DDDDDDDD8847F70BBBBBBB
          BBB088DF88888888888844440BBBBB3333B0888F888888DDDD8847F70BBBBBBB
          BBB088DF8888888888884444000000000000888D88888888888847FFFFFFF4DD
          DDDD88DDDDDDDDDDDDDD4444444444DDDDDD8888888888DDDDDD}
        NumGlyphs = 2
      end
    end
    object ts2: TTabSheet
      Caption = 'Select register records'
      ImageIndex = 1
      object grRegister: TevDBGrid
        Left = 0
        Top = 0
        Width = 665
        Height = 401
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Do;CheckBox;True;False'
          'MANUAL_TYPE;CustomEdit;evDBComboBox1;F'
          'REGISTER_TYPE;CustomEdit;evDBComboBox2;F'
          'STATUS;CustomEdit;evDBComboBox3;F')
        Selected.Strings = (
          'CHECK_SERIAL_NUMBER'#9'10'#9'Check #'#9'T'
          'CHECK_DATE'#9'10'#9'Check Date'#9'T'
          'STATUS'#9'1'#9'Status'#9'T'
          'STATUS_DATE'#9'10'#9'Status Date'#9'T'
          'TRANSACTION_EFFECTIVE_DATE'#9'10'#9'Effective Date'#9'T'
          'AMOUNT'#9'12'#9'Amount'#9'T'
          'CLEARED_AMOUNT'#9'12'#9'Cleared Amount'#9'T'
          'REGISTER_TYPE'#9'1'#9'Register Type'#9'T'
          'MANUAL_TYPE'#9'1'#9'Manual Type'#9'T'
          'PROCESS_DATE'#9'10'#9'Process Date'#9'T'
          'Do'#9'5'#9'Correct'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TUTIL_REVERS_BANK_REGISTER_DEB_CRE\grRegister'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsReg
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnCalcCellColors = grRegisterCalcCellColors
        PaintOptions.AlternatingRowColor = clCream
      end
      object Button2: TevBitBtn
        Left = 136
        Top = 408
        Width = 81
        Height = 25
        Caption = 'Back'
        TabOrder = 1
        OnClick = Button2Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDA2DDDDDDDDDDDDDDFFDDDDDDDDDDDDAAA
          2DDDDDDDDDDDDFFD8DDDDDDDDDDAAAAA2DDDDDDDDDDFFDDD8DDDDDDDDAAAAAAA
          2DDDDDDDDFFDDDDD8DDDDDDAAAAAAAAA2DDDDDDFFDDDDDDD8DDDD222AAAAAAAA
          2DDDDFF8DDDDDDDD8DDDDD2222AAAAAA2DDDDD8888DDDDDD8DDDDDDD2222AAAA
          2DDDDDDD8888DDDD8DDDDDDDDD2222AA2DDDDDDDDD8888DD8DDDDDDDDDDD2222
          2DDDDDDDDDDD88888DDDDDDDDDDDDD222DDDDDDDDDDDDD888DDDDDDDDDDDDDDD
          2DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button3: TevBitBtn
        Left = 336
        Top = 408
        Width = 97
        Height = 25
        Caption = 'Toggle all'
        TabOrder = 2
        OnClick = Button3Click
      end
      object Button4: TevBitBtn
        Left = 440
        Top = 408
        Width = 97
        Height = 25
        Caption = 'Toggle selected'
        TabOrder = 3
        OnClick = Button4Click
      end
      object Button1: TevBitBtn
        Left = 568
        Top = 408
        Width = 97
        Height = 25
        Caption = 'Reverse records'
        TabOrder = 4
        OnClick = Button1Click
      end
      object evDBComboBox1: TevDBComboBox
        Left = 360
        Top = 104
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MANUAL_TYPE'
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object evDBComboBox2: TevDBComboBox
        Left = 360
        Top = 128
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REGISTER_TYPE'
        DataSource = dsReg
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object evDBComboBox3: TevDBComboBox
        Left = 360
        Top = 152
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'STATUS'
        DataSource = dsReg
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
    end
  end
  object cdClCo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    FieldDefs = <
      item
        Name = 'CUSTOM_CLIENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CLIENT_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'COMPANY_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'TAX_SERVICE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FEIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'TRUST_SERVICE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TERMINATION_CODE'
        DataType = ftString
        Size = 1
      end>
    Left = 24
    object cdClCoCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 15
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdClCoCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 30
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdClCoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Company #'
      DisplayWidth = 15
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdClCoCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 30
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdClCoTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdClCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdClCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdClCoFEIN: TStringField
      FieldName = 'FEIN'
      Visible = False
      Size = 9
    end
    object cdClCoTRUST_SERVICE: TStringField
      FieldName = 'TRUST_SERVICE'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object cdClCoTERMINATION_CODE: TStringField
      FieldName = 'TERMINATION_CODE'
      Visible = False
      FixedChar = True
      Size = 1
    end
  end
  object dsClCo: TevDataSource
    DataSet = cdClCo
    Left = 56
  end
  object dsReg: TevDataSource
    DataSet = cdReg
    Left = 248
  end
  object cdReg: TevClientDataSet
    PictureMasks.Strings = (
      'AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9'T'#9'F'
      
        'CLEARED_AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]' +
        '}'#9'T'#9'F')
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    FieldDefs = <
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_BANK_ACCOUNTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'TRANSACTION_EFFECTIVE_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'CLEARED_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'REGISTER_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MANUAL_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_MANUAL_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_PR_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_PAYMENT_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PROCESS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 1
      end
      item
        Name = 'PR_MISCELLANEOUS_CHECKS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_BANK_ACCOUNT_REGISTER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'CLOSE_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'Do'
        DataType = ftBoolean
      end>
    Left = 216
    object cdRegCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object cdRegSB_BANK_ACCOUNTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_BANK_ACCOUNTS_NBR'
    end
    object cdRegPR_CHECK_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_NBR'
    end
    object cdRegCHECK_SERIAL_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'CHECK_SERIAL_NUMBER'
    end
    object cdRegCHECK_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CHECK_DATE'
    end
    object cdRegSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object cdRegSTATUS_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'STATUS_DATE'
    end
    object cdRegTRANSACTION_EFFECTIVE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'TRANSACTION_EFFECTIVE_DATE'
    end
    object cdRegAMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object cdRegCLEARED_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'CLEARED_AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object cdRegTYPE: TStringField
      DisplayWidth = 1
      FieldName = 'REGISTER_TYPE'
      Size = 1
    end
    object cdRegMANUAL_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'MANUAL_TYPE'
      Size = 1
    end
    object cdRegCO_MANUAL_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_MANUAL_ACH_NBR'
    end
    object cdRegCO_PR_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PR_ACH_NBR'
    end
    object cdRegCO_TAX_PAYMENT_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_PAYMENT_ACH_NBR'
    end
    object cdRegPROCESS_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'PROCESS_DATE'
    end
    object cdRegNOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Size = 1
    end
    object cdRegPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_MISCELLANEOUS_CHECKS_NBR'
    end
    object cdRegCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
      Visible = False
    end
    object cdRegFILLER: TStringField
      DisplayWidth = 10
      FieldName = 'FILLER'
      Visible = False
      Size = 512
    end
    object cdRegCLOSE_DATE: TDateTimeField
      FieldName = 'CLOSE_DATE'
    end
    object cdRegDo: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'Do'
    end
  end
end
