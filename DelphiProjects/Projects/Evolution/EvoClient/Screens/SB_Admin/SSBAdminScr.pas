// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSBAdminScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  ISBasicClasses, EvCommonInterfaces, EvMainboard, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TSBAdminFrm = class(TFramePackageTmpl, IevSBAdminScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

{$R *.DFM}

var
  SBAdminFrm: TSBAdminFrm;

function GetSBAdminScreens: IevSBAdminScreens;
begin
  if not Assigned(SBAdminFrm) then
    SBAdminFrm := TSBAdminFrm.Create(nil);
  Result := SBAdminFrm;
end;



{ TAdminFrm }

function TSBAdminFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TSBAdminFrm.PackageCaption: string;
begin
  Result := 'SB Admin';
end;

function TSBAdminFrm.PackageSortPosition: string;
begin
  Result := '005';
end;

procedure TSBAdminFrm.UserPackageStructure;
begin
  AddStructure('Service Bureau|100', 'TEDIT_SB');
  AddStructure('Teams|300');
  AddStructure('Teams\Teams|1000', 'TEDIT_SB_TEAM');
  AddStructure('Teams\Members|1010', 'TEDIT_SB_TEAM_MEMBER');
  AddStructure('Register credit-debit reverse|500', 'TUTIL_REVERS_BANK_REGISTER_DEB_CRE');
  AddStructure('Security|400');
  AddStructure('Security\SB Templates|100', 'TEDIT_SB_SEC_TEMPLATES');
  AddStructure('Scheduler|800', 'TScheduler');
  AddStructure('Task Priority Overrides|810', 'TEDIT_SB_QUEUE_PRIORITY');
  AddStructure('User Messages|840', 'TEDIT_SB_USER_MESSAGES');
  AddStructure('Maintenance|860', 'TfrmMaintenance');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSBAdminScreens, IevSBAdminScreens, 'Screens SB Admin');

finalization
  SBAdminFrm.Free;

end.
