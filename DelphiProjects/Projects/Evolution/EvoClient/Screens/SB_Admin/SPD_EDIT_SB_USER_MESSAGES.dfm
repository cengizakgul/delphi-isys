inherited EDIT_SB_USER_MESSAGES: TEDIT_SB_USER_MESSAGES
  object pctlEDIT_SB_TEAM: TevPageControl [0]
    Left = 16
    Top = 8
    Width = 345
    Height = 577
    HelpContext = 37001
    ActivePage = tshtBrowse
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object evLabel1: TevLabel
        Left = 8
        Top = 382
        Width = 66
        Height = 13
        Caption = 'Order Number'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblData: TevLabel
        Left = 8
        Top = 421
        Width = 52
        Height = 16
        Caption = '~Message'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtDescription: TevDBEdit
        Left = 8
        Top = 397
        Width = 65
        Height = 21
        DataField = 'ORDER_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edtUserStereotype: TevDBRadioGroup
        Left = 8
        Top = 338
        Width = 281
        Height = 36
        Caption = '~User Stereo Type'
        Columns = 3
        DataField = 'USER_STEREOTYPE'
        DataSource = wwdsDetail
        TabOrder = 0
      end
      object edtData: TEvDBMemo
        Left = 8
        Top = 436
        Width = 281
        Height = 89
        DataField = 'DATA'
        DataSource = wwdsDetail
        TabOrder = 2
      end
      object wwdgBrowse: TevDBGrid
        Left = 8
        Top = 0
        Width = 281
        Height = 330
        DisableThemesInTitle = False
        ControlType.Strings = (
          'USER_STEREOTYPE;CustomEdit;edtGridStreoType;F')
        Selected.Strings = (
          'USER_STEREOTYPE'#9'15'#9'User Stereo Type'#9'F'
          'ORDER_NUMBER'#9'10'#9'Order')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_USER_MESSAGES\wwdgBrowse'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 3
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object edtGridStreoType: TevDBComboBox
        Left = 224
        Top = 136
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'USER_STEREOTYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_USER_MESSAGES.SB_USER_MESSAGES
    MasterDataSource = nil
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 320
    Top = 80
  end
end
