// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_TEAM;
{**************************************************************************}
{* unit: SPD_EDIT_SB_TEAM.pas                                             *}
{* Description: Form used to maintain Service Bureau Holiday information  *}
{*              from the Service Bureau -> Team comboboxes.                     *}
{*                                                                        *}
{* Copyright(c) PayData Payroll Services, Inc.                            *}
{*                                                                        *}
{* Author:  Tim Stevenson                                                 *}
{* Written: 04/11/1998                                                    *}
{* Updated: 05/21/1998                                                    *}
{*                                                                        *}
{**************************************************************************}

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_TEAM = class(TFrameEntry)
    pctlEDIT_SB_TEAM: TevPageControl;
    tshtBrowse: TTabSheet;
    wwdgBrowse: TevDBGrid;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    evLabel1: TevLabel;
    evDBComboBox1: TevDBComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_TEAM: TEDIT_SB_TEAM;

implementation

uses SysUtils;

{$R *.DFM}

{ TEDIT_SB_TEAM }

function TEDIT_SB_TEAM.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_TEAM;
end;

function TEDIT_SB_TEAM.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

initialization
  RegisterClass(TEDIT_SB_TEAM);

end.
