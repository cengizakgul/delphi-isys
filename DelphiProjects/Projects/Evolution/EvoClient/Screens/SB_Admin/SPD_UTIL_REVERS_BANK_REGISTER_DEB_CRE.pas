// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_UTIL_REVERS_BANK_REGISTER_DEB_CRE;

interface

uses
  SFrameEntry,  Db,  StdCtrls, Controls,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Classes, Wwdatsrc, Graphics, Buttons, Variants,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, Mask, wwdbedit, Wwdotdot, Wwdbcomb, SFieldCodeValues, EvUIComponents, EvClientDataSet;

type
  TUTIL_REVERS_BANK_REGISTER_DEB_CRE = class(TFrameEntry)
    cdClCo: TevClientDataSet;
    cdClCoCL_NBR: TIntegerField;
    cdClCoCUSTOM_CLIENT_NUMBER: TStringField;
    cdClCoCLIENT_NAME: TStringField;
    cdClCoCO_NBR: TIntegerField;
    cdClCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdClCoCOMPANY_NAME: TStringField;
    cdClCoFEIN: TStringField;
    cdClCoTAX_SERVICE: TStringField;
    cdClCoTRUST_SERVICE: TStringField;
    cdClCoTERMINATION_CODE: TStringField;
    dsClCo: TevDataSource;
    Pc: TevPageControl;
    ts1: TTabSheet;
    wwDBGrid1: TevDBGrid;
    ts2: TTabSheet;
    grRegister: TevDBGrid;
    dsReg: TevDataSource;
    cdReg: TevClientDataSet;
    cdRegCO_NBR: TIntegerField;
    cdRegSB_BANK_ACCOUNTS_NBR: TIntegerField;
    cdRegPR_CHECK_NBR: TIntegerField;
    cdRegCHECK_SERIAL_NUMBER: TIntegerField;
    cdRegCHECK_DATE: TDateTimeField;
    cdRegSTATUS: TStringField;
    cdRegSTATUS_DATE: TDateTimeField;
    cdRegTRANSACTION_EFFECTIVE_DATE: TDateTimeField;
    cdRegAMOUNT: TFloatField;
    cdRegCLEARED_AMOUNT: TFloatField;
    cdRegTYPE: TStringField;
    cdRegMANUAL_TYPE: TStringField;
    cdRegCO_MANUAL_ACH_NBR: TIntegerField;
    cdRegCO_PR_ACH_NBR: TIntegerField;
    cdRegCO_TAX_PAYMENT_ACH_NBR: TIntegerField;
    cdRegPROCESS_DATE: TDateTimeField;
    cdRegNOTES: TBlobField;
    cdRegPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField;
    cdRegCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField;
    cdRegFILLER: TStringField;
    cdRegCLOSE_DATE: TDateTimeField;
    cdRegDo: TBooleanField;
    btnGo: TevBitBtn;
    Button2: TevBitBtn;
    Button3: TevBitBtn;
    Button4: TevBitBtn;
    Button1: TevBitBtn;
    evDBComboBox1: TevDBComboBox;
    evDBComboBox2: TevDBComboBox;
    evDBComboBox3: TevDBComboBox;
    procedure btnGoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure grRegisterCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

uses EvUtils, SDataStructure, EvConsts, SysUtils;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.btnGoClick(Sender: TObject);
begin
  ctx_StartWait('Loading...');
  try
    ctx_DataAccess.OpenClient(cdClCo['CL_NBR']);
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_NBR='+ IntToStr(cdClCo['CO_NBR']));
    cdReg.Close;
    cdReg.Data := DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Data;
    cdReg.Resync([]);
    grRegister.SelectRecord;
    cdReg.DisableControls;
    with cdReg do
    try
      First;
      while not Eof do
      begin
        Edit;
        FieldValues['Do'] := False;
        if not VarIsNull(FieldValues['CHECK_SERIAL_NUMBER']) then
        begin
          FieldValues['Do'] :=
            ((FieldValues['Amount'] < 0)
              and (string(FieldValues['REGISTER_Type'])[1] in [
                BANK_REGISTER_TYPE_Void, BANK_REGISTER_TYPE_Agency_Void,
                BANK_REGISTER_TYPE_Tax_Void, BANK_REGISTER_TYPE_Billing_Void]))
            or ((FieldValues['Amount'] > 0)
              and (string(FieldValues['REGISTER_Type'])[1] in [
                BANK_REGISTER_TYPE_Regular, BANK_REGISTER_TYPE_Manual,
                BANK_REGISTER_TYPE_Billing,
                BANK_REGISTER_TYPE_Agency, BANK_REGISTER_TYPE_Tax]));
        end;
        Post;
        Next;
      end;
      First;
    finally
      EnableControls;
    end;
  finally
    ctx_EndWait;
  end;
  grRegister.UnselectAll;
  Pc.ActivePage := ts2;
  ts2.TabVisible := True;
  ts1.TabVisible := False;
end;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.Button1Click(Sender: TObject);
begin
  inherited;
  cdReg.CheckBrowseMode;
  cdReg.DisableControls;
  ctx_StartWait('Processing...');
  try
    cdReg.First;
    while not cdReg.Eof do
    begin
      if cdReg['Do'] then
      begin
        Assert(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR', cdReg['CO_BANK_ACCOUNT_REGISTER_NBR'], []));
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['AMOUNT'] := -DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['AMOUNT'];
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      end;
      cdReg.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
  finally
    cdReg.EnableControls;
    ctx_EndWait;
  end;
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.Button2Click(Sender: TObject);
begin
  inherited;
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.grRegisterCalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;
  if cdReg['Do'] then
    ABrush.Color := clYellow;
end;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.Button3Click(Sender: TObject);
var
  v: Boolean;
begin
  inherited;
  cdReg.DisableControls;
  try
    cdReg.First;
    v := not cdReg['Do'];
    while not cdReg.Eof do
    begin
      cdReg.Edit;
      cdReg['Do'] := v;
      cdReg.Post;
      cdReg.Next;
    end;
    cdReg.First;
  finally
    cdReg.EnableControls;
  end;
end;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.Button4Click(Sender: TObject);
var
  i: Integer;
  v: Boolean;
  b: TBookMarkStr;
begin
  inherited;
  v := False;
  cdReg.DisableControls;
  try
    b := cdReg.Bookmark;
    for i := 0 to grRegister.SelectedList.Count-1 do
    begin
      cdReg.GotoBookmark(grRegister.SelectedList[i]);
      if i = 0 then
        v := not cdReg['Do'];
      cdReg.Edit;
      cdReg['Do'] := v;
      cdReg.Post;
    end;
    cdReg.Bookmark := b;
  finally
    cdReg.EnableControls;
  end;
end;

procedure TUTIL_REVERS_BANK_REGISTER_DEB_CRE.Activate;
begin
  inherited;
  with ctx_DataAccess.TEMP_CUSTOM_VIEW do
  begin
    Close;
    DataRequest(TExecDSWrapper.Create('SelectClientsCompanies').AsVariant);
    Open;
    cdClCo.Data := Data;
    Close;
  end;
  wwDBGrid1.SelectRecord;
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
  evDBComboBox1.Items.Text := GroupBox_Bank_Register_ManualType_ComboChoices;
  evDBComboBox1.MapList := True;
  evDBComboBox2.Items.Text := GroupBox_Bank_Register_Type_ComboChoices;
  evDBComboBox2.MapList := True;
  evDBComboBox3.Items.Text := GroupBox_Bank_Register_Status_ComboChoices;
  evDBComboBox3.MapList := True;
end;

initialization
  RegisterClass(TUTIL_REVERS_BANK_REGISTER_DEB_CRE);

end.
