// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_SEC_TEMPLATES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_SY_SEC_TEMPLATES, SDataStructure, Db, Wwdatsrc,
  StdCtrls, Buttons, Mask, wwdbedit, Grids, Wwdbigrd, Wwdbgrid,
  SDataDictbureau, SDDClasses, SDataDictsystem, ISBasicClasses,
  EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBEdit,
  EvClientDataSet;

type                         
  TEDIT_SB_SEC_TEMPLATES = class(TEDIT_SY_SEC_TEMPLATES)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure wwDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetBlobField: TBlobField; override;
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  EDIT_SB_SEC_TEMPLATES: TEDIT_SB_SEC_TEMPLATES;

implementation


uses SFrameEntry;

{$R *.DFM}

{ TEDIT_SB_SEC_TEMPLATES }

procedure TEDIT_SB_SEC_TEMPLATES.Activate;
begin
   Inherited;
   //5-16-2012  CRAIG
   //added this section to ensure that the SB_SEC_TEMPLATES
   //vs. the inherited SY_SEC_TEMPLATES.
   //Reso # 96013
   wwdsMaster.DataSet := DM_SERVICE_BUREAU.SB_SEC_TEMPLATES;
end;

function TEDIT_SB_SEC_TEMPLATES.GetBlobField: TBlobField;
begin
  Result := wwdsDetail.DataSet.FindField('TEMPLATE') as TBlobField;
end;

function TEDIT_SB_SEC_TEMPLATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_SEC_TEMPLATES;
end;

procedure TEDIT_SB_SEC_TEMPLATES.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  bEdit.Click;
end;

initialization
  RegisterClass(TEDIT_SB_SEC_TEMPLATES);

end.
