inherited EDIT_SB_TEAM: TEDIT_SB_TEAM
  Width = 624
  Height = 565
  object pctlEDIT_SB_TEAM: TevPageControl [0]
    Left = 16
    Top = 8
    Width = 585
    Height = 465
    HelpContext = 37001
    ActivePage = tshtBrowse
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object lablDescription: TevLabel
        Left = 8
        Top = 336
        Width = 65
        Height = 13
        Caption = '~Description'
        FocusControl = dedtDescription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 8
        Top = 384
        Width = 153
        Height = 13
        Caption = '~Correction Runs Category'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwdgBrowse: TevDBGrid
        Left = 0
        Top = 0
        Width = 281
        Height = 329
        Selected.Strings = (
          'TEAM_DESCRIPTION'#9'40'#9'Description')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object dedtDescription: TevDBEdit
        Left = 8
        Top = 352
        Width = 273
        Height = 21
        DataField = 'TEAM_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBComboBox1: TevDBComboBox
        Left = 8
        Top = 400
        Width = 145
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'CR_CATEGORY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
end
