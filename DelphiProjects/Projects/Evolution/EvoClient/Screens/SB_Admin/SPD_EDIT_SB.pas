// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit, Forms,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, Wwdbspin, EvUtils, Graphics, SShowLogo,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, EvContext, EvConsts, evTypes, EvExceptions,
  SDDClasses, SDataDicttemp, EvUIComponents, EvClientDataSet, isUIEdit,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBComboBox, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, LMDCustomButton, LMDButton,
  isUILMDButton, isUIDBImage, isUIwwDBEdit;

type
  TEDIT_SB = class(TFrameEntry)
    PageControl1: TevPageControl;
    tshtGeneral_Info: TTabSheet;
    tshtFlags_And_Settings: TTabSheet;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablName: TevLabel;
    lablE_Mail: TevLabel;
    lablEIN_Number: TevLabel;
    lablEFTPS_TIN_Nbr: TevLabel;
    lablFuture_Invoice_Nbr: TevLabel;
    wwdeAddress1: TevDBEdit;
    wwdeAddress2: TevDBEdit;
    wwdeCity: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwdeName: TevDBEdit;
    wwdeE_Mail: TevDBEdit;
    wwdeEIN_Number: TevDBEdit;
    panlBelowAddressInfo: TevPanel;
    wwdeEFTPS_TIN_Nbr: TevDBEdit;
    wwdeFuture_Invoice_Nbr: TevDBEdit;
    drgpUse_Prenote: TevDBRadioGroup;
    drgpEFTPS_Bank_Format: TevDBRadioGroup;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    dedtState: TevDBEdit;
    tshtCover_Notes: TTabSheet;
    dmemCover_Notes: TEvDBMemo;
    lPayrollEMail: TevLabel;
    ePayrollEMail: TevDBEdit;
    lablDefault_Check_Format: TevLabel;
    wwcbDefault_Check_Format: TevDBComboBox;
    lablMICR_Horizontal_Adjustment: TevLabel;
    lablAuto_Save_Minutes: TevLabel;
    wwseAuto_Save_Minutes: TevDBSpinEdit;
    lblAutoSaveMins: TevLabel;
    wwdeFax: TevDBEdit;
    Label1: TevLabel;
    wwdePhone: TevDBEdit;
    lablPhone: TevLabel;
    bevEDIT_SB2: TEvBevel;
    bevSB1: TEvBevel;
    bevSB2: TEvBevel;
    bevSB3: TEvBevel;
    tshtTaxCover_Notes: TTabSheet;
    Bevel1: TEvBevel;
    dmemTaxCover_Notes: TEvDBMemo;
    tshtInvoice_Notes: TTabSheet;
    Bevel2: TEvBevel;
    dmemInvoice_Notes: TEvDBMemo;
    wwdeACH_Directory: TevDBEdit;
    Label3: TevLabel;
    Label2: TevLabel;
    wwDBEdit1: TevDBEdit;
    Label4: TevLabel;
    wwdeImport_Directory: TevDBEdit;
    dlgOpen: TOpenDialog;
    wwseDays_In_Prenote: TevDBSpinEdit;
    Label6: TevLabel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBSpinEdit1: TevDBSpinEdit;
    cdsInvNumber: TevClientDataSet;
    cdsInvNumberNumber: TIntegerField;
    dsInvNumber: TevDataSource;
    butnOpenARImportDirectory: TevSpeedButton;
    butnOpenACHDirectory: TevSpeedButton;
    cbNoOfflines: TevDBCheckBox;
    lablLogo: TevLabel;
    dimgLogo: TevDBImage;
    bbtnLoad_Logo: TevBitBtn;
    odlgGetBitmap: TOpenDialog;
    bbtnClear_Logo: TevBitBtn;
    evDBRadioGroup1: TevDBRadioGroup;
    gbSecurity: TevGroupBox;
    evDBCheckBox1: TevDBCheckBox;
    evDBSpinEdit2: TevDBSpinEdit;
    evLabel2: TevLabel;
    evDBCheckBox2: TevDBCheckBox;
    lablUser_Password_Duration_Days: TevLabel;
    wwseUser_Password_Duration_Days: TevDBSpinEdit;
    lablUSER_PASSWORD_DURATION_IN_DAYS_DAYS: TevLabel;
    evDBCheckBox3: TevDBCheckBox;
    evDBCheckBox4: TevDBCheckBox;
    TabSheet1: TTabSheet;
    EvBevel1: TEvBevel;
    dmemVMR_Conf_Notes: TEvDBMemo;
    drgpDOBforW2: TevDBRadioGroup;
    evDBComboBox1: TevDBComboBox;
    evLabel3: TevLabel;
    evDBComboBox2: TevDBComboBox;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel4: TevLabel;
    evDBComboBox3: TevDBComboBox;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evDBComboBox4: TevDBComboBox;
    evDBComboBox5: TevDBComboBox;
    evLabel9: TevLabel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evDBRadioGroup2: TevDBRadioGroup;
    wwseDaysPriorToCheckDate: TevDBSpinEdit;
    evLabel12: TevLabel;
    evDBEdit2: TevDBEdit;
    evDBComboBox6: TevDBComboBox;
    evDBComboBox7: TevDBComboBox;
    wwDBLookupCombo1: TevDBLookupCombo;
    Label1x: TevLabel;
    lablMisc_Check_Format: TevLabel;
    wwcbMisc_Check_Format: TevDBComboBox;
    evDBRadioGroup3: TevDBRadioGroup;
    DM_TEMPORARY: TDM_TEMPORARY;
    tsTermsOfUse: TTabSheet;
    EvDBMemo1: TEvDBMemo;
    EvBevel2: TEvBevel;
    EvDBMemo2: TEvDBMemo;
    evLabel13: TevLabel;
    evLabel14: TevLabel;
    evLabel15: TevLabel;
    evDBComboBox8: TevDBComboBox;
    ExternalLogins: TTabSheet;
    evGroupBox1: TevGroupBox;
    evLabel16: TevLabel;
    editAdminUsername: TevEdit;
    evLabel17: TevLabel;
    editPassword: TevEdit;
    evGroupBox2: TevGroupBox;
    lbCompanyUserID: TevLabel;
    lbCompanyPassword: TevLabel;
    edCompanyUserID: TevEdit;
    edCompanyPassword: TevEdit;
    labESSLogo: TevLabel;
    dimgESSLogo: TevDBImage;
    bbtnLoad_ESSLogo: TevBitBtn;
    bbtnClear_ESSLogo: TevBitBtn;
    gbSessionTimeouts: TevGroupBox;
    evLabel18: TevLabel;
    evLabel20: TevLabel;
    edSessionTimeout: TevDBSpinEdit;
    evLabel21: TevLabel;
    edSessionLockout: TevDBSpinEdit;
    evLabel22: TevLabel;

    procedure butnOpenARImportDirectoryClick(Sender: TObject);
    procedure butnOpenACHDirectoryClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure bbtnLoad_LogoClick(Sender: TObject);
    procedure bbtnClear_LogoClick(Sender: TObject);
    procedure drgpUse_PrenoteChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);

    procedure LoadSwipeClockAndMRC;
    procedure editAdminUsernameChange(Sender: TObject);
    procedure bbtnClear_ESSLogoClick(Sender: TObject);
    procedure bbtnLoad_ESSLogoClick(Sender: TObject);
  Private
    FSCUserName, FSCPassword, FMRCUser, FMRCPass : string;
    procedure ChangeSSOMode (avalue : integer);
  public
    { Public declarations }
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB: TEDIT_SB;

implementation

uses SysUtils, SPackageEntry, ISBasicUtils, isVCLBugFix, isBaseClasses, EvStreamUtils,SEncryptionRoutines;

{$R *.DFM}

procedure TEDIT_SB.butnOpenARImportDirectoryClick(Sender: TObject);
begin
  inherited;
  dlgOpen.Title := 'Select AR Export Directory';
  dlgOpen.FileName := wwdeImport_Directory.Text;
  if dlgOpen.Execute then
  begin
    with wwdeImport_Directory.DataSource.DataSet do
    begin
      if not (State in [dsEdit, dsInsert]) then
        edit;
      FieldByName(wwdeImport_Directory.DataField).AsString := ExtractFilePath(DlgOpen.FileName);
    end;
  end;
end;

procedure TEDIT_SB.butnOpenACHDirectoryClick(Sender: TObject);
begin
  inherited;
  dlgOpen.Title := 'Select ACH Directory';
  dlgOpen.FileName := wwdeACH_Directory.Text;
  if dlgOpen.Execute then
  begin
    with wwdeACH_Directory.DataSource.DataSet do
    begin
      if not (State in [dsEdit, dsInsert]) then
        edit;
      FieldByName(wwdeACH_Directory.DataField).AsString := ExtractFilePath(DlgOpen.FileName);
    end;
  end;
end;

function TEDIT_SB.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB;
end;

procedure TEDIT_SB.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_TEMPORARY.TMP_CL, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_STORAGE, EditDataSets, '((tag=''SWIPECLOCK'')or(tag=''MYRECRUITINGCENTER''))');
end;

function TEDIT_SB.GetInsertControl: TWinControl;
begin
  Result := wwdeName;
end;

procedure TEDIT_SB.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  inherited;
  if (Kind in [NavFirst..NavLast, NavDelete])
  or ((Kind = NavInsert) and wwdsDetail.Active and (wwdsDetail.DataSet.RecordCount > 0)) then
    Value := False;
end;

procedure TEDIT_SB.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.State = dsInsert then
  begin
    wwdsDetail.DataSet['DEVELOPMENT_MODEM_NUMBER'] := 'x';
    editAdminUsername.Text := '';
    editPassword.Text := '';
    edCompanyUserID.Text := '';
    edCompanyPassword.Text := '';
  end;
end;

procedure TEDIT_SB.Activate;
var
  ads: TArrayDS;
begin
  inherited;
  PageControl1.ActivePage := tshtGeneral_Info;
  cdsInvNumber.CreateDataSet;
  cdsInvNumber.Append;
  cdsInvNumber.Fields[0].AsInteger := ctx_DBAccess.GetGeneratorValue('G_NEXT_INVOICE_NUMBER', dbtBureau, 0);
  cdsInvNumber.Post;
  cdsInvNumber.MergeChangeLog;

  ads := (Owner as TFramePackageTmpl).EditDataSets;
  AddDS(cdsInvNumber, ads);
  (Owner as TFramePackageTmpl).EditDataSets := ads;

  LoadSwipeClockAndMRC;
end;

procedure TEDIT_SB.Deactivate;
begin
  inherited;
  cdsInvNumber.Close;
end;

procedure TEDIT_SB.ButtonClicked(Kind: Integer; var Handled: Boolean);

  procedure AddData(aUser, aPass, aTag : string);
  var
    tmpStream : IisStream;
    List : IislistOfValues;
  begin
     List := TislistOfValues.Create;
     List.AddValue(SBSTORAGE_TAG_SSOUSERNAME, aUser);
     List.AddValue(SBSTORAGE_TAG_SSOPASSWORD, EncryptString(aPass));
     tmpStream := TevDualStreamHolder.CreateInMemory;
     List.WriteToStream(tmpStream);
     if aTag = '' then
        DM_SERVICE_BUREAU.SB_STORAGE.edit
     else begin
        DM_SERVICE_BUREAU.SB_STORAGE.Append;
        DM_SERVICE_BUREAU.SB_STORAGE.FieldByName('TAG').asString := aTag;
     end;

     TBlobField(DM_SERVICE_BUREAU.SB_STORAGE.FieldByName('STORAGE_DATA')).LoadFromStream(tmpStream.RealStream) ;
     DM_SERVICE_BUREAU.SB_STORAGE.Post;
  end;


  procedure ApplyOption;
  begin
    editAdminUsername.Text := Trim(editAdminUsername.Text);
    editPassword.Text := Trim(editPassword.Text);
    edCompanyUserID.Text := Trim(edCompanyUserID.Text);
    edCompanyPassword.Text := Trim(edCompanyPassword.Text);

    if AnsiSameStr( FSCUserName, editAdminUsername.Text) and
       AnsiSameStr( FSCPassword, editPassword.Text) and
       AnsiSameStr( FMRCUser, edCompanyUserID.Text) and
       AnsiSameStr( FMRCPass, edCompanyPassword.Text) then exit;

    if (not AnsiSameStr( FSCUserName, editAdminUsername.Text)) or
       (not AnsiSameStr( FSCPassword, editPassword.Text)) then
    begin
      if DM_SERVICE_BUREAU.SB_STORAGE.Locate('TAG',SBSTORAGE_TAG_SWIPECLOCK, []) then
      begin
        if (editAdminUsername.Text <> '') or (editPassword.Text <> '') then
          AddData(editAdminUsername.Text, editPassword.Text,'')
        else
          DM_SERVICE_BUREAU.SB_STORAGE.Delete; // one of them is empty, will be deleted
      end
      else if (editAdminUsername.Text <> '') or (editPassword.Text <> '') then
         AddData(editAdminUsername.Text,editPassword.Text, SBSTORAGE_TAG_SWIPECLOCK);
    end;

    if (not AnsiSameStr( FMRCUser, edCompanyUserID.Text) ) or
       (not AnsiSameStr( FMRCPass, edCompanyPassword.Text) ) then
    begin
      if DM_SERVICE_BUREAU.SB_STORAGE.Locate('TAG',SBSTORAGE_TAG_MYRECRUITINGCENTER, []) then
      begin
        if (edCompanyUserID.Text <> '') or (edCompanyPassword.Text <> '')  then
          AddData(edCompanyUserID.Text, edCompanyPassword.Text,'')
        else
          DM_SERVICE_BUREAU.SB_STORAGE.Delete; // one of them is empty, will be deleted
      end
      else if (edCompanyUserID.Text<>'') or (edCompanyPassword.Text<>'') then
         AddData(edCompanyUserID.Text,edCompanyPassword.Text, SBSTORAGE_TAG_MYRECRUITINGCENTER);
    end;
  end;
begin
  inherited;
  if Kind = NavCommit then
  begin
    if cdsInvNumber.ChangeCount > 0 then
    begin
      ctx_DBAccess.SetGeneratorValue('G_NEXT_INVOICE_NUMBER', dbtBureau, cdsInvNumber.Fields[0].AsInteger);
      cdsInvNumber.MergeChangeLog;
      (Owner as TFramePackageTmpl).btnNavAbort.Enabled := DM_SERVICE_BUREAU.SB.ChangeCount > 0;
      (Owner as TFramePackageTmpl).btnNavCommit.Enabled := DM_SERVICE_BUREAU.SB.ChangeCount > 0;
    end;
  end
  else if Kind = NavOK then
  begin
    ChangeSSOMode (1);
    ApplyOption;
    ChangeSSOMode (0);
    LoadSwipeClockAndMRC;
  end;

end;

procedure TEDIT_SB.bbtnLoad_LogoClick(Sender: TObject);
var
  TempPicture: TPicture;
begin
  inherited;
  if odlgGetBitmap.Execute then
  begin
    CheckFileSize(odlgGetBitmap.FileName, 700);
    if not (dimgLogo.DataSource.DataSet.State in [dsInsert, dsEdit]) then
      dimgLogo.DataSource.DataSet.Edit;
    TempPicture := TPicture.Create;
    try
      TempPicture.LoadFromFile(odlgGetBitmap.FileName);
      dimgLogo.Picture := TempPicture;
    finally
      TempPicture.Free;
    end;
  end;

  LogoPresenter.Refresh;
end;

procedure TEDIT_SB.bbtnClear_LogoClick(Sender: TObject);
begin
  inherited;
  if not (dimgLogo.DataSource.DataSet.State in [dsInsert, dsEdit]) then
    dimgLogo.DataSource.DataSet.Edit;
  dimgLogo.DataSource.DataSet.FieldByName(dimgLogo.DataField).Clear;
  LogoPresenter.Refresh;
end;

procedure TEDIT_SB.bbtnLoad_ESSLogoClick(Sender: TObject);
var
  TempPicture: TPicture;
begin
  inherited;
  if odlgGetBitmap.Execute then
  begin
    CheckFileSize(odlgGetBitmap.FileName, 700);
    if not (dimgESSLogo.DataSource.DataSet.State in [dsInsert, dsEdit]) then
      dimgESSLogo.DataSource.DataSet.Edit;
    TempPicture := TPicture.Create;
    try
      TempPicture.LoadFromFile(odlgGetBitmap.FileName);
      dimgESSLogo.Picture := TempPicture;
    finally
      TempPicture.Free;
    end;
  end;
end;

procedure TEDIT_SB.bbtnClear_ESSLogoClick(Sender: TObject);
begin
  inherited;
  if not (dimgESSLogo.DataSource.DataSet.State in [dsInsert, dsEdit]) then
    dimgESSLogo.DataSource.DataSet.Edit;
  dimgESSLogo.DataSource.DataSet.FieldByName(dimgESSLogo.DataField).Clear;
end;

procedure TEDIT_SB.drgpUse_PrenoteChange(Sender: TObject);
begin
  inherited;
  wwseDays_In_Prenote.Enabled := drgpUse_Prenote.ItemIndex = 0;
  // MR 9/28/06 Removed extra label for reso #40827
  //  Label5.Enabled := wwseDays_In_Prenote.Enabled;
  Label6.Enabled := wwseDays_In_Prenote.Enabled;
end;

procedure TEDIT_SB.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tshtFlags_And_Settings then
  begin
      with HorzScrollBar do
      begin
        Range := 770;
        Position := 0;
        Visible := True;
      end;
  end
  else
  begin
      with HorzScrollBar do
      begin
        Range := 0;
        Position := 0;
        Visible := True;
      end;
  end;
end;

procedure TEDIT_SB.LoadSwipeClockAndMRC;
   procedure LoadValue (var aUser, aPass : string);
   var List : IisListOfValues;
       tmpStream: IisStream;
   begin
     tmpStream := DM_SERVICE_BUREAU.SB_STORAGE.GetBlobData('STORAGE_DATA');
     if assigned(tmpStream) then
     begin
       List := TisListOfValues.Create;
       List.ReadFromStream(tmpStream);

       if List.ValueExists(SBSTORAGE_TAG_SSOUSERNAME) then
           aUser := List.Value[SBSTORAGE_TAG_SSOUSERNAME];

       if List.ValueExists(SBSTORAGE_TAG_SSOPASSWORD) then
           aPass := Decryptstring(List.Value[SBSTORAGE_TAG_SSOPASSWORD]);
     end;
   end;
begin
  // Swipe clock : dgerasimov, Evolution
  // MRC :   peodemo , 2w!ag4
  ChangeSSOMode(1);
  try
    editAdminUsername.Text := '';
    editPassword.Text := '';
    edCompanyUserID.Text := '';
    edCompanyPassword.Text := '';
    FSCUserName := '';
    FSCPassword := '';
    FMRCUser := '';
    FMRCPass := '';

    if DM_SERVICE_BUREAU.SB_STORAGE.Locate('TAG', SBSTORAGE_TAG_SWIPECLOCK,[]) then
     if not DM_SERVICE_BUREAU.SB_STORAGE.FieldByName('Storage_data').IsNull then
     begin
       LoadValue(FSCUserName, FSCPassword);
       editAdminUsername.Text := FSCUserName;
       editPassword.Text := FSCPassword;
     end;

    if DM_SERVICE_BUREAU.SB_STORAGE.Locate('TAG', SBSTORAGE_TAG_MYRECRUITINGCENTER,[]) then
     if not DM_SERVICE_BUREAU.SB_STORAGE.FieldByName('Storage_data').IsNull then
     begin
       LoadValue(FMRCUser, FMRCPass);
       edCompanyUserID.Text := FMRCUser;
       edCompanyPassword.Text := FMRCPass;
     end;
  finally
    ChangeSSOMode(0);
  end;
end;

procedure TEDIT_SB.editAdminUsernameChange(Sender: TObject);
begin
  if (PageControl1.ActivePage = ExternalLogins) and
     wwdsDetail.Active and ((Sender as TComponent).Tag = 0) then
  begin
   (Sender as TComponent).Tag := 1;
    wwdsDetail.DataSet.edit;
   (Sender as TComponent).Tag := 0;
  end;
end;



procedure TEDIT_SB.ChangeSSOMode(avalue: integer);
begin
  editAdminUsername.Tag := avalue;
  editPassword.Tag := avalue;
  edCompanyUserID.Tag := avalue;
  edCompanyPassword.Tag := avalue;
end;

initialization
  RegisterClass(TEDIT_SB);

end.
