// Screens of "SB Admin"
unit scr_SB_Admin;

interface

uses
  SSBAdminScr,
  FSchedFrame,
  SPD_EDIT_SB,
  SPD_EDIT_SB_TEAM,
  SPD_EDIT_SB_TEAM_MEMBER,
  SPD_UTIL_REVERS_BANK_REGISTER_DEB_CRE,
  SPD_EDIT_SB_SEC_TEMPLATES,
  SPD_SB_SCHEDULER,
  SPD_EDIT_SB_QUEUE_PRIORITY,
  SPD_EDIT_SB_USER_MESSAGES,
  MainUnit;

implementation

end.
