// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit FSchedFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Spin, ExtCtrls, SDataStructure, ISBasicClasses, EvUIComponents,
  isUIEdit;

type
  TSchedFrame = class(TFrame)
    pFrame: TevPanel;
    pButtons: TevPanel;
    lPriority: TevLabel;
    eEMail: TevEdit;
    cbEMail: TevCheckBox;
    cbEmailSendRule: TevComboBox;
    sePriority: TevEdit;
    procedure cbEMailClick(Sender: TObject);
  private
    procedure CMChanged(var Message: TCMChanged); message CM_CHANGED;
  public
    DontChange: Boolean;
    Changing: Boolean;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}

{ TSchedFrame }

constructor TSchedFrame.Create(AOwner: TComponent);
begin
  DontChange := True;
  Changing := False;
  inherited;
end;

procedure TSchedFrame.cbEMailClick(Sender: TObject);
begin
  eEMail.Enabled := cbEMail.Enabled and cbEMail.Checked;
  cbEmailSendRule.Enabled := cbEMail.Enabled and cbEMail.Checked;

end;

procedure TSchedFrame.CMChanged(var Message: TCMChanged);
begin
  if not DontChange then
  begin
    Changing := True;
    try
      DM_SERVICE_BUREAU.SB_TASK.Edit;
    finally
      Changing := False;
    end;
  end;
end;

end.
