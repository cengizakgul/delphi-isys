inherited EDIT_SB_TEAM_MEMBER: TEDIT_SB_TEAM_MEMBER
  object dtxtTEAM_DESCRIPTION: TevDBText [0]
    Left = 48
    Top = 8
    Width = 249
    Height = 17
    DataField = 'TEAM_DESCRIPTION'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lablTeam: TevLabel [1]
    Left = 8
    Top = 8
    Width = 27
    Height = 13
    Caption = 'Team'
  end
  object pctlSB_TEAM_MEMBER: TevPageControl [2]
    Left = 8
    Top = 32
    Width = 585
    Height = 417
    HelpContext = 36503
    ActivePage = tshtTeam_Member_Detail
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgTeam: TevDBGrid
        Left = 0
        Top = 0
        Width = 281
        Height = 389
        Selected.Strings = (
          'TEAM_DESCRIPTION'#9'40'#9'Team Description')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object wwdgTeamMembers: TevDBGrid
        Left = 281
        Top = 0
        Width = 296
        Height = 389
        Selected.Strings = (
          'UserName'#9'40'#9'User Name'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
    end
    object tshtTeam_Member_Detail: TTabSheet
      Caption = 'Details'
      object Bevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 577
        Height = 389
        Align = alClient
      end
      object Label2: TevLabel
        Left = 8
        Top = 336
        Width = 57
        Height = 13
        Caption = '~User'
        FocusControl = wwDBLookupLast1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBLookupLast1: TevDBLookupCombo
        Left = 8
        Top = 352
        Width = 217
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'LAST_NAME'#9'30'#9'LAST_NAME'
          'FIRST_NAME'#9'20'#9'FIRST_NAME'#9'F'
          'MIDDLE_INITIAL'#9'1'#9'MIDDLE_INITIAL'#9'F')
        DataField = 'SB_USER_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_USER.SB_USER
        LookupField = 'SB_USER_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        AllowClearKey = True
      end
      object evDBGrid1: TevDBGrid
        Left = 8
        Top = 8
        Width = 345
        Height = 321
        Selected.Strings = (
          'UserName'#9'40'#9'User Name'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_TEAM.SB_TEAM
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_TEAM_MEMBERS.SB_TEAM_MEMBERS
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 256
    Top = 16
  end
end
