// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, DBTables, ComCtrls, Grids, SFrameEntry,
  ExtCtrls, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, Buttons, ISBasicClasses,  Wwdatsrc,
  isThreadManager, SyncObjs, Variants, isBaseClasses, EvConsts,
  isBasicUtils, EvMainboard, EvUtils, EvCommonInterfaces, EvUIUtils, EvUIComponents,
  EvDataset, LMDCustomButton, LMDButton, isUILMDButton, Menus, Wwdbigrd,
  Wwdbgrid;
const
  WM_THREAD_EXCEPTION = WM_USER + $1010;

type
  TfrmMaintenance = class(TFrameEntry)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    TabSheet3: TTabSheet;
    Memo1: TMemo;
    evplRebuildTmp: TevPanel;
    Label7: TLabel;
    edtThreads: TEdit;
    UpDown1: TUpDown;
    Bevel1: TBevel;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    edAddressTo: TEdit;
    evplDeletedDB: TevPanel;
    evPanel1: TevPanel;
    btnDoRebuild: TevBitBtn;
    btnSendTest: TevBitBtn;
    btnList: TevBitBtn;
    chbTaskQueue: TCheckBox;
    cbSendEmail: TCheckBox;
    pmDeletedClient: TevPopupMenu;
    miUndeleteClient: TMenuItem;
    dsDeletedClients: TEvBasicClientDataSet;
    evDataSource1: TevDataSource;
    dsDeletedClientsName: TStringField;
    dsDeletedClientsClientID: TIntegerField;
    grDeletedClients: TevDBGrid;
    procedure btnDoRebuildClick(Sender: TObject);
    procedure btnSendTestClick(Sender: TObject);
    procedure btnListClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure chbTaskQueueClick(Sender: TObject);
    procedure miUndeleteClientClick(Sender: TObject);
  private
    FCS: TCriticalSection;
    FClientsRebuilt: Integer;
    FClientsFailed: Integer;
    FErrorList: IisStringList;
    TimeStarted: TDateTime;
    procedure HANDLE_WM_THREAD_EXCEPTION(var M: TMessage); message WM_THREAD_EXCEPTION;
    procedure RebuildTempTables(const ClientNbr: String; const ThreadsNbr: Integer);
    procedure RebuildClient(const Params: TTaskParamList);
    procedure RunAsLocal(const ClientNbr: String);
    procedure RunAsTask(const ClientNbr: String);
  public
    procedure Activate; override;
    procedure Deactivate; override;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

implementation

uses isSettings;


{$R *.DFM}

procedure TfrmMaintenance.btnDoRebuildClick(Sender: TObject);
var
  ClientNbr: String;
begin
// This is a stupid message,
//  if EvMessage('Are you sure you want to rebuild temporary tables?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
//    Exit;

  if not EvDialog('Client Number Required', 'Client number(s) to rebuild (blank for a full rebuild):', ClientNbr) then
    Exit;

  if chbTaskQueue.Checked then
    RunAsTask(ClientNbr)
  else
    RunAsLocal(ClientNbr);
end;

procedure TfrmMaintenance.btnSendTestClick(Sender: TObject);
begin
  ctx_EMailer.SendQuickMail(edAddressTo.Text, 'Evolution Maintenance <EvolutionMaintenance>', 'Test message', 'Evolution Maintenance email test successful!');
end;

procedure TfrmMaintenance.btnListClick(Sender: TObject);
var
  AllClients: IisStringList;
  I: Integer;
  Q: IevQuery;
begin
  ctx_StartWait;
  dsDeletedClients.DisableControls;
  try
    dsDeletedClients.EmptyDataSet;
    dsDeletedClients.Open;

    AllClients := TisStringList.Create;
    AllClients.Text := ctx_DBAccess.GetClientsList;

    Q := TevQuery.Create('SELECT cl_nbr FROM tmp_cl');
    Q.Result.IndexFieldNames := 'cl_nbr';
    for I := 0 to AllClients.Count - 1 do
    begin
      if not Q.Result.FindKey([StrToInt(AllClients[I])]) then
        dsDeletedClients.AppendRecord(['CL_' + AllClients[I] + '.gdb', StrToInt(AllClients[I])]);
    end;

    dsDeletedClients.First;
  finally
    dsDeletedClients.EnableControls;
    ctx_EndWait;
  end;
end;

procedure TfrmMaintenance.Timer1Timer(Sender: TObject);
begin
  StatusBar1.Panels[2].Text := 'Time Elapsed: ' + FormatDateTime('hh:nn:ss', Now - TimeStarted);
end;

procedure TfrmMaintenance.HANDLE_WM_THREAD_EXCEPTION(var M: TMessage);
begin
  raise Exception(M.WParam);
end;

procedure TfrmMaintenance.Activate;
begin
  inherited;
  edAddressTo.Text := mb_AppSettings.GetValue('EvolMaintenance\Email To', Context.UserAccount.EMail);
  chbTaskQueue.Checked := True;
end;

procedure TfrmMaintenance.Deactivate;
begin
  inherited;
  mb_AppSettings.AsString['EvolMaintenance\Email To'] := edAddressTo.Text;
end;

constructor TfrmMaintenance.Create(AOwner: TComponent);
begin
  inherited;
  FErrorList := TisStringList.Create;
  FCS := TCriticalSection.Create;
end;

procedure TfrmMaintenance.RebuildTempTables(const ClientNbr: String; const ThreadsNbr: Integer);
var
  I, J, K: Integer;
  AllClients: TStringList;
  TM: IThreadManager;
  Threads: TStringList;
  s: String;
  TempClients: IisStringList;

begin
  Application.MainForm.Enabled := False;
  try
    FErrorList.Clear;
    FErrorList.Add('Temporary tables rebuild started ' + FormatDateTime('mm/dd/yyyy hh:nn:ss AM/PM', Now));

    TM := TThreadManager.Create('Rebuild Temp Tables TM');
    TM.Capacity := ThreadsNbr;

    FClientsRebuilt := 0;
    FClientsFailed := 0;
    AllClients := TStringList.Create;
    Threads := TStringList.Create;
    TempClients := TisStringList.Create;
    TempClients.Text := ctx_DBAccess.GetClientsList(True, True);
    try
      ctx_StartWait('Analyzing list of clients');
      try
        if ClientNbr = '' then
          AllClients.Text := ctx_DBAccess.GetClientsList(True)
        else if Pos('-', ClientNbr) = 0 then
          AllClients.CommaText := ClientNbr

        else
        begin
          AllClients.Text := ctx_DBAccess.GetClientsList;
          J := StrToInt(Copy(ClientNbr, 1, Pos('-', ClientNbr) - 1));
          K := StrToInt(Copy(ClientNbr, Pos('-', ClientNbr) + 1, Length(ClientNbr)));

          for I := AllClients.Count - 1 downto 0 do
            if not ((StrToInt(AllClients[I]) >= J) and (StrToInt(AllClients[I]) <= K)) then
              AllClients.Delete(i);
        end;
      finally
        ctx_EndWait;
      end;


      ctx_StartWait('Rebuilding ', AllClients.Count);
      try
        if ClientNbr = '' then
          ctx_DBAccess.BeginRebuildAllClients;

        for i := 0 to AllClients.Count - 1 do
        begin
          Threads.AddObject('CL_' + AllClients[i],
            Pointer(Context.RunParallelTask(RebuildClient, Self, MakeTaskParams([StrToInt(AllClients[i]), ClientNbr = '', TempClients.IndexOf(AllClients[i]) >= 0]), 'Rebuild Tmp Tables CL_' + AllClients[i], tpLower, TM)));

          repeat
            s := '';
            for j := Threads.Count - 1 downto 0 do
              if TM.TaskIsTerminated(TTaskID(Threads.Objects[j])) then
                Threads.Delete(j)
              else
                AddStrValue(s, Threads[j], ',');

            ctx_UpdateWait('Rebuilding temp tables'#13 +
              'Total: ' + IntToStr(AllClients.Count) +
              '  Working: ' + IntToStr(Threads.Count) +
              '  Finished: ' + IntToStr(i + 1 - Threads.Count) + #13#13 +
              'Currently rebuilding: ' + s, i + 1);

            Sleep(100);
          until Threads.Count < ThreadsNbr;
        end;

        for i := 0 to Threads.Count - 1 do
        begin
          TM.WaitForTaskEnd(TTaskID(Threads.Objects[i]));
        end;

        if ClientNbr = '' then
          ctx_DBAccess.EndRebuildAllClients;
      finally
        ctx_EndWait;
      end;

      FErrorList.Add('Temporary tables rebuild ended ' + FormatDateTime('mm/dd/yyyy hh:nn:ss AM/PM', Now));
    finally
      Threads.Free;
      AllClients.Free;
    end;
  finally
    Application.MainForm.Enabled := True;
  end;

// Plymouth TODO: if bAccessNewClient then
//     EvMessage('Please review the list of companies on the Messages tab.  These companies have not been added to your Access to New Clients group.  You will need to add them manually as applicable.');
end;

procedure TfrmMaintenance.RebuildClient(const Params: TTaskParamList);
var
  CL_NBR: Integer;
  E2: Exception;
  Err: String;
  AllClientsMode, tempClientsMode: Boolean;

begin
  try
    CL_NBR := Params[0];
    AllClientsMode := Params[1];
    tempClientsMode := Params[2];

    Err := '';
    try
      ctx_DBAccess.RebuildTemporaryTables(CL_NBR, AllClientsMode, tempClientsMode);
    except
      on E: Exception do
        Err := E.Message;
    end;

    FCS.Enter;
    try
      if Err = '' then
        Inc(FClientsRebuilt)
      else
      begin
        Inc(FClientsFailed);
        FErrorList.Add('CL_' + IntToStr(CL_NBR) + #13 + Err);
      end;
    finally
      FCS.Leave;
    end;

  except
    on E: Exception do
    begin
      E2 := Exception.CreateHelp(E.Message, E.HelpContext);
      PostMessage(Handle, WM_THREAD_EXCEPTION, Integer(Pointer(E2)), 0);
    end;
  end;
end;

destructor TfrmMaintenance.Destroy;
begin
  FCS.Free;
  inherited;
end;

procedure TfrmMaintenance.RunAsTask(const ClientNbr: String);
var
  T: IevRebuildTempTablesTask;
  s: String;
begin
  T := mb_TaskQueue.CreateTask(QUEUE_TASK_REBUILD_TMP_TABLES, True) as IevRebuildTempTablesTask;

  if edAddressTo.Text <> '' then
  begin
    T.NotificationEmail := edAddressTo.Text;
    T.SendEmailNotification := cbSendEmail.Checked;
  end;

  if ClientNbr = '' then
    T.FullRebuild := True
  else if Contains(ClientNbr, '-') then
  begin
    T.UseRange := True;
    s := ClientNbr;
    T.FromClient := StrToInt(Trim(GetNextStrValue(s, '-')));
    T.ToClient := StrToInt(Trim(s));
  end
  else
  begin
    T.UseNumbers := True;
    T.ClientNumbers := ClientNbr;
  end;

  ctx_EvolutionGUI.AddTaskQueue(T);
end;

procedure TfrmMaintenance.chbTaskQueueClick(Sender: TObject);
begin
  edtThreads.Enabled := not chbTaskQueue.Checked;
  UpDown1.Enabled := not chbTaskQueue.Checked;
  Label7.Enabled := not chbTaskQueue.Checked;
end;

procedure TfrmMaintenance.miUndeleteClientClick(Sender: TObject);
var
  cl: Integer;
begin
  if dsDeletedClients.RecordCount > 0 then
  begin
   cl := dsDeletedClients['ClientId'];
   ctx_DBAccess.UndeleteClientDB(cl);
   dsDeletedClients.Delete;

   if EvMessage('Client database CL_' + IntToStr(cl) + '.gdb has been undeleted. Do you want to rebuild temp tables for this client?', mtConfirmation, [mbYes, mbNo]) = mrYes then
     RunAsLocal(IntToStr(cl));
  end;
end;

procedure TfrmMaintenance.RunAsLocal(const ClientNbr: String);
var
  OldCursor: TCursor;
  s: String;
begin
  OldCursor:=Screen.Cursor;
  Screen.Cursor:=crHourGlass;
  TimeStarted := Now;
  Timer1.Enabled := True;
  StatusBar1.Panels[0].Text := 'Rebuilding...';
  StatusBar1.Panels[1].Text := '';
  Memo1.Lines.Clear;
  try
    RebuildTempTables(ClientNbr, StrToInt(edtThreads.Text));
  except
    Screen.Cursor := OldCursor;
    Timer1.Enabled := False;
    StatusBar1.Panels[0].Text := 'Idle...';
    StatusBar1.Panels[1].Text := '';
    raise;
  end;
  Screen.Cursor := OldCursor;
  Timer1.Enabled := False;
  Beep;
  if FClientsRebuilt - FClientsFailed = 1 then
    StatusBar1.Panels[0].Text := '1 client rebuilt'
  else
    StatusBar1.Panels[0].Text := IntToStr(FClientsRebuilt{ - FClientsFailed}) + ' clients rebuilt';
  if FClientsFailed = 1 then
    StatusBar1.Panels[1].Text := '1 client failed'
  else
    StatusBar1.Panels[1].Text := IntToStr(FClientsFailed) + ' clients failed';

  Memo1.Lines.Text := FErrorList.Text;
  if FClientsFailed > 0 then
    PageControl1.ActivePageIndex := 1;

  if cbSendEmail.Checked then
  begin
   if FClientsFailed = 0 then
     s := 'Evolution Maintenance table rebuild was successful'
   else
     s := 'Evolution Maintenance table rebuild had errors';

    ctx_EMailer.SendQuickMail(edAddressTo.Text, 'Evolution Maintenance <EvolutionMaintenance>', s, FErrorList.Text);
  end;
end;

initialization
  RegisterClass(TfrmMaintenance);

end.
