// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_QUEUE_PRIORITY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db, Wwdatsrc,  wwdblook, SDataStructure,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd, Wwdbgrid,
  SDDClasses, SDataDictbureau, ISBasicClasses, SFieldCodeValues,
  isBasicUtils, EvConsts, EvLegacy, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SB_QUEUE_PRIORITY = class(TFrameEntry)
    evDBGrid1: TevDBGrid;
    cbTask: TevDBComboBox;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    lcUser: TevDBLookupCombo;
    lcGroup: TevDBLookupCombo;
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

{ TEDIT_SB_QUEUE_PRIORITY }

procedure TEDIT_SB_QUEUE_PRIORITY.Activate;
begin
  inherited;
  cbTask.Items.Text := QueueTaskType_ComboChoices;
end;

function TEDIT_SB_QUEUE_PRIORITY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY;
end;

procedure TEDIT_SB_QUEUE_PRIORITY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_USER, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_GROUPS, SupportDataSets, '');
end;

procedure TEDIT_SB_QUEUE_PRIORITY.wwdsListDataChange(Sender: TObject; Field: TField);
begin
 if (TaskTypeToMethodName(QUEUE_TASK_RUN_REPORT) = DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.METHOD_NAME.AsString) or
    (TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_PAYMENTS) = DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.METHOD_NAME.AsString) or
    (TaskTypeToMethodName(QUEUE_TASK_PREPROCESS_QUARTER_END) = DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.METHOD_NAME.AsString) or
    (TaskTypeToMethodName(QUEUE_TASK_QEC) = DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.METHOD_NAME.AsString) or
    (TaskTypeToMethodName(QUEUE_TASK_REBUILD_TMP_TABLES) = DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.METHOD_NAME.AsString)
 then
   evDBGrid1.ColumnByName('Threads').ReadOnly := False
 else
 begin
   if (Field = DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.METHOD_NAME) and (DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.State in [dsEdit, dsInsert]) then
     DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY.THREADS.AsInteger := 0;
   evDBGrid1.ColumnByName('Threads').ReadOnly := True;
 end;
end;

initialization
  RegisterClass(TEDIT_SB_QUEUE_PRIORITY);

end.
