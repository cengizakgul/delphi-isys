inherited EDIT_SB: TEDIT_SB
  Width = 753
  Height = 572
  object evLabel5: TevLabel [0]
    Left = 416
    Top = 419
    Width = 98
    Height = 13
    Caption = 'Trust Impound Type '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel19: TevLabel [1]
    Left = 32
    Top = 336
    Width = 19
    Height = 13
    Caption = 'UID'
  end
  object PageControl1: TevPageControl [2]
    Left = 0
    Top = 0
    Width = 753
    Height = 572
    HelpContext = 11520
    ActivePage = tshtFlags_And_Settings
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object tshtGeneral_Info: TTabSheet
      Caption = 'General Info'
      object bevSB1: TEvBevel
        Left = 0
        Top = 0
        Width = 797
        Height = 572
        Align = alClient
      end
      object lablAddress1: TevLabel
        Left = 16
        Top = 46
        Width = 56
        Height = 16
        Caption = '~Address 1'
        FocusControl = wwdeAddress1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAddress2: TevLabel
        Left = 16
        Top = 86
        Width = 47
        Height = 13
        Caption = 'Address 2'
        FocusControl = wwdeCity
      end
      object lablCity: TevLabel
        Left = 16
        Top = 126
        Width = 26
        Height = 16
        Caption = '~City'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablName: TevLabel
        Left = 16
        Top = 6
        Width = 37
        Height = 16
        Caption = '~Name'
        Color = clBtnFace
        FocusControl = wwdeName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object lablE_Mail: TevLabel
        Left = 16
        Top = 166
        Width = 29
        Height = 13
        Caption = 'E-Mail'
        FocusControl = wwdeE_Mail
      end
      object lablEIN_Number: TevLabel
        Left = 360
        Top = 86
        Width = 67
        Height = 16
        Caption = '~EIN Number'
        FocusControl = wwdeEIN_Number
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablEFTPS_TIN_Nbr: TevLabel
        Left = 360
        Top = 126
        Width = 95
        Height = 13
        Caption = 'EFTPS TIN Number'
        FocusControl = wwdeEFTPS_TIN_Nbr
      end
      object lablFuture_Invoice_Nbr: TevLabel
        Left = 360
        Top = 166
        Width = 112
        Height = 13
        Caption = 'Current Invoice Number'
        FocusControl = wwdeFuture_Invoice_Nbr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablState: TevLabel
        Left = 176
        Top = 126
        Width = 34
        Height = 16
        Caption = '~State'
        FocusControl = dedtState
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablZip_Code: TevLabel
        Left = 216
        Top = 126
        Width = 52
        Height = 16
        Caption = '~Zip Code'
        FocusControl = wwdeZip_Code
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TevLabel
        Left = 360
        Top = 46
        Width = 26
        Height = 16
        Caption = '~Fax'
        FocusControl = wwdeFax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablPhone: TevLabel
        Left = 360
        Top = 6
        Width = 40
        Height = 16
        Caption = '~Phone'
        FocusControl = wwdePhone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 16
        Top = 206
        Width = 98
        Height = 13
        Caption = 'Service Bureau URL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 16
        Top = 246
        Width = 134
        Height = 13
        Caption = 'Remote superuser password'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablLogo: TevLabel
        Left = 552
        Top = 6
        Width = 24
        Height = 13
        Caption = 'Logo'
        FocusControl = dimgLogo
      end
      object labESSLogo: TevLabel
        Left = 552
        Top = 182
        Width = 101
        Height = 13
        Caption = 'ESS/Webclient Logo'
        FocusControl = dimgESSLogo
      end
      object wwdeAddress1: TevDBEdit
        Left = 16
        Top = 62
        Width = 305
        Height = 21
        HelpContext = 11502
        DataField = 'ADDRESS1'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeAddress2: TevDBEdit
        Left = 16
        Top = 102
        Width = 305
        Height = 21
        HelpContext = 11502
        DataField = 'ADDRESS2'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeCity: TevDBEdit
        Left = 16
        Top = 142
        Width = 153
        Height = 21
        HelpContext = 11502
        DataField = 'CITY'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeZip_Code: TevDBEdit
        Left = 216
        Top = 142
        Width = 105
        Height = 21
        HelpContext = 11502
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        Picture.PictureMask = '*5{#}[-*4#]'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeName: TevDBEdit
        Left = 16
        Top = 22
        Width = 305
        Height = 21
        HelpContext = 11502
        DataField = 'SB_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeE_Mail: TevDBEdit
        Left = 16
        Top = 182
        Width = 305
        Height = 21
        HelpContext = 11502
        DataField = 'E_MAIL_ADDRESS'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeEIN_Number: TevDBEdit
        Left = 360
        Top = 102
        Width = 153
        Height = 21
        HelpContext = 11502
        DataField = 'EIN_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object panlBelowAddressInfo: TevPanel
        Left = 8
        Top = 365
        Width = 705
        Height = 6
        TabOrder = 17
      end
      object wwdeEFTPS_TIN_Nbr: TevDBEdit
        Left = 360
        Top = 142
        Width = 153
        Height = 21
        HelpContext = 11502
        DataField = 'EFTPS_TIN_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 12
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeFuture_Invoice_Nbr: TevDBEdit
        Left = 360
        Top = 182
        Width = 153
        Height = 21
        HelpContext = 11502
        DataField = 'Number'
        DataSource = dsInvNumber
        TabOrder = 13
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtState: TevDBEdit
        Left = 176
        Top = 142
        Width = 33
        Height = 21
        HelpContext = 11502
        DataField = 'STATE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&,@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeFax: TevDBEdit
        Left = 360
        Top = 62
        Width = 153
        Height = 21
        HelpContext = 11502
        DataField = 'FAX'
        DataSource = wwdsDetail
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePhone: TevDBEdit
        Left = 360
        Top = 22
        Width = 153
        Height = 21
        DataField = 'PHONE'
        DataSource = wwdsDetail
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwDBEdit1: TevDBEdit
        Left = 16
        Top = 222
        Width = 305
        Height = 21
        HelpContext = 11508
        DataField = 'SB_URL'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evDBEdit1: TevDBEdit
        Left = 16
        Top = 262
        Width = 305
        Height = 21
        HelpContext = 11508
        DataField = 'PARENT_SB_MODEM_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dimgLogo: TevDBImage
        Left = 552
        Top = 22
        Width = 153
        Height = 108
        HelpContext = 21013
        DataField = 'SB_LOGO'
        DataSource = wwdsDetail
        Stretch = True
        TabOrder = 14
      end
      object bbtnLoad_Logo: TevBitBtn
        Left = 552
        Top = 138
        Width = 73
        Height = 25
        Caption = 'Load'
        TabOrder = 15
        OnClick = bbtnLoad_LogoClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
          DDDDDFFFFFFFFFFDDDDD800000000008DDDDF8888888888FDDDD3B0333333330
          8DDD8D8888888888FDDD3B803333333308DD8DD8888888888FDD384B03333333
          308D8D8F8888888888FD384BB033333333088D8FD8888888888F3B8448000000
          00008DD88F88888888883BBBB844F4BB3DDD8DDDDDDDDDDDDDDD3BBBB8FF4433
          DDDD8DDDDDFF88F8FDDD3BBB844444DDDDDD8DDDD88888FDDDDDD33384FF44DD
          DDDDD888D8FD88FDDDDDDDDDD84444DDDDDDDDDDDD8888FDDDDDDDDDDD844DDD
          DDDDDDDDDDD88FDDDDDDDDDDD84444DDDDDDDDDDDD8888FDDDDDDDDDD84444DD
          DDDDDDDDDD8888FDDDDDDDDDDD844DDDDDDDDDDDDDD88FDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object bbtnClear_Logo: TevBitBtn
        Left = 633
        Top = 138
        Width = 73
        Height = 25
        Caption = 'Clear'
        TabOrder = 16
        OnClick = bbtnClear_LogoClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD9
          999DDDDDDDDDDDD8888DDDDDDDDDDDDDD999DDDDDDDDDDDDDD8D48D484484484
          44148FD8F88F88F888F848D4848D48D484D48FD8F8FD8FD8F8F8D4DD4D4DD4D4
          DD4DD8FD8F8FD8F8FD8FD48D4D48D48D4D4DD8FF8F8FD8FD8F8F8444D4448D8D
          4449D888D888FDDD888DDDDDDDD4D84DD999DDDDDDD8FD8FDDD8DD9D08880111
          999DDD8DDDDDD8D8888D9DD0F777701118888DDDD8888D888DDDDD907F77770D
          888DDD8D8D8888DDDDDDDDD077F77770DDDDDDDD88D8888DDDDDDDDD077F7777
          0DDDDDDD888D8888DDDDDDDDD077F7780DDDDDDDD888D8888DDDDDDDDD077F88
          0DDDDDDDDD888D888DDDDDDDDDD077880DDDDDDDDDD888888DDD}
        NumGlyphs = 2
        Margin = 0
      end
      object dimgESSLogo: TevDBImage
        Left = 552
        Top = 198
        Width = 153
        Height = 108
        HelpContext = 21013
        DataField = 'ESS_LOGO'
        DataSource = wwdsDetail
        Stretch = True
        TabOrder = 18
      end
      object bbtnLoad_ESSLogo: TevBitBtn
        Left = 552
        Top = 314
        Width = 73
        Height = 25
        Caption = 'Load'
        TabOrder = 19
        OnClick = bbtnLoad_ESSLogoClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
          DDDDDFFFFFFFFFFDDDDD800000000008DDDDF8888888888FDDDD3B0333333330
          8DDD8D8888888888FDDD3B803333333308DD8DD8888888888FDD384B03333333
          308D8D8F8888888888FD384BB033333333088D8FD8888888888F3B8448000000
          00008DD88F88888888883BBBB844F4BB3DDD8DDDDDDDDDDDDDDD3BBBB8FF4433
          DDDD8DDDDDFF88F8FDDD3BBB844444DDDDDD8DDDD88888FDDDDDD33384FF44DD
          DDDDD888D8FD88FDDDDDDDDDD84444DDDDDDDDDDDD8888FDDDDDDDDDDD844DDD
          DDDDDDDDDDD88FDDDDDDDDDDD84444DDDDDDDDDDDD8888FDDDDDDDDDD84444DD
          DDDDDDDDDD8888FDDDDDDDDDDD844DDDDDDDDDDDDDD88FDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object bbtnClear_ESSLogo: TevBitBtn
        Left = 633
        Top = 314
        Width = 73
        Height = 25
        Caption = 'Clear'
        TabOrder = 20
        OnClick = bbtnClear_ESSLogoClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD9
          999DDDDDDDDDDDD8888DDDDDDDDDDDDDD999DDDDDDDDDDDDDD8D48D484484484
          44148FD8F88F88F888F848D4848D48D484D48FD8F8FD8FD8F8F8D4DD4D4DD4D4
          DD4DD8FD8F8FD8F8FD8FD48D4D48D48D4D4DD8FF8F8FD8FD8F8F8444D4448D8D
          4449D888D888FDDD888DDDDDDDD4D84DD999DDDDDDD8FD8FDDD8DD9D08880111
          999DDD8DDDDDD8D8888D9DD0F777701118888DDDD8888D888DDDDD907F77770D
          888DDD8D8D8888DDDDDDDDD077F77770DDDDDDDD88D8888DDDDDDDDD077F7777
          0DDDDDDD888D8888DDDDDDDDD077F7780DDDDDDDD888D8888DDDDDDDDD077F88
          0DDDDDDDDD888D888DDDDDDDDDD077880DDDDDDDDDD888888DDD}
        NumGlyphs = 2
        Margin = 0
      end
    end
    object tshtFlags_And_Settings: TTabSheet
      Caption = 'Flags && Settings'
      object bevSB2: TEvBevel
        Left = 0
        Top = 0
        Width = 745
        Height = 544
        Align = alClient
      end
      object lPayrollEMail: TevLabel
        Left = 16
        Top = 135
        Width = 119
        Height = 13
        Caption = 'Payroll Notification E-Mail'
        FocusControl = ePayrollEMail
      end
      object lablDefault_Check_Format: TevLabel
        Left = 16
        Top = 14
        Width = 112
        Height = 16
        Caption = '~Default Check Format'
        FocusControl = wwcbDefault_Check_Format
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablMICR_Horizontal_Adjustment: TevLabel
        Left = 16
        Top = 95
        Width = 129
        Height = 16
        Caption = '~MICR Vertical Adjustment'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAuto_Save_Minutes: TevLabel
        Left = 16
        Top = 500
        Width = 59
        Height = 16
        Caption = '~Auto Save'
        FocusControl = wwseAuto_Save_Minutes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblAutoSaveMins: TevLabel
        Left = 72
        Top = 523
        Width = 22
        Height = 13
        Caption = 'Mins'
      end
      object bevEDIT_SB2: TEvBevel
        Left = 291
        Top = 13
        Width = 6
        Height = 523
        Style = bsRaised
      end
      object Label3: TevLabel
        Left = 16
        Top = 216
        Width = 67
        Height = 13
        Caption = 'ACH Directory'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label4: TevLabel
        Left = 16
        Top = 176
        Width = 93
        Height = 13
        Caption = 'AR Export Directory'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TevLabel
        Left = 304
        Top = 164
        Width = 76
        Height = 13
        Caption = 'In Prenote Days'
      end
      object butnOpenARImportDirectory: TevSpeedButton
        Left = 256
        Top = 187
        Width = 29
        Height = 26
        HideHint = True
        AutoSize = False
        OnClick = butnOpenARImportDirectoryClick
        NumGlyphs = 2
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD7F7D7F7D7F
          7DDDDDDFDDDFDDDFDDDDDD80FD80FD80FDDDDD88FD88FD88FDDDDD087D087D08
          7DDDDDD8DDD8DDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentColor = False
        ShortCut = 0
      end
      object butnOpenACHDirectory: TevSpeedButton
        Left = 256
        Top = 227
        Width = 29
        Height = 26
        HideHint = True
        AutoSize = False
        Visible = False
        OnClick = butnOpenACHDirectoryClick
        NumGlyphs = 2
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD7F7D7F7D7F
          7DDDDDDFDDDFDDDFDDDDDD80FD80FD80FDDDDD88FD88FD88FDDDDD087D087D08
          7DDDDDD8DDD8DDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentColor = False
        ShortCut = 0
      end
      object evLabel3: TevLabel
        Left = 473
        Top = 19
        Width = 107
        Height = 16
        Caption = '~Trust Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel6: TevLabel
        Left = 473
        Top = 43
        Width = 101
        Height = 16
        Caption = '~Tax Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel4: TevLabel
        Left = 473
        Top = 67
        Width = 150
        Height = 16
        Caption = '~Direct Deposit Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel7: TevLabel
        Left = 473
        Top = 92
        Width = 110
        Height = 16
        Caption = '~Billing Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel8: TevLabel
        Left = 473
        Top = 116
        Width = 153
        Height = 16
        Caption = '~Workers Comp Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel9: TevLabel
        Left = 473
        Top = 206
        Width = 116
        Height = 16
        Caption = '~Total ACH File Amount'
        FocusControl = evDBEdit2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel10: TevLabel
        Left = 473
        Top = 231
        Width = 129
        Height = 16
        Caption = '~Total ACH File Limitations'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel11: TevLabel
        Left = 473
        Top = 256
        Width = 148
        Height = 16
        Caption = '~Tax Exception Impound Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel12: TevLabel
        Left = 304
        Top = 411
        Width = 188
        Height = 13
        Caption = 'Days Prior To Check Date for Backdate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1x: TevLabel
        Left = 473
        Top = 288
        Width = 94
        Height = 13
        Caption = 'Tax Payment Client '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablMisc_Check_Format: TevLabel
        Left = 16
        Top = 54
        Width = 100
        Height = 16
        Caption = '~Misc Check Format'
        FocusControl = wwcbMisc_Check_Format
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object drgpUse_Prenote: TevDBRadioGroup
        Left = 304
        Top = 103
        Width = 162
        Height = 57
        HelpContext = 11513
        Caption = '~Use PreNote'
        Columns = 2
        DataField = 'USE_PRENOTE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N')
        OnChange = drgpUse_PrenoteChange
      end
      object drgpEFTPS_Bank_Format: TevDBRadioGroup
        Left = 304
        Top = 14
        Width = 162
        Height = 83
        HelpContext = 11511
        Caption = '~EFTPS Bank Format'
        DataField = 'EFTPS_BANK_FORMAT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Anexysis'
          'Bank of America'
          'Batch Provider')
        ParentFont = False
        TabOrder = 9
        Values.Strings = (
          'C'
          'N'
          'B')
      end
      object ePayrollEMail: TevDBEdit
        Left = 16
        Top = 150
        Width = 269
        Height = 21
        HelpContext = 11504
        DataField = 'DEVELOPMENT_FTP_PASSWORD'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        UsePictureMask = False
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwcbDefault_Check_Format: TevDBComboBox
        Left = 16
        Top = 30
        Width = 269
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DEFAULT_CHECK_FORMAT'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'will have list when ready')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object wwseAuto_Save_Minutes: TevDBSpinEdit
        Left = 16
        Top = 516
        Width = 49
        Height = 21
        HelpContext = 11510
        Increment = 1.000000000000000000
        DataField = 'AUTO_SAVE_MINUTES'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
      end
      object wwdeACH_Directory: TevDBEdit
        Left = 16
        Top = 230
        Width = 237
        Height = 21
        HelpContext = 11522
        DataField = 'ACH_DIRECTORY'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeImport_Directory: TevDBEdit
        Left = 16
        Top = 190
        Width = 237
        Height = 21
        HelpContext = 11521
        DataField = 'AR_IMPORT_DIRECTORY'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwseDays_In_Prenote: TevDBSpinEdit
        Left = 304
        Top = 180
        Width = 49
        Height = 21
        HelpContext = 11510
        Increment = 1.000000000000000000
        MaxValue = 365.000000000000000000
        DataField = 'DAYS_IN_PRENOTE'
        DataSource = wwdsDetail
        TabOrder = 11
        UnboundDataType = wwDefault
      end
      object evDBSpinEdit1: TevDBSpinEdit
        Left = 16
        Top = 110
        Width = 49
        Height = 21
        Increment = 1.000000000000000000
        DataField = 'MICR_HORIZONTAL_ADJUSTMENT'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object cbNoOfflines: TevDBCheckBox
        Left = 305
        Top = 209
        Width = 97
        Height = 17
        Caption = 'Prohibit Offline'
        DataField = 'MICR_FONT'
        DataSource = wwdsDetail
        TabOrder = 12
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 304
        Top = 229
        Width = 162
        Height = 125
        HelpContext = 11513
        Caption = '~Bank Check'
        DataField = 'AR_EXPORT_FORMAT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
        OnChange = drgpUse_PrenoteChange
      end
      object gbSecurity: TevGroupBox
        Left = 16
        Top = 262
        Width = 269
        Height = 146
        Caption = 'Security'
        TabOrder = 6
        object evLabel2: TevLabel
          Left = 10
          Top = 74
          Width = 126
          Height = 13
          Caption = 'Minimum Password Length'
        end
        object lablUser_Password_Duration_Days: TevLabel
          Left = 10
          Top = 20
          Width = 98
          Height = 16
          Caption = '~Password Duration'
          FocusControl = wwseUser_Password_Duration_Days
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablUSER_PASSWORD_DURATION_IN_DAYS_DAYS: TevLabel
          Left = 216
          Top = 20
          Width = 24
          Height = 13
          Caption = 'Days'
        end
        object evLabel15: TevLabel
          Left = 10
          Top = 47
          Width = 99
          Height = 16
          Caption = '~Enhanced Security'
          FocusControl = wwseUser_Password_Duration_Days
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBCheckBox1: TevDBCheckBox
          Left = 8
          Top = 120
          Width = 163
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Send Screenshot With Error'
          DataField = 'ERROR_SCREEN'
          DataSource = wwdsDetail
          TabOrder = 4
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object evDBSpinEdit2: TevDBSpinEdit
          Left = 158
          Top = 71
          Width = 49
          Height = 21
          HelpContext = 11510
          Increment = 1.000000000000000000
          MaxValue = 365.000000000000000000
          MinValue = 1.000000000000000000
          DataField = 'PSWD_MIN_LENGTH'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
        end
        object evDBCheckBox2: TevDBCheckBox
          Left = 8
          Top = 99
          Width = 163
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Enforce Mixed Password'
          DataField = 'PSWD_FORCE_MIXED'
          DataSource = wwdsDetail
          TabOrder = 3
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object wwseUser_Password_Duration_Days: TevDBSpinEdit
          Left = 158
          Top = 15
          Width = 49
          Height = 21
          HelpContext = 11509
          Increment = 1.000000000000000000
          DataField = 'USER_PASSWORD_DURATION_IN_DAYS'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object evDBComboBox8: TevDBComboBox
          Left = 158
          Top = 43
          Width = 97
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = False
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'EE_LOGIN_TYPE'
          DataSource = wwdsDetail
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 1
          UnboundDataType = wwDefault
        end
      end
      object evDBCheckBox3: TevDBCheckBox
        Left = 305
        Top = 364
        Width = 200
        Height = 17
        Caption = 'Block Negative Checks From Trust'
        DataField = 'IMPOUND_TRUST_MONIES_AS_RECEIV'
        DataSource = wwdsDetail
        TabOrder = 14
        ValueChecked = 'N'
        ValueUnchecked = 'Y'
      end
      object evDBCheckBox4: TevDBCheckBox
        Left = 305
        Top = 388
        Width = 200
        Height = 17
        Caption = 'TOA Descriptions on Check'
        DataField = 'PAY_TAX_FROM_PAYABLES'
        DataSource = wwdsDetail
        TabOrder = 15
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object drgpDOBforW2: TevDBRadioGroup
        Left = 304
        Top = 456
        Width = 269
        Height = 37
        HelpContext = 11513
        Caption = '~Enforce mandatory DOB for W2 employees'
        Columns = 2
        DataField = 'ENFORCE_EE_DOB_DEFAULT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 17
        OnChange = drgpUse_PrenoteChange
      end
      object evDBComboBox1: TevDBComboBox
        Left = 642
        Top = 14
        Width = 90
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TRUST_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 19
        UnboundDataType = wwDefault
      end
      object evDBComboBox2: TevDBComboBox
        Left = 642
        Top = 38
        Width = 91
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 20
        UnboundDataType = wwDefault
      end
      object evDBComboBox3: TevDBComboBox
        Left = 642
        Top = 62
        Width = 91
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DD_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 21
        UnboundDataType = wwDefault
      end
      object evDBComboBox4: TevDBComboBox
        Left = 642
        Top = 87
        Width = 91
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'BILLING_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 22
        UnboundDataType = wwDefault
      end
      object evDBComboBox5: TevDBComboBox
        Left = 642
        Top = 111
        Width = 91
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WC_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 23
        UnboundDataType = wwDefault
      end
      object evDBRadioGroup2: TevDBRadioGroup
        Left = 304
        Top = 500
        Width = 269
        Height = 37
        HelpContext = 11513
        Caption = '~Mark Liabilities as Paid Default'
        Columns = 2
        DataField = 'MARK_LIABS_PAID_DEFAULT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 18
        Values.Strings = (
          'Y'
          'N')
      end
      object wwseDaysPriorToCheckDate: TevDBSpinEdit
        Left = 304
        Top = 428
        Width = 49
        Height = 21
        HelpContext = 11510
        Increment = 1.000000000000000000
        MaxValue = 365.000000000000000000
        DataField = 'DAYS_PRIOR_TO_CHECK_DATE'
        DataSource = wwdsDetail
        TabOrder = 16
        UnboundDataType = wwDefault
      end
      object evDBEdit2: TevDBEdit
        Left = 643
        Top = 198
        Width = 91
        Height = 21
        DataField = 'SB_MAX_ACH_FILE_TOTAL'
        DataSource = wwdsDetail
        TabOrder = 25
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evDBComboBox6: TevDBComboBox
        Left = 643
        Top = 223
        Width = 91
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SB_ACH_FILE_LIMITATIONS'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 26
        UnboundDataType = wwDefault
      end
      object evDBComboBox7: TevDBComboBox
        Left = 643
        Top = 248
        Width = 91
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SB_EXCEPTION_PAYMENT_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 27
        UnboundDataType = wwDefault
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 473
        Top = 303
        Width = 259
        Height = 21
        HelpContext = 7010
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
          'NAME'#9'40'#9'Name'#9#9
          'CL_NBR'#9'10'#9'Internal CL#'#9'F')
        DataField = 'SB_CL_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_TMP_CL.TMP_CL
        LookupField = 'CL_NBR'
        Options = [loTitles]
        Style = csDropDownList
        TabOrder = 28
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwcbMisc_Check_Format: TevDBComboBox
        Left = 16
        Top = 70
        Width = 269
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MISC_CHECK_FORM'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'will have list when ready')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object evDBRadioGroup3: TevDBRadioGroup
        Left = 475
        Top = 142
        Width = 258
        Height = 42
        HelpContext = 11513
        Caption = '~Block DD Impound Type changes'
        Columns = 2
        DataField = 'MICR_FONT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 24
        Values.Strings = (
          'Y'
          'N')
        OnChange = drgpUse_PrenoteChange
      end
      object gbSessionTimeouts: TevGroupBox
        Left = 16
        Top = 413
        Width = 269
        Height = 79
        TabOrder = 7
        object evLabel18: TevLabel
          Left = 10
          Top = 21
          Width = 143
          Height = 13
          Caption = 'Session Management Timeout'
        end
        object evLabel20: TevLabel
          Left = 10
          Top = 50
          Width = 149
          Height = 13
          Caption = 'Session Management Lock Out'
        end
        object evLabel21: TevLabel
          Left = 230
          Top = 21
          Width = 22
          Height = 13
          Caption = 'Mins'
        end
        object evLabel22: TevLabel
          Left = 230
          Top = 49
          Width = 22
          Height = 13
          Caption = 'Mins'
        end
        object edSessionTimeout: TevDBSpinEdit
          Left = 174
          Top = 18
          Width = 49
          Height = 21
          HelpContext = 11510
          Increment = 1.000000000000000000
          MaxValue = 1000000.000000000000000000
          DataField = 'SESSION_TIMEOUT'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object edSessionLockout: TevDBSpinEdit
          Left = 174
          Top = 46
          Width = 49
          Height = 21
          HelpContext = 11510
          Increment = 1.000000000000000000
          MaxValue = 1000000.000000000000000000
          DataField = 'SESSION_LOCKOUT'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
        end
      end
    end
    object tshtCover_Notes: TTabSheet
      Caption = 'Cover Letter Notes'
      object bevSB3: TEvBevel
        Left = 0
        Top = 0
        Width = 785
        Height = 583
        Align = alClient
      end
      object dmemCover_Notes: TEvDBMemo
        Left = 16
        Top = 14
        Width = 569
        Height = 393
        HelpContext = 11520
        DataField = 'COVER_LETTER_NOTES'
        DataSource = wwdsDetail
        TabOrder = 0
      end
    end
    object tshtTaxCover_Notes: TTabSheet
      Caption = 'Tax Cover Letter  Notes'
      ImageIndex = 3
      object Bevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 904
        Height = 493
        Align = alClient
      end
      object dmemTaxCover_Notes: TEvDBMemo
        Left = 16
        Top = 14
        Width = 569
        Height = 393
        HelpContext = 11520
        DataField = 'TAX_COVER_LETTER_NOTES'
        DataSource = wwdsDetail
        TabOrder = 0
      end
    end
    object tshtInvoice_Notes: TTabSheet
      Caption = 'Invoice Notes'
      ImageIndex = 4
      object Bevel2: TEvBevel
        Left = 0
        Top = 0
        Width = 785
        Height = 583
        Align = alClient
      end
      object dmemInvoice_Notes: TEvDBMemo
        Left = 16
        Top = 14
        Width = 569
        Height = 393
        HelpContext = 11520
        DataField = 'INVOICE_NOTES'
        DataSource = wwdsDetail
        TabOrder = 0
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'VMR Confidentiality Notice'
      ImageIndex = 5
      object EvBevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 785
        Height = 583
        Align = alClient
      end
      object dmemVMR_Conf_Notes: TEvDBMemo
        Left = 16
        Top = 14
        Width = 569
        Height = 393
        HelpContext = 11520
        DataField = 'VMR_CONFIDENCIAL_NOTES'
        DataSource = wwdsDetail
        TabOrder = 0
      end
    end
    object tsTermsOfUse: TTabSheet
      Caption = 'Terms of Use'
      ImageIndex = 6
      object EvBevel2: TEvBevel
        Left = 0
        Top = 0
        Width = 755
        Height = 550
        Align = alClient
      end
      object evLabel13: TevLabel
        Left = 12
        Top = 11
        Width = 102
        Height = 13
        Caption = 'Terms of Use for ESS'
      end
      object evLabel14: TevLabel
        Left = 12
        Top = 249
        Width = 130
        Height = 13
        Caption = 'Terms of Use for WebClient'
      end
      object EvDBMemo1: TEvDBMemo
        Left = 12
        Top = 28
        Width = 569
        Height = 200
        HelpContext = 11520
        DataField = 'ESS_TERMS_OF_USE'
        DataSource = wwdsDetail
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object EvDBMemo2: TEvDBMemo
        Left = 12
        Top = 267
        Width = 569
        Height = 200
        HelpContext = 11520
        DataField = 'WC_TERMS_OF_USE'
        DataSource = wwdsDetail
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object ExternalLogins: TTabSheet
      Caption = 'External Logins'
      ImageIndex = 7
      object evGroupBox1: TevGroupBox
        Left = 16
        Top = 8
        Width = 265
        Height = 97
        Caption = ' Swipe Clock '
        TabOrder = 0
        object evLabel16: TevLabel
          Left = 16
          Top = 28
          Width = 83
          Height = 13
          Caption = 'Admin User name'
        end
        object evLabel17: TevLabel
          Left = 16
          Top = 63
          Width = 46
          Height = 13
          Caption = 'Password'
        end
        object editAdminUsername: TevEdit
          Left = 112
          Top = 26
          Width = 121
          Height = 21
          TabOrder = 0
          OnChange = editAdminUsernameChange
        end
        object editPassword: TevEdit
          Left = 112
          Top = 60
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
          OnChange = editAdminUsernameChange
        end
      end
      object evGroupBox2: TevGroupBox
        Left = 16
        Top = 119
        Width = 268
        Height = 90
        Caption = 'My Recruiting Center '
        TabOrder = 1
        object lbCompanyUserID: TevLabel
          Left = 11
          Top = 27
          Width = 68
          Height = 13
          Caption = 'Admin User ID'
        end
        object lbCompanyPassword: TevLabel
          Left = 11
          Top = 56
          Width = 78
          Height = 13
          Caption = 'Admin Password'
        end
        object edCompanyUserID: TevEdit
          Left = 128
          Top = 23
          Width = 121
          Height = 21
          TabOrder = 0
          OnChange = editAdminUsernameChange
        end
        object edCompanyPassword: TevEdit
          Left = 128
          Top = 52
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
          OnChange = editAdminUsernameChange
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 128
    Top = 0
  end
  inherited wwdsDetail: TevDataSource
    OnStateChange = wwdsDetailStateChange
    MasterDataSource = nil
    Left = 176
    Top = 0
  end
  inherited wwdsList: TevDataSource
    Top = 34
  end
  object dlgOpen: TOpenDialog
    Left = 220
  end
  object cdsInvNumber: TevClientDataSet
    Left = 520
    Top = 452
    object cdsInvNumberNumber: TIntegerField
      FieldName = 'Number'
    end
  end
  object dsInvNumber: TevDataSource
    DataSet = cdsInvNumber
    Left = 488
    Top = 453
  end
  object odlgGetBitmap: TOpenDialog
    Filter = 'Windows BitMap (*.bmp)|*.bmp'
    Options = [ofReadOnly, ofHideReadOnly]
    Left = 272
    Top = 3
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 616
    Top = 400
  end
end
