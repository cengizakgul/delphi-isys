// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SB_SCHEDULER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db, Wwdatsrc,  SDataStructure, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, evDataset,
  ExtCtrls, SPackageEntry, EvTaskViewerFrm, SFieldCodeValues, DateUtils,
  wwdbdatetimepicker, Spin, DBCtrls, SSecurityInterface, isVCLBugFix,
  EvUtils, Buttons, FSchedFrame, SDDClasses, EvContext, EvTypes, EvConsts,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, EvLegacy,
  EvCommonInterfaces, EvMainboard, isBaseClasses, EvStreamUtils, isBasicUtils,
  isSchedule, EvUIUtils, EvUIComponents, EvClientDataSet, isUIwwDBComboBox,
  isUIwwDBEdit;

type
  TScheduler = class(TFrameEntry)
    pcMain: TevPageControl;
    tsList: TTabSheet;
    tsParams: TTabSheet;
    tsSchedule: TTabSheet;
    gTasks: TevDBGrid;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    pTask: TevPanel;
    lTask: TevLabel;
    cbTask: TevDBComboBox;
    gRuns: TevDBGrid;
    Splitter: TevSplitter;
    lCaption: TevLabel;
    lCapt: TevLabel;
    SchedFrame: TSchedFrame;
    Panel1: TPanel;
    lSchedDesc: TevDBText;
    EvBevel1: TEvBevel;
    lScheduleTask: TevLabel;
    lStartTime: TevLabel;
    lStartDate: TevLabel;
    gbScheduleWeekly: TevGroupBox;
    lWeeklyWeeks: TevLabel;
    lWeeklyEvery: TevLabel;
    seWeekly: TevSpinEdit;
    cbMon: TevCheckBox;
    cbTue: TevCheckBox;
    cbWed: TevCheckBox;
    cbThu: TevCheckBox;
    cbFri: TevCheckBox;
    cbSat: TevCheckBox;
    cbSun: TevCheckBox;
    gbScheduleDaily: TevGroupBox;
    lDailyEvery: TevLabel;
    lDailyDays: TevLabel;
    seDaily: TevSpinEdit;
    cbScheduleTask: TevComboBox;
    dtStartTime: TevDateTimePicker;
    cbEndDate: TevCheckBox;
    dtStartDate: TevDateTimePicker;
    dtEndDate: TevDateTimePicker;
    pRepeatTask: TevPanel;
    lEvery: TevLabel;
    lUntil: TevLabel;
    lHours: TevLabel;
    lMinutes: TevLabel;
    seRepeatInterval: TevSpinEdit;
    cbRepeatUnits: TevComboBox;
    rbTime: TevRadioButton;
    dtRepeatEndTime: TevDateTimePicker;
    rbDuration: TevRadioButton;
    seHours: TevSpinEdit;
    seMinutes: TevSpinEdit;
    cbRepeatTask: TevCheckBox;
    cbRunIfMissed: TevCheckBox;
    gbScheduleMonthly: TevGroupBox;
    lMonthlyMonths1: TevLabel;
    lMonthlyMonths2: TevLabel;
    rbMonthlyDay: TevRadioButton;
    rbMonthlyThe: TevRadioButton;
    seMonthly: TevComboBox;
    cbMonthlyFirst: TevComboBox;
    cbDayOfWeek: TevComboBox;
    cbApr: TevCheckBox;
    cbMar: TevCheckBox;
    cbFeb: TevCheckBox;
    cbJan: TevCheckBox;
    cbMay: TevCheckBox;
    cbJun: TevCheckBox;
    cbJul: TevCheckBox;
    cbAug: TevCheckBox;
    cbSep: TevCheckBox;
    cbOct: TevCheckBox;
    cbNov: TevCheckBox;
    cbDec: TevCheckBox;
    lbSchedule: TListBox;
    lLastRun: TLabel;
    Label1: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    evLabel1: TevLabel;
    edtNotes: TevDBEdit;
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure cbTaskChange(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure cbEndDateClick(Sender: TObject);
    procedure cbRepeatTaskClick(Sender: TObject);
    procedure rbTimeClick(Sender: TObject);
    procedure rbMonthlyDayClick(Sender: TObject);
    procedure seHoursChange(Sender: TObject);
    procedure dtRepeatEndTimeChange(Sender: TObject);
    procedure cbScheduleTaskClick(Sender: TObject);
    procedure dtStartTimeChange(Sender: TObject);
    procedure cbJanClick(Sender: TObject);
    procedure seWeeklyChange(Sender: TObject);
    procedure seMonthlyChange(Sender: TObject);
    procedure gTasksRowChanged(Sender: TObject);
  private
    ParamFrame: TTaskViewer;
    FCurrentTask: IevTask;
    FSynchSchedControls: Boolean;
    CTask: string;
    FTaskRuns: IevDataSet;
    procedure SetupScheduler;
    function  EncodeScheduler: string;
    procedure ReadCommonInfo(var s: string);
    function  WriteCommonInfo: string;
    procedure DoOnChangeScheduleControls;
    procedure SyncSchedulePreview;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure Activate; override;
    procedure Deactivate; override;
    function  GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

{$R *.DFM}

uses isDataSet;

{ TScheduler }

procedure TScheduler.Activate;

  function TaskType_ComboChoices: String;

    function RemoveTaskWord(const ATaskName: String): String;
    begin
      Result := Copy(ATaskName, 6, Length(ATaskName) - 5);
    end;

  begin
    Result := RemoveTaskWord(QUEUE_TASK_PREPROCESS_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_PREPROCESS_PAYROLL) + #13 +
              RemoveTaskWord(QUEUE_TASK_PROCESS_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_PAYROLL) + #13 +
              RemoveTaskWord(QUEUE_TASK_CREATE_PAYROLL) + #9 + TaskTypeToMethodName(QUEUE_TASK_CREATE_PAYROLL) + #13 +
              RemoveTaskWord(QUEUE_TASK_PREPROCESS_QUARTER_END) + #9 + TaskTypeToMethodName(QUEUE_TASK_PREPROCESS_QUARTER_END) + #13 +
              RemoveTaskWord(QUEUE_TASK_PROCESS_TAX_RETURNS) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_RETURNS) + #13 +
              RemoveTaskWord(QUEUE_TASK_PROCESS_TAX_PAYMENTS) + #9 + TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_PAYMENTS) + #13 +
              RemoveTaskWord(QUEUE_TASK_RUN_REPORT) + #9 + TaskTypeToMethodName(QUEUE_TASK_RUN_REPORT) + #13 +
              RemoveTaskWord(QUEUE_TASK_REBUILD_TMP_TABLES) + #9 + TaskTypeToMethodName(QUEUE_TASK_REBUILD_TMP_TABLES) + #13 +
              RemoveTaskWord(QUEUE_TASK_PAYROLL_ACH) + #9 + TaskTypeToMethodName(QUEUE_TASK_PAYROLL_ACH) + #13 +
              RemoveTaskWord(QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS) + #9 + TaskTypeToMethodName(QUEUE_TASK_BENEFITS_ENROLLMENT_NOTIFICATIONS) + #13 +
              RemoveTaskWord(QUEUE_TASK_ACA_STATUS_UPDATE) + #9 + TaskTypeToMethodName(QUEUE_TASK_ACA_STATUS_UPDATE);
  end;

var
  i: integer;
begin
  inherited;
  cbTask.Items.Text := TaskType_ComboChoices;
  for i := cbTask.Items.Count - 1 downto 0 do
  begin
    if Pos(TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_PAYMENTS), cbTask.Items[i]) > 0 then
      cbTask.Items.Delete(i)
    else
    if Pos(TaskTypeToMethodName(QUEUE_TASK_PAYROLL_ACH), cbTask.Items[i]) > 0 then
      cbTask.Items.Delete(i);
  end;
  SchedFrame.DontChange := False;
end;

function TScheduler.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_TASK;
end;

function TScheduler.GetInsertControl: TWinControl;
begin
  Result := cbTask;
end;

procedure TScheduler.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
var
  Q: IevQuery;
begin
  inherited;

  wwdsDetail.DataSet := nil;

  Q := TevQuery.Create('SbTaskRuns');
  Q.Params.AddValue('date_b', IncMonth(SysDate, -18));
  FTaskRuns := Q.Result;
  FTaskRuns.vclDataSet.AddIndex('SORT', 'sb_task_nbr;run_date', [ixDescending], 'run_date');
  FTaskRuns.vclDataSet.IndexName := 'SORT';

  wwdsDetail.DataSet := FTaskRuns.vclDataSet;

  AddDSWithCheck(DM_SYSTEM_MISC.SY_QUEUE_PRIORITY, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_QUEUE_PRIORITY, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, SupportDataSets, '');
end;

procedure TScheduler.wwdsMasterDataChange(Sender: TObject; Field: TField);
var
  t: IevTask;
  S: IevDualStream;
begin
  if (csLoading in ComponentState) or SchedFrame.Changing then
    Exit;

  inherited;

  if Assigned(wwdsMaster.DataSet) and wwdsMaster.DataSet.Active and (pcMain.ActivePage = tsParams) then
  begin
    ctx_StartWait;
    try
      if (wwdsMaster.DataSet.State in [dsEdit, dsInsert]) and
         (Field = wwdsMaster.DataSet.FieldByName('Description')) then
      begin
        t := mb_TaskQueue.CreateTask(MethodNameToTaskType(wwdsMaster.DataSet.FieldByName('Description').AsString), True);

        if not Assigned(FCurrentTask) or
          (MethodNameToTaskType(wwdsMaster.DataSet.FieldByName('Description').AsString) <> FCurrentTask.TaskType) then
        begin
          lCapt.Caption := '';
          s := (t as IisInterfacedObject).AsStream;
          s.Position := 0;
          TBlobField(wwdsMaster.DataSet.FieldByName('Task')).LoadFromStream(s.RealStream);
          t := nil;
        end;
        wwdsMaster.DataSet.FieldByName('SB_USER_NBR').AsInteger := Context.UserAccount.InternalNbr;
      end;

      if (not Assigned(Field) or (Field = wwdsMaster.DataSet.FieldByName('Task'))) and
         (wwdsMaster.DataSet.FieldByName('Task').AsString <> CTask) then
      begin
        FreeAndNil(ParamFrame);
        FCurrentTask := nil;
        CTask := '';
        if not wwdsMaster.DataSet.FieldByName('Task').IsNull then
        begin
          FCurrentTask := TaskFromBlobField(MethodNameToTaskType(wwdsMaster.DataSet.FieldByName('Description').AsString),
              TBlobField(wwdsMaster.DataSet.FieldByName('Task')));

           // In case if administrator renamed evolution domain
           if not AnsiSameText(FCurrentTask.Domain, ctx_DomainInfo.DomainName) then
           begin
             SchedFrame.Changing := True;
             try
               wwdsMaster.DataSet.Edit;
             finally
               SchedFrame.Changing := False;
             end;
             (FCurrentTask as IevTaskExt).SetAccountInfo(ctx_DomainInfo.DomainName, FCurrentTask.User);
             EvMessage('Some internal information of this task was updated. You need to save these changes.',
               mtWarning, [mbOK]);
           end;

          CTask := wwdsMaster.DataSet.FieldByName('Task').AsString;
          SchedFrame.DontChange := True;
          try
            ParamFrame := TTaskViewer.Create(Self);
            ParamFrame.Parent := SchedFrame.pFrame;
            ParamFrame.Align := alClient;
            ParamFrame.ShowTaskParams(FCurrentTask);

            lCapt.Caption := FCurrentTask.Caption;
            SchedFrame.sePriority.Text := IntToStr(FCurrentTask.Priority);
            SchedFrame.cbEMail.Checked := FCurrentTask.SendEmailNotification;
            SchedFrame.eEMail.Text := FCurrentTask.NotificationEmail;
            SchedFrame.cbEmailSendRule.ItemIndex := Ord(FCurrentTask.EmailSendRule);

          finally
            SchedFrame.DontChange := False;
          end;
        end
        else
        begin
          SchedFrame.DontChange := True;
          try
            lCapt.Caption := '';
            SchedFrame.sePriority.Text := '5';
            SchedFrame.cbEMail.Checked := False;
            SchedFrame.eEMail.Text := '';
            SchedFrame.cbEmailSendRule.ItemIndex := 0;

          finally
            SchedFrame.DontChange := False;
          end;
        end;
      end;
    finally
      ctx_EndWait;
    end;
  end
  else if not Assigned(Field) and
          not (wwdsMaster.DataSet.State in [dsEdit, dsInsert]) then
  begin
    FreeAndNil(ParamFrame);
    FCurrentTask := nil;
    CTask := '';
  end;

  if Assigned(wwdsMaster.DataSet) and
     wwdsMaster.DataSet.Active and
     (wwdsMaster.DataSet.State = dsBrowse) and
     (not Assigned(Field) or
      (Field = wwdsMaster.DataSet.FieldByName('Schedule'))) then
  begin
    if pcMain.ActivePage = tsSchedule then
      pcMain.SetFocus;
    SetupScheduler;
  end;
end;

procedure TScheduler.Deactivate;
begin
  inherited;
  FreeAndNil(ParamFrame);
  FCurrentTask := nil;
  CTask := '';
end;

procedure TScheduler.cbTaskChange(Sender: TObject);
begin
  inherited;
  if cbTask.Focused and
     (wwdsMaster.DataSet.State in [dsInsert]) and
     (wwdsMaster.DataSet.FieldByName('Description').AsString <> cbTask.Value) then
    wwdsMaster.DataSet.FieldByName('Notes').AsString := cbTask.Text;

  if cbTask.Focused and
     (wwdsMaster.DataSet.State in [dsEdit, dsInsert]) and
     (wwdsMaster.DataSet.FieldByName('Description').AsString <> cbTask.Value) then
  begin
    wwdsMaster.DataSet.FieldByName('Description').AsString := cbTask.Value;
    if Assigned(ParamFrame) then
      PutFocus(ParamFrame);
  end;

  SchedFrame.Enabled := cbTask.Text <> '';
end;

procedure TScheduler.pcMainChange(Sender: TObject);
begin
  inherited;
  if (pcMain.ActivePage <> tsList) and not Assigned(FCurrentTask) then
    wwdsMasterDataChange(Self, nil);
end;

procedure TScheduler.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  S: IevDualStream;
  pv: Integer;
  sn: Boolean;
  ne: string;
  esr : integer;
  i : integer;

  // delete and post one record
  procedure DeleteCurrentRecord;
  begin
    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      DM_SERVICE_BUREAU.SB_TASK.Delete;
      ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_TASK]);
      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.CancelDataSets([DM_SERVICE_BUREAU.SB_TASK]);
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  end;

begin
  inherited;
  if Kind = NavOK then
  begin
    wwdsMaster.DataSet['SCHEDULE'] := EncodeScheduler;

    if Assigned(FCurrentTask) then
    begin
      pv := StrToIntDef(SchedFrame.sePriority.Text, 0);
      sn := SchedFrame.cbEMail.Checked;
      ne := SchedFrame.eEMail.Text;
      esr := SchedFrame.cbEmailSendRule.ItemIndex;
      ctx_StartWait;
      try
        if Assigned(ParamFrame) then
          ParamFrame.ApplyUpdates;

        FCurrentTask.Priority := pv;
        FCurrentTask.SendEmailNotification := sn;
        FCurrentTask.NotificationEmail := ne;
        FCurrentTask.EmailSendRule := TevEmailSendRule(esr);

        lCapt.Caption := FCurrentTask.Caption;

        S := (FCurrentTask as IisInterfacedObject).AsStream;
        CTask := wwdsMaster.DataSet.FieldByName('Task').AsString;
        S.Position := 0;
        TBlobField(wwdsMaster.DataSet.FieldByName('Task')).LoadFromStream(S.RealStream);
      finally
        ctx_EndWait;
      end;
    end;
  end

  else if Kind = NavDelete then
  begin
    if gTasks.SelectedList.Count > 0 then
    begin
      if EvMessage(IntToStr(gTasks.SelectedList.Count) + ' rows are selected. Are you sure?', mtConfirmation, mbOKCancel) <> mrOK then
        AbortEx
    end
    else
      if EvMessage('Are you sure?', mtConfirmation, mbOKCancel) <> mrOK then
        AbortEx;

    DM_SERVICE_BUREAU.SB_TASK.DisableControls;
    try
      if gTasks.SelectedList.Count > 0 then
      begin
        for i := 0 to gTasks.SelectedList.Count-1 do
        begin
          DM_SERVICE_BUREAU.SB_TASK.GotoBookmark(gTasks.SelectedList.items[i]);
          DeleteCurrentRecord;
        end;
      end
      else
         DeleteCurrentRecord;
      Handled := True;
    finally
      DM_SERVICE_BUREAU.SB_TASK.EnableControls;
    end;
  end;
  if (Kind in [NavCancel, NavAbort]) and Assigned(ParamFrame) then
       ParamFrame.CancelUpdates;
end;

procedure TScheduler.cbEndDateClick(Sender: TObject);
begin
  inherited;
  dtEndDate.Enabled := cbEndDate.Checked;
  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.cbRepeatTaskClick(Sender: TObject);
begin
  inherited;
  lEvery.Enabled := cbRepeatTask.Checked;
  lUntil.Enabled := cbRepeatTask.Checked;
  seRepeatInterval.Enabled := cbRepeatTask.Checked;
  cbRepeatUnits.Enabled := cbRepeatTask.Checked;
  rbTime.Enabled := cbRepeatTask.Checked;
  rbDuration.Enabled := cbRepeatTask.Checked;
  dtRepeatEndTime.Enabled := cbRepeatTask.Checked and rbTime.Checked;
  seHours.Enabled := cbRepeatTask.Checked and rbDuration.Checked;
  seMinutes.Enabled := cbRepeatTask.Checked and rbDuration.Checked;
  lHours.Enabled := cbRepeatTask.Checked and rbDuration.Checked;
  lMinutes.Enabled := cbRepeatTask.Checked and rbDuration.Checked;

  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.rbTimeClick(Sender: TObject);
begin
  inherited;
  dtRepeatEndTime.Enabled := cbRepeatTask.Checked and rbTime.Checked;
  seHours.Enabled := cbRepeatTask.Checked and rbDuration.Checked;
  seMinutes.Enabled := cbRepeatTask.Checked and rbDuration.Checked;
  lHours.Enabled := cbRepeatTask.Checked and rbDuration.Checked;
  lMinutes.Enabled := cbRepeatTask.Checked and rbDuration.Checked;

  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.rbMonthlyDayClick(Sender: TObject);
begin
  inherited;
  seMonthly.Enabled := rbMonthlyDay.Checked;
  lMonthlyMonths1.Enabled := rbMonthlyDay.Checked;
  cbMonthlyFirst.Enabled := rbMonthlyThe.Checked;
  cbDayOfWeek.Enabled := rbMonthlyThe.Checked;
  lMonthlyMonths2.Enabled := rbMonthlyThe.Checked;

  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.SetupScheduler;
var
  s, t, d: string;
  b: Word;
begin
  FSynchSchedControls := True;
  try
    s := wwdsMaster.DataSet.FieldByName('SCHEDULE').AsString;
    t := GetNextStrValue(s, ' ');

    cbScheduleTask.ItemIndex := 0;
    dtStartTime.Time := Time + 1/24; // 1 hour;
    dtStartDate.Date := Date;
    cbEndDate.Checked := False;
    dtEndDate.Date := Date;
    cbRepeatTask.Checked := False;
    seRepeatInterval.Value := 10;
    cbRepeatUnits.ItemIndex := 0;
    rbDuration.Checked := True;
    dtRepeatEndTime.Time := dtStartTime.Time + 1/24; // 1 hour
    seHours.Value := 1;
    seMinutes.Value := 0;
    seDaily.Value := 1;
    seWeekly.Value := 1;
    cbMon.Checked := True;
    cbTue.Checked := False;
    cbWed.Checked := False;
    cbThu.Checked := False;
    cbFri.Checked := False;
    cbSat.Checked := False;
    cbSun.Checked := False;
    rbMonthlyDay.Checked := True;
    seMonthly.ItemIndex := 0;
    cbMonthlyFirst.ItemIndex := 0;
    cbDayOfWeek.ItemIndex := 0;
    cbJan.Checked := True;
    cbFeb.Checked := True;
    cbMar.Checked := True;
    cbApr.Checked := True;
    cbMay.Checked := True;
    cbJun.Checked := True;
    cbJul.Checked := True;
    cbAug.Checked := True;
    cbSep.Checked := True;
    cbOct.Checked := True;
    cbNov.Checked := True;
    cbDec.Checked := True;
    cbRunIfMissed.Checked := True;

    if t = 'D' then
    begin
      cbScheduleTask.ItemIndex := 0;
      ReadCommonInfo(s);
      cbEndDate.Checked := GetNextStrValue(s, ' ') = 'Y';
      if cbEndDate.Checked then
        dtEndDate.Date := StrToInt(GetNextStrValue(s, ' '));
      seDaily.Value := StrToInt(GetNextStrValue(s, ' '));
    end
    else if t = 'W' then
    begin
      cbScheduleTask.ItemIndex := 1;
      ReadCommonInfo(s);
      cbEndDate.Checked := GetNextStrValue(s, ' ') = 'Y';
      if cbEndDate.Checked then
        dtEndDate.Date := StrToInt(GetNextStrValue(s, ' '));
      seWeekly.Value := StrToInt(GetNextStrValue(s, ' '));
      b := StrToInt(GetNextStrValue(s, ' '));
      cbMon.Checked := Boolean(b and $01);
      cbTue.Checked := Boolean(b and $02);
      cbWed.Checked := Boolean(b and $04);
      cbThu.Checked := Boolean(b and $08);
      cbFri.Checked := Boolean(b and $10);
      cbSat.Checked := Boolean(b and $20);
      cbSun.Checked := Boolean(b and $40);
    end
    else if t = 'M' then
    begin
      cbScheduleTask.ItemIndex := 2;
      ReadCommonInfo(s);
      cbEndDate.Checked := GetNextStrValue(s, ' ') = 'Y';
      if cbEndDate.Checked then
        dtEndDate.Date := StrToInt(GetNextStrValue(s, ' '));
      rbMonthlyDay.Checked := GetNextStrValue(s, ' ') = 'D';
      rbMonthlyThe.Checked := not rbMonthlyDay.Checked;
      if rbMonthlyDay.Checked then
      begin
        d := GetNextStrValue(s, ' ');
        if d = 'L' then
          seMonthly.ItemIndex := Pred(seMonthly.Items.Count)
        else
          seMonthly.ItemIndex := seMonthly.Items.IndexOf(d);
      end
      else
      begin
        cbMonthlyFirst.ItemIndex := StrToInt(GetNextStrValue(s, ' '));
        cbDayOfWeek.ItemIndex := StrToInt(GetNextStrValue(s, ' '));
      end;
      b := StrToInt(GetNextStrValue(s, ' '));
      cbJan.Checked := (b and $0001) <> 0;
      cbFeb.Checked := (b and $0002) <> 0;
      cbMar.Checked := (b and $0004) <> 0;
      cbApr.Checked := (b and $0008) <> 0;
      cbMay.Checked := (b and $0010) <> 0;
      cbJun.Checked := (b and $0020) <> 0;
      cbJul.Checked := (b and $0040) <> 0;
      cbAug.Checked := (b and $0080) <> 0;
      cbSep.Checked := (b and $0100) <> 0;
      cbOct.Checked := (b and $0200) <> 0;
      cbNov.Checked := (b and $0400) <> 0;
      cbDec.Checked := (b and $0800) <> 0;
    end
    else if t = 'O' then
    begin
      cbScheduleTask.ItemIndex := 3;
      ReadCommonInfo(s);
    end
    else if t = 'S' then
      cbScheduleTask.ItemIndex := 4;
    cbScheduleTaskClick(Self);
  finally
    FSynchSchedControls := False;
  end;

  SyncSchedulePreview;
end;

procedure TScheduler.seHoursChange(Sender: TObject);
begin
  inherited;

  if seHours.Value > seHours.MaxValue then
    seHours.Value := seHours.MaxValue;

  if rbDuration.Checked then
    dtRepeatEndTime.Time := dtStartTime.Time + (60 * seHours.Value + seMinutes.Value) / 1440;

  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.dtRepeatEndTimeChange(Sender: TObject);
var
  d: TDateTime;
begin
  inherited;
  if rbTime.Checked then
  begin
    if dtRepeatEndTime.Time >= dtStartTime.Time then
      d := dtRepeatEndTime.Time - dtStartTime.Time
    else
      d := 1 + dtRepeatEndTime.Time - dtStartTime.Time;
    seHours.Value := Round(d * 1440) div 60;
    seMinutes.Value := Round(d * 1440) mod 60;
  end;

  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

function TScheduler.EncodeScheduler: string;
begin
  case cbScheduleTask.ItemIndex of
  0:
  begin
    Result := 'D ' + WriteCommonInfo;
    if cbEndDate.Checked then
      Result := Result + 'Y ' + IntToStr(Trunc(dtEndDate.Date)) + ' '
    else
      Result := Result + 'N ';
    Result := Result + IntToStr(seDaily.Value);
  end;
  1:
  begin
    Result := 'W ' + WriteCommonInfo;
    if cbEndDate.Checked then
      Result := Result + 'Y ' + IntToStr(Trunc(dtEndDate.Date)) + ' '
    else
      Result := Result + 'N ';
    Result := Result + IntToStr(seWeekly.Value) + ' ' +
               IntToStr(Integer(cbMon.Checked) or
                        Integer(cbTue.Checked) shl 1 or
                        Integer(cbWed.Checked) shl 2 or
                        Integer(cbThu.Checked) shl 3 or
                        Integer(cbFri.Checked) shl 4 or
                        Integer(cbSat.Checked) shl 5 or
                        Integer(cbSun.Checked) shl 6);
  end;
  2:
  begin
    Result := 'M ' + WriteCommonInfo;
    if cbEndDate.Checked then
      Result := Result + 'Y ' + IntToStr(Trunc(dtEndDate.Date)) + ' '
    else
      Result := Result + 'N ';
    if rbMonthlyDay.Checked then
      if seMonthly.ItemIndex = Pred(seMonthly.Items.Count) then
        Result := Result + 'D L '
      else
        Result := Result + 'D ' + seMonthly.Text + ' '
    else
      Result := Result + 'T ' + IntToStr(cbMonthlyFirst.ItemIndex) + ' ' + IntToStr(cbDayOfWeek.ItemIndex) + ' ';
    Result := Result + IntToStr(Integer(cbJan.Checked) or
                                Integer(cbFeb.Checked) shl 1 or
                                Integer(cbMar.Checked) shl 2 or
                                Integer(cbApr.Checked) shl 3 or
                                Integer(cbMay.Checked) shl 4 or
                                Integer(cbJun.Checked) shl 5 or
                                Integer(cbJul.Checked) shl 6 or
                                Integer(cbAug.Checked) shl 7 or
                                Integer(cbSep.Checked) shl 8 or
                                Integer(cbOct.Checked) shl 9 or
                                Integer(cbNov.Checked) shl 10 or
                                Integer(cbdec.Checked) shl 11);
  end;
  3:
    Result := 'O ' + WriteCommonInfo;
  4:
    Result := 'S';
  else
    Assert(False);
  end;
end;

procedure TScheduler.cbScheduleTaskClick(Sender: TObject);
begin
  inherited;
  gbScheduleDaily.Visible := cbScheduleTask.ItemIndex = 0;
  gbScheduleWeekly.Visible := cbScheduleTask.ItemIndex = 1;
  gbScheduleMonthly.Visible := cbScheduleTask.ItemIndex = 2;
  cbEndDate.Enabled := cbScheduleTask.ItemIndex <= 2;
  cbEndDate.Checked := cbEndDate.Checked and cbEndDate.Enabled;
  cbRepeatTask.Enabled := cbScheduleTask.ItemIndex <= 3;
  cbRepeatTask.Checked := cbRepeatTask.Checked and cbRepeatTask.Enabled;
  dtStartDate.Enabled := cbScheduleTask.ItemIndex <= 3;
  lStartDate.Enabled := cbScheduleTask.ItemIndex <= 3;
  dtStartTime.Enabled := cbScheduleTask.ItemIndex <= 3;
  lStartTime.Enabled := cbScheduleTask.ItemIndex <= 3;
  cbRunIfMissed.Enabled := cbScheduleTask.ItemIndex <= 3;

  if Sender = cbScheduleTask then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.dtStartTimeChange(Sender: TObject);
begin
  inherited;
  DoOnChangeScheduleControls;
end;

procedure TScheduler.ReadCommonInfo(var s: string);
begin
  dtStartTime.Time := StrToFloat(GetNextStrValue(s, ' '));
  dtStartDate.Date := StrToInt(GetNextStrValue(s, ' '));
  cbRepeatTask.Checked := GetNextStrValue(s, ' ') = 'Y';
  if cbRepeatTask.Checked then
  begin
    seRepeatInterval.Value := StrToInt(GetNextStrValue(s, ' '));
    cbRepeatUnits.ItemIndex := StrToInt(GetNextStrValue(s, ' '));
    rbDuration.Checked := GetNextStrValue(s, ' ') = 'D';
    rbTime.Checked := not rbDuration.Checked;
    if rbTime.Checked then
    begin
      dtRepeatEndTime.Time := StrToFloat(GetNextStrValue(s, ' '));
      dtRepeatEndTimeChange(Self);
    end
    else
    begin
      seHours.Value := StrToInt(GetNextStrValue(s, ' '));
      seMinutes.Value := StrToInt(GetNextStrValue(s, ' '));
      seHoursChange(Self);
    end;
  end
  else
    dtRepeatEndTime.Time := dtStartTime.Time + 1/24; // 1 hour
  cbRunIfMissed.Checked := GetNextStrValue(s, ' ') = 'Y';
end;

function TScheduler.WriteCommonInfo: string;
begin
  Result := FloatToStr(Frac(dtStartTime.Time)) + ' ' + IntToStr(Trunc(dtStartDate.Date)) + ' ';
  if cbRepeatTask.Checked then
  begin
    Result := Result + 'Y ' + IntToStr(seRepeatInterval.Value) + ' ' + IntToStr(cbRepeatUnits.ItemIndex) + ' ';
    if rbDuration.Checked then
      Result := Result + 'D ' + IntToStr(seHours.Value) + ' ' + IntToStr(seMinutes.Value) + ' '
    else
      Result := Result + 'T ' + FloatToStr(Frac(dtRepeatEndTime.Time)) + ' ';
  end
  else
    Result := Result + 'N ';
  if cbRunIfMissed.Checked then
    Result := Result + 'Y '
  else
    Result := Result + 'N ';
end;

procedure TScheduler.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavInsert then
    SetupScheduler;
end;

procedure TScheduler.cbJanClick(Sender: TObject);
begin
  inherited;
  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

procedure TScheduler.seWeeklyChange(Sender: TObject);
begin
  inherited;
  if TWinControl(Sender).Focused then
    DoOnChangeScheduleControls;
end;

function TScheduler.GetDataSetConditions(sName: string): string;
begin
  if sName = 'SB_TASK' then
    if ctx_AccountRights.Functions.GetState('ALLOW_ADMIN_QUEUE') = stEnabled then
      Result := ''
    else
      Result := 'SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr)
  else
    Result := inherited GetDataSetConditions(sName);
end;


procedure TScheduler.DoOnChangeScheduleControls;
begin
  if wwdsMaster.DataSet.State = dsBrowse then
    wwdsMaster.DataSet.Edit;
  SyncSchedulePreview;
end;

procedure TScheduler.SyncSchedulePreview;
var
  s: String;
  Event: IisScheduleEvent;
  aSchedule: TRunSchedule;
  i: Integer;
  EventsToShow: Integer;
begin
  if FSynchSchedControls then
    Exit;

  s := EncodeScheduler;

  // Show scheduling dates
  Event := OldFormatToScheduleEvent(s);

  if DM_SERVICE_BUREAU.SB_TASK.LAST_RUN.IsNull then
    lLastRun.Caption := ''
  else
  begin
    Event.LastRunDate := DM_SERVICE_BUREAU.SB_TASK.LAST_RUN.AsDateTime;
    lLastRun.Caption := 'Last Run: ' + FormatDateTime('mm/dd/yy  hh:nn AM/PM (dddd)', Event.LastRunDate);
  end;

  aSchedule := Event.GetRunScheduleFor(SysTime, IncYear(SysTime));
  EventsToShow := Length(aSchedule);
  if EventsToShow > 200 then
    EventsToShow := 200;

  lbSchedule.Items.BeginUpdate;
  try
    lbSchedule.Clear;
    lbSchedule.Items.Capacity := EventsToShow;
    for i := Low(aSchedule) to Low(aSchedule) + EventsToShow - 1  do
      lbSchedule.Items.Add(FormatDateTime('mm/dd/yy  hh:nn AM/PM (ddd)', aSchedule[i]));

    if EventsToShow < Length(aSchedule) then
      lbSchedule.Items.Add('...');
  finally
    lbSchedule.Items.EndUpdate;
  end;
end;

procedure TScheduler.seMonthlyChange(Sender: TObject);
begin
  inherited;
  DoOnChangeScheduleControls;
end;

procedure TScheduler.gTasksRowChanged(Sender: TObject);
begin
  inherited;
  FTaskRuns.SetRange([DM_SERVICE_BUREAU.SB_TASK.SB_TASK_NBR.AsInteger], [DM_SERVICE_BUREAU.SB_TASK.SB_TASK_NBR.AsInteger]);
  FTaskRuns.First;
end;

initialization
  RegisterClass(TScheduler);

end.
