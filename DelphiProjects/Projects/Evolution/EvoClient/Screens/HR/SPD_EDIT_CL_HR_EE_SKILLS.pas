// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_EE_SKILLS;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, Mask, wwdbedit,  DBActns, ActnList, EvUIComponents, EvClientDataSet,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, ISBasicClasses, SDDClasses, SDataDictclient,
  ImgList, SDataDicttemp, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIwwDBEdit, isUIDBMemo;

type
  TEDIT_CL_HR_EE_SKILLS = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    isUIFashionPanel1: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    EvDBMemo1: TEvDBMemo;
    evDBEdit1: TevDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  end;

implementation

uses SPD_EDIT_Open_BASE, SPD_EDIT_EE_HR_BASE;

{$R *.DFM}

{ TEDIT_CL_HR_EE_SKILLS }


function TEDIT_CL_HR_EE_SKILLS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_PERSON_SKILLS;
end;

function TEDIT_CL_HR_EE_SKILLS.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

initialization
  RegisterClass( TEDIT_CL_HR_EE_SKILLS );

end.
