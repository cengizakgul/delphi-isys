// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SelectEEs;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, DB, Wwdatsrc,  Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, ISBasicClasses, EvUIComponents;

type
  TSelEEs = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    dtBegin: TevDateTimePicker;
    dtEnd: TevDateTimePicker;
    evLabel1: TevLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
