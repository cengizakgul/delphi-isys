// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_APPLICANT_HANDICAPS;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_APPLICANT_DESCR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, EvUIComponents, EvClientDataSet, SDataDictsystem, DBActns,
  ActnList, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_HR_APPLICANT_HANDICAPS = class(TEDIT_CO_HR_APPLICANT_DESCR_BASE)
    evDBLookupCombo1: TevDBLookupCombo;
    lblHandicap: TevLabel;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DM_SYSTEM_HR: TDM_SYSTEM_HR;
    fpHandicapDetail: TisUIFashionPanel;
    sbApplicantBrowse: TScrollBox;
    evSplitter1: TevSplitter;
    pnlLeftBorder: TevPanel;
    pnlRightBorder: TevPanel;
    pnlTopBorder: TevPanel;
    pnlBottomBorder: TevPanel;
    pnlBrowseLeft: TevPanel;
    fpBrowseLeft: TisUIFashionPanel;
    pnlFPLeftBody: TevPanel;
    pnlBrowseRight: TevPanel;
    fpBrowseRight: TisUIFashionPanel;
    pnlFPRightBody: TevPanel;
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}
{ TEDIT_CL_HR_APPLICANT_HANDICAPS }

function TEDIT_CL_HR_APPLICANT_HANDICAPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_PERSON_HANDICAPS;
end;

function TEDIT_CL_HR_APPLICANT_HANDICAPS.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

procedure TEDIT_CL_HR_APPLICANT_HANDICAPS.PageControl1Change(
  Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePageIndex <> 1 then exit;


end;

procedure TEDIT_CL_HR_APPLICANT_HANDICAPS.Activate;
begin
  inherited;

end;

initialization
  RegisterClass( TEDIT_CL_HR_APPLICANT_HANDICAPS );


end.
