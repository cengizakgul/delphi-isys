// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_APPLICANT_BASE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvUtils,
  wwdblook,  DBActns, ActnList, SPackageEntry,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_APPLICANT_BASE = class(TEDIT_CO_BASE)
    pnlBrowse: TevPanel;
    evDBGrid1: TevDBGrid;
    wwdsSubMaster2: TevDataSource;
    pnlEEChoice: TevPanel;
    dblkupEE_NBR: TevDBLookupCombo;
    dbtxtname: TevDBLookupCombo;
    EEClone: TevClientDataSet;
    EECloneCO_HR_APPLICANT_NBR: TIntegerField;
    EECloneSSNLookup: TStringField;
    EECloneName_Calculate: TStringField;
    ActionList: TevActionList;
    EENext: TDataSetNext;
    EEPrior: TDataSetPrior;
    FocusNbr: TAction;
    FocusName: TAction;
    Action1: TAction;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure evDBGrid1DblClick(Sender: TObject);
    procedure evDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EENextExecute(Sender: TObject);
    procedure EEPriorExecute(Sender: TObject);
    procedure dblkupEE_NBRChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FocusNameExecute(Sender: TObject);
    procedure FocusNbrExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure dblkupEE_NBRCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure EECloneFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure evDBGrid1FilterChange(Sender: TObject; DataSet: TDataSet;
      NewFilter: String);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
  private
    EEFilter: string;
    procedure CreateEEFilter;
  protected
    procedure AfterDataSetsReopen; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure SetReadOnly(Value: Boolean); override;
  public
    { Public declarations }
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

var
  CurrentFilter: string = '0=0 AND not APPLICANT_STATUS like ''%H%''';

{$R *.DFM}

{ TEDIT_CO_HR_APPLICANT_BASE }

procedure TEDIT_CO_HR_APPLICANT_BASE.Activate;
var
  s: string;
begin
  DM_COMPANY.CO_HR_APPLICANT.FilterOptions := DM_COMPANY.CO_HR_APPLICANT.FilterOptions + [foCaseInsensitive];
  DM_COMPANY.CO_HR_APPLICANT.UserFilter := CurrentFilter;
  DM_COMPANY.CO_HR_APPLICANT.UserFiltered := True;
  CreateEEFilter;

  inherited;

  s := DM_COMPANY.CO_HR_APPLICANT.IndexName;
  try
    DM_COMPANY.CO_HR_APPLICANT.IndexName := '';
    EEClone.CloneCursor(DM_COMPANY.CO_HR_APPLICANT, False, True);
  finally
    if s <> '' then
      DM_COMPANY.CO_HR_APPLICANT.IndexName := s;
  end;
  dblkupEE_NBR.Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
  dbtxtname.Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;

end;

procedure TEDIT_CO_HR_APPLICANT_BASE.CreateEEFilter;
var
  r: Integer;
  i: Integer;
begin
  r := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsInteger;
  DM_COMPANY.CO_HR_APPLICANT.DisableControls;
  DM_COMPANY.CO_HR_APPLICANT.LookupsEnabled := False;
  try
    DM_COMPANY.CO_HR_APPLICANT.First;
    EEFilter := ';';
    i := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').Index;
    while not DM_COMPANY.CO_HR_APPLICANT.Eof do
    begin
      EEFilter := EEFilter + DM_COMPANY.CO_HR_APPLICANT.Fields[i].AsString + ';';
      DM_COMPANY.CO_HR_APPLICANT.Next;
    end;
  finally
    DM_COMPANY.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', r, []);
    DM_COMPANY.CO_HR_APPLICANT.LookupsEnabled := True;
    DM_COMPANY.CO_HR_APPLICANT.EnableControls;
  end;
end;

function TEDIT_CO_HR_APPLICANT_BASE.GetDataSetConditions(
  sName: string): string;
begin
  if (sName = 'CO_BRANCH') or
     (sName = 'CO_DEPARTMENT') or
     (sName = 'CO_TEAM')   then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);

end;

procedure TEDIT_CO_HR_APPLICANT_BASE.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_PERSON, aDS);
  AddDS(DM_COMPANY.CO_HR_APPLICANT, aDS );
  inherited;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_COMPANY.CO_HR_APPLICANT.Active then
  begin
    if dblkupEE_NBR.LookupValue <> DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString then
      dblkupEE_NBR.LookupValue := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
    if dbtxtname.LookupValue <> DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString then
      dbtxtname.LookupValue := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
    EENext.Enabled := DM_COMPANY.CO_HR_APPLICANT.RecNo < DM_COMPANY.CO_HR_APPLICANT.RecordCount;
    EEPrior.Enabled := DM_COMPANY.CO_HR_APPLICANT.RecNo > 1;
  end;

  if (DM_COMPANY.CO_HR_APPLICANT.State = dsBrowse) and
     (TevClientDataSet(DM_CLIENT.CL_PERSON).State = dsBrowse) then
    if (DM_COMPANY.CO_HR_APPLICANT.RecordCount = 0) and
       ((DM_CLIENT.CL_PERSON.RecordCount <> 0) or
         DM_CLIENT.CL_PERSON.Filtered) then
    begin
  //    ODS( 'wwdsDetailDataChange: set filter');
      DM_CLIENT.CL_PERSON.Filter := 'CL_PERSON_NBR = 0';
      DM_CLIENT.CL_PERSON.Filtered := True;
    end
    else
    begin
//      ODS( 'wwdsDetailDataChange: locate');
      DM_CLIENT.CL_PERSON.Filtered := False;
      DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_COMPANY.CO_HR_APPLICANT['CL_PERSON_NBR'], []);
    end;

end;

procedure TEDIT_CO_HR_APPLICANT_BASE.evDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.RecordCount > 0 then
    PageControl1.SelectNextPage(True);
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.evDBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (wwdsDetail.DataSet.RecordCount > 0) then
  begin
    PageControl1.SelectNextPage(True);
    Key := 0;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.EENextExecute(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
  DM_COMPANY.CO_HR_APPLICANT.Next;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.EEPriorExecute(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
  DM_COMPANY.CO_HR_APPLICANT.Prior;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.Deactivate;
begin
  inherited;
  EEClone.Close;
  DM_COMPANY.CO_HR_APPLICANT.UserFiltered := False;
  DM_COMPANY.CO_HR_APPLICANT.UserFilter := '';
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.dblkupEE_NBRChange(Sender: TObject);
begin
  inherited;
  if TevDBLookupCombo(Sender).LookupTable.Active and
     TevDBLookupCombo(Sender).Focused and
     (Trim(TevDBLookupCombo(Sender).Text) <> '') then
    TevDBLookupCombo(Sender).LookupTable.Locate(TevDBLookupCombo(Sender).LookupField, TevDBLookupCombo(Sender).LookupValue, []);
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.BitBtn1Click(Sender: TObject);
var
  s, s1: string;
  b: Boolean;
begin
  s1 := DM_COMPANY.CO_HR_APPLICANT.UserFilter;
  b := DM_COMPANY.CO_HR_APPLICANT.UserFiltered;
  EEClone.Close;
  DM_CLIENT.CL_PERSON.Close;
  inherited;
  DM_COMPANY.CO_HR_APPLICANT.UserFilter := s1;
  DM_COMPANY.CO_HR_APPLICANT.UserFiltered := b;
  CreateEEFilter;
  s := DM_COMPANY.CO_HR_APPLICANT.IndexName;
  try
    DM_COMPANY.CO_HR_APPLICANT.IndexName := '';
    EEClone.CloneCursor(DM_COMPANY.CO_HR_APPLICANT, False, True);
    EEClone.UserFilter := s1;
    EEClone.UserFiltered := b;
  finally
    if s <> '' then
      DM_COMPANY.CO_HR_APPLICANT.IndexName := s;
  end;
  dblkupEE_NBR.Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
  dbtxtname.Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.FocusNameExecute(Sender: TObject);
begin
  inherited;
  if dbtxtname.CanFocus then
    dbtxtname.SetFocus;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.FocusNbrExecute(Sender: TObject);
begin
  inherited;
  if dblkupEE_NBR.CanFocus then
    dblkupEE_NBR.SetFocus;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.Action1Execute(Sender: TObject);
begin
  inherited;
  PutFocus(Self);
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.SetReadOnly(Value: Boolean);
begin
  inherited;
  dblkupEE_NBR.SecurityRO := False;
  dbtxtname.SecurityRO := False;
  butnNext.SecurityRO := False;
  butnPrior.SecurityRO := False;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.dblkupEE_NBRCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if not Modified then
    TevDBLookupCombo(Sender).LookupValue := TevDBLookupCombo(Sender).OldLookup
  else if TevDBLookupCombo(Sender).Focused and (Trim(TevDBLookupCombo(Sender).Text) <> '') and
          (TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField] <> DM_COMPANY.CO_HR_APPLICANT['CO_HR_APPLICANT_NBR']) then
    DM_COMPANY.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField], []);
  inherited;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.EECloneFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := Pos(';' + DataSet.FieldByName('CO_HR_APPLICANT_NBR').AsString + ';', EEFilter) > 0;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.evDBGrid1FilterChange(Sender: TObject;
  DataSet: TDataSet; NewFilter: String);
var
  s: string;
begin
  inherited;
  CurrentFilter := NewFilter;
  EEClone.Close;
  EEClone.UserFilter := DM_COMPANY.CO_HR_APPLICANT.UserFilter;
  EEClone.UserFiltered := DM_COMPANY.CO_HR_APPLICANT.UserFiltered;
  CreateEEFilter;
  s := DM_COMPANY.CO_HR_APPLICANT.IndexName;
  try
    DM_COMPANY.CO_HR_APPLICANT.IndexName := '';
    EEClone.CloneCursor(DM_COMPANY.CO_HR_APPLICANT, False, True);
  finally
    if s <> '' then
      DM_COMPANY.CO_HR_APPLICANT.IndexName := s;
  end;
  dblkupEE_NBR.Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
  dbtxtname.Value := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsString;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.wwdsListDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(dblkupEE_NBR) then
  begin
    dblkupEE_NBR.Enabled := not BitBtn1.Enabled;
    dbtxtname.Enabled := not BitBtn1.Enabled;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_CLIENT.CL_PERSON, SupportDataSets);
  AddDS(DM_COMPANY.CO_HR_APPLICANT, SupportDataSets);
end;

procedure TEDIT_CO_HR_APPLICANT_BASE.AfterDataSetsReopen;
begin
  inherited;
  SetReadOnly(GetIfReadOnly or BlockHr);
end;

initialization
  RegisterClass( TEDIT_CO_HR_APPLICANT_BASE );

end.
