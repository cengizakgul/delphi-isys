// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_EE_EDUCATION;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CL_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, wwdbdatetimepicker, Mask, wwdbedit,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE,  DBActns, ActnList,
  Wwdotdot, Wwdbcomb, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIDBMemo, isUIwwDBComboBox, isUIwwDBDateTimePicker,
  isUIwwDBEdit;

type
  TEDIT_CL_HR_EE_EDUCATION = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    fpEducation: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evDBComboBox1: TevDBComboBox;
    evLabel2: TevLabel;
    EvDBMemo1: TEvDBMemo;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;


  end;

implementation

uses SPD_EDIT_Open_BASE, SPD_EDIT_EE_HR_BASE;

{$R *.DFM}

{ TEDIT_CL_HR_EE_EDUCATION }

procedure TEDIT_CL_HR_EE_EDUCATION.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  inherited;
  AddDS( DM_CLIENT.CL_HR_SCHOOL, aDS);
end;


function TEDIT_CL_HR_EE_EDUCATION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_PERSON_EDUCATION;
end;

function TEDIT_CL_HR_EE_EDUCATION.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

initialization
  RegisterClass( TEDIT_CL_HR_EE_EDUCATION );

end.      
