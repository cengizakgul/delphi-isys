inherited EDIT_CL_HR_SCHOOL: TEDIT_CL_HR_SCHOOL
  Width = 809
  Height = 693
  inherited Panel1: TevPanel
    Width = 809
    inherited pnlFavoriteReport: TevPanel
      Left = 694
    end
    inherited pnlTopRight: TevPanel
      Width = 694
    end
  end
  inherited PageControl1: TevPageControl
    Width = 809
    Height = 639
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 801
        Height = 610
        inherited pnlBorder: TevPanel
          Width = 797
          Height = 606
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 797
          Height = 606
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 0
              Width = 800
              BevelOuter = bvNone
              object sbSchools: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlSchoolBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object evSplitter1: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlSchoolLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpClient: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpClient'
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPClientBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 249
                        Height = 150
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlSchoolRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpSchool: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Schools'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPSchoolBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 249
                        Height = 150
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 375
              Top = 94
            end
          end
          inherited Panel3: TevPanel
            Width = 749
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 749
            Height = 499
            inherited Splitter1: TevSplitter
              Height = 495
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 495
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 415
                IniAttributes.SectionName = 'TEDIT_CL_HR_SCHOOL\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 393
              Height = 495
              Title = 'Schools'
              object evDBGrid2: TevDBGrid
                Left = 18
                Top = 48
                Width = 343
                Height = 415
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'STATE'#9'3'#9'State'#9'F'
                  'CITY'#9'20'#9'City'#9'F'
                  'ZIP_CODE'#9'10'#9'Zip code'#9'F'
                  'ADDRESS1'#9'30'#9'Address 1'#9'F'
                  'ADDRESS2'#9'30'#9'Address 2'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_HR_SCHOOL\evDBGrid2'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnDblClick = evDBGrid2DblClick
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tbshDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 801
        Height = 610
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 801
          Height = 610
          Align = alClient
          TabOrder = 0
          object evPanel3: TevPanel
            Left = 0
            Top = 0
            Width = 797
            Height = 606
            Align = alClient
            BevelOuter = bvNone
            BevelWidth = 2
            BorderWidth = 8
            TabOrder = 0
            Visible = False
          end
          object evPanel2: TevPanel
            Left = 0
            Top = 606
            Width = 797
            Height = 0
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
          end
          object pnlBrowseSchool: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 698
            Height = 279
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            ParentBackground = False
            TabOrder = 2
            OnResize = pnlBrowseSchoolResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlBrowseSchoolBody: TevPanel
              Left = 12
              Top = 35
              Width = 665
              Height = 223
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object evDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 665
                Height = 223
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'STATE'#9'3'#9'State'#9'F'
                  'CITY'#9'20'#9'City'#9'F'
                  'ZIP_CODE'#9'10'#9'Zip code'#9'F'
                  'ADDRESS1'#9'30'#9'Address 1'#9'F'
                  'ADDRESS2'#9'30'#9'Address 2'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_HR_SCHOOL\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          object pnlEditSchool: TisUIFashionPanel
            Left = 8
            Top = 295
            Width = 698
            Height = 209
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'pnlEditSchool'
            Color = 14737632
            TabOrder = 3
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'School Detail'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evLabel1: TevLabel
              Left = 12
              Top = 35
              Width = 44
              Height = 13
              Caption = '~Name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel2: TevLabel
              Left = 197
              Top = 152
              Width = 25
              Height = 13
              Caption = 'State'
            end
            object evLabel3: TevLabel
              Left = 12
              Top = 152
              Width = 17
              Height = 13
              Caption = 'City'
            end
            object evLabel4: TevLabel
              Left = 12
              Top = 74
              Width = 47
              Height = 13
              Caption = 'Address 1'
            end
            object evLabel5: TevLabel
              Left = 12
              Top = 113
              Width = 47
              Height = 13
              Caption = 'Address 2'
            end
            object evLabel6: TevLabel
              Left = 260
              Top = 152
              Width = 43
              Height = 13
              Caption = 'Zip Code'
            end
            object dedtName: TevDBEdit
              Left = 12
              Top = 50
              Width = 369
              Height = 21
              DataField = 'NAME'
              DataSource = wwdsDetail
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
            end
            object evDBEdit2: TevDBEdit
              Left = 197
              Top = 167
              Width = 55
              Height = 21
              DataField = 'STATE'
              DataSource = wwdsDetail
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&,@}'
              TabOrder = 4
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
            end
            object evDBEdit3: TevDBEdit
              Left = 12
              Top = 167
              Width = 181
              Height = 21
              DataField = 'CITY'
              DataSource = wwdsDetail
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
            end
            object evDBEdit4: TevDBEdit
              Left = 12
              Top = 89
              Width = 369
              Height = 21
              DataField = 'ADDRESS1'
              DataSource = wwdsDetail
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
            end
            object evDBEdit5: TevDBEdit
              Left = 12
              Top = 128
              Width = 369
              Height = 21
              DataField = 'ADDRESS2'
              DataSource = wwdsDetail
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
            end
            object evDBEdit6: TevDBEdit
              Left = 260
              Top = 167
              Width = 121
              Height = 21
              DataField = 'ZIP_CODE'
              DataSource = wwdsDetail
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*5{#,?}[-*4{#,?}]'
              TabOrder = 5
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_HR_SCHOOL.CL_HR_SCHOOL
    MasterDataSource = wwdsMaster
  end
end
