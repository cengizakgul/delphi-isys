inherited EDIT_EE_HR_INJURY_OCCURRENCE: TEDIT_EE_HR_INJURY_OCCURRENCE
  Width = 1017
  Height = 1121
  inherited Panel1: TevPanel
    Width = 1017
    inherited pnlFavoriteReport: TevPanel
      Left = 865
    end
    inherited pnlTopLeft: TevPanel
      Width = 865
    end
  end
  inherited PageControl1: TevPageControl
    Width = 1017
    Height = 1034
    ActivePage = tbshBlobs
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1009
        Height = 1005
        inherited pnlBorder: TevPanel
          Width = 1005
          Height = 1001
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1005
          Height = 1001
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Width = 947
              Height = 365
            end
            inherited pnlSubbrowse: TevPanel
              Width = 622
              Height = 363
            end
          end
          inherited Panel3: TevPanel
            Width = 957
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 957
            Height = 894
            inherited Splitter1: TevSplitter
              Height = 890
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 890
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 810
                IniAttributes.SectionName = 'TEDIT_EE_HR_INJURY_OCCURRENCE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 635
              Height = 890
              inherited wwDBGrid4: TevDBGrid
                Width = 585
                Height = 810
                IniAttributes.SectionName = 'TEDIT_EE_HR_INJURY_OCCURRENCE\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Width = 1009
        Height = 1005
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 828
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 812
            Height = 225
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 779
              Height = 169
              inherited evDBGrid1: TevDBGrid
                Width = 779
                Height = 169
                Selected.Strings = (
                  'CASE_NUMBER'#9'15'#9'Case Number'#9'F'
                  'CASE_STATUS'#9'1'#9'Case Status'#9'F'
                  'InjuryCode'#9'40'#9'Injury Code'#9'F'
                  'AnatomicCode'#9'40'#9'Anatomic Code'#9'F'
                  'DATE_FILED'#9'18'#9'Date Filed'#9'F'
                  'DATE_OCCURRED'#9'18'#9'Date Occurred'#9'F'
                  'DAYS_AWAY'#9'10'#9'Days Away'#9'F'
                  'DAYS_RESTRICTED'#9'10'#9'Days Restricted'#9'F'
                  'DEATH'#9'1'#9'Death'#9'F'
                  'FIRST_AID'#9'1'#9'First Aid'#9'F'
                  'LABOR_COST'#9'10'#9'Labor Cost'#9'F'
                  'MEDICAL_COST'#9'10'#9'Medical Cost'#9'F'
                  'WC_CLAIM_NUMBER'#9'10'#9'WC Claim Number'#9'F'
                  'PRIVACY_CASE'#9'1'#9'Privacy Case'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_HR_INJURY_OCCURRENCE\evDBGrid1'
              end
            end
          end
        end
        object fpInjuryDetails: TisUIFashionPanel
          Left = 8
          Top = 241
          Width = 301
          Height = 287
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Injury Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 74
            Width = 81
            Height = 17
            Caption = '~Case Number'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 150
            Top = 152
            Width = 70
            Height = 13
            Alignment = taRightJustify
            Caption = 'Date Occurred'
          end
          object evLabel3: TevLabel
            Left = 150
            Top = 191
            Width = 48
            Height = 13
            Alignment = taRightJustify
            Caption = 'Date Filed'
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 113
            Width = 63
            Height = 13
            Caption = 'WC Claim No'
          end
          object evLabel5: TevLabel
            Left = 12
            Top = 152
            Width = 46
            Height = 13
            Caption = 'WC Code'
          end
          object evLabel6: TevLabel
            Left = 12
            Top = 230
            Width = 53
            Height = 13
            Caption = 'Injury Code'
          end
          object evLabel14: TevLabel
            Left = 12
            Top = 191
            Width = 72
            Height = 13
            Caption = 'Anatomic Code'
          end
          object evLabel24: TevLabel
            Left = 150
            Top = 35
            Width = 83
            Height = 13
            Caption = 'Employee Activity'
          end
          object evLabel23: TevLabel
            Left = 150
            Top = 113
            Width = 79
            Height = 13
            Caption = 'Where Occurred'
          end
          object evLabel25: TevLabel
            Left = 150
            Top = 74
            Width = 87
            Height = 13
            Caption = 'Object Description'
          end
          object evDBEdit1: TevDBEdit
            Left = 12
            Top = 89
            Width = 130
            Height = 21
            DataField = 'CASE_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 130
            Height = 36
            Caption = '~Case Status'
            Columns = 2
            DataField = 'CASE_STATUS'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Open'
              'Closed')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'O'
              'C')
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 150
            Top = 167
            Width = 130
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DATE_OCCURRED'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 9
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 150
            Top = 206
            Width = 130
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DATE_FILED'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 10
          end
          object evDBEdit2: TevDBEdit
            Left = 12
            Top = 128
            Width = 130
            Height = 21
            DataField = 'WC_CLAIM_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 130
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'WORKERS_COMP_CODE'#9'5'#9'Workers Comp Code'#9'F'
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 130
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'SY_HR_OSHA_ANATOMIC_CODES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_HR_OSHA_ANATOMIC_CODES.SY_HR_OSHA_ANATOMIC_CODES
            LookupField = 'SY_HR_OSHA_ANATOMIC_CODES_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 245
            Width = 130
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'SY_HR_INJURY_CODES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_HR_INJURY_CODES.SY_HR_INJURY_CODES
            LookupField = 'SY_HR_INJURY_CODES_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBEdit14: TevDBEdit
            Left = 150
            Top = 50
            Width = 130
            Height = 21
            DataField = 'EMPLOYEE_ACTIVITY'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit16: TevDBEdit
            Left = 150
            Top = 128
            Width = 130
            Height = 21
            DataField = 'WHERE_OCCURED'
            DataSource = wwdsDetail
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit15: TevDBEdit
            Left = 150
            Top = 89
            Width = 130
            Height = 21
            DataField = 'OBJECT_DESCRIPTION'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object fpMedicalFacility: TisUIFashionPanel
          Left = 626
          Top = 241
          Width = 194
          Height = 287
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'isUIFashionPanel1'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Medical Facility'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel16: TevLabel
            Left = 12
            Top = 35
            Width = 76
            Height = 13
            Caption = 'Physician Name'
          end
          object evLabel17: TevLabel
            Left = 12
            Top = 74
            Width = 63
            Height = 13
            Caption = 'Facility Name'
          end
          object evLabel18: TevLabel
            Left = 12
            Top = 113
            Width = 47
            Height = 13
            Caption = 'Address 1'
          end
          object evLabel19: TevLabel
            Left = 12
            Top = 152
            Width = 47
            Height = 13
            Caption = 'Address 2'
          end
          object evLabel20: TevLabel
            Left = 12
            Top = 191
            Width = 17
            Height = 13
            Caption = 'City'
          end
          object evLabel21: TevLabel
            Left = 12
            Top = 230
            Width = 25
            Height = 13
            Caption = 'State'
          end
          object evLabel22: TevLabel
            Left = 96
            Top = 230
            Width = 43
            Height = 13
            Caption = 'Zip Code'
          end
          object evDBEdit3: TevDBEdit
            Left = 12
            Top = 50
            Width = 160
            Height = 21
            DataField = 'PHYSICIAN_NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit8: TevDBEdit
            Left = 12
            Top = 89
            Width = 160
            Height = 21
            DataField = 'HOSPITAL_NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit9: TevDBEdit
            Left = 12
            Top = 128
            Width = 160
            Height = 21
            DataField = 'ADDRESS1'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit10: TevDBEdit
            Left = 12
            Top = 167
            Width = 160
            Height = 21
            DataField = 'ADDRESS2'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit11: TevDBEdit
            Left = 12
            Top = 206
            Width = 160
            Height = 21
            DataField = 'CITY'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit12: TevDBEdit
            Left = 12
            Top = 245
            Width = 75
            Height = 21
            DataField = 'STATE'
            DataSource = wwdsDetail
            Picture.PictureMask = '*{&,@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit13: TevDBEdit
            Left = 96
            Top = 245
            Width = 76
            Height = 21
            DataField = 'ZIP_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object fpTreatmentDetails: TisUIFashionPanel
          Left = 317
          Top = 241
          Width = 301
          Height = 287
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'isUIFashionPanel1'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Treatment Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel7: TevLabel
            Left = 150
            Top = 35
            Width = 53
            Height = 13
            Caption = 'Days Away'
          end
          object evLabel10: TevLabel
            Left = 150
            Top = 113
            Width = 50
            Height = 13
            Caption = 'Labor cost'
          end
          object evLabel15: TevLabel
            Left = 12
            Top = 230
            Width = 67
            Height = 13
            Alignment = taRightJustify
            Caption = 'Date of Death'
          end
          object evLabel8: TevLabel
            Left = 150
            Top = 74
            Width = 75
            Height = 13
            Caption = 'Days Restricted'
          end
          object evLabel9: TevLabel
            Left = 150
            Top = 152
            Width = 60
            Height = 13
            Caption = 'Medical cost'
          end
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 130
            Height = 36
            Caption = 'First Aid'
            Columns = 2
            DataField = 'FIRST_AID'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup4: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 130
            Height = 36
            Caption = 'Emergency Room'
            Columns = 2
            DataField = 'EMERGENCY_ROOM'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup5: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 130
            Height = 36
            Caption = 'Overnight Hospitalization'
            Columns = 2
            DataField = 'OVERNIGHT_HOSPITALIZATION'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup6: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 130
            Height = 36
            Caption = 'Privacy Case'
            Columns = 2
            DataField = 'PRIVACY_CASE'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup3: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 130
            Height = 36
            Caption = 'Death'
            Columns = 2
            DataField = 'DEATH'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
            OnChange = evDBRadioGroup3Change
          end
          object evDBEdit4: TevDBEdit
            Left = 150
            Top = 50
            Width = 130
            Height = 21
            DataField = 'DAYS_AWAY'
            DataSource = wwdsDetail
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit7: TevDBEdit
            Left = 150
            Top = 128
            Width = 130
            Height = 21
            DataField = 'LABOR_COST'
            DataSource = wwdsDetail
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 12
            Top = 245
            Width = 130
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DATE_OF_DEATH'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object evDBEdit5: TevDBEdit
            Left = 150
            Top = 89
            Width = 130
            Height = 21
            DataField = 'DAYS_RESTRICTED'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit6: TevDBEdit
            Left = 150
            Top = 167
            Width = 130
            Height = 21
            DataField = 'MEDICAL_COST'
            DataSource = wwdsDetail
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
    object tbshBlobs: TTabSheet
      Caption = 'Notes'
      ImageIndex = 11
      object sbInjuryNotes: TScrollBox
        Left = 0
        Top = 0
        Width = 1009
        Height = 1005
        Align = alClient
        TabOrder = 0
        object fpSpecialNotes: TisUIFashionPanel
          Left = 8
          Top = 316
          Width = 704
          Height = 146
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Special Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object EvDBMemo3: TEvDBMemo
            Left = 12
            Top = 35
            Width = 670
            Height = 89
            DataField = 'SPECIAL_NOTES'
            DataSource = wwdsDetail
            TabOrder = 0
          end
        end
        object fpRestrictions: TisUIFashionPanel
          Left = 8
          Top = 162
          Width = 704
          Height = 146
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Restrictions'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object EvDBMemo2: TEvDBMemo
            Left = 12
            Top = 35
            Width = 670
            Height = 89
            DataField = 'RESTRICTIONS'
            DataSource = wwdsDetail
            TabOrder = 0
          end
        end
        object fpCause: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 704
          Height = 146
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Cause'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object EvDBMemo1: TEvDBMemo
            Tag = 35
            Left = 12
            Top = 35
            Width = 670
            Height = 89
            DataField = 'CAUSE'
            DataSource = wwdsDetail
            TabOrder = 0
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 1017
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_HR_INJURY_OCCURRENCE.EE_HR_INJURY_OCCURRENCE
    MasterDataSource = wwdsSubMaster
  end
  inherited wwdsEmployee: TevDataSource
    Left = 202
  end
  inherited ActionList: TevActionList
    Left = 483
    Top = 3
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 632
    Top = 57
  end
end
