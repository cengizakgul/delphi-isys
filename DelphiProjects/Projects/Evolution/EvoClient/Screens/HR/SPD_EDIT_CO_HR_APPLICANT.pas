// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_APPLICANT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, Mask, wwdbedit, wwdbdatetimepicker, Wwdotdot, Wwdbcomb, ExtDlgs,
  SPD_MiniNavigationFrame, SPD_EDIT_CO_HR_APPLICANT_BASE, DBActns, EvBasicUtils,
  ActnList,  SPackageEntry, SPD_ChoiceFromListQuery, EvSendMail,
  EvConsts, EvStreamUtils, ShellAPI, ISZippingRoutines,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDDClasses, 
  ISDataAccessComponents, EvDataAccessComponents, SPD_PersonDocuments,
  SDataDictclient, SDataDicttemp, SDataDictsystem, EvContext, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBDateTimePicker, isUIwwDBEdit, isUIwwDBComboBox,
  ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIDBImage, EvCommonInterfaces, EvDataset;

type
  TEDIT_CO_HR_APPLICANT = class(TEDIT_CO_HR_APPLICANT_BASE)
    tshtDetails: TTabSheet;
    tshtNotes: TTabSheet;
    EvDBMemo1: TEvDBMemo;    
    wwdsInterview: TevDataSource;
    tshtInterview: TTabSheet;
    tshtPerson: TTabSheet;
    OpenDialog1: TOpenPictureDialog;
    lablCar: TevLabel;
    pnlDbControls: TevPanel;
    pnlBrowseInterview: TevPanel;
    evDBGrid2: TevDBGrid;
    evSpeedButton1: TevSpeedButton;
    TabSheet2: TTabSheet;
    evPanel1: TevPanel;
    evBitBtn2: TevBitBtn;
    evDBComboBox3: TevDBComboBox;
    tsDocuments: TTabSheet;
    TPersonDocuments1: TPersonDocuments;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    sbApplicantBrowse: TScrollBox;
    pnlLeftBorder: TevPanel;
    pnlRightBorder: TevPanel;
    pnlTopBorder: TevPanel;
    pnlBottomBorder: TevPanel;
    pnlBrowseLeft: TevPanel;
    evSplitter1: TevSplitter;
    pnlBrowseRight: TevPanel;
    fpBrowseLeft: TisUIFashionPanel;
    pnlFPLeftBody: TevPanel;
    fpBrowseRight: TisUIFashionPanel;
    pnlFPRightBody: TevPanel;
    sbPerson: TScrollBox;
    fpApplicantInfo: TisUIFashionPanel;
    lablSocial_Security_Number: TevLabel;
    lablLast_Name: TevLabel;
    lablFirst_Name: TevLabel;
    lablMiddle_Initial: TevLabel;
    evLabel21: TevLabel;
    evLabel1: TevLabel;
    dedtSocial_Security_Number: TevDBEdit;
    dedtLast_Name: TevDBEdit;
    dedtFirst_Name: TevDBEdit;
    dedtMiddle_Initial: TevDBEdit;
    evDBComboBox2: TevDBComboBox;
    evDBDateTimePicker1: TevDBDateTimePicker;
    fpMilitary: TisUIFashionPanel;
    lablVeteran_Discharge_Date: TevLabel;
    drgpMilitary_Reserve: TevDBRadioGroup;
    drgpDisabled_Veteran: TevDBRadioGroup;
    drgpVietnam_Veteran: TevDBRadioGroup;
    wwDBDateTimePicker13: TevDBDateTimePicker;
    drgpVeteran: TevDBRadioGroup;
    evRGServiceMedalVeteran: TevDBRadioGroup;
    evRGOtherProtectedVeteran: TevDBRadioGroup;
    fpAddress: TisUIFashionPanel;
    lablAddress_1: TevLabel;
    lablAddress_2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablCounty: TevLabel;
    lablPrimary_Phone: TevLabel;
    lablSecondary_Phone: TevLabel;
    lablThird_Phone: TevLabel;
    lablEmail_Address: TevLabel;
    dedtAddress_1: TevDBEdit;
    dedtAddress_2: TevDBEdit;
    dedtCity: TevDBEdit;
    dedtState: TevDBEdit;
    dedtZip_Code: TevDBEdit;
    dedtCounty: TevDBEdit;
    dedtPrimary_Phone: TevDBEdit;
    dedtPrimary_Phone_Description: TevDBEdit;
    dedtSecondary_Phone_Description: TevDBEdit;
    dedtSecondary_Phone: TevDBEdit;
    dedtThird_Phone: TevDBEdit;
    dedtThird_Phone_Description: TevDBEdit;
    dedtEmail_Address: TevDBEdit;
    lblExtension1: TevLabel;
    lblExtension2: TevLabel;
    lblExtension3: TevLabel;
    fpContact: TisUIFashionPanel;
    fpDBDT: TisUIFashionPanel;
    fpDesired: TisUIFashionPanel;
    lablPosition_Status: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evDBRadioGroup1: TevDBRadioGroup;
    wwcbPosition_Status: TevDBComboBox;
    evDBEdit3: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evDBEdit5: TevDBEdit;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evDBRadioGroup3: TevDBRadioGroup;
    evDBEdit6: TevDBEdit;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evDBDateTimePicker4: TevDBDateTimePicker;
    evLabel8: TevLabel;
    evLabel9: TevLabel;
    lablJobs_Number: TevLabel;
    Label75: TevLabel;
    Label78: TevLabel;
    Label76: TevLabel;
    Label77: TevLabel;
    SpeedButton4: TevSpeedButton;
    wwlcJobs_Number: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwDBLookupCombo2: TevDBLookupCombo;
    wwDBLookupCombo3: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBLookupCombo8: TevDBLookupCombo;
    wwDBLookupCombo7: TevDBLookupCombo;
    wwDBLookupCombo6: TevDBLookupCombo;
    wwDBLookupCombo5: TevDBLookupCombo;
    sbApplicantDetails: TScrollBox;
    fpVehicle: TisUIFashionPanel;
    fpVisa: TisUIFashionPanel;
    evLabel23: TevLabel;
    evLabel24: TevLabel;
    evLabel25: TevLabel;
    evLabel26: TevLabel;
    evLabel27: TevLabel;
    evLabel22: TevLabel;
    evDBEdit8: TevDBEdit;
    evDBEdit9: TevDBEdit;
    evDBEdit10: TevDBEdit;
    evDBRadioGroup5: TevDBRadioGroup;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBDateTimePicker11: TevDBDateTimePicker;
    evDBDateTimePicker12: TevDBDateTimePicker;
    evLabel28: TevLabel;
    evLabel29: TevLabel;
    evLabel30: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBComboBox4: TevDBComboBox;
    evDBDateTimePicker13: TevDBDateTimePicker;
    sbMisc: TScrollBox;
    sbNotes: TScrollBox;
    fpNotes: TisUIFashionPanel;
    sbInterview: TScrollBox;
    fpInterviewSummary: TisUIFashionPanel;
    fpInterviewDetails: TisUIFashionPanel;
    evLabel13: TevLabel;
    evLabel14: TevLabel;
    evLabel16: TevLabel;
    evLabel17: TevLabel;
    evLabel20: TevLabel;
    evLabel19: TevLabel;
    evLabel18: TevLabel;
    evLabel15: TevLabel;
    evDBDateTimePicker7: TevDBDateTimePicker;
    evDBEdit7: TevDBEdit;
    evDBRadioGroup4: TevDBRadioGroup;
    evDBDateTimePicker8: TevDBDateTimePicker;
    evDBDateTimePicker9: TevDBDateTimePicker;
    evDBDateTimePicker10: TevDBDateTimePicker;
    EvDBMemo4: TEvDBMemo;
    EvDBMemo3: TEvDBMemo;
    EvDBMemo2: TEvDBMemo;
    InterviewMiniNavigationFrame: TMiniNavigationFrame;
    evLabel3: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel4: TevLabel;
    evDBDateTimePicker2: TevDBDateTimePicker;
    lablRecruiter: TevLabel;
    wwlcRecruiter: TevDBLookupCombo;
    lablReferral: TevLabel;
    wwlcReferral: TevDBLookupCombo;
    Supervisor: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    lablSecurity_Clearance: TevLabel;
    dedtSecurity_Clearance: TevDBEdit;
    evLabel33: TevLabel;
    evDBDateTimePicker6: TevDBDateTimePicker;
    evLabel12: TevLabel;
    evLabel2: TevLabel;
    Label53: TevLabel;
    evDBComboBox1: TevDBComboBox;
    Label79: TevLabel;
    evDBComboBox5: TevDBComboBox;
    evDBLookupCombo10: TevDBLookupCombo;
    evLabel32: TevLabel;
    btnHire: TevBitBtn;
    btnApplyForNewPosition: TevBitBtn;
    wwDBComboBox6: TevDBComboBox;
    wwlcEEOCode: TevDBLookupCombo;
    evLabel31: TevLabel;
    dimgPicture: TevDBImage;
    bbtnLoad: TevBitBtn;
    lablPicture: TevLabel;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBDateTimePicker5: TevDBDateTimePicker;
    procedure SpeedButton4Click(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure bbtnLoadClick(Sender: TObject);
    procedure dedtSocial_Security_NumberExit(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure btnHireClick(Sender: TObject);
    procedure wwdsSubMaster2StateChange(Sender: TObject);
    procedure dedtSocial_Security_NumberKeyPress(Sender: TObject;
      var Key: Char);
    procedure evBitBtn2Click(Sender: TObject);
    procedure dedtStateExit(Sender: TObject);
    procedure btnApplyForNewPositionClick(Sender: TObject);
    procedure tshtPersonShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ResultFieldSSNFunction;
    procedure PageControl1Change(Sender: TObject);
    procedure InterviewMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure TPersonDocuments1btnAddDocClick(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation 

uses
  SPD_DBDTSelectionListFiltr, evutils, typinfo, DateUtils, EvTypes,
  SPD_EDIT_Open_BASE;

{$R *.DFM}

{ TEDIT_CO_HR_APPLICANT }


function TEDIT_CO_HR_APPLICANT.GetInsertControl: TWinControl;
begin
  Result := dedtSocial_Security_Number;
end;

procedure TEDIT_CO_HR_APPLICANT.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin

  AddDS( DM_COMPANY.CO_DIVISION, aDS );
  AddDS( DM_COMPANY.CO_HR_REFERRALS, aDS );
  AddDS( DM_COMPANY.CO_DEPARTMENT, aDS );
  AddDS( DM_COMPANY.CO_TEAM, aDS );
  AddDS( DM_COMPANY.CO_HR_CAR, aDS );
  AddDS( DM_COMPANY.CO_BRANCH, aDS );
  AddDS( DM_COMPANY.CO_HR_RECRUITERS, aDS );
  AddDS( DM_COMPANY.CO_JOBS, aDS );
  AddDS( DM_COMPANY.CO_HR_SUPERVISORS, aDS );
  AddDS( DM_COMPANY.CO_HR_APPLICANT_INTERVIEW, aDS);
  AddDS( DM_CLIENT.CL_PERSON_DOCUMENTS, aDS);
  AddDS( DM_COMPANY.CO_HR_POSITIONS, aDS);

  inherited;

end;

function TEDIT_CO_HR_APPLICANT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_APPLICANT;
end;

procedure TEDIT_CO_HR_APPLICANT.RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
  var InsertDataSets, EditDataSets: TArrayDS;
  var DeleteDataSet: TevClientDataSet; var SupportDataSets: TArrayDS);
begin
  AddDS(DM_CLIENT.CL_PERSON, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON, InsertDataSets);
  inherited;
  AddDS(DM_COMPANY.CO_HR_APPLICANT_INTERVIEW, EditDataSets);
  AddDS(DM_CLIENT.CL_PERSON_DOCUMENTS, EditDataSets);
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
  AddDS(DM_SYSTEM_STATE.SY_HR_EEO, SupportDataSets);
end;

procedure TEDIT_CO_HR_APPLICANT.SpeedButton4Click(Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DM_PAYROLL, DBDTSelectionList do
    begin
      DBDTSelectionList.Setup(CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value
        , wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value, wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value
        , wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
          begin
            if wwdsDetail.DataSet.RecordCount = 0 then
              wwdsDetail.DataSet.Insert
            else
              wwdsDetail.DataSet.Edit;
          end;
          wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          wwdsDetail.DataSet.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (DM_COMPANY.CO_HR_APPLICANT.State = dsBrowse) and
     (TevClientDataSet(DM_CLIENT.CL_PERSON).State = dsBrowse) then
  begin
    if (DM_COMPANY.CO_HR_APPLICANT.RecordCount = 0) and
       ((DM_CLIENT.CL_PERSON.RecordCount <> 0) or
         DM_CLIENT.CL_PERSON.Filtered) then
    begin
  //    ODS( 'wwdsDetailDataChange: set filter');
      DM_CLIENT.CL_PERSON.Filter := 'CL_PERSON_NBR = 0';
      DM_CLIENT.CL_PERSON.Filtered := True;
    end
    else
    begin
//      ODS( 'wwdsDetailDataChange: locate');
      DM_CLIENT.CL_PERSON.Filtered := False;
      DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_COMPANY.CO_HR_APPLICANT['CL_PERSON_NBR'], []);
    end;

    if Assigned(btnHire) then
      btnHire.Enabled := DM_COMPANY.CO_HR_APPLICANT.FieldByName('APPLICANT_STATUS').AsString <> APPLICANT_STATUS_HIRED;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.bbtnLoadClick(Sender: TObject);
begin
  inherited;
  if OpenDialog1.Execute then
  begin
    if wwdsSubMaster2.DataSet.State = dsBrowse then
      wwdsSubMaster2.DataSet.Edit;
    dimgPicture.Picture.LoadFromFile(OpenDialog1.FileName);
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.dedtSocial_Security_NumberExit(
  Sender: TObject);
var
  TempSSN: string;
  t: TevClientDataSet;
begin
  inherited;
  with DM_CLIENT.CL_PERSON do
    if TevClientDataSet(DM_CLIENT.CL_PERSON).State = dsInsert then
    begin
      DisableControls;
      t := TevClientDataSet.Create(Self);
      try
        t.CloneCursor(DM_CLIENT.CL_PERSON, True);
        t.Filtered := False;
        TempSSN := FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
        if t.Locate('SOCIAL_SECURITY_NUMBER', TempSSN, []) then
        begin
          Cancel;
          Filtered := False;
          Locate('SOCIAL_SECURITY_NUMBER', TempSSN, []);
          Edit;
          if DM_COMPANY.CO_HR_APPLICANT.State in [dsInsert] then
            DM_COMPANY.CO_HR_APPLICANT.FieldByName('CL_PERSON_NBR').Assign(FieldByName('CL_PERSON_NBR'));
        end;
      finally
        EnableControls;
        t.Free;
      end;
    end;
end;


function TEDIT_CO_HR_APPLICANT.GetDataSetConditions(sName: string): string;
begin
  if sNAme = 'CO_HR_APPLICANT_INTERVIEW' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_HR_APPLICANT.evSpeedButton1Click(Sender: TObject);
  procedure dump( ds: TevDatasource );
  begin
    with ds do
      ODS( '%s: State=%s rc=%d condition=<%s> filtered=%d filter =<%s>',[
      ds.name,
      GetEnumName( TypeInfo(TDataSetState), ord(Dataset.State) ),
      dataSet.RecordCount,
      (dataset as TevClientDataSet).retrievecondition,
      ord(dataSet.Filtered),
      dataset.filter] );
  end;
begin
//  wwdsSubMaster2.Active := not wwdsSubMaster2.Active;
//  wwdsSubMaster2.Active := not wwdsSubMaster2.Active;
//  dump( wwdsSubMaster2 );
//  dump( wwdsDetail );
end;

procedure TEDIT_CO_HR_APPLICANT.Activate;
begin
  inherited;
  InterviewMiniNavigationFrame.DataSource := wwdsInterview;
  InterviewMiniNavigationFrame.InsertFocusControl := evDBDateTimePicker7;
  ResultFieldSSNFunction;
end;

procedure TEDIT_CO_HR_APPLICANT.btnHireClick(Sender: TObject);
var DS: TevClientDataSet;
   Frm: TChoiceFromListQuery;
   SSN: string;
   Found: boolean;
begin
  DS := TevClientDataSet.Create(Self);
  SSN := DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
  Found := False;
  try
    DS.CloneCursor(DM_ClIENT.EE, True);
    //
    // Search for existent Employee by SSN (reso# 35361). Added by Andrew.
    //
    if (DS.Active and DS.Locate('SOCIAL_SECURITY_NUMBER', SSN, [])) then begin
      if EvMessage('Employee(s) with such SSN were found. You can select employee or create new one.', mtConfirmation, [mbOk, mbCancel]) <> mrOk then begin
        //DS.Free;
        Exit;
      end;
      Found := True;
      Frm := TChoiceFromListQuery.Create(nil);
      Frm.Caption := 'Select Employee';
      Frm.dgChoiceList.Selected.Clear;
      //Frm.dgChoiceList.Selected.Add('SOCIAL_SECURITY_NUMBER'#9'20'#9'SSN'#9'F');
      Frm.dgChoiceList.Selected.Add('CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F');
      Frm.dgChoiceList.Selected.Add('Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F');
      Frm.dgChoiceList.Selected.Add('Employee_MI_Calculate'#9'2'#9'MI'#9'F');
      Frm.dgChoiceList.Selected.Add('Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F');
      Frm.dgChoiceList.Selected.Add('Position_Desc'#9'15'#9'Position'#9'F');
      Frm.dgChoiceList.ApplySelected;
      Frm.dgChoiceList.Options := Frm.dgChoiceList.Options - [dgMultiSelect];
      Frm.btnYes.Caption := 'Use Selected Employee';
      Frm.btnNo.Caption := 'Create New Employee';
      Frm.btnYes.Glyph := nil;
      Frm.btnNo.Glyph := nil;
      Frm.btnYes.Width := 150;
      Frm.btnNo.Width := 150;
      Frm.btnYes.Left := 18;
      Frm.btnNo.Left := 186;
      DS.UserFiltered := False;
      DS.UserFilter := 'SOCIAL_SECURITY_NUMBER='+SSN;
      DS.UserFiltered := True;
      Frm.dsChoiceList.DataSet := DS;
      if Frm.ShowModal = mrYes then begin
        ActivateFrameAs('Employee - Employee', [DS.FieldByName('EE_NBR').AsString, DM_CLIENT.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsInteger], 2);  // Use existent employee
      end
      else
        ActivateFrameAs('Employee - Employee', [DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString, DM_CLIENT.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsInteger], 1);   //Create new employee
    end;
  finally
    DS.Free;
  end;

  if not(Found) then
    ActivateFrameAs('Employee - Employee', [DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString,DM_CLIENT.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsInteger], 1);   //Create new employee
end;

procedure TEDIT_CO_HR_APPLICANT.wwdsSubMaster2StateChange(Sender: TObject);
begin
  if wwdsSubMaster2.DataSet.State = dsInsert then
  begin
    wwdsSubMaster2.DataSet.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := CreateFakeSSN;
    dedtLast_Name.SetFocus;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.dedtSocial_Security_NumberKeyPress(
  Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    dedtLast_Name.SetFocus;
    dedtSocial_Security_Number.Setfocus;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.evBitBtn2Click(Sender: TObject);
var
  Frm: TChoiceFromListQuery;
  DS: TevClientDataSet;
  i, empt: Integer;
  EM: TStringList;
  ind: String;
  anbr: Integer;
begin
  DM_COMPANY.CO_HR_APPLICANT.DisableControls;
  anbr := DM_COMPANY.CO_HR_APPLICANT.FieldByName('CO_HR_APPLICANT_NBR').AsInteger;
  ind := DM_COMPANY.CO_HR_APPLICANT.IndexName;
  DM_COMPANY.CO_HR_APPLICANT.IndexName := '';
  DS := TevClientDataSet.Create(Self);
  EM := TStringList.Create;
  Frm := TChoiceFromListQuery.Create(nil);
  try
    Frm.Caption := 'E-mail';
    DS.CloneCursor(DM_COMPANY.CO_HR_APPLICANT, True);
    Frm.dgChoiceList.Selected.Clear;
    Frm.dgChoiceList.Selected.Add('Name_Calculate'#9'20'#9'Name'#9'F');
    Frm.dgChoiceList.Selected.Add('SSNLookup'#9'20'#9'Applicant Code'#9'F');
    Frm.dgChoiceList.Selected.Add('E_MAIL_Calc'#9'30'#9'eMail'#9'F');
    Frm.dgChoiceList.ApplySelected;
    Frm.dsChoiceList.DataSet := DS;
    DS.Locate('CO_HR_APPLICANT_NBR', anbr, []);
    Frm.Width := 470;
    if Frm.ShowModal = mrYes then
    begin
      empt := 0;
      DS.DisableControls;
      for i := 0 to Frm.dgChoiceList.SelectedList.Count - 1 do
      begin
        DS.GotoBookmark(Frm.dgChoiceList.SelectedList[i]);
        if DS.FieldByName('E_MAIL_Calc').AsString <> '' then
          EM.Add(DS.FieldByName('E_MAIL_Calc').AsString)
        else
          Inc(empt);
      end;
      DS.EnableControls;

      if (Empt > 0) and
         (EvMessage('There are ' + IntToStr(Empt) + ' Applicants who do not have emails entered. Do you want to continue?', mtConfirmation, [mbYes, mbNo], mbYes) <> mrYes) then
        Exit;

      MailTo(EM, nil, nil, nil, '', '');
    end;
  finally
    Frm.Free;
    DS.Free;
    EM.Free;
    DM_COMPANY.CO_HR_APPLICANT.IndexName := ind;
    DM_COMPANY.CO_HR_APPLICANT.Locate('CO_HR_APPLICANT_NBR', anbr, []);
    DM_COMPANY.CO_HR_APPLICANT.EnableControls;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.dedtStateExit(Sender: TObject);
begin
  if (TDataSet(DM_CLIENT.CL_PERSON).State in [dsInsert, dsEdit]) and
    DM_CLIENT.CL_PERSON.RESIDENTIAL_STATE_NBR.IsNull and not DM_CLIENT.CL_PERSON.STATE.IsNull then
  begin
    if DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CL_PERSON.STATE.AsString, []) then
      DM_CLIENT.CL_PERSON.RESIDENTIAL_STATE_NBR.AsInteger :=  DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.AsInteger;
  end;
end;

// Apply for new position

procedure TEDIT_CO_HR_APPLICANT.btnApplyForNewPositionClick(
  Sender: TObject);
var SSN: string;
begin
  inherited;
  if TFramePackageTmpl(Owner).btnNavInsert.Enabled then
  begin
    SSN := DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
    TFramePackageTmpl(Owner).ClickButton(NavInsert);
    DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := SSN;
    dedtSocial_Security_Number.OnExit(nil);
    dedtSocial_Security_Number.SetFocus;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;

  if AContext = 1 then  //Apply employee for new position
    if TFramePackageTmpl(Owner).btnNavInsert.Enabled then
    begin
       TFramePackageTmpl(Owner).ClickButton(NavInsert);
       PageControl1.ActivePage := tshtPerson;
       DM_CLIENT.CL_PERSON.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := AParams[0];
       dedtSocial_Security_Number.OnExit(nil);
       dedtSocial_Security_Number.SetFocus;
    end;
end;

procedure TEDIT_CO_HR_APPLICANT.tshtPersonShow(Sender: TObject);
begin
  inherited;

  if not(wwdsSubMaster2.State in [dsInsert, dsEdit]) and not(wwdsSubMaster2.DataSet.IsEmpty) then begin
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('COLUMNS', 'PICTURE');
      SetMacro('TABLENAME', 'CL_PERSON');
      SetMacro('NBRFIELD', 'CL_PERSON');
      SetParam('RecordNbr', wwdsSubMaster2.DataSet.FieldByName('CL_PERSON_NBR').AsInteger);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Open;
    with wwdsSubMaster2.DataSet as TevClientDataSet do
    begin
      DisableControls;
      try
        Edit;
        FieldByName('PICTURE').Assign(ctx_DataAccess.CUSTOM_VIEW.FieldByName('PICTURE'));
        Post;
        MergeChangeLog;
      finally
        EnableControls;
      end;
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ResultFieldSSNFunction;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.ButtonClicked(Kind: Integer; var Handled: Boolean);

  function isNameExist:boolean;
  var
    Q: IevQuery;
    s: string;
  begin
    Result := false;
    if (DM_COMPANY.CL_PERSON_DOCUMENTS.State in [dsInsert]) then
    begin
      DM_COMPANY.CL_PERSON_DOCUMENTS.UpdateRecord;
      if (not DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').IsNull) then
         Result := true;
    end;
    
    if (DM_COMPANY.CL_PERSON_DOCUMENTS.State in [dsEdit]) then
    begin
      DM_COMPANY.CL_PERSON_DOCUMENTS.UpdateRecord;
      if (not DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').IsNull) and
         (DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').OldValue <>
          DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').NewValue) then
           Result := true;
    end;

    if result then
    begin
      s:= 'SELECT count(*) FROM cl_person_documents WHERE name = :pName AND CL_PERSON_DOCUMENTS_NBR <> :pClPersonDocumentsNbr ' +
              ' and  {AsOfNow<cl_person_documents>} ';

      Q := TevQuery.Create(s);
      Q.Params.AddValue('pName', DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('NAME').asString);
      Q.Params.AddValue('pClPersonDocumentsNbr', DM_COMPANY.CL_PERSON_DOCUMENTS.FieldByName('CL_PERSON_DOCUMENTS_NBR').AsInteger);
      Q.Execute;
      Result := Q.Result.Fields[0].AsInteger > 0;
    end;
  end;

begin
  inherited;

  case Kind of
  NavOk:
    begin
      if (DM_COMPANY.CO_HR_APPLICANT.State in [dsInsert]) then
        DM_COMPANY.CO_HR_APPLICANT.UpdateRecord;

      if isNameExist  then
        raise EUpdateError.CreateHelp('File name is currently in use. Please create a unique name and save.', IDH_ConsistencyViolation);

      if (DM_COMPANY.CO_HR_APPLICANT.FieldByName('APPLICATION_DATE').IsNull) then
        raise EUpdateError.CreateHelp('Application Date can''t be empty', IDH_ConsistencyViolation);
      if (DM_COMPANY.CO_HR_APPLICANT.FieldByName('APPLICATION_DATE').IsNull) then
        raise EUpdateError.CreateHelp('Application Date can''t be empty', IDH_ConsistencyViolation);
      if (DM_COMPANY.CO_HR_APPLICANT.FieldByName('APPLICANT_TYPE').IsNull) then
        raise EUpdateError.CreateHelp('Documents Submitted can''t be empty', IDH_ConsistencyViolation);
    end;
  end;
end;

procedure TEDIT_CO_HR_APPLICANT.AfterClick(Kind: Integer);
begin
  ResultFieldSSNFunction;
end;

procedure TEDIT_CO_HR_APPLICANT.BitBtn1Click(Sender: TObject);
begin
  inherited;
  ResultFieldSSNFunction;
end;

procedure TEDIT_CO_HR_APPLICANT.ResultFieldSSNFunction;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_SSN') <> stEnabled then
   Begin
   (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
   (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;   
    dedtSocial_Security_Number.Enabled := False;
    btnHire.Enabled := false;
    btnApplyForNewPosition.Enabled := False;
   end;
End;

procedure TEDIT_CO_HR_APPLICANT.PageControl1Change(Sender: TObject);
begin
  inherited;
  if (PageControl1.ActivePage = tsDocuments)
      and Assigned(TPersonDocuments1.dsEditDoc.DataSet)
  then TPersonDocuments1.dsEditDoc.DataSet.First; //refresh
end;

procedure TEDIT_CO_HR_APPLICANT.InterviewMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  InterviewMiniNavigationFrame.InsertRecordExecute(Sender);

end;

procedure TEDIT_CO_HR_APPLICANT.TPersonDocuments1btnAddDocClick(
  Sender: TObject);
begin
  inherited;
  TPersonDocuments1.btnAddDocClick(Sender);

end;

initialization
  RegisterClass( TEDIT_CO_HR_APPLICANT );

end.
