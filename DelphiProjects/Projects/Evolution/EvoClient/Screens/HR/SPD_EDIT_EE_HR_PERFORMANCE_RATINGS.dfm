inherited EDIT_EE_HR_PERFORMANCE_RATINGS: TEDIT_EE_HR_PERFORMANCE_RATINGS
  Width = 820
  Height = 790
  inherited Panel1: TevPanel
    Width = 820
    inherited pnlFavoriteReport: TevPanel
      Left = 668
    end
    inherited pnlTopLeft: TevPanel
      Width = 668
    end
  end
  inherited PageControl1: TevPageControl
    Width = 820
    Height = 703
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 812
        Height = 674
        inherited pnlBorder: TevPanel
          Width = 808
          Height = 670
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 808
          Height = 670
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Width = 1068
              Height = 472
            end
            inherited pnlSubbrowse: TevPanel
              Width = 743
              Height = 470
            end
          end
          inherited Panel3: TevPanel
            Width = 760
            inherited bOpenFilteredByDBDTG: TevBitBtn
              TabOrder = 4
            end
            inherited bOpenFilteredByEe: TevBitBtn
              Left = 348
              TabOrder = 2
            end
            inherited cbShowRemovedEmployees: TevCheckBox
              Left = 561
              TabOrder = 3
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 760
            Height = 563
            inherited Splitter1: TevSplitter
              Height = 559
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 559
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 479
                IniAttributes.SectionName = 'TEDIT_EE_HR_PERFORMANCE_RATINGS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 438
              Height = 559
              inherited wwDBGrid4: TevDBGrid
                Width = 388
                Height = 479
                IniAttributes.SectionName = 'TEDIT_EE_HR_PERFORMANCE_RATINGS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Width = 812
        Height = 674
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 570
          Height = 306
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 554
            Height = 290
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Top = 36
              Width = 522
              Height = 234
              Align = alClient
              inherited evDBGrid1: TevDBGrid
                Width = 522
                Height = 234
                ControlType.Strings = (
                  'INCREASE_TYPE;CustomEdit;evDBComboBox2;F')
                Selected.Strings = (
                  'PerformanceLookup'#9'20'#9'Performance Rating'#9'F'
                  'ReasonLookup'#9'21'#9'Reason Code'#9'F'
                  'INCREASE_TYPE'#9'9'#9'Change Type'#9'F'
                  'INCREASE_AMOUNT'#9'9'#9'Change Value'#9'F'
                  'INCREASE_EFFECTIVE_DATE'#9'15'#9'Effective Date'#9'F'
                  'OVERALL_RATING'#9'13'#9'Overall Rating'#9'F'
                  'REVIEW_DATE'#9'11'#9'Review Date'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_HR_PERFORMANCE_RATINGS\evDBGrid1'
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = False
              end
            end
          end
        end
        object fpPerformanceRatingDetails: TisUIFashionPanel
          Left = 10
          Top = 304
          Width = 553
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPerformanceRatingDetails'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Performance Rating Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel9: TevLabel
            Left = 12
            Top = 152
            Width = 28
            Height = 13
            Caption = 'Notes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel8: TevLabel
            Left = 382
            Top = 74
            Width = 118
            Height = 13
            Caption = '~Review Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel7: TevLabel
            Left = 382
            Top = 35
            Width = 67
            Height = 13
            Caption = 'Overall Rating'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel6: TevLabel
            Left = 219
            Top = 35
            Width = 89
            Height = 15
            Caption = '~Change Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel
            Left = 219
            Top = 113
            Width = 145
            Height = 15
            Caption = '~Change Effective Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 219
            Top = 74
            Width = 104
            Height = 15
            Caption = '~Change Value'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 35
            Width = 118
            Height = 13
            Caption = '~Performance Rating'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 113
            Width = 65
            Height = 13
            Caption = '~Supervisor'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel10: TevLabel
            Left = 382
            Top = 113
            Width = 145
            Height = 13
            Caption = 'Rate Number (blank for Salary)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 74
            Width = 88
            Height = 13
            Caption = '~Reason Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EvDBMemo1: TEvDBMemo
            Left = 12
            Top = 167
            Width = 520
            Height = 58
            DataField = 'NOTES'
            DataSource = wwdsDetail
            TabOrder = 9
          end
          object evDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 201
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'SUPERVISOR_NAME'#9'40'#9'SUPERVISOR_NAME'#9'F')
            DataField = 'CO_HR_SUPERVISORS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_SUPERVISORS.CO_HR_SUPERVISORS
            LookupField = 'CO_HR_SUPERVISORS_NBR'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBEdit3: TevDBEdit
            Left = 382
            Top = 128
            Width = 150
            Height = 21
            DataField = 'RATE_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit2: TevDBEdit
            Left = 382
            Top = 50
            Width = 150
            Height = 21
            DataField = 'OVERALL_RATING'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit1: TevDBEdit
            Left = 219
            Top = 89
            Width = 154
            Height = 21
            DataField = 'INCREASE_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 382
            Top = 89
            Width = 150
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'REVIEW_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 219
            Top = 128
            Width = 154
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'INCREASE_EFFECTIVE_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 7
          end
          object evDBComboBox1: TevDBComboBox
            Left = 219
            Top = 50
            Width = 154
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'INCREASE_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Amount'#9'C'
              'Additional Amount'#9'A'
              'Percent'#9'P')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object cbReasonCode: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 200
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME'#9'F')
            DataField = 'CL_HR_REASON_CODES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_HR_REASON_CODES.CL_HR_REASON_CODES
            LookupField = 'CL_HR_REASON_CODES_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbPerformanceRating: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 201
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'CO_HR_PERFORMANCE_RATINGS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_PERFORMANCE_RATINGS.CO_HR_PERFORMANCE_RATINGS
            LookupField = 'CO_HR_PERFORMANCE_RATINGS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 820
    object bApply: TevBitBtn
      Left = 597
      Top = 3
      Width = 132
      Height = 25
      Caption = 'Change Rates'
      Enabled = False
      TabOrder = 4
      OnClick = miApplyfordaterangeClick
      NumGlyphs = 2
      Margin = 0
    end
  end
  object evDBComboBox2: TevDBComboBox [3]
    Left = 208
    Top = 156
    Width = 73
    Height = 21
    TabStop = False
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'INCREASE_TYPE'
    DataSource = wwdsDetail
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Amount'#9'C'
      'Additional Amount'#9'A'
      'Percent'#9'P')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_HR_PERFORMANCE_RATINGS.EE_HR_PERFORMANCE_RATINGS
  end
  inherited dsCL: TevDataSource
    Top = 55
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 352
    Top = 47
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 442
    Top = 66
  end
  inherited wwdsEmployee: TevDataSource
    Left = 206
    Top = 64
  end
  inherited ActionList: TevActionList
    Left = 387
    Top = 51
  end
end
