inherited EDIT_EE_HR_CO_PROVIDED_EDUCATN: TEDIT_EE_HR_CO_PROVIDED_EDUCATN
  Width = 903
  Height = 660
  inherited Panel1: TevPanel
    Width = 903
    inherited pnlFavoriteReport: TevPanel
      Left = 751
    end
    inherited pnlTopLeft: TevPanel
      Width = 751
    end
  end
  inherited PageControl1: TevPageControl
    Width = 903
    Height = 573
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 768
        Height = 625
        inherited pnlBorder: TevPanel
          Width = 764
          Height = 621
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 748
          Height = 605
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Height = 344
            end
            inherited pnlSubbrowse: TevPanel
              Height = 342
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Height = 342
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 342
                IniAttributes.SectionName = 'TEDIT_EE_HR_CO_PROVIDED_EDUCATN\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 342
                IniAttributes.SectionName = 'TEDIT_EE_HR_CO_PROVIDED_EDUCATN\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Width = 895
        Height = 544
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 717
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 701
            Height = 225
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Top = 40
              Width = 668
              inherited evDBGrid1: TevDBGrid
                Width = 668
                ControlType.Strings = (
                  'NOTES;CustomEdit;EvDBMemo2;F')
                Selected.Strings = (
                  'CHECK_ISSUED_BY'#9'3'#9'Check Issued By'#9'F'
                  'CHECK_NUMBER'#9'12'#9'Check Number'#9'F'
                  'COMPANY_COST'#9'12'#9'Company Cost'#9'F'
                  'COMPLETED'#9'1'#9'Completed'#9'F'
                  'COURSE_DESC'#9'20'#9'Course'#9'F'
                  'DATE_FINISH'#9'18'#9'Date Finish'#9'F'
                  'DATE_ISSUED'#9'18'#9'Date Issued'#9'F'
                  'DATE_START'#9'18'#9'Date Start'#9'F'
                  'TAXABLE'#9'1'#9'Taxable'#9'F'
                  'CREDIT_HOURS'#9'10'#9'Credit Hours'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_HR_CO_PROVIDED_EDUCATN\evDBGrid1'
              end
            end
          end
        end
        object fpProvidedEducation: TisUIFashionPanel
          Left = 8
          Top = 241
          Width = 458
          Height = 173
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpProvidedEducation'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Provided Education Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel2: TevLabel
            Left = 12
            Top = 113
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = 'Course'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 228
            Top = 35
            Width = 48
            Height = 13
            Alignment = taRightJustify
            Caption = 'Date Start'
          end
          object evLabel4: TevLabel
            Left = 228
            Top = 74
            Width = 53
            Height = 13
            Alignment = taRightJustify
            Caption = 'Date Finish'
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 74
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = 'School'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel8: TevLabel
            Left = 336
            Top = 113
            Width = 68
            Height = 13
            Alignment = taRightJustify
            Caption = 'Company Cost'
          end
          object evLabel13: TevLabel
            Left = 228
            Top = 113
            Width = 58
            Height = 13
            Caption = 'Credit Hours'
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 228
            Top = 50
            Width = 208
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DATE_START'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 228
            Top = 89
            Width = 208
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DATE_FINISH'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 120
            Top = 35
            Width = 100
            Height = 36
            HelpContext = 19002
            Caption = '~Taxable'
            Columns = 2
            DataField = 'TAXABLE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 100
            Height = 36
            HelpContext = 19002
            Caption = '~Completed'
            Columns = 2
            DataField = 'COMPLETED'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBEdit3: TevDBEdit
            Left = 336
            Top = 128
            Width = 100
            Height = 21
            DataField = 'COMPANY_COST'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 208
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            DataField = 'CL_HR_SCHOOL_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_HR_SCHOOL.CL_HR_SCHOOL
            LookupField = 'CL_HR_SCHOOL_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evcbCOURSE: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 208
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            DataField = 'CL_HR_COURSE_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_HR_COURSE.CL_HR_COURSE
            LookupField = 'CL_HR_COURSE_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = evcbCOURSEChange
          end
          object evDBEdit1: TevDBEdit
            Left = 228
            Top = 128
            Width = 100
            Height = 21
            DataField = 'CREDIT_HOURS'
            DataSource = wwdsDetail
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        object fpProvidedEdCheck: TisUIFashionPanel
          Left = 8
          Top = 422
          Width = 225
          Height = 173
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpProvidedEdCheck'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payment'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel9: TevLabel
            Left = 12
            Top = 35
            Width = 80
            Height = 13
            Caption = 'Check Issued By'
          end
          object evLabel7: TevLabel
            Left = 12
            Top = 75
            Width = 71
            Height = 13
            Caption = 'Check Number'
          end
          object evLabel6: TevLabel
            Left = 12
            Top = 114
            Width = 57
            Height = 13
            Alignment = taRightJustify
            Caption = 'Date Issued'
          end
          object evDBEdit5: TevDBEdit
            Left = 12
            Top = 51
            Width = 191
            Height = 21
            DataField = 'CHECK_ISSUED_BY'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit4: TevDBEdit
            Left = 12
            Top = 90
            Width = 191
            Height = 21
            DataField = 'CHECK_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 12
            Top = 129
            Width = 191
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DATE_ISSUED'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
        end
        object fpProvidedEdRenewal: TisUIFashionPanel
          Left = 241
          Top = 422
          Width = 225
          Height = 173
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpProvidedEdRenewal'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Renewal'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel10: TevLabel
            Left = 12
            Top = 35
            Width = 75
            Height = 13
            Caption = 'Renewal Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel11: TevLabel
            Left = 12
            Top = 114
            Width = 91
            Height = 13
            Caption = 'Last Renewal Date'
          end
          object evLabel12: TevLabel
            Left = 12
            Top = 75
            Width = 68
            Height = 13
            Caption = 'Renewal Date'
          end
          object evDBDateTimePicker4: TevDBDateTimePicker
            Left = 12
            Top = 129
            Width = 191
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'LAST_REVIEW_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object evDBDateTimePicker5: TevDBDateTimePicker
            Left = 12
            Top = 90
            Width = 191
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'RENEWAL_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object wwcbRENEWAL_STATUS: TevDBComboBox
            Left = 12
            Top = 50
            Width = 191
            Height = 21
            HelpContext = 39003
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'RENEWAL_STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
        end
        object fpProvidedEdNotes: TisUIFashionPanel
          Left = 476
          Top = 241
          Width = 235
          Height = 353
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpProvidedEdNotes'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel5: TevLabel
            Left = 12
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object EvDBMemo1: TEvDBMemo
            Left = 12
            Top = 50
            Width = 200
            Height = 282
            DataField = 'NOTES'
            DataSource = wwdsDetail
            TabOrder = 0
          end
        end
      end
      inherited pnlDBControls: TevPanel
        Left = 760
        Top = 158
        Align = alNone
        Visible = False
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 903
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_HR_CO_PROVIDED_EDUCATN.EE_HR_CO_PROVIDED_EDUCATN
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 518
    Top = 157
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 413
    Top = 162
  end
end
