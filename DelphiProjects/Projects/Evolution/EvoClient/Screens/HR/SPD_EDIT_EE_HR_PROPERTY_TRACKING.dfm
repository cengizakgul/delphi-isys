inherited EDIT_EE_HR_PROPERTY_TRACKING: TEDIT_EE_HR_PROPERTY_TRACKING
  Width = 687
  Height = 437
  inherited Panel1: TevPanel
    Width = 687
    inherited pnlFavoriteReport: TevPanel
      Left = 535
    end
    inherited pnlTopLeft: TevPanel
      Width = 535
    end
  end
  inherited PageControl1: TevPageControl
    Width = 687
    Height = 350
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 679
        Height = 321
        inherited pnlBorder: TevPanel
          Width = 675
          Height = 317
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 675
          Height = 317
          inherited Panel3: TevPanel
            Width = 627
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 627
            Height = 210
            inherited Splitter1: TevSplitter
              Height = 206
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 206
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 126
                IniAttributes.SectionName = 'TEDIT_EE_HR_PROPERTY_TRACKING\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 305
              Height = 206
              inherited wwDBGrid4: TevDBGrid
                Width = 255
                Height = 126
                IniAttributes.SectionName = 'TEDIT_EE_HR_PROPERTY_TRACKING\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Width = 679
        Height = 321
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 495
          Height = 227
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 479
            Height = 211
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 435
              inherited evDBGrid1: TevDBGrid
                Width = 435
                Selected.Strings = (
                  'Property'#9'30'#9'Property'#9'F'
                  'ISSUED_DATE'#9'18'#9'Issued Date'#9'F'
                  'SERIAL_NUMBER'#9'40'#9'Serial Number'#9'F'
                  'RENEWAL_DATE'#9'18'#9'Renewal Date'#9'F'
                  'RETURNED_DATE'#9'18'#9'Returned Date'#9'F'
                  'LOST_DATE'#9'18'#9'Lost Date'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_HR_PROPERTY_TRACKING\evDBGrid1'
              end
            end
          end
        end
        object fpPropertyTracking: TisUIFashionPanel
          Left = 8
          Top = 232
          Width = 479
          Height = 143
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPropertyTracking'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Property Tracking Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel6: TevLabel
            Left = 160
            Top = 35
            Width = 66
            Height = 13
            Caption = 'Serial Number'
          end
          object evLabel5: TevLabel
            Left = 160
            Top = 75
            Width = 68
            Height = 13
            Caption = 'Renewal Date'
          end
          object evLabel4: TevLabel
            Left = 308
            Top = 35
            Width = 70
            Height = 13
            Alignment = taRightJustify
            Caption = 'Returned Date'
          end
          object evLabel3: TevLabel
            Left = 308
            Top = 75
            Width = 46
            Height = 13
            Caption = 'Lost Date'
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 75
            Width = 57
            Height = 13
            Caption = 'Issued Date'
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 73
            Height = 13
            Alignment = taRightJustify
            Caption = '~Property'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 140
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'PROPERTY_DESCRIPTION'#9'40'#9'Property Description'#9'F')
            DataField = 'CO_HR_PROPERTY_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_PROPERTY.CO_HR_PROPERTY
            LookupField = 'CO_HR_PROPERTY_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBDateTimePicker4: TevDBDateTimePicker
            Left = 160
            Top = 90
            Width = 140
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'RENEWAL_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 308
            Top = 50
            Width = 140
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'RETURNED_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 308
            Top = 90
            Width = 140
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'LOST_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 12
            Top = 90
            Width = 140
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'ISSUED_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
          end
          object dbedSerial: TevDBEdit
            Left = 160
            Top = 50
            Width = 140
            Height = 21
            DataField = 'SERIAL_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
      inherited pnlDBControls: TevPanel
        Left = 328
        Top = 46
        Align = alNone
        Visible = False
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 687
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_HR_PROPERTY_TRACKING.EE_HR_PROPERTY_TRACKING
  end
end
