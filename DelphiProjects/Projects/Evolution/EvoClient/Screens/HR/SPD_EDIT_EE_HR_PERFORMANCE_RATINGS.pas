// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_PERFORMANCE_RATINGS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_EE_HR_SUBBROWSE_BASE, DB,  EvConsts, EvContext,
   DBActns, ActnList, SDataStructure, Wwdatsrc, StdCtrls,
  wwdblook, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Wwdotdot, Wwdbcomb, wwdbdatetimepicker, Mask, wwdbedit, Menus, EvUtils,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, SDDClasses, 
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, SPackageEntry,EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBComboBox, isUIwwDBDateTimePicker, isUIwwDBEdit,
  ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_HR_PERFORMANCE_RATINGS = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    cbPerformanceRating: TevDBLookupCombo;
    evLabel3: TevLabel;
    evLabel1: TevLabel;
    cbReasonCode: TevDBLookupCombo;
    evLabel2: TevLabel;
    evDBLookupCombo3: TevDBLookupCombo;
    evLabel4: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel5: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evLabel6: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evLabel7: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel8: TevLabel;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evLabel9: TevLabel;
    EvDBMemo1: TEvDBMemo;
    evLabel10: TevLabel;
    evDBEdit3: TevDBEdit;
    evDBComboBox2: TevDBComboBox;
    bApply: TevBitBtn;
    procedure miApplyfordaterangeClick(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
  private

  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses SelectEEs, SPD_EDIT_EE_BASE, SPD_EDIT_Open_BASE;

{$R *.dfm}

{ TEDIT_EE_HR_PERFORMANCE_RATINGS }

procedure TEDIT_EE_HR_PERFORMANCE_RATINGS.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_PERSON, aDS);
  AddDS(DM_COMPANY.CO_HR_PERFORMANCE_RATINGS, aDS);
  AddDS(DM_CLIENT.CL_HR_REASON_CODES, aDS);
  AddDS(DM_COMPANY.CO_HR_SUPERVISORS, aDS);
  AddDS(DM_EMPLOYEE.EE_RATES, aDS);
  AddDS(DM_EMPLOYEE.EE, aDS);
  inherited;
end;

function TEDIT_EE_HR_PERFORMANCE_RATINGS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS;
end;

procedure TEDIT_EE_HR_PERFORMANCE_RATINGS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDS( DM_CLIENT.CL_PERSON, SupportDataSets);
  AddDS( DM_CLIENT.CL_HR_REASON_CODES, SupportDataSets);
  AddDS( DM_COMPANY.CO_HR_SUPERVISORS, SupportDataSets);
  AddDS( DM_EMPLOYEE.EE_RATES, SupportDataSets);
  inherited;
  AddDS( DM_EMPLOYEE.EE, EditDataSets);
  AddDS( DM_COMPANY.CO_HR_PERFORMANCE_RATINGS, EditDataSets);
end;

procedure TEDIT_EE_HR_PERFORMANCE_RATINGS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind in [NavOK] then
  begin
   if wwdsDetail.DataSet.State in [dsInsert] then
   begin
     DM_EMPLOYEE.EE.Edit;
     DM_EMPLOYEE.EE['CO_HR_PERFORMANCE_RATINGS_NBR'] := wwdsDetail.DataSet['CO_HR_PERFORMANCE_RATINGS_NBR'];
     DM_EMPLOYEE.EE['REVIEW_DATE'] := wwdsDetail.DataSet['REVIEW_DATE'];
   end;
  end;
end;


procedure TEDIT_EE_HR_PERFORMANCE_RATINGS.miApplyfordaterangeClick(
  Sender: TObject);
var
  sErr: string;
  i: Integer;
  DS: TevClientDataSet;
  sField: string;
  r: Integer;
begin
  sErr := '';
  inherited;
  if ctx_AccountRights.Functions.GetState('USER_CAN_GLOBALLY_UPDATE_PAY_RATES') <> stEnabled then
     raise ENoRights.CreateHelp('You have no security rights', IDH_SecurityViolation);
  SaveDSStates([DM_EMPLOYEE.EE]);
  DM_EMPLOYEE.EE.EnableControls;
  with TSelEEs.Create(nil) do
  try
    evDataSource1.DataSet := DM_EMPLOYEE.EE;
    evDBGrid1.SelectAll;
    dtBegin.Date := Date;
    dtEnd.Date := Date + 6;
    if ShowModal = mrOK then
    begin
      SaveDSStates([DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS]);

      DM_EMPLOYEE.EE.DisableControls;
      DM_EMPLOYEE.EE_RATES.DisableControls;
      DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.DisableControls;

      DM_EMPLOYEE.EE.LookupsEnabled := False;
      DM_EMPLOYEE.EE_RATES.LookupsEnabled := False;
      DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.LookupsEnabled := False;

      DM_EMPLOYEE.EE_RATES.IndexFieldNames := 'EE_NBR;RATE_NUMBER';
      DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.RetrieveCondition := '';
      DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.IndexFieldNames := 'EE_NBR';
      DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.Filter := 'INCREASE_EFFECTIVE_DATE >= ''' + DateToStr(dtBegin.Date) + ''' and INCREASE_EFFECTIVE_DATE <= ''' + DateToStr(dtEnd.Date) + '''';
      DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.Filtered := True;

      ctx_StartWait('', Pred(evDBGrid1.SelectedList.Count));
      try
        for i := 0 to Pred(evDBGrid1.SelectedList.Count) do
        begin
          ctx_UpdateWait('Applying employee ' + DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, i);
          DM_EMPLOYEE.EE.GotoBookmark(evDBGrid1.SelectedList[i]);

          DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FindKey([DM_EMPLOYEE.EE['EE_NBR']]);
          while (DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['EE_NBR'] = DM_EMPLOYEE.EE['EE_NBR']) and
                not DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.Eof do
          begin
            if DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FieldByName('RATE_NUMBER').IsNull then
            begin
              if DM_EMPLOYEE.EE.FieldByName('SALARY_AMOUNT').AsCurrency <> 0 then
              begin
                DS := DM_EMPLOYEE.EE;
                sField := 'SALARY_AMOUNT';
                r := 2;
              end
              else
              begin
                sErr := sErr + 'Employee ' + DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString +
                              ' does not have a salary'#13#10;
                sField := '';
                DS := nil;
                r := 0;
              end;
            end
            else if DM_EMPLOYEE.EE_RATES.FindKey([DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['EE_NBR'], DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['RATE_NUMBER']]) then
            begin
              DS := DM_EMPLOYEE.EE_RATES;
              sField := 'RATE_AMOUNT';
              r := 4;
            end
            else
            begin
              sErr := sErr + 'Employee ' + DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString +
                            ' Rate number ' + DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FieldByName('RATE_NUMBER').AsString + ' not found'#13#10;
              sField := '';
              DS := nil;
              r := 0;
            end;

            if (sField <> '') and
               (DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['INCREASE_TYPE'] <> PERFORMANCE_RATINGS_Type_NoChange) then
            begin
              DS.Edit;
              if DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['INCREASE_TYPE'] = PERFORMANCE_RATINGS_Type_Amount then
                DS[sField] := RoundAll(DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FieldByName('INCREASE_AMOUNT').AsFloat, r)
              else if DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS['INCREASE_TYPE'] = PERFORMANCE_RATINGS_Type_AddAmount then
                DS[sField] := RoundAll(DS.FieldByName(sField).AsFloat + DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FieldByName('INCREASE_AMOUNT').AsFloat, r)
              else
                DS[sField] := RoundAll(DS.FieldByName(sField).AsFloat * (1 + DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.FieldByName('INCREASE_AMOUNT').AsFloat/100), r);
              DS.Post;
            end;

            DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.Next;
          end;
        end;
        ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_RATES]);
      finally
        DM_EMPLOYEE.EE.LookupsEnabled := True;
        DM_EMPLOYEE.EE_RATES.LookupsEnabled := True;
        DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.LookupsEnabled := True;

        DM_EMPLOYEE.EE.EnableControls;
        DM_EMPLOYEE.EE_RATES.EnableControls;
        DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS.EnableControls;

        ctx_EndWait;
        LoadDSStates([DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_HR_PERFORMANCE_RATINGS]);
      end;
      if sErr <> '' then
        EvErrMessage(sErr)
      else
        EvMessage('Done!');
    end;
  finally
    Free;
    DM_EMPLOYEE.EE.DisableControls;
    LoadDSStates([DM_EMPLOYEE.EE]);
  end;
end;

procedure TEDIT_EE_HR_PERFORMANCE_RATINGS.wwdsListDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if not (csLoading in ComponentState) then
    bApply.Enabled := (not BitBtn1.Enabled) and (ctx_AccountRights.Functions.GetState('USER_CAN_GLOBALLY_UPDATE_PAY_RATES') = stEnabled);
end;

initialization
  RegisterClass(TEDIT_EE_HR_PERFORMANCE_RATINGS);

end.
