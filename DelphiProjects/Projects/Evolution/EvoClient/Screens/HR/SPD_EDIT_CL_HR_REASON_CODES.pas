// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_REASON_CODES;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, SPD_EDIT_CL_BASE, SDataStructure, DB, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Mask, wwdbedit, EvUIComponents, EvClientDataSet, isUIwwDBEdit,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_HR_REASON_CODES = class(TEDIT_CL_BASE)
    TabSheet2: TTabSheet;
    wwDBGrid1: TevDBGrid;
    evDBGrid1: TevDBGrid;
    fpReasonsSummary: TisUIFashionPanel;
    sbReasons: TScrollBox;
    evPanel2: TevPanel;
    pnlReasonBody: TevPanel;
    fpReasonsDetail: TisUIFashionPanel;
    pnlDetails: TevPanel;
    evLabel1: TevLabel;
    edName: TevDBEdit;
    sbSkillsBrowse: TScrollBox;
    pnlSkillsBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlSkillsBrowseLeft: TevPanel;
    fpSkillsLeft: TisUIFashionPanel;
    pnlFPSkillsLeftBody: TevPanel;
    pnlSkillsBrowseRight: TevPanel;
    fpSkillsRight: TisUIFashionPanel;
    pnlFPSkillsRightBody: TevPanel;
    procedure wwDBGrid1DblClick(Sender: TObject);
  private
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.dfm}

{ TEDIT_CL_HR_REASON_CODES }

function TEDIT_CL_HR_REASON_CODES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_REASON_CODES;
end;

function TEDIT_CL_HR_REASON_CODES.GetInsertControl: TWinControl;
begin
  Result := edName;
end;

procedure TEDIT_CL_HR_REASON_CODES.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CL_HR_REASON_CODES.Activate;
begin
  inherited;

end;

initialization
  RegisterClass(TEDIT_CL_HR_REASON_CODES);

end.
