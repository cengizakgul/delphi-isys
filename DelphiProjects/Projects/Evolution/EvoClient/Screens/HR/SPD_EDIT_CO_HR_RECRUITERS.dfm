inherited EDIT_CO_HR_RECRUITERS: TEDIT_CO_HR_RECRUITERS
  inherited PageControl1: TevPageControl
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Recruiters'
                    end
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 220
                IniAttributes.SectionName = 'TEDIT_CO_HR_RECRUITERS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Recruiters'
              inherited evDBGrid1: TevDBGrid
                Height = 220
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_RECRUITERS\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        inherited pnlevBorder: TevPanel
          inherited pnlFashionDetails: TisUIFashionPanel
            Width = 320
            inherited pnlFashionDetailBody: TevPanel
              Width = 288
              Height = 226
              Align = alClient
              inherited pnlBrowse: TevPanel
                Width = 288
                Height = 226
                inherited evDBGrid2: TevDBGrid
                  Width = 288
                  Height = 226
                  Selected.Strings = (
                    'NAME'#9'41'#9'Name'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_RECRUITERS\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Width = 320
            Height = 94
            Title = 'Recruiter Details'
            inherited pnlFPDBControlsBody: TevPanel
              Width = 293
              Height = 41
              inherited pnlDBControls: TevPanel
                Top = 0
                Width = 293
                Height = 41
                inherited evLabel1: TevLabel
                  Left = 0
                  Top = 0
                  Caption = '~Name'
                end
                inherited dedtDescription: TevDBEdit
                  Left = 0
                  Top = 15
                  Width = 283
                  DataField = 'NAME'
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*''*~}],@}'
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_RECRUITERS.CO_HR_RECRUITERS
  end
end