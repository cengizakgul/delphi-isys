// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_ATTENDANCE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, Db,  
  DBActns, ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdbdatetimepicker, Mask, wwdbedit, EvUIComponents, EvClientDataSet,
  isUIDBMemo, ISBasicClasses, isUIwwDBDateTimePicker, isUIwwDBEdit,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, SDataDictclient, ImgList,
  SDataDicttemp, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_HR_ATTENDANCE = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBEdit4: TevDBEdit;
    evLabel7: TevLabel;                         
    drgpPrimary_Rate: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    EvDBMemo1: TEvDBMemo;
    evLabel5: TevLabel;
    fpHRAttendanceDetail: TisUIFashionPanel;
    evDBEdit1: TevDBEdit;
    evLabel2: TevLabel;
  private
    { Private declarations }
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  end;

implementation

uses
  evutils, SPD_EDIT_EE_HR_BASE, SPD_EDIT_Open_BASE;

{$R *.DFM}
 //
{ TEDIT_EE_HR_ATTENDANCE }

procedure TEDIT_EE_HR_ATTENDANCE.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS( DM_COMPANY.CO_HR_ATTENDANCE_TYPES, aDS ); 
end;

function TEDIT_EE_HR_ATTENDANCE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_HR_ATTENDANCE;
end;

function TEDIT_EE_HR_ATTENDANCE.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;


initialization
  RegisterClass( TEDIT_EE_HR_ATTENDANCE );

end.
