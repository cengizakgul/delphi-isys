inherited EDIT_CO_HR_REFERRALS: TEDIT_CO_HR_REFERRALS
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Referrals'
                    end
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 213
                IniAttributes.SectionName = 'TEDIT_CO_HR_REFERRALS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Referrals'
              inherited evDBGrid1: TevDBGrid
                Height = 213
                IniAttributes.SectionName = 'TEDIT_CO_HR_REFERRALS\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        Width = 589
        Height = 408
        inherited pnlevBorder: TevPanel
          Width = 585
          Height = 404
          inherited pnlFashionDetails: TisUIFashionPanel
            Width = 320
            inherited pnlFashionDetailBody: TevPanel
              Width = 288
              Height = 226
              Align = alClient
              inherited pnlBrowse: TevPanel
                Width = 288
                Height = 226
                inherited evDBGrid2: TevDBGrid
                  Width = 288
                  Height = 226
                  Selected.Strings = (
                    'DESCRIPTION'#9'41'#9'Description'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_REFERRALS\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Width = 320
            Height = 94
            Title = 'Referral Details'
            inherited pnlFPDBControlsBody: TevPanel
              Height = 43
              inherited pnlDBControls: TevPanel
                Top = 0
                Height = 43
                inherited evLabel1: TevLabel
                  Left = 0
                  Top = 0
                end
                inherited dedtDescription: TevDBEdit
                  Left = 0
                  Top = 15
                  Width = 283
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*''*~}],@}'
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_REFERRALS.CO_HR_REFERRALS
  end
  inherited PageControlImages: TevImageList
    Left = 368
    Top = 184
  end
end