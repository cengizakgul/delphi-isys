// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sHRScr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPackageEntry, ExtCtrls, Menus,  ImgList, EvCommonInterfaces,
  ComCtrls, ToolWin, StdCtrls, Buttons, ActnList, EvTypes, EvUtils, SDataStructure,
  ISBasicClasses, EvMainboard, EvContext, EvExceptions, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  THRFrm = class(TFramePackageTmpl, IevHRScreens)
  protected
    function InitPackage: Boolean; override;
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
    function ActivatePackage(WorkPlace: TWinControl): Boolean; override;
  end;


implementation

{$R *.DFM}

var
  HRFrm: THRFrm;

function GetHRScreens: IevHRScreens;
begin
  if not Assigned(HRFrm) then
    HRFrm := THRFrm.Create(nil);
  Result := HRFrm;
end;


{ THRFrm }

function THRFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;


function THRFrm.InitPackage: Boolean;
begin
  if Context.License.HR then
    Result := inherited InitPackage
  else
    Result := False;  
end;

function THRFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);//gdy13
end;


function THRFrm.PackageCaption: string;
begin
  Result := '&HR Module';
end;

function THRFrm.PackageSortPosition: string;
begin
  Result := '070';
end;

procedure THRFrm.UserPackageStructure;
begin
  AddStructure('Client|200');
  AddStructure('Client\Skills|210', 'TEDIT_CL_HR_SKILLS' );
  AddStructure('Client\Schools|220', 'TEDIT_CL_HR_SCHOOL');
  AddStructure('Client\Reason Codes|230', 'TEDIT_CL_HR_REASON_CODES');
  AddStructure('Client\Courses|240', 'TEDIT_CL_HR_COURSE');
  
  AddStructure('Company|300');
  AddStructure('Company\Attendance|310', 'TEDIT_CO_HR_ATTENDANCE_TYPES');
  AddStructure('Company\Vehicle|320', 'TEDIT_CO_HR_CAR');
  AddStructure('Company\Performance Ratings|330', 'TEDIT_CO_HR_PERFORMANCE_RATINGS');
  AddStructure('Company\Property|350', 'TEDIT_CO_HR_PROPERTY');
  AddStructure('Company\Recruiters|360', 'TEDIT_CO_HR_RECRUITERS');
  AddStructure('Company\Referrals|370', 'TEDIT_CO_HR_REFERRALS');
  AddStructure('Company\Pay Grades|380', 'TEDIT_CO_HR_SALARY_GRADES');
  AddStructure('Company\Supervisors|390', 'TEDIT_CO_HR_SUPERVISORS');
  AddStructure('Company\HRSentry|395', 'TEDIT_CO_HR_SENTRY');

  AddStructure('Employee\|500');
  AddStructure('Employee\Benefits|505', 'TEDIT_EE_BENEFITS');
  AddStructure('Employee\Attendance|510', 'TEDIT_EE_HR_ATTENDANCE');
  AddStructure('Employee\Vehicle|520', 'TEDIT_EE_HR_CAR');
  AddStructure('Employee\Performance Ratings|525', 'TEDIT_EE_HR_PERFORMANCE_RATINGS');
  AddStructure('Employee\Property Tracking|530', 'TEDIT_EE_HR_PROPERTY_TRACKING');
  AddStructure('Employee\Provided Education|540', 'TEDIT_EE_HR_CO_PROVIDED_EDUCATN');
  AddStructure('Employee\Injury Occurrence|550', 'TEDIT_EE_HR_INJURY_OCCURRENCE');
  AddStructure('Employee\Education|560', 'TEDIT_CL_HR_EE_EDUCATION');
  AddStructure('Employee\Disabilities|565', 'TEDIT_CL_HR_EE_HANDICAPS');
  AddStructure('Employee\Skills|570', 'TEDIT_CL_HR_EE_SKILLS');
  AddStructure('Employee\Emergency Contacts|590', 'TEDIT_EE_EMERGENCY_CONTACTS');
  AddStructure('Employee\HR Applicant\Applicants|610', 'TEDIT_CO_HR_APPLICANT');
  AddStructure('Employee\HR Applicant\Education|620', 'TEDIT_CL_HR_APPLICANT_EDUCATION');
  AddStructure('Employee\HR Applicant\Disabilities|630', 'TEDIT_CL_HR_APPLICANT_HANDICAPS');
  AddStructure('Employee\HR Applicant\Skills|640', 'TEDIT_CL_HR_APPLICANT_SKILLS');

end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetHRScreens, IevHRScreens, 'Screens HR');

finalization
  FreeAndNil(HRFrm);

end.
