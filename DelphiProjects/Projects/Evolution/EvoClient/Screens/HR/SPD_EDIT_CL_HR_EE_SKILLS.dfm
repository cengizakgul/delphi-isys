inherited EDIT_CL_HR_EE_SKILLS: TEDIT_CL_HR_EE_SKILLS
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Width = 613
              Height = 359
            end
            inherited pnlSubbrowse: TevPanel
              Width = 288
              Height = 357
            end
            inherited sbEESunkenBrowse2: TScrollBox
              inherited pnlBrowseBorder: TevPanel
                inherited fpEEBrowseRight2: TisUIFashionPanel
                  Title = 'Skills'
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 277
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_SKILLS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Skills'
              inherited wwDBGrid4: TevDBGrid
                Height = 277
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_SKILLS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 406
          Height = 467
          Align = alClient
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 293
            Height = 269
            Align = alNone
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 256
              Height = 207
              inherited evDBGrid1: TevDBGrid
                Width = 256
                Height = 207
                Selected.Strings = (
                  'Skill'#9'21'#9'Skill'#9'F'
                  'YEARS_EXPERIENCE'#9'10'#9'Years Experience'#9'F')
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_SKILLS\evDBGrid1'
              end
            end
          end
        end
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 8
          Top = 292
          Width = 293
          Height = 175
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'isUIFashionPanel1'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Skill Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 36
            Width = 44
            Height = 13
            Caption = '~Skill'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 74
            Width = 28
            Height = 13
            Caption = 'Notes'
            Transparent = False
          end
          object evLabel3: TevLabel
            Left = 171
            Top = 36
            Width = 83
            Height = 13
            Caption = 'Years Experience'
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 150
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'CL_HR_SKILLS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_HR_SKILLS.CL_HR_SKILLS
            LookupField = 'CL_HR_SKILLS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object EvDBMemo1: TEvDBMemo
            Left = 12
            Top = 89
            Width = 259
            Height = 64
            DataField = 'NOTES'
            DataSource = wwdsDetail
            TabOrder = 2
          end
          object evDBEdit1: TevDBEdit
            Left = 171
            Top = 50
            Width = 100
            Height = 21
            DataField = 'YEARS_EXPERIENCE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_HR_PERSON_SKILLS.CL_HR_PERSON_SKILLS
    MasterDataSource = wwdsSubMaster2
  end
end
