// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_PERFORMANCE_RATINGS;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Mask, wwdbedit, SPD_EDIT_CO_HR_DESCR_BASE, SPD_EDIT_CO_BASE, EvUIComponents, EvClientDataSet,
  SDDClasses, SDataDictclient, ImgList, ISBasicClasses, SDataDicttemp,
  isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_PERFORMANCE_RATINGS = class(TEDIT_CO_HR_DESCR_BASE)
    procedure fpBaseDBControlsResize(Sender: TObject);
    procedure pnlFashionDetailsResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;

  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_PERFORMANCE_RATINGS }

function TEDIT_CO_HR_PERFORMANCE_RATINGS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_PERFORMANCE_RATINGS
end;

procedure TEDIT_CO_HR_PERFORMANCE_RATINGS.fpBaseDBControlsResize(
  Sender: TObject);
begin
//  inherited; Leave this off.

end;

procedure TEDIT_CO_HR_PERFORMANCE_RATINGS.pnlFashionDetailsResize(
  Sender: TObject);
begin
//  inherited;

end;

initialization
  RegisterClass( TEDIT_CO_HR_PERFORMANCE_RATINGS );


end.
