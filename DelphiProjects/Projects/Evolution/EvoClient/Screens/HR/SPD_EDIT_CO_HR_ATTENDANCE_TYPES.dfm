inherited EDIT_CO_HR_ATTENDANCE_TYPES: TEDIT_CO_HR_ATTENDANCE_TYPES
  Width = 597
  Height = 617
  inherited Panel1: TevPanel
    Width = 597
    inherited pnlFavoriteReport: TevPanel
      Left = 445
    end
    inherited pnlTopLeft: TevPanel
      Width = 445
    end
  end
  inherited PageControl1: TevPageControl
    Width = 597
    Height = 563
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Height = 452
        inherited pnlBorder: TevPanel
          Height = 448
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Height = 448
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Attendance'
                    end
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Height = 341
            inherited Splitter1: TevSplitter
              Height = 337
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 337
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 257
                IniAttributes.SectionName = 'TEDIT_CO_HR_ATTENDANCE_TYPES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Height = 337
              Title = 'Attendance'
              inherited evDBGrid1: TevDBGrid
                Height = 257
                Selected.Strings = (
                  'ATTENDANCE_DESCRIPTION'#9'40'#9'Attendance Description'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_ATTENDANCE_TYPES\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        Width = 589
        Height = 534
        inherited pnlevBorder: TevPanel
          Width = 585
          Height = 530
          inherited pnlFashionDetails: TisUIFashionPanel
            inherited pnlFashionDetailBody: TevPanel
              inherited pnlBrowse: TevPanel
                inherited evDBGrid2: TevDBGrid
                  Selected.Strings = (
                    'ATTENDANCE_DESCRIPTION'#9'50'#9'Attendance Description'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_ATTENDANCE_TYPES\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Height = 92
            Title = 'Attendance Details'
            inherited pnlFPDBControlsBody: TevPanel
              Top = 36
              Width = 290
              Height = 36
              Align = alClient
              inherited pnlDBControls: TevPanel
                Width = 290
                Height = 36
                inherited evLabel1: TevLabel
                  Left = 0
                  Top = 0
                end
                inherited dedtDescription: TevDBEdit
                  Left = 0
                  Top = 15
                  Width = 290
                  DataField = 'ATTENDANCE_DESCRIPTION'
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_ATTENDANCE_TYPES.CO_HR_ATTENDANCE_TYPES
  end
end
