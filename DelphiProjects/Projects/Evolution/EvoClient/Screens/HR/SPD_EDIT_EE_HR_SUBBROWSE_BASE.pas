// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_SUBBROWSE_BASE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_BASE, Db,   DBActns,
  ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvUtils,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvClientDataSet, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_HR_SUBBROWSE_BASE = class(TEDIT_EE_HR_BASE)
    evDBGrid1: TevDBGrid;
    sb_EE_HR_SUBBrowse: TScrollBox;
    pnl_EE_HR_SubbrowseBorderHeader: TevPanel;
    fp_EE_HR_Subbrowse: TisUIFashionPanel;
    pnl_EE_HR_SubbrowseBody: TevPanel;
    pnlDBControls: TevPanel;
    procedure fp_EE_HR_SubbrowseResize(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure AfterDataSetsReopen; override;
  public
    { Public declarations }
    procedure SetReadOnly(Value: Boolean); override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}

{ TEDIT_EE_HR_SUBBROWSE_BASE }

procedure TEDIT_EE_HR_SUBBROWSE_BASE.AfterDataSetsReopen;
begin
  inherited;
  SetReadOnly(GetIfReadOnly or BlockHr);
end;

procedure TEDIT_EE_HR_SUBBROWSE_BASE.SetReadOnly(Value: Boolean);
begin
  inherited;
  BitBtn1.Enabled := (wwdsList.DataSet.RecordCount > 0) and
                     ((wwdsList.KeyValue <> wwdsMaster.KeyValue) and wwdsMaster.DataSet.Active {and (wwdsMaster.DataSet.RecordCount > 0)} or
                      (wwdsList.DataSet.FieldByName('CL_NBR').AsInteger <> TevClientDataSet(wwdsMaster.DataSet).ClientID));
end;

procedure TEDIT_EE_HR_SUBBROWSE_BASE.fp_EE_HR_SubbrowseResize(
  Sender: TObject);
begin
  inherited;
  pnl_EE_HR_SubbrowseBody.Width  := fp_EE_HR_Subbrowse.Width - 40;
  pnl_EE_HR_SubbrowseBody.Height := fp_EE_HR_Subbrowse.Height - 65;
end;

end.
