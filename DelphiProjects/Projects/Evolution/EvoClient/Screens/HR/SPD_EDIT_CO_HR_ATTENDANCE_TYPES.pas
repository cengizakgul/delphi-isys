// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_ATTENDANCE_TYPES;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Mask, wwdbedit, SPD_EDIT_CO_HR_DESCR_BASE, EvUIComponents, EvClientDataSet,
  SDDClasses, SDataDictclient, ImgList, ISBasicClasses, SDataDicttemp,
  isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_ATTENDANCE_TYPES = class(TEDIT_CO_HR_DESCR_BASE)
    procedure fpBaseDBControlsResize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;

  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_ATTENDANCE_TYPES }

function TEDIT_CO_HR_ATTENDANCE_TYPES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_ATTENDANCE_TYPES
end;

procedure TEDIT_CO_HR_ATTENDANCE_TYPES.fpBaseDBControlsResize(
  Sender: TObject);
begin
//  inherited;  Leave this off.

end;

procedure TEDIT_CO_HR_ATTENDANCE_TYPES.PageControl1Change(Sender: TObject);
begin
  inherited;
  //GUI 2.0
  evDBGrid2.Columns[0].DisplayWidth := 50;
end;

initialization
  RegisterClass( TEDIT_CO_HR_ATTENDANCE_TYPES );


end.
