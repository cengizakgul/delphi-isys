inherited EDIT_CO_HR_PERFORMANCE_RATINGS: TEDIT_CO_HR_PERFORMANCE_RATINGS
  Width = 769
  Height = 630
  inherited Panel1: TevPanel
    Width = 769
    inherited pnlFavoriteReport: TevPanel
      Left = 617
    end
    inherited pnlTopLeft: TevPanel
      Width = 617
    end
  end
  inherited PageControl1: TevPageControl
    Width = 769
    Height = 576
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 761
        Height = 547
        inherited pnlBorder: TevPanel
          Width = 757
          Height = 543
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 757
          Height = 543
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Performance Ratings'
                    end
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 709
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 709
            Height = 436
            inherited Splitter1: TevSplitter
              Height = 432
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 432
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 352
                IniAttributes.SectionName = 'TEDIT_CO_HR_PERFORMANCE_RATINGS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 386
              Height = 432
              Title = 'Performance Ratings'
              inherited evDBGrid1: TevDBGrid
                Width = 336
                Height = 352
                IniAttributes.SectionName = 'TEDIT_CO_HR_PERFORMANCE_RATINGS\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        Width = 761
        Height = 547
        inherited pnlevBorder: TevPanel
          Width = 757
          Height = 543
          inherited pnlFashionDetails: TisUIFashionPanel
            inherited pnlFashionDetailBody: TevPanel
              Top = 35
              Width = 287
              Height = 225
              inherited pnlBrowse: TevPanel
                Width = 287
                Height = 225
                inherited evDBGrid2: TevDBGrid
                  Width = 287
                  Height = 225
                  Selected.Strings = (
                    'DESCRIPTION'#9'42'#9'Description'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_PERFORMANCE_RATINGS\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Height = 93
            Title = 'Performance Rating Details'
            inherited pnlFPDBControlsBody: TevPanel
              Height = 41
              inherited pnlDBControls: TevPanel
                Top = 0
                Height = 41
                inherited evLabel1: TevLabel
                  Left = 0
                  Top = 0
                end
                inherited dedtDescription: TevDBEdit
                  Left = 0
                  Top = 15
                  Width = 287
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_PERFORMANCE_RATINGS.CO_HR_PERFORMANCE_RATINGS
  end
end
