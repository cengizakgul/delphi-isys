// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_APPLICANT_SKILLS;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_APPLICANT_DESCR_BASE, SDataStructure, Db, Wwdatsrc,
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, Mask, wwdbedit, EvUIComponents, EvClientDataSet, isUIwwDBEdit,
  ISBasicClasses, isUIDBMemo, DBActns, ActnList, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, SDataDictclient, ImgList, SDataDicttemp, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_HR_APPLICANT_SKILLS = class(TEDIT_CO_HR_APPLICANT_DESCR_BASE)
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    EvDBMemo1: TEvDBMemo;
    evLabel2: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel3: TevLabel;
    sbApplicantBrowse: TScrollBox;
    evSplitter1: TevSplitter;
    pnlLeftBorder: TevPanel;
    pnlRightBorder: TevPanel;
    pnlTopBorder: TevPanel;
    pnlBottomBorder: TevPanel;
    pnlBrowseLeft: TevPanel;
    fpBrowseLeft: TisUIFashionPanel;
    pnlFPLeftBody: TevPanel;
    pnlBrowseRight: TevPanel;
    fpBrowseRight: TisUIFashionPanel;
    pnlFPRightBody: TevPanel;
    fpSkillDetail: TisUIFashionPanel;
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CL_HR_APPLICANT_SKILLS }


function TEDIT_CL_HR_APPLICANT_SKILLS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_PERSON_SKILLS;
end;

function TEDIT_CL_HR_APPLICANT_SKILLS.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

procedure TEDIT_CL_HR_APPLICANT_SKILLS.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePageIndex <> 1 then exit;

end;

procedure TEDIT_CL_HR_APPLICANT_SKILLS.Activate;
begin
  inherited;

end;

initialization
  RegisterClass( TEDIT_CL_HR_APPLICANT_SKILLS );

end.
