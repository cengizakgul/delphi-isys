inherited EDIT_EE_HR_ATTENDANCE: TEDIT_EE_HR_ATTENDANCE
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 840
        Height = 536
        inherited pnlBorder: TevPanel
          Width = 836
          Height = 532
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 820
          Height = 516
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_EE_HR_ATTENDANCE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_EE_HR_ATTENDANCE\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Height = 149
        object evLabel2: TevLabel [0]
          Left = 397
          Top = 306
          Width = 133
          Height = 13
          Alignment = taRightJustify
          Caption = '~Attendance Type Number'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = False
        end
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 380
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 364
            Height = 225
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 331
              Height = 169
              inherited evDBGrid1: TevDBGrid
                Width = 331
                Height = 169
                Selected.Strings = (
                  'AttendanceDescr'#9'40'#9'Attendance'#9'F'
                  'EXCUSED'#9'11'#9'Excused'#9'F'
                  'HOURS_USED'#9'12'#9'Hours Used'#9'F'
                  'PERIOD_FROM'#9'18'#9'Period From'#9'F'
                  'PERIOD_TO'#9'18'#9'Period To'#9'F'
                  'WARNING_ISSUED'#9'16'#9'Warning Issued'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_HR_ATTENDANCE\evDBGrid1'
              end
            end
          end
        end
        object fpHRAttendanceDetail: TisUIFashionPanel
          Left = 8
          Top = 244
          Width = 364
          Height = 256
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpHRAttendanceDetail'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Attendance Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel7: TevLabel
            Left = 12
            Top = 74
            Width = 56
            Height = 13
            Caption = 'Hours Used'
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 73
            Height = 13
            Alignment = taRightJustify
            Caption = '~Attendance'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 180
            Top = 35
            Width = 56
            Height = 13
            Caption = 'Period From'
          end
          object evLabel4: TevLabel
            Left = 180
            Top = 74
            Width = 46
            Height = 13
            Caption = 'Period To'
          end
          object evLabel5: TevLabel
            Left = 12
            Top = 152
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object EvDBMemo1: TEvDBMemo
            Left = 12
            Top = 167
            Width = 332
            Height = 65
            DataField = 'NOTES'
            DataSource = wwdsDetail
            TabOrder = 6
          end
          object drgpPrimary_Rate: TevDBRadioGroup
            Left = 180
            Top = 113
            Width = 164
            Height = 36
            HelpContext = 19002
            Caption = 'Warning Issued'
            Columns = 2
            DataField = 'WARNING_ISSUED'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 160
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ATTENDANCE_DESCRIPTION'#9'40'#9'Attendance Description'#9'F')
            DataField = 'CO_HR_ATTENDANCE_TYPES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_ATTENDANCE_TYPES.CO_HR_ATTENDANCE_TYPES
            LookupField = 'CO_HR_ATTENDANCE_TYPES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 180
            Top = 50
            Width = 164
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'PERIOD_FROM'
            DataSource = wwdsDetail
            Date = 37007.000000000000000000
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 180
            Top = 89
            Width = 164
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'PERIOD_TO'
            DataSource = wwdsDetail
            Date = 37007.000000000000000000
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object evDBEdit4: TevDBEdit
            Left = 12
            Top = 89
            Width = 160
            Height = 21
            DataField = 'HOURS_USED'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 160
            Height = 36
            HelpContext = 19002
            Caption = 'Excused'
            Columns = 2
            DataField = 'EXCUSED'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object evDBEdit1: TevDBEdit
          Left = 397
          Top = 322
          Width = 130
          Height = 21
          DataField = 'ATTENDANCE_TYPE_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
          Visible = False
          WantReturns = False
          WordWrap = False
        end
      end
      inherited pnlDBControls: TevPanel
        Left = 4000
        Align = alNone
        Visible = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_HR_ATTENDANCE.EE_HR_ATTENDANCE
    MasterDataSource = wwdsSubMaster
  end
  inherited dsCL: TevDataSource
    Top = 63
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 47
  end
  inherited ActionList: TevActionList
    Top = 59
  end
end
