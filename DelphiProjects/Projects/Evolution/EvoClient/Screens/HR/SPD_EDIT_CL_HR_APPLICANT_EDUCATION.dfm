inherited EDIT_CL_HR_APPLICANT_EDUCATION: TEDIT_CL_HR_APPLICANT_EDUCATION
  Width = 931
  Height = 644
  inherited Panel1: TevPanel
    Width = 931
    inherited pnlFavoriteReport: TevPanel
      Left = 779
    end
    inherited pnlTopLeft: TevPanel
      Width = 779
    end
  end
  inherited PageControl1: TevPageControl
    Width = 931
    Height = 559
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 923
        Height = 530
        inherited pnlBorder: TevPanel
          Width = 919
          Height = 526
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 919
          Height = 526
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 320
              Width = 480
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                Width = 480
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  Width = 476
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 470
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 464
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 452
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 470
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 320
              Width = 480
              inherited pnlBrowse: TevPanel
                Left = 43
                Align = alNone
              end
              object sbApplicantBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 480
                Height = 565
                Align = alClient
                Color = clBtnFace
                ParentColor = False
                TabOrder = 1
                object evSplitter1: TevSplitter
                  Left = 356
                  Top = 6
                  Height = 549
                end
                object pnlLeftBorder: TevPanel
                  Left = 0
                  Top = 6
                  Width = 6
                  Height = 549
                  Align = alLeft
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 0
                end
                object pnlRightBorder: TevPanel
                  Left = 470
                  Top = 6
                  Width = 6
                  Height = 549
                  Align = alRight
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 1
                end
                object pnlTopBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 476
                  Height = 6
                  Align = alTop
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 2
                end
                object pnlBottomBorder: TevPanel
                  Left = 0
                  Top = 555
                  Width = 476
                  Height = 6
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 3
                end
                object pnlBrowseLeft: TevPanel
                  Left = 6
                  Top = 6
                  Width = 350
                  Height = 549
                  Align = alLeft
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 4
                  object fpBrowseLeft: TisUIFashionPanel
                    Left = 6
                    Top = 6
                    Width = 338
                    Height = 537
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 12
                    Color = 14737632
                    TabOrder = 0
                    RoundRect = True
                    ShadowDepth = 8
                    ShadowSpace = 8
                    ShowShadow = True
                    ShadowColor = clSilver
                    TitleColor = clGrayText
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWhite
                    TitleFont.Height = -13
                    TitleFont.Name = 'Arial'
                    TitleFont.Style = [fsBold]
                    Title = 'Company'
                    LineWidth = 0
                    LineColor = clWhite
                    Theme = ttCustom
                    object pnlFPLeftBody: TevPanel
                      Left = 12
                      Top = 35
                      Width = 297
                      Height = 298
                      BevelOuter = bvNone
                      ParentColor = True
                      TabOrder = 0
                    end
                  end
                end
                object pnlBrowseRight: TevPanel
                  Left = 359
                  Top = 6
                  Width = 111
                  Height = 549
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 5
                  object fpBrowseRight: TisUIFashionPanel
                    Left = 6
                    Top = 6
                    Width = 99
                    Height = 537
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 12
                    Color = 14737632
                    TabOrder = 0
                    RoundRect = True
                    ShadowDepth = 8
                    ShadowSpace = 8
                    ShowShadow = True
                    ShadowColor = clSilver
                    TitleColor = clGrayText
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWhite
                    TitleFont.Height = -13
                    TitleFont.Name = 'Arial'
                    TitleFont.Style = [fsBold]
                    Title = 'Applicant'
                    LineWidth = 0
                    LineColor = clWhite
                    Theme = ttCustom
                    object pnlFPRightBody: TevPanel
                      Left = 12
                      Top = 35
                      Width = 297
                      Height = 298
                      BevelOuter = bvNone
                      ParentColor = True
                      TabOrder = 0
                    end
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 871
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 871
            Height = 419
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 415
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Height = 415
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 335
                IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_EDUCATION\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 544
              Height = 415
              inherited evDBGrid1: TevDBGrid
                Width = 494
                Height = 335
                IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_EDUCATION\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tshtDetails: TTabSheet
      inherited pnlBrowseDetails: TevPanel
        Left = 33
        Top = 25
        Width = 875
        Height = 213
      end
      object sbApplicantEd: TScrollBox [1]
        Left = 0
        Top = 0
        Width = 923
        Height = 530
        Align = alClient
        TabOrder = 1
        object pnlDetailsBorder: TevPanel
          Left = 0
          Top = 0
          Width = 919
          Height = 526
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object fpEducationList: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 581
            Height = 265
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpEducationList'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlFPListBody: TevPanel
              Left = 12
              Top = 35
              Width = 545
              Height = 206
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
            end
          end
        end
      end
      inherited pnlDBControls: TevPanel
        Left = 28
        Top = 275
        Width = 425
        Height = 251
        ParentColor = True
        TabOrder = 3
      end
      inherited sbAppDescBaseDetail: TScrollBox
        Width = 923
        Height = 530
        inherited fpAppDescBaseSummary: TisUIFashionPanel
          Width = 451
          inherited evDBGrid2: TevDBGrid
            Width = 419
            Selected.Strings = (
              'SchoolName'#9'42'#9'School Name'#9'F'
              'MAJOR'#9'30'#9'Major'#9'F'
              'DEGREE_LEVEL'#9'1'#9'Degree Level'#9'F'
              'GPA'#9'10'#9'Gpa'#9'F'
              'START_DATE'#9'18'#9'Start Date'#9'F'
              'END_DATE'#9'18'#9'End Date'#9'F'
              'GRADUATION_DATE'#9'18'#9'Graduation Date'#9'F')
            IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_EDUCATION\evDBGrid2'
            Anchors = []
            BiDiMode = bdRightToLeftNoAlign
            ParentBiDiMode = False
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            PaintOptions.AlternatingRowColor = 14544093
          end
        end
        object fpEducationDetails: TisUIFashionPanel
          Left = 8
          Top = 322
          Width = 451
          Height = 305
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Education Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel8: TevLabel
            Left = 12
            Top = 152
            Width = 28
            Height = 13
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'Notes'
            ParentBiDiMode = False
          end
          object evLabel7: TevLabel
            Left = 308
            Top = 113
            Width = 76
            Height = 13
            Alignment = taRightJustify
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'Graduation date'
            ParentBiDiMode = False
          end
          object evLabel6: TevLabel
            Left = 308
            Top = 74
            Width = 43
            Height = 13
            Alignment = taRightJustify
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'End date'
            ParentBiDiMode = False
          end
          object evLabel5: TevLabel
            Left = 308
            Top = 35
            Width = 46
            Height = 13
            Alignment = taRightJustify
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'Start date'
            ParentBiDiMode = False
          end
          object evLabel4: TevLabel
            Left = 160
            Top = 113
            Width = 22
            Height = 13
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'GPA'
            ParentBiDiMode = False
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 74
            Width = 26
            Height = 13
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'Major'
            ParentBiDiMode = False
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 113
            Width = 64
            Height = 13
            BiDiMode = bdRightToLeftNoAlign
            Caption = 'Degree Level'
            ParentBiDiMode = False
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 44
            Height = 13
            BiDiMode = bdRightToLeftNoAlign
            Caption = '~School'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentBiDiMode = False
            ParentFont = False
          end
          object evDBComboBox1: TevDBComboBox
            Left = 12
            Top = 128
            Width = 140
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            ParentBiDiMode = False
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DEGREE_LEVEL'
            DataSource = wwdsSubDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Associates'#9'A'
              'Bachelors'#9'B'
              'Masters'#9'M'
              'Doctorate'#9'D'
              'GED'#9'G'
              'Certificate'#9'C'
              'Other'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object EvDBMemo1: TEvDBMemo
            Left = 12
            Top = 167
            Width = 417
            Height = 116
            BiDiMode = bdRightToLeftNoAlign
            DataField = 'NOTES'
            DataSource = wwdsSubDetail
            ParentBiDiMode = False
            TabOrder = 1
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 288
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            ParentBiDiMode = False
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            DataField = 'CL_HR_SCHOOL_NBR'
            DataSource = wwdsSubDetail
            LookupTable = DM_CL_HR_SCHOOL.CL_HR_SCHOOL
            LookupField = 'CL_HR_SCHOOL_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBEdit3: TevDBEdit
            Left = 160
            Top = 128
            Width = 140
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            ParentBiDiMode = False
            DataField = 'GPA'
            DataSource = wwdsSubDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit2: TevDBEdit
            Left = 12
            Top = 89
            Width = 288
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            ParentBiDiMode = False
            DataField = 'MAJOR'
            DataSource = wwdsSubDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 308
            Top = 128
            Width = 121
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'GRADUATION_DATE'
            DataSource = wwdsSubDetail
            Epoch = 1950
            ParentBiDiMode = False
            ShowButton = True
            TabOrder = 5
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 308
            Top = 89
            Width = 121
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'END_DATE'
            DataSource = wwdsSubDetail
            Epoch = 1950
            ParentBiDiMode = False
            ShowButton = True
            TabOrder = 6
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 308
            Top = 50
            Width = 121
            Height = 21
            BiDiMode = bdRightToLeftNoAlign
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'START_DATE'
            DataSource = wwdsSubDetail
            Epoch = 1950
            ParentBiDiMode = False
            ShowButton = True
            TabOrder = 7
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 931
  end
  inherited wwdsSubDetail: TevDataSource
    DataSet = DM_CL_HR_PERSON_EDUCATION.CL_HR_PERSON_EDUCATION
  end
end
