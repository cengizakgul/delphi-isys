inherited EDIT_EE_HR_SUBBROWSE_BASE: TEDIT_EE_HR_SUBBROWSE_BASE
  inherited PageControl1: TevPageControl
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 306
                IniAttributes.SectionName = 'TEDIT_EE_HR_SUBBROWSE_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 306
                IniAttributes.SectionName = 'TEDIT_EE_HR_SUBBROWSE_BASE\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      object sb_EE_HR_SUBBrowse: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 342
        Align = alClient
        TabOrder = 0
        object pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Left = 0
          Top = 0
          Width = 410
          Height = 241
          BevelOuter = bvNone
          BorderWidth = 12
          ParentColor = True
          TabOrder = 0
          object fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 12
            Top = 12
            Width = 386
            Height = 217
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            OnResize = fp_EE_HR_SubbrowseResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnl_EE_HR_SubbrowseBody: TevPanel
              Left = 12
              Top = 35
              Width = 349
              Height = 149
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object evDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 349
                Height = 149
                DisableThemesInTitle = False
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_EE_HR_SUBBROWSE_BASE\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
      object pnlDBControls: TevPanel
        Left = 0
        Top = -10
        Width = 427
        Height = 159
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
      end
    end
  end
end
