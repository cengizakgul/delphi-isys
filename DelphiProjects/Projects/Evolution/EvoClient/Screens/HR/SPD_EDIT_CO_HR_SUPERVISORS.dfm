inherited EDIT_CO_HR_SUPERVISORS: TEDIT_CO_HR_SUPERVISORS
  inherited PageControl1: TevPageControl
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Supervisors'
                    end
                  end
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 207
                IniAttributes.SectionName = 'TEDIT_CO_HR_SUPERVISORS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Supervisors'
              inherited evDBGrid1: TevDBGrid
                Height = 207
                Selected.Strings = (
                  'SUPERVISOR_NAME'#9'40'#9'Supervisor Name'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_SUPERVISORS\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        inherited pnlevBorder: TevPanel
          inherited pnlFashionDetails: TisUIFashionPanel
            Width = 320
            inherited pnlFashionDetailBody: TevPanel
              Width = 288
              Height = 226
              Align = alClient
              inherited pnlBrowse: TevPanel
                Width = 288
                Height = 226
                inherited evDBGrid2: TevDBGrid
                  Width = 288
                  Height = 226
                  Selected.Strings = (
                    'SUPERVISOR_NAME'#9'41'#9'Supervisor Name'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_SUPERVISORS\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Width = 320
            Title = 'Supervisor Details'
            inherited pnlFPDBControlsBody: TevPanel
              inherited pnlDBControls: TevPanel
                Top = 0
                Height = 97
                inherited evLabel1: TevLabel
                  Caption = '~Name'
                end
                inherited dedtDescription: TevDBEdit
                  Width = 283
                  DataField = 'SUPERVISOR_NAME'
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_SUPERVISORS.CO_HR_SUPERVISORS
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 416
  end
end
