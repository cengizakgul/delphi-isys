// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_CAR;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, Db,  
  DBActns, ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvUIComponents, EvUtils, EvClientDataSet,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, ISBasicClasses, SDDClasses, SDataDictclient,
  ImgList, SDataDicttemp, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_HR_CAR = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    pnlHREEVehicle: TisUIFashionPanel;
  private
    { Private declarations }
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    
  end;

implementation

uses SPD_EDIT_Open_BASE, SPD_EDIT_EE_HR_BASE;

{$R *.DFM}

{ TEDIT_EE_HR_CAR }

procedure TEDIT_EE_HR_CAR.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS( DM_COMPANY.CO_HR_CAR, aDS );
  inherited;
end;

function TEDIT_EE_HR_CAR.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_HR_CAR;
end;

function TEDIT_EE_HR_CAR.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

initialization
  RegisterClass( TEDIT_EE_HR_CAR );

end.
