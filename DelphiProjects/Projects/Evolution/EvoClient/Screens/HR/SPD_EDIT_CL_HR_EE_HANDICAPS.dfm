inherited EDIT_CL_HR_EE_HANDICAPS: TEDIT_CL_HR_EE_HANDICAPS
  Width = 724
  Height = 555
  inherited Panel1: TevPanel
    Width = 724
    inherited pnlFavoriteReport: TevPanel
      Left = 572
    end
    inherited pnlTopLeft: TevPanel
      Width = 572
    end
  end
  inherited PageControl1: TevPageControl
    Width = 724
    Height = 468
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 716
        Height = 439
        inherited pnlBorder: TevPanel
          Width = 712
          Height = 435
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 712
          Height = 435
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Width = 675
            end
            inherited pnlSubbrowse: TevPanel
              Width = 350
            end
          end
          inherited Panel3: TevPanel
            Width = 664
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 664
            Height = 328
            inherited Splitter1: TevSplitter
              Height = 324
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 324
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 244
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_HANDICAPS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 342
              Height = 324
              inherited wwDBGrid4: TevDBGrid
                Width = 292
                Height = 244
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_HANDICAPS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Width = 716
        Height = 439
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 359
          Height = 230
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 343
            Height = 214
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 307
              inherited evDBGrid1: TevDBGrid
                Width = 307
                Selected.Strings = (
                  'Handicap'#9'45'#9'Disabilities'#9'F')
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_HANDICAPS\evDBGrid1'
              end
            end
          end
        end
        object fpDisabilityDetails: TisUIFashionPanel
          Left = 8
          Top = 232
          Width = 342
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Disability Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblHandicap: TevLabel
            Left = 12
            Top = 35
            Width = 65
            Height = 13
            Caption = '~Disability'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 308
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'SY_HR_HANDICAPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_HR_HANDICAPS.SY_HR_HANDICAPS
            LookupField = 'SY_HR_HANDICAPS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 724
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_HR_PERSON_HANDICAPS.CL_HR_PERSON_HANDICAPS
    MasterDataSource = wwdsSubMaster2
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 488
    Top = 24
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 540
    Top = 128
  end
end
