// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_DESCR_BASE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  SPD_EDIT_CO_HR_BASE, Mask, wwdbedit, EvUtils, SDDClasses,
  ISBasicClasses, SDataDictclient, SDataDicttemp, EvUIComponents,
  isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_DESCR_BASE = class(TEDIT_CO_HR_BASE)
    dedtDescription: TevDBEdit;
    evLabel1: TevLabel;
  private
    { Private declarations }
  protected
    procedure AfterDataSetsReopen; override;
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_DESCR }

procedure TEDIT_CO_HR_DESCR_BASE.AfterDataSetsReopen;
begin
  inherited;
  SetReadOnly(GetIfReadOnly or BlockHr);
end;

function TEDIT_CO_HR_DESCR_BASE.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

initialization
  RegisterClass( TEDIT_CO_HR_DESCR_BASE );

end.
