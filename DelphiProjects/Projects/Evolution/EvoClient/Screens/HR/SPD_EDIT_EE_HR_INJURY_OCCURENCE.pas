// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_INJURY_OCCURENCE;

interface                                      

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_BASE, Db,   DBActns,
  ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Mask, wwdbedit,
  wwdbdatetimepicker, SPD_EDIT_EE_HR_SUBBROWSE_BASE, 
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, SDataDictsystem, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet, isUIDBMemo,
  isUIwwDBDateTimePicker, isUIwwDBEdit, ImgList, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_HR_INJURY_OCCURRENCE = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    tbshBlobs: TTabSheet;
    EvDBMemo1: TEvDBMemo;
    EvDBMemo2: TEvDBMemo;
    EvDBMemo3: TEvDBMemo;
    DM_SYSTEM_HR: TDM_SYSTEM_HR;
    fpCause: TisUIFashionPanel;
    fpRestrictions: TisUIFashionPanel;
    fpSpecialNotes: TisUIFashionPanel;
    fpInjuryDetails: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel14: TevLabel;
    evLabel24: TevLabel;
    evLabel23: TevLabel;
    evLabel25: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBEdit2: TevDBEdit;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBEdit14: TevDBEdit;
    evDBEdit16: TevDBEdit;
    evDBEdit15: TevDBEdit;
    fpMedicalFacility: TisUIFashionPanel;
    evLabel16: TevLabel;
    evLabel17: TevLabel;
    evLabel18: TevLabel;
    evLabel19: TevLabel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evDBEdit3: TevDBEdit;
    evDBEdit8: TevDBEdit;
    evDBEdit9: TevDBEdit;
    evDBEdit10: TevDBEdit;
    evDBEdit11: TevDBEdit;
    evDBEdit12: TevDBEdit;
    evDBEdit13: TevDBEdit;
    fpTreatmentDetails: TisUIFashionPanel;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBRadioGroup4: TevDBRadioGroup;
    evDBRadioGroup5: TevDBRadioGroup;
    evDBRadioGroup6: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    evLabel7: TevLabel;
    evDBEdit4: TevDBEdit;
    evLabel10: TevLabel;
    evDBEdit7: TevDBEdit;
    evLabel15: TevLabel;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evLabel8: TevLabel;
    evDBEdit5: TevDBEdit;
    evLabel9: TevLabel;
    evDBEdit6: TevDBEdit;
    sbInjuryNotes: TScrollBox;
    procedure evDBRadioGroup3Change(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;


  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;

  end;

implementation

uses SPD_EDIT_Open_BASE;

//
{$R *.DFM}

{ TEDIT_EE_HR_INJURY_OCCURRENCE }

procedure TEDIT_EE_HR_INJURY_OCCURRENCE.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  inherited;
  AddDS( DM_COMPANY.CO_WORKERS_COMP, aDS );
end;

function TEDIT_EE_HR_INJURY_OCCURRENCE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_HR_INJURY_OCCURRENCE;
end;

function TEDIT_EE_HR_INJURY_OCCURRENCE.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;
end;

procedure TEDIT_EE_HR_INJURY_OCCURRENCE.evDBRadioGroup3Change(
  Sender: TObject);
begin
  if evDBRadioGroup3.Value = 'N' then
  begin
    evDBDateTimePicker3.Enabled := False;
    if wwdsDetail.DataSet.State <> dsBrowse then
      wwdsDetail.DataSet.FieldByName('DATE_OF_DEATH').Clear;
  end
  else
    evDBDateTimePicker3.Enabled := True;
end;

initialization
  RegisterClass( TEDIT_EE_HR_INJURY_OCCURRENCE );

end.
